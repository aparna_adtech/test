﻿using com.breg.vision.bcsintegration.BCSCacheDataProcessing.BCSProxy;
using com.breg.vision.Logger;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace com.breg.vision.bcsintegration.BCSCacheDataProcessing
{
    public class BCSCacheSync
    {
        string sAuthToken = ConfigurationManager.AppSettings["sAuthToken"];
        private static string BCGSvcEndpoint = ConfigurationManager.AppSettings["BCGSvcEndpoint"];
        int MaxObjects = Convert.ToInt32(ConfigurationManager.AppSettings["MaxObjects"].ToString());
        BCSProxy.BCGSystemType eSystemToken = BCSProxy.BCGSystemType.BCS;
        BCSCacheDataLayer bCSCacheDataLayer;
		public Guid _CorrelationId { get; private set; }
		public FileLogger _Logger { get; private set; }
        public SQLLogger _sqlLogger { get; private set; }

        public BCSCacheSync(Guid correlationId, FileLogger logger, SQLLogger sqlLogger)
        {
			_CorrelationId = correlationId;
			_Logger = logger;
            _sqlLogger = sqlLogger;
			bCSCacheDataLayer = new BCSCacheDataLayer();
        }
     
        #region Methods 
        private List<ItemMasterItem> GetBCSData()
        {
            Console.WriteLine($"Started gettting data from BCG {DateTime.Now}");
            var proxy = new BCSProxy.BregGatewayServiceClient(BCGSvcEndpoint);
            // TODO Needs Clarification  
            System.Net.ServicePointManager.ServerCertificateValidationCallback += (a, b, c, d) => true;
            var items = proxy.GetAllItems(sAuthToken, eSystemToken).ToList();
			_Logger.WriteLog(items.Count + " BCS Items Retrieved \n", null, "com.breg.vision.bcsintegration.BCSCacheDataProcessing", _CorrelationId);
            Console.WriteLine($"Finished retrieving data from BCG {DateTime.Now}");
            return items;
        }

        public void UpdateBCSCache()
        {
            var items = GetBCSData();

            if (MaxObjects != 0)
                items = items.GetRange(0, MaxObjects).ToList();

            bCSCacheDataLayer.UpdateBCSCache(items, _CorrelationId, _Logger,_sqlLogger);

            _Logger.WriteLog("Synchronization Complete for Process" + _CorrelationId, null, "com.breg.vision.bcsintegration.BCSCacheDataProcessing", _CorrelationId);
        }
        #endregion
    }
}
