﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.bcsintegration.BCSCacheDataProcessing
{
    public class BCSDataSyncException : Exception
    {
        public BCSDataSyncException()
        {

        }

        public BCSDataSyncException(string message)
            : base(message)
        {

        }

        public BCSDataSyncException(string message,Exception inner)
            : base(message,inner)
        {

        }

    }
}
