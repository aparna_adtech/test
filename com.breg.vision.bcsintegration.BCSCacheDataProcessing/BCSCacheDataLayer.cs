﻿using com.breg.vision.bcsintegration.BCSCacheDataProcessing.BCSProxy;
using com.breg.vision.Logger;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel;

namespace com.breg.vision.bcsintegration.BCSCacheDataProcessing
{
    public class BCSCacheDataLayer
    {
        private static bool isSqlLogging = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSqlLogging"]);
		private static string connString = ConfigurationManager.ConnectionStrings["BCSItemContext"].ToString();

		public BCSCacheDataLayer()
        {

        }

        public void UpdateBCSCache(List<ItemMasterItem> items, Guid CorelationID, FileLogger logger, SQLLogger sqlLogger)
        {
            try
            {
                string LogMessage = "";
                using (var db = new BCSItemContext())
                {
                    var tempTableName = "BCSItemCatalog_" + Guid.NewGuid().ToString("N");

                    if (isSqlLogging)
                        db.Database.Log = sqlLogger.SQLpointer.Write;

                    LogMessage = $"Process {CorelationID} Retrieved {items.Count} BCS Cache  Records at {DateTime.Now}\n";
                    logger.WriteLog(LogMessage, null, "com.breg.vision.bcsintegration.BCSCacheDataProcessing", CorelationID);

                    db.Database.ExecuteSqlCommand($"Select top 1 * into [{tempTableName}] from BCSItemCatalog");
                    db.Database.ExecuteSqlCommand($"Truncate table [{tempTableName}]");
                    Console.WriteLine("Created temporary table " + DateTime.Now);

                    Console.WriteLine("Started copying data to a temporary table "+DateTime.Now);
                    BulkInsert<ItemMasterItem>(tempTableName, items);
                    Console.WriteLine("Copied data successfully " + DateTime.Now);
                    using (var dbTransaction = db.Database.BeginTransaction())
                    {
                        try
                        {
                            Console.WriteLine("Truncating BCSItemCatalog table " + DateTime.Now);
                            db.Database.ExecuteSqlCommand("Truncate table BCSItemCatalog");

                            Console.WriteLine($"Started writing data back to BCSItemCatalog table from temp table {tempTableName} at {DateTime.Now}");
                            db.Database.ExecuteSqlCommand($"Insert into BCSItemCatalog Select * from [{tempTableName}]");
                            db.Database.ExecuteSqlCommand($"Update BCSItemCatalog set CacheItemCreatedDate=getdate()");
                            Console.WriteLine($"Finished writing data back to BCSItemCatalog table from temp table {tempTableName} at {DateTime.Now}");

                            dbTransaction.Commit();
                        }
                        catch (Exception)
                        {
                            if (dbTransaction != null)
                                dbTransaction.Rollback();
                        }                      
                    }

                    Console.WriteLine($"Dropping temporary table {tempTableName} at {DateTime.Now}");
                    db.Database.ExecuteSqlCommand($"Drop table [{tempTableName}]");
                    LogMessage = $"Process {CorelationID} Updated {items.Count} BCS Cache Records at {DateTime.Now}\n";
                    logger.WriteLog(LogMessage, null, "com.breg.vision.bcsintegration.BCSCacheDataProcessing", CorelationID);
                }
            }           
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new BCSDataSyncException(message, raise);
                    }
                }
                throw raise;
            }
        }

		public static void BulkInsert<T>(string tableName, IList<T> list)
		{
			using (var bulkCopy = new SqlBulkCopy(connString))
			{
				bulkCopy.BatchSize = list.Count;
				bulkCopy.DestinationTableName = tableName;

				var table = new DataTable();
				var props = TypeDescriptor.GetProperties(typeof(T))
										   //Dirty hack to make sure we only have system data types 
										   //i.e. filter out the relationships/collections
										   .Cast<PropertyDescriptor>()
										   .Where(propertyInfo => propertyInfo.PropertyType.Namespace.Equals("System"))
										   .ToArray();

				foreach (var propertyInfo in props)
				{
					bulkCopy.ColumnMappings.Add(propertyInfo.Name, propertyInfo.Name);
					table.Columns.Add(propertyInfo.Name, Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType);
				}

				var values = new object[props.Length];
				foreach (var item in list)
				{
					for (var i = 0; i < values.Length; i++)
					{
						object val = props[i].GetValue(item);
						if (val != null && val.GetType() == typeof(DateTime) && ((DateTime)val) == DateTime.MinValue)
							val = null;
						values[i] = val;
					}
					table.Rows.Add(values);
				}

				bulkCopy.WriteToServer(table);
			}
		}
	}
}
