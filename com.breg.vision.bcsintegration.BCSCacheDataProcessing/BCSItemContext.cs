namespace com.breg.vision.bcsintegration.BCSCacheDataProcessing
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class BCSItemContext : DbContext
    {
        public BCSItemContext()
            : base("name=BCSItemContext")
        {
        }

        public virtual DbSet<BCSItemCatalog> BCSItemCatalogs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BCSItemCatalog>()
                .Property(e => e.ATR)
                .HasPrecision(18, 0);

            modelBuilder.Entity<BCSItemCatalog>()
                .Property(e => e.aTRField)
                .HasPrecision(18, 0);
        }
    }
}
