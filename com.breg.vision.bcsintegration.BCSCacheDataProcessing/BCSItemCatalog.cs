namespace com.breg.vision.bcsintegration.BCSCacheDataProcessing
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BCSItemCatalog")]
    public partial class BCSItemCatalog
    {
        public decimal? ATR { get; set; }

        public bool? ATRSpecified { get; set; }

        public bool? Breg_Rental { get; set; }

        public bool? Breg_RentalSpecified { get; set; }

        public DateTime? CreateDate { get; set; }

        public bool? CreateDateSpecified { get; set; }

        public string CreatedBy { get; set; }

        public string DefaultHCPCS { get; set; }

        public string DefaultShippingOrg { get; set; }

        public string HCPCSCode { get; set; }

        public int? HCPCSMultiplier { get; set; }

        public bool? HCPCSMultiplierSpecified { get; set; }

        public long? ID { get; set; }

        public bool? IDSpecified { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int InventoryItemID { get; set; }

        public bool? InventoryItemIDSpecified { get; set; }

        public string InventoryItemStatusCode { get; set; }

        public bool? IsModel { get; set; }

        public bool? IsModelSpecified { get; set; }

        public bool? IsSubstituteItem { get; set; }

        public bool? IsSubstituteItemSpecified { get; set; }

        public string ItemDescription { get; set; }

        public string ItemNumber { get; set; }

        public DateTime? LastUpdateDate { get; set; }

        public bool? LastUpdateDateSpecified { get; set; }

        public string LastUpdatedBy { get; set; }

        public string LegacyItemNo { get; set; }

        public int? MaxOrderQuantity { get; set; }

        public bool? MaxOrderQuantitySpecified { get; set; }

        public int? Max_MinmaxQuantity { get; set; }

        public bool? Max_MinmaxQuantitySpecified { get; set; }

        public int? MinOrderQuantity { get; set; }

        public bool? MinOrderQuantitySpecified { get; set; }

        public int? Min_MinmaxQuantity { get; set; }

        public bool? Min_MinmaxQuantitySpecified { get; set; }

        public int? OnHandQuantity { get; set; }

        public bool? OnHandQuantitySpecified { get; set; }

        public string OrgName { get; set; }

        public int? OrganizationId { get; set; }

        public bool? OrganizationIdSpecified { get; set; }

        public int? PickingOrder { get; set; }

        public bool? PickingOrderSpecified { get; set; }

        public int? PrimarySubInventoryFlag { get; set; }

        public bool? PrimarySubInventoryFlagSpecified { get; set; }

        public string PrimaryUomCode { get; set; }

        public string ProductLine { get; set; }

        public int? Quantity { get; set; }

        public bool? QuantitySpecified { get; set; }

        public string SecondaryInventory { get; set; }

        public string SerialNumber { get; set; }

        public int? SourceOrganizationId { get; set; }

        public bool? SourceOrganizationIdSpecified { get; set; }

        public string SourceSubInventory { get; set; }

        public string SubInvName { get; set; }

        public int? TotalRecords { get; set; }

        public bool? TotalRecordsSpecified { get; set; }

        public decimal? aTRField { get; set; }

        public bool? aTRFieldSpecified { get; set; }

        public bool? breg_RentalField { get; set; }

        public bool? breg_RentalFieldSpecified { get; set; }

        public DateTime? createDateField { get; set; }

        public bool? createDateFieldSpecified { get; set; }

        public string createdByField { get; set; }

        public string defaultHCPCSField { get; set; }

        public string defaultShippingOrgField { get; set; }

        public string hCPCSCodeField { get; set; }

        public int? hCPCSMultiplierField { get; set; }

        public bool? hCPCSMultiplierFieldSpecified { get; set; }

        public long? idField { get; set; }

        public bool? idFieldSpecified { get; set; }

        public int? inventoryItemIDField { get; set; }

        public bool? inventoryItemIDFieldSpecified { get; set; }

        public string inventoryItemStatusCodeField { get; set; }

        public bool? isModelField { get; set; }

        public bool? isModelFieldSpecified { get; set; }

        public bool? isSubstituteItemField { get; set; }

        public bool? isSubstituteItemFieldSpecified { get; set; }

        public string itemDescriptionField { get; set; }

        public string itemNumberField { get; set; }

        public DateTime? lastUpdateDateField { get; set; }

        public bool? lastUpdateDateFieldSpecified { get; set; }

        public string lastUpdatedByField { get; set; }

        public string legacyItemNoField { get; set; }

        public int? maxOrderQuantityField { get; set; }

        public bool? maxOrderQuantityFieldSpecified { get; set; }

        public int? max_MinmaxQuantityField { get; set; }

        public bool? max_MinmaxQuantityFieldSpecified { get; set; }

        public int? minOrderQuantityField { get; set; }

        public bool? minOrderQuantityFieldSpecified { get; set; }

        public int? min_MinmaxQuantityField { get; set; }

        public bool? min_MinmaxQuantityFieldSpecified { get; set; }

        public int? onHandQuantityField { get; set; }

        public bool? onHandQuantityFieldSpecified { get; set; }

        public string orgNameField { get; set; }

        public int? organizationIdField { get; set; }

        public bool? organizationIdFieldSpecified { get; set; }

        public int? pickingOrderField { get; set; }

        public bool? pickingOrderFieldSpecified { get; set; }

        public int? primarySubInventoryFlagField { get; set; }

        public bool? primarySubInventoryFlagFieldSpecified { get; set; }

        public string primaryUomCodeField { get; set; }

        public string productLineField { get; set; }

        public int? quantityField { get; set; }

        public bool? quantityFieldSpecified { get; set; }

        public string secondaryInventoryField { get; set; }

        public string serialNumberField { get; set; }

        public int? sourceOrganizationIdField { get; set; }

        public bool? sourceOrganizationIdFieldSpecified { get; set; }

        public string sourceSubInventoryField { get; set; }

        public string subInvNameField { get; set; }

        public int? totalRecordsField { get; set; }

        public bool? totalRecordsFieldSpecified { get; set; }

        public DateTime? CacheItemCreatedDate { get; set; }
    }
}
