﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using HelpSystem.HelpSystemService;
using System.ComponentModel;
using System.Text;

namespace HelpSystem
{
    public delegate void GetHelpGroupCompleted(HelpGroupResponse response);
    public delegate void GetHelpArticleCompleted(HelpArticleResponse response);
    public delegate void SearchForHelpArticlesAndHelpMeidaCompleted(HelpGroupResponse response);
    public delegate void InsertHelpGroupCompleted(HelpGroupResponse response);
    public delegate void InsertHelpArticleCompleted(HelpArticleResponse response);
    public delegate void InsertHelpMediaCompleted(HelpMediaResponse response);
    public delegate void DeleteHelpEntityCompleted(HelpEntityResponse response);
    public delegate void InsertHelpEntityMapCompleted(HelpEntityResponse response);
    public delegate void UpdateHelpGroupCompleted(HelpGroupResponse response);
    public delegate void UpdateHelpArticleCompleted(HelpArticleResponse response);
    public delegate void UpdateHelpMediaCompleted(HelpMediaResponse response);
    public delegate void UpdateHelpEntityMapCompleted(HelpEntityResponse response);

    public class MainViewModel : INotifyPropertyChanged
    {
        public event GetHelpGroupCompleted OnGetHelpGroupCompleted;
        public event GetHelpArticleCompleted OnGetHelpArticleCompleted;
        public event SearchForHelpArticlesAndHelpMeidaCompleted OnSearchForHelpArticlesAndHelpMeidaCompleted;
        public event InsertHelpGroupCompleted OnInsertHelpGroupCompleted;
        public event InsertHelpArticleCompleted OnInsertHelpArticleCompleted;
        public event InsertHelpEntityMapCompleted OnInsertHelpEntityMapCompleted;
        public event InsertHelpMediaCompleted OnInsertHelpMediaCompleted;
        public event DeleteHelpEntityCompleted OnDeleteHelpEntityCompleted;
        public event UpdateHelpGroupCompleted OnUpdateHelpGroupCompleted;
        public event UpdateHelpArticleCompleted OnUpdateHelpArticleCompleted;
        public event UpdateHelpMediaCompleted OnUpdateHelpMediaCompleted;
        public event UpdateHelpEntityMapCompleted OnUpdateHelpEntityMapCompleted;

        HelpGroup _HelpGroup;
        HelpSystemServiceClient client;

        public MainViewModel()
        {
            client = new HelpSystemServiceClient();

            client.Endpoint.Address = GetServiceURI("HelpSystem/HelpSystemService.svc");
            if (client.Endpoint.Address.Uri.Scheme.ToLower() == "https")
                client.Endpoint.Binding = new System.ServiceModel.BasicHttpBinding(System.ServiceModel.BasicHttpSecurityMode.Transport);

            client.GetHelpGroupCompleted += new EventHandler<GetHelpGroupCompletedEventArgs>(client_GetHelpGroupCompleted);
            client.GetHelpArticleCompleted += new EventHandler<GetHelpArticleCompletedEventArgs>(client_GetHelpArticleCompleted);
            client.SearchForHelpArticlesAndHelpMeidaCompleted += new EventHandler<SearchForHelpArticlesAndHelpMeidaCompletedEventArgs>(client_SearchForHelpArticlesAndHelpMeidaCompleted);
            client.InsertHelpGroupCompleted += new EventHandler<InsertHelpGroupCompletedEventArgs>(client_InsertHelpGroupCompleted);
            client.InsertHelpArticleCompleted += new EventHandler<InsertHelpArticleCompletedEventArgs>(client_InsertHelpArticleCompleted);
            client.InsertHelpMediaCompleted += new EventHandler<InsertHelpMediaCompletedEventArgs>(client_InsertHelpMediaCompleted);
            client.DeleteHelpEntityCompleted += new EventHandler<DeleteHelpEntityCompletedEventArgs>(client_DeleteHelpEntityCompleted);
            client.InsertHelpEntityMapCompleted += new EventHandler<InsertHelpEntityMapCompletedEventArgs>(client_InsertHelpEntityMapCompleted);
            client.UpdateHelpGroupCompleted += new EventHandler<UpdateHelpGroupCompletedEventArgs>(client_UpdateHelpGroupCompleted);
            client.UpdateHelpArticleCompleted += new EventHandler<UpdateHelpArticleCompletedEventArgs>(client_UpdateHelpArticleCompleted);
            client.UpdateHelpMediaCompleted += new EventHandler<UpdateHelpMediaCompletedEventArgs>(client_UpdateHelpMediaCompleted);
            client.UpdateHelpEntityMapCompleted += new EventHandler<UpdateHelpEntityMapCompletedEventArgs>(client_UpdateHelpEntityMapCompleted);

        }

        public static System.ServiceModel.EndpointAddress GetServiceURI(string serviceAddress)
        {
            System.ServiceModel.EndpointAddress newAddress;
            Uri baseUriString = GetBaseUri();
            string newUriString = baseUriString + serviceAddress;
            Uri newUri = new Uri(newUriString);
            newAddress = new System.ServiceModel.EndpointAddress(newUri);
            return newAddress;
        }

        private static Uri GetBaseUri()
        {
            
            Uri currentUri = new Uri( Application.Current.Host.Source.AbsoluteUri);
            string baseUrl = getBaseUrl(currentUri);

            return new Uri(baseUrl); 
        }
        private static string getBaseUrl(Uri currentUri)
        {
        	StringBuilder url = new StringBuilder();
            url.Append(currentUri.Scheme);
        	url.Append("://");
            url.Append(currentUri.Host);
            if (currentUri.Port != 80)
        	{
        		url.Append(":");
                url.Append(currentUri.Port);
        	}
            if (currentUri.Host.ToLower() == "localhost")
            {
                url.Append(getFirstSegment(currentUri.AbsolutePath));
            }
            url.Append("/");
        	return url.ToString();
        }
        //when we develop locally, we use a virtual directory, which always has vision in it's name. ??? I hope...
        private static string getFirstSegment(string absolutePath)
        {
            string[] segments = absolutePath.Split("/".ToCharArray());
            foreach (string segment in segments)
            {
                if (segment != "")
                {
                    if (segment.ToLower().Contains("vision"))
                    {
                        return string.Format("/{0}", segment);
                    }
                }
            }
            return "";
        }


        void client_GetHelpArticleCompleted(object sender, GetHelpArticleCompletedEventArgs e)
        {
            RaiseOnGetHelpArticleCompleted(e.Result);
        }

        void client_InsertHelpEntityMapCompleted(object sender, InsertHelpEntityMapCompletedEventArgs e)
        {
            RaiseInsertHelpEntityMapCompleted(e.Result);
        }

        void client_UpdateHelpEntityMapCompleted(object sender, UpdateHelpEntityMapCompletedEventArgs e)
        {
            RaiseUpdateHelpEntityMapCompleted(e.Result);
        }

        void client_DeleteHelpEntityCompleted(object sender, DeleteHelpEntityCompletedEventArgs e)
        {
            RaiseDeleteHelpEntityCompleted(e.Result);
        }

        void client_InsertHelpMediaCompleted(object sender, InsertHelpMediaCompletedEventArgs e)
        {
            RaiseInsertHelpMediaCompleted(e.Result);
        }

        void client_InsertHelpArticleCompleted(object sender, InsertHelpArticleCompletedEventArgs e)
        {
            RaiseInsertHelpArticleCompleted(e.Result);
        }

        void client_InsertHelpGroupCompleted(object sender, InsertHelpGroupCompletedEventArgs e)
        {
            RaiseInsertHelpGroupCompleted(e.Result);
        }

        void client_UpdateHelpMediaCompleted(object sender, UpdateHelpMediaCompletedEventArgs e)
        {
            RaiseUpdateHelpMediaCompleted(e.Result);
        }

        void client_UpdateHelpArticleCompleted(object sender, UpdateHelpArticleCompletedEventArgs e)
        {
            RaiseUpdateHelpArticleCompleted(e.Result);
        }

        void client_UpdateHelpGroupCompleted(object sender, UpdateHelpGroupCompletedEventArgs e)
        {
            RaiseUpdateHelpGroupCompleted(e.Result);
        }

        void client_SearchForHelpArticlesAndHelpMeidaCompleted(object sender, SearchForHelpArticlesAndHelpMeidaCompletedEventArgs e)
        {
            RaiseOnSearchForHelpArticlesAndHelpMeidaCompleted(e.Result);
        }

        public void SearchForHelpArticlesAndHelpMeida(string searchText)
        {
            client.SearchForHelpArticlesAndHelpMeidaAsync(searchText);
        }

        public void GetHelpArticle(int id)
        {
            client.GetHelpArticleAsync(id);
        }

        public void InsertHelpGroup(HelpGroup entity)
        {
            client.InsertHelpGroupAsync(entity);
        }

        public void InsertHelpArticle(HelpArticle entity)
        {
            client.InsertHelpArticleAsync(entity);
        }

        public void InsertHelpMedia(HelpMedia entity)
        {
            client.InsertHelpMediaAsync(entity);
        }

        public void UpdateHelpGroup(HelpGroup entity)
        {
            client.UpdateHelpGroupAsync(entity);
        }

        public void UpdateHelpArticle(HelpArticle entity)
        {
            client.UpdateHelpArticleAsync(entity);
        }

        public void UpdateHelpMedia(HelpMedia entity)
        {
            client.UpdateHelpMediaAsync(entity);
        }

        public void UpdateHelpEntityMap(HelpEntity entity)
        {
            client.UpdateHelpEntityMapAsync(entity);
        }

        public void InsertHelpEntityMap(HelpEntity entity)
        {
            client.InsertHelpEntityMapAsync(entity);
        }

        public void DeleteHelpEntity(HelpEntity entity, bool removeEntity)
        {
            //covariance is fun!  .NET 4.0 will fix this
            HelpEntity baseEntity = new HelpEntity();
            baseEntity.Id = entity.Id;
            baseEntity.ParentId = entity.ParentId;
            baseEntity.EntityId = entity.EntityId;
            baseEntity.HelpEntityType = entity.HelpEntityType;

            client.DeleteHelpEntityAsync(baseEntity, removeEntity);
        }

        private bool _IsSearchActive;

        public bool IsSearchActive
        {
            get { return _IsSearchActive; }
            set
            {
                _IsSearchActive = value;
                OnPropertyChanged("IsSearchActive");
            }
        }

        public HelpGroup HelpGroup
        {
            get { return _HelpGroup; }
            set 
            { 
                _HelpGroup = value;
                OnPropertyChanged("HelpGroup");
            }
        }

        bool _IsDragDropEnabled = false;
        public bool IsDragDropEnabled
        {
            get { return _IsDragDropEnabled; }
            set
            {
                _IsDragDropEnabled = value;
                OnPropertyChanged("IsDragDropEnabled");
            }
        }

        public void GetHelpGroup(int id, int depth)
        {
            IsSearchActive = true;
            client.GetHelpGroupAsync(id, depth);
        }

        void client_GetHelpGroupCompleted(object sender, GetHelpGroupCompletedEventArgs e)
        {
            RaiseOnGetHelpGroupCompleted(e.Result);
            IsSearchActive = false;
        }

        void RaiseOnGetHelpGroupCompleted(HelpGroupResponse response)
        {
            this.HelpGroup = response.HelpGroup;

            if (OnGetHelpGroupCompleted != null)
                OnGetHelpGroupCompleted(response);
        }

        void RaiseOnGetHelpArticleCompleted(HelpArticleResponse response)
        {
            //this.HelpArticle = response.HelpArticle;

            if (OnGetHelpArticleCompleted != null)
                OnGetHelpArticleCompleted(response);
        }

        void RaiseOnSearchForHelpArticlesAndHelpMeidaCompleted(HelpGroupResponse response)
        {
            this.HelpGroup = response.HelpGroup;

            if (OnSearchForHelpArticlesAndHelpMeidaCompleted != null)
                OnSearchForHelpArticlesAndHelpMeidaCompleted(response);
        }

        void RaiseInsertHelpGroupCompleted(HelpGroupResponse response)
        {
            if (OnInsertHelpGroupCompleted != null)
                OnInsertHelpGroupCompleted(response);
        }

        void RaiseInsertHelpArticleCompleted(HelpArticleResponse response)
        {
            if (OnInsertHelpArticleCompleted != null)
                OnInsertHelpArticleCompleted(response);
        }

        void RaiseInsertHelpMediaCompleted(HelpMediaResponse response)
        {
            if (OnInsertHelpMediaCompleted != null)
                OnInsertHelpMediaCompleted(response);
        }

        
        void RaiseUpdateHelpGroupCompleted(HelpGroupResponse response)
        {
            if (OnUpdateHelpGroupCompleted != null)
                OnUpdateHelpGroupCompleted(response);
        }

        void RaiseUpdateHelpArticleCompleted(HelpArticleResponse response)
        {
            if (OnUpdateHelpArticleCompleted != null)
                OnUpdateHelpArticleCompleted(response);
        }

        void RaiseUpdateHelpMediaCompleted(HelpMediaResponse response)
        {
            if (OnUpdateHelpMediaCompleted != null)
                OnUpdateHelpMediaCompleted(response);
        }


        void RaiseDeleteHelpEntityCompleted(HelpEntityResponse response)
        {
            if (OnDeleteHelpEntityCompleted != null)
                OnDeleteHelpEntityCompleted(response);
        }

        void RaiseUpdateHelpEntityMapCompleted(HelpEntityResponse response)
        {
            if (OnUpdateHelpEntityMapCompleted != null)
                OnUpdateHelpEntityMapCompleted(response);
        }


        void RaiseInsertHelpEntityMapCompleted(HelpEntityResponse response)
        {
            if (OnInsertHelpEntityMapCompleted != null)
                OnInsertHelpEntityMapCompleted(response);
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
