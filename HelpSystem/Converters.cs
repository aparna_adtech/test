﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;

namespace HelpSystem
{
    public class DescriptionToHtmlConverter : IValueConverter 
    { 
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) 
        { 
            string description = (string)value;
            return string.Format("<div style=\"height:100%;width:100%;padding:0;margin:0;font-family:Tahoma,Arial,Helvetica;font-size:12px;text-align:left\">{0}</div>", description); 
        } 
        
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) 
        { 
            throw new NotImplementedException(); 
        } 
    }
    
}
