﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using HelpSystem.HelpSystemService;

namespace HelpSystem
{
    public partial class AddArticle : UserControl
    {
        public string ArticleTitle
        {
            get { return txtTitle.Text; }
            set { txtTitle.Text = value; }
        }

        public string ArticleDescription
        {
            get { return txtDescription.Text; }
            set { txtDescription.Text = value; }
        }

        
        public AddArticle()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            ((RadWindow)this.Parent).DialogResult = true;
            ((RadWindow)this.Parent).Close();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            ((RadWindow)this.Parent).DialogResult = false;
            ((RadWindow)this.Parent).Close();
        }

        private void btnPreview_Click(object sender, RoutedEventArgs e)
        {
            RadWindow window = new RadWindow();
            window.ResizeMode = ResizeMode.CanResize;
            window.Width = 400;
            window.CanClose = false;
            window.Height = 400;
            
            PreviewArticle previewArticle = new PreviewArticle();
            HelpArticle entity = new HelpArticle(); 
            entity.Description = txtDescription.Text;
            previewArticle.DataContext = entity;
            window.Content = previewArticle;
            window.Header = "Preview Article Description";
            window.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterOwner;

            window.ShowDialog();
        }
    }
}
