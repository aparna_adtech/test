﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace HelpSystem
{
    public partial class PreviewArticle : UserControl
    {
        public PreviewArticle()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            ((RadWindow)this.Parent).DialogResult = false;
            ((RadWindow)this.Parent).Close();
        }
    }
}
