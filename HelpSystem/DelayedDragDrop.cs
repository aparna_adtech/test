﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections;

namespace HelpSystem
{
    public class DelayedDragDrop
    {
        
        public Action DragAction { get; set; }
        public Action DropActionUpdateEntity { get; set; }
        public Action DropActionInsertRelatedArticle { get; set; }
        public Action DropActionInsertRelatedMediaItem { get; set; } 

        public object DraggedItem { get; set; }
        
    }
}
