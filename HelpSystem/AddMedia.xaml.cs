﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace HelpSystem
{
    public partial class AddMedia : UserControl
    {
        public string MediaTitle
        {
            get { return txtTitle.Text; }
            set { txtTitle.Text = value; }
        }

        public string MediaDescription
        {
            get { return txtDescription.Text; }
            set { txtDescription.Text = value; }
        }

        public string MediaURL
        {
            get { return txtUrl.Text; }
            set { txtUrl.Text = value; }
        }
        public AddMedia()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            ((RadWindow)this.Parent).DialogResult = true;
            ((RadWindow)this.Parent).Close();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            ((RadWindow)this.Parent).DialogResult = false;
            ((RadWindow)this.Parent).Close();
        }
    }
}
