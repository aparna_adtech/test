﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace HelpSystem
{
    public partial class WaitIndicator : UserControl
    {
        public static readonly DependencyProperty MyPropertyProperty =
            DependencyProperty.Register("IsActive",   //Property name 
            typeof( bool ),                           //Property type        
            typeof(WaitIndicator),             //Type of the dependency property provider    
            new PropertyMetadata(IsActiveChanged)); //Callback invoked on property value has changes

        private static void IsActiveChanged(object sender, DependencyPropertyChangedEventArgs args)    
        {        
            
        }

        public static DependencyProperty IsActiveProperty = DependencyProperty.Register("IsActive", typeof(bool), typeof(WaitIndicator), new PropertyMetadata(false));

        private Ellipse[] m_ellipseArray = null;
        private Storyboard m_indicatorStoryboard = null;

        private const byte ALPHA_LEVEL_1 = 30;
        private const byte ALPHA_LEVEL_2 = 30;
        private const byte ALPHA_LEVEL_3 = 30;
        private const byte ALPHA_LEVEL_4 = 46;
        private const byte ALPHA_LEVEL_5 = 62;
        private const byte ALPHA_LEVEL_6 = 109;
        private const byte ALPHA_LEVEL_7 = 156;
        private const byte ALPHA_LEVEL_8 = 204;

        private const int INTERVAL_MS = 150;
        private const int ELLIPSE_COUNT = 8;




        public WaitIndicator()
        {
            InitializeComponent();

            m_ellipseArray = new Ellipse[ELLIPSE_COUNT];
            m_ellipseArray[0] = Ellipse1;
            m_ellipseArray[1] = Ellipse2;
            m_ellipseArray[2] = Ellipse3;
            m_ellipseArray[3] = Ellipse4;
            m_ellipseArray[4] = Ellipse5;
            m_ellipseArray[5] = Ellipse6;
            m_ellipseArray[6] = Ellipse7;
            m_ellipseArray[7] = Ellipse8;

            DefineStoryboard();

            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                LayoutRoot.Visibility = Visibility.Collapsed;
        }


        //bool _IsActive;
        public bool IsActive
        {
            get
            {
                return (bool)GetValue(IsActiveProperty);
            }
            set
            {
                this.SetValue(IsActiveProperty, value);
                if (value)
                {
                    Start();
                }
                else
                {
                    Stop();
                }
            }
        }

        private void Start()
        {
            LayoutRoot.Visibility = Visibility.Visible;
            m_indicatorStoryboard.Begin();
        }

        private void Stop()
        {
            LayoutRoot.Visibility = Visibility.Collapsed;
            m_indicatorStoryboard.Stop();
        }

        private void DefineStoryboard()
        {
            byte[] alphaLevelsArray = new byte[16] { ALPHA_LEVEL_1, ALPHA_LEVEL_2, ALPHA_LEVEL_3, ALPHA_LEVEL_4, ALPHA_LEVEL_5, ALPHA_LEVEL_6, ALPHA_LEVEL_7, ALPHA_LEVEL_8, ALPHA_LEVEL_1, ALPHA_LEVEL_2, ALPHA_LEVEL_3, ALPHA_LEVEL_4, ALPHA_LEVEL_5, ALPHA_LEVEL_6, ALPHA_LEVEL_7, ALPHA_LEVEL_8 };

            m_indicatorStoryboard = new Storyboard();

            for (int ellipseIndex = 0; ellipseIndex < ELLIPSE_COUNT; ellipseIndex++)
            {
                Ellipse animateEllipse = m_ellipseArray[ellipseIndex];

                ColorAnimationUsingKeyFrames animation = new ColorAnimationUsingKeyFrames();
                animation.RepeatBehavior = RepeatBehavior.Forever;

                Storyboard.SetTarget(animation, animateEllipse);
                Storyboard.SetTargetProperty(animation, new PropertyPath("(Fill).(SolidBrush.Color)"));

                for (int frameIndex = 0; frameIndex <= 7; frameIndex++)
                {
                    LinearColorKeyFrame keyFrame = new LinearColorKeyFrame();
                    keyFrame.Value = Color.FromArgb(alphaLevelsArray[ellipseIndex + ELLIPSE_COUNT - frameIndex], 0, 0, 0);
                    keyFrame.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(frameIndex * INTERVAL_MS));

                    animation.KeyFrames.Add(keyFrame);
                }

                m_indicatorStoryboard.Children.Add(animation);
            }

            this.Resources.Add("IndicatorStoryboard", m_indicatorStoryboard);
        }

        private void LayoutRoot_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.IsActive = false;
        }
    }
}
