﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Browser;
using HelpSystem.HelpSystemService;
using Telerik.Windows.Controls;
using System.Windows.Threading;
using Telerik.Windows.Controls.DragDrop;
using System.Collections;

namespace HelpSystem
{
    public partial class MainView : UserControl
    {
        MainViewModel _ViewModel;
        
        //TODO should really move these to the ViewModel
        RadTreeViewItem _TopNode = new RadTreeViewItem();
        RadTreeViewItem _ActiveTreeViewItem;
        bool _FoundActiveTreeViewItem;
        string _BregAdmin = string.Empty;
        bool _IsFullMode = false;
        int _DefaultNode = 3; //defaults to "Help Topics"

        private DispatcherTimer doubleClickTimer = new DispatcherTimer();
        private bool doubleClickFlag;
        
        

        public MainView()
        {
            _ViewModel = new MainViewModel();
            _ViewModel.OnGetHelpGroupCompleted += new GetHelpGroupCompleted(_ViewModel_OnGetHelpGroupCompleted);
            _ViewModel.OnGetHelpArticleCompleted += new GetHelpArticleCompleted(_ViewModel_OnGetHelpArticleCompleted);
            _ViewModel.OnSearchForHelpArticlesAndHelpMeidaCompleted += new SearchForHelpArticlesAndHelpMeidaCompleted(_ViewModel_OnSearchForHelpArticlesAndHelpMeidaCompleted);
            _ViewModel.OnInsertHelpGroupCompleted += new InsertHelpGroupCompleted(_ViewModel_OnInsertHelpGroupCompleted);
            _ViewModel.OnInsertHelpArticleCompleted += new InsertHelpArticleCompleted(_ViewModel_OnInsertHelpArticleCompleted);
            _ViewModel.OnInsertHelpMediaCompleted += new InsertHelpMediaCompleted(_ViewModel_OnInsertHelpMediaCompleted);
            _ViewModel.OnDeleteHelpEntityCompleted += new DeleteHelpEntityCompleted(_ViewModel_OnDeleteHelpEntityCompleted);
            _ViewModel.OnUpdateHelpGroupCompleted += new UpdateHelpGroupCompleted(_ViewModel_OnUpdateHelpGroupCompleted);
            _ViewModel.OnUpdateHelpArticleCompleted += new UpdateHelpArticleCompleted(_ViewModel_OnUpdateHelpArticleCompleted);
            _ViewModel.OnUpdateHelpMediaCompleted += new UpdateHelpMediaCompleted(_ViewModel_OnUpdateHelpMediaCompleted);
            _ViewModel.OnUpdateHelpEntityMapCompleted += new UpdateHelpEntityMapCompleted(_ViewModel_OnUpdateHelpEntityMapCompleted);
            _ViewModel.OnInsertHelpEntityMapCompleted += new InsertHelpEntityMapCompleted(_ViewModel_OnInsertHelpEntityMapCompleted);
            this.DataContext = _ViewModel;

            if (App.Current.Resources["BregAdmin"] != null)
            {
                _BregAdmin = App.Current.Resources["BregAdmin"].ToString();
            }

            if (App.Current.Resources["IsFullMode"] != null)
            {
                _IsFullMode = bool.Parse(App.Current.Resources["IsFullMode"].ToString());
            }

            if (_BregAdmin == "True")
            {
                _DefaultNode = int.Parse(App.Current.Resources["AdminNode"].ToString());
            }
            else
            {
                _DefaultNode = int.Parse(App.Current.Resources["UserNode"].ToString());
            }

            StyleManager.ApplicationTheme = new Windows7Theme();

            RadDragAndDropManager.DragStartThreshold = 2;

            InitializeComponent();
           
            this.doubleClickTimer.Interval = TimeSpan.FromMilliseconds(300);
            this.doubleClickTimer.Tick += new EventHandler(doubleClickTimer_Tick);
            this.treeView.AddHandler(RadTreeViewItem.MouseLeftButtonDownEvent, new MouseButtonEventHandler(treeView_MouseLeftButtonDown), true);

            

            RadDragAndDropManager.AddDropInfoHandler(this, OnDropInfo);

            this.Loaded += new System.Windows.RoutedEventHandler(Page_Loaded);
            
        }

        void _ViewModel_OnGetHelpArticleCompleted(HelpArticleResponse response)
        {
            if (response.Exception == null)
            {
                treeView.Items.Clear();
                _TopNode.Items.Clear();
                ProcessArticleTooltip(response.HelpArticle, _TopNode);
                //treeView.Items.Add(_TopNode);
                List<RadTreeViewItem> items = new List<RadTreeViewItem>();
                foreach (RadTreeViewItem tvi in _TopNode.Items)
                {
                    items.Add(tvi);
                }
                _TopNode.Items.Clear();
                foreach (RadTreeViewItem tvi in items)
                {
                    treeView.Items.Add(tvi);
                }
                SwapPanels(true);
            }
            else
            {
                DisplayError(response.Exception);
            }
            WaitIndicator.IsActive = false;
        }

        void _ViewModel_OnInsertHelpEntityMapCompleted(HelpEntityResponse response)
        {
            if (response.Exception == null)
            {
                RadTreeViewItem treeViewItem = (RadTreeViewItem)treeView.SelectedItem;
                WaitIndicator.IsActive = true;
                _ViewModel.GetHelpGroup(_DefaultNode, 1000); //reload the tree nodes from the db as multiple nodes may have changed
                SetActiveTreeViewItem(response.HelpEntity.Id);
            }
            else
            {
                DisplayError(response.Exception);
            }
        }

        void _ViewModel_OnUpdateHelpEntityMapCompleted(HelpEntityResponse response)
        {
            if (response.Exception == null)
            {
                RadTreeViewItem treeViewItem = (RadTreeViewItem)treeView.SelectedItem;
                WaitIndicator.IsActive = true;
                _ViewModel.GetHelpGroup(_DefaultNode,1000); //reload the tree nodes from the db as multiple nodes may have changed
                SetActiveTreeViewItem(response.HelpEntity.Id);
            }
            else
            {
                DisplayError(response.Exception);
            }
        }

        void _ViewModel_OnDeleteHelpEntityCompleted(HelpEntityResponse response)
        {
            if (response.Exception == null)
            {
                RadTreeViewItem treeViewItem = (RadTreeViewItem)treeView.SelectedItem;
                WaitIndicator.IsActive = true;
                _ViewModel.GetHelpGroup(_DefaultNode, 1000); //reload the tree nodes from the db as multiple nodes may have changed
                SetActiveTreeViewItem(((HelpEntity)treeViewItem.PreviousItem.DataContext).Id);
            }
            else
            {
                DisplayError(response.Exception);
            }
        }

        void _ViewModel_OnUpdateHelpMediaCompleted(HelpMediaResponse response)
        {
            if (response.Exception == null)
            {
                WaitIndicator.IsActive = true;
                _ViewModel.GetHelpGroup(_DefaultNode, 1000); //The initial load of the tree    

                GetActiveTreeViewItem(_TopNode, response.HelpMedia.Id);
            }
            else
            {
                DisplayError(response.Exception);
            }
        }

        void _ViewModel_OnUpdateHelpArticleCompleted(HelpArticleResponse response)
        {
            if (response.Exception == null)
            {
                WaitIndicator.IsActive = true;
                _ViewModel.GetHelpGroup(_DefaultNode, 1000); //The initial load of the tree    
                GetActiveTreeViewItem(_TopNode, response.HelpArticle.Id);
            }
            else
            {
                DisplayError(response.Exception);
            }
        }

        void _ViewModel_OnUpdateHelpGroupCompleted(HelpGroupResponse response)
        {
            if (response.Exception == null)
            {
                WaitIndicator.IsActive = true;
                _ViewModel.GetHelpGroup(_DefaultNode, 1000); //The initial load of the tree    
                GetActiveTreeViewItem(_TopNode, response.HelpGroup.Id);
            }
            else
            {
                DisplayError(response.Exception);
            }
        }
        
        void _ViewModel_OnInsertHelpMediaCompleted(HelpMediaResponse response)
        {
            if (response.Exception == null)
            {
                RadTreeViewItem treeViewItem = (RadTreeViewItem)treeView.SelectedItem;

                RadTreeViewItem thisNode = new RadTreeViewItem();
                thisNode.DataContext = response.HelpMedia;
                thisNode.Header = response.HelpMedia.Title;
                thisNode.DefaultImageSrc = "Images/vid16.png";
                treeViewItem.Items.Add(thisNode);

                GetActiveTreeViewItem(_TopNode, response.HelpMedia.Id);
            }
            else
            {
                DisplayError(response.Exception);
            }
        }

        void _ViewModel_OnInsertHelpArticleCompleted(HelpArticleResponse response)
        {
            if (response.Exception == null)
            {
                RadTreeViewItem treeViewItem = (RadTreeViewItem)treeView.SelectedItem;

                RadTreeViewItem thisNode = new RadTreeViewItem();
                thisNode.DataContext = response.HelpArticle;
                thisNode.Header = response.HelpArticle.Title;
                thisNode.DefaultImageSrc = "Images/question16.png";
                treeViewItem.Items.Add(thisNode);

                GetActiveTreeViewItem(_TopNode, response.HelpArticle.Id);
            }
            else
            {
                DisplayError(response.Exception);
            }
        }

        void _ViewModel_OnInsertHelpGroupCompleted(HelpGroupResponse response)
        {
            if (response.Exception == null)
            {
                RadTreeViewItem treeViewItem = (RadTreeViewItem)treeView.SelectedItem;

                RadTreeViewItem thisNode = new RadTreeViewItem();
                thisNode.DataContext = response.HelpGroup;
                thisNode.Header = response.HelpGroup.Title;
                thisNode.DefaultImageSrc = "Images/folder16.png";
                treeViewItem.Items.Add(thisNode);

                GetActiveTreeViewItem(_TopNode, response.HelpGroup.Id);
            }
            else
            {
                DisplayError(response.Exception);
            }
        }

        void _ViewModel_OnSearchForHelpArticlesAndHelpMeidaCompleted(HelpGroupResponse response)
        {
            if (response.Exception == null)
            {
                treeView.Items.Clear();
                _TopNode.Items.Clear();
                ProcessGroup(response.HelpGroup, _TopNode);
                //treeView.Items.Add(_TopNode);
                List<RadTreeViewItem> items = new List<RadTreeViewItem>();
                foreach (RadTreeViewItem tvi in _TopNode.Items)
                {
                    items.Add(tvi);
                }
                _TopNode.Items.Clear();
                foreach (RadTreeViewItem tvi in items)
                {
                    treeView.Items.Add(tvi);
                }
                SwapPanels(true);
            }
            else
            {
                DisplayError(response.Exception);
            }
            WaitIndicator.IsActive = false;
        }

        void Page_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            //TODO pass in ROLE not UserId in the ASP.net page
            if (_BregAdmin == "True" && _IsFullMode==true)
            {
                adminToolbar.Visibility = Visibility.Visible;
                _ViewModel.IsDragDropEnabled = true;
            }
            
            HtmlPage.RegisterScriptableObject("API", this);

            if (App.Current.Resources["ArticleId"] != null)
            {
                try
                {
                    int articleId = int.Parse(App.Current.Resources["ArticleId"].ToString());
                    WaitIndicator.IsActive = true;
                    _ViewModel.GetHelpArticle(articleId);
                }
                catch { }
            }
            else
            {
                WaitIndicator.IsActive = true;
                _ViewModel.GetHelpGroup(_DefaultNode, 1000); //The initial load of the tree    
            }
        }

        [ScriptableMember]
        public void LoadHelpContext(int id, int depth)
        {
            //This crops the tree and only load the item and it's children
            WaitIndicator.IsActive = true;
            _ViewModel.GetHelpGroup(id, depth);            
        }

        [ScriptableMember]
        public void SelectHelpContext(int id)
        {
            //This locates the item in the tree and selects it.
            SetActiveTreeViewItem(id);
        }

        [ScriptableMember]
        public void GetHelpArticle(int id)
        {
            //Get the article by id from the id of the selected tooltip and show it's related content
            WaitIndicator.IsActive = true;
            _ViewModel.GetHelpArticle(id);
        }

        [ScriptableMember]
        public void SearchForHelpArticlesAndHelpMeida(string searchText)
        {
            txtSearch.Text = searchText;
            _ViewModel.SearchForHelpArticlesAndHelpMeida(txtSearch.Text);
        }

        


        void _ViewModel_OnGetHelpGroupCompleted(HelpGroupResponse response)
        {
            if (response.Exception == null)
            {
                treeView.Items.Clear();
                _TopNode.Items.Clear();
                ProcessGroup(response.HelpGroup, _TopNode);
                //treeView.Items.Add(_TopNode);
                List<RadTreeViewItem> items = new List<RadTreeViewItem>();
                foreach (RadTreeViewItem tvi in _TopNode.Items)
                {
                    items.Add(tvi);
                }
                _TopNode.Items.Clear();
                foreach (RadTreeViewItem tvi in items)
                {
                    treeView.Items.Add(tvi);
                }
                SwapPanels(true);
            }
            else
            {
                DisplayError(response.Exception);
            }

            WaitIndicator.IsActive = false;
        }

        private void ProcessArticleTooltip(HelpArticle thisArticle, RadTreeViewItem thisNode)
        {
            //the top node in the tree must be a group so create a dummy group here, add the article to it then
            //process as a normal group.

            HelpGroup searchResultsGroup = new HelpGroup();
            searchResultsGroup.Id = -1; //for correct placement in the treeview
            searchResultsGroup.HelpEntityType = HelpEntityType.NotSet;
            searchResultsGroup.Title = "More Info";

            thisArticle.ParentId = -1; //for correct placement in the treeview
            searchResultsGroup.HelpGroups = new System.Collections.ObjectModel.ObservableCollection<HelpGroup>();
            searchResultsGroup.HelpMedias = new System.Collections.ObjectModel.ObservableCollection<HelpMedia>();
            searchResultsGroup.HelpArticles = new System.Collections.ObjectModel.ObservableCollection<HelpArticle>();
            searchResultsGroup.HelpArticles.Add(thisArticle);

            ProcessGroup(searchResultsGroup, thisNode);

        }
        private void ProcessGroup(HelpGroup thisGroup, RadTreeViewItem thisNode)
        {
            thisNode.DataContext = thisGroup;
            thisNode.Header = thisGroup.Title;
            thisNode.DefaultImageSrc = "Images/folder16.png";

            foreach (HelpGroup group in thisGroup.HelpGroups)
            {
                RadTreeViewItem node = new RadTreeViewItem();
                ProcessGroup(group, node);
                thisNode.Items.Add(node);
            }

            foreach (HelpArticle article in thisGroup.HelpArticles)
            {
                RadTreeViewItem node = new RadTreeViewItem();
                ProcessArticle(article, node);
                thisNode.Items.Add(node);
            }

            foreach (HelpMedia media in thisGroup.HelpMedias)
            {
                RadTreeViewItem node = new RadTreeViewItem();
                ProcessMedia(media, node);
                thisNode.Items.Add(node);
            }

            if (thisGroup.HelpGroups.Count > 0 || thisGroup.HelpArticles.Count > 0 || thisGroup.HelpMedias.Count > 0)
            {
                thisNode.IsExpanded = true;
            }
        }

        private void ProcessArticle(HelpArticle thisArticle, RadTreeViewItem thisNode)
        {
            thisNode.DataContext = thisArticle;
            thisNode.Header = thisArticle.Title;
            thisNode.DefaultImageSrc = "Images/question16.png";

            foreach (HelpArticle article in thisArticle.RelatedArticles)
            {
                RadTreeViewItem node = new RadTreeViewItem();
                ProcessArticle(article, node);
                thisNode.Items.Add(node);
            }

            foreach (HelpMedia media in thisArticle.RelatedMedia)
            {
                RadTreeViewItem node = new RadTreeViewItem();
                ProcessMedia(media, node);
                thisNode.Items.Add(node);
            }

            if (thisArticle.RelatedArticles.Count > 0 || thisArticle.RelatedMedia.Count > 0)
            {
                thisNode.IsExpanded = true;
            }
        }

        private void ProcessMedia(HelpMedia thisMedia, RadTreeViewItem thisNode)
        {
            thisNode.DataContext = thisMedia;
            thisNode.Header = thisMedia.Title;
            thisNode.DefaultImageSrc = "Images/vid16.png";
            
            foreach (HelpMedia media in thisMedia.RelatedMedia)
            {
                RadTreeViewItem node = new RadTreeViewItem();
                ProcessMedia(media, node);
                thisNode.Items.Add(node);
            }

            foreach (HelpArticle article in thisMedia.RelatedArticles)
            {
                RadTreeViewItem node = new RadTreeViewItem();
                ProcessArticle(article, node);
                thisNode.Items.Add(node);
            }

            if (thisMedia.RelatedArticles.Count > 0 || thisMedia.RelatedMedia.Count > 0)
            {
                thisNode.IsExpanded = true;
            }
        }

        void DisplayError(Exception ex)
        {
            MessageBox.Show(ex.Message);
        }

        private void toolbarButton_Click(object sender, RoutedEventArgs e)
        {
            switch ((string)((Button)sender).Tag)
	        {
                case "Home":
                    WaitIndicator.IsActive = true;
                    _ViewModel.GetHelpGroup(_DefaultNode, 1000); //The initial load of the tree   
                    break;
                case "Search":
                    WaitIndicator.IsActive = true;
                    _ViewModel.SearchForHelpArticlesAndHelpMeida(txtSearch.Text);
                    break;
                case "Back":
                    SwapPanels(true);
                    break;
                case "Forward":
                    
                    break;
		        default:
                    break;
	        }
        }

        private void treeView_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            RadTreeViewItem selectedItem = treeView.SelectedItem as RadTreeViewItem;
            if (selectedItem != null)
            {
                if (this.doubleClickFlag)
                {
                    DisplayItem(selectedItem);
                }
                else
                {
                    this.doubleClickTimer.Start();
                    this.doubleClickFlag = true;
                }
            }
        }

        void doubleClickTimer_Tick(object sender, EventArgs e)
        {
            this.doubleClickTimer.Stop();
            this.doubleClickFlag = false;
        }

        private bool SetActiveTreeViewItem(int id)
        {
            _FoundActiveTreeViewItem = false;
            GetActiveTreeViewItem(_TopNode, id);
            if (!_FoundActiveTreeViewItem)
            {
                //MessageBox.Show("Help Topic not found in tree Id=" + id.ToString());
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool GetActiveTreeViewItem(RadTreeViewItem treeViewItem, int Id)
        {
            HelpEntity entity = (HelpEntity)treeViewItem.DataContext;

            if (entity == null)
                return false;

            if (entity.Id == Id)
            {
                _ActiveTreeViewItem = treeViewItem;
                _ActiveTreeViewItem.IsExpanded = true;
                _ActiveTreeViewItem.BringIntoView();
                _ActiveTreeViewItem.IsSelected = true;

                DisplayItem(_ActiveTreeViewItem);

                _FoundActiveTreeViewItem = true;
                return true;
            }
            else
            {
                foreach (RadTreeViewItem item in treeViewItem.Items)
                {
                    GetActiveTreeViewItem(item, Id);
                }
                return false;
            }
        }

        void SwapPanels(bool treeViewState)
        {
            if (_IsFullMode)
            {
                splitter.Visibility = Visibility.Visible;
                detailGrid.Visibility = Visibility.Visible;
                btnBack.Visibility = Visibility.Collapsed;
                treeView.Visibility = Visibility.Visible;
                htmlHost.Visibility = Visibility.Visible;
            }
            else
            {
                htmlHost.HtmlSource = "";
                htmlHost.Visibility = Visibility.Collapsed; //we show the content in an HTML div not in the silverlight control
                if (treeViewState)
                {
                    colTree.Width = new GridLength(1, GridUnitType.Star);
                    colData.Width = new GridLength(1, GridUnitType.Auto);

                    splitter.Visibility = Visibility.Collapsed;
                    detailGrid.Visibility = Visibility.Collapsed;
                    btnBack.IsEnabled = false;

                    treeView.Visibility = Visibility.Visible;
                    treeView.HorizontalAlignment = HorizontalAlignment.Stretch;
                    HtmlPage.Window.Invoke("HideHelpArticle");
                    mediaPlayer.Pause(); //if viewing the tree don't play the video
                }
                else
                {

                    colTree.Width = new GridLength(1, GridUnitType.Auto);
                    colData.Width = new GridLength(1, GridUnitType.Star);

                    treeView.Visibility = Visibility.Collapsed;
                    splitter.Visibility = Visibility.Collapsed;
                    detailGrid.Visibility = Visibility.Visible;
                    detailGrid.HorizontalAlignment = HorizontalAlignment.Stretch;

                    btnBack.IsEnabled = true;
                    
                }
            }
        }

        void DisplayItem(RadTreeViewItem treeViewItem)
        {
            detailGrid.DataContext = treeViewItem.DataContext;

            if (detailGrid.DataContext.GetType() == typeof(HelpArticle) && !_IsFullMode)
            {
                HelpArticle article = (HelpArticle)detailGrid.DataContext;

                HtmlPage.Window.Invoke("SetHelpArticle", article.Description);
                HtmlPage.Window.Invoke("ShowHelpArticle");
            }

            if (detailGrid.DataContext.GetType() == typeof(HelpMedia) || detailGrid.DataContext.GetType() == typeof(HelpArticle))
            {
                SwapPanels(false);
            }
            else
            {
                SwapPanels(true);
            }
            
            if (detailGrid.DataContext.GetType() == typeof(HelpMedia))
            {
                mediaPlayer.Visibility = Visibility.Visible;
                htmlHost.Visibility = Visibility.Collapsed;
            }
            else
            {
                mediaPlayer.Pause();
                mediaPlayer.Visibility = Visibility.Collapsed;
                htmlHost.Visibility = Visibility.Visible;
            }
        }

        private void player_FullScreenChanged(object sender, EventArgs e)
        {
            Application.Current.Host.Content.IsFullScreen = !Application.Current.Host.Content.IsFullScreen;
        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                _ViewModel.SearchForHelpArticlesAndHelpMeida(txtSearch.Text);
        }

        private void adminToolbarButton_Click(object sender, RoutedEventArgs e)
        {
            if (treeView.SelectedItem == null)
            {
                MessageBox.Show("Select a tree node 1st.");
                return;
            }

            HelpEntity selectedEntity = (HelpEntity)((RadTreeViewItem)treeView.SelectedItem).DataContext;

            switch ((string)((Button)sender).Tag)
            {
                case "DeleteEntity":
                    
                    if (((RadTreeViewItem)treeView.SelectedItem).HasItems)
                    {
                        MessageBox.Show("Cannot delete nodes with children");
                    }
                    else if (selectedEntity.ParentId == _DefaultNode)
                    {
                        //TODO need some more conditions here
                         MessageBox.Show("Top node cannot be deleted.");     
                    }
                    else
                    {
                        if (MessageBox.Show("Are you sure you want to delete this tree node?\r\n" + selectedEntity.Title, "Delete", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                        {
                            //if a related article or related media we just delete the mapping in the tree
                            //if an actual entity we delete the tree reference and delete the entity only if it is not used elsewhere in the tree
                            bool removeEntity = (selectedEntity.HelpEntityType == HelpEntityType.RelatedArticle || selectedEntity.HelpEntityType == HelpEntityType.RelatedMedia ? false : true);
                            _ViewModel.DeleteHelpEntity(selectedEntity as HelpEntity, removeEntity);
                        }
                    }
                    break;
                case "AddGroup":

                    if (selectedEntity.HelpEntityType == HelpEntityType.Group)
                    {
                        RadWindow window = new RadWindow();
                        window.ResizeMode = ResizeMode.NoResize;
                        window.Width = 400;
                        window.CanClose = false;
                        window.Height = 200;
                        window.Content = new AddGroup();
                        window.Header = "Add Group to " + selectedEntity.Title;
                        window.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterOwner;

                        window.Closed += (object s1, WindowClosedEventArgs args) =>
                        {
                            if (window.DialogResult != null && window.DialogResult == true)
                            {
                                HelpGroup entity = new HelpGroup();
                                entity.ParentId = selectedEntity.Id;
                                entity.HelpEntityType = HelpEntityType.Group;

                                entity.Title = ((AddGroup)((RadWindow)s1).Content).GroupTitle;

                                _ViewModel.InsertHelpGroup(entity);
                            }
                        };

                        window.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Cannot insert a group here");
                    }
                    break;
                case "AddArticle":
                    if (selectedEntity.HelpEntityType == HelpEntityType.Group)
                    {
                        RadWindow window = new RadWindow();
                        window.ResizeMode = ResizeMode.NoResize;
                        window.Width = 425;
                        window.CanClose = false;
                        window.Height = 200;
                        window.Content = new AddArticle();
                        window.Header = "Add Article to " + selectedEntity.Title;
                        window.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterOwner;

                        window.Closed += (object s1, WindowClosedEventArgs args) =>
                        {
                            if (window.DialogResult != null && window.DialogResult == true)
                            {
                                HelpArticle entity = new HelpArticle();
                                entity.ParentId = selectedEntity.Id;
                                entity.HelpEntityType = HelpEntityType.Article;
                                entity.Title = ((AddArticle)((RadWindow)s1).Content).ArticleTitle; ;
                                entity.Description = ((AddArticle)((RadWindow)s1).Content).ArticleDescription; ;
                                
                                _ViewModel.InsertHelpArticle(entity);
                            }
                        };

                        window.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Cannot insert article here");
                    }

                    break;
                case "AddMediaItem":

                    if (selectedEntity.HelpEntityType == HelpEntityType.Group)
                    {
                        RadWindow window = new RadWindow();
                        window.ResizeMode = ResizeMode.NoResize;
                        window.Width = 425;
                        window.CanClose = false;
                        window.Height = 200;
                        window.Content = new AddMedia();
                        window.Header = "Add Media Item to " + selectedEntity.Title;
                        window.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterOwner;

                        window.Closed += (object s1, WindowClosedEventArgs args) =>
                        {
                            if (window.DialogResult != null && window.DialogResult == true)
                            {
                                HelpMedia entity = new HelpMedia();
                                entity.ParentId = selectedEntity.Id;
                                entity.HelpEntityType = HelpEntityType.Media;
                                entity.Title = ((AddMedia)((RadWindow)s1).Content).MediaTitle; ;
                                entity.Description = ((AddMedia)((RadWindow)s1).Content).MediaDescription; ;
                                entity.Url = ((AddMedia)((RadWindow)s1).Content).MediaURL; ;
                                _ViewModel.InsertHelpMedia(entity);

                            }
                        };

                        window.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Cannot insert media item here");
                    }

                    break;
                
                case "UpdateEntity":

                    if (selectedEntity.HelpEntityType == HelpEntityType.Group)
                    {
                        HelpGroup entity = (HelpGroup)((RadTreeViewItem)treeView.SelectedItem).DataContext;

                        RadWindow window = new RadWindow();
                        window.ResizeMode = ResizeMode.NoResize;
                        window.Width = 425;
                        window.CanClose = false;
                        window.Height = 200;

                        AddGroup frm = new AddGroup();
                        frm.GroupTitle = entity.Title;
                        
                        window.Content = frm;
                        window.Header = "Update: " + selectedEntity.Title;
                        window.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterOwner;

                        window.Closed += (object s1, WindowClosedEventArgs args) =>
                        {
                            if (window.DialogResult != null && window.DialogResult == true)
                            {
                                entity.Title = ((AddGroup)((RadWindow)s1).Content).GroupTitle; ;
                                
                                _ViewModel.UpdateHelpGroup(entity);

                            }
                        };

                        window.ShowDialog();
                    }
                    else if (selectedEntity.HelpEntityType == HelpEntityType.Article || selectedEntity.HelpEntityType == HelpEntityType.RelatedArticle)
                    {
                        HelpArticle entity = (HelpArticle)((RadTreeViewItem)treeView.SelectedItem).DataContext;

                        RadWindow window = new RadWindow();
                        window.ResizeMode = ResizeMode.NoResize;
                        window.Width = 425;
                        window.CanClose = false;
                        window.Height = 200;

                        AddArticle frm = new AddArticle();
                        frm.ArticleTitle = entity.Title;
                        frm.ArticleDescription = entity.Description;

                        window.Content = frm;
                        window.Header = "Update Article: " + selectedEntity.Title;
                        window.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterOwner;

                        window.Closed += (object s1, WindowClosedEventArgs args) =>
                        {
                            if (window.DialogResult != null && window.DialogResult == true)
                            {
                                entity.Title = ((AddArticle)((RadWindow)s1).Content).ArticleTitle; ;
                                entity.Description = ((AddArticle)((RadWindow)s1).Content).ArticleDescription; ;

                                _ViewModel.UpdateHelpArticle(entity);

                            }
                        };

                        window.ShowDialog();
                    }
                    else if (selectedEntity.HelpEntityType == HelpEntityType.Media || selectedEntity.HelpEntityType == HelpEntityType.RelatedMedia)
                    {
                        HelpMedia entity = (HelpMedia)((RadTreeViewItem)treeView.SelectedItem).DataContext;

                        RadWindow window = new RadWindow();
                        window.ResizeMode = ResizeMode.NoResize;
                        window.Width = 425;
                        window.CanClose = false;
                        window.Height = 200;
                        
                        AddMedia frm = new AddMedia();
                        frm.MediaTitle = entity.Title;
                        frm.MediaDescription = entity.Description;
                        frm.MediaURL = entity.Url;

                        window.Content = frm;
                        window.Header = "Update Media Item: " + selectedEntity.Title;
                        window.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterOwner;

                        window.Closed += (object s1, WindowClosedEventArgs args) =>
                        {
                            if (window.DialogResult != null && window.DialogResult == true)
                            {
                                entity.Title = ((AddMedia)((RadWindow)s1).Content).MediaTitle; ;
                                entity.Description = ((AddMedia)((RadWindow)s1).Content).MediaDescription; ;
                                entity.Url = ((AddMedia)((RadWindow)s1).Content).MediaURL; ;
                                _ViewModel.UpdateHelpMedia(entity);

                            }
                        };

                        window.ShowDialog();
                    }

                    break;

                case "SortUp":
                    HelpEntity su = new HelpEntity();
                    su.Id = selectedEntity.Id;
                    su.ParentId = selectedEntity.ParentId;
                    su.SortOrder = selectedEntity.SortOrder + 1;
                    _ViewModel.UpdateHelpEntityMap(su);

                    break;
                case "SortDown":
                    HelpEntity sd = new HelpEntity();
                    sd.Id = selectedEntity.Id;
                    sd.ParentId = selectedEntity.ParentId;
                    sd.SortOrder = selectedEntity.SortOrder - 1;
                    _ViewModel.UpdateHelpEntityMap(sd);

                    break;
                default:
                    break;
            }
        }

        private void OnDropInfo(object sender, DragDropEventArgs e)
        {
            //Here we can change the drag cue to let the user know what
            //action is possible. We could change the background of the
            //arrow if it is in use.
            if (e.Options.Status == DragStatus.DropPossible)
            {
                //Drop is possible, change the content of the DragCue or 
                //notify the user that a drag is possible.
            }

            if (e.Options.Status == DragStatus.DropImpossible)
            {
                //Drop is not possible. Here generally we will revert the changes
                //done in DropPossible.
            }

            if (e.Options.Status == DragStatus.DropComplete)
            {

                //Here we can generally check the payload and add handle the case
                //when it is an IEnumerable (when multiple items are dragged).
                //The paylaod could also be a custom class that contains more info
                //for the DragDrop. Here we know that it always is an item that we can 
                //display. We just add the item here, we do not interact with the
                //drag source. Removing the item (if at all removed should happen OnDragIndo
                //so that there will be separation of handling the DragDrop, which is more flexible.

                HelpEntity sourceEntity = (HelpEntity)((RadTreeViewItem)e.Options.Source).DataContext;
                HelpEntity destEntity = (HelpEntity)((RadTreeViewItem)e.Options.Destination).DataContext;
                
                //DragDrop delay helper
                var delayedDragDrop = new DelayedDragDrop();
                delayedDragDrop.DraggedItem = (e.Options.Payload as IList)[0];

                delayedDragDrop.DropActionUpdateEntity = () =>
                {
                    RadTreeViewItem targetTreeViewItem = e.Options.Destination as RadTreeViewItem;
                    if (targetTreeViewItem != null)
                    {
                        //covariance in .net 40 will allow us to pass the sourceEntity and not create a base class version as we are doing here
                        HelpEntity helpEntity = new HelpEntity();
                        helpEntity.Id = sourceEntity.Id;
                        helpEntity.ParentId = destEntity.Id;
                        helpEntity.SortOrder = sourceEntity.SortOrder;

                        _ViewModel.UpdateHelpEntityMap(helpEntity);

                        this.RemoveItem(e.Options.Source as RadTreeViewItem);
                        targetTreeViewItem.IsExpanded = true;
                        targetTreeViewItem.Items.Add(delayedDragDrop.DraggedItem as RadTreeViewItem);
                    }
                };

                delayedDragDrop.DropActionInsertRelatedArticle = () =>
                {
                    RadTreeViewItem targetTreeViewItem = e.Options.Destination as RadTreeViewItem;
                    if (targetTreeViewItem != null)
                    {
                        //covariance in .net 40 will allow us to pass the sourceEntity and not create a base class version as we are doing here
                        HelpEntity helpEntity = new HelpEntity();
                        helpEntity.EntityId = sourceEntity.EntityId;
                        helpEntity.HelpEntityType = HelpEntityType.RelatedArticle;
                        helpEntity.ParentId = destEntity.Id;
                        helpEntity.SortOrder = sourceEntity.SortOrder;

                        _ViewModel.InsertHelpEntityMap(helpEntity);

                        //Note: the async result will refresh the tree
                    }
                };

                delayedDragDrop.DropActionInsertRelatedMediaItem = () =>
                {
                    RadTreeViewItem targetTreeViewItem = e.Options.Destination as RadTreeViewItem;
                    if (targetTreeViewItem != null)
                    {
                        //covariance in .net 40 will allow us to pass the sourceEntity and not create a base class version as we are doing here
                        HelpEntity helpEntity = new HelpEntity();
                        helpEntity.EntityId = sourceEntity.EntityId;
                        helpEntity.HelpEntityType = HelpEntityType.RelatedMedia;
                        helpEntity.ParentId = destEntity.Id;
                        helpEntity.SortOrder = sourceEntity.SortOrder;

                        _ViewModel.InsertHelpEntityMap(helpEntity);

                        //Note: the async result will refresh the tree
                    }
                };

                //is is possible to do what is being requested.
                //take correct action by source/dest combination.
                if (sourceEntity.HelpEntityType == HelpEntityType.Group)
                {
                    if (destEntity.HelpEntityType != HelpEntityType.Group)
                    {
                        MessageBox.Show("You can only drop a Group on a Group.");
                    }
                    else
                    {
                        RadWindow.Confirm("Do you want to move the Group from: \"" + sourceEntity.Title + "\" to: \"" + destEntity.Title + "\"?", (window, closedArgs) =>
                        {
                            if (closedArgs.DialogResult == true)
                            {
                                delayedDragDrop.DropActionUpdateEntity();
                            }
                        });
                    }
                }
                else if (sourceEntity.HelpEntityType == HelpEntityType.Article)
                {
                    if (destEntity.HelpEntityType == HelpEntityType.Article  || destEntity.HelpEntityType == HelpEntityType.Media)
                    {
                        RadWindow.Confirm("Do you want to create a related article from: \"" + sourceEntity.Title + "\" on: \"" + destEntity.Title + "\"?", (window, closedArgs) =>
                        {
                            if (closedArgs.DialogResult == true)
                            {
                                delayedDragDrop.DropActionInsertRelatedArticle();
                            }
                        });
                    }
                    else if (destEntity.HelpEntityType == HelpEntityType.Group)
                    {
                        RadWindow.Confirm("Do you want to move the Article from: \"" + sourceEntity.Title + "\" to: \"" + destEntity.Title + "\"?", (window, closedArgs) =>
                        {
                            if (closedArgs.DialogResult == true)
                            {
                                delayedDragDrop.DropActionUpdateEntity();
                            }
                            else
                            {
                                //TODO prompt for clone to this group
                            }
                        });
                    }
                    else
                    {
                        MessageBox.Show("Unsupported drag and drop action.");
                    }
                }
                else if (sourceEntity.HelpEntityType == HelpEntityType.Media)
                {
                    if (destEntity.HelpEntityType == HelpEntityType.Article || destEntity.HelpEntityType == HelpEntityType.Media)
                    {
                        RadWindow.Confirm("Do you want to create a related media item from: \"" + sourceEntity.Title + "\" on: \"" + destEntity.Title + "\"?", (window, closedArgs) =>
                        {
                            if (closedArgs.DialogResult == true)
                            {
                                delayedDragDrop.DropActionInsertRelatedMediaItem();
                            }
                        });
                    }
                    else if (destEntity.HelpEntityType == HelpEntityType.Group)
                    {
                        RadWindow.Confirm("Do you want to move the Media Item from: \"" + sourceEntity.Title + "\" to: \"" + destEntity.Title + "\"?", (window, closedArgs) =>
                        {
                            if (closedArgs.DialogResult == true)
                            {
                                delayedDragDrop.DropActionUpdateEntity();
                            }
                            else
                            {
                                //TODO prompt for clone to this group
                            }
                        });
                    }
                    else
                    {
                        MessageBox.Show("Unsupported drag and drop action.");
                    }
                }
                else
                {
                    MessageBox.Show("Unsupported Drag and Drop function.");
                }
            }
        }

        private void RemoveItem(RadTreeViewItem item)
        {
            RadTreeViewItem itemsSource;
            if (item.Level < 1)
            {
                itemsSource = this.treeView.Items[0] as RadTreeViewItem;
            }
            else
            {
                itemsSource = item.ParentItem as RadTreeViewItem;
            }

            if (itemsSource != null)
            {
                itemsSource.Items.Remove(item);
            }
        }

        private void treeView_PreviewDragEnded(object sender, RadTreeViewDragEndedEventArgs e)
        {
            // Prevent the treeview from handling the drop as it would
            // normally do, so that we can show the dialog
            e.Handled = true;
        }

        private void AboutButton_Click(object sender, RoutedEventArgs e_args)
        {
            try
            {
                var baseuri = MainViewModel.GetServiceURI(string.Empty);
                var logouri = baseuri + "/Images/VisionLogo.gif";
                {
                    var c = new WebClient();

                    c.DownloadStringCompleted +=
                        (s, e) =>
                        {
                            var html = string.Format(@"<div style='margin: 5px;'><img src='{0}' title='Breg Vision Logo' /><br />Version {1}<br /><br />Copyright &copy; {2} Breg, Inc. All rights reserved. Breg is an Orthofix company.</div>", logouri, (e.Error == null) ? e.Result : "0.0.0", DateTime.Now.Year);
#if DEBUG
                            html += @"<div style='margin: 20px 5px 5px 5px; color: Red; font-weight: bold;'>Debugging is enabled.</div>";
#endif
                            HtmlPage.Window.Invoke("SetHelpArticle", html);
                            HtmlPage.Window.Invoke("ShowHelpArticle");
                            SwapPanels(false);
                            detailGrid.Visibility = System.Windows.Visibility.Collapsed;
                        };
                    c.DownloadStringAsync(new Uri(baseuri + "/Version.ashx"));
                }
            }
            catch { }
        }

        void c_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            throw new NotImplementedException();
        }
        
    }
    
}
