﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BregWcfService.EMRIntegrationReference {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="schedule_item", Namespace="http://schemas.datacontract.org/2004/07/API.Web")]
    [System.SerializableAttribute()]
    public partial class schedule_item : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime creation_dateField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string diagnosis_icd9Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool is_activeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool is_checked_inField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<System.DateTime> last_modify_dateField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string messageField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<System.DateTime> patient_dobField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string patient_emailField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string patient_first_nameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string patient_idField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string patient_last_nameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string patient_nameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string physician_nameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int practice_idField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int practice_location_idField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string remote_schedule_item_idField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Guid schedule_item_idField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime scheduled_visit_datetimeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string scheduled_visit_datetime_timezoneidField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime creation_date {
            get {
                return this.creation_dateField;
            }
            set {
                if ((this.creation_dateField.Equals(value) != true)) {
                    this.creation_dateField = value;
                    this.RaisePropertyChanged("creation_date");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string diagnosis_icd9 {
            get {
                return this.diagnosis_icd9Field;
            }
            set {
                if ((object.ReferenceEquals(this.diagnosis_icd9Field, value) != true)) {
                    this.diagnosis_icd9Field = value;
                    this.RaisePropertyChanged("diagnosis_icd9");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool is_active {
            get {
                return this.is_activeField;
            }
            set {
                if ((this.is_activeField.Equals(value) != true)) {
                    this.is_activeField = value;
                    this.RaisePropertyChanged("is_active");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool is_checked_in {
            get {
                return this.is_checked_inField;
            }
            set {
                if ((this.is_checked_inField.Equals(value) != true)) {
                    this.is_checked_inField = value;
                    this.RaisePropertyChanged("is_checked_in");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<System.DateTime> last_modify_date {
            get {
                return this.last_modify_dateField;
            }
            set {
                if ((this.last_modify_dateField.Equals(value) != true)) {
                    this.last_modify_dateField = value;
                    this.RaisePropertyChanged("last_modify_date");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string message {
            get {
                return this.messageField;
            }
            set {
                if ((object.ReferenceEquals(this.messageField, value) != true)) {
                    this.messageField = value;
                    this.RaisePropertyChanged("message");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<System.DateTime> patient_dob {
            get {
                return this.patient_dobField;
            }
            set {
                if ((this.patient_dobField.Equals(value) != true)) {
                    this.patient_dobField = value;
                    this.RaisePropertyChanged("patient_dob");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string patient_email {
            get {
                return this.patient_emailField;
            }
            set {
                if ((object.ReferenceEquals(this.patient_emailField, value) != true)) {
                    this.patient_emailField = value;
                    this.RaisePropertyChanged("patient_email");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string patient_first_name {
            get {
                return this.patient_first_nameField;
            }
            set {
                if ((object.ReferenceEquals(this.patient_first_nameField, value) != true)) {
                    this.patient_first_nameField = value;
                    this.RaisePropertyChanged("patient_first_name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string patient_id {
            get {
                return this.patient_idField;
            }
            set {
                if ((object.ReferenceEquals(this.patient_idField, value) != true)) {
                    this.patient_idField = value;
                    this.RaisePropertyChanged("patient_id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string patient_last_name {
            get {
                return this.patient_last_nameField;
            }
            set {
                if ((object.ReferenceEquals(this.patient_last_nameField, value) != true)) {
                    this.patient_last_nameField = value;
                    this.RaisePropertyChanged("patient_last_name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string patient_name {
            get {
                return this.patient_nameField;
            }
            set {
                if ((object.ReferenceEquals(this.patient_nameField, value) != true)) {
                    this.patient_nameField = value;
                    this.RaisePropertyChanged("patient_name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string physician_name {
            get {
                return this.physician_nameField;
            }
            set {
                if ((object.ReferenceEquals(this.physician_nameField, value) != true)) {
                    this.physician_nameField = value;
                    this.RaisePropertyChanged("physician_name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int practice_id {
            get {
                return this.practice_idField;
            }
            set {
                if ((this.practice_idField.Equals(value) != true)) {
                    this.practice_idField = value;
                    this.RaisePropertyChanged("practice_id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int practice_location_id {
            get {
                return this.practice_location_idField;
            }
            set {
                if ((this.practice_location_idField.Equals(value) != true)) {
                    this.practice_location_idField = value;
                    this.RaisePropertyChanged("practice_location_id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string remote_schedule_item_id {
            get {
                return this.remote_schedule_item_idField;
            }
            set {
                if ((object.ReferenceEquals(this.remote_schedule_item_idField, value) != true)) {
                    this.remote_schedule_item_idField = value;
                    this.RaisePropertyChanged("remote_schedule_item_id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Guid schedule_item_id {
            get {
                return this.schedule_item_idField;
            }
            set {
                if ((this.schedule_item_idField.Equals(value) != true)) {
                    this.schedule_item_idField = value;
                    this.RaisePropertyChanged("schedule_item_id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime scheduled_visit_datetime {
            get {
                return this.scheduled_visit_datetimeField;
            }
            set {
                if ((this.scheduled_visit_datetimeField.Equals(value) != true)) {
                    this.scheduled_visit_datetimeField = value;
                    this.RaisePropertyChanged("scheduled_visit_datetime");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string scheduled_visit_datetime_timezoneid {
            get {
                return this.scheduled_visit_datetime_timezoneidField;
            }
            set {
                if ((object.ReferenceEquals(this.scheduled_visit_datetime_timezoneidField, value) != true)) {
                    this.scheduled_visit_datetime_timezoneidField = value;
                    this.RaisePropertyChanged("scheduled_visit_datetime_timezoneid");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="OutboundQueueItem", Namespace="http://schemas.datacontract.org/2004/07/API.Web.Models")]
    [System.SerializableAttribute()]
    public partial class OutboundQueueItem : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime CreationDateField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string FilenameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Guid IDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int PracticeIDField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime CreationDate {
            get {
                return this.CreationDateField;
            }
            set {
                if ((this.CreationDateField.Equals(value) != true)) {
                    this.CreationDateField = value;
                    this.RaisePropertyChanged("CreationDate");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Filename {
            get {
                return this.FilenameField;
            }
            set {
                if ((object.ReferenceEquals(this.FilenameField, value) != true)) {
                    this.FilenameField = value;
                    this.RaisePropertyChanged("Filename");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Guid ID {
            get {
                return this.IDField;
            }
            set {
                if ((this.IDField.Equals(value) != true)) {
                    this.IDField = value;
                    this.RaisePropertyChanged("ID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int PracticeID {
            get {
                return this.PracticeIDField;
            }
            set {
                if ((this.PracticeIDField.Equals(value) != true)) {
                    this.PracticeIDField = value;
                    this.RaisePropertyChanged("PracticeID");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="OutboundHL7DispenseItem", Namespace="http://schemas.datacontract.org/2004/07/API.Web.Models")]
    [System.SerializableAttribute()]
    public partial class OutboundHL7DispenseItem : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string GenderField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string SizeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string abn_reasonField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime dispense_dateField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private decimal dme_depositField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string hcpcs_codeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool is_abnField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool is_medicareField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string item_codeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string item_nameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string modifier1Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string modifier2Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string modifier3Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string modifier4Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<System.DateTime> patient_signature_dateField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<System.DateTime> physician_signature_dateField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int quantityField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string supplier_nameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private decimal unit_priceField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Gender {
            get {
                return this.GenderField;
            }
            set {
                if ((object.ReferenceEquals(this.GenderField, value) != true)) {
                    this.GenderField = value;
                    this.RaisePropertyChanged("Gender");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Size {
            get {
                return this.SizeField;
            }
            set {
                if ((object.ReferenceEquals(this.SizeField, value) != true)) {
                    this.SizeField = value;
                    this.RaisePropertyChanged("Size");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string abn_reason {
            get {
                return this.abn_reasonField;
            }
            set {
                if ((object.ReferenceEquals(this.abn_reasonField, value) != true)) {
                    this.abn_reasonField = value;
                    this.RaisePropertyChanged("abn_reason");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime dispense_date {
            get {
                return this.dispense_dateField;
            }
            set {
                if ((this.dispense_dateField.Equals(value) != true)) {
                    this.dispense_dateField = value;
                    this.RaisePropertyChanged("dispense_date");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public decimal dme_deposit {
            get {
                return this.dme_depositField;
            }
            set {
                if ((this.dme_depositField.Equals(value) != true)) {
                    this.dme_depositField = value;
                    this.RaisePropertyChanged("dme_deposit");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string hcpcs_code {
            get {
                return this.hcpcs_codeField;
            }
            set {
                if ((object.ReferenceEquals(this.hcpcs_codeField, value) != true)) {
                    this.hcpcs_codeField = value;
                    this.RaisePropertyChanged("hcpcs_code");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool is_abn {
            get {
                return this.is_abnField;
            }
            set {
                if ((this.is_abnField.Equals(value) != true)) {
                    this.is_abnField = value;
                    this.RaisePropertyChanged("is_abn");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool is_medicare {
            get {
                return this.is_medicareField;
            }
            set {
                if ((this.is_medicareField.Equals(value) != true)) {
                    this.is_medicareField = value;
                    this.RaisePropertyChanged("is_medicare");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string item_code {
            get {
                return this.item_codeField;
            }
            set {
                if ((object.ReferenceEquals(this.item_codeField, value) != true)) {
                    this.item_codeField = value;
                    this.RaisePropertyChanged("item_code");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string item_name {
            get {
                return this.item_nameField;
            }
            set {
                if ((object.ReferenceEquals(this.item_nameField, value) != true)) {
                    this.item_nameField = value;
                    this.RaisePropertyChanged("item_name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string modifier1 {
            get {
                return this.modifier1Field;
            }
            set {
                if ((object.ReferenceEquals(this.modifier1Field, value) != true)) {
                    this.modifier1Field = value;
                    this.RaisePropertyChanged("modifier1");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string modifier2 {
            get {
                return this.modifier2Field;
            }
            set {
                if ((object.ReferenceEquals(this.modifier2Field, value) != true)) {
                    this.modifier2Field = value;
                    this.RaisePropertyChanged("modifier2");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string modifier3 {
            get {
                return this.modifier3Field;
            }
            set {
                if ((object.ReferenceEquals(this.modifier3Field, value) != true)) {
                    this.modifier3Field = value;
                    this.RaisePropertyChanged("modifier3");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string modifier4 {
            get {
                return this.modifier4Field;
            }
            set {
                if ((object.ReferenceEquals(this.modifier4Field, value) != true)) {
                    this.modifier4Field = value;
                    this.RaisePropertyChanged("modifier4");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<System.DateTime> patient_signature_date {
            get {
                return this.patient_signature_dateField;
            }
            set {
                if ((this.patient_signature_dateField.Equals(value) != true)) {
                    this.patient_signature_dateField = value;
                    this.RaisePropertyChanged("patient_signature_date");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<System.DateTime> physician_signature_date {
            get {
                return this.physician_signature_dateField;
            }
            set {
                if ((this.physician_signature_dateField.Equals(value) != true)) {
                    this.physician_signature_dateField = value;
                    this.RaisePropertyChanged("physician_signature_date");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int quantity {
            get {
                return this.quantityField;
            }
            set {
                if ((this.quantityField.Equals(value) != true)) {
                    this.quantityField = value;
                    this.RaisePropertyChanged("quantity");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string supplier_name {
            get {
                return this.supplier_nameField;
            }
            set {
                if ((object.ReferenceEquals(this.supplier_nameField, value) != true)) {
                    this.supplier_nameField = value;
                    this.RaisePropertyChanged("supplier_name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public decimal unit_price {
            get {
                return this.unit_priceField;
            }
            set {
                if ((this.unit_priceField.Equals(value) != true)) {
                    this.unit_priceField = value;
                    this.RaisePropertyChanged("unit_price");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="DiagnosisCode", Namespace="http://schemas.datacontract.org/2004/07/API.Web.Models")]
    [System.SerializableAttribute()]
    public partial class DiagnosisCode : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string CodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private BregWcfService.EMRIntegrationReference.DiagnosisCodeType TypeField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Code {
            get {
                return this.CodeField;
            }
            set {
                if ((object.ReferenceEquals(this.CodeField, value) != true)) {
                    this.CodeField = value;
                    this.RaisePropertyChanged("Code");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public BregWcfService.EMRIntegrationReference.DiagnosisCodeType Type {
            get {
                return this.TypeField;
            }
            set {
                if ((this.TypeField.Equals(value) != true)) {
                    this.TypeField = value;
                    this.RaisePropertyChanged("Type");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="DiagnosisCodeType", Namespace="http://schemas.datacontract.org/2004/07/API.Web.Models")]
    public enum DiagnosisCodeType : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        ICD9 = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        ICD10 = 1,
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ScheduledPatient", Namespace="http://schemas.datacontract.org/2004/07/API.Web")]
    [System.SerializableAttribute()]
    public partial class ScheduledPatient : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="EMRIntegrationReference.IEMRIntegration")]
    public interface IEMRIntegration {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IEMRIntegration/GetPatients", ReplyAction="http://tempuri.org/IEMRIntegration/GetPatientsResponse")]
        BregWcfService.EMRIntegrationReference.schedule_item[] GetPatients(int practiceId, int practiceLocationId, System.DateTime utcStartDate, System.Nullable<System.DateTime> utcEndDate);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IEMRIntegration/QueueOutboundFile", ReplyAction="http://tempuri.org/IEMRIntegration/QueueOutboundFileResponse")]
        void QueueOutboundFile(int practiceId, string fileName, byte[] fileContents, string vccScheduleId, int sourceItemId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IEMRIntegration/GetOutboundQueueItems", ReplyAction="http://tempuri.org/IEMRIntegration/GetOutboundQueueItemsResponse")]
        BregWcfService.EMRIntegrationReference.OutboundQueueItem[] GetOutboundQueueItems();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IEMRIntegration/GetOutboundFileContentsByOutboundQueueID", ReplyAction="http://tempuri.org/IEMRIntegration/GetOutboundFileContentsByOutboundQueueIDRespon" +
            "se")]
        byte[] GetOutboundFileContentsByOutboundQueueID(System.Guid outboundQueueId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IEMRIntegration/SetOutboundQueueItemProcessed", ReplyAction="http://tempuri.org/IEMRIntegration/SetOutboundQueueItemProcessedResponse")]
        void SetOutboundQueueItemProcessed(System.Guid outboundQueueId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IEMRIntegration/GetQueueProcessorStoragePathForPracticeId", ReplyAction="http://tempuri.org/IEMRIntegration/GetQueueProcessorStoragePathForPracticeIdRespo" +
            "nse")]
        string GetQueueProcessorStoragePathForPracticeId(int practiceId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IEMRIntegration/AddQueueProcessorErrorMessageForQueueItem", ReplyAction="http://tempuri.org/IEMRIntegration/AddQueueProcessorErrorMessageForQueueItemRespo" +
            "nse")]
        void AddQueueProcessorErrorMessageForQueueItem(System.Guid outboundQueueId, string message);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IEMRIntegration/QueueOutboundHL7Request", ReplyAction="http://tempuri.org/IEMRIntegration/QueueOutboundHL7RequestResponse")]
        void QueueOutboundHL7Request(int practiceID, string scheduleIdentifier, BregWcfService.EMRIntegrationReference.OutboundHL7DispenseItem[] dispenseItems, BregWcfService.EMRIntegrationReference.DiagnosisCode[] diagnosisCodes, int dispenseId, string emrClinicianId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IEMRIntegration/AddScheduledPatient", ReplyAction="http://tempuri.org/IEMRIntegration/AddScheduledPatientResponse")]
        void AddScheduledPatient(BregWcfService.EMRIntegrationReference.ScheduledPatient Patient);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IEMRIntegrationChannel : BregWcfService.EMRIntegrationReference.IEMRIntegration, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class EMRIntegrationClient : System.ServiceModel.ClientBase<BregWcfService.EMRIntegrationReference.IEMRIntegration>, BregWcfService.EMRIntegrationReference.IEMRIntegration {
        
        public EMRIntegrationClient() {
        }
        
        public EMRIntegrationClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public EMRIntegrationClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public EMRIntegrationClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public EMRIntegrationClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public BregWcfService.EMRIntegrationReference.schedule_item[] GetPatients(int practiceId, int practiceLocationId, System.DateTime utcStartDate, System.Nullable<System.DateTime> utcEndDate) {
            return base.Channel.GetPatients(practiceId, practiceLocationId, utcStartDate, utcEndDate);
        }
        
        public void QueueOutboundFile(int practiceId, string fileName, byte[] fileContents, string vccScheduleId, int sourceItemId) {
            base.Channel.QueueOutboundFile(practiceId, fileName, fileContents, vccScheduleId, sourceItemId);
        }
        
        public BregWcfService.EMRIntegrationReference.OutboundQueueItem[] GetOutboundQueueItems() {
            return base.Channel.GetOutboundQueueItems();
        }
        
        public byte[] GetOutboundFileContentsByOutboundQueueID(System.Guid outboundQueueId) {
            return base.Channel.GetOutboundFileContentsByOutboundQueueID(outboundQueueId);
        }
        
        public void SetOutboundQueueItemProcessed(System.Guid outboundQueueId) {
            base.Channel.SetOutboundQueueItemProcessed(outboundQueueId);
        }
        
        public string GetQueueProcessorStoragePathForPracticeId(int practiceId) {
            return base.Channel.GetQueueProcessorStoragePathForPracticeId(practiceId);
        }
        
        public void AddQueueProcessorErrorMessageForQueueItem(System.Guid outboundQueueId, string message) {
            base.Channel.AddQueueProcessorErrorMessageForQueueItem(outboundQueueId, message);
        }
        
        public void QueueOutboundHL7Request(int practiceID, string scheduleIdentifier, BregWcfService.EMRIntegrationReference.OutboundHL7DispenseItem[] dispenseItems, BregWcfService.EMRIntegrationReference.DiagnosisCode[] diagnosisCodes, int dispenseId, string emrClinicianId) {
            base.Channel.QueueOutboundHL7Request(practiceID, scheduleIdentifier, dispenseItems, diagnosisCodes, dispenseId, emrClinicianId);
        }
        
        public void AddScheduledPatient(BregWcfService.EMRIntegrationReference.ScheduledPatient Patient) {
            base.Channel.AddScheduledPatient(Patient);
        }
    }
}
