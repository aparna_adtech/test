﻿
namespace BregWcfService.EmailTemplates
{
    public class TemplateHelper
    {
        public const string DateFormat = "M/dd/yyyy";
        public const string TimeFormat = "h:mm tt PST";
        public const string DateTimeFormat = "M/dd/yyyy h:mm tt PST";
        public const string AppDateTimeFormat = "M/dd/yyyy hh:mm:ss tt zzz";
    }
}