﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BregWcfService.EmailTemplates
{
    public partial class InventoryComplete : System.Web.UI.UserControl
    {

        public enum ConsignmentEmailType
        {
            ConsignmentInventoryComplete,
            ConsignmentItemsAddedToCart
        }

        public string PracticeName { get; set; }
        public string PracticeLocationName { get; set; }
        public int PracticeLocationID { get; set; }
        public ConsignmentEmailType EmailType { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LoadControlInfo()
        {
            grdProducts.ItemDataBound += new DataGridItemEventHandler(grdProducts_ItemDataBound);
            lblPractice.Text = PracticeName;
            lblPracticeLocation.Text = PracticeLocationName;
            SetEmailText();

            InventoryDiscrepencyItem[] discrepencies = BregWcfService.ConsignmentItem.GetConsignmentReplenishmentList(PracticeLocationID);

            grdProducts.DataSource = discrepencies.OrderBy(a => a.ReOrderQuantity < 1).ThenBy(a => a.IsRMA).ThenBy(b => b.IsBuyout);
            grdProducts.DataBind();
        }

        private void SetEmailText()
        {
            if (EmailType == ConsignmentEmailType.ConsignmentInventoryComplete)
            {
                lblEmailText.Text = "A Consignment Inventory was just completed by the following Practice Location";
            }
            else if (EmailType == ConsignmentEmailType.ConsignmentItemsAddedToCart)
            {
                lblEmailText.Text = "A Consignment order was just added to the cart for the following Practice Location";
            }
        }

        void grdProducts_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            //Reorder Quantity
            try
            {                
                var valueField = DataBinder.Eval(e.Item.DataItem, "ReorderQuantity");
                if (valueField != null)
                {
                    int value = 0;
                    int.TryParse(valueField.ToString(), out value);
                    
                    if (value > 0)
                    {
                        e.Item.Cells[4].ForeColor = System.Drawing.Color.Red;
                    }
                }
            }
            catch (Exception ex)
            {
            }

            // Is RMA
            SetColumnText(e, "IsRMA", "RMA", 7);
            SetColumnText(e, "IsBuyout", "Buyout", 8);

        }

        private static void SetColumnText(DataGridItemEventArgs e, string fieldName, string replacementText, int columnNumber)
        {
            try
            {
                var valueField = DataBinder.Eval(e.Item.DataItem, fieldName);
                if (valueField != null)
                {
                    bool value = false;
                    bool.TryParse(valueField.ToString(), out value);

                    if (value == true)
                    {
                        e.Item.Cells[columnNumber].Text = replacementText;
                        e.Item.Cells[columnNumber].ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        e.Item.Cells[columnNumber].Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}