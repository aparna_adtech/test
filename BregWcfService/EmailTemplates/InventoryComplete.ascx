﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InventoryComplete.ascx.cs" Inherits="BregWcfService.EmailTemplates.InventoryComplete" %>

Hi Debbie,
<br /><br />
<p>
    <asp:Label ID="lblEmailText" runat="server"></asp:Label>
</p>
<p>Practice : <asp:Label ID="lblPractice" runat="server"></asp:Label></p>
<p>Location: <asp:Label ID="lblPracticeLocation" runat="server"></asp:Label></p>

<br />
<asp:DataGrid ID="grdProducts" runat="server" AutoGenerateColumns="false">
    <Columns>
        <asp:BoundColumn DataField="Code" HeaderText="Code"></asp:BoundColumn>
        <asp:BoundColumn DataField="ProductName" ItemStyle-Width="400" HeaderText="Product"></asp:BoundColumn> 
        <asp:BoundColumn DataField="ConsignmentQuantity" HeaderText="Qty"></asp:BoundColumn>      
        <asp:BoundColumn DataField="QOH" HeaderText="Count"></asp:BoundColumn>
        <asp:BoundColumn DataField="ReOrderQuantity" HeaderText="Results"></asp:BoundColumn>
        <asp:BoundColumn DataField="QuantityOnOrder" HeaderText="On Order"></asp:BoundColumn>        
        <asp:BoundColumn DataField="WholesaleCost" HeaderText="Cost" DataFormatString="{0:C}"></asp:BoundColumn>
        <asp:BoundColumn DataField="IsRMA" HeaderText="RMA"></asp:BoundColumn>
        <asp:BoundColumn DataField="IsBuyout" HeaderText="Buyout"></asp:BoundColumn>
    </Columns>
</asp:DataGrid>
<br />
The Vision System
<br />