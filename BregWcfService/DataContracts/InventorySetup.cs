﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web.Security;
using ClassLibrary.DAL;
using System.Collections;
using System.Collections.Generic;

namespace BregWcfService
{

    [DataContract]
    public class InventorySetup
    {

        [DataMember]
        public InventoryCycle[] InventoryCycles { get; set; }



        //Turn this into get into "GetInventoryCycleTypes" for this user
        public static InventoryCycleType[] GetInventoryCycleTypes(int practiceLocationID, string userName)
        {            
            string[] roles = Roles.GetRolesForUser(userName);

            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {



                //find a previous consignment count for this practice and at least one item marked "IsConsignment"
                bool canViewConsignmentType = CanViewConsignmentInventory(practiceLocationID);


                var query = from ict in visionDB.InventoryCycleTypes
                            join ictp in visionDB.InventoryCycleTypePermissions on ict.InventoryCycleTypeID equals ictp.InventoryCycleTypeID
                            where roles.Contains(ictp.Role)
                            && (
                                    (ict.CycleName.ToLower().Contains("consignment") == false)
                                    ||
                                    (
                                        ict.CycleName.ToLower().Contains("consignment") == true
                                         && canViewConsignmentType == true
                                    )
                               )
                            select ict;





                return query.Distinct<InventoryCycleType>().ToArray<InventoryCycleType>();  
            }
            
        }

        public static bool CanViewConsignmentInventory(int practiceLocationID)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {



                //find a previous consignment count for this practice and at least one item marked "IsConsignment"
                var isPreviousConsignmentCount = (from piQoh in visionDB.ProductInventory_QuantityOnHandPhysicals
                                                  join pi in visionDB.ProductInventories on piQoh.ProductInventoryID equals pi.ProductInventoryID
                                                  join ic in visionDB.InventoryCycles on piQoh.InventoryCycleID equals ic.InventoryCycleID
                                                  join ict in visionDB.InventoryCycleTypes on ic.InventoryCycleTypeID equals ict.InventoryCycleTypeID
                                                  join ictp in visionDB.InventoryCycleTypePermissions on ict.InventoryCycleTypeID equals ictp.InventoryCycleTypeID
                                                  where
                                                     pi.PracticeLocationID == practiceLocationID
                                                     && pi.IsConsignment == true
                                                     && ict.CycleName.ToLower().Contains("consignment")
                                                     && ictp.Role.ToLower().Contains("consignment")
                                                  select piQoh).Any();

                var isConsignmentItems = (from pi in visionDB.ProductInventories
                                          where
                                             pi.PracticeLocationID == practiceLocationID
                                             && pi.IsConsignment == true
                                          select pi).Any();



                return (isPreviousConsignmentCount == true || isConsignmentItems == true);




                
            }
        }

        public static InventoryCycle[] GetInventoryCyclesByType(int practiceLocationID, string userName, string password, string deviceID, string lastLocation,  InventoryCycleType inventoryCycleType)
        {
            return GetOpenInventoryCyclesByType(practiceLocationID, userName, inventoryCycleType);
        }

        public static InventoryCycle FindMostRecentlyClosedCycle(   int practiceLocationID,
                                                                    string userName,                                                                    
                                                                    bool isConsignmentCount = true)
        {

            string cycleTypeSearchTerm = isConsignmentCount == true ? "consignment" : "standard";

            string[] roles = Roles.GetRolesForUser(userName);

            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();



            using (visionDB)
            {

                var query = (from ic in visionDB.InventoryCycles
                             join ict in visionDB.InventoryCycleTypes on ic.InventoryCycleTypeID equals ict.InventoryCycleTypeID
                             join ictp in visionDB.InventoryCycleTypePermissions on ict.InventoryCycleTypeID equals ictp.InventoryCycleTypeID
                             where roles.Contains(ictp.Role)
                             && ict.InventoryCycleTypeID == (
                                                                ( from ict1 in visionDB.InventoryCycleTypes
                                                                  where ict1.CycleName.ToLower().Contains(cycleTypeSearchTerm.ToLower())
                                                                  select ict1
                                                                ).First().InventoryCycleTypeID
                                                            )
                             &&  ic.EndDate > DateTime.Now.Subtract(new TimeSpan(14, 0, 0, 0)) // should be within the last 2 weeks(14 days)
                             && ic.EndDate != null
                             select ic).OrderByDescending(a => a.EndDate);

                return query.FirstOrDefault<InventoryCycle>();
            }

            
        }

        public static InventoryCycle[] GetOpenInventoryCyclesByType(int practiceLocationID, string userName, InventoryCycleType inventoryCycleType)
        {
            UserContext userContext = null;
            int createdUserId = 1;

            string[] roles = Roles.GetRolesForUser(userName);

            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var query = (from ic in visionDB.InventoryCycles
                             join ict in visionDB.InventoryCycleTypes on ic.InventoryCycleTypeID equals ict.InventoryCycleTypeID
                             join ictp in visionDB.InventoryCycleTypePermissions on ict.InventoryCycleTypeID equals ictp.InventoryCycleTypeID
                             where
                             ic.PracticeLocationID == practiceLocationID
                             && roles.Contains(ictp.Role)
                             && ict.InventoryCycleTypeID == inventoryCycleType.InventoryCycleTypeID
                             //&& DateTime.Now.Subtract( ic.StartDate ) < TimeSpan.FromDays(15)
                             && ic.EndDate == null
                             select ic)
                             .Distinct()
                             .OrderByDescending(a => a.StartDate);

                //create a cycle if there isn't one
                if (query.Count() == 0)
                {
                    try
                    {
                        userContext = UserContext.Login(userName);
                        if (userContext != null && userContext.User != null)
                        {
                            createdUserId = userContext.User.UserID;
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    InventoryCycle newCycle = new InventoryCycle();
                    newCycle.StartDate = DateTime.Now;
                    newCycle.EndDate = null;

                    newCycle.InventoryCycleTypeID = inventoryCycleType.InventoryCycleTypeID;
                    newCycle.PracticeLocationID = practiceLocationID;


                    newCycle.CreatedUserID = createdUserId;
                    newCycle.CreatedDate = DateTime.Now;
                    newCycle.ModifiedUserID = createdUserId;
                    newCycle.ModifiedDate = DateTime.Now;
                    newCycle.IsActive = true;

                    visionDB.InventoryCycles.InsertOnSubmit(newCycle);
                    visionDB.SubmitChanges();

                    List<InventoryCycle> cycles = new List<InventoryCycle>();
                    cycles.Add(newCycle);

                    return cycles.ToArray<InventoryCycle>();

                }
                else
                {
                    return query.ToArray<InventoryCycle>();
                }
            }
        }

        public static InventoryCycle GetInventoryCycle(int inventoryCycleID)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                System.Data.Linq.DataLoadOptions dlo = new System.Data.Linq.DataLoadOptions();
                dlo.LoadWith<InventoryCycle>(ict => ict.InventoryCycleType);
                visionDB.LoadOptions = dlo;

                InventoryCycle inventoryCycle = (
                                                    from ic in visionDB.InventoryCycles
                                                    where ic.InventoryCycleID == inventoryCycleID
                                                    select ic 

                                                ).FirstOrDefault<InventoryCycle>();

                return inventoryCycle;
            }
        }

        public static bool AddInventoryCount(int practiceLocationID, string userName, string password, string deviceID, string lastLocation, InventoryCycleType inventoryCycleType, InventoryCycle inventoryCycle, InventoryItemCount[] inventoryItemCounts, bool fromDevice = true)
        {
            UserContext userContext = null;
            int createdUserId = 1;
            Guid membershipUserID = Guid.Empty;
            

            string[] roles = Roles.GetRolesForUser(userName);

            try
            {
                if (fromDevice == true)
                {
                    userContext = UserContext.Login(userName, password, deviceID, lastLocation);
                    if (userContext != null && userContext.User != null)
                    {
                        createdUserId = userContext.User.UserID;
                    }
                }
                membershipUserID = (Guid)Membership.GetUser(userName).ProviderUserKey;
            }
            catch (Exception ex)
            {

            }

            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {

                foreach (InventoryItemCount inventoryItemCount in inventoryItemCounts)
                {
                    //inventoryItemCount.Count > 0
                    if (inventoryItemCount.Count  == 0 && fromDevice == true)
                        throw new ApplicationException("Item Count must be greater than 0.");
                    //inventoryItemCount.CountDate == today
                    //if (inventoryItemCount.CountDate.Day != DateTime.Now.Day)
                        //throw new ApplicationException("Item CountDate must be today");
                    //inventoryItemCount.InventoryItemCountID == 0
                    if (inventoryItemCount.InventoryItemCountID > 0)
                        throw new ApplicationException("The record already has an inventoryItemCountID.");
                    // inventoryItemCount.ProductInventoryID . isValid
                    if(inventoryItemCount.ProductInventoryID < 1)
                        throw new ApplicationException("Invalid Product Inventory ID.");

                    var validQuery = from pi in visionDB.ProductInventories
                                     where pi.ProductInventoryID == inventoryItemCount.ProductInventoryID
                                     && pi.PracticeLocationID == practiceLocationID
                                     select pi;
                    if(validQuery.FirstOrDefault<ProductInventory>() == null)
                        throw new ApplicationException("The ProductInventoryID does not seeem to be for this practice location.");

                    var cycleQuery = (from ic in visionDB.InventoryCycles
                                     where ic.InventoryCycleID == inventoryCycle.InventoryCycleID
                                     select ic).FirstOrDefault();

                    if (cycleQuery != null && cycleQuery.EndDate != null) // Then endDate should be null if the cycle is open
                        return false;
                        //throw new ApplicationException("The Inventory Cycle is Closed.");

                    inventoryItemCount.InventoryCycleID = inventoryCycle.InventoryCycleID;
                    inventoryItemCount.MembershipUserID = membershipUserID ;  

                    inventoryItemCount.UserID = createdUserId;
                    inventoryItemCount.DeviceIdentifier = deviceID;
                    inventoryItemCount.CreatedUserID = createdUserId;
                    inventoryItemCount.CreatedDate = DateTime.Now;
                    inventoryItemCount.ModifiedUserID = createdUserId;
                    inventoryItemCount.ModifiedDate = DateTime.Now;
                    inventoryItemCount.IsActive = true;

                }

                visionDB.InventoryItemCounts.InsertAllOnSubmit(inventoryItemCounts);
                visionDB.SubmitChanges();
                return true;
            }
        }

    }
}