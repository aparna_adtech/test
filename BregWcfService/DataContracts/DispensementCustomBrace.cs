﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web.Security;
using BregWcfService.DataContracts;
using ClassLibrary.DAL;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;


namespace BregWcfService
{


    [DataContract]
    public class DispensementCustomBrace : usp_GetCustomBraceOrdersforDispense_ParentLevelResult
    {
        //[DataMember]
        //DispensementCustomBrace[] CustomBraceDispensement { get; set; }

        [DataMember]
        public bool SaveToQueueOnly { get; set; }

        [DataMember]
        public int PracticeLocationID { get; set; }

        [DataMember]
        public int DispenseCount { get; set; }

        [DataMember]
        public DispenseStatus DispenseStatus { get; set; }

        [DataMember]
        public Int32 DispenseBatchID { get; set; }

        [DataMember]
        public string PatientEmail { get; set; }

        [DataMember]
        public int? PracticePayerID { get; set; }

        [DataMember]
        public string PatientCode { get; set; }

        [DataMember]
        public string PatientFirstName { get; set; }

        [DataMember]
        public string PatientLastName { get; set; }

        [DataMember]
        public string DispensementNote { get; set; }

        [DataMember]
        public byte[] PatientSignature { get; set; }

        private DateTime _PatientSignedDate;

        [DataMember]
        public DateTime? PatientSignedDate
        {
            get
            {
                return _PatientSignedDate;
            }
            set
            {
                var d = DispenseItem.CorrectUtcDateSerialization(value.GetValueOrDefault());
                d = DispenseItem.LimitDateValue(d, 2);
                _PatientSignedDate = d;
            }


        }

        [DataMember]
        public bool PhysicianSigned { get; set; }

        [DataMember]
        public string PhysicianSignedDate { get; set; }

        [DataMember]
        public int ZeroQuantityDispenseCount { get; set; }

        [DataMember]
        public int ItemsSelectedCount { get; private set; }

        [DataMember]
        public int[] ZeroQtyProductInventoryIDs { get; set; }

        [DataMember]
        public int[] SelectedProductInventoryIDs { get; set; }

        [DataMember]
        public DispenseCustomBraceDetail[] CustomBraceDetail { get; set; }

        [DataMember]
        public int PhysicianIDNum { get; set; }

        [DataMember]
        public byte[] PatientReceipt { get; set; }

        [DataMember]
        public string QueuedReason { get; set; }

        [DataMember]
        public int PatientSignatureRelationshipID { get; set; }

        private int _fitterID;

        [DataMember]
        public int FitterID
        {
            get
            {
                try
                {
                    if (this.CustomBraceDetail != null)
                        _fitterID = this.CustomBraceDetail.Select(x => x.FitterID).FirstOrDefault();
                    else
                        _fitterID = 0;

                    return _fitterID;
                }
                catch (Exception)
                {
                    throw;
                }
            }
            set
            {
                try
                {
                    _fitterID = value;
                    this.CustomBraceDetail.Select(f => { f.FitterID = _fitterID; return f; }).ToList();                    
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="practiceID"></param>
        /// <param name="practiceLocationID"></param>
        /// <param name="deviceIdentifier"></param>
        /// <param name="dispenseIdentifier"></param>
        /// <returns></returns>
        public bool AddCustomBraceItemsToDispenseQueue(string userName, string password, int practiceID, Int32 practiceLocationID, string deviceIdentifier, string dispenseIdentifier, bool IsV5Dispensement)
        {
            // If the dispensement indicated by the combination of the deviceIdentifier and the dispenseIdentifier already exists, then we've done this before and we're finished
            if (DAL.PracticeLocation.Dispensement.IsDispensementExist(deviceIdentifier, dispenseIdentifier))
                return false;

            Dispensement dispensement = new Dispensement();
            dispensement.PatientSignature = this.PatientSignature;
            dispensement.PhysicianSigned = this.PhysicianSigned;
            dispensement.PhysicianSignedDate = this.PhysicianSignedDate;
            dispensement.PracticeLocationID = this.PracticeLocationID;
            dispensement.SaveToQueueOnly = this.SaveToQueueOnly;
            dispensement.PatientSignedDate = this.PatientSignedDate;
            dispensement.PatientFirstName = this.PatientFirstName;
            dispensement.PatientLastName = this.PatientLastName;
            dispensement.PracticePayerID = this.PracticePayerID;
            dispensement.DispensementNote = this.DispensementNote;
            dispensement.QueuedReason = this.QueuedReason;
            dispensement.PatientSignatureRelationshipID = this.PatientSignatureRelationshipID;
            
            List<DispenseItem> dispenseItems = new List<DispenseItem>();

            //foreach (DispensementCustomBrace customBraceOrder in this.CustomBraceDispensement)
            //{
            foreach (var braceItem in this.CustomBraceDetail)  //customBraceOrder.CustomBraceDetail)
            {

                DispenseItem dispenseItem = new DispenseItem(this, braceItem);

                // TODO: override the patient code coming in from the custom brace dispensement
                //dispenseItem.PatientCode = this.PatientCode;

                if (isPracticeCustomFitEnabled(practiceID))
                    dispenseItem.IsCustomFit = braceItem.IsCustomFit || braceItem.IsCustomBrace;
                else
                    dispenseItem.IsCustomFit = null;

                dispenseItems.Add(dispenseItem);
            }
            //}

            dispensement.DispenseItems = dispenseItems.ToArray<DispenseItem>();

            MemoryStream pdfMemoryStream = null;
            if (isPracticeHandheldPrintEnabled(practiceID))
            {
                pdfMemoryStream = new MemoryStream();
            }

            bool customBraceOrdersOk = dispensement.DispenseOrQueueProducts(userName, password, practiceID, practiceLocationID, pdfMemoryStream, deviceIdentifier, dispenseIdentifier, IsV5Dispensement);

            if (pdfMemoryStream != null)
            {
                PatientReceipt = pdfMemoryStream.ToArray();
            }

            this.DispenseBatchID = dispensement.DispenseBatchID;
            this.DispenseCount = dispensement.DispenseCount;
            this.DispenseStatus = dispensement.DispenseStatus;

            return customBraceOrdersOk;

        }

        private bool isPracticeHandheldPrintEnabled(int practiceId)
        {
            return DAL.Practice.Practice.IsHandheldPrintEnabled(practiceId);
        }

        private bool isPracticeCustomFitEnabled(int practiceId)
        {
            return DAL.Practice.Practice.IsCustomFitEnabled(practiceId);
        }

        protected static void UpdateDispenseQueue(int practiceLocationID, int InventoryID, string PatientCode, int PhysicianID, int Quantity, bool IsMedicare, bool ABNForm, string Side, decimal DMEDeposit, int? BregVisionOrderID, bool isCustomFit, bool isCustomFabricated, string abnType, string mod1, string mod2, string mod3, string QueuedReason, bool preAuthorized)
        {
            DAL.PracticeLocation.Dispensement.AddUpdateDispensement(
                                                                        DispenseQueueID: 0,
                                                                        PracticeLocationID: practiceLocationID,
                                                                        InventoryID: InventoryID,
                                                                        PatientCode: PatientCode,
                                                                        PhysicianID: PhysicianID, 
                                                                        ICD9_Code: "",
                                                                        IsMedicare: IsMedicare,
                                                                        ABNForm: ABNForm,
                                                                        IsCashCollection: false,
                                                                        Side: Side,
                                                                        DMEDeposit: DMEDeposit,
                                                                        AbnType: abnType,
                                                                        BregVisionOrderID: BregVisionOrderID,
                                                                        Quantity: 1, 
                                                                        isCustomFit: isCustomFit,
                                                                        mod1: mod1,
                                                                        mod2: mod2,
                                                                        mod3: mod3,
                                                                        DispenseDate: null,
                                                                        CreatedUser: 1,
                                                                        CreatedDate: DateTime.Now,
                                                                        QueuedReason: QueuedReason,
                                                                        isCustomFabricated: isCustomFabricated,
                                                                        preAuthorized: preAuthorized,
                                                                        IsCalledFromV1Endpoint:false
                                                                    );
        }

		public static DispensementCustomBrace[] GetCustomBraceOrdersforDispense(out int count, int practiceLocationID, string searchCriteria, string searchText, int skip, int take)
		{
			List<DispensementCustomBrace> dispenseCustomBraces = new List<DispensementCustomBrace>();

			VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

			using (visionDB)
			{
				var query = from cbo in visionDB.usp_GetCustomBraceOrdersforDispense_ParentLevel(practiceLocationID, searchCriteria, searchText)

							select new DispensementCustomBrace
							{
								BregVisionOrderID = cbo.BregVisionOrderID,
								CreatedDate = cbo.CreatedDate,
								CreatedUserID = cbo.CreatedUserID,
								CustomPurchaseOrderCode = cbo.CustomPurchaseOrderCode,
								PatientName = cbo.PatientName,
								PhysicianID = cbo.PhysicianID ?? 0,
								PhysicianIDNum = cbo.PhysicianID ?? 0,
								PurchaseOrder = cbo.PurchaseOrder,
								ShippingType = cbo.ShippingType,
								Status = cbo.Status,
								Total = cbo.Total,
								CustomBraceDetail = new DispenseCustomBraceDetail[0]
							};
				var detailQuery = from cbod in visionDB.usp_GetCustomBraceOrdersforDispense_Detail1Level(practiceLocationID, searchCriteria, searchText)
								  select new DispenseCustomBraceDetail
								  {
									  ActualWholesaleCost = cbod.ActualWholesaleCost,
									  BregVisionOrderID = cbod.BregVisionOrderID,
									  Code = cbod.Code,
									  Gender = cbod.Gender,
									  ProductInventoryID = cbod.ProductInventoryID,
									  ProductName = cbod.ProductName,
									  QuantityOnHand = cbod.QuantityOnHand,
									  Side = cbod.Side,
									  Size = cbod.Size,

								  };

				dispenseCustomBraces = query.ToList<DispensementCustomBrace>();

				var detailArray = detailQuery.ToArray<DispenseCustomBraceDetail>();
				foreach (var dispenseCustomBrace in dispenseCustomBraces)
				{
					List<DispenseCustomBraceDetail> detailList = new List<DispenseCustomBraceDetail>();
					foreach (var detail in detailArray)
					{
						if (detail.BregVisionOrderID == dispenseCustomBrace.BregVisionOrderID)
						{
							detailList.Add(detail);
						}
					}
					dispenseCustomBrace.CustomBraceDetail = detailList.ToArray();

					foreach (var customOrderItemDetail in dispenseCustomBrace.CustomBraceDetail)
					{
						var prodInvItemMasterCatalogProduct = (from prodInv in visionDB.ProductInventories
															   where prodInv.ProductInventoryID == customOrderItemDetail.ProductInventoryID
															   select new
															   {
																   IsCustomBrace = prodInv.PracticeCatalogProduct.MasterCatalogProduct != null ? prodInv.PracticeCatalogProduct.MasterCatalogProduct.IsCustomBrace : false,
																   IsSplitCode = prodInv.PracticeCatalogProduct.IsSplitCode,
																   IsAlwaysOffTheShelf = prodInv.PracticeCatalogProduct.IsAlwaysOffTheShelf
															   }).FirstOrDefault();
						if (prodInvItemMasterCatalogProduct == null)
							continue;
						customOrderItemDetail.IsCustomBrace = prodInvItemMasterCatalogProduct.IsCustomBrace;
						customOrderItemDetail.IsSplitCode = prodInvItemMasterCatalogProduct.IsSplitCode;
						customOrderItemDetail.IsAlwaysOffTheShelf = prodInvItemMasterCatalogProduct.IsAlwaysOffTheShelf;
					}
				}

				count = dispenseCustomBraces.Count;

				return dispenseCustomBraces.Skip<DispensementCustomBrace>(skip).Take<DispensementCustomBrace>(take).ToArray<DispensementCustomBrace>();
			}

		}
	}


	[DataContract]
    public class DispenseCustomBraceDetail : usp_GetCustomBraceOrdersforDispense_Detail1LevelResult
    {
        [DataMember]
        public bool IsMedicare { get; set; }

        [DataMember]
        public bool PreAuthorized { get; set; }

        [DataMember]
        public bool IsABNForm { get; set; }

        [DataMember]
        public string ABNReason { get; set; }

        [DataMember]
        public string ABNType { get; set; }

        [DataMember]
        public string DeclineReason { get; set; }

        [DataMember]
        public DateTime? DispenseDate { get; set; }

        [DataMember]
        public int MedicareOption { get; set; }

        [DataMember]
        public bool IsRental { get; set; }

        [DataMember]
        public bool IsCashCollection { get; set; }

        [DataMember]
        public decimal DMEDeposit { get; set; }

        [DataMember]
        public string ICD9_Code { get; set; }

        [DataMember]
        public int DispenseSignatureID { get; set; }

        [DataMember]
        public int PatientSignatureRelationshipID { get; set; }

        [DataMember]
        public int FitterID { get; set; }

        [DataMember]
        public CustomFitProcedureResponse[] CustomFitProcedureResponses { get; set; }

        [DataMember]
        public bool IsCustomBrace { get; set; }

        [DataMember]
        public bool IsSplitCode { get; set; }

        [DataMember]
        public bool IsAlwaysOffTheShelf { get; set; }

        [DataMember]
        public string PatientFirstName { get; set; }

        [DataMember]
        public string PatientLastName { get; set; }

        [DataMember]
        public string Mod1 { get; set; }

        [DataMember]
        public string Mod2 { get; set; }

        [DataMember]
        public string Mod3 { get; set; }
        
        [DataMember]
        public bool IsCustomFit { get; set; }
        
        [DataMember]
        public int QuantityToDispense { get; set; }
    }
}