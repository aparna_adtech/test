﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BregWcfService.DataContracts
{
    [DataContract]
    public class CustomFitProcedureResponse
    {
        [DataMember]
        public int CustomFitProcedureId { get; set; }

        [DataMember]
        public string UserInput { get; set; }
    }
}