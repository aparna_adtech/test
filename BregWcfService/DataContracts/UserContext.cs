﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.Security;
using ClassLibrary.DAL;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Net;


namespace BregWcfService
{

    [DataContract]
    public class UserContext
    {

        [DataMember]
        public User User { get; set; }

        [DataMember]
        public Contact UserContact { get; set; }

        [DataMember]
        public ClassLibrary.DAL.Practice Practice { get; set; }

        [DataMember]
        public ClassLibrary.DAL.PracticeLocation[] PracticeLocations { get; set; }

        [DataMember]
        public ClassLibrary.DAL.PracticeLocation DefaultLocation { get; set; }

        [DataMember]
        public string[] SecurityRoles { get; set; }

        [DataMember]
        public bool IsBregAdmin { get; set; }

        [DataMember]
        public bool IsPracticeAdmin { get; set; }

        [DataMember]
        public bool IsConsignmentRep { get; set; }


        [DataMember]
        public BregWcfService.Practice[] Practices { get; set; }

        public static bool ValidateLogin(string userName, string password, string deviceID, string lastLocation)
        {
            #region WCF Header Stuff (unused)
            try
            {
                if (HttpContext.Current != null)
                {
                    if (HttpContext.Current.Session != null)
                    {
                        OperationContext context = OperationContext.Current;
                        if (context != null)
                        {
                            MessageProperties properties = context.IncomingMessageProperties;
                            if (properties != null)
                            {
                                RemoteEndpointMessageProperty endpoint = properties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
                                if (endpoint != null)
                                {
                                    string ip = endpoint.Address;
                                    HttpContext.Current.Session["IPAddress"] = ip;
                                }
                            }
                        }
                    }
                }
            }
            catch { }
            #endregion

            if (Membership.ValidateUser(userName, password) && Classes.Device.SimpleValidateDevice(deviceID, lastLocation))
                return true;

            throw new ApplicationException("Unable to Validate User.");
        }

        public static UserContext Login(string userName, string password, string deviceID, string lastLocation)
        {
            var isMembershipProviderAuthenticated = Membership.ValidateUser(userName, password);
            var userContext = _GetUserContextForUsername(userName, deviceID, lastLocation);
            var isVisionAccountAuthenticated = (userContext != null);

            if (isMembershipProviderAuthenticated && isVisionAccountAuthenticated)
            {
                return userContext;
            }
            else
            {
                //Pass a fault containing the ApplicationException back to the client.  
                throw new LoginException("The Username Password combination are Invalid");
            }
        }

        private static UserContext _GetUserContextForUsername(string userName, string deviceID, string lastLocation)
        {
            var ctx = Login(userName);
            if (ctx != null)
            {
                int? practice_id = (ctx.Practice != null) ? ctx.Practice.PracticeID : (int?)null;
                Classes.Device.ValidateOrRegisterDeviceForAuthorization(deviceID, lastLocation, userName, practice_id);
            }
            return ctx;
        }

        public static UserContext Login(string userName)
        {
            // try to find the user using the current membership provider
            MembershipUser membershipUser = Membership.GetUser(userName);
            // if user is not found, then we will have a null result... shortcut the rest of the method if this is the case
            if (membershipUser == null)
                return null;

            Guid userID = Guid.Parse(membershipUser.ProviderUserKey.ToString());

            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var context =
                    (from u in visionDB.Users
                     from p in visionDB.Practices
                     where
                         u.PracticeID == p.PracticeID
                         && u.IsActive
                         && u.AspNet_UserID.Equals(userID)
                     select new UserContext
                     {
                         User = u,
                         UserContact = ((from c in visionDB.Contacts where c.ContactID == u.ContactID select c).FirstOrDefault<Contact>()),
                         Practice = p,
                         DefaultLocation = ((from pl in visionDB.PracticeLocations where pl.PracticeLocationID == u.DefaultLocationID select pl).FirstOrDefault<ClassLibrary.DAL.PracticeLocation>()),
                         PracticeLocations = (from pl in visionDB.PracticeLocations where pl.PracticeID == p.PracticeID select pl).ToArray()
                     })
                    .FirstOrDefault();

                if (context == null)
                    context = new UserContext();

                context.SecurityRoles = Roles.GetRolesForUser(userName);

                bool bregAdmin = Roles.IsUserInRole(userName, "bregadmin");
                bool consignmentAdmin = Roles.IsUserInRole(userName, "BregConsignment");
                bool practiceAdmin = Roles.IsUserInRole(userName, "practiceadmin");

                if (bregAdmin == true)
                {
                    context.IsBregAdmin = bregAdmin;
                }
                else if (consignmentAdmin == true)
                {
                    context.IsConsignmentRep = consignmentAdmin;
                }
                else if (practiceAdmin == true)
                {
                    context.IsPracticeAdmin = practiceAdmin;
                }

                // handle invalid conditions here, only BregAdmin and BregConsignment roles can have no practice assignment
                if ((context.Practice == null) && !(bregAdmin || consignmentAdmin))
                    return null;

                return context;
            }
        }
    }

    [Serializable()]
    public class LoginException : Exception
    {
        public LoginException() : base() { }
        public LoginException(string message) : base(message) { }
        //public LoginException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client. 
        protected LoginException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}