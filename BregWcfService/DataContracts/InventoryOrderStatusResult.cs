﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web.Security;
using System.Collections.Generic;
using ClassLibrary.DAL;

namespace BregWcfService
{
    [Serializable]
    [DataContract]
    public class InventoryOrderStatusResult : usp_GetInventoryOrderStatusResult
    {

		[DataMember]
		public string HcpcString { get; set; }

		[DataMember]
		public string HcpcOtsString { get; set; }

		//       [DataMember]
		//       public bool IsConsignment { get; set; }

		//       [DataMember]
		//       public bool IsABN { get; set; }

		[DataMember]
		public bool IsABNNeeded { get; set; }

		//       [DataMember]
		//       public int ConsignmentQuantity { get; set; }

		//       [DataMember]
		//       public int MasterCatalogProductID { get; set; }

		//       [DataMember]
		//       public int ThirdPartyProductID { get; set; }

		[DataMember]
		public bool IsThirdParty { get; set; }

		//       [DataMember]
		//       public decimal BillingCharge { get; set; }

		//       [DataMember]
		//       public decimal BillingChargeCash { get; set; }

		//       [DataMember]
		//       public string Packaging { get; set; }

		//       [DataMember]
		//       public string Gender { get; set; }

		//       [DataMember]
		//       public string Color { get; set; }

		//       [DataMember]
		//       public Decimal WholesaleCost { get; set; }

		//       [DataMember]
		//       public Decimal DMEDeposit { get; set; }

		[DataMember]
		public string[] UPCCode_List { get; set; }

		//       [DataMember]
		//       public bool? IsCustomBrace { get; set; }

		[DataMember]
		public string[] ModiferStrings { get; set; }

		//      [DataMember]
		//      public bool IsAlwaysOffTheShelf { get; set; }

		//      [DataMember]
		//      public bool IsPreAuthorization { get; set; }

		//      [DataMember]
		//      public bool IsActive { get; set; }

		[DataMember]
		public string Side { get; set; }

		[DataMember]
		public string BrandName { get; set; }

		[DataMember]
		public string ProductName { get; set; }

		//[DataMember]
		//public int? NewMasterCatalogProductID { get; set; }

		//[DataMember]
		//public int? NewThirdPartyProductID { get; set; }

		//[DataMember]
		//public int? NewPracticeCatalogProductID { get; set; }

		[DataMember]
		public int? OldQuantityOnHand { get; set; }

		//[DataMember]
		//public string NewProductCode { get; set; }

		//[DataMember]
		//public string NewProductName { get; set; }

        //[DataMember]
        //public bool? IsLateralityExempt { get; set; }
    }
}