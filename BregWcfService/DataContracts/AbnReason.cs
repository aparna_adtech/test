﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web.Security;
using ClassLibrary.DAL;
using System.Collections;
using System.Collections.Generic;

namespace BregWcfService
{
    [DataContract]
    public class AbnReason
    {
        public static ABNReason[] GetAbnReasons()
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var reasonQuery = (from ar in visionDB.ABNReasons
                                   select ar).OrderBy(a => a.SortOrder);

                return reasonQuery.ToArray<ABNReason>();
            }

        }
    }
}