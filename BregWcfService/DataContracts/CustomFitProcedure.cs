﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BregWcfService.DataContracts
{
    [DataContract]
    public class CustomFitProcedure
    {
        [DataMember]
        public int CustomFitProcedureId { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int SortPosition { get; set; }

        [DataMember]
        public bool IsUserEntered { get; set; }
    }
}