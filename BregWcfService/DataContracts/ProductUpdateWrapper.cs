﻿using System.Collections.Generic;
using System.Linq;
using ClassLibrary.DAL;
using System.Runtime.Serialization;
using DAL.PracticeLocation;
using Telerik.Charting.Styles;

namespace BregWcfService.DataContracts
{
    [DataContract]
    public class ProductUpdateWrapper
    {
        [DataMember]
        public int LatestSerialNumber { get; set; }

        [DataMember]
        public InventoryOrderStatusResult[] ProductInventory { get; set; }

  
        [DataMember]
        public double TotalServerTime { get; set; }

        public void UpdateLatestProductInventory(int serialNumber, int practiceLocationId, int practiceId)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var logs =
                    visionDB.ProductChangeLogs
                    .Where(
                        log =>
                            (log.Id > serialNumber) &&
                            (
                                (log.PracticeLocationId == practiceLocationId) ||
                                (log.PracticeId == practiceId) ||
                                ((log.PracticeId != null) && (log.PracticeId == -1)) ||
                                ((log.PracticeLocationId != null) && (log.PracticeLocationId == -1))
                            )
                    )
                    .OrderBy(x => x.Id) //ordering by id so you can grab the highest serial number in the group
                    ;

                LatestSerialNumber = logs.Any() ? logs.Max(x => x.Id) : serialNumber;

                var list1 = (from log in logs.Where(x => x.ProductInventoryId.HasValue)
                             join pi in visionDB.ProductInventories on log.ProductInventoryId.Value equals pi.ProductInventoryID
                             select pi.ProductInventoryID).Distinct();
                var list2 = (from log in logs.Where(x => x.PracticeCatalogProductId.HasValue)
                             join pi in visionDB.ProductInventories on log.PracticeCatalogProductId.Value equals pi.PracticeCatalogProductID
                             where pi.PracticeLocationID == practiceLocationId
                             select pi.ProductInventoryID).Distinct();
                var list3 = (from log in logs.Where(x => x.MasterCatalogProductId.HasValue)
                             join pi in visionDB.ProductInventories on log.MasterCatalogProductId.Value equals pi.PracticeCatalogProduct.MasterCatalogProductID
                             where pi.PracticeLocationID == practiceLocationId
                             select pi.ProductInventoryID).Distinct();

                var productInventoryIds = list1.Union(list2).Union(list3);
                var productInventoryIDChunks = Utilities.BuildIdChunks(productInventoryIds.ToList());

                var results = new List<InventoryOrderStatusResult>();
                foreach (var chunk in productInventoryIDChunks)
                {
                    results.AddRange(
                        LandingData.GetInventoryOrderStatusWithPriority(practiceLocationId, chunk)
                    );
                }
                ProductInventory = results.ToArray();
            }
        }

        public static int GetLatestSerialNumber(int practiceLocationId, int practiceId)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var serial = 0;
                var rows =
                    visionDB.ProductChangeLogs.Where(
                        x => x.PracticeLocationId == practiceLocationId || x.PracticeId == practiceId);
                if (rows.Any())
                    serial = rows.Max(x => x.Id);
                return serial;
            }
        }  
    }
}