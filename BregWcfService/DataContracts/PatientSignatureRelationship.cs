﻿using ClassLibrary.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BregWcfService
{
    [DataContract]
    public class PatientSignatureRelationship
    {
        public PatientSignatureRelationship()
        {

        }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string PatientSignatureRelation { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        public static PatientSignatureRelationship[] GetPatientSignatureRelationshipData()
        {           
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var query = from psr in visionDB.PatientSignatureRelationships
                            select new PatientSignatureRelationship
                            {
                                Id = psr.Id,
                                PatientSignatureRelation = psr.PatientSignatureRelationship1,
                                IsActive = psr.IsActive.HasValue ? psr.IsActive.Value : false
                            };
                
                return query.Where(x=>x.IsActive).ToArray<PatientSignatureRelationship>();
            }
        }
    }
}