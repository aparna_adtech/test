﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web.Security;
using BregWcfService.DataContracts;
using ClassLibrary.DAL;
using System.Collections.Generic;
using com.breg.vision.Integration.VisionBcsModels;
using System.Net;
using com.breg.vision.BCSIntegrationHelper;
using com.breg.vision.Logger;

namespace BregWcfService
{
    [DataContract]
    public class Dispensement
    {
        public static FileLogger logger = new FileLogger();
        static string projectTitle = "BregWcfService.DataContracts.Dispensement";

        public Dispensement()
        {
            ZeroQtyProductInventoryIDs = new int[0];
            DispenseBatchID = 0;
        }


        [DataMember]
        public bool SaveToQueueOnly { get; set; }

        [DataMember]
        public int PracticeLocationID { get; set; }

        /// <summary>
        /// The Number of Items Dispensed
        /// </summary>
        [DataMember]
        public int DispenseCount { get; set; }

        [DataMember]
        public string DispenseDate { get; set; }

        [DataMember]
        public DispenseStatus DispenseStatus { get; set; }

        [DataMember]
        public string QueuedReason { get; set; }

        [DataMember]
        public Int32 DispenseBatchID { get; set; }

        [DataMember]
        public DispenseItem[] DispenseItems { get; set; }

        [DataMember]
        public byte[] PatientSignature { get; set; }

        private DateTime? _PatientSignedDate;

        [DataMember]
        public DateTime? PatientSignedDate
        {
            get
            {
                return _PatientSignedDate;
            }
            set
            {
                _PatientSignedDate = value.HasValue ? value.Value.ToLocalTime() : (DateTime?)null;
            }
        }

        [DataMember]
        public Int32 PhysicianID { get; set; }

        [DataMember]
        public bool PhysicianSigned { get; set; }

        [DataMember]
        public string PhysicianSignedDate { get; set; }

        [DataMember]
        public string PatientCode { get; set; }

        [DataMember]
        public string PatientFirstName { get; set; }

        [DataMember]
        public string PatientLastName { get; set; }

        [DataMember]
        public string DispensementNote { get; set; }

        [DataMember]
        public int? PracticePayerID { get; set; }

        [DataMember]
        public int ZeroQuantityDispenseCount { get; set; }

        [DataMember]
        public int ItemsSelectedCount { get; private set; }

        [DataMember]
        public int[] ZeroQtyProductInventoryIDs { get; set; }

        [DataMember]
        public int[] SelectedProductInventoryIDs { get; set; }

        [DataMember]
        public int DispenseSignatureID { get; set; }

        [DataMember]
        public int PatientSignatureRelationshipID { get; set; }

        [DataMember]
        public byte[] PatientReceipt { get; set; }

        [DataMember]
        public int DispenseDocumentVersion { get; set; }

        private int? _fitterID;     

        [DataMember]         
        public int? FitterID
        {
            get
            {
                try
                {
                    if (this.DispenseItems != null)
                        _fitterID = this.DispenseItems.Select(x => x.FitterID).FirstOrDefault();
                    else
                        _fitterID = null;

                    return _fitterID;
                }
                catch (Exception)
                {
                    throw;
                }
            }
            set
            {
                try
                {
                    _fitterID = value.HasValue ? value.Value : (int?)null;
                    this.DispenseItems.Select(f => { f.FitterID = _fitterID; return f; }).ToList();  
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        [DataMember]
        public int DocumentType { get; set; }

        [DataMember]
        public int DocumentSource { get; set; }

        [DataMember]
        public string[] DocumentIds { get; set; }

        public int DispenseID { get; set; }
        public int SetProdductInventoryIDs()
        {
            var query = from p in DispenseItems
                        where p.IsSelected == true
                        select p.ProductInventoryID;

            SelectedProductInventoryIDs = query.ToArray<int>();

            ItemsSelectedCount = SelectedProductInventoryIDs.Count();

            return SelectedProductInventoryIDs.Count();
        }

        //public int SetZeroQuantityDispenseCount()
        //{
        //    if (ZeroQtyProductInventoryIDs.GetUpperBound(0) > -1)
        //    {
        //        return ZeroQtyProductInventoryIDs.Count();
        //    }
        //    return 0;
        //}

        public bool ItemsWereDispensed()
        {
            if (!(ZeroQtyProductInventoryIDs.GetUpperBound(0) > -1 && ZeroQtyProductInventoryIDs.Count() >= DispenseCount))
            {
                return true;
            }
            return false;
        }

        public void SetDispenseStatus(BregWcfService.DispenseStatus dispenseStatus)
        {
            if (dispenseStatus == BregWcfService.DispenseStatus.DispenseAttempted)
            {
                if (ItemsWereDispensed())
                {
                    DispenseStatus = BregWcfService.DispenseStatus.Dispensed;
                }
                else
                {
                    DispenseStatus = BregWcfService.DispenseStatus.NotDispensedZeroQuantity;
                }
            }
            else
            {
                if (DispenseStatus != BregWcfService.DispenseStatus.Dispensed)
                {
                    DispenseStatus = dispenseStatus;
                }
            }
        }

        public static Dispensement[] GetDispensedUnsigned(out int count, int practiceLocationID, int skip, int take)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var dispensed_items_without_signature = (from dd in visionDB.DispenseDetails
                                                         join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                                                         join s1 in visionDB.DispenseSignatures on dq.DispenseSignatureID equals s1.DispenseSignatureID into s2
                                                         from s in s2.DefaultIfEmpty()
                                                         join pi in visionDB.ProductInventories on dq.ProductInventoryID equals pi.ProductInventoryID
                                                         join pcp in visionDB.PracticeCatalogProducts on dd.PracticeCatalogProductID equals pcp.PracticeCatalogProductID
                                                         join pcsb in visionDB.PracticeCatalogSupplierBrands on pcp.PracticeCatalogSupplierBrandID equals pcsb.PracticeCatalogSupplierBrandID
                                                         join mcp in visionDB.MasterCatalogProducts on pcp.MasterCatalogProductID equals mcp.MasterCatalogProductID into mcps
                                                         from mcp in mcps.DefaultIfEmpty()
                                                         join tpp in visionDB.ThirdPartyProducts on pcp.ThirdPartyProductID equals tpp.ThirdPartyProductID into tpps
                                                         from tpp in tpps.DefaultIfEmpty()
                                                         where (dq.PracticeLocationID == practiceLocationID)
                                                            // NOTE(Henry): Logic requires interpretation that missing signature row is considered as signed for legacy compatibility
                                                            && ((s != null) && (s.PhysicianSigned == false))
                                                            && dq.DeclineReason == "Accepted"
                                                            && dd.Dispense.IsActive
                                                         select new
                                                         {
                                                             dd.Dispense.PracticeLocationID,
                                                             dd.Dispense.DispenseBatchID,
                                                             dd.Dispense.DateDispensed,
                                                             dd.Dispense.PatientCode,
                                                             dd.PhysicianID,
                                                             dq.DispenseSignatureID,
                                                             ABN_Needed = (from p in visionDB.ProductHCPCs
                                                                           join a in visionDB.ABN_HCPCs on p.HCPCS equals a.HCPCs
                                                                           where (a.ABN_Needed == true) && (p.PracticeCatalogProductID == pcp.PracticeCatalogProductID)
                                                                           select a).Any(a => a.ABN_Needed),
                                                             pcp.BillingCharge,
                                                             pcp.BillingChargeCash,
                                                             dq.ABNReason,
                                                             pcsb.BrandShortName,
                                                             Code = pcp.IsThirdPartyProduct ? tpp.Code : mcp.Code,
                                                             dq.DeclineReason,
                                                             dd.DMEDeposit,
                                                             pcsb.SupplierShortName,
                                                             Gender = pcp.IsThirdPartyProduct ? tpp.Gender : mcp.Gender,
                                                             HcpcString = dq.HCPCs ?? visionDB.ConcatHCPCS(pcp.PracticeCatalogProductID),
                                                             dq.ICD9_Code,
                                                             IsABNForm = dq.ABNForm,
                                                             dq.IsCashCollection,
                                                             IsDeleted = (dq.QuantityToDispense == 0) ? true : false,
                                                             dq.IsMedicare,
                                                             dq.IsRental,
                                                             dq.MedicareOption,
                                                             ProductName = pcp.IsThirdPartyProduct ? tpp.ShortName : mcp.ShortName,
                                                             QuantityOnHand = pi.QuantityOnHandPerSystem,
                                                             QuantityDispensed = dd.Quantity,
                                                             dq.Side,
                                                             Size = pcp.IsThirdPartyProduct ? tpp.Size : mcp.Size,
                                                             SupplierName = pcsb.SupplierShortName,
                                                             pcp.WholesaleCost,
                                                             dd.IsCustomFit
                                                         })
                                                        .ToList()
                                                        .GroupBy(x => new { x.PracticeLocationID, x.DispenseBatchID, x.DateDispensed, x.PatientCode, x.PhysicianID, x.DispenseSignatureID })
                                                        .Select(
                                                            x =>
                                                                new Dispensement
                                                                {
                                                                    PracticeLocationID = x.Key.PracticeLocationID,
                                                                    DispenseBatchID = x.Key.DispenseBatchID.GetValueOrDefault(),
                                                                    DispenseStatus = DispenseStatus.DispensedNeedSignature,
                                                                    DispenseDate = x.Key.DateDispensed.ToShortDateString(),
                                                                    PatientCode = x.Key.PatientCode,
                                                                    PhysicianID = x.Key.PhysicianID.GetValueOrDefault(),
                                                                    DispenseSignatureID = x.Key.DispenseSignatureID.GetValueOrDefault(),
                                                                    DispenseItems =
                                                                        x.Select(
                                                                            xx =>
                                                                                new DispenseItem
                                                                                {
                                                                                    ABN_Needed = xx.ABN_Needed,
                                                                                    BillingCharge = xx.BillingCharge,
                                                                                    BillingChargeCash = xx.BillingChargeCash,
                                                                                    ABNReason = xx.ABNReason,
                                                                                    BrandName = xx.BrandShortName,
                                                                                    Code = xx.Code,
                                                                                    DeclineReason = xx.DeclineReason,
                                                                                    DispenseDate = xx.DateDispensed,
                                                                                    DispenseStatus = DispenseStatus.DispensedNeedSignature,
                                                                                    DMEDeposit = xx.DMEDeposit.GetValueOrDefault(),
                                                                                    Fullname = xx.SupplierShortName,
                                                                                    Gender = xx.Gender,
                                                                                    HcpcString = xx.HcpcString,
                                                                                    ICD9_Code = xx.ICD9_Code,
                                                                                    IsABNForm = xx.IsABNForm,
                                                                                    IsCashCollection = xx.IsCashCollection,
                                                                                    IsDeleted = xx.IsDeleted,
                                                                                    IsMedicare = xx.IsMedicare,
                                                                                    IsRental = xx.IsRental,
                                                                                    MedicareOption = xx.MedicareOption,
                                                                                    PracticeLocationID = xx.PracticeLocationID,
                                                                                    ProductName = xx.ProductName,
                                                                                    QuantityOnHand = xx.QuantityOnHand,
                                                                                    QuantityDispensed = xx.QuantityDispensed.GetValueOrDefault(),
                                                                                    Side = xx.Side,
                                                                                    Size = xx.Size,
                                                                                    SupplierName = xx.SupplierName,
                                                                                    WholesaleCost = xx.WholesaleCost,
                                                                                    IsCustomFit = xx.IsCustomFit
                                                                                }
                                                                        )
                                                                        .ToArray()
                                                                }
                                                        )
                                                        .ToList();

                count = dispensed_items_without_signature.Count();
                return dispensed_items_without_signature.ToArray<Dispensement>();
            }
        }

        public static Dispensement GetDispensementById(int practiceLocationID, int dispenseID)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                #region retrieve dispensement
                var d = (from x in visionDB.Dispenses
                         where x.PracticeLocationID == practiceLocationID && x.DispenseID == dispenseID
                         select x)
                        .SingleOrDefault();

                if (d == null)
                    return null;
                #endregion

                #region retrieve items in dispensement
                var items = (from dd in visionDB.DispenseDetails
                             join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                             join s1 in visionDB.DispenseSignatures on dq.DispenseSignatureID equals s1.DispenseSignatureID into s2
                             from s in s2.DefaultIfEmpty()
                             join pi in visionDB.ProductInventories on dq.ProductInventoryID equals pi.ProductInventoryID
                             join pcp in visionDB.PracticeCatalogProducts on dd.PracticeCatalogProductID equals pcp.PracticeCatalogProductID
                             join pcsb in visionDB.PracticeCatalogSupplierBrands on pcp.PracticeCatalogSupplierBrandID equals pcsb.PracticeCatalogSupplierBrandID
                             join mcp in visionDB.MasterCatalogProducts on pcp.MasterCatalogProductID equals mcp.MasterCatalogProductID into mcps
                             from mcp in mcps.DefaultIfEmpty()
                             join tpp in visionDB.ThirdPartyProducts on pcp.ThirdPartyProductID equals tpp.ThirdPartyProductID into tpps
                             from tpp in tpps.DefaultIfEmpty()
                             where dq.PracticeLocationID == practiceLocationID && dd.DispenseID == dispenseID
                             select new
                             {
                                 PatientSignature = s != null ? s.PatientSignature : null,
                                 PatientSignedDate = s != null ? s.PatientSignedDate : null,
                                 dd.PhysicianID,
                                 PhysicianSigned = s != null ? s.PhysicianSigned : null,
                                 PhysicianSignedDate = s != null ? s.PhysicianSignedDate : null,
                                 ABN_Needed = (from p in visionDB.ProductHCPCs
                                               join a in visionDB.ABN_HCPCs on p.HCPCS equals a.HCPCs
                                               where (a.ABN_Needed == true) && (p.PracticeCatalogProductID == pcp.PracticeCatalogProductID)
                                               select a).Any(a => a.ABN_Needed),
                                 dq.ABNReason,
                                 dq.ABNType,
                                 pcp.BillingCharge,
                                 pcp.BillingChargeCash,
                                 pcsb.BrandShortName,
                                 Code = pcp.IsThirdPartyProduct ? tpp.Code : mcp.Code,
                                 dq.DeclineReason,
                                 DMEDeposit = dd.DMEDeposit.GetValueOrDefault(),
                                 pcsb.SupplierShortName,
                                 Gender = pcp.IsThirdPartyProduct ? tpp.Gender : mcp.Gender,
                                 HcpcString = dq.HCPCs ?? visionDB.ConcatHCPCS(pcp.PracticeCatalogProductID),
                                 dq.ICD9_Code,
                                 ProductHCPCs = pcp.ProductHCPCs.ToArray(),
                                 IsABNForm = dq.ABNForm,
                                 dq.IsCashCollection,
                                 IsDeleted = (dq.QuantityToDispense == 0) ? true : false,
                                 dq.IsMedicare,
                                 dq.IsRental,
                                 dq.MedicareOption,
                                 ProductName = pcp.IsThirdPartyProduct ? tpp.ShortName : mcp.ShortName,
                                 QuantityOnHand = pi.QuantityOnHandPerSystem,
                                 QuantityDispensed = dd.Quantity,
                                 dq.Side,
                                 Size = pcp.IsThirdPartyProduct ? tpp.Size : mcp.Size,
                                 SupplierName = pcsb.SupplierShortName,
                                 pcp.WholesaleCost,
                                 dd.IsCustomFit,
                                 dq.IsCustomFabricated,
                                 dq.BregVisionOrderID,
                                 pcp.IsThirdPartyProduct,
                                 dd.Mod1,
                                 dd.Mod2,
                                 dd.Mod3,
                                 dd.PracticeCatalogProductID,
                                 dq.ProductInventoryID,
                                 pcsb.PracticeCatalogSupplierBrandID,
                                 dq.FitterID,
                                 CustomFitProcedureResponses = (from cfp in visionDB.DispenseQueue_CFPs where cfp.DispenseQueueID == dq.DispenseQueueID select cfp).ToArray(),
                                 dq.DispenseSignatureID,
                                 s.PatientSignatureRelationshipId,
                                 dq.Note,
                                 dq.DispenseQueueID,
                                 dq.PreAuthorized
                             })
                             .ToList();
                #endregion

                #region calculate dispense status
                // NOTE: dispensed items are already dispensed; so, options are only whether or not a physician has signed
                var d_status = items.Any(x => x.PhysicianSigned ?? false) ? DispenseStatus.Dispensed : DispenseStatus.DispensedNeedSignature;
                #endregion

                #region assemble and return dispensement object
                var r =
                    new Dispensement
                    {
                        DispenseBatchID = d.DispenseBatchID ?? -1,
                        DispenseDate = d.DateDispensed.ToShortDateString(),
                        DispensementNote = d.Note,
                        DispenseSignatureID = (from i in items select (i.DispenseSignatureID.HasValue ? i.DispenseSignatureID.Value : -1)).FirstOrDefault(),
                        PatientSignatureRelationshipID = (from i in items select (i.PatientSignatureRelationshipId.HasValue ? i.PatientSignatureRelationshipId.Value : -1)).FirstOrDefault(),
                        PatientCode = d.PatientCode,
                        PatientFirstName = d.PatientFirstName,
                        PatientLastName = d.PatientLastName,
                        PatientSignature = (from i in items select (i.PatientSignature != null ? i.PatientSignature.ToArray() : new byte[0])).FirstOrDefault(),
                        PatientSignedDate = (from i in items select i.PatientSignedDate).FirstOrDefault(),
                        PhysicianID = (from i in items select i.PhysicianID ?? -1).FirstOrDefault(),
                        PhysicianSigned = (from i in items select i.PhysicianSigned ?? false).FirstOrDefault(),
                        PhysicianSignedDate = (from i in items select (i.PhysicianSignedDate.HasValue ? i.PhysicianSignedDate.Value.ToString(EmailTemplates.TemplateHelper.AppDateTimeFormat) : (string)null)).FirstOrDefault(),
                        PracticeLocationID = d.PracticeLocationID,
                        PracticePayerID = d.PracticePayerID,
                        DispenseItems =
                            items
                            .Select(
                                x =>
                                    new DispenseItem
                                    {
                                        ABNReason = x.ABNReason,
                                        ABNType = x.ABNType,
                                        ABN_Needed = x.ABN_Needed,
                                        BillingCharge = x.BillingCharge,
                                        BillingChargeCash = x.BillingChargeCash,
                                        BrandName = x.BrandShortName,
                                        BregVisionOrderID = x.BregVisionOrderID,
                                        Code = x.Code,
                                        CreatedDate = d.CreatedDate,
                                        CreatedUser = d.CreatedUserID,
                                        CustomFitProcedureResponses = x.CustomFitProcedureResponses.Select(a => new CustomFitProcedureResponse { CustomFitProcedureId = a.CustomFitProcedureID, UserInput = a.UserInput }).ToArray(),
                                        DeclineReason = x.DeclineReason,
                                        DispenseDate = d.DateDispensed,
                                        DispenseStatus = d_status,
                                        DispenseSignatureID = x.DispenseSignatureID.HasValue ? x.DispenseSignatureID.Value : -1,
                                        DispenseQueueID = x.DispenseQueueID,
                                        DMEDeposit = x.DMEDeposit,
                                        FitterID = x.FitterID,
                                        Gender = x.Gender,
                                        HcpcString = x.HcpcString,
                                        ICD9_Code = x.ICD9_Code,
                                        ICD9_List = DispenseItem.GetDispenseItemICD9s(practiceLocationID, x.ProductHCPCs.ToList()),
                                        IsABNForm = x.IsABNForm,
                                        IsCashCollection = x.IsCashCollection,
                                        IsCustomFabricated = x.IsCustomFabricated ?? false,
                                        IsCustomFit = x.IsCustomFit,
                                        IsMedicare = x.IsMedicare,
                                        IsRental = x.IsRental,
                                        IsThirdPartySupplier = x.IsThirdPartyProduct,
                                        MedicareOption = x.MedicareOption,
                                        Mod1 = x.Mod1,
                                        Mod2 = x.Mod2,
                                        Mod3 = x.Mod3,
                                        Note = d.Note,
                                        PatientCode = d.PatientCode,
                                        PatientEmail = d.PatientEmail,
                                        PatientFirstName = d.PatientFirstName,
                                        PatientLastName = d.PatientLastName,
                                        PhysicianID = x.PhysicianID ?? -1,
                                        PhysicianSigned = x.PhysicianSigned ?? false,
                                        PhysicianSignedDate = x.PhysicianSignedDate.HasValue ? x.PhysicianSignedDate.Value.ToString(EmailTemplates.TemplateHelper.AppDateTimeFormat) : null,
                                        PONumber = null,//TODO
                                        PracticeCatalogProductID = x.PracticeCatalogProductID,
                                        PracticeLocationID = d.PracticeLocationID,
                                        PracticePayerID = d.PracticePayerID,
                                        ProductInventoryID = x.ProductInventoryID,
                                        ProductName = x.ProductName,
                                        QuantityDispensed = x.QuantityDispensed ?? 0,
                                        QuantityOnHand = x.QuantityOnHand,
                                        Side = x.Side,
                                        Size = x.Size,
                                        SupplierID = x.PracticeCatalogSupplierBrandID,
                                        SupplierName = x.SupplierName,
                                        WholesaleCost = x.WholesaleCost,
                                        PreAuthorized = x.PreAuthorized
                                    }
                            )
                            .ToArray()
                    };

                return r;
                #endregion
            }
        }

        public static Dispensement GetQueuedDispensementByDispenseQueueIDs(int practiceLocationID, int[] dispenseQueueIDs)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                #region retrieve items in dispensement
                var items = (from dq in visionDB.DispenseQueues
                             join s1 in visionDB.DispenseSignatures on dq.DispenseSignatureID equals s1.DispenseSignatureID into s2
                             from s in s2.DefaultIfEmpty()
                             join pi in visionDB.ProductInventories on dq.ProductInventoryID equals pi.ProductInventoryID
                             join pcp in visionDB.PracticeCatalogProducts on pi.PracticeCatalogProductID equals pcp.PracticeCatalogProductID
                             join pcsb in visionDB.PracticeCatalogSupplierBrands on pcp.PracticeCatalogSupplierBrandID equals pcsb.PracticeCatalogSupplierBrandID
                             join mcp in visionDB.MasterCatalogProducts on pcp.MasterCatalogProductID equals mcp.MasterCatalogProductID into mcps
                             from mcp in mcps.DefaultIfEmpty()
                             join tpp in visionDB.ThirdPartyProducts on pcp.ThirdPartyProductID equals tpp.ThirdPartyProductID into tpps
                             from tpp in tpps.DefaultIfEmpty()
                             where dq.PracticeLocationID == practiceLocationID && dispenseQueueIDs.ToList().Contains(dq.DispenseQueueID)
                             select new
                             {
                                 PatientSignature = s != null ? s.PatientSignature : null,
                                 PatientSignedDate = s != null ? s.PatientSignedDate : null,
                                 dq.PhysicianID,
                                 PhysicianSigned = s != null ? s.PhysicianSigned : null,
                                 PhysicianSignedDate = s != null ? s.PhysicianSignedDate : null,
                                 ABN_Needed = (from p in visionDB.ProductHCPCs
                                               join a in visionDB.ABN_HCPCs on p.HCPCS equals a.HCPCs
                                               where (a.ABN_Needed == true) && (p.PracticeCatalogProductID == pcp.PracticeCatalogProductID)
                                               select a).Any(a => a.ABN_Needed),
                                 dq.ABNReason,
                                 dq.ABNType,
                                 pcp.BillingCharge,
                                 pcp.BillingChargeCash,
                                 pcsb.BrandShortName,
                                 Code = pcp.IsThirdPartyProduct ? tpp.Code : mcp.Code,
                                 dq.DeclineReason,
                                 DMEDeposit = dq.DMEDeposit,
                                 pcsb.SupplierShortName,
                                 Gender = pcp.IsThirdPartyProduct ? tpp.Gender : mcp.Gender,
                                 HcpcString = dq.HCPCs ?? visionDB.ConcatHCPCS(pcp.PracticeCatalogProductID),
                                 dq.ICD9_Code,
                                 ProductHCPCs = pcp.ProductHCPCs.ToArray(),
                                 IsABNForm = dq.ABNForm,
                                 dq.IsCashCollection,
                                 IsDeleted = (dq.QuantityToDispense == 0) ? true : false,
                                 dq.IsMedicare,
                                 dq.IsRental,
                                 dq.MedicareOption,
                                 ProductName = pcp.IsThirdPartyProduct ? tpp.ShortName : mcp.ShortName,
                                 QuantityOnHand = pi.QuantityOnHandPerSystem,
                                 QuantityDispensed = dq.QuantityToDispense,
                                 dq.Side,
                                 Size = pcp.IsThirdPartyProduct ? tpp.Size : mcp.Size,
                                 SupplierName = pcsb.SupplierShortName,
                                 pcp.WholesaleCost,
                                 dq.IsCustomFit,
                                 dq.IsCustomFabricated,
                                 dq.BregVisionOrderID,
                                 pcp.IsThirdPartyProduct,
                                 dq.Mod1,
                                 dq.Mod2,
                                 dq.Mod3,
                                 pcp.PracticeCatalogProductID,
                                 dq.ProductInventoryID,
                                 pcsb.PracticeCatalogSupplierBrandID,
                                 dq.FitterID,
                                 CustomFitProcedureResponses = (from cfp in visionDB.DispenseQueue_CFPs where cfp.DispenseQueueID == dq.DispenseQueueID select cfp).ToArray(),
                                 dq.DispenseDate,
                                 dq.Note,
                                 dq.PatientCode,
                                 dq.PatientFirstName,
                                 dq.PatientLastName,
                                 dq.PatientEmail,
                                 dq.PracticePayerID,
                                 dq.CreatedDate,
                                 CreatedUserID = dq.CreatedUser,
                                 dq.DispenseQueueID,
                                 dq.DispenseSignatureID,
                                 s.PatientSignatureRelationshipId,
                                 dq.PreAuthorized
                             })
                             .ToList();

                if (items.Count < 1)
                    return null;
                #endregion

                #region calculate dispense status
                var d_status = DispenseStatus.NotDispensed;
                #endregion

                #region assemble and return dispensement object
                var r =
                    new Dispensement
                    {
                        DispenseBatchID = -1, //no dispense batch ID because not dispensed yet
                        DispenseDate = (from i in items select (i.DispenseDate.HasValue ? i.DispenseDate.ToString() : null)).FirstOrDefault(),
                        DispensementNote = (from i in items select i.Note).FirstOrDefault(),
                        DispenseSignatureID = (from i in items select (i.DispenseSignatureID.HasValue ? i.DispenseSignatureID.Value : -1)).FirstOrDefault(),
                        PatientSignatureRelationshipID = (from i in items select (i.PatientSignatureRelationshipId.HasValue ? i.PatientSignatureRelationshipId.Value : -1)).FirstOrDefault(),
                        PatientCode = (from i in items select i.PatientCode).FirstOrDefault(),
                        PatientFirstName = (from i in items select i.PatientFirstName).FirstOrDefault(),
                        PatientLastName = (from i in items select i.PatientLastName).FirstOrDefault(),
                        PatientSignature = (from i in items select ((i.PatientSignature != null) ? i.PatientSignature.ToArray() : null)).FirstOrDefault(),
                        PatientSignedDate = (from i in items select i.PatientSignedDate).FirstOrDefault(),
                        PhysicianID = (from i in items select i.PhysicianID ?? -1).FirstOrDefault(),
                        PhysicianSigned = (from i in items select i.PhysicianSigned ?? false).FirstOrDefault(),
                        PhysicianSignedDate = (from i in items select (i.PhysicianSignedDate.HasValue ? i.PhysicianSignedDate.Value.ToString(EmailTemplates.TemplateHelper.AppDateTimeFormat) : (string)null)).FirstOrDefault(),
                        PracticeLocationID = practiceLocationID,
                        PracticePayerID = (from i in items select i.PracticePayerID).FirstOrDefault(),
                        DispenseItems =
                            items
                            .Select(
                                x =>
                                    new DispenseItem
                                    {
                                        ABNReason = x.ABNReason,
                                        ABNType = x.ABNType,
                                        ABN_Needed = x.ABN_Needed,
                                        BillingCharge = x.BillingCharge,
                                        BillingChargeCash = x.BillingChargeCash,
                                        BrandName = x.BrandShortName,
                                        BregVisionOrderID = x.BregVisionOrderID,
                                        Code = x.Code,
                                        CreatedDate = x.CreatedDate,
                                        CreatedUser = x.CreatedUserID,
                                        CustomFitProcedureResponses = x.CustomFitProcedureResponses.Select(a => new CustomFitProcedureResponse { CustomFitProcedureId = a.CustomFitProcedureID, UserInput = a.UserInput }).ToArray(),
                                        DeclineReason = x.DeclineReason,
                                        DispenseDate = x.DispenseDate,
                                        DispenseStatus = d_status,
                                        DispenseQueueID = x.DispenseQueueID,
                                        DispenseSignatureID = x.DispenseSignatureID.HasValue ? x.DispenseSignatureID.Value : -1,
                                        DMEDeposit = x.DMEDeposit,
                                        FitterID = x.FitterID,
                                        Gender = x.Gender,
                                        HcpcString = x.HcpcString,
                                        ICD9_Code = x.ICD9_Code,
                                        ICD9_List = DispenseItem.GetDispenseItemICD9s(practiceLocationID, x.ProductHCPCs.ToList()),
                                        IsABNForm = x.IsABNForm,
                                        IsCashCollection = x.IsCashCollection,
                                        IsCustomFabricated = x.IsCustomFabricated ?? false,
                                        IsCustomFit = x.IsCustomFit,
                                        IsMedicare = x.IsMedicare,
                                        IsRental = x.IsRental,
                                        IsThirdPartySupplier = x.IsThirdPartyProduct,
                                        MedicareOption = x.MedicareOption,
                                        Mod1 = x.Mod1,
                                        Mod2 = x.Mod2,
                                        Mod3 = x.Mod3,
                                        Note = x.Note,
                                        PatientCode = x.PatientCode,
                                        PatientEmail = x.PatientEmail,
                                        PatientFirstName = x.PatientFirstName,
                                        PatientLastName = x.PatientLastName,
                                        PhysicianID = x.PhysicianID ?? -1,
                                        PhysicianSigned = x.PhysicianSigned ?? false,
                                        PhysicianSignedDate = x.PhysicianSignedDate.HasValue ? x.PhysicianSignedDate.Value.ToString(EmailTemplates.TemplateHelper.AppDateTimeFormat) : null,
                                        PONumber = null,//TODO
                                        PracticeCatalogProductID = x.PracticeCatalogProductID,
                                        PracticeLocationID = practiceLocationID,
                                        PracticePayerID = x.PracticePayerID,
                                        ProductInventoryID = x.ProductInventoryID,
                                        ProductName = x.ProductName,
                                        QuantityDispensed = x.QuantityDispensed,
                                        QuantityOnHand = x.QuantityOnHand,
                                        Side = x.Side,
                                        Size = x.Size,
                                        SupplierID = x.PracticeCatalogSupplierBrandID,
                                        SupplierName = x.SupplierName,
                                        WholesaleCost = x.WholesaleCost,
                                        PreAuthorized = x.PreAuthorized
                                    }
                            )
                            .ToArray()
                    };

                return r;
                #endregion
            }
        }

        public static Dispensement[] GetDispensedUnsigned2(out int count, int practiceLocationID, int skip, int take)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var detailQuery = from dd in visionDB.DispenseDetails
                                  join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                                  join s1 in visionDB.DispenseSignatures on dq.DispenseSignatureID equals s1.DispenseSignatureID into s2
                                  from s in s2.DefaultIfEmpty()
                                  where (dq.PracticeLocationID == practiceLocationID)
                                    // NOTE(Henry): Logic requires interpretation that missing signature row is considered as signed for legacy compatibility
                                    && ((s != null) && (s.PhysicianSigned == false))
                                    && dq.DeclineReason == "Accepted"
                                  select dd;

                var dispenseDetail = detailQuery.OrderByDescending(a => a.DispenseDetailID);

                var query = (
                                from d in visionDB.Dispenses
                                where d.PracticeLocationID == practiceLocationID
                                select d
                            )
                            .Where(dispensement => dispensement.DispenseDetails.Any(dd => dispenseDetail.Contains(dd)));


                var dispensements = query.OrderByDescending(a => a.DispenseID).Skip(skip).Take(take);

                var convertedQuery = (from d in dispensements
                                      select new Dispensement
                                      {
                                          PracticeLocationID = d.PracticeLocationID,
                                          DispenseBatchID = d.DispenseBatchID.GetValueOrDefault(),
                                          DispenseStatus = DispenseStatus.DispensedNeedSignature,
                                          DispenseDate = d.DateDispensed.ToShortDateString(),
                                          PatientCode = d.PatientCode,
                                          PhysicianID = (
                                                                     (
                                                                         from dd in dispenseDetail
                                                                         join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                                                                         where dd.DispenseID == d.DispenseID
                                                                         select dd.PhysicianID
                                                                     )
                                                                     .FirstOrDefault()
                                                                     .GetValueOrDefault()
                                                                 ),
                                          DispenseSignatureID = (
                                                                     (
                                                                         from dd in dispenseDetail
                                                                         join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                                                                         where dd.DispenseID == d.DispenseID
                                                                         select dq.DispenseSignatureID
                                                                     )
                                                                     .FirstOrDefault()
                                                                     .GetValueOrDefault()
                                                                 ),
                                          DispenseItems = (
                                                              from dd in dispenseDetail
                                                              join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                                                              join pi in visionDB.ProductInventories on dq.ProductInventoryID equals pi.ProductInventoryID
                                                              join pcp in visionDB.PracticeCatalogProducts on dd.PracticeCatalogProductID equals pcp.PracticeCatalogProductID
                                                              join pcsb in visionDB.PracticeCatalogSupplierBrands on pcp.PracticeCatalogSupplierBrandID equals pcsb.PracticeCatalogSupplierBrandID
                                                              join mcp in visionDB.MasterCatalogProducts on pcp.MasterCatalogProductID equals mcp.MasterCatalogProductID into mastercatalog
                                                              from mc in mastercatalog.DefaultIfEmpty()
                                                              join tpp in visionDB.ThirdPartyProducts on pcp.ThirdPartyProductID equals tpp.ThirdPartyProductID into thirdparty
                                                              from tp in thirdparty.DefaultIfEmpty()
                                                              where dd.DispenseID == d.DispenseID
                                                              select new DispenseItem
                                                              {
                                                                  ABN_Needed = ((from p in visionDB.ProductHCPCs
                                                                                 join a in visionDB.ABN_HCPCs on p.HCPCS equals a.HCPCs
                                                                                 where a.ABN_Needed == true
                                                                                 && p.PracticeCatalogProductID == pcp.PracticeCatalogProductID
                                                                                 select (bool?)a.ABN_Needed).Take(1).First()) ?? false,
                                                                  BillingCharge = pcp.BillingCharge,
                                                                  BillingChargeCash = pcp.BillingChargeCash,
                                                                  ABNReason = dq.ABNReason,
                                                                  BrandName = pcsb.BrandShortName,
                                                                  Code = pcp.IsThirdPartyProduct == true ? tp.Code : mc.Code,
                                                                  DeclineReason = dq.DeclineReason,
                                                                  DispenseDate = d.DateDispensed,
                                                                  DispenseStatus = DispenseStatus.DispensedNeedSignature,
                                                                  DMEDeposit = dd.DMEDeposit.GetValueOrDefault(),
                                                                  Fullname = pcsb.SupplierShortName,
                                                                  Gender = pcp.IsThirdPartyProduct == true ? tp.Gender : mc.Gender,
                                                                  HcpcString = dq.HCPCs ?? visionDB.ConcatHCPCS(pcp.PracticeCatalogProductID),
                                                                  ICD9_Code = dq.ICD9_Code,
                                                                  IsABNForm = dq.ABNForm,
                                                                  IsCashCollection = dq.IsCashCollection,
                                                                  IsDeleted = dq.QuantityToDispense == 0 ? true : false,
                                                                  IsMedicare = dq.IsMedicare,
                                                                  IsRental = dq.IsRental,
                                                                  MedicareOption = dq.MedicareOption,
                                                                  PracticeLocationID = dq.PracticeLocationID,
                                                                  ProductName = pcp.IsThirdPartyProduct == true ? tp.ShortName : mc.ShortName,
                                                                  QuantityOnHand = pi.QuantityOnHandPerSystem,
                                                                  QuantityDispensed = dd.Quantity.GetValueOrDefault(),
                                                                  Side = dq.Side,
                                                                  Size = pcp.IsThirdPartyProduct == true ? tp.Size : mc.Size,
                                                                  SupplierName = pcsb.SupplierShortName,
                                                                  WholesaleCost = pcp.WholesaleCost
                                                              }
                                                          ).ToArray()
                                      }).ToList();

                count = convertedQuery.Count();
                return convertedQuery.ToArray<Dispensement>();
            }
        }


        public static DispenseItem[] GetDispenseQueue(out int count, int practiceLocationID, bool isDispenseQueueSortDescending, int skip, int take)
        {
            // get the entire dispense queue for this practice location
            List<DispenseQueueInterface> dispenseQueue = DAL.PracticeLocation.Dispensement.GetDispenseQueue(practiceLocationID, isDispenseQueueSortDescending);

            if (dispenseQueue.Count == 0)
            {
                count = 0;
                return new DispenseItem[]{};
            }

            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                // pull dispense signatures from database
                var signatures = from signature in visionDB.DispenseSignatures
                                 where dispenseQueue.Select(x => x.DispenseSignatureID).ToArray().Contains(signature.DispenseSignatureID)
                                 select new { signature.DispenseSignatureID, signature.PhysicianSigned, signature.PhysicianSignedDate };

                // pull the product gender specifications from the database
                var gender_details = from pcp in visionDB.PracticeCatalogProducts
                                     join mcp in visionDB.MasterCatalogProducts on pcp.MasterCatalogProductID equals mcp.MasterCatalogProductID into j1
                                     from submcp in j1.DefaultIfEmpty()
                                     join tpp in visionDB.ThirdPartyProducts on pcp.ThirdPartyProductID equals tpp.ThirdPartyProductID into j2
                                     from subtpp in j2.DefaultIfEmpty()
                                     where dispenseQueue.Select(x => x.PracticeCatalogProductID).ToArray().Contains(pcp.PracticeCatalogProductID)
                                     select new
                                     {
                                         pcp.PracticeCatalogProductID,
                                         Gender = pcp.IsThirdPartyProduct
                                                       ? ((subtpp == null) ? null : subtpp.Gender)
                                                       : ((submcp == null) ? null : submcp.Gender)
                                     };

                // combine dispense queue with signatures and gender information into a DispenseItem record
                var query = from dq in dispenseQueue
                            join signature in signatures.ToArray() on dq.DispenseSignatureID equals signature.DispenseSignatureID into j1
                            from subsignature in j1.DefaultIfEmpty()
                            join gender_detail in gender_details.ToArray() on dq.PracticeCatalogProductID equals gender_detail.PracticeCatalogProductID into j2
                            from subgender_detail in j2.DefaultIfEmpty()
                            select new DispenseItem // MWS , these mappings might be wrong.....
                            {
                                ABN_Needed = dq.ABN_Needed,
                                BillingCharge = dq.BillingCharge,
                                BillingChargeCash = dq.BillingCharge,
                                BrandName = dq.BrandShortName,
                                BregVisionOrderID = null,
                                Code = dq.Code,
                                CreatedDate = dq.CreatedDate ?? DateTime.Now,
                                _DispenseDate = dq.DispenseDate ?? DateTime.Now,
                                DispenseQueueID = dq.DispenseQueueID,
                                DMEDeposit = dq.DMEDeposit,
                                Fullname = dq.SupplierShortName,
                                Gender = (subgender_detail != null) ? subgender_detail.Gender : null,
                                HcpcString = dq.HCPCsString,
                                ICD9_Code = dq.ICD9Code,
                                IsABNForm = dq.ABNForm,
                                IsMedicare = dq.IsMedicare,
                                IsRental = dq.IsRental,
                                PatientCode = dq.PatientCode,
                                PatientEmail = dq.PatientEmail,
                                PhysicianID = dq.PhysicianID ?? 0,
                                PracticeCatalogProductID = dq.PracticeCatalogProductID,
                                PracticeLocationID = dq.PracticeLocationID,
                                ProductInventoryID = dq.ProductInventoryID,
                                ProductName = dq.ShortName,
                                QuantityDispensed = dq.QuantityToDispense,
                                QuantityOnHand = dq.QuantityOnHandPerSystem,
                                Side = dq.Side,
                                Size = dq.Size,
                                SupplierName = dq.SupplierShortName,
                                WholesaleCost = dq.WholesaleCost,
                                DeclineReason = dq.DeclineReason,
                                MedicareOption = dq.MedicareOption,
                                PhysicianSigned = (subsignature != null) ? subsignature.PhysicianSigned.GetValueOrDefault() : false,
                                PhysicianSignedDate = (subsignature != null) ? subsignature.PhysicianSignedDate.GetValueOrDefault().ToString(EmailTemplates.TemplateHelper.AppDateTimeFormat) : string.Empty,
                                PatientFirstName = dq.PatientFirstName,
                                PatientLastName = dq.PatientLastName,
                                IsCustomFit = dq.IsCustomFit,
                                FitterID = dq.FitterID,
                                Note = dq.Note,
                                PracticePayerID = dq.PracticePayorID
                            };
  
                // run query and store values
                var dispenseItems = query.ToList();

                // update count for caller
                count = dispenseItems.Count;

                // return the requested items (following skip/take parameters)
                return dispenseItems.Skip(skip).Take(take).ToArray();
            }
        }

        public bool DispenseOrQueueProducts(string userName, string password, int practiceID, int practiceLocationID, MemoryStream pdfMemoryStream, string deviceIdentifier, string dispenseIdentifier, bool IsV5Dispensement)
        {
            bool _isBregBillingEnabled = DAL.Practice.Practice.IsBregBillingEnabled(practiceLocationID);
            bool _isStatusDispensed = false;
            List<int> productInventoryIds = new List<int>();
            // If the dispensement indicated by the combination of the deviceIdentifier and the dispenseIdentifier already exists, then we've done this before and we're finished
            if (DAL.PracticeLocation.Dispensement.IsDispensementExist(deviceIdentifier, dispenseIdentifier))
                return false;

            if (this.DispenseStatus != BregWcfService.DispenseStatus.Dispensed && this.DispenseStatus != BregWcfService.DispenseStatus.DispensedNeedSignature)
            {
                byte[] CompressedPDF;
                List<int> selectedProductInventoryIDs;
                int DispenseId;
                _isStatusDispensed = DispenseOrQueueProducts(out selectedProductInventoryIDs, userName, practiceID, practiceLocationID, pdfMemoryStream, deviceIdentifier, dispenseIdentifier, IsV5Dispensement, out DispenseId,out CompressedPDF);
                DispenseID = DispenseId;
                productInventoryIds = selectedProductInventoryIDs;
                if (selectedProductInventoryIDs!= null && selectedProductInventoryIDs.Count > 0)
                {
                    if (_isBregBillingEnabled && _isStatusDispensed)
                        SubmitToBcs(this, CompressedPDF, userName, password, selectedProductInventoryIDs);
                }
                return _isStatusDispensed;
            }
            else if (this.DispenseStatus == BregWcfService.DispenseStatus.DispensedNeedSignature)
            {
                byte[] CompressedPDF;
                var updateSignatureId = this.DispenseSignatureID < 1 ;
                var dispenseSignatureID = SaveDispenseSignature(userName, pdfMemoryStream,out CompressedPDF, practiceLocationID: practiceLocationID);
                //Only update Dispense Queue with Signature ID if new record is created when a user is back signing queue that need a signature 3/22/2016
                if (updateSignatureId)
                    UpdateDispenseSignature(dispenseSignatureID);
                this.DispenseStatus = BregWcfService.DispenseStatus.Dispensed;
                    if (_isBregBillingEnabled)
                        SubmitToBcs(this, CompressedPDF, userName, password);
                return true;
            }
            return false;
        }

        private void SubmitToBcs(Dispensement dispensement, byte[] pdfbyteArray, string userName, string password, List<int> selectedProductInventoryIDs = null)
        {
            var correlationId = Guid.NewGuid();
            try
            {
                if (dispensement!= null)
                {
                    if ((dispensement.PatientSignature != null && dispensement.PatientSignature.Length > 0))
                    {
                        var claimSuibmissionModel = BuildClaimSubmissionModel(dispensement, selectedProductInventoryIDs, pdfbyteArray, userName, password);
                                                
                        //Call to the Helper class
                        new BcsIntegrationHelper().PostDataToVisionIntegrationQueue(claimSuibmissionModel, correlationId);
                    } 
                }
            }
            catch (Exception ex)
            {
                logger.WriteLog("Exception in SubmitToBcs(): coud not submit to Bcs", ex, projectTitle, correlationId);
            }
        } 

        private static BcsClaimSubmission BuildClaimSubmissionModel(Dispensement dispensement, List<int> selectedProductInventoryIds, byte[] pdfByteArray, string username, string password)
        {
            List<string> productCodes = new List<string>();
            List<string> icd9Codes = new List<string>();
            var practice = DAL.Practice.Practice.GetPracticeByPracticeLocationId(dispensement.PracticeLocationID);

            var claimObj = new BcsClaimSubmission
            {
                Patient_FName = dispensement.PatientFirstName,
                Patient_LName = dispensement.PatientLastName,
                Patient_DOB = Convert.ToDateTime(dispensement.PatientCode),
                DateOfService = Convert.ToDateTime(dispensement.DispenseDate),
                Patient_SelfPay = true,
                SourceEntity = dispensement.DispenseID, // ToDo: SourceEntity should be an enum

                SourceEntityID = 1, // 1 resembles dispense ID in the entity lookup table in integrations DB. Need to change it later
                RMQID = Guid.NewGuid(),
               
                PhysicianSigned = dispensement.PhysicianSigned,
                PatientSignedDate = Convert.ToDateTime(dispensement.PatientSignedDate),
                BcsSubInventoryId = (practice!= null)? practice.BCSSubInventoryID : string.Empty, 

                DocumentSource = dispensement.DocumentSource, 
                DocumentType = dispensement.DocumentType,
                DocumentIDs = dispensement.DocumentIds
            };

            if (!string.IsNullOrEmpty(dispensement.PhysicianSignedDate))
                claimObj.PhysicianSignedDate = Convert.ToDateTime(dispensement.PhysicianSignedDate);
            else
                claimObj.PhysicianSignedDate = null;

            if(!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
            {
                claimObj.UserName = username;
                claimObj.Password = password;
            }

            if (selectedProductInventoryIds!=null && selectedProductInventoryIds.Count>0)
            {
                //Getting product bar codes
                foreach (var item in selectedProductInventoryIds)
                {
                    string productCode = GetProductCode(Convert.ToInt32(item.ToString()));

                    if (!productCodes.Contains(productCode))
                        productCodes.Add(productCode);
                }

                if (productCodes != null && productCodes.Count > 0)
                    claimObj.ProductBarCodes = productCodes; 
            }
            else
                claimObj.ProductBarCodes = productCodes; //this is for Bcs claim xml null handling 

            if (dispensement.DispenseItems!=null && dispensement.DispenseItems.Length>0)
            {
                //Getting ICD9 codes
                foreach (var item in dispensement.DispenseItems)
                {
                    if (!icd9Codes.Contains(item.ICD9_Code))
                        icd9Codes.Add(item.ICD9_Code);

                    // Getting the NPI Code for the physician
                    string NPICode = GetNPICode(item.PhysicianID);
                    claimObj.PhysicianNPICode = !string.IsNullOrEmpty(NPICode) ? NPICode : string.Empty;
                }

                if (icd9Codes != null && icd9Codes.Count > 0)
                    claimObj.ICD9Codes = icd9Codes; 
            }

            //Getting patient signature details
            if (dispensement.DispenseSignatureID > 0)
            {
                var dispensementSignature = GetDispensementSignatureDetail(dispensement.DispenseSignatureID);

                if (dispensementSignature != null)
                {
                    claimObj.PatientSignedRelationship = dispensementSignature.PatientSignedRelationship;
                    claimObj.PatientSigned = dispensementSignature.PatientSignature;
                }

                var physicianDetail = GetPhysicianDetail(dispensement.DispenseSignatureID);
                if (physicianDetail != null)
                {
                    claimObj.PhysicianFirstName = physicianDetail.PhysicianFirstName;
                    claimObj.PhysicianLastName = physicianDetail.PhysicianLastName;
                }
            }

            if (pdfByteArray!=null && pdfByteArray.Length > 0)
                claimObj.CompressedPdf = pdfByteArray;

            if (claimObj != null)
                return claimObj;
            else
                return null;
        }

        private static string GetNPICode(int PhysicianId)
        {
            using (var db = VisionDataContext.GetVisionDataContext())
            {
                return db.Clinicians.SingleOrDefault(physician => physician.ClinicianID == PhysicianId).NPI;
            }
        }
        private static string GetProductCode(int productInventoryId)
        {
            using (var db = VisionDataContext.GetVisionDataContext())
            {
                return (from masterCatalog in db.MasterCatalogProducts
                        join practiceCatalog in db.PracticeCatalogProducts on masterCatalog.MasterCatalogProductID equals practiceCatalog.MasterCatalogProductID
                        join productInventory in db.ProductInventories on practiceCatalog.PracticeCatalogProductID equals productInventory.PracticeCatalogProductID
                        where productInventory.ProductInventoryID == productInventoryId
                        select masterCatalog.Code).FirstOrDefault();
            }
        }

        private static dynamic GetPhysicianDetail(int dispenseSignatureId)
        {
            using (var db = VisionDataContext.GetVisionDataContext())
            {
                return (from contact in db.Contacts
                        join clinician in db.Clinicians on contact.ContactID equals clinician.ContactID
                        join dispenseQueue in db.DispenseQueues on clinician.ClinicianID equals dispenseQueue.PhysicianID
                        where dispenseQueue.DispenseSignatureID == dispenseSignatureId
                        select new
                        {
                            PhysicianFirstName = contact.FirstName,
                            PhysicianLastName = contact.LastName
                        }).FirstOrDefault();
            }
        }

        private static dynamic GetDispensementSignatureDetail(int dispenseSignatureId)
        {
            using (var db = VisionDataContext.GetVisionDataContext())
            {
                return (from dispenseSignature in db.DispenseSignatures                      
                        where dispenseSignature.DispenseSignatureID == dispenseSignatureId
                        select new
                        {
                            PatientSignature = dispenseSignature.PatientSignedDate == null ? false : true,                          
                            PatientSignedRelationship = dispenseSignature.PatientSignatureRelationshipId == null ? null : db.PatientSignatureRelationships.FirstOrDefault(c => c.Id == dispenseSignature.PatientSignatureRelationshipId).PatientSignatureRelationship1

                        }).FirstOrDefault();
            }
        }

        private void UpdateDispenseSignature(int dispenseSignatureId)
        {
            VisionDataContext visionDB = null;
            visionDB = VisionDataContext.GetVisionDataContext();
            var queueIDs = new List<int>();
            foreach (BregWcfService.DispenseItem dispenseItem in this.DispenseItems)
            {
                if (dispenseItem.DispenseQueueID.HasValue)
                    queueIDs.Add(dispenseItem.DispenseQueueID.Value);
            }

            using (visionDB)
            {
                var query = from dq in visionDB.DispenseQueues
                    where queueIDs.Contains(dq.DispenseQueueID) && !dq.DispenseSignatureID.HasValue
                    select dq;

                foreach (DispenseQueue dq in query)
                {
                    dq.DispenseSignatureID = dispenseSignatureId;
                }

                try
                {
                    visionDB.SubmitChanges();
                }
                catch(Exception ex)
                {
                    throw new Exception("Can't update dispensequeue: " + ex.Message);
                }

                //visionDB.DispenseQueues
                //.Where(x => queueIDs.Contains(x.DispenseQueueID))
                //.Where(b => b.DispenseSignatureID != dispenseSignatureId)
                //.ToList()
                //.ForEach(a => a.DispenseSignatureID = dispenseSignatureId);

                //visionDB.SubmitChanges();
            }
        }

        public bool DispenseOrQueueProducts(out List<int> selectedProductInventoryIDs, string userName, int practiceID, int practiceLocationID, MemoryStream pdfMemoryStream, string deviceIdentifier, string dispenseIdentifier, bool IsV5Dispensement, out int DispenseId,out byte[] CompressedPDF)
        {
            DispenseId = 0;
            CompressedPDF = null;
            // If the dispensement indicated by the combination of the deviceIdentifier and the dispenseIdentifier already exists, then we've done this before and we're finished
            bool alreadyDispensed = DAL.PracticeLocation.Dispensement.IsDispensementExist(deviceIdentifier, dispenseIdentifier);
            if (alreadyDispensed)
            {
                selectedProductInventoryIDs = null;
                return false;
            }

            bool customBraceOrdersOk = false;
            List<int> dispensedProducts;
            //var zeroQtyProductInventoryIDs = (List<int>)null; //Set the ItemDispenseStatus to not Dispensed.  this is a list of piIDs that are saved but not dispensed because qtyOnHand < QuantityToDispense and the user is not allowed zero qty dispensing
            var selectedDispenseQueueIDs = (List<int>)null; // each dispense item Id is put in this list if it is actually saved and it was selected in the list

            DispenseSignatureID = SaveDispenseSignature(userName,pdfMemoryStream, out CompressedPDF, practiceLocationID);

            selectedProductInventoryIDs = SaveDispenseQueue(userName, practiceID, practiceLocationID, deviceIdentifier, dispenseIdentifier, out selectedDispenseQueueIDs);

            if (SaveToQueueOnly == false)
            {
                dispensedProducts = new List<int>(); // this is a list, each item containing the QuantityDispensed on that dispensement. This can be replaced by summing DispenseItem[].QuantityDispensed
                


                customBraceOrdersOk = DAL.PracticeLocation.Dispensement.AreCustomBracesInDispensementCompletlySelected(practiceLocationID, selectedDispenseQueueIDs);
                if (customBraceOrdersOk == true)
                {
                    this.DispenseBatchID = DAL.PracticeLocation.Dispensement.DispenseProducts(practiceLocationID, selectedProductInventoryIDs, dispensedProducts, selectedDispenseQueueIDs, IsV5Dispensement, out DispenseId, createdByHandheld: true);
                    this.DispenseCount = dispensedProducts.Sum();
                    this.SetProdductInventoryIDs();
                    this.SetDispenseStatus(BregWcfService.DispenseStatus.DispenseAttempted);
                    if (selectedProductInventoryIDs.Count() > 0 && DispenseStatus == BregWcfService.DispenseStatus.Dispensed)
                    {
                        // Send Patient email here
                        BregWcfService.Classes.Email.SendPatientEmail(this.DispenseBatchID, practiceLocationID, pdfMemoryStream,out CompressedPDF);

                        BregWcfService.Classes.Email.SendBillingEmail(this.DispenseBatchID, practiceLocationID);
                    }

                }
                else
                {
                    this.SetDispenseStatus(BregWcfService.DispenseStatus.NotDispensed);
                }
            }

            return customBraceOrdersOk;
        }

        //mws - need to split out the ui specific stuff from this method
        private List<int> SaveDispenseQueue(string userName, int practiceID, int practiceLocationID, string deviceIdentifier, string dispenseIdentifier, out List<int> selectedDispenseQueueIDs)
        {
            var selectedProductInventoryIDs = new List<int>();

            SaveDispenseQueue(userName, practiceID, practiceLocationID, deviceIdentifier, dispenseIdentifier, out selectedDispenseQueueIDs, out selectedProductInventoryIDs);

            return selectedProductInventoryIDs;
        }

        public void SaveDispenseQueue(string userName, int practiceID, int practiceLocationID, string deviceIdentifier, string dispenseIdentifier, out List<int> selectedDispenseQueueIDs, out List<int> selectedProductInventoryIDs)
        {
            selectedProductInventoryIDs = new List<int>();
            List<int> zeroQtyProductInventoryIDs = new List<int>();
            selectedDispenseQueueIDs = new List<int>();

            bool firstItem = true;
            foreach (BregWcfService.DispenseItem dispenseItem in this.DispenseItems)
            {
                
                List<Tuple<int, string>> responseTuples = null;
                if (dispenseItem.CustomFitProcedureResponses != null)
                {
                    responseTuples = new List<Tuple<int, string>>();
                    foreach (var customFitProcedureResponse in dispenseItem.CustomFitProcedureResponses)
                    {
                        responseTuples.Add(new Tuple<int, string>(customFitProcedureResponse.CustomFitProcedureId,
                            customFitProcedureResponse.UserInput));
                    }
                }
                //ofter this call, dispenseItem.DispenseQueueID will be set



                SaveDispenseQueue(practiceID,
                            practiceLocationID,
                            dispenseItem,
                            selectedDispenseQueueIDs,
                            selectedProductInventoryIDs,
                            dispenseItem.DispenseQueueID ?? 0,
                            dispenseItem.ProductInventoryID,
                            dispenseItem.QuantityOnHand,
                            dispenseItem.QuantityDispensed,
                            dispenseItem.IsMedicare,
                            dispenseItem.IsABNForm,
                            dispenseItem.BregVisionOrderID,
                            dispenseItem.PatientCode,
                            dispenseItem.PatientEmail,
                            dispenseItem.PracticeCatalogProductID,
                            dispenseItem.PhysicianID,
                            dispenseItem.Side,
                            dispenseItem.IsCashCollection,
                            dispenseItem.DMEDeposit,
                            dispenseItem.ABNType,
                            dispenseItem.DispenseDate,
                            dispenseItem.ICD9_Code,
                            dispenseItem.HcpcString,
                            dispenseItem.IsSelected,
                            dispenseItem.ABNReason,
                            dispenseItem.IsRental,
                            dispenseItem.DeclineReason,
                            dispenseItem.MedicareOption,
                            this.DispenseSignatureID,
                            dispenseItem.IsDeleted,
                            this.PatientFirstName??dispenseItem.PatientFirstName,
                            this.PatientLastName??dispenseItem.PatientLastName,
                            this.DispensementNote,
                            this.PracticePayerID,
                            dispenseItem.IsCustomFit,
                            responseTuples, 
                            dispenseItem.IsCustomFabricated,
                            dispenseItem.FitterID,
                            firstItem ? deviceIdentifier : null,
                            firstItem ? dispenseIdentifier : null,
                            dispenseItem.Mod1,
                            dispenseItem.Mod2,
                            dispenseItem.Mod3,
                            queuedReason: this.QueuedReason,
                            preAuthorized: dispenseItem.PreAuthorized, 
                            documentIds: this.DocumentIds
                            );
                firstItem = false;

            }

            this.ZeroQtyProductInventoryIDs = zeroQtyProductInventoryIDs.ToArray<int>();
        }

        private int SaveDispenseSignature(string userName, MemoryStream pdfMemoryStream, out byte[] CompressedPDF, int practiceLocationID = 0)
        {
            CompressedPDF = null;
            if (this.PatientSignature != null || !string.IsNullOrEmpty(this.PhysicianSignedDate))
            {
                UserContext userContext = null;
                int contextUserId = 1;

                try
                {
                    userContext = UserContext.Login(userName);
                    if (userContext != null && userContext.User != null)
                    {
                        contextUserId = userContext.User.UserID;
                    }

                    //membershipUserID = (Guid)Membership.GetUser(userName).ProviderUserKey;
                }
                catch (Exception ex)
                {

                }

                VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

                using (visionDB)
                {
                    DispenseSignature dispenseSignature = null;
                    bool is_new_signature = false;
                    if (this.DispenseSignatureID > 0)
                    {
                        var existingDsQuery = from ds in visionDB.DispenseSignatures
                                              where ds.DispenseSignatureID == this.DispenseSignatureID
                                              select ds;
                        dispenseSignature = existingDsQuery.FirstOrDefault();
                    }
                    else
                    {
                        is_new_signature = true;
                        dispenseSignature = new DispenseSignature();
                    }

                    // enforce write-once for physician signature
                    // allow write if physician signed is null and sign date is null
                    // allow write if physician signed is false and sign date is null
                    {
                        bool is_physician_signed = ((dispenseSignature.PhysicianSigned.HasValue == true) && (dispenseSignature.PhysicianSigned.Value == true));
                        bool has_physician_sign_date = (dispenseSignature.PhysicianSignedDate.HasValue == true);
                        
                        // physician signature/date must not have been previously stored as "signed"
                        if (!is_physician_signed && !has_physician_sign_date)
                        {
                            // set signed
                            dispenseSignature.PhysicianSigned = this.PhysicianSigned;

                            if (this.PhysicianSigned == true)
                            {
                                // try to parse the provided signature date or use "today"
                                // NOTE: DateTime.TryParse without culture and style info returns DateTimeKind.Local
                                DateTime physicianSignedDate;
                                if (DateTime.TryParse(this.PhysicianSignedDate, out physicianSignedDate))
                                    dispenseSignature.PhysicianSignedDate = physicianSignedDate;
                                else
                                    dispenseSignature.PhysicianSignedDate = DateTime.Now;
                            }
                            else
                                dispenseSignature.PhysicianSignedDate = null;
                        }
                    }

                    // enforce write-once for patient signature
                    {
                        if ((dispenseSignature.PatientSignature == null)
                            && (dispenseSignature.PatientSignedDate.HasValue == false))
                        {
                            // must have both patient signature date and signature
                            if ((this.PatientSignedDate.HasValue == true)
                                && (this.PatientSignature != null)
                                && (this.PatientSignature.GetLength(0) > 0))
                            {
                                dispenseSignature.PatientSignedDate = this.PatientSignedDate.Value;
                                dispenseSignature.PatientSignature = this.PatientSignature;
                                dispenseSignature.PatientSignatureRelationshipId = this.PatientSignatureRelationshipID;
                            }
                        }
                    }

                    // update modification date
                    dispenseSignature.ModifiedUserID = contextUserId;
                    dispenseSignature.ModifiedDate = DateTime.Now;

                    // handle new-signature values and record insertion
                    if (is_new_signature == true)
                    {
                        dispenseSignature.CreatedUserID = contextUserId;
                        dispenseSignature.CreatedDate = DateTime.Now;
                        dispenseSignature.IsActive = true;
                        visionDB.DispenseSignatures.InsertOnSubmit(dispenseSignature);
                    }

                    // commit changes to the database
                    visionDB.SubmitChanges();

                    {
                        // get the dispense ids associate with this dispense signature and send patient e-mails
                        var x_dispense_objs =
                            visionDB.DispenseDetails
                            .Join(
                                visionDB.DispenseQueues,
                                outer => outer.DispenseQueueID,
                                inner => inner.DispenseQueueID,
                                (outer, inner) => new { outer.DispenseID, inner.DispenseQueueID, inner.DispenseSignatureID, inner.PracticeLocationID }
                                    )
                            .Join(
                                visionDB.Dispenses,
                                outer => outer.DispenseID,
                                inner => inner.DispenseID,
                                (outer, inner) => new { outer.DispenseID, outer.DispenseSignatureID, outer.PracticeLocationID }
                                    )
                            .Where(x => x.DispenseSignatureID == this.DispenseSignatureID)
                            .Select(x => new { x.DispenseID, x.PracticeLocationID })
                            .GroupBy(x => x.PracticeLocationID)
                            ;

                        foreach (var grp in x_dispense_objs)
                        {
                            BregWcfService.Classes.Email.SendPatientEmail(grp.Select(x => x.DispenseID).ToArray(), grp.Key, pdfMemoryStream, out CompressedPDF, fromVision: false, createPDF: true, sendEmail: true);
                            //BregWcfService.Classes.Email.SendPatientEmail(this.DispenseBatchID, practiceLocationID);
                        }
                    }

                    return dispenseSignature.DispenseSignatureID;
                } 
            }
            return 0;
        }

        private static void SaveDispenseQueue(int practiceID,
                                                int practiceLocationID,
                                                BregWcfService.DispenseItem dispenseItem,
                                                List<int> selectedDispenseQueueIDs,
                                                List<int> selectedProductInventoryIDs,
                                                int dispenseQueueID,
                                                int ProductInventoryID,
                                                int quantityOnHand,
                                                int Quantity,
                                                bool isMedicare,
                                                bool isABNForm,
                                                int? bregVisionOrderID,
                                                string txtPatientCode,
                                                string patientEmail,
                                                int PracticeCatalogProductID,
                                                int PhysicianID,
                                                string side,
                                                bool IsCashCollection,
                                                decimal DMEDeposit,
                                                string abntype,
                                                DateTime? DispenseDate,
                                                string icd9Code,
                                                string hcpcsString,
                                                bool selected,
                                                string abnReason,
                                                bool isRental,
                                                string declineReason,
                                                int medicareOption,
                                                int dispenseSignatureID,
                                                bool isDeleted,
                                                string patientFirstName,
                                                string patientLastName,
                                                string dispenseNote,
                                                int? practicePayerID,
                                                bool? isCustomFit,
                                                List<Tuple<int, string>> customFitProcedureResponses,
                                                bool isCustomFabricated,
                                                int? fitterId,
                                                string deviceIdentifier,
                                                string dispenseIdentifier,
                                                string mod1,
                                                string mod2,
                                                string mod3,
                                                string queuedReason,
                                                bool preAuthorized, 
                                                string[] documentIds
                                                )
        {


            #region save any new ICD9 codes
            SaveNewICD9(icd9Code, hcpcsString, practiceID, practiceLocationID);
            #endregion

            #region save changes to dispense queue
            var savedDispenseQueueID = 0;
            if (ProductInventoryID > 0)
            {
                savedDispenseQueueID =
                    DAL.PracticeLocation.Dispensement.AddUpdateDispensement
                    (
                            DispenseQueueID: dispenseQueueID,
                            PracticeLocationID: practiceLocationID,
                            InventoryID: ProductInventoryID,
                            PatientCode: txtPatientCode,
                            PhysicianID: PhysicianID,
                            ICD9_Code: icd9Code,
                            IsMedicare: isMedicare,
                            ABNForm: isABNForm,
                            IsCashCollection: IsCashCollection,
                            Side: side,
                            AbnType: abntype,
                            DMEDeposit: DMEDeposit,                         
                            BregVisionOrderID: bregVisionOrderID,
                            Quantity: Quantity,
                            isCustomFit: isCustomFit,
                            DispenseDate: DispenseDate.Value.TimeOfDay.Equals(new TimeSpan())? DispenseDate : DispenseDate.Value.ToLocalTime(),
                            mod1: mod1,
                            mod2: mod2,
                            mod3: mod3,
                            CreatedUser: 1,
                            CreatedDate: DateTime.Now,
                            QueuedReason: queuedReason,
                            abnReason: abnReason,
                            patientEmail: patientEmail,
                            isRental: isRental,
                            declineReason: declineReason,
                            medicareOption: medicareOption,
                            dispenseSignatureID: dispenseSignatureID,
                            isDeleted: isDeleted,
                            patientFirstName: patientFirstName,
                            patientLastName: patientLastName,
                            note: dispenseNote,
                            payerID: practicePayerID,                          
                            customFitProcedureResponses: customFitProcedureResponses,                         
                            FitterID:fitterId,
                            isCustomFabricated: isCustomFabricated,
                            deviceIdentifier: deviceIdentifier,
                            dispenseIdentifier: dispenseIdentifier,
                            preAuthorized: preAuthorized,
                            IsCalledFromV1Endpoint:false, 
                            documentIds: documentIds
                    );
            }
            #endregion

            #region capture product inventory IDs
            if (selected)
            {
//                if ((quantityOnHand < Quantity) && (allowZeroQuantityDispense == false))
//                {
//                    VISFOURONE-69 Enable email alert for 0 quantity dispensing	
//                }
//                else
//                {
                    selectedProductInventoryIDs.Add(ProductInventoryID); //set the dispenseItem to Saved here
                    dispenseItem.DispenseStatus = BregWcfService.DispenseStatus.Saved;
//                }
            }
            #endregion

            #region handle zero-qty dispense condition / capture list of selected dispense queue IDs for possible dispensement
            if (selected && (savedDispenseQueueID > 0))
            {
//                if ((quantityOnHand < Quantity) && (allowZeroQuantityDispense == false))
//                {
//                    VISFOURONE-69 Enable email alert for 0 quantity dispensing	
//                }
//                else
//                {
                    // qty OK or user is allowed zero-qty dispense, add to selected dispense queue IDs list
                    selectedDispenseQueueIDs.Add(savedDispenseQueueID); // set DispenseItem.DispenseQueueID here.
                    dispenseItem.DispenseQueueID = savedDispenseQueueID;
//                }
            }
            #endregion
        }

        public static ICD9[] GetDispenseQueueICD9s(int practiceLocationID)
        {
            IEnumerable<ICD9> iCD9_Codes = DAL.PracticeLocation.Dispensement.GetDispenseQueueICD9s(practiceLocationID);
            return iCD9_Codes.ToArray<ICD9>();
        }

        public static IEnumerable<ICD9> SaveNewICD9(string UserICD9Code, string HCPCsString, int practiceID, int practiceLocationID)
        {
            IEnumerable<ICD9> iCD9_Codes = null;
            //Load the IDC9's 
            iCD9_Codes = DAL.PracticeLocation.Dispensement.GetDispenseQueueICD9s(practiceLocationID);

            if (!(string.IsNullOrEmpty(UserICD9Code) || string.IsNullOrEmpty(UserICD9Code.Trim()) || string.IsNullOrEmpty(HCPCsString) || string.IsNullOrEmpty(HCPCsString.Trim())))
            {

                //parse the HCPCsString from a comma separated string to an array usable by LINQ
                var hcpcsArray = HCPCsString.Replace(" ", "").Split(',');

                //Find the the ICD9 items that match these HCPCs
                var hcpcsForThisRow = from icd in iCD9_Codes
                                      where hcpcsArray.Contains(icd.HCPCs)
                                      select icd;

                //Se if the user ICD9 Code(can be picked from the list Items in the comboBox or typed in) is in the list
                var icd9Exists = from existing_icd9 in hcpcsForThisRow
                                 where existing_icd9.ICD9Code == UserICD9Code
                                 select existing_icd9;
                if (icd9Exists.Count() < 1)
                {
                    //user typed a new ICD9 code that wasn't in the dropdown
                    foreach (var hcpc in hcpcsArray)
                    {
                        //Add the ICD9 the user typed in to both the ICD9 and Practice_ICD9 tables
                        if (string.IsNullOrEmpty(hcpc.Trim()) == false && string.IsNullOrEmpty(UserICD9Code.Trim()) == false)
                        {
                            var icd9ID = DAL.PracticeLocation.Dispensement.AddPracticeICD9(practiceID, hcpc.Trim(), UserICD9Code.Trim());
                            DAL.PracticeLocation.Dispensement.InsertPracticeICD9(icd9ID, practiceID);
                        }
                    }
                }
            }
            return iCD9_Codes;
        }

        public static DispenseSummaryContainer GetDispenseSummary(int practiceLocationID, bool isDispenseQueueSortDescending)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var container = new DispenseSummaryContainer();

                #region last dispensed summary
                {
                    var last20ids =
                        visionDB
                        .usp_GetLast10ItemsDispensed(1, practiceLocationID)
                        .Select(x => x.DISPENSEID);
                    var last20summary =
                        (from dd in visionDB.DispenseDetails
                         join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                         where last20ids.Contains(dd.DispenseID) 
                         select new DispenseSummaryItem
                         {
                             DispenseDate = dq.DispenseDate,
                             DispenseId = dd.DispenseID,
                             DispenseIdentifier = dq.DispenseIdentifier,
                             PatientCode = dq.PatientCode,
                             PatientFirstName = dq.PatientFirstName,
                             PatientLastName = dq.PatientLastName,
                             PhysicianID = dq.PhysicianID,
                             DbRowCreateDate = dd.Dispense.CreatedDate,
                             DispenseGroupingId = dq.DispenseGroupingId
                         })
                         .Distinct() //because the data is sourced from line-level dispense details, use distinct to collapse it to per-patient summary
                         .ToArray();

                    container.LastDispensementsSummary = last20summary;
                }
                #endregion

                #region unsigned dispensements
                {
                    var dispensedUnsigned =
                        (from dd in visionDB.DispenseDetails
                         join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                         join s1 in visionDB.DispenseSignatures on dq.DispenseSignatureID equals s1.DispenseSignatureID into s2
                         from s in s2.DefaultIfEmpty()
                         where (dq.PracticeLocationID == practiceLocationID)
                             // NOTE(Henry): Logic requires interpretation that missing signature row is considered as signed for legacy compatibility
                             && ((s != null) && (s.PhysicianSigned == false))
                             && dq.DeclineReason == "Accepted"
                             && dd.Dispense.IsActive
                         select new DispenseSummaryItem
                         {
                             DispenseId = dd.DispenseID,
                             DispenseDate = dq.DispenseDate,
                             DispenseIdentifier = dq.DispenseIdentifier,
                             PatientCode = dq.PatientCode,
                             PatientFirstName = dq.PatientFirstName,
                             PatientLastName = dq.PatientLastName,
                             PhysicianID = dq.PhysicianID,
                             DbRowCreateDate = dd.Dispense.CreatedDate,
                             DispenseGroupingId = dq.DispenseGroupingId
                         })
                         .Distinct() //because the data is sourced from line-level dispense details, use distinct to collapse it to per-patient summary
                         .ToArray();

                    container.PhysicianUnsignedDispensementsSummary = dispensedUnsigned;
                }
                #endregion

                #region queued dispensements
                {
                    // TODO: the new mobile app groups dispense queue items into a patient-level, a grouping identifier needs to be passed to the back end to avoid the needing to guess... for now, send it item-level and allow the app to apply the grouping logic because there exists the possibility of items queued in the web that need to follow suit (also consider old data previously queued before this upgrade...) -hchan 02252016
                    var queuedDispensements =
                        DAL.PracticeLocation.Dispensement.GetDispenseQueue(practiceLocationID, isDispenseQueueSortDescending)
                        .Select(
                            x =>
                                new DispenseQueueSummaryItem
                                {
                                    DispenseDate = x.DispenseDate,
                                    DispenseIdentifier = x.DispenseIdentifier,
                                    DispenseQueueId = x.DispenseQueueID,
                                    QueuedReason = x.QueuedReason,
                                    PatientCode = x.PatientCode,
                                    PatientFirstName = x.PatientFirstName,
                                    PatientLastName = x.PatientLastName,
                                    PhysicianID = x.PhysicianID,
                                    DbRowCreateDate = x.CreatedDate,
                                    BregVisionOrderID = x.BregVisionOrderID,
                                    DispenseGroupingId = x.DispenseGroupingID
                                
                                }
                        )
                        .ToArray();

                    container.DispenseQueueSummary = queuedDispensements;
                }
                #endregion

                return container;
            }
        }
    }
}