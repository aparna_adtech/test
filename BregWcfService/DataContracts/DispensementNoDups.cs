﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
namespace BregWcfService
{
    [DataContract]
    public class DispensementNoDups
    {
        public DispensementNoDups()
        {
        }

        [DataMember]
        public Dispensement Dispensement { get; set; }

        [DataMember]
        public bool IsDuplicate { get; set; }
    }
}