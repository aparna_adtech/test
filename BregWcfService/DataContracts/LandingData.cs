﻿using System;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web.Security;
using ClassLibrary.DAL;
using System.Collections.Generic;
using ClassLibrary.DAL.Helpers;
using DAL.PracticeLocation;

namespace BregWcfService
{
    public interface IShoppingCartItemCompact
    {
        int PracticeCatalogProductID { get; set; }
        int ReOrderQuantity { get; set; }
        bool IsConsignment { get; set; }
        int ConsignmentQuantity { get; set; }
        int QuantityInCart { get; set; }
        int CalculatedReOrderQuantity { get; set; }
    }

    [DataContract]
    public class ShoppingCartItemCompact : IShoppingCartItemCompact
    {
        [DataMember]
        public int PracticeCatalogProductID { get; set; }

        [DataMember]
        public int ReOrderQuantity { get; set; }

        [DataMember]
        public int ConsignmentQuantity { get; set; }

        [DataMember]
        public bool IsConsignment { get; set; }

        [DataMember]
        public int QuantityInCart { get; set; }

        [DataMember]
        public int CalculatedReOrderQuantity
        {
            get
            {
                return (ReOrderQuantity - QuantityInCart) < 0 ?
                        0 :
                        (ReOrderQuantity - QuantityInCart);
            }
            set{}

        }


        /// <summary>
        /// This is for cleanup of the unit tests 
        /// </summary>
        /// <param name="practiceLocationID"></param>
        public static void DeleteAllFromCart(int practiceLocationID)
        {
            DataSet dsCart = BLL.PracticeLocation.ShoppingCart.GetShoppingCart(practiceLocationID);

            foreach (DataRow row in dsCart.Tables[0].Rows)
            {
                DAL.PracticeLocation.Inventory.DeleteItemFromShoppingCart(Convert.ToInt32(row["ShoppingCartItemID"]));
            }

        }


        public static usp_GetShoppingCartResult[] GetShoppingCart(out int count, int practiceLocationID, int skip, int take)
        {

            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var query = from sc in visionDB.usp_GetShoppingCart(practiceLocationID)
                            select sc; //new usp_GetLast10ItemsDispensedResult();

                List<usp_GetShoppingCartResult> cart = query.ToList<usp_GetShoppingCartResult>();

                count = cart.Count();

                return cart.Skip<usp_GetShoppingCartResult>(skip).Take<usp_GetShoppingCartResult>(take).ToArray<usp_GetShoppingCartResult>();
            }
        }
    }



    [DataContract]
    public class LandingData
    {
        public enum InventoryOrderStatusPriority
        {
            None = 4,
            OnOrder = 3,
            FlaggedForReOrder = 2,
            FlaggedCritical = 1
        }

        public enum InventoryOrderStatusPriorityWithCart
        {
            None = 5,
            OnOrder = 4,
            InShoppingCart = 3,
            FlaggedForReOrder = 2,
            FlaggedCritical = 1
        }

        [DataMember]
        public usp_GetLast10ItemsDispensedResult[] Last10ItemsDispensed { get; set; }

        [DataMember]
        public InventoryOrderStatusResult[] InventoryOrderStatus { get; set; }


        public static LandingData GetLandingData(int practiceID, int practiceLocationID)
        {

            LandingData landingData = new LandingData();

            landingData.Last10ItemsDispensed = GetLastItemsDispensed(practiceID, practiceLocationID);

            landingData.InventoryOrderStatus = GetInventoryOrderStatus(practiceID, practiceLocationID);

            return landingData;

        }

        public static LandingData GetLandingData(out int count, int practiceID, int practiceLocationID, int skip, int take)
        {

            LandingData landingData = new LandingData();

            landingData.Last10ItemsDispensed = GetLastItemsDispensed(practiceID, practiceLocationID);

            landingData.InventoryOrderStatus = GetInventoryOrderStatus(out count, practiceID, practiceLocationID, skip, take);

            return landingData;

        }

        public static int AddToShoppingCartMultiple(int practiceLocationID, int UserID, IShoppingCartItemCompact[] shoppingCartItems)
        {
            int shoppingCartID = 0;

            foreach (IShoppingCartItemCompact sci in shoppingCartItems)
            {
                shoppingCartID = AddToShoppingCart(practiceLocationID, sci.PracticeCatalogProductID, UserID, sci.CalculatedReOrderQuantity);
            }

            return shoppingCartID;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="practiceLocationID">The practice location of the order</param>
        /// <param name="practiceCatalogProductID">The Product ID to be ordered</param>
        /// <param name="UserID">The User Ordering</param>
        /// <param name="Quantity">The Quantity to be ordered</param>
        /// <returns>Shopping Cart ID</returns>
        public static int AddToShoppingCart(int practiceLocationID, int practiceCatalogProductID, int UserID, int Quantity)
        {
            short plid = System.Convert.ToInt16(practiceLocationID);
            short uid = System.Convert.ToInt16(UserID);
            short qty = System.Convert.ToInt16(Quantity);

            return DAL.PracticeLocation.Inventory.UpdateInventoryData(plid, practiceCatalogProductID, uid, qty);
        }

		public static usp_SearchForItemsInInventory_ByPracticeCatalogProductID_With_ThirdPartyProductsResult[] usp_SearchForItemsInInventory_ByPracticeCatalogProductID_With_ThirdPartyProducts(
			int practiceLocationID, int practiceCatalogProductID)
		{
			using (var visionDB = VisionDataContext.GetVisionDataContext())
			{
				var query = from d in visionDB.usp_SearchForItemsInInventory_ByPracticeCatalogProductID_With_ThirdPartyProducts(practiceLocationID, practiceCatalogProductID)
							select new usp_SearchForItemsInInventory_ByPracticeCatalogProductID_With_ThirdPartyProductsResult
							{
								ProductInventoryID = d.ProductInventoryID,
								PracticeCatalogProductID = d.PracticeCatalogProductID,
								BrandName = d.BrandName,
								SupplierName = d.SupplierName,
								SupplierID = d.SupplierID,
								Category = d.Category,
								ProductName = d.ProductName,
								Code = d.Code,
								Packaging = d.Packaging,
								Side = d.Side,
								Size = d.Size,
								Gender = d.Gender,
								QOH = d.QOH,
								ParLevel = d.ParLevel,
								ReorderLevel = d.ReorderLevel,
								CriticalLevel = d.CriticalLevel,
								WholesaleCost = d.WholesaleCost,
								IsActive = d.IsActive,
								IsThirdPartyProduct = d.IsThirdPartyProduct,
								OldMasterCatalogProductID = d.OldMasterCatalogProductID,
								OldThirdPartyProductID = d.OldThirdPartyProductID,
								NewMasterCatalogProductID = d.NewMasterCatalogProductID,
								NewThirdPartyProductID = d.NewThirdPartyProductID,
								NewPracticeCatalogProductID = d.NewPracticeCatalogProductID,
								NewProductName = d.NewProductName,
								NewProductCode = d.NewProductCode,
								NewPracticeID = d.NewPracticeID
							};
				var results = query.ToArray<usp_SearchForItemsInInventory_ByPracticeCatalogProductID_With_ThirdPartyProductsResult>();
				return results;
			}
		}

        public static InventoryOrderStatusResult[] GetInventoryOrderStatus2(int practiceID, int practiceLocationID)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var query = from d in visionDB.usp_GetInventoryOrderStatus(practiceID, practiceLocationID)
                            select new InventoryOrderStatusResult
                            {
                                DMEDeposit = d.DMEDeposit,
                                PracticeCatalogProductID = d.PracticeCatalogProductID,
                                ProductInventoryID = d.ProductInventoryID,
                                MCName = d.MCName,
                                Code = d.Code,
                                //UPCCode = d.UPCCode,
                                LeftRightSide = d.LeftRightSide,
                                Size = d.Size,
                                Gender = d.Gender,
                                Color = d.Color,
                                Packaging = d.Packaging,
                                WholesaleCost = d.WholesaleCost,
                                SupplierName = d.SupplierName,
                                IsThirdPartySupplier = d.IsThirdPartySupplier,
                                SupplierSequence = d.SupplierSequence,
                                QuantityOnHand = d.QuantityOnHand,
                                QuantityOnOrder = d.QuantityOnOrder,
                                QuantityOnHandPlusOnOrder = d.QuantityOnHandPlusOnOrder,
                                ParLevel = d.ParLevel,
                                ReorderLevel = d.ReorderLevel,
                                CriticalLevel = d.CriticalLevel,
                                SuggestedReorderLevel = d.SuggestedReorderLevel,
                                Priority = d.Priority,
                                StockingUnits = d.StockingUnits,
                                HcpcString = d.Hcpcs,
                                HcpcOtsString = d.HcpcsOffTheShelf,
                                IsConsignment = d.IsConsignment,
                                ConsignmentQuantity = d.ConsignmentQuantity,
                                IsThirdParty = d.IsThirdPartySupplier ?? false,
                                MasterCatalogProductID = d.MasterCatalogProductID ?? 0,
                                ThirdPartyProductID = d.ThirdPartyProductID ?? 0,
                                BillingCharge = d.BillingCharge,
                                BillingChargeCash = d.BillingChargeCash,
                                IsSplitCode = d.IsSplitCode,
                                IsCustomBrace = d.IsCustomBrace,
                                IsAlwaysOffTheShelf = d.IsAlwaysOffTheShelf,
                                IsPreAuthorization = d.IsPreAuthorization
                            }; //new usp_GetLast10ItemsDispensedResult();
                var inventoryOrderStatusResults = query.ToArray<InventoryOrderStatusResult>();
                PopulateUpcCodesForMasterCatalogProducts(inventoryOrderStatusResults, visionDB);
                PopulateUpcCodesForThirdPartyProducts(inventoryOrderStatusResults, visionDB);
                ApplyIsAlwaysOffAndIsCustomFabricated(inventoryOrderStatusResults, visionDB);
                return inventoryOrderStatusResults;
            }
        }

        //todo add net new columns here for mobile

        public static InventoryOrderStatusResult[] GetInventoryLandingData(int practiceID, int practiceLocationID)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var query = from d in visionDB.usp_GetInventoryOrderStatus(practiceID, practiceLocationID)
                            select new InventoryOrderStatusResult
                            {
                                DMEDeposit = d.DMEDeposit,
                                PracticeCatalogProductID = d.PracticeCatalogProductID,
                                ProductInventoryID = d.ProductInventoryID,
                                MCName = d.MCName,
                                Code = d.Code,
                                //UPCCode = d.UPCCode,
                                LeftRightSide = d.LeftRightSide,
                                Side = d.LeftRightSide,
                                Size = d.Size,
                                Gender = d.Gender,
                                Color = d.Color,
                                Packaging = d.Packaging,
                                WholesaleCost = d.WholesaleCost,
                                SupplierName = d.SupplierName,
                                IsThirdPartySupplier = d.IsThirdPartySupplier,
                                SupplierSequence = d.SupplierSequence,
                                QuantityOnHand = d.QuantityOnHand,
                                QuantityOnOrder = d.QuantityOnOrder,
                                QuantityOnHandPlusOnOrder = d.QuantityOnHandPlusOnOrder,
                                ParLevel = d.ParLevel,
                                ReorderLevel = d.ReorderLevel,
                                CriticalLevel = d.CriticalLevel,
                                SuggestedReorderLevel = d.SuggestedReorderLevel,
                                Priority = d.Priority,
                                StockingUnits = d.StockingUnits,
                                HcpcString = d.Hcpcs,
                                HcpcOtsString = d.HcpcsOffTheShelf,
                                IsConsignment = d.IsConsignment,
                                ConsignmentQuantity = d.ConsignmentQuantity,
                                IsThirdParty = d.IsThirdPartySupplier ?? false,
                                MasterCatalogProductID = d.MasterCatalogProductID ?? 0,
                                ThirdPartyProductID = d.ThirdPartyProductID ?? 0,
                                BillingCharge = d.BillingCharge,
                                BillingChargeCash = d.BillingChargeCash,
                                IsSplitCode = d.IsSplitCode,
                                IsCustomBrace = d.IsCustomBrace,
                                IsAlwaysOffTheShelf = d.IsAlwaysOffTheShelf,
                                IsPreAuthorization = d.IsPreAuthorization,
                                IsLateralityExempt = d.IsLateralityExempt,
                                ModiferStrings = new string[3] {d.Mod1, d.Mod2, d.Mod3},
                                IsActive = d.IsActive.HasValue ? d.IsActive.Value : false,
                                ProductName = d.MCName,
                                BrandName = ""// todo - add this to the stored proc
                            }; 
                var inventoryOrderStatusResults = query.ToArray<InventoryOrderStatusResult>();

                PopulateUpcCodesForMasterCatalogProducts(inventoryOrderStatusResults, visionDB);
                PopulateUpcCodesForThirdPartyProducts(inventoryOrderStatusResults, visionDB);
                ApplyIsAlwaysOffAndIsCustomFabricated(inventoryOrderStatusResults, visionDB);
                PopulateABNNeeded(inventoryOrderStatusResults, visionDB);
                return inventoryOrderStatusResults;

            }
        }


        private static void ApplyIsAlwaysOffAndIsCustomFabricated(
            IEnumerable<InventoryOrderStatusResult> inventoryOrderStatusResults, VisionDataContext visionDb)
        {
            var customFabricatedCodes = HcpcsHelper.GetCustomFabricatedCodes(visionDb);
            foreach (var result in inventoryOrderStatusResults)
            {

                if (result.IsSplitCode && result.IsAlwaysOffTheShelf)
                {
                    result.IsSplitCode = false;
                }

                if(HcpcsHelper.IsHcpcCustomFabricated(result.HcpcString,customFabricatedCodes) ||
                    HcpcsHelper.IsHcpcCustomFabricated(result.HcpcOtsString, customFabricatedCodes))
                {
                    result.IsCustomBrace = true;
                }
            }
        }

        /*   private static void PopulateABNNeeded(InventoryOrderStatusResult[] inventoryOrderStatusResults, VisionDataContext visionDB)
           {
               var inventoryIds = inventoryOrderStatusResults.Select(x => x.PracticeCatalogProductID);

               var idsThatNeedabn = (from p in visionDB.ProductHCPCs
                                     join a in visionDB.ABN_HCPCs on p.HCPCS equals a.HCPCs
                                     where (a.ABN_Needed == true) && inventoryIds.Contains(p.PracticeCatalogProductID)
                                     select p.PracticeCatalogProductID);


               var productIds = idsThatNeedabn.Where(x=>x.HasValue).Select(x => x.Value).ToArray();

               foreach (var item in inventoryOrderStatusResults)
               {
                   item.IsABNNeeded = productIds.Contains(item.PracticeCatalogProductID.Value);
               }
           }*/

        private static void PopulateABNNeeded(InventoryOrderStatusResult[] inventoryOrderStatusResults, VisionDataContext visionDB)
        {
            var practiceCatalogProductIDs = inventoryOrderStatusResults.Where(x => x.PracticeCatalogProductID.HasValue).Select(x => x.PracticeCatalogProductID.Value);

            var practiceCatalogProductIDChunks = Utilities.BuildIdChunks(practiceCatalogProductIDs.ToList());
            var abn_needed_lookup = new List<int>();
            foreach (var chunk in practiceCatalogProductIDChunks)
            {
                abn_needed_lookup.AddRange(
                    from p in visionDB.ProductHCPCs
                    join a in visionDB.ABN_HCPCs on (p.HCPCS ?? "").Substring(0, 5) equals a.HCPCs
                    where a.ABN_Needed && p.PracticeCatalogProductID.HasValue && chunk.Contains(p.PracticeCatalogProductID.Value)
                    select p.PracticeCatalogProductID.Value
                );
            }

            foreach (var item in inventoryOrderStatusResults)
            {
                if (item.PracticeCatalogProductID.HasValue == false)
                    continue;
                item.IsABNNeeded = abn_needed_lookup.Contains(item.PracticeCatalogProductID.Value);
            }
        }

        private class catalogUPCItem
        {
            public int? CatalogProductID { get; set; }
            public string Code { get; set; }
        }
        private enum catalogUPCItemIDType
        {
            MasterCatalog,
            ThirdPartyProduct
        }


        private static List<catalogUPCItem> GetMatchingCatalogUPCItems(VisionDataContext visionDb, List<int> catalogIDs, catalogUPCItemIDType catalogIDType)
        {
            var catalogUPCs = new List<catalogUPCItem>();

            // use 2000-record chunking method to avoid SQL too-many-parameters issue in .Contain extension method
            for (var i = 0; i <= (int)(catalogIDs.Count() / 2000); i++)
            {
                var remaining = catalogIDs.Count() - (i * 2000);
                var catalogIDblock =
                    catalogIDs.GetRange(
                        i * 2000,
                        (remaining > 2000) ? 2000 : remaining
                    );

                switch (catalogIDType)
                {
                    case catalogUPCItemIDType.MasterCatalog:
                        catalogUPCs.AddRange(
                            visionDb.UPCCodes
                            .Where(upc => catalogIDblock.Contains(upc.MasterCatalogProductID.Value) && upc.IsActive)
                            .Select(upc =>
                                new catalogUPCItem
                                {
                                    CatalogProductID = upc.MasterCatalogProductID,
                                    Code = upc.Code
                                }));
                        break;
                    case catalogUPCItemIDType.ThirdPartyProduct:
                        catalogUPCs.AddRange(
                            visionDb.UPCCodes
                            .Where(upc => catalogIDblock.Contains(upc.ThirdPartyProductID.Value) && upc.IsActive)
                            .Select(upc =>
                                new catalogUPCItem
                                {
                                    CatalogProductID = upc.ThirdPartyProductID,
                                    Code = upc.Code
                                }));
                        break;
                }
            }

            return catalogUPCs;
        }

        private static void PopulateUpcCodesForMasterCatalogProducts(InventoryOrderStatusResult[] inventoryOrderStatusResults,
            VisionDataContext visionDb)
        {
            var mcpIds =
                inventoryOrderStatusResults.Where(pcp => (pcp.MasterCatalogProductID ?? 0) != 0)
                    .Select(pcp => pcp.MasterCatalogProductID ?? 0)
                    .OrderBy(id => id)
                    .ToList();

            List<catalogUPCItem> mcpUpcs = GetMatchingCatalogUPCItems(visionDb, mcpIds, catalogUPCItemIDType.MasterCatalog);

            var mcpUpscDictionary = mcpUpcs
                .GroupBy(x => x.CatalogProductID)
                .ToDictionary(upc => upc.Key.Value, upc => upc.ToList());


            foreach (var inventoryOrderStatusResult in inventoryOrderStatusResults)
            {
                if (inventoryOrderStatusResult.MasterCatalogProductID == 0)
                {
                    continue;
                }

                if (!mcpUpscDictionary.ContainsKey(inventoryOrderStatusResult.MasterCatalogProductID ?? 0))
                {
                    inventoryOrderStatusResult.UPCCode_List = new string[] { };
                    continue;
                }
                var mapEntry = mcpUpscDictionary[inventoryOrderStatusResult.MasterCatalogProductID ?? 0];

                inventoryOrderStatusResult.UPCCode_List = mapEntry.Select(upc => upc.Code).ToArray();
                inventoryOrderStatusResult.UPCCode = inventoryOrderStatusResult.UPCCode_List.FirstOrDefault();
            }
        }

        private static void PopulateUpcCodesForThirdPartyProducts(InventoryOrderStatusResult[] inventoryOrderStatusResults,
            VisionDataContext visionDb)
        {
            var tppIds =
                inventoryOrderStatusResults.Where(pcp => (pcp.ThirdPartyProductID ?? 0) != 0)
                    .Select(pcp => pcp.ThirdPartyProductID ?? 0)
                    .OrderBy(id => id)
                    .ToList();

            List<catalogUPCItem> tppUpcs = GetMatchingCatalogUPCItems(visionDb, tppIds, catalogUPCItemIDType.ThirdPartyProduct);

            var tppUpscDictionary = tppUpcs
                .GroupBy(tpp => tpp.CatalogProductID)
                .ToDictionary(upc => upc.Key.Value, upc => upc.ToList());


            foreach (var inventoryOrderStatusResult in inventoryOrderStatusResults)
            {
                if (inventoryOrderStatusResult.ThirdPartyProductID == 0)
                {
                    continue;
                }

                if (!tppUpscDictionary.ContainsKey(inventoryOrderStatusResult.ThirdPartyProductID ?? 0))
                {
                    inventoryOrderStatusResult.UPCCode_List = new string[] { };
                    continue;
                }
                var mapEntry = tppUpscDictionary[inventoryOrderStatusResult.ThirdPartyProductID ?? 0];
                inventoryOrderStatusResult.UPCCode_List = mapEntry.Select(upc => upc.Code).ToArray();
                inventoryOrderStatusResult.UPCCode = inventoryOrderStatusResult.UPCCode_List.FirstOrDefault();
            }
        }

        public static int SetPriorityLevelForInventoryOrderStatusResult(int? quantityOnOrder, int? quantityOnHandPlusOnOrder, int? reorderLevel, int? criticalLevel, int? parLevel, bool isNotFlaggedForReorder)
        {
            var priority = 4;
            if (quantityOnOrder > 0)
                priority = 3;
            if ((quantityOnHandPlusOnOrder <= reorderLevel) && parLevel > 0 && isNotFlaggedForReorder == false)
                priority = 2;
            if ((quantityOnHandPlusOnOrder <= criticalLevel) && parLevel > 0 && isNotFlaggedForReorder == false)
                priority = 1;

            return priority;
        }
#if true
        public static InventoryOrderStatusResult[] GetInventoryOrderStatusWithPriority(int practiceLocationId, List<int> productInventoryIds)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                Func<PracticeCatalogProduct, string> _getThirdPartyProductSupplierName =
                    (a) =>
                        (a.PracticeCatalogSupplierBrand.IsThirdPartySupplier
                            && (a.PracticeCatalogSupplierBrand.SupplierShortName != a.PracticeCatalogSupplierBrand.BrandShortName))
                        ? (a.PracticeCatalogSupplierBrand.SupplierShortName + " - " + a.PracticeCatalogSupplierBrand.BrandShortName)
                        : a.PracticeCatalogSupplierBrand.SupplierShortName;

						var results = (from pi in visionDB.ProductInventories
								join pcp in visionDB.PracticeCatalogProducts on pi.PracticeCatalogProductID equals pcp.PracticeCatalogProductID
								from prp in visionDB.ProductReplacementProducts.Where(prp => 
									(prp.PracticeCatalogProductID == pcp.PracticeCatalogProductID) && prp.OldPracticeCatalogProductID != null).DefaultIfEmpty()
								from opi in visionDB.ProductInventories.Where(opi => opi.PracticeCatalogProductID == prp.OldPracticeCatalogProductID &&
									(opi.PracticeLocationID == practiceLocationId) && opi.IsActive).DefaultIfEmpty()
							   where pcp.IsActive == true && pi.IsActive && productInventoryIds.Contains(pi.ProductInventoryID) && (pi.PracticeLocationID == practiceLocationId)

							   select new InventoryOrderStatusResult
							   {
								   BillingCharge = pcp.BillingCharge,
								   BillingChargeCash = pcp.BillingChargeCash,
								   DMEDeposit = pcp.DMEDeposit,
								   PracticeCatalogProductID = pcp.PracticeCatalogProductID,
								   ProductInventoryID = pi.ProductInventoryID,
								   MCName = pcp.IsThirdPartyProduct ? pcp.ThirdPartyProduct.Name : pcp.MasterCatalogProduct.Name,
								   Code = pcp.IsThirdPartyProduct ? pcp.ThirdPartyProduct.Code : pcp.MasterCatalogProduct.Code,
								   UPCCode =
									   pcp.IsThirdPartyProduct
										   ? pcp.ThirdPartyProduct.UPCCodes.Select(x => x.Code).FirstOrDefault()
										   : pcp.MasterCatalogProduct.UPCCodes.Select(x => x.Code).FirstOrDefault(),
								   LeftRightSide =
									   pcp.IsThirdPartyProduct
										   ? pcp.ThirdPartyProduct.LeftRightSide
										   : pcp.MasterCatalogProduct.LeftRightSide,
								   Size = pcp.IsThirdPartyProduct ? pcp.ThirdPartyProduct.Size : pcp.MasterCatalogProduct.Size,
								   Side = pcp.IsThirdPartyProduct ? pcp.ThirdPartyProduct.LeftRightSide : pcp.MasterCatalogProduct.LeftRightSide,
								   Gender = pcp.IsThirdPartyProduct ? pcp.ThirdPartyProduct.Gender : pcp.MasterCatalogProduct.Gender,
								   Color = pcp.IsThirdPartyProduct ? pcp.ThirdPartyProduct.Color : pcp.MasterCatalogProduct.Color,
								   Packaging = pcp.IsThirdPartyProduct ? pcp.ThirdPartyProduct.Packaging : pcp.MasterCatalogProduct.Packaging,
								   WholesaleCost = pcp.WholesaleCost,
								   SupplierName =
									   pcp.IsThirdPartyProduct
										   ? _getThirdPartyProductSupplierName(pcp)
										   : pcp.PracticeCatalogSupplierBrand.MasterCatalogSupplier.SupplierName,
								   BrandName = pcp.PracticeCatalogSupplierBrand.BrandShortName,
								   ProductName = pcp.IsThirdPartyProduct ? pcp.ThirdPartyProduct.Name : pcp.MasterCatalogProduct.Name,
								   IsThirdPartySupplier = pcp.IsThirdPartyProduct,
								   SupplierSequence = pcp.PracticeCatalogSupplierBrand.Sequence ?? 999,//NOTE: magic number 999 copied from usp_GetInventoryOrderStatus stored proc -hchan 20160322
								   QuantityOnHand = pi.QuantityOnHandPerSystem,
								   QuantityOnOrder = 0,//calculated later in the method
								   QuantityOnHandPlusOnOrder = 0,//calculated later in the method
								   ParLevel = pi.ParLevel,
								   ReorderLevel = pi.ReorderLevel,
								   CriticalLevel = pi.CriticalLevel,
								   SuggestedReorderLevel = 0,//calculated later in the method
								   Priority = 0,//calculated later in the method
								   StockingUnits = pcp.StockingUnits,
								   HcpcString = visionDB.ConcatHCPCS(pcp.PracticeCatalogProductID),
								   HcpcOtsString = visionDB.ConcatHCPCSOTS(pcp.PracticeCatalogProductID),
								   IsConsignment = pi.IsConsignment,
								   ConsignmentQuantity = pi.ConsignmentQuantity,
								   IsThirdParty = pcp.IsThirdPartyProduct,
								   MasterCatalogProductID = pcp.MasterCatalogProductID ?? 0,
								   ThirdPartyProductID = pcp.ThirdPartyProductID ?? 0,
								   UPCCode_List =
									   pcp.IsThirdPartyProduct
										   ? pcp.ThirdPartyProduct.UPCCodes.Select(x => x.Code).ToArray()
										   : pcp.MasterCatalogProduct.UPCCodes.Select(x => x.Code).ToArray(),
								   IsSplitCode = pcp.IsSplitCode,
								   IsCustomBrace = (pcp.MasterCatalogProduct != null) ? pcp.MasterCatalogProduct.IsCustomBrace : false,
								   ModiferStrings = new string[3] { pcp.Mod1, pcp.Mod2, pcp.Mod3 },
								   IsPreAuthorization = pcp.IsPreAuthorization,
                                   IsLateralityExempt = pcp.IsLateralityExempt.HasValue ? pcp.IsLateralityExempt.Value : false,
								   IsActive =
									   (pcp.IsThirdPartyProduct
										  ? pcp.ThirdPartyProduct.IsActive
										  : pcp.MasterCatalogProduct.IsActive)
									   && (pcp.IsActive ?? false)
									   && pi.IsActive,
								   IsAlwaysOffTheShelf = pcp.IsAlwaysOffTheShelf,
								   NewMasterCatalogProductID = (prp == null ? 0 : prp.NewMasterCatalogProductID),
								   NewThirdPartyProductID = (prp == null ? 0 : prp.NewThirdPartyProductID),
								   //NewPracticeCatalogProductID = (npcp == null ? 0 : npcp.PracticeCatalogProductID),
								   NewPracticeCatalogProductID = (prp == null ? 0 : prp.NewPracticeCatalogProductID),
								   OldQuantityOnHand = (opi == null ? 0 : opi.QuantityOnHandPerSystem),
							   })
                               .ToArray();

                var reorderLevels = GetSuggestReorderLevelForProductInventoryIDs(productInventoryIds, practiceLocationId);
                var shoppingCartIds = GetShoppingCartPracticeCatalogProductIds(practiceLocationId).ToDictionary(x => x);

                foreach (var result in results)
                {
                    var isNotFlaggedForReOrder = false;
                    if (reorderLevels.Any(x => x.ProductInventoryID == result.ProductInventoryID))
                    {
                        var reorderLevel = reorderLevels.First(x => x.ProductInventoryID == result.ProductInventoryID);
                        result.QuantityOnOrder = reorderLevel.QuantityOnOrder;
                        result.QuantityOnHandPlusOnOrder = result.QuantityOnOrder + result.QuantityOnHand;
                        result.SuggestedReorderLevel = reorderLevel.SuggestedReorderLevel;
                        isNotFlaggedForReOrder = reorderLevel.IsNotFlaggedForReorder;
                    }
                    result.Priority =
                        SetPriorityLevelForInventoryOrderStatusResult(
                            result.QuantityOnOrder,
                            result.QuantityOnHandPlusOnOrder,
                            result.ReorderLevel,
                            result.CriticalLevel,
                            result.ParLevel,
                            isNotFlaggedForReOrder);
                    result.IsShoppingCart = shoppingCartIds.ContainsKey(result.PracticeCatalogProductID.Value);
                }
                PopulateABNNeeded(results, visionDB);
                return results;
            }
        }
#endif
#if false
		public static InventoryOrderStatusResult[] GetInventoryOrderStatusWithPriority(int practiceLocationId, List<int> productInventoryIds)
		{
			using (var visionDB = VisionDataContext.GetVisionDataContext())
			{
				var reorderLevels = GetSuggestReorderLevelForProductInventoryIDs(productInventoryIds, practiceLocationId);
				var shoppingCartIds = GetShoppingCartPracticeCatalogProductIds(practiceLocationId).ToDictionary(x => x);

				string productInventoryIdsString = string.Empty;
				foreach (int productInventoryId in productInventoryIds)
				{
					productInventoryIdsString += (productInventoryIdsString == string.Empty ? string.Empty : ",") + productInventoryId.ToString();
				}

				var results = visionDB.usp_SearchForItemsInInventory_ByInventoryIds_With_ThirdPartyProducts(
					practiceLocationId, productInventoryIdsString).ToArray();
				List<InventoryOrderStatusResult> inventory = new List<InventoryOrderStatusResult>();
				foreach (var result in results)
				{
					InventoryOrderStatusResult inventoryResult = new InventoryOrderStatusResult();
					var isNotFlaggedForReOrder = false;
					if (reorderLevels.Any(x => x.ProductInventoryID == result.ProductInventoryID))
					{
						var reorderLevel = reorderLevels.First(x => x.ProductInventoryID == result.ProductInventoryID);





//						inventoryResult.BillingCharge = result.BillingCharge;
//						inventoryResult.BillingChargeCash = result.BillingChargeCash;
//						inventoryResult.DMEDeposit = result.DMEDeposit;
						inventoryResult.PracticeCatalogProductID = result.PracticeCatalogProductID;
						inventoryResult.ProductInventoryID = result.ProductInventoryID;
						inventoryResult.MCName = result.ProductName;// result.IsThirdPartyProduct ? result.ThirdPartyProduct.Name : result.MasterCatalogProduct.Name;
						inventoryResult.Code = result.Code;// result.IsThirdPartyProduct ? result.ThirdPartyProduct.Code : result.MasterCatalogProduct.Code;
						inventoryResult.UPCCode = result.Code;// results.GetHashCode;// result.IsThirdPartyProduct
															  //? result.ThirdPartyProduct.UPCCodes.Select(x => x.Code).FirstOrDefault()
															  //: result.MasterCatalogProduct.UPCCodes.Select(x => x.Code).FirstOrDefault();
						inventoryResult.LeftRightSide = result.Side;
						//result.IsThirdPartyProduct
						//	? result.ThirdPartyProduct.LeftRightSide
						//	: result.MasterCatalogProduct.LeftRightSide;
						inventoryResult.Size = result.Size;//result.IsThirdPartyProduct ? result.ThirdPartyProduct.Size : result.MasterCatalogProduct.Size;
						inventoryResult.Side = result.Side;// result.IsThirdPartyProduct ? result.ThirdPartyProduct.LeftRightSide : result.MasterCatalogProduct.LeftRightSide;
						inventoryResult.Gender = result.Gender;// IsThirdPartyProduct ? result.ThirdPartyProduct.Gender : result.MasterCatalogProduct.Gender;
//						inventoryResult.Color = result.Color;// result.IsThirdPartyProduct ? result.ThirdPartyProduct.Color : result.MasterCatalogProduct.Color;
						inventoryResult.Packaging = result.Packaging;// result.IsThirdPartyProduct ? result.ThirdPartyProduct.Packaging : result.MasterCatalogProduct.Packaging;
						inventoryResult.WholesaleCost = result.WholesaleCost;
						inventoryResult.SupplierName = result.SupplierName;
						//result.IsThirdPartyProduct
						//	? _getThirdPartyProductSupplierName(result)
						//	: result.PracticeCatalogSupplierBrand.MasterCatalogSupplier.SupplierName;
						inventoryResult.BrandName = result.BrandName;// result.PracticeCatalogSupplierBrand.BrandShortName;
						inventoryResult.ProductName = result.ProductName;// result.IsThirdPartyProduct ? result.ThirdPartyProduct.Name : result.MasterCatalogProduct.Name;
						inventoryResult.IsThirdPartySupplier = (result.IsThirdPartyProduct == 0);
//						inventoryResult.SupplierSequence = result.PracticeCatalogSupplierBrand.Sequence ?? 999;//NOTE: magic number 999 copied from usp_GetInventoryOrderStatus stored proc -hchan 20160322
						inventoryResult.QuantityOnHand = result.QOH;// result.QuantityOnHandPerSystem;
						inventoryResult.QuantityOnOrder = 0;//calculated later in the method
						inventoryResult.QuantityOnHandPlusOnOrder = 0;//calculated later in the method
						inventoryResult.ParLevel = result.ParLevel;
						inventoryResult.ReorderLevel = result.ReorderLevel;
						inventoryResult.CriticalLevel = result.CriticalLevel;
						inventoryResult.SuggestedReorderLevel = 0;//calculated later in the method
						inventoryResult.Priority = 0;//calculated later in the method
//						inventoryResult.StockingUnits = result.StockingUnits;
						inventoryResult.HcpcString = visionDB.ConcatHCPCS(result.PracticeCatalogProductID);
						inventoryResult.HcpcOtsString = visionDB.ConcatHCPCSOTS(result.PracticeCatalogProductID);
//						inventoryResult.IsConsignment = result.IsConsignment;
//						inventoryResult.ConsignmentQuantity = result.ConsignmentQuantity;
//						inventoryResult.IsThirdParty = result.IsThirdPartyProduct;
//						inventoryResult.MasterCatalogProductID = result.MasterCatalogProductID ?? 0;
//						inventoryResult.ThirdPartyProductID = result.ThirdPartyProductID ?? 0;
//						inventoryResult.UPCCode_List = result.Code;
							//result.IsThirdPartyProduct
							//	? result.ThirdPartyProduct.UPCCodes.Select(x => x.Code).ToArray()
							//	: result.MasterCatalogProduct.UPCCodes.Select(x => x.Code).ToArray();
//						inventoryResult.IsSplitCode = result.IsSplitCode;
//						inventoryResult.IsCustomBrace = (result.MasterCatalogProduct != null) ? result.MasterCatalogProduct.IsCustomBrace : false;
//						inventoryResult.ModiferStrings = new string[3] { result.Mod1, result.Mod2, result.Mod3 };
//						inventoryResult.IsPreAuthorization = result.IsPreAuthorization;
                        inventoryResult.IsLateralityExempt = result.IsLateralityExempt.HasValue ? result.IsLateralityExempt.Value : false,
//						inventoryResult.IsActive = (result.IsActive ?? false) && result.IsActive ? true : false;
//						inventoryResult.IsAlwaysOffTheShelf = result.IsAlwaysOffTheShelf;












//						result.QuantityOnOrder = reorderLevel.QuantityOnOrder;
//						result.QuantityOnHandPlusOnOrder = result.QuantityOnOrder + result.QuantityOnHand;
//						result.SuggestedReorderLevel = reorderLevel.SuggestedReorderLevel;
						isNotFlaggedForReOrder = reorderLevel.IsNotFlaggedForReorder;
					}
//					result.Priority =
//						SetPriorityLevelForInventoryOrderStatusResult(
//							result.QuantityOnOrder,
//							result.QuantityOnHandPlusOnOrder,
//							result.ReorderLevel,
//							result.CriticalLevel,
//							result.ParLevel,
//							isNotFlaggedForReOrder);
//					result.IsShoppingCart = shoppingCartIds.ContainsKey(result.PracticeCatalogProductID.Value);
					inventory.Add(inventoryResult);
				}
//				PopulateABNNeeded(results, visionDB);
				return inventory.ToArray();
			}
		}
#endif

        public static List<int> GetShoppingCartPracticeCatalogProductIds(int practiceLocationId)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var shoppingCartIds = from sci in visionDB.ShoppingCartItems
                    join sc in visionDB.ShoppingCarts on sci.ShoppingCartID equals sc.ShoppingCartID
                    where sc.PracticeLocationID == practiceLocationId && sc.IsActive == true && sci.IsActive == true
                    select sci.PracticeCatalogProductID.Value;

                return shoppingCartIds.ToList();
            }
        }

        public static List<SuggestReorderLevelForProductInventory> GetSuggestReorderLevelForProductInventoryIDs(List<int> productInventoryIds, int practiceLocationId)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                // construct result "shell" to fill in
                // NOTE: the join to PracticeCatalogProducts is only needed to ensure database consistency and should theoretically be already handled by foreign key constraints in the database.
                var results =
                    (
                        from pi in visionDB.ProductInventories
                        join pcp in visionDB.PracticeCatalogProducts on new { pi.PracticeCatalogProductID, IsActive = true } equals new { pcp.PracticeCatalogProductID, IsActive = pcp.IsActive ?? false }
                        where productInventoryIds.Contains(pi.ProductInventoryID)
                        select new SuggestReorderLevelForProductInventory
                        {
                            ProductInventoryID = pi.ProductInventoryID,
                            PracticeCatalogProductID = pcp.PracticeCatalogProductID,
                            QuantityOnHand = pi.QuantityOnHandPerSystem,
                            ParLevel = pi.ParLevel ?? 0,
                            ReorderLevel = pi.ReorderLevel ?? 0,
                            CriticalLevel = pi.CriticalLevel ?? 0,
                            IsNotFlaggedForReorder = pi.IsNotFlaggedForReorder,
                            StockingUnits = pcp.StockingUnits,
                         
                        }
                    )
                    .ToList();

                // build list of Practice Catalog IDs from the inventory IDs
                var practiceCatalogProductIds = results.Select(x => x.PracticeCatalogProductID).Distinct().ToArray();

                // calculate current on-order quantities
                var StatusOrdered_Quantity_Ordered =
                    (
                        from pi in visionDB.ProductInventories
                        join pcp in visionDB.PracticeCatalogProducts on pi.PracticeCatalogProductID equals pcp.PracticeCatalogProductID
                        join bvo in visionDB.BregVisionOrders on pi.PracticeLocationID equals bvo.PracticeLocationID
                        join so in visionDB.SupplierOrders on bvo.BregVisionOrderID equals so.BregVisionOrderID
                        join soli in visionDB.SupplierOrderLineItems on new { pcp.PracticeCatalogProductID, so.SupplierOrderID } equals new { soli.PracticeCatalogProductID, soli.SupplierOrderID }
                        where
                            practiceCatalogProductIds.Contains(pi.PracticeCatalogProductID)
                            && pi.PracticeLocationID == practiceLocationId
                            && pi.IsActive == true
                            && pcp.IsActive == true
                            && bvo.IsActive == true
                            && so.IsActive == true
                            && soli.IsActive == true
                            && soli.SupplierOrderLineItemStatusID == 1
                        select new { pi.PracticeCatalogProductID, soli.QuantityOrdered } into T
                        group T by T.PracticeCatalogProductID into items
                        select new
                        {
                            PracticeCatalogProductID = items.Key,
                            StatusOrdered_Quantity_Ordered = items.Sum(x => x.QuantityOrdered)
                        }
                    )
                    .ToArray()
                    .ToDictionary(x => x.PracticeCatalogProductID);

                // calculate quantities of ordered products that have not been checked in yet
                var StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory =
                    (
                        from pi in visionDB.ProductInventories
                        join pcp in visionDB.PracticeCatalogProducts on pi.PracticeCatalogProductID equals pcp.PracticeCatalogProductID
                        join bvo in visionDB.BregVisionOrders on pi.PracticeLocationID equals bvo.PracticeLocationID
                        join so in visionDB.SupplierOrders on bvo.BregVisionOrderID equals so.BregVisionOrderID
                        join soli in visionDB.SupplierOrderLineItems on new { pcp.PracticeCatalogProductID, so.SupplierOrderID } equals new { soli.PracticeCatalogProductID, soli.SupplierOrderID }
                        join pi_oci in visionDB.ProductInventory_OrderCheckIns on new { pi.ProductInventoryID, soli.SupplierOrderLineItemID } equals new { pi_oci.ProductInventoryID, pi_oci.SupplierOrderLineItemID }
                        where
                            practiceCatalogProductIds.Contains(pi.PracticeCatalogProductID)
                            && pi.PracticeLocationID == practiceLocationId
                            && pi.IsActive == true
                            && pcp.IsActive == true
                            && bvo.IsActive == true
                            && so.IsActive == true
                            && soli.IsActive == true
                            && pi_oci.IsActive == true
                            && soli.SupplierOrderLineItemStatusID == 3
                        select new
                        {
                            soli.PracticeCatalogProductID,
                            soli.QuantityOrdered,
                            pi_oci.Quantity
                        } into T
                        group T by T.PracticeCatalogProductID into items
                        select new
                        {
                            PracticeCatalogProductID = items.Key,
                            StatusPartiallyCheckedIn_Quantity_Ordered = items.Select(x => x.QuantityOrdered).FirstOrDefault(),
                            StatusPartiallyCheckedIn_Quantity_CheckedintoInventory = items.Sum(x => x.Quantity),
                            StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory = items.Select(x => x.QuantityOrdered).FirstOrDefault() - items.Sum(x => x.Quantity)
                        }
                    )
                    .ToArray()
                    .ToDictionary(x => x.PracticeCatalogProductID);

                // populate results
                foreach (var result in results)
                {
                    // retrieve calculated values
                    // NOTE: The stored procedure that we adapted this from isn't very clear in naming variables... the StatusOrdered_Quantity_Ordered data is comprised of only orders that have had no check-in activity, and the StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory data is comprised of orders that have had some check-in activity. With both quantities being mutually exclusive, it's not very clear that the true quantity that is waiting to be added to on-hand quantity is the quantity ordered plus the quantity that has not been checked-in yet in a partially checked-in order.
                    var orderedTotal =
                        StatusOrdered_Quantity_Ordered.ContainsKey(result.PracticeCatalogProductID)
                        ? StatusOrdered_Quantity_Ordered[result.PracticeCatalogProductID].StatusOrdered_Quantity_Ordered
                        : 0;
                    var notCheckedInTotal =
                        StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory.ContainsKey(result.PracticeCatalogProductID)
                        ? StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory[result.PracticeCatalogProductID].StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory
                        : 0;

                    var remainingToBeReceivedFromOrder = orderedTotal + notCheckedInTotal;

                    // set QuantityOnOrder
                    result.QuantityOnOrder = remainingToBeReceivedFromOrder;

                    // set SuggestedReorderLevel
                   
                    var onHandPlusToBeReceived = result.QuantityOnHand + remainingToBeReceivedFromOrder;
                    var suggestedReorderLevel = (result.ParLevel - onHandPlusToBeReceived) / result.StockingUnits;
                    if (suggestedReorderLevel < 0)
                        suggestedReorderLevel = 0;
                    result.SuggestedReorderLevel = suggestedReorderLevel;
                }

                return results;
            }
        }

        /// <summary>
        /// Get an inventory order status result based on Product Inventory ID. This should
        /// provide results for both active and inactive inventory items and is used by
        /// the mobile app for purposes of being able to dereference inactive items that are
        /// currently still in the dispense queue.
        /// </summary>
        /// <param name="practiceID"></param>
        /// <param name="practiceLocationID"></param>
        /// <param name="productInventoryID"></param>
        /// <returns></returns>
        public static InventoryOrderStatusResult[] GetInventoryOrderStatusByInventoryID(int practiceLocationID, int[] productInventoryIDs)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var results = (from pi in visionDB.ProductInventories
                    join pcp in visionDB.PracticeCatalogProducts on pi.PracticeCatalogProductID equals
                        pcp.PracticeCatalogProductID
                    where productInventoryIDs.Contains(pi.ProductInventoryID) 
                          && pi.PracticeLocationID == practiceLocationID
                    select new InventoryOrderStatusResult
                    {
                        BillingCharge = pcp.BillingCharge,
                        BillingChargeCash = pcp.BillingChargeCash,
                        DMEDeposit = pcp.DMEDeposit,
                        PracticeCatalogProductID = pcp.PracticeCatalogProductID,
                        ProductInventoryID = pi.ProductInventoryID,
                        MCName = pcp.IsThirdPartyProduct ? pcp.ThirdPartyProduct.Name : pcp.MasterCatalogProduct.Name,
                        Code = pcp.IsThirdPartyProduct ? pcp.ThirdPartyProduct.Code : pcp.MasterCatalogProduct.Code,
                        UPCCode =
                            pcp.IsThirdPartyProduct
                                ? pcp.ThirdPartyProduct.UPCCodes.Select(x => x.Code).FirstOrDefault()
                                : pcp.MasterCatalogProduct.UPCCodes.Select(x => x.Code).FirstOrDefault(),
                        LeftRightSide =
                            pcp.IsThirdPartyProduct
                                ? pcp.ThirdPartyProduct.LeftRightSide
                                : pcp.MasterCatalogProduct.LeftRightSide,
                        Size = pcp.IsThirdPartyProduct ? pcp.ThirdPartyProduct.Size : pcp.MasterCatalogProduct.Size,
                        Gender =
                            pcp.IsThirdPartyProduct ? pcp.ThirdPartyProduct.Gender : pcp.MasterCatalogProduct.Gender,
                        Color = pcp.IsThirdPartyProduct ? pcp.ThirdPartyProduct.Color : pcp.MasterCatalogProduct.Color,
                        Packaging =
                            pcp.IsThirdPartyProduct
                                ? pcp.ThirdPartyProduct.Packaging
                                : pcp.MasterCatalogProduct.Packaging,
                        WholesaleCost = pcp.WholesaleCost,
                        SupplierName = pcp.IsThirdPartyProduct
                            ? ((pcp.PracticeCatalogSupplierBrand.IsThirdPartySupplier &&
                                (pcp.PracticeCatalogSupplierBrand.SupplierShortName !=
                                 pcp.PracticeCatalogSupplierBrand.BrandShortName))
                                ? (pcp.PracticeCatalogSupplierBrand.SupplierShortName + " - " +
                                   pcp.PracticeCatalogSupplierBrand.BrandShortName)
                                : pcp.PracticeCatalogSupplierBrand.SupplierShortName)
                            : pcp.PracticeCatalogSupplierBrand.MasterCatalogSupplier.SupplierName,
                        IsThirdPartySupplier = pcp.IsThirdPartyProduct,
                        SupplierSequence = pcp.PracticeCatalogSupplierBrand.Sequence ?? 999,
                        //NOTE: magic number 999 copied from usp_GetInventoryOrderStatus stored proc -hchan 20160322
                        QuantityOnHand = pi.QuantityOnHandPerSystem,
                        QuantityOnOrder = 0,
                        //TODO match logic from usp_GetInventoryOrderStatus stored proc -hchan 20160322
                        QuantityOnHandPlusOnOrder = pi.QuantityOnHandPerSystem + 0,
                        //TODO match logic from usp_GetInventoryOrderStatus stored proc -hchan 20160322
                        ParLevel = pi.ParLevel,
                        ReorderLevel = pi.ReorderLevel,
                        CriticalLevel = pi.CriticalLevel,
                        SuggestedReorderLevel = pi.ReorderLevel,
                        Priority = 0, //TODO match logic from usp_GetInventoryOrderStatus stored proc -hchan 20160322
                        StockingUnits = pcp.StockingUnits,
                        HcpcString = visionDB.ConcatHCPCS(pcp.PracticeCatalogProductID),
                        HcpcOtsString = visionDB.ConcatHCPCSOTS(pcp.PracticeCatalogProductID),
                        IsConsignment = pi.IsConsignment,
                        ConsignmentQuantity = pi.ConsignmentQuantity,
                        IsThirdParty = pcp.IsThirdPartyProduct,
                        MasterCatalogProductID = pcp.MasterCatalogProductID ?? 0,
                        ThirdPartyProductID = pcp.ThirdPartyProductID ?? 0,
                        UPCCode_List =
                            pcp.IsThirdPartyProduct
                                ? pcp.ThirdPartyProduct.UPCCodes.Select(x => x.Code).ToArray()
                                : pcp.MasterCatalogProduct.UPCCodes.Select(x => x.Code).ToArray(),
                        IsSplitCode = pcp.IsSplitCode,
                        IsCustomBrace =
                            (pcp.MasterCatalogProduct != null) ? pcp.MasterCatalogProduct.IsCustomBrace : false,
                        ModiferStrings = new string[3] {pcp.Mod1, pcp.Mod2, pcp.Mod3},
                        IsPreAuthorization = pcp.IsPreAuthorization,
                        IsLateralityExempt = pcp.IsLateralityExempt.HasValue ? pcp.IsLateralityExempt.Value : false,
                        IsActive = (pcp.IsActive ?? false) && pi.IsActive ? true : false
                    });
               
                return results.ToArray();
            }
        }

        public static InventoryOrderStatusResult[] GetInventoryOrderStatus(int practiceID, int practiceLocationID)
        {
            return GetInventoryOrderStatus2(practiceID, practiceLocationID);
        }

        public static InventoryOrderStatusResult[] GetInventoryOrderStatus(out int count, int practiceID, int practiceLocationID, int skip, int take, bool noSkipTake = false)
        {

            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var query = from d in visionDB.usp_GetInventoryOrderStatus(practiceID, practiceLocationID)
                            join pi in visionDB.ProductInventories on d.ProductInventoryID equals pi.ProductInventoryID
                            join pcp in visionDB.PracticeCatalogProducts on d.PracticeCatalogProductID equals pcp.PracticeCatalogProductID
                            join mcp in visionDB.MasterCatalogProducts on pcp.MasterCatalogProductID equals mcp.MasterCatalogProductID into mastercatalog
                            from mc in mastercatalog.DefaultIfEmpty()
                            join tpp in visionDB.ThirdPartyProducts on pcp.ThirdPartyProductID equals tpp.ThirdPartyProductID into thirdparty
                            from tp in thirdparty.DefaultIfEmpty()
                            select new InventoryOrderStatusResult
                            {
                                BillingCharge = pcp.BillingCharge,
                                BillingChargeCash = pcp.BillingChargeCash,
                                DMEDeposit = pcp.DMEDeposit,
                                PracticeCatalogProductID = d.PracticeCatalogProductID,
                                ProductInventoryID = d.ProductInventoryID,
                                MCName = d.MCName,
                                Code = d.Code,
                                UPCCode = d.UPCCode,
                                LeftRightSide = d.LeftRightSide,
                                Size = d.Size,
                                Gender = pcp.IsThirdPartyProduct == true ? tp.Gender : mc.Gender,
                                Color = pcp.IsThirdPartyProduct == true ? tp.Color : mc.Color,
                                Packaging = pcp.IsThirdPartyProduct == true ? tp.Packaging : mc.Packaging,
                                WholesaleCost = pcp.WholesaleCost,
                                SupplierName = d.SupplierName,
                                IsThirdPartySupplier = d.IsThirdPartySupplier,
                                SupplierSequence = d.SupplierSequence,
                                QuantityOnHand = d.QuantityOnHand,
                                QuantityOnOrder = d.QuantityOnOrder,
                                QuantityOnHandPlusOnOrder = d.QuantityOnHandPlusOnOrder,
                                ParLevel = d.ParLevel,
                                ReorderLevel = d.ReorderLevel,
                                CriticalLevel = d.CriticalLevel,
                                SuggestedReorderLevel = d.SuggestedReorderLevel,
                                Priority = d.Priority,
                                StockingUnits = d.StockingUnits,
                                HcpcString = visionDB.ConcatHCPCS(d.PracticeCatalogProductID),
                                HcpcOtsString = visionDB.ConcatHCPCSOTS(d.PracticeCatalogProductID),
                                IsConsignment = pi.IsConsignment,
                                ConsignmentQuantity = pi.ConsignmentQuantity,
                                IsThirdParty = d.IsThirdPartySupplier ?? false,
                                MasterCatalogProductID = pcp.MasterCatalogProductID ?? 0,
                                ThirdPartyProductID = pcp.ThirdPartyProductID ?? 0,
                                UPCCode_List = pcp.IsThirdPartyProduct ? pcp.ThirdPartyProduct.UPCCodes.Select(x => x.Code).ToArray() : pcp.MasterCatalogProduct.UPCCodes.Select(x => x.Code).ToArray(),
                                IsSplitCode = pcp.IsSplitCode,
                                IsCustomBrace = mc != null ? mc.IsCustomBrace : false,
                                ModiferStrings = new string[3] {pcp.Mod1,pcp.Mod2,pcp.Mod3},
                                IsPreAuthorization = pcp.IsPreAuthorization,
                                IsLateralityExempt = pcp.IsLateralityExempt.HasValue ? pcp.IsLateralityExempt.Value : false,
                                IsAlwaysOffTheShelf = pcp.IsAlwaysOffTheShelf
                            }; //new usp_GetLast10ItemsDispensedResult();,

                //

                List<InventoryOrderStatusResult> inventoryResults = query.ToList<InventoryOrderStatusResult>();

                count = inventoryResults.Count();

                IEnumerable<InventoryOrderStatusResult> results;

                results = noSkipTake ? inventoryResults : inventoryResults.Skip<InventoryOrderStatusResult>(skip).Take<InventoryOrderStatusResult>(take);

                ApplyIsAlwaysOffAndIsCustomFabricated(inventoryResults, visionDB);
                //count = results.Count();

                return results.ToArray<InventoryOrderStatusResult>();
            }

        }

        public static usp_GetLast10ItemsDispensedResult[] GetLastItemsDispensed(int practiceID, int practiceLocationID)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var query = from d in visionDB.usp_GetLast10ItemsDispensed(1, practiceLocationID)
                            select d; //new usp_GetLast10ItemsDispensedResult();

                return query.Take<usp_GetLast10ItemsDispensedResult>(20).ToArray<usp_GetLast10ItemsDispensedResult>();
            }
        }
    }

    public class SuggestReorderLevelForProductInventory
    {
        public int ProductInventoryID { get; set; }
        public int PracticeCatalogProductID { get; set; }
        public int QuantityOnHand { get; set; }
        public int QuantityOnOrder { get; set; }
        public int ParLevel { get; set; }
        public int ReorderLevel { get; set; }
        public int CriticalLevel { get; set; }
        public bool IsNotFlaggedForReorder { get; set; }
        public int SuggestedReorderLevel { get; set; }
        public int StockingUnits { get; set; }
    }
}
    