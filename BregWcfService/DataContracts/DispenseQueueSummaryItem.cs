﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BregWcfService
{
    [DataContract]
    public class DispenseQueueSummaryItem
    {
        [DataMember]
        public int DispenseQueueId { get; set; }

        [DataMember]
        public string QueuedReason { get; set; }

        [DataMember]
        public string PatientFirstName { get; set; }

        [DataMember]
        public string PatientLastName { get; set; }

        [DataMember]
        public string PatientCode { get; set; }

        [DataMember]
        public DateTime? DispenseDate { get; set; }

        [DataMember]
        public int? PhysicianID { get; set; }

        [DataMember]
        public string DispenseIdentifier { get; set; }

        [DataMember]
        public DateTime? DbRowCreateDate { get; set; }

        [DataMember]
        public int? BregVisionOrderID { get; set; }
        [DataMember]
        public string DispenseGroupingId { get; set; }
    }
}