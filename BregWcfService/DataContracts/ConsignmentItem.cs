﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web.Security;
using ClassLibrary.DAL;
using System.Collections;
using System.Collections.Generic;

namespace BregWcfService
{
    [DataContract]
    public class ConsignmentItem : InventoryDiscrepencyItem, IShoppingCartItemCompact
    {

        //[DataMember]
        //public int ReOrderQuantity { get; set; }

        //[DataMember]
        //public int ConsignmentQuantity { get; set; }

        //[DataMember]
        //public bool IsConsignment { get; set; }

        //[DataMember]
        //public int QuantityOnOrder { get; set; }

        //[DataMember]
        //public bool IsRMA { get; set; }

        //[DataMember]
        //public bool IsBuyout { get; set; }

        [DataMember]
        public int BuyoutRMAInventoryCycleID { get; set; }

        public static ConsignmentItem[] GetConsignmentReplenishmentListCompact(int practiceLocationID, string userName, string password, string deviceID, string lastLocation)
        {
            return GetConsignmentReplenishmentListCompact(practiceLocationID);
        }

        private static ConsignmentItem[] GetConsignmentReplenishmentListCompact(int practiceLocationID)
        {
            

            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var consignmentQuery = from pi in visionDB.ProductInventories
                                       join pcp in visionDB.PracticeCatalogProducts on pi.PracticeCatalogProductID equals pcp.PracticeCatalogProductID
                                       join mcp in visionDB.MasterCatalogProducts on pcp.MasterCatalogProductID equals mcp.MasterCatalogProductID
                                       join sr in visionDB.udf_GetSuggestedReorder_Given_PracticeLocationID(practiceLocationID) on pi.PracticeCatalogProductID equals sr.PracticeCatalogProductID //This looks wrong because the fk is on pcpId instead of piId but the records are filtered by practiceLocationID                           
                                       where pi.PracticeLocationID == practiceLocationID
                                       //&& pi.QuantityOnHandPerSystem < pi.ConsignmentQuantity
                                       && pi.IsConsignment == true
                                       && pi.ConsignmentQuantity > 0
                                       select new ConsignmentItem
                                       {
                                           QOH = pi.QuantityOnHandPerSystem,
                                           ConsignmentQuantity = pi.ConsignmentQuantity,
                                           PracticeCatalogProductID = pi.PracticeCatalogProductID,
                                           QuantityOnOrder = sr.QuantityOnOrder ?? 0,
                                           ReOrderQuantity = (      //if consignmentQty - onHand + onOrder < 0 then it = 0, otherwise calculated
                                                                    (pi.ConsignmentQuantity - (pi.QuantityOnHandPerSystem + sr.QuantityOnOrder ?? 0)) < 0 ?
                                                                    0 :
                                                                    (pi.ConsignmentQuantity - (pi.QuantityOnHandPerSystem + sr.QuantityOnOrder ?? 0))
                                                             ),
                                           IsConsignment = true,
                                           Code = mcp.Code
                                       };

                return consignmentQuery.ToArray<ConsignmentItem>();
            }
        }


        public static InventoryDiscrepencyItem[] GetConsignmentReplenishmentList(int practiceLocationID, string userName, string password, string deviceID, string lastLocation)
        {
            return GetConsignmentReplenishmentList(practiceLocationID);
        }

        public static InventoryDiscrepencyItem[] GetConsignmentReplenishmentList(int practiceLocationID)
        {
            
            InventoryDiscrepencyItem[] inventoryItems = InventoryDiscrepencyItem.GetInventoryCycleDiscrepencies(practiceLocationID, 
                                                                                                                inventoryCycle: null, //we don't care about the cycle at this point because it should be closed 
                                                                                                                IncludeAll: true, //this refers to counted vs uncounted.  we want to include all because the count is over and cycle is closed.
                                                                                                                isConsignmentCount: true);

            var query = from i in inventoryItems
                        where i.IsConsignment == true
                        && i.ConsignmentQuantity > 0
                        select i;

            return query.ToArray<InventoryDiscrepencyItem>() ;
#region old code

            //ConsignmentItem[] consignmentItems = GetConsignmentReplenishmentListCompact(practiceLocationID);

            //string[] codes = consignmentItems.Select(x => x.Code).ToArray<string>();

            //string codeString = string.Join(",", codes);

            //VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            //using (visionDB)
            //{
            //    var query = from i in visionDB.usp_SearchforItemsInInventory_With_ProductCode_Array("Code", codeString, practiceLocationID)
            //                join pi in visionDB.ProductInventories on i.ProductInventoryID equals pi.ProductInventoryID
            //                join pcp in visionDB.PracticeCatalogProducts on i.PracticeCatalogProductID equals pcp.PracticeCatalogProductID
            //                join ci in consignmentItems on i.PracticeCatalogProductID equals ci.PracticeCatalogProductID
            //                where 
            //                    ci.IsConsignment == true
            //                    && ci.ConsignmentQuantity > 0
            //                    //ci.ReOrderQuantity > 0
            //                    //|| pi.IsBuyout == true
            //                    //|| pi.IsRMA == true
            //                select new ConsignmentItem
            //                {
            //                    BrandShortName = i.BrandName,
            //                    Code = i.Code,
            //                    CriticalLevel = i.CriticalLevel,
            //                    Gender = i.Gender,
            //                    IsThirdPartyProduct = 0,
            //                    Packaging = i.Packaging,
            //                    ParLevel = i.ParLevel,
            //                    PracticeCatalogProductID = i.PracticeCatalogProductID ?? 0,
            //                    Product = i.ProductName,
            //                    ProductInventoryID = i.ProductInventoryID ?? 0,
            //                    QOH = i.QOH ?? 0,
            //                    ConsignmentQuantity = ci.ConsignmentQuantity,
            //                    ReorderLevel = i.ReorderLevel,
            //                    Side = i.Side,
            //                    Size = i.Size,
            //                    SupplierID = i.SupplierID ?? 0,
            //                    SupplierShortName = i.SupplierName,
            //                    WholesaleCost = i.WholesaleCost ?? 0,
            //                    BillingCharge = pcp.BillingCharge,
            //                    BillingChargeCash = pcp.BillingChargeCash,
            //                    ICD9_List = InventoryItem.GetDispenseItemICD9s(practiceLocationID, i.ProductInventoryID ?? 0),
            //                    ReOrderQuantity = ci.ReOrderQuantity,
            //                    QuantityOnOrder = ci.QuantityOnOrder,
            //                    IsConsignment = ci.IsConsignment,
            //                    IsRMA = pi.IsRMA,
            //                    IsBuyout = pi.IsBuyout,
            //                    BuyoutRMAInventoryCycleID = pi.BuyoutRMAInventoryCycleID ?? 0,
            //                    IsActive = ci.IsActive,
            //                };

            //    return query.ToArray<ConsignmentItem>();

            #endregion
            



        }

        public static void AddReplenishmentConsignmentListToShoppingCart(int UserID, int practiceLocationID, bool fromVision = false)
        {
            InventoryDiscrepencyItem[] inventoryDiscrepencyItems = GetConsignmentReplenishmentList( practiceLocationID);

            InventoryDiscrepencyItem[] discrepenciesWithoutRMABuyout = (from d in inventoryDiscrepencyItems
                                                                        where d.CalculatedReOrderQuantity > 0
                                                                        && d.IsBuyout == false
                                                                        && d.IsRMA == false
                                                                        select d).ToArray<InventoryDiscrepencyItem>();

            int[] inventoryItems = (from i in discrepenciesWithoutRMABuyout
                                    select i.ProductInventoryID).ToArray<int>();

            //test if there is a a custom brace or CustomBraceAccessories in the cart before trying to add.
            bool containsCustomBrace = DAL.PracticeLocation.ShoppingCart.ContainsCustomBrace(practiceLocationID);

            if (containsCustomBrace == false)
            {
                ConsignmentItem[] consignmentItems = InventoryDiscrepencyItem.Cast(discrepenciesWithoutRMABuyout);

                AddReplenishmentConsignmentListToShoppingCart(UserID, consignmentItems, practiceLocationID, fromVision);
            }
            else
            {
                throw new CustomBraceInCartException("Cart contains a custom brace or custom brace accessory and no other items can be added. ");
            }

        }


        public static void AddReplenishmentConsignmentListToShoppingCart(int UserID, ConsignmentItem[] consignmentItems, int practiceLocationID, bool fromVision = false)
        {
            ConsignmentItem[] consignmentItemsForCart = consignmentItems.Where(a => a.ReOrderQuantity > 0 && a.IsBuyout == false && a.IsRMA == false).ToArray<ConsignmentItem>();

            IShoppingCartItemCompact[] shoppingCartItems = consignmentItemsForCart.Cast<IShoppingCartItemCompact>().ToArray<IShoppingCartItemCompact>();
            
            AddReplenishmentListToShoppingCart(UserID, shoppingCartItems, practiceLocationID);

            Classes.Email.SendBillingNotificationEmail(practiceLocationID, fromVision, EmailTemplates.InventoryComplete.ConsignmentEmailType.ConsignmentItemsAddedToCart);
        }

       
        public static void AddReplenishmentListToShoppingCart(int UserID, IShoppingCartItemCompact[] shoppingCartItems, int practiceLocationID)
        {
            int shoppingCartID = LandingData.AddToShoppingCartMultiple(practiceLocationID, UserID, shoppingCartItems);

            DAL.PracticeLocation.ShoppingCart.SetConsignment(shoppingCartID);
        }


    }

    [Serializable]
    public class CustomBraceInCartException : System.Exception
    {
        public string ErrorMessage
        {
            get
            {    
                return base.Message.ToString();
            }
        }
        public CustomBraceInCartException(string errorMessage) : base(errorMessage) 
        {
        }
        public CustomBraceInCartException(string errorMessage, Exception innerEx) : base(errorMessage, innerEx) 
        { 
        }
    }
}