﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web.Security;
using ClassLibrary.DAL;
using System.Collections;
using System.Collections.Generic;

namespace BregWcfService
{


    [DataContract]
    public class InventoryCriteria
    {
        public InventoryCriteria()
        {

        }
        public InventoryCriteria(string name, string value)
        {
            Name = name;
            Value = value;
        }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Value { get; set; }


        public static InventoryCriteria[] GetInventorySearchCodes()
        {
            return new InventoryCriteria[] 
            { 
                new InventoryCriteria("All", "All"),
                new InventoryCriteria("Code", "Code"),
                new InventoryCriteria("UPCCode", "UPCCode"),
                new InventoryCriteria("Brand", "Brand"),
                new InventoryCriteria("Name", "Name"),
                new InventoryCriteria("Category", "Category"),
                new InventoryCriteria("HCPCs", "HCPCs"),
            };
        }

        public static InventoryCriteria[] GetDispenseSearchCodes()
        {
            return new InventoryCriteria[] 
            { 
                new InventoryCriteria("'Name", "'Name"),
                new InventoryCriteria("Code", "Code"),
                new InventoryCriteria("UPCCode", "'UPCCODE"),
                new InventoryCriteria("Brand", "Brand"),
                new InventoryCriteria("HCPCs", "HCPCs"),
            };
        }

        public static InventoryCriteria[] GetDispenseCustomBraceSearchCodes()
        {
            return new InventoryCriteria[] 
            { 
                new InventoryCriteria("PO Number", "PONumber"),
                new InventoryCriteria("Patient ID", "PatientID"),
            };
        }

        public static InventoryCriteria[] GetDispenseSideCodes()
        {
            return new InventoryCriteria[] 
            { 
                new InventoryCriteria("'U", "'U"),
                new InventoryCriteria("LT", "L"),
                new InventoryCriteria("RT", "R"),

            };
        }

    }



    [DataContract]
    public class InventoryItem : usp_SearchforItemsInInventoryResult
    {

        [DataMember]
        public Decimal BillingCharge { get; set; }

        [DataMember]
        public Decimal BillingChargeCash { get; set; }

        [DataMember]
        public string[] ICD9_List { get; set; }

        [DataMember]
        public string[] UPCCode_List { get; set; }


        public static InventoryItem[] SearchForItemsInInventory(out int count, int practiceLocationID, string searchCriteria, string searchText, int skip, int take)
        {
           
            string[] singleSearchText = searchText.Split(','); 

            return SearchForItemsInInventory(out count, practiceLocationID, searchCriteria, singleSearchText, skip, take);
        }

        public static InventoryItem[] SearchForItemsInInventory(out int count, int practiceLocationID, string searchCriteria, string[] singleSearchText, int skip, int take)
        {
            List<InventoryItem> singleSearchItem = new List<InventoryItem>();
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                foreach (string text in singleSearchText)
                {
                    if (!string.IsNullOrEmpty(text))
                    {
                        var query = from i in visionDB.usp_SearchforItemsInInventory(searchCriteria, text, practiceLocationID)
                                    join pcp in visionDB.PracticeCatalogProducts on i.PracticeCatalogProductID equals pcp.PracticeCatalogProductID
                                    join mcp in visionDB.MasterCatalogProducts on pcp.MasterCatalogProductID equals mcp.MasterCatalogProductID into mastercatalog
                                    from mc in mastercatalog.DefaultIfEmpty()
                                    join tpp in visionDB.ThirdPartyProducts on pcp.ThirdPartyProductID equals tpp.ThirdPartyProductID into thirdParty
                                    from tpp in thirdParty.DefaultIfEmpty()
                                    where mc == null || mc.IsCustomBrace == false
                                    select new InventoryItem
                                    {
                                        BrandShortName = i.BrandShortName,
                                        Category = i.Category,
                                        Code = i.Code,
                                        CriticalLevel = i.CriticalLevel,
                                        Gender = i.Gender,
                                        HCPCSString = i.HCPCSString,
                                        IsActive = i.IsActive,
                                        IsThirdPartyProduct = i.IsThirdPartyProduct,
                                        Packaging = i.Packaging,
                                        ParLevel = i.ParLevel,
                                        PracticeCatalogProductID = i.PracticeCatalogProductID,
                                        Product = i.Product,
                                        ProductInventoryID = i.ProductInventoryID,
                                        QOH = i.QOH,
                                        ReorderLevel = i.ReorderLevel,
                                        Side = i.Side,
                                        Size = i.Size,
                                        SupplierID = i.SupplierID,
                                        SupplierShortName = i.SupplierShortName,
                                        WholesaleCost = i.WholesaleCost,
                                        BillingCharge = pcp.BillingCharge,
                                        BillingChargeCash = pcp.BillingChargeCash,
                                        ICD9_List = GetDispenseItemICD9s(practiceLocationID, i.ProductInventoryID),
                                        UPCCode_List = pcp.IsThirdPartyProduct ? pcp.ThirdPartyProduct.UPCCodes.Select(x => x.Code).ToArray() : pcp.MasterCatalogProduct.UPCCodes.Select(x => x.Code).ToArray()
                                    };

                        
                        singleSearchItem.AddRange(query.ToList<InventoryItem>());
                    }
                }
            }

            count = singleSearchItem.Count;

            return singleSearchItem.Skip<InventoryItem>(skip).Take<InventoryItem>(take).ToArray<InventoryItem>();
        }

        public static string[] GetDispenseItemICD9s(int practiceLocationID, int productInventoryID)
        {
            ArrayList productInventoryIDs = new ArrayList();
            productInventoryIDs.Add(productInventoryID);

            var _ICD9_Codes = DAL.PracticeLocation.Dispensement.GetDispenseQueueICD9s(productInventoryIDs, practiceLocationID);

            string[] icd9s =
                        _ICD9_Codes
                        .Select(icd9 => icd9.ICD9Code.Trim())
                        .Distinct()
                        .ToArray<string>();
            return icd9s;
        }

        public static void SaveUpcCode(int practiceID, string userName, string password, string deviceID, string lastLocation, int productID, string upcCode, bool isThirdPartyProduct)
        {
            // is user a practice admin or breg admin?
            var isUserInRoleAllowingUpcUpdates =
                (Roles.IsUserInRole(userName, "practiceadmin") || Roles.IsUserInRole(userName, "bregadmin"));

            if (!isUserInRoleAllowingUpcUpdates)
                throw new ApplicationException("Permissions denied saving UPCCode to product");

            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var isProductIDValid = false;
                var upcExists = false;

                if (isThirdPartyProduct == false)
                {
                    // is the productID a valid master catalog product id?
                    isProductIDValid =
                        visionDB.MasterCatalogProducts.Any(
                            x =>
                                x.MasterCatalogProductID == productID
                                && x.IsActive == true
                        );

                    // does the requested upc code already exist?
                    upcExists =
                        visionDB.UPCCodes.Any(
                            x =>
                                x.MasterCatalogProductID == productID
                                && x.Code == upcCode
                                && x.IsActive == true
                        );
                }
                else
                {
                    // is the productID a valid third party product id?
                    isProductIDValid =
                        visionDB.PracticeCatalogProducts.Any(
                            x =>
                                x.ThirdPartyProductID == productID
                                && x.PracticeID == practiceID
                                && x.IsActive == true
                        );

                    // does the requested upc code already exist?
                    upcExists =
                        visionDB.UPCCodes.Any(
                            x =>
                                x.MasterCatalogProductID == productID
                                && x.Code == upcCode
                                && x.IsActive == true
                        );
                }

                // insert new record if code doesn't already exist
                if (isProductIDValid && !upcExists)
                {
                    var newCode = new UPCCode();
                    newCode.Code = upcCode;
                    newCode.CreatedDate = DateTime.Now;
                    newCode.CreatedUserID = 1;
                    newCode.IsActive = true;
                    if (isThirdPartyProduct)
                        newCode.ThirdPartyProductID = productID;
                    else
                        newCode.MasterCatalogProductID = productID;

                    visionDB.UPCCodes.InsertOnSubmit(newCode); // note(when upcs are added a trigger will fire creating in an productchangelog record)

                    visionDB.SubmitChanges();
                }
            }
        }
    }
}

