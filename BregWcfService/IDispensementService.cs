﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace BregWcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IDispensementService" in both code and config file together.
    [ServiceContract]
    public interface IDispensementService
    {
        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetPatientSignatureRelations?userName={userName}&password={password}&deviceID={deviceId}&lastLocation={lastLocation}")]

        PatientSignatureRelationship[] GetPatientSignatureRelations(string userName, string password, string deviceID, string lastLocation);
    }
}
