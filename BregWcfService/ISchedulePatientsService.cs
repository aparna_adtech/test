﻿using BregWcfService.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace BregWcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISchedulePatientsService" in both code and config file together.
    [ServiceContract]
    public interface ISchedulePatientsService
    {
        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "schedulePatients?practiceId={practiceId}&practiceLocationID={practiceLocationID}&startDate={utcstartDate}&userName={userName}&password={password}&deviceID={deviceId}&lastLocation={lastLocation}&skip={skip}&take={take}")]
        ScheduleItem[] GetSchedulePatients(int practiceId, int practiceLocationId, DateTime utcStartDate, string userName, string password, string deviceID, string lastLocation, int skip, int take);
        //schedulePatients?practiceId=1&practiceLocationID=4254&startDate=11-11-2016&userName=testadmin&password=Breg2007&deviceID=2322342&lastLocation=2&skip=2&take=1
    }
}
