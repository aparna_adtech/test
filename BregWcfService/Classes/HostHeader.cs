﻿using System.ServiceModel;

namespace BregWcfService
{
    public class HostHeader
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string DeviceID { get; set; }
        public string LastLocation { get; set; }

        public static HostHeader Current 
        { 
            get
            {
                HostHeader _current = new HostHeader();
                _current.UserName = OperationContext.Current.IncomingMessageHeaders.GetHeader<string>("UserName", "VisionHeader");
                _current.Password = OperationContext.Current.IncomingMessageHeaders.GetHeader<string>("Password", "VisionHeader");
                _current.DeviceID = OperationContext.Current.IncomingMessageHeaders.GetHeader<string>("DeviceID", "VisionHeader");
                _current.LastLocation = OperationContext.Current.IncomingMessageHeaders.GetHeader<string>("LastLocation", "VisionHeader");
                return _current;
            }

        }
    }
}