﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BregWcfService.Classes
{
    public class TempFileManager : IDisposable
    {
        private bool _disposed = false;
        private string _tempfile = Guid.NewGuid().ToString("N").ToLower() + ".dat";
        
        private static string TempfilePath
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["TempfilePath"] ?? @"X:\";
            }
        }

        public string FullTempfilePath
        {
            get
            {
                return System.IO.Path.GetFullPath(string.Format(@"{0}\{1}", TempfilePath, _tempfile));
            }
        }

        public void CopyToPath(string targetPath)
        {
            System.IO.File.Copy(FullTempfilePath, targetPath, true);
        }

        public byte[] GetBytes()
        {
            return System.IO.File.ReadAllBytes(FullTempfilePath);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                #region free managed resources
                if (System.IO.File.Exists(FullTempfilePath))
                {
                    try
                    {
                        System.IO.File.Delete(FullTempfilePath);
                    }
                    catch { /* leave file if error in removal, non-critical error condition */ }
                }
                #endregion
            }

            #region free unmanaged resources
            #endregion

            _disposed = true;
        }

        ~TempFileManager()
        {
            Dispose(false);
        }
    }
}