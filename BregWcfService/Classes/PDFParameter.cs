﻿using System;

namespace BregWcfService.Classes
{
    public class PDFParameter
    {
        public string html { get; set; }
        public string filename { get; set; }
        public int practiceID { get; set; }
        public int practiceLocationID { get; set; }
        public string patientCode { get; set; }
        public bool emrEnabled { get; set; }
        public DateTime dispenseDate { get; set; }
        public byte[][] attachments { get; set; }
    }
}
