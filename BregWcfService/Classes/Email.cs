﻿using System;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregWcfService.DocumentServiceReference;
using BregWcfService.EMRIntegrationReference;
using Microsoft.Practices.EnterpriseLibrary.Data;
using ClassLibrary.DAL;
using BregWcfService.EmailTemplates;
using System.Configuration;
using System.Collections.Generic;
using com.breg.vision.DispenseReceiptBuilder.DataModels;
using com.breg.vision.DispenseReceiptBuilder.ViewLoaders;
using com.breg.vision.DispenseReceiptBuilder;

namespace BregWcfService.Classes
{
    public class Email
    {
        private class VisionSessionProvider : BLL.IVisionSessionProvider
        {
            private Int32 _practiceID;
            private Int32 _practiceLocationID;

            public VisionSessionProvider(Int32 practiceLocationID)
            {
                try
                {
                    _practiceLocationID = practiceLocationID;
                    using (var db = ClassLibrary.DAL.VisionDataContext.GetVisionDataContext())
                    {
                        _practiceID = db.PracticeLocations.Where(x => x.PracticeLocationID == _practiceLocationID).Select(x => x.PracticeID).First();
                    }
                }
                catch
                {
                    _practiceID = -1;
                    _practiceLocationID = -1;
                }
            }

            public int GetCurrentPracticeID()
            {
                return _practiceID;
            }

            public int GetCurrentPracticeLocationID()
            {
                return _practiceLocationID;
            }
        }

        public static String SendEmail(string MailFromProfile, string MailTo, string MailBody, string MailSubject, string filePath, BLL.SecureMessaging.MessageClass messageClass, BLL.IVisionSessionProvider sessionProvider = null)
        {
            #region secure messaging
            // temp storage for message parts
            var x_sender = "";
            var x_recipient = MailTo;
            var x_body = MailBody;
            var x_subject = MailSubject;
            // apply updates to message parts
            // TODO: need to use 1 instance instead of instantiating per message send...
            new BLL.SecureMessaging(sessionProvider).ApplySecureMessagingAdjustments(messageClass, ref x_sender, ref x_recipient, ref x_body, ref x_subject);
            #endregion

            #region send mail (via database mail)
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");

            DbCommand dbCommand = db.GetStoredProcCommand("msdb.dbo.sp_send_dbmail");
            
            db.AddInParameter(dbCommand, "profile_name", DbType.String, MailFromProfile);
            db.AddInParameter(dbCommand, "recipients", DbType.String, x_recipient);
            db.AddInParameter(dbCommand, "subject", DbType.String, x_subject);
            db.AddInParameter(dbCommand, "body", DbType.String, x_body);
            db.AddInParameter(dbCommand, "body_format", DbType.String, "HTML");
            if (!string.IsNullOrEmpty(filePath) && File.Exists(filePath))
                db.AddInParameter(dbCommand, "file_attachments", DbType.String, filePath);

#if(!DEBUG)
            int SQLReturn = db.ExecuteNonQuery(dbCommand);
#endif
            #endregion

            return MailTo;
        }

        public static String SendEmail(string MailFromProfile, string MailTo, string MailBody, string MailSubject, BLL.SecureMessaging.MessageClass messageClass, BLL.IVisionSessionProvider sessionProvider = null)
        {
            return SendEmail(MailFromProfile, MailTo, MailBody, MailSubject, null, messageClass, sessionProvider);
        }


        public static string SendBillingEmail(int DispenseBatchID, int practiceLocationID)
        {
            if (DispenseBatchID < 1)
            {
                return "";
            }

            using (var visionDB = ClassLibrary.DAL.VisionDataContext.GetVisionDataContext())
            {
                Func<string, string, string, string, string> _formatModifiers = (lrm, m1, m2, m3) =>
                {
                    var x = string.Empty;
                    Action __appendComma = () => { if (!string.IsNullOrEmpty(x)) { x += ", "; } };
                    Action<string> __appendItem = (s) => { if (!string.IsNullOrEmpty(s)) { __appendComma(); x += s; } };
                    __appendItem(lrm);
                    __appendItem(m1);
                    __appendItem(m2);
                    __appendItem(m3);
                    return x;
                };

                var itemsDispensed = from db in visionDB.DispenseBatches
                                     join d in visionDB.Dispenses on db.DispenseBatchID equals d.DispenseBatchID
                                     join dd in visionDB.DispenseDetails on d.DispenseID equals dd.DispenseID
                                     join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                                     where db.DispenseBatchID == DispenseBatchID
                                     && (dq.DeclineReason.ToLower() == "accepted".ToLower() || dq.DeclineReason == null || dq.DeclineReason.Equals(string.Empty))
                                     //&& dd.IsCashCollection == false
                                     select new
                                     {
                                         DispensedDate = d.DateDispensed.ToShortDateString(),
                                         PatientName = (((d.PatientFirstName ?? "") + " " + (d.PatientLastName ?? ""))).Trim(),
                                         d.PatientCode,
                                         Physician = dd.Clinician.Contact.FirstName + " " + dd.Clinician.Contact.LastName,
                                         HCPCs = dq.HCPCs,
                                         Modifiers = _formatModifiers(dd.LRModifier, dq.Mod1, dq.Mod2, dq.Mod3),
                                         ActualChargeBilled = string.Format("{0:C}", dd.ActualChargeBilled),
                                         DMEDeposit = string.Format("{0:C}", dd.DMEDeposit),
                                         CashCarry = dd.IsCashCollection,
                                         ProductName = dd.PracticeCatalogProduct.IsThirdPartyProduct == true ? dd.PracticeCatalogProduct.ThirdPartyProduct.Name : dd.PracticeCatalogProduct.MasterCatalogProduct.Name,
                                         ProductCode = dd.PracticeCatalogProduct.IsThirdPartyProduct == true ? dd.PracticeCatalogProduct.ThirdPartyProduct.Code : dd.PracticeCatalogProduct.MasterCatalogProduct.Code,
                                         Size = dd.PracticeCatalogProduct.IsThirdPartyProduct == true ? dd.PracticeCatalogProduct.ThirdPartyProduct.Size : dd.PracticeCatalogProduct.MasterCatalogProduct.Size,
                                         QuantityToDispense = dd.Quantity,
                                         DxCode = dd.ICD9Code,
                                         PatientRefusalReason = dq.DeclineReason,
                                         dd.IsMedicare,
                                         ABNForm = dd.ABNType
                                         //dd.PracticeCatalogProduct //brand
                                     };

                //make a grid for the dispense queue to render for an email
                var dispenseEmail = new GridView();
                dispenseEmail.DataSource = itemsDispensed;
                dispenseEmail.DataBind();

                var ctlAll = new System.Web.UI.WebControls.Panel();
                ctlAll.Controls.Add(dispenseEmail);
                ctlAll.Controls.Add(new LiteralControl("<br/><br/>"));

                var sb = new StringBuilder();
                var sw = new StringWriter(sb);
                var htmlTW = new HtmlTextWriter(sw);
                ctlAll.RenderControl(htmlTW);

                var html = sb.ToString();

                var MailTo = BLL.PracticeLocation.Email.GetBillingEmail(practiceLocationID);

                var dispenseBatchIDDebugInfo = "";
                //TODO: It seems the empty MailTo check should be done before the expense of building the email.
                if (string.IsNullOrEmpty(MailTo.Trim()) == false && itemsDispensed.Count() > 0)
                {
                    SendEmail("OrderConfirmation", MailTo, html, string.Format("DME Dispensement Information - Billing {0}", dispenseBatchIDDebugInfo), BLL.SecureMessaging.MessageClass.Billing, new VisionSessionProvider(practiceLocationID));
                }

                return html;
            }
        }


        public static bool SendPatientEmail(int DispenseBatchID, int practiceLocationID, MemoryStream pdfMemoryStream,out byte[] CompressedPDF,bool fromVision = false)
        {
            #region Exit if no Batch ID
            CompressedPDF = null;

            if (DispenseBatchID < 1)
            {
                //pdfMemoryStream = null;
                //return false;
            }
            #endregion

            //int[] batchID = { DispenseBatchID }; What I want here is DispenseID's
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                int[] dispenseIDs = (from d in visionDB.Dispenses
                                     where d.DispenseBatchID == DispenseBatchID
                                     select d.DispenseID).ToArray<int>();

                SendPatientEmail(dispenseIDs, practiceLocationID, pdfMemoryStream,out CompressedPDF,fromVision: fromVision, createPDF: true, sendEmail: true);
            }

            return true;
        }

        public static bool RenderDispenseReceiptPDF(int dispenseID, int practiceLocationID, out string pdfFilename)
        {
            var fromVision = false; // always false for PDF generation (left here to keep code consistent)
            var virtualDirName = "BregWcfService/";
            var emailTemplatePath = string.Format("~/{0}EmailTemplates/", virtualDirName);
            
            pdfFilename = null;

            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var dispensement = GetItemsDispensed(visionDB, new int[] { dispenseID }).FirstOrDefault();

                if (dispensement == null)
                    return false;

                var isMedicare = dispensement.DispenseItems.Any(di => di.IsMedicare);

                #region build dispense receipt and email body user controls and html
                var page = new InertHTMLPage();
                var ctlPatientReceipt = new System.Web.UI.WebControls.Panel();
                var ctlPatientEmailBody = new System.Web.UI.WebControls.Panel();
                bool? createPdf = null;
                bool lastItem = true;
                
                PatientEmailBody emailBody = page.LoadControl(string.Format("{0}PatientEmailBody.ascx", emailTemplatePath)) as PatientEmailBody;
                //set any properties here after we get text from Kevin
                ctlPatientEmailBody.Controls.Add(emailBody);

                var receiptBuilder = DispenseRecieptBuilderFactory.GetRecieptBuilder(dispensement);

                var html = receiptBuilder.BuildDispenseReceipt(practiceLocationID, fromVision, createPdf, isMedicare, dispensement.PatientCode, page, ctlPatientReceipt, lastItem);
                #endregion

                #region render pdf
                string path = System.Web.Hosting.HostingEnvironment.MapPath(string.Format("~/{0}DispenseReceipts", virtualDirName));

                if (!string.IsNullOrEmpty(path))
                {
                    string filename = Path.Combine(path, string.Format("DispenseReceipt{0}.pdf", dispensement.DispenseID)); //string.Format("{0}/test.pdf");

                    PDFParameter pdfParameter = new PDFParameter();
                    pdfParameter.html = html;
                    pdfParameter.filename = filename;
                    pdfParameter.practiceID = dispensement.Practice.PracticeID;
                    pdfParameter.practiceLocationID = practiceLocationID;
                    pdfParameter.patientCode = dispensement.PatientCode;
                    pdfParameter.emrEnabled = dispensement.EMREnabled;
                    pdfParameter.dispenseDate = dispensement.DateDispensed;

                    var documents = GetActiveCustomDocuments(dispensement.Practice.PracticeID);

                    using (var tempFile = new TempFileManager())
                    {
                        CreatePDF(pdfParameter, tempFile, documents);
                        tempFile.CopyToPath(filename);
                    }

                    pdfFilename = filename;

                    return true;
                }
                else
                    return false;
                #endregion
            }
        }


        public static StringBuilder SendPatientEmail(int[] dispenseIDs, int practiceLocationID, MemoryStream pdfMemoryStream,out byte[] CompressedPDF,bool fromVision = false, bool createPDF = true, bool sendEmail = true, bool hidePrintButton = false, bool sendFax = true, bool queueOutboundMessages = true)
        {
            var completeString = new StringBuilder();
            var virtualDirName = fromVision ? "BregWcfService/" : string.Empty;
            var emailTemplatePath = string.Format("~/{0}EmailTemplates/", virtualDirName);
            CompressedPDF = null;
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var practiceLocation =
                    visionDB.PracticeLocations
                        .Where(x => x.PracticeLocationID == practiceLocationID)
                        .Select(x =>
                                    new
                                    {
                                        x.PracticeLocationID,
                                        x.IsHcpSignatureRequired
                                    })
                        .Single();

                #region get items dispensed
                var itemsDispensed = GetItemsDispensed(visionDB, dispenseIDs);
                #endregion

                var dispensements = itemsDispensed.ToList();
                var isMedicareQuery = dispensements.Any(d => d.DispenseItems.Any(di => di.IsMedicare));

                var counter = 1;
                foreach (var dispensement in dispensements)
                {
                    var patient_code = dispensement.PatientCode;

                    // get cloud connect remote_schedule_item_id from the patient code, if possible
                    var vcc_schedule_id = GetCloudConnectRemoteScheduleItemId(ref patient_code);

                    #region build dispense receipt and email body user controls

                    var page = new InertHTMLPage();
                    var ctlPatientReceipt = new System.Web.UI.WebControls.Panel();
                    var ctlPatientEmailBody = new System.Web.UI.WebControls.Panel();
                    bool lastItem = (counter >= dispensements.Count) ? true : false;
                    #region Email Body

                    PatientEmailBody emailBody = page.LoadControl(string.Format("{0}PatientEmailBody.ascx", emailTemplatePath)) as PatientEmailBody;
                    //set any properties here after we get text from Kevin
                    ctlPatientEmailBody.Controls.Add(emailBody);

                    #endregion

                    var receiptBuilder = DispenseRecieptBuilderFactory.GetRecieptBuilder(dispensement);

                    var html = receiptBuilder.BuildDispenseReceipt(practiceLocationID, fromVision, createPDF, isMedicareQuery, patient_code, page, ctlPatientReceipt, lastItem);


                    #endregion

                    #region render html

                    if (counter == 1)
                    {
                        html = "<div id=\"divNonPrintable\"><input type=\"button\" value=\"Print\" onclick=\"PrintContent()\" /></div>" + html;
                    }

                    if (createPDF || hidePrintButton)
                    {
                        html = html.Replace("<div id=\"divNonPrintable\">", "<div id=\"divNonPrintable\" style=\"display:none\">"); // Hide "Print" button in the PDF file added by u= 
                    }

                    completeString.Append(html);

                    #endregion

                    #region render pdf

                    string filename = "";
                    try
                    {
                        string path = HostingEnvironment.MapPath(string.Format("~/{0}DispenseReceipts", virtualDirName));

                        bool savePdfToFile = (!string.IsNullOrEmpty(path) && createPDF);
                        bool emrIntegrationDispenseReceiptPushEnabled = dispensement.EMRIntegrationDispenseReceiptPushEnabled;
                        bool emrIntegrationHL7PushEnabled = dispensement.EMRIntegrationHL7PushEnabled;
                        string dispenseReceiptFileName = string.Format("DispenseReceipt{0}.pdf", dispensement.DispenseID);

                        if (savePdfToFile || emrIntegrationDispenseReceiptPushEnabled || (pdfMemoryStream != null))
                        {
                            filename = Path.Combine(path, dispenseReceiptFileName); //string.Format("{0}/test.pdf");

                            PDFParameter pdfParameter = new PDFParameter();
                            pdfParameter.html = html;
                            pdfParameter.filename = filename;
                            pdfParameter.practiceID = dispensement.Practice.PracticeID;
                            pdfParameter.practiceLocationID = practiceLocationID;
                            pdfParameter.patientCode = patient_code;
                            pdfParameter.emrEnabled = dispensement.EMREnabled;
                            pdfParameter.dispenseDate = dispensement.DateDispensed;

                            var documents = GetActiveCustomDocuments(dispensement.Practice.PracticeID);

                            using (var tempFile = new TempFileManager())
                            {
                                CreatePDF(pdfParameter, tempFile, documents);

                                CompressedPDF = tempFile.GetBytes(); // set output compressed file
                                
                                if (emrIntegrationDispenseReceiptPushEnabled || (pdfMemoryStream != null))
                                {
                                    byte[] tempFileBytes = tempFile.GetBytes();

                                    if (queueOutboundMessages && emrIntegrationDispenseReceiptPushEnabled)
                                    {
                                        string pushFilename = string.IsNullOrEmpty(patient_code) ? dispenseReceiptFileName : (patient_code + ".pdf");
                                        PushDispenseReceiptPdf(tempFile, dispensement.Practice.PracticeID, pushFilename, vcc_schedule_id, dispensement.DispenseID);
                                    }

                                    if (pdfMemoryStream != null)
                                        pdfMemoryStream.Write(tempFileBytes, 0, tempFileBytes.Length);
                                }

                                if (savePdfToFile)
                                    tempFile.CopyToPath(pdfParameter.filename);
                            }
                        }

                        if (queueOutboundMessages && emrIntegrationHL7PushEnabled && !string.IsNullOrEmpty(vcc_schedule_id))
                        {
                            var client = new EMRIntegrationReference.EMRIntegrationClient();
                            var dispense_items = new List<OutboundHL7DispenseItem>();
                            var diagnosis_codes = new List<DiagnosisCode>();
                            var patientSignatureDate = dispensement.PatientSignedDate;
                            var physicianSignatureDate = dispensement.PhysicianSignedDate;
                            foreach (var item in dispensement.DispenseItems)
                            {
                                var qty = item.QuantityToDispense.HasValue ? item.QuantityToDispense.Value : 0;
                                decimal unitprice = 0m;
                                unitprice = ConvertCurrencyToDecimal(item.ActualChargeBilled);
                                decimal dmedeposit = 0m;
                                dmedeposit = ConvertCurrencyToDecimal(item.DMEDeposit);
                                dispense_items.Add(
                                    new OutboundHL7DispenseItem
                                    {
                                        unit_price = unitprice,
                                        quantity = qty,
                                        hcpcs_code = item.HCPCs,
                                        item_code = item.Code,
                                        modifier1 = item.LRModifier,
                                        Gender = item.Gender,
                                        Size = item.Size,
                                        dispense_date = dispensement.DateDispensed,
                                        abn_reason = item.AbnReasonText,
                                        is_abn = string.IsNullOrEmpty(item.ABNType) ? false : true,
                                        dme_deposit = dmedeposit,
                                        is_medicare = item.IsMedicare,
                                        item_name = item.ShortName,
                                        supplier_name = item.SupplierName,
                                        patient_signature_date = patientSignatureDate,
                                        physician_signature_date = physicianSignatureDate,
                                    });
                                diagnosis_codes.Add(new DiagnosisCode { Code = item.ICD9Code, Type = DiagnosisCodeType.ICD9 }); // TODO: handle ICD10 in future
                            }
                            client.QueueOutboundHL7Request(dispensement.Practice.PracticeID, vcc_schedule_id, dispense_items.ToArray(), diagnosis_codes.ToArray(), dispensement.DispenseID, dispensement.CloudConnectId);
                        }
                    }
                    catch (Exception e)
                    {
                        VisionErrHandler.Current.RaiseError(e);
                    }

                    #endregion

                    #region build e-mail recipient list

                    var patientMailTo = "";
                    var printDispenseMailTo = "";

                    // add patient
                    if (string.IsNullOrEmpty(dispensement.PatientEmail) == false)
                    {
                        patientMailTo = dispensement.PatientEmail;
                    }

                    // add admin print dispense (only if dispensement is physician signed)
                    if ((string.IsNullOrEmpty(dispensement.EmailForPrintDispense) == false) && dispensement.CreatedWithHandheld.HasValue && (dispensement.CreatedWithHandheld == true))
                    {
                        printDispenseMailTo = dispensement.EmailForPrintDispense;
                    }

                    #endregion

                    #region send e-mail

                    if (sendEmail == true)
                    {
                        var locationName =
                            String.IsNullOrEmpty(dispensement.Practice.PracticeName.Trim())
                                ? String.Empty
                                : (dispensement.Practice.PracticeName.Trim() + " ");
                        var subject = locationName + "DME Dispensement Information";
                        if (!dispensement.IsPhysicianSigned)
                            subject += " without Physician Signature";

                        var sessionProvider = new VisionSessionProvider(practiceLocationID);

                        if (createPDF == false)
                        {
                            if (!string.IsNullOrEmpty(patientMailTo.Trim()))
                                SendEmail("OrderConfirmation", patientMailTo, html, subject, BLL.SecureMessaging.MessageClass.Patient, sessionProvider);
                            if (!string.IsNullOrEmpty(printDispenseMailTo.Trim()))
                                SendEmail("OrderConfirmation", printDispenseMailTo, html, subject, BLL.SecureMessaging.MessageClass.PrintDispensement, sessionProvider);
                        }
                        else
                        {
                            #region render html e-mail body
                            var bodySB = new StringBuilder();
                            var bodySW = new StringWriter(bodySB);
                            var bodyHTMLTW = new HtmlTextWriter(bodySW);
                            try { ctlPatientEmailBody.RenderControl(bodyHTMLTW); }
                            catch { }
                            var bodyHTML = bodySB.ToString();
                            #endregion

#if(DEBUG)
                            if (!string.IsNullOrEmpty(patientMailTo.Trim()))
                                SendEmail("OrderConfirmation", patientMailTo, bodyHTML + "<br/><br/>" + html, string.Format(subject), BLL.SecureMessaging.MessageClass.Patient, sessionProvider);
                            if (!string.IsNullOrEmpty(printDispenseMailTo.Trim()))
                                SendEmail("OrderConfirmation", printDispenseMailTo, bodyHTML + "<br/><br/>" + html, string.Format(subject), BLL.SecureMessaging.MessageClass.PrintDispensement, sessionProvider);
#else
                            if (!string.IsNullOrEmpty(patientMailTo.Trim()))
                                SendEmail("OrderConfirmation", patientMailTo, bodyHTML, subject, filename, BLL.SecureMessaging.MessageClass.Patient, sessionProvider);
                            if (!string.IsNullOrEmpty(printDispenseMailTo.Trim()))
                                SendEmail("OrderConfirmation", printDispenseMailTo, bodyHTML, subject, filename, BLL.SecureMessaging.MessageClass.PrintDispensement, sessionProvider);
#endif
                        }
                    }

                    #endregion

                    #region send fax (only if physician signed (if HCP signature option is enabled) and created by handheld)

                    if ((sendFax == true)
                        && ((practiceLocation.IsHcpSignatureRequired == false) || (dispensement.IsPhysicianSigned == true))
                        && (dispensement.CreatedWithHandheld == true))
                    {
                        var faxnum = (from pl in visionDB.PracticeLocations
                                      where pl.PracticeLocationID == practiceLocationID
                                      select pl.FaxForPrintDispense).FirstOrDefault();
                        var fax_enabled =
                                visionDB.PracticeLocations
                                .Where(pl => pl.PracticeLocationID == practiceLocationID)
                                .Any(pl => pl.Practice.FaxDispenseReceiptEnabled);

                        if (fax_enabled && !string.IsNullOrEmpty(faxnum))
                        {
                            var f = new Fax();
                            if (!createPDF)
                                f.SendFax(faxnum, html);
                            else
                                f.SendFax(faxnum, filename, 0, J2.eFaxDeveloper.FaxFileType.pdf);
                        }
                    }

                    #endregion

                    #region clean up rendered pdf file

                    // NOTE: DO NOT CLEAN UP PDF FILE BECAUSE IT MAY BE QUEUED FOR TRANSMISSION TO EFAX
                    //if (createPDF)
                    //{
                    //    try { File.Delete(filename); }
                    //    catch { }
                    //}

                    #endregion

                    counter++;
                }
            }

            return completeString;
        }

        //TODO: Not so great to write private static methods but need to support the current code structure.
        private static IEnumerable<DispensementDetails> GetItemsDispensed(VisionDataContext visionDB, int[] dispenseIDs)
        {
            return from d in visionDB.Dispenses
                   join pl in visionDB.PracticeLocations on d.PracticeLocationID equals pl.PracticeLocationID
                   join p in visionDB.Practices on pl.PracticeID equals p.PracticeID
                   where dispenseIDs.Contains(d.DispenseID)
                   select new DispensementDetails()
                   {
                       CreatedWithHandheld = d.CreatedWithHandheld,
                       IsPhysicianSigned = (
                          (
                              from dd in visionDB.DispenseDetails
                              where dd.DispenseID == d.DispenseID
                              join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                              join ds in visionDB.DispenseSignatures on dq.DispenseSignatureID equals ds.DispenseSignatureID
                              select new { ds.PhysicianSigned }
                          )
                          .Any(x => x.PhysicianSigned.HasValue && (x.PhysicianSigned.Value == true))
                       ),
                       PhysicianSignedDate =
                           (
                           from dd in visionDB.DispenseDetails
                           where dd.DispenseID == d.DispenseID
                           join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                           join ds in visionDB.DispenseSignatures on dq.DispenseSignatureID equals ds.DispenseSignatureID
                           select ds.PhysicianSignedDate
                           ).FirstOrDefault(),
                       DispenseID = d.DispenseID,
                       DateDispensed = d.DateDispensed,
                       PatientCode = d.PatientCode,
                       PatientEmail = d.PatientEmail,
                       PatientFirstName = d.PatientFirstName,
                       PatientLastName = d.PatientLastName,
                       Practice = p,
                       IsSecureMessagingExternalEnabled = p.IsSecureMessagingExternalEnabled,
                       PracticeLocationName = pl.Name,
                       EMRIntegrationDispenseReceiptPushEnabled = p.EMRIntegrationDispenseReceiptPushEnabled,
                       EMRIntegrationHL7PushEnabled = p.EMRIntegrationHL7PushEnabled,
                       EmailForPrintDispense = pl.EmailForPrintDispense,
                       EMREnabled = p.EMRIntegrationPullEnabled,
					   DispenseDocumentVersion = d.DispenseDocumentVersion,
					   DispenseDocumentFeaturesFlags = d.DispenseDocumentFeaturesFlags,
					   CloudConnectId = (from dd1 in visionDB.DispenseDetails where dd1.DispenseID == d.DispenseID select dd1.Clinician.CloudConnectId).FirstOrDefault(),
                       PhysicianName = (from dd1 in visionDB.DispenseDetails where dd1.DispenseID == d.DispenseID select new PhysicianName { FullName = dd1.Clinician.Contact.FirstName + " " + dd1.Clinician.Contact.LastName }).FirstOrDefault(),
                       PatientRefusalReason = (from dq1 in visionDB.DispenseQueues
                                               join dd2 in visionDB.DispenseDetails on dq1.DispenseQueueID equals dd2.DispenseQueueID
                                               where dd2.DispenseID == d.DispenseID
                                               select dq1.DeclineReason).FirstOrDefault(),
                       FitterName = (from dd in visionDB.DispenseDetails
                                     join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                                     join cl in visionDB.Clinicians on dq.FitterID equals cl.ClinicianID
                                     join c in visionDB.Contacts on cl.ContactID equals c.ContactID
                                     where dd.DispenseID == d.DispenseID
                                     select c.FirstName + ' ' + c.LastName +' '+ c.Suffix).FirstOrDefault(),
                       PatientSignatureInfo = (
                                              from dd in visionDB.DispenseDetails
                                              join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                                              join ds in visionDB.DispenseSignatures on dq.DispenseSignatureID equals ds.DispenseSignatureID
                                              where dd.DispenseID == d.DispenseID && ds.PatientSignature.Length > 10
                                              select new DispenseSignatureMin { DispenseSignatureID = ds.DispenseSignatureID}
                                              ).FirstOrDefault(),
                       PatientSignerRelationship =(from dd in visionDB.DispenseDetails
                                                   join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                                                   join ds in visionDB.DispenseSignatures on dq.DispenseSignatureID equals ds.DispenseSignatureID
                                                   join psr in visionDB.PatientSignatureRelationships on ds.PatientSignatureRelationshipId equals psr.Id
                                                   where dd.DispenseID == d.DispenseID
                                                   select psr.PatientSignatureRelationship1
                                                   ).FirstOrDefault(),
                       PatientSignedDate = (
                                             from dd in visionDB.DispenseDetails
                                             join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                                             join ds in visionDB.DispenseSignatures on dq.DispenseSignatureID equals ds.DispenseSignatureID
                                             where dd.DispenseID == d.DispenseID && ds.PatientSignature.Length > 10
                                             select ds.PatientSignedDate
                                            ).FirstOrDefault(),
                       DispenseItems = (from dd in visionDB.DispenseDetails
                                        join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                                        where dd.DispenseID == d.DispenseID /* && dd.IsCashCollection == false */
                                        select new DispenseItemDto
                                        {
                                            DispenseDetailID = dd.DispenseDetailID,
                                            DispenseQueueID = dd.DispenseQueueID,
                                            IsMedicare = dd.IsMedicare,
                                            ABNForm = dd.ABNForm,
                                            ABNType = dd.ABNType,
                                            Cash = dd.IsCashCollection,
                                            Physician = dd.Clinician.Contact.FirstName + " " + dd.Clinician.Contact.LastName,
                                            Name = dd.PracticeCatalogProduct.IsThirdPartyProduct == true ? dd.PracticeCatalogProduct.ThirdPartyProduct.Name : dd.PracticeCatalogProduct.MasterCatalogProduct.Name,
                                            ShortName = dd.PracticeCatalogProduct.IsThirdPartyProduct == true ? dd.PracticeCatalogProduct.ThirdPartyProduct.ShortName : dd.PracticeCatalogProduct.MasterCatalogProduct.ShortName,
                                            Code = dd.PracticeCatalogProduct.IsThirdPartyProduct == true ? dd.PracticeCatalogProduct.ThirdPartyProduct.Code : dd.PracticeCatalogProduct.MasterCatalogProduct.Code,
                                            LRModifier = dd.LRModifier,
                                            Gender = dd.PracticeCatalogProduct.IsThirdPartyProduct ? dd.PracticeCatalogProduct.ThirdPartyProduct.Gender : dd.PracticeCatalogProduct.MasterCatalogProduct.Gender,
                                            Size = dd.PracticeCatalogProduct.IsThirdPartyProduct == true ? dd.PracticeCatalogProduct.ThirdPartyProduct.Size : dd.PracticeCatalogProduct.MasterCatalogProduct.Size,
                                            HCPCs = dq.HCPCs,
                                            ICD9Code = dd.ICD9Code,
                                            MedicareOption = dq.MedicareOption,
                                            ActualChargeBilled = string.Format("{0:C}", dd.ActualChargeBilled),
                                            DMEDeposit = string.Format("{0:C}", dd.DMEDeposit),
                                            QuantityToDispense = dd.Quantity,
                                            AbnReasonText = dq.ABNReason,
                                            SupplierName = (
                                            dd.PracticeCatalogProduct.IsThirdPartyProduct == false ?
                                                dd.PracticeCatalogProduct.MasterCatalogProduct.MasterCatalogSubCategory.MasterCatalogCategory.MasterCatalogSupplier.SupplierName :
                                                ((
                                                    from pcsb in visionDB.PracticeCatalogSupplierBrands
                                                    join tps in visionDB.ThirdPartySuppliers on pcsb.ThirdPartySupplierID equals tps.ThirdPartySupplierID
                                                    where pcsb.PracticeCatalogSupplierBrandID == dd.PracticeCatalogProduct.PracticeCatalogSupplierBrandID
                                                    select tps

                                                    ).FirstOrDefault()).SupplierShortName
                                            ),
                                            WarrantyInfo = dd.PracticeCatalogProduct.PracticeCatalogSupplierBrand.SupplierWarranty.ProductWarranty
                                        })
                   };
        }

        private static string GetCloudConnectRemoteScheduleItemId(ref string patient_code)
        {
            string vcc_schedule_id = null;
            var prefix = " (SID:";
            var suffix = ")";

            // exit routine early if there is no patient_code
            if (patient_code == null || patient_code.Length < prefix.Length)
                return vcc_schedule_id;

            // get position of sid marker
            var p1 = patient_code.IndexOf(prefix);
            if (p1 >= 0)
            {
                var o = new StringBuilder();
                // copy everything up to the marker
                o.Append(patient_code.Substring(0, p1));
                // read marker all the way through to the right-parenthesis
                var sid = patient_code.Substring(p1);
                var p2 = sid.IndexOf(suffix);
                // cap off marker and "fix" o so it doesn't contain the marker
                if (p2 > -1)
                {
                    // copy everything past end of marker into o
                    o.Append(sid.Substring(p2 + 1));
                    // cap off sid so it's just the sid
                    sid = sid.Substring(0, p2);
                }
                // strip off the prefix of sid
                sid = sid.Replace(prefix, "");

                // replace patient code so it doesn't contain the sid
                patient_code = o.ToString();

                // return the sid to caller
                vcc_schedule_id = sid;
            }
            return vcc_schedule_id;
        }

        public static void PushDispenseReceiptPdf(TempFileManager tempFileManager, int practiceId, string patientCode, string vccScheduleId, int dispenseId)
        {
            var client = new EMRIntegrationClient();
            client.QueueOutboundFile(practiceId, patientCode, tempFileManager.GetBytes(), vccScheduleId, dispenseId);
        }

        public static void CreatePDF(PDFParameter pdfParameter, TempFileManager tempFileManager, byte[][] attachments)
        {
            var client = new CreatePdfClient();
            if (attachments != null && attachments.Length > 0)
            {
                client.CreatePdfWithAttachments(pdfParameter.html, tempFileManager.FullTempfilePath, attachments);
            }
            else
            {
                client.CreatePdf(pdfParameter.html, tempFileManager.FullTempfilePath);
            }
        }

        public static void SendBillingNotificationEmail(int practiceLocationID, bool fromVision = false, InventoryComplete.ConsignmentEmailType emailType = InventoryComplete.ConsignmentEmailType.ConsignmentInventoryComplete)
        {
            InventoryDiscrepencyItem[] discrepencies = ConsignmentItem.GetConsignmentReplenishmentList(practiceLocationID);

            if (discrepencies.Length > 0)
            {

                string html = GetConsignmentEmailHtml(practiceLocationID, fromVision, emailType);


                var MailTo = GetConsignmentEmailAddress();

                if (string.IsNullOrEmpty(MailTo.Trim()) == false)
                {
                    SendEmail("OrderConfirmation", MailTo, html, "DME Dispensement Information - Billing", BLL.SecureMessaging.MessageClass.Billing, new VisionSessionProvider(practiceLocationID));
                }

            }
        }

        private static string GetConsignmentEmailAddress()
        {
            //    //--mws was turned on
            var MailTo = "";

            MailTo = System.Configuration.ConfigurationManager.AppSettings["ConsignmentAdminEmailAddress"];

#if(DEBUG)
            MailTo += ";klord@breg.com";
            if (!MailTo.ToLower().Contains("sneen"))
            {
                MailTo += ";michael_sneen@yahoo.com";
            }
#endif
            return MailTo;
        }

        public static string GetConsignmentEmailHtml(int practiceLocationID, bool fromVision, InventoryComplete.ConsignmentEmailType emailType = InventoryComplete.ConsignmentEmailType.ConsignmentInventoryComplete)
        {
            string path = "";
            string templateName = "~/{0}EmailTemplates/InventoryComplete.ascx";
            string virtualDirName = "";
            string html = "";

            if (fromVision == true)
            {
                virtualDirName = "BregWcfService/";
            }

            path = string.Format(templateName, virtualDirName);

            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var practiceQuery = from p in visionDB.Practices
                                    join pl in visionDB.PracticeLocations on p.PracticeID equals pl.PracticeID
                                    where pl.PracticeLocationID == practiceLocationID
                                    select new
                                    {
                                        PracticeName = p.PracticeName,
                                        PracticeLocationName = pl.Name
                                    };

                var practiceInfo = practiceQuery.FirstOrDefault();

                Page page = new Page();
                var ctlAll = new System.Web.UI.WebControls.Panel();

                //string x = BLL.Website.GetAbsPath(path); ;

                BregWcfService.EmailTemplates.InventoryComplete inventoryComplete = page.LoadControl(path) as BregWcfService.EmailTemplates.InventoryComplete;
                inventoryComplete.PracticeName = practiceInfo.PracticeName;
                inventoryComplete.PracticeLocationName = practiceInfo.PracticeLocationName;
                inventoryComplete.PracticeLocationID = practiceLocationID;
                inventoryComplete.EmailType = emailType;

                inventoryComplete.LoadControlInfo();
                ctlAll.Controls.Add(inventoryComplete);
                ctlAll.Controls.Add(new LiteralControl("<br/><br/>"));

                var sb = new StringBuilder();
                var sw = new StringWriter(sb);
                var htmlTW = new HtmlTextWriter(sw);
                ctlAll.RenderControl(htmlTW);

                html = sb.ToString();

            }
            return html;
        }

        //TODO: This method needs to be optimized there are code clones for this method in the DispenseReceptBuilder Project
        public static decimal ConvertCurrencyToDecimal(string currency)
        {
            decimal decimalValue;
            if (decimal.TryParse(currency, NumberStyles.Currency, NumberFormatInfo.CurrentInfo, out decimalValue))
            {
                return decimalValue;
            }
            return -1;
        }

        public static byte[][] GetActiveCustomDocuments(int practiceId)
        {
            var ret = new List<byte[]>();
            var client = new BlobService.BlobServiceClient();

            using (var db = VisionDataContext.GetVisionDataContext())
            {
                var activeCustomForms = from cf
                                        in db.GetTable<DispenseReceiptCustomForm>()
                                        where cf.PracticeId == practiceId
                                        where cf.IsActive
                                        orderby cf.BlobOrder
                                        select cf;

                foreach (var form in activeCustomForms)
                {
                    var path = practiceId + "\\" + form.Name;
                    var file = client.GetFile(path);
                    ret.Add(file);
                }
            }

            return ret.ToArray();
        }
    }
}
