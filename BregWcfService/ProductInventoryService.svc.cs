﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using BregWcfService.DataContracts;
using ClassLibrary.DAL.Helpers;

namespace BregWcfService
{
    /*
     * This service is was created because the payload of the product getinvnetorylandingdata call in large practices could not be handled by the xamarin WCF client 
     * 
     * The second reason is there is a general goal of moving all services away from Wcf SOAP to Rest. Ideally a full move to Web api but moving things to WCF Rest is a good starting point
     * 
     * If additional methods are copied out of VisionService.svc they should be moved to a new Wcf Service based on functional breakdown. 
     * 
     * E.G. if a dispensement method is moved it should be moved to a new WCF service called DispensementService.svc In this way would not want to create all of the functions in one service because 
     * Rest does not require a soap wsdl contract to be known by the client. Therefore, having many different service files does not become cumbersome to consume like it would for WCF Soap. 
     * 
     * Eventually we will have a rest api structure that will look something like
     *  /inventory
     *  /dispensement
     *  /orders
     *  /users
     *  /practice
     *  etc. 
     *  
     *  Slowly moving functions to new wcf services will ease the process of moving to Web API in the future.
     *  
    */
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ProductInventoryService : IProductInventoryService
    {
        public ProductUpdateWrapper GetInventoryLandingData(int practiceId, int practiceLocationID, string userName, string password, string deviceID, string lastLocation)
        {
            var startTime = DateTime.Now;

            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            var productInventory = LandingData.GetInventoryLandingData(practiceId, practiceLocationID);
            var ret = new ProductUpdateWrapper()
            {
                ProductInventory = productInventory,
                LatestSerialNumber = ProductUpdateWrapper.GetLatestSerialNumber(practiceLocationID, practiceId),
                TotalServerTime = (DateTime.Now - startTime).TotalSeconds
            };

            return ret;
        }
    }
}
