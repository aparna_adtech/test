﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Channels;

namespace BregWcfService.HeaderClasses
{
    public class ContextPusher : IClientMessageInspector
    {

        public void AfterReceiveReply(ref Message reply, object correlationState)
        {

            // Do nothing.

        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {

            MessageHeader myHeader = MessageHeader.CreateHeader("MyHeader", "ns", "HeaderValue");

            request.Headers.Add(myHeader);

            return null;

        }

    }
}