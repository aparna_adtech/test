﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.ServiceModel;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Channels;

namespace BregWcfService.HeaderClasses
{
    public class CustomMessageInspector : IDispatchMessageInspector, IClientMessageInspector
    {

        #region Message Inspector of the Service

        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            // Look for my custom header in the request
            Int32 headerPosition = request.Headers.FindHeader(CustomHeaderNames.CustomHeaderName, CustomHeaderNames.CustomHeaderNamespace);

            // Get an XmlDictionaryReader to read the header content
            XmlDictionaryReader reader = request.Headers.GetReaderAtHeader(headerPosition);

            // Read it through its static method ReadHeader
            CustomHeader header = CustomHeader.ReadHeader(reader);

            // Add the content of the header to the IncomingMessageProperties dictionary
            OperationContext.Current.IncomingMessageProperties.Add("key", header.Key);

            return null;
        }

        public void BeforeSendReply(ref Message reply, object correlationState)
        {
        }

        #endregion

        #region Message Inspector of the Consumer

        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            // Prepare the request message copy to be modified
            MessageBuffer buffer = request.CreateBufferedCopy(Int32.MaxValue);
            request = buffer.CreateMessage();

            // Simulate to have a random Key generation process
            request.Headers.Add(new CustomHeader(Guid.NewGuid().ToString()));

            return null;
        }

        #endregion
    }
}