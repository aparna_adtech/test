﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;



namespace BregWcfService.HeaderClasses
{
    public class CustomBehaviorExtensionElement : BehaviorExtensionElement
    {
        protected override object CreateBehavior()
        {
            return new CustomBehaviorAttribute();
        }

        public override Type BehaviorType
        {
            get
            {
                return typeof(CustomBehaviorAttribute);
            }
        }

    }
}