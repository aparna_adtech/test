﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BregWcfService.HeaderClasses
{
    public static class CustomHeaderNames
    {
        public const String CustomHeaderName = "CustomHeader";

        public const String KeyName = "Key";

        public const String CustomHeaderNamespace = "http://schemas.BregVision.com/CustomHeader";

    }
}