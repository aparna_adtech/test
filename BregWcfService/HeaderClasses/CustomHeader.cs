﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace BregWcfService.HeaderClasses
{
    public class CustomHeader : MessageHeader
    {
        private String _key;

        public String Key
        {
            get
            {
                return (this._key);
            }
        }

        public CustomHeader(String key)
        {
            this._key = key;
        }

        public override string Name
        {
            get { return (CustomHeaderNames.CustomHeaderName); }
        }

        public override string Namespace
        {
            get { return (CustomHeaderNames.CustomHeaderNamespace); }
        }

        protected override void OnWriteHeaderContents(System.Xml.XmlDictionaryWriter writer, MessageVersion messageVersion)
        {
            // Write the content of the header directly using the XmlDictionaryWriter
            writer.WriteElementString(CustomHeaderNames.KeyName, this.Key);
        }

        public static CustomHeader ReadHeader(XmlDictionaryReader reader)
        {
            // Read the header content (key) using the XmlDictionaryReader
            if (reader.ReadToDescendant(CustomHeaderNames.KeyName, CustomHeaderNames.CustomHeaderNamespace))
            {
                String key = reader.ReadElementString();
                return (new CustomHeader(key));
            }
            else
            {
                return null;
            }
        }
    }
}