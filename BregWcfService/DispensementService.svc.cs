﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;

namespace BregWcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "DispensementService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select DispensementService.svc or DispensementService.svc.cs at the Solution Explorer and start debugging.

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class DispensementService : IDispensementService
    {
        public PatientSignatureRelationship[] GetPatientSignatureRelations(string userName, string password, string deviceID, string lastLocation)
        {
            PatientSignatureRelationship[] wrapper = GetPatientSignatureRelations1(userName, password, deviceID, lastLocation);
            return wrapper;
        }
        public PatientSignatureRelationship[] GetPatientSignatureRelations1(string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);
            return PatientSignatureRelationship.GetPatientSignatureRelationshipData();
        }
    }
}
