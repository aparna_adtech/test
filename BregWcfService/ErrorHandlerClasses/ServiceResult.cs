﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Web.UI.WebControls;
using BregWcfService.DataContracts;

namespace BregWcfService.ErrorHandlerClasses
{
    [DataContract]
    public class ServiceResult<T>
    {
        [DataMember]
        public string ExceptionDetails { get; set; }
        [DataMember]
        public Boolean IsSuccessful { get; set; }

        [DataMember]
        public T Data { get; set; }

        public ServiceResult(Boolean isSuccessful, string details )
        {
            ExceptionDetails = details;
            IsSuccessful = isSuccessful;
        }

        public ServiceResult(Boolean isSuccessful, string details, T data)
        {
            ExceptionDetails = details;
            IsSuccessful = isSuccessful;
            Data = data;
        }
    }

    [DataContract]
    public class ServiceResult
    {
        [DataMember]
        public string ExceptionDetails { get; set; }
        [DataMember]
        public Boolean IsSuccessful { get; set; }
        
        public ServiceResult(Boolean isSuccessful, string details)
        {
            ExceptionDetails = details;
            IsSuccessful = isSuccessful;
        }

     
    }
}