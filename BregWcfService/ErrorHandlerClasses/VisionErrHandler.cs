using System;
using System.Web;
using System.Diagnostics;
using System.Web.Mail;
using System.Configuration;
using System.Web.SessionState;
using System.IO;
using System.Text;
using System.Web.UI;
using log4net;
using log4net.Config;


namespace BregWcfService
{
    public class VisionErrHandler
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(VisionErrHandler));

        public string MailSubject = "Auto-generated error message";

        private static VisionErrHandler _Current;
        public static VisionErrHandler Current
        {
            get
            {
                if (_Current == null)
                    _Current = new VisionErrHandler();
                return VisionErrHandler._Current;
            }
            set { VisionErrHandler._Current = value; }
        }

        public VisionErrHandler()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        public virtual void RaiseError(Exception ext, string appendHTML = "")
        {
            try
            {
                var sb = new StringBuilder();

                sb.Append("<h3><b>Error in Vision</b></h3>");

                var ex = ext;
                var level = 0;
                while (ex != null)
                {
                    var fmt = "<div style='white-space: pre; font-family: monospace; margin: 10px;'><span style='font-weight: bold;'>{0}:</span>\r\n{1}</div>";
                    sb.Append("<div style='border-left: 4px solid #efefef; margin: 5px 0;'><div style='background-color: #efefef;'>Exception Level " + level.ToString() + "</div>");
                    sb.AppendFormat(fmt, "Message", ex.Message);
                    sb.AppendFormat(fmt, "Source", ex.Source);
                    sb.AppendFormat(fmt, "TargetSite", ex.TargetSite);
                    sb.AppendFormat(fmt, "StackTrace", ex.StackTrace);
                    sb.AppendFormat(fmt, "Frame", "<pre>" + _GetStackFrameForException(ex) + "</pre>");
                    sb.Append("</div>");
                    level++;
                    ex = ex.InnerException;
                }

                try
                {
                    var fmt = @"<div style='margin-top: 20px;'>IPAddress = <span style='font-weight: bold;'>{0}</span></div>";
                    if (HttpContext.Current != null && HttpContext.Current.Session != null)
                    {
                        string ip = HttpContext.Current.Session["IPAddress"].ToString();
                        sb.Append(string.Format(fmt, ip));
                    }
                    else if (System.ServiceModel.OperationContext.Current != null)
                    {
                        var ctx = System.ServiceModel.OperationContext.Current;
                        var messageProperties = ctx.IncomingMessageProperties;
                        var endpointProperty =
                            messageProperties[System.ServiceModel.Channels.RemoteEndpointMessageProperty.Name]
                            as System.ServiceModel.Channels.RemoteEndpointMessageProperty;
                        sb.Append(string.Format(fmt, endpointProperty.Address));
                    }
                }
                catch { }

                if (!string.IsNullOrEmpty(appendHTML))
                {
                    var fmt = @"<div style='margin-top: 20px; font-weight: bold;'>{0}</div>";
                    sb.AppendFormat(fmt, appendHTML);
                }

                string result = sb.ToString();
                RaiseError(result, ext);
            }
            catch
            {
                RaiseError(ext.ToString(), ext);
            }
        }

        public virtual void RaiseError(string _message, Exception ex)
        {
            try
            {
                // try to log error via log4net, limiting to first 4000 characters of the message
                logger.Error(
                    (_message.Length > 4000) ? _message.Substring(0, 4000) : _message,
                    ex);
            }
            catch
            {
                // if logging failure, try sending e-mail
                VisionErrHandler.Current.SendMail(_message);
            }
        }

        public void SendMail(string _message)
        {
            try
            {
                string emailReciever = ConfigurationManager.AppSettings["MailReciever"];
                BregWcfService.Classes.Email.SendEmail("OrderConfirmation", emailReciever, _message, this.MailSubject, BLL.SecureMessaging.MessageClass.BregMaintenance);
            }
            catch { /* sending e-mail is failsafe, if this fails as well, we will ignore */ }
        }

        private string _GetStackFrameForException(Exception ex)
        {
            try
            {
                var sb = new StringBuilder();

                var t = new System.Diagnostics.StackTrace(ex, true);
                foreach (var f in t.GetFrames())
                    sb.AppendLine(string.Format(@"{0}: {1} (line {2})", f.GetFileName(), f.GetMethod(), f.GetFileLineNumber()));

                return sb.ToString();
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
