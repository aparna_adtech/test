﻿using System;
using System.IO;
using System.Linq;
using PushDispenseReceiptPdfBot.EMRIntegration;


namespace PushDispenseReceiptPdfBot
{
    class PushDispenseReceiptPdf
    {
        static void Main()
        {
            var client = new EMRIntegrationClient();
            var queItems = client.GetOutboundQueueItems();
            foreach (var outboundQueueItem in queItems)
            {

                try
                {
                    if (string.IsNullOrEmpty(outboundQueueItem.Filename))
                    {
                        continue;
                    }

                    string filename = outboundQueueItem.Filename;
                    filename = CleanseFileName(filename);

                    var practiceOutFolder = client.GetQueueProcessorStoragePathForPracticeId(outboundQueueItem.PracticeID);
                    var qualifiedFileName = Path.Combine(practiceOutFolder,filename);

                    
                    if (File.Exists(qualifiedFileName))
                    {
                        continue;
                    }

                    var fileData =  client.GetOutboundFileContentsByOutboundQueueID(outboundQueueItem.ID);

                    var file = new FileStream(qualifiedFileName, FileMode.CreateNew);
                    file.Write(fileData, 0,Enumerable.Count<byte>(fileData));
                    file.Flush();
                    file.Close();

                    client.SetOutboundQueueItemProcessed(outboundQueueItem.ID);
                }
                catch (Exception e)
                {
                    //TODO: Send an email to BregError dl and log to db
                    Console.WriteLine(e);
                }
            }
        }

        public static string CleanseFileName(string filename)
        {
            char[] invalidPathChars = Path.GetInvalidPathChars();

            foreach (var invalidPathChar in invalidPathChars)
            {
                if (filename.Contains(invalidPathChar))
                {
                    filename = filename.Replace(invalidPathChar, '_');
                }
            }

            char[] invalidFileNameChars = Path.GetInvalidFileNameChars();

            foreach (var invalidFileNameChar in invalidFileNameChars)
            {
                if (filename.Contains(invalidFileNameChar))
                {
                    filename = filename.Replace(invalidFileNameChar, '_');
                }
            }
            return filename;
        }
    }
}
