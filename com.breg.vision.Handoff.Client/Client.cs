﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.Client
{
    public class Client
    {
        public static string GetBearerToken(string username, string password, Uri baseUri)
        {
            var httpClient = new HttpClient();
            httpClient.BaseAddress = baseUri;

            var content =
                new FormUrlEncodedContent(
                    new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("grant_type", "password"),
                        new KeyValuePair<string, string>("username", username),
                        new KeyValuePair<string, string>("password", password)
                    });

            var responseTask = httpClient.PostAsync("token", content);
            responseTask.Wait();

            if (responseTask.Result.IsSuccessStatusCode)
            {
                var get_response_json_task = responseTask.Result.Content.ReadAsStringAsync();
                get_response_json_task.Wait();

                var token_response_object = (ClientModels.TokenResponseModel)Newtonsoft.Json.JsonConvert.DeserializeObject<ClientModels.TokenResponseModel>(get_response_json_task.Result);

                if (token_response_object != null)
                    return token_response_object.access_token;
            }

            return null;
        }
    }
}
