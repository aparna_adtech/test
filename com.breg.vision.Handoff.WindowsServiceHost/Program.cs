﻿using System;
using System.Configuration.Install;
using System.Reflection;
using System.ServiceProcess;
using System.Threading;

namespace com.breg.vision.Handoff.WindowsServiceHost
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            if (System.Environment.UserInteractive)
            {
                AllocConsole();

                if (args.Length > 0)
                {
                    switch (args[0])
                    {
                        case "-i":
                            Console.WriteLine("Installing service...");
                            ManagedInstallerClass.InstallHelper(new string[] { Assembly.GetExecutingAssembly().Location });
                            break;
                        case "-u":
                            Console.WriteLine("Uninstalling service...");
                            ManagedInstallerClass.InstallHelper(new string[] { "/u", Assembly.GetExecutingAssembly().Location });
                            break;
                    }

                    Console.WriteLine("Press any key to exit...");
                    Console.ReadKey(true);
                    return;
                }
                else
                {
                    // if not one of the above install/uninstall options, run interactively
                    RunInteractively();
                }
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                   new HandoffWorkerService()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }

        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        private static extern bool AllocConsole();

        private static void RunInteractively()
        {
            Action<string, object> logger = (msg, sender) => Console.WriteLine($"[{sender.GetType().FullName}]: {msg}");

            var cts = new CancellationTokenSource();
            var broker = new HandoffWorkerBroker();

            Console.WriteLine("Press any key to terminate the application...\n");

            Thread t2 = new Thread(new ParameterizedThreadStart(broker.StartProcess));
            t2.Start(cts.Token);

            Console.ReadKey(true);
            Console.WriteLine("!!! TERMINATING !!!!");
            cts.Cancel();

            t2.Join();
            cts.Dispose();

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey(true);
        }
    }
}
