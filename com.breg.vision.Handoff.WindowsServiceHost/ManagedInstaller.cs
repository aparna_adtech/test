﻿using System.ComponentModel;
using System.Diagnostics;
using System.ServiceProcess;

namespace com.breg.vision.Handoff.WindowsServiceHost
{
    [RunInstaller(true)]
    public partial class ManagedInstaller : System.Configuration.Install.Installer
    {
        public ManagedInstaller()
        {
            var EventLogInstaller =
                new EventLogInstaller()
                {
                    Source = @"Handoff.WindowsServiceHost",
                    Log = @"Application"
                };
            var ProcessInstaller =
                new ServiceProcessInstaller
                {
                    Account = ServiceAccount.LocalService
                };
            var ServiceInstaller =
                new ServiceInstaller
                {
                    DisplayName = @"Vision Handoff Worker Service",
                    StartType = ServiceStartMode.Automatic,
                    Description = @"Processes Vision Handoff jobs.",
                    ServiceName = @"HandoffWindowsServiceHost"
                };

            Installers.Add(EventLogInstaller);
            Installers.Add(ProcessInstaller);
            Installers.Add(ServiceInstaller);
        }
    }
}
