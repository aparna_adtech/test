﻿using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Threading;
using com.breg.vision.Handoff.Worker;
using System.Configuration;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace com.breg.vision.Handoff.WindowsServiceHost
{
    public class HandoffWorkerBroker
    {
        private static string RabbitMQHostUri = ConfigurationManager.AppSettings["RabbitMQServiceUrl"];
        private static int NumberOfWorkerThreads = int.Parse(ConfigurationManager.AppSettings["HandoffWorker.MaxConcurrentRequests"] ?? "10");

        public EventLog Logger { get; internal set; }

        private void WriteLog(string msg)
        {
            if (Logger != null)
                Logger.WriteEntry(msg);
            else
                Console.WriteLine(msg);
        }

        public void StartProcess(object ct)
        {
            CheckRSAStatus();

            CancellationToken cancellationToken = (ct is CancellationToken) ? (CancellationToken)ct : CancellationToken.None;

            var factory = new ConnectionFactory() { HostName = RabbitMQHostUri };
            factory.AutomaticRecoveryEnabled = true;

            using (var connection = factory.CreateConnection())
            {
                WriteLog($"HandoffWorkerBroker starting, spinning up {NumberOfWorkerThreads} threads");
                List<Thread> workers = new List<Thread>();

                for (int i = 0; i < NumberOfWorkerThreads; i++)
                {
                    var t = new Thread(new ParameterizedThreadStart(StartRMQChannel));
                    t.Start(new RMQChannelParameters(connection, cancellationToken, WriteLog));

                    workers.Add(t);
                }

                WriteLog($"HandoffWorkerBroker threads running");
                while (cancellationToken.IsCancellationRequested == false)
                    Thread.Sleep(1000);

                WriteLog($"HandoffWorkerBroker stopping, waiting for threads to exit");
                foreach (var worker in workers)
                    worker.Join();
            }
        }

        private void CheckRSAStatus()
        {
            var rsa_status = Worker.HandoffWorker.TestRSAEnvironment() ? "passed" : "failed";
            WriteLog($"Handoff RSA Environment test status: {rsa_status}");
        }

        private class RMQChannelParameters
        {
            public IConnection connection { get; set; }
            public CancellationToken cancellationToken { get; set; }
            public Action<string> loggingFunc { get; private set; }

            public RMQChannelParameters(IConnection conn, CancellationToken token, Action<string> logfunc)
            {
                connection = conn;
                cancellationToken = token;
                loggingFunc = logfunc;

                // make sure we have a default if null is passed
                if (loggingFunc == null)
                    loggingFunc = (msg) => Console.WriteLine(msg);
            }
        }

        private static void StartRMQChannel(object parameters)
        {
            var parameter_data_unboxed = (RMQChannelParameters)parameters;
            var connection = parameter_data_unboxed.connection;
            var cancellationToken = parameter_data_unboxed.cancellationToken;

            var count = 0;
            parameter_data_unboxed.loggingFunc($"Start channel listener on thread ID {Thread.CurrentThread.ManagedThreadId}");

            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "HandoffQueue",
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                channel.BasicQos(0, 1, false);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var handoff_token = Encoding.UTF8.GetString(body);
                    parameter_data_unboxed.loggingFunc($"[{Thread.CurrentThread.ManagedThreadId}] received token {handoff_token}");

                    try
                    {
                        var worker = new HandoffWorker();
                        worker.ProcessJob(handoff_token);

                        channel.BasicAck(ea.DeliveryTag, false);
                        parameter_data_unboxed.loggingFunc($"[{Thread.CurrentThread.ManagedThreadId}] processed token {handoff_token}");
                    }
                    catch(Exception ex)
                    {
                        // NOTE: ProcessJob is supposed to handle all processing errors leaving us with other errors that are assumed to be retry-able...
                        channel.BasicReject(ea.DeliveryTag, true);
                        parameter_data_unboxed.loggingFunc($@"[{Thread.CurrentThread.ManagedThreadId}] error encountered while processing token {handoff_token}

{ex.GetType().FullName}
{ex.Message}
{ex.StackTrace}");
                    }

                    count++;
                };
                channel.BasicConsume(queue: "HandoffQueue",
                                     noAck: false,
                                     consumer: consumer);

                while (cancellationToken.IsCancellationRequested == false)
                    Thread.Sleep(1000);
            }

            parameter_data_unboxed.loggingFunc($"Stopping channel listener on thread ID {Thread.CurrentThread.ManagedThreadId}... {count} message(s) processed");
        }
    }
}
