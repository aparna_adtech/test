﻿using com.breg.vision.bcsintegration.BCSCacheDataProcessing;
using com.breg.vision.bcsintegration.BCSVisionDataProcessing;
using com.breg.vision.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.bcsintegration.BCSVisionDataProcessor
{
    class Program
    {
        public static Guid _CorrelationId = Guid.NewGuid();
        public static FileLogger _Logger = new FileLogger();
        public static SQLLogger _sqlLogger = new SQLLogger();

        static void Main(string[] args)
        {
            var entrypoint = args[0];
            if (!string.IsNullOrEmpty(entrypoint))
                try
                {
                    Console.WriteLine($"*********** Run Started At {DateTime.Now} *********** ", _CorrelationId, _Logger);
                    _Logger.WriteLog($"*********** Run Started At {DateTime.Now} *********** ", null, "com.breg.vision.bcsintegration.BCSVisionDataProcessor", _CorrelationId);
                    if (entrypoint.Equals("BCSCache"))
                    {
                        Console.WriteLine($"Starting the BCS Cache Sync at {DateTime.Now}");
                        _Logger.WriteLog($"Starting the BCS Cache Sync \n", null, "com.breg.vision.bcsintegration.BCSVisionDataProcessor", _CorrelationId);
                        new BCSCacheSync(_CorrelationId, _Logger,_sqlLogger).UpdateBCSCache();
                        _Logger.WriteLog($"Ended BCS Cache Sync at {DateTime.Now}", null, "com.breg.vision.bcsintegration.BCSVisionDataProcessor", _CorrelationId);
                        Console.WriteLine($"Ended the BCS Cache Sync \n", _CorrelationId, _Logger);
                    }
                    else if (entrypoint.Equals("BCSSync"))
                    {
                        Console.WriteLine($"Starting the BCS  to Vision Sync at {DateTime.Now}");
                        _Logger.WriteLog($"Starting the BCS to Vision Sync", null, "com.breg.vision.bcsintegration.BCSVisionDataProcessor", _CorrelationId);
                        new BCSToVision(_CorrelationId, _Logger).SyncCacheToVision();
                        _Logger.WriteLog($"Ended BCS to Vision Sync \n", null, "com.breg.vision.bcsintegration.BCSVisionDataProcessor", _CorrelationId);
                        Console.WriteLine($"Ending the BCS  to Vision Sync at {DateTime.Now}");

                    }
                    _Logger.WriteLog($"Synchronization done and returned to main \n", null, "com.breg.vision.bcsintegration.BCSVisionDataProcessor", _CorrelationId);
                    Console.WriteLine($"Synchronization done.\n");
                }
                catch (BCSDataSyncException error)
                {
                    _Logger.WriteLog(error.Message, null, "com.breg.vision.bcsintegration.BCSVisionDataProcessor", _CorrelationId);
                    _Logger.NonAsyncTraceLogger(error.Message, null, "com.breg.vision.bcsintegration.BCSVisionDataProcessor");
                }
                catch (Exception error)
                {
                    _Logger.WriteLog(error.Message, error, "com.breg.vision.bcsintegration.BCSVisionDataProcessor", _CorrelationId);
                    _Logger.NonAsyncTraceLogger(error.Message, error, "com.breg.vision.bcsintegration.BCSVisionDataProcessor");
                }
                finally
                {
                    _Logger.WriteLog($"***********  Run Ended At {DateTime.Now} ***********", null, "com.breg.vision.bcsintegration.BCSVisionDataProcessor", _CorrelationId);
                    _Logger.AwaitAllTasks(_Logger.Tasks);

                    _Logger.Applicationpointer.Flush();
                    _Logger.Applicationpointer.Close();

                    _sqlLogger.SQLpointer.Flush();
                    _sqlLogger.SQLpointer.Close();
                   
                }


        }
    }
}
