﻿using com.breg.vision.Handoff.Models;
using com.breg.vision.MongoDataModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace com.breg.vision.Handoff.Models.MongoDB
{
    public class MongoHandoffModel : Entity
    {
        public string Token { get; set; }
        public JobStatus Status { get; set; }
        public HandoffRequestModel Request { get; set; }
        public HandoffResponseModel Response { get; set; }

        public MongoHandoffModel()
        {
            Status = JobStatus.Pending;
        }

        public void SetStatusFailedUnknownExecutor()
        {
            SetStatusFailedWithFailureMessages(DateTime.Now, new[] { $"No executor for requested job type: {Request.JobType.ToString()}" });
        }

        public void SetStatusFailedInvalidRequest()
        {
            SetStatusFailedWithFailureMessages(DateTime.Now, new[] { $"Invalid request" });
        }

        public void SetStatusFailedWithFailureMessages(DateTime start_timestamp, IList<string> failureMessages)
        {
            this.Status = JobStatus.Failed;
            this.Response =
                new HandoffResponseModel
                {
                    ExecutionTime = GetExecutionTime(start_timestamp),
                    FailureMessages = (failureMessages != null) ? failureMessages.ToArray() : null
                };
        }

        public void SetStatusCompletedWithResponsePayload(DateTime start_timestamp, string responsePayload)
        {
            Status = JobStatus.Completed;
            Response =
                new HandoffResponseModel
                {
                    ExecutionTime = GetExecutionTime(start_timestamp),
                    ResponsePayload = responsePayload
                };
        }

        private static int GetExecutionTime(DateTime start_timestamp)
        {
            return Convert.ToInt32(Math.Ceiling((DateTime.Now - start_timestamp).TotalSeconds));
        }
    }
}