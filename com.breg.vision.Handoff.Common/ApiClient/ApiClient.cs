﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace com.breg.vision.Handoff.ApiClient
{
    public class ApiResponse
    {
        public bool IsSuccess { get; set; }
        public string Content { get; set; }
    }

    public class ApiClient
    {
        private const string DataType = "application/json";
        public async Task<ApiResponse> Post<T>(string url, T data, string HandoffAuthToken = null, bool isHandoffCall = false)
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = null;
                try
                {
                    client.BaseAddress = new Uri(url);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(DataType));

                    if(isHandoffCall)
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", HandoffAuthToken);

                    StringContent content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, DataType);
                    response = await client.PostAsync(url, content);

                    return Task.FromResult<ApiResponse>(new ApiResponse() {
                        IsSuccess = response.IsSuccessStatusCode,
                        Content = response.Content.ReadAsStringAsync().Result
                    }).Result;
                }
                catch(Exception ex)
                {
                    return Task.FromResult<ApiResponse>(new ApiResponse()
                    {
                        IsSuccess = false,
                        Content = ex.Message
                    }).Result;
                }
            }
        }

        public async Task<ApiResponse> Put<T>(string url, T data, bool serializationRequired = true)
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = null;
                try
                {
                    client.BaseAddress = new Uri(url);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(DataType));

                    StringContent content = new StringContent(serializationRequired ? JsonConvert.SerializeObject(data) : Convert.ToString(data), Encoding.UTF8, DataType);
                    response = await client.PutAsync(url, content);

                    return Task.FromResult<ApiResponse>(new ApiResponse()
                    {
                        IsSuccess = response.IsSuccessStatusCode,
                        Content = response.Content.ReadAsStringAsync().Result
                    }).Result;

                }
                catch(Exception ex)
                {
                    return Task.FromResult<ApiResponse>(new ApiResponse()
                    {
                        IsSuccess = false,
                        Content = ex.Message
                    }).Result;
                }
            }
        }

        public async Task<ApiResponse> Get(string url)
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = null;
                try
                {
                    client.BaseAddress = new Uri(url);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(DataType));

                    response = await client.GetAsync(url);
                    return Task.FromResult<ApiResponse>(new ApiResponse()
                    {
                        IsSuccess = response.IsSuccessStatusCode,
                        Content = response.Content.ReadAsStringAsync().Result
                    }).Result;
                }
                catch(Exception ex)
                {
                    return Task.FromResult<ApiResponse>(new ApiResponse()
                    {
                        IsSuccess = false,
                        Content = ex.Message
                    }).Result;
                }
            }
        }
    }
}
