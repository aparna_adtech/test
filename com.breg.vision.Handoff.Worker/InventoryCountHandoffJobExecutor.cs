﻿using com.breg.vision.Handoff.ClientModels;
using com.breg.vision.Handoff.ClientModels.VisionServiceReference;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.Worker
{
    internal class InventoryCountHandoffJobExecutor : BaseHandoffJobExecutor, IHandoffJobExecutor
    {
        public Task<ProcessRequestResponseModel> ProcessRequestPayload(string payload)
        {
            var tcs = new TaskCompletionSource<ProcessRequestResponseModel>();
            InventoryCountPayloadModel payload_model = null;

            // try to decode the payload as an InventoryCountPayloadModel
            try
            {
                payload_model = Newtonsoft.Json.JsonConvert.DeserializeObject<InventoryCountPayloadModel>(payload);
            }
            catch (Exception ex)
            {
                tcs.SetResult(
                    new ProcessRequestResponseModel
                    {
                        IsSuccess = false,
                        FailureMessages = new[] { "Unable to deserialize payload", JsonConvert.SerializeObject(ex) }
                    });
            }

            // submit the payload to the web service
            if (payload_model != null)
            {
                var client = GetVisionServiceClient();
                client.AddInventoryCount1Completed +=
                    (s, e) =>
                    {
                        if (e.Cancelled)
                            tcs.SetResult(new ProcessRequestResponseModel { IsSuccess = false, FailureMessages = new[] { "Request cancelled" } });
                        else if (e.Error != null)
                            tcs.SetResult(new ProcessRequestResponseModel { IsSuccess = false, FailureMessages = new[] { JsonConvert.SerializeObject(e.Error) } });
                        else
                            tcs.SetResult(new ProcessRequestResponseModel { IsSuccess = true, ResponsePayload = JsonConvert.SerializeObject(e.Result) });
                    };

                client.AddInventoryCount1Async(payload_model.PracticeLocationID, payload_model.Username, payload_model.Password, payload_model.DeviceID, payload_model.LastLocation, payload_model.InventoryCycleType, payload_model.InventoryCycle, new System.Collections.ObjectModel.ObservableCollection<InventoryItemCount>(payload_model.InventoryItemCounts));
            }

            return tcs.Task;
        }
    }
}
