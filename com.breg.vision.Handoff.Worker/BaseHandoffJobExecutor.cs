using com.breg.vision.Handoff.ClientModels.VisionServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using com.breg.vision.Logger;

namespace com.breg.vision.Handoff.Worker
{
    public abstract class BaseHandoffJobExecutor
    {
        private readonly FileLogger logger;

        public BaseHandoffJobExecutor()
        {
            logger = new FileLogger();
        }

        internal VisionServiceClient GetVisionServiceClient()
        {
            var serviceUrl = System.Configuration.ConfigurationManager.AppSettings["VisionServiceURL"] ?? "https://stage1-switch.bregvision.com/api/V2/BregWcfService/VisionService.svc";

            bool isSSL = true;
            try
            {
                isSSL = new Uri(serviceUrl).Scheme.ToLower().Equals("http") ? false : true;
            }
            catch { /* leave isSSL to true if there is an exception */ }

            var endpointAddress = new System.ServiceModel.EndpointAddress(serviceUrl);
            var binding = _GetVisionServiceClientBinding(isSSL);

            var client = new VisionServiceClient(binding, endpointAddress);
            client.Endpoint.Binding.ReceiveTimeout = TimeSpan.FromMinutes(5);

            return client;
        }

        internal void Log(string message, Exception ex, string source, Guid corelationId)
        {
            logger.WriteLog(message, ex, source, corelationId);
        }

        private System.ServiceModel.Channels.Binding _GetVisionServiceClientBinding(bool isSSL = true)
        {
            BasicHttpBinding result =
                new BasicHttpBinding()
                {
                    MaxBufferSize = int.MaxValue,
                    MaxReceivedMessageSize = int.MaxValue
                };
            if (isSSL)
                result.Security.Mode = BasicHttpSecurityMode.Transport;
            return result;
        }
    }
}
