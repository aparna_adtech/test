﻿using com.breg.vision.Handoff.ClientModels;
using com.breg.vision.Handoff.ClientModels.VisionServiceReference;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.Worker
{
    public class IntegrationToBcsDocHandoffJobExecutor : BaseHandoffJobExecutor, IHandoffJobExecutor
    {
        private readonly string BcsPushApiUrl;
        public IntegrationToBcsDocHandoffJobExecutor()
        {
            if(ConfigurationManager.AppSettings["BcsPushApiUrl"] != null)
            {
                BcsPushApiUrl = ConfigurationManager.AppSettings.Get("BcsPushApiUrl");
            }
        }

        public Task<ProcessRequestResponseModel> ProcessRequestPayload(string payload)
        {
            var tcs = new TaskCompletionSource<ProcessRequestResponseModel>();

            if (!string.IsNullOrEmpty(payload))
            {
                // Post data to BCS Push service.
                var webClient = new ApiClient.ApiClient();
                webClient.Put<string>(BcsPushApiUrl, payload, serializationRequired: false).ContinueWith(t =>
                {
                    if (t.Status == TaskStatus.RanToCompletion && t.Result.IsSuccess)
                    {
                        tcs.SetResult(new ProcessRequestResponseModel { IsSuccess = true, ResponsePayload = JsonConvert.SerializeObject(t.Result.Content) });
                    }
                    else
                    {
                        tcs.SetResult(new ProcessRequestResponseModel { IsSuccess = false, FailureMessages = new string[] { t.Result.Content } });
                    }
                });
            }

            return tcs.Task;
        }
    }
}
