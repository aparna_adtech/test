﻿using com.breg.vision.Handoff.ClientModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.Worker
{
    public class SendLogToServerHandoffJobExecutor : BaseHandoffJobExecutor, IHandoffJobExecutor
    {
        public Task<ProcessRequestResponseModel> ProcessRequestPayload(string payload)
        {
            var tcs = new TaskCompletionSource<ProcessRequestResponseModel>();

            Task.Run(
                () =>
                {
                    SendLogToServerPayloadModel payload_model = null;

                    // try to decode the payload
                    try
                    {
                        payload_model = JsonConvert.DeserializeObject<SendLogToServerPayloadModel>(payload);
                    }
                    catch (Exception ex)
                    {
                        tcs.SetResult(
                            new ProcessRequestResponseModel
                            {
                                IsSuccess = false,
                                FailureMessages = new[] { "Unable to deserialize payload", JsonConvert.SerializeObject(ex) }
                            });
                    }

                    // write out the file
                    try
                    {
                        var output_basedir = System.Configuration.ConfigurationManager.AppSettings["SendLogToServer.OutputBaseDir"];
                        if (string.IsNullOrEmpty(output_basedir))
                            throw new Exception("Invalid SendLogToServer.OutputBaseDir AppSettings value: Is null or empty");

                        var output_subdir1 = payload_model.SubmissionDate.ToString("MMddyyyy");

                        var output_subdir2 = payload_model.Username?.ToLower();
                        if (string.IsNullOrEmpty(output_subdir2))
                            throw new Exception("payload_model.Username is null or empty");
                        output_subdir2 = SanitizeForFilenameUse(output_subdir2);

                        var output_dir = Path.Combine(output_basedir, output_subdir1);
                        output_dir = Path.Combine(output_dir, output_subdir2);

                        var filename = payload_model.ArchiveFilename;
                        if (string.IsNullOrEmpty(filename))
                            throw new Exception("payload_model.ArchiveFilename is null or empty");
                        filename = SanitizeForFilenameUse(filename);

                        var output_path = Path.Combine(output_dir, filename);

                        if (Directory.Exists(output_dir) == false)
                            Directory.CreateDirectory(output_dir);

                        File.WriteAllBytes(output_path, payload_model.LogArchiveContents);

                        tcs.SetResult(
                            new ProcessRequestResponseModel
                            {
                                IsSuccess = true,
                                ResponsePayload = JsonConvert.SerializeObject(true)
                            });
                    }
                    catch (Exception ex)
                    {
                        tcs.SetResult(
                            new ProcessRequestResponseModel
                            {
                                IsSuccess = false,
                                FailureMessages = new[] { "Unable to create output file", JsonConvert.SerializeObject(ex) }
                            });
                    }
                });

            return tcs.Task;
        }

        private static string SanitizeForFilenameUse(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                // credit goes out to: https://www.codeproject.com/Tips/758861/Removing-characters-which-are-not-allowed-in-Windo
                Regex illegal_char_matcher = new Regex(string.Format("[{0}]", Regex.Escape(new string(Path.GetInvalidFileNameChars()))), RegexOptions.Compiled);
                filename = illegal_char_matcher.Replace(filename, "");
            }
            return filename;
        }
    }
}
