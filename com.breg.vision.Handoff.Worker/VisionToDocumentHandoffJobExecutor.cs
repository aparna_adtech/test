﻿using com.breg.vision.Handoff.ClientModels;
using com.breg.vision.Handoff.ClientModels.VisionServiceReference;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.Worker
{
    public class VisionToDocumentHandoffJobExecutor : BaseHandoffJobExecutor, IHandoffJobExecutor
    {
        private readonly string DocumentApiUrl;
        public VisionToDocumentHandoffJobExecutor()
        {
            if(ConfigurationManager.AppSettings["DocumentApiUrl"] != null)
            {
                DocumentApiUrl = ConfigurationManager.AppSettings.Get("DocumentApiUrl");
            }
        }

        public Task<ProcessRequestResponseModel> ProcessRequestPayload(string payload)
        {
            var tcs = new TaskCompletionSource<ProcessRequestResponseModel>();

            if (!string.IsNullOrEmpty(payload))
            {
                // POST data to integration service.
                var webClient = new ApiClient.ApiClient();
                webClient.Put<string>(DocumentApiUrl, payload, false)
                    .ContinueWith(t =>
                    {
                        if (t.Status == TaskStatus.RanToCompletion && t.Result.IsSuccess)
                        {
                            // Call the service, get the response (Document Id) and set the task result.
                            Log($"Dispensement Id updated for document, {t.Result.Content}", null, "Handsoff.Worker.VisionToDocument", Guid.NewGuid());
                            tcs.SetResult(new ProcessRequestResponseModel { IsSuccess = true, ResponsePayload = t.Result.Content });
                        }
                        else
                        {
                            Log($"Dispensement Id updation failed for document with message: {t.Result.Content}", t.Exception, "Handsoff.Worker.VisionToDocument", Guid.NewGuid());
                            tcs.SetResult(new ProcessRequestResponseModel { IsSuccess = false, FailureMessages = new string[] { t.Result.Content } });
                        }
                    });
            }

            return tcs.Task;
        }
    }
}
