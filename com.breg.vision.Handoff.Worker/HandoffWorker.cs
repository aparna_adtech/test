﻿using com.breg.vision.Handoff.CryptoLib.AES256;
using com.breg.vision.Handoff.Models;
using com.breg.vision.Handoff.Models.MongoDB;
using com.breg.vision.MongoRepository;
using com.breg.vision.MongoRepository.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.Worker
{
    public class HandoffWorker : IHandoffRepositoryFactory
    {
        /// <summary>
        /// Process the received token by running the appropriate job executor for it.
        /// 
        /// This method makes every effort to consume the token and handle all errors.
        /// </summary>
        /// <param name="token">ID of job in the database</param>
        public void ProcessJob(string token)
        {
            var start_timestamp = DateTime.Now;
            var mongodb = GetHandoffRepository();

            var item = mongodb.Get(entity => entity.Token == token);
            if (item != null)
            {
                // skip over jobs that are not in pending state... this avoids the possible redelivery scenario from RabbitMQ when a connection and topology is automatically recovered during a connection drop.
                // TODO: this means that work on a failed jobs dashboard should change job status to JobStatus.Pending before re-submitting a job to RMQ
                if (item.Status != JobStatus.Pending)
                    return;

                if (item.Request != null)
                {
                    var copy_of_request = new HandoffRequestModel(item.Request);
                    
                    Decrypter.TryDecrypt(copy_of_request);

                    var executor = GetExecutorForJobType(copy_of_request.JobType);

                    if (executor != null)
                    {
                        var processTask = executor.ProcessRequestPayload(copy_of_request.Payload);
                        processTask.Wait();

                        if (!processTask.Result.IsSuccess)
                            // processing failed
                            item.SetStatusFailedWithFailureMessages(start_timestamp, processTask.Result.FailureMessages);
                        else
                            // processing succeeded
                            item.SetStatusCompletedWithResponsePayload(start_timestamp, processTask.Result.ResponsePayload);
                    }
                    else
                        item.SetStatusFailedUnknownExecutor();
                }
                else
                    item.SetStatusFailedInvalidRequest();

                if (item.Response != null)
                    Encrypter.TryEncrypt(item.Response);

                mongodb.Update(item);
            }
        }

        private static IHandoffJobExecutor GetExecutorForJobType(JobTypes jobtype)
        {
            IHandoffJobExecutor executor = null;
            switch (jobtype)
            {
                case JobTypes.Dispensement:
                    executor = new DispensementHandoffJobExecutor();
                    break;
                case JobTypes.DispensementCustomBrace:
                    executor = new DispensementCustomBraceHandoffJobExecutor();
                    break;
                case JobTypes.InventoryCount:
                    executor = new InventoryCountHandoffJobExecutor();
                    break;
                case JobTypes.Document:
                    executor = new DocumentHandoffJobExecutor();
                    break;
                case JobTypes.VisionToIntegration:
                    executor = new VisionToIntegrationHandoffJobExecutor();
                    break;
                case JobTypes.VisionToDocument:
                    executor = new VisionToDocumentHandoffJobExecutor();
                    break;
                case JobTypes.IntegrationToBCS:
                    executor = new IntegrationToBcsHandoffJobExecutor();
                    break;
                case JobTypes.BcsToIntegration:
                    executor = new BcsToIntegrationHandoffJobExecutor();
                    break;
                case JobTypes.IntegrationToBcsDoc:
                    executor = new IntegrationToBcsDocHandoffJobExecutor();
					break;
                case JobTypes.SendLogToServer:
                    executor = new SendLogToServerHandoffJobExecutor();
                    break;
                default:
                    break;
            }

            return executor;
        }

        private IRepository<MongoHandoffModel> _handoffMongoRepository = null;
        public IRepository<MongoHandoffModel> GetHandoffRepository()
        {
            if (_handoffMongoRepository == null)
                _handoffMongoRepository = new MongoRepository<MongoHandoffModel>();
            return _handoffMongoRepository;
        }

        public static bool TestRSAEnvironment()
        {
            try
            {
                Handoff.CryptoLib.AES256.KeyManager.GetCurrentRSAKey();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
