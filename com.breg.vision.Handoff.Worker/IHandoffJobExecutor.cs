﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.Worker
{
    public interface IHandoffJobExecutor
    {
        Task<ProcessRequestResponseModel> ProcessRequestPayload(string payload);
    }
}
