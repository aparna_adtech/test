﻿using com.breg.vision.Handoff.Models.MongoDB;
using com.breg.vision.MongoRepository;
using com.breg.vision.MongoRepository.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.Worker
{
    public interface IHandoffRepositoryFactory
    {
        IRepository<MongoHandoffModel> GetHandoffRepository();
    }
}
