﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.Worker
{
    public class ProcessRequestResponseModel
    {
        public bool IsSuccess { get; set; }
        public string ResponsePayload { get; set; }
        public IList<string> FailureMessages { get; set; }
    }
}
