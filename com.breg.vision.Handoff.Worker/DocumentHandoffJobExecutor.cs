﻿using com.breg.vision.Handoff.ClientModels;
using com.breg.vision.Handoff.ClientModels.VisionServiceReference;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.Worker
{
    public class DocumentHandoffJobExecutor : BaseHandoffJobExecutor, IHandoffJobExecutor
    {
        private readonly string DocumentApiUrl;
        public DocumentHandoffJobExecutor()
        {
            if(ConfigurationManager.AppSettings["DocumentApiUrl"] != null)
            {
                DocumentApiUrl = ConfigurationManager.AppSettings.Get("DocumentApiUrl");
            }
        }

        public Task<ProcessRequestResponseModel> ProcessRequestPayload(string payload)
        {
            var tcs = new TaskCompletionSource<ProcessRequestResponseModel>();
            DocumentPayloadModel payload_model = null;

            try
            {
                payload_model = JsonConvert.DeserializeObject<DocumentPayloadModel>(payload);
            }
            catch (Exception ex)
            {
                Log("Unable to deserialize payload", ex, "Handsoff.Worker.Document", Guid.NewGuid());
                tcs.SetResult(
                    new ProcessRequestResponseModel
                    {
                        IsSuccess = false,
                        FailureMessages = new[] { "Unable to deserialize payload", JsonConvert.SerializeObject(ex) }
                    });
            }

            if (payload_model != null)
            {
                // POST document data to Document Api.
                var client = new ApiClient.ApiClient();
                var response = client.Post<DocumentPayloadModel>(DocumentApiUrl, payload_model)
                    .ContinueWith(t =>
                    {
                        if (t.Status == TaskStatus.RanToCompletion && t.Result.IsSuccess)
                        {
                            // Call the service, get the response (Document Id) and set the task result.
                            Log($"Saved document with document id: {t.Result.Content}", null, "Handsoff.Worker.Document", Guid.Parse(payload_model.CorelationId));
                            tcs.SetResult(new ProcessRequestResponseModel { IsSuccess = true, ResponsePayload = t.Result.Content });
                        }
                        else
                        {
                            Log($"Document save failed with message: {t.Result.Content}", t.Exception, "Handsoff.Worker.Document", Guid.Parse(payload_model.CorelationId));
                            tcs.SetResult(new ProcessRequestResponseModel { IsSuccess = false, FailureMessages = new string[] { t.Result.Content } });
                        }
                    });
                }

            return tcs.Task;
        }
    }
}
