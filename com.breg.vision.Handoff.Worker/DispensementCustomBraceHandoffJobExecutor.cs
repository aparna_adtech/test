﻿using com.breg.vision.Handoff.ClientModels;
using com.breg.vision.Handoff.ClientModels.VisionServiceReference;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.Worker
{
    internal class DispensementCustomBraceHandoffJobExecutor : BaseHandoffJobExecutor, IHandoffJobExecutor
    {
        public Task<ProcessRequestResponseModel> ProcessRequestPayload(string payload)
        {
            var tcs = new TaskCompletionSource<ProcessRequestResponseModel>();
            DispensementCustomBracePayloadModel payload_model = null;

            // try to decode the payload as an InventoryCountPayloadModel
            try
            {
                payload_model = JsonConvert.DeserializeObject<DispensementCustomBracePayloadModel>(payload);
            }
            catch (Exception ex)
            {
                tcs.SetResult(
                    new ProcessRequestResponseModel
                    {
                        IsSuccess = false,
                        FailureMessages = new[] { "Unable to deserialize payload", JsonConvert.SerializeObject(ex) }
                    });
            }

            // submit the payload to the web service
            if (payload_model != null)
            {
                var client = GetVisionServiceClient();
                client.DispenseOrQueueCustomBraceProductsNoDupsCompleted +=
                    (s, e) =>
                    {
                        if (e.Cancelled)
                            tcs.SetResult(new ProcessRequestResponseModel { IsSuccess = false, FailureMessages = new[] { "Request cancelled" } });
                        else if (e.Error != null)
                            tcs.SetResult(new ProcessRequestResponseModel { IsSuccess = false, FailureMessages = new[] { JsonConvert.SerializeObject(e.Error) } });
                        else
                            tcs.SetResult(new ProcessRequestResponseModel { IsSuccess = true, ResponsePayload = JsonConvert.SerializeObject(e.Result) });
                    };

                client.DispenseOrQueueCustomBraceProductsNoDupsAsync(payload_model.Dispensement, payload_model.Username, payload_model.Password, payload_model.DeviceID, payload_model.LastLocation, payload_model.DispenseIdentifier, payload_model.IsV5Dispensement);
            }

            return tcs.Task;
        }
    }
}
