﻿using com.breg.vision.Handoff.ClientModels;
using com.breg.vision.Handoff.ClientModels.VisionServiceReference;
using com.breg.vision.Integration.VisionBcsModels;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.Worker
{
    public class BcsToIntegrationHandoffJobExecutor : BaseHandoffJobExecutor, IHandoffJobExecutor
    {
        private readonly string BcsToIntegrationApiUrl;
        public BcsToIntegrationHandoffJobExecutor()
        {
            if(ConfigurationManager.AppSettings["BcsToIntegrationApiUrl"] != null)
            {
                BcsToIntegrationApiUrl = ConfigurationManager.AppSettings.Get("BcsToIntegrationApiUrl");
            }
        }

        public Task<ProcessRequestResponseModel> ProcessRequestPayload(string payload)
        {
            var tcs = new TaskCompletionSource<ProcessRequestResponseModel>();
            BcsClaimReturn payload_model = null;

            try
            {
                payload_model = JsonConvert.DeserializeObject<BcsClaimReturn>(payload);
            }
            catch (Exception ex)
            {
                Log("Unable to deserialize payload", ex, "Handsoff.Worker.IntegrationToBcs", Guid.NewGuid());
                tcs.SetResult(new ProcessRequestResponseModel
                {
                    IsSuccess = false,
                    FailureMessages = new[] { "Unable to deserialize payload", JsonConvert.SerializeObject(ex) }
                });
            }
            if (!string.IsNullOrEmpty(payload))
            {
                // POST data to integration service.
                var webClient = new ApiClient.ApiClient();
                webClient.Post<BcsClaimReturn>(BcsToIntegrationApiUrl, payload_model)
                    .ContinueWith(t =>
                    {
                        if (t.Status == TaskStatus.RanToCompletion && t.Result.IsSuccess)
                        {
                            // ToDo: Handle the corelation id.
                            Log($"Data transfered from BCS to Integration: {t.Result.Content}", null, "Handsoff.Worker.BCSToIntegration", Guid.NewGuid());
                            tcs.SetResult(new ProcessRequestResponseModel { IsSuccess = true, ResponsePayload = JsonConvert.SerializeObject(t.Result.Content) });
                        }
                        else
                        {
                            Log($"BCS to Integration data transfer failed.", t.Exception, "Handsoff.Worker.BCSToIntegration", Guid.NewGuid());
                            tcs.SetResult(new ProcessRequestResponseModel { IsSuccess = false, FailureMessages = new string[] { t.Result.Content } });
                        }
                    });
            }

            return tcs.Task;
        }
    }
}
