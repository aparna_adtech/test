﻿using com.breg.vision.Handoff.ClientModels;
using com.breg.vision.Handoff.ClientModels.VisionServiceReference;
using com.breg.vision.Integration.VisionBcsModels;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.Worker
{
    public class VisionToIntegrationHandoffJobExecutor : BaseHandoffJobExecutor, IHandoffJobExecutor
    {
        private readonly string IntegrationApiUrl;
        public VisionToIntegrationHandoffJobExecutor()
        {
            if(ConfigurationManager.AppSettings["IntegrationApiUrl"] != null)
            {
                IntegrationApiUrl = ConfigurationManager.AppSettings.Get("IntegrationApiUrl");
            }
        }

        public Task<ProcessRequestResponseModel> ProcessRequestPayload(string payload)
        {
            var tcs = new TaskCompletionSource<ProcessRequestResponseModel>();
            BcsClaimSubmission payload_model = null;

            try
            {
                payload_model = JsonConvert.DeserializeObject<BcsClaimSubmission>(payload);
            }
            catch (Exception ex)
            {
                Log("Unable to deserialize payload", ex, "Handsoff.Worker.VisionToIntegration", Guid.NewGuid());
                tcs.SetResult(new ProcessRequestResponseModel
                {
                    IsSuccess = false,
                    FailureMessages = new[] { "Unable to deserialize payload", JsonConvert.SerializeObject(ex) }
                });
            }

            if (payload_model != null)
            {
                // POST data to integration service.
                var webClient = new ApiClient.ApiClient();
                webClient.Post<BcsClaimSubmission>(IntegrationApiUrl, payload_model).ContinueWith(t =>
                {
                    if (t.Status == TaskStatus.RanToCompletion && t.Result.IsSuccess)
                    {
                        tcs.SetResult(new ProcessRequestResponseModel { IsSuccess = true, ResponsePayload = JsonConvert.SerializeObject(t.Result.Content) });
                    }
                    else
                    {
                        tcs.SetResult(new ProcessRequestResponseModel { IsSuccess = false, FailureMessages = new string[] { t.Result.Content } });
                    }
                });
            }

            return tcs.Task;
        }
    }
}
