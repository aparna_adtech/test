﻿using com.breg.vision.Handoff.ClientModels;
using com.breg.vision.Handoff.ClientModels.VisionServiceReference;
using com.breg.vision.Integration.VisionBcsModels;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.Worker
{
    public class IntegrationToBcsHandoffJobExecutor : BaseHandoffJobExecutor, IHandoffJobExecutor
    {
        private readonly string BcsPushApiUrl;
        public IntegrationToBcsHandoffJobExecutor()
        {
            if(ConfigurationManager.AppSettings["BcsPushApiUrl"] != null)
            {
                BcsPushApiUrl = ConfigurationManager.AppSettings.Get("BcsPushApiUrl");
            }
        }

        public Task<ProcessRequestResponseModel> ProcessRequestPayload(string payload)
        {
            var tcs = new TaskCompletionSource<ProcessRequestResponseModel>();
            BcsClaimSubmission payload_model = null;

            try
            {
                payload_model = JsonConvert.DeserializeObject<BcsClaimSubmission>(payload);
            }
            catch (Exception ex)
            {
                Log("Unable to deserialize payload", ex, "Handsoff.Worker.IntegrationToBcs", Guid.NewGuid());
                tcs.SetResult(new ProcessRequestResponseModel
                {
                    IsSuccess = false,
                    FailureMessages = new[] { "Unable to deserialize payload", JsonConvert.SerializeObject(ex) }
                });
            }
            if (!string.IsNullOrEmpty(payload))
            {
                // Post data to BCS Push service.
                var webClient = new ApiClient.ApiClient();
                webClient.Post<BcsClaimSubmission>(BcsPushApiUrl, payload_model).ContinueWith(t =>
                {
                    if (t.Status == TaskStatus.RanToCompletion && t.Result.IsSuccess)
                    {
                        tcs.SetResult(new ProcessRequestResponseModel { IsSuccess = true, ResponsePayload = JsonConvert.SerializeObject(t.Result.Content) });
                    }
                    else
                    {
                        tcs.SetResult(new ProcessRequestResponseModel { IsSuccess = false, FailureMessages = new string[] { t.Result.Content } });
                    }
                });
            }

            return tcs.Task;
        }
    }
}
