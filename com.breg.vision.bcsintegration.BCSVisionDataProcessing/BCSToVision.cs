﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using com.breg.vision.Logger;
using System.Data.Linq;

namespace com.breg.vision.bcsintegration.BCSVisionDataProcessing
{
    public class BCSToVision
    {
		private const int LONG_NAME_MAX_LENGTH = 100;
		private const int SHORT_NAME_MAX_LENGTH = 50;
		private const int SUBMIT_BATCH_SIZE = 20;

        public Guid _CorrelationId { get; private set; }
        public FileLogger _Logger { get; private set; }

        public BCSToVision(Guid correlationId, FileLogger logger)
        {
            _CorrelationId = correlationId;
            _Logger = logger;
        }

        public void SyncCacheToVision()
		{
			LogEntry("*** BCSToVision Product Synchronization Started ***");
			List<BCSItemCatalog> bics = new List<BCSItemCatalog>();
			using (var bcsDB = BcsVisionDataContext.GetBcsDataContext())
			{
                LogEntry($"Getting Items from BCSItemCatalog started at {DateTime.Now}");
				bics = GetBCSItemCatalogs(bcsDB);
                LogEntry($"Getting Items from BCSItemCatalog ended at {DateTime.Now}");
            }

			if (bics.Count == 0)
			{
				LogEntry($"*** Aborting import - No BCSItemCatalog rows found!");
				return;
			}

			using (var visionDB = BcsVisionDataContext.GetVisionDataContext())
			{
				LogEntry($"Running against connection [{visionDB.Connection.ConnectionString}]");
				LogEntry($"Getting HCPCs for the Master Catalog Product started at {DateTime.Now}");
				DataLoadOptions options = new DataLoadOptions();
                options.LoadWith<MasterCatalogProduct>(parent => parent.BregProductHCPCs);
                visionDB.LoadOptions = options;
				LogEntry($"Getting HCPCs for the Master Catalog Product ended at {DateTime.Now}");

				LogEntry($"Getting BCS Supplier started at {DateTime.Now}");
                MasterCatalogSupplier bcsSupplier = GetBCSSupplier(visionDB);
				LogEntry($"Getting BCS Supplier ended at {DateTime.Now}");

				LogEntry($"Getting BCS Sub Category started at {DateTime.Now}");
                MasterCatalogSubCategory bcsSubCategory = GetBCSSubCategory(visionDB);
				LogEntry($"Getting BCS Sub Category ended at {DateTime.Now}");

				LogEntry($"Getting Master Catalog Products started at {DateTime.Now}");
                List<MasterCatalogProduct> mcps = GetMasterCatalogProducts(visionDB, bcsSupplier);
				LogEntry($"Getting Master Catalog Products ended at {DateTime.Now}");

				LogEntry($"Deactivating all Master Catalog Products missing from BCS started at {DateTime.Now}");
			
				DeactivateMissingMCPs(bics, mcps);
				visionDB.SubmitChanges();
				LogEntry($"Deactivating all Master Catalog Products missing from BCS ended at {DateTime.Now}");

				DateTime startTime = DateTime.Now;
				bool operationPerformed = false;
				int itemIndex = 1;
				int maxitemIndex = bics.Count;
				foreach (BCSItemCatalog bic in bics)
				{
					MasterCatalogProduct matchingMCP = GetMatchingMCP(bic, mcps);
					MasterCatalogProduct mcp = null;
					string operation = "Skipped";
					if (matchingMCP != null && HasProductChanged(bic, matchingMCP))
					{
						operation = "Replaced";
						mcp = InsertBCSProductIntoVision(visionDB, bic, matchingMCP, bcsSupplier, bcsSubCategory);
						DeactivateMCP(matchingMCP);
					}
					else if (matchingMCP == null)
					{
						operation = "Inserted";
						mcp = InsertBCSProductIntoVision(visionDB, bic, null, bcsSupplier, bcsSubCategory);
					}
					if (mcp != null)
						operationPerformed = true;

					string itemDescription = GetItemDescription(bic, true);
					LogEntry($"Product # {itemIndex.ToString()} {bic.ItemNumber}) {itemDescription} has been {operation}");
					if (operationPerformed && ((itemIndex % SUBMIT_BATCH_SIZE == 0) || (itemIndex == bics.Count)))
					{
						DateTime beforeSubmit = DateTime.Now;
						visionDB.SubmitChanges();
						DateTime afterSubmit = DateTime.Now;
						double estimatedSecondsPerItem = (afterSubmit - startTime).TotalSeconds / itemIndex;
						double estimatedSecondsRemaining = 0;
						double itemsRemaining = maxitemIndex - itemIndex;
						if (itemsRemaining > 0)
							estimatedSecondsRemaining = estimatedSecondsPerItem * itemsRemaining;
						LogEntry($"Insert/Replace batch submit of {SUBMIT_BATCH_SIZE} items took {(afterSubmit - beforeSubmit).TotalMilliseconds} ms;\n Estimated minutes remaining:{string.Format("{0:0.00}", estimatedSecondsRemaining / 60)}");
						operationPerformed = false;
					}
					itemIndex++;
				}
			}
			LogEntry("*** BCSToVision Product Synchronization Completed ***");
		}

		private List<MasterCatalogProduct> GetMasterCatalogProducts(BcsVisionDataContext visionDB, MasterCatalogSupplier bcsSupplier)
		{
			var mcpQuery =
				from mcp in visionDB.MasterCatalogProducts
				where mcp.MasterCatalogSupplierID == bcsSupplier.MasterCatalogSupplierID && mcp.IsActive
				orderby mcp.MasterCatalogProductID descending
				select new { mcp };

			List<MasterCatalogProduct> mcps = new List<MasterCatalogProduct>();
			foreach (var mcp in mcpQuery)
			{
				mcps.Add(mcp.mcp);
			}
			return mcps;
		}

		private List<BCSItemCatalog> GetBCSItemCatalogs(BcsVisionDataContext visionDB)
		{
			var bicQuery =
				from bic in visionDB.BCSItemCatalogs
				orderby bic.ItemNumber ascending
				select new { bic };

			List<BCSItemCatalog> bics = new List<BCSItemCatalog>();
			foreach (var bic in bicQuery)
			{
				bics.Add(bic.bic);
			}
			return bics;
		}

		private MasterCatalogSupplier GetBCSSupplier(BcsVisionDataContext visionDB)
		{
			var mcsQuery =
				from mcs in visionDB.MasterCatalogSuppliers
				where mcs.IsBCSSupplier == true
				select new { mcs };

			foreach (var mcs in mcsQuery)
			{
				return mcs.mcs;
			}
			return null;
		}

		private MasterCatalogSubCategory GetBCSSubCategory(BcsVisionDataContext visionDB)
		{
			var mcsQuery =
				from mcs in visionDB.MasterCatalogSubCategories
				where mcs.IsBCSSubCategory == true
				select new { mcs };

			foreach (var mcs in mcsQuery)
			{
				return mcs.mcs;
			}
			return null;
		}

		private MasterCatalogProduct GetMatchingMCP(BCSItemCatalog bic, List<MasterCatalogProduct> mcps)
		{
			foreach (MasterCatalogProduct mcp in mcps)
			{
				if (IsProductMatch(bic, mcp))
				{
					return mcp;
				}
			}
			return null;
		}

		private void DeactivateMissingMCPs(List<BCSItemCatalog> bics, List<MasterCatalogProduct> mcps)
		{
			foreach (MasterCatalogProduct mcp in mcps)
			{
				bool found = false;
				foreach (BCSItemCatalog bic in bics)
				{
					if (IsProductMatch(bic, mcp))
					{
						found = true;
						break;
					}
				}
				if (!found)
					DeactivateMCP(mcp);
			}
		}

		private void DeactivateMCP(MasterCatalogProduct mcp)
		{
			if (mcp != null)
			{
				foreach (UPCCode uc in mcp.UPCCodes)
				{
					uc.IsActive = false;
				}
				foreach (BregProductHCPC bph in mcp.BregProductHCPCs)
				{
					bph.IsActive = false;
				}
				foreach (PracticeCatalogProduct pcp in mcp.PracticeCatalogProducts)
				{
					foreach (ProductInventory pi in pcp.ProductInventories)
					{
						foreach (InventoryItemCount iic in pi.InventoryItemCounts)
						{
							iic.IsActive = false;
						}
						pi.IsActive = false;
					}
					foreach (ProductHCPC ph in pcp.ProductHCPCs)
					{
						ph.IsActive = false;
					}
					pcp.IsActive = false;
				}
				mcp.IsActive = false;
			}
		}

		private MasterCatalogProduct InsertBCSProductIntoVision(BcsVisionDataContext visionDB, BCSItemCatalog bicItem, MasterCatalogProduct mcpItem,
			MasterCatalogSupplier bcsSupplier, MasterCatalogSubCategory bcsSubCategory)
		{
			try
			{
				MasterCatalogProduct newMCP = GenerateMCP(bicItem, mcpItem, bcsSupplier, bcsSubCategory);
				visionDB.MasterCatalogProducts.InsertOnSubmit(newMCP);
				return newMCP;
			}
			catch (Exception ex)
			{
				LogEntry("Error ecountered in InsertBCSProductIntoVision():\n\n" + ex.Message + "\n\n" + ex.StackTrace);
			}
			return null;
		}

		private MasterCatalogProduct GenerateMCP(BCSItemCatalog bicItem, MasterCatalogProduct mcpItem,
			MasterCatalogSupplier bcsSupplier, MasterCatalogSubCategory bcsSubCategory)
		{
			MasterCatalogProduct newMCP = new MasterCatalogProduct();
			if (mcpItem != null)
			{
				//newMCP.MasterCatalogProductID = mcpItem.MasterCatalogProductID;
				newMCP.MasterCatalogSupplierID = mcpItem.MasterCatalogSupplierID;
				newMCP.MasterCatalogSubCategoryID = mcpItem.MasterCatalogSubCategoryID;
				newMCP.Code = mcpItem.Code;
				newMCP.Name = mcpItem.Name;
				newMCP.ShortName = mcpItem.ShortName;
				newMCP.Packaging = mcpItem.Packaging;
				newMCP.LeftRightSide = mcpItem.LeftRightSide;
				newMCP.Size = mcpItem.Size;
				newMCP.Color = mcpItem.Color;
				newMCP.Gender = mcpItem.Gender;
				newMCP.WholesaleListCost = mcpItem.WholesaleListCost;
				newMCP.Description = mcpItem.Description;
				newMCP.IsDiscontinued = mcpItem.IsDiscontinued;
				newMCP.DateDiscontinued = mcpItem.DateDiscontinued;
				newMCP.CreatedUserID = 1;
				newMCP.CreatedDate = DateTime.Now;
				newMCP.ModifiedUserID = 1;
				newMCP.ModifiedDate = DateTime.Now;
				newMCP.IsActive = true;
				newMCP.IsCustomBrace = mcpItem.IsCustomBrace;
				newMCP.UPCCode = mcpItem.UPCCode;
				newMCP.isCustomBraceAccessory = mcpItem.isCustomBraceAccessory;
				newMCP.IsLogoAblePart = mcpItem.IsLogoAblePart;
				newMCP.Mod1 = mcpItem.Mod1;
				newMCP.Mod2 = mcpItem.Mod2;
				newMCP.Mod3 = mcpItem.Mod3;
				newMCP.BCSInventoryItemID = mcpItem.BCSInventoryItemID;

				CopyPracticeCatalogProductLinks(newMCP, mcpItem, bicItem == null ? null : bicItem.HCPCSCode);
			}

			if (bicItem != null)
			{
				string shortName = GetItemDescription(bicItem, false);
				string longName = GetItemDescription(bicItem, true);

				newMCP.MasterCatalogSubCategoryID = bcsSubCategory.MasterCatalogSubCategoryID;
				newMCP.MasterCatalogSupplierID = bcsSupplier.MasterCatalogSupplierID;
				newMCP.Code = bicItem.ItemNumber;
				newMCP.Name = longName;
				newMCP.ShortName = shortName;
				newMCP.Packaging = bicItem.PrimaryUomCode.Trim().ToUpper();
				if (mcpItem == null)
				{
					newMCP.WholesaleListCost = 0;
					newMCP.Mod1 = string.Empty;
					newMCP.Mod2 = string.Empty;
					newMCP.Mod3 = string.Empty;
					newMCP.IsLogoAblePart = false;
					newMCP.LeftRightSide = string.Empty;
					newMCP.Color = string.Empty;
				}
				newMCP.CreatedDate = DateTime.Now;
				newMCP.BCSInventoryItemID = bicItem.InventoryItemID;

				SetUPCCode(newMCP, bicItem.ItemNumber, mcpItem);
				SetHCPCs(newMCP, bicItem.HCPCSCode, mcpItem);
			}
			newMCP.IsActive = true;

			return newMCP;
		}

		private void CopyPracticeCatalogProductLinks(MasterCatalogProduct targetMCP, MasterCatalogProduct sourceMCP, string targetHCPC)
		{
			if (targetMCP != null && sourceMCP != null)
			{
				foreach (PracticeCatalogProduct pcp in sourceMCP.PracticeCatalogProducts)
				{
					if (pcp.IsActive == null || !(pcp.IsActive.Value))
						continue;

					PracticeCatalogProduct newPCP = new PracticeCatalogProduct()
					{
						//MasterCatalogProductID = pcp.MasterCatalogProductID,
						PracticeID = pcp.PracticeID,
						PracticeCatalogSupplierBrandID = pcp.PracticeCatalogSupplierBrandID,
						ThirdPartyProductID = pcp.ThirdPartyProductID,
						IsThirdPartyProduct = pcp.IsThirdPartyProduct,
						WholesaleCost = pcp.WholesaleCost,
						BillingCharge = pcp.BillingCharge,
						DMEDeposit = pcp.DMEDeposit,
						BillingChargeCash = pcp.BillingChargeCash,
						StockingUnits = pcp.StockingUnits,
						BuyingUnits = pcp.BuyingUnits,
						Sequence = pcp.Sequence,
						CreatedDate = DateTime.Now,
						CreatedUserID = 1,
						ModifiedDate = DateTime.Now,
						ModifiedUserID = 1,
						IsActive = true,
						IsLogoAblePart = pcp.IsLogoAblePart,
						IsSplitCode = pcp.IsSplitCode,
						IsAlwaysOffTheShelf = pcp.IsAlwaysOffTheShelf,
						IsPreAuthorization = pcp.IsPreAuthorization,
						Mod1 = pcp.Mod1,
						Mod2 = pcp.Mod2,
						Mod3 = pcp.Mod3,
						UsePCPMods = pcp.UsePCPMods,
						IsLateralityExempt = pcp.IsLateralityExempt
					};

					if (targetHCPC != null && targetHCPC != string.Empty)
					{
                        bool existingIsOffTheShelf = pcp.ProductHCPCs.FirstOrDefault(x => x.HCPCS == targetHCPC)?.IsOffTheShelf ?? false;
                        ProductHCPC newHCPC = new ProductHCPC()
                        {
                            HCPCS = targetHCPC,
                            CreatedDate = DateTime.Now,
                            CreatedUserID = 1,
                            ModifiedDate = DateTime.Now,
                            ModifiedUserID = 1,
                            IsActive = true,
                            IsOffTheShelf = existingIsOffTheShelf,
							PracticeCatalogProduct = newPCP
						};
						newPCP.ProductHCPCs.Add(newHCPC);
					}

					foreach (ProductInventory pi in pcp.ProductInventories)
					{
						if (!pi.IsActive)
							continue;

						ProductInventory newPI = new ProductInventory
						{
							//ProductInventoryID = pi.ProductInventoryID,
							//PracticeCatalogProduct = pcp,
							PracticeLocationID = pi.PracticeLocationID,
							PracticeCatalogProductID = pi.PracticeCatalogProductID,
							QuantityOnHandPerSystem = pi.QuantityOnHandPerSystem,
							ParLevel = pi.ParLevel,
							ReorderLevel = pi.ReorderLevel,
							CriticalLevel = pi.CriticalLevel,
							CreatedUserID = 1,
							CreatedDate = DateTime.Now,
							ModifiedUserID = 1,
							ModifiedDate = DateTime.Now,
							IsActive = true,
							IsNotFlaggedForReorder = pi.IsNotFlaggedForReorder,
							IsSetToActive = pi.IsSetToActive,
							ConsignmentQuantity = pi.ConsignmentQuantity,
							IsConsignment = pi.IsConsignment,
							ConsignmentLockParLevel = pi.ConsignmentLockParLevel,
							IsBuyout = pi.IsBuyout,
							IsRMA = pi.IsRMA,
							BuyoutRMAInventoryCycleID = pi.BuyoutRMAInventoryCycleID,
							IsLogoAblePart = pi.IsLogoAblePart,
						};

						foreach (InventoryItemCount iic in pi.InventoryItemCounts)
						{
							if (!iic.IsActive)
								continue;

							InventoryItemCount newIIC = new InventoryItemCount
							{
								//InventoryItemCountID = iic.InventoryItemCountID,
								Count = iic.Count,
								CountDate = iic.CountDate,
								DeviceIdentifier = iic.DeviceIdentifier,
								InventoryCycleID = iic.InventoryCycleID,
								MembershipUserID = iic.MembershipUserID,
								ProductInventory = pi,
								UserID = iic.UserID,
								CreatedUserID = 1,
								CreatedDate = DateTime.Now,
								ModifiedUserID = 1,
								ModifiedDate = DateTime.Now,
								IsActive = true,
							};
							newPI.InventoryItemCounts.Add(newIIC);
						}
						newPCP.ProductInventories.Add(newPI);
					}
					targetMCP.PracticeCatalogProducts.Add(newPCP);
				}
			}
		}

		private void SetUPCCode(MasterCatalogProduct newMCP, string upcCode, MasterCatalogProduct oldMCP)
		{
			newMCP.UPCCode = upcCode;

			bool found = false;
			if (oldMCP != null)
			{
				foreach (UPCCode code in oldMCP.UPCCodes)
				{
					if (code.Code == upcCode)
						found = true;

					UPCCode oldUPCCode = new UPCCode()
					{
						Code = code.Code,
						CreatedDate = DateTime.Now,
						CreatedUserID = 1,
						ModifiedDate = DateTime.Now,
						ModifiedUserID = 1,
						IsActive = true,
					};
					newMCP.UPCCodes.Add(oldUPCCode);
				}
			}

			if (!found)
			{
				UPCCode newUPCCode = new UPCCode()
				{
					Code = upcCode,
					CreatedDate = DateTime.Now,
					CreatedUserID = 1,
					ModifiedDate = DateTime.Now,
					ModifiedUserID = 1,
					IsActive = true,
				};
				newMCP.UPCCodes.Add(newUPCCode);
			}
		}

		private void SetHCPCs(MasterCatalogProduct newMCP, string hcpc, MasterCatalogProduct mcpItem)
		{
            bool existingIsOffTheShelf = mcpItem?.BregProductHCPCs.FirstOrDefault(x => x.HCPCS == hcpc)?.IsOffTheShelf ?? false;
			BregProductHCPC newHCPC = new BregProductHCPC()
			{
				HCPCS = hcpc,
				IsOffTheShelf = existingIsOffTheShelf,
				CreatedDate = DateTime.Now,
				CreatedUserID = 1,
				ModifiedDate = DateTime.Now,
				ModifiedUserID = 1,
				IsActive = true,
			};
			newMCP.BregProductHCPCs.Add(newHCPC);
		}

		private bool IsProductMatch(BCSItemCatalog bicItem, MasterCatalogProduct mcpItem)
		{
			return mcpItem.Code == bicItem.ItemNumber;
		}

		private bool HasProductChanged(BCSItemCatalog bicItem, MasterCatalogProduct mcpItem)
		{
			string currentItemDescription = mcpItem.Name;
			string currentPrimaryUomCode = mcpItem.Packaging;
			string currentHCPCSCode = string.Empty;

			bool firstIteration = true;
			foreach (BregProductHCPC hcpc in mcpItem.BregProductHCPCs)
			{
				if (!firstIteration)
					currentHCPCSCode += ",";
				currentHCPCSCode += hcpc.HCPCS;
				firstIteration = false;
			}

			string itemDescription = GetItemDescription(bicItem, true);
			if (
					(itemDescription != currentItemDescription) ||
					(bicItem.PrimaryUomCode.Trim().ToUpper() != currentPrimaryUomCode) ||
					(bicItem.HCPCSCode != currentHCPCSCode)
				)
				return true;
			return false;
		}

		private string GetItemDescription(BCSItemCatalog itemCatalog, bool longVersion)
		{
			int len = SHORT_NAME_MAX_LENGTH;
			if (longVersion)
				len = LONG_NAME_MAX_LENGTH;

			string itemDescription = itemCatalog.ItemDescription;

			if (itemDescription.Length > len)
			{
				itemDescription = itemDescription.Substring(0, len);
				if (longVersion)
					LogEntry(">> ItemDescription being truncated from (" + itemCatalog.ItemDescription + ") to (" + itemDescription + ")");
			}
			return itemDescription;
		}

		private void LogEntry(string line)
		{
			if (!Console.IsOutputRedirected)
				Console.WriteLine(line);
			else
				Trace.WriteLine(line);
			if (_Logger != null)
				_Logger.WriteLog(line, null, "com.breg.vision.bcsintegration.BCSVisionDataProcessing", _CorrelationId);
		}
	}
}
