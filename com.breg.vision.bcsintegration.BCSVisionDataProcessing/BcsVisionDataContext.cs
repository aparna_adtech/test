﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.bcsintegration.BCSVisionDataProcessing
{
	public partial class BcsVisionDataContext
	{
		public static BcsVisionDataContext GetVisionDataContext()
		{
			string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["BregVisionConnectionString"].ToString();
			return new BcsVisionDataContext(connectionString);
		}
		public static BcsVisionDataContext GetBcsDataContext()
		{
			string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["BCSItemContext"].ToString();
			return new BcsVisionDataContext(connectionString);
		}

		public static void CloseConnection(BcsVisionDataContext visionDB)
		{
			if (visionDB != null)
			{
				if (visionDB.Connection != null && visionDB.Connection.State != System.Data.ConnectionState.Closed)
				{
					visionDB.Connection.Close();
					visionDB.Connection.Dispose();
					visionDB.Dispose();
					visionDB = null;
				}
			}
		}

		public static BcsVisionDataContext GetBcsVisionDataContext(string connectionString)
		{
			return new BcsVisionDataContext(connectionString);
		}
	}
}
