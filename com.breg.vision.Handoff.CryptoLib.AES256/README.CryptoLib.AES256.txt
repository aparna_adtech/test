﻿This library requires a machine-level RSA key container to be created. Create
the container by calling the KeyManager.CreateRSAContainerInMachineKeyStore()
method using an administrator account.

Then use aspnet_regiis.exe as administrator to grant read permissions to:
1) the "IIS AppPool\{apppool_name_here}" that Handoff.WebService run as
and
2) the service account that Handoff.WindowsServiceHost service runs as

---

For a quick-and-dirty way to get this up and running (dev-only... this only
creates a 1024-bit key since the options to aspnet_regiis.exe for 2048-bit
do not seem to work):
1) aspnet_regiis.exe -pc Vision -exp
2) aspnet_regiis.exe -pa Vision "IIS AppPool\BregVision"
3) aspnet_regiis.exe -pa Vision "LOCAL SERVICE"