﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Security.Cryptography;
using System.IO;

namespace com.breg.vision.Handoff.CryptoLib.AES256
{
    public static class Encrypter
    {
        public static bool TryEncrypt(object obj)
        {
            var properties_needing_encryption = Common.GetPropertiesWithEncryptAttribute(obj);
            var errors_encountered = false;

            foreach (var property in properties_needing_encryption)
            {
                // only encrypt if it is a string
                if (property.PropertyType != typeof(string))
                    continue;

                try
                {
                    EncryptAndReplacePropertyValue(obj, property);
                }
                catch
                {
                    /* do nothing if there was an error encrypting... i.e. leave it decrypted */
                    errors_encountered = true;
                }
            }

            return errors_encountered ? false : true;
        }

        private static void EncryptAndReplacePropertyValue(object obj, PropertyInfo property)
        {
            var plaintext = (string)property.GetValue(obj);

            if (!string.IsNullOrEmpty(plaintext))
            {
                var encrypted_output = EncryptString(plaintext);
                if (encrypted_output != null)
                {
                    var encoded_encrypted_string = Convert.ToBase64String(encrypted_output);
                    property.SetValue(obj, encoded_encrypted_string);
                }
            }
        }

        private static byte[] EncryptString(string plaintext)
        {
            byte[] encrypted_content, iv, rsaencrypted_key;

            using (var algorithm = Aes.Create())
            {
                algorithm.GenerateKey();
                rsaencrypted_key = KeyManager.EncryptKey(algorithm.Key);

                algorithm.GenerateIV();
                iv = algorithm.IV;

                algorithm.Mode = CipherMode.CBC;

                var encryptor = algorithm.CreateEncryptor(algorithm.Key, algorithm.IV);

                using (var ms = new MemoryStream())
                {
                    using (var cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                    {
                        using (var sw = new StreamWriter(cs))
                        {
                            sw.Write(plaintext);
                        }
                        encrypted_content = ms.ToArray();
                    }
                }
            }

            var encrypted_output = new byte[rsaencrypted_key.Length + iv.Length + encrypted_content.Length];
            Array.Copy(rsaencrypted_key, 0, encrypted_output, 0, rsaencrypted_key.Length);
            Array.Copy(iv, 0, encrypted_output, rsaencrypted_key.Length, iv.Length);
            Array.Copy(encrypted_content, 0, encrypted_output, rsaencrypted_key.Length + iv.Length, encrypted_content.Length);

            return encrypted_output;
        }
    }
}
