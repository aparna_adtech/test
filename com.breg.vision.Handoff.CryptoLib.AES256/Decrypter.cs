﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.CryptoLib.AES256
{
    public static class Decrypter
    {
        public static bool TryDecrypt(object obj)
        {
            var properties = Common.GetPropertiesWithEncryptAttribute(obj);
            var errors_encountered = false;

            foreach (var property in properties)
            {
                // only decrypt if it is a string
                if (property.PropertyType != typeof(string))
                    continue;

                try
                {
                    DecryptAndReplacePropertyValue(obj, property);
                }
                catch
                {
                    /* do nothing if there was an error decrypting... i.e. leave it encrypted */
                    errors_encountered = true;
                }
            }

            return errors_encountered ? false : true;
        }

        private static void DecryptAndReplacePropertyValue(object obj, PropertyInfo property)
        {
            var encoded_encrypted_string = (string)property.GetValue(obj);

            if (!string.IsNullOrEmpty(encoded_encrypted_string))
            {
                var plaintext = DecryptData(encoded_encrypted_string);
                property.SetValue(obj, plaintext);
            }
        }

        public static string DecryptData(string encoded_encrypted_string)
        {
            var encrypted_data = Convert.FromBase64String(encoded_encrypted_string);

            string plaintext;
            using (var algorithm = Aes.Create())
            {
                byte[] encrypted_cryptokey = new byte[KeyManager.GetCurrentRSAKeyLengthBytes()];
                byte[] iv = new byte[algorithm.BlockSize / 8];
                byte[] encrypted_content = new byte[encrypted_data.Length - encrypted_cryptokey.Length - iv.Length];

                Array.Copy(encrypted_data, encrypted_cryptokey, encrypted_cryptokey.Length);
                Array.Copy(encrypted_data, encrypted_cryptokey.Length, iv, 0, iv.Length);
                Array.Copy(encrypted_data, encrypted_cryptokey.Length + iv.Length, encrypted_content, 0, encrypted_content.Length);

                algorithm.Key = KeyManager.DecryptKey(encrypted_cryptokey);
                algorithm.IV = iv;

                algorithm.Mode = CipherMode.CBC;

                var decryptor = algorithm.CreateDecryptor(algorithm.Key, algorithm.IV);

                using (var ms = new MemoryStream(encrypted_content))
                {
                    using (var cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
                    {
                        using (var sw = new StreamReader(cs))
                        {
                            plaintext = sw.ReadToEnd();
                        }
                    }
                }
            }

            return plaintext;
        }
    }
}
