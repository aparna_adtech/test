﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.CryptoLib.AES256
{
    internal class Common
    {
        internal static IEnumerable<PropertyInfo> GetPropertiesWithEncryptAttribute(object obj)
        {
            return obj
                .GetType()
                .GetRuntimeProperties()
                .Where(
                    p =>
                        p.GetCustomAttributes<EncryptAttribute>()
                        .Any()
                )
                .Select(p => p)
                .AsEnumerable();
        }
    }
}
