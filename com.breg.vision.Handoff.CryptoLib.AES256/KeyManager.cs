﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.CryptoLib.AES256
{
    public static class KeyManager
    {
        private static Func<CspParameters> _CreateCspParameters =
            () =>
            new CspParameters
            {
                KeyContainerName = System.Configuration.ConfigurationManager.AppSettings["KeyContainer"] ?? "Vision",
                Flags = CspProviderFlags.NoPrompt | CspProviderFlags.UseExistingKey | CspProviderFlags.UseMachineKeyStore
            };

        public static byte[] EncryptKey(byte[] key)
        {
            var cp = _CreateCspParameters();
            using (var rsacsp = new RSACryptoServiceProvider(cp))
            {
                return rsacsp.Encrypt(key, true);
            }
        }

        public static byte[] DecryptKey(byte[] encryptedkey)
        {
            var cp = _CreateCspParameters();
            using (var rsacsp = new RSACryptoServiceProvider(cp))
            {
                return rsacsp.Decrypt(encryptedkey, true);
            }
        }

        public static string GetCurrentRSAKey(bool includePrivateKey = false)
        {
            var cp = _CreateCspParameters();
            using (var rsacsp = new RSACryptoServiceProvider(cp))
            {
                return rsacsp.ToXmlString(includePrivateKey);
            }
        }

        public static int GetCurrentRSAKeyLengthBytes()
        {
            var cp = _CreateCspParameters();
            using (var rsacsp = new RSACryptoServiceProvider(cp))
            {
                return rsacsp.KeySize / 8;
            }
        }

        public static void CreateRSAContainerInMachineKeyStore()
        {
            var cp = _CreateCspParameters();
            cp.Flags &= ~CspProviderFlags.UseExistingKey;

            try
            {
                using (var rsacsp = new RSACryptoServiceProvider(cp))
                {
                    rsacsp.PersistKeyInCsp = false;
                    rsacsp.Clear();
                }
            }
            catch { }

            using (var rsacsp = new RSACryptoServiceProvider(2048, cp))
            {
                return;
            }
        }
    }
}
