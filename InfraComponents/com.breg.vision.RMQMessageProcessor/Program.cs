﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Configuration;
using com.breg.vision.Logger;

namespace com.breg.vision.RMQMessageProcessor
{
    public class Program
    {
        static string RabbitMQHostUri = ConfigurationManager.AppSettings["RabbitMQServiceUrl"];
        static string IntServiceURI = ConfigurationManager.AppSettings["IntegrationServiceURI"];
        static string IntServiceURIParams = ConfigurationManager.AppSettings["IntegrationServiceURIParams"];
        static string pushServiceURI = ConfigurationManager.AppSettings["PushServiceURI"];
        static string pushServiceURIParams = ConfigurationManager.AppSettings["PushServiceURIParams"];
        static string BcsToIntUriParams = ConfigurationManager.AppSettings["BcsToIntegrationUriParams"];
        static string message = string.Empty;
        static string VsnToIntEntryPoint = "VisionToIntegrationQueue";
        static string IntToBcsEntryPoint = "IntegrationToBcsQueue";
        static string BcsToIntEntryPoint = "BcsToIntegrationQueue";
        static string projectTitle = "com.breg.vision.com.breg.vision.RMQMessageProcessor";
        public static FileLogger _Logger = new FileLogger();
        private static Guid correlationId;
        static void Main(string[] args)
        {
            correlationId = Guid.NewGuid();

            try
            {
                var factory = new ConnectionFactory() { HostName = RabbitMQHostUri };
                factory.Protocol = Protocols.AMQP_0_9_1;
                bool runEveryTask = false;

                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    var entrypoint = args[0];
                    if (channel == null)
                        _Logger.WriteLog("No Channel available", null, projectTitle, correlationId);

                    if (!string.IsNullOrEmpty(entrypoint))
                    {
                        if (entrypoint.Equals("All"))
                            runEveryTask = true;

                        if (entrypoint.Equals(VsnToIntEntryPoint) || runEveryTask)
                            VisionToIntegrationQueueMessageProcess(channel, VsnToIntEntryPoint);

                        if (entrypoint.Equals(IntToBcsEntryPoint) || runEveryTask)
                            IntegrationToBcsQueueMessageProcess(channel, IntToBcsEntryPoint);

                        if (entrypoint.Equals(BcsToIntEntryPoint) || runEveryTask)
                            BcsToIntegrationQueueMessageProcess(channel, BcsToIntEntryPoint);
                    }
                    else
                        _Logger.WriteLog("Entrypoint is empty", null, projectTitle, correlationId);                  
                }
            }
            catch (Exception ex)
            {
                _Logger.WriteLog(ex.Message, ex, projectTitle, correlationId);
            }
        }
        static async Task SendMessageToAPIAsync(string claimJson, string apiUri, string uriParams)
        {
            _Logger.WriteLog($"Call to {apiUri}/{uriParams} starts At {DateTime.Now} ", null, projectTitle, correlationId);

            using (var client = new HttpClient())
            {                
                client.BaseAddress = new Uri(apiUri);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.PostAsJsonAsync(uriParams, claimJson);
                
                if (!response.IsSuccessStatusCode)
                {
                    var message = await response.Content.ReadAsStringAsync();
                    string msg = $"The call to {apiUri}/{uriParams} returned with HTTP Status code - {response.StatusCode} with message-{message}";
                    throw new Exception(msg);
                }
            }
            _Logger.WriteLog($"Call to {apiUri}/{uriParams} ends At {DateTime.Now} ", null, projectTitle, correlationId);
        }

        static void VisionToIntegrationQueueMessageProcess(IModel channel, string entrypoint)
        {
            BasicGetResult result = null;
            try
            {
                bool noAck = false;

                if (channel.IsOpen)
                    result = channel.BasicGet(entrypoint, noAck);
                else
                    return;               

                if(result == null) // for the first pass the channel is still open so we need to check if result is null
                    return;

                //Check if we can retrieve message from the channel
                string returnMessageFeromQueue = PullMessagesFromQueue(result, channel);

                if (!string.IsNullOrEmpty(returnMessageFeromQueue))
                    //Make a post call to PlatformIntegrationService and send the json string
                    SendMessageToAPIAsync(returnMessageFeromQueue, IntServiceURI, IntServiceURIParams).Wait();
            }
            catch (Exception ex)
            {
                _Logger.WriteLog(ex.Message, ex, projectTitle, correlationId);
                ///reject the message but push back to queue for later re-try
                channel.BasicReject(result.DeliveryTag, true);
            }
            _Logger.WriteLog($"VisionToIntegrationQueue Ended At {DateTime.Now}", null, projectTitle, correlationId);
        }

        static void IntegrationToBcsQueueMessageProcess(IModel channel, string entrypoint)
        {
            _Logger.WriteLog($"IntegrationToBcsQueue Started At {DateTime.Now} ", null, projectTitle, correlationId);

            BasicGetResult result = null;
            try
            {
                bool noAck = false;
                if (channel.IsOpen)
                    result = channel.BasicGet(entrypoint, noAck);
                else
                    return;

                if (result == null) // for the first pass the channel is still open so we need to check if result is null
                    return;

                //Check if we can retrieve message from the channel
                string returnMessageFeromQueue = PullMessagesFromQueue(result, channel);

                if (!string.IsNullOrEmpty(returnMessageFeromQueue))
                    //Make a post call to PlatformIntegrationService and send the json string
                    SendMessageToAPIAsync(returnMessageFeromQueue, pushServiceURI, pushServiceURIParams).Wait();
            }
            catch (Exception ex)
            {
                _Logger.WriteLog(ex.Message, ex, projectTitle, correlationId);
                ///reject the message but push back to queue for later re-try
                channel.BasicReject(result.DeliveryTag, true);
            }
            _Logger.WriteLog($"IntegrationToBcsQueue Ended At {DateTime.Now} ", null, projectTitle, correlationId);
        }

        static void BcsToIntegrationQueueMessageProcess(IModel channel, string entrypoint)
        {
            _Logger.WriteLog($"BcsToIntegrationQueue Started At {DateTime.Now}", null, projectTitle, correlationId);

            BasicGetResult result = null;
            try
            {
                bool noAck = false;
                if (channel.IsOpen)
                    result = channel.BasicGet(entrypoint, noAck);
                else
                    return;

                if (result == null) // for the first pass the channel is still open so we need to check if result is null
                    return;

                //Check if we can retrieve message from the channel
                string returnMessageFeromQueue = PullMessagesFromQueue(result, channel);
                
                if (!string.IsNullOrEmpty(returnMessageFeromQueue))
                    //Make a post call to PlatformIntegrationService and send the json string
                    SendMessageToAPIAsync(returnMessageFeromQueue, IntServiceURI, BcsToIntUriParams).Wait();
            }
            catch (Exception ex)
            {
                _Logger.WriteLog(ex.Message, ex, projectTitle, correlationId);
                ///reject the message but push back to queue for later re-try
                channel.BasicReject(result.DeliveryTag, true);
            }

            _Logger.WriteLog($"BcsToIntegrationQueue Ended At {DateTime.Now} ", null, projectTitle, correlationId);
        }

        static string PullMessagesFromQueue(BasicGetResult result, IModel channel)
        {
            if (result == null)
            {
                throw new Exception($"Basic Get Result Object is null");
            }

            IBasicProperties props = result.BasicProperties;
            props.Persistent = true;
            byte[] body = result.Body;
            message = Encoding.UTF8.GetString(body);

            if (!string.IsNullOrEmpty(message))
            {
                // acknowledge receipt of the message
                channel.BasicAck(result.DeliveryTag, false);
                return message;
            }
            else
                throw new Exception($"The message in the queue is null or empty.");
        }
    }
}
