﻿using com.breg.vision.Handoff.ApiClient;
using com.breg.vision.Handoff.Models;
using com.breg.vision.Integration.VisionBcsModels;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace com.breg.vision.RMQInterface.Controllers
{
    public class IntToBcsSubscriberController : ApiController
    {
        private static string RabbitMQHostUri = ConfigurationManager.AppSettings["RabbitMQServiceUrl"];
        private readonly string HandsOffServiceUri = ConfigurationManager.AppSettings["HandsOffServiceUrl"];
        private Logger.FileLogger logger = new Logger.FileLogger();

        [HttpPost]
        public IHttpActionResult QueueRMQRequest([FromBody]string claimJSON)
        {
            try
            {
                BcsClaimSubmission claim = null;
                if (string.IsNullOrWhiteSpace(claimJSON))
                {
                    ModelState.AddModelError(nameof(claimJSON), "Input claim json is null or empty!.");
                }
                else
                {
                    claim = JsonConvert.DeserializeObject<BcsClaimSubmission>(claimJSON);
                    Validate(claim);
                }

                if (!ModelState.IsValid)
                {
                    logger.WriteLog($"Error reading message from Integration to BCS Queue", null, "com.breg.vision.RMQInterface", Guid.NewGuid());
                    return BadRequest(ModelState);
                }

                // Call Hansoff Web Api. claimsJson + documentId.
                var webClient = new ApiClient();
                var data = new HandoffRequestModel() { JobType = JobTypes.IntegrationToBCS, Payload = claimJSON };
                webClient.Post<HandoffRequestModel>(HandsOffServiceUri, data).Wait();

                logger.WriteLog($"Pushed message from Integration to BCS Queue", null, "com.breg.vision.RMQInterface", claim.RMQID);

            }
            catch (System.Exception ex)
            {
                logger.WriteLog(ex.Message, ex, "com.breg.vision.BCSClaimPushService", Guid.NewGuid());
                //log the exception
            }

            return Ok();      
        }
    }

    
}
