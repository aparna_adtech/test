﻿using com.breg.vision.Handoff.ApiClient;
using com.breg.vision.Handoff.Models;
using com.breg.vision.Integration.VisionBcsModels;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.MessagePatterns;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace com.breg.vision.RMQInterface.Controllers
{
    public class VSNToIntSubscriberController : ApiController
    {
        private static string RabbitMQHostUri = ConfigurationManager.AppSettings["RabbitMQServiceUrl"];
        private readonly string HandsOffServiceUri = ConfigurationManager.AppSettings["HandsOffServiceUrl"];
        private Logger.FileLogger logger = new Logger.FileLogger();

        [HttpPost]
        public IHttpActionResult QueueRMQRequest([FromBody]BcsClaimSubmission claim)
        {
            try
            {
                string claimJSON = JsonConvert.SerializeObject(claim);
                if (string.IsNullOrWhiteSpace(claimJSON))
                {
                    ModelState.AddModelError(nameof(claimJSON), "Input claim json is null or empty!.");
                }
                else
                {                   
                    Validate(claim);
                }

                if (!ModelState.IsValid)
                {
                    logger.WriteLog($"Error reading claim model from BregWcfService", null, "com.breg.vision.RMQInterface", Guid.NewGuid());
                    return BadRequest(ModelState);
                }

                // Call Hansoff Web Api. claimsJson + documentId.
                var webClient = new ApiClient();
                var data = new HandoffRequestModel() { JobType = JobTypes.VisionToIntegration, Payload = claimJSON };

                // Send dispensement id info to Document service for upgrade.
                var docData = new HandoffRequestModel() { JobType = JobTypes.VisionToDocument, Payload = claimJSON };

                var visionToIntegration = webClient.Post<HandoffRequestModel>(HandsOffServiceUri, data);
                var visionToDocApi = webClient.Put<HandoffRequestModel>(HandsOffServiceUri, docData);

                Task.WaitAll(new Task[] { visionToIntegration, visionToDocApi });

                // Poll the Handsoff queue to check whether the message is send to BCS and Claim number is generated or not.
                // Once the claim number is generated, push the documents one by one to the queue.
                logger.WriteLog($"Pushed message from Vision to Integration Queue {claimJSON}", null, "com.breg.vision.RMQInterface", Guid.NewGuid());

            }
            catch (System.Exception ex)
            {
                logger.WriteLog(ex.Message, ex, "com.breg.vision.BCSClaimPushService", Guid.NewGuid());
                //log the exception
            }

            return Ok();           
        }     
    }
}
