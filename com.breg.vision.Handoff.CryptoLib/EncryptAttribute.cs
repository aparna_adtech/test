﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.CryptoLib
{
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = true)]
    public sealed class EncryptAttribute : Attribute
    {
        public EncryptAttribute()
        {
            // there are no named or positional parameters to this attribute
        }
    }
}
