﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.breg.vision.Pdf.Compressor
{
    public interface IFileCompressor
    {
        byte[] Compress(byte[] memory_Image);
    }
}
