﻿using ImageMagick;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;

namespace com.breg.vision.Pdf.Compressor.Compressors
{
    internal class ImageMagickCompressor : FileCompressorBase
    {
        private int PosterizationLevel = 3;
        public override byte[] Compress(byte[] memory_Image)
        {
            // compression logic 
            //Size newSize = new Size(Convert.ToInt32(910), Convert.ToInt32(1020));
            //Bitmap bp = ResizeImage(new Bitmap(new MemoryStream(memory_Image)), newSize);
            //var Image_Bytes = BitmapToByte(bp);

            // posterization logic
            MagickImage image = new MagickImage(memory_Image);
            image.Grayscale(PixelIntensityMethod.Average);
            image.Posterize(PosterizationLevel);
            MemoryStream ms = new MemoryStream();

            image.Write(ms, MagickFormat.Png);
            return image.ToByteArray();
        }

        #region Private methods
        private byte[] BitmapToByte(Image img)
        {
            using (var stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                return stream.ToArray();
            }
        }

        private Bitmap ResizeImage(Bitmap mg, Size newSize)
        {
            double ratio = 0d;
            double myThumbWidth = 0d;
            double myThumbHeight = 0d;
            int x = 0;
            int y = 0;

            Bitmap bp;

            if ((mg.Width / Convert.ToDouble(newSize.Width)) > (mg.Height /
            Convert.ToDouble(newSize.Height)))
                ratio = Convert.ToDouble(mg.Width) / Convert.ToDouble(newSize.Width);
            else
                ratio = Convert.ToDouble(mg.Height) / Convert.ToDouble(newSize.Height);
            myThumbHeight = Math.Ceiling(mg.Height / ratio);
            myThumbWidth = Math.Ceiling(mg.Width / ratio);

            //Size thumbSize = new Size((int)myThumbWidth, (int)myThumbHeight);
            Size thumbSize = new Size((int)newSize.Width, (int)newSize.Height);
            bp = new Bitmap(newSize.Width, newSize.Height);
            x = (newSize.Width - thumbSize.Width) / 2;
            y = (newSize.Height - thumbSize.Height);
            // Had to add System.Drawing class in front of Graphics ---
            System.Drawing.Graphics g = Graphics.FromImage(bp);
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            Rectangle rect = new Rectangle(x, y, thumbSize.Width, thumbSize.Height);
            g.DrawImage(mg, rect, 0, 0, mg.Width, mg.Height, GraphicsUnit.Pixel);

            return bp;

        }
        #endregion
    }
}
