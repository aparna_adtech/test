﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.breg.vision.Pdf.Compressor.Compressors
{
    internal abstract class FileCompressorBase : IFileCompressor
    {
        public abstract byte[] Compress(byte[] memory_Image);
    }
}
