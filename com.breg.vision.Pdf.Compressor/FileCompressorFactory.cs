﻿using com.breg.vision.Pdf.Compressor.Compressors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.breg.vision.Pdf.Compressor
{
    public static class FileCompressorFactory
    {
        public static IFileCompressor GetCompressor(CompressorFrameWork Type)
        {
            {
                switch (Type)
                {
                    case CompressorFrameWork.ImageMagick: return new ImageMagickCompressor();
                    default: throw new NotImplementedException();
                }
            }
        }
    }
}
