﻿using ClassLibrary.DAL;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace com.breg.vision.ContentLoaders
{
    /// <summary>
    /// Summary description for PatientSignature
    /// </summary>
    public class PatientSignature : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string strPatientSignatureID = context.Request.QueryString["id"].ToString();
            int patientSignatureID = 0;
            int.TryParse(strPatientSignatureID, out patientSignatureID);
            if (patientSignatureID > 0)
            {
                using (var visionDB = VisionDataContext.GetVisionDataContext())
                {
                    var signatureQuery = (from ds in visionDB.DispenseSignatures
                                where ds.DispenseSignatureID == patientSignatureID
                                select ds).FirstOrDefault();

                    if (signatureQuery != null)
                    {
                        byte[] signature = signatureQuery.PatientSignature.ToArray();

                        if (signature != null && signature.Length > 0)
                        {
                            context.Response.ContentType = "image/jpeg";
                            context.Response.OutputStream.Write(signature, 0, signature.Length);
                            return;
                        }
                    }
                }
            }

            var image = new Bitmap(1, 1);
            image.SetPixel(0, 0, Color.White);
            MemoryStream stream = new MemoryStream();
            image.Save(stream, ImageFormat.Jpeg);
            stream.Position = 0;
            using (BinaryReader br = new BinaryReader(stream))
            {
                byte[] pixelImage = br.ReadBytes(Convert.ToInt32(stream.Length));
                context.Response.ContentType = "image/jpeg";
                context.Response.OutputStream.Write(pixelImage, 0, pixelImage.Length);
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

		public static byte[] GetImageBytes(int dispenseSignatureID)
		{
			using (var visionDb = VisionDataContext.GetVisionDataContext())
			{
				var signatureQuery = (from ds in visionDb.DispenseSignatures
										where ds.DispenseSignatureID == dispenseSignatureID
										select ds).FirstOrDefault();

				if (signatureQuery != null)
					return signatureQuery.PatientSignature.ToArray();
			}
			return null;
		}

		public static string GetImageUrl(int dispenseSignatureID, bool fromVision, bool visible)
		{
			string signatureBase64 = string.Empty;

			if (visible)
			{
				byte[] signature = GetImageBytes(dispenseSignatureID);
				if (signature != null)
					return "data:image/png;base64," + Convert.ToBase64String(signature);
			}
			return GetEmptySignatureImageUrl();
		}

		private static string GetEmptySignatureImageUrl()
        {
            string signatureBase64;
            var image = new Bitmap(1, 1);
            image.SetPixel(0, 0, Color.White);
            MemoryStream stream = new MemoryStream();
            image.Save(stream, ImageFormat.Jpeg);
            stream.Position = 0;
            using (BinaryReader br = new BinaryReader(stream))
            {
                byte[] pixelImage = br.ReadBytes(Convert.ToInt32(stream.Length));
                signatureBase64 = Convert.ToBase64String(pixelImage);
            }
            return "data:image/jpeg;base64," + signatureBase64;
        }
    }
}