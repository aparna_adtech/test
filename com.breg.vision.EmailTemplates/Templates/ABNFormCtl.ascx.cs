﻿using com.breg.vision.ContentLoaders;
using System;

namespace com.breg.vision.EmailTemplates
{
    public partial class ABNFormCtl : System.Web.UI.UserControl
    {
        public int PracticeLocationID { get; set; }
        public string PracticeLocationName { get; set; }
        public string PracticeName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string BillingPhone { get; set; }
        public string BillingFax { get; set; }
        public string PatientName { get; set; }

        public string ProductName { get; set; }
        public Decimal ProductCost { get; set; }

        public string PatientCode { get; set; }
        public string AbnReason { get; set; }

        public int PatientSignatureID { get; set; }
        public DateTime? PatientSignedDate { get; set; }


        public string PhysicianName { get; set; }

        public int MedicareOption { get; set; }

        public bool FromVision { get; set; }

        public string ImageUrl { get; private set; }

        public string ABNType { get; set; }

        public string PatientSignerRelationship { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadControlInfo();
        }

        public void LoadControlInfo()
        {
            lblPracticeName.Text = PracticeName.ToString();
            lblAddress1.Text = Address1.ToString();
            lblAddress2.Text = Address2.ToString();
            lblCity.Text = City.ToString();
            lblState.Text = State.ToString();
            lblZip.Text = Zip.ToString();
            if (!string.IsNullOrEmpty(BillingPhone))
            {
                lblPhone.Text = "Ph" + (BillingPhone.Length == 10 ? "(" + BillingPhone.Substring(0, 3) + ")" + BillingPhone.Substring(3, 3) + "-" + BillingPhone.Substring(6, 4) : BillingPhone.ToString());
            }
            if (!string.IsNullOrEmpty(BillingFax))
            {
                lblFax.Text = "F " + (BillingFax.Length == 10 ? "(" + BillingFax.Substring(0, 3) + ")" + BillingFax.Substring(3, 3) + "-" + BillingFax.Substring(6, 4) : BillingFax.ToString());
            }
            lblProductName.Text = ProductName;
            lblCost.Text = ProductCost.ToString("c");
            lblPatientName.Text = PatientName;
            lblPatientCode.Text = PatientCode;
            lblAbnReason.Text = AbnReason;

            // we want it to render an image either way.  either the signature or a single white dot
            bool renderSignatureUrl = this.PatientSignatureID > 0;

            ImageUrl = PatientSignature.GetImageUrl(PatientSignatureID, FromVision, renderSignatureUrl);

            if (this.PatientSignatureID > 0 && PatientSignedDate.HasValue)
            {
                lblSignatureDate.Text = PatientSignedDate.Value.ToShortDateString();
                lblSignatureDate.Visible = true;
            }

            if (!string.IsNullOrEmpty(this.PatientSignerRelationship) && PatientSignerRelationship != null)
            {
                lblPatientSignerRelationship.Text = $"K. Patient/Signer Relationship: {PatientSignerRelationship}";
                lblPatientSignerRelationship.Visible = true;
            }

            switch (this.MedicareOption)
            {
                case 1:
                    chkMedicare1.Checked = true;
                    break;
                case 2:
                    chkMedicare2.Checked = true;
                    break;
                case 3:
                    chkMedicare3.Checked = true;
                    break;
                default:
                    break;
            }
                
           
        }
    }
}