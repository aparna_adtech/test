﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Warranty.ascx.cs" Inherits="com.breg.vision.EmailTemplates.Warranty" %>
<div style="page-break-before: always;">
    <h3 class="warranty">
        <u>ATTENTION: PATIENT</u></h3>
    <p>
        Important Information Regarding the Medical Product You Are About to Receive:</p>
    <p>
        <asp:Label ID="lblPractice01" runat="server" CssClass="practiceText"></asp:Label> is providing you with the brace, splint, support, and/or other medical device that the healthcare professional has prescribed for your care. It is our policy to provide high quality product to support the healthcare professional's protocol for treatment. <asp:Label ID="lblPractice02" runat="server" CssClass="practiceText" Text="[Practice]"></asp:Label> will bill your insurance company for the product. *<u>Remember that if the bill is denied or is not covered by your insurance company, you will be responsible for payment in full of this bill.</u> *<em>If you do not wish to receive this product from the healthcare professional, you may ask your healthcare professional for a prescription. The prescription can be filled at a local Orthotic &amp; Prosthetic shop or medical supply company.</em></p>
    <p>
        <span style="text-decoration: underline;">Orthotic and Prosthetic Deposit or Medicare Co-Insurance</span>: It has become common practice in the orthotic and prosthetic industry to require a Deposit or Medicare Co-Insurance pending further reimbursement from the insurance companies.  This &quot;Deposit&quot; or &quot;Medicare Co-Insurance&quot; is applied toward the total cost of the brace/supply. Insurance providers may have a separate deductible and co-insurance for orthotics and prosthetics. If your insurance covers the brace/supply at 100% then the Deposit or Medicare Co-Insurance will be credited to your <asp:Label ID="lblPractice03" runat="server" CssClass="practiceText" Text="[Practice]"></asp:Label> account.</p>
    <p>
        <span style="text-decoration: underline;">Return Policy for Orthotics &amp; Prosthetics</span>: Medical devices are considered one time use and once the device leaves the healthcare professional's office they are considered non-returnable. There are two exceptions to the rule. 1) If there is a manufacturer defect. 2) If the product was deemed unsuitable for the patient at the time it was provided.  If there is a manufacturer defect, the product will be exchanged for the same product.</p>
    <p>
        <span style="text-decoration: underline;">Fitting and Safety Instructions</span>: Apply and remove the brace as instructed at the time of fitting or described in detail with the instructions for use.  Wear the brace for the period of time described by your healthcare professional.  The brace can be cleaned with mild soap and water and allowed to air dry only.  Should the brace result in increased pain, decreased feeling (numbness/tingling), increased swelling or an overall worsening of your medical condition, please contact our office immediately.  <span style="font-weight: bold;">If an emergency situation occurs from wearing the brace after our business hours, please dial 911 and seek immediate medical attention.</span> Please contact our office should you have any questions about the brace issued to you.</p>
    <p>
        <span style="text-decoration: underline;">Medicare Secondary Payer Guideline</span>:  It is against the Medicare Secondary Payer laws to accept payment from a beneficiary upon admission or when services are being rendered when another insurer is primary to Medicare.</p>
    <p>
        <span style="text-decoration: underline;">Medicare Beneficiary</span>: The products and/or services provided to you by (<asp:Label ID="lblPractice04" runat="server" CssClass="practiceText" Text="[Practice]"></asp:Label>) are subject to the supplier standards contained in the Federal regulations shown at 42 Code of Federal Regulations Section 424.57(c). These standards concern business, professional and operational matters (e.g., honoring warranties and hours of operation). The full text of these standards can be obtained at http://ecfr.gpoaccess.gov. Upon request we will furnish you a written copy of the standards.</p>
</div>
<div style="">
    <asp:Repeater ID="rptrWarranty" runat="server" OnItemDataBound="rptrWarranty_ItemDataBound">
        <HeaderTemplate>
            <div>
                <h3 class="warranty">
                    <u>DMEPOS Product Warranty</u></h3>
                <p>
                    Products are warranted for manufacturing or material defects. If you should have
                    an issue with your product under warranty, please return it to
                    <asp:Label ID="lblPractice05" runat="server" CssClass="practiceText" Text="[Practice]"></asp:Label>
                    for repair or replacement at no charge. The warranty periods are as follows:</p>
            </div>
        </HeaderTemplate>
        <ItemTemplate>
            <div style="margin-top: 20px;">
                <h3 class="warranty">
                    <u>
                        <%# DataBinder.Eval(Container.DataItem, "SupplierName") %></u></h3>
                <asp:Repeater ID="rptrProducts" runat="server">
                    <ItemTemplate>
                        <p>
                            <%# DataBinder.Eval(Container.DataItem, "ProductName") %>:
                            <%# DataBinder.Eval(Container.DataItem, "Warranty") %></p>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>
