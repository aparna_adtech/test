﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary.DAL;

namespace com.breg.vision.EmailTemplates
{
    public partial class CustomFitCriteria : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public DispenseResults Dispensement;

        public void LoadControlInfo()
        {
            using (var db = VisionDataContext.GetVisionDataContext())
            {
                var dispenseDetail = db.DispenseDetails.First(dd => dd.DispenseDetailID == Dispensement.DispenseDetailID);

                var fitter = dispenseDetail.Fitter;
                    //If fitter is missing after updating dbml, rename Parent relationship that uses FitterID to Clinician table.
                    //In properties on association between DispenseDetail and Clinician tables (Clinician_DispenseDetail1) the 
                    //Parent Property -> Name may have changed to Clinician1.  Change it to Fitter.,  Ask Ron or Basil.
                if (fitter != null)//Being defensive while definition for IsCustomFit behavior is in question
                {
                    lblBraceFitter.Text = fitter.Contact.FirstName + " " + fitter.Contact.LastName + " " +
                                          fitter.Contact.Suffix;
                    lblProductName.Text = Dispensement.ShortName;

                    var procedureList = from procedure in db.CustomFitProcedures
                        join selected in db.DispenseQueue_CFPs on
                            procedure.CustomFitProcedureID equals selected.CustomFitProcedureID
                            into g
                        from subprocedure in (from selected in g
                            where selected.DispenseQueueID == dispenseDetail.DispenseQueueID
                            select selected).DefaultIfEmpty()
                        select new DisplayProcedure
                        {
                            Description =
                                subprocedure.UserInput == null
                                    ? procedure.Description
                                    : "EXPLANATION: " + subprocedure.UserInput,
                            IsChecked = (subprocedure != null)
                        };

                    Procedures = procedureList.ToList<DisplayProcedure>();
                }
            }
        }

        public List<DisplayProcedure> Procedures { get; set; }

        public class DisplayProcedure
        {
            public string Description { get; set; }
            public bool IsChecked { get; set; }
        }
    }
}