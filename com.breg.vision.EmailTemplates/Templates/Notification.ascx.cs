﻿using com.breg.vision.ContentLoaders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.breg.vision.EmailTemplates
{
    public partial class Notification : System.Web.UI.UserControl
    {
        public string productName { get; set; }
        public int PatientSignatureID { get; set; }
        public DateTime? PatientSignedDate { get; set; }
        public string PhysicianName { get; set; }
        public bool FromVision { get; set; }
        public string ImageUrl { get; private set; }
        public string PatientSignerRelationship { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            //LoadControlInfo();
        }

        public void LoadControlInfo()
        {
            lblProductName.Text = productName;

            // we want it to render an image either way.  either the signature or a single white dot
            bool renderSignatureUrl = this.PatientSignatureID > 0;

            ImageUrl = PatientSignature.GetImageUrl(PatientSignatureID, FromVision, renderSignatureUrl);


            if (this.PatientSignatureID > 0 && PatientSignedDate.HasValue)
            {
                lblSignatureDate.Text = PatientSignedDate.Value.ToShortDateString();
                lblSignatureDate.Visible = true;
            }

            if (!string.IsNullOrEmpty(this.PatientSignerRelationship) && PatientSignerRelationship != null)
            {
                lblPataientSignerRelationships.Text = $"Patient/Signer Relationship: {PatientSignerRelationship}";
                lblPataientSignerRelationships.Visible = true;
            }

        }
    }
}