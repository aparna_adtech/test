﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomFitCriteria.ascx.cs" Inherits="com.breg.vision.EmailTemplates.CustomFitCriteria" %>
<div style="font-size: 8pt; line-height: 1.0; margin-top: 15px; page-break-inside: avoid;">
    <div style="font-weight: bold; font-size: 130%;">
        CUSTOM FIT CRITERIA
    </div>
    <div style="padding: 5px; border: 1px solid gray;">
        <table bgcolor="white" width="100%">
            <tr>
                <td align="right" width="15%">Bracing Fitter:</td>
                <td>&nbsp;<asp:Label ID="lblBraceFitter" runat="server"></asp:Label></td>
                <td align="right" width="15%">Product:</td>
                <td>&nbsp;<asp:Label ID="lblProductName" runat="server"></asp:Label></td>
            </tr>
       </table>
        <% if (Procedures.Count > 0){ %>
        Modification<% if(Procedures.Count > 1){%>s<% } %>:
        <ol style="list-style: none;">
                <% foreach (var procedure in Procedures)
                   { %>
                    <li><input type="checkbox" disabled <%= procedure.IsChecked? "checked" : "" %> />&nbsp;<%= procedure.Description %></li>
                <% } %>
        </ol>
        <% } %>
        <br/>
        <div><i>Item was customized to fit the patient by an individual with expertise and/or specialized training, or
            by a physician or an individual working under the direct supervision of a physician.</i></div>
    </div>
    <br/>
</div>