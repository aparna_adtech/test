﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HippaWaiver.ascx.cs"
    Inherits="com.breg.vision.EmailTemplates.HippaWaiver" %>
<div style="page-break-inside: avoid; page-break-after: always;">
    <h3 class="warranty">
        <u>E-MAIL WAIVER</u></h3>
    <span class="warrantyLgText">The Health Insurance Portability and Accountability Act
        of 1996 (HIPAA) sets standards for protecting the rights of individuals (patients).
        <asp:Label ID="lblPractice01" runat="server" CssClass="practiceText"></asp:Label>
        follows the laws that grant every individual the right to the privacy and confidentiality
        of their health information. To comply with HIPAA regulations, email correspondence
        that contains protected health information must be sent encrypted (secured). If
        you wish to have unencrypted (unsecure) e-mail sent to you for the sake of your
        convenience, you must sign the following waiver:</span><br />
    <br />
    <span class="warrantyLgText">I
        <asp:Label ID="lblPatientNameRequest" runat="server" CssClass="practiceText"></asp:Label>
        request that, for my convenience,
        <asp:Label ID="lblPractice02" runat="server" CssClass="practiceText"></asp:Label>
        correspond with me by unencrypted (unsecure) e-mail to relay information concerning products/DME received
		<asp:Label ID="lblReceivedDate01" runat="server" CssClass="practiceText"></asp:Label>.
		I understand that emails sent
        to me may contain protected health information. I further understand that unencrypted
        e-mail and e-mail attachments are not secure and may be viewed by others. I agree
        to hold harmless <asp:Label ID="lblPractice03" runat="server" CssClass="practiceText"></asp:Label>,
        its officers, agents, employees, and contract health providers from any and all liability,
        loss, damages, costs, or expenses which are sustained, incurred, or required arising
        from the transmission of unencrypted (unsecure) e-mail correspondence and attachments.</span><br />
    <br />
    <span class="warrantyLgText">I hereby direct
        <asp:Label ID="lblPractice04" runat="server" CssClass="practiceText"></asp:Label>
        to send all emails relating to products/DME received on
		<asp:Label ID="lblReceivedDate02" runat="server" CssClass="practiceText"></asp:Label>
		in an unencrypted (unsecure) format to this address:
        <asp:Label ID="lblPatientEmail" runat="server" CssClass="practiceText"></asp:Label>.</span><br />
    <br />
    <span class="warrantyLgText">This waiver will remain in force until revoked in writing.
        It may be revoked in writing at any time.</span><br />
    <br />
    <span class="warrantyLgText">
        <asp:Label ID="lblSignature" runat="server" Text="Signature:"></asp:Label>
        <asp:Literal ID="PatientSignatureLiteralControl" runat="server"></asp:Literal>
        <asp:Literal ID="PatientDeclineSignatureLiteralControl" runat="server" Visible="false"></asp:Literal><br />
        <asp:Label ID="lblPatientSignatureDate" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblPatientRefusalDate" runat="server" Visible="false"></asp:Label><br />
        <asp:Label ID="lblPatientSignerRelationships" runat="server" Visible="false"></asp:Label><br />
        <asp:Label ID="lblPatientRefusalReason" runat="server" Visible="false"></asp:Label><br />
        <asp:Label ID="lblPatientName" runat="server"></asp:Label><br />
        <asp:Label ID="lblCreatedDate" runat="server" Text="Date:"></asp:Label>
    </span>
    <asp:Label ID="lblDate" runat="server" CssClass="practiceText"></asp:Label><br />
</div>
