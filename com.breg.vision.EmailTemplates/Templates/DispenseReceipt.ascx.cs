﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary.DAL;
using System.Data;
using System.Configuration;
using com.breg.vision.ContentLoaders;
using System.IO;

namespace com.breg.vision.EmailTemplates
{
    public partial class DispenseReceipt : System.Web.UI.UserControl
    {
        public bool IsBillingChargeDisplayed{ get; set; }
        public int DispenseID { get; set; }
        public int PracticeLocationID { get; set; }
        public string PracticeLocationName { get; set; }
		public Practice Practice { get; set; }
		public bool LastItem { get; set; }
        public int PatientSignatureID { get; set; }
        public DateTime? PatientSignedDate { get; set; }
        public string PhysicianName { get; set; }
        public string PatientRefusalReason { get; set; }
        public bool FromVision { get; set; }
        public string PatientSignerRelationship { get; set; }
		public string FitterName { get; set; }
		public bool IsSupportsBregBilling { get; set; }

		protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void LoadControlInfo(int dispenseID, string emailTemplatePath)
        {
            string createdDate = GetCreateDate(dispenseID).ToString(TemplateHelper.DateTimeFormat);
            lblPracticeName.Text = this.Practice.PracticeName;
            lblPracticeName2.Text = this.Practice.PracticeName.ToUpper();
            lblPracticeAuth.Text = this.Practice.PracticeName;
            lblPracticeInsurance.Text = this.Practice.PracticeName;
            lblPracticeLocationName.Text = this.PracticeLocationName;
            lblDateDelivered.Text = createdDate;  //pod(proof of delivery)
            lblPatientSignerRelationships.Text = this.PatientSignerRelationship; // setting the patient relationship
            lblFitter.Text = this.FitterName;
            PopulateAddressWithShipping();

            PopulateSpecificContact();

			PopulateDisclaimers();

			List<DispenseResults> dispenses = GetDispenseReceipt(dispenseID);

            if (dispenses.Count > 0)
            {
                var dispense1 = dispenses[0];

                if (!dispense1.IsHcpSignatureRequired)
                {
                    detailedWrittenOrderBody.Visible = false;
                    detailedWrittenOrderHeader.Visible = false;
                }

                if (dispense1.PhysicianSigned == true)
                {
                    lblPhysicianName.Text = string.Format("{0} {1} {2} {3}", dispense1.Title, dispense1.FirstName,
                        dispense1.LastName, dispense1.Suffix);
                    lblPhysicianSignedDate.Text = dispense1.PhysicianSignedDate.HasValue ? (dispense1.PhysicianSignedDate.Value.ToString(TemplateHelper.DateFormat) + "<br/>" + (dispense1.PhysicianSignedDate.Value.ToString(TemplateHelper.TimeFormat))) : string.Empty;
                }
                else
                {
                    lblElectronicallySigned.Visible = false;
                }

                string patientCodeMRN = dispense1.PatientCode;
                string patientName = (dispense1.PatientFirstName ?? string.Empty) + " " + (dispense1.PatientLastName ?? string.Empty);
                string patientDOB = ""; // DOB goes here
                string dispensementNote = dispense1.Note;

                lblDetailedWrittenOrderPatientName.Text = patientName;
                lblDetailedWrittenOrderPatientCode.Text = patientCodeMRN;
                lblDetailedWrittenOrderPatientDOB.Text = patientDOB;
                lblDetailedWrittenOrderNotes.Text = dispensementNote;
                
                rptrDispenseReceipt.DataSource = dispenses;
                rptrDispenseReceipt.DataBind();

                rptrDispenseReceipt1.DataSource = dispenses; //pod(proof of delivery)
                rptrDispenseReceipt1.DataBind(); //pod(proof of delivery)

                var bregProductCount = (from b in dispenses
                                        where b.SupplierName.ToLower().Contains("breg") 
                                        select b).Count();
                if (bregProductCount > 0)
                {
                    divBregProduct.Visible = true;
                }

                lblConsentPatientName.Text = patientName;
                lblProofOfDeliveryPatientName.Text = patientName;
                lblPhysicianName1.Text = string.Format("{0} {1} {2} {3}", dispense1.Title, dispense1.FirstName, dispense1.LastName, dispense1.Suffix);
                lblPhysicianName2.Text = string.Format("{0} {1} {2} {3}", dispense1.Title, dispense1.FirstName, dispense1.LastName, dispense1.Suffix);
                lblICD9Code.Text = dispense1.ICD9Code;

                if (!string.IsNullOrEmpty(this.PatientSignerRelationship) && PatientSignerRelationship != null)
                {
                    lblPatientSignerRelationships.Text = $"Patient/Signer Relationship: {dispense1.PatientSignerRelationship}";
                    lblPatientSignerRelationships.Visible = true;
                }

                if (!string.IsNullOrEmpty(FitterName))
                {
                    lblFitter.Text = $"Fitter: {FitterName}";
                    lblFitter.Visible = true;
                }
                var productNames = string.Join(" and ", dispenses.AsEnumerable().Select(d => d.ShortName).ToList());
                lblProductNames.Text = productNames;

                bool? patientAccepted = null;
                if (!string.IsNullOrEmpty(this.PatientRefusalReason)) // this means it was probably from vision where we don't capture this field yet
                {
                    patientAccepted = (this.PatientRefusalReason.ToLower() == "accepted");
                }

                // we want it to render an image either way.  either the signature or a single white dot
                bool renderSignatureUrl = patientAccepted.HasValue && (patientAccepted.Value == true) && (this.PatientSignatureID > 0);
                bool renderDeclineUrl = patientAccepted.HasValue && (patientAccepted.Value == false) && (this.PatientSignatureID > 0);

                Action<Literal, string> _SetPatientSignatureLiteralControl =
                    (c, url) =>
                    {
                        if (c != null)
                            c.Text = string.Format(@"<img height='87' width='216' src='{0}' alt='' />", url);
                    };
                _SetPatientSignatureLiteralControl(PatientSignatureLiteralControl, PatientSignature.GetImageUrl(this.PatientSignatureID, FromVision, renderSignatureUrl));

                if (this.PatientSignatureID > 0 && PatientSignedDate.HasValue)
                {
                    if (patientAccepted == true)
                    {
                        lblPatientSignatureDate.Text = PatientSignedDate.Value.ToString(TemplateHelper.DateTimeFormat);
                        lblPatientSignatureDate.Visible = true;
                    }
                    lblCreatedDate.Text = PatientSignedDate.HasValue ? PatientSignedDate.Value.ToString(TemplateHelper.DateTimeFormat) : "          "; // TODO: need to refactor this to use styling to build space instead of spaces
                }

                using (VisionDataContext visionDB = VisionDataContext.GetVisionDataContext())
                {
                    var practicePayer = visionDB.PracticePayers
                        .Join(visionDB.Dispenses, pp => pp.PracticePayerID, d => d.PracticePayerID,
                            (pp, d) => new {Payer = pp, Dispense = d})
                        .SingleOrDefault(d => d.Dispense.DispenseID == dispenseID);
                    if (practicePayer != null)
                    {
                        lblPayer.Text = practicePayer.Payer.Name;
                    }

                }
            }

            if (dispenses.Any(d => d.IsCustomFit.HasValue && d.IsCustomFit.Value))
            {
                AttachCustomFitCriteria(dispenses, emailTemplatePath);
            }

            // check if order was a custom brace order and attach custom brace details
            AttachCustomBraceDetails(emailTemplatePath);

			if (IsSupportsBregBilling)
			{
				Dictionary<string, string> templateTexts = new Dictionary<string, string>();
				if (this.PatientSignerRelationship != null)
					templateTexts.Add("PatientSignerRelationship", this.PatientSignerRelationship);
				if (this.PatientSignedDate != null)
					templateTexts.Add("PatientSignedDate", this.PatientSignedDate != null ? this.PatientSignedDate.Value.ToShortDateString() : string.Empty);

				Dictionary<string, byte[]> templateImages = new Dictionary<string, byte[]>();
				byte[] signature = PatientSignature.GetImageBytes(this.PatientSignatureID);
				if (signature != null)
					templateImages.Add("Signature", signature);

				PDFRenderer.AddPDFAsControls(this.Page, this.Controls, "BillingPatientACK.pdf", emailTemplatePath, templateTexts, templateImages);
			}
		}

		private void PopulateDisclaimers()
		{
			if (IsSupportsBregBilling)
			{
				DisclaimerBilled.Text =
					"<tr>" +
						"<td colspan=\"6\">" +
							"<label>" +
								"- Your services/supplies may be provided and/or billed through any of Breg's family of companies including Breg, Inc. " +
								"and certain other Breg Affiliates.</label>" +
						"</td>" +
					"</tr>";

				DisclaimerReceivedReviewed.Text =
					"<tr>" +
						"<td colspan=\"6\">" +
							"<label>" +
								"- I have received and reviewed the Patient Information Sheet with Deposit, Return Policy, Warranty " +
								"Information, Fitting/Safety Instructions, Notice of Privacy Practices, Patient Bill " +
								"of Rights, Contact and Supplier Information, and Supplier Standards (if applicable)</label>" +
						"</td>" +
					"</tr>";
			}
		}

		private void PopulateSpecificContact()
        {
            
            var practiceSpecificContact = DAL.Practice.Practice.GetContact(Practice.PracticeID);
            var specificContact = practiceSpecificContact != null && practiceSpecificContact.IsSpecificContact? practiceSpecificContact:null;

            if (specificContact == null)
            {
                specificContact = DAL.PracticeLocation.PracticeLocation.GetContact(PracticeLocationID);
                specificContact = specificContact != null && specificContact.IsSpecificContact ? specificContact : null;
            }

            if (specificContact != null)
            {
                lblSpecificContact.Text = BuildSpecificContactDisplay(specificContact);
            }

        }

        public static string BuildSpecificContactDisplay(Contact specificContact)
        {
            var specificContactBuilder = new StringBuilder();
            specificContactBuilder.Append("Contact Name: ");
            specificContactBuilder.Append(specificContact.FirstName);
            specificContactBuilder.Append(" ");
            specificContactBuilder.Append(specificContact.LastName);
            specificContactBuilder.Append(" ");
            if (!string.IsNullOrWhiteSpace(specificContact.PhoneWork))
            {
                specificContactBuilder.Append("Phone: ");
                specificContactBuilder.Append(Regex.Replace(specificContact.PhoneWork, @"(\d{3})(\d{3})(\d{4})", "($1)$2-$3"));
                if (!string.IsNullOrWhiteSpace(specificContact.PhoneWorkExtension))
                {
                    specificContactBuilder.Append(" Ext: ");
                    specificContactBuilder.Append(specificContact.PhoneWorkExtension);
                }
            }

            if (!string.IsNullOrWhiteSpace(specificContact.EmailAddress))
            {
                specificContactBuilder.Append(" Email: ");
                specificContactBuilder.Append(specificContact.EmailAddress);
            }
            return specificContactBuilder.ToString();
        }


        private void PopulateAddressWithBilling()
        {
            DataRow drPracticeLocationBillingAddress = null;
            DAL.PracticeLocation.BillingAddress dalPLBA = new DAL.PracticeLocation.BillingAddress();
            string attentionOf = "";
            int addressID = 0;
            string addressLine1 = "";
            string addressLine2 = "";
            string city = "";
            string state = "";
            string zipCode = "";
            string zipCodePlus4 = "";
            dalPLBA.Select(PracticeLocationID,
                out attentionOf,
                out addressID,
                out addressLine1,
                out addressLine2,
                out city,
                out state,
                out zipCode,
                out zipCodePlus4);

            lblAddressLine1.Text = addressLine1;
            lblCity.Text = city;
            lblState.Text = state;
            lblZipCode.Text = zipCode;

            string fax = "";
            DataSet dsPracticeLocationBillingAddress = dalPLBA.Select(PracticeLocationID);
            if (dsPracticeLocationBillingAddress.Tables[0].Rows.Count > 0)
            {
                drPracticeLocationBillingAddress = dsPracticeLocationBillingAddress.Tables[0].Rows[0];
                fax = drPracticeLocationBillingAddress["Fax"].ToString();
                string phone = drPracticeLocationBillingAddress["WorkPhone"].ToString();
                lblFax.Text = fax;
                lblPhone.Text = phone;
            }
        }
        private void PopulateAddressWithShipping()
        {
            var dalPracticeLocationShippingAddress = new DAL.PracticeLocation.ShippingAddress();
            var practiceLocationShippingAddress = dalPracticeLocationShippingAddress.Select(PracticeLocationID);
            if (practiceLocationShippingAddress != null)
            {
                //First field is pod field and second is header information
                lblAddressLine1_1.Text  = lblAddressLine1.Text  = practiceLocationShippingAddress.ShipAddress1;
                lblCity1.Text           = lblCity.Text          = practiceLocationShippingAddress.ShipCity;
                lblState1.Text          = lblState.Text         = practiceLocationShippingAddress.ShipState;
                lblZip1.Text            = lblZipCode.Text       = practiceLocationShippingAddress.ShipZipCode;
            }
            
            var contact = new DAL.PracticeLocation.Contact().Select(PracticeLocationID);
            if (contact != null)
            {
                string phone = contact.PhoneWork;
                string extension = contact.PhoneWorkExtension;
                if (!string.IsNullOrWhiteSpace(phone))
                {
                    if (!string.IsNullOrWhiteSpace(extension))
                    {
                        phone += " ext: " + extension;
                    }
                }
                lblFax.Text = contact.Fax;
                lblPhone.Text = phone;
            }
        }


        private void AttachCustomBraceDetails(string emailTemplatePath)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                // grab distinct list of order ids
                var orderIds = (from d in visionDB.Dispenses
                                join dd in visionDB.DispenseDetails on d.DispenseID equals dd.DispenseID
                                join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                                where d.DispenseID == DispenseID && dq.BregVisionOrderID.HasValue
                                select dq.BregVisionOrderID.Value)
                                .Distinct()
                                .ToArray();

                // add each order id only if we have a valid shopping cart custom brace record
                foreach (var orderId in orderIds)
                {
                    // skip if no record
                    if (!visionDB.ShoppingCartCustomBraces.Any(x => x.BregVisionOrderID.Value == orderId))
                        continue;

                    // add page
                    Page page = this.Page;
                    if (HttpContext.Current != null)
                    {
                        var ctl = page.LoadControl(string.Format("{0}CustomBraceJPG.ascx", emailTemplatePath)) as CustomBraceJPG;
                        ctl.PracticeID = this.Practice.PracticeID;
                        ctl.PracticeLocationID = this.PracticeLocationID;
                        ctl.BregVisionOrderID = orderId;
						ctl.DisplayType = "JPG";
						ctl.LoadControlInfo();
						this.Controls.Add(ctl);
                    }
                }
            }
        }

        private void AttachCustomFitCriteria(IEnumerable<DispenseResults> dispenses, string emailTemplatePath)
        {
            var page = this.Page;

            foreach (var dispensement in dispenses.Where(d => d.IsCustomFit.HasValue && d.IsCustomFit.Value))
            {
                var ctl = page.LoadControl(string.Format("{0}CustomFitCriteria.ascx", emailTemplatePath)) as CustomFitCriteria;
                            ctl.Dispensement = dispensement;
                        ctl.LoadControlInfo();
                        this.Controls.Add(ctl);
                    }
            
        }

        public static string CreateModifierString(string mod1, string mod2, string mod3)
        {
            var modifiersList = new List<string>();
            if (!string.IsNullOrEmpty(mod1))
                modifiersList.Add(mod1);
            if (!string.IsNullOrEmpty(mod2))
                modifiersList.Add(mod2);
            if (!string.IsNullOrEmpty(mod3))
                modifiersList.Add(mod3);
            var modArray = modifiersList.ToArray();

            return string.Join(",", modArray);
        }

        private static List<DispenseResults> GetDispenseReceipt(int dispenseID)
        {
            using (VisionDataContext visionDB = VisionDataContext.GetVisionDataContext())
            {

                var dispenseQuery = from d in visionDB.usp_GetDispenseReceipt(dispenseID)
                                    select new DispenseResults
                                    {
                                        Code = d.Code,
                                        Color = d.Color,
                                        DMEDeposit = d.DMEDeposit ?? 0 * d.Quantity ?? 0,
                                        FirstName = d.FirstName,
                                        Gender = d.Gender,
                                        Grandtotal = d.Grandtotal,
                                        HCPCSString = d.HCPCSString,
                                        ICD9Code = d.ICD9Code,
                                        IsCustomBrace = d.IsCustomBrace,
                                        LastName = d.LastName,
                                        LineTotal = d.LineTotal,
                                        Packaging = d.Packaging,
                                        PatientCode = d.PatientCode,
                                        PhysicianSigned = d.PhysicianSigned,
                                        Quantity = d.Quantity,
                                        ReceiptCode = d.ReceiptCode,
                                        ShortName = d.ShortName,
                                        Side = d.Side,
                                        Size = d.Size,
                                        Suffix = d.Suffix,
                                        SupplierName = d.SupplierName,
                                        Title = d.Title,
                                        Total = (d.Total ?? 0 * d.Quantity ?? 0),
                                        PatientFirstName = d.PatientFirstName,
                                        PatientLastName = d.PatientLastName,
                                        Note = d.Note,
                                        IsCustomFit = d.IsCustomFit,
                                        FitterID = d.FitterID,
                                        DispenseDetailID = d.DispenseDetailID,
                                        PhysicianSignedDate = d.PhysicianSignedDate,
                                        PatientSignerRelationship= d.PatientSignatureRelationship,
                                        IsHcpSignatureRequired = d.IsHcpSignatureRequired,
                                        Modifiers = CreateModifierString(d.Mod1, d.Mod2, d.Mod3),
                                        Mod1 = d.Mod1,
                                        Mod2 = d.Mod2,
                                        Mod3 = d.Mod3
                                    };

                List<DispenseResults> dispenses = dispenseQuery.ToList<DispenseResults>();

                if (dispenses != null)
                {
                    return dispenses.ToList<DispenseResults>();
                }

                throw new ApplicationException("No Dispense Items Found");
            }
        }



        private static DateTime GetCreateDate(int dispenseID)
        {
            using (VisionDataContext visionDB = VisionDataContext.GetVisionDataContext())
            {
                var createDateQuery = (from cd in visionDB.usp_GetCreatedDateForReceipt(dispenseID)
                                       select cd).FirstOrDefault();

                if (createDateQuery != null)
                {
                    return createDateQuery.DateDispensed;
                }

                throw new ApplicationException("Dispense Date Not Found.");
            }
        }
    }

    public class DispenseResults : usp_GetDispenseReceiptResult
    {
        public Decimal Total { get; set; }

        public DateTime? PhysicianSignedDate { get; set; }

        public string PatientSignerRelationship { get; set; }

        public string Modifiers { get; set; }
    }
}
