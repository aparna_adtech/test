﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ServiceRefusal.ascx.cs" Inherits="com.breg.vision.EmailTemplates.ServiceRefusal" %>

<div style="font-size: 8pt; line-height: 1.0; margin-top: 15px; page-break-inside: avoid;">
    <div style="font-weight: bold; font-size: 130%;">
        SERVICE REFUSAL
    </div>
    <div style="padding: 5px; border: 1px solid gray;">
        <table bgcolor="white" width="100%">
            <tr>
                <td align="right" width="15%">Service Refusal:
                    </td>
                <td>&nbsp;
                    </td>
            </tr>
            <tr>
                <td>&nbsp;
                    </td>
                <td align="left">I have decided not to receive this item from
                    <asp:Label ID="lblPracticeRefusal" runat="server"></asp:Label>

                </td>
            </tr>
            <tr>
                <td>&nbsp;
                    </td>
                <td align="left">I understand that my physician has prescribed this item as part of my treatment
                    </td>
            </tr>
            <tr>
                <td align="right">&nbsp;
                    </td>
                <td align="left">&nbsp;
                    </td>
            </tr>
            <tr>
                <td align="right">Reason for Refusal:
                    </td>
                <td align="left">
                    <asp:Label ID="lblPatientRefusalReason" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right" valign="bottom">Patient/Guarantor Signature:
                    </td>
                <td align="left" valign="bottom">
                    <asp:Literal ID="PatientDeclineSignatureLiteralControl" runat="server"></asp:Literal>
                </td>
                    <td align="left" valign="bottom">
                        <asp:Label ID="lblPatientSignerRelationships" runat="server"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td align="right">Date:
                    </td>
                <td align="left">
                    <asp:Label ID="lblPatientRefusalDate" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</div>
