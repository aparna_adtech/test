﻿using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary.DAL;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace com.breg.vision.EmailTemplates
{
    public partial class CustomBrace : UserControl
    {
        #region delegates

        /// <summary>
        /// Delegate signature for method that accepts left and right leg values and outputs
        /// 2 values representing database storage column values with business logic applied.
        /// </summary>
        /// <param name="l">Left leg value.</param>
        /// <param name="r">Right leg value.</param>
        /// <param name="v1">Value to place in first database storage column.</param>
        /// <param name="v2">Value to place in second database storage column.</param>
        private delegate void GetBilatSaveValuesDelegate(string l, string r, out string v1, out string v2);

        /// <summary>
        /// Delegate signature for method that accepts column name and extracts left-leg
        /// and right-leg values with business logic applied.
        /// </summary>
        /// <param name="c">Column name.</param>
        /// <param name="l">Left-leg value.</param>
        /// <param name="r">Right-leg value.</param>
        private delegate void GetBilatLoadValuesDelegate(string c, out string l, out string r);

        /// <summary>
        /// Column suffix used in database for all columns related to right-leg value storage
        /// in bilat orders.
        /// </summary>
        private const string BilatDbColumnSuffix = "_BilatRightLeg";

        #endregion

        #region properties

        private bool _IsReadOnly = false;
        public bool IsReadOnly
        {
            get { return _IsReadOnly; }
            set { _IsReadOnly = value; }
        }

        private int? _BregVisionOrderID { get; set; }
        public int? BregVisionOrderID
        {
            get { return _BregVisionOrderID; }
            set { _BregVisionOrderID = value; }
        }

        public int PracticeID { get; set; }

        public Int32 PracticeLocationID { get; set; }

        public string PatientSignatureUrl { get; set; }

        public string PhysicianName { get; set; }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            //    Page.ClientScript.RegisterStartupScript(
            //        this.GetType(),
            //        "PreventBackButton",
            //        "<script language=JavaScript>try {window.history.forward(1);} catch (e) {}</script>");

            //    if (_IsReadOnly)
            //        btnNext.Visible = false;
            //}
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {

        }

        public void LoadControlInfo()
        {
            //Int16 practiceLocationID = Convert.ToInt16(Session["PracticeLocationID"]);

            PopulateBillingAddress(PracticeLocationID);
            PopulateShippingAddress(PracticeLocationID);
            PopulateContactTelephoneNumber(PracticeLocationID);
            GetBregAccountCode(PracticeLocationID);
            PopulatePracticeLocationPhysicians(PracticeLocationID);

            if (BregVisionOrderID.HasValue)
                GetProductInfoByOrder(_BregVisionOrderID.Value, PracticeLocationID);
            else
                GetProductInfo(PracticeLocationID);

//            if (_IsReadOnly)
//            {
//                ScriptManager.RegisterStartupScript(
//                    this,
//                    this.GetType(),
//                    "ReadOnly",
//                    @"
//$(function () {{
//    $('body').bind('keydown', function(e) {{ return false; }});
//    $('#customBraceReadOnlyDiv')[0].style.display = ""block"";
//    $('#copyFromRightLegAnchor')[0].style.display = ""none"";
//    $('#clearLeftLegMeasurementsAnchor')[0].style.display = ""none"";
//    $('#copyFromLeftLegAnchor')[0].style.display = ""none"";
//    $('#clearRightLegMeasurementsAnchor')[0].style.display = ""none"";
//}});
//",
//                    true);
//            }
        }

        private enum GetCustomBraceInfoIDType
        {
            BregVisionOrderID,
            ShoppingCartID
        }

        private void GetCustomBraceInfo(int practiceLocationID, int brace_id, GetCustomBraceInfoIDType id_type)
        {
            DataSet dsCustomBraceInfo = null;
            if (id_type == GetCustomBraceInfoIDType.BregVisionOrderID)
            {
                dsCustomBraceInfo =
                    DAL.PracticeLocation.Inventory.GetCustomBraceExtendedInfoFromOrder(null, brace_id);
            }
            else
            {
                dsCustomBraceInfo =
                    DAL.PracticeLocation.Inventory.GetCustomBraceExtendedInfoFromOrder(brace_id);
            }

            if (dsCustomBraceInfo.Tables[0].Rows.Count == 1)
            {
                DataRow drCustomBraceInfo = dsCustomBraceInfo.Tables[0].Rows[0];
                bool isBilat = drCustomBraceInfo["BilatCastReform"].ToString().ToLower().Contains("bilat");
                bool isBraceForLeft = drCustomBraceInfo["BraceFor"].ToString().ToLower().Contains("left");

                var getV =
                    (GetBilatLoadValuesDelegate)
                    delegate(string c, out string l, out string r)
                    {
                        var v1 = drCustomBraceInfo[c].ToString();
                        var v2 = drCustomBraceInfo[c + BilatDbColumnSuffix].ToString();
                        l = (!isBilat && !isBraceForLeft) ? v2 : v1;
                        r = (isBilat) ? v2 : ((!isBraceForLeft) ? v1 : "");
                    };

                CustomerNumber.Text = drCustomBraceInfo["Billing_CustomerNumber"].ToString();
                txtBillPhone.Text = drCustomBraceInfo["Billing_Phone"].ToString();
                BillAttention.Text = drCustomBraceInfo["Billing_Attention"].ToString();
                BillAddress1.Text = drCustomBraceInfo["Billing_Address1"].ToString();
                BillAddress2.Text = drCustomBraceInfo["Billing_Address2"].ToString();
                BillCity.Text = drCustomBraceInfo["Billing_City"].ToString();
                BillState.Text = drCustomBraceInfo["Billing_State"].ToString();
                BillZipCode.Text = drCustomBraceInfo["Billing_Zip"].ToString();

                ShipAttention.Text = drCustomBraceInfo["Shipping_Attention"].ToString();
                ShipAddress1.Text = drCustomBraceInfo["Shipping_Address1"].ToString();
                ShipAddress2.Text = drCustomBraceInfo["Shipping_Address2"].ToString();
                ShipCity.Text = drCustomBraceInfo["Shipping_City"].ToString();
                ShipState.Text = drCustomBraceInfo["Shipping_State"].ToString();
                ShipZipCode.Text = drCustomBraceInfo["Shipping_Zip"].ToString();

                txtPatientName.Text = drCustomBraceInfo["PatientName"].ToString();
                txtAge.Text = drCustomBraceInfo["Patient_Age"].ToString();
                txtWeight.Text = drCustomBraceInfo["Patient_Weight"].ToString();
                txtHeight.Text = drCustomBraceInfo["Patient_Height"].ToString();
                SetSelectedValue(ddlPhysician.Items, drCustomBraceInfo["PhysicianID"].ToString());
                SetSelectedValue(rblSex.Items, drCustomBraceInfo["Patient_Sex"].ToString());

                SetSelectedValue(ref rblBraceFor, drCustomBraceInfo["BraceFor"].ToString());
                SetSelectedValue(ref rblLegMeasurements, drCustomBraceInfo["BilatCastReform"].ToString());
                txtMeasurementsTakenBy.Text = drCustomBraceInfo["MeasurementsTakenBy"].ToString();

                {
                    string l, r;

                    getV("Instability", out l, out r);
                    SetSelectedValue(ref rblInstability, l);
                    SetSelectedValue(ref rblInstabilityR, r);

                    getV("ThighCircumference", out l, out r);
                    txtThigh.Text = l;
                    txtThighR.Text = r;

                    getV("CalfCircumference", out l, out r);
                    txtCalf.Text = l;
                    txtCalfR.Text = r;

                    getV("KneeOffset", out l, out r);
                    txtKneeOffset.Text = l;
                    txtKneeOffsetR.Text = r;

                    getV("KneeWidth", out l, out r);
                    txtKneeWidth.Text = l;
                    txtKneeWidthR.Text = r;

                    getV("Extension", out l, out r);
                    SetSelectedValue(ref rblExtension, l);
                    SetSelectedValue(ref rblExtensionR, r);

                    getV("Flexion", out l, out r);
                    SetSelectedValue(ref rblFlexion, l);
                    SetSelectedValue(ref rblFlexionR, r);
                }

                SetSelectedValue(chkX2KColor.Items, drCustomBraceInfo["X2K_FramePadColor1"].ToString());
                SetSelectedValue(chkX2KColor.Items, drCustomBraceInfo["X2K_FramePadColor2"].ToString());
                {
                    string l, r;

                    getV("X2K_Counterforce", out l, out r);
                    SetSelectedValue(ref rblCounterForce, l);
                    SetSelectedValue(ref rblCounterForceR, r);

                    getV("X2K_Counterforce_degrees", out l, out r);
                    txtCounterForceDegrees.Text = l;
                    txtCounterForceDegreesR.Text = r;
                }

                txtNotes.Text = drCustomBraceInfo["Fusion_Notes"].ToString();
                SetSelectedValueByText(rblColorPatternEnhancement.Items, drCustomBraceInfo["Fusion_Enhancement"].ToString());
                SetSelectedValue(ref rblColor, drCustomBraceInfo["Fusion_Color"].ToString());
                txtPantone.Text = drCustomBraceInfo["Fusion_Pantone"].ToString();
                SetSelectedValue(ref rblPattern, drCustomBraceInfo["Fusion_Pattern"].ToString());
                txtColorPattern.Text = drCustomBraceInfo["Fusion_Pattern_Notes"].ToString();

                SetSelectedValue(ref rblX2KShippingType, drCustomBraceInfo["ShippingTypeID"].ToString());
                SetSelectedValue(ref rblFusionShippingType, drCustomBraceInfo["ShippingTypeID"].ToString());

                SetSelectedValue(ref rblX2KShippingCarrier, drCustomBraceInfo["ShippingCarrierID"].ToString());
                SetSelectedValue(ref rblFusionShippingCarrier, drCustomBraceInfo["ShippingCarrierID"].ToString());
            }
        }

        private void SetSelectedValue(ref RadioButtonList control, string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                control.ClearSelection();
                control.SelectedValue = (value);
                //foreach (ListItem li in control.Items)
                //{
                //    li.Selected = false;
                //    if (li.Value == value)
                //    {
                //        li.Selected = true;

                //        return;
                //    }
                //}
            }
        }

        private void SetSelectedValue(ListItemCollection Items, string value)
        {
            if (!string.IsNullOrEmpty(value))
            {

                foreach (ListItem li in Items)
                {
                    if (li.Value == value)
                    {
                        li.Selected = true;
                        return;
                    }
                }
            }
        }

        private void SetSelectedValueByText(ListItemCollection Items, string value)
        {
            if (!string.IsNullOrEmpty(value))
            {

                foreach (ListItem li in Items)
                {
                    if (li.Text == value)
                    {
                        li.Selected = true;
                        return;
                    }
                }
            }
        }

        private void SetProductVisibility(string productName, string code)
        {
            DataTable productAccessories = null;
            GridView grdProducts = null;

            if (code.StartsWith("2")) //(productName.ToLower().Contains("x2k"))
            {
                //change these to trX2KTitle and trX2KBody when the aspx for them is done
                trX2KAll.Visible = true;
                //trX2KAll.Style.Clear();
                //trX2KAll.Style.Add("display", "inline");

                trX2KBody.Visible = true;
                //trX2KBody.Style.Clear();
                //trX2KBody.Style.Add("display", "inline");

                trX2KUnloading.Visible = true;
                trX2KUnloadingR.Visible = true;
                //trX2KUnloading.Style.Clear();
                //trX2KUnloading.Style.Add("display", "inline");

                productAccessories = GetX2KAccessoryData();
                grdProducts = grdX2KAccessories;
            }
            else if (productName.ToLower().Contains("fusion") || productName.ToLower().Contains("solus"))
            {
                //Fusion & Solus Products
                trFusionTitle.Visible = true;
                //trFusionTitle.Style.Clear();
                //trFusionTitle.Style.Add("display", "inline");

                trFusionBody.Visible = true;
                trFusionBodyAccessories.Visible = true;
                //trFusionBody.Style.Clear();
                //trFusionBody.Style.Add("display", "inline");

                trFusionUnloading.Visible = true;
                trFusionUnloadingR.Visible = true;
                //trFusionUnloading.Style.Clear();
                //trFusionUnloading.Style.Add("display", "inline");

                productAccessories = GetFusionAccessoryData();
                grdProducts = grdFusionAccessories;
            }
            else if (productName.ToLower().Contains("lpr"))
            {
                trFusionTitle.Visible = true;
                trFusionBody.Visible = true;
                trFusionBodyAccessories.Visible = true;
                trFusionUnloading.Visible = true;
                trFusionUnloadingR.Visible = true;

                productAccessories = GetLPRAccessoryData();
                grdProducts = grdFusionAccessories;
            }

            PopulateAccessories(productAccessories, grdProducts);

            SetProduct(productName, code);

        }

        private void SetProduct(string ProductName, String Code)
        {
            ListItem li = new ListItem(Code, Code);
            li.Selected = true;

            if (Code.StartsWith("2"))    //(productName.ToLower().Contains("x2k"))
            {
                lblX2K.Text = ProductName;
                rblX2KCode.Items.Add(li);
            }
            else
            {
                lblFusion.Text = ProductName;
                rblFusionCode.Items.Add(li);
            }
        }

        private void PopulateAccessories(DataTable productAccessories, GridView grdProduct)
        {
            //DataTable fusionAccessories = GetFusionAccessoryData();
            DataTable dtProductsInCart = DAL.PracticeLocation.Inventory.GetCustomBraceFromShoppingCart(this.PracticeLocationID, false);

            if (dtProductsInCart == null)
            {
                dtProductsInCart = new DataTable();
            }
            var productsInCart = dtProductsInCart.AsEnumerable();
            var productAccessory = productAccessories.AsEnumerable();

            var query = from pa in productAccessory
                        join pc in productsInCart
                            on pa.Field<Int32>("MasterCatalogProductID") equals pc.Field<Int32>("MasterCatalogProductID") into papc
                        from p in papc.DefaultIfEmpty()
                        select new
                        {
                            MasterCatalogProductID = pa.Field<Int32>("MasterCatalogProductID"),
                            DisplayName = string.Format("{0}, {1} ({2})", pa.Field<string>("ShortName"), pa.Field<string>("Size"), pa.Field<string>("Code")),
                            Quantity = p == null ? 0 : p.Field<Int32?>("Quantity")
                        };

            if (query != null)
            {
                grdProduct.DataSource = query;
                grdProduct.DataBind();
            }

        }

        private DataTable GetX2KAccessoryData()
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetX2KAccessories");

            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds.Tables[0];

        }

        private DataTable GetFusionAccessoryData()
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetFusionAccessories");

            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds.Tables[0];

        }

        private DataTable GetLPRAccessoryData()
        {
            DataTable dt = GetFusionAccessoryData();

            DataRow dr20000 = null;
            DataRow dr20001 = null;

            foreach (DataRow dr in dt.Rows)
            {
                if (dr["Code"].ToString().Contains("22000"))
                {
                    dr20000 = dr;
                }
                else if (dr["Code"].ToString().Contains("22001"))
                {
                    dr20001 = dr;
                }
            }

            dt.Rows.Remove(dr20000);
            dt.Rows.Remove(dr20001);
            return dt;
        }

        private void GetProductInfo(int practiceLocationID)
        {
            //if there is only one data row returned
            var dtProduct =
                DAL.PracticeLocation.Inventory.GetCustomBraceFromShoppingCart(
                    practiceLocationID,
                    true);
            if (dtProduct.Rows.Count == 1)
            {
                SetProductVisibility(
                    dtProduct.Rows[0]["Product"].ToString(),
                    dtProduct.Rows[0]["code"].ToString());
                GetCustomBraceInfo(
                    practiceLocationID,
                    Convert.ToInt32(dtProduct.Rows[0]["ShoppingCartID"]),
                    GetCustomBraceInfoIDType.ShoppingCartID);
            }
        }

        private void GetProductInfoByOrder(int order_id, int practiceLocationID)
        {
            // get product name and code from BregVisionOrderID
            using (var db = VisionDataContext.GetVisionDataContext())
            {
                var r =
                    db.SupplierOrders
                    .Where(x => x.BregVisionOrderID == order_id)
                    .SelectMany(
                        x =>
                            x.SupplierOrderLineItems
                            .Where(xx => !xx.PracticeCatalogProduct.IsThirdPartyProduct)
                            .Select(
                                xx =>
                                    new
                                    {
                                        xx.PracticeCatalogProduct.MasterCatalogProduct.ShortName,
                                        xx.PracticeCatalogProduct.MasterCatalogProduct.Code
                                    }
                            )
                    )
                    .FirstOrDefault();

                // if found, then set visibility of options based on product
                if (r != null)
                    SetProductVisibility(r.ShortName, r.Code);
            }

            // get brace information
            GetCustomBraceInfo(
                practiceLocationID,
                Convert.ToInt32(order_id),
                GetCustomBraceInfoIDType.BregVisionOrderID);
        }

        //private DataRow[] GetProductData(int practiceLocationID, bool GetBrace)
        //{
        //    int isCustomBrace = GetBrace == true ? 1 : 0;

        //    DataSet dsProduct = DAL.PracticeLocation.Inventory.GetShoppingCart(practiceLocationID);
        //    DataTable dtProduct = dsProduct.Tables[0];

        //    DataRow[] drProductRows = dtProduct.Select(string.Format("IsCustomBrace={0}", isCustomBrace));

        //    return drProductRows;

        //}

        //public DataSet GetShoppingCart(int practiceLocationID)
        //{

        //    Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
        //    DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetShoppingCart");

        //    db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
        //    DataSet ds = db.ExecuteDataSet(dbCommand);
        //    return ds;
        //}

        //private void SetOnClickAttributes()
        //{
        //    chkStdHinge.Attributes.Add("onclick", "ResetInstability(this)");
        //    //chkStdHingeColor.Attributes.Add("onclick", "ResetInstability(this)");
        //    chkOA.Attributes.Add("onclick", "ResetInstability(this)");
        //    chkOAEnhancement.Attributes.Add("onclick", "ResetInstability(this)");
        //    chkEnhancement.Attributes.Add("onclick", "ResetInstability(this)");
        //}

        private void PopulatePracticeLocationPhysicians(int practiceLocationID)
        {
            BLL.PracticeLocation.Physician bllPLP = new BLL.PracticeLocation.Physician();
            DataTable dtPracticeLocationPhysician = bllPLP.SelectAllNames(practiceLocationID);
            ddlPhysician.DataSource = dtPracticeLocationPhysician;
            ddlPhysician.DataTextField = "ClinicianName";
            ddlPhysician.DataValueField = "ClinicianID";
            ddlPhysician.DataBind();
        }

        private void PopulateBillingAddress(int practiceLocationID)
        {

            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.USP_GetBillingAddressByPracticeLocation");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.String, practiceLocationID);

            IDataReader reader = db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                BillAttention.Text = reader["BillAttentionOf"].ToString();
                BillAddress1.Text = reader["BillAddress1"].ToString();
                BillAddress2.Text = reader["BillAddress2"].ToString();
                BillCity.Text = reader["BillCity"].ToString();
                BillState.Text = reader["BillState"].ToString();
                BillZipCode.Text = reader["BillZipCode"].ToString();
            }


        }

        private void GetBregAccountCode(int practiceLocationID)
        {

            DAL.PracticeLocation.SupplierCode dalPLSupplierCode = new DAL.PracticeLocation.SupplierCode();
            DataTable dt = dalPLSupplierCode.SelectAll(practiceLocationID);
            DataRow[] rows = dt.Select("SupplierShortName='Breg'");
            if (rows.GetUpperBound(0) > -1)
            {
                DataRow bregRow = rows[0];
                string bregCode = bregRow["AccountCode"].ToString();
                CustomerNumber.Text = bregCode;
            }

        }

        private void PopulateContactTelephoneNumber(int PracticeLocationID)
        {
            BLL.PracticeLocation.Contact bllPLContact = new BLL.PracticeLocation.Contact();
            //bllPLContact.PracticeLocationID = practiceLocationID;
            bllPLContact = bllPLContact.Select(PracticeLocationID);

            if (bllPLContact.ContactID > 0)   //Always true;
            {
                txtBillPhone.Text = bllPLContact.PhoneWork;
            }
        }

        private void PopulateShippingAddress(int practiceLocationID)
        {


            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.USP_GetShippingAddressByPracticeLocation");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.String, practiceLocationID);

            IDataReader reader = db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                ShipAttention.Text = reader["ShipAttentionOf"].ToString();
                ShipAddress1.Text = reader["ShipAddress1"].ToString();
                ShipAddress2.Text = reader["ShipAddress2"].ToString();
                ShipCity.Text = reader["ShipCity"].ToString();
                ShipState.Text = reader["ShipState"].ToString();
                ShipZipCode.Text = reader["ShipZipCode"].ToString();
            }

        }

        //protected void btnNext_Click(object sender, EventArgs e)
        //{

        //    DataTable dtProduct = DAL.PracticeLocation.Inventory.GetCustomBraceFromShoppingCart(this.PracticeLocationID, true);
        //    string code = dtProduct.Rows[0]["code"].ToString();

        //    SaveCustomBraceInfo(dtProduct.Rows[0], code);
        //    SaveAccessories(dtProduct.Rows[0], code);

        //    // SaveCustomBraceInfo sets the session variable for ShippingTypeID based on the
        //    // data entered in the form in either the fusion or x2k; so, we just pick it up
        //    // here and update/override the shopping cart shipping type so that this selection
        //    // overrides the value from before.
        //    {
        //        var practiceLocationID = Convert.ToInt32(Session["PracticeLocationID"]);
        //        var shippingType = Convert.ToInt32(Session["ShippingTypeID"].ToString());
        //        DAL.PracticeLocation.ShoppingCart.UpdateShippingType(practiceLocationID, shippingType);
        //    }

        //    Response.Redirect("InvoiceSnapshot.aspx");
        //}

        protected void SaveAccessories(DataRow drProduct, string code)
        {
            // do not save anything if we are in read-only mode
            if (_IsReadOnly)
                return;

            int practiceID = this.PracticeID;
            Int16 practiceLocationID = Convert.ToInt16(this.PracticeLocationID);
            int productInventoryID = 0; // as long as we send this as 0 to the 'UpdateInventoryData' it will add it to the catalog and inventory if not there
            int practiceCatalogProductID = 0; // as long as we send this as 0 to the 'UpdateInventoryData' it will add it to the catalog and inventory if not there
            Int16 userID = 1;
            Int16 quantity = 0;
            GridView grdProducts = null;
            DataTable dsAccessories = null;
            DataTable dtGridAccessoryData = new DataTable();

            DataTable dtProductsInCart = DAL.PracticeLocation.Inventory.GetCustomBraceFromShoppingCart(this.PracticeLocationID, false);
            if (dtProductsInCart == null)
            {
                dtProductsInCart = new DataTable();
            }
            var productsInCart = dtProductsInCart.AsEnumerable();

            if (code.StartsWith("2")) //(productName.ToLower().Contains("x2k"))
            {
                grdProducts = grdX2KAccessories;
                dsAccessories = GetX2KAccessoryData();
            }
            else
            {
                grdProducts = grdFusionAccessories;
                dsAccessories = GetFusionAccessoryData();
            }


            foreach (GridViewRow accessoryRow in grdProducts.Rows)
            {
                //get master catalog product ID
                string strMasterCatalogProductID = ((HiddenField)accessoryRow.Cells[2].FindControl("MasterCatalogProductID")).Value;
                int masterCatalogProductID = 0;
                int.TryParse(strMasterCatalogProductID, out masterCatalogProductID);

                //Get the Quantity
                string strQuantity = ((TextBox)accessoryRow.Cells[0].FindControl("txtQuantity")).Text;
                Int16.TryParse(strQuantity, out quantity);

                if (masterCatalogProductID > 0)
                {  //the current mcpID and quantity come from the grid.
                    //the mcpID and quantity from the cart are in 'productsInCart'
                    //Note:  I should make a new function in DAL.PracticeLocation.Inventory called AdjustInventory(MCP_ID, OldQuantity, NewQuantity)
                    var query = from pic in productsInCart
                                where pic.Field<Int32>("MasterCatalogProductID") == masterCatalogProductID
                                //&& pic.Field<Int32>("Quantity") != quantity
                                select new
                                {
                                    NewQuantity = Convert.ToInt16(quantity),
                                    NewQuantityMinusOldQuantity = Convert.ToInt16(quantity - pic.Field<Int32>("Quantity")),
                                    ShoppingCartItemID = pic.Field<Int32>("ShoppingCartItemID")
                                };

                    Int16 quantityToAddToCart = 0;
                    if (query.Count() > 0)  // if the item is already in the cart
                    {

                        if (query.First().NewQuantityMinusOldQuantity < 0)
                        {
                            //if the new quantity is less than the  old quantity in the cart
                            // then delete what's in the cart and re-Add it
                            DAL.PracticeLocation.Inventory.DeleteItemFromShoppingCart(query.First().ShoppingCartItemID);
                            quantityToAddToCart = query.First().NewQuantity;
                        }
                        else if (query.First().NewQuantityMinusOldQuantity > 0)
                        {
                            //if the new quantity is greater than the old quantity in the cart, then add the difference
                            quantityToAddToCart = query.First().NewQuantityMinusOldQuantity;
                        }
                        //if the new quantity is equal to the old quantity in the cart then do nothing

                    }
                    else if (quantity > 0) // this item wasn't iin the cart yet so just add it
                    {
                        quantityToAddToCart = quantity;
                    }

                    if (quantityToAddToCart > 0)
                    {
                        DAL.PracticeLocation.Inventory.UpdateInventoryData(practiceID, practiceLocationID,
                                                                                    masterCatalogProductID, productInventoryID, practiceCatalogProductID,
                                                                                    userID, quantityToAddToCart);
                    }

                }

            }

            //foreach (ListItem liAccessory in cblProducts.Items)
            //{
            //    if (liAccessory.Selected == true)
            //    {
            //        int masterCatalogProductID = Convert.ToInt32(liAccessory.Value);

            //        DataRow[] drFoundInCart = null;
            //        if (dtProductsInCart != null)
            //        {
            //            drFoundInCart = dtProductsInCart.Select(string.Format("MasterCatalogProductID={0}", masterCatalogProductID));
            //        }

            //        if (drFoundInCart == null || drFoundInCart.GetUpperBound(0) < 0) // if it isn't already in the cart then add it.  There should only be one of each item
            //        {

            //            DAL.PracticeLocation.Inventory.UpdateInventoryData(practiceID, practiceLocationID,
            //                                                                                        masterCatalogProductID, productInventoryID, practiceCatalogProductID,
            //                                                                                        userID, quantity);
            //        }
            //    }
            //}

        }

        protected void SaveCustomBraceInfo(DataRow drProduct, string code)
        {
            // NOTE: make sure we don't save anything if we're in read-only mode
            if (!_IsReadOnly && (drProduct != null))
            {
                int shoppingCartID = Convert.ToInt32(drProduct["ShoppingCartID"]);
                bool isBilat = rblLegMeasurements.SelectedValue.ToLower().Contains("bilat");
                bool isBraceForLeft = rblBraceFor.SelectedValue.ToLower().Contains("left");

                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_CreateCustomBraceOrderNew");

                db.AddInParameter(dbCommand, "ShoppingCartID", DbType.Int32, shoppingCartID);

                db.AddInParameter(dbCommand, "Billing_CustomerNumber", DbType.String, CustomerNumber.Text);
                db.AddInParameter(dbCommand, "Billing_Phone", DbType.String, txtBillPhone.Text);
                db.AddInParameter(dbCommand, "Billing_Attention", DbType.String, BillAttention.Text);
                db.AddInParameter(dbCommand, "Billing_Address1", DbType.String, BillAddress1.Text);
                db.AddInParameter(dbCommand, "Billing_Address2", DbType.String, BillAddress2.Text);
                db.AddInParameter(dbCommand, "Billing_City", DbType.String, BillCity.Text);
                db.AddInParameter(dbCommand, "Billing_State", DbType.String, BillState.Text);
                db.AddInParameter(dbCommand, "Billing_Zip", DbType.String, BillZipCode.Text);

                db.AddInParameter(dbCommand, "Shipping_Attention", DbType.String, ShipAttention.Text);
                db.AddInParameter(dbCommand, "Shipping_Address1", DbType.String, ShipAddress1.Text);
                db.AddInParameter(dbCommand, "Shipping_Address2", DbType.String, ShipAddress2.Text);
                db.AddInParameter(dbCommand, "Shipping_City", DbType.String, ShipCity.Text);
                db.AddInParameter(dbCommand, "Shipping_State", DbType.String, ShipState.Text);
                db.AddInParameter(dbCommand, "Shipping_Zip", DbType.String, ShipZipCode.Text);

                db.AddInParameter(dbCommand, "PatientName", DbType.String, txtPatientName.Text);
                db.AddInParameter(dbCommand, "PhysicianID", DbType.Int32, ddlPhysician.SelectedValue);
                db.AddInParameter(dbCommand, "Patient_Age", DbType.Int32, Convert.ToInt32(txtAge.Text));
                db.AddInParameter(dbCommand, "Patient_Weight", DbType.Int32, Convert.ToInt32(txtWeight.Text));
                db.AddInParameter(dbCommand, "Patient_Height", DbType.String, txtHeight.Text);
                db.AddInParameter(dbCommand, "Patient_Sex", DbType.String, rblSex.SelectedValue);

                db.AddInParameter(dbCommand, "BraceFor", DbType.String, rblBraceFor.SelectedValue);
                db.AddInParameter(dbCommand, "MeasurementsTakenBy", DbType.String, txtMeasurementsTakenBy.Text);
                db.AddInParameter(dbCommand, "BilatCastReform", DbType.String, rblLegMeasurements.SelectedValue);
                if (isBilat)
                    DAL.PracticeLocation.Inventory.AddBilatProductToCart(PracticeLocationID);
                else
                    DAL.PracticeLocation.Inventory.CorrectNonBilatProductInCart(PracticeLocationID);

                // this delegate provides the logic for storing bilat right-leg info
                // in additional columns while maintaining backwards compatibility by 
                // storing single-leg info in the original columns
                var getV =
                    (GetBilatSaveValuesDelegate)
                    delegate(string l, string r, out string v1, out string v2)
                    {
                        v1 = (isBraceForLeft || isBilat) ? l : r;
                        v2 = isBilat ? r : "";
                    };

                // this delegate applies the left/right/bilat storage logic and
                // adds the necessary database parameters for proper storage
                var addP =
                    (Action<string, string, string>)
                    delegate(string p, string l, string r)
                    {
                        string v1;
                        string v2;
                        getV(l, r, out v1, out v2);
                        var t = DbType.String;
                        db.AddInParameter(dbCommand, p, t, v1);
                        db.AddInParameter(dbCommand, p + BilatDbColumnSuffix, t, v2);
                    };

                addP("Instability", rblInstability.SelectedValue, rblInstabilityR.SelectedValue);
                addP("ThighCircumference", txtThigh.Text, txtThighR.Text);
                addP("CalfCircumference", txtCalf.Text, txtCalfR.Text);
                addP("KneeOffset", txtKneeOffset.Text, txtKneeOffsetR.Text);
                addP("KneeWidth", txtKneeWidth.Text, txtKneeWidthR.Text);
                addP("Extension", rblExtension.SelectedValue, rblExtensionR.SelectedValue);
                addP("Flexion", rblFlexion.SelectedValue, rblFlexionR.SelectedValue);

                string x2kFramePadColor1 = "";
                string x2kFramePadColor2 = "";
                string x2k_Counterforce = "", x2k_Counterforce_BilatRightLeg = "";
                string x2k_Counterforce_Degrees = "", x2k_Counterforce_Degrees_BilatRightLeg = "";

                string fusion_Notes = "";
                string fusion_Color = "";
                string fusion_Pattern = "";
                string fusion_Pattern_Notes = "";
                string fusion_Pantone = "";

                string shippingTypeID = "";
                string shippingCarrierID = "";

                if (code.StartsWith("2")) //(productName.ToLower().Contains("x2k"))
                {


                    x2kFramePadColor1 = chkX2KColor.SelectedValue;
                    foreach (ListItem fpc in chkX2KColor.Items)
                    {
                        if (fpc.Selected == true && fpc != chkX2KColor.SelectedItem)
                        {
                            x2kFramePadColor2 = fpc.Value;
                        }
                    }
                    getV(rblCounterForce.SelectedValue, rblCounterForceR.SelectedValue, out x2k_Counterforce, out x2k_Counterforce_BilatRightLeg);
                    getV(txtCounterForceDegrees.Text, txtCounterForceDegreesR.Text, out x2k_Counterforce_Degrees, out x2k_Counterforce_Degrees_BilatRightLeg);
                    shippingTypeID = rblX2KShippingType.SelectedValue;
                    shippingCarrierID = rblX2KShippingCarrier.SelectedValue;

                    //Set Session Variable for Delivery method
                    //Session["ShippingTypeID"] = rblX2KShippingType.SelectedValue.ToString();
                }
                else
                {
                    fusion_Notes = txtNotes.Text;
                    fusion_Color = rblColor.SelectedValue;
                    fusion_Pantone = txtPantone.Text;

                    if (rblColorPatternEnhancement.SelectedIndex > -1)
                    {
                        fusion_Pattern = rblColorPatternEnhancement.SelectedItem.Text;
                    }
                    fusion_Pattern_Notes = txtColorPattern.Text;
                    shippingTypeID = rblFusionShippingType.SelectedValue;
                    shippingCarrierID = rblFusionShippingCarrier.SelectedValue;

                    //Set Session Variable for Delivery method
                    //Session["ShippingTypeID"] = rblFusionShippingType.SelectedValue.ToString();
                }

                db.AddInParameter(dbCommand, "X2K_FramePadColor1", DbType.String, x2kFramePadColor1);
                db.AddInParameter(dbCommand, "X2K_FramePadColor2", DbType.String, x2kFramePadColor2);
                db.AddInParameter(dbCommand, "X2K_Counterforce", DbType.String, x2k_Counterforce);
                db.AddInParameter(dbCommand, "X2K_Counterforce" + BilatDbColumnSuffix, DbType.String, x2k_Counterforce_Degrees_BilatRightLeg);
                db.AddInParameter(dbCommand, "X2K_Counterforce_degrees", DbType.String, x2k_Counterforce_Degrees);
                db.AddInParameter(dbCommand, "X2K_Counterforce_degrees" + BilatDbColumnSuffix, DbType.String, x2k_Counterforce_Degrees_BilatRightLeg);

                db.AddInParameter(dbCommand, "Fusion_Notes", DbType.String, fusion_Notes);
                db.AddInParameter(dbCommand, "Fusion_Enhancement", DbType.String, fusion_Pattern);
                db.AddInParameter(dbCommand, "Fusion_Color", DbType.String, fusion_Color);
                db.AddInParameter(dbCommand, "Fusion_Pantone", DbType.String, fusion_Pantone);
                db.AddInParameter(dbCommand, "Fusion_Pattern", DbType.String, rblPattern.SelectedValue);
                db.AddInParameter(dbCommand, "Fusion_Pattern_Notes", DbType.String, fusion_Pattern_Notes);

                db.AddInParameter(dbCommand, "ShippingTypeID", DbType.Int32, Convert.ToInt32(shippingTypeID));
                db.AddInParameter(dbCommand, "ShippingCarrierID", DbType.Int32, Convert.ToInt32(shippingCarrierID));

                db.AddParameter(dbCommand, "@return_value", DbType.Int32, ParameterDirection.ReturnValue, null, DataRowVersion.Default, null);

                db.ExecuteNonQuery(dbCommand);

                var return_value = Convert.ToInt32(db.GetParameterValue(dbCommand, "@return_value"));
                if (return_value != 0)
                    throw new ApplicationException("Custom brace insert failed. Return value was: " + return_value.ToString() + ".");
            }
        }
    }
}