﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Notification.ascx.cs"
    Inherits="com.breg.vision.EmailTemplates.Notification" %>
<div style="page-break-before: always; margin: 0;">
    <h3>
        Medicare Inexpensive or Routinely Purchased Items Notification
    </h3>
    <p>
        I received instructions and understand that Medicare defines the
        <asp:Label ID="lblProductName" runat="server" Text="" Font-Underline="true"></asp:Label>
        that I received as being an inexpensive or routinely purchased item.</p>
    <p>
        &nbsp;FOR INEXPENSIVE OR ROUTINELY PURCHASED ITEMS:</p>
    <ul style="line-height: 20px">
        <li>Equipment in this category can be purchased or rented; however, the total amount
            paid for monthly rentals cannot exceed the fee schedule purchase amount.</li>
        <li>Examples of this type of equipment include:<br />
            Canes, walkers, crutches, commode chairs, low pressure and positioning equalization
            pads, home blood glucose monitors, seat lift mechanisms, pneumatic compressors (lymphedema
            pumps), bed side rails, and traction equipment.</li>
        <li>I select the:
            <table style="border: 1px solid gray;">
                <tr>
                    <td style="line-height: 50px">
                        Purchase Option
                    </td>
                    <td style="text-decoration: underline">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; X&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td>
                        &nbsp;&nbsp;&nbsp;
                    </td>
                    <td>
                        Rental Option (Provide RX)
                    </td>
                </tr>
            </table>
        </li>
    </ul>
    <table id="tbl01" runat="server" cellpadding="0" cellspacing="5" width="680px" border="0">
        <tr>
            <td align="left" valign="bottom">
                <span>
                    <img height="87" width="216" src="<%=ImageUrl %>" alt="" /></span>
            </td>
            <td align="left" colspan="2" valign="bottom">
                <span>
                    <asp:Label ID="lblSignatureDate" runat="server" Visible="false"></asp:Label></span>
            </td>
        </tr>
        <tr>
            <td align="left" style="border-top: 1px solid black;">
                Beneficiary Signature
            </td>
            <td align="left" colspan="2" style="border-top: 1px solid black;">
                Date
            </td>
        </tr>
         <tr>          
            <td align="left" colspan="2" style="border-top: 1px solid black;">
            <span><asp:Label ID="lblPataientSignerRelationships" runat="server" Visible="false"></asp:Label></span>  
            </td>
        </tr>
    </table>
</div>
