﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DispenseReceipt.ascx.cs"
    Inherits="com.breg.vision.EmailTemplates.DispenseReceipt" %>
<style type="text/css">
    @media print
    {
         #divNonPrintable { display: none;}   
    }
</style>

<script language="javascript" type="text/javascript">
    function PrintContent() {
        window.print();
     }
</script>

<div style="font-size: 8pt; line-height: 1.0;">
    <div style="page-break-inside: avoid;">
        <table width="100%">
            <tr>
                <td align="left" style="width:50%">
                    <div style="font-weight: bold; font-size: 130%;">
                        <asp:Label ID="lblPracticeName2" runat="server"></asp:Label> PATIENT RECEIPT</div>
                </td>
                <td align="right">Date Dispensed (for Office Use Only): <asp:Label ID="lblDateDelivered" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td align="left" >
                    <asp:Label ID="lblPracticeName" runat="server"></asp:Label>
                </td>
                <td rowspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td align="left" >
                    <asp:Label ID="lblPracticeLocationName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left" >
                    <asp:Label ID="lblAddressLine1" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left" >
                    <asp:Label ID="lblCity" runat="server"></asp:Label>
                    <label>
                        ,&nbsp;</label>
                    <asp:Label ID="lblState" runat="server"></asp:Label>
                    <label>
                        &nbsp;&nbsp;</label>
                    <asp:Label ID="lblZipCode" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left" >
                    <label>Phone:&nbsp;</label>
                    <asp:Label ID="lblPhone" runat="server"></asp:Label>
                    <label style="padding-left: 10px">Fax:&nbsp;</label>
                    <asp:Label ID="lblFax" runat="server"></asp:Label>
                </td>
                <td align="left">
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2">
                    <asp:Label ID="lblSpecificContact" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div style="margin-top: 15px; page-break-inside: avoid;">
        <div style="font-weight: bold; font-size: 130%; position: relative;" runat="server" id="detailedWrittenOrderHeader">
            DETAILED WRITTEN ORDER
            <div style="position: absolute; bottom: 4px; right: 0; width: 33%; text-align: right;">Date of Order: <asp:Label ID="lblCreatedDate" runat="server"></asp:Label></div></div>
        <div style="border: 1px solid gray; padding: 5px;">
            <table bgcolor="white" width="100%" runat="server" id="detailedWrittenOrderBody">
                <tr>
                    <td align="right" width="10%">
                        <label>
                            Provider:&nbsp;</label>
                    </td>
                    <td align="left" width="15%">
                        <asp:Label ID="lblPhysicianName2" runat="server"></asp:Label>
                    </td>
                    <td align="right" width="10%">
                        <label>
                            Signature:&nbsp;</label>
                    </td>
                    <td align="left" width="30%">
                        <asp:Label ID="lblElectronicallySigned" runat="server" Text="Electronically signed by "></asp:Label>
                        <asp:Label ID="lblPhysicianName" runat="server"></asp:Label>
                    </td>
                    <td align="right" width="5%">
                        <label>
                            Date:&nbsp;</label>
                    </td>
                    <td align="left" width="15%">
                        <asp:Label ID="lblPhysicianSignedDate" runat="server"></asp:Label>
                    </td>
                     <td align="right" width="5%"></td>
                    <td align="left"  width="10%">
                        <asp:Label ID="lblDetailedWrittenOrderPatientDOB" runat="server"></asp:Label>
                    </td>
               </tr>
                <tr>
                    <td align="right">
                        <label style="white-space: nowrap">
                            Patient Name:&nbsp;</label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lblDetailedWrittenOrderPatientName" runat="server"></asp:Label>
                    </td>
                    <td align="right">
                        <label  style="white-space: nowrap">
                            Patient Code/MRN:&nbsp;</label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lblDetailedWrittenOrderPatientCode" runat="server"></asp:Label>
                    </td>
                    <td align="right">
                        <label>
                            Notes:&nbsp;</label>
                    </td>
                    <td align="left" colspan="2">
                        <asp:Label ID="lblDetailedWrittenOrderNotes" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            <asp:Repeater ID="rptrDispenseReceipt" runat="server">
                <HeaderTemplate>
                    <table cellspacing="0" cellpadding="3" border="1" width="98%">
                        <tr>
                            <td align="left">
                                <label>
                                    Supplier</label>
                            </td>
                            <td align="left">
                                <label>
                                    Product Name</label>
                            </td>
                            <td align="left">
                                <label>
                                    Part #</label>
                            </td>
                            <td align="left">
                                <label>
                                    Qty.</label>
                            </td>
                            <td align="left">
                                <label>
                                    HCPCs</label>
                            </td>
                            <td align="left">
                                <label>
                                    Side</label>
                            </td>
                            <%--<td align="left">
                                <label>
                                    Modifiers</label>
                            </td>--%>
                            <td align="left">
                                <label style="white-space: nowrap">
                                    Dx Code</label>
                            </td>
                            <td align="left">
                                <label style="white-space: nowrap">
                                    Unit DME Dep.</label>
                            </td>
                            <td align="left">
                                <label>
                                    Size</label>
                            </td>
                            <td align="left">
                                <label>
                                    Gender</label>
                            </td>
                            <% if (IsBillingChargeDisplayed)
                               { %>
                                <td align="left" style="white-space: nowrap">
                                    <label>
                                        Unit Bill. Chg.</label>
                                </td>
                            <% } %>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblSupplierName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SupplierName")%>'></asp:Label>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblShortName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ShortName")%>'></asp:Label>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblCode" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Code")%>'></asp:Label>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblQuantity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Quantity")%>'></asp:Label>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblHCPCsString" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "HCPCSString")%>'></asp:Label>
                        </td>                       
                        <td align="left">
                            <asp:Label ID="lblSide" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Side")%>'></asp:Label>
                        </td>
                        <%--<td align="left">
                            <asp:Label ID="lblModsString" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Modifiers")%>'></asp:Label>
                        </td>--%>
                        <td align="left">
                            <asp:Label ID="lblICD9Code" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ICD9Code")%>'></asp:Label>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblDMEDeposit" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "DMEDeposit","{0:c}")%>'></asp:Label>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblSize" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Size")%>'></asp:Label>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblGender" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Gender")%>'></asp:Label>
                        </td>
                        <% if (IsBillingChargeDisplayed){ %>
                            <td align="left">
                                <asp:Label ID="lblTotal" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Total","{0:c}")%>'></asp:Label>
                            </td>
                        <% } %>

                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <div id="divBregProduct" runat="server" visible="false" style="margin-top: 5px; margin-left: 5px;">
                For more detail on your Breg product, please go to: <a href="http://www.breg.com/medical-professionals/patient-education-tools">
                    http://www.breg.com/medical-professionals/patient-education-tools</a>
            </div>
        </div>
    </div>
    <div style="page-break-inside: avoid;">
    <div style="margin-top: 15px; ">
        <div style="font-weight: bold; font-size: 130%;">
            PATIENT CONSENT AND ASSIGNMENT OF BENEFITS</div>
        <div style="padding: 5px; border: 1px solid gray;">
            <table bgcolor="white" width="100%" >
                <tr>
                    <td align="right" width="6%">
                        <label style="white-space: nowrap">
                            Patient Name:
                        </label>
                    </td>
                    <td width="15%">
                        <asp:Label ID="lblConsentPatientName" runat="server"></asp:Label>
                    </td>
                    <td align="right" width="6%">
                        <label>
                            Provider:
                        </label>
                    </td>
                    <td width="30%">
                        <asp:Label ID="lblPhysicianName1" runat="server"></asp:Label>
                    </td>
                    <td align="right" width="10%"></td>
                    <td width="25%"></td>
                </tr>
                <tr>
                    <td align="right">
                        <label>
                            Dx Code:</label>
                    </td>
                    <td>
                        <asp:Label ID="lblICD9Code" runat="server"></asp:Label>
                    </td>
                    <td align="right">
                        <label>
                            Insurance:
                        </label>
                    </td>
                    <td colspan="2" align="left">
                        <asp:Label ID="lblPayer" runat="server">N/A</asp:Label>
                    </td>                  
                    <td>
                        <asp:Label ID="lblFitter" runat="server" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        &nbsp
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="font-weight: bold;">
                        <label>
                            Durable Medical Equipment and Orthotics Patient Consent</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <label>
                            - I understand that my provider has prescribed the
                                <asp:Label ID="lblProductNames" runat="server"></asp:Label>
                                 as part of my treatment plan
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <label>
                            - I understand that I have a choice in where I receive my prescribed supplies and services</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <label>
                            - I authorize <asp:Label ID="lblPracticeAuth" runat="server"></asp:Label> to furnish this service/product and to provide
                            my insurance provider with any information requested for payment</label>
                    </td>
                </tr>
				<asp:Literal ID="DisclaimerBilled" runat="server"></asp:Literal>
                <tr>
                    <td colspan="6">
                        <label>
                            - I instruct my insurance provider to pay <asp:Label ID="lblPracticeInsurance" runat="server"></asp:Label> directly for
                            these services/products</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <label>
                            - I understand that my insurance company may deny payment for this supply because
                            it is a non-covered item or deemed not medically necessary</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <label>
                            - I understand that I am fully responsible for any deductible or co-insurance cost
                            related to this service/supply</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <label>
                            - I understand any costs not covered by my insurance provider will be my financial
                            responsibility</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <label>
                            - I have received the prescribed item and have been fully instructed on the use
                            and care of the above products/services</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <label>
                            - I understand that all medical devices are not returnable unless there is a material
                            defect</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <label>
                            - I have read and understand that canes, crutches, and walkers are categorized as
                            inexpensive/routinely purchased items and agree to purchase this item (if provided)</label>
                    </td>
                </tr>
				<asp:Literal ID="DisclaimerReceivedReviewed" runat="server"></asp:Literal>
            </table>
        </div>
    </div>
    <div style="margin-top: 15px; ">
        <div style="font-weight: bold; font-size: 130%;">
            PROOF OF DELIVERY</div>
        <div style="padding: 5px; border: 1px solid gray;">
            <table bgcolor="white" width="100%">
                <tr>
                    <td align="right" valign="top" width="15%">
                        Patient Name:
                    </td>
                    <td valign="top">
                        <asp:Label ID="lblProofOfDeliveryPatientName" runat="server"></asp:Label>
                    </td>
                    <td align="right" valign="top">
                        Delivery Address:
                    </td>
                    <td align="left" valign="top">
                        <asp:Label ID="lblAddressLine1_1" runat="server"></asp:Label><br />
                        <asp:Label ID="lblCity1" runat="server"></asp:Label>
                        <label>
                            ,&nbsp;</label>
                        <asp:Label ID="lblState1" runat="server"></asp:Label>
                        <label>
                            &nbsp;&nbsp;</label>
                        <asp:Label ID="lblZip1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="bottom">
                        Product(s) Delivered:
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <asp:Repeater ID="rptrDispenseReceipt1" runat="server">
                            <HeaderTemplate>
                                <table cellspacing="0" cellpadding="3" border="1" width="100%">
                                    <tr>
                                        <td align="left">
                                            <label>
                                                Supplier</label>
                                        </td>
                                        <td align="left">
                                            <label>
                                                Product Name</label>
                                        </td>
                                        <td align="left">
                                            <label>
                                                Part #</label>
                                        </td>
                                        <td align="left">
                                            <label>
                                                Qty.</label>
                                        </td>
                                        <td align="left">
                                            <label>
                                                HCPCs</label>
                                        </td>
                                        <%--<td align="left">
                                            <label>
                                                Modifiers</label>
                                        </td>--%>
                                        <td align="left">
                                            <label>
                                                Side</label>
                                        </td>
                                        <td align="left">
                                            <label style="white-space: nowrap">
                                                Dx Code</label>
                                        </td>
                                        <td align="left">
                                            <label style="white-space: nowrap">
                                               Unit DME Dep.</label>
                                        </td>
                                        <td align="left">
                                            <label>
                                                Size</label>
                                        </td>
                                        <td align="left">
                                            <label>
                                                Gender</label>
                                        </td>
                                        <% if (IsBillingChargeDisplayed)
                                           { %>
                                            <td align="left" style="white-space: nowrap">
                                                <label>
                                                    Unit Bill. Chg.</label>
                                            </td>
                                        <% } %>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblSupplierName1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SupplierName")%>'></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblShortName1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ShortName")%>'></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblCode1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Code")%>'></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblQuantity1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Quantity")%>'></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblHCPCsString1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "HCPCSString")%>'></asp:Label>
                                    </td>
                                    <%--<td align="left">
                                       <asp:Label ID="lblModsString" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Modifiers")%>'></asp:Label>        
                                    </td>--%>
                                    <td align="left">
                                        <asp:Label ID="lblSide1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Side")%>'></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblICD9Code1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ICD9Code")%>'></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblDMEDeposit1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "DMEDeposit","{0:c}")%>'></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblSize1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Size")%>'></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblGender1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Gender")%>'></asp:Label>
                                    </td>
                                    <% if (IsBillingChargeDisplayed){ %>
                                        <td align="left">
                                            <asp:Label ID="lblTotal1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Total","{0:c}")%>'></asp:Label>
                                        </td>
                                    <% } %>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="bottom">
                        Patient/Guarantor Signature:
                    </td>
                    <td align="left" valign="bottom">
                        <asp:Literal ID="PatientSignatureLiteralControl" runat="server"></asp:Literal>
                    </td>                 
                    <td align="left" valign="bottom">
                        <asp:Label ID="lblPatientSignerRelationships" runat="server" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="middle">
                        Date of Delivery:
                    </td>
                    <td align="left" valign="middle">
                        <asp:Label ID="lblPatientSignatureDate" runat="server" Visible="false"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </div>
</div>
