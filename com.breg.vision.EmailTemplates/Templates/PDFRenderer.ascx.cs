﻿using com.breg.vision.ContentLoaders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using com.breg.vision.Pdf.Compressor;

namespace com.breg.vision.EmailTemplates
{
	public partial class PDFRenderer : System.Web.UI.UserControl
	{
        private static bool IsCompressPDF = Convert.ToBoolean(ConfigurationManager.AppSettings["IsCompressPDF"].ToString());
		public string PDFFile { get; set; }
		public Dictionary<string, string> TemplateVariables { get; set; }
        public static void AddPDFAsControls(Page page, ControlCollection controls, string pdfFile, string emailTemplatePath,
			Dictionary<string, string> templateTexts = null, Dictionary<string, byte[]> templateImages = null, int dpi = 0)
		{
			if (dpi <= 0)
			{
				dpi = 125;
				string dpiSetting = ConfigurationManager.AppSettings["PDFRendererDPI"];
				if (dpiSetting != null)
					Int32.TryParse(dpiSetting, out dpi);
			}
			string pdfPath = string.Format(@"{0}PDFRendererFiles/{1}", emailTemplatePath, pdfFile);
			byte[] pdfContents = File.ReadAllBytes(page.Server.MapPath(pdfPath.Trim()));

			int pageCount;
			pdfContents = PDFHelper.PopulatePDF(pdfContents, templateTexts, templateImages, out pageCount);

			for (int pageNumber = 1; pageNumber <= pageCount; pageNumber++)
			{
				var ctl = page.LoadControl(string.Format("{0}PDFRenderer.ascx", emailTemplatePath)) as PDFRenderer;
				MemoryStream pdfMS = new MemoryStream(pdfContents);
				MemoryStream jpgMS;
				PDFHelper.PDFToImage(null, pdfMS, out jpgMS, pageNumber, dpi);

                if (IsCompressPDF)
                {
                    //call to compress Images
                    byte[] image = null;
                    var imageMagickCompression = FileCompressorFactory.GetCompressor(CompressorFrameWork.ImageMagick);
                    image = imageMagickCompression.Compress(jpgMS.ToArray());

                    ctl.PDFImage.ImageUrl = "data:image/jpg;base64," + Convert.ToBase64String(image, 0, image.Length);
                }
                else
                    ctl.PDFImage.ImageUrl = "data:image/jpg;base64," + Convert.ToBase64String(jpgMS.ToArray(), 0, jpgMS.ToArray().Length);

                controls.Add(ctl);
			}
		}
    }
}