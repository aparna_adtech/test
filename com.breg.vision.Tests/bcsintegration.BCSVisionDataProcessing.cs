﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using com.breg.vision.bcsintegration.BCSVisionDataProcessing;
using com.breg.vision.Logger;

namespace com.breg.vision.Tests
{
	[TestClass]
	public class BCSVisionDataProcessing
	{
		[TestMethod]
		public void SyncCacheToVision()
		{
			try
			{
				Guid correlationId = Guid.NewGuid();
				FileLogger logger = new FileLogger();
				BCSToVision sync = new BCSToVision(correlationId, logger);
				sync.SyncCacheToVision();
			}
			catch (Exception ex)
			{
				Assert.IsTrue(false, ex.Message);
			}
			Assert.IsTrue(true);
		}
	}
}
