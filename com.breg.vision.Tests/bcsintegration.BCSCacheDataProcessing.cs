﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using com.breg.vision.bcsintegration.BCSCacheDataProcessing;
using com.breg.vision.Logger;

namespace com.breg.vision.Tests
{
	[TestClass]
	public class BCSCacheDataProcessing
	{
		[TestMethod]
		public void SyncBCSToCache()
		{
			try
			{
				Guid correlationId = Guid.NewGuid();
				FileLogger logger = new FileLogger();
                SQLLogger sqlLogger = new SQLLogger();
				BCSCacheSync sync = new BCSCacheSync(correlationId, logger, sqlLogger);
				sync.UpdateBCSCache();
			}
			catch (Exception ex)
			{
				Assert.IsTrue(false, ex.Message);
			}
			Assert.IsTrue(true);
		}
	}
}
