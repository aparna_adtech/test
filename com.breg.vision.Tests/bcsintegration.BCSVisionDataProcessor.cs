﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.Tests
{
	[TestClass]
    public class bcsintegration
    {
        [TestMethod]
        public void SyncBCSToCache()
        {
            try
            {
                Guid correlationId = new Guid();
                DataLogger logger = new DataLogger();
                BCSCacheSync sync = new BCSCacheSync(correlationId, logger);
                sync.UpdateBCSCache();
            }
            catch (Exception ex)
            {
                Assert.IsTrue(false, ex.Message);
            }
            Assert.IsTrue(true);
        }
    }
}
