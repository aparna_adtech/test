﻿using com.breg.vision.Handoff.ClientModels.VisionServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.ClientModels
{
    public class DispensementCustomBracePayloadModel
    {
        public DispensementCustomBrace Dispensement { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string DeviceID { get; set; }
        public string LastLocation { get; set; }
        public string DispenseIdentifier { get; set; }
        public bool IsV5Dispensement { get; set; }
    }
}
