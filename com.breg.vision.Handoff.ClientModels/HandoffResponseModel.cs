﻿using com.breg.vision.Handoff.CryptoLib;
using System;
using System.Collections.Generic;
using System.Linq;

namespace com.breg.vision.Handoff.Models
{
    public class HandoffResponseModel
    {
        [Encrypt]
        public string ResponsePayload { get; set; }
        public string[] FailureMessages { get; set; }
        public int ExecutionTime { get; set; }

        public HandoffResponseModel() { }

        public HandoffResponseModel(HandoffResponseModel source)
        {
            ResponsePayload = source.ResponsePayload;
            if (source.FailureMessages != null)
                FailureMessages = source.FailureMessages.Select(x => new String(x.ToCharArray())).ToArray();
            ExecutionTime = source.ExecutionTime;
        }
    }
}
