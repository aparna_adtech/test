﻿using com.breg.vision.Handoff.ClientModels.VisionServiceReference;
using System.Collections.ObjectModel;

namespace com.breg.vision.Handoff.ClientModels
{
    public class InventoryCountPayloadModel
    {
        public int PracticeLocationID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string DeviceID { get; set; }
        public string LastLocation { get; set; }
        public InventoryCycleType InventoryCycleType { get; set; }
        public InventoryCycle InventoryCycle { get; set; }
        public ObservableCollection<InventoryItemCount> InventoryItemCounts { get; set; }
    }
}
