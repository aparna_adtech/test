﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.Models
{
    public class HandoffStatusResponseModel
    {
        public JobStatus Status { get; set; }

        public HandoffResponseModel ReponseData { get; set; }

        public static HandoffStatusResponseModel InvalidTokenResponse()
        {
            return
                new HandoffStatusResponseModel
                {
                    Status = JobStatus.Failed,
                    ReponseData =
                            new HandoffResponseModel
                            {
                                FailureMessages = new[] { "Invalid token" }
                            }
                };
        }

        public static HandoffStatusResponseModel JobStillPendingResponse()
        {
            return
                new HandoffStatusResponseModel
                {
                    Status = JobStatus.Pending
                };
        }
    }
}
