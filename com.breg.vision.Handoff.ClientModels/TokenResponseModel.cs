﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.ClientModels
{
    public class TokenResponseModel
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_int { get; set; }
    }
}
