﻿namespace com.breg.vision.Handoff.Models
{
    public class HandoffSubmitResponseModel
    {
        public string Token { get; set; }
        public int ExpectedWait { get; set; }
    }
}