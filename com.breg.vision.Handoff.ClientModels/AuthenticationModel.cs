﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.ClientModels
{
    public class AuthenticationModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string DeviceID { get; set; }
        public string LastLocation { get; set; }
    }
}
