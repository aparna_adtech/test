﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.Models
{
    public enum JobStatus
    {
        Pending = 1,
        Completed = 2,
        Failed = 3
    }
}
