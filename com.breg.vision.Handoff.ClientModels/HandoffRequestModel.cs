﻿using com.breg.vision.Handoff.CryptoLib;

namespace com.breg.vision.Handoff.Models
{
    public class HandoffRequestModel
    {
        public JobTypes JobType { get; set; }
        [Encrypt]
        public string Payload { get; set; }
        public int HoldFor { get; set; }

        public string CorelationId { get; set; }

        public HandoffRequestModel() { }

        public HandoffRequestModel(HandoffRequestModel source)
        {
            JobType = source.JobType;
            Payload = source.Payload;
            HoldFor = source.HoldFor;
        }
    }
}