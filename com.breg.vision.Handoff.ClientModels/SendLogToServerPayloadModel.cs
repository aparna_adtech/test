﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.ClientModels
{
    public class SendLogToServerPayloadModel
    {
        public string Username { get; set; }
        public DateTime SubmissionDate { get; set; }
        public string ArchiveFilename { get; set; }
        public byte[] LogArchiveContents { get; set; }
    }
}
