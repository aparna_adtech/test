﻿using com.breg.vision.Handoff.ClientModels.Constants;

namespace com.breg.vision.Handoff.ClientModels
{
    public class DocumentPayloadModel : AuthenticationModel
    {
        public string DocumentId { get; set; }

        public string Document { get; set; }

        public string CorelationId { get; set; }

        //public enum DocumentStatus { get; set; } // enum

        public DocumentTypes DocumentType { get; set; } 

        public AssociatedEntityTypes AssociatedEntity { get; set; } // enum > Dispensement
        public int? AssociatedEntityId { get; set; } // int >> DispensementId

        public ReferenceSystemTypes ReferenceSystem { get; set; } // enum >  BCS, Cloud Connect
        public ReferenceEntityTypes ReferenceEntity { get; set; } // enum > Claim
        public string ReferenceEntityID { get; set; } // string > Claim Number
    }
}
