﻿namespace com.breg.vision.Handoff.Models
{
    public enum JobTypes
    {
        Dispensement = 1,
        DispensementCustomBrace = 2,
        InventoryCount = 3, 
        Document = 4, 
        VisionToIntegration = 5,
        VisionToDocument = 6,
        IntegrationToBCS = 7, 
        BcsToIntegration = 8,
        IntegrationToBcsDoc = 9,
        SendLogToServer = 99
    }
}