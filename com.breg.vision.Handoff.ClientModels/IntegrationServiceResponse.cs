﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.ClientModels
{
    public class IntegrationServiceResponse
    {
        public int RequestID { get; set; }
        public Guid RMQID { get; set; }
        public int IntegrationID { get; set; }
        public int StatusID { get; set; }
        public int EntityID { get; set; }
        public string EntityValue { get; set; }

        public string SentJSON { get; set; }
        public string ResultJSON { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
        public string ClaimId { get; set; }
    }
}
