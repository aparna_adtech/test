﻿using com.breg.vision.Handoff.ClientModels.VisionServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.Handoff.ClientModels
{
    public class DispensementPayloadModel : AuthenticationModel
    {
        public Dispensement Dispensement { get; set; }
        public string DispenseIdentifier { get; set; }
        public bool IsV5Dispensement { get; set; }
    }
}
