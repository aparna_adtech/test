﻿namespace com.breg.vision.Handoff.ClientModels.Constants
{
    public enum ReferenceSystemTypes
    {
        BCS = 1, 
        CloudConnect = 2
    }
}
