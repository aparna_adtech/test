﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BregVision.Domain.DispenseHistory.Utility;
using System.Linq;
using BregVision.Domain.DispenseHistory.Models;
using System.Collections.Generic;

namespace BregVision.Test
{
    [TestClass]
    public class ReceiptHistoryAggregatorTests
    {
        #region Test Params
        private int practiceLocationID = 3;
        private int productID = 103720;
        private DateTime startDate = new DateTime(2014, 01, 01);
        private DateTime endDate = new DateTime(2015, 12, 31);

        #endregion Test Params

        [TestMethod]
        public void CanRetrieveAndGenerateProductHistoricalSummary()
        {
            var result = ReceiptHistoryAggregator.GenerateReceiptHistorySummary(productID,practiceLocationID, startDate, endDate);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void CanRetrieveMonthlyAggregates()
        {
            var result = ReceiptHistoryAggregator.GetMonthlyReceiptHistoryByPracticeLocationID(practiceLocationID, startDate, endDate);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Any());
        }


        [TestMethod]
        public void CanRetrieveMonthlyAggregatesForProduct()
        {
            var result = ReceiptHistoryAggregator.GetMonthlyReceiptHistoryByPracticeCatalogProductID(productID,practiceLocationID, startDate, endDate);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Any());
        }

        [TestMethod]
        public void GeneratedSummaryFindsValidHighLowAvgMonths()
        {
            var monthAggs = generateSampleAggs();
            var summary = ReceiptHistoryAggregator.GenerateReceiptHistorySummary(monthAggs, startDate, endDate);
            Assert.IsTrue(summary.HighestMonth.Year == 2014 && summary.HighestMonth.Month == 3); //Found highest
            Assert.IsTrue(summary.LowestMonth.Year == 2014 && summary.LowestMonth.Month == 7); //Found lowest
            Assert.IsTrue(summary.AverageMonth.TotalQuantity > 2.00 && summary.AverageMonth.TotalQuantity < 2.01); //Found correct average (using total months in range, accounting for leap years)

        }

        private List<MonthlyHistoricalProductReceiptAggregate> generateSampleAggs()
        {
            return new List<MonthlyHistoricalProductReceiptAggregate>()
            {
                new MonthlyHistoricalProductReceiptAggregate() { PracticeCatalogProductID = 1, Year = 2014, Month =1, TotalQuantity = 7 },
                new MonthlyHistoricalProductReceiptAggregate() { PracticeCatalogProductID = 1, Year = 2014, Month =3, TotalQuantity = 16 },
                new MonthlyHistoricalProductReceiptAggregate() { PracticeCatalogProductID = 1, Year = 2014, Month =7, TotalQuantity = 0 },
                new MonthlyHistoricalProductReceiptAggregate() { PracticeCatalogProductID = 1, Year = 2014, Month =11, TotalQuantity = 2 },
                new MonthlyHistoricalProductReceiptAggregate() { PracticeCatalogProductID = 1, Year = 2015, Month =1, TotalQuantity = 11 },
                new MonthlyHistoricalProductReceiptAggregate() { PracticeCatalogProductID = 1, Year = 2015, Month =11, TotalQuantity = 12 }
            }; 
        }
    }
}
