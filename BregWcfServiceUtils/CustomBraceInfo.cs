﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BregWcfServiceUtils
{
    public class CustomBraceInfo
    {
        public string CustomPOCode;
        public int UserID;
        public short PracticeLocationID;
        public string Code;
        public int PracticeCatalogProductID;
        public short Quantity;

        public string Billing_CustomerNumber;
        public string Billing_Phone;
        public string Billing_Attention;
        public string Billing_Address1;
        public string Billing_Address2;
        public string Billing_City;
        public string Billing_State;
        public string Billing_Zip;

        public string Shipping_Attention;
        public string Shipping_Address1;
        public string Shipping_Address2;
        public string Shipping_City;
        public string Shipping_State;
        public string Shipping_Zip;

        public string PatientName;
        public string PhysicianID;
        public string Patient_Age;
        public string Patient_Weight;
        public string Patient_Height;
        public string Patient_Sex;

        public string BraceFor;
        public string MeasurementsTakenBy;
        public string BilatCastReform;

        public class ListItem
        {
            public bool Selected { get; set; }
            public string Value { get; set; }
            public string Text { get; set; }
        }
        public class rbl
        {
            public string SelectedValue { get; set; }
            public int SelectedIndex { get; set; }
            public ListItem SelectedItem { get; set; }
        }
        public class txt
        {
            public string Text { get; set; }
        }
        public class chk
        {
            public string SelectedValue { get; set; }
            public ListItem SelectedItem { get; set; }
            public List<ListItem> Items { get; set; }
        }
        public delegate void GetBilatSaveValuesDelegate(string l, string r, out string v1, out string v2);
    }
}
