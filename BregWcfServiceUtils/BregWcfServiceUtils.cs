﻿using System;
using System.Collections.Generic;
using System.Linq;
using ClassLibrary.DAL;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using Newtonsoft.Json;
using System.IO;

namespace BregWcfServiceUtils
{
    public class BregWcfServiceUtils : IVisionServiceUtils
    {
        readonly bool _IsReadOnly = false;

        public void CreateOrderFromCart(int userID, int practiceLocationID, string customPOCode)
        {
            try
            {
                int shoppingCartID = GetShoppingCartID(practiceLocationID);
                if (shoppingCartID == 0)
                    throw new Exception(
                        "Cannot create the order since there's nothing in the shopping cart for practice location " + practiceLocationID);
                CreatePurchaseOrder(practiceLocationID, shoppingCartID, userID, 4, customPOCode);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void CreateCustomBrace(CustomBraceInfo brace)
        {
            try
            {
                EmptyShoppingCart(brace.PracticeLocationID);
                DAL.PracticeLocation.Inventory.UpdateInventoryData(brace.PracticeLocationID, brace.PracticeCatalogProductID, brace.UserID, brace.Quantity);
                DataTable dtProduct = DAL.PracticeLocation.Inventory.GetCustomBraceFromShoppingCart(brace.PracticeLocationID, true);
                if (dtProduct == null)
                    throw new Exception("No custom braces in shopping cart for Practice Location ID " + brace.PracticeLocationID);

                int shoppingCartID = SaveCustomBraceInfo(dtProduct.Rows[0], brace.Code, brace);
                UpdateShoppingCartCustomPOCode(brace.PracticeLocationID, brace.CustomPOCode);
                CreatePurchaseOrder(brace.PracticeLocationID, shoppingCartID, brace.UserID, 4, brace.CustomPOCode);
                CheckInOrder(brace);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void AuthorizeAllPendingDevicesForPractice(int practiceID)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                // get active records that match the device id
                var query =
                    visionDB.DeviceAuthenticationRequests
                    .Where(
                        d => d.PracticeID == practiceID
                    );

                foreach (var device in query)
                {
                    ValidateOrRegisterDeviceForAuthorization(device.DeviceIdentifier, device.PracticeID);
                }
            }
        }

        public Stream GetTestProducts(int practiceLocationID, int maxItemsPerCategory)
        {
            string json = string.Empty;

            try
            {
                using (var visionDB = VisionDataContext.GetVisionDataContext())
                {
                    List<Dispense> dispenseDetails = new List<Dispense>();
                    var allProducts = (
                        from pcp in visionDB.PracticeCatalogProducts
                        join pi in visionDB.ProductInventories on pcp.PracticeCatalogProductID equals pi.PracticeCatalogProductID
                        join mcp in visionDB.MasterCatalogProducts on pcp.MasterCatalogProductID equals mcp.MasterCatalogProductID into mcpo
                        from mcp in mcpo.DefaultIfEmpty()
                        join tpp in visionDB.ThirdPartyProducts on pcp.ThirdPartyProductID equals tpp.ThirdPartyProductID into tppo
                        from tpp in tppo.DefaultIfEmpty()
                        where
                            (pi.PracticeLocationID == practiceLocationID) &&
                            (pcp.IsActive == true) && (pi.IsActive == true) &&
                            ((mcp != null && mcp.IsActive) || (tpp != null && tpp.IsActive)) &&
							(pi.QuantityOnHandPerSystem > 2)

                        select new Product
                        {
                            pcp = pcp != null ? new Product.PracticeCatalogProduct(pcp) : null,
                            pi = pi != null ? new Product.ProductInventory(pi) : null,
                            mcp = mcp != null ? new Product.MasterCatalogProduct(mcp) : null,
                            tpp = tpp != null ? new Product.ThirdPartyProduct(tpp) : null
                        }).ToArray();

                    Products products = new Products();
                    foreach (var product in allProducts)
                    {
						if (product.mcp != null && product.mcp.IsCustomBrace)
							products.CustomBraces.Add(product);
						else if (product.pcp.IsSplitCode && product.pcp.IsPreAuthorization)
							products.SplitCodePreAuthProducts.Add(product);
						else if (product.pcp.IsPreAuthorization)
							products.PreAuthOnlyProducts.Add(product);
						else if (product.pcp.IsSplitCode)
							products.SplitCodeOnlyProducts.Add(product);
						else
							products.NonSplitCodeNonPreAuthProducts.Add(product);
					}

					Products randomizedProducts = RandomizeProducts(products, maxItemsPerCategory);

					json = "{\"GetTestProducts\": " + JsonConvert.SerializeObject(randomizedProducts, Formatting.None,
                        new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }) + "}";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return new MemoryStream(System.Text.Encoding.UTF8.GetBytes(json));
        }

		private Products RandomizeProducts(Products products, int maxItemsPerCategory)
		{
			Products randomizedProducts = new Products();
			Random rand = new Random();
			bool done = false;
			while (!done)
			{
				done = true;
				for (int i = 0; i < 2; i++)
				{
					bool isCustomBrace = false;
					switch (i)
					{
						case 0:
							isCustomBrace = false;
							break;
						case 1:
							isCustomBrace = true;
							break;
					}
					for (int j = 0; j < 5; j++)
					{
						List<Product> source = null;
						List<Product> dest = null;
						switch (j)
						{
							case 0:
								source = products.NonSplitCodeNonPreAuthProducts;
								dest = randomizedProducts.NonSplitCodeNonPreAuthProducts;
								break;
							case 1:
								source = products.PreAuthOnlyProducts;
								dest = randomizedProducts.PreAuthOnlyProducts;
								break;
							case 2:
								source = products.SplitCodeOnlyProducts;
								dest = randomizedProducts.SplitCodeOnlyProducts;
								break;
							case 3:
								source = products.SplitCodePreAuthProducts;
								dest = randomizedProducts.SplitCodePreAuthProducts;
								break;
							case 4:
								source = products.CustomBraces;
								dest = randomizedProducts.CustomBraces;
								break;
						}
						if (source.Count > 0 && dest.Count < maxItemsPerCategory)
						{
							done = false;
							int index = rand.Next() % source.Count;
							Product product = source[index];
							if ((product.mcp == null && !isCustomBrace) ||
								(product.mcp != null && product.mcp.IsCustomBrace == isCustomBrace))
							{
								dest.Add(product);
								source.Remove(product);
							}
						}
					}
				}
			}
			return randomizedProducts;
		}

		public void AddScheduledPatient(ScheduledPatient patient)
		{
			EMRIntegration.EMRIntegrationClient service = new EMRIntegration.EMRIntegrationClient("EMRIntegration");
			service.AddScheduledPatient(
				patient.RemoteScheduleItemID,
				patient.PracticeID,
				patient.PracticeLocationID,
				patient.PatientID,
				patient.FirstName,
				patient.LastName,
				patient.PatientEmail,
				patient.PhysicianName,
				patient.DiagnosisICD9,
				patient.ScheduledVisitDateTime,
				patient.ScheduledVisitTimezone,
				patient.PatientDOBDateTime
			);
		}

		public class Product
        {
			public class PracticeCatalogProduct
			{
				public PracticeCatalogProduct(ClassLibrary.DAL.PracticeCatalogProduct pcp)
				{
					if (pcp != null)
					{
						PracticeCatalogProductID = pcp.PracticeCatalogProductID;
						PracticeID = pcp.PracticeID;
						PracticeCatalogSupplierBrandID = pcp.PracticeCatalogSupplierBrandID;
						MasterCatalogProductID = pcp.MasterCatalogProductID != null ? pcp.MasterCatalogProductID.Value : 0;
						ThirdPartyProductID = pcp.ThirdPartyProductID != null ? pcp.ThirdPartyProductID.Value : 0;
						IsThirdPartyProduct = pcp.IsThirdPartyProduct;
						WholesaleCost = pcp.WholesaleCost;
						BillingCharge = pcp.BillingCharge;
						DMEDeposit = pcp.DMEDeposit;
						BillingChargeCash = pcp.BillingChargeCash;
						StockingUnits = pcp.StockingUnits;
						BuyingUnits = pcp.BuyingUnits;
						Sequence = pcp.Sequence != null ? pcp.Sequence.Value : 0;
						CreatedUserID = pcp.CreatedUserID;
						CreatedDate = pcp.CreatedDate;
						ModifiedUserID = pcp.ModifiedUserID != null ? pcp.ModifiedUserID.Value : 0;
						ModifiedDate = pcp.ModifiedDate != null ? pcp.ModifiedDate.Value : DateTime.MinValue;
						IsActive = pcp.IsActive != null ? pcp.IsActive.Value : false;
						IsLogoAblePart = pcp.IsLogoAblePart;
						IsSplitCode = pcp.IsSplitCode;
						IsPreAuthorization = pcp.IsPreAuthorization;
						IsAlwaysOffTheShelf = pcp.IsAlwaysOffTheShelf;
					}
				}
				public int PracticeCatalogProductID;
				public int PracticeID;
				public int PracticeCatalogSupplierBrandID;
				public int MasterCatalogProductID;
				public int ThirdPartyProductID;
				public bool IsThirdPartyProduct;
				public decimal WholesaleCost;
				public decimal BillingCharge;
				public decimal DMEDeposit;
				public decimal BillingChargeCash;
				public int StockingUnits;
				public int BuyingUnits;
				public int Sequence;
				public int CreatedUserID;
				public DateTime CreatedDate;
				public int ModifiedUserID;
				public DateTime ModifiedDate;
				public bool IsActive;
				public bool IsLogoAblePart;
				public bool IsSplitCode;
				public bool IsPreAuthorization;
				public bool IsAlwaysOffTheShelf;
			}
			public PracticeCatalogProduct pcp;

			public class MasterCatalogProduct
			{
				public MasterCatalogProduct(ClassLibrary.DAL.MasterCatalogProduct mcp)
				{
					if (mcp != null)
					{
						MasterCatalogProductID = mcp.MasterCatalogProductID;
						MasterCatalogSupplierID = mcp.MasterCatalogSupplierID != null ? mcp.MasterCatalogSupplierID.Value : 0;
						MasterCatalogSubCategoryID = mcp.MasterCatalogSubCategoryID;
						Code = mcp.Code;
						Name = mcp.Name;
						ShortName = mcp.ShortName;
						Packaging = mcp.Packaging;
						LeftRightSide = mcp.LeftRightSide;
						Size = mcp.Size;
						Color = mcp.Color;
						Gender = mcp.Gender;
						WholesaleListCost = mcp.WholesaleListCost != null ? mcp.WholesaleListCost.Value : 0;
						Description = mcp.Description;
						IsDiscontinued = mcp.IsDiscontinued;
						DateDiscontinued = mcp.DateDiscontinued != null ? mcp.DateDiscontinued.Value : DateTime.MinValue;
						CreatedUserID = mcp.CreatedUserID;
						CreatedDate = mcp.CreatedDate;
						ModifiedUserID = mcp.ModifiedUserID != null ? mcp.ModifiedUserID.Value : 0;
						ModifiedDate = mcp.ModifiedDate != null ? mcp.ModifiedDate.Value : DateTime.MinValue;
						IsActive = mcp.IsActive;
						IsCustomBrace = mcp.IsCustomBrace;
						UPCCode = mcp.UPCCode;
						isCustomBraceAccessory = mcp.isCustomBraceAccessory;
						IsLogoAblePart = mcp.IsLogoAblePart;
					}
				}
				public int MasterCatalogProductID;
				public int MasterCatalogSupplierID;
				public int MasterCatalogSubCategoryID;
				public string Code;
				public string Name;
				public string ShortName;
				public string Packaging;
				public string LeftRightSide;
				public string Size;
				public string Color;
				public string Gender;
				public decimal WholesaleListCost;
				public string Description;
				public bool IsDiscontinued;
				public DateTime DateDiscontinued;
				public int CreatedUserID;
				public DateTime CreatedDate;
				public int ModifiedUserID;
				public DateTime ModifiedDate;
				public bool IsActive;
				public bool IsCustomBrace;
				public string UPCCode;
				public bool isCustomBraceAccessory;
				public bool IsLogoAblePart;
			}
			public MasterCatalogProduct mcp;

			public class ThirdPartyProduct
			{
				public ThirdPartyProduct(ClassLibrary.DAL.ThirdPartyProduct tpp)
				{
					if (tpp != null)
					{
						ThirdPartyProductID = tpp.ThirdPartyProductID;
						PracticeCatalogSupplierBrandID = tpp.PracticeCatalogSupplierBrandID;
						Code = tpp.Code;
						Name = tpp.Name;
						ShortName = tpp.ShortName;
						Packaging = tpp.Packaging;
						LeftRightSide = tpp.LeftRightSide;
						Size = tpp.Size;
						Color = tpp.Color;
						Gender = tpp.Gender;
						WholesaleListCost = tpp.WholesaleListCost != null ? tpp.WholesaleListCost.Value : 0;
						Description = tpp.Description;
						IsDiscontinued = tpp.IsDiscontinued;
						DateDiscontinued = tpp.DateDiscontinued != null ? tpp.DateDiscontinued.Value : DateTime.MinValue;
						OldMasterCatalogProductID = tpp.OldMasterCatalogProductID != null ? tpp.OldMasterCatalogProductID.Value : 0;
						CreatedUserID = tpp.CreatedUserID;
						CreatedDate = tpp.CreatedDate;
						ModifiedUserID = tpp.ModifiedUserID != null ? tpp.ModifiedUserID.Value : 0;
						ModifiedDate = tpp.ModifiedDate != null ? tpp.ModifiedDate.Value : DateTime.MinValue;
						IsActive = tpp.IsActive;
						Source = tpp.Source;
						UPCCode = tpp.UPCCode;
						IsLogoAblePart = tpp.IsLogoAblePart;
					}
				}
				public int ThirdPartyProductID;
				public int PracticeCatalogSupplierBrandID;
				public string Code;
				public string Name;
				public string ShortName;
				public string Packaging;
				public string LeftRightSide;
				public string Size;
				public string Color;
				public string Gender;
				public decimal WholesaleListCost;
				public string Description;
				public bool IsDiscontinued;
				public DateTime DateDiscontinued;
				public int OldMasterCatalogProductID;
				public int CreatedUserID;
				public DateTime CreatedDate;
				public int ModifiedUserID;
				public DateTime ModifiedDate;
				public bool IsActive;
				public string Source;
				public string UPCCode;
				public bool IsLogoAblePart;
			}
			public ThirdPartyProduct tpp;


			public class ProductInventory
			{
				public ProductInventory(ClassLibrary.DAL.ProductInventory pi)
				{
					if (pi != null)
					{
						ProductInventoryID = pi.ProductInventoryID;
						PracticeLocationID = pi.PracticeLocationID;
						PracticeCatalogProductID = pi.PracticeCatalogProductID;
						QuantityOnHandPerSystem = pi.QuantityOnHandPerSystem;
						ParLevel = pi.ParLevel != null ? pi.ParLevel.Value : 0;
						ReorderLevel = pi.ReorderLevel != null ? pi.ReorderLevel.Value : 0;
						CriticalLevel = pi.CriticalLevel != null ? pi.CriticalLevel.Value : 0;
						CreatedUserID = pi.CreatedUserID;
						CreatedDate = pi.CreatedDate;
						ModifiedUserID = pi.ModifiedUserID != null ? pi.ModifiedUserID.Value : 0;
						ModifiedDate = pi.ModifiedDate != null ? pi.ModifiedDate.Value : DateTime.MinValue;
						IsNotFlaggedForReorder = pi.IsNotFlaggedForReorder;
						IsSetToActive = pi.IsSetToActive != null ? pi.IsSetToActive.Value : false;
						IsActive = pi.IsActive;
						ConsignmentQuantity = pi.ConsignmentQuantity;
						IsConsignment = pi.IsConsignment;
						ConsignmentLockParLevel = pi.ConsignmentLockParLevel;
						IsBuyout = pi.IsBuyout;
						IsRMA = pi.IsRMA;
						BuyoutRMAInventoryCycleID = pi.BuyoutRMAInventoryCycleID != null ? pi.BuyoutRMAInventoryCycleID.Value : 0;
						IsLogoAblePart = pi.IsLogoAblePart;
					}
				}
				public int ProductInventoryID;
				public int PracticeLocationID;
				public int PracticeCatalogProductID;
				public int QuantityOnHandPerSystem;
				public int ParLevel;
				public int ReorderLevel;
				public int CriticalLevel;
				public int CreatedUserID;
				public DateTime CreatedDate;
				public int ModifiedUserID;
				public DateTime ModifiedDate;
				public bool IsNotFlaggedForReorder;
				public bool IsSetToActive;
				public bool IsActive;
				public int ConsignmentQuantity;
				public bool IsConsignment;
				public bool ConsignmentLockParLevel;
				public bool IsBuyout;
				public bool IsRMA;
				public int BuyoutRMAInventoryCycleID;
				public bool IsLogoAblePart;
			}
			public ProductInventory pi;
        }

		public class Products
		{
			public Products()
			{
				SplitCodeOnlyProducts = new List<Product>();
				PreAuthOnlyProducts = new List<Product>();
				SplitCodePreAuthProducts = new List<Product>();
				NonSplitCodeNonPreAuthProducts = new List<Product>();
				CustomBraces = new List<Product>();
			}
			public List<Product> SplitCodeOnlyProducts;
			public List<Product> PreAuthOnlyProducts;
			public List<Product> SplitCodePreAuthProducts;
			public List<Product> NonSplitCodeNonPreAuthProducts;
			public List<Product> CustomBraces;
		}

		private bool ValidateOrRegisterDeviceForAuthorization(string deviceIdentifier, int? practiceID)
        {
            if (deviceIdentifier == null)
                deviceIdentifier = string.Empty;

            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                // get active records that match the device id
                var query =
                    visionDB.Devices
                    .Where(
                        d =>
                            (d.DeviceIdentifier == deviceIdentifier.Trim())
                            && d.IsActive
                    );

                // if practice information is provided, then only match records that have a practice set
                if (practiceID.HasValue)
                    query = query.Where(d => d.PracticeID.HasValue && (d.PracticeID.Value == practiceID.Value));
                // if practice is not provided, then only match records that don't have a practice set
                else
                    query = query.Where(d => !d.PracticeID.HasValue);

                // check if there's at least 1 match
                var device = query.FirstOrDefault();
                if (device != null)
                {
                    //// update location if one was provided
                    //if (!string.IsNullOrEmpty(lastLocation))
                    //{
                    //    device.LastLocation = lastLocation;
                    //    visionDB.SubmitChanges();
                    //}
                    return true;
                }
                /* no device match means device not authorized, send back false and allow caller to decide
                 * if authorization is needed */
                else
                {
                    // always allow apple test account to validate
                    //if (userName.ToLower().Trim().Equals("appletest"))
                    {
                        CreateAuthorizedDevice(deviceIdentifier, practiceID);
                        return true;
                    }


                    //SendDeviceAuthorizationRequest(deviceIdentifier, userName, practiceID);
                    //throw new LoginException("Authorization for this device is pending.");
                }
            }
        }

        private void CreateAuthorizedDevice(string deviceIdentifier, int? practiceID)
        {
            var requestId = CreateDeviceAuthorization(deviceIdentifier, practiceID);
            ConfirmDeviceAuthorization(requestId.Value);
        }

        private bool ConfirmDeviceAuthorization(Guid authorizationRequestId)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var request =
                    visionDB.DeviceAuthenticationRequests
                    .Where(x => x.RequestID == authorizationRequestId)
                    .FirstOrDefault();

                if (request != null)
                {
                    var new_device =
                        new ClassLibrary.DAL.Device
                        {
                            DeviceIdentifier = request.DeviceIdentifier,
                            PracticeID = request.PracticeID,
                            PracticeLocationID = request.PracticeLocationID,
                            IsActive = true,
                            CreatedDate = DateTime.Now,
                            CreatedUserID = 1
                        };

                    visionDB.Devices.InsertOnSubmit(new_device);
                    visionDB.DeviceAuthenticationRequests.DeleteOnSubmit(request);
                    visionDB.SubmitChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private Guid? CreateDeviceAuthorization(string deviceIdentifier, int? practiceID)
        {
            try
            {
                using (var visionDB = VisionDataContext.GetVisionDataContext())
                {
                    var existing_req =
                        visionDB.DeviceAuthenticationRequests
                        .Where(
                            x =>
                                (x.DeviceIdentifier == deviceIdentifier.Trim())
                                && (x.PracticeID == practiceID)
                        )
                        .FirstOrDefault();

                    if (existing_req == null)
                    {
                        var req_id = Guid.NewGuid();

                        var request =
                            new DeviceAuthenticationRequest
                            {
                                CreatedDate = DateTime.Now,
                                DeviceIdentifier = deviceIdentifier,
                                PracticeID = practiceID,
                                PracticeLocationID = null,
                                RequestID = req_id
                            };
                        visionDB.DeviceAuthenticationRequests.InsertOnSubmit(request);
                        visionDB.SubmitChanges();

                        return req_id;
                    }
                    else
                    {
                        return existing_req.RequestID;
                    }
                }
            }
            catch
            {
                return null;
            }
        }

        private void CheckInOrder(CustomBraceInfo brace)
        {
            DataSet ds = DAL.PracticeLocation.CheckIn.GetOustandingCustomBracesforCheckIn_ParentLevel(brace.PracticeLocationID);
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    if ((string)row["CustomPurchaseOrderCode"] == brace.CustomPOCode)
                    {
                        CheckInBregVisionOrder((int)row["BregVisionOrderID"], brace);
                    }
                }
            }
        }

        private void CheckInBregVisionOrder(int bregVisionOrderID, CustomBraceInfo brace)
        {
            DataSet ds = DAL.PracticeLocation.CheckIn.GetOustandingCustomBracesforCheckIn_Detail1Level(brace.PracticeLocationID);
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    if ((int)row["BregVisionOrderID"] == bregVisionOrderID)
                    {
                        CheckInSupplierOrder(bregVisionOrderID, (int)row["SupplierOrderID"], brace);
                    }
                }
            }
        }

        private void CheckInSupplierOrder(int bregVisionOrderID, int supplierOrderID, CustomBraceInfo brace)
        {
            DataSet ds = DAL.PracticeLocation.CheckIn.GetOustandingCustomBracesforCheckIn_Detail2Level(brace.PracticeLocationID);
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    if ((int)row["SupplierOrderID"] == supplierOrderID)
                    {
                        DAL.PracticeLocation.CheckIn.POCheckIn_InsertUpdate(brace.UserID, (int)row["PracticeCatalogProductID"],
                            (int)row["ProductInventoryID"], bregVisionOrderID, supplierOrderID,
                            (int)row["SupplierOrderLineItemID"], (decimal)row["ActualWholesaleCost"], (int)row["QuantityToCheckIn"]);
                    }
                }
            }
        }

        private int SaveCustomBraceInfo(DataRow drProduct, string code, CustomBraceInfo brace)
        {
            const string BilatDbColumnSuffix = "_BilatRightLeg";
            CustomBraceInfo.rbl rblLegMeasurements = new CustomBraceInfo.rbl { SelectedValue = "" };
            CustomBraceInfo.rbl rblBraceFor = new CustomBraceInfo.rbl { SelectedValue = "" };
            CustomBraceInfo.rbl rblInstability = new CustomBraceInfo.rbl { SelectedValue = "None" };
            CustomBraceInfo.rbl rblInstabilityR = new CustomBraceInfo.rbl { SelectedValue = "None" };
            CustomBraceInfo.txt txtThigh = new CustomBraceInfo.txt { Text = "" };
            CustomBraceInfo.txt txtThighR = new CustomBraceInfo.txt { Text = "" };
            CustomBraceInfo.txt txtCalf = new CustomBraceInfo.txt { Text = "" };
            CustomBraceInfo.txt txtCalfR = new CustomBraceInfo.txt { Text = "" };
            CustomBraceInfo.txt txtKneeOffset = new CustomBraceInfo.txt { Text = "" };
            CustomBraceInfo.txt txtKneeOffsetR = new CustomBraceInfo.txt { Text = "" };
            CustomBraceInfo.txt txtKneeWidth = new CustomBraceInfo.txt { Text = "" };
            CustomBraceInfo.txt txtKneeWidthR = new CustomBraceInfo.txt { Text = "" };
            CustomBraceInfo.rbl rblExtension = new CustomBraceInfo.rbl { SelectedValue = "" };
            CustomBraceInfo.rbl rblExtensionR = new CustomBraceInfo.rbl { SelectedValue = "" };
            CustomBraceInfo.rbl rblFlexion = new CustomBraceInfo.rbl { SelectedValue = "" };
            CustomBraceInfo.rbl rblFlexionR = new CustomBraceInfo.rbl { SelectedValue = "" };
            CustomBraceInfo.rbl rblCounterForce = new CustomBraceInfo.rbl { SelectedValue = "" };
            CustomBraceInfo.rbl rblCounterForceR = new CustomBraceInfo.rbl { SelectedValue = "" };
            CustomBraceInfo.rbl rblX2KShippingType = new CustomBraceInfo.rbl { SelectedValue = "" };
            CustomBraceInfo.rbl rblX2KShippingCarrier = new CustomBraceInfo.rbl { SelectedValue = "" };
            CustomBraceInfo.txt txtCounterForceDegrees = new CustomBraceInfo.txt { Text = "" };
            CustomBraceInfo.txt txtCounterForceDegreesR = new CustomBraceInfo.txt { Text = "" };

            CustomBraceInfo.txt txtNotes = new CustomBraceInfo.txt { Text = "" };
            CustomBraceInfo.rbl rblColor = new CustomBraceInfo.rbl { SelectedValue = "" };
            CustomBraceInfo.txt txtPantone = new CustomBraceInfo.txt { Text = "" };
            CustomBraceInfo.rbl rblColorPatternEnhancement = new CustomBraceInfo.rbl { SelectedValue = "", SelectedItem = new CustomBraceInfo.ListItem() };
            CustomBraceInfo.txt txtColorPattern = new CustomBraceInfo.txt { Text = "" };
            CustomBraceInfo.rbl rblFusionShippingType = new CustomBraceInfo.rbl { SelectedValue = "1" }; // Next Day
            CustomBraceInfo.rbl rblFusionShippingCarrier = new CustomBraceInfo.rbl { SelectedValue = "1" }; // UPS
            CustomBraceInfo.rbl rblPattern = new CustomBraceInfo.rbl { SelectedValue = "" };

            CustomBraceInfo.chk chkX2KColor = new CustomBraceInfo.chk {
                Items = new List<CustomBraceInfo.ListItem>(),
                SelectedItem = new CustomBraceInfo.ListItem(),
                SelectedValue = "" };

            // NOTE: make sure we don't save anything if we're in read-only mode
            if (!_IsReadOnly && (drProduct != null))
            {
                int shoppingCartID = Convert.ToInt32(drProduct["ShoppingCartID"]);
                bool isBilat = rblLegMeasurements.SelectedValue.ToLower().Contains("bilat");
                bool isBraceForLeft = rblBraceFor.SelectedValue.ToLower().Contains("left");

                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_CreateCustomBraceOrderNew");

                db.AddInParameter(dbCommand, "ShoppingCartID", DbType.Int32, shoppingCartID);

                db.AddInParameter(dbCommand, "Billing_CustomerNumber", DbType.String, brace.Billing_CustomerNumber);
                db.AddInParameter(dbCommand, "Billing_Phone", DbType.String, brace.Billing_Phone);
                db.AddInParameter(dbCommand, "Billing_Attention", DbType.String, brace.Billing_Attention);
                db.AddInParameter(dbCommand, "Billing_Address1", DbType.String, brace.Billing_Attention);
                db.AddInParameter(dbCommand, "Billing_Address2", DbType.String, brace.Billing_Address2);
                db.AddInParameter(dbCommand, "Billing_City", DbType.String, brace.Billing_City);
                db.AddInParameter(dbCommand, "Billing_State", DbType.String, brace.Billing_State);
                db.AddInParameter(dbCommand, "Billing_Zip", DbType.String, brace.Billing_Zip);

                db.AddInParameter(dbCommand, "Shipping_Attention", DbType.String, brace.Shipping_Attention);
                db.AddInParameter(dbCommand, "Shipping_Address1", DbType.String, brace.Shipping_Address1);
                db.AddInParameter(dbCommand, "Shipping_Address2", DbType.String, brace.Shipping_Address2);
                db.AddInParameter(dbCommand, "Shipping_City", DbType.String, brace.Shipping_City);
                db.AddInParameter(dbCommand, "Shipping_State", DbType.String, brace.Shipping_State);
                db.AddInParameter(dbCommand, "Shipping_Zip", DbType.String, brace.Shipping_Zip);

                db.AddInParameter(dbCommand, "PatientName", DbType.String, brace.PatientName);
                db.AddInParameter(dbCommand, "PhysicianID", DbType.Int32, brace.PhysicianID);
                db.AddInParameter(dbCommand, "Patient_Age", DbType.Int32, brace.Patient_Age);
                db.AddInParameter(dbCommand, "Patient_Weight", DbType.Int32, brace.Patient_Weight);
                db.AddInParameter(dbCommand, "Patient_Height", DbType.String, brace.Patient_Height);
                db.AddInParameter(dbCommand, "Patient_Sex", DbType.String, brace.Patient_Sex);

                db.AddInParameter(dbCommand, "BraceFor", DbType.String, brace.BraceFor);
                db.AddInParameter(dbCommand, "MeasurementsTakenBy", DbType.String, brace.MeasurementsTakenBy);
                db.AddInParameter(dbCommand, "BilatCastReform", DbType.String, brace.BilatCastReform);
                if (isBilat)
                    DAL.PracticeLocation.Inventory.AddBilatProductToCart(brace.PracticeLocationID);
                else
                    DAL.PracticeLocation.Inventory.CorrectNonBilatProductInCart(brace.PracticeLocationID);

                // this delegate provides the logic for storing bilat right-leg info
                // in additional columns while maintaining backwards compatibility by 
                // storing single-leg info in the original columns
                var getV =
                    (CustomBraceInfo.GetBilatSaveValuesDelegate)
                    delegate (string l, string r, out string v1, out string v2)
                    {
                        v1 = (isBraceForLeft || isBilat) ? l : r;
                        v2 = isBilat ? r : "";
                    };

                // this delegate applies the left/right/bilat storage logic and
                // adds the necessary database parameters for proper storage
                var addP =
                    (Action<string, string, string>)
                    delegate (string p, string l, string r)
                    {
                        string v1;
                        string v2;
                        getV(l, r, out v1, out v2);
                        var t = DbType.String;
                        db.AddInParameter(dbCommand, p, t, v1);
                        db.AddInParameter(dbCommand, p + BilatDbColumnSuffix, t, v2);
                    };

                addP("Instability", rblInstability.SelectedValue, rblInstabilityR.SelectedValue);
                addP("ThighCircumference", txtThigh.Text, txtThighR.Text);
                addP("CalfCircumference", txtCalf.Text, txtCalfR.Text);
                addP("KneeOffset", txtKneeOffset.Text, txtKneeOffsetR.Text);
                addP("KneeWidth", txtKneeWidth.Text, txtKneeWidthR.Text);
                addP("Extension", rblExtension.SelectedValue, rblExtensionR.SelectedValue);
                addP("Flexion", rblFlexion.SelectedValue, rblFlexionR.SelectedValue);

                string x2kFramePadColor1 = "";
                string x2kFramePadColor2 = "";
                string x2k_Counterforce = "", x2k_Counterforce_BilatRightLeg = "";
                string x2k_Counterforce_Degrees = "", x2k_Counterforce_Degrees_BilatRightLeg = "";

                string fusion_Notes = "";
                string fusion_Color = "";
                string fusion_Pattern = "None";
                string fusion_Pattern_Notes = "";
                string fusion_Pantone = "";

                string shippingTypeID = "4";
                string shippingCarrierID = "";

                if (code.StartsWith("2")) //(productName.ToLower().Contains("x2k"))
                {
                    x2kFramePadColor1 = chkX2KColor.SelectedValue;
                    foreach (CustomBraceInfo.ListItem fpc in chkX2KColor.Items)
                    {
                        if (fpc.Selected == true && fpc != chkX2KColor.SelectedItem)
                        {
                            x2kFramePadColor2 = fpc.Value;
                        }
                    }
                    getV(rblCounterForce.SelectedValue, rblCounterForceR.SelectedValue, out x2k_Counterforce, out x2k_Counterforce_BilatRightLeg);
                    getV(txtCounterForceDegrees.Text, txtCounterForceDegreesR.Text, out x2k_Counterforce_Degrees, out x2k_Counterforce_Degrees_BilatRightLeg);
                    //shippingTypeID = rblX2KShippingType.SelectedValue;
                    shippingCarrierID = rblX2KShippingCarrier.SelectedValue;

                    //Set Session Variable for Delivery method
                    //Session["ShippingTypeID"] = rblX2KShippingType.SelectedValue.ToString();
                }
                else
                {
                    fusion_Notes = txtNotes.Text;
                    fusion_Color = rblColor.SelectedValue;
                    fusion_Pantone = txtPantone.Text;

                    //if (rblColorPatternEnhancement.SelectedIndex > -1)
                    //{
                    //    fusion_Pattern = rblColorPatternEnhancement.SelectedItem.Text;
                    //}
                    fusion_Pattern_Notes = txtColorPattern.Text;
                    //shippingTypeID = rblFusionShippingType.SelectedValue;
                    shippingCarrierID = rblFusionShippingCarrier.SelectedValue;

                    //Set Session Variable for Delivery method
                    //Session["ShippingTypeID"] = rblFusionShippingType.SelectedValue.ToString();
                }

                db.AddInParameter(dbCommand, "X2K_FramePadColor1", DbType.String, x2kFramePadColor1);
                db.AddInParameter(dbCommand, "X2K_FramePadColor2", DbType.String, x2kFramePadColor2);
                db.AddInParameter(dbCommand, "X2K_Counterforce", DbType.String, x2k_Counterforce);
                db.AddInParameter(dbCommand, "X2K_Counterforce" + BilatDbColumnSuffix, DbType.String, x2k_Counterforce_Degrees_BilatRightLeg);
                db.AddInParameter(dbCommand, "X2K_Counterforce_degrees", DbType.String, x2k_Counterforce_Degrees);
                db.AddInParameter(dbCommand, "X2K_Counterforce_degrees" + BilatDbColumnSuffix, DbType.String, x2k_Counterforce_Degrees_BilatRightLeg);

                db.AddInParameter(dbCommand, "Fusion_Notes", DbType.String, fusion_Notes);
                db.AddInParameter(dbCommand, "Fusion_Enhancement", DbType.String, fusion_Pattern);
                db.AddInParameter(dbCommand, "Fusion_Color", DbType.String, fusion_Color);
                db.AddInParameter(dbCommand, "Fusion_Pantone", DbType.String, fusion_Pantone);
                db.AddInParameter(dbCommand, "Fusion_Pattern", DbType.String, rblPattern.SelectedValue);
                db.AddInParameter(dbCommand, "Fusion_Pattern_Notes", DbType.String, fusion_Pattern_Notes);

                db.AddInParameter(dbCommand, "ShippingTypeID", DbType.Int32, Convert.ToInt32(shippingTypeID));
                db.AddInParameter(dbCommand, "ShippingCarrierID", DbType.Int32, Convert.ToInt32(shippingCarrierID));

                db.AddParameter(dbCommand, "@return_value", DbType.Int32, ParameterDirection.ReturnValue, null, DataRowVersion.Default, null);

                db.ExecuteNonQuery(dbCommand);

                var return_value = Convert.ToInt32(db.GetParameterValue(dbCommand, "@return_value"));
                if (return_value != 0)
                    throw new ApplicationException("Custom brace insert failed. Return value was: " + return_value.ToString() + ".");
                return shoppingCartID;
            }
            return 0;
        }
        private void UpdateShoppingCartCustomPOCode(int practiceLocatonID, string customPO)
        {
            var db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            var cmd = db.GetStoredProcCommand("usp_ShoppingCart_Update_CustomPurchaseOrderCodeByPracticeLocationID");
            db.AddInParameter(
                cmd,
                "PracticeLocationID",
                DbType.Int32,
                practiceLocatonID);
            db.AddInParameter(
                cmd,
                "CustomPurchaseOrderCode",
                DbType.String,
                string.IsNullOrEmpty(customPO)
                    ? (object)DBNull.Value : (object)customPO);
            db.ExecuteNonQuery(cmd);
        }
        private Int32 CreatePurchaseOrder(Int32 PracticeLocationID,
                Int32 ShoppingCartID, Int32 UserID, Int16 ShippingTypeID, string CustomPurchaseOrderCode)
        {
            Int32 PurchaseOrderID = 0;
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_CreatePurchaseOrderNew");
            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);
            db.AddInParameter(dbCommand, "ShoppingCartID", DbType.Int32, ShoppingCartID);
            db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
            db.AddInParameter(dbCommand, "ShippingTypeID", DbType.Int16, ShippingTypeID);
            db.AddInParameter(dbCommand, "CustomPurchaseOrderCode", DbType.String, CustomPurchaseOrderCode);
            db.AddOutParameter(dbCommand, "PurchaseOrderID", DbType.Int32, PurchaseOrderID);

            Int32 SQLReturn = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            Int32 return_value = Convert.ToInt32("0" + db.GetParameterValue(dbCommand, "PurchaseOrderID").ToString());
            return return_value;
        }
        private void EmptyShoppingCart(int practiceLocationID)
        {
            DataSet ds = GetShoppingCart(practiceLocationID);
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    DeleteItemFromShoppingCart((int)row["ShoppingCartItemID"]);
                }
            }
        }
        private int GetShoppingCartID(int practiceLocationID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetShoppingCart");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    int shoppingCartID = (int)row["ShoppingCartID"];
                    return shoppingCartID;
                }
            }
            return 0;
        }
        private DataSet GetShoppingCart(int practiceLocationID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetShoppingCart");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        private void DeleteItemFromShoppingCart(Int32 shoppingCartItemID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_DeleteItemfromShoppingCartItem");

            db.AddInParameter(dbCommand, "ShoppingCartItemID", DbType.Int32, shoppingCartItemID);

            int SQLReturn = db.ExecuteNonQuery(dbCommand);
        }
    }
}
