﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.IO;

namespace BregWcfServiceUtils
{
    [ServiceContract]
    public interface IVisionServiceUtils
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "CreateCustomBrace")]
        void CreateCustomBrace(CustomBraceInfo customBrace);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "CreateOrderFromCart?userID={userID}&practiceLocationID={practiceLocationID}&customPOCode={customPOCode}")]
        void CreateOrderFromCart(int userID, int practiceLocationID, string customPOCode);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "AuthorizeAllPendingDevicesForPractice?practiceID={practiceID}")]
        void AuthorizeAllPendingDevicesForPractice(int practiceID);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "GetTestProducts?PracticeLocationID={practiceLocationID}&MaxItemsPerCategory={maxItemsPerCategory}")]
        Stream GetTestProducts(int practiceLocationID, int maxItemsPerCategory);

		[OperationContract]
		[WebInvoke(Method = "POST",
			RequestFormat = WebMessageFormat.Json,
			ResponseFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Wrapped,
			UriTemplate = "AddScheduledPatient")]
		void AddScheduledPatient(ScheduledPatient Patient);
    }

	public class ScheduledPatient
	{
		public string RemoteScheduleItemID;
		public int PracticeID;
		public int PracticeLocationID;
		public string PatientID;
		public string FirstName;
		public string LastName;
		public string PatientEmail;
		public string PhysicianName;
		public string DiagnosisICD9;
		public string ScheduledVisitDateTime;
		public string ScheduledVisitTimezone;
		public string PatientDOBDateTime;
	}
}
