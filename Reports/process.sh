#!/bin/sh --

#
# NOTES
# -----
# This script is designed to run on a UNIX system (i.e. Mac OSX Terminal).
# It creates beta versions of reports that should be uploaded to the 
# /PracticeAdminReports/4.1test directory on the SSRS server.
#
# HOWTO
# -----
# 1. Make sure "o" directory is created
# 2. % cat - | ./process.sh
#    (enter 1 filename per line)
#    CTRL-D
#
while read x; do
	sed -f sedfile.txt "$x" > "o/$x"
done
