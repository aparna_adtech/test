﻿using ClassLibrary.DAL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BregVision.GroupingIdConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                /*
                 * this is commented out so that it is not accidentally run.
                 *  To run the application uncomment the below line*/

                // GroupUngroupedDispenseQueues();
            }
            catch (Exception e)
            {
                Console.WriteLine("There was an error");
                Console.WriteLine(e.Message);
            }
            // GroupUngroupedDispenseQueues();
            Console.WriteLine("Press enter to end execution");
            Console.ReadLine();
        }

        public static bool GroupUngroupedDispenseQueues()
        {
            int recordsUpdated = 0;
            //dispensed
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {

                Console.WriteLine(visionDB.Connection.ConnectionString);
                List<string> sqlStatements = new List<string>();
                int maxId = visionDB.Dispenses.Max(x => x.DispenseID);
                int dispenseId = -1;
                var notDone = true;
                while (notDone)
                {
                    var plus1000 = (dispenseId + 1000);


                    Console.WriteLine(dispenseId + " before quere " + DateTime.Now.TimeOfDay);
                    var groupedDispenseDetailsForPractice = visionDB.ExecuteQuery<DispenseDetail>($"select * from dispenseDetail Where DispenseID >= {dispenseId} and DispenseID < {plus1000} " +
                                                                                                  $"and  DispenseQueueID is not null").ToList().GroupBy(x => x.DispenseID).ToList();
                    Console.WriteLine(dispenseId + " after " + DateTime.Now.TimeOfDay);



                    foreach (var group in groupedDispenseDetailsForPractice)
                    {
                        var guid = Guid.NewGuid().ToString();
                        var idCsv = string.Join(", ", group.Select(x => x.DispenseQueueID));
                        var sqlStatement = $"update dispensequeue set dispenseGroupingId = \'{guid}\' where dispenseQueueId in ({idCsv});";
                        sqlStatements.Add(sqlStatement);
                        if (sqlStatements.Count > 999)
                        {
                            visionDB.ExecuteCommand(string.Join(" ", sqlStatements));
                            recordsUpdated += 999;
                            Console.WriteLine(recordsUpdated);
                            sqlStatements.Clear();
                        }
                    }

                    if (!groupedDispenseDetailsForPractice.Any() && maxId < dispenseId)
                    {
                        notDone = false;
                    }

                    dispenseId += 1000;

                }
                if (sqlStatements.Any())
                {
                    visionDB.ExecuteCommand(string.Join(" ", sqlStatements));
                    recordsUpdated += 1000;
                    Console.WriteLine(recordsUpdated);
                    sqlStatements.Clear();
                }
            }
            // undispensed
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                List<string> sqlStatements = new List<string>();
                int maxId = visionDB.DispenseQueues.Max(x => x.PracticeLocationID);
                var minHcp = visionDB.DispenseQueues.Min(x => x.PhysicianID.Value);
                for (int practiceLocationId = -1; practiceLocationId <= maxId; practiceLocationId++)
                {
                    Console.WriteLine(practiceLocationId);
                    var currentPracticeLocationId = practiceLocationId;
                    var groupedDispenseQueues = visionDB.ExecuteQuery<DispenseQueue>($"select * from dispensequeue where dispenseGroupingId is null and IsDispensed = 0 and PracticeLocationID = {currentPracticeLocationId} " +
                                                                                     $"and PatientCode is not null and PatientCode != '' and PhysicianID is not null and PhysicianID > {minHcp} and " +
                                                                                     $"DispenseDate is not null").GroupBy(x => new { x.IsActive, x.PatientCode, x.DispenseDate, x.PhysicianID, DispenseSignatureId = x.DispenseSignatureID ?? -1, BregVisionOrderId = x.BregVisionOrderID ?? -1 }).ToList();

                    foreach (var group in groupedDispenseQueues)
                    {
                        var guid = Guid.NewGuid().ToString();
                        var idCsv = string.Join(", ", group.Select(x => x.DispenseQueueID.ToString()));
                        var sqlStatement = $"update dispensequeue set dispenseGroupingId = \'{guid}\' where dispenseQueueId in ({idCsv});";
                        sqlStatements.Add(sqlStatement);
                        if (sqlStatements.Count > 1000)
                        {
                            visionDB.ExecuteCommand(string.Join(" ", sqlStatements));
                            recordsUpdated += 1000;
                            Console.WriteLine(recordsUpdated);
                            sqlStatements.Clear();
                        }
                    }
                }
                if (sqlStatements.Any())
                {
                    visionDB.ExecuteCommand(string.Join(" ", sqlStatements));
                    recordsUpdated += 1000;
                    Console.WriteLine(recordsUpdated);
                    sqlStatements.Clear();
                }
            }
            //remaining
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                List<string> sqlStatements = new List<string>();
                int maxId = visionDB.DispenseQueues.Max(x => x.PracticeLocationID);

                for (int practiceLocationId = -1; practiceLocationId <= maxId; practiceLocationId++)
                {
                    var currentPracticeLoactionId = practiceLocationId;
                    var remainingForPractice = visionDB.ExecuteQuery<DispenseQueue>($"Select * from dispensequeue where DispenseGroupingId is null and practicelocationid = {currentPracticeLoactionId} ").ToList();
                    Console.WriteLine(currentPracticeLoactionId);
                    foreach (var queueRecord in remainingForPractice)
                    {
                        var guid = Guid.NewGuid().ToString();
                        var sqlStatement =
                            $"update dispensequeue set dispenseGroupingId = \'{guid}\' where dispenseQueueId = {queueRecord.DispenseQueueID};";
                        sqlStatements.Add(sqlStatement);
                        if (sqlStatements.Count > 1000)
                        {
                            visionDB.ExecuteCommand(string.Join(" ", sqlStatements));
                            recordsUpdated += 1000;
                            Console.WriteLine(recordsUpdated);
                            sqlStatements.Clear();
                        }
                    }
                }
                if (sqlStatements.Any())
                {
                    visionDB.ExecuteCommand(string.Join(" ", sqlStatements));
                    recordsUpdated += 1000;
                    Console.WriteLine(recordsUpdated);
                    sqlStatements.Clear();
                }
            }
            return true;
        }
    }
}
