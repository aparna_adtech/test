﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.IO;

namespace BregWcfServiceEMR
{
    [ServiceContract]
    public interface IVisionServiceEMR
    {
		[OperationContract]
		[WebInvoke(Method = "GET",
			ResponseFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Wrapped,
			UriTemplate = "GetDispenseData?DispenseID={dispenseID}")]
		Stream GetDispenseData(int dispenseID);

		[OperationContract]
		[WebInvoke(Method = "GET",
			ResponseFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Wrapped,
			UriTemplate = "GetScheduleItemData?PracticeID={practiceID}&PracticeLocationID={practiceLocationID}&MinDate={minDate}&MaxDate={maxDate}")]
		Stream GetScheduleItemData(int practiceID, int practiceLocationID, string minDate, string maxDate);

		[OperationContract]
		[WebInvoke(Method = "GET",
			ResponseFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Wrapped,
			UriTemplate = "GetPractices")]
		Stream GetPractices();

		[OperationContract]
		[WebInvoke(Method = "GET",
			ResponseFormat = WebMessageFormat.Json,
			BodyStyle = WebMessageBodyStyle.Wrapped,
			UriTemplate = "GetPracticeLocations?PracticeID={practiceID}")]
		Stream GetPracticeLocations(int practiceID);
	}
}
