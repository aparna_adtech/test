//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BregWcfServiceEMR
{
    using System;
    using System.Collections.Generic;
    
    public partial class DispenseQueue
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DispenseQueue()
        {
            this.DispenseDetails = new HashSet<DispenseDetail>();
        }
    
        public int DispenseQueueID { get; set; }
        public int PracticeLocationID { get; set; }
        public int ProductInventoryID { get; set; }
        public string PatientCode { get; set; }
        public Nullable<int> PhysicianID { get; set; }
        public string ICD9_Code { get; set; }
        public bool IsMedicare { get; set; }
        public bool ABNForm { get; set; }
        public string Side { get; set; }
        public decimal DMEDeposit { get; set; }
        public int QuantityToDispense { get; set; }
        public Nullable<System.DateTime> DispenseDate { get; set; }
        public bool IsCashCollection { get; set; }
        public bool IsDispensed { get; set; }
        public Nullable<int> BregVisionOrderID { get; set; }
        public int CreatedUser { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedUser { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public bool IsActive { get; set; }
        public string ABNReason { get; set; }
        public Nullable<int> DispenseSignatureID { get; set; }
        public string PatientEmail { get; set; }
        public bool IsRental { get; set; }
        public string DeclineReason { get; set; }
        public int MedicareOption { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientLastName { get; set; }
        public string Note { get; set; }
        public Nullable<int> PracticePayerID { get; set; }
        public string MedicalRecordNumber { get; set; }
        public Nullable<bool> IsCustomFit { get; set; }
        public Nullable<int> FitterID { get; set; }
        public Nullable<bool> IsCustomFabricated { get; set; }
        public string HCPCs { get; set; }
        public string DeviceIdentifier { get; set; }
        public string DispenseIdentifier { get; set; }
        public string ABNType { get; set; }
        public string Mod1 { get; set; }
        public string Mod2 { get; set; }
        public string Mod3 { get; set; }
        public string QueuedReason { get; set; }
        public string DispenseGroupingId { get; set; }
        public bool PreAuthorized { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DispenseDetail> DispenseDetails { get; set; }
        public virtual DispenseSignature DispenseSignature { get; set; }
    }
}
