﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using Newtonsoft.Json;

namespace BregWcfServiceEMR
{
    public class BregWcfServiceEMR : IVisionServiceEMR
    {
        readonly bool _IsReadOnly = false;

		public Stream GetDispenseData(int dispenseID)
		{
			string json = string.Empty;

			try
			{
				using (var db = new BregVisionEntities())
				{
					db.Configuration.LazyLoadingEnabled = false;
					db.Configuration.ProxyCreationEnabled = false;

					var dispenseQuery = (
						from d in db.Dispenses
						join dd in db.DispenseDetails on d.DispenseID equals dd.DispenseID
						join dq in db.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
						join pl in db.PracticeLocations on d.PracticeLocationID equals pl.PracticeLocationID
						join p in db.Practices on pl.PracticeID equals p.PracticeID
						join ds in db.DispenseSignatures on dq.DispenseSignatureID equals ds.DispenseSignatureID into dso
						from ds in dso.DefaultIfEmpty()
						where
							(d.DispenseID == dispenseID)
						//orderby so.SupplierOrderID descending, soli.SupplierOrderLineItemID descending
						select new
						{
							d,
							dd,
							dq,
							pl,
							p,
							ds
						}).ToList();

					List<Dispense> dispenses = new List<Dispense>();
					foreach (var item in dispenseQuery)
					{
						dispenses.Add(item.d);
					}
					Dispense[] dispensesArray = dispenses.ToArray();
					json = "{\"GetDispenseData\": " + JsonConvert.SerializeObject(dispensesArray, Formatting.None,
						new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }) + "}";
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
			}
			return new MemoryStream(System.Text.Encoding.UTF8.GetBytes(json));
		}

		public Stream GetPracticeLocations(int practiceID)
		{
			string json = string.Empty;

			try
			{
				using (var db = new BregVisionEntities())
				{
					db.Configuration.LazyLoadingEnabled = false;
					db.Configuration.ProxyCreationEnabled = false;

					var practiceLocationQuery = (
						from pl in db.PracticeLocations
						where
							(pl.IsActive && pl.PracticeID == practiceID)
						//orderby so.SupplierOrderID descending, soli.SupplierOrderLineItemID descending
						select new
						{
							pl
						}).ToList();

					List<PracticeLocation> practiceLocations = new List<PracticeLocation>();
					foreach (var item in practiceLocationQuery)
					{
						practiceLocations.Add(item.pl);
					}
					PracticeLocation[] practiceLocationsArray = practiceLocations.ToArray();
					json = "{\"GetPracticeLocations\": " + JsonConvert.SerializeObject(practiceLocationsArray, Formatting.None,
						new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }) + "}";
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
			}
			return new MemoryStream(System.Text.Encoding.UTF8.GetBytes(json));
		}

		public Stream GetPractices()
		{
			string json = string.Empty;

			try
			{
				using (var db = new BregVisionEntities())
				{
					db.Configuration.LazyLoadingEnabled = false;
					db.Configuration.ProxyCreationEnabled = false;

					var practiceQuery = (
						from p in db.Practices
						where
							(p.IsActive)
						//orderby so.SupplierOrderID descending, soli.SupplierOrderLineItemID descending
						select new
						{
							p
						}).ToList();

					List<Practice> practices = new List<Practice>();
					foreach (var item in practiceQuery)
					{
						practices.Add(item.p);
					}
					Practice[] practicesArray = practices.ToArray();
					json = "{\"GetPractices\": " + JsonConvert.SerializeObject(practicesArray, Formatting.None,
						new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }) + "}";
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
			}
			return new MemoryStream(System.Text.Encoding.UTF8.GetBytes(json));
		}

		public Stream GetScheduleItemData(int practiceID, int practiceLocationID, string minDate, string maxDate)
		{
			string json = string.Empty;

			if (practiceLocationID != 0)
				practiceID = 0;
			if (practiceID != 0)
				practiceLocationID = 0;

			DateTime theMinDate = DateTime.Today.AddMonths(-1);
			DateTime theMaxDate = DateTime.Today.AddDays(1);
			if (minDate != null && minDate != string.Empty)
				DateTime.TryParse(minDate, out theMinDate);
			if (maxDate != null && maxDate != string.Empty)
				DateTime.TryParse(maxDate, out theMaxDate);

			try
			{
				using (var db = new BregVisionEMREntities())
				{
					db.Configuration.LazyLoadingEnabled = false;
					db.Configuration.ProxyCreationEnabled = false;

					var scheduleItemQuery =
						from si in db.schedule_item
						where
							(si.practice_id == practiceID || si.practice_location_id == practiceLocationID) &&
							(si.scheduled_visit_datetime > theMinDate) && (si.scheduled_visit_datetime < theMaxDate)
						group si by si.practice_id into pg
						from plg in
							(from si in pg
							 group si by si.practice_location_id)
						group plg by pg.Key;

					List<object> pgItems = new List<object>();
					foreach (var pg in scheduleItemQuery)
					{
						List<object> plgItems = new List<object>();
						foreach (var plg in pg)
						{
							plgItems.Add(new { practice_location_id = plg.Key, schedule_items = plg });
						}
						pgItems.Add(new { practice_id = pg.Key, practice_locations = plgItems });
					}
					object[] scheduleItemsArray = pgItems.ToArray();
					json = "{\"GetScheduleItemData\": " + JsonConvert.SerializeObject(scheduleItemsArray, Formatting.None,
						new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }) + "}";
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
			}
			return new MemoryStream(System.Text.Encoding.UTF8.GetBytes(json));
		}
	}
}
