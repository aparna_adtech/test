﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data.Linq;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using ClassLibrary.DAL;
using System.Net;
using System.Configuration;
using Tamir.SharpSsh;



namespace BregOracle
{
    public class BregOracleOrder
    {
        public struct ShipServiceLevelMapItem
        {
            public string VisionShipServiceLevelText { get; set; }
            public string OracleShipServiceLevelText { get; set; }
        }

        public static readonly ShipServiceLevelMapItem[] ShipServiceLevelMap =
            new ShipServiceLevelMapItem[] {
                new ShipServiceLevelMapItem { VisionShipServiceLevelText = "Ground", OracleShipServiceLevelText = "UPS-Parcel-Ground" },
                new ShipServiceLevelMapItem { VisionShipServiceLevelText = "Ground (3-5 days)", OracleShipServiceLevelText = "UPS-Parcel-Ground" },
                new ShipServiceLevelMapItem { VisionShipServiceLevelText = "Third Day", OracleShipServiceLevelText = "UPS-Air-3 Day Select" },
                new ShipServiceLevelMapItem { VisionShipServiceLevelText = "Second Day", OracleShipServiceLevelText = "UPS-Air-2nd Day Air" },
                new ShipServiceLevelMapItem { VisionShipServiceLevelText = "Next Day Saver", OracleShipServiceLevelText = "UPS-Air-Next Day Air Saver" },
                new ShipServiceLevelMapItem { VisionShipServiceLevelText = "Next Day", OracleShipServiceLevelText = "UPS-Air-Next Day Air" }
            };

        public static string GetShipServiceLevelFromOrder(BregOrder bregOrder)
        {
            return
                ShipServiceLevelMap
                        .Where(x => x.VisionShipServiceLevelText.Equals(bregOrder.ShippingTypeName, StringComparison.OrdinalIgnoreCase))
                        .Select(x => x.OracleShipServiceLevelText).FirstOrDefault()
                        ?? "Error";
        }

        public static XmlDocument WriteXMLDocument(BregOrder bregOrder)
        {

            //Notes  
            //We don't have a way to actually determine the time zone.  We are using server time with +8 for Pacific time zone
            //we need to store a code for Partner Address, and then invalidate it by putting "-E" at beginning or end if they change their address in Vision.
            //I should be sending a Customer# in UserArea/Attribute2, but I'm not sure which to send

            try
            {
                XmlDocument doc = BregOracleOrder.LoadXMLDoc();

                XmlNode xnDateTime = doc.SelectNodes("/SHOW_SHIPMENT_005/CNTROLAREA/DATETIME").Item(0);

                WriteDateTimeToXML(xnDateTime, bregOrder.CreatedDate.GetValueOrDefault());

                XmlNode xnDocDateTime = doc.SelectNodes("/SHOW_SHIPMENT_005/DATAAREA/SHOW_SHIPMENT/SHIPMENT/DATETIME").Item(0);

                WriteDateTimeToXML(xnDocDateTime, bregOrder.CreatedDate.GetValueOrDefault());

                #region Document Ref
                doc.SelectSingleNode("/SHOW_SHIPMENT_005/DATAAREA/SHOW_SHIPMENT/SHIPMENT/DOCUMENTID").InnerText = bregOrder.PoNumber;
                #endregion

                #region UserArea
                doc.SelectSingleNode("/SHOW_SHIPMENT_005/DATAAREA/SHOW_SHIPMENT/SHIPMENT/USERAREA/ATTRIBUTE2").InnerText = bregOrder.UniqueOrderID;
                doc.SelectSingleNode("/SHOW_SHIPMENT_005/DATAAREA/SHOW_SHIPMENT/SHIPMENT/USERAREA/ATTRIBUTE3").InnerText = bregOrder.AccountCode;
                doc.SelectSingleNode("/SHOW_SHIPMENT_005/DATAAREA/SHOW_SHIPMENT/SHIPMENT/USERAREA/ATTRIBUTE4").InnerText = string.Format("{0:C}", bregOrder.Total);
                doc.SelectSingleNode("/SHOW_SHIPMENT_005/DATAAREA/SHOW_SHIPMENT/SHIPMENT/USERAREA/ATTRIBUTE13").InnerText = bregOrder.EmailAddress;
                if (!string.IsNullOrEmpty(bregOrder.LogoPartNumber))
                {
                    doc.SelectSingleNode("/SHOW_SHIPMENT_005/DATAAREA/SHOW_SHIPMENT/SHIPMENT/USERAREA/ATTRIBUTE5").InnerText = "LOGO ORDER";
                }
                #endregion

                #region Ship To
                XmlNode xnPartnerShipping = doc.SelectNodes("/SHOW_SHIPMENT_005/DATAAREA/SHOW_SHIPMENT/SHIPMENT/PARTNER").Item(0);
                xnPartnerShipping.SelectSingleNode("NAME").InnerText = bregOrder.PracticeName + "-" + bregOrder.PracticeLocationName;

                var hasShippingAddress =
                    (bregOrder.ShippingAddress != null)
                    && (bregOrder.ShippingAddress.BregOracleIDInvalid.HasValue
                        && (bregOrder.ShippingAddress.BregOracleIDInvalid.Value == false));

                var shippingPARTNRID = hasShippingAddress ? (bregOrder.ShippingAddress.BregOracleID ?? "Error") : "Error";
                if (shippingPARTNRID == "Error")
                    throw new Exception("Practice location is missing or has invalid Oracle ship-to ID");
                xnPartnerShipping.SelectSingleNode("PARTNRID").InnerText = shippingPARTNRID; // ship to address Code

                xnPartnerShipping.SelectNodes("ADDRESS/ADDRLINE").Item(0).InnerText = hasShippingAddress ? bregOrder.ShippingAddress.ShipAddress1 : string.Empty;
                xnPartnerShipping.SelectNodes("ADDRESS/ADDRLINE").Item(1).InnerText = hasShippingAddress ? bregOrder.ShippingAddress.ShipAddress2 : string.Empty;

                xnPartnerShipping.SelectSingleNode("ADDRESS/CITY").InnerText = hasShippingAddress ? bregOrder.ShippingAddress.ShipCity : string.Empty;
                xnPartnerShipping.SelectSingleNode("ADDRESS/REGION").InnerText = hasShippingAddress ? bregOrder.ShippingAddress.ShipState : string.Empty;
                xnPartnerShipping.SelectSingleNode("ADDRESS/POSTALCODE").InnerText = hasShippingAddress ? bregOrder.ShippingAddress.ShipZipCode : string.Empty;
                xnPartnerShipping.SelectSingleNode("ADDRESS/COUNTRY").InnerText = hasShippingAddress ? "USA" : string.Empty;

                xnPartnerShipping.SelectSingleNode("CONTACT/NAME").InnerText = hasShippingAddress ? bregOrder.ShippingAddress.ShipAttentionOf : string.Empty;
                #endregion Ship To

                #region Bill To
                XmlNode xnPartnerBilling = doc.SelectNodes("/SHOW_SHIPMENT_005/DATAAREA/SHOW_SHIPMENT/SHIPMENT/PARTNER").Item(1);
                xnPartnerBilling.SelectSingleNode("NAME").InnerText = bregOrder.PracticeName + "-" + bregOrder.PracticeLocationName;

                var hasBillingAddress =
                    (bregOrder.BillingAddress != null)
                    && (bregOrder.BillingAddress.BregOracleIDInvalid.HasValue
                        && (bregOrder.BillingAddress.BregOracleIDInvalid.Value == false));

                var billingPARTNRID = hasBillingAddress ? (bregOrder.BillingAddress.BregOracleID ?? "Error") : "Error";
                if (billingPARTNRID == "Error")
                    throw new Exception("Practice is missing or has invalid Oracle bill-to ID");
                xnPartnerBilling.SelectSingleNode("PARTNRID").InnerText = billingPARTNRID; // bill to address code

                xnPartnerBilling.SelectNodes("ADDRESS/ADDRLINE").Item(0).InnerText = hasBillingAddress ? bregOrder.BillingAddress.BillAddress1 : string.Empty;
                xnPartnerBilling.SelectNodes("ADDRESS/ADDRLINE").Item(1).InnerText = hasBillingAddress ? bregOrder.BillingAddress.BillAddress2 : string.Empty;

                xnPartnerBilling.SelectSingleNode("ADDRESS/CITY").InnerText = hasBillingAddress ? bregOrder.BillingAddress.BillCity : string.Empty;
                xnPartnerBilling.SelectSingleNode("ADDRESS/REGION").InnerText = hasBillingAddress ? bregOrder.BillingAddress.BillState : string.Empty;
                xnPartnerBilling.SelectSingleNode("ADDRESS/POSTALCODE").InnerText = hasBillingAddress ? bregOrder.BillingAddress.BillZipCode : string.Empty;
                xnPartnerBilling.SelectSingleNode("ADDRESS/COUNTRY").InnerText = hasBillingAddress ? "USA" : string.Empty;
                xnPartnerBilling.SelectSingleNode("ADDRESS/TELEPHONE").InnerText = hasBillingAddress ? bregOrder.BillingAddress.WorkPhone : string.Empty;

                xnPartnerBilling.SelectSingleNode("CONTACT/NAME").InnerText = hasBillingAddress ? bregOrder.BillingAddress.BillAttentionOf : string.Empty;
                #endregion Bill To

                #region Ship Service Level
                XmlNode xnShipServiceLevel = doc.SelectNodes("/SHOW_SHIPMENT_005/DATAAREA/SHOW_SHIPMENT/SHIPMENT/SHIPSRVLVL").Item(0);
                var shipServiceLevel = GetShipServiceLevelFromOrder(bregOrder);
                xnShipServiceLevel.InnerText = shipServiceLevel; // Ground, Air, 3-day, etc.
                #endregion

                #region LineItem
                XmlNode xnItem = doc.SelectNodes("/SHOW_SHIPMENT_005/DATAAREA/SHOW_SHIPMENT/SHIPMENT/SHIPITEM").Item(0);

                XmlNode xnShipment = doc.SelectNodes("/SHOW_SHIPMENT_005/DATAAREA/SHOW_SHIPMENT/SHIPMENT").Item(0);

                foreach (var item in bregOrder.LineItems)
                {
                    XmlNode xnItem1 = xnItem.CloneNode(true);
                    xnItem1.SelectSingleNode("QUANTITY/VALUE").InnerText = item.QuantityOrdered.ToString();
                    xnItem1.SelectSingleNode("QUANTITY/UOM").InnerText = item.Packaging;
                    {
                        // TEMP: remove 50. product codes from export
                        var x_code = item.Code;
                        if (x_code.StartsWith("50."))
                            x_code = x_code.Substring(3);
                        xnItem1.SelectSingleNode("ITEM").InnerText = x_code;
                    }
                    //xnItem1.SelectSingleNode("LOGOPART").InnerText = bregOrder.LogoPartNumber ?? string.Empty;
                    string logoPartNumber = "";
                    if (item.IsLogoPart.HasValue && item.IsLogoPart.Value == true)
                    {
                        logoPartNumber = string.IsNullOrEmpty(bregOrder.LogoPartNumber) ? string.Empty : string.Format("LOGOPART# {0}", bregOrder.LogoPartNumber);
                    }
                    xnItem1.SelectSingleNode("NOTES").InnerText = logoPartNumber;

                    xnShipment.AppendChild(xnItem1);
                    //doc.DocumentElement.AppendChild(xnItem1);
                }

                xnShipment.RemoveChild(xnItem);
                //doc.SelectNodes("/SHOW_SHIPMENT_005/DATAAREA/SHOW_SHIPMENT/SHIPMENT/SHIPITEM")
                //xnItem.RemoveAll();
                #endregion LineItem

                return doc;
            }
#if(DEBUG)
            catch (Exception ex)
            {
                if (System.Diagnostics.Debugger.IsAttached)
                    System.Diagnostics.Debugger.Break();
                return null;
            }
#else
            catch (Exception ex)
            {
                Console.WriteLine("Cannot create XML order: " + ex.ToString());
                return null;
            }
#endif

        }

        private static void WriteDateTimeToXML(XmlNode xnDateTime, DateTime dateTime)
        {
            xnDateTime.SelectSingleNode("YEAR").InnerText = dateTime.Year.ToString();
            xnDateTime.SelectSingleNode("MONTH").InnerText = string.Format("{0:00}", dateTime.Month);
            xnDateTime.SelectSingleNode("DAY").InnerText = string.Format("{0:00}", dateTime.Day);
            xnDateTime.SelectSingleNode("HOUR").InnerText = string.Format("{0:00}", dateTime.Hour);
            xnDateTime.SelectSingleNode("MINUTE").InnerText = string.Format("{0:00}", dateTime.Minute);
            xnDateTime.SelectSingleNode("SECOND").InnerText = string.Format("{0:00}", dateTime.Second);
            //xnDateTime.SelectSingleNode("TIMEZONE").InnerText = "+8";
        }

        public static bool CreateXMLOrder(int bregvisionOrderID)
        {
            return CreateXMLOrder(bregvisionOrderID, null);
        }

        public static bool CreateXMLOrder(int bregvisionOrderID, int? supplierOrderID)
        {

            BregOrder bregOrder = GetOrderInfo(bregvisionOrderID, supplierOrderID);
            if (bregOrder == null)
                return false;

            XmlDocument orderDoc = WriteXMLDocument(bregOrder);
            if (orderDoc == null)
                return false;

            if (!SaveXmlOrderDocument(orderDoc, bregOrder))
                return false;

            //if (!SendFTPOrder(orderDoc, bregOrder))
            //    return false;
            SendSFTPOrder(orderDoc, bregOrder);

            return true;

        }

        private static bool SaveXmlOrderDocument(XmlDocument orderDoc, BregOrder bregOrder)
        {
            try
            {
                string path = System.Web.Hosting.HostingEnvironment.MapPath("~/OracleOrders");
                string filename = "";

                if (!string.IsNullOrEmpty(path))
                {
                    filename = Path.Combine(path, bregOrder.XMLFilename);

                    orderDoc.Save(filename);
                }

                return true;
            }
#if(DEBUG)
            catch (Exception ex)
            {
                if (System.Diagnostics.Debugger.IsAttached)
                    System.Diagnostics.Debugger.Break();
                return false;
            }
#else
            catch
            {
                return false;
            }
#endif
        }

        public static bool SendFTPOrder(XmlDocument orderDoc, BregOrder bregOrder)
        {
            try
            {
                string ftpPath = ConfigurationManager.AppSettings["OracleOrderFTPLocation"];  //"localhost";
                string ftpUsername = ConfigurationManager.AppSettings["OracleOrderFTPUsername"];
                string ftpPassword = ConfigurationManager.AppSettings["OracleOrderFTPPassword"];

                // Get the object used to communicate with the server.
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}/{1}", ftpPath, bregOrder.XMLFilename));
                request.Method = WebRequestMethods.Ftp.UploadFile;

                // This example assumes the FTP site uses anonymous logon.
                request.Credentials = new NetworkCredential(ftpUsername, ftpPassword);

                request.KeepAlive = true;
                request.UseBinary = true;

                byte[] fileContents = Encoding.UTF8.GetBytes(orderDoc.OuterXml);

                request.ContentLength = fileContents.Length;

                Stream requestStream = request.GetRequestStream();
                requestStream.Write(fileContents, 0, fileContents.Length);
                requestStream.Close();

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                Console.WriteLine("Upload File Complete, status {0}", response.StatusDescription);

                response.Close();

                return true;
            }
#if(DEBUG)
            catch (Exception ex)
            {
                if (System.Diagnostics.Debugger.IsAttached)
                    System.Diagnostics.Debugger.Break();
                return false;
            }
#else
            catch
            {
                return false;
            }
#endif
        }

        public static bool SendSFTPOrder(XmlDocument orderDoc, BregOrder bregOrder)
        {
            try
            {
                var sftpPath = ConfigurationManager.AppSettings["OracleOrderFTPLocation"];
                var sftpHost = sftpPath.Split('/').ElementAt(0);
                var sftpTargetDir = sftpPath.Substring(sftpPath.IndexOf('/') + 1);

                var sftpUsername = ConfigurationManager.AppSettings["OracleOrderFTPUsername"];
                var sftpPassword = ConfigurationManager.AppSettings["OracleOrderFTPPassword"];

                var sftp_provider = new Tamir.SharpSsh.Sftp(sftpHost, sftpUsername, sftpPassword);

                var target_path = string.Format(@"/{0}/{1}", sftpTargetDir, bregOrder.XMLFilename);

                var source_path = System.IO.Path.GetTempFileName();
                File.WriteAllBytes(source_path, Encoding.UTF8.GetBytes(orderDoc.OuterXml));

                sftp_provider.Connect();
                sftp_provider.Put(source_path, target_path);
                sftp_provider.Close();

                File.Delete(source_path);

                Console.WriteLine("Upload File Complete");

                return true;
            }
#if(DEBUG)
            catch (Exception ex)
            {
                if (System.Diagnostics.Debugger.IsAttached)
                    System.Diagnostics.Debugger.Break();
                return false;
            }
#else
            catch (Exception ex)
            {
                throw new ApplicationException("Unable to upload XML order via SFTP.", ex);
            }
#endif
        }

        public static BregOrder GetOrderInfo(int bregVisionOrderID)
        {
            return GetOrderInfo(bregVisionOrderID, null);
        }

        public static BregOrder GetOrderInfo(int bregVisionOrderID, int? supplierOrderID)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                if (supplierOrderID.HasValue)
                {
                    // NOTE: we want to support only master catalog items for vision-to-oracle
                    // because everything in a third party supplier is editable and could break
                    // the interface.
                    var has_mastercatalog_breg =
                        visionDB.BregVisionOrders
                        .Where(x => x.BregVisionOrderID == bregVisionOrderID)
                        .Any(x => x.SupplierOrders.Any(y => (y.SupplierOrderID == supplierOrderID.Value) && y.PracticeCatalogSupplierBrand.MasterCatalogSupplier.SupplierName.ToLower().Contains("breg")));
                    if (!has_mastercatalog_breg)
                        return null;
                }

                var orderQuery = (IEnumerable<usp_GetBVOSupplierInfoForFaxEmail_NewResult>)null;
                if (supplierOrderID.HasValue)
                {
                    orderQuery = from so in visionDB.usp_GetBVOSupplierInfoForFaxEmail_New(bregVisionOrderID)
                                 where so.SupplierName.ToLower().Contains("breg") && (so.SupplierOrderID == supplierOrderID)
                                 select so;
                }
                else
                {
                    orderQuery = from so in visionDB.usp_GetBVOSupplierInfoForFaxEmail_New(bregVisionOrderID)
                                 where so.SupplierName.ToLower().Equals("breg")
                                 select so;
                }

                var order = (from so in orderQuery
                             select new BregOrder
                             {
                                 AccountCode = so.AccountCode,
                                 CreatedDate = so.CreatedDate,
                                 CustomPurchaseOrderCode = so.CustomPurchaseOrderCode,
                                 EmailAddress = so.EmailAddress,
                                 Fax = so.Fax,
                                 LogoPartNumber = so.LogoPartNumber,
                                 PhoneWork = so.PhoneWork,
                                 PracticeLocationID = so.PracticeLocationID,
                                 PurchaseOrder = so.PurchaseOrder,
                                 ShippingTypeName = so.ShippingTypeName,
                                 SupplierOrderID = so.SupplierOrderID,
                                 SupplierName = so.SupplierName,
                                 Total = so.Total
                             }).FirstOrDefault();

                if (order != null)
                {
                    order.BregVisionOrderId = bregVisionOrderID;

                    var practiceQuery = from pr in visionDB.PracticeLocations
                                        join p in visionDB.Practices on pr.PracticeID equals p.PracticeID
                                        where pr.PracticeLocationID == order.PracticeLocationID
                                        select new
                                        {
                                            PracticeName = p.PracticeName,
                                            PracticeLocationName = pr.Name
                                        };

                    var practiceInfo = practiceQuery.FirstOrDefault();
                    order.PracticeName = practiceInfo.PracticeName;
                    order.PracticeLocationName = practiceInfo.PracticeLocationName;

                    var billingAddressQuery = from ba in visionDB.usp_GetBillingAddressByPracticeLocation(order.PracticeLocationID)
                                              select ba;

                    order.BillingAddress = billingAddressQuery.FirstOrDefault();

                    var shippingAddressQuery = from sa in visionDB.usp_GetShippingAddressByPracticeLocation(order.PracticeLocationID)
                                               select sa;

                    order.ShippingAddress = shippingAddressQuery.FirstOrDefault();

                    var lineItemsQuery = from li in visionDB.usp_GetBVOSupplierLineItemInfoForFaxEmail_New(order.SupplierOrderID)
                                         select li;

                    order.LineItems = lineItemsQuery.ToList<usp_GetBVOSupplierLineItemInfoForFaxEmail_NewResult>(); ;
                }

                return order;
            }

        }


        public static XmlDocument LoadXMLDoc()
        {
            XmlDocument doc = new XmlDocument();


            Stream s = Assembly.GetAssembly(typeof(BregOracleOrder)).GetManifestResourceStream("BregOracle.OracleTemplate.xml");
            StreamReader sr = new StreamReader(s);

            string file = sr.ReadToEnd();

            doc.LoadXml(file);

            return doc;
        }
    }
}
