﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data.Linq;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using ClassLibrary.DAL;

namespace BregOracle
{
    public class BregOrder : usp_GetBVOSupplierInfoForFaxEmail_NewResult
    {
        public int BregVisionOrderId { get; set; }

        public string PoNumber
        {
            get
            {
                return (string.IsNullOrEmpty(this.CustomPurchaseOrderCode) ? this.PurchaseOrder.ToString() : this.CustomPurchaseOrderCode).ToUpper();
            }
        }

        public string PracticeLocationName { get; set; }

        public string PracticeName { get; set; }

        public usp_GetBillingAddressByPracticeLocationResult BillingAddress { get; set; }

        public usp_GetShippingAddressByPracticeLocationResult ShippingAddress { get; set; }

        public List<usp_GetBVOSupplierLineItemInfoForFaxEmail_NewResult> LineItems { get; set; }

        public string UniqueOrderID
        {
            get
            {
                return string.Format(@"{0}_{1}", this.BregVisionOrderId.ToString(), this.SupplierOrderID.ToString());
            }
        }

        public string XMLFilename
        {
            get
            {
                return string.Format("Order{0}.xml", this.UniqueOrderID);
            }
        }
    }
}
