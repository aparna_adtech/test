﻿using System.ServiceModel;

namespace DocumentService
{
    [ServiceContract]
    public interface ICreatePdf
    {

        [OperationContract]
        void CreatePdf(string html, string path);

        [OperationContract]
        void CreatePdfWithAttachments(string html, string path, byte[][] documents);
    }
}