﻿namespace DocumentService
{
    public class DocumentService : ICreatePdf
    {
        void ICreatePdf.CreatePdf(string html, string path)
        {
            DocumentGenerator.CreatePdf(html, path);
        }

        void ICreatePdf.CreatePdfWithAttachments(string html, string path, byte[][] documents)
        {
            DocumentGenerator.CreatePdfWithAttachments(html, path, documents);      
        }
    }
}