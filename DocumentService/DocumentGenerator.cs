﻿using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System;
using System.IO;
using TuesPechkin;

namespace DocumentService
{
    public static class DocumentGenerator
    {

        #region application settings
        private static double LeftMarginSetting
        {
            get { return Convert.ToDouble(float.Parse(System.Configuration.ConfigurationManager.AppSettings["sideMargins"] ?? "0.5f")); }
        }
        private static double RightMarginSetting
        {
            get { return LeftMarginSetting; }
        }
        private static double TopMarginSetting
        {
            get { return Convert.ToDouble(float.Parse(System.Configuration.ConfigurationManager.AppSettings["topBottomMargins"] ?? "0.5f")); }
        }
        private static double BottomMarginSetting
        {
            get { return TopMarginSetting; }
        }
        #endregion


        #region Public Methods
        public static bool CreatePdf(string html, string path)
        {
            var pdf = GeneratePDFFromHTML(html);
            if (pdf == null || pdf.Length == 0)
            {
                return false;
            }
            WritePdfToPath(pdf, path);
            return true;
        }

        public static bool CreatePdfWithAttachments(string html, string path, byte[][] documents)
        {
            var pdf = GeneratePDFFromHTML(html);
            if(pdf == null || pdf.Length == 0)
            {
                return false;
            }

            var concatenatedPdf = MergeDocuments(pdf, documents);
            if (concatenatedPdf == null || concatenatedPdf.Length == 0)
            {
                return false;
            }
            WritePdfToPath(concatenatedPdf, path);
            return true;
        }
        #endregion Public Methods


        #region Private Methods
        private static IConverter TuesPechkinConverter =
            new ThreadSafeConverter(
                new RemotingToolset<PdfToolset>(
                    new WinAnyCPUEmbeddedDeployment(
                        new TempFolderDeployment())));

        private static byte[] GeneratePDFFromHTML(string html)
        {
            //Config
            var globalSettings = new TuesPechkin.GlobalSettings()
            {
                PaperSize = System.Drawing.Printing.PaperKind.Letter,
                OutputFormat = TuesPechkin.GlobalSettings.DocumentOutputFormat.PDF,
                ProduceOutline = false,
                Margins =
                {
                    Bottom = BottomMarginSetting,
                    Left = LeftMarginSetting,
                    Right = RightMarginSetting,
                    Top = TopMarginSetting,
                    Unit = TuesPechkin.Unit.Inches,
                },
            };

            //Init Document Object/Factory
            var document =
                new TuesPechkin.HtmlToPdfDocument
                {
                    GlobalSettings = globalSettings,
                    Objects = { new TuesPechkin.ObjectSettings { HtmlText = html }, },
                };

            //Generate
            return TuesPechkinConverter.Convert(document);
        }

        private static byte[] MergeDocuments(byte[] orig, byte[][] files)
        {
            PdfDocument outputDocument = new PdfDocument();
            var i = -1;
            do
            {
                var file = i == -1 ? orig : files[i];
                using (var stream = new MemoryStream(file))
                {
                    var inputDocument = PdfReader.Open(stream, PdfDocumentOpenMode.Import);
                    int count = inputDocument.PageCount;
                    for (int idx = 0; idx < count; idx++)
                    {
                        // Get the page from the external document...
                        PdfPage page = inputDocument.Pages[idx];
                        // ...and add it to the output document.
                        outputDocument.AddPage(page);
                    }
                }
            }
            while (++i < files.Length);

            using (var stream = new MemoryStream())
            {
                outputDocument.Save(stream);
                return stream.ToArray();
            }

        }

        private static void WritePdfToPath(byte[] pdf, string path)
        {
            using (var fileStream = new FileStream(path, FileMode.Create))
            {
                fileStream.Write(pdf, 0, pdf.Length);
            }
        }
        #endregion Private Methods


    }
}