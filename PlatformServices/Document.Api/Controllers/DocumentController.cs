﻿namespace com.breg.vision.Document.Service.Controllers
{
    using com.breg.vision.Handoff.CryptoLib.AES256;
    using Handoff.ClientModels;
    using Handoff.ClientModels.Constants;
    using Integration.VisionBcsModels;
    using MongoRepository.Abstractions;
    using Service.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http;

    public class DocumentController : ApiController
    {
        
        private readonly IRepository<DispensementDocumentModel> repository;
      
        public DocumentController()
        {
            repository = new MongoRepository.MongoRepository<DispensementDocumentModel>();
        }

        public IHttpActionResult Post([FromBody]DocumentPayloadModel data)
        {
            try
            {
                // Compress and Encrypt the document; then save it to Mongo File storage DB.
                var model = new DispensementDocumentModel()
                {
                    Data = data.Document,
                    CorelationId = data.CorelationId,
                    DocumentType = data.DocumentType.ToString(), 
                    AssociatedEntity = data.AssociatedEntity.ToString(), 
                    AssociatedEntityId = data.AssociatedEntityId, 
                    ReferenceSystem = data.ReferenceSystem.ToString(),
                    ReferenceEntity = data.ReferenceEntity.ToString(), 
                    ReferenceEntityID = data.ReferenceEntityID
                };
                Encrypter.TryEncrypt(model);
                repository.Add(model);
                data.DocumentId = model.Id;
                return Ok<DocumentPayloadModel>(data);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Get(AssociatedEntityTypes entityType, int entityId, DocumentTypes docType, ReferenceSystemTypes documentSource)
        {
            // Compress and send the file to BCS.
            IEnumerable<byte[]> response = null;
            try
            {
                var data = repository.GetAll(x => x.AssociatedEntityId == entityId && x.AssociatedEntity == entityType.ToString() &&
                                                 x.DocumentType == docType.ToString() && x.ReferenceSystem == documentSource.ToString());
                if(data != null && data.Count > 0)
                {
                    response = data.Select(x => Convert.FromBase64String(x.Data));
                }
            }
            catch(Exception ex)
            {

            }
            Decrypter.TryDecrypt(response);
            return Ok<IEnumerable<byte[]>>(response);
        }

        public IHttpActionResult Put([FromBody]BcsClaimSubmission data)
        {
            if (data != null && data.DocumentIDs != null && data.DocumentIDs.Length > 0)
            {
                foreach (var documentId in data.DocumentIDs)
                {
                    var item = repository.Get(x => x.Id == documentId);
                    if (item == null)
                    {
                        return BadRequest();
                    }

                    item.AssociatedEntity = ((AssociatedEntityTypes)data.SourceEntity).ToString();
                    item.AssociatedEntityId = data.SourceEntityID;
                    repository.Update(item);
                }
            }
            else
            {
                return BadRequest();
            }
            return Ok<bool>(true);
        }
    }
}