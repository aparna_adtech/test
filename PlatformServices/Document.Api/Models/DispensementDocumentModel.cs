﻿using com.breg.vision.Handoff.ClientModels;
using com.breg.vision.Handoff.ClientModels.Constants;
using com.breg.vision.MongoDataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.breg.vision.Document.Service.Models
{
    public class DispensementDocumentModel : Entity
    {
        public string Data { get; set; }

        public string CorelationId { get; set; }

        public string DocumentType { get; set; }

        public string AssociatedEntity { get; set; }
        public int? AssociatedEntityId { get; set; } 

        public string ReferenceSystem { get; set; } 
        public string ReferenceEntity { get; set; }
        public string ReferenceEntityID { get; set; } 
    }
}