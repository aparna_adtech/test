GO
-- =============================================
-- Author:	 Basil Juan
-- Create date: 12/05/2014
-- Description:	Used in Product Dispensement.  Built off of usp_GetCustomBraceOrdersforDispense_Detail1Level, modified
--	 to accommodate split code changes.
--
--exec usp_GetCustomBraceOrdersforDispense_Detail1Level_20141205 3, '', ''
--exec usp_GetCustomBraceOrdersforDispense_Detail1Level_20141205 3, 'PONumber', '109001'
--exec usp_GetCustomBraceOrdersforDispense_Detail1Level_20141205 3, 'PatientID', 'greg'
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetCustomBraceOrdersforDispense_Detail1Level_20141205] --162, 15  -- Prod 222, 3  -- BimsDev 169, 3   -- BimsDev 162, 15

	@PracticeLocationID int,
	@SearchCriteria varchar(50), -- we only care if this is 'PONumber' OR 'PatientID' but could also be 'Browse', 'All', '',
	@SearchText  varchar(50)

AS
BEGIN

	SET NOCOUNT ON;

	Declare @BregVisionOrderID int
	Declare @PatientID varchar(50)

	set @bregVisionOrderID = null
	Set @PatientID = null

	if (@SearchCriteria='PONumber' AND ISNUMERIC(@SearchText) = 1)
	begin
	  set @BregVisionOrderID = (Cast( @SearchText as int) - 100000 )  --need to convert this to a number and do any 100000 suberaction necessary
	end
	else if(@SearchCriteria='PatientID')
	begin
	  set @PatientID = '%' + @SearchText + '%'
	end

	select
		BVO.BregVisionOrderID,
		PI.ProductInventoryID,
		MCP.Code,
		MCP.LeftRightSide as Side,
		MCP.Size,
		MCP.Gender,
		ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityOnHand,
		SOLI.ActualWholesaleCost,
		MCP.ShortName as ProductName,
		PCP.IsSplitCode,
		PCP.IsAlwaysOffTheShelf,
		MCP.IsCustomBrace
		, PCP.Mod1
		, PCP.Mod2
		, PCP.Mod3

	from SupplierOrderLineItem SOLI
	inner join SupplierOrder SO on SOLI.SupplierOrderID = SO.SupplierOrderID
	inner join BregVisionOrder BVO on SO.BregVisionOrderID= BVO.BregVisionOrderID
	inner join PracticeCatalogPRoduct PCP
		on SOLI.PracticeCatalogProductID = 	PCP.PracticeCatalogPRoductID
	inner join MasterCatalogProduct MCP
		on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
	inner join ProductInventory PI
		on SOLI.PracticeCatalogProductID = PI.PracticeCatalogProductID
	inner join SupplierOrderLineItemStatus SOLIS
		on SOLI.SupplierOrderLineItemStatusID = SOLIS.SupplierOrderLineItemStatusID
	INNER JOIN ShoppingCartCustomBrace SCCB
		ON SCCB.BregVisionOrderID = BVO.BregVisionOrderID
	LEFT JOIN (SELECT SupplierOrderLineItemID, SUM(Quantity) AS QuantityCheckedIn
				FROM dbo.ProductInventory_OrderCheckIn
				WHERE IsActive = 1
				GROUP BY SupplierOrderLineItemID)
				AS SubPIOCI
					ON SOLI.SupplierOrderLineItemID = SubPIOCI.SupplierOrderLineItemID
	where
		PI.PracticeLocationID = @PracticeLocationID
		AND BVO.BregVisionOrderStatusID IN (2)
		AND BVO.isactive = 1
		AND BVO.PracticeLocationID = @PracticeLocationID
		and SO.SupplierOrderStatusID in (2)
		and SO.isactive = 1
		AND PCP.IsThirdPartyProduct = 0		-- MasterCatalogProduct
		--and PCP.isactive = 1
		--and MCP.isactive = 1
		AND SCCB.DispenseID is null

		--this, in conjunction with the definition of these variables above, handles custom search
		and SCCB.BregVisionOrderID = Coalesce(@BregVisionOrderID, SCCB.BregVisionOrderID)
		AND ((@PatientID Is Not NULL) AND  (SCCB.PatientName in(Select Distinct PatientName from ShoppingCartCustomBrace where PatientName Like @PatientID)) OR (@PatientID IS NULL))
	ORDER BY
		ProductName,
		Code,
		Side


END
GO

-----
GO
-- =============================================
-- Author:		Greg Ross
-- Create date: Sept. 24, 2007
-- Description:	Gets all data necessary to prioduce Dispense Receipt
-- Modifications:  20071002 JB modify for Dispense Receipts without a Physician.
--
--		--20071116 Add Size -- may need to remove Packaging
-- usp_GetDispenseReceipt 670828
--		--20110623 Added SupplierName (by hchan@breg.com)
		--20120104 Added Dispense Signature (by Michael_sneen@yahoo.com)
		--20141218 Altered HCPCSString to use DispenseQueue's HCPCs, if present (bjuan@breg.com)
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetDispenseReceipt]  -- 46
	@DispenseID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		select
			D.PatientCode,
			ISNULL( D.ReceiptCode, '')	AS ReceiptCode,  -- D.ReceiptCode
			D.Total as Grandtotal,
			D.PatientFirstName AS PatientFirstName,
			D.PatientLastName AS PatientLastName,
			D.Note,
			ISNULL( C.Title, '')	AS Title,
			ISNULL( C.FirstName, '') AS FirstName,
			ISNULL( C.LastName, '')  AS LastName,
			ISNULL( C.Suffix, '')	 AS Suffix,
			SupplierName = (
				   Select mcs1.SupplierShortName from MasterCatalogSupplier mcs1
				   join MasterCatalogCategory mcc on mcs1.MasterCatalogSupplierID = mcc.MasterCatalogSupplierID
				   join MasterCatalogSubCategory mcsc on mcc.MasterCatalogCategoryID = mcsc.MasterCatalogCategoryID
				   where mcsc.MasterCatalogSubCategoryID = MCP.MasterCatalogSubCategoryID
				),
			MCP.ShortName,
			MCP.Code,
			MCP.IsCustomBrace,
			MCP.Packaging,

			--MCP.LeftRightSide as Side,  --20091026 MWS Replaced MCP Side with User Chosen Side
			DD.LRModifier as Side,
			MCP.Size,						--20071116 Add Size -- may need to remove Packaging


			ISNULL( MCP.Color, '') AS Color,
			MCP.Gender,
			DD.ICD9Code,
			DD.ActualChargeBilled as Total,
			DD.DMEDeposit,
			DD.Quantity,
			DD.LineTotal,
			COALESCE(DQ.HCPCs, dbo.ConcatHCPCS(PCP.PracticeCatalogProductID)) AS HCPCSString, -- JB 20071004 Added Column for UDF Concat of HCPCS
																							  -- BJ 20141218 Altered Column to use DispenseQueue HCPCs, if present
			ISNULL( DS.PhysicianSigned , 0 ) as PhysicianSigned,
			CASE WHEN DS.PhysicianSigned=1 THEN DS.PhysicianSignedDate ELSE null END as PhysicianSignedDate,
			DD.IsCustomFit,
			DD.FitterID,
			DD.DispenseDetailID

		from DispenseDetail DD
		inner join dispense D on D.DispenseID= DD.DispenseID
		inner join PracticeCatalogProduct PCP on DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID
		inner join MasterCatalogProduct MCP on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
		left join MasterCatalogSupplier MCS on MCP.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
		left join Physician P on DD.PhysicianID = P.PhysicianID
		left join Contact C on P.ContactID = C.ContactID
		left join DispenseQueue DQ on DD.DispenseQueueID = DQ.DispenseQueueID
		Left Join DispenseSignature DS on DQ.DispenseSignatureID = DS.DispenseSignatureID
		where D.DispenseID=@DispenseID
		and D.isactive=1
		and DD.isactive=1
		--and P.isactive=1
		--and C.isactive = 1
		and PCP.isactive=1
		and MCP.isactive=1
		and ((DS.IsActive Is NULL) OR (DS.IsActive Is NOT NULL and DS.IsActive=1))

	UNION ALL

			select
			D.PatientCode,
			ISNULL( D.ReceiptCode, '')	AS ReceiptCode,  -- D.ReceiptCode
			D.Total as Grandtotal,
			D.PatientFirstName AS PatientFirstName,
			D.PatientLastName AS PatientLastName,
			D.Note,
			ISNULL( C.Title, '')	AS Title,
			ISNULL( C.FirstName, '') AS FirstName,
			ISNULL( C.LastName, '')  AS LastName,
			ISNULL( C.Suffix, '')	 AS Suffix,
			PCSB.SupplierName,
			TPP.ShortName,
			TPP.Code,
			CAST(0 as bit) AS IsCustomBrace,
			TPP.Packaging,

			--TPP.LeftRightSide as Side,  --20091026 MWS Replaced TPP Side with User Chosen Side
			DD.LRModifier as Side,
			TPP.Size,					--20071116 Add Size -- may need to remove Packaging

			ISNULL(TPP.Color, '') AS Color,
			TPP.Gender,
			DD.ICD9Code,
			DD.ActualChargeBilled as Total,
			DD.DMEDeposit,
			DD.Quantity,
			DD.LineTotal,
			COALESCE(DQ.HCPCs, dbo.ConcatHCPCS(PCP.PracticeCatalogProductID)) AS HCPCSString, -- JB 20071004 Added Column for UDF Concat of HCPCS
																							  -- BJ 20141218 Added
			ISNULL( DS.PhysicianSigned , 0 ) as PhysicianSigned,
			CASE WHEN DS.PhysicianSigned=1 THEN DS.PhysicianSignedDate ELSE null END as PhysicianSignedDate,
			DD.IsCustomFit,
			DD.FitterID,
			DD.DispenseDetailID
		from DispenseDetail DD
		inner join dispense D on D.DispenseID= DD.DispenseID
		inner join PracticeCatalogProduct PCP on DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID
		inner join ThirdPartyProduct TPP on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
		left join PracticeCatalogSupplierBrand PCSB on TPP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		left join Physician P on DD.PhysicianID = P.PhysicianID
		left join Contact C on P.ContactID = C.ContactID
		left join DispenseQueue DQ on DD.DispenseQueueID = DQ.DispenseQueueID
		Left Join DispenseSignature DS on DQ.DispenseSignatureID = DS.DispenseSignatureID
		where D.DispenseID=@DispenseID
		and D.isactive=1
		and DD.isactive=1
		--and P.isactive=1
		--and C.isactive = 1
		and PCP.isactive=1
		and TPP.isactive=1
		and ((DS.IsActive Is NULL) OR (DS.IsActive Is NOT NULL and DS.IsActive=1))

	--) AS Sub

END


SET ANSI_NULLS ON

---------------------------
GO
-- =============================================
-- Author:		<Greg Ross>
-- Create date: <07/31/07>
-- Description:	<Used to get all data for the shopping cart>
--
--  Modification:  20071011 JB  Added Third Party Supplier Information.
--    check this for Cost
-- exec usp_GetShoppingCart 3
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetShoppingCart]   --18
	@PracticeLocationID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		  SUB.Supplier
		, SUB.Category
		, SUB.Product
		, SUB.Code
		, SUB.Packaging
		, SUB.Side
		, SUB.Size
		, SUB.Gender
		, cast(SUB.IsCustomBrace as bit) as IsCustomBrace
		, cast(SUB.IsCustomBraceAccessory as bit) as IsCustomBraceAccessory
		, SUB.Quantity
		, SUB.Cost           -- check this
		, CAST( (SUB.Quantity * SUB.Cost) AS DECIMAL(9, 2) )  AS LineTotal
		--, (SUB.Quantity * SUB.Cost) AS TotalCost
		, SUB.PracticeCatalogProductID
		, SUB.ShoppingCartItemID
		, SUB.IsThirdPartyProduct
		, SUB.Sequence
		, SUB.ParLevel
		, SUB.QuantityOnHand
		, SUB.ShoppingCartID
		, SUB.MasterCatalogProductID
		, SUB.IsConsignment
		, SUB.IsLogoPart
		, Cast(SUB.McpIsLogoAblePart as bit) as McpIsLogoAblePart

	FROM
			(
				SELECT  MCS.[SupplierName] AS Supplier,
						MCC.[Name] AS Category,
						MCP.[Name] AS Product,
						MCP.[Code] AS Code,
						MCP.[Packaging] AS Packaging,
						MCP.[LeftRightSide] AS Side,
						MCP.[Size] AS Size,
						MCP.[Gender] AS Gender,
						MCP.[IsCustomBrace] AS IsCustomBrace,
						MCP.IsCustomBraceAccessory,
						SCI.[Quantity] AS Quantity,
						SCI.[ActualWholesaleCost] AS Cost,
						PCP.[PracticeCatalogproductid] AS PracticeCatalogProductID,
						SCI.[ShoppingCartItemID] AS ShoppingCartItemID,
						PCP.IsThirdPartyProduct,
						ISNULL(PCSB.Sequence, 0) AS Sequence,
						ISNULL(PRI.ParLevel, 0) AS ParLevel,
						ISNULL(PRI.QuantityOnHandPerSystem, 0) AS QuantityOnHand,
						SC.ShoppingCartID as ShoppingCartID,
						MCP.MasterCatalogProductID as MasterCatalogProductID,
						SC.IsConsignment,
						SCI.IsLogoPart,
						(PRI.IsLogoAblePart & PCP.IsLogoAblePart & MCP.IsLogoAblePart) as McpIsLogoAblePart

				FROM
						[ShoppingCart] SC

						INNER JOIN shoppingcartitem SCI
							ON SC.[ShoppingCartID] = SCI.[ShoppingCartID]

						INNER JOIN [PracticeCatalogProduct] PCP
							ON SCI.[PracticeCatalogProductID] = PCP.[PracticeCatalogProductID]

						INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
							ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

						INNER JOIN [MasterCatalogProduct] MCP
							ON PCP.[MasterCatalogProductID] = MCP.[MasterCatalogProductID]

						INNER JOIN [MasterCatalogSubCategory] MCSC
							ON MCP.[MasterCatalogSubCategoryID] = MCSC.[MasterCatalogSubCategoryID]

						INNER JOIN [MasterCatalogCategory] MCC
							ON MCSC.[MasterCatalogCategoryID] = MCC.[MasterCatalogCategoryID]

						INNER JOIN [MasterCatalogSupplier] MCS
							ON MCC.[MasterCatalogSupplierID] = MCS.[MasterCatalogSupplierID]
						Left Outer JOIN [ProductInventory] PRI
							ON PRI.PracticeCatalogProductID = SCI.PracticeCatalogProductID AND PRI.PracticeLocationID = @practicelocationID

				WHERE
						SC.[ShoppingCartStatusID] = 0
						AND SCI.[ShoppingCartItemStatusID] = 0
						AND SC.[IsActive] = 1
						AND SCI.[IsActive] = 1
						AND pcp.[IsActive] = 1
						AND mcsc.[IsActive] = 1
						AND mcc.[IsActive] = 1
						AND mcs.[IsActive] = 1
						AND PCSB.IsActive = 1
						AND SC.[PracticeLocationID] = @practicelocationID
						AND PCSB.IsThirdPartySupplier  = 0
						AND PRI.IsActive = 1

			UNION

				SELECT
						PCSB.[SupplierName]AS Supplier,
						'NA' AS Category,
						TPP.[Name] AS Product,
						TPP.[Code]AS Code,
						TPP.[Packaging] AS Packaging,
						TPP.[LeftRightSide] AS Side,
						TPP.[Size] AS Size,
						TPP.[Gender] AS Gender,
						IsCustomBrace = 0,
						IsCustomBraceAccessory = 0,
						SCI.[Quantity] AS Quantity,
						SCI.[ActualWholesaleCost] AS Cost,
						PCP.[PracticeCatalogproductid] AS PracticeCatalogProductID,
						SCI.[ShoppingCartItemID] AS ShoppingCartItemID,
						PCP.IsThirdPartyProduct,
						ISNULL(PCSB.Sequence, 0) AS Sequence,
						ISNULL(PRI.ParLevel, 0) AS ParLevel,
						ISNULL(PRI.QuantityOnHandPerSystem, 0) AS QuantityOnHand,
						SC.ShoppingCartID as ShoppingCartID,
						0 as MasterCatalogProductID,
						SC.IsConsignment,
						0 as IsLogoPart,
						0 AS McpIsLogoAblePart
				FROM
						[ShoppingCart] SC

						INNER JOIN shoppingcartitem SCI
							ON SC.[ShoppingCartID] = SCI.[ShoppingCartID]

						INNER JOIN [PracticeCatalogProduct] PCP
							ON SCI.[PracticeCatalogProductID] = PCP.[PracticeCatalogProductID]

						INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
							ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

						INNER JOIN [ThirdPartyProduct] TPP
							ON PCP.[ThirdPartyProductID] = TPP.[ThirdPartyProductID]
						Left Outer JOIN [ProductInventory] PRI
							ON PRI.PracticeCatalogProductID = SCI.PracticeCatalogProductID AND PRI.PracticeLocationID = @practicelocationID
				WHERE
						SC.[ShoppingCartStatusID] = 0
						AND SCI.[ShoppingCartItemStatusID] = 0
						AND SC.[IsActive] = 1
						AND SCI.[IsActive] = 1

						AND pcp.IsActive = 1
						AND PCSB.IsActive = 1
						AND TPP.IsActive = 1
						AND PCSB.IsThirdPartySupplier  = 1

						AND SC.[PracticeLocationID] = @practicelocationID
						AND PRI.IsActive = 1

				) AS SUB

			ORDER BY
				SUB.IsThirdPartyProduct
				, SUB.Sequence
				, SUB.Supplier


END
GO


------------------------------------

GO
/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocation_Insert

   Description:  Inserts a record into table PracticeLocation.

   AUTHOR:       John Bongiorni 7/23/2007 11:05:42 AM

   --Test Script:
   DECLARE	@return_value int,
		@PracticeLocationID int

	EXEC	@return_value = [dbo].[usp_PracticeLocation_Insert]
			@PracticeLocationID = @PracticeLocationID OUTPUT,
			@PracticeID = 12,
			@Name = N'Clairemont',
			@IsPrimaryLocation = 1,
			@CreatedUserID = 0

	SELECT	@PracticeLocationID as N'@PracticeLocationID'

	SELECT	'Return Value' = @return_value
	GO

   Modifications:  20071017 John Bongiorni
			-- If first practice, then set it as the primary location.
   ------------------------------------------------------------ */


ALTER PROCEDURE [dbo].[usp_PracticeLocation_Insert]
(
      @PracticeLocationID				INT			= NULL OUTPUT
    , @PracticeID						INT
    , @Name								VARCHAR(50)
 	  , @GeneralLedgerNumber				VARCHAR(50)
    , @IsPrimaryLocation				BIT			= 0
--    , @EmailForOrderApproval			VARCHAR(100)
--    , @EmailForLowInventoryAlerts		VARCHAR(100)
--    , @IsEmailForLowInventoryAlertsOn	BIT
    , @CreatedUserID					INT
	, @IsHCPSignatureRequired			BIT
)
AS
BEGIN

DECLARE		@TransactionCountOnEntry    INT       -- Transaction Count before the transaction begins
DECLARE		@Err             			INT        --  holds the @@ERROR code returned by SQL Server
DECLARE		@RecordCount				INT
DECLARE     @PracticeLocationCount		INT

SET @RecordCount = 0




		-- Count active location for the practice
		SELECT @PracticeLocationCount = COUNT(1)
		FROM PracticeLocation AS PL
		INNER JOIN dbo.Practice AS P
			ON P.PracticeID = PL.PracticeID
		WHERE P.PracticeID = @PracticeID
		AND PL.IsActive = 1
		AND PL.IsPrimaryLocation = 1



		SELECT @RecordCount = COUNT(1)
		FROM dbo.PracticeLocation AS pl
		WHERE pl.PracticeID = @PracticeID
			AND pl.[Name] = @Name
			AND pl.IsActive = 1



		IF (@RecordCount = 0)
			BEGIN

				-- If first practice, then set it as the primary location.
				IF (@PracticeLocationCount = 0)
				BEGIN
					SET @IsPrimaryLocation = 1
				END

				INSERT
				INTO dbo.PracticeLocation
				(
					  PracticeID
					, Name
					, GeneralLedgerNumber
					, IsPrimaryLocation
		--			, EmailForOrderApproval
		--			, EmailForLowInventoryAlerts
		--			, IsEmailForLowInventoryAlertsOn
					, CreatedUserID
					, IsActive
					, IsHcpSignatureRequired
				)
				VALUES
				(
					  @PracticeID
					, @Name
					, @GeneralLedgerNumber
					, @IsPrimaryLocation
		--			, @EmailForOrderApproval
		--			, @EmailForLowInventoryAlerts
		--			, @IsEmailForLowInventoryAlertsOn
					, @CreatedUserID
					, 1
					, @IsHCPSignatureRequired
				)

	--			SET @Err = @@ERROR
	--
	--		END
	--		IF @Err = 0
	--		BEGIN

				SELECT @PracticeLocationID = SCOPE_IDENTITY()

	--			SET @Err = @@ERROR

	--		END
	--		IF @Err = 0
	--		BEGIN
			IF ( @IsPrimaryLocation = 1 )
			BEGIN

				EXEC usp_PracticeLocation_Set_Primary_Location @PracticeLocationID = @PracticeLocationID

			END
	--		SET @Err = @@ERROR
	--		END

		END

	ELSE
		BEGIN

			SELECT @PracticeLocationID = pl.PracticeLocationID
			FROM dbo.PracticeLocation AS pl
			WHERE pl.PracticeID = @PracticeID
				AND pl.NAME = @Name
				AND pl.IsActive = 1

		END




--	IF @@TranCount > @TransactionCountOnEntry
--	BEGIN
--
--		IF @Err = 0
--			BEGIN
--				COMMIT TRANSACTION
--				RETURN @Err
--			END
--		ELSE
--			BEGIN
--				ROLLBACK TRANSACTION
--				RETURN @Err
--				--  Add any database logging here
--			END
END
GO

-------------------------------------------
GO
-- =============================================
-- Author:		Greg Ross
-- Create date: 5/5/08
-- Description:	Used to get all information so that user can choose a historical dispensement to view or print
-- Modified Nov 12 to add third party products to the data - GR
--usp_ReceiptHistory_Detail 3, 'PhysicianName', 'Alleyne'
--usp_ReceiptHistory_Detail 3, 'PhysicianName', 'kane'
--usp_ReceiptHistory_Detail 3, 'PatientCode', 'Sneen'
-- =============================================
ALTER PROCEDURE [dbo].[usp_ReceiptHistory_Detail]
	-- Add the parameters for the stored procedure here
	@PracticeLocationID int
	, @SearchCriteria varchar(50)
	, @SearchText  varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		/*These variables are used in conjunction with the coalesce statements in the where clause.
	each of these variables will be null except one is @SearchCriteria matches.*/
	Declare @PatientCode varchar(50)			--NULL   These default to null
	Declare @PhysicianName varchar(50)

	if @SearchCriteria='PatientCode'
		Set @PatientCode = '%' +  @SearchText + '%'
	if @SearchCriteria='PhysicianName'
		Set @PhysicianName = '%' +  @SearchText + '%'

Select
	SUB.DispenseID
	,SUB.DispenseDetailID
	,SUB.DDDispenseID
	,SUB.ActualChargeBilled
	,SUB.DMEDeposit
	,SUB.Quantity
	,cast (SUB.ABNForm as bit) as ABNForm
	,SUB.ABNType
	,SUB.LineTotal
	,SUB.PhysicianName
	,SUB.WholesaleCost
	,SUB.BillingCharge
	,SUB.BillingChargeCash
	,SUB.SupplierName
	,SUB.SupplierShortName
	,SUB.Name
	,SUB.ShortName
	,SUB.Code
	,SUB.Packaging
	,SUB.Side
	,SUB.Size
	,SUB.Color
	,SUB.Gender
	,SUB.Mod1
	,SUB.Mod2
	,SUB.Mod3

FROM
	(
	 select D.DispenseID
		,DD.DispenseDetailID
		,DD.DispenseID as DDDispenseID
		,DD.ActualChargeBilled
		,DD.Quantity
		,DD.ABNForm
		,DD.ABNType
		,LineTotal
		,C.FirstName + ' ' + C.LastName as PhysicianName
		,PCP.WholesaleCost
		,PCP.BillingCharge
		--,PCP.DMEDeposit
		,DD.DMEDeposit
		,PCP.BillingChargeCash
		,PCSB.SupplierName
		,PCSB.SupplierShortName
		,MCP.Name
		,MCP.ShortName
		,MCP.Code
		,MCP.Packaging
		,DD.LRModifier as Side --,MCP.LeftRightSide as Side
		,MCP.Size
		,MCP.Color
		,MCP.Gender
		,DD.Mod1
		,DD.Mod2
		,DD.Mod3
	 from dispensedetail DD
	left outer join Physician P on DD.PhysicianID = P.PhysicianID
	left outer join Contact C on P.ContactID = C.ContactID
	inner join practicecatalogproduct PCP
		on DD.practicecatalogproductID = PCP.practicecatalogproductID
	inner join PracticeCatalogSupplierBrand PCSB
		on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
	inner join MasterCatalogProduct MCP
		on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
	inner join Dispense D
		on DD.DispenseID = D.DispenseID
	--left outer join DispenseQueue dq
	--	on DD.DispenseQueueID = dq.DispenseQueueID

	where D.PracticeLocationID=@PracticeLocationID
	and D.IsActive=1
	and DD.Isactive=1
	--and PCP.Isactive=1
	--and PCSB.Isactive=1
	--and MCP.ISACTIVE=1
	AND COALESCE(D.PatientCode, '') like COALESCE(@PatientCode , D.PatientCode, '')
	AND(((@PhysicianName Is Not Null) AND (DD.PhysicianID = (Select top 1 subP.PhysicianID from Physician subP join Contact subC on subP.ContactID = subC.ContactID
							AND COALESCE(subC.FirstName + ' ' + subC.LastName, '') like COALESCE(@PhysicianName , subC.FirstName + ' ' + subC.LastName, '')
						  ))) OR (@PhysicianName IS Null))
	and PCP.ISThirdPartyProduct=0

	UNION All

	 select D.DispenseID
		,DD.DispenseDetailID as DDDispenseID
		,DD.DispenseID
		,DD.ActualChargeBilled
		,DD.Quantity
		,DD.ABNForm
		,DD.ABNType
		,LineTotal
		,C.FirstName + ' ' + C.LastName as PhysicianName
		,PCP.WholesaleCost
		,PCP.BillingCharge
		--,PCP.DMEDeposit
		,DD.DMEDeposit
		,PCP.BillingChargeCash
		,PCSB.SupplierName
		,PCSB.SupplierShortName
		,TPP.Name
		,TPP.ShortName
		,TPP.Code
		,TPP.Packaging
		,DD.LRModifier as Side --,TPP.LeftRightSide as Side
		,TPP.Size
		,TPP.Color
		,TPP.Gender
		,DD.Mod1
		,DD.Mod2
		,DD.Mod3
	 from dispensedetail DD
	inner join Physician P on DD.PhysicianID = P.PhysicianID
	left outer join Contact C on P.ContactID = C.ContactID
	left outer join practicecatalogproduct PCP
		on DD.practicecatalogproductID = PCP.practicecatalogproductID
	inner join PracticeCatalogSupplierBrand PCSB
		on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
	inner join THirdPartyProduct TPP
		on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
	inner join Dispense D
		on DD.DispenseID = D.DispenseID
		--left outer join DispenseQueue dq
		--on DD.DispenseQueueID = dq.DispenseQueueID

	where D.PracticeLocationID=@PracticeLocationID
	and D.IsActive=1
	and DD.Isactive=1
	--and PCP.Isactive=1
	--and PCSB.Isactive=1
	--and TPP.ISACTIVE=1
	and PCP.ISThirdPartyProduct=1
	AND COALESCE(D.PatientCode, '') like COALESCE(@PatientCode , D.PatientCode, '')
	AND(((@PhysicianName Is Not Null) AND (DD.PhysicianID = (Select top 1 subP.PhysicianID from Physician subP join Contact subC on subP.ContactID = subC.ContactID
							AND COALESCE(subC.FirstName + ' ' + subC.LastName, '') like COALESCE(@PhysicianName , subC.FirstName + ' ' + subC.LastName, '')
						  ))) OR (@PhysicianName IS Null))
	)
	AS SUB

END
GO

---------------------------------------------------

GO
ALTER procedure [dbo].[usp_Report_ManualCheckIn_By_Locations_ProdCodes_DateRange]
	@PracticeLocationID nvarchar(max),
	@Code nvarchar(max),
	@StartDate datetime,
	@EndDate datetime,
	@PracticeId int,
	@InternalCode nvarchar(1000),
	@InternalPracticeLocationID nvarchar(1000)
as
begin

	set nocount on;

	--- begin multi-value @CodeID parsing
	declare @codes as table ( [seq] int, [code] varchar(50) );
	if(@InternalCode = 'ALL')
	begin
		declare @tempCodes table ([code] varchar(50));
		insert @tempCodes
		exec dbo.usp_GetProductCodes_ByPractice @PracticeId;

		insert @codes ([code], [seq])
		select code
				,row_number() over(order by code) as [seq]
		from @tempCodes;
	end;
	else
	begin
		with pieces([seq], [start], [stop])
		as
		(
			select 1, 1, charindex(',', @Code)
			union all
			select cast([seq] + 1 as int), cast([stop] + 1 as int), charindex(',', @Code, [stop] + 1)
			from pieces
			where [stop] > 0
		)
		insert into @codes
		select
			[seq],
			cast(substring(@Code, [start], case when [stop] > 0 then [stop] - start else 512 end) as varchar(50))
		from
			pieces
		option
			(maxrecursion 10000);
	end;
	--- end multi-value @CodeID parsing

	--- begin multi-value @PracticeLocationID parsing
	declare @locations as table ( [seq] int, [location] int );
	if(@InternalPracticeLocationId = 'ALL')
	begin
		declare @tempLocations table (PracticeLocationID int, PracticeLocationName varchar(50), IsPrimaryLocation bit);

		insert @tempLocations
		exec dbo.usp_PracticeLocation_Select_All_ID_Name_Given_PracticeID @PracticeId;

		insert @locations ([location], [seq])
		select PracticeLocationID, Row_Number() over (order by PracticeLocationID)
		from @tempLocations;
	end;
	else
	begin
		with pieces([seq], [start], [stop])
		as
		(
			select 1, 1, charindex(',', @PracticeLocationID)
			union all
			select cast([seq] + 1 as int), cast([stop] + 1 as int), charindex(',', @PracticeLocationID, [stop] + 1)
			from pieces
			where [stop] > 0
		)
		insert into @locations
		select
			[seq],
			cast(substring(@PracticeLocationID, [start], case when [stop] > 0 then [stop] - start else 512 end) as int)
		from
			pieces;
	end;
	--- end multi-value @PracticeLocationID parsing

	set nocount off;

	select
		SUB.PracticeLocation,
		SUB.PracticeCatalogProductID,
		SUB.SupplierID,
		SUB.ProductInventoryID,
		SUB.BrandShortname,
		SUB.ShortName,
		SUB.Code,
		SUB.Packaging,
		SUB.LeftRightSide,
		SUB.Size,
		SUB.Color,
		SUB.Gender,
		SUB.QuantityOnHandPerSystem,
		SUB.WholesaleCost,
		SUB.IsThirdPartyProduct,
		SUB.DateCheckedIn,
		SUB.Comments
	from
		(
			select
				PL.Name as PracticeLocation,
				PCP.PracticeCatalogProductID,
				PCSB.MasterCatalogSupplierID as SupplierID,
				PI.ProductInventoryID,
				PCSB.BrandShortName,
				MCP.ShortName,
				MCP.Code,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				PI.QuantityOnHandPerSystem,
				PCP.WholesaleCost,
				0 as IsThirdPartyProduct,
				PIMCI.DateCheckedIn,
				PIMCI.Comments
			from
				ProductInventory_ManualCheckIn PIMCI
					inner join ProductInventory PI
						on PIMCI.ProductInventoryID = PI.ProductInventoryID
					inner join PracticeLocation PL
						on PL.PracticeLocationID = PI.PracticeLocationID
					inner join PracticeCatalogProduct PCP
						on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
					inner join MasterCatalogProduct MCP
						on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
					inner join PracticeCatalogSupplierBrand PCSB
						on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
			where
				PI.IsActive = 1
				and PIMCI.DateCheckedIn between @StartDate and @EndDate
				--and MCP.IsActive = 1
				--and PCSB.IsActive = 1
				and
				(
					@Code is null
					or
					MCP.Code in (select code from @codes)
				)
				and PI.PracticeLocationID in (select location from @locations)

			union

			select
				PL.Name as PracticeLocation,
				PCP.PracticeCatalogProductID,
				PCSB.MasterCatalogSupplierID as SupplierID,
				PI.ProductInventoryID,
				PCSB.BrandShortName,
				TPP.ShortName,
				TPP.Code,
				TPP.Packaging,
				TPP.LeftRightSide,
				TPP.Size,
				TPP.Color,
				TPP.Gender,
				PI.QuantityOnHandPerSystem,
				PCP.WholesaleCost,
				1 as IsThirdPartyProduct,
				PIMCI.DateCheckedIn,
				PIMCI.Comments
			from
				ProductInventory_ManualCheckIn PIMCI
					inner join ProductInventory PI
						on PIMCI.ProductInventoryID = PI.ProductInventoryID
					inner join PracticeLocation PL
						on PL.PracticeLocationID = PI.PracticeLocationID
					inner join PracticeCatalogProduct PCP
						on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
					inner join ThirdPartyProduct as TPP with(nolock)
						on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
					inner join PracticeCatalogSupplierBrand PCSB
						on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
			where
				PI.IsActive = 1
				and PIMCI.DateCheckedIn between @StartDate and @EndDate
				--and TPP.IsActive = 1
				--and PCSB.IsActive = 1
				and
				(
					@Code is null
					or
					TPP.Code in (select code from @codes)
				)
				and PI.PracticeLocationID in (select location from @locations)
		) as SUB
	order by
		SUB.IsThirdPartyProduct asc,
		SUB.DateCheckedIn asc

end
GO

----------------------------------------------

GO
ALTER PROC [dbo].[usp_Report_PracticeCatalog_By_PracticeID]

@PracticeID int = -1
AS
BEGIN

SELECT
	  pcsb.BrandName AS Manufacturer
	  ,COALESCE(mcp.Code ,tpp.Code) AS 'Manuf prod #'
	  ,pcsb.SupplierName AS 'Vendor'
	  ,COALESCE(mcp.Size, tpp.Size) AS 'Size'
	  ,COALESCE(mcp.LeftRightSide, tpp.LeftRightSide) AS 'Side'
		,dbo.ConcatHCPCS(pcp.PracticeCatalogProductID) AS HCPCS
		,COALESCE(mcp.Name, tpp.Name) AS 'Product Description'
      ,[WholesaleCost] AS 'Cost'
      ,[BillingCharge] AS 'Bill Chg'
      ,[BillingChargeCash] AS 'Bill Chg Cash'
      ,pcp.[Mod1]
      ,pcp.[Mod2]
      ,pcp.[Mod3]
  FROM [PracticeCatalogProduct] pcp
  LEFT JOIN dbo.PracticeCatalogSupplierBrand pcsb ON pcsb.PracticeCatalogSupplierBrandID = pcp.PracticeCatalogSupplierBrandID
  LEFT JOIN dbo.MasterCatalogProduct mcp ON mcp.MasterCatalogProductID = pcp.MasterCatalogProductID
  LEFT JOIN dbo.MasterCatalogSupplier mcs ON mcs.MasterCatalogSupplierID = pcsb.MasterCatalogSupplierID
  LEFT JOIN dbo.ThirdPartyProduct tpp ON tpp.ThirdPartyProductID = pcp.ThirdPartyProductID
  LEFT JOIN dbo.ThirdPartySupplier tps ON tps.ThirdPartySupplierID = pcsb.ThirdPartySupplierID
  WHERE pcp.PracticeID = @PracticeID
  --AND pcp.IsActive = 1
  --AND (mcp.IsActive = 1 OR tpp.IsActive = 1)
  --AND pcsb.IsActive = 1


END
GO

--------------------------------

GO
/*

	Author:  John Bongiorni
	Created:  2007.12.18

	EXEC dbo.[usp_Report_ProductReconciliation] 6
				, '13, 15, 16, 17, 18, 19, 20, 22, 23, 24'
				, '34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 74, 75, 76, 81, 82, 83, 99, 101'
				, '1/1/2007'
				, '12/31/2007'

	Modifications:

	Late Updated:  2007.12.18 14:05
*/


ALTER PROC [dbo].[usp_Report_ProductReconciliation]
		  @PracticeID			INT
		, @PracticeLocationID	VARCHAR(2000)
		, @SupplierBrandID		VARCHAR(2000)   --  ID String.
		, @StartDate			DATETIME
		, @EndDate				DATETIME
		, @InternalPracticeLocationID VARCHAR(2000)
		, @InternalSupplierBrandID VARCHAR(2000)
AS
BEGIN

--	DECLARE @PracticeID INT
--	SET @PracticeID = 6


	SELECT @EndDate = DATEADD(DAY, 1, @EndDate);

	-- Parse the ID strings
	-- Parse the Practice Location
	DECLARE @locations TABLE ([PracticeLocationID] INT);

	-- If the Internal parameter is "ALL", we select all of the Practice Locations
	-- for the passed PracticeID.
	-- Otherwise, split as we were doing before.
	IF(@InternalPracticeLocationID = 'ALL')
	BEGIN
		DECLARE @tempLocations TABLE (
			PracticeLocationID INT
			,PracticeLocationName VARCHAR(50)
			,IsPrimaryLocation BIT
			);

		INSERT @tempLocations
		EXEC dbo.usp_PracticeLocation_Select_All_ID_Name_Given_PracticeID @PracticeID = @PracticeID;

		INSERT @locations ([PracticeLocationID])
		SELECT PracticeLocationID
		FROM @tempLocations;
	END;
	ELSE
	BEGIN
		INSERT @locations ([PracticeLocationID])
		SELECT ID
		FROM dbo.udf_ParseArrayToTable(@PracticeLocationID, ',');
	END;

	-- Parse the Supplier Brand
	DECLARE @supplierBrands TABLE([SupplierBrandID] INT);

	-- If the Internal parameter is "ALL", we select all of the Supplier Brands
	-- for the passed PracticeID.
	-- Otherwise, split as we were doing before.
	IF(@InternalSupplierBrandID = 'ALL')
	BEGIN
		DECLARE @tempSupplierBrands TABLE (PracticeCatelogSupplierBrandID INT, SupplierBrand VARCHAR(1000), SupplierName VARCHAR(1000), BrandName VARCHAR(1000), IsThirdPartySupplier BIT, DerivedSequence INT);

		INSERT @tempSupplierBrands
		EXEC dbo.usp_Report_GetPracticeCatalogSupplierBrands_Formatted @PracticeID = @PracticeID;

		INSERT @supplierBrands ([SupplierBrandID])
		SELECT PracticeCatelogSupplierBrandID
		FROM @tempSupplierBrands;
	END;
	ELSE
	BEGIN
		INSERT @supplierBrands ([SupplierBrandID])
		SELECT ID
		FROM dbo.udf_ParseArrayToTable(@SupplierBrandID, ',');
	END;

	SELECT
			 SUB.PracticeName						AS Practice
			, SUB.PurchaseOrder						AS PO
			, SUB.CustomPurchaseOrderCode			AS CustomPO
			, SUB.InvoiceNumber
			, CASE SUB.InvoicePaid
				WHEN 1 then 'Yes'
				ELSE 'No'
			  END									AS InvoicePaid

			, SUB.PracticeLocation					AS Location

			, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
				ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
				END									AS SupplierBrand

			, SUB.ProductShortName					AS Product
			, SUB.Side + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender
			, SUB.Code

			, SUB.QuantityOrdered								AS QuantityOrdered
--			, SUB.ActualWholesaleCost							AS ActualWholesale
			, CAST(SUB.LineTotal  AS DECIMAL(15, 2))			AS QuantityOrderedCost

			, SUB.QuantityCheckedIn
			, CAST((SUB.QuantityCheckedIn * SUB.ActualWholesaleCost)
											AS DECIMAL(15, 2))  AS QuantityCheckInCost

			, SUB.QuantityToCheckIn								AS QuantityOutstanding
			, CAST((SUB.QuantityToCheckIn * SUB.ActualWholesaleCost)
											AS DECIMAL(15, 2)) AS QuantityOutstandingCost

			, SUB.OrderedDate									AS OrderedDate
			, SUB.Sequence

	FROM

		(
		--  MC SUPPLIER PRODUCTS --

		SELECT
			   P.PracticeID
			, P.PracticeName
			, SO.PurchaseOrder
			, SO.CustomPurchaseOrderCode
			, SO.InvoiceNumber
			, SO.InvoicePaid
    		, PL.PracticeLocationID
			, PL.NAME AS PracticeLocation

--			, CASE WHEN PCSB.SupplierShortName = PCSB.BrandShortName THEN PCSB.SupplierShortName
--				ELSE PCSB.SupplierShortName + ' (' + PCSB.BrandShortName + ')'
--				END					AS SupplierBrand

			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) AS Sequence

			, PCP.PracticeCatalogProductID
			, MCP.[ShortName] AS ProductShortName
			, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS Side
			, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
			, ISNULL(MCP.Code, '') AS Code

			, SOLI.ActualWholesaleCost
			, SOLI.QuantityOrdered
			, SOLI.LineTotal

			, ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityCheckedIn
			, CASE WHEN ( SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) ) < 0 THEN 0
				ELSE  SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0)
				END									AS QuantityToCheckIn					--  FUDGED due to bad checkin data.
		--	, SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0)  AS QuantityToCheckIn  --Correct


			, CONVERT(VARCHAR(10), SOLI.CreatedDate, 101)  AS OrderedDate

		FROM SupplierOrderLineItem SOLI  WITH (NOLOCK)

		INNER JOIN SupplierOrder AS SO  WITH (NOLOCK)
			ON SOLI.SupplierOrderID = SO.SupplierOrderID

		INNER JOIN BregVisionOrder AS BVO  WITH (NOLOCK)
			ON SO.BregVisionOrderID = BVO.BregVisionOrderID

		INNER JOIN PracticeLocation AS PL  WITH (NOLOCK)
			ON BVO.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P  WITH (NOLOCK)
			ON P.PracticeID = PL.PracticeID

		INNER JOIN PracticeCatalogPRoduct PCP  WITH (NOLOCK)
			on SOLI.PracticeCatalogProductID = 	PCP.PracticeCatalogPRoductID


		INNER JOIN MasterCatalogProduct MCP  WITH (NOLOCK)
			on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID


		INNER JOIN PracticeCatalogSupplierBrand AS PCSB  WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

		LEFT JOIN (SELECT SupplierOrderLineItemID, SUM(Quantity) AS QuantityCheckedIn
					FROM dbo.ProductInventory_OrderCheckIn
					WHERE IsActive = 1
					GROUP BY SupplierOrderLineItemID)AS SubPIOCI
						ON SOLI.SupplierOrderLineItemID = SubPIOCI.SupplierOrderLineItemID

		WHERE
			P.PracticeID = @PracticeID

			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND BVO.IsActive = 1
			AND SO.IsActive = 1
			AND SOLI.IsActive = 1
			--AND PCP.isactive = 1
			--AND PCSB.isactive = 1
			--AND MCP.isactive = 1

			AND PL.PracticeLocationID IN
				(SELECT PracticeLocationID
				FROM @locations)

			AND PCSB.PracticeCatalogSupplierBrandID IN
				(SELECT SupplierBrandID
				FROM @supplierBrands)

			AND SOLI.CreatedDate >= @StartDate
			AND SOLI.CreatedDate < @EndDate --  Less than since @EndDate has one day

	UNION

	--  THIRD PARTY SUPPLIER PRODUCTS --
	--DECLARE @PracticeID INT
	--SET @PracticeID = 6
		SELECT
				   P.PracticeID
				, P.PracticeName
				, SO.PurchaseOrder
				, SO.CustomPurchaseOrderCode
				, SO.InvoiceNumber
				, SO.InvoicePaid
    			, PL.PracticeLocationID
				, PL.NAME AS PracticeLocation

				, PCSB.SupplierShortName
				, PCSB.BrandShortName
				, ISNULL(PCSB.Sequence, 999) + 1000 AS Sequence

				, PCP.PracticeCatalogProductID
				, TPP.[ShortName] AS ProductShortName
				, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS Side
				, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )			AS Size
				, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
				, ISNULL(TPP.Code, '') AS Code

				, SOLI.ActualWholesaleCost
				, SOLI.QuantityOrdered
				, SOLI.LineTotal

				, ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityCheckedIn
				, CASE WHEN ( SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) ) < 0 THEN 0
					ELSE  SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0)
					END									AS QuantityToCheckIn
			--	, SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityToCheckIn

				, CONVERT(VARCHAR(10), SOLI.CreatedDate, 101)  AS OrderedDate


		FROM SupplierOrderLineItem SOLI  WITH (NOLOCK)

		INNER JOIN SupplierOrder AS SO  WITH (NOLOCK)
			ON SOLI.SupplierOrderID = SO.SupplierOrderID

		INNER JOIN BregVisionOrder AS BVO  WITH (NOLOCK)
			ON SO.BregVisionOrderID = BVO.BregVisionOrderID

		INNER JOIN PracticeLocation AS PL  WITH (NOLOCK)
			ON BVO.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P  WITH (NOLOCK)
			ON P.PracticeID = PL.PracticeID

		INNER JOIN PracticeCatalogPRoduct PCP  WITH (NOLOCK)
			on SOLI.PracticeCatalogProductID = 	PCP.PracticeCatalogPRoductID

		INNER JOIN ThirdPartyProduct TPP  WITH (NOLOCK)
			on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

		INNER JOIN PracticeCatalogSupplierBrand AS PCSB  WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

		LEFT JOIN (SELECT SupplierOrderLineItemID, SUM(Quantity) AS QuantityCheckedIn
					FROM dbo.ProductInventory_OrderCheckIn
					WHERE IsActive = 1
					GROUP BY SupplierOrderLineItemID)AS SubPIOCI
						ON SOLI.SupplierOrderLineItemID = SubPIOCI.SupplierOrderLineItemID

		WHERE
			P.PracticeID = @PracticeID

			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND BVO.IsActive = 1
			AND SO.IsActive = 1
			AND SOLI.IsActive = 1
			--AND PCP.isactive = 1
			--AND PCSB.isactive = 1
			--and TPP.isactive = 1

			AND PL.PracticeLocationID IN
				(SELECT PracticeLocationID
				FROM @locations)

			AND PCSB.PracticeCatalogSupplierBrandID IN
				(SELECT SupplierBrandID
				FROM @supplierBrands)

			AND SOLI.CreatedDate >= @StartDate
			AND SOLI.CreatedDate < @EndDate --  Less than since @EndDate has one day

		) AS SUB


	ORDER BY
			 SUB.PracticeName
			, SUB.PurchaseOrder
			, SUB.PracticeLocation
			, SUB.Sequence

			, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
				ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
				END								--AS SupplierBrand


			, SUB.ProductShortName
			, SUB.Code
			, SUB.Side + ', ' + SUB.Size  + ', ' + SUB.Gender --AS SideSizeGender
			, SUB.QuantityOrdered

END
GO


--------------------------------------------------
GO
/*
  exec [usp_Report_ProductsDispensed_By_HCPCS_With_DateDispensed_PatientCode]  1, '3','1/1/2009', '12/31/2009',1,0, 'H1202,H1202A,L1111'
Select * from dbo.udf_ParseStringArrayToTable('H1202,H1202A,L1111,<none>', ',')
*/
----

--  [usp_Report_ProductsDispensed_By_Physician_With_DateDispensed_PatientCode]  18, '56,57', '1', '1/1/2007', '12/31/2007'
--    [usp_Report_ProductsDispensed_By_Physician_With_DateDispensed_PatientCode]  1, '3', '2,-999', '1/1/2007', '12/31/2007'  --94

--  Added Supplier Brand.
--  Added Sequence
--  Add IsActive = 1

-- Modification:  , D.DateDispensed AS DateDispensed  -- , DD.CreatedDate AS DateDispensed  --  2008.02.12  JB

ALTER PROC [dbo].[usp_Report_ProductsDispensed_By_HCPCS_With_DateDispensed_PatientCode]
		  @PracticeID			INT
		, @PracticeLocationID	VARCHAR(2000)
		--, @PhysicianID			VARCHAR(2000)
		, @StartDate			DATETIME
		, @EndDate				DATETIME
		, @OrderBy				int
		, @OrderDirection		bit
		, @HCPCsList			Varchar(2000) = null
		, @IsMedicare           bit = null
		, @MedicareOrderDirection bit = null
		, @InternalPracticeLocationID VARCHAR(2000)
		, @InternalHCPCsList	VARCHAR(2000) = NULL
AS
BEGIN

DECLARE @PracticeLocationIDString VARCHAR(2000)

-- Parse Practice Location List
DECLARE @locations TABLE([PracticeLocationID] INT);

IF(@InternalPracticeLocationID = 'ALL')
BEGIN
	DECLARE @tempLocations TABLE (
			PracticeLocationID INT
			,PracticeLocationName VARCHAR(50)
			,IsPrimaryLocation BIT
			);

		INSERT @tempLocations
		EXEC dbo.usp_PracticeLocation_Select_All_ID_Name_Given_PracticeID @PracticeID = @PracticeID;

		INSERT @locations ([PracticeLocationID])
		SELECT PracticeLocationID
		FROM @tempLocations;
END;
ELSE
BEGIN
	INSERT @locations ([PracticeLocationID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable(@PracticeLocationID, ',');
END;

-- Get All HCPCs if called for
IF(@InternalHCPCsList = 'ALL')
BEGIN
	DECLARE @locationList NVARCHAR(MAX);
	IF(@InternalPracticeLocationID = 'ALL')
	BEGIN
		SELECT @locationList = COALESCE(@locationList + CAST(PracticeLocationID AS NVARCHAR(10)) + ',', '')
		FROM @locations;

		IF(LEN(@locationList) > 0)
			SET @locationList = LEFT(@locationList, LEN(@locationList) - 1);
	END;
	ELSE
	BEGIN
		SET @locationList = @PracticeLocationID;
	END;

	DECLARE @hcpcs TABLE (HCPCS VARCHAR(50));

	INSERT @hcpcs
	EXEC dbo.usp_GetPracticeLocationHCPCs @PracticeLocationIDString = @locationList;

	SET @HCPCsList = '';

	SELECT @HCPCsList = COALESCE(@HCPCsList + HCPCS + ',','')
	FROM @hcpcs;

	IF(LEN(@HCPCsList) > 0)
		SET @HCPCsList = LEFT(@HCPCsList, LEN(@HCPCsList) - 1);
END;

IF NOT EXISTS (
  select * from (
          select distinct dq.HCPCs
            from DispenseQueue dq
            where dq.PracticeLocationID in (select ID from dbo.udf_ParseArrayToTable(@PracticeLocationID, ','))
              and nullif(dq.HCPCs, '') is not null
          union
          select distinct ph.HCPCS
            from
              ProductInventory pi
              inner join PracticeCatalogProduct pcp on pi.PracticeCatalogProductID = pcp.PracticeCatalogProductID
              inner join ProductHCPCS ph on pcp.PracticeCatalogProductID = ph.PracticeCatalogProductID
            where
              pi.PracticeLocationID in (select ID from dbo.udf_ParseArrayToTable(@PracticeLocationID, ','))
        ) as T
        where ltrim(rtrim(t.HCPCS)) not in (select ltrim(rtrim(TextValue)) from dbo.udf_ParseStringArrayToTable(@HCPCsList, ','))
	) SET @HCPCsList = NULL

IF @HCPCsList = '' set @HCPCsList = NULL

SELECT @EndDate = DATEADD(DAY, 1, @EndDate)

SELECT
		SUB.HCPCS

		, SUB.PracticeName		AS Practice
		, SUB.PracticeLocation	AS Location

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
			ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
			END					AS SupplierBrand


		, SUB.SupplierShortName	AS Supplier

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
		ELSE SUB.BrandShortName
		END						AS Brand


		, SUB.Sequence

		, SUB.ProductName			AS Product

		, SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender

		, SUB.Code


		, case SUB.IsCashCollection
			when 1 then Sub.BillingChargeCash
			when 0 then 0
			end
			 as BillingChargeCash


		, CONVERT(VARCHAR(10), SUB.DateDispensed, 101) AS DateDispensed
		, SUB.PatientCode

		, CASE SUB.IsMedicare
			When 1 then 'Y'
			When 0 then 'N'
		  End
		  as IsMedicare
		, CASE CAST(SUB.ABNType AS varchar(50))
			When 'Medicare' then 'M' + ',' +  CAST(SUB.MedicareOption AS varchar(50))
			When 'Commercial' then 'C' + ',' + CAST(SUB.MedicareOption AS varchar(50))
		  End
		  as ABNType
		, SUB.ICD9Code
		, SUB.Quantity 		--, SUM( SUB.Quantity )

		, CAST(  SUB.ActualWholesaleCostLineTotal  AS DECIMAL(15,2) )	AS ActualWholesaleCostLineTotal
		, CAST(  SUB.DMEDepositLineTotal  AS DECIMAL(15,2) )			AS DMEDepositLineTotal
		, SUB.IsCashCollection
		,  case SUB.IsCashCollection
			when 1 then 0
			when 0 then CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )
			end
			AS ActualChargeBilledLineTotal

		, ISNULL(SUB.PayerName, 'N/A') AS PayerName
		, SUB.DispenseNote
		, SUB.OriginalActualChargeBilled
		, LTRIM(RTRIM(ISNULL(SUB.PatientFirstName,''))) AS PatientFirstName
		, LTRIM(RTRIM(ISNULL(SUB.PatientLastName,''))) AS PatientLastName
		, SUB.Mod1
		, SUB.Mod2
		, SUB.Mod3
FROM


		(
		SELECT
			  --PH.HCPCS
			ISNULL(NULLIF(
				ISNULL(DQ.HCPCs,
					IIF(DQ.IsCustomFit = NULL or (DQ.IsCustomFit = 0 AND pcp.IsSplitCode=1),
						dbo.ConcatHCPCSOTS(pcp.PracticeCatalogProductID),
						dbo.ConcatHCPCS(pcp.PracticeCatalogProductID)))
				,''), 'No HCPC') AS HCPCS
			, P.PracticeName
			, PL.Name AS PracticeLocation

			, D.PatientCode

			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) AS Sequence

			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash

			, MCP.[Name] AS ProductName

			--, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender

			, ISNULL(MCP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed  -- , DD.CreatedDate AS DateDispensed  --  2008.02.12  JB  --, DD.CreatedDate AS DateDispensed
			, DD.IsMedicare
			, DD.ABNType
			, DD.ICD9Code
			, DD.Quantity
			, PI_DD.ActualWholesaleCost
			, DD.ActualChargeBilled as ActualChargeBilled
			, DD.DMEDeposit
			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit AS DMEDepositLineTotal
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.

			, PP.Name AS PayerName
			, D.Notes AS DispenseNote
			, DD.ActualChargeBilled as OriginalActualChargeBilled
			, D.PatientFirstName as PatientFirstName
			, D.PatientLastName as PatientLastName
			, DD.Mod1
			, DD.Mod2
			, DD.Mod3
			, DQ.MedicareOption

		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOin DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
			ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

		LEFT OUTER JOIN DispenseQueue DQ
			on  DQ.DispenseQueueID = DD.DispenseQueueID

		--LEFT OUTER JOIN ProductHCPCS PH
		--	on  PH.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		LEFT JOIN PracticePayer PP
			on PP.PracticePayerID = D.PracticePayerID

		WHERE
			P.PracticeID = @PracticeID
			AND DD.IsMedicare = Coalesce(@IsMedicare, DD.IsMedicare)
			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			--AND PCP.IsActive = 1
			--AND PCSB.IsActive = 1
			--AND MCP.IsActive = 1

		AND
		PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			FROM @locations)

		--AND
		--	PH.HCPCS IN (Select TextValue as HCPC s from dbo.udf_ParseStringArrayToTable(@HCPCsList, ','))


		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM

		UNION ALL

		SELECT
			  --PH.HCPCS
			ISNULL(NULLIF(
				ISNULL(DQ.HCPCs,
					IIF(DQ.IsCustomFit = NULL or (DQ.IsCustomFit = 0 AND pcp.IsSplitCode=1),
						dbo.ConcatHCPCSOTS(pcp.PracticeCatalogProductID),
						dbo.ConcatHCPCS(pcp.PracticeCatalogProductID)))
				,''), 'No HCPC') AS HCPCS
			  --Ph.PhysicianID
			, P.PracticeName
			, PL.Name AS PracticeLocation
			--, IsNULL(RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix), '') AS Physician

			, D.PatientCode

			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) + 1000 AS Sequence

			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash

			, TPP.[Name] AS ProductName

			--, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender


			, ISNULL(TPP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed  -- , DD.CreatedDate AS DateDispensed  --  2008.02.12  JB
			, DD.IsMedicare
			, DD.ABNType
			, DD.ICD9Code
			, DD.Quantity
			, PI_DD.ActualWholesaleCost
			, DD.ActualChargeBilled as ActualChargeBilled
			, DD.DMEDeposit
			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit AS DMEDepositLineTotal
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.

			, PP.Name AS PayerName
			, D.Notes AS DispenseNote
			, DD.ActualChargeBilled as OriginalActualChargeBilled
			, D.PatientFirstName as PatientFirstName
			, D.PatientLastName as PatientLastName
			, DD.Mod1
			, DD.Mod2
			, DD.Mod3
			, DQ.MedicareOption

		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOin DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
			ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

		LEFT OUTER JOIN DispenseQueue DQ
			on  DQ.DispenseQueueID = DD.DispenseQueueID

		--LEFT OUTER JOIN ProductHCPCS PH
		--	on  PH.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		LEFT JOIN PracticePayer PP
			on PP.PracticePayerID = D.PracticePayerID

		WHERE
			P.PracticeID = @PracticeID
			AND DD.IsMedicare = Coalesce(@IsMedicare, DD.IsMedicare)
			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			--AND PCP.IsActive = 1
			--AND PCSB.IsActive = 1
			--AND TPP.IsActive = 1

		AND
		PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			FROM @locations)

		--AND
		--	PH.HCPCS IN (Select TextValue as HCPC s from dbo.udf_ParseStringArrayToTable(@HCPCsList, ','))


		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM

		) AS Sub

--	where (@HCPCsList IS NULL OR ISNULL(SUB.HCPCS,'No HCPC') in(Select TextValue as HCPCs from dbo.udf_ParseStringArrayToTable(@HCPCsList, ',')))
	where (@HCPCsList IS NULL OR SUB.HCPCS = 'No HCPC' OR (dbo.CompareDelimitedStrings(SUB.HCPCS, @HCPCsList, ',') = 1))

	 order by


		CASE
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName DESC))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName))

			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName DESC))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName))

			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName DESC ))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName ))


			WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed))

			WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed))

			WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed))

		END



END
GO

--------------------------------------------------------------------------------------------------------------------------------

GO
/*

--  [usp_Report_ProductsDispensed_By_PatientCode]  6, '13, 15, 16, 17, 18, 19, 20, 22, 23, 24', '1/1/2007', '12/31/2007'
--  Added Supplier Brand.
--  Added Sequence
--  Add IsActive = 1

--  Modified from usp_Report_ProductsDispensed

--  Created by:  John Bongiorni
--  Date:		 2008.01.04
--  Modified:  2008.02.12 JB , D.DateDispensed AS DateDispensed	--, DD.CreatedDate AS DateDispensed  --JB 2008.02.12
	usp_Report_ProductsDispensed_By_PatientCode 1, '3', '12/1/2009', '12/4/2009', 1, 1, null,1
*/

ALTER PROC [dbo].[usp_Report_ProductsDispensed_By_PatientCode]
		  @PracticeID	INT
		, @PracticeLocationID	VARCHAR(2000)
		, @StartDate		DATETIME
		, @EndDate			DATETIME
		, @OrderBy				int
		, @OrderDirection		bit
		, @IsMedicare           bit = null
		, @MedicareOrderDirection bit = null
		, @InternalPracticeLocationID VARCHAR(2000)
AS
BEGIN
print @MedicareOrderDirection

--DECLARE @Output VARCHAR(50)
--SET @Output = ''
--
--SELECT @Output =
--	CASE WHEN @Output = '' THEN CAST(PracticeLocationID AS VARCHAR(50))
--		 ELSE @Output + ', ' + CAST(PracticeLocationID AS VARCHAR(50))
--		 END
--FROM PracticeLocation
--WHERE PracticeID = 6
--
--SELECT @Output AS Output



--	DECLARE @PracticeID INT
--	SET @PracticeID = 6


--  SET @EndDate to 11:59:59 PM
SELECT @EndDate = DATEADD(DAY, 1, @EndDate)

DECLARE @locations TABLE([PracticeLocationID] INT);

IF(@InternalPracticeLocationID = 'ALL')
BEGIN
	DECLARE @tempLocations TABLE (
		PracticeLocationID INT
		,PracticeLocationName VARCHAR(50)
		,IsPrimaryLocation BIT
		);

	INSERT @tempLocations
	EXEC dbo.usp_PracticeLocation_Select_All_ID_Name_Given_PracticeID @PracticeID = @PracticeID;

	INSERT @locations ([PracticeLocationID])
	SELECT PracticeLocationID
	FROM @tempLocations;
END;
ELSE
BEGIN
	INSERT @locations ([PracticeLocationID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable(@PracticeLocationID, ',');
END;

--DECLARE @PhysicianIDString VARCHAR(2000)
--SET @PhysicianIDString = @PhysicianID
---  Insert select here ----

SELECT
		  SUB.PracticeName		AS Practice
		, SUB.PracticeLocation	AS Location

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
			ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
			END					AS SupplierBrand


		, SUB.SupplierShortName	AS Supplier

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
		ELSE SUB.BrandShortName
		END						AS Brand

--		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN 1
--		ELSE 0
--		END AS IsBrandSuppressed
--		, SUB.IsThirdPartySupplier
		, SUB.Sequence

		, SUB.ProductName			AS Product

		, SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender

		, SUB.Code

		, COALESCE(SUB.HCPCs, dbo.ConcatHCPCS(SUB.PracticeCatalogProductID)) AS HCPCS -- JB 20071004 Added Column for UDF Concat of HCPCS
		--, if (SUB.ISCashCollection=1)  SUB.BillingChargeCash
		, case SUB.IsCashCollection
			when 1 then Sub.BillingChargeCash
			when 0 then 0
			end
			 as BillingChargeCash

		, SUB.Physician
		, SUB.PatientCode
		, Sub.DispenseID
		, CONVERT(VARCHAR(10), SUB.DateDispensed, 101) AS DateDispensed
		, CASE SUB.IsMedicare
			When 1 then 'Y'
			When 0 then 'N'
		  End
		  as IsMedicare
		, CASE CAST(SUB.ABNType AS varchar(50))
			When 'Medicare' then 'M' + ',' + CAST(SUB.MedicareOption AS varchar(50))
			When 'Commercial' then 'C' + ',' + CAST(SUB.MedicareOption AS varchar(50))
		  End
		  as ABNType
		  , SUB.ICD9Code
		  , SUB.Quantity 		--, SUM( SUB.Quantity )

--		, CAST( SUB.ActualWholesaleCost AS DECIMAL(15,2) )	AS ActualWholesaleCost
--		, CAST( SUB.ActualChargeBilled AS DECIMAL(15,2) )	AS ActualChargeBilled
--		, CAST( SUB.DMEDeposit AS DECIMAL(15,2) )			AS DMEDeposit

		, CAST(  SUB.ActualWholesaleCostLineTotal  AS DECIMAL(15,2) )	AS ActualWholesaleCostLineTotal
		, CAST(  SUB.DMEDepositLineTotal  AS DECIMAL(15,2) )			AS DMEDepositLineTotal
		, SUB.IsCashCollection
		--, CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )    AS ActualChargeBilledLineTotal
		,  case SUB.IsCashCollection
			when 1 then 0
			when 0 then CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )
			end
			AS ActualChargeBilledLineTotal
--		, SUB.Comment  -- Not Displayed for Version 1.

		, ISNULL(SUB.PayerName, 'N/A') AS PayerName
		, SUB.DispenseNote
		, SUB.OriginalActualChargeBilled
		, LTRIM(RTRIM(ISNULL(SUB.PatientFirstName,''))) AS PatientFirstName
		, LTRIM(RTRIM(ISNULL(SUB.PatientLastName,''))) AS PatientLastName
		, SUB.IsCustomFit AS IsCustomFit
		, SUB.Mod1
		, SUB.Mod2
		, SUB.Mod3
FROM


		(
		SELECT

			  P.PracticeName
			, PL.Name AS PracticeLocation
			, IsNULL(RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix), '') AS Physician

			, D.PatientCode
			, D.DispenseID

			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) AS Sequence

			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash

			, MCP.[Name] AS ProductName

			--, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender

			, ISNULL(MCP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed	--, DD.CreatedDate AS DateDispensed  --JB 2008.02.12
			, DD.IsMedicare
			, DD.ABNType
			, DD.ICD9Code
			, DD.Quantity
			, PI_DD.ActualWholesaleCost
			, DD.ActualChargeBilled as ActualChargeBilled
			, DD.DMEDeposit
			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit AS DMEDepositLineTotal
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.

			, PP.Name AS PayerName
			, D.Notes AS DispenseNote
			, DD.ActualChargeBilled as OriginalActualChargeBilled
			, D.PatientFirstName as PatientFirstName
			, D.PatientLastName as PatientLastName
			, DQ.HCPCs
			, DD.IsCustomFit AS IsCustomFit
			, DD.Mod1
			, DD.Mod2
			, DD.Mod3
			, Dq.MedicareOption
		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOin DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
			ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID		    --  Can be inactive

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1

		LEFT JOIN PracticePayer PP
			on PP.PracticePayerID = D.PracticePayerID

		LEFT JOIN DispenseQueue AS DQ
			on DQ.DispenseQueueID = DD.DispenseQueueID

		WHERE
			P.PracticeID = @PracticeID
			AND DD.IsMedicare = Coalesce(@IsMedicare, DD.IsMedicare)
			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			--AND PCP.IsActive = 1
			--AND PCSB.IsActive = 1
			--AND MCP.IsActive = 1

		AND
		PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			FROM @locations)



--		AND
--		Ph.PhysicianID IN
--			(SELECT ID
--			 FROM dbo.udf_ParseArrayToTable(@PhysicianIDString, ','))

		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM

		UNION All

		SELECT

			 P.PracticeName
			, PL.Name AS PracticeLocation
			, C.FirstName + ' ' + C.LastName AS Physician  --  Get proper line for this!!

			, D.PatientCode
			, D.DispenseID

			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) + 1000 AS Sequence

			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash

			, TPP.[Name] AS ProductName

			--, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender


			, ISNULL(TPP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed	--, DD.CreatedDate AS DateDispensed  --JB 2008.02.12
			, DD.IsMedicare
			, DD.ABNType
			, DD.ICD9Code
			, DD.Quantity
			, PI_DD.ActualWholesaleCost
			, DD.ActualChargeBilled as ActualChargeBilled
			, DD.DMEDeposit

			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit AS DMEDepositLineTotal
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.

			, PP.Name AS PayerName
			, D.Notes AS DispenseNote
			, DD.ActualChargeBilled as OriginalActualChargeBilled
			, D.PatientFirstName as PatientFirstName
			, D.PatientLastName as PatientLastName
			, DQ.HCPCs
			, DD.IsCustomFit AS IsCustomFit
			, DD.Mod1
			, DD.Mod2
			, DD.Mod3
			, Dq.MedicareOption
		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOin DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
			ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID			--  Can be inactive

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1

		LEFT JOIN PracticePayer PP
			on PP.PracticePayerID = D.PracticePayerID

		LEFT JOIN DispenseQueue AS DQ
			on DQ.DispenseQueueID = DD.DispenseQueueID

		WHERE
			P.PracticeID = @PracticeID
			AND DD.IsMedicare = Coalesce(@IsMedicare, DD.IsMedicare)
			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			--AND PCP.IsActive = 1
			--AND PCSB.IsActive = 1
			--AND TPP.IsActive = 1

		AND
		PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			FROM @locations)

		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM

		) AS Sub

----
------GROUP BY
------		SUB.PracticeName
------		, SUB.PracticeLocation
------		, SUB.Physician
------		, SUB.IsThirdPartySupplier
------		, SUB.Sequence
------		, SUB.SupplierShortName
------		, SUB.BrandShortName
------
------		, SUB.ProductName
------		, SUB.Side
------		, SUB.Size
------		, SUB.Gender
------		, SUB.Code
------
------		, SUB.PatientCode
------
------		, SUB.DateDispensed
------
------		, SUB.ActualWholesaleCost
------		, SUB.ActualChargeBilled
------		, Sub.DMEDeposit
------		, Sub.Comment
----

	 order by
	 --/*
		CASE
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName DESC))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName))

			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName DESC))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName))

			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName DESC ))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName ))


			WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed))

			WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed))

			WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed))

		END
		--*/

		/*
		CASE WHEN @OrderBy = 1 and @OrderDirection = 0 THEN SUB.ProductName End DESC,
		CASE WHEN @OrderBy = 1 and @OrderDirection = 1 THEN SUB.ProductName END,
		CASE WHEN @OrderBy = 2 and @OrderDirection = 0 THEN SUB.DateDispensed End DESC,
		CASE WHEN @OrderBy = 2 and @OrderDirection = 1 THEN SUB.DateDispensed END,
		CASE WHEN @OrderBy = 3 and @OrderDirection = 0 THEN SUB.ProductName End DESC,
		CASE WHEN @OrderBy = 3 and @OrderDirection = 1 THEN SUB.ProductName END
		*/



----
----	ORDER BY
----		SUB.PracticeName
----		, SUB.PracticeLocation
----
------		, SUB.Physician
----
----		, SUB.Sequence		--  Note 3rd Party has 100 added to it sequence for this query.
------		, SUB.IsThirdPartySupplier
----		, SUB.SupplierShortName
----		, SUB.BrandShortName
----
----		--  Sort here by patient code, date dispensed, and physician  --JB  2008.01.04
----		, SUB.PatientCode
----		, CONVERT(VARCHAR(10), SUB.DateDispensed, 101) --AS DateDispensed
----		, SUB.Physician
----
----		, SUB.ProductName
----		, SUB.Code
----
----		, SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender

END
GO



------------------------------------------------------------------------------------
GO
--  [usp_Report_ProductsDispensed_By_Physician_With_DateDispensed_PatientCode]  6, '13, 15, 16, 17, 18, 19, 20, 22, 23, 24',
--		'34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 58, 59, 60, 61, 62, 63, 64, 65, 66'
--		,'1/1/2007', '12/31/2007'  -- 1110

----

--  [usp_Report_ProductsDispensed_By_Physician_With_DateDispensed_PatientCode]  18, '56,57', '1', '1/1/2007', '12/31/2007'
--    [usp_Report_ProductsDispensed_By_Physician_With_DateDispensed_PatientCode]  1, '3', '2,-999', '1/1/2007', '12/31/2007'  --94

--  Added Supplier Brand.
--  Added Sequence
--  Add IsActive = 1

-- Modification:  , D.DateDispensed AS DateDispensed  -- , DD.CreatedDate AS DateDispensed  --  2008.02.12  JB

ALTER PROC [dbo].[usp_Report_ProductsDispensed_By_Physician_With_DateDispensed_PatientCode]
		  @PracticeID			INT
		, @PracticeLocationID	VARCHAR(2000)
		, @PhysicianID			VARCHAR(2000)
		, @StartDate			DATETIME
		, @EndDate				DATETIME
		, @OrderBy				int
		, @OrderDirection		bit
		, @IsMedicare           bit = null
		, @MedicareOrderDirection bit = null
		, @InternalPracticeLocationID VARCHAR(2000)
		, @InternalPhysicianID	VARCHAR(2000)

AS
BEGIN


--SELECT * FROM PracticelOCATION WHERE PRACTICEID = 18
--select * from Practice WHERE PRACTICEID = 18


--DECLARE @Output VARCHAR(500)
--SET @Output = ''
--
--SELECT @Output =
--	CASE WHEN @Output = '' THEN CAST(PracticeLocationID AS VARCHAR(50))
--		 ELSE @Output + ', ' + CAST(PracticeLocationID AS VARCHAR(50))
--		 END
--FROM PracticeLocation
--WHERE PracticeID = 6

--
--SELECT @Output AS Output

--	DECLARE @PracticeID INT
--	SET @PracticeID = 6

--  SET @EndDate to 11:59:59 PM
SELECT @EndDate = DATEADD(DAY, 1, @EndDate)

DECLARE @locations TABLE([PracticeLocationID] INT);

IF(@InternalPracticeLocationID = 'ALL')
BEGIN
	DECLARE @tempLocations TABLE (
		PracticeLocationID INT
		,PracticeLocationName VARCHAR(50)
		,IsPrimaryLocation BIT
		);

	INSERT @tempLocations
	EXEC dbo.usp_PracticeLocation_Select_All_ID_Name_Given_PracticeID @PracticeID = @PracticeID;

	INSERT @locations ([PracticeLocationID])
	SELECT PracticeLocationID
	FROM @tempLocations;
END;
ELSE
BEGIN
	INSERT @locations ([PracticeLocationID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable(@PracticeLocationID, ',');
END;

DECLARE @physicians TABLE([PhysicianID] INT);

IF(@InternalPhysicianID = 'ALL')
BEGIN
	DECLARE @tempPhysicians TABLE (
		PracticeID INT
		,PhysicianID INT
		,ContactID INT
		,PhysicianName VARCHAR(100)
		);

	INSERT @tempPhysicians
	EXEC dbo.usp_Physician_SelectALL_Names_ByPracticeID @PracticeID = @PracticeID;

	INSERT @physicians ([PhysicianID])
	SELECT PhysicianID
	FROM @tempPhysicians;

	INSERT @physicians ([PhysicianID])
	SELECT NULL;
END;
ELSE
BEGIN
	INSERT @physicians ([PhysicianID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianID, ',');
END;


--DECLARE @PhysicianIDString VARCHAR(2000)
--SET @PhysicianIDString = @PhysicianID
---  Insert select here ----

INSErt into dbo.zzzReportMessage
		(MESSAGE)
SELECT @PhysicianID



SELECT

		  ISNULL(SUB.PhysicianID, -999) AS PhysicianID
		, SUB.PracticeName		AS Practice
		, SUB.PracticeLocation	AS Location

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
			ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
			END					AS SupplierBrand


		, SUB.SupplierShortName	AS Supplier

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
		ELSE SUB.BrandShortName
		END						AS Brand

--		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN 1
--		ELSE 0
--		END AS IsBrandSuppressed
--		, SUB.IsThirdPartySupplier
		, SUB.Sequence

		, SUB.ProductName			AS Product

		, SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender

		, SUB.Code

		, COALESCE(SUB.HCPCs, dbo.ConcatHCPCS(SUB.PracticeCatalogProductID)) AS HCPCS -- JB 20071004 Added Column for UDF Concat of HCPCS
		, case SUB.IsCashCollection
			when 1 then Sub.BillingChargeCash
			when 0 then 0
			end
			 as BillingChargeCash

		, CASE WHEN LEN(SUB.Physician) > 0 THEN SUB.Physician ELSE '<blank>' END AS Physician
		, CONVERT(VARCHAR(10), SUB.DateDispensed, 101) AS DateDispensed
		, SUB.PatientCode

		, CASE SUB.IsMedicare
			When 1 then 'Y'
			When 0 then 'N'
		  End
		  as IsMedicare
		, CASE CAST(SUB.ABNType AS varchar(50))
			When 'Medicare' then 'M' + ',' + CAST(SUB.MedicareOption AS varchar(50))
			When 'Commercial' then 'C' + ',' + CAST(SUB.MedicareOption AS varchar(50))
		  End
		  as ABNType

		, SUB.ICD9Code
		, SUB.Quantity 		--, SUM( SUB.Quantity )

--		, CAST( SUB.ActualWholesaleCost AS DECIMAL(15,2) )	AS ActualWholesaleCost
--		, CAST( SUB.ActualChargeBilled AS DECIMAL(15,2) )	AS ActualChargeBilled
--		, CAST( SUB.DMEDeposit AS DECIMAL(15,2) )			AS DMEDeposit

		, CAST(  SUB.ActualWholesaleCostLineTotal  AS DECIMAL(15,2) )	AS ActualWholesaleCostLineTotal
		, CAST(  SUB.DMEDepositLineTotal  AS DECIMAL(15,2) )			AS DMEDepositLineTotal
		, SUB.IsCashCollection
		--, CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )    AS ActualChargeBilledLineTotal
		,  case SUB.IsCashCollection
			when 1 then 0
			when 0 then CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )
			end
			AS ActualChargeBilledLineTotal

--		, SUB.Comment  -- Not Displayed for Version 1.

		, ISNULL(SUB.PayerName, 'N/A') AS PayerName
		, SUB.DispenseNote
		, SUB.OriginalActualChargeBilled
		, LTRIM(RTRIM(ISNULL(SUB.PatientFirstName,''))) AS PatientFirstName
		, LTRIM(RTRIM(ISNULL(SUB.PatientLastName,''))) AS PatientLastName
		, SUB.Mod1
		, SUB.Mod2
		, SUB.Mod3
FROM


		(
		SELECT
			  Ph.PhysicianID
			, P.PracticeName
			, PL.Name AS PracticeLocation
			, IsNULL(RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix), '') AS Physician

			, D.PatientCode

			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) AS Sequence

			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash

			, MCP.[Name] AS ProductName

			--, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender

			, ISNULL(MCP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed  -- , DD.CreatedDate AS DateDispensed  --  2008.02.12  JB  --, DD.CreatedDate AS DateDispensed
			, DD.IsMedicare
			, DD.ABNType
			, DD.ICD9Code
			, DD.Quantity
			, PI_DD.ActualWholesaleCost
			, DD.ActualChargeBilled as ActualChargeBilled
			, DD.DMEDeposit
			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit AS DMEDepositLineTotal
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.

			, PP.Name AS PayerName
			, D.Notes AS DispenseNote
			, DQ.HCPCs
			, DD.ActualChargeBilled as OriginalActualChargeBilled
			, D.PatientFirstName as PatientFirstName
			, D.PatientLastName as PatientLastName
			, DD.Mod1
			, DD.Mod2
			, DD.Mod3
			, dq.MedicareOption

		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOin DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
			ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID		    --  Can be inactive

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1

		LEFT JOIN PracticePayer PP
			on PP.PracticePayerID = D.PracticePayerID

		LEFT JOIN DispenseQueue DQ
			on DQ.DispenseQueueID = DD.DispenseQueueID

		WHERE
			P.PracticeID = @PracticeID
			AND DD.IsMedicare = Coalesce(@IsMedicare, DD.IsMedicare)
			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			--AND PCP.IsActive = 1
			--AND PCSB.IsActive = 1
			--AND MCP.IsActive = 1

		AND
		PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			FROM @locations)

--		AND
--		Ph.PhysicianID IN
--			(SELECT ID
--			 FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianIDString, ','))

		AND
			(
					PH.PhysicianID
					IN (
						 SELECT PhysicianID
						 FROM @physicians
						)
					OR
				(
					(PH.PhysicianID IS NULL)
						AND
					( 1 = (SELECT CASE WHEN PhysicianID = -999 THEN 1 ELSE 0 END --AS IsNoPhysician
					 FROM @physicians
					 WHERE PhysicianID = -999)
					)
				)
			)

		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM

		UNION ALL

		SELECT

			  Ph.PhysicianID
			, P.PracticeName
			, PL.Name AS PracticeLocation
			, IsNULL(RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix), '') AS Physician

			, D.PatientCode

			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) + 1000 AS Sequence

			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash

			, TPP.[Name] AS ProductName

			--, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender


			, ISNULL(TPP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed  -- , DD.CreatedDate AS DateDispensed  --  2008.02.12  JB
			, DD.IsMedicare
			, DD.ABNType
			, DD.ICD9Code
			, DD.Quantity
			, PI_DD.ActualWholesaleCost
			, DD.ActualChargeBilled as ActualChargeBilled
			, DD.DMEDeposit
			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit AS DMEDepositLineTotal
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.

			, PP.Name AS PayerName
			, D.Notes AS DispenseNote
			, DQ.HCPCs
			, DD.ActualChargeBilled as OriginalActualChargeBilled
			, D.PatientFirstName as PatientFirstName
			, D.PatientLastName as PatientLastName
			, DD.Mod1
			, DD.Mod2
			, DD.Mod3
			, dq.MedicareOption

		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOin DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
			ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID			--  Can be inactive

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1

		LEFT JOIN PracticePayer PP
			on PP.PracticePayerID = D.PracticePayerID

		LEFT JOIN DispenseQueue DQ
			on DQ.DispenseQueueID = DD.DispenseQueueID

		WHERE
			P.PracticeID = @PracticeID
			AND DD.IsMedicare = Coalesce(@IsMedicare, DD.IsMedicare)
			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			--AND PCP.IsActive = 1
			--AND PCSB.IsActive = 1
			--AND TPP.IsActive = 1

		AND
		PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			FROM @locations)

		AND
			(
					PH.PhysicianID
					IN (
						 SELECT PhysicianID
						 FROM @physicians
						)
					OR

				(
					(PH.PhysicianID IS NULL)
						AND
					( 1 = (SELECT CASE WHEN PhysicianID = -999 THEN 1 ELSE 0 END --AS IsNoPhysician
					 FROM @physicians
					 WHERE PhysicianID = -999)
					)
				)

--				(
--					(PH.PhysicianID IS NULL)
--						AND
--					( 1 = (SELECT CASE WHEN ISNULL(ID, -999) = -999 THEN 1 ELSE 0 END --AS IsNoPhysician
--					 FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianIDString, ',')
--					 WHERE ISNULL(ID, -999) = -999)
--					)
--				)
			)


		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM

		) AS Sub

----
------GROUP BY
------		SUB.PracticeName
------		, SUB.PracticeLocation
------		, SUB.Physician
------		, SUB.IsThirdPartySupplier
------		, SUB.Sequence
------		, SUB.SupplierShortName
------		, SUB.BrandShortName
------
------		, SUB.ProductName
------		, SUB.Side
------		, SUB.Size
------		, SUB.Gender
------		, SUB.Code
------
------		, SUB.PatientCode
------
------		, SUB.DateDispensed
------
------		, SUB.ActualWholesaleCost
------		, SUB.ActualChargeBilled
------		, Sub.DMEDeposit
------		, Sub.Comment
----
----

--if @OrderBy	= 1
--begin
	 order by

	 --/*
		CASE
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName DESC))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName))

			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName DESC))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName))

			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName DESC ))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName ))


			WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed))

			WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed))

			WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed))

		END
		--*/
/*
		CASE WHEN @OrderBy = 1 and @OrderDirection = 0  THEN SUB.ProductName End DESC,
		CASE WHEN @OrderBy = 1 and @OrderDirection = 1  THEN SUB.ProductName END,
		CASE WHEN @OrderBy = 2 and @OrderDirection = 0  THEN SUB.DateDispensed End DESC,
		CASE WHEN @OrderBy = 2 and @OrderDirection = 1  THEN SUB.DateDispensed END,
		CASE WHEN @OrderBy = 3 and @OrderDirection = 0  THEN SUB.ProductName End DESC,
		CASE WHEN @OrderBy = 3 and @OrderDirection = 1  THEN SUB.ProductName END
*/

--	if @OrderDirection = 0
--	begin
--		select ' desc'
--	end
--	CASE
--		WHEN @OrderDirection = 0 THEN ' desc'
--	END
--SUB.ProductName
--end
----	ORDER BY

----		SUB.PracticeName
----		, SUB.PracticeLocation
----
------		, SUB.Physician
----
----		, SUB.Sequence		--  Note 3rd Party has 100 added to it sequence for this query.
------		, SUB.IsThirdPartySupplier
----		, SUB.SupplierShortName
----		, SUB.BrandShortName
----
----		, SUB.ProductName
----		, SUB.Code
----
----		, SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender
----		, SUB.DateDispensed		--  JB  2008.01.09
----		, SUB.PatientCode		--  JB  2008.01.09

END
GO


--------------------------------------------------------------------------------------------------

GO
ALTER PROC [dbo].[usp_Report_ProductsDispensed_By_Physician_With_DateDispensed_PatientCode_Allowable]
		  @PracticeID			INT
		, @PracticeLocationID	VARCHAR(2000)
		, @PhysicianID			VARCHAR(2000)
		, @StartDate			DATETIME
		, @EndDate				DATETIME
		, @OrderBy				INT
		, @OrderDirection		BIT
		, @IsMedicare           BIT = NULL
		, @MedicareOrderDirection BIT = NULL
		, @InternalPracticeLocationID	VARCHAR(2000)
		, @InternalPhysicianID	VARCHAR(2000)

AS
BEGIN


SELECT @EndDate = DATEADD(DAY, 1, @EndDate)

-- Parse delimited lists of IDs
DECLARE @locations TABLE([PracticeLocationID] INT);

-- If we passed "All" for Practice Locations, get all of the
-- Practice Locations for the passed PracticeID.
-- Otherwise, parse as before.
IF(@InternalPracticeLocationID = 'ALL')
BEGIN
	DECLARE @tempLocations TABLE (
		PracticeLocationID INT
		,PracticeLocationName VARCHAR(50)
		,IsPrimaryLocation BIT
		);

	INSERT @tempLocations
	EXEC dbo.usp_PracticeLocation_Select_All_ID_Name_Given_PracticeID @PracticeID = @PracticeID;

	INSERT @locations ([PracticeLocationID])
	SELECT PracticeLocationID
	FROM @tempLocations;
END;
ELSE
BEGIN
	INSERT @locations ([PracticeLocationID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable(@PracticeLocationID, ',');
END;

-- Parse Physicians
DECLARE @physicians TABLE([PhysicianID] INT);

-- If we passed "All" for Physicians, get all of the
-- Physicians for the passed Practice Locations.
-- Otherwise, parse as before.
IF(@InternalPhysicianID = 'ALL')
BEGIN
	DECLARE @tempPhysicians TABLE (
		PracticeID INT
		,PhysicianID INT
		,ContactID INT
		,PhysicianName VARCHAR(100)
		);

	INSERT @tempPhysicians
	EXEC dbo.usp_Physician_SelectALL_Names_ByPracticeID @PracticeID = @PracticeID;

	INSERT @physicians ([PhysicianID])
	SELECT PhysicianID
	FROM @tempPhysicians;

	-- Include NULL as a valid value in "All"
	INSERT @physicians ([PhysicianID])
	SELECT NULL;
END;
ELSE
BEGIN
	INSERT @physicians ([PhysicianID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianID, ',');
END;

SELECT

		  ISNULL(SUB.PhysicianID, -999) AS PhysicianID
		, SUB.PracticeName		AS Practice
		, SUB.PracticeLocation	AS Location

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
			ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
			END					AS SupplierBrand
		, SUB.SupplierShortName	AS Supplier

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
		ELSE SUB.BrandShortName
		END						AS Brand
		, SUB.Sequence
		, SUB.ProductName			AS Product
		, SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender
		, SUB.Code
		, COALESCE(SUB.HCPCs, dbo.ConcatHCPCS(SUB.PracticeCatalogProductID)) AS HCPCS -- JB 20071004 Added Column for UDF Concat of HCPCS
		, CASE SUB.IsCashCollection
			WHEN 1 THEN Sub.BillingChargeCash
			WHEN 0 THEN 0
			END
			 AS BillingChargeCash

		, CASE WHEN LEN(SUB.Physician) > 0 THEN SUB.Physician ELSE '<blank>' END AS Physician
		, CONVERT(VARCHAR(10), SUB.DateDispensed, 101) AS DateDispensed
		, SUB.PatientCode

		, CASE SUB.IsMedicare
			WHEN 1 THEN 'Y'
			WHEN 0 THEN 'N'
		  END
		  AS IsMedicare
		, CASE CAST(SUB.ABNType AS varchar(50))
			WHEN 'Medicare' THEN 'M' + ',' + CAST(SUB.MedicareOption AS varchar(50))
			WHEN 'Commercial' THEN 'C' + ',' + CAST(SUB.MedicareOption AS varchar(50))
		  END
		  AS ABNType

		, SUB.ICD9Code
		, SUB.Quantity 		--, SUM( SUB.Quantity )
		, CAST(  SUB.ActualWholesaleCostLineTotal  AS DECIMAL(15,2) )	AS ActualWholesaleCostLineTotal
		, CAST(  SUB.DMEDepositLineTotal  AS DECIMAL(15,2) )			AS DMEDepositLineTotal
		, SUB.IsCashCollection
		,  CASE SUB.IsCashCollection
			WHEN 1 THEN 0
			WHEN 0 THEN CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )
			END
			AS ActualChargeBilledLineTotal
		, ISNULL(SUB.PayerName, 'N/A') AS PayerName
		, SUB.DispenseNote
		, SUB.OriginalActualChargeBilled
		, CAST(ISNULL(SUB.AllowableLineTotal,0)   AS DECIMAL(15,2) ) AS AllowableLineTotal
		, LTRIM(RTRIM(ISNULL(SUB.PatientFirstName,''))) AS PatientFirstName
		, LTRIM(RTRIM(ISNULL(SUB.PatientLastName,''))) AS PatientLastName
		, SUB.Mod1
		, SUB.Mod2
		, SUB.Mod3
FROM


		(
		SELECT
			  Ph.PhysicianID
			, P.PracticeName
			, PL.Name AS PracticeLocation
			, ISNULL(RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix), '') AS Physician

			, D.PatientCode

			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) AS Sequence

			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash

			, MCP.[Name] AS ProductName
			, CAST(ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender

			, ISNULL(MCP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed  -- , DD.CreatedDate AS DateDispensed  --  2008.02.12  JB  --, DD.CreatedDate AS DateDispensed
			, DD.IsMedicare
			, DD.ABNType
			, DD.ICD9Code
			, DD.Quantity
			, PI_DD.ActualWholesaleCost
			, DD.ActualChargeBilled AS ActualChargeBilled
			, DD.DMEDeposit
			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit AS DMEDepositLineTotal
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.

			, PP.Name AS PayerName
			, D.Notes AS DispenseNote
			, DQ.HCPCs
			, DD.ActualChargeBilled AS OriginalActualChargeBilled
			, allowables.allowableamountSum AS AllowableLineTotal
			, D.PatientFirstName AS PatientFirstName
			, D.PatientLastName AS PatientLastName
			, DD.Mod1
			, DD.Mod2
			, DD.Mod3
			, DQ.MedicareOption

		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOIN DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
			ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID		    --  Can be inactive

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1

		LEFT JOIN PracticePayer PP
			ON PP.PracticePayerID = D.PracticePayerID

		LEFT JOIN DispenseQueue DQ
			ON DQ.DispenseQueueID = DD.DispenseQueueID

		LEFT JOIN (SELECT  sqppa.practicepayerid,  sqph.PracticeCatalogProductID AS pcpID, SUM(sqppa.allowableamount) AS allowableAmountSum
			FROM dbo.ProductHCPCs sqph
				JOIN dbo.Hcpcs sqhcpcs ON SUBSTRING(sqph.HCPCS,1,5) = sqhcpcs.Code
				JOIN [dbo].[PracticePayerAllowable] sqppa ON sqppa.hcpcsID = sqhcpcs.hcpcsid
				JOIN dbo.practicepayer sqpp ON sqpp.practicepayerid = sqppa.practicepayerid
				GROUP BY sqppa.practicepayerid,  sqph.PracticeCatalogProductID
			  ) AS Allowables
			ON Allowables.pcpID = PCP.PracticeCatalogProductID AND Allowables.practicepayerid = PP.PracticePayerID

		WHERE
			P.PracticeID = @PracticeID
			AND DD.IsMedicare = COALESCE(@IsMedicare, DD.IsMedicare)
			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			--AND PCSB.IsActive = 1

		AND
		PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			 FROM @locations)

		AND
			(
					PH.PhysicianID
					IN (
						 SELECT PhysicianID
						 FROM @physicians
						)
					OR
				(
					(PH.PhysicianID IS NULL)
						AND
					( 1 = (SELECT CASE WHEN PhysicianID = -999 THEN 1 ELSE 0 END --AS IsNoPhysician
					 FROM @physicians
					 WHERE PhysicianID = -999)
					)
				)
			)

		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM

		UNION ALL

		SELECT

			  Ph.PhysicianID
			, P.PracticeName
			, PL.Name AS PracticeLocation
			, ISNULL(RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix), '') AS Physician
			, D.PatientCode
			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) + 1000 AS Sequence
			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash
			, TPP.[Name] AS ProductName
			, CAST(ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
			, ISNULL(TPP.Code, '') AS Code
			, D.DateDispensed AS DateDispensed  -- , DD.CreatedDate AS DateDispensed  --  2008.02.12  JB
			, DD.IsMedicare
			, DD.ABNType
			, DD.ICD9Code
			, DD.Quantity
			, PI_DD.ActualWholesaleCost
			, DD.ActualChargeBilled AS ActualChargeBilled
			, DD.DMEDeposit
			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit AS DMEDepositLineTotal
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal
			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.
			, PP.Name AS PayerName
			, D.Notes AS DispenseNote
			, DQ.HCPCs
			, DD.ActualChargeBilled AS OriginalActualChargeBilled
			, allowables.allowableamountSum AS AllowableLineTotal
			, D.PatientFirstName AS PatientFirstName
			, D.PatientLastName AS PatientLastName
			, DD.Mod1
			, DD.Mod2
			, DD.Mod3
			, DQ.MedicareOption
		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOIN DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
			ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID			--  Can be inactive

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1

		LEFT JOIN PracticePayer PP
			ON PP.PracticePayerID = D.PracticePayerID

		LEFT JOIN DispenseQueue DQ
			ON DQ.DispenseQueueID = DD.DispenseQueueID

		LEFT JOIN (SELECT  sqppa.practicepayerid,  sqph.PracticeCatalogProductID AS pcpID, SUM(sqppa.allowableamount) AS allowableAmountSum
			FROM dbo.ProductHCPCs sqph
				JOIN dbo.Hcpcs sqhcpcs ON SUBSTRING(sqph.HCPCS,1,5) = sqhcpcs.Code
				JOIN [dbo].[PracticePayerAllowable] sqppa ON sqppa.hcpcsID = sqhcpcs.hcpcsid
				JOIN dbo.practicepayer sqpp ON sqpp.practicepayerid = sqppa.practicepayerid
				GROUP BY sqppa.practicepayerid,  sqph.PracticeCatalogProductID
			  ) AS Allowables
			ON Allowables.pcpID = PCP.PracticeCatalogProductID AND Allowables.practicepayerid = PP.PracticePayerID

		WHERE
			P.PracticeID = @PracticeID
			AND DD.IsMedicare = COALESCE(@IsMedicare, DD.IsMedicare)
			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			--AND PCP.IsActive = 1
			--AND PCSB.IsActive = 1
			--AND TPP.IsActive = 1

		AND
		PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			 FROM @locations)

		AND
			(
					PH.PhysicianID
					IN (
						 SELECT PhysicianID
						 FROM @physicians
						)
					OR

				(
					(PH.PhysicianID IS NULL)
						AND
					( 1 = (SELECT CASE WHEN PhysicianID = -999 THEN 1 ELSE 0 END --AS IsNoPhysician
					 FROM @physicians
					 WHERE PhysicianID = -999)
					)
				)
			)


		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM

		) AS Sub

	 ORDER BY
		CASE
			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName DESC))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName))

			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName DESC))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName))

			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName DESC ))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName ))


			WHEN @OrderBy = 2 AND @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 AND @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed))

			WHEN @OrderBy = 2 AND @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 AND @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed))

			WHEN @OrderBy = 2 AND @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 AND @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed))

		END
END
GO

----------------------------------------------------------------------------------------------------------------
GO
--  [usp_Report_ProductsDispensed]  6, '13, 15, 16, 17, 18, 19, 20, 22, 23, 24', '1/1/2007', '12/31/2007'
--  Added Supplier Brand.
--  Added Sequence
--  Add IsActive = 1


ALTER PROC [dbo].[usp_Report_ProductsDispensed_By_Practice]
		  @PracticeID	INT
		, @StartDate		DATETIME
		, @EndDate			DATETIME
		, @OrderBy				INT
		, @OrderDirection		BIT
		, @IsMedicare           BIT = NULL
		, @MedicareOrderDirection BIT = NULL
AS
BEGIN


--  SET @EndDate to 11:59:59 PM
SELECT @EndDate = DATEADD(DAY, 1, @EndDate)




SELECT
		  SUB.PracticeName		AS Practice
		, SUB.PracticeLocation	AS Location

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
			ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
			END					AS SupplierBrand


		, SUB.SupplierShortName	AS Supplier

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
		ELSE SUB.BrandShortName
		END						AS Brand


		, SUB.Sequence

		, SUB.ProductName			AS Product

		, SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender

		, SUB.Code

		, COALESCE(SUB.HCPCs, dbo.ConcatHCPCS(SUB.PracticeCatalogProductID)) AS HCPCS -- JB 20071004 Added Column for UDF Concat of HCPCS
		, CASE SUB.IsCashCollection
			WHEN 1 THEN Sub.BillingChargeCash
			WHEN 0 THEN 0
			END
			 AS BillingChargeCash

		, SUB.Physician
		, SUB.PatientCode
		, CONVERT(VARCHAR(10), SUB.DateDispensed, 101) AS DateDispensed

		, CASE SUB.IsMedicare
			WHEN 1 THEN 'Y'
			WHEN 0 THEN 'N'
		  END
		  AS IsMedicare
		, CASE CAST(SUB.ABNType AS varchar(50))
			When 'Medicare' then 'M' + ',' + CAST(SUB.MedicareOption AS varchar(50))
			When 'Commercial' then 'C' + ',' + CAST(SUB.MedicareOption AS varchar(50))
		  End
		  as ABNType
		, SUB.ICD9Code
		, SUB.Quantity 		--, SUM( SUB.Quantity )

		, CAST(  SUB.ActualWholesaleCostLineTotal  AS DECIMAL(15,2) )	AS ActualWholesaleCostLineTotal
		, CAST(  SUB.DMEDepositLineTotal  AS DECIMAL(15,2) )			AS DMEDepositLineTotal
		, SUB.IsCashCollection
		--, CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )    AS ActualChargeBilledLineTotal
		,  CASE SUB.IsCashCollection
			WHEN 1 THEN 0
			WHEN 0 THEN CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )
			END
			AS ActualChargeBilledLineTotal

--		, SUB.Comment  -- Not Displayed for Version 1.

		, ISNULL(SUB.PayerName, 'N/A') AS PayerName
		, SUB.DispenseNote
		, SUB.OriginalActualChargeBilled
		, LTRIM(RTRIM(ISNULL(SUB.PatientFirstName,''))) AS PatientFirstName
		, LTRIM(RTRIM(ISNULL(SUB.PatientLastName,''))) AS PatientLastName
		, SUB.Mod1
		, SUB.Mod2
		, SUB.Mod3
FROM


		(
		SELECT

			  P.PracticeName
			, PL.Name AS PracticeLocation
			, ISNULL(RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix), '') AS Physician

			, D.PatientCode

			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) AS Sequence

			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash

			, MCP.[Name] AS ProductName

			--, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender

			, ISNULL(MCP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed

			, DD.IsMedicare
			, DD.ABNType
			, DD.ICD9Code
			, DD.Quantity
			, PI_DD.ActualWholesaleCost
			, DD.ActualChargeBilled AS ActualChargeBilled
			, DD.DMEDeposit
			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit AS DMEDepositLineTotal
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.

			, PP.Name AS PayerName
			, D.Notes AS DispenseNote
			, DQ.HCPCs
			, DD.ActualChargeBilled AS OriginalActualChargeBilled
			, D.PatientFirstName AS PatientFirstName
			, D.PatientLastName AS PatientLastName
			, DD.Mod1
			, DD.Mod2
			, DD.Mod3
			, DQ.MedicareOption

		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOIN DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
			ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID		    --  Can be inactive

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1

		LEFT JOIN PracticePayer PP
			ON PP.PracticePayerID = D.PracticePayerID

		LEFT JOIN DispenseQueue AS DQ
			ON DQ.DispenseQueueID = DD.DispenseQueueID

		WHERE
			    P.PracticeID = @PracticeID
			AND DD.IsMedicare = COALESCE(@IsMedicare, DD.IsMedicare)
			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			--AND PCP.IsActive = 1
			--AND PCSB.IsActive = 1
			--AND MCP.IsActive = 1


		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM

		UNION ALL

		SELECT

			 P.PracticeName
			, PL.Name AS PracticeLocation
			, C.FirstName + ' ' + C.LastName AS Physician  --  Get proper line for this!!

			, D.PatientCode

			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) + 1000 AS Sequence

			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash

			, TPP.[Name] AS ProductName

			--, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender


			, ISNULL(TPP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed

			, DD.IsMedicare
			, DD.ABNType
			, DD.ICD9Code
			, DD.Quantity
			, PI_DD.ActualWholesaleCost
			, DD.ActualChargeBilled AS ActualChargeBilled
			, DD.DMEDeposit
			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit AS DMEDepositLineTotal
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.

			, PP.Name AS PayerName
			, D.Notes AS DispenseNote
			, DQ.HCPCs
			, DD.ActualChargeBilled AS OriginalActualChargeBilled
			, D.PatientFirstName AS PatientFirstName
			, D.PatientLastName AS PatientLastName
			, DD.Mod1
			, DD.Mod2
			, DD.Mod3
			, DQ.MedicareOption

		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOIN DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
			ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID			--  Can be inactive

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1

		LEFT JOIN PracticePayer PP
			ON PP.PracticePayerID = D.PracticePayerID

		LEFT JOIN DispenseQueue AS DQ
			ON DQ.DispenseQueueID = DD.DispenseQueueID
		WHERE
			P.PracticeID = @PracticeID
			AND DD.IsMedicare = COALESCE(@IsMedicare, DD.IsMedicare)
			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			--AND PCP.IsActive = 1
			--AND PCSB.IsActive = 1
			--AND TPP.IsActive = 1


		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM

		) AS Sub


	ORDER BY

		CASE
			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.ProductName DESC,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.ProductName     ,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))

			WHEN (@OrderBy = 2				  ) AND @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.DateDispensed DESC,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))
			WHEN (@OrderBy = 2				  ) AND @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.DateDispensed     ,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))

			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.IsMedicare DESC,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.ProductName DESC,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.IsMedicare DESC,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.ProductName     ,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))

			WHEN (@OrderBy = 2				  ) AND @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.IsMedicare DESC,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.DateDispensed DESC,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))
			WHEN (@OrderBy = 2				  ) AND @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.IsMedicare DESC,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.DateDispensed     ,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))

			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.IsMedicare,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.ProductName DESC,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.IsMedicare,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.ProductName     ,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))

			WHEN (@OrderBy = 2				  ) AND @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.IsMedicare,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.DateDispensed DESC,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))
			WHEN (@OrderBy = 2				  ) AND @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.IsMedicare,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.DateDispensed     ,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))


		END
		  --SUB.PracticeName,SUB.PracticeLocation,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName

		--,SUB.ProductName
		--,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender

END
GO

----------------------------------------------------------------------
GO

ALTER PROC [dbo].[usp_Report_ProductsDispensed_CustomFit]
		  @PracticeID	INT
		, @PracticeLocationID	VARCHAR(2000)
		, @StartDate		DATETIME
		, @EndDate			DATETIME
		, @OrderBy				INT
		, @OrderDirection		BIT
		, @IsMedicare           BIT = NULL
		, @MedicareOrderDirection BIT = NULL
		, @InternalPracticeLocationID VARCHAR(2000)
AS
BEGIN
print @MedicareOrderDirection

-- Parse delimited lists of IDs
DECLARE @locations TABLE([PracticeLocationID] INT);

-- If we passed "All" for Practice Locations, get all of the
-- Practice Locations for the passed PracticeID.
-- Otherwise, parse as before.
IF(@InternalPracticeLocationID = 'ALL')
BEGIN
	DECLARE @tempLocations TABLE (
		PracticeLocationID INT
		,PracticeLocationName VARCHAR(50)
		,IsPrimaryLocation BIT
		);

	INSERT @tempLocations
	EXEC dbo.usp_PracticeLocation_Select_All_ID_Name_Given_PracticeID @PracticeID = @PracticeID;

	INSERT @locations ([PracticeLocationID])
	SELECT PracticeLocationID
	FROM @tempLocations;
END;
ELSE
BEGIN
	INSERT @locations ([PracticeLocationID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable(@PracticeLocationID, ',');
END;


SELECT @EndDate = DATEADD(DAY, 1, @EndDate);

SELECT
		  SUB.PracticeName		AS Practice
		, SUB.PracticeLocation	AS Location
		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
			ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
			END					AS SupplierBrand
		, SUB.SupplierShortName	AS Supplier
		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
		ELSE SUB.BrandShortName
		END						AS Brand
		, SUB.Sequence
		, SUB.ProductName			AS Product
		, SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender
		, SUB.Code
		, COALESCE(SUB.HCPCs, dbo.ConcatHCPCS(SUB.PracticeCatalogProductID)) AS HCPCS -- JB 20071004 Added Column for UDF Concat of HCPCS
		, CASE SUB.IsCashCollection
			WHEN 1 THEN Sub.BillingChargeCash
			WHEN 0 THEN 0
			END
			 AS BillingChargeCash
		, SUB.Physician
		, SUB.Fitter
		, SUB.PatientCode
		, Sub.DispenseID
		, CONVERT(VARCHAR(10), SUB.DateDispensed, 101) AS DateDispensed
		, CASE SUB.IsMedicare
			WHEN 1 THEN 'Y'
			WHEN 0 THEN 'N'
		  END
		  AS IsMedicare
		, CASE CAST(SUB.ABNType AS varchar(50))
			When 'Medicare' then 'M' + ',' + CAST(SUB.MedicareOption AS varchar(50))
			When 'Commercial' then 'C' + ',' + CAST(SUB.MedicareOption AS varchar(50))
		  End
		  as ABNType
		  , SUB.ICD9Code
		  , SUB.Quantity 		--, SUM( SUB.Quantity )
		, CAST(  SUB.ActualWholesaleCostLineTotal  AS DECIMAL(15,2) )	AS ActualWholesaleCostLineTotal
		, CAST(  SUB.DMEDepositLineTotal  AS DECIMAL(15,2) )			AS DMEDepositLineTotal

		, SUB.IsCashCollection
		,  CASE SUB.IsCashCollection
			WHEN 1 THEN 0
			WHEN 0 THEN CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )
			END
			AS ActualChargeBilledLineTotal

		, ISNULL(SUB.PayerName, 'N/A') AS PayerName
		, SUB.DispenseNote
		, SUB.OriginalActualChargeBilled
		, LTRIM(RTRIM(ISNULL(SUB.PatientFirstName,''))) AS PatientFirstName
		, LTRIM(RTRIM(ISNULL(SUB.PatientLastName,''))) AS PatientLastName
		, SUB.Mod1
		, SUB.Mod2
		, SUB.Mod3
FROM
		(
		SELECT

			  P.PracticeName
			, PL.Name AS PracticeLocation
			, ISNULL(RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix), '') AS Physician
			, ISNULL(RTRIM( FitterContact.FirstName + ' ' + FitterContact.LastName + ' ' + FitterContact.Suffix), '') AS Fitter

			, D.PatientCode
			, D.DispenseID

			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) AS Sequence

			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash

			, MCP.[Name] AS ProductName

			--, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender

			, ISNULL(MCP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed	--, DD.CreatedDate AS DateDispensed  --JB 2008.02.12
			, DD.IsMedicare
			, DD.ABNType
			, DD.ICD9Code
			, DD.Quantity
			, PI_DD.ActualWholesaleCost
			, DD.ActualChargeBilled AS ActualChargeBilled
			, DD.DMEDeposit
			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit AS DMEDepositLineTotal -- * DD.Quantity removed to match patient receipt.  In Report re-write use DMEDeposit
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.

			, PP.Name AS PayerName
			, D.Notes AS DispenseNote
			, DD.ActualChargeBilled AS OriginalActualChargeBilled
			, D.PatientFirstName AS PatientFirstName
			, D.PatientLastName AS PatientLastName
			, DQ.HCPCs
			, DD.Mod1
			, DD.Mod2
			, DD.Mod3
			, DQ.MedicareOption
		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOIN DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
			ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID		    --  Can be inactive

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1

		LEFT JOIN PracticePayer PP
			ON PP.PracticePayerID = D.PracticePayerID

		LEFT JOIN DispenseQueue AS DQ
			ON DQ.DispenseQueueID = DD.DispenseQueueID

		LEFT JOIN dbo.Clinician AS Fitter
			ON DQ.FitterID = Fitter.ClinicianID		    --  Can be inactive

		LEFT JOIN Contact AS FitterContact
			ON FitterContact.ContactID = Fitter.ContactID
			AND FitterContact.IsActive = 1

		WHERE
			P.PracticeID = @PracticeID
			AND DD.IsMedicare = COALESCE(@IsMedicare, DD.IsMedicare)
			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			--AND PCP.IsActive = 1
			--AND PCSB.IsActive = 1
			--AND MCP.IsActive = 1
			AND DQ.IsCustomFit = 1

		AND
		PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			 FROM @locations)

		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM

		UNION ALL

		SELECT

			 P.PracticeName
			, PL.Name AS PracticeLocation
			, ISNULL(RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix), '') AS Physician
			, ISNULL(RTRIM( FitterContact.FirstName + ' ' + FitterContact.LastName + ' ' + FitterContact.Suffix), '') AS Fitter

			, D.PatientCode
			, D.DispenseID

			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) + 1000 AS Sequence

			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash

			, TPP.[Name] AS ProductName

			, CAST(ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender


			, ISNULL(TPP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed	--, DD.CreatedDate AS DateDispensed  --JB 2008.02.12
			, DD.IsMedicare
			, DD.ABNType
			, DD.ICD9Code
			, DD.Quantity
			, PI_DD.ActualWholesaleCost
			, DD.ActualChargeBilled AS ActualChargeBilled
			, DD.DMEDeposit

			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit AS DMEDepositLineTotal -- * DD.Quantity removed to match patient receipt.  In Report re-write use DMEDeposit
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.

			, PP.Name AS PayerName
			, D.Notes AS DispenseNote
			, DD.ActualChargeBilled AS OriginalActualChargeBilled
			, D.PatientFirstName AS PatientFirstName
			, D.PatientLastName AS PatientLastName
			, DQ.HCPCs
			, DD.Mod1
			, DD.Mod2
			, DD.Mod3
			, DQ.MedicareOption
		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOIN DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
			ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID			--  Can be inactive

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1

		LEFT JOIN PracticePayer PP
			ON PP.PracticePayerID = D.PracticePayerID

		LEFT JOIN DispenseQueue AS DQ
			ON DQ.DispenseQueueID = DD.DispenseQueueID

		LEFT JOIN Clinician AS Fitter
			ON DQ.FitterID = Fitter.ClinicianID		    --  Can be inactive

		LEFT JOIN Contact AS FitterContact
			ON FitterContact.ContactID = Fitter.ContactID
			AND FitterContact.IsActive = 1

		WHERE
			P.PracticeID = @PracticeID
			AND DD.IsMedicare = COALESCE(@IsMedicare, DD.IsMedicare)
			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			--AND PCP.IsActive = 1
			--AND PCSB.IsActive = 1
			--AND TPP.IsActive = 1
			AND DQ.IsCustomFit = 1

		AND
		PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			 FROM @locations)

		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM

		) AS Sub

	 ORDER BY
	 --/*
		CASE
			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName DESC))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName))

			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName DESC))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName))

			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName DESC ))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName ))


			WHEN @OrderBy = 2 AND @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 AND @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed))

			WHEN @OrderBy = 2 AND @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 AND @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed))

			WHEN @OrderBy = 2 AND @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 AND @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed))

		END
		--*/
END
GO


----------------------------------------------------------------------------------------------------------------------------------------
GO
--exec usp_Report_ProductShrinkage 1,3, '1', '112,111,110,109,108'

ALTER PROC [dbo].[usp_Report_ProductShrinkage]
		  @PracticeID	INT
		, @PracticeLocationID int
		, @SupplierBrandID  VARCHAR(2000)   --  ID String.
		, @InventoryCycleID VARCHAR(2000)
		, @InternalSupplierBrandID VARCHAR(2000)
		, @InternalInventoryCycleID VARCHAR(2000)

AS
BEGIN
/*
Declare @StartOfCycleDate DateTime
Declare @StartDate DateTime
Declare @EndDate DateTime

Select @EndDate = ICE.EndDate from InventoryCycle ICE where ICE.InventoryCycleID = @InventoryCycleID
--Print @EndDate

Select @StartOfCycleDate = ICE1.StartDate from InventoryCycle ICE1 where ICE1.InventoryCycleID = @InventoryCycleID

Select TOP 1 @StartDate =  IC2.EndDate
						from InventoryCycle IC2
						where
							IC2.InventoryCycleID < @InventoryCycleID
							AND IC2.PracticeLocationID = @PracticeLocationID
						Order By IC2.InventoryCycleID DESC


SELECT @EndDate = DATEADD(DAY, 1, @EndDate)
Print 'StartDate'
print @StartDate
Print 'EndDate'
Print @EndDate
*/

-- Parse the Supplier Brand
DECLARE @supplierBrands TABLE([SupplierBrandID] INT);

-- If the Internal parameter is "ALL", we select all of the Supplier Brands
-- for the passed PracticeID.
-- Otherwise, split as we were doing before.
IF(@InternalSupplierBrandID = 'ALL')
BEGIN
	DECLARE @tempSupplierBrands TABLE (PracticeCatelogSupplierBrandID INT, SupplierBrand VARCHAR(1000), SupplierName VARCHAR(1000), BrandName VARCHAR(1000), IsThirdPartySupplier BIT, DerivedSequence INT);

	INSERT @tempSupplierBrands
	EXEC dbo.usp_Report_GetPracticeCatalogSupplierBrands_Formatted @PracticeID = @PracticeID;

	INSERT @supplierBrands ([SupplierBrandID])
	SELECT PracticeCatelogSupplierBrandID
	FROM @tempSupplierBrands;
END;
ELSE
BEGIN
	INSERT @supplierBrands ([SupplierBrandID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable(@SupplierBrandID, ',');
END;

-- Parse the Inventory Cycle
DECLARE @inventoryCycles TABLE([InventoryCycleID] INT);

-- If the Internal parameter is "ALL", we select all of the Inventory Cycles
-- for the passed PracticeLocationID.
-- Otherwise, split as we were doing before.
IF(@InternalInventoryCycleID = 'ALL')
BEGIN
	DECLARE @tempInventoryCycles TABLE (EndDate DATETIME, InventoryCycleID INT, StartDate DATETIME, CycleTitle VARCHAR(1000));

	INSERT @tempInventoryCycles
	EXEC dbo.usp_Report_GetInventoryCycles @PracticeLocationID = @PracticeLocationID;

	INSERT @inventoryCycles ([InventoryCycleID])
	SELECT InventoryCycleID
	FROM @tempInventoryCycles;
END;
ELSE
BEGIN
	INSERT @inventoryCycles ([InventoryCycleID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable(@InventoryCycleID, ',');
END;

SELECT
	PI.ProductInventoryID,
	S.InventoryCycleID,
	Code = COALESCE(MCP.Code, TPP.Code, 'N/A'),
	S.OldQuantityOnHandPerSystem,
	S.NewQuantityOnHandPerSystem,
	S.QuantityOfShrinkage,
	TotalCost = 0,
	PI.QuantityOnHandPerSystem,
	PCSB.SupplierShortName,
	PCSB.BrandShortName,
	SupplierBrand = PCSB.SupplierShortName + '(' + PCSB.BrandShortName + ')',
	Product = COALESCE(MCP.Name, TPP.Name, 'N/A'),  --COALESCE(MCP., TPP., 'N/A')
	Side = COALESCE(MCP.LeftRightSide, TPP.LeftRightSide, 'N/A'),
	Size = COALESCE(MCP.Size, TPP.Size, 'N/A'),
	Gender = COALESCE(MCP.Gender, TPP.Gender, 'N/A'),
	SideSizeGender = (COALESCE(MCP.LeftRightSide, TPP.LeftRightSide, 'N/A')  + ', ' + COALESCE(MCP.Size, TPP.Size, 'N/A') + ', ' + COALESCE(MCP.Gender, TPP.Gender, 'N/A')),
	HCPCsString = dbo.ConcatHCPCS(PCP.PracticeCatalogProductID),
	Sequence = ISNULL(PCSB.Sequence, 999),
	ActualWholesaleCost = (PCP.WholesaleCost / PCP.StockingUnits),
	PracticeLocation = pl.Name,
	PracticeName = P.PracticeName,
	PI.ConsignmentQuantity,
	PI.ParLevel,
	StartOfCycleDate = Convert(Varchar, IC.StartDate) + (CASE WHEN IC.Title IS NULL THEN '' ELSE '-' + IC.Title END),
	StartDate= IC.StartDate,
	Title = IC.Title,

	-- OnHand
	WholesaleCostOnHand = (Convert(money,PCP.WholesaleCost) / PCP.StockingUnits) * PI.QuantityOnHandPerSystem,

	--Purchased
	WholesaleCostPurchased = Coalesce((Select (SUM(SOLI.ActualWholesaleCost) / PCP.StockingUnits) -- This should be divided by stocking units
							  From dbo.SupplierOrderLineItem AS SOLI	WITH (NOLOCK)
							  	INNER JOIN dbo.SupplierOrder AS SO	WITH (NOLOCK)
									ON SOLI.SupplierOrderID = SO.SupplierOrderID
								INNER JOIN dbo.BregVisionOrder AS BVO	WITH (NOLOCK)
									ON BVO.BregVisionOrderID = SO.BregVisionOrderID
								Where SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
								AND BVO.PracticeLocationID = PL.PracticeLocationID
								Group By SOLI.PracticeCatalogProductID
							  ), 0),


	--ProductsDispensed
	WholesaleCostDispensed = Coalesce((Select (SUM(PI_DD.ActualWholesaleCost) / PCP.StockingUnits)
								FROM Dispense AS D  WITH (NOLOCK)
								INNER JOin DispenseDetail AS DD  WITH (NOLOCK)
									ON D.DispenseID = DD.DispenseID

								INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
									ON DD.DispenseDetailID = PI_DD.DispenseDetailID
								Where
								D.PracticeLocationID = PL.PracticeLocationID
								AND PI_DD.ProductInventoryID = PI.ProductInventoryID
								AND D.IsActive = 1
								AND DD.IsActive = 1
								AND PI_DD.IsActive = 1
								Group By PI_DD.ProductInventoryID
							  ), 0),

	--Shrinkage
	WholesaleCostShrinkage = (S.QuantityOfShrinkage * (PCP.WholesaleCost / PCP.StockingUnits)),

	--Total Cost
	TotalSalesRetail =		0   /* 		 Coalesce((Select SUM(DD.LineTotal)
								FROM DispenseDetail AS DD  WITH (NOLOCK)

								INNER JOin Dispense AS D  WITH (NOLOCK)
									ON DD.DispenseID = D.DispenseID

								INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
									ON DD.DispenseDetailID = PI_DD.DispenseDetailID
								Where
									PI_DD.ProductInventoryID = PI.ProductInventoryID
									AND D.PracticeLocationID = @PracticeLocationID
									AND DD.IsActive = 1
									AND D.IsActive = 1
									AND PI_DD.IsActive = 1
									AND PI_DD.CreatedDate >= @StartDate
									AND PI_DD.CreatedDate < @EndDate
								Group By PI_DD.ProductInventoryID
							  ), 0)	*/
	, pi.IsConsignment
	From dbo.ProductInventory_ResetQuantityOnHandPerSystem as S  with (NOLOCK)

	Inner Join dbo.ProductInventory as PI with (NOLOCK) on S.ProductInventoryID = PI.ProductInventoryID

	INNER JOIN dbo.PracticeCatalogProduct AS PCP	WITH (NOLOCK)
		on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID

	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON PI.PracticeLocationID = PL.PracticeLocationID

	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID

	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

	LEFT OUTER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
		ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID AND PCP.IsThirdPartyProduct = 0

	LEFT OUTER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
		ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID AND PCP.IsThirdPartyProduct = 1

	Inner Join InventoryCycle IC
		ON S.InventoryCycleID = IC.InventoryCycleID

Where
		--S.InventoryCycleID = @InventoryCycleID
		--AND
		S.OldQuantityOnHandPerSystem <> S.NewQuantityOnHandPerSystem
		AND P.PracticeID = @PracticeID
		AND PL.PracticeLocationID = @PracticeLocationID
		--AND P.IsActive = 1
		--AND PL.IsActive = 1
		--AND PCP.IsActive = 1
		and PI.IsSetToActive=1
		and PI.IsActive=1
		--AND (Coalesce(MCP.IsActive, TPP.IsActive) = 1)

		AND S.InventoryCycleID IN
			(Select InventoryCycleID
			 FROM @inventoryCycles )

		AND PCSB.PracticeCatalogSupplierBrandID IN
			(SELECT SupplierBrandID
			 FROM @supplierBrands)


		Order By S.InventoryCycleID Desc,  COALESCE(MCP.Code, TPP.Code, 'N/A')

End
GO


--------------------------------------------------------------------------------------
GO
/*

	EXEC [usp_Report_ProductsOnHand] 6, '13, 15, 16, 17, 18, 19, 20, 22, 23, 24', '34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46'
	EXEC  [usp_Report_ProductsOnHand] 62, '187, 186, 188 ,190 ,189', '857'

*/


ALTER PROC [dbo].[usp_Report_ProductsOnHand]
		  @PracticeID	INT
		, @PracticeLocationID VARCHAR(2000)
		, @SupplierBrandID  VARCHAR(2000)   --  ID String.
		, @InternalPracticeLocationID VARCHAR(2000)
		, @InternalSupplierBrandID VARCHAR(2000)

AS
BEGIN

-- Parse the Practice Location
DECLARE @locations TABLE ([PracticeLocationID] INT);
DECLARE @PracticeLocationIDString VARCHAR(2000);

-- If the Internal parameter is "ALL", we select all of the Practice Locations
-- for the passed PracticeID.
-- Otherwise, split as we were doing before.
IF(@InternalPracticeLocationID = 'ALL')
BEGIN
	DECLARE @tempLocations TABLE (
		PracticeLocationID INT
		,PracticeLocationName VARCHAR(50)
		,IsPrimaryLocation BIT
		);

	INSERT @tempLocations
	EXEC dbo.usp_PracticeLocation_Select_All_ID_Name_Given_PracticeID @PracticeID = @PracticeID;

	INSERT @locations ([PracticeLocationID])
	SELECT PracticeLocationID
	FROM @tempLocations;

	SELECT @PracticeLocationIDString = COALESCE(@PracticeLocationIDString + CAST(PracticeLocationID AS NVARCHAR(10)) + ',', '')
			FROM @locations;

	IF(LEN(@PracticeLocationIDString) > 0)
		SET @PracticeLocationIDString = LEFT(@PracticeLocationIDString, LEN(@PracticeLocationIDString) - 1);
END;
ELSE
BEGIN
	INSERT @locations ([PracticeLocationID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable(@PracticeLocationID, ',');

	SET @PracticeLocationIDString = @PracticeLocationID;
END;

-- Parse the Supplier Brand
DECLARE @supplierBrands TABLE([SupplierBrandID] INT);

-- If the Internal parameter is "ALL", we select all of the Supplier Brands
-- for the passed PracticeID.
-- Otherwise, split as we were doing before.
IF(@InternalSupplierBrandID = 'ALL')
BEGIN
	DECLARE @tempSupplierBrands TABLE (PracticeCatelogSupplierBrandID INT, SupplierBrand VARCHAR(1000), SupplierName VARCHAR(1000), BrandName VARCHAR(1000), IsThirdPartySupplier BIT, DerivedSequence INT);

	INSERT @tempSupplierBrands
	EXEC dbo.usp_Report_GetPracticeCatalogSupplierBrands_Formatted @PracticeID = @PracticeID;

	INSERT @supplierBrands ([SupplierBrandID])
	SELECT PracticeCatelogSupplierBrandID
	FROM @tempSupplierBrands;
END;
ELSE
BEGIN
	INSERT @supplierBrands ([SupplierBrandID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable(@SupplierBrandID, ',');
END;

SELECT
		  SUB.PracticeName
		, SUB.PracticeLocation

		, SUB.PracticeCatalogSupplierBrandID

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
			ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
			END					AS SupplierBrand

		, SUB.SupplierShortName	AS Supplier

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
		ELSE SUB.BrandShortName
		END						AS Brand

		, SUB.Sequence

		, SUB.ProductName			AS Product
		, SUB.Side + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender
		, SUB.Code


		, SUM(SUB.QuantityOnHandPerSystem)					AS QuantityOnHandPerSystem
		, SUB.ConsignmentQuantity							AS ConsignmentQuantity
		, SUB.ParLevel										AS ParLevel
		, SUB.ReorderLevel									AS ReorderLevel
		, SUB.CriticalLevel									AS CriticalLevel
		, CAST(SUB.WholesaleCost AS DECIMAL(15,2) )			AS ActualWholesaleCost
		, SUB.SuggestedReorderLevel
		, CAST( SUM(SUB.LineTotal)  AS DECIMAL(15,2) )		AS TotalCost
		, dbo.ConcatHCPCS(Sub.PracticeCatalogProductID) +
		 CASE WHEN dbo.ConcatHCPCSOTS(Sub.PracticeCatalogProductID) <> '' THEN
			'/ ' + dbo.ConcatHCPCSOTS(Sub.PracticeCatalogProductID)
			ELSE ''
			END

			AS HCPCSString
		, (SUB.ParLevel - SUM(SUB.QuantityOnHandPerSystem))	AS SuggestedOrderQuantity
		, SUB.IsConsignment
FROM
	(SELECT
		  P.PracticeName
		, PL.NAME													AS PracticeLocation

		, PCSB.IsThirdPartySupplier

		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierShortName
		, PCSB.BrandShortName

		, ISNULL(PCSB.Sequence, 999)								AS Sequence
		, PCP.PracticeCatalogProductID

		, MCP.[Name] AS ProductName
		, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) )		AS Side
		, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )				AS Size
		, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
		, ISNULL(MCP.Code, '')										AS Code

		, (PCP.WholesaleCost / PCP.StockingUnits) as WholesaleCost
		, PI.QuantityOnHandPerSystem
		, PI.ParLevel
		, PI.ReorderLevel
		, PI.CriticalLevel
		, Levels.SuggestedReorderLevel
		, (Convert(money,PCP.WholesaleCost) / PCP.StockingUnits) * PI.QuantityOnHandPerSystem as LineTotal
		, PI.ConsignmentQuantity
		, PI.IsConsignment


	From dbo.ProductInventory as PI with (NOLOCK)

	INNER JOIN dbo.PracticeCatalogProduct AS PCP	WITH (NOLOCK)
		on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID

	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON PI.PracticeLocationID = PL.PracticeLocationID

	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID

	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

	INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
		ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

	Left Outer JOIN dbo.udf_GetSuggestedReorder_Given_PracticeLocationID_String( @PracticeLocationIDString )	AS Levels
				ON PCP.PracticeCatalogProductID = Levels.PracticeCatalogProductID

	WHERE
		P.PracticeID = @PracticeID
    --AND P.IsActive = 1
		--AND PL.IsActive = 1
		--AND PCP.IsActive = 1
		--AND PCSB.IsActive = 1
		--AND MCP.IsActive = 1
		and PI.IsSetToActive=1
		and PI.IsActive=1

		AND PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			 FROM @locations)

		AND PCSB.PracticeCatalogSupplierBrandID IN
			(SELECT SupplierBrandID
			 FROM @supplierBrands)


	UNION

		SELECT
		  P.PracticeName
		, PL.NAME													AS PracticeLocation

		, PCSB.IsThirdPartySupplier

		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierShortName
		, PCSB.BrandShortName

		, ISNULL(PCSB.Sequence, 999) + 1000							AS Sequence
		, PCP.PracticeCatalogProductID

		, TPP.[Name]												AS ProductName
		, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) )		AS Side
		, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )				AS Size
		, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
		, ISNULL(TPP.Code, '')										AS Code

		, (PCP.WholesaleCost / PCP.StockingUnits) as WholesaleCost
		, PI.QuantityOnHandPerSystem
		, PI.ParLevel
		, PI.ReorderLevel
		, PI.CriticalLevel
		, Levels.SuggestedReorderLevel
		, (Convert(money,PCP.WholesaleCost) / PCP.StockingUnits) * PI.QuantityOnHandPerSystem			as LineTotal
		, PI.ConsignmentQuantity
		, PI.IsConsignment


	From dbo.ProductInventory as PI with (NOLOCK)

	INNER JOIN dbo.PracticeCatalogProduct AS PCP	WITH (NOLOCK)
		on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID

	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON PI.PracticeLocationID = PL.PracticeLocationID

	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID

	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

	INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
		ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

	Left Outer JOIN dbo.udf_GetSuggestedReorder_Given_PracticeLocationID_String( @PracticeLocationIDString )	AS Levels
		ON PCP.PracticeCatalogProductID = Levels.PracticeCatalogProductID

	WHERE
		P.PracticeID = @PracticeID
    --AND P.IsActive = 1
		--AND PL.IsActive = 1
		--AND PCP.IsActive = 1
		--AND PCSB.IsActive = 1
		--AND TPP.IsActive = 1
		and PI.IsSetToActive=1
		and PI.IsActive=1

		AND PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			 FROM @locations)

		AND PCSB.PracticeCatalogSupplierBrandID IN
			(SELECT SupplierBrandID
			 FROM @supplierBrands )
	)
		AS SUB

	GROUP BY
		  SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.PracticeCatalogSupplierBrandID
		, SUB.IsThirdPartySupplier
		, SUB.Sequence
		, SUB.PracticeCatalogProductID
		, SUB.SupplierShortName
		, SUB.BrandShortName

		, SUB.ProductName
		, SUB.Side
		, SUB.Size
		, SUB.Gender
		, SUB.Code

		, SUB.WholesaleCost
		, SUB.ConsignmentQuantity
		, SUB.ParLevel
		, SUB.ReorderLevel
		, SUB.CriticalLevel
		, SUB.SuggestedReorderLevel
		, SUB.IsConsignment

	ORDER BY
		  SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.Sequence
		, SUB.SupplierShortName
		, SUB.BrandShortName
		, SUB.ProductName
		, SUB.Code
		, SUB.Side + ', ' + SUB.Size  + ', ' + SUB.Gender

END
GO


---------------------------------------------------------------------------
GO
/*

	EXEC [usp_Report_ProductsOnHand_PracticeReport] 6, '13, 15, 16, 17, 18, 19, 20, 22, 23, 24', '34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46'
	EXEC  [usp_Report_ProductsOnHand_PracticeReport] 62, '187, 186, 188 ,190 ,189', '857'

*/


ALTER PROC [dbo].[usp_Report_ProductsOnHand_PracticeReport]
		  @PracticeID	INT
		, @PracticeLocationID VARCHAR(2000)
		, @SupplierBrandID  VARCHAR(2000)   --  ID String.
		, @InternalPracticeLocationID VARCHAR(2000)
		, @InternalSupplierBrandID VARCHAR(2000)

AS
BEGIN

-- Parse the Practice Location
DECLARE @locations TABLE ([PracticeLocationID] INT);
DECLARE @PracticeLocationIDString VARCHAR(2000);

-- If the Internal parameter is "ALL", we select all of the Practice Locations
-- for the passed PracticeID.
-- Otherwise, split as we were doing before.
IF(@InternalPracticeLocationID = 'ALL')
BEGIN
	DECLARE @tempLocations TABLE (
		PracticeLocationID INT
		,PracticeLocationName VARCHAR(50)
		,IsPrimaryLocation BIT
		);

	INSERT @tempLocations
	EXEC dbo.usp_PracticeLocation_Select_All_ID_Name_Given_PracticeID @PracticeID = @PracticeID;

	INSERT @locations ([PracticeLocationID])
	SELECT PracticeLocationID
	FROM @tempLocations;

	SELECT @PracticeLocationIDString = COALESCE(@PracticeLocationIDString + CAST(PracticeLocationID AS NVARCHAR(10)) + ',', '')
			FROM @locations;

	IF(LEN(@PracticeLocationIDString) > 0)
		SET @PracticeLocationIDString = LEFT(@PracticeLocationIDString, LEN(@PracticeLocationIDString) - 1);
END;
ELSE
BEGIN
	INSERT @locations ([PracticeLocationID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable(@PracticeLocationID, ',');

	SET @PracticeLocationIDString = @PracticeLocationID;
END;

-- Parse the Supplier Brand
DECLARE @supplierBrands TABLE([SupplierBrandID] INT);

-- If the Internal parameter is "ALL", we select all of the Supplier Brands
-- for the passed PracticeID.
-- Otherwise, split as we were doing before.
IF(@InternalSupplierBrandID = 'ALL')
BEGIN
	DECLARE @tempSupplierBrands TABLE (PracticeCatelogSupplierBrandID INT, SupplierBrand VARCHAR(1000), SupplierName VARCHAR(1000), BrandName VARCHAR(1000), IsThirdPartySupplier BIT, DerivedSequence INT);

	INSERT @tempSupplierBrands
	EXEC dbo.usp_Report_GetPracticeCatalogSupplierBrands_Formatted @PracticeID = @PracticeID;

	INSERT @supplierBrands ([SupplierBrandID])
	SELECT PracticeCatelogSupplierBrandID
	FROM @tempSupplierBrands;
END;
ELSE
BEGIN
	INSERT @supplierBrands ([SupplierBrandID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable(@SupplierBrandID, ',');
END;

SELECT
		  --SUB.PracticeName
		--, SUB.PracticeLocation

		--,
		  SUB.PracticeCatalogSupplierBrandID

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
			ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
			END					AS SupplierBrand

		, SUB.SupplierShortName	AS Supplier

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
		ELSE SUB.BrandShortName
		END						AS Brand

		, SUB.Sequence

		, SUB.ProductName			AS Product
		, SUB.Side + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender
		, SUB.Code


		, SUM(SUB.QuantityOnHandPerSystem)					AS QuantityOnHandPerSystem
		, SUM(SUB.ConsignmentQuantity)						AS ConsignmentQuantity
		, SUM(SUB.ParLevel)										AS ParLevel
		, SUM(SUB.ReorderLevel	)								AS ReorderLevel
		, SUM(SUB.CriticalLevel	)								AS CriticalLevel
		, CAST(SUB.WholesaleCost AS DECIMAL(15,2) )			AS ActualWholesaleCost
		, SUB.SuggestedReorderLevel
		, CAST( SUM(SUB.LineTotal)  AS DECIMAL(15,2) )		AS TotalCost
		, dbo.ConcatHCPCS(Sub.PracticeCatalogProductID) +
			 CASE WHEN dbo.ConcatHCPCSOTS(Sub.PracticeCatalogProductID) <> '' THEN
				'/ ' + dbo.ConcatHCPCSOTS(Sub.PracticeCatalogProductID)
				ELSE ''
				END											AS HCPCSString
		, SUB.IsConsignment

FROM
	(SELECT
		  P.PracticeName
		, PL.NAME													AS PracticeLocation

		, PCSB.IsThirdPartySupplier

		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierShortName
		, PCSB.BrandShortName

		, ISNULL(PCSB.Sequence, 999)								AS Sequence
		, PCP.PracticeCatalogProductID

		, MCP.[Name] AS ProductName
		, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) )		AS Side
		, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )				AS Size
		, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
		, ISNULL(MCP.Code, '')										AS Code

		, (PCP.WholesaleCost / PCP.StockingUnits) as WholesaleCost
		, PI.QuantityOnHandPerSystem
		, PI.ConsignmentQuantity
		, PI.ParLevel
		, PI.ReorderLevel
		, PI.CriticalLevel
		, Levels.SuggestedReorderLevel
		, (Convert(money,PCP.WholesaleCost) / PCP.StockingUnits) * PI.QuantityOnHandPerSystem as LineTotal
		, PI.IsConsignment


	From dbo.ProductInventory as PI with (NOLOCK)

	INNER JOIN dbo.PracticeCatalogProduct AS PCP	WITH (NOLOCK)
		on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID

	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON PI.PracticeLocationID = PL.PracticeLocationID

	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID

	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

	INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
		ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

	Left Outer JOIN dbo.udf_GetSuggestedReorder_Given_PracticeLocationID_String( @PracticeLocationIDString )	AS Levels
				ON PCP.PracticeCatalogProductID = Levels.PracticeCatalogProductID

	WHERE
		P.PracticeID = @PracticeID
    --AND P.IsActive = 1
		--AND PL.IsActive = 1
		--AND PCP.IsActive = 1
		--AND PCSB.IsActive = 1
		--AND MCP.IsActive = 1
		and PI.IsSetToActive=1
		and PI.IsActive=1

		AND PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			 FROM @locations)

		AND PCSB.PracticeCatalogSupplierBrandID IN
			(SELECT SupplierBrandID
			 FROM @supplierBrands)


	UNION

		SELECT
		  P.PracticeName
		, PL.NAME													AS PracticeLocation

		, PCSB.IsThirdPartySupplier

		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierShortName
		, PCSB.BrandShortName

		, ISNULL(PCSB.Sequence, 999) + 1000							AS Sequence
		, PCP.PracticeCatalogProductID

		, TPP.[Name]												AS ProductName
		, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) )		AS Side
		, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )				AS Size
		, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
		, ISNULL(TPP.Code, '')										AS Code

		, (PCP.WholesaleCost / PCP.StockingUnits) as WholesaleCost
		, PI.QuantityOnHandPerSystem
		, PI.ConsignmentQuantity
		, PI.ParLevel
		, PI.ReorderLevel
		, PI.CriticalLevel
		, Levels.SuggestedReorderLevel
		, (Convert(money,PCP.WholesaleCost) / PCP.StockingUnits) * PI.QuantityOnHandPerSystem			as LineTotal
		, PI.IsConsignment


	From dbo.ProductInventory as PI with (NOLOCK)

	INNER JOIN dbo.PracticeCatalogProduct AS PCP	WITH (NOLOCK)
		on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID

	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON PI.PracticeLocationID = PL.PracticeLocationID

	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID

	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

	INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
		ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

	Left Outer JOIN dbo.udf_GetSuggestedReorder_Given_PracticeLocationID_String( @PracticeLocationIDString )	AS Levels
		ON PCP.PracticeCatalogProductID = Levels.PracticeCatalogProductID

	WHERE
		P.PracticeID = @PracticeID
    --AND P.IsActive = 1
		--AND PL.IsActive = 1
		--AND PCP.IsActive = 1
		--AND PCSB.IsActive = 1
		--AND TPP.IsActive = 1
		and PI.IsSetToActive=1
		and PI.IsActive=1

		AND PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			 FROM @locations)

		AND PCSB.PracticeCatalogSupplierBrandID IN
			(SELECT SupplierBrandID
			 FROM @supplierBrands )
	)
		AS SUB

	GROUP BY
		  --SUB.PracticeName
		--, SUB.PracticeLocation
		--,
		  SUB.PracticeCatalogSupplierBrandID
		, SUB.IsThirdPartySupplier
		, SUB.Sequence
		, SUB.PracticeCatalogProductID
		, SUB.SupplierShortName
		, SUB.BrandShortName

		, SUB.ProductName
		, SUB.Side
		, SUB.Size
		, SUB.Gender
		, SUB.Code

		, SUB.WholesaleCost
		--, SUB.ParLevel
		--, SUB.ReorderLevel
		--, SUB.CriticalLevel
		, SUB.SuggestedReorderLevel
		, SUB.IsConsignment


	ORDER BY
		 --SUB.PracticeName
		--, SUB.PracticeLocation
		--,
		  SUB.Sequence
		, SUB.SupplierShortName
		, SUB.BrandShortName
		, SUB.ProductName
		, SUB.Code
		, SUB.Side + ', ' + SUB.Size  + ', ' + SUB.Gender

END
GO


------------------------------------------------------------------------------------
GO
/*

	EXEC [usp_Report_ProductsOnHand] 6, '13, 15, 16, 17, 18, 19, 20, 22, 23, 24', '34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46'
	EXEC  [usp_Report_ProductsOnHand] 62, '187, 186, 188 ,190 ,189', '857'

*/


ALTER PROC [dbo].[usp_Report_ProductsOnHand_With_Supplier]
		  @PracticeID	INT
		, @PracticeLocationID VARCHAR(2000)
		, @SupplierBrandID  VARCHAR(2000)   --  ID String.
		, @InternalPracticeLocationID VARCHAR(2000)
		, @InternalSupplierBrandID VARCHAR(2000)

AS
BEGIN


-- Parse the Practice Location
DECLARE @locations TABLE ([PracticeLocationID] INT);
DECLARE @PracticeLocationIDString VARCHAR(2000);

-- If the Internal parameter is "ALL", we select all of the Practice Locations
-- for the passed PracticeID.
-- Otherwise, split as we were doing before.
IF(@InternalPracticeLocationID = 'ALL')
BEGIN
	DECLARE @tempLocations TABLE (
		PracticeLocationID INT
		,PracticeLocationName VARCHAR(50)
		,IsPrimaryLocation BIT
		);

	INSERT @tempLocations
	EXEC dbo.usp_PracticeLocation_Select_All_ID_Name_Given_PracticeID @PracticeID = @PracticeID;

	INSERT @locations ([PracticeLocationID])
	SELECT PracticeLocationID
	FROM @tempLocations;

	SELECT @PracticeLocationIDString = COALESCE(@PracticeLocationIDString + CAST(PracticeLocationID AS NVARCHAR(10)) + ',', '')
			FROM @locations;

	IF(LEN(@PracticeLocationIDString) > 0)
		SET @PracticeLocationIDString = LEFT(@PracticeLocationIDString, LEN(@PracticeLocationIDString) - 1);
END;
ELSE
BEGIN
	INSERT @locations ([PracticeLocationID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable(@PracticeLocationID, ',');

	SET @PracticeLocationIDString = @PracticeLocationID;
END;

-- Parse the Supplier Brand
DECLARE @supplierBrands TABLE([SupplierBrandID] INT);

-- If the Internal parameter is "ALL", we select all of the Supplier Brands
-- for the passed PracticeID.
-- Otherwise, split as we were doing before.
IF(@InternalSupplierBrandID = 'ALL')
BEGIN
	DECLARE @tempSupplierBrands TABLE (PracticeCatelogSupplierBrandID INT, SupplierBrand VARCHAR(1000), SupplierName VARCHAR(1000), BrandName VARCHAR(1000), IsThirdPartySupplier BIT, DerivedSequence INT);

	INSERT @tempSupplierBrands
	EXEC dbo.usp_Report_GetPracticeCatalogSupplierBrands_Formatted @PracticeID = @PracticeID;

	INSERT @supplierBrands ([SupplierBrandID])
	SELECT PracticeCatelogSupplierBrandID
	FROM @tempSupplierBrands;
END;
ELSE
BEGIN
	INSERT @supplierBrands ([SupplierBrandID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable(@SupplierBrandID, ',');
END;

SELECT
		  SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.PracticeCatalogSupplierBrandID
		, SUB.SupplierShortName	AS Supplier
		, SUB.BrandShortName AS Brand
		, SUB.Sequence
		, SUB.ProductName			AS Product
		, SUB.Side + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender
		, SUB.Code


		, SUM(SUB.QuantityOnHandPerSystem)					AS QuantityOnHandPerSystem
		, SUB.ConsignmentQuantity							AS ConsignmentQuantity
		, SUB.ParLevel										AS ParLevel
		, SUB.ReorderLevel									AS ReorderLevel
		, SUB.CriticalLevel									AS CriticalLevel
		, CAST(SUB.WholesaleCost AS DECIMAL(15,2) )			AS ActualWholesaleCost
		, SUB.SuggestedReorderLevel
		, CAST( SUM(SUB.LineTotal)  AS DECIMAL(15,2) )		AS TotalCost
		, dbo.ConcatHCPCS(Sub.PracticeCatalogProductID) +
		 CASE WHEN dbo.ConcatHCPCSOTS(Sub.PracticeCatalogProductID) <> '' THEN
			'/ ' + dbo.ConcatHCPCSOTS(Sub.PracticeCatalogProductID)
			ELSE ''
			END

			AS HCPCSString
		, (SUB.ParLevel - SUM(SUB.QuantityOnHandPerSystem))	AS SuggestedOrderQuantity
		, SUB.IsConsignment

FROM
	(SELECT
		  P.PracticeName
		, PL.NAME													AS PracticeLocation

		, PCSB.IsThirdPartySupplier

		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierShortName
		, PCSB.BrandShortName

		, ISNULL(PCSB.Sequence, 999)								AS Sequence
		, PCP.PracticeCatalogProductID

		, MCP.[Name] AS ProductName
		, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) )		AS Side
		, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )				AS Size
		, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
		, ISNULL(MCP.Code, '')										AS Code

		, (PCP.WholesaleCost / PCP.StockingUnits) as WholesaleCost
		, PI.QuantityOnHandPerSystem
		, PI.ConsignmentQuantity
		, PI.ParLevel
		, PI.ReorderLevel
		, PI.CriticalLevel
		, Levels.SuggestedReorderLevel
		, (Convert(money,PCP.WholesaleCost) / PCP.StockingUnits) * PI.QuantityOnHandPerSystem as LineTotal
		, PI.IsConsignment


	From dbo.ProductInventory as PI with (NOLOCK)

	INNER JOIN dbo.PracticeCatalogProduct AS PCP	WITH (NOLOCK)
		on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID

	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON PI.PracticeLocationID = PL.PracticeLocationID

	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID

	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

	INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
		ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

	Left Outer JOIN dbo.udf_GetSuggestedReorder_Given_PracticeLocationID_String( @PracticeLocationIDString )	AS Levels
				ON PCP.PracticeCatalogProductID = Levels.PracticeCatalogProductID

	WHERE
		P.PracticeID = @PracticeID
    --AND P.IsActive = 1
		--AND PL.IsActive = 1
		--AND PCP.IsActive = 1
		--AND PCSB.IsActive = 1
		--AND MCP.IsActive = 1
		and PI.IsSetToActive=1
		and PI.IsActive=1

		AND PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			 FROM @locations)

		AND PCSB.PracticeCatalogSupplierBrandID IN
			(SELECT SupplierBrandID
			 FROM @supplierBrands )


	UNION

		SELECT
		  P.PracticeName
		, PL.NAME													AS PracticeLocation

		, PCSB.IsThirdPartySupplier

		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierShortName
		, PCSB.BrandShortName

		, ISNULL(PCSB.Sequence, 999) + 1000							AS Sequence
		, PCP.PracticeCatalogProductID

		, TPP.[Name]												AS ProductName
		, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) )		AS Side
		, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )				AS Size
		, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
		, ISNULL(TPP.Code, '')										AS Code

		, (PCP.WholesaleCost / PCP.StockingUnits) as WholesaleCost
		, PI.QuantityOnHandPerSystem
		, PI.ConsignmentQuantity
		, PI.ParLevel
		, PI.ReorderLevel
		, PI.CriticalLevel
		, Levels.SuggestedReorderLevel
		, (Convert(money,PCP.WholesaleCost) / PCP.StockingUnits) * PI.QuantityOnHandPerSystem			as LineTotal
		, PI.IsConsignment


	From dbo.ProductInventory as PI with (NOLOCK)

	INNER JOIN dbo.PracticeCatalogProduct AS PCP	WITH (NOLOCK)
		on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID

	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON PI.PracticeLocationID = PL.PracticeLocationID

	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID

	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

	INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
		ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

	Left Outer JOIN dbo.udf_GetSuggestedReorder_Given_PracticeLocationID_String( @PracticeLocationIDString )	AS Levels
		ON PCP.PracticeCatalogProductID = Levels.PracticeCatalogProductID

	WHERE
		P.PracticeID = @PracticeID
    --AND P.IsActive = 1
		--AND PL.IsActive = 1
		--AND PCP.IsActive = 1
		--AND PCSB.IsActive = 1
		--AND TPP.IsActive = 1
		and PI.IsSetToActive=1
		and PI.IsActive=1

		AND PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			 FROM @locations)

		AND PCSB.PracticeCatalogSupplierBrandID IN
			(SELECT SupplierBrandID
			 FROM @supplierBrands )
	)
		AS SUB

	GROUP BY
		  SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.PracticeCatalogSupplierBrandID
		, SUB.IsThirdPartySupplier
		, SUB.Sequence
		, SUB.PracticeCatalogProductID
		, SUB.SupplierShortName
		, SUB.BrandShortName

		, SUB.ProductName
		, SUB.Side
		, SUB.Size
		, SUB.Gender
		, SUB.Code

		, SUB.WholesaleCost
		, SUB.ConsignmentQuantity
		, SUB.ParLevel
		, SUB.ReorderLevel
		, SUB.CriticalLevel
		, SUB.SuggestedReorderLevel
		, SUB.IsConsignment

	ORDER BY
		  SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.Sequence
		, SUB.SupplierShortName
		, SUB.BrandShortName
		, SUB.ProductName
		, SUB.Code
		, SUB.Side + ', ' + SUB.Size  + ', ' + SUB.Gender

END
GO


---------------------------------------------------------------------------------
GO
/*

	DECLARE @Output VARCHAR(2000)
	--SET @Output = ''
	--
	--SELECT @Output =
	--	CASE WHEN @Output = '' THEN CAST(PracticeCatalogSupplierBrandID AS VARCHAR(50))
	--		 ELSE @Output + ', ' + CAST(PracticeCatalogSupplierBrandID AS VARCHAR(50))
	--		 END
	--FROM PracticeCatalogSupplierBrand
	--WHERE PracticeID = 6
	--and isactive = 1
	--
	--SELECT @Output AS Output

	Modification:  JB 2007.12.15
			Restructure query to work as redefined by Bobby.


	EXEC [usp_Report_ProductsPurchased] 6, '13, 15, 16, 17, 18, 19, 20, 22, 23, 24', '34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 74, 75, 76, 81, 82, 83, 99, 101'
			, '1/1/2007', '12/31/2007'

*/


ALTER PROC [dbo].[usp_Report_ProductsPurchased]
		  @PracticeID	INT
		, @PracticeLocationID VARCHAR(2000)
		, @SupplierBrandID  VARCHAR(2000)		--  ID String.
		, @StartDate		DATETIME
		, @EndDate			DATETIME
		, @InternalPracticeLocationID VARCHAR(2000)
		, @InternalSupplierBrandID VARCHAR(2000)
AS
BEGIN

SELECT @EndDate = DATEADD(DAY, 1, @EndDate)

-- Parse the Practice Location
DECLARE @locations TABLE ([PracticeLocationID] INT);

-- If the Internal parameter is "ALL", we select all of the Practice Locations
-- for the passed PracticeID.
-- Otherwise, split as we were doing before.
IF(@InternalPracticeLocationID = 'ALL')
BEGIN
	DECLARE @tempLocations TABLE (
		PracticeLocationID INT
		,PracticeLocationName VARCHAR(50)
		,IsPrimaryLocation BIT
		);

	INSERT @tempLocations
	EXEC dbo.usp_PracticeLocation_Select_All_ID_Name_Given_PracticeID @PracticeID = @PracticeID;

	INSERT @locations ([PracticeLocationID])
	SELECT PracticeLocationID
	FROM @tempLocations;
END;
ELSE
BEGIN
	INSERT @locations ([PracticeLocationID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable(@PracticeLocationID, ',');
END;

-- Parse the Supplier Brand
DECLARE @supplierBrands TABLE([SupplierBrandID] INT);

-- If the Internal parameter is "ALL", we select all of the Supplier Brands
-- for the passed PracticeID.
-- Otherwise, split as we were doing before.
IF(@InternalSupplierBrandID = 'ALL')
BEGIN
	DECLARE @tempSupplierBrands TABLE (PracticeCatelogSupplierBrandID INT, SupplierBrand VARCHAR(1000), SupplierName VARCHAR(1000), BrandName VARCHAR(1000), IsThirdPartySupplier BIT, DerivedSequence INT);

	INSERT @tempSupplierBrands
	EXEC dbo.usp_Report_GetPracticeCatalogSupplierBrands_Formatted @PracticeID = @PracticeID;

	INSERT @supplierBrands ([SupplierBrandID])
	SELECT PracticeCatelogSupplierBrandID
	FROM @tempSupplierBrands;
END;
ELSE
BEGIN
	INSERT @supplierBrands ([SupplierBrandID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable(@SupplierBrandID, ',');
END;


SELECT
		 SUB.PracticeName
		, SUB.PracticeLocation

		, SUB.PracticeCatalogSupplierBrandID

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
			ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
			END					AS SupplierBrand

		, SUB.SupplierShortName	AS Supplier

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
		ELSE SUB.BrandShortName
		END						AS Brand


		, SUB.ProductName			AS Product
		, SUB.Side + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender
		, SUB.Code


		, CAST(SUB.ActualWholesaleCost AS DECIMAL(15,2) )	AS ActualWholesaleCost
		, SUM(SUB.QuantityOrdered)							AS QuantityOrdered
		, CAST( SUM(SUB.LineTotal)  AS DECIMAL(15,2) )		AS TotalCost

		, SUB.Sequence

--		, SUB.OrderDate
FROM
	(SELECT
		  P.PracticeName
		, PL.NAME													AS PracticeLocation

		, PCSB.IsThirdPartySupplier

		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierShortName
		, PCSB.BrandShortName

		, ISNULL(PCSB.Sequence, 999)								AS Sequence
		, PCP.PracticeCatalogProductID

		, MCP.[Name] AS ProductName
		, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) )		AS Side
		, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )				AS Size
		, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
		, ISNULL(MCP.Code, '')										AS Code

		, SOLI.ActualWholesaleCost
		, SOLI.QuantityOrdered
		, SOLI.LineTotal

		, SOLI.CreatedDate AS OrderDate

	FROM dbo.SupplierOrderLineItem AS SOLI WITH (NOLOCK)

	INNER JOIN dbo.SupplierOrder AS SO	WITH (NOLOCK)
		ON SOLI.SupplierOrderID = SO.SupplierOrderID

	INNER JOIN dbo.BregVisionOrder AS BVO	WITH (NOLOCK)
		ON BVO.BregVisionOrderID = SO.BregVisionOrderID

	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON BVO.PracticeLocationID = PL.PracticeLocationID

	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID

	INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
		ON SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID

	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

	INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
		ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

	WHERE
		P.PracticeID = @PracticeID
		--AND P.IsActive = 1
		AND SOLI.IsActive = 1
		AND SO.IsActive = 1
		AND BVO.IsActive = 1
		--AND PL.IsActive = 1
		--AND PCP.IsActive = 1
		--AND PCSB.IsActive = 1
		--AND MCP.IsActive = 1

		AND
		PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			 FROM @locations)

		AND PCSB.PracticeCatalogSupplierBrandID IN
			(SELECT SupplierBrandID
			 FROM @supplierBrands )

		AND SOLI.CreatedDate >= @StartDate
		AND SOLI.CreatedDate < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM

	UNION

		SELECT
		  P.PracticeName
		, PL.NAME													AS PracticeLocation

		, PCSB.IsThirdPartySupplier

		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierShortName
		, PCSB.BrandShortName

		, ISNULL(PCSB.Sequence, 999) + 1000								AS Sequence
		, PCP.PracticeCatalogProductID

		, TPP.[Name] AS ProductName
		, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) )		AS Side
		, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )				AS Size
		, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
		, ISNULL(TPP.Code, '')										AS Code

		, SOLI.ActualWholesaleCost
		, SOLI.QuantityOrdered
		, SOLI.LineTotal

		, SOLI.CreatedDate AS OrderDate


	FROM dbo.SupplierOrderLineItem AS SOLI	WITH (NOLOCK)

	INNER JOIN dbo.SupplierOrder AS SO	WITH (NOLOCK)
		ON SOLI.SupplierOrderID = SO.SupplierOrderID

	INNER JOIN dbo.BregVisionOrder AS BVO	WITH (NOLOCK)
		ON BVO.BregVisionOrderID = SO.BregVisionOrderID

	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON BVO.PracticeLocationID = PL.PracticeLocationID

	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID

	INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
		ON SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID

	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

	INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
		ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

	WHERE
		P.PracticeID = @PracticeID
		--AND P.IsActive = 1
		AND SOLI.IsActive = 1
		AND SO.IsActive = 1
		AND BVO.IsActive = 1
		--AND PL.IsActive = 1
		--AND PCP.IsActive = 1
		--AND PCSB.IsActive = 1
		--AND TPP.IsActive = 1

		AND PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			 FROM @locations)

		AND PCSB.PracticeCatalogSupplierBrandID IN
			(SELECT SupplierBrandID
			 FROM @supplierBrands )

		AND SOLI.CreatedDate >= @StartDate
		AND SOLI.CreatedDate < @EndDate --  Less than since @EndDate has one day added to it since the time is 12AM

	)
		AS SUB

	GROUP BY
		SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.PracticeCatalogSupplierBrandID
		, SUB.IsThirdPartySupplier
		, SUB.Sequence
		, SUB.SupplierShortName
		, SUB.BrandShortName

		, SUB.ProductName
		, SUB.Side
		, SUB.Size
		, SUB.Gender
		, SUB.Code

		, SUB.ActualWholesaleCost

	ORDER BY
		SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.IsThirdPartySupplier
		, SUB.Sequence
		, SUB.SupplierShortName
		, SUB.BrandShortName
		, SUB.ProductName
		, SUB.Code

END
GO


---------------------------------------------

GO
-- =============================================
-- Author:		<Author: Greg Ross>
-- Create date: <Create Date: 10/28/08>
-- Description:	<Description: Used for the Patient Code search in the dispensement modification section of the admin pages>
-- =============================================
ALTER PROCEDURE [dbo].[usp_SearchForDispensementsAll]

	@PracticeLocationID int,
	@SearchText varchar(30)
AS
BEGIN


			SELECT
				  Sub.BrandShortName
				, Sub.DispenseID
				, Sub.PatientCode
				, Sub.PatientEmail
				, SUB.Total
				, SUB.DateDispensed			--.PracticeCatalogSupplierBrandID
				, SUB.DispenseDetailID
				, SUB.PhysicianName
				, SUB.PhysicianID
				, SUB.ActualChargeBilled
				, SUB.DMEDeposit
				, SUB. Quantity
				, SUB.LineTotal
				, SUB.Product
				, SUB.Code

				, ISNULL(SUB.Packaging, '') AS Packaging
				, ISNULL(SUB.LeftRightSide, '') AS Side
				, ISNULL(SUB.Size, '') AS Size
				, ISNULL(SUB.Gender, '') AS Gender
				, ISNULL(SUB.PatientFirstName,'') As PatientFirstName
				, ISNULL(SUB.PatientLastName,'') As PatientLastName
				, SUB.Mod1
				, SUB.Mod2
				, SUB.Mod3

			FROM
			(



				select PCSB.BrandShortName
					, D.DispenseID
					, D.PatientCode
					, D.PatientEmail
					, D.Total
					, D.DateDispensed
					, DD.DispenseDetailID
					, C.FirstName + ' '  + C.LastName as PhysicianName
					, P.PhysicianID
					, DD.ActualChargeBilled
					, DD.DMEDeposit
					, DD.Quantity
					, DD.LineTotal
 				    , MCP.[Name]			AS Product
                    , MCP.[Code]			AS Code
                    , MCP.[Packaging]		AS Packaging
                    --, MCP.[LeftRightSide] AS LeftRightSide
                    , DD.LRModifier			AS LeftRightSide
                    , MCP.[Size]			AS Size
                    , MCP.[Gender]		AS Gender
					, D.PatientFirstName
					, D.PatientLastName
					, DD.Mod1
					, DD.Mod2
					, DD.Mod3

				from dispense D

					inner join Dispensedetail DD
						on DD.DispenseID = D.DispenseID
					inner join PracticeCatalogPRoduct PCP
						on PCP.PracticeCatalogProductID=DD.PracticeCatalogProductID
                    INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
						ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
					inner join MasterCatalogProduct MCP
						on MCP.mastercatalogproductid=PCP.mastercatalogproductid
					inner join Physician P
						on DD.PhysicianID = P.PhysicianID
					inner join Contact C
						on P.ContactID = C.ContactID

				where D.practicelocationid= @PracticeLocationID
					AND PCP.IsThirdPartyProduct = 0  -- MasterCatalogProductsOnly
					--AND PCSB.IsActive =	1
					--AND PCP.IsActive = 1
					--AND MCP.IsActive = 1
					--and P.IsActive = 1
					--and C.IsActive = 1
					and D.IsActive = 1
					and DD.IsActive = 1

union

				select PCSB.BrandShortName
					, D.DispenseID
					, D.PatientCode
					, D.PatientEmail
					, D.Total
					, D.DateDispensed
					, DD.DispenseDetailID
					, C.FirstName + ' '  + C.LastName as PhysicianName
					, P.PhysicianID
					, DD.ActualChargeBilled
					, DD.DMEDeposit
					, DD.Quantity
					, DD.LineTotal
 				    , TPP.[Name]			AS Product
                    , TPP.[Code]			AS Code
                    , TPP.[Packaging]		AS Packaging
                    --, TPP.[LeftRightSide] AS LeftRightSide
                    , DD.LRModifier			AS LeftRightSide
                    , TPP.[Size]			AS Size
                    , TPP.[Gender]		AS Gender
					, D.PatientFirstName
					, D.PatientLastName
					, DD.Mod1
					, DD.Mod2
					, DD.Mod3

				from dispense D

					inner join Dispensedetail DD
						on DD.DispenseID = D.DispenseID
					inner join PracticeCatalogPRoduct PCP
						on PCP.PracticeCatalogProductID=DD.PracticeCatalogProductID
                    INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
						ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
					inner join ThirdPartyProduct TPP
						on TPP.ThirdPartyProductid=PCP.ThirdPartyProductid
					inner join Physician P
						on DD.PhysicianID = P.PhysicianID
					inner join Contact C
						on P.ContactID = C.ContactID

				where D.practicelocationid= @PracticeLocationID
					AND PCP.IsThirdPartyProduct = 1  -- ThirdPartyProductsOnly
					--AND PCSB.IsActive =	1
					--AND PCP.IsActive = 1
					--AND TPP.IsActive = 1
					--and P.IsActive = 1
					--and C.IsActive = 1
					and D.IsActive = 1
					and DD.IsActive = 1


			) AS SUB
				Order By
				  --Sub.IsThirdPartyProduct
				 Sub.PatientCode
--				, Sub.PracticeCatalogProductID
--				, SUB.Product
--				, SUB.Code

END
GO

------------------------------------------
GO
-- =============================================
-- Author:		<Author: Greg Ross>
-- Create date: <Create Date: 10/28/08>
-- Description:	<Description: Used for the Patient Code search in the dispensement modification section of the admin pages>
-- Exec [usp_SearchForDispensementsWithDispenseDate] 3, '12/1/2009'
-- =============================================
ALTER PROCEDURE [dbo].[usp_SearchForDispensementsWithDispenseDate]

	@PracticeLocationID int,
	@SearchText varchar(50)


AS
BEGIN

declare @SearchText2 datetime

set @SearchText2 =  cast(@SearchText as DateTime)

--print @SearchText2


			SELECT
				  Sub.BrandShortName
				, Sub.DispenseID
				, Sub.PatientCode
				, Sub.PatientEmail

				, SUB.Total
				, SUB.DateDispensed			--.PracticeCatalogSupplierBrandID
				, SUB.DispenseDetailID
				, SUB.PhysicianName
				, SUB.PhysicianID
				, SUB.ActualChargeBilled
				, SUB.DMEDeposit
				, SUB. Quantity
				, SUB.LineTotal
				, SUB.Product
				, SUB.Code

				, ISNULL(SUB.Packaging, '') AS Packaging
				, ISNULL(SUB.LeftRightSide, '') AS Side
				, ISNULL(SUB.Size, '') AS Size
				, ISNULL(SUB.Gender, '') AS Gender
				, ISNULL(SUB.PatientFirstName,'') As PatientFirstName
				, ISNULL(SUB.PatientLastName,'') As PatientLastName
				, SUB.Mod1
				, SUB.Mod2
				, SUB.Mod3

			FROM
			(



				select PCSB.BrandShortName
					, D.DispenseID
					, D.PatientCode
					, D.PatientEmail
					, D.Total
					, D.DateDispensed
					, DD.DispenseDetailID
					, C.FirstName + ' '  + C.LastName as PhysicianName
					, P.PhysicianID
					, DD.ActualChargeBilled
					, DD.DMEDeposit
					, DD.Quantity
					, DD.LineTotal
 				    , MCP.[Name]			AS Product
                    , MCP.[Code]			AS Code
                    , MCP.[Packaging]		AS Packaging
                    --, MCP.[LeftRightSide] AS LeftRightSide
                    , DD.LRModifier			AS LeftRightSide
                    , MCP.[Size]			AS Size
                    , MCP.[Gender]		AS Gender
					, D.PatientFirstName
					, D.PatientLastName
					, DD.Mod1
					, DD.Mod2
					, DD.Mod3

				from dispense D

					inner join Dispensedetail DD
						on DD.DispenseID = D.DispenseID
					inner join PracticeCatalogPRoduct PCP
						on PCP.PracticeCatalogProductID=DD.PracticeCatalogProductID
                    INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
						ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
					inner join MasterCatalogProduct MCP
						on MCP.mastercatalogproductid=PCP.mastercatalogproductid
					inner join Physician P
						on DD.PhysicianID = P.PhysicianID
					inner join Contact C
						on P.ContactID = C.ContactID

				where D.practicelocationid= @PracticeLocationID
					and D.DateDispensed >= dateadd(day,0,@SearchText2)
					and D.DateDispensed < dateadd(day,1,@SearchText2)
					AND PCP.IsThirdPartyProduct = 0  -- MasterCatalogProductsOnly
					--AND PCSB.IsActive =	1
					--AND PCP.IsActive = 1
					--AND MCP.IsActive = 1
					--and P.IsActive = 1
					--and C.IsActive = 1
					and D.IsActive = 1
					and DD.IsActive = 1

union

				select PCSB.BrandShortName
					, D.DispenseID
					, D.PatientCode
					, D.PatientEmail
					, D.Total
					, D.DateDispensed
					, DD.DispenseDetailID
					, C.FirstName + ' '  + C.LastName as PhysicianName
					, P.PhysicianID
					, DD.ActualChargeBilled
					, DD.DMEDeposit
					, DD.Quantity
					, DD.LineTotal
 				    , TPP.[Name]			AS Product
                    , TPP.[Code]			AS Code
                    , TPP.[Packaging]		AS Packaging
                    --, TPP.[LeftRightSide] AS LeftRightSide
                    , DD.LRModifier			AS LeftRightSide
                    , TPP.[Size]			AS Size
                    , TPP.[Gender]		AS Gender
					, D.PatientFirstName
					, D.PatientLastName
					, DD.Mod1
					, DD.Mod2
					, DD.Mod3

				from dispense D

					inner join Dispensedetail DD
						on DD.DispenseID = D.DispenseID
					inner join PracticeCatalogPRoduct PCP
						on PCP.PracticeCatalogProductID=DD.PracticeCatalogProductID
                    INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
						ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
					inner join ThirdPartyProduct TPP
						on TPP.ThirdPartyProductid=PCP.ThirdPartyProductid
					inner join Physician P
						on DD.PhysicianID = P.PhysicianID
					inner join Contact C
						on P.ContactID = C.ContactID

				where D.practicelocationid= @PracticeLocationID
					and D.DateDispensed >= dateadd(day,0,@SearchText2)
					and D.DateDispensed < dateadd(day,1,@SearchText2)
					AND PCP.IsThirdPartyProduct = 1  -- ThirdPartyProductsOnly
					--AND PCSB.IsActive =	1
					--AND PCP.IsActive = 1
					--AND TPP.IsActive = 1
					--and P.IsActive = 1
					--and C.IsActive = 1
					and D.IsActive = 1
					and DD.IsActive = 1


			) AS SUB
				Order By
				  --Sub.IsThirdPartyProduct
				 Sub.PhysicianName
--				, Sub.PracticeCatalogProductID
--				, SUB.Product
--				, SUB.Code

END
GO

-----------------------------------------

GO
-- =============================================
-- Author:		<Author: Greg Ross>
-- Create date: <Create Date: 10/28/08>
-- Description:	<Description: Used for the Patient Code search in the dispensement modification section of the admin pages>
-- =============================================
ALTER PROCEDURE [dbo].[usp_SearchForDispensementsWithPatientCode]

	@PracticeLocationID int,
	@PatientCodeSearchText varchar(30) = '',
	@PatientNameSearchText varchar(30) = ''
AS
BEGIN

if @PatientCodeSearchText <> ''
begin
	set @PatientCodeSearchText = '%' + @PatientCodeSearchText + '%'
end

if @PatientNameSearchText <> ''
begin
	set @PatientNameSearchText = '%' + @PatientNameSearchText + '%'
end

			SELECT
				  Sub.BrandShortName
				, Sub.DispenseID
				, Sub.PatientCode
				, Sub.PatientEmail

				, SUB.Total
				, SUB.DateDispensed			--.PracticeCatalogSupplierBrandID
				, SUB.DispenseDetailID
				, SUB.PhysicianName
				, SUB.PhysicianID
				, SUB.ActualChargeBilled
				, SUB.DMEDeposit
				, SUB. Quantity
				, SUB.LineTotal
				, SUB.Product
				, SUB.Code

				, ISNULL(SUB.Packaging, '') AS Packaging
				, ISNULL(SUB.LeftRightSide, '') AS Side
				, ISNULL(SUB.Size, '') AS Size
				, ISNULL(SUB.Gender, '') AS Gender
				, ISNULL(SUB.PatientFirstName,'') As PatientFirstName
				, ISNULL(SUB.PatientLastName,'') As PatientLastName

				,SUB.Mod1
				,SUB.Mod2
				,SUB.Mod3

			FROM
			(



				select PCSB.BrandShortName
					, D.DispenseID
					, D.PatientCode
					, D.PatientEmail
					, D.Total
					, D.DateDispensed
					, DD.DispenseDetailID
					, C.FirstName + ' '  + C.LastName as PhysicianName
					, P.PhysicianID
					, DD.ActualChargeBilled
					, DD.DMEDeposit
					, DD.Quantity
					, DD.LineTotal
 				    , MCP.[Name]			AS Product
                    , MCP.[Code]			AS Code
                    , MCP.[Packaging]		AS Packaging
                    --, MCP.[LeftRightSide] AS LeftRightSide
                    , DD.LRModifier			AS LeftRightSide
                    , MCP.[Size]			AS Size
                    , MCP.[Gender]		AS Gender
					, D.PatientFirstName
					, D.PatientLastName
					, DD.Mod1
					, DD.Mod2
					, DD.Mod3
				from dispense D

					inner join Dispensedetail DD
						on DD.DispenseID = D.DispenseID
					inner join PracticeCatalogPRoduct PCP
						on PCP.PracticeCatalogProductID=DD.PracticeCatalogProductID
                    INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
						ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
					inner join MasterCatalogProduct MCP
						on MCP.mastercatalogproductid=PCP.mastercatalogproductid
					inner join Physician P
						on DD.PhysicianID = P.PhysicianID
					inner join Contact C
						on P.ContactID = C.ContactID

				where D.practicelocationid= @PracticeLocationID
					and (D.PatientCode <> '' and D.PatientCode like @PatientCodeSearchText
						or (
								(d.PatientFirstName <> '' and d.PatientFirstName like @PatientNameSearchText)
							 or
								(d.PatientLastName <> '' and d.PatientLastName like @PatientNameSearchText ))
							 )
					AND PCP.IsThirdPartyProduct = 0  -- MasterCatalogProductsOnly
					--AND PCSB.IsActive =	1
					--AND PCP.IsActive = 1
					--AND MCP.IsActive = 1
					--and P.IsActive = 1
					--and C.IsActive = 1
					and D.IsActive = 1
					and DD.IsActive = 1

union

				select PCSB.BrandShortName
					, D.DispenseID
					, D.PatientCode
					, D.PatientEmail
					, D.Total
					, D.DateDispensed
					, DD.DispenseDetailID
					, C.FirstName + ' '  + C.LastName as PhysicianName
					, P.PhysicianID
					, DD.ActualChargeBilled
					, DD.DMEDeposit
					, DD.Quantity
					, DD.LineTotal
 				    , TPP.[Name]			AS Product
                    , TPP.[Code]			AS Code
                    , TPP.[Packaging]		AS Packaging
                    --, TPP.[LeftRightSide] AS LeftRightSide
                    , DD.LRModifier			AS LeftRightSide
                    , TPP.[Size]			AS Size
                    , TPP.[Gender]		AS Gender
					, D.PatientFirstName
					, D.PatientLastName
					, DD.Mod1
					, DD.Mod2
					, DD.Mod3
				from dispense D

					inner join Dispensedetail DD
						on DD.DispenseID = D.DispenseID
					inner join PracticeCatalogPRoduct PCP
						on PCP.PracticeCatalogProductID=DD.PracticeCatalogProductID
                    INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
						ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
					inner join ThirdPartyProduct TPP
						on TPP.ThirdPartyProductid=PCP.ThirdPartyProductid
					inner join Physician P
						on DD.PhysicianID = P.PhysicianID
					inner join Contact C
						on P.ContactID = C.ContactID

				where D.practicelocationid= @PracticeLocationID
					and (D.PatientCode <> '' and D.PatientCode like @PatientCodeSearchText
						or (
								(d.PatientFirstName <> '' and d.PatientFirstName like @PatientNameSearchText)
							 or
								(d.PatientLastName <> '' and d.PatientLastName like @PatientNameSearchText ))
							 )
					AND PCP.IsThirdPartyProduct = 1  -- ThirdPartyProductsOnly
					--AND PCSB.IsActive =	1
					--AND PCP.IsActive = 1
					--AND TPP.IsActive = 1
					--and P.IsActive = 1
					--and C.IsActive = 1
					and D.IsActive = 1
					and DD.IsActive = 1


			) AS SUB
				Order By
				  --Sub.IsThirdPartyProduct
				 Sub.PatientCode
--				, Sub.PracticeCatalogProductID
--				, SUB.Product
--				, SUB.Code

END
GO



-----------------------------------------
GO
-- =============================================
-- Author:		<Author: Greg Ross>
-- Create date: <Create Date: 10/28/08>
-- Description:	<Description: Used for the Patient Code search in the dispensement modification section of the admin pages>
-- =============================================
ALTER PROCEDURE [dbo].[usp_SearchForDispensementsWithPhysicianName]

	@PracticeLocationID int,
	@SearchText varchar(50)
AS
BEGIN

set @SearchText = '%' + @SearchText + '%'


			SELECT
				  Sub.BrandShortName
				, Sub.DispenseID
				, Sub.PatientCode
				, Sub.PatientEmail

				, SUB.Total
				, SUB.DateDispensed			--.PracticeCatalogSupplierBrandID
				, SUB.DispenseDetailID
				, SUB.PhysicianName
				, SUB.PhysicianID
				, SUB.ActualChargeBilled
				, SUB.DMEDeposit
				, SUB. Quantity
				, SUB.LineTotal
				, SUB.Product
				, SUB.Code

				, ISNULL(SUB.Packaging, '') AS Packaging
				, ISNULL(SUB.LeftRightSide, '') AS Side
				, ISNULL(SUB.Size, '') AS Size
				, ISNULL(SUB.Gender, '') AS Gender
				, ISNULL(SUB.PatientFirstName,'') As PatientFirstName
				, ISNULL(SUB.PatientLastName,'') As PatientLastName
				, SUB.Mod1
				, SUB.Mod2
				, SUB.Mod3

			FROM
			(



				select PCSB.BrandShortName
					, D.DispenseID
					, D.PatientCode
					, D.PatientEmail
					, D.Total
					, D.DateDispensed
					, DD.DispenseDetailID
					, C.FirstName + ' '  + C.LastName as PhysicianName
					, P.PhysicianID
					, DD.ActualChargeBilled
					, DD.DMEDeposit
					, DD.Quantity
					, DD.LineTotal
 				    , MCP.[Name]			AS Product
                    , MCP.[Code]			AS Code
                    , MCP.[Packaging]		AS Packaging
                    --, MCP.[LeftRightSide] AS LeftRightSide
                    , DD.LRModifier			AS LeftRightSide
                    , MCP.[Size]			AS Size
                    , MCP.[Gender]		AS Gender
					, D.PatientFirstName
					, D.PatientLastName
					, DD.Mod1
					, DD.Mod2
					, DD.Mod3

				from dispense D

					inner join Dispensedetail DD
						on DD.DispenseID = D.DispenseID
					inner join PracticeCatalogPRoduct PCP
						on PCP.PracticeCatalogProductID=DD.PracticeCatalogProductID
                    INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
						ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
					inner join MasterCatalogProduct MCP
						on MCP.mastercatalogproductid=PCP.mastercatalogproductid
					inner join Physician P
						on DD.PhysicianID = P.PhysicianID
					inner join Contact C
						on P.ContactID = C.ContactID

				where D.practicelocationid= @PracticeLocationID
					and (C.FirstName like @SearchText or C.LastName like @SearchText)
					AND PCP.IsThirdPartyProduct = 0  -- MasterCatalogProductsOnly
					--AND PCSB.IsActive =	1
					--AND PCP.IsActive = 1
					--AND MCP.IsActive = 1
					--and P.IsActive = 1
					--and C.IsActive = 1
					and D.IsActive = 1
					and DD.IsActive = 1

union

				select PCSB.BrandShortName
					, D.DispenseID
					, D.PatientCode
					, D.PatientEmail
					, D.Total
					, D.DateDispensed
					, DD.DispenseDetailID
					, C.FirstName + ' '  + C.LastName as PhysicianName
					, P.PhysicianID
					, DD.ActualChargeBilled
					, DD.DMEDeposit
					, DD.Quantity
					, DD.LineTotal
 				    , TPP.[Name]			AS Product
                    , TPP.[Code]			AS Code
                    , TPP.[Packaging]		AS Packaging
                    --, TPP.[LeftRightSide] AS LeftRightSide
                    , DD.LRModifier			AS LeftRightSide
                    , TPP.[Size]			AS Size
                    , TPP.[Gender]		AS Gender
					, D.PatientFirstName
					, D.PatientLastName
					, DD.Mod1
					, DD.Mod2
					, DD.Mod3

				from dispense D

					inner join Dispensedetail DD
						on DD.DispenseID = D.DispenseID
					inner join PracticeCatalogPRoduct PCP
						on PCP.PracticeCatalogProductID=DD.PracticeCatalogProductID
                    INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
						ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
					inner join ThirdPartyProduct TPP
						on TPP.ThirdPartyProductid=PCP.ThirdPartyProductid
					inner join Physician P
						on DD.PhysicianID = P.PhysicianID
					inner join Contact C
						on P.ContactID = C.ContactID

				where D.practicelocationid= @PracticeLocationID
					and (C.FirstName like @SearchText or C.LastName like @SearchText)
					AND PCP.IsThirdPartyProduct = 1  -- ThirdPartyProductsOnly
					--AND PCSB.IsActive =	1
					--AND PCP.IsActive = 1
					--AND TPP.IsActive = 1
					--and P.IsActive = 1
					--and C.IsActive = 1
					and D.IsActive = 1
					and DD.IsActive = 1


			) AS SUB
				Order By
				  --Sub.IsThirdPartyProduct
				 Sub.PhysicianName
--				, Sub.PracticeCatalogProductID
--				, SUB.Product
--				, SUB.Code

END
GO



-----------------------------------------
GO
-- =============================================
-- Author:		<Author: Greg Ross>
-- Create date: <Create Date: 10/28/08>
-- Description:	<Description: Used for the Patient Code search in the dispensement modification section of the admin pages>
-- =============================================
ALTER PROCEDURE [dbo].[usp_SearchForDispensementsWithProductCode]

	@PracticeLocationID int,
	@SearchText varchar(50)
AS
BEGIN

set @SearchText =  '%' + @SearchText + '%'


			SELECT
				  Sub.BrandShortName
				, Sub.DispenseID
				, Sub.PatientCode
				, Sub.PatientEmail

				, SUB.Total
				, SUB.DateDispensed			--.PracticeCatalogSupplierBrandID
				, SUB.DispenseDetailID
				, SUB.PhysicianName
				, SUB.PhysicianID
				, SUB.ActualChargeBilled
				, SUB.DMEDeposit
				, SUB. Quantity
				, SUB.LineTotal
				, SUB.Product
				, SUB.Code

				, ISNULL(SUB.Packaging, '') AS Packaging
				, ISNULL(SUB.LeftRightSide, '') AS Side
				, ISNULL(SUB.Size, '') AS Size
				, ISNULL(SUB.Gender, '') AS Gender
				, ISNULL(SUB.PatientFirstName,'') As PatientFirstName
				, ISNULL(SUB.PatientLastName,'') As PatientLastName
				, SUB.Mod1
				, SUB.Mod2
				, SUB.Mod3

			FROM
			(



				select PCSB.BrandShortName
					, D.DispenseID
					, D.PatientCode
					, D.PatientEmail
					, D.Total
					, D.DateDispensed
					, DD.DispenseDetailID
					, C.FirstName + ' '  + C.LastName as PhysicianName
					, P.PhysicianID
					, DD.ActualChargeBilled
					, DD.DMEDeposit
					, DD.Quantity
					, DD.LineTotal
 				    , MCP.[Name]			AS Product
                    , MCP.[Code]			AS Code
                    , MCP.[Packaging]		AS Packaging
                    --, MCP.[LeftRightSide] AS LeftRightSide
                    , DD.LRModifier			AS LeftRightSide
                    , MCP.[Size]			AS Size
                    , MCP.[Gender]		AS Gender
					, D.PatientFirstName
					, D.PatientLastName
					, DD.Mod1
					, DD.Mod2
					, DD.Mod3

				from dispense D

					inner join Dispensedetail DD
						on DD.DispenseID = D.DispenseID
					inner join PracticeCatalogPRoduct PCP
						on PCP.PracticeCatalogProductID=DD.PracticeCatalogProductID
                    INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
						ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
					inner join MasterCatalogProduct MCP
						on MCP.mastercatalogproductid=PCP.mastercatalogproductid
					inner join Physician P
						on DD.PhysicianID = P.PhysicianID
					inner join Contact C
						on P.ContactID = C.ContactID

				where D.practicelocationid= @PracticeLocationID
					and (MCP.Code like @SearchText)
					AND PCP.IsThirdPartyProduct = 0  -- MasterCatalogProductsOnly
					--AND PCSB.IsActive =	1
					--AND PCP.IsActive = 1
					--AND MCP.IsActive = 1
					--and P.IsActive = 1
					--and C.IsActive = 1
					and D.IsActive = 1
					and DD.IsActive = 1

union

				select PCSB.BrandShortName
					, D.DispenseID
					, D.PatientCode
					, D.PatientEmail
					, D.Total
					, D.DateDispensed
					, DD.DispenseDetailID
					, C.FirstName + ' '  + C.LastName as PhysicianName
					, P.PhysicianID
					, DD.ActualChargeBilled
					, DD.DMEDeposit
					, DD.Quantity
					, DD.LineTotal
 				    , TPP.[Name]			AS Product
                    , TPP.[Code]			AS Code
                    , TPP.[Packaging]		AS Packaging
                    --, TPP.[LeftRightSide] AS LeftRightSide
                    , DD.LRModifier			AS LeftRightSide
                    , TPP.[Size]			AS Size
                    , TPP.[Gender]		AS Gender
					, D.PatientFirstName
					, D.PatientLastName
					, DD.Mod1
					, DD.Mod2
					, DD.Mod3

				from dispense D

					inner join Dispensedetail DD
						on DD.DispenseID = D.DispenseID
					inner join PracticeCatalogPRoduct PCP
						on PCP.PracticeCatalogProductID=DD.PracticeCatalogProductID
                    INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
						ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
					inner join ThirdPartyProduct TPP
						on TPP.ThirdPartyProductid=PCP.ThirdPartyProductid
					inner join Physician P
						on DD.PhysicianID = P.PhysicianID
					inner join Contact C
						on P.ContactID = C.ContactID

				where D.practicelocationid= @PracticeLocationID
					and (TPP.Code like @SearchText)
					AND PCP.IsThirdPartyProduct = 1  -- ThirdPartyProductsOnly
					--AND PCSB.IsActive =	1
					--AND PCP.IsActive = 1
					--AND TPP.IsActive = 1
					--and P.IsActive = 1
					--and C.IsActive = 1
					and D.IsActive = 1
					and DD.IsActive = 1


			) AS SUB
				Order By
				  --Sub.IsThirdPartyProduct
				 Sub.Code
--				, Sub.PracticeCatalogProductID
--				, SUB.Product
--				, SUB.Code

END
GO


-----------------------------------------
GO
-- =============================================
-- Author:		Greg Ross
-- Create date: Nov. 5, 2007
-- Description:	Used for Search in Check In Screen
-- This query searches on both Supplier and date range
-- =============================================
ALTER PROCEDURE [dbo].[usp_SearchForPOsByDateRange_With_ThirdPartyProducts] --3, '01/01/2007', '12/12/2007'
	@PracticeLocationID int,
	@SearchFromDate datetime = null,
	@SearchToDate datetime = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   select

		SUB.BregVisionOrderID,
		SUB.CustomPurchaseOrderCode,
		SUB.SupplierOrderID,
		SUB.SupplierOrderLineItemID,
		SUB.ProductInventoryID,
		SUB.PracticeCatalogProductID,
		SUB.BrandShortname,
		SUB.PurchaseOrder,
		SUB.ShortName,
		SUB.Code,
		SUB.ActualWholeSaleCost,
		SUB.SupplierOrderLineItemStatusID,

		SUB.InvoiceNumber,  --added by u= 1/1/2010
		SUB.InvoicePaid,    --added by u= 1/1/2010

		--  2007.11.15		jb
		SUB.Status AS BVOStatus,
		SUB.Status  AS SOStatus,

		SUB.Status,

		SUB.Packaging,
		SUB.LeftRightSide,
		SUB.Size,
		SUB.Color,
		SUB.Gender,
		SUB.CreatedDate,

		SUB.QuantityOrdered,
		SUB.QuantityCheckedIn,
		SUB.QuantityToCheckIn,

		SUB.IsThirdPartyProduct

--		SUB.BregVisionOrderID,
--		SUB.SupplierOrderID,
--		SUB.SupplierOrderLineItemID,
--		SUB.ProductInventoryID,
--		SUB.PracticeCatalogProductID,
--		SUB.BrandShortname,
--		SUB.PurchaseOrder,
--		SUB.ShortName,
--		SUB.Code,
--		SUB.ActualWholeSaleCost,
--		SUB.SupplierOrderLineItemStatusID,
--		SUB.Status,
--		SUB.Packaging,
--		SUB.LeftRightSide,
--		SUB.Size,
--		SUB.Color,
--		SUB.Gender,
--		SUB.CreatedDate,
--		SUB.QuantityOrdered,
--		SUB.IsThirdPartyProduct

from
(

		select
			BVO.BregVisionOrderID,
			BVO.CustomPurchaseOrderCode,
			SO.SupplierOrderID,
			SOLI.SupplierOrderLineItemID,
			PI.ProductInventoryID,
			SOLI.PracticeCatalogProductID,
			PCSB.BrandShortname,
			BVO.PurchaseOrder,
			MCP.ShortName,
			MCP.Code,
			SOLI.ActualWholeSaleCost,
			SOLI.SupplierOrderLineItemStatusID,

			SO.InvoiceNumber, --added by u= 1/1/2010
			SO.InvoicePaid,   --added by u= 1/1/2010

			BVOS.Status AS BVOStatus,
			SOS.Status  AS SOStatus,

			SOLIS.Status,
			MCP.Packaging,
			MCP.LeftRightSide,
			MCP.Size,
			MCP.Color,
			MCP.Gender,
			BVO.CreatedDate,

			SOLI.QuantityOrdered,

			--  2007.11.15 JB
			ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityCheckedIn,

			CASE
			WHEN ( SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) ) < 0 THEN 0
			ELSE SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0)
			END 								  AS QuantityToCheckIn,


			0 AS IsThirdPartyProduct


		from BregVisionOrder BVO

		inner join SupplierOrder SO
			on BVO.BregVisionOrderID = SO.BregVisionOrderID

		inner join SupplierOrderLineItem SOLI
			on SO.SupplierOrderID = SOLI.SupplierOrderID

		inner join PracticeCatalogSupplierBrand PCSB
			on SO.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

		inner join PracticeCatalogPRoduct PCP
			on SOLI.PracticeCatalogProductID = 	PCP.PracticeCatalogPRoductID

		inner join MasterCatalogProduct MCP
			on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

		inner join SupplierOrderLineItemStatus SOLIS
			on SOLI.SupplierOrderLineItemStatusID = SOLIS.SupplierOrderLineItemStatusID

		--  2007.11.15  JB
		INNER JOIN dbo.SupplierOrderStatus AS sos
			ON SO.SupplierOrderStatusID = SOS.SupplierOrderStatusID

		INNER JOIN dbo.BregVisionOrderStatus AS bvos
			ON BVO.BregVisionOrderStatusID = BVOS.BregVisionOrderStatusID


		inner join ProductInventory PI
			on SOLI.PracticeCatalogPRoductID = pi.PracticeCatalogPRoductID

			--2007.11.15 JB And Second part of join condition.
			AND BVO.PracticeLocationID = PI.PracticeLocationID  --2007.11.15 JB And Second part of join condition.
			--AND PI.IsActive = 1

		-- JB 2007.11.15 fix query to put all quantity checked-in for the supplier order line item
		LEFT JOIN (SELECT SupplierOrderLineItemID, SUM(Quantity) AS QuantityCheckedIn
							FROM dbo.ProductInventory_OrderCheckIn
							WHERE IsActive = 1
							GROUP BY SupplierOrderLineItemID)AS SubPIOCI
								ON SOLI.SupplierOrderLineItemID = SubPIOCI.SupplierOrderLineItemID

		where BVO.isactive=1
		and SO.isactive = 1
		and SOLI.isactive = 1


		--and BVO.BregVisionOrderStatusID in (1,3)
		--and SO.SupplierOrderStatusID in (1,3)

		--and SOLI.SupplierOrderLineItemStatusID in (1,3)


		--  2007.11.15  JB Show all Order regardless of status.
		--  and BVO.BregVisionOrderStatusID in (1,3)
		--  and SO.SupplierOrderStatusID in (1,3)


		and BVO.PracticeLocationID = @PracticeLocationID
		and PI.PracticeLocationID = @PracticeLocationID

		and BVO.CreatedDate >= @SearchFromDate
		and BVO.CreatedDate <= @SearchToDate
		--and PCSB.BrandShortName like @SearchText
		and MCP.isactive = 1
		--order by c desc
union

		select
			BVO.BregVisionOrderID,
			BVO.CustomPurchaseOrderCode,
			SO.SupplierOrderID,
			SOLI.SupplierOrderLineItemID,
			PI.ProductInventoryID,
			SOLI.PracticeCatalogProductID,
			PCSB.BrandShortname,
			BVO.PurchaseOrder,
			TPP.ShortName,
			TPP.Code,
			SOLI.ActualWholeSaleCost,
			SOLI.SupplierOrderLineItemStatusID,

			SO.InvoiceNumber,  --added by u= 1/1/2010
			SO.InvoicePaid,    --added by u= 1/1/2010

			BVOS.Status AS BVOStatus,
			SOS.Status  AS SOStatus,
			SOLIS.Status,

			TPP.Packaging,
			TPP.LeftRightSide,
			TPP.Size,
			TPP.Color,
			TPP.Gender,
			BVO.CreatedDate,

			SOLI.QuantityOrdered,

			--  2007.11.15 JB
			ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityCheckedIn,

			CASE
			WHEN ( SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) ) < 0 THEN 0
			ELSE SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0)
			END 								  AS QuantityToCheckIn,


			1 AS IsThirdPartyProduct

		from BregVisionOrder BVO

		inner join SupplierOrder SO
			on BVO.BregVisionOrderID = SO.BregVisionOrderID

		inner join SupplierOrderLineItem SOLI
			on SO.SupplierOrderID = SOLI.SupplierOrderID

		inner join PracticeCatalogSupplierBrand PCSB
			on SO.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

		inner join PracticeCatalogPRoduct PCP
			on SOLI.PracticeCatalogProductID = 	PCP.PracticeCatalogPRoductID

		inner join ThirdPartyProduct AS TPP  WITH(NOLOCK)
			on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

		--inner join MasterCatalogProduct MCP
		--	on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
		inner join SupplierOrderLineItemStatus SOLIS
			on SOLI.SupplierOrderLineItemStatusID = SOLIS.SupplierOrderLineItemStatusID

		--  2007.11.15  JB
		INNER JOIN dbo.SupplierOrderStatus AS sos
			ON SO.SupplierOrderStatusID = SOS.SupplierOrderStatusID

		INNER JOIN dbo.BregVisionOrderStatus AS bvos
			ON BVO.BregVisionOrderStatusID = BVOS.BregVisionOrderStatusID


		inner join ProductInventory PI on SOLI.PracticeCatalogPRoductID = pi.PracticeCatalogPRoductID

			--2007.11.15 JB And Second part of join condition.
			AND BVO.PracticeLocationID = PI.PracticeLocationID  --2007.11.15 JB And Second part of join condition.
			--AND PI.IsActive = 1

		-- JB 2007.11.15 fix query to put all quantity checked-in for the supplier order line item
		LEFT JOIN (SELECT SupplierOrderLineItemID, SUM(Quantity) AS QuantityCheckedIn
							FROM dbo.ProductInventory_OrderCheckIn
							WHERE IsActive = 1
							GROUP BY SupplierOrderLineItemID)AS SubPIOCI
								ON SOLI.SupplierOrderLineItemID = SubPIOCI.SupplierOrderLineItemID



		where BVO.isactive=1
		and SO.isactive = 1
		and SOLI.isactive = 1

		--  2007.11.15  JB Show all Order regardless of status.
		--  and BVO.BregVisionOrderStatusID in (1,3)
		--  and SO.SupplierOrderStatusID in (1,3)
		--and SOLI.SupplierOrderLineItemStatusID in (1,3)
		--and SOLI.SupplierOrderLineItemStatusID in (1,3)
		and BVO.PracticeLocationID = @PracticeLocationID
		and PI.PracticeLocationID = @PracticeLocationID
		and BVO.CreatedDate >= @SearchFromDate
		and BVO.CreatedDate <= @SearchToDate
		--and PCSB.BrandShortName like @SearchText
		and TPP.isactive = 1
		--order by BVO.CreatedDate desc
) AS Sub
ORDER BY
	SUB.CreatedDate desc,
	Sub.IsThirdPartyProduct


END
GO


-----------------------------------------
GO
-- =============================================
-- Author:		Greg Ross
-- Create date: Nov. 5, 2007
-- Description:	Used for Search in Check In Screen
-- This query searches on both Supplier and date range
--  Think this might just search on the supplier.
--  Modifications:  2007.11.15 JB
--			Product Inventory need to also be joined properly
--			Add the QuantityCheckedIn and the QuantityToCheckIn
--			Add 		BVO.Status AS BVOStatus, SO.Status  AS SOStatus,
--			Show all order regardless of status.
-- =============================================
ALTER PROCEDURE [dbo].[usp_SearchForPOsBySupplier_With_ThirdPartyProducts]   --15, 'BREG'
	@PracticeLocationID int,
	@SearchText varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	set @SearchText = '%' + @SearchText + '%'
    select

		SUB.BregVisionOrderID,
		SUB.CustomPurchaseOrderCode,
		SUB.SupplierOrderID,
		SUB.SupplierOrderLineItemID,
		SUB.ProductInventoryID,
		SUB.PracticeCatalogProductID,
		SUB.BrandShortname,
		SUB.PurchaseOrder,
		SUB.ShortName,
		SUB.Code,
		SUB.ActualWholeSaleCost,
		SUB.SupplierOrderLineItemStatusID,

		--  2007.11.15		jb
		SUB.Status AS BVOStatus,
		SUB.Status  AS SOStatus,

		SUB.Status,

		SUB.Packaging,
		SUB.LeftRightSide,
		SUB.Size,
		SUB.Color,
		SUB.Gender,
		SUB.CreatedDate,

		SUB.InvoiceNumber,  --added by u= 1/1/2010
		SUB.InvoicePaid,    --added by u= 1/1/2010

		SUB.QuantityOrdered,
		SUB.QuantityCheckedIn,
		SUB.QuantityToCheckIn,

		SUB.IsThirdPartyProduct

--		SUB.BregVisionOrderID,
--		SUB.SupplierOrderID,
--		SUB.SupplierOrderLineItemID,
--		SUB.ProductInventoryID,
--		SUB.PracticeCatalogProductID,
--		SUB.BrandShortname,
--		SUB.PurchaseOrder,
--		SUB.ShortName,
--		SUB.Code,
--		SUB.ActualWholeSaleCost,
--		SUB.SupplierOrderLineItemStatusID,
--		SUB.Status,
--		SUB.Packaging,
--		SUB.LeftRightSide,
--		SUB.Size,
--		SUB.Color,
--		SUB.Gender,
--		SUB.CreatedDate,
--		SUB.QuantityOrdered,
--		SUB.IsThirdPartyProduct

from
(

		select
			BVO.BregVisionOrderID,
			BVO.CustomPurchaseOrderCode,
			SO.SupplierOrderID,
			SOLI.SupplierOrderLineItemID,
			PI.ProductInventoryID,
			SOLI.PracticeCatalogProductID,
			PCSB.BrandShortname,
			BVO.PurchaseOrder,
			MCP.ShortName,
			MCP.Code,
			SOLI.ActualWholeSaleCost,
			SOLI.SupplierOrderLineItemStatusID,

			SO.InvoiceNumber, --added by u= 1/1/2010
			SO.InvoicePaid,	  --added by u= 1/1/2010

			BVOS.Status AS BVOStatus,
			SOS.Status  AS SOStatus,

			SOLIS.Status,
			MCP.Packaging,
			MCP.LeftRightSide,
			MCP.Size,
			MCP.Color,
			MCP.Gender,
			BVO.CreatedDate,

			SOLI.QuantityOrdered,

			--  2007.11.15 JB
			ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityCheckedIn,

			CASE
			WHEN ( SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) ) < 0 THEN 0
			ELSE SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0)
			END 								  AS QuantityToCheckIn,


			0 AS IsThirdPartyProduct


		from BregVisionOrder BVO

		inner join SupplierOrder SO
			on BVO.BregVisionOrderID = SO.BregVisionOrderID

		inner join SupplierOrderLineItem SOLI
			on SO.SupplierOrderID = SOLI.SupplierOrderID

		inner join PracticeCatalogSupplierBrand PCSB
			on SO.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

		inner join PracticeCatalogPRoduct PCP
			on SOLI.PracticeCatalogProductID = 	PCP.PracticeCatalogPRoductID

		inner join MasterCatalogProduct MCP
			on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

		inner join SupplierOrderLineItemStatus SOLIS
			on SOLI.SupplierOrderLineItemStatusID = SOLIS.SupplierOrderLineItemStatusID

		--  2007.11.15  JB
		INNER JOIN dbo.SupplierOrderStatus AS sos
			ON SO.SupplierOrderStatusID = SOS.SupplierOrderStatusID

		INNER JOIN dbo.BregVisionOrderStatus AS bvos
			ON BVO.BregVisionOrderStatusID = BVOS.BregVisionOrderStatusID


		inner join ProductInventory PI
			on SOLI.PracticeCatalogPRoductID = pi.PracticeCatalogPRoductID

			--2007.11.15 JB And Second part of join condition.
			AND BVO.PracticeLocationID = PI.PracticeLocationID  --2007.11.15 JB And Second part of join condition.
			--AND PI.IsActive = 1

		-- JB 2007.11.15 fix query to put all quantity checked-in for the supplier order line item
		LEFT JOIN (SELECT SupplierOrderLineItemID, SUM(Quantity) AS QuantityCheckedIn
							FROM dbo.ProductInventory_OrderCheckIn
							WHERE IsActive = 1
							GROUP BY SupplierOrderLineItemID)AS SubPIOCI
								ON SOLI.SupplierOrderLineItemID = SubPIOCI.SupplierOrderLineItemID

		where BVO.isactive=1
		and SO.isactive = 1
		and SOLI.isactive = 1


		--and BVO.BregVisionOrderStatusID in (1,3)
		--and SO.SupplierOrderStatusID in (1,3)
		--and SOLI.SupplierOrderLineItemStatusID in (1,3)


		--  2007.11.15  JB Show all Order regardless of status.
		--  and BVO.BregVisionOrderStatusID in (1,3)
		--  and SO.SupplierOrderStatusID in (1,3)


		and BVO.PracticeLocationID = @PracticeLocationID
		and PI.PracticeLocationID = @PracticeLocationID
		--and BVO.CreatedDate >= @SearchFromDate
		--and BVO.CreatedDate <= @SearchToDate
		and PCSB.BrandShortName like @SearchText
		and MCP.isactive = 1
		--order by c desc
union

		select
			BVO.BregVisionOrderID,
			BVO.CustomPurchaseOrderCode,
			SO.SupplierOrderID,
			SOLI.SupplierOrderLineItemID,
			PI.ProductInventoryID,
			SOLI.PracticeCatalogProductID,
			PCSB.BrandShortname,
			BVO.PurchaseOrder,
			TPP.ShortName,
			TPP.Code,
			SOLI.ActualWholeSaleCost,
			SOLI.SupplierOrderLineItemStatusID,

			SO.InvoiceNumber,  --added by u= 1/1/2010
			SO.InvoicePaid,    --added by u= 1/1/2010

			BVOS.Status AS BVOStatus,
			SOS.Status  AS SOStatus,
			SOLIS.Status,

			TPP.Packaging,
			TPP.LeftRightSide,
			TPP.Size,
			TPP.Color,
			TPP.Gender,
			BVO.CreatedDate,

			SOLI.QuantityOrdered,

			--  2007.11.15 JB
			ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityCheckedIn,

			CASE
			WHEN ( SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) ) < 0 THEN 0
			ELSE SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0)
			END 								  AS QuantityToCheckIn,


			1 AS IsThirdPartyProduct

		from BregVisionOrder BVO

		inner join SupplierOrder SO
			on BVO.BregVisionOrderID = SO.BregVisionOrderID

		inner join SupplierOrderLineItem SOLI
			on SO.SupplierOrderID = SOLI.SupplierOrderID

		inner join PracticeCatalogSupplierBrand PCSB
			on SO.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

		inner join PracticeCatalogPRoduct PCP
			on SOLI.PracticeCatalogProductID = 	PCP.PracticeCatalogPRoductID

		inner join ThirdPartyProduct AS TPP  WITH(NOLOCK)
			on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

		--inner join MasterCatalogProduct MCP
		--	on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
		inner join SupplierOrderLineItemStatus SOLIS
			on SOLI.SupplierOrderLineItemStatusID = SOLIS.SupplierOrderLineItemStatusID

		--  2007.11.15  JB
		INNER JOIN dbo.SupplierOrderStatus AS sos
			ON SO.SupplierOrderStatusID = SOS.SupplierOrderStatusID

		INNER JOIN dbo.BregVisionOrderStatus AS bvos
			ON BVO.BregVisionOrderStatusID = BVOS.BregVisionOrderStatusID


		inner join ProductInventory PI on SOLI.PracticeCatalogPRoductID = pi.PracticeCatalogPRoductID

			--2007.11.15 JB And Second part of join condition.
			AND BVO.PracticeLocationID = PI.PracticeLocationID  --2007.11.15 JB And Second part of join condition.
			--AND PI.IsActive = 1

		-- JB 2007.11.15 fix query to put all quantity checked-in for the supplier order line item
		LEFT JOIN (SELECT SupplierOrderLineItemID, SUM(Quantity) AS QuantityCheckedIn
							FROM dbo.ProductInventory_OrderCheckIn
							WHERE IsActive = 1
							GROUP BY SupplierOrderLineItemID)AS SubPIOCI
								ON SOLI.SupplierOrderLineItemID = SubPIOCI.SupplierOrderLineItemID



		where BVO.isactive=1
		and SO.isactive = 1
		and SOLI.isactive = 1

		--  2007.11.15  JB Show all Order regardless of status.
		--  and BVO.BregVisionOrderStatusID in (1,3)
		--  and SO.SupplierOrderStatusID in (1,3)
		--and SOLI.SupplierOrderLineItemStatusID in (1,3)
		--and SOLI.SupplierOrderLineItemStatusID in (1,3)

		and BVO.PracticeLocationID = @PracticeLocationID
		and PI.PracticeLocationID = @PracticeLocationID
		--and BVO.CreatedDate >= @SearchFromDate
		--and BVO.CreatedDate <= @SearchToDate
		and PCSB.BrandShortName like @SearchText
		and TPP.isactive = 1
		--order by BVO.CreatedDate desc
) AS Sub
ORDER BY
	SUB.CreatedDate desc,
	Sub.IsThirdPartyProduct
END
GO


-----------------------------------------
GO
-- =============================================
-- Author:		Greg Ross
-- Create date: Nov. 5, 2007
-- Description:	Used for Search in Check In Screen
-- This query searches on both Supplier and date range
-- =============================================
ALTER PROCEDURE [dbo].[usp_SearchForPOsBySupplierAndDateRange_With_ThirdPartyProducts]
	@PracticeLocationID int,
	@SearchText varchar(50),
	@SearchFromDate datetime = null,
	@SearchToDate datetime = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	set @SearchText = '%' + @SearchText + '%'
   select

		SUB.BregVisionOrderID,
		SUB.CustomPurchaseOrderCode,
		SUB.SupplierOrderID,
		SUB.SupplierOrderLineItemID,
		SUB.ProductInventoryID,
		SUB.PracticeCatalogProductID,
		SUB.BrandShortname,
		SUB.PurchaseOrder,
		SUB.ShortName,
		SUB.Code,
		SUB.ActualWholeSaleCost,
		SUB.SupplierOrderLineItemStatusID,

		--  2007.11.15		jb
		SUB.Status AS BVOStatus,
		SUB.Status  AS SOStatus,

		SUB.Status,

		SUB.Packaging,
		SUB.LeftRightSide,
		SUB.Size,
		SUB.Color,
		SUB.Gender,
		SUB.CreatedDate,

		SUB.InvoiceNumber,  --added by u= 1/1/2010
		SUB.InvoicePaid,    --added by u= 1/1/2010

		SUB.QuantityOrdered,
		SUB.QuantityCheckedIn,
		SUB.QuantityToCheckIn,

		SUB.IsThirdPartyProduct

--		SUB.BregVisionOrderID,
--		SUB.SupplierOrderID,
--		SUB.SupplierOrderLineItemID,
--		SUB.ProductInventoryID,
--		SUB.PracticeCatalogProductID,
--		SUB.BrandShortname,
--		SUB.PurchaseOrder,
--		SUB.ShortName,
--		SUB.Code,
--		SUB.ActualWholeSaleCost,
--		SUB.SupplierOrderLineItemStatusID,
--		SUB.Status,
--		SUB.Packaging,
--		SUB.LeftRightSide,
--		SUB.Size,
--		SUB.Color,
--		SUB.Gender,
--		SUB.CreatedDate,
--		SUB.QuantityOrdered,
--		SUB.IsThirdPartyProduct

from
(

		select
			BVO.BregVisionOrderID,
			BVO.CustomPurchaseOrderCode,
			SO.SupplierOrderID,
			SOLI.SupplierOrderLineItemID,
			PI.ProductInventoryID,
			SOLI.PracticeCatalogProductID,
			PCSB.BrandShortname,
			BVO.PurchaseOrder,
			MCP.ShortName,
			MCP.Code,
			SOLI.ActualWholeSaleCost,
			SOLI.SupplierOrderLineItemStatusID,

			SO.InvoiceNumber, --added by u= 1/1/2010
			SO.InvoicePaid,	  --added by u= 1/1/2010

			BVOS.Status AS BVOStatus,
			SOS.Status  AS SOStatus,

			SOLIS.Status,
			MCP.Packaging,
			MCP.LeftRightSide,
			MCP.Size,
			MCP.Color,
			MCP.Gender,
			BVO.CreatedDate,

			SOLI.QuantityOrdered,

			--  2007.11.15 JB
			ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityCheckedIn,

			CASE
			WHEN ( SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) ) < 0 THEN 0
			ELSE SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0)
			END 								  AS QuantityToCheckIn,


			0 AS IsThirdPartyProduct


		from BregVisionOrder BVO

		inner join SupplierOrder SO
			on BVO.BregVisionOrderID = SO.BregVisionOrderID

		inner join SupplierOrderLineItem SOLI
			on SO.SupplierOrderID = SOLI.SupplierOrderID

		inner join PracticeCatalogSupplierBrand PCSB
			on SO.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

		inner join PracticeCatalogPRoduct PCP
			on SOLI.PracticeCatalogProductID = 	PCP.PracticeCatalogPRoductID

		inner join MasterCatalogProduct MCP
			on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

		inner join SupplierOrderLineItemStatus SOLIS
			on SOLI.SupplierOrderLineItemStatusID = SOLIS.SupplierOrderLineItemStatusID

		--  2007.11.15  JB
		INNER JOIN dbo.SupplierOrderStatus AS sos
			ON SO.SupplierOrderStatusID = SOS.SupplierOrderStatusID

		INNER JOIN dbo.BregVisionOrderStatus AS bvos
			ON BVO.BregVisionOrderStatusID = BVOS.BregVisionOrderStatusID


		inner join ProductInventory PI
			on SOLI.PracticeCatalogPRoductID = pi.PracticeCatalogPRoductID

			--2007.11.15 JB And Second part of join condition.
			AND BVO.PracticeLocationID = PI.PracticeLocationID  --2007.11.15 JB And Second part of join condition.
			--AND PI.IsActive = 1

		-- JB 2007.11.15 fix query to put all quantity checked-in for the supplier order line item
		LEFT JOIN (SELECT SupplierOrderLineItemID, SUM(Quantity) AS QuantityCheckedIn
							FROM dbo.ProductInventory_OrderCheckIn
							WHERE IsActive = 1
							GROUP BY SupplierOrderLineItemID)AS SubPIOCI
								ON SOLI.SupplierOrderLineItemID = SubPIOCI.SupplierOrderLineItemID

		where BVO.isactive=1
		and SO.isactive = 1
		and SOLI.isactive = 1


		--and BVO.BregVisionOrderStatusID in (1,3)
		--and SO.SupplierOrderStatusID in (1,3)
		--and SOLI.SupplierOrderLineItemStatusID in (1,3)


		--  2007.11.15  JB Show all Order regardless of status.
		--  and BVO.BregVisionOrderStatusID in (1,3)
		--  and SO.SupplierOrderStatusID in (1,3)


		and BVO.PracticeLocationID = @PracticeLocationID
		and PI.PracticeLocationID = @PracticeLocationID
		and BVO.CreatedDate >= @SearchFromDate
		and BVO.CreatedDate <= @SearchToDate
		and PCSB.BrandShortName like @SearchText
		and MCP.isactive = 1
		--order by c desc
union

		select
			BVO.BregVisionOrderID,
			BVO.CustomPurchaseOrderCode,
			SO.SupplierOrderID,
			SOLI.SupplierOrderLineItemID,
			PI.ProductInventoryID,
			SOLI.PracticeCatalogProductID,
			PCSB.BrandShortname,
			BVO.PurchaseOrder,
			TPP.ShortName,
			TPP.Code,
			SOLI.ActualWholeSaleCost,
			SOLI.SupplierOrderLineItemStatusID,

			SO.InvoiceNumber,  --added by u= 1/1/2010
			SO.InvoicePaid,    --added by u= 1/1/2010

			BVOS.Status AS BVOStatus,
			SOS.Status  AS SOStatus,
			SOLIS.Status,

			TPP.Packaging,
			TPP.LeftRightSide,
			TPP.Size,
			TPP.Color,
			TPP.Gender,
			BVO.CreatedDate,

			SOLI.QuantityOrdered,

			--  2007.11.15 JB
			ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityCheckedIn,

			CASE
			WHEN ( SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) ) < 0 THEN 0
			ELSE SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0)
			END 								  AS QuantityToCheckIn,


			1 AS IsThirdPartyProduct

		from BregVisionOrder BVO

		inner join SupplierOrder SO
			on BVO.BregVisionOrderID = SO.BregVisionOrderID

		inner join SupplierOrderLineItem SOLI
			on SO.SupplierOrderID = SOLI.SupplierOrderID

		inner join PracticeCatalogSupplierBrand PCSB
			on SO.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

		inner join PracticeCatalogPRoduct PCP
			on SOLI.PracticeCatalogProductID = 	PCP.PracticeCatalogPRoductID

		inner join ThirdPartyProduct AS TPP  WITH(NOLOCK)
			on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

		--inner join MasterCatalogProduct MCP
		--	on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
		inner join SupplierOrderLineItemStatus SOLIS
			on SOLI.SupplierOrderLineItemStatusID = SOLIS.SupplierOrderLineItemStatusID

		--  2007.11.15  JB
		INNER JOIN dbo.SupplierOrderStatus AS sos
			ON SO.SupplierOrderStatusID = SOS.SupplierOrderStatusID

		INNER JOIN dbo.BregVisionOrderStatus AS bvos
			ON BVO.BregVisionOrderStatusID = BVOS.BregVisionOrderStatusID


		inner join ProductInventory PI on SOLI.PracticeCatalogPRoductID = pi.PracticeCatalogPRoductID

			--2007.11.15 JB And Second part of join condition.
			AND BVO.PracticeLocationID = PI.PracticeLocationID  --2007.11.15 JB And Second part of join condition.
			--AND PI.IsActive = 1

		-- JB 2007.11.15 fix query to put all quantity checked-in for the supplier order line item
		LEFT JOIN (SELECT SupplierOrderLineItemID, SUM(Quantity) AS QuantityCheckedIn
							FROM dbo.ProductInventory_OrderCheckIn
							WHERE IsActive = 1
							GROUP BY SupplierOrderLineItemID)AS SubPIOCI
								ON SOLI.SupplierOrderLineItemID = SubPIOCI.SupplierOrderLineItemID



		where BVO.isactive=1
		and SO.isactive = 1
		and SOLI.isactive = 1

		--  2007.11.15  JB Show all Order regardless of status.
		--  and BVO.BregVisionOrderStatusID in (1,3)
		--  and SO.SupplierOrderStatusID in (1,3)
		--and SOLI.SupplierOrderLineItemStatusID in (1,3)
		--and SOLI.SupplierOrderLineItemStatusID in (1,3)

		and BVO.PracticeLocationID = @PracticeLocationID
		and PI.PracticeLocationID = @PracticeLocationID
		and BVO.CreatedDate >= @SearchFromDate
		and BVO.CreatedDate <= @SearchToDate
		and PCSB.BrandShortName like @SearchText
		and TPP.isactive = 1
		--order by BVO.CreatedDate desc
) AS Sub
ORDER BY
	SUB.CreatedDate desc,
	Sub.IsThirdPartyProduct


END
GO


-----------------------------------------
GO
/*
EXEC usp_User_Select_All_Logins_ByPracticeID 1
Get all active User Login Info


Michael Sneen 4/2/2009 *****


*/


ALTER PROCEDURE [dbo].[usp_User_Select_All_Logins_ByPracticeID]
		@PracticeID INT
AS
BEGIN
	SELECT
		U.UserID,
		U.AspNet_UserID,
		AU.UserName,
		AM.Email,
		U.ShowProductInfo,
		U.AllowDispense,
		U.AllowInventory,
		U.AllowCheckIn,
		U.AllowReports,
		U.AllowCart,
		U.allowDispenseModification,
		U.AllowPracticeUserOrderWithoutApproval,
		U.ScrollEntirePage,
		U.DispenseQueueSortDescending,
		U.AllowInventoryClose,
		U.DefaultLocationID,
		U.AllowZeroQtyDispense,
		U.IsActive,
		ISNULL(U.DefaultRecordsPerPage, 10) as DefaultRecordsPerPage,
		U.DefaultDispenseSearchCriteria,
    U.BcsUser,
		Pr.PracticeID,
		UserRole = (Select Top 1 AR.RoleName from [aspnetdb].[dbo].[aspnet_Roles] AR Inner Join [aspnetdb].[dbo].[aspnet_UsersInRoles] UIR ON AR.RoleId = UIR.RoleId Where UIR.UserId = U.AspNet_UserID),
		AM.IsLockedOut
	FROM
		dbo.[User] AS U
		INNER JOIN dbo.Practice AS Pr
			ON U.PracticeID = Pr.PracticeID
		INNER JOIN[aspnetdb].[dbo].[aspnet_Users] as AU
			ON U.AspNet_UserID = AU.UserId
		Left Outer JOIN [aspnetdb].[dbo].[aspnet_Membership] as AM
			ON U.AspNet_UserID = AM.UserId
	WHERE
			Pr.PracticeID = @PracticeID
		--AND U.IsActive = 1
	Order BY
		U.IsActive DESC,
		AU.UserName

END
GO

