/*
This script was created by Visual Studio on 7/31/2016 at 12:00 PM.
Run this script on tcp:www.bregvision.com,1433.BregVision (sa) to make it the same as tcp:v5beta.bregvision.com,1433.BregVision (sa).
This script performs its actions in the following order:
1. Disable foreign-key constraints.
2. Perform DELETE commands. 
3. Perform UPDATE commands.
4. Perform INSERT commands.
5. Re-enable foreign-key constraints.
Please back up your target database before running this script.
*/
SET NUMERIC_ROUNDABORT OFF
GO
SET XACT_ABORT, ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
/*Pointer used for text / image updates. This might not be needed, but is declared here just in case*/
BEGIN TRANSACTION
SET IDENTITY_INSERT [dbo].[ABNReason] ON
INSERT INTO [dbo].[ABNReason] ([ABNReasonID], [ABNReasonText], [SortOrder], [CreatedUserID], [CreatedDate], [ModifiedUserID], [ModifiedDate], [IsActive]) VALUES (5, N'Same/Similar', 5, 1, '20160412 01:26:11.387', NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[ABNReason] OFF
COMMIT TRANSACTION
