USE [BregVision]
GO
/****** Object:  UserDefinedFunction [dbo].[ConcatHCPCS]    Script Date: 01/30/2009 13:12:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--  SELECT * FROM dbo.ProductHCPCS AS ph
--  SELECT [dbo].[ConcatHCPCS](1072) 

Create FUNCTION [dbo].[ConcatBregHCPCS](@MasterCatalogProductID INT)
RETURNS VARCHAR(200)
AS
BEGIN
DECLARE @Output VARCHAR(200)
	SET @Output = ''

	SELECT @Output =	CASE @Output 
				WHEN '' THEN HCPCS 
				ELSE @Output + ', ' + HCPCS 
				END
	FROM BregProductHCPCS AS BPHCPCS WITH (NOLOCK)
	WHERE 
		BPHCPCS.MasterCatalogProductID = @MasterCatalogProductID
		AND BPHCPCS.IsActive = 1
	
	--ORDER BY ProductHCPCSID

	RETURN @Output
	--SELECT @Output AS output

END
