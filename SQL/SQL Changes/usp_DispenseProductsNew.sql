USE [BregVision_STG_VPWMP-57]
GO

/****** Object:  StoredProcedure [dbo].[usp_DispenseProductsNew]    Script Date: 1/22/2016 1:07:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Greg Ross
-- Create date: Aug. 16, 20076
-- Description:	Used to do all jobs related to dispensing products. Hits 4 tables (Product Inventory, PI_DispenseDetail, Dispense,DispenseDetail)
-- Modification: 2007-10-02 JB If PhysicianID = -then null.
-- Modification: 2011-09-14 MWS Added PatientEmail
-- Modification: 2014-12-04 BJ Added FitterID
-- =============================================
CREATE PROCEDURE [dbo].[usp_DispenseProductsNew]
	
	@UserID int,
	@PracticeLocationID int, 
	@ProductInventoryID int,
	@PatientCode varchar(255),
	@PracticeCatalogProductID int,	
		
	@PhysicianID int,
	@ICD9Code varchar(50),
	@LRModifier varchar(2),
	@IsMedicare bit,
	@ABNForm bit,
	@ActualChargeBilled smallmoney,	
	@Quantity int,
	@IsCashCollection bit,
	@WholeSaleCost smallmoney ,
	@DMEDeposit smallmoney ,
	@DispenseIDIn int,
	@DispenseDate datetime,
	@CreatedWithHandheld bit = 0,
	@DispenseBatchID int,
	@DispenseQueueID int,
	@PatientEmail Varchar(50) = '',
	@DispenseID   int  output,
	@PatientFirstName varchar(50)='', 
	@PatientLastName varchar(50)='',
	@PracticePayerID int,
	@Note varchar(MAX),
	@IsCustomFit bit,
	@FitterID int

AS
BEGIN

	SET NOCOUNT ON;

       
--		set @UserID = 1
--		set @PracticeLocationID = 3
--		set @PatientCode = 'A12345'
--		set @PracticeCatalogProductID = 2
--		set @PhysicianID = 3
--		set @ActualChargeBilled = '30.00'
--		set @Quantity = 2
--		set @IsCashCollection = 1
--		set @ProductInventoryID = 1
--		set @WholeSaleCost = '41.50'
		
		DECLARE @RecordCount INT
        DECLARE @Err INT 
        DECLARE @TransactionCountOnEntry INT 
		Declare @DispenseDetailID int


SELECT  @TransactionCountOnEntry = @@TRANCOUNT
        SELECT  @Err = @@ERROR
        BEGIN TRANSACTION      	

     IF @Err = 0 
            BEGIN
				
   
        --PRINT @ReOrderQuanity 	
                SELECT  @RecordCount = COUNT(1)
                FROM    dbo.[Dispense]
                WHERE   DispenseID = @DispenseIDIn
                        AND IsActive = 1
                SET @Err = @@ERROR

            END

		IF @Err = 0 
            BEGIN
                IF ( @RecordCount = 0 ) 
				
					begin
						--Initial insert into Dispense Table
						insert into Dispense(PracticeLocationID,PatientCode,Total,dateDispensed, CreatedUserID, CreatedDate, CreatedWithHandheld, DispenseBatchID, PatientEmail, PatientFirstName, PatientLastName, PracticePayerID, Note, IsActive) 
						Values (@PracticeLocationID,@PatientCode,0,@DispenseDate,@UserID,getdate(),@CreatedWithHandheld, @DispenseBatchID, @PatientEmail, @PatientFirstName, @PatientLastName, 
						CASE WHEN (@PracticePayerID = -1 OR @PracticePayerID = 0) THEN NULL ELSE @PracticePayerID END ,SUBSTRING(@Note,1,250), 1)
						Select  @DispenseID = SCOPE_IDENTITY()
						--print @DispenseID
					end 
				else 
					begin
						set @DispenseID=@DispenseIDIn
						update Dispense
							set
							 ModifiedUserID = @UserID,
							 ModifiedDate = getdate()
							where DispenseID = @DispenseID
					end		
			end 
	SET @Err = @@ERROR	
	--Insert into  Dispense Detail
    iF @Err = 0 
            BEGIN
				
					declare @lineTotal smallmoney
					set @lineTotal = @ActualChargeBilled * @Quantity
--					print 'Line Total'
--					print @linetotal
					insert into DispenseDetail
					(DispenseID,
					PracticeCatalogProductID,
					PhysicianID,
					ICD9Code,
					LRModifier,
					IsMedicare,
					ABNForm, 
					ActualChargeBilled,
					DMEDeposit,
					Quantity,
					LineTotal,
					IsCashCollection,
					IsCustomFit,
					FitterID,
					DispenseQueueID,
					CreatedUserID,
					CreatedDate,
					IsActive)
					Values
					(@DispenseID,
					 @PracticeCatalogProductID,
					 
					 CASE WHEN (@PhysicianID = -1 OR @PhysicianID = 0) THEN NULL ELSE @PhysicianID END,  --IF physicianID IS -1 INSERT NULL.
					 @ICD9Code,
					 @LRModifier,
					 @IsMedicare,
					 @ABNForm,
					 @ActualChargeBilled,
					 @DMEDeposit, 
					 @Quantity,
					 @lineTotal,
					 @IsCashCollection,
					 @IsCustomFit,
					 @FitterID,
					 @DispenseQueueID,
					 @UserID,
					 GetDate(),
					 1)
					Select  @DispenseDetailID = SCOPE_IDENTITY()

	End

	SET @Err = @@ERROR	
	--Update Quantities in ProductInventory
    iF @Err = 0 
            BEGIN
				declare @QOHTemp int
				set @QOHTemp = (select QuantityOnHandPerSystem from ProductInventory where ProductInventoryID = @ProductInventoryID	)
--				print 'QOHTemp'				
--				print @QOHTemp
				set @QOHTemp = @QOHTemp - @Quantity
--				print 'Adjusted QOH'				
--				print @QOHTemp
				if @QOHTemp < 0 
					begin
						set @QOHTemp = 0
					end 
				
					update productinventory 
						set
						 QuantityOnHandPerSystem=@QOHTemp,
						 ModifiedUserID=@UserID,
						 ModifiedDate = getdate()	
						 where ProductInventoryID = @ProductInventoryID	

			End 

	SET @Err = @@ERROR	
	--Update/Insert date into ProductInventory_DispenseDetail
    iF @Err = 0 
            BEGIN
				Insert into ProductInventory_DispenseDetail
					(ProductInventoryID, 
					 DispenseDetailID,
					 ActualWholesaleCost,
					 CreatedUserID,
					 CreatedDate,
					 IsActive)
					Values
					(@ProductInventoryID,
					 @DispenseDetailID,
					 @WholeSaleCost,
					 @UserID,
					 getdate(),
					 1)
					 
						
			end 
			SET @Err = @@ERROR	
                        IF @@TranCount > @TransactionCountOnEntry 
                            BEGIN

                                IF @Err = 0 
                                    BEGIN 
                                        COMMIT TRANSACTION
                                        RETURN @Err
                               -- PRINT @err
                                    END
                                ELSE 
                                    BEGIN
                                        ROLLBACK TRANSACTION
                                        RETURN @Err
                                -- PRINT @err
					--  Add any database logging here      
                                    END
								end


END

GO

