USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProductInventoryOrderStatusKPI]    Script Date: 02/09/2009 13:22:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mike Sneen
-- Create date: 02/06/09
-- Description:	Gets last 7 and last 30 days of cost for items dispensed
-- =============================================
Create PROCEDURE [dbo].[usp_ProductInventoryOrderStatusKPI]
	@PracticeLocationID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT 
	  --BVO.BregVisionOrderID
	--, BVO.CustomPurchaseOrderCode
	--, BVO.PurchaseOrder
	--, BVO.CreatedUserID
	BVOS.Status as Status
	, Count(BVOS.Status) as TotalPOs
	--, ST.ShippingTypeName as ShippingType
	, Sum(BVO.Total) as TotalDollars
	--, BVO.CreatedDate 
	FROM BregVisionOrder AS BVO
	INNER JOIN BregVisionOrderStatus AS BVOS 
		ON BVO.BregVisionOrderStatusID = BVOS.BregVisionOrderStatusID
	INNER JOIN ShippingType AS ST 
		ON BVO.ShippingTypeID = ST.ShippingTypeID
	WHERE BVO.BregVisionOrderStatusID IN (1,3) 
		AND BVO.isactive = 1 
		AND BVO.PracticeLocationID = @PracticeLocationID 
	--ORDER BY CreatedDate
	
	Group By BVOS.Status

END

--  Exec usp_ProductInventoryOrderStatusKPI 3