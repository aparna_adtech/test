USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetPracticeCatalogSupplierBrands]    Script Date: 02/10/2009 14:59:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
-- =============================================
-- Author:		Greg Ross	
-- Create date: 09/27/07
-- Description:	Used to get the parent detail for a practice. Used in MCToPC.aspx
-- =============================================
--  Modification:  2007.11.27  JB  
--		Proc below does not make any sense and 
--		is being commented out.
--      Add PCSB.IsActive = 1
--		Add SupplierName
--		--  2007.12.12 JB Order by IsThirdPartySupplier, SupplierName, BrandName.
	EXEC [usp_GetPracticeCatalogSupplierBrands] 1
*/
ALTER PROCEDURE [dbo].[usp_GetPracticeCatalogSupplierBrands] 
	-- Add the parameters for the stored procedure here
	@PracticeID int
AS
BEGIN
-- select * from PracticeCatalogSupplierBrand order by practiceID
		SELECT DISTINCT 
				  PCSB.PracticeCatalogSupplierBrandID
				, PCSB.SupplierShortName	AS SupplierName
				, PCSB.BrandShortName		AS BrandName
				, PCSB.IsThirdPartySupplier AS IsThirdPartySupplier
				, ISNULL(PCSB.Sequence, 999) AS DerivedSequence -- JB
				--, PCP.*
				--, TPP.*
        FROM 
			PracticeCatalogSupplierBrand AS PCSB WITH (NOLOCK)
			inner join PracticeCatalogProduct PCP on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
			Left Outer join ThirdPartyProduct AS TPP  WITH(NOLOCK) 
			on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
		WHERE 
			PCSB.PracticeID = @PracticeID
			AND PCSB.IsActive = 1	
			AND PCP.IsActive = 1	
			AND ISNULL(TPP.IsActive, 1) = 1		
		ORDER BY 
			  PCSB.IsThirdPartySupplier
			, ISNULL(PCSB.Sequence, 999)  -- JB
			, PCSB.SupplierShortName
			, PCSB.BrandShortName 

--  OLD PROC CODE IS BELOW  -- JB commented out on 2007.11.27 
--	-- SET NOCOUNT ON added to prevent extra result sets from
--	-- interfering with SELECT statements.
--	SET NOCOUNT ON;
--
--
--     SELECT     DISTINCT 
--				PCSB.PracticeCatalogSupplierBrandID,
--				PCSB.BrandShortName as BrandName
--				--MCS.[SupplierName] AS Brand,
--                --MCS.[MasterCatalogSupplierID] AS SupplierID,
--				--c.[FirstName] AS Name,
--                --C.[LastName],
--                --c.[PhoneWork],
--                --A.[AddressLine1] AS Address,
--                --A.City AS City,
--                --A.[ZipCode] AS Zipcode
--				--MCC.[Name] AS Category
--        FROM   PracticeCatalogProduct PCP
--                --INNER JOIN address A ON MCS.[AddressID] = A.[AddressID]
--                --INNER JOIN [Contact] C ON mcs.[ContactID] = c.[ContactID]
--				--INNER JOIN [MasterCatalogCategory] MCC ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
--				--INNER JOIN [MasterCatalogSubCategory] MCSC ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
--				--INNER JOIN [MasterCatalogProduct] MCP ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
--				--INNER Join PracticeCatalogSupplierBrand PCSB on MCS.MasterCatalogSupplierID = PCSB.MasterCatalogSupplierID
--				inner join PracticeCatalogSupplierBrand PCSB on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
--				where PCP.PracticeID = @PracticeID
--				order by PCSB.BrandShortName 

END





