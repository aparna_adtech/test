USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMasterCatalogProductInfo]    Script Date: 01/30/2009 13:03:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Greg Ross
-- Create date: 09/05/2007
-- Description:	Gets all data from the masterCatalogProduct Tables
-- Modifications:  Order by MCSupplierID, ProductName, Code.
--   JB 2008.01.23  Not Done Should Combine LeftRightSide and Size as SideSize 
--   MWS 2009.01.30  Addedd Search by HCPC
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetMasterCatalogProductInfo]

@SearchCriteria varchar(50),
@SearchText  varchar(50)

AS


BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
set @SearchText = '%' + @SearchText + '%'	
	if (@SearchCriteria='Browse' or @SearchCriteria='All' or @SearchCriteria='')
	begin
		select MCS.MasterCatalogSupplierID as SupplierID,
				MCP.MastercatalogProductID,
				MCP.MasterCatalogSubCategoryID,
				MCP.Code,
				MCP.ShortName,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				MCP.WholesaleListCost
		 from mastercatalogSupplier MCS
					INNER JOIN [MasterCatalogCategory] MCC ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
					INNER JOIN [MasterCatalogSubCategory] MCSC ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
					INNER JOIN [MasterCatalogProduct] MCP ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
		where MCP.isactive=1
			and MCC.Isactive=1
			and MCSC.isactive=1
		ORDER BY 
			MCS.MasterCatalogSupplierID,
--			MCC.Sequence, 
--			MCS.Sequence,
			MCP.ShortName,
			MCP.Code
	end 

else if @SearchCriteria='Name'
	begin
		select MCS.MasterCatalogSupplierID as SupplierID,
				MCP.MastercatalogProductID,
				MCP.MasterCatalogSubCategoryID,
				MCP.Code,
				MCP.ShortName,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				MCP.WholesaleListCost
		 from mastercatalogSupplier MCS
					INNER JOIN [MasterCatalogCategory] MCC ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
					INNER JOIN [MasterCatalogSubCategory] MCSC ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
					INNER JOIN [MasterCatalogProduct] MCP ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
		where MCP.isactive=1
			and MCC.Isactive=1
			and MCSC.isactive=1
			and (MCP.Name like @SearchText or MCP.ShortName like  @SearchText)
		ORDER BY 
			MCS.MasterCatalogSupplierID,
--			MCC.Sequence, 
--			MCS.Sequence,
			MCP.ShortName,
			MCP.Code

	end
else if @SearchCriteria='Code'
	begin
		select MCS.MasterCatalogSupplierID as SupplierID,
				MCP.MastercatalogProductID,
				MCP.MasterCatalogSubCategoryID,
				MCP.Code,
				MCP.ShortName,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				MCP.WholesaleListCost
		 from mastercatalogSupplier MCS
					INNER JOIN [MasterCatalogCategory] MCC ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
					INNER JOIN [MasterCatalogSubCategory] MCSC ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
					INNER JOIN [MasterCatalogProduct] MCP ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
		where MCP.isactive=1
			and MCC.Isactive=1
			and MCSC.isactive=1
			and MCP.Code like @SearchText 
		ORDER BY 
			MCS.MasterCatalogSupplierID,
--			MCC.Sequence, 
--			MCS.Sequence,
			MCP.ShortName,
			MCP.Code
	end
else if @SearchCriteria='Brand'
	begin
		select MCS.MasterCatalogSupplierID as SupplierID,
				MCP.MastercatalogProductID,
				MCP.MasterCatalogSubCategoryID,
				MCP.Code,
				MCP.ShortName,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				MCP.WholesaleListCost
		 from mastercatalogSupplier MCS
					INNER JOIN [MasterCatalogCategory] MCC ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
					INNER JOIN [MasterCatalogSubCategory] MCSC ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
					INNER JOIN [MasterCatalogProduct] MCP ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
					--inner join MasterCatalogSupplier MCS on MCC.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
		where MCP.isactive=1
			and MCC.Isactive=1
			and MCSC.isactive=1
			and (MCS.SupplierName like @SearchText or MCS.SupplierShortName like @SearchText)
		ORDER BY 
			MCS.MasterCatalogSupplierID,
--			MCC.Sequence, 
--			MCS.Sequence,
			MCP.ShortName,
			MCP.Code
	end
else if @SearchCriteria='Category'
	begin
		select MCS.MasterCatalogSupplierID as SupplierID,
				MCP.MastercatalogProductID,
				MCP.MasterCatalogSubCategoryID,
				MCP.Code,
				MCP.ShortName,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				MCP.WholesaleListCost
		 from mastercatalogSupplier MCS
					INNER JOIN [MasterCatalogCategory] MCC ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
					INNER JOIN [MasterCatalogSubCategory] MCSC ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
					INNER JOIN [MasterCatalogProduct] MCP ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
					--inner join MasterCatalogSupplier MCS on MCC.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
		where MCP.isactive=1
			and MCC.Isactive=1
			and MCSC.isactive=1
			and (MCC.Name like @SearchText or MCSC.Name like @SearchText)
		ORDER BY 
			MCS.MasterCatalogSupplierID,
--			MCC.Sequence, 
--			MCS.Sequence,
			MCP.ShortName,
			MCP.Code
	end
	else if @SearchCriteria='HCPCs'
	begin
		select MCS.MasterCatalogSupplierID as SupplierID,
				MCP.MastercatalogProductID,
				MCP.MasterCatalogSubCategoryID,
				MCP.Code,
				MCP.ShortName,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				MCP.WholesaleListCost
		 from mastercatalogSupplier MCS
					INNER JOIN [MasterCatalogCategory] MCC ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
					INNER JOIN [MasterCatalogSubCategory] MCSC ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
					INNER JOIN [MasterCatalogProduct] MCP ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
					--inner join MasterCatalogSupplier MCS on MCC.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
		where MCP.isactive=1
			and MCC.Isactive=1
			and MCSC.isactive=1
			--and (MCS.SupplierName like @SearchText or MCS.SupplierShortName like @SearchText)
			AND dbo.ConcatBregHCPCS(MCP.MastercatalogProductID) like @SearchText
		ORDER BY 
			MCS.MasterCatalogSupplierID,
--			MCC.Sequence, 
--			MCS.Sequence,
			MCP.ShortName,
			MCP.Code
	end
END
		





