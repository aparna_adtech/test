USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAllInventoryDataforPCToIventory]    Script Date: 03/24/2009 14:27:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Greg Ross
-- Create date: Oct. 1st 2007
-- Description:	Gets all data for PCToInventory Table

-- Modifications:  
--  
--		2007109 1800 JB Add nested procs and comment out old queries.
--		20071011 1349 JB  Add the PracticeCatalogSupplierBrandID to the searches to join with the parent grid.
--		2007.11.07 11:22  Add the ProductName to the Search
--								by calling a nested proc.
--		2007.11.07  18:14  Post to production.  
--      2008.01.31  John Bongiorni Update the nested procs to use the PI.IsSetToActive column
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetAllInventoryDataforPCToIventory]
	@PracticeLocationID int,
	@SearchCriteria varchar(50),
	@SearchText  varchar(50),
	@IsActive bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
set @SearchText = '%' + @SearchText + '%'	
	if (@SearchCriteria='Browse' or @SearchCriteria='All' or @SearchCriteria='')
	begin

--		EXEC	[dbo].[usp_GetAllInventoryDataforPCToIventory_ALL_With_ThirdPartyProducts]
		EXEC	[dbo].[usp_GetAllInventoryDataforPCToIventory_ALL_With_ThirdPartyProducts]
		@PracticeLocationID = @PracticeLocationID
		, @IsActive = @IsActive


		-- SELECT PCP.[PracticeCatalogProductID],
		--                            mcs.[MasterCatalogSupplierID] AS SupplierID,
		--							mCC.NAME AS cATEGORY,
		--                            mcsc.[Name] AS sUBcATEGORY,
		--                            MCP.[Name] AS pRODUCT,
		--                            MCP.[Code] AS Code,
		--                            MCP.[Packaging] AS Packaging,
		--                            MCP.[LeftRightSide] AS LeftRightSide,
		--                            mcp.[Size] AS Size,
		--                            mcp.[Gender] AS Gender,
		--							PI.QuantityOnHandPerSystem as QOH,
		--							PI.ParLevel,
		--							PI.ReorderLevel,
		--							PI.CriticalLevel,
		--							PCP.WholesaleCost,
		--							PI.Isactive
		--                     FROM   [MasterCatalogSupplier] MCS 
		--                            INNER JOIN [MasterCatalogCategory] MCC ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
		--                            INNER JOIN [MasterCatalogSubCategory] MCSC ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
		--                            INNER JOIN [MasterCatalogProduct] MCP ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
		--                            INNER JOIN [PracticeCatalogProduct] PCP ON MCP.[MasterCatalogProductID] = PCP.[MasterCatalogProductID]
		--                            INNER JOIN [ProductInventory] PI ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
		--                     WHERE  PI.[PracticeLocationID] = @PracticeLocationID
		--					
		--					-- JB 20071008 Commenting on the following as a workaround for now.  
		--						and PI.IsActive = @IsActive	



	end 
else if @SearchCriteria='Name'
	begin

		EXEC dbo.usp_GetAllInventoryDataforPCToIventory_ByProductName_With_ThirdPartyProducts
						  @PracticeLocationID = @PracticeLocationID
						, @SearchText = @SearchText 
						, @IsActive = @IsActive


--						SELECT PCP.[PracticeCatalogProductID],
--
--							--  JB 20071012  Switch all Supplier ID
--                            --mcs.[MasterCatalogSupplierID] AS SupplierID,
--							PCP.PracticeCatalogSupplierBrandID AS SupplierID,
--
--							mCC.NAME AS cATEGORY,
--                            mcsc.[Name] AS sUBcATEGORY,
--                            MCP.[Name] AS pRODUCT,
--                            MCP.[Code] AS Code,
--                            MCP.[Packaging] AS Packaging,
--                            MCP.[LeftRightSide] AS LeftRightSide,
--                            mcp.[Size] AS Size,
--                            mcp.[Gender] AS Gender,
--							PI.QuantityOnHandPerSystem as QOH,
--							PI.ParLevel,
--							PI.ReorderLevel,
--							PI.CriticalLevel,
--							PCP.WholesaleCost,
--							PI.Isactive
--                     FROM   [MasterCatalogSupplier] MCS 
--                            INNER JOIN [MasterCatalogCategory] MCC ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
--                            INNER JOIN [MasterCatalogSubCategory] MCSC ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
--                            INNER JOIN [MasterCatalogProduct] MCP ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
--                            INNER JOIN [PracticeCatalogProduct] PCP ON MCP.[MasterCatalogProductID] = PCP.[MasterCatalogProductID]
--                          
--							--  20071012 JB Add to provide the PracticeCatalogSupplierBrandID and commment                          
--                            --INNER JOIN PracticeCatalogSupplierBrand AS PCSB ON PCP.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
--                            
--                            INNER JOIN [ProductInventory] PI ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
--                     WHERE  
--                     
--							PI.[PracticeLocationID] = @PracticeLocationID
--							and PI.IsActive = @IsActive						
--							and (MCP.Name like @SearchText or MCP.ShortName like  @SearchText)
	end
else if @SearchCriteria='Code'
	begin
	
				EXEC dbo.usp_GetAllInventoryDataforPCToIventory_ByProductCode_With_ThirdPartyProducts
				  @PracticeLocationID = @PracticeLocationID
				, @SearchText = @SearchText 
				, @IsActive = @IsActive
	
	
--							SELECT PCP.[PracticeCatalogProductID],
--							--  JB 20071012  Switch the Supplier ID
--                            --  mcs.[MasterCatalogSupplierID] AS SupplierID,
--							PCP.PracticeCatalogSupplierBrandID AS SupplierID,
--
--							mCC.NAME AS cATEGORY,
--                            mcsc.[Name] AS sUBcATEGORY,
--                            MCP.[Name] AS pRODUCT,
--                            MCP.[Code] AS Code,
--                            MCP.[Packaging] AS Packaging,
--                            MCP.[LeftRightSide] AS LeftRightSide,
--                            mcp.[Size] AS Size,
--                            mcp.[Gender] AS Gender,
--							PI.QuantityOnHandPerSystem as QOH,
--							PI.ParLevel,
--							PI.ReorderLevel,
--							PI.CriticalLevel,
--							PCP.WholesaleCost,
--							PI.Isactive
--                     FROM   [MasterCatalogSupplier] MCS 
--                            INNER JOIN [MasterCatalogCategory] MCC ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
--                            INNER JOIN [MasterCatalogSubCategory] MCSC ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
--                            INNER JOIN [MasterCatalogProduct] MCP ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
--                            INNER JOIN [PracticeCatalogProduct] PCP ON MCP.[MasterCatalogProductID] = PCP.[MasterCatalogProductID]
--                            INNER JOIN [ProductInventory] PI ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
--                     WHERE  
--							PI.[PracticeLocationID] = @PracticeLocationID
--							and PI.IsActive = @IsActive						
--							and MCP.Code like @SearchText 
	end
else if @SearchCriteria='Brand'
	begin
	
		EXEC dbo.usp_GetAllInventoryDataforPCToIventory_ByBrand_With_ThirdPartyProducts
						  @PracticeLocationID = @PracticeLocationID
						, @SearchText = @SearchText 
						, @IsActive = @IsActive
		
	
--						SELECT 
--							PCP.[PracticeCatalogProductID],
--                            
--							--  JB 20071012  Switch the Supplier ID
--                            --  mcs.[MasterCatalogSupplierID] AS SupplierID,
--							PCP.PracticeCatalogSupplierBrandID AS SupplierID,                            
--							
--							mCC.NAME AS cATEGORY,
--                            mcsc.[Name] AS sUBcATEGORY,
--                            MCP.[Name] AS pRODUCT,
--                            MCP.[Code] AS Code,
--                            MCP.[Packaging] AS Packaging,
--                            MCP.[LeftRightSide] AS LeftRightSide,
--                            mcp.[Size] AS Size,
--                            mcp.[Gender] AS Gender,
--							PI.QuantityOnHandPerSystem as QOH,
--							PI.ParLevel,
--							PI.ReorderLevel,
--							PI.CriticalLevel,
--							PCP.WholesaleCost,
--							PI.Isactive
--                     FROM   [MasterCatalogSupplier] MCS 
--                            INNER JOIN [MasterCatalogCategory] MCC ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
--                            INNER JOIN [MasterCatalogSubCategory] MCSC ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
--                            INNER JOIN [MasterCatalogProduct] MCP ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
--                            INNER JOIN [PracticeCatalogProduct] PCP ON MCP.[MasterCatalogProductID] = PCP.[MasterCatalogProductID]
--                            INNER JOIN [ProductInventory] PI ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
--							inner join PracticeCatalogSupplierBrand PCSB 
--								on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
--
--                     WHERE  
--							PI.[PracticeLocationID] = @PracticeLocationID
--							and PI.IsActive = @IsActive
--							and (PCSB.SupplierName like @SearchText or PCSB.SupplierShortName like @SearchText)
							
	end
else if @SearchCriteria='Category'
	begin

	
		EXEC dbo.usp_GetAllInventoryDataforPCToIventory_ByCategory_With_ThirdPartyProducts
						  @PracticeLocationID = @PracticeLocationID
						, @SearchText = @SearchText 
						, @IsActive = @IsActive
				


--					SELECT  
--					
--							PCP.[PracticeCatalogProductID],
--                            
--                            --  JB 20071012  Switch the Supplier ID
--                            --  mcs.[MasterCatalogSupplierID] AS SupplierID,
--							PCP.PracticeCatalogSupplierBrandID AS SupplierID,
--							
--							mCC.NAME AS cATEGORY,
--                            mcsc.[Name] AS sUBcATEGORY,
--                            MCP.[Name] AS pRODUCT,
--                            MCP.[Code] AS Code,
--                            MCP.[Packaging] AS Packaging,
--                            MCP.[LeftRightSide] AS LeftRightSide,
--                            mcp.[Size] AS Size,
--                            mcp.[Gender] AS Gender,
--							PI.QuantityOnHandPerSystem as QOH,
--							PI.ParLevel,
--							PI.ReorderLevel,
--							PI.CriticalLevel,
--							PCP.WholesaleCost,
--							PI.Isactive
--                     FROM   [MasterCatalogSupplier] MCS 
--                            INNER JOIN [MasterCatalogCategory] MCC ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
--                            INNER JOIN [MasterCatalogSubCategory] MCSC ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
--                            INNER JOIN [MasterCatalogProduct] MCP ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
--                            INNER JOIN [PracticeCatalogProduct] PCP ON MCP.[MasterCatalogProductID] = PCP.[MasterCatalogProductID]
--                            INNER JOIN [ProductInventory] PI ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
--                     WHERE  
--							PI.[PracticeLocationID] = @PracticeLocationID 
--							and PI.IsActive = @IsActive						
--							and (MCC.Name like @SearchText or MCSC.Name like @SearchText) 
							
						
	end
	
	else if @SearchCriteria='HCPCs'
	begin

	
		EXEC dbo.usp_GetAllInventoryDataforPCToIventory_ByHCPCs_With_ThirdPartyProducts
						  @PracticeLocationID = @PracticeLocationID
						, @SearchText = @SearchText 
						, @IsActive = @IsActive
				


--					SELECT  
--					
--							PCP.[PracticeCatalogProductID],
--                            
--                            --  JB 20071012  Switch the Supplier ID
--                            --  mcs.[MasterCatalogSupplierID] AS SupplierID,
--							PCP.PracticeCatalogSupplierBrandID AS SupplierID,
--							
--							mCC.NAME AS cATEGORY,
--                            mcsc.[Name] AS sUBcATEGORY,
--                            MCP.[Name] AS pRODUCT,
--                            MCP.[Code] AS Code,
--                            MCP.[Packaging] AS Packaging,
--                            MCP.[LeftRightSide] AS LeftRightSide,
--                            mcp.[Size] AS Size,
--                            mcp.[Gender] AS Gender,
--							PI.QuantityOnHandPerSystem as QOH,
--							PI.ParLevel,
--							PI.ReorderLevel,
--							PI.CriticalLevel,
--							PCP.WholesaleCost,
--							PI.Isactive
--                     FROM   [MasterCatalogSupplier] MCS 
--                            INNER JOIN [MasterCatalogCategory] MCC ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
--                            INNER JOIN [MasterCatalogSubCategory] MCSC ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
--                            INNER JOIN [MasterCatalogProduct] MCP ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
--                            INNER JOIN [PracticeCatalogProduct] PCP ON MCP.[MasterCatalogProductID] = PCP.[MasterCatalogProductID]
--                            INNER JOIN [ProductInventory] PI ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
--                     WHERE  
--							PI.[PracticeLocationID] = @PracticeLocationID 
--							and PI.IsActive = @IsActive						
--							and (MCC.Name like @SearchText or MCSC.Name like @SearchText) 
							
						
	end
END


