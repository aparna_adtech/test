USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_PracticeCatalogSupplierBrand_Select_Unique_By_HCPCs]    Script Date: 01/29/2009 20:52:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		John Bongiorni
-- Create date: 2007 10 09
-- Description:	Gets ProductCatalogProduct Info for MC and 3rdParty Products,
--				Given the PSCB.BrandName or PSCB.BrandShortName or portion thereof.
--			Used to get the detail for a Practice. Used in the MCToPC.aspx page
-- Modification: 20071008 2033 JB Add Third Party and code to make this generic
--  (FOR MCProducts and ThirdPartyProducts)
--				20071009 1203 JB Add WITH(NOLOCK) cLAUSES to Tables.
--
--		2007.12.12  John Bongiorni  	Order by Sub.ShortName, Sub.Code
--		2008.01.23 JB Add LeftRightSide, Size and SideSize
--      2008.02.25 JB Added Column for "Deletable" PCPs

-- =============================================
Create PROCEDURE [dbo].[usp_PracticeCatalogSupplierBrand_Select_Unique_By_HCPCs]

	  @PracticeID int
	, @SearchText  varchar(50)
AS
BEGIN
	
	SET @SearchText = '%' + @SearchText  + '%'

select
--	Sub.PracticeID 
--	, Sub.PracticecatalogProductID
--	, Sub.ProductID	AS MasterCatalogProductID  -- Needs to be called ProductID (to handle MC and ThirdParty)
--	, 

	  Sub.PracticeCatalogSupplierBrandID
	, Sub.SupplierShortName	AS SupplierName						--  Supplier Short Name can be used by app. to limit supplier's displayed.
	, Sub.BrandShortName	AS BrandName						--  Brand Short Name can be used by app. to limit supplier's displayed.
	, SUB.Sequence
	, SUB.IsThirdPartySupplier
--	, Sub.ShortName
--	, Sub.Code
--	
--	, ISNULL(Sub.LeftRightSide, ' ') + ', ' + ISNULL(Sub.Size, ' ') AS SideSize
--	, ISNULL(Sub.LeftRightSide, ' ') AS LeftRightSide
--	, ISNULL(Sub.Size, ' ') AS Size
--	
--	, ISNULL( Sub.WholesaleCost, 0 )	AS WholesaleCost
--	, ISNULL( Sub.BillingCharge, 0 )	AS BillingCharge
--	, ISNULL( Sub.DMEDeposit, 0 )		AS DMEDeposit
--	, ISNULL( Sub.BillingChargeCash, 0 ) AS BillingChargeCash
--	
--	, Sub.IsThirdPartyProduct
--	,dbo.ConcatHCPCS(Sub.PracticeCatalogProductID) AS HCPCSString -- JB 20071004 Added Column for UDF Concat of HCPCS
--						-- Need to be put in app code and grid to distinguish 
--												--  Master Catalog Product and Third Party Product.
--	, CASE (SELECT COUNT(1) 
--	FROM productinventory AS PI WITH (NOLOCK) 
--	WHERE PI.practiceCatalogProductID = sub.practiceCatalogProductID
--    ) WHEN 0 THEN 1 ELSE 0 END AS IsDeletable   --  JB  20080225  Added Column for "Deletable" PCPs
	
FROM
(
		SELECT distinct
			
	--		PCP.PracticeID, 
	--		PCP.PracticecatalogProductID,
	--		PCP.MasterCatalogProductID	AS ProductID,  -- Needs to be ProductID
			PCP.PracticeCatalogSupplierBrandID,
			PCSB.SupplierShortName,
			PCSB.BrandShortName,
			PCSB.Sequence,
	--		MCP.ShortName,
--			MCP.Code,
--			
--			MCP.LeftRightSide,
--			MCP.Size,
--			
--			PCP.WholesaleCost,
--			PCP.BillingCharge,
--			PCP.DMEDeposit,
--			PCP.BillingChargeCash,
			0 AS IsThirdPartySupplier
			-- 0 AS IsThirdPartyProduct					-- Needs to know if this is a Third Party Product.
		 FROM PracticeCatalogProduct AS PCP WITH(NOLOCK) 

		 INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK) 
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

		INNER JOIN MasterCatalogProduct AS MCP WITH(NOLOCK) 
			ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

		WHERE PCP.practiceID =				@PracticeID
			AND PCP.IsThirdPartyProduct = 0		-- MasterCatalogProduct
			AND PCP.isactive = 1
			AND MCP.isactive = 1
			AND PCSB.IsActive = 1
			AND dbo.ConcatHCPCS(PCP.PracticecatalogProductID) like @SearchText
			/*
			AND (
					PCSB.BrandName LIKE @SearchText 
					OR PCSB.BrandShortName LIKE @SearchText
					OR PCSB.SupplierName LIKE @SearchText 
					OR PCSB.SupplierShortName LIKE @SearchText
				)
			*/

		UNION

			SELECT Distinct
--			PCP.PracticeID, 
--			PCP.PracticecatalogProductID,
--			PCP.ThirdPartyProductID		 AS ProductID,  -- Needs to be ProductID
			PCP.PracticeCatalogSupplierBrandID,
			PCSB.SupplierShortName,
			PCSB.BrandShortName,
			PCSB.Sequence,
--			TPP.ShortName,
--			TPP.Code,
--			
--			TPP.LeftRightSide,
--			TPP.Size,
--						
--			PCP.WholesaleCost,
--			PCP.BillingCharge,
--			PCP.DMEDeposit,
--			PCP.BillingChargeCash,
			1 AS IsThirdPartySupplier
			-- 1 AS IsThirdPartyProduct					-- Needs to know if this is a Third Party Product.

		 from PracticeCatalogProduct AS PCP  WITH(NOLOCK) 

		 INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK) 
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

		inner join ThirdPartyProduct AS TPP  WITH(NOLOCK) 
			on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID 

		where PCP.practiceID =			@PracticeID
			AND PCP.IsThirdPartyProduct = 1		-- ThirdPartyProduct
			and PCP.isactive = 1				-- ? Is this neccessary?
			and TPP.isactive = 1			
			AND PCSB.IsActive = 1
			AND dbo.ConcatHCPCS(PCP.PracticecatalogProductID) like @SearchText
			/*
			AND ( 
					(PCSB.BrandName LIKE @SearchText OR PCSB.BrandShortName LIKE @SearchText)
				OR
					(PCSB.SupplierName LIKE @SearchText OR PCSB.SupplierShortName LIKE @SearchText)
				)
			*/
) AS Sub

ORDER BY 
	  SUB.IsThirdPartySupplier
	, ISNULL(SUB.Sequence, 999)  -- JB
	, SUB.SupplierShortName
	, SUB.BrandShortName 

END







