USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetPracticeCatalogProductInfoMCToPC]    Script Date: 01/29/2009 16:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Greg Ross
-- Create date: 09/27/07
-- Description:	Used to get the detail for a Practice. Used in the MCToPC.aspx page
-- Modifications:  
--  BROWSE, ALL, EMPTY SEARCH 
--		20071008 JB comment out existing Browse, All, empty search to show 
--				Third Party Vendors.  Calls nested proc to include 3rd party products.
--
--  PRODUCTNAME 
--		20071008  JB PRODUCTNAME comment out existing ProductName search to show 
--				Third Party Vendors. Calls nested proc to include 3rd party products.
--				Also search with wildcards on both sides for the 
--				search criteria.
--
--  PRODUCTCODE 
--		20071008  JB PRODUCTCODE comment out existing  ProductCode search to show 
--				Third Party Vendors.  Calls nested proc to include 3rd party products.
--
--	BRAND 
--		20071008  JB BRAND comment out existing BrandName search to show 
--				Third Party Vendors. Will search BrandName and BrandShortName for MC products.
--				Will search SupplierName, SupplierShortName , BrandName and BrandShortName for Third Party products.
--				Calls nested proc to include 3rd party products.
--
--	HCPCs 
--		20090129  MWS Added HCPCs 
--				Third Party Vendors. Will search HCPCs for MC products.
--				Will search HCPCs for Third Party products.
--				Calls nested proc to include 3rd party products.
--  2008.01.23 JB  Expand Wildcard to both side rather than just the right side.
--
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetPracticeCatalogProductInfoMCToPC]

	@PracticeID int,
	@SearchCriteria varchar(50),
	@SearchText  varchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
set @SearchText = '%' + @SearchText + '%'	
	if (@SearchCriteria='Browse' or @SearchCriteria='All' or @SearchCriteria='')
	begin
	
		--   20071008 JB The follow will show the mcproducts and thirdpartyproducts.
		
		EXEC	[dbo].[usp_GetPracticeCatalogProductInfoMCToPC_All_With_3rdPartyProducts]
				@PracticeID = @PracticeID

	
	
--		The following will select all with master catalog only.	
--		   select
--			PCP.PracticeID, 
--			PCP.PracticecatalogProductID,
--			PCP.MasterCatalogProductID,	
--			PCP.PracticeCatalogSupplierBrandID,
--			MCP.ShortName,
--			MCP.Code,
--			PCP.WholesaleCost,
--			PCP.BillingCharge,
--			PCP.DMEDeposit,
--			PCP.BillingChargeCash
--		 from PracticeCatalogProduct PCP
--		--inner join PracticeCatalogSupplierBrand PCSB 
--		--	on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
--		inner join MasterCatalogProduct MCP 
--			on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
--		where PCP.practiceID = @PracticeID
--			and PCP.isactive = 1
--			--and PCSB.isactive = 1
--			and MCP.isactive = 1
--		
	
	end
else if @SearchCriteria='Name'	-- ProductName
	begin
	
		-- When searching for a product name, search with wildcard on both sides.
		SET @SearchText = '%' + @SearchText
		
		--   20071008 JB The follow will show the mcproducts and thirdpartyproducts.
		
		EXEC  [dbo].[usp_GetPracticeCatalogProductInfoMCToPC_By_ProductName_With_3rdPartyProducts]
				  @PracticeID = @PracticeID
				, @SearchText = @SearchText 

	
	
--		The following will select all with master catalog only.		
--	   select
--		PCP.PracticeID, 
--		PCP.PracticecatalogProductID,
--		PCP.MasterCatalogProductID,	
--		PCP.PracticeCatalogSupplierBrandID,
--		MCP.ShortName,
--		MCP.Code,
--		PCP.WholesaleCost,
--		PCP.BillingCharge,
--		PCP.DMEDeposit,
--		PCP.BillingChargeCash
--	 from PracticeCatalogProduct PCP
--	--inner join PracticeCatalogSupplierBrand PCSB 
--	--	on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
--	inner join MasterCatalogProduct MCP 
--		on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
--	where PCP.practiceID = @PracticeID
--		and PCP.isactive = 1
--		--and PCSB.isactive = 1
--		and MCP.isactive = 1
--		and (MCP.Name like @SearchText or MCP.ShortName like  @SearchText)
--	
	
	end
else if @SearchCriteria='Code'
	begin
	
	
	--   20071008 JB The follow will show the mcproducts and thirdpartyproducts.
		
		EXEC	[dbo].[usp_GetPracticeCatalogProductInfoMCToPC_By_ProductCode_With_3rdPartyProducts]
		  @PracticeID = @PracticeID
		, @SearchText = @SearchText 

	
--		The following will select all with master catalog only.		
--	   select
--		PCP.PracticeID, 
--		PCP.PracticecatalogProductID,
--		PCP.MasterCatalogProductID,	
--		PCP.PracticeCatalogSupplierBrandID,
--		MCP.ShortName,
--		MCP.Code,
--		PCP.WholesaleCost,
--		PCP.BillingCharge,
--		PCP.DMEDeposit,
--		PCP.BillingChargeCash
--	 from PracticeCatalogProduct PCP
--	--inner join PracticeCatalogSupplierBrand PCSB 
--	--	on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
--	inner join MasterCatalogProduct MCP 
--		on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
--	where PCP.practiceID = @PracticeID
--		and PCP.isactive = 1
--		--and PCSB.isactive = 1
--		and MCP.isactive = 1
--		and MCP.Code like @SearchText 

	end

else if @SearchCriteria='Brand'
	begin
	
		--   20071008 JB The follow will show the mcproducts and thirdpartyproducts.

		EXEC	[dbo].[usp_GetPracticeCatalogProductInfoMCToPC_By_BrandName_With_3rdPartyProducts]
					@PracticeID = @PracticeID
					, @SearchText = @SearchText
					
--		
--	   select
--		PCP.PracticeID, 
--		PCP.PracticecatalogProductID,
--		PCP.MasterCatalogProductID,	
--		PCP.PracticeCatalogSupplierBrandID,
--		MCP.ShortName,
--		MCP.Code,
--		PCP.WholesaleCost,
--		PCP.BillingCharge,
--		PCP.DMEDeposit,
--		PCP.BillingChargeCash
--	 from PracticeCatalogProduct PCP
--	inner join PracticeCatalogSupplierBrand PCSB 
--		on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
--	inner join MasterCatalogProduct MCP 
--		on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
--	where PCP.practiceID = @PracticeID
--		and PCP.isactive = 1
--		--and PCSB.isactive = 1
--		and MCP.isactive = 1
--		and (PCSB.SupplierName like @SearchText or PCSB.SupplierShortName like @SearchText)

	end
	
	else if @SearchCriteria='HCPCs'
	begin
	
		--   20071008 JB The follow will show the mcproducts and thirdpartyproducts.

		EXEC	[dbo].[usp_GetPracticeCatalogProductInfoMCToPC_By_HCPCs_With_3rdPartyProducts]
					@PracticeID = @PracticeID
					, @SearchText = @SearchText
					
--		
--	   select
--		PCP.PracticeID, 
--		PCP.PracticecatalogProductID,
--		PCP.MasterCatalogProductID,	
--		PCP.PracticeCatalogSupplierBrandID,
--		MCP.ShortName,
--		MCP.Code,
--		PCP.WholesaleCost,
--		PCP.BillingCharge,
--		PCP.DMEDeposit,
--		PCP.BillingChargeCash
--	 from PracticeCatalogProduct PCP
--	inner join PracticeCatalogSupplierBrand PCSB 
--		on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
--	inner join MasterCatalogProduct MCP 
--		on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
--	where PCP.practiceID = @PracticeID
--		and PCP.isactive = 1
--		--and PCSB.isactive = 1
--		and MCP.isactive = 1
--		AND dbo.ConcatHCPCS(PCP.PracticecatalogProductID) like @SearchText

	end

END





