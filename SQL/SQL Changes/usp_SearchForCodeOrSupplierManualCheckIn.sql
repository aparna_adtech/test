USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_SearchForCodeOrSupplierManualCheckIn]    Script Date: 01/29/2009 14:24:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








-- =============================================
-- Author:		Greg Ross
-- Create date: 10/05/07
-- Description:	Search used in the Manual Check In section of the Check In tab
-- =============================================
ALTER PROCEDURE [dbo].[usp_SearchForCodeOrSupplierManualCheckIn]
	@SearchCriteria varchar(50),
	@SearchText varchar(50),
	@PracticeLocationID int


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	set @SearchText = @SearchText + '%'
	

if 	@SearchCriteria = 'Code'
	begin
	Select 
			SUB.PracticeCatalogProductID,
			SUB.SupplierID,
			SUB.ProductInventoryID,
			SUB.BrandShortname,
			SUB.ShortName,
			SUB.Code,
			SUB.Packaging,
			SUB.LeftRightSide,
			SUB.Size,
			SUB.Color,
			SUB.Gender,
			SUB.QuantityOnHandPerSystem,
			SUB.WholesaleCost,
			SUB.IsThirdPartyProduct
		from
		(
			select 
				PCP.PracticeCatalogProductID,
				PCSB.MasterCatalogSupplierID as SupplierID,
				PI.ProductInventoryID,
				PCSB.BrandShortname,
				MCP.ShortName,
				MCP.Code,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				PI.QuantityOnHandPerSystem,
				PCP.WholesaleCost,
				0 AS IsThirdPartyProduct
			from ProductInventory PI
			inner join PracticeCatalogPRoduct PCP
				on PI.PracticeCatalogPRoductID = PCP.PracticeCatalogPRoductID
			inner join MasterCatalogProduct MCP
				on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
			inner join PracticeCatalogSupplierBrand PCSB	
				on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID	

			where PI.Isactive = 1
			and MCP.isactive=1
			and PCSB.isactive=1
			and MCP.Code like @SearchText
			and PI.PracticeLocationID = @PracticeLocationID
			
	union

			select 
				PCP.PracticeCatalogProductID,
				PCSB.MasterCatalogSupplierID as SupplierID,
				PI.ProductInventoryID,
				PCSB.BrandShortname,
				TPP.ShortName,
				TPP.Code,
				TPP.Packaging,
				TPP.LeftRightSide,
				TPP.Size,
				TPP.Color,
				TPP.Gender,
				PI.QuantityOnHandPerSystem,
				PCP.WholesaleCost,
				1 AS IsThirdPartyProduct
			from ProductInventory PI
			inner join PracticeCatalogPRoduct PCP
				on PI.PracticeCatalogPRoductID = PCP.PracticeCatalogPRoductID
			inner join ThirdPartyProduct AS TPP  WITH(NOLOCK) 
				on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID 	
			inner join PracticeCatalogSupplierBrand PCSB	
				on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID	
			where PI.Isactive = 1
			and TPP.isactive=1
			and PCSB.isactive=1
			and TPP.Code like @SearchText
			and PI.PracticeLocationID = @PracticeLocationID
	) AS Sub
	ORDER BY
		Sub.IsThirdPartyProduct
end 	
if 	@SearchCriteria = 'Name'
	begin
	set @SearchText = '%' + @SearchText 
	Select 
			SUB.PracticeCatalogProductID,
			SUB.SupplierID,
			SUB.ProductInventoryID,
			SUB.BrandShortname,
			SUB.ShortName,
			SUB.Code,
			SUB.Packaging,
			SUB.LeftRightSide,
			SUB.Size,
			SUB.Color,
			SUB.Gender,
			SUB.QuantityOnHandPerSystem,
			SUB.WholesaleCost,
			SUB.IsThirdPartyProduct
		from
		(
			select 
				PCP.PracticeCatalogProductID,
				PCSB.MasterCatalogSupplierID as SupplierID,
				PI.ProductInventoryID,
				PCSB.BrandShortname,
				MCP.ShortName,
				MCP.Code,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				PI.QuantityOnHandPerSystem,
				PCP.WholesaleCost,
				0 AS IsThirdPartyProduct
			from ProductInventory PI
			inner join PracticeCatalogPRoduct PCP
				on PI.PracticeCatalogPRoductID = PCP.PracticeCatalogPRoductID
			inner join MasterCatalogProduct MCP
				on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
			inner join PracticeCatalogSupplierBrand PCSB	
				on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID	

			where PI.Isactive = 1
			and MCP.isactive=1
			and PCSB.isactive=1
			and (MCP.Name like @SearchText or MCP.ShortName like @SearchText)
			and PI.PracticeLocationID = @PracticeLocationID
			
	union

			select 
				PCP.PracticeCatalogProductID,
				PCSB.MasterCatalogSupplierID as SupplierID,
				PI.ProductInventoryID,
				PCSB.BrandShortname,
				TPP.ShortName,
				TPP.Code,
				TPP.Packaging,
				TPP.LeftRightSide,
				TPP.Size,
				TPP.Color,
				TPP.Gender,
				PI.QuantityOnHandPerSystem,
				PCP.WholesaleCost,
				1 AS IsThirdPartyProduct
			from ProductInventory PI
			inner join PracticeCatalogPRoduct PCP
				on PI.PracticeCatalogPRoductID = PCP.PracticeCatalogPRoductID
			inner join ThirdPartyProduct AS TPP  WITH(NOLOCK) 
				on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID 	
			inner join PracticeCatalogSupplierBrand PCSB	
				on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID	
			where PI.Isactive = 1
			and TPP.isactive=1
			and PCSB.isactive=1
			and (TPP.Name like @SearchText or TPP.ShortName like @SearchText)
			and PI.PracticeLocationID = @PracticeLocationID
	) AS Sub
	ORDER BY
		Sub.IsThirdPartyProduct
end 	

if 	@SearchCriteria = 'Supplier' 
	begin
	set @SearchText = '%' + @SearchText 
	Select 
			SUB.PracticeCatalogProductID,
			SUB.SupplierID,
			SUB.ProductInventoryID,
			SUB.BrandShortname,
			SUB.ShortName,
			SUB.Code,
			SUB.Packaging,
			SUB.LeftRightSide,
			SUB.Size,
			SUB.Color,
			SUB.Gender,
			SUB.QuantityOnHandPerSystem,
			SUB.WholesaleCost,
			SUB.IsThirdPartyProduct
		from
		(
			select 
				PCP.PracticeCatalogProductID,
				PCSB.MasterCatalogSupplierID as SupplierID,
				PI.ProductInventoryID,
				PCSB.BrandShortname,
				MCP.ShortName,
				MCP.Code,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				PI.QuantityOnHandPerSystem,
				PCP.WholesaleCost,
				0 AS IsThirdPartyProduct
			from ProductInventory PI
			inner join PracticeCatalogPRoduct PCP
				on PI.PracticeCatalogPRoductID = PCP.PracticeCatalogPRoductID
			inner join MasterCatalogProduct MCP
				on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
			inner join PracticeCatalogSupplierBrand PCSB	
				on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID	

			where PI.Isactive = 1
			and MCP.isactive=1
			and PCSB.isactive=1
			and PCSB.BrandShortName like @SearchText
			and PI.PracticeLocationID = @PracticeLocationID
			
	union

			select 
				PCP.PracticeCatalogProductID,
				PCSB.MasterCatalogSupplierID as SupplierID,
				PI.ProductInventoryID,
				PCSB.BrandShortname,
				TPP.ShortName,
				TPP.Code,
				TPP.Packaging,
				TPP.LeftRightSide,
				TPP.Size,
				TPP.Color,
				TPP.Gender,
				PI.QuantityOnHandPerSystem,
				PCP.WholesaleCost,
				1 AS IsThirdPartyProduct
			from ProductInventory PI
			inner join PracticeCatalogPRoduct PCP
				on PI.PracticeCatalogPRoductID = PCP.PracticeCatalogPRoductID
			inner join ThirdPartyProduct AS TPP  WITH(NOLOCK) 
				on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID 	
			inner join PracticeCatalogSupplierBrand PCSB	
				on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID	
			where PI.Isactive = 1
			and TPP.isactive=1
			and PCSB.isactive=1
			and PCSB.BrandShortName like @SearchText
			and PI.PracticeLocationID = @PracticeLocationID
	) AS Sub
	ORDER BY
		Sub.IsThirdPartyProduct

	end 
	
	--HCPCs
	if 	@SearchCriteria = 'HCPCs' 
	begin
	set @SearchText = '%' + @SearchText 
	Select 
			SUB.PracticeCatalogProductID,
			SUB.SupplierID,
			SUB.ProductInventoryID,
			SUB.BrandShortname,
			SUB.ShortName,
			SUB.Code,
			SUB.Packaging,
			SUB.LeftRightSide,
			SUB.Size,
			SUB.Color,
			SUB.Gender,
			SUB.QuantityOnHandPerSystem,
			SUB.WholesaleCost,
			SUB.IsThirdPartyProduct
		from
		(
			select 
				PCP.PracticeCatalogProductID,
				PCSB.MasterCatalogSupplierID as SupplierID,
				PI.ProductInventoryID,
				PCSB.BrandShortname,
				MCP.ShortName,
				MCP.Code,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				PI.QuantityOnHandPerSystem,
				PCP.WholesaleCost,
				0 AS IsThirdPartyProduct
			from ProductInventory PI
			inner join PracticeCatalogPRoduct PCP
				on PI.PracticeCatalogPRoductID = PCP.PracticeCatalogPRoductID
			inner join MasterCatalogProduct MCP
				on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
			inner join PracticeCatalogSupplierBrand PCSB	
				on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID	

			where PI.Isactive = 1
			and MCP.isactive=1
			and PCSB.isactive=1
			--and PCSB.BrandShortName like @SearchText
			and dbo.ConcatHCPCS(PI.PracticeCatalogProductID) like @SearchText
			and PI.PracticeLocationID = @PracticeLocationID
			
	union

			select 
				PCP.PracticeCatalogProductID,
				PCSB.MasterCatalogSupplierID as SupplierID,
				PI.ProductInventoryID,
				PCSB.BrandShortname,
				TPP.ShortName,
				TPP.Code,
				TPP.Packaging,
				TPP.LeftRightSide,
				TPP.Size,
				TPP.Color,
				TPP.Gender,
				PI.QuantityOnHandPerSystem,
				PCP.WholesaleCost,
				1 AS IsThirdPartyProduct
			from ProductInventory PI
			inner join PracticeCatalogPRoduct PCP
				on PI.PracticeCatalogPRoductID = PCP.PracticeCatalogPRoductID
			inner join ThirdPartyProduct AS TPP  WITH(NOLOCK) 
				on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID 	
			inner join PracticeCatalogSupplierBrand PCSB	
				on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID	
			where PI.Isactive = 1
			and TPP.isactive=1
			and PCSB.isactive=1
			--and PCSB.BrandShortName like @SearchText
			and dbo.ConcatHCPCS(PI.PracticeCatalogProductID) like @SearchText
			and PI.PracticeLocationID = @PracticeLocationID
	) AS Sub
	ORDER BY
		Sub.IsThirdPartyProduct

	end 

END









