USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAllProductDatabyLocation]    Script Date: 01/29/2009 11:27:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/* ------------------------------------------------------------
   PROCEDURE:    [dbo].[usp_GetAllProductDatabyLocation]
   
   Description:  Gets all Child data for Inventory on Inventory.aspx page.
		
	Dependencies:  	udf_GetSuggestedReorder_Given_PracticeLocationID	 
   
   AUTHOR:       John Bongiorni 10/18/2007 5:45:17 PM
   
   Modifications:  
   ------------------------------------------------------------ */ 


ALTER PROCEDURE [dbo].[usp_GetAllProductDatabyLocation]   
		@PracticeLocationID			INT
	  , @PracticeID					INT = 0			--  For backward compatibility with application code, @PracticeID is never used.
	  , @SearchCriteria varchar(50)
	  , @SearchText  varchar(50)
AS 
    BEGIN


	-- Table Variable used as a working table
        DECLARE @WorkingTable TABLE
            (
              Category VARCHAR(50)
            , SubCategory VARCHAR(50)
            , Product VARCHAR(100)
            , Code VARCHAR(50)
            , Packaging VARCHAR(50)
            , LeftRightSide VARCHAR(50)
            , Size VARCHAR(50)
            , Gender VARCHAR(50)
            , Color VARCHAR(50)
            , SupplierID INT
            , PracticeCatalogProductID INT

            , QuantityOnHand INT DEFAULT 0
            , QuantityOnOrder INT DEFAULT 0
            , PARLevel INT DEFAULT 0
            , ReorderLevel INT DEFAULT 0
            , CriticalLevel INT DEFAULT 0
            , SuggestedReorderLevel INT DEFAULT 0
            , IsThirdPartyProduct BIT
			, StockingUnits int default 1
            )



		-- 1.  Insert the PracticeCatalogProductID for the location's products that are in inventory and active.
		
        INSERT INTO
            @WorkingTable
            (
              PracticeCatalogProductID,
			  StockingUNits
			
            )
            SELECT
                PI.PracticeCatalogProductID,
				PCP.StockingUnits
            FROM
                ProductInventory AS PI
                INNER JOIN [PracticeCatalogProduct] AS PCP
                    ON PCP.PracticeCatalogProductID = PI.PracticeCatalogProductID
            WHERE
                PI.PracticeLocationID = @PracticeLocationID
                AND PI.IsActive = 1
                AND PCP.IsActive = 1
         
         
         
         --  2. Get Quntity on Hand, Quntity on Order, Inventory Counts, and Suggested Reorder Level        
         UPDATE @WorkingTable		
		 SET   
			  WT.QuantityOnHand = Levels.QuantityOnHand
			, WT.QuantityOnOrder = Levels.QuantityOnOrder

			, WT.ParLevel = Levels.ParLevel
			, WT.ReorderLevel = Levels.ReorderLevel
			, WT.CriticalLevel = Levels.CriticalLevel	
			
			, WT.SuggestedReorderLevel = (Levels.SuggestedReorderLevel	/ WT.StockingUnits)
			
		 FROM @WorkingTable AS WT 
		 INNER JOIN dbo.udf_GetSuggestedReorder_Given_PracticeLocationID( @PracticeLocationID )	AS Levels
				ON WT.PracticeCatalogProductID = Levels.PracticeCatalogProductID
			
			
			        
		--  3.  Get Product Info for PracticeCatalogProduct in inventory. 
	set @SearchText = @SearchText + '%'	
	if (@SearchCriteria='Browse' or @SearchCriteria='All' or @SearchCriteria='')
	begin		
        UPDATE
            @WorkingTable
        SET 
            WT.Category = CheckIn.Category
          , WT.SubCategory = CheckIn.SubCategory
          , WT.Product = CheckIn.Product
          , WT.Code = CheckIn.Code
          , WT.Packaging = CheckIn.Packaging
          , WT.LeftRightSide = CheckIn.LeftRightSide
          , WT.Size = CheckIn.Size
          , WT.Gender = CheckIn.Gender
          , WT.Color = CheckIn.Color
          , WT.SupplierID = CheckIn.SupplierID
          , WT.IsThirdPartyProduct = CheckIn.IsThirdPartyProduct
        FROM
            @WorkingTable AS WT
            INNER JOIN (
                         SELECT
                            Sub.PracticeCatalogProductID
                          , Sub.CATEGORY
                          , Sub.SUBCATEGORY
                          , Sub.PRODUCT
                          , Sub.Code
                          , Sub.Packaging
                          , Sub.LeftRightSide
                          , Sub.Size
                          , Sub.Gender
						  , Sub.Color
                          , Sub.SupplierID
                          , Sub.IsThirdPartyProduct
                         FROM
                            (
                              SELECT
                                PCP.[PracticeCatalogProductID]
                              , mCC.NAME AS cATEGORY
                              , mcsc.[Name] AS sUBcATEGORY
                              , MCP.[Name] AS pRODUCT
                              , MCP.[Code] AS Code
                              , MCP.[Packaging] AS Packaging
                              , MCP.[LeftRightSide] AS LeftRightSide
                              , mcp.[Size] AS Size
                              , mcp.[Gender] AS Gender
						      , mcp.[Color] AS Color
                              , PCP.PracticeCatalogSupplierBrandID AS SupplierID
                              , PCP.IsThirdPartyProduct
                              FROM
                                [MasterCatalogSupplier] AS MCS WITH ( NOLOCK )
                                INNER JOIN [MasterCatalogCategory] AS MCC WITH ( NOLOCK )
                                                                               ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
                                INNER JOIN [MasterCatalogSubCategory] AS MCSC
                                WITH ( NOLOCK )
                                     ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
                                INNER JOIN [MasterCatalogProduct] AS MCP WITH ( NOLOCK )
                                                                              ---ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
                                                                              ON MCP.MasterCatalogSubCategoryID = MCSC.MasterCatalogSubCategoryID
                                INNER JOIN [PracticeCatalogProduct] AS PCP
                                WITH ( NOLOCK )
                                     ON MCP.[MasterCatalogProductID] = PCP.[MasterCatalogProductID]
                                INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
                                WITH ( NOLOCK )
                                     ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
                                INNER JOIN [ProductInventory] AS PI WITH ( NOLOCK )
                                                                         ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
                              WHERE
                                PI.[PracticeLocationID] = @PracticeLocationID
                                AND PCP.IsThirdPartyProduct = 0
	                   
                   --  20071011 JB Union the thirdpartyproducts.
                              UNION
                              SELECT
                                PCP.[PracticeCatalogProductID]
                              , 'NA' AS cATEGORY
                              , 'NA' AS sUBcATEGORY
                              , TPP.[Name] AS pRODUCT
                              , TPP.[Code] AS Code
                              , TPP.[Packaging] AS Packaging
                              , TPP.[LeftRightSide] AS LeftRightSide
                              , TPP.[Size] AS Size
                              , TPP.[Gender] AS Gender
							  , TPP.[Color] AS Color
                              , PCP.PracticeCatalogSupplierBrandID AS SupplierID
                              , PCP.IsThirdPartyProduct
                              FROM                              
                                dbo.ThirdPartyProduct AS TPP WITH ( NOLOCK )
                                INNER JOIN [PracticeCatalogProduct] PCP WITH ( NOLOCK )
                                                                             ON PCP.[ThirdPartyProductID] = TPP.[ThirdPartyProductID]
                                INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
                                WITH ( NOLOCK )
                                     ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
                                INNER JOIN [ProductInventory] PI WITH ( NOLOCK )
                                                                      ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
                              WHERE
                                PI.[PracticeLocationID] = @PracticeLocationID
                                AND PCP.IsThirdPartyProduct = 1
                            ) AS Sub
                       ) AS CheckIn
                ON WT.practicecatalogproductid = CheckIn.practicecatalogproductid

end 
else if @SearchCriteria='Name'	-- ProductName
	begin
	
		-- When searching for a product name, search with wildcard on both sides.
		SET @SearchText = '%' + @SearchText
        UPDATE
            @WorkingTable
        SET 
            WT.Category = CheckIn.Category
          , WT.SubCategory = CheckIn.SubCategory
          , WT.Product = CheckIn.Product
          , WT.Code = CheckIn.Code
          , WT.Packaging = CheckIn.Packaging
          , WT.LeftRightSide = CheckIn.LeftRightSide
          , WT.Size = CheckIn.Size
          , WT.Gender = CheckIn.Gender
          , WT.Color = CheckIn.Color
          , WT.SupplierID = CheckIn.SupplierID
          , WT.IsThirdPartyProduct = CheckIn.IsThirdPartyProduct
        FROM
            @WorkingTable AS WT
            INNER JOIN (
                         SELECT
                            Sub.PracticeCatalogProductID
                          , Sub.CATEGORY
                          , Sub.SUBCATEGORY
                          , Sub.PRODUCT
                          , Sub.Code
                          , Sub.Packaging
                          , Sub.LeftRightSide
                          , Sub.Size
                          , Sub.Gender
						  , Sub.Color
                          , Sub.SupplierID
                          , Sub.IsThirdPartyProduct
                         FROM
                            (
                              SELECT
                                PCP.[PracticeCatalogProductID]
                              , mCC.NAME AS cATEGORY
                              , mcsc.[Name] AS sUBcATEGORY
                              , MCP.[Name] AS pRODUCT
                              , MCP.[Code] AS Code
                              , MCP.[Packaging] AS Packaging
                              , MCP.[LeftRightSide] AS LeftRightSide
                              , mcp.[Size] AS Size
                              , mcp.[Gender] AS Gender
                              , mcp.[Color] AS Color
                              , PCP.PracticeCatalogSupplierBrandID AS SupplierID
                              , PCP.IsThirdPartyProduct
                              FROM
                                [MasterCatalogSupplier] AS MCS WITH ( NOLOCK )
                                INNER JOIN [MasterCatalogCategory] AS MCC WITH ( NOLOCK )
                                                                               ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
                                INNER JOIN [MasterCatalogSubCategory] AS MCSC
                                WITH ( NOLOCK )
                                     ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
                                INNER JOIN [MasterCatalogProduct] AS MCP WITH ( NOLOCK )
                                                                              ---ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
                                                                              ON MCP.MasterCatalogSubCategoryID = MCSC.MasterCatalogSubCategoryID
                                INNER JOIN [PracticeCatalogProduct] AS PCP
                                WITH ( NOLOCK )
                                     ON MCP.[MasterCatalogProductID] = PCP.[MasterCatalogProductID]
                                INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
                                WITH ( NOLOCK )
                                     ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
                                INNER JOIN [ProductInventory] AS PI WITH ( NOLOCK )
                                                                         ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
                              WHERE
                                PI.[PracticeLocationID] = @PracticeLocationID
                                AND PCP.IsThirdPartyProduct = 0
								and (MCP.Name like @SearchText or MCP.ShortName like  @SearchText)
	                   
                   --  20071011 JB Union the thirdpartyproducts.
                              UNION
                              SELECT
                                PCP.[PracticeCatalogProductID]
                              , 'NA' AS cATEGORY
                              , 'NA' AS sUBcATEGORY
                              , TPP.[Name] AS pRODUCT
                              , TPP.[Code] AS Code
                              , TPP.[Packaging] AS Packaging
                              , TPP.[LeftRightSide] AS LeftRightSide
                              , TPP.[Size] AS Size
                              , TPP.[Gender] AS Gender
							  , TPP.[Color] AS Color
                              , PCP.PracticeCatalogSupplierBrandID AS SupplierID
                              , PCP.IsThirdPartyProduct
                              FROM                              
                                dbo.ThirdPartyProduct AS TPP WITH ( NOLOCK )
                                INNER JOIN [PracticeCatalogProduct] PCP WITH ( NOLOCK )
                                                                             ON PCP.[ThirdPartyProductID] = TPP.[ThirdPartyProductID]
                                INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
                                WITH ( NOLOCK )
                                     ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
                                INNER JOIN [ProductInventory] PI WITH ( NOLOCK )
                                                                      ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
                              WHERE
                                PI.[PracticeLocationID] = @PracticeLocationID
                                AND PCP.IsThirdPartyProduct = 1
								and (TPP.Name like @SearchText or TPP.ShortName like  @SearchText)
                            ) AS Sub
                       ) AS CheckIn
                ON WT.practicecatalogproductid = CheckIn.practicecatalogproductid
end 
else if @SearchCriteria='Code'
begin

        UPDATE
            @WorkingTable
        SET 
            WT.Category = CheckIn.Category
          , WT.SubCategory = CheckIn.SubCategory
          , WT.Product = CheckIn.Product
          , WT.Code = CheckIn.Code
          , WT.Packaging = CheckIn.Packaging
          , WT.LeftRightSide = CheckIn.LeftRightSide
          , WT.Size = CheckIn.Size
          , WT.Gender = CheckIn.Gender
          , WT.Color = CheckIn.Color
          , WT.SupplierID = CheckIn.SupplierID
          , WT.IsThirdPartyProduct = CheckIn.IsThirdPartyProduct
        FROM
            @WorkingTable AS WT
            INNER JOIN (
                         SELECT
                            Sub.PracticeCatalogProductID
                          , Sub.CATEGORY
                          , Sub.SUBCATEGORY
                          , Sub.PRODUCT
                          , Sub.Code
                          , Sub.Packaging
                          , Sub.LeftRightSide
                          , Sub.Size
                          , Sub.Gender
						  , Sub.Color
                          , Sub.SupplierID
                          , Sub.IsThirdPartyProduct
                         FROM
                            (
                              SELECT
                                PCP.[PracticeCatalogProductID]
                              , mCC.NAME AS cATEGORY
                              , mcsc.[Name] AS sUBcATEGORY
                              , MCP.[Name] AS pRODUCT
                              , MCP.[Code] AS Code
                              , MCP.[Packaging] AS Packaging
                              , MCP.[LeftRightSide] AS LeftRightSide
                              , mcp.[Size] AS Size
                              , mcp.[Gender] AS Gender
							  , mcp.[Color] AS Color
                              , PCP.PracticeCatalogSupplierBrandID AS SupplierID
                              , PCP.IsThirdPartyProduct
                              FROM
                                [MasterCatalogSupplier] AS MCS WITH ( NOLOCK )
                                INNER JOIN [MasterCatalogCategory] AS MCC WITH ( NOLOCK )
                                                                               ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
                                INNER JOIN [MasterCatalogSubCategory] AS MCSC
                                WITH ( NOLOCK )
                                     ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
                                INNER JOIN [MasterCatalogProduct] AS MCP WITH ( NOLOCK )
                                                                              ---ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
                                                                              ON MCP.MasterCatalogSubCategoryID = MCSC.MasterCatalogSubCategoryID
                                INNER JOIN [PracticeCatalogProduct] AS PCP
                                WITH ( NOLOCK )
                                     ON MCP.[MasterCatalogProductID] = PCP.[MasterCatalogProductID]
                                INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
                                WITH ( NOLOCK )
                                     ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
                                INNER JOIN [ProductInventory] AS PI WITH ( NOLOCK )
                                                                         ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
                              WHERE
                                PI.[PracticeLocationID] = @PracticeLocationID
                                AND PCP.IsThirdPartyProduct = 0
								and MCP.Code like @SearchText 
	                   
                   --  20071011 JB Union the thirdpartyproducts.
                              UNION
                              SELECT
                                PCP.[PracticeCatalogProductID]
                              , 'NA' AS cATEGORY
                              , 'NA' AS sUBcATEGORY
                              , TPP.[Name] AS pRODUCT
                              , TPP.[Code] AS Code
                              , TPP.[Packaging] AS Packaging
                              , TPP.[LeftRightSide] AS LeftRightSide
                              , TPP.[Size] AS Size
                              , TPP.[Gender] AS Gender
							  , TPP.[Color] AS Color
                              , PCP.PracticeCatalogSupplierBrandID AS SupplierID
                              , PCP.IsThirdPartyProduct
                              FROM                              
                                dbo.ThirdPartyProduct AS TPP WITH ( NOLOCK )
                                INNER JOIN [PracticeCatalogProduct] PCP WITH ( NOLOCK )
                                                                             ON PCP.[ThirdPartyProductID] = TPP.[ThirdPartyProductID]
                                INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
                                WITH ( NOLOCK )
                                     ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
                                INNER JOIN [ProductInventory] PI WITH ( NOLOCK )
                                                                      ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
                              WHERE
                                PI.[PracticeLocationID] = @PracticeLocationID
                                AND PCP.IsThirdPartyProduct = 1
								and TPP.Code like @SearchText
                            ) AS Sub
                       ) AS CheckIn
                ON WT.practicecatalogproductid = CheckIn.practicecatalogproductid

end                
else if @SearchCriteria='Brand'
begin

        UPDATE
            @WorkingTable
        SET 
            WT.Category = CheckIn.Category
          , WT.SubCategory = CheckIn.SubCategory
          , WT.Product = CheckIn.Product
          , WT.Code = CheckIn.Code
          , WT.Packaging = CheckIn.Packaging
          , WT.LeftRightSide = CheckIn.LeftRightSide
          , WT.Size = CheckIn.Size
          , WT.Gender = CheckIn.Gender
		  , WT.Color = CheckIn.Color
          , WT.SupplierID = CheckIn.SupplierID
          , WT.IsThirdPartyProduct = CheckIn.IsThirdPartyProduct
        FROM
            @WorkingTable AS WT
            INNER JOIN (
                         SELECT
                            Sub.PracticeCatalogProductID
                          , Sub.CATEGORY
                          , Sub.SUBCATEGORY
                          , Sub.PRODUCT
                          , Sub.Code
                          , Sub.Packaging
                          , Sub.LeftRightSide
                          , Sub.Size
                          , Sub.Gender
						  , Sub.Color	
                          , Sub.SupplierID
                          , Sub.IsThirdPartyProduct
                         FROM
                            (
                              SELECT
                                PCP.[PracticeCatalogProductID]
                              , mCC.NAME AS cATEGORY
                              , mcsc.[Name] AS sUBcATEGORY
                              , MCP.[Name] AS pRODUCT
                              , MCP.[Code] AS Code
                              , MCP.[Packaging] AS Packaging
                              , MCP.[LeftRightSide] AS LeftRightSide
                              , mcp.[Size] AS Size
                              , mcp.[Gender] AS Gender
							  , mcp.[Color] AS Color
                              , PCP.PracticeCatalogSupplierBrandID AS SupplierID
                              , PCP.IsThirdPartyProduct
                              FROM
                                [MasterCatalogSupplier] AS MCS WITH ( NOLOCK )
                                INNER JOIN [MasterCatalogCategory] AS MCC WITH ( NOLOCK )
                                                                               ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
                                INNER JOIN [MasterCatalogSubCategory] AS MCSC
                                WITH ( NOLOCK )
                                     ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
                                INNER JOIN [MasterCatalogProduct] AS MCP WITH ( NOLOCK )
                                                                              ---ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
                                                                              ON MCP.MasterCatalogSubCategoryID = MCSC.MasterCatalogSubCategoryID
                                INNER JOIN [PracticeCatalogProduct] AS PCP
                                WITH ( NOLOCK )
                                     ON MCP.[MasterCatalogProductID] = PCP.[MasterCatalogProductID]
                                INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
                                WITH ( NOLOCK )
                                     ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
                                INNER JOIN [ProductInventory] AS PI WITH ( NOLOCK )
                                                                         ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
                              WHERE
                                PI.[PracticeLocationID] = @PracticeLocationID
                                AND PCP.IsThirdPartyProduct = 0
								AND (PCSB.BrandName LIKE @SearchText OR PCSB.BrandShortName LIKE @SearchText)
	                   
                   --  20071011 JB Union the thirdpartyproducts.
                              UNION
                              SELECT
                                PCP.[PracticeCatalogProductID]
                              , 'NA' AS cATEGORY
                              , 'NA' AS sUBcATEGORY
                              , TPP.[Name] AS pRODUCT
                              , TPP.[Code] AS Code
                              , TPP.[Packaging] AS Packaging
                              , TPP.[LeftRightSide] AS LeftRightSide
                              , TPP.[Size] AS Size
                              , TPP.[Gender] AS Gender
							  , TPP.[Color] AS Color
                              , PCP.PracticeCatalogSupplierBrandID AS SupplierID
                              , PCP.IsThirdPartyProduct
                              FROM                              
                                dbo.ThirdPartyProduct AS TPP WITH ( NOLOCK )
                                INNER JOIN [PracticeCatalogProduct] PCP WITH ( NOLOCK )
                                                                             ON PCP.[ThirdPartyProductID] = TPP.[ThirdPartyProductID]
                                INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
                                WITH ( NOLOCK )
                                     ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
                                INNER JOIN [ProductInventory] PI WITH ( NOLOCK )
                                                                      ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
                              WHERE
                                PI.[PracticeLocationID] = @PracticeLocationID
                                AND PCP.IsThirdPartyProduct = 1
								AND ( 
										(PCSB.BrandName LIKE @SearchText OR PCSB.BrandShortName LIKE @SearchText)
									OR
										(PCSB.SupplierName LIKE @SearchText OR PCSB.SupplierShortName LIKE @SearchText)
									)
                            ) AS Sub
                       ) AS CheckIn
                ON WT.practicecatalogproductid = CheckIn.practicecatalogproductid

end             
else if @SearchCriteria='HCPCs' --MWS this section adds search by HCPCs
begin

        UPDATE
            @WorkingTable
        SET 
            WT.Category = CheckIn.Category
          , WT.SubCategory = CheckIn.SubCategory
          , WT.Product = CheckIn.Product
          , WT.Code = CheckIn.Code
          , WT.Packaging = CheckIn.Packaging
          , WT.LeftRightSide = CheckIn.LeftRightSide
          , WT.Size = CheckIn.Size
          , WT.Gender = CheckIn.Gender
		  , WT.Color = CheckIn.Color
          , WT.SupplierID = CheckIn.SupplierID
          , WT.IsThirdPartyProduct = CheckIn.IsThirdPartyProduct
        FROM
            @WorkingTable AS WT
            INNER JOIN (
                         SELECT
                            Sub.PracticeCatalogProductID
                          , Sub.CATEGORY
                          , Sub.SUBCATEGORY
                          , Sub.PRODUCT
                          , Sub.Code
                          , Sub.Packaging
                          , Sub.LeftRightSide
                          , Sub.Size
                          , Sub.Gender
						  , Sub.Color	
                          , Sub.SupplierID
                          , Sub.IsThirdPartyProduct
                         FROM
                            (
                              SELECT
                                PCP.[PracticeCatalogProductID]
                              , mCC.NAME AS cATEGORY
                              , mcsc.[Name] AS sUBcATEGORY
                              , MCP.[Name] AS pRODUCT
                              , MCP.[Code] AS Code
                              , MCP.[Packaging] AS Packaging
                              , MCP.[LeftRightSide] AS LeftRightSide
                              , mcp.[Size] AS Size
                              , mcp.[Gender] AS Gender
							  , mcp.[Color] AS Color
                              , PCP.PracticeCatalogSupplierBrandID AS SupplierID
                              , PCP.IsThirdPartyProduct
                              FROM
                                [MasterCatalogSupplier] AS MCS WITH ( NOLOCK )
                                INNER JOIN [MasterCatalogCategory] AS MCC WITH ( NOLOCK )
                                                                               ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
                                INNER JOIN [MasterCatalogSubCategory] AS MCSC
                                WITH ( NOLOCK )
                                     ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
                                INNER JOIN [MasterCatalogProduct] AS MCP WITH ( NOLOCK )
                                                                              ---ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
                                                                              ON MCP.MasterCatalogSubCategoryID = MCSC.MasterCatalogSubCategoryID
                                INNER JOIN [PracticeCatalogProduct] AS PCP
                                WITH ( NOLOCK )
                                     ON MCP.[MasterCatalogProductID] = PCP.[MasterCatalogProductID]
                                INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
                                WITH ( NOLOCK )
                                     ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
                                INNER JOIN [ProductInventory] AS PI WITH ( NOLOCK )
                                                                         ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
                              WHERE
                                PI.[PracticeLocationID] = @PracticeLocationID
                                AND PCP.IsThirdPartyProduct = 0
								--AND (PCSB.BrandName LIKE @SearchText OR PCSB.BrandShortName LIKE @SearchText)
								and dbo.ConcatHCPCS(PI.PracticeCatalogProductID) like @SearchText
								
                   --  20071011 JB Union the thirdpartyproducts.
                              UNION
                              SELECT
                                PCP.[PracticeCatalogProductID]
                              , 'NA' AS cATEGORY
                              , 'NA' AS sUBcATEGORY
                              , TPP.[Name] AS pRODUCT
                              , TPP.[Code] AS Code
                              , TPP.[Packaging] AS Packaging
                              , TPP.[LeftRightSide] AS LeftRightSide
                              , TPP.[Size] AS Size
                              , TPP.[Gender] AS Gender
							  , TPP.[Color] AS Color
                              , PCP.PracticeCatalogSupplierBrandID AS SupplierID
                              , PCP.IsThirdPartyProduct
                              FROM                              
                                dbo.ThirdPartyProduct AS TPP WITH ( NOLOCK )
                                INNER JOIN [PracticeCatalogProduct] PCP WITH ( NOLOCK )
                                                                             ON PCP.[ThirdPartyProductID] = TPP.[ThirdPartyProductID]
                                INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
                                WITH ( NOLOCK )
                                     ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
                                INNER JOIN [ProductInventory] PI WITH ( NOLOCK )
                                                                      ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
                              WHERE
                                PI.[PracticeLocationID] = @PracticeLocationID
                                AND PCP.IsThirdPartyProduct = 1
                                and dbo.ConcatHCPCS(PI.PracticeCatalogProductID) like @SearchText
								--AND ( 
										--(PCSB.BrandName LIKE @SearchText OR PCSB.BrandShortName LIKE @SearchText)
									--OR
										--(PCSB.SupplierName LIKE @SearchText OR PCSB.SupplierShortName LIKE @SearchText)
										
								--	)
                            ) AS Sub
                       ) AS CheckIn
                ON WT.practicecatalogproductid = CheckIn.practicecatalogproductid

end                                     
--  Below get counts


        SELECT
				Category
			  , SubCategory
			  , Product
			  , Code
			  , Packaging
			  , LeftRightSide
			  , Size
			  , Gender
			  , Color
	          
			  , SupplierID
			  , PracticeCatalogProductID

			  , QuantityOnHand
			  , QuantityOnOrder
	                    
			  , PARLevel
			  , ReorderLevel
			  , CriticalLevel
	          
			  , SuggestedReorderLevel
	          
			  --, IsThirdPartyProduct
        
        FROM
            @WorkingTable
        ORDER BY
            Category
          , Subcategory
          , Product
          , Code


    END




