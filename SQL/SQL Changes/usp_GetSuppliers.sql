USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetSuppliers]    Script Date: 02/03/2009 13:53:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author: Greg Ross>
-- Create date: <Create Date: 07/23/07>
-- Description:	<Description: Used to get all suppliers/Brands in database>
-- modification:  jb 10072007 fix query to be effient and add thirdpartysuppliers.
--  [dbo].[usp_GetSuppliers_2]  10
--
--     SupplierID by itself (using both master catalogsupplierid and thirdpartysupplierID
--		  will not distinguish between MCS Bird and Cronin and TPS DeRoyal
--			leading to B&C products being placed under DeRoyal

--  Modification:
--	2007.11.09 JB  Add to WHERE Clause "AND PCSB.ISACTIVE = 1" ; Add to LEFT JOIN ON CONTACT "AND C.ISACTIVE = 1"
--  20080317 JB  Format the Supplier-Brand when supplier is a third party vendor.  
--				Format the phone number, combine the firstandlast name of the contact.
--				Add state to city.
--
--                CASE WHEN (PCSB.IsThirdPartySupplier = 1 )AND (PCSB.SupplierShortName <> PCSB.BrandShortName) 		
--							THEN PCSB.SupplierShortName + ' - ' + PCSB.BrandShortName 
--							ELSE PCSB.SupplierShortName END AS Brand,      --AS SupplierName,  -- 20080317 JB
--Copied from usp_GetSuppliers, which is called from pages: 
--		Inventory.aspx
--		.aspx
--		.aspx
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetSuppliers]  --3 --10  --  _With_Brands
	@PracticeLocationID int
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
--        SET NOCOUNT ON ;
     
     SELECT 
		Sub.Brand,		
		Sub.SupplierID,

-- comment this block out to test; uncomment to use.
        --  use LATER for displaying the FullName        
        Sub.Name + ' ' + Sub.LastName + CHAR(13)+ CHAR(10) + Sub.PhoneWork AS FullName,        
        --  use LATER for displaying the FullAddress
--        CASE WHEN LEN(Sub.Address) = 0 
--			THEN '' 
--			ELSE Sub.Address + CHAR(13)+ CHAR(10) + Sub.City + ', ' + Sub.State + '  ' + Sub.ZipCode 
--			END AS FullAddress,

        Sub.Name + ' ' + Sub.LastName AS NAME, --AS FullName,        
		'' as lastname, --        Sub.LastName,
		
      CASE WHEN LEN(Sub.PhoneWork) = 10   
			THEN '(' + LEFT(Sub.PhoneWork, 3) + ') ' + LEFT(right(Sub.PhoneWork, 7), 3) + '-' + RIGHT(Sub.PhoneWork, 4)
			ELSE ''
			END AS PhoneWork,
        Sub.Address,
        
        CASE WHEN LEN(LTRIM(RTRIM(Sub.City))) = 0 THEN ''
        ELSE RTRIM(Sub.City) + ', ' + Sub.State
        END AS City,
        
       -- Sub.City,  
        Sub.Zipcode,        
        Sub.IsThirdPartySupplier


----      uncomment to test, comment to use
--		--  Use as TEST for a professional display.
--        --  use LATER for displaying the FullName        
--        Sub.Name + ' ' + Sub.LastName AS NAME, --AS FullName,        
--        CASE WHEN LEN(Sub.PhoneWork) = 10   
--			THEN '(' + LEFT(Sub.PhoneWork, 3) + ') ' + LEFT(right(Sub.PhoneWork, 7), 3) + '-' + RIGHT(Sub.PhoneWork, 4)
--			ELSE ''
--			END AS PhoneWork,
--			
--        --  use LATER for displaying the FullAddress
--        CASE WHEN LEN(Sub.Address) = 0 
--			THEN '' 
--			ELSE Sub.Address + '<br>' + Sub.City + ', ' + Sub.State + '  ' + Sub.ZipCode 
--			END AS Address, --AS FullAddress,
----		Sub.Name,
--		'' as lastname, --        Sub.LastName,
----		Sub.PhoneWork,
----        Sub.Address,
--		'' as city, --        Sub.City,  
--		'' as zipcode, --        Sub.Zipcode,        
--        Sub.IsThirdPartySupplier

				
     
	FROM     
     
     (
     SELECT DISTINCT  
					--  DISTINCT PCSB.[SupplierName] AS Brand,
					PCSB.BrandShortName AS Brand,  --JB display name for brand
                
                
                PCSB.PracticeCatalogSupplierBrandID AS supplierID,           
                --  PCSB.[MasterCatalogSupplierID] AS OldSupplierID,
                
                --PCSB.[MasterCatalogSupplierID] AS SupplierID,
                
				ISNULL(c.[FirstName], '') AS Name,
                ISNULL(C.[LastName], '')  AS LastName,
                ISNULL(c.[PhoneWork], '')  AS PhoneWork,
                
                ISNULL(A.[AddressLine1], '')   AS Address,
                ISNULL(A.City, '')  AS City,  
                ISNULL(A.State, '')  AS State,  
                ISNULL(A.[ZipCode], '')   AS Zipcode,
                0 AS IsThirdPartySupplier,
                PCSB.Sequence
				--MCC.[Name] AS Category


        FROM    [MasterCatalogSupplier] AS MCS  WITH (NOLOCK)
                LEFT JOIN address A			  WITH (NOLOCK)
						ON MCS.[AddressID] = A.[AddressID]
                LEFT JOIN [Contact] C			  WITH (NOLOCK)
						ON mcs.[ContactID] = c.[ContactID]
						AND C.ISACTIVE = 1
				INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB		  WITH (NOLOCK)
					ON MCS.MasterCatalogSupplierID = pcsb.MasterCatalogSupplierID

				INNER JOIN [PracticeCatalogProduct] PCP				  WITH (NOLOCK)
					ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID				
										
				INNER JOIN [ProductInventory] PI		  WITH (NOLOCK)
					ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]

				WHERE PI.[PracticeLocationID] = @PracticeLocationID
					AND    MCS.ISACTIVE = 1
					AND A.ISACTIVE = 1
					AND PCSB.IsActive = 1
					AND PCP.ISACTIVE = 1
					--  AND PI.ISACTIVE = 1
					AND IsThirdPartySupplier = 0 -- MasterCatalogSuppliers.
					
UNION

     SELECT     DISTINCT --  DISTINCT PCSB.[SupplierName] AS Brand,
				--DISTINCT PCSB.BrandShortName AS Brand,  --JB display name for brand
                
                CASE WHEN (PCSB.IsThirdPartySupplier = 1 )AND (PCSB.SupplierShortName <> PCSB.BrandShortName) 		
							THEN PCSB.SupplierShortName + ' - ' + PCSB.BrandShortName 
							ELSE PCSB.SupplierShortName END AS Brand,      --AS SupplierName,  -- 20080317 JB

                PCSB.PracticeCatalogSupplierBrandID AS supplierID,    --  SupplierBrandID is neccessary to associate
--														--   the child grid without criss-crossing.              
--                TPS.[ThirdPartySupplierID] AS OldSupplierID,
				--TPS.[ThirdPartySupplierID] AS SupplierID,
                
				ISNULL(c.[FirstName], '') AS Name,
                ISNULL(C.[LastName], '')  AS LastName,
                ISNULL(c.[PhoneWork], '')  AS PhoneWork,
                
                ISNULL(A.[AddressLine1], '')   AS Address,
                ISNULL(A.City, '') AS City,  
                ISNULL(A.State, '') AS State,  
                ISNULL(A.[ZipCode], '')   AS Zipcode,
                1 AS IsThirdPartySupplier,
                PCSB.Sequence
				--MCC.[Name] AS Category


        FROM    [dbo].[ThirdPartySupplier] AS TPS    WITH (NOLOCK)
                LEFT JOIN address A					  WITH (NOLOCK)
						ON TPS.[AddressID] = A.[AddressID]
                lefT JOIN [Contact] C				  WITH (NOLOCK)
						ON TPS.[ContactID] = c.[ContactID]
						AND C.ISACTIVE = 1
				INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	  WITH (NOLOCK)
					ON TPS.ThirdPartySupplierID = pcsb.ThirdPartySupplierID

				INNER JOIN [PracticeCatalogProduct] PCP   WITH (NOLOCK)
					ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID				
										
				INNER JOIN [ProductInventory] PI   WITH (NOLOCK)
					ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]

				WHERE PI.[PracticeLocationID] = @PracticeLocationID
					AND    tps.ISACTIVE = 1
					--AND A.ISACTIVE = 1
					--AND C.ISACTIVE = 1
					AND PCSB.IsActive = 1
					AND PCP.ISACTIVE = 1
					AND PI.ISACTIVE = 1
					AND IsThirdPartySupplier = 1 -- ThirdPartySuppliers.
					
			) AS Sub
			
		ORDER BY
			SUB.IsThirdPartySupplier
			, SUB.Sequence
			, SUB.Brand

    END





