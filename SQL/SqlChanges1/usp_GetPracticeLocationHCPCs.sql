USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetPracticeLocationHCPCs]    Script Date: 12/14/2015 4:35:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Author Mike Sneen
Date   2/20/2010
	exec usp_GetPracticeLocationHCPCs '3,4,5'
	exec usp_GetPracticeLocationHCPCs '3'
	exec usp_GetPracticeLocationHCPCs @PracticeLocationIDString=N'26,14'
*/

ALTER PROC [dbo].[usp_GetPracticeLocationHCPCs]  
		  @PracticeLocationIDString			varchar(2000),
		  @ExtraHCPC						VARCHAR(2000) = NULL
AS	  
Begin

IF @ExtraHCPC IS NULL
BEGIN
	Select 
		LTrim(RTrim(ph.HCPCS )) AS HCPCS
	from 
		ProductInventory pi
		inner join PracticeCatalogProduct pcp
			on pi.PracticeCatalogProductID = pcp.PracticeCatalogProductID
		inner join ProductHCPCS ph on
			pcp.PracticeCatalogProductID = ph.PracticeCatalogProductID
	where
		pi.PracticeLocationID in (SELECT ID 
				 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))
	Group By ph.HCPCS
	Order By ph.HCPCS
END
ELSE
BEGIN
	SELECT HCPCS FROM
		(SELECT HCPCS FROM
			(Select 
				LTrim(RTrim(ph.HCPCS )) AS HCPCS
			from 
				ProductInventory pi
				inner join PracticeCatalogProduct pcp
					on pi.PracticeCatalogProductID = pcp.PracticeCatalogProductID
				inner join ProductHCPCS ph on
					pcp.PracticeCatalogProductID = ph.PracticeCatalogProductID
			where
				pi.PracticeLocationID in (SELECT ID 
						 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))
			) a

		UNION

			(SELECT @ExtraHCPC AS HCPCS)
		) AS SUB
	Group By HCPCS
	Order By HCPCS
END

END