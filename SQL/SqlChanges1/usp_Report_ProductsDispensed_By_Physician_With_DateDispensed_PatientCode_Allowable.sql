USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_Report_ProductsDispensed_By_Physician_With_DateDispensed_PatientCode_Allowable]    Script Date: 3/26/2017 11:35:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[usp_Report_ProductsDispensed_By_Physician_With_DateDispensed_PatientCode_Allowable]
		  @PracticeID			INT
		, @PracticeLocationID	VARCHAR(2000)
		, @PhysicianID			VARCHAR(2000)
		, @StartDate			DATETIME
		, @EndDate				DATETIME
		, @OrderBy				INT
		, @OrderDirection		BIT
		, @IsMedicare           BIT = NULL
		, @MedicareOrderDirection BIT = NULL
		, @InternalPracticeLocationID	VARCHAR(2000)
		, @InternalPhysicianID	VARCHAR(2000)

AS
BEGIN


SELECT @EndDate = DATEADD(DAY, 1, @EndDate)

-- Parse delimited lists of IDs
DECLARE @locations TABLE([PracticeLocationID] INT);

-- If we passed "All" for Practice Locations, get all of the
-- Practice Locations for the passed PracticeID.
-- Otherwise, parse as before.
IF(@InternalPracticeLocationID = 'ALL')
BEGIN
	DECLARE @tempLocations TABLE (
		PracticeLocationID INT
		,PracticeLocationName VARCHAR(50)
		,IsPrimaryLocation BIT
		);

	INSERT @tempLocations
	EXEC dbo.usp_PracticeLocation_Select_All_ID_Name_Given_PracticeID @PracticeID = @PracticeID;

	INSERT @locations ([PracticeLocationID])
	SELECT PracticeLocationID
	FROM @tempLocations;
END;
ELSE
BEGIN
	INSERT @locations ([PracticeLocationID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable(@PracticeLocationID, ',');
END;

-- Parse Physicians
DECLARE @physicians TABLE([PhysicianID] INT);

-- If we passed "All" for Physicians, get all of the
-- Physicians for the passed Practice Locations.
-- Otherwise, parse as before.
IF(@InternalPhysicianID = 'ALL')
BEGIN
	DECLARE @tempPhysicians TABLE (
		PracticeID INT
		,PhysicianID INT
		,ContactID INT
		,PhysicianName VARCHAR(100)
		);

	INSERT @tempPhysicians
	EXEC dbo.usp_Physician_SelectALL_Names_ByPracticeID @PracticeID = @PracticeID;

	INSERT @physicians ([PhysicianID])
	SELECT PhysicianID
	FROM @tempPhysicians;

	-- Include NULL as a valid value in "All"
	INSERT @physicians ([PhysicianID])
	SELECT NULL;
END;
ELSE
BEGIN
	INSERT @physicians ([PhysicianID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianID, ',');
END;

SELECT

		  ISNULL(SUB.PhysicianID, -999) AS PhysicianID
		, SUB.PracticeName		AS Practice
		, SUB.PracticeLocation	AS Location

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
			ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
			END					AS SupplierBrand
		, SUB.SupplierShortName	AS Supplier

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
		ELSE SUB.BrandShortName
		END						AS Brand
		, SUB.Sequence
		, SUB.ProductName			AS Product
		, SUB.FitterName
		, SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender
		, SUB.Code
		, COALESCE(SUB.HCPCs, dbo.ConcatHCPCS(SUB.PracticeCatalogProductID)) AS HCPCS -- JB 20071004 Added Column for UDF Concat of HCPCS
		, CASE SUB.IsCashCollection
			WHEN 1 THEN Sub.BillingChargeCash
			WHEN 0 THEN 0
			END
			 AS BillingChargeCash

		, CASE WHEN LEN(SUB.Physician) > 0 THEN SUB.Physician ELSE '<blank>' END AS Physician
		, CONVERT(VARCHAR(10), SUB.DateDispensed, 101) AS DateDispensed
		, SUB.PatientCode

		, CASE SUB.IsMedicare
			WHEN 1 THEN 'Y'
			WHEN 0 THEN 'N'
		  END
		  AS IsMedicare
		, CASE CAST(SUB.ABNType AS varchar(50))
			WHEN 'Medicare' THEN 'M' + ',' + CAST(SUB.MedicareOption AS varchar(50))
			WHEN 'Commercial' THEN 'C' + ',' + CAST(SUB.MedicareOption AS varchar(50))
		  END
		  AS ABNType

		, SUB.ICD9Code
		, SUB.Quantity 		--, SUM( SUB.Quantity )
		, CAST(  SUB.ActualWholesaleCostLineTotal  AS DECIMAL(15,2) )	AS ActualWholesaleCostLineTotal
		, CAST(  SUB.DMEDepositLineTotal  AS DECIMAL(15,2) )			AS DMEDepositLineTotal
		, SUB.IsCashCollection
		,  CASE SUB.IsCashCollection
			WHEN 1 THEN 0
			WHEN 0 THEN CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )
			END
			AS ActualChargeBilledLineTotal
		, ISNULL(SUB.PayerName, 'N/A') AS PayerName
		, SUB.DispenseNote
		, SUB.OriginalActualChargeBilled
		, CAST(ISNULL(SUB.AllowableLineTotal,0)   AS DECIMAL(15,2) ) AS AllowableLineTotal
		, LTRIM(RTRIM(ISNULL(SUB.PatientFirstName,''))) AS PatientFirstName
		, LTRIM(RTRIM(ISNULL(SUB.PatientLastName,''))) AS PatientLastName
		, SUB.Mod1
		, SUB.Mod2
		, SUB.Mod3
FROM


		(
		SELECT
			  Ph.PhysicianID
			, P.PracticeName
			, PL.Name AS PracticeLocation
			, ISNULL(RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix), '') AS Physician

			, D.PatientCode

			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) AS Sequence

			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash

			, MCP.[Name] AS ProductName
			, CAST(ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender

			, ISNULL(MCP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed  -- , DD.CreatedDate AS DateDispensed  --  2008.02.12  JB  --, DD.CreatedDate AS DateDispensed
			, DD.IsMedicare
			, DD.ABNType
			, DD.ICD9Code
			, DD.Quantity
			, FC.FirstName + ' ' + FC.LastName as FitterName
			, PI_DD.ActualWholesaleCost
			, DD.ActualChargeBilled AS ActualChargeBilled
			, DD.DMEDeposit
			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit AS DMEDepositLineTotal
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.

			, PP.Name AS PayerName
			, D.Notes AS DispenseNote
			, DQ.HCPCs
			, DD.ActualChargeBilled AS OriginalActualChargeBilled
			, allowables.allowableamountSum AS AllowableLineTotal
			, D.PatientFirstName AS PatientFirstName
			, D.PatientLastName AS PatientLastName
			, DD.Mod1
			, DD.Mod2
			, DD.Mod3
			, DQ.MedicareOption

		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOIN DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
			ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID		    --  Can be inactive

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1

		LEFT JOIN PracticePayer PP
			ON PP.PracticePayerID = D.PracticePayerID

		LEFT JOIN DispenseQueue DQ
			ON DQ.DispenseQueueID = DD.DispenseQueueID

		LEFT JOIN (SELECT  sqppa.practicepayerid,  sqph.PracticeCatalogProductID AS pcpID, SUM(sqppa.allowableamount) AS allowableAmountSum
			FROM dbo.ProductHCPCs sqph
				JOIN dbo.Hcpcs sqhcpcs ON SUBSTRING(sqph.HCPCS,1,5) = sqhcpcs.Code
				JOIN [dbo].[PracticePayerAllowable] sqppa ON sqppa.hcpcsID = sqhcpcs.hcpcsid
				JOIN dbo.practicepayer sqpp ON sqpp.practicepayerid = sqppa.practicepayerid
				GROUP BY sqppa.practicepayerid,  sqph.PracticeCatalogProductID
			  ) AS Allowables
			ON Allowables.pcpID = PCP.PracticeCatalogProductID AND Allowables.practicepayerid = PP.PracticePayerID
		
		INNER JOIN Clinician AS CP
			ON CP.ClinicianID = DD.FitterID
		INNER JOIN Contact AS FC
			ON CP.ContactID = FC.ContactID

		WHERE
			P.PracticeID = @PracticeID
			AND DD.IsMedicare = COALESCE(@IsMedicare, DD.IsMedicare)
			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			--AND PCSB.IsActive = 1

		AND
		PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			 FROM @locations)

		AND
			(
					PH.PhysicianID
					IN (
						 SELECT PhysicianID
						 FROM @physicians
						)
					OR
				(
					(PH.PhysicianID IS NULL)
						AND
					( 1 = (SELECT CASE WHEN PhysicianID = -999 THEN 1 ELSE 0 END --AS IsNoPhysician
					 FROM @physicians
					 WHERE PhysicianID = -999)
					)
				)
			)

		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM

		UNION ALL

		SELECT

			  Ph.PhysicianID
			, P.PracticeName
			, PL.Name AS PracticeLocation
			, ISNULL(RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix), '') AS Physician
			, D.PatientCode
			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) + 1000 AS Sequence
			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash
			, TPP.[Name] AS ProductName
			, CAST(ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
			, ISNULL(TPP.Code, '') AS Code
			, D.DateDispensed AS DateDispensed  -- , DD.CreatedDate AS DateDispensed  --  2008.02.12  JB
			, DD.IsMedicare
			, DD.ABNType
			, DD.ICD9Code
			, DD.Quantity
			, FC.FirstName + ' ' + FC.LastName as FitterName
			, PI_DD.ActualWholesaleCost
			, DD.ActualChargeBilled AS ActualChargeBilled
			, DD.DMEDeposit
			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit AS DMEDepositLineTotal
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal
			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.
			, PP.Name AS PayerName
			, D.Notes AS DispenseNote
			, DQ.HCPCs
			, DD.ActualChargeBilled AS OriginalActualChargeBilled
			, allowables.allowableamountSum AS AllowableLineTotal
			, D.PatientFirstName AS PatientFirstName
			, D.PatientLastName AS PatientLastName
			, DD.Mod1
			, DD.Mod2
			, DD.Mod3
			, DQ.MedicareOption
		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOIN DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
			ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID			--  Can be inactive

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1

		LEFT JOIN PracticePayer PP
			ON PP.PracticePayerID = D.PracticePayerID

		LEFT JOIN DispenseQueue DQ
			ON DQ.DispenseQueueID = DD.DispenseQueueID

		LEFT JOIN (SELECT  sqppa.practicepayerid,  sqph.PracticeCatalogProductID AS pcpID, SUM(sqppa.allowableamount) AS allowableAmountSum
			FROM dbo.ProductHCPCs sqph
				JOIN dbo.Hcpcs sqhcpcs ON SUBSTRING(sqph.HCPCS,1,5) = sqhcpcs.Code
				JOIN [dbo].[PracticePayerAllowable] sqppa ON sqppa.hcpcsID = sqhcpcs.hcpcsid
				JOIN dbo.practicepayer sqpp ON sqpp.practicepayerid = sqppa.practicepayerid
				GROUP BY sqppa.practicepayerid,  sqph.PracticeCatalogProductID
			  ) AS Allowables
			ON Allowables.pcpID = PCP.PracticeCatalogProductID AND Allowables.practicepayerid = PP.PracticePayerID
		
		INNER JOIN Clinician AS CP
			ON CP.ClinicianID = DD.FitterID
		INNER JOIN Contact AS FC
			ON CP.ContactID = FC.ContactID

		WHERE
			P.PracticeID = @PracticeID
			AND DD.IsMedicare = COALESCE(@IsMedicare, DD.IsMedicare)
			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			--AND PCP.IsActive = 1
			--AND PCSB.IsActive = 1
			--AND TPP.IsActive = 1

		AND
		PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			 FROM @locations)

		AND
			(
					PH.PhysicianID
					IN (
						 SELECT PhysicianID
						 FROM @physicians
						)
					OR

				(
					(PH.PhysicianID IS NULL)
						AND
					( 1 = (SELECT CASE WHEN PhysicianID = -999 THEN 1 ELSE 0 END --AS IsNoPhysician
					 FROM @physicians
					 WHERE PhysicianID = -999)
					)
				)
			)


		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM

		) AS Sub

	 ORDER BY
		CASE
			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName DESC))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName))

			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName DESC))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName))

			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName DESC ))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName ))


			WHEN @OrderBy = 2 AND @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 AND @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed))

			WHEN @OrderBy = 2 AND @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 AND @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed))

			WHEN @OrderBy = 2 AND @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 AND @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed))

		END
END
