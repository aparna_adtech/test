use [BregVision]
go

if exists (select * from dbo.syscolumns where id = object_id(N'PracticeApplicationAgreementTerms'))
drop table [PracticeApplicationAgreementTerms]
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PracticeApplicationAgreementTerms](
	[PracticeAppAgreementTermsID] [int] IDENTITY(1,1) NOT NULL,
	[Terms] [nvarchar](max) NULL,
	[DateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_PracticeApplicationAgreementTerms] PRIMARY KEY CLUSTERED 
(
	[PracticeAppAgreementTermsID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



GO