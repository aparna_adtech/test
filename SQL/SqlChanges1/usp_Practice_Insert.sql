USE [BregVision_STG]
GO
/****** Object:  StoredProcedure [dbo].[usp_Practice_Insert]    Script Date: 11/22/2014 9:09:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Practice_Insert
   
   Description:  Inserts a record into table Practice
   
   AUTHOR:       John Bongiorni 7/25/2007 9:45:20 PM
   
   Modifications: 
   Declare @Pid int 
   exec [usp_Practice_Insert] @Pid, 'test Practice Proc', 1, 0, 0
   
   Added FaxDispenseReceiptEnabled				Ysi             09/07/2013
   ------------------------------------------------------------ */  
   
ALTER PROCEDURE [dbo].[usp_Practice_Insert]
(
      @PracticeID                    INT = NULL OUTPUT
    , @PracticeName                  VARCHAR(50)
    , @CreatedUserID                 INT
    , @IsOrthoSelect				 bit
    , @IsVisionLite					 bit = 0
    , @PaymentTypeID				 INT = 1
    , @CameraScanEnabled			 bit = 0
    , @EMRIntegrationPullEnabled	 bit = 0
    , @FaxDispenseReceiptEnabled	 bit = 0
	, @IsSecureMessagingInternalEnabled bit = 0
	, @IsSecureMessagingExternalEnabled bit = 0
)
AS
BEGIN
	DECLARE @Err INT

	INSERT
	INTO dbo.Practice
	(
		  PracticeName
        , CreatedUserID
        , IsOrthoSelect
        , IsVisionLite
        , BillingStartDate
        , PaymentTypeID
        , IsActive
        , CameraScanEnabled
        , EMRIntegrationPullEnabled
        , FaxDispenseReceiptEnabled
		, IsSecureMessagingInternalEnabled
		, IsSecureMessagingExternalEnabled
	)
	VALUES
	(
          @PracticeName
        , @CreatedUserID
        , @IsOrthoSelect
        , @IsVisionLite
        , Case When @IsVisionLite = 1 then GETDATE() else '12/31/2019 12:00:00 AM' END
        , @PaymentTypeID
        , 1
        , @CameraScanEnabled
        , @EMRIntegrationPullEnabled
        , @FaxDispenseReceiptEnabled
		, @IsSecureMessagingInternalEnabled
		, @IsSecureMessagingExternalEnabled
	)

	SET @Err = @@ERROR
	SELECT @PracticeID = SCOPE_IDENTITY()
	

	RETURN @Err
END

