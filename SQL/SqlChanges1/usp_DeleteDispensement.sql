USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteDispensement]    Script Date: 12/02/2009 12:25:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
	Greg Ross
	2008.11.28
	Delete Dispensements

*/

--  usp_CreatePurchaseOrder  0, 414

ALTER PROC [dbo].[usp_DeleteDispensement]

    @DispenseID INT,		--  USELESS, Not used for; for backward compatibility only. 
    @DispenseDetailID INT,
	@Quantity INT,
    @DeleteStatus INT OUTPUT

AS 



declare @PracticelocationID int
declare @PracticeCatalogProductID int

set @PracticelocationID = (select practicelocationid from Dispense where dispenseid=@DispenseID)
set @PracticeCatalogProductID = (select PracticeCatalogProductID from DispenseDetail where DispenseDetailID = @DispenseDetailID)


	BEGIN TRY
		BEGIN TRY
			BEGIN TRANSACTION    -- Start the transaction
				
				--Delete from two tables where Dispense Detail wwil be 
				delete from productinventory_dispensedetail where dispensedetailid	= @DispenseDetailID		
				delete from DispenseDetail where DispenseDetailID = @DispenseDetailID	
				--Add Item back to inventory		
   				--All records deleted add  items back to inventory
				update productinventory set QuantityOnHandPerSystem = QuantityOnHandPerSystem + @Quantity
					where PracticelocationID = @PracticelocationID
					and PracticeCatalogProductID = @PracticeCatalogProductID	  

			COMMIT TRANSACTION	

			
			
			SET @DeleteStatus = 0
		END TRY
		BEGIN CATCH
		  -- There was an error, so rollback the entire transaction.
		  
			  IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION
			  SET @DeleteStatus = -1
				
			  Declare  @ErrMsg nvarchar(4000), @ErrSeverity int
			  Select @ErrMsg=ERROR_MESSAGE(), @ErrSeverity=ERROR_SEVERITY()
				
			  RAISERROR(@ErrMsg,@ErrSeverity, 1)

		END CATCH
		
		Begin Try
			Delete from Dispense where DispenseID =	@DispenseID	
		End Try
		Begin Catch
			SET @DeleteStatus = 1
		End Catch
	END TRY
	BEGIN CATCH
	
	END CATCH

		
