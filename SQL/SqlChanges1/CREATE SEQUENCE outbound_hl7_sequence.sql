use BregVisionEMR
go

create sequence outbound_hl7_sequence
	as int
	start with 0
	increment by 1
	cycle
	no cache
;
