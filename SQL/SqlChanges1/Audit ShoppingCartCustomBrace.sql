USE [BregVision]
GO

/****** Object:  Table [dbo].[ShoppingCartCustomBrace_Audit]    Script Date: 07/16/2012 10:21:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ShoppingCartCustomBrace_Audit](
	[ShoppingCartCustomBraceAuditID] [int] IDENTITY(1,1) NOT NULL,
	[ShoppingCartID] [int] NOT NULL,
	[BregVisionOrderID] [int] NULL,
	[DispenseID] [int] NULL,
	[Billing_CustomerNumber] [varchar](50) NULL,
	[Billing_Phone] [varchar](50) NULL,
	[Billing_Attention] [varchar](50) NULL,
	[Billing_Address1] [varchar](50) NULL,
	[Billing_Address2] [varchar](50) NULL,
	[Billing_City] [varchar](50) NULL,
	[Billing_State] [varchar](50) NULL,
	[Billing_Zip] [varchar](50) NULL,
	[Shipping_Attention] [varchar](50) NULL,
	[Shipping_Address1] [varchar](50) NULL,
	[Shipping_Address2] [varchar](50) NULL,
	[Shipping_City] [varchar](50) NULL,
	[Shipping_State] [varchar](50) NULL,
	[Shipping_Zip] [varchar](50) NULL,
	[PatientName] [varchar](50) NULL,
	[PhysicianID] [int] NULL,
	[Patient_Age] [int] NULL,
	[Patient_Weight] [int] NULL,
	[Patient_Height] [varchar](50) NULL,
	[Patient_Sex] [varchar](8) NULL,
	[BraceFor] [varchar](50) NULL,
	[Instability] [varchar](50) NULL,
	[BilatCastReform] [varchar](50) NULL,
	[ThighCircumference] [varchar](50) NULL,
	[CalfCircumference] [varchar](50) NULL,
	[KneeOffset] [varchar](50) NULL,
	[KneeWidth] [varchar](50) NULL,
	[Extension] [varchar](50) NULL,
	[Flexion] [varchar](50) NULL,
	[MeasurementsTakenBy] [varchar](150) NULL,
	[X2K_Counterforce] [varchar](50) NULL,
	[X2K_Counterforce_Degrees] [varchar](50) NULL,
	[X2K_FramePadColor1] [varchar](50) NULL,
	[X2K_FramePadColor2] [varchar](50) NULL,
	[Fusion_Enhancement] [varchar](50) NULL,
	[Fusion_Notes] [varchar](1000) NULL,
	[Fusion_Color] [varchar](50) NULL,
	[Fusion_Pantone] [varchar](50) NULL,
	[Fusion_Pattern] [varchar](50) NULL,
	[Fusion_Pattern_Notes] [varchar](250) NULL,
	[ShippingTypeID] [int] NULL,
	[ShippingCarrierID] [int] NULL,
	[Instability_BilatRightLeg] [varchar](50) NULL,
	[X2K_Counterforce_BilatRightLeg] [varchar](50) NULL,
	[X2K_Counterforce_Degrees_BilatRightLeg] [varchar](50) NULL,
	[ThighCircumference_BilatRightLeg] [varchar](50) NULL,
	[CalfCircumference_BilatRightLeg] [varchar](50) NULL,
	[KneeOffset_BilatRightLeg] [varchar](50) NULL,
	[KneeWidth_BilatRightLeg] [varchar](50) NULL,
	[Extension_BilatRightLeg] [varchar](50) NULL,
	[Flexion_BilatRightLeg] [varchar](50) NULL,
	[AuditAction] [varchar](50) NULL,
	[AuditCreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ShoppingCartCustomBrace_Audit] PRIMARY KEY CLUSTERED 
(
	[ShoppingCartCustomBraceAuditID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ShoppingCartCustomBrace_Audit] ADD  CONSTRAINT [DF_ShoppingCartCustomBrace_Audit_AuditCreatedDate]  DEFAULT (GETDATE()) FOR [AuditCreatedDate]
GO

CREATE TRIGGER [dbo].[trg_ShoppingCartCustomBrace_Deleted] 
ON [dbo].[ShoppingCartCustomBrace] 
FOR DELETE 
AS
BEGIN

	INSERT INTO [ShoppingCartCustomBrace_Audit]
	(
		[ShoppingCartID]
		,[BregVisionOrderID]
		,[DispenseID]
		,[Billing_CustomerNumber]
		,[Billing_Phone]
		,[Billing_Attention]
		,[Billing_Address1]
		,[Billing_Address2]
		,[Billing_City]
		,[Billing_State]
		,[Billing_Zip]
		,[Shipping_Attention]
		,[Shipping_Address1]
		,[Shipping_Address2]
		,[Shipping_City]
		,[Shipping_State]
		,[Shipping_Zip]
		,[PatientName]
		,[PhysicianID]
		,[Patient_Age]
		,[Patient_Weight]
		,[Patient_Height]
		,[Patient_Sex]
		,[BraceFor]
		,[Instability]
		,[BilatCastReform]
		,[ThighCircumference]
		,[CalfCircumference]
		,[KneeOffset]
		,[KneeWidth]
		,[Extension]
		,[Flexion]
		,[MeasurementsTakenBy]
		,[X2K_Counterforce]
		,[X2K_Counterforce_Degrees]
		,[X2K_FramePadColor1]
		,[X2K_FramePadColor2]
		,[Fusion_Enhancement]
		,[Fusion_Notes]
		,[Fusion_Color]
		,[Fusion_Pantone]
		,[Fusion_Pattern]
		,[Fusion_Pattern_Notes]
		,[ShippingTypeID]
		,[ShippingCarrierID]
		,[Instability_BilatRightLeg]
		,[X2K_Counterforce_BilatRightLeg]
		,[X2K_Counterforce_Degrees_BilatRightLeg]
		,[ThighCircumference_BilatRightLeg]
		,[CalfCircumference_BilatRightLeg]
		,[KneeOffset_BilatRightLeg]
		,[KneeWidth_BilatRightLeg]
		,[Extension_BilatRightLeg]
		,[Flexion_BilatRightLeg]
		,[AuditAction]
	)
	SELECT
		[ShoppingCartID]
		,[BregVisionOrderID]
		,[DispenseID]
		,[Billing_CustomerNumber]
		,[Billing_Phone]
		,[Billing_Attention]
		,[Billing_Address1]
		,[Billing_Address2]
		,[Billing_City]
		,[Billing_State]
		,[Billing_Zip]
		,[Shipping_Attention]
		,[Shipping_Address1]
		,[Shipping_Address2]
		,[Shipping_City]
		,[Shipping_State]
		,[Shipping_Zip]
		,[PatientName]
		,[PhysicianID]
		,[Patient_Age]
		,[Patient_Weight]
		,[Patient_Height]
		,[Patient_Sex]
		,[BraceFor]
		,[Instability]
		,[BilatCastReform]
		,[ThighCircumference]
		,[CalfCircumference]
		,[KneeOffset]
		,[KneeWidth]
		,[Extension]
		,[Flexion]
		,[MeasurementsTakenBy]
		,[X2K_Counterforce]
		,[X2K_Counterforce_Degrees]
		,[X2K_FramePadColor1]
		,[X2K_FramePadColor2]
		,[Fusion_Enhancement]
		,[Fusion_Notes]
		,[Fusion_Color]
		,[Fusion_Pantone]
		,[Fusion_Pattern]
		,[Fusion_Pattern_Notes]
		,[ShippingTypeID]
		,[ShippingCarrierID]
		,[Instability_BilatRightLeg]
		,[X2K_Counterforce_BilatRightLeg]
		,[X2K_Counterforce_Degrees_BilatRightLeg]
		,[ThighCircumference_BilatRightLeg]
		,[CalfCircumference_BilatRightLeg]
		,[KneeOffset_BilatRightLeg]
		,[KneeWidth_BilatRightLeg]
		,[Extension_BilatRightLeg]
		,[Flexion_BilatRightLeg]
		,'Deleted'
	FROM deleted

END
GO

CREATE TRIGGER [dbo].[trg_ShoppingCartCustomBrace_Updated] 
ON [dbo].[ShoppingCartCustomBrace] 
FOR UPDATE 
AS
BEGIN

	INSERT INTO [ShoppingCartCustomBrace_Audit]
	(
		[ShoppingCartID]
		,[BregVisionOrderID]
		,[DispenseID]
		,[Billing_CustomerNumber]
		,[Billing_Phone]
		,[Billing_Attention]
		,[Billing_Address1]
		,[Billing_Address2]
		,[Billing_City]
		,[Billing_State]
		,[Billing_Zip]
		,[Shipping_Attention]
		,[Shipping_Address1]
		,[Shipping_Address2]
		,[Shipping_City]
		,[Shipping_State]
		,[Shipping_Zip]
		,[PatientName]
		,[PhysicianID]
		,[Patient_Age]
		,[Patient_Weight]
		,[Patient_Height]
		,[Patient_Sex]
		,[BraceFor]
		,[Instability]
		,[BilatCastReform]
		,[ThighCircumference]
		,[CalfCircumference]
		,[KneeOffset]
		,[KneeWidth]
		,[Extension]
		,[Flexion]
		,[MeasurementsTakenBy]
		,[X2K_Counterforce]
		,[X2K_Counterforce_Degrees]
		,[X2K_FramePadColor1]
		,[X2K_FramePadColor2]
		,[Fusion_Enhancement]
		,[Fusion_Notes]
		,[Fusion_Color]
		,[Fusion_Pantone]
		,[Fusion_Pattern]
		,[Fusion_Pattern_Notes]
		,[ShippingTypeID]
		,[ShippingCarrierID]
		,[Instability_BilatRightLeg]
		,[X2K_Counterforce_BilatRightLeg]
		,[X2K_Counterforce_Degrees_BilatRightLeg]
		,[ThighCircumference_BilatRightLeg]
		,[CalfCircumference_BilatRightLeg]
		,[KneeOffset_BilatRightLeg]
		,[KneeWidth_BilatRightLeg]
		,[Extension_BilatRightLeg]
		,[Flexion_BilatRightLeg]
		,[AuditAction]
	)
	SELECT
		[ShoppingCartID]
		,[BregVisionOrderID]
		,[DispenseID]
		,[Billing_CustomerNumber]
		,[Billing_Phone]
		,[Billing_Attention]
		,[Billing_Address1]
		,[Billing_Address2]
		,[Billing_City]
		,[Billing_State]
		,[Billing_Zip]
		,[Shipping_Attention]
		,[Shipping_Address1]
		,[Shipping_Address2]
		,[Shipping_City]
		,[Shipping_State]
		,[Shipping_Zip]
		,[PatientName]
		,[PhysicianID]
		,[Patient_Age]
		,[Patient_Weight]
		,[Patient_Height]
		,[Patient_Sex]
		,[BraceFor]
		,[Instability]
		,[BilatCastReform]
		,[ThighCircumference]
		,[CalfCircumference]
		,[KneeOffset]
		,[KneeWidth]
		,[Extension]
		,[Flexion]
		,[MeasurementsTakenBy]
		,[X2K_Counterforce]
		,[X2K_Counterforce_Degrees]
		,[X2K_FramePadColor1]
		,[X2K_FramePadColor2]
		,[Fusion_Enhancement]
		,[Fusion_Notes]
		,[Fusion_Color]
		,[Fusion_Pantone]
		,[Fusion_Pattern]
		,[Fusion_Pattern_Notes]
		,[ShippingTypeID]
		,[ShippingCarrierID]
		,[Instability_BilatRightLeg]
		,[X2K_Counterforce_BilatRightLeg]
		,[X2K_Counterforce_Degrees_BilatRightLeg]
		,[ThighCircumference_BilatRightLeg]
		,[CalfCircumference_BilatRightLeg]
		,[KneeOffset_BilatRightLeg]
		,[KneeWidth_BilatRightLeg]
		,[Extension_BilatRightLeg]
		,[Flexion_BilatRightLeg]
		,'Updated (OLD)'
	FROM deleted

	INSERT INTO [ShoppingCartCustomBrace_Audit]
	(
		[ShoppingCartID]
		,[BregVisionOrderID]
		,[DispenseID]
		,[Billing_CustomerNumber]
		,[Billing_Phone]
		,[Billing_Attention]
		,[Billing_Address1]
		,[Billing_Address2]
		,[Billing_City]
		,[Billing_State]
		,[Billing_Zip]
		,[Shipping_Attention]
		,[Shipping_Address1]
		,[Shipping_Address2]
		,[Shipping_City]
		,[Shipping_State]
		,[Shipping_Zip]
		,[PatientName]
		,[PhysicianID]
		,[Patient_Age]
		,[Patient_Weight]
		,[Patient_Height]
		,[Patient_Sex]
		,[BraceFor]
		,[Instability]
		,[BilatCastReform]
		,[ThighCircumference]
		,[CalfCircumference]
		,[KneeOffset]
		,[KneeWidth]
		,[Extension]
		,[Flexion]
		,[MeasurementsTakenBy]
		,[X2K_Counterforce]
		,[X2K_Counterforce_Degrees]
		,[X2K_FramePadColor1]
		,[X2K_FramePadColor2]
		,[Fusion_Enhancement]
		,[Fusion_Notes]
		,[Fusion_Color]
		,[Fusion_Pantone]
		,[Fusion_Pattern]
		,[Fusion_Pattern_Notes]
		,[ShippingTypeID]
		,[ShippingCarrierID]
		,[Instability_BilatRightLeg]
		,[X2K_Counterforce_BilatRightLeg]
		,[X2K_Counterforce_Degrees_BilatRightLeg]
		,[ThighCircumference_BilatRightLeg]
		,[CalfCircumference_BilatRightLeg]
		,[KneeOffset_BilatRightLeg]
		,[KneeWidth_BilatRightLeg]
		,[Extension_BilatRightLeg]
		,[Flexion_BilatRightLeg]
		,[AuditAction]
	)
	SELECT
		[ShoppingCartID]
		,[BregVisionOrderID]
		,[DispenseID]
		,[Billing_CustomerNumber]
		,[Billing_Phone]
		,[Billing_Attention]
		,[Billing_Address1]
		,[Billing_Address2]
		,[Billing_City]
		,[Billing_State]
		,[Billing_Zip]
		,[Shipping_Attention]
		,[Shipping_Address1]
		,[Shipping_Address2]
		,[Shipping_City]
		,[Shipping_State]
		,[Shipping_Zip]
		,[PatientName]
		,[PhysicianID]
		,[Patient_Age]
		,[Patient_Weight]
		,[Patient_Height]
		,[Patient_Sex]
		,[BraceFor]
		,[Instability]
		,[BilatCastReform]
		,[ThighCircumference]
		,[CalfCircumference]
		,[KneeOffset]
		,[KneeWidth]
		,[Extension]
		,[Flexion]
		,[MeasurementsTakenBy]
		,[X2K_Counterforce]
		,[X2K_Counterforce_Degrees]
		,[X2K_FramePadColor1]
		,[X2K_FramePadColor2]
		,[Fusion_Enhancement]
		,[Fusion_Notes]
		,[Fusion_Color]
		,[Fusion_Pantone]
		,[Fusion_Pattern]
		,[Fusion_Pattern_Notes]
		,[ShippingTypeID]
		,[ShippingCarrierID]
		,[Instability_BilatRightLeg]
		,[X2K_Counterforce_BilatRightLeg]
		,[X2K_Counterforce_Degrees_BilatRightLeg]
		,[ThighCircumference_BilatRightLeg]
		,[CalfCircumference_BilatRightLeg]
		,[KneeOffset_BilatRightLeg]
		,[KneeWidth_BilatRightLeg]
		,[Extension_BilatRightLeg]
		,[Flexion_BilatRightLeg]
		,'Updated (NEW)'
	FROM inserted

END
GO

CREATE TRIGGER [dbo].[trg_ShoppingCartCustomBrace_Inserted] 
ON [dbo].[ShoppingCartCustomBrace] 
FOR INSERT 
AS
BEGIN

	INSERT INTO [ShoppingCartCustomBrace_Audit]
	(
		[ShoppingCartID]
		,[BregVisionOrderID]
		,[DispenseID]
		,[Billing_CustomerNumber]
		,[Billing_Phone]
		,[Billing_Attention]
		,[Billing_Address1]
		,[Billing_Address2]
		,[Billing_City]
		,[Billing_State]
		,[Billing_Zip]
		,[Shipping_Attention]
		,[Shipping_Address1]
		,[Shipping_Address2]
		,[Shipping_City]
		,[Shipping_State]
		,[Shipping_Zip]
		,[PatientName]
		,[PhysicianID]
		,[Patient_Age]
		,[Patient_Weight]
		,[Patient_Height]
		,[Patient_Sex]
		,[BraceFor]
		,[Instability]
		,[BilatCastReform]
		,[ThighCircumference]
		,[CalfCircumference]
		,[KneeOffset]
		,[KneeWidth]
		,[Extension]
		,[Flexion]
		,[MeasurementsTakenBy]
		,[X2K_Counterforce]
		,[X2K_Counterforce_Degrees]
		,[X2K_FramePadColor1]
		,[X2K_FramePadColor2]
		,[Fusion_Enhancement]
		,[Fusion_Notes]
		,[Fusion_Color]
		,[Fusion_Pantone]
		,[Fusion_Pattern]
		,[Fusion_Pattern_Notes]
		,[ShippingTypeID]
		,[ShippingCarrierID]
		,[Instability_BilatRightLeg]
		,[X2K_Counterforce_BilatRightLeg]
		,[X2K_Counterforce_Degrees_BilatRightLeg]
		,[ThighCircumference_BilatRightLeg]
		,[CalfCircumference_BilatRightLeg]
		,[KneeOffset_BilatRightLeg]
		,[KneeWidth_BilatRightLeg]
		,[Extension_BilatRightLeg]
		,[Flexion_BilatRightLeg]
		,[AuditAction]
	)
	SELECT
		[ShoppingCartID]
		,[BregVisionOrderID]
		,[DispenseID]
		,[Billing_CustomerNumber]
		,[Billing_Phone]
		,[Billing_Attention]
		,[Billing_Address1]
		,[Billing_Address2]
		,[Billing_City]
		,[Billing_State]
		,[Billing_Zip]
		,[Shipping_Attention]
		,[Shipping_Address1]
		,[Shipping_Address2]
		,[Shipping_City]
		,[Shipping_State]
		,[Shipping_Zip]
		,[PatientName]
		,[PhysicianID]
		,[Patient_Age]
		,[Patient_Weight]
		,[Patient_Height]
		,[Patient_Sex]
		,[BraceFor]
		,[Instability]
		,[BilatCastReform]
		,[ThighCircumference]
		,[CalfCircumference]
		,[KneeOffset]
		,[KneeWidth]
		,[Extension]
		,[Flexion]
		,[MeasurementsTakenBy]
		,[X2K_Counterforce]
		,[X2K_Counterforce_Degrees]
		,[X2K_FramePadColor1]
		,[X2K_FramePadColor2]
		,[Fusion_Enhancement]
		,[Fusion_Notes]
		,[Fusion_Color]
		,[Fusion_Pantone]
		,[Fusion_Pattern]
		,[Fusion_Pattern_Notes]
		,[ShippingTypeID]
		,[ShippingCarrierID]
		,[Instability_BilatRightLeg]
		,[X2K_Counterforce_BilatRightLeg]
		,[X2K_Counterforce_Degrees_BilatRightLeg]
		,[ThighCircumference_BilatRightLeg]
		,[CalfCircumference_BilatRightLeg]
		,[KneeOffset_BilatRightLeg]
		,[KneeWidth_BilatRightLeg]
		,[Extension_BilatRightLeg]
		,[Flexion_BilatRightLeg]
		,'Inserted'
	FROM inserted

END
GO

SET ANSI_PADDING OFF
GO

