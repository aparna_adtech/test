USE [BregVision]
GO

/****** Object:  Table [dbo].[User_PracticeLocation_Permissions]    Script Date: 07/03/2009 16:55:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[User_PracticeLocation_Permissions](
	[UserID] [int] NOT NULL,
	[PracticeLocationID] [int] NOT NULL,
	[AllowAccess] [bit] NOT NULL,
	[CreatedUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedUserID] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_User_PracticeLocation_Permissions] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[PracticeLocationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[User_PracticeLocation_Permissions] ADD  CONSTRAINT [DF_User_PracticeLocation_Permissions_AllowAccess]  DEFAULT ((1)) FOR [AllowAccess]
GO

ALTER TABLE [dbo].[User_PracticeLocation_Permissions] ADD  CONSTRAINT [DF_User_PracticeLocation_Permissions_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO


