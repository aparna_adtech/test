USE [BregVision]

Alter Table [MasterCatalogProduct] Add IsDiscontinued bit not null default 0
GO

Alter Table [ThirdPartyProduct] Add IsDiscontinued bit not null default 0
GO