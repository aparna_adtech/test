﻿/*
Deployment script for BregVision

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "BregVision"
:setvar DefaultFilePrefix "BregVision"
:setvar DefaultDataPath "T:\"
:setvar DefaultLogPath "S:\"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Altering [dbo].[usp_GetShoppingCart]...';


GO


-- =============================================
-- Author:		<Greg Ross>
-- Create date: <07/31/07>
-- Description:	<Used to get all data for the shopping cart>
--  
--  Modification:  20071011 JB  Added Third Party Supplier Information.
--    check this for Cost
-- exec usp_GetShoppingCart 3
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetShoppingCart]   --18
	@PracticeLocationID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		  SUB.Supplier
		, SUB.Category
		, SUB.Product
		, SUB.Code
		, SUB.Packaging
		, SUB.Side
		, SUB.Size
		, SUB.Gender
		, cast(SUB.IsCustomBrace as bit) as IsCustomBrace
		, cast(SUB.IsCustomBraceAccessory as bit) as IsCustomBraceAccessory
		, SUB.Quantity
		, SUB.Cost           -- check this
		, CAST( (SUB.Quantity * SUB.Cost) AS DECIMAL(9, 2) )  AS LineTotal
		--, (SUB.Quantity * SUB.Cost) AS TotalCost
		, SUB.PracticeCatalogProductID
		, SUB.ShoppingCartItemID
		, SUB.IsThirdPartyProduct
		, SUB.Sequence
		, SUB.ParLevel
		, SUB.QuantityOnHand
		, SUB.ShoppingCartID
		, SUB.MasterCatalogProductID
		, SUB.IsConsignment
		, SUB.IsLogoPart
		, Cast(SUB.McpIsLogoAblePart as bit) as McpIsLogoAblePart

	FROM
			(
				SELECT  MCS.[SupplierName] AS Supplier,
						MCC.[Name] AS Category,
						MCP.[Name] AS Product,
						MCP.[Code] AS Code,
						MCP.[Packaging] AS Packaging,
						MCP.[LeftRightSide] AS Side,
						MCP.[Size] AS Size,
						MCP.[Gender] AS Gender,
						MCP.[IsCustomBrace] AS IsCustomBrace,
						MCP.IsCustomBraceAccessory,
						SCI.[Quantity] AS Quantity,
						SCI.[ActualWholesaleCost] AS Cost,
						PCP.[PracticeCatalogproductid] AS PracticeCatalogProductID,
						SCI.[ShoppingCartItemID] AS ShoppingCartItemID,
						PCP.IsThirdPartyProduct,
						ISNULL(PCSB.Sequence, 0) AS Sequence,
						ISNULL(PRI.ParLevel, 0) AS ParLevel,
						ISNULL(PRI.QuantityOnHandPerSystem, 0) AS QuantityOnHand,
						SC.ShoppingCartID as ShoppingCartID, 
						MCP.MasterCatalogProductID as MasterCatalogProductID, 
						SC.IsConsignment,
						SCI.IsLogoPart,
						(PRI.IsLogoAblePart & PCP.IsLogoAblePart & MCP.IsLogoAblePart) as McpIsLogoAblePart
						
				FROM    
						[ShoppingCart] SC
				        
						INNER JOIN shoppingcartitem SCI 
							ON SC.[ShoppingCartID] = SCI.[ShoppingCartID]
						
						INNER JOIN [PracticeCatalogProduct] PCP 
							ON SCI.[PracticeCatalogProductID] = PCP.[PracticeCatalogProductID]
						
						INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
							ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID							

						INNER JOIN [MasterCatalogProduct] MCP 
							ON PCP.[MasterCatalogProductID] = MCP.[MasterCatalogProductID]

						INNER JOIN [MasterCatalogSubCategory] MCSC 
							ON MCP.[MasterCatalogSubCategoryID] = MCSC.[MasterCatalogSubCategoryID]
						
						INNER JOIN [MasterCatalogCategory] MCC 
							ON MCSC.[MasterCatalogCategoryID] = MCC.[MasterCatalogCategoryID]
						
						INNER JOIN [MasterCatalogSupplier] MCS 
							ON MCC.[MasterCatalogSupplierID] = MCS.[MasterCatalogSupplierID]
						Left Outer JOIN [ProductInventory] PRI 
							ON PRI.PracticeCatalogProductID = SCI.PracticeCatalogProductID AND PRI.PracticeLocationID = @practicelocationID

				WHERE   
						SC.[ShoppingCartStatusID] = 0
						AND SCI.[ShoppingCartItemStatusID] = 0
						AND SC.[IsActive] = 1
						AND SCI.[IsActive] = 1
						AND pcp.[IsActive] = 1
						AND mcsc.[IsActive] = 1
						AND mcc.[IsActive] = 1
						AND mcs.[IsActive] = 1
						AND PCSB.IsActive = 1
						AND SC.[PracticeLocationID] = @practicelocationID
						AND PCSB.IsThirdPartySupplier  = 0
						AND PRI.IsActive = 1

			UNION
			
				SELECT  
						PCSB.[SupplierName]AS Supplier,
						'NA' AS Category,
						TPP.[Name] AS Product,
						TPP.[Code]AS Code,
						TPP.[Packaging] AS Packaging,
						TPP.[LeftRightSide] AS Side,
						TPP.[Size] AS Size,
						TPP.[Gender] AS Gender,
						IsCustomBrace = 0,
						IsCustomBraceAccessory = 0,
						SCI.[Quantity] AS Quantity,
						SCI.[ActualWholesaleCost] AS Cost,
						PCP.[PracticeCatalogproductid] AS PracticeCatalogProductID,
						SCI.[ShoppingCartItemID] AS ShoppingCartItemID,
						PCP.IsThirdPartyProduct,
						ISNULL(PCSB.Sequence, 0) AS Sequence,
						ISNULL(PRI.ParLevel, 0) AS ParLevel,
						ISNULL(PRI.QuantityOnHandPerSystem, 0) AS QuantityOnHand,
						SC.ShoppingCartID as ShoppingCartID, 
						0 as MasterCatalogProductID,
						SC.IsConsignment,
						0 as IsLogoPart,
						0 AS McpIsLogoAblePart
				FROM    
						[ShoppingCart] SC
				        
						INNER JOIN shoppingcartitem SCI 
							ON SC.[ShoppingCartID] = SCI.[ShoppingCartID]
							
						INNER JOIN [PracticeCatalogProduct] PCP 
							ON SCI.[PracticeCatalogProductID] = PCP.[PracticeCatalogProductID]
							
						INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
							ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID							
							
						INNER JOIN [ThirdPartyProduct] TPP 
							ON PCP.[ThirdPartyProductID] = TPP.[ThirdPartyProductID]
						Left Outer JOIN [ProductInventory] PRI 
							ON PRI.PracticeCatalogProductID = SCI.PracticeCatalogProductID AND PRI.PracticeLocationID = @practicelocationID
				WHERE   
						SC.[ShoppingCartStatusID] = 0
						AND SCI.[ShoppingCartItemStatusID] = 0
						AND SC.[IsActive] = 1
						AND SCI.[IsActive] = 1
						
						AND pcp.IsActive = 1
						AND PCSB.IsActive = 1
						AND TPP.IsActive = 1
						AND PCSB.IsThirdPartySupplier  = 1
						
						AND SC.[PracticeLocationID] = @practicelocationID
						AND PRI.IsActive = 1
						
				) AS SUB
				
			ORDER BY 
				SUB.IsThirdPartyProduct
				, SUB.Sequence
				, SUB.Supplier


END
GO
PRINT N'Altering [dbo].[usp_Inventory_Update_Insert]...';


GO



-- =============================================
--EXEC usp_Inventory_Update_Insert 3, 8, 1, 1, 0
-- Exec usp_Inventory_Update_Insert @PracticeLocationID=3, @PracticeCatalogProductID=33553, @UserID=1, @Quantity=1, @Bilat=1, @ShoppingCartIDReturn=0
-- Author:		Mike Sneen
-- Create date: 04/17/09
-- NOTE: Rewritten by Mike Sneen to clean up logic, add structured try/catch, and raise an error if user tries to add a custom brace and other items to the same cart
-- Description:	Used to update and insert records from the Inventory UI
-- Modification: 02/06/2012 Mike Sneen
-- Created 'isCustomBraceAccessory' in MCP 
-- used list of items previously in this proc to set to true.  
-- Removed physical list of items and query where 'isCustomBraceAccessory' is 1
--  Modification:  
--				2007.11.14 JB
--					On update to shopping cart item, update the quantity rather than replace.

-- =============================================
ALTER PROCEDURE [dbo].[usp_Inventory_Update_Insert]
	-- Add the parameters for the stored procedure here
    @PracticeLocationID INT,
    @PracticeCatalogProductID INT,
    @UserID INT,
    @Quantity int,
    @Bilat int = 0,
	@ShoppingCartIDReturn int output
	
AS
BEGIN TRY
		SET NOCOUNT ON 
		
		declare @Msg varchar(4000)
        DECLARE @TransactionCountOnEntry INT 
        DECLARE @CartExistsRecordCount INT
        Declare @CurrentItemAlreadyInCartCount int
        DECLARE @ShoppingCartID INT
        DECLARE @ShoppingCartItemID INT
        

        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

        SELECT  @TransactionCountOnEntry = @@TRANCOUNT
        BEGIN TRANSACTION  
        --Body
			IF ((@Quantity = 0))
			BEGIN
				Set @Msg = 'Application Error - quantity must be at least 1'
				RAISERROR(@Msg, 18, 1)	
			END
        
        
        
			SELECT  @CartExistsRecordCount = COUNT(1)
                FROM    dbo.[ShoppingCart]
                WHERE   PracticeLocationID = @PracticeLocationID
                        AND IsActive = 1
                        
                        
			IF ( @CartExistsRecordCount = 0 ) 
            BEGIN   --Insert New record into ShoppingCart
                INSERT  INTO dbo.ShoppingCart
                        (
                          [PracticeLocationID],
                          [ShoppingCartStatusID],
                          [ShippingTypeID],
                          [DeniedComments],
                          [CreatedUserID],
                          IsActive
					
                        )
                VALUES  (
                          @PracticeLocationID,
                          0,
                          4,
                          '',
                          @UserID,
                          1
					
                        )

                Select  @ShoppingCartID = SCOPE_IDENTITY()             
            END           
			ELSE 
            BEGIN --Get the existing shopping cart id
                SELECT  @ShoppingCartID = ShoppingCartID
                FROM    dbo.ShoppingCart
                WHERE   PracticeLocationID = @PracticeLocationID
                        AND IsActive = 1                      
            END   --End Else if @CartExistsRecordCount = 0             
				--the record count wasn't zero  if this item isCustomBrace then it can't be added.  If the item in the cart        isCustomBrace, then this item can't be added
				
				
				--Check if the item being added is a custom brace
				DECLARE @IsCustomBraceCount INT 
				Select 
					@IsCustomBraceCount = COUNT(1)
				from 
					PracticeCatalogProduct PCP
				Left Outer Join MasterCatalogProduct MCP
					on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
				Where 
					ISNULL(MCP.IsCustomBrace, 0) = 1
					AND PCP.PracticeCatalogProductID= @PracticeCatalogProductID
				--END Check if the item being added is a custom brace
				
				
				
				--Check to see if there is a CustomBrace item in the cart
				Declare @CustomBraceInCartCount int
				SELECT  @CustomBraceInCartCount = COUNT(1)
                    FROM    dbo.[ShoppingCartItem] SCI
					Inner Join PracticeCatalogProduct  PCP
						ON SCI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
					Left Outer Join MasterCatalogProduct MCP
						on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
                    WHERE   SCI.[ShoppingCartID] = @ShoppingCartID
                            --AND SCI.[PracticeCatalogProductID] = @PracticeCatalogProductID
                            AND ISNULL(MCP.IsCustomBrace, 0) = 1
                            AND SCI.IsActive = 1
				--END Check to see if there is a CustomBrace item in the cart
				
				--Check to see if the item being added is a custom Brace accessory
				Declare @IsCustomBraceAccessory int
				SELECT @IsCustomBraceAccessory = COUNT(1)
				FROM PracticeCatalogProduct PCPsub
					join MasterCatalogProduct MCPsub on PCPsub.MasterCatalogProductID = MCPsub.MasterCatalogProductID
					WHERE
					(
						MCPsub.isCustomBraceAccessory = 1
						AND PCPsub.PracticeCatalogProductID = @PracticeCatalogProductID
					) 
				
				
				--Check to see if there are any items in the cart
				 Declare @ItemsInCartCount int  
				 SELECT  @ItemsInCartCount = COUNT(1) 
					FROM    dbo.[ShoppingCartItem] SCI
					WHERE   SCI.[ShoppingCartID] = @ShoppingCartID
							AND SCI.IsActive = 1
							AND SCI.PracticeCatalogProductID NOT IN
							(
								Select PracticeCatalogProductID from PracticeCatalogProduct PCPsub1
								join MasterCatalogProduct MCPsub1 on PCPsub1.MasterCatalogProductID = MCPsub1.MasterCatalogProductID
								WHERE
								(
									MCPsub1.isCustomBraceAccessory = 1
								) 
							)
				

		
				--Check to see if there are any items in the cart
				--If there is already a custom Brace in the Cart AND This is not a custom brace accessory
				--OR
				--If this is a custom brace and there are already items in the cart(that are not custom brace accessories)
				--then we can't add this item
				IF ((@CustomBraceInCartCount > 0 AND @IsCustomBraceAccessory < 1) OR (@IsCustomBraceCount > 0 and @ItemsInCartCount > 0))--then we can't add
				BEGIN
					--print '@CustomBraceInCartCount='  + cast(@CustomBraceInCartCount as varchar)
					--print '@IsCustomBraceAccessory='  + cast(@IsCustomBraceAccessory as varchar)
					--print '@IsCustomBraceCount='  + cast(@IsCustomBraceCount as varchar)
					--print '@ItemsInCartCount='  + cast(@ItemsInCartCount as varchar)
					IF(@Bilat = 0)
					BEGIN
						Set @Msg = 'Application Error - custom brace must be the only item in an order'
						RAISERROR(@Msg, 17, 1)	
					END
				END
				--if @CustomBraceInCartCount = 0 and @IsCustomBraceCount > 0 and @ItemsInCartCount > 0 --then we can't add
				--if @CustomBraceInCartCount > 0 and @IsCustomBraceCount = 0 --then ok to add add
				--if @CustomBraceInCartCount = 0 and @IsCustomBraceCount = 0 --then ok to add add
				
				IF(@IsCustomBraceCount > 0 and @Quantity > 1)
				BEGIN
					Set @Msg = 'Application Error - custom brace must have a quantity of 1'
					RAISERROR(@Msg, 18, 1)	
				END				
				
				--MWS Check to see if this item is in the cart
				--PRINT @ReOrderQuanity 	
                SELECT  @CurrentItemAlreadyInCartCount = COUNT(1)
                FROM    dbo.[ShoppingCartItem]
                WHERE   [ShoppingCartID] = @ShoppingCartID
                        AND [PracticeCatalogProductID] = @PracticeCatalogProductID
                        AND IsActive = 1
                
                
                IF ( @CurrentItemAlreadyInCartCount = 0 ) --MWS if this item is NOT in the cart
                BEGIN   --Insert New record into ShoppingCart
                    DECLARE @wlc SMALLMONEY
					SET @wlc = (select WholesaleCost from PracticeCatalogProduct 
						where PracticeCatalogProduct.PracticeCatalogProductID = @PracticeCatalogProductID)

					DECLARE @defaultLogoFlag BIT
					SET @defaultLogoFlag=0
					SELECT @defaultLogoFlag=(ISNULL(a.IsLogoAblePart, 0) & ISNULL(b.IsLogoAblePart, 0) & ISNULL(c.IsLogoAblePart, 0))
					FROM
						ProductInventory a
						LEFT JOIN PracticeCatalogProduct b ON a.PracticeCatalogProductID=b.PracticeCatalogProductID
						LEFT JOIN MasterCatalogProduct c ON b.MasterCatalogProductID=c.MasterCatalogProductID
					WHERE
						b.PracticeCatalogProductID=@PracticeCatalogProductID
						AND a.PracticeLocationID=@PracticeLocationID
						AND a.IsActive=1
						AND b.IsActive=1
						AND c.IsActive=1

					INSERT  INTO dbo.ShoppingCartItem
                        (
                          [ShoppingCartID],
                          [PracticeCatalogProductID],
                          [ShoppingCartItemStatusID],
                          [Quantity],
                          [ActualWholesaleCost],
                          [CreatedUserID],
                          IsActive,
						  IsLogoPart
                        )
                    VALUES  (
                          @ShoppingCartID,
                          @PracticeCatalogProductID,
                          0,
                          @Quantity,
						  @wlc,	
                          @UserID,
                          1,
						  @defaultLogoFlag
                        )

                    Select  @ShoppingCartItemID = SCOPE_IDENTITY()

                END
                ELSE 
                BEGIN --If the item being added is in the cart then Update Existing record								
					
					IF(@Quantity > 0)
					BEGIN
						UPDATE  [ShoppingCartItem]
						SET     Quantity = Quantity + @Quantity,  
								[ModifiedUserID] = @UserID,
								[ModifiedDate] = GETDATE()
						WHERE   ShoppingCartID = @ShoppingCartID
								AND [PracticeCatalogProductID] = @PracticeCatalogProductID
								AND (
										(@Bilat = 0)
									OR
										(	
											@Bilat = 1
											AND
											Quantity < 2
										)
									)
					END --IF(@Quantity > 0)
					ELSE
					BEGIN --If the quantity is 0, delete the item from the cart.
						DELETE FROM [ShoppingCartItem]
						WHERE ShoppingCartID = @ShoppingCartID
							  AND [PracticeCatalogProductID] = @PracticeCatalogProductID
					END
                END
	
                  
                        
                        
        --END Body 	
		IF @@TRANCOUNT > @TransactionCountOnEntry 
			COMMIT TRANSACTION
			set @ShoppingCartIDReturn = @ShoppingCartID				
   END TRY	
   BEGIN CATCH
		-- Whoops, there was an error
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION
			set @ShoppingCartIDReturn = 0
				-- Raise an error with the details of the exception
		DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
		SELECT @ErrMsg = ERROR_MESSAGE(),
			@ErrSeverity = ERROR_SEVERITY()	
		
	    EXECUTE [dbo].[uspLogError]
	    EXECUTE [dbo].[uspPrintError]
		RAISERROR(@ErrMsg, @ErrSeverity, 1)
   END CATCH
GO
PRINT N'Update complete.';


GO
