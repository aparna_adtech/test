USE [BregVision]

Alter Table [MasterCatalogProduct] Add isCustomBraceAccessory bit not null default 0
GO

Update [MasterCatalogProduct]
Set isCustomBraceAccessory = 1
Where MasterCatalogProductID in(

SELECT MCP.MasterCatalogProductID
				FROM MasterCatalogProduct MCP 
					WHERE
					(
						
							(MCP.[Code] = '22000'
						OR	MCP.[Code] = '22001'
						OR	MCP.[Code] like '0985%'
						OR	MCP.[Code] like '0735%'
						OR	MCP.[Code] like '1008%'
						OR	MCP.[Code] = '70057'
						OR	MCP.[Code] = '70058'
						OR	MCP.[Code] like '1099%'
						OR	MCP.[Code] like '1931%')
						
					) 

)
GO