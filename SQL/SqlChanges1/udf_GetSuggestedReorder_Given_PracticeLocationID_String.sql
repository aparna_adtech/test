USE [BregVision]
GO
/****** Object:  UserDefinedFunction [dbo].[udf_GetSuggestedReorder_Given_PracticeLocationID]    Script Date: 12/15/2009 20:50:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--  Test Script
/*
	DECLARE @PracticeLocationIDString varchar(2000)
	SET @PracticeLocationIDString = '3,4,5'
	SELECT * FROM dbo.[udf_GetSuggestedReorder_Given_PracticeLocationID_String]( @PracticeLocationIDString) ORDER BY PracticeCatalogProductID
 ------------------------------------------------------------
   FUNCTION:    [dbo].[udf_GetSuggestedReorder_Given_PracticeLocationID]
   
   Description:  Gets Qusntity on Hand, Quntity on Order, Inventory Counts
					, and Suggested Reorder Level 
					for all location inventory products given a PracticeLocationID.
		
	Dependencies:  	udf_GetSuggestedReorder_Given_PracticeLocationID	 
   
   AUTHOR:       John Bongiorni 10/18/2007 5:45:17 PM
   
   Modifications:  
				2008.02.05 JB  Add column PI.IsNotFlaggedForReorder 
   ------------------------------------------------------------ */ 

Create FUNCTION [dbo].[udf_GetSuggestedReorder_Given_PracticeLocationID_String]
(
	  @PracticeLocationIDString		varchar(2000)	
)  
RETURNS @ReturnTable TABLE 
	( 
		  PracticeCatalogProductID INT

		, QuantityOnHand INT DEFAULT 0
		, QuantityOnOrder INT DEFAULT 0    --  RemainingToBeReceivedfromOrder

		, ParLevel INT DEFAULT 0					
		, ReorderLevel INT DEFAULT 0			
		, CriticalLevel INT DEFAULT 0
		
		, IsNotFlaggedForReorder BIT DEFAULT 0

		, SuggestedReorderLevel INT DEFAULT 0


		--  The following four columns work and may be uncommented to test or debug.
		--		, StatusOrdered_Quantity_Ordered INT DEFAULT 0
		--		, StatusPartiallyCheckedIn_Quantity_Ordered INT DEFAULT 0
		--		, StatusPartiallyCheckedIn_Quantity_CheckedintoInventory INT DEFAULT 0
		--		, StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory INT DEFAULT 0		
											
		--	The following column works may be uncommented to test or debug.
		--	     , QuantityOnHand_plus_ToBeReceieved INT DEFAULT 0   -- used for comparison with reorderLevel for suggestedReorderLevel
				
	) 
AS
BEGIN	


	--  Table Variable for work.
	DECLARE @Table TABLE
		(
			  PracticeCatalogProductID INT
			, StatusOrdered_Quantity_Ordered INT DEFAULT 0
			, StatusPartiallyCheckedIn_Quantity_Ordered INT DEFAULT 0
			, StatusPartiallyCheckedIn_Quantity_CheckedintoInventory INT DEFAULT 0
			, StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory INT DEFAULT 0
			, RemainingToBeReceivedfromOrder INT DEFAULT 0
			
			
			, QuantityOnHand  INT DEFAULT 0
			, PARLevel  INT DEFAULT 0
			, ReorderLevel INT DEFAULT 0
            , CriticalLevel  INT DEFAULT 0

			, IsNotFlaggedForReorder BIT DEFAULT 0
            
            , QuantityOnHand_plus_ToBeReceieved INT DEFAULT 0
            
            , SuggestedReOrderLevel INT DEFAULT 0
		)


	-- 1. Get all PracticeCatalogProductID in inventory for the Practice Location
	INSERT INTO @Table
		( 
			  PracticeCatalogProductID
			, IsNotFlaggedForReorder 
			
		)
	SELECT  
		  PI.PracticeCatalogProductID
        , PI.IsNotFlaggedForReorder         
    FROM ProductInventory AS PI WITH(NOLOCK)
    INNER JOIN [PracticeCatalogProduct] AS PCP WITH(NOLOCK)
         ON PCP.PracticeCatalogProductID = PI.PracticeCatalogProductID
	WHERE 
		PI.PracticeLocationID in
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))
			 
		AND PI.IsActive = 1
		AND PCP.IsActive = 1



	--  2. Get the SupplierOrderLineItem.QuantityOrder with status of Ordered
	
		UPDATE  @Table
		SET     T.StatusOrdered_Quantity_Ordered = StatusOrdered.StatusOrdered_Quantity_Ordered
		FROM    @Table AS T
			
		INNER JOIN (
				SELECT 
					  SOLI.PracticeCatalogProductID
					, SUM( SOLI.QuantityOrdered ) AS StatusOrdered_Quantity_Ordered
		
				FROM dbo.ProductInventory AS PI WITH(NOLOCK)
			    
				INNER JOIN dbo.PracticeCatalogProduct AS PCP WITH(NOLOCK) 
					ON PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
			    
				INNER JOIN dbo.BregVisionOrder AS BVO WITH(NOLOCK)
					ON PI.PracticeLocationID = BVO.PracticeLocationID
			        
				INNER JOIN dbo.SupplierOrder AS SO WITH(NOLOCK)
					ON BVO.BregVisionOrderID = SO.BregVisionOrderID      
			        
				INNER JOIN dbo.SupplierOrderLineItem AS SOLI WITH(NOLOCK)
					ON SO.SupplierOrderID = SOLI.SupplierOrderID 
					  AND PCP.PracticeCatalogProductID = SOLI.PracticeCatalogProductID      
			   
				INNER JOIN dbo.SupplierOrderLineItemStatus AS SOLIStatus WITH(NOLOCK)
					ON SOLI.SupplierOrderLineItemStatusID = SOLIStatus.SupplierOrderLineItemStatusID   
					
			   WHERE 
					PI.PracticeLocationID IN 
						(SELECT ID 
							FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ',')) 
					AND PI.IsActive = 1    
					AND PCP.IsActive = 1
					AND BVO.IsActive = 1
					AND SO.IsActive = 1
					AND SOLI.IsActive = 1
					AND SOLIStatus.SupplierOrderLineItemStatusID = 1      -- SOLineItemStatus is 'Ordered''
			        
				GROUP BY
					SOLI.PracticeCatalogProductID
					) AS StatusOrdered
							ON T.PracticeCatalogProductID = StatusOrdered.PracticeCatalogProductID



			--  3. Get StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory
			
			UPDATE  @Table
			SET     
				  T.StatusPartiallyCheckedIn_Quantity_Ordered = StatusPartiallyCheckedIn.StatusPartiallyCheckedIn_Quantity_Ordered
				, T.StatusPartiallyCheckedIn_Quantity_CheckedintoInventory = StatusPartiallyCheckedIn.StatusPartiallyCheckedIn_Quantity_CheckedintoInventory
				, T.StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory = StatusPartiallyCheckedIn.StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory

			FROM    @Table 
				AS T
				INNER JOIN (
							SELECT 
						  SOLI.PracticeCatalogProductID
						, SUM( SOLI.QuantityOrdered )  AS StatusPartiallyCheckedIn_Quantity_Ordered          -- Checked-IN amount for Partially checked-in SOLineItems
						, SUM( PI_OCI.Quantity )  AS StatusPartiallyCheckedIn_Quantity_CheckedintoInventory  -- QuantityOrdered amount for Partially checked-in SOLineItems
						, ( SUM( SOLI.QuantityOrdered ) - SUM( PI_OCI.Quantity ) ) AS StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory  -- NOT Checked-IN amount for Partially checked-in SOLineItems
																				
					FROM
						dbo.ProductInventory AS PI WITH(NOLOCK)
					    
						INNER JOIN dbo.PracticeCatalogProduct AS PCP WITH(NOLOCK) 
							ON PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
					    
						INNER JOIN dbo.BregVisionOrder AS BVO WITH(NOLOCK)
							ON PI.PracticeLocationID = BVO.PracticeLocationID
					        
						INNER JOIN dbo.SupplierOrder AS SO WITH(NOLOCK)
							ON BVO.BregVisionOrderID = SO.BregVisionOrderID      
					        
						INNER JOIN dbo.SupplierOrderLineItem AS SOLI WITH(NOLOCK)
							ON SO.SupplierOrderID = SOLI.SupplierOrderID 
							  AND PCP.PracticeCatalogProductID = SOLI.PracticeCatalogProductID      
					   
						INNER JOIN dbo.SupplierOrderLineItemStatus AS SOLIStatus WITH(NOLOCK)
							ON SOLI.SupplierOrderLineItemStatusID = SOLIStatus.SupplierOrderLineItemStatusID   
					        
						INNER JOIN dbo.ProductInventory_OrderCheckIn AS PI_OCI
							ON   PI.ProductInventoryID = PI_OCI.ProductInventoryID
							AND  SOLI.SupplierOrderLineItemID = PI_OCI.SupplierOrderLineItemID
							
					   WHERE 
							PI.PracticeLocationID IN 
							(SELECT ID 
								FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))  
							AND PI.IsActive = 1   
							AND PCP.IsActive = 1
							AND BVO.IsActive = 1
							AND SO.IsActive = 1
							AND SOLI.IsActive = 1
							AND PI_OCI.IsActive = 1
							AND SOLIStatus.SupplierOrderLineItemStatusID = 3      -- SOLineItemStatus is Partially checked-in
					        
						GROUP BY
							SOLI.PracticeCatalogProductID
							
							) AS StatusPartiallyCheckedIn
									ON T.PracticeCatalogProductID = StatusPartiallyCheckedIn.PracticeCatalogProductID
							
							
			-- 4.  Update StatusOrderedorPartiallyCheckedIn_Quantity_RemainingToBeReceivedfromOrder			
				
			UPDATE  @Table
			SET RemainingToBeReceivedfromOrder 
					= 	  StatusOrdered_Quantity_Ordered		
						+ StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory
       

			-- 5. --  Update QuantityOnHand, Par Level, ReOrder Level, and Critical Levels
			
			UPDATE  @Table
			SET		
					  T.QuantityOnHand = PILevels.QuantityOnHand
					, T.PARLevel = PILevels.PARLevel
					, T.ReorderLevel = PILevels.ReorderLevel
                    , T.CriticalLevel = PILevels.CriticalLevel
                    
			FROM    @Table AS T
			INNER JOIN 
					( SELECT 
							  PI.PracticeCatalogProductID
                            , PI.QuantityOnHandPerSystem AS QuantityOnHand
                            , PI.ParLevel
                            , PI.ReorderLevel
                            , PI.CriticalLevel
                                                        
                     FROM  
                            ProductInventory AS PI
                            INNER JOIN PracticeCatalogProduct AS PCP
								ON PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
                     WHERE  
					        PI.PracticeLocationID IN 
							(SELECT ID 
								FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))                
                            AND  PI.IsActive = 1   
							AND PCP.IsActive = 1
                     
                   ) AS PILevels 
						ON T.PracticeCatalogProductID = PILevels.PracticeCatalogProductID



			--  6.  UPDATE  SuggestedReorderLevel
			
					UPDATE @Table
					SET     QuantityOnHand_plus_ToBeReceieved 
						=  QuantityonHand + RemainingToBeReceivedfromOrder
						
						
			
			
					UPDATE @Table
					SET     SuggestedReOrderLevel 
					= CASE 
							WHEN( (ParLevel - QuantityOnHand_plus_ToBeReceieved ) < 0 ) 
							THEN 0
							ELSE ParLevel - QuantityOnHand_plus_ToBeReceieved
					  END


            -- 7. Insert the contents of working table into the table variable to be returned.
            Insert INTO @ReturnTable 
				( 
				      PracticeCatalogProductID 
--					, StatusOrdered_Quantity_Ordered 
--					, StatusPartiallyCheckedIn_Quantity_Ordered 
--					, StatusPartiallyCheckedIn_Quantity_CheckedintoInventory 
--					, StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory 
					, QuantityOnOrder		--     RemainingToBeReceivedfromOrder
															
					, QuantityOnHand  
					, PARLevel
					, ReorderLevel 
					, CriticalLevel  

					, IsNotFlaggedForReorder 			
					
--					, QuantityOnHand_plus_ToBeReceieved 
					, SuggestedReOrderLevel
				)
            SELECT 
					  PracticeCatalogProductID 
--					, StatusOrdered_Quantity_Ordered 
--					, StatusPartiallyCheckedIn_Quantity_Ordered 
--					, StatusPartiallyCheckedIn_Quantity_CheckedintoInventory 
--					, StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory 
					, RemainingToBeReceivedfromOrder	AS QuantityOnOrder
					
					, QuantityOnHand  
					, PARLevel  
					, ReorderLevel
					, CriticalLevel 
		
					, IsNotFlaggedForReorder 			

--					, QuantityOnHand_plus_ToBeReceieved 

					, SuggestedReOrderLevel
					
			FROM @Table


            
--            --  Replace the array item in the array with an empty string.
--            SET  @Array = STUFF( @Array, 1, @SeparatorPosition, '' )
--            
--        END        
        
	RETURN	--This will return the table variable @ReturnTable
END            

