USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_SuperbillAddOn_InsertUpdate]    Script Date: 03/12/2009 17:51:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Greg Ross
-- Create date: 10/01/07
-- Description:	Transfers products from the practice catalog to the product inventory
-- Modifications:  
--      2008.01.31  John Bongiorni
--		Set PI.IsSetToActive to 0, PI.IsActive to 1 on insert.
--      2008.02.15  John Bongiorni 
--		Set PI.IsSetToActive to 1, PI.IsActive to 1 on insert.
-- =============================================
Create PROCEDURE [dbo].[usp_SuperbillAddOn_InsertUpdate]
	@SuperBillProductAddOnID int, --the id of the record to update
	@SuperBillProductID int, --the id of the parent superbill product
	@PracticeLocationID int,
	@PracticeCatalogProductID int,
	@UserID int,
	@SuperbillCategoryID int,
	@HCPCs varchar(8) = null,
	@Deposit smallmoney,
	@Cost smallmoney,
	@Charge smallmoney,
	@FlowsheetOnly bit,
	@ProductName varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @RecordCount int
declare @RecordCountInActive int

SELECT  @RecordCount = COUNT(1)
                FROM    dbo.SuperBillProduct
                WHERE   SuperBillProductID =@SuperBillProductAddOnID
                       
--print @recordcount

if @recordcount = 0 -- No record exists
begin
	--Insert new record
	insert into SuperBillProduct
		(PracticeLocationID,
		PracticeCatalogProductID,
		SuperBillCategoryID,
		HCPCs,
		Deposit,
		Cost,
		Charge,
		Name,
		FlowsheetOnly,
		CreateUserID,
		CreatedDate,
		isactive,
		IsAddOn,
		SuperBillProductID_FK)
		Values
		(@PracticeLocationID,
		@PracticeCatalogProductID,
		@SuperbillCategoryID,
		@HCPCs, 
		@Deposit,
		@Cost,
		@Charge,
		@ProductName,
		@FlowsheetOnly,
		@UserID,
		getdate(),
		1,
		1,
		@SuperBillProductID)
	
end
ELSE
	BEGIN
		
		UPDATE dbo.SuperBillProduct
		SET  
			PracticeLocationID=@PracticeLocationID,
		PracticeCatalogProductID=@PracticeCatalogProductID,
		SuperBillCategoryID=@SuperbillCategoryID,
		HCPCs=@HCPCs,
		Deposit=@Deposit,
		Cost=@Cost,
		Charge=@Charge,
		Name=@ProductName,
		FlowsheetOnly=@FlowsheetOnly,
		CreateUserID=@UserID,
		CreatedDate=getdate(),
		isactive=1,
		IsAddOn=1,
		SuperBillProductID_FK=@SuperBillProductID
	    WHERE   SuperBillProductID = @SuperBillProductAddOnID
			
	
	END
	
END
