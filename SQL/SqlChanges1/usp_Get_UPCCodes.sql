USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_Get_UPCCodes]    Script Date: 01/01/2012 18:29:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--exec [usp_Get_UPCCodes] 33,0
--exec [usp_Get_UPCCodes] 0, 121
Create PROCEDURE [dbo].[usp_Get_UPCCodes] 
		@MasterCatalogProductID INT,
		@ThirdPartyProductID INT
		
AS
BEGIN

Select code from UPCCode u
where
	(((@MasterCatalogProductID <> 0) AND (u.MasterCatalogProductID = @MasterCatalogProductID)) OR (@MasterCatalogProductID = 0)) AND
	(((@ThirdPartyProductID <> 0) AND (u.ThirdPartyProductID = @ThirdPartyProductID)) OR (@ThirdPartyProductID = 0))

END