USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_ThirdPartySupplier_Delete]    Script Date: 08/26/2010 00:43:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ------------------------------------------------------------

   PROCEDURE:    dbo.usp_ThirdPartySupplier_Delete
   
   Description:  Cascade Deactivates "Deletes" records from table dbo.ThirdPartySupplier to ProductInventory
   					This actual flags the IsActive column to false.
					
   AUTHOR:       Rewritten by Mike Sneen 8/26/2010--John Bongiorni 9/28/2007 6:39:00 PM
   
   Modifications:  
		  JB  2008.01.07  Set the PCSupplierBrand as Inactive after the thirdpartyproduct has been inactivated.

   ------------------------------------------------------------ */  ------------------ DELETE ----------------

ALTER PROCEDURE [dbo].[usp_ThirdPartySupplier_Delete]
(
	@ThirdPartySupplierID int
	, @ModifiedUserID INT
	
)
AS
BEGIN
	BEGIN TRY

		BEGIN TRANSACTION
			-------Shopping Cart--------------------------------------------------
			UPDATE ShoppingCartItem
				SET IsActive = 0,
				ModifiedUserID = @ModifiedUserID,
				ModifiedDate = GETDATE() 
			FROM dbo.ShoppingCartItem AS SCI
				Inner Join PracticeCatalogProduct PCP
					on SCI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
				Inner Join ThirdPartyProduct TPP
					on TPP.ThirdPartyProductID = PCP.ThirdPartyProductID
				Inner Join PracticeCatalogSupplierBrand PCSB
					on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
				Inner Join ThirdPartySupplier TPS
					on TPS.ThirdPartySupplierID = PCSB.ThirdPartySupplierID
				Where
					TPS.ThirdPartySupplierID = @ThirdPartySupplierID
			
			-------Product Inventory------------------------------------------------------------
			UPDATE ProductInventory
				SET IsActive = 0,
				QuantityOnHandPerSystem = 0,
				ParLevel = 0,
				ReorderLevel = 0,
				CriticalLevel = 0,
				ModifiedUserID = @ModifiedUserID,
				ModifiedDate = GETDATE() 
			from 
				ProductInventory PI
				Inner Join PracticeCatalogProduct PCP
					on pi.PracticeCatalogProductID = PCP.PracticeCatalogProductID
				Inner Join ThirdPartyProduct TPP
					on TPP.ThirdPartyProductID = PCP.ThirdPartyProductID
				Inner Join PracticeCatalogSupplierBrand PCSB
					on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
				Inner Join ThirdPartySupplier TPS
					on TPS.ThirdPartySupplierID = PCSB.ThirdPartySupplierID
				Where
					TPS.ThirdPartySupplierID = @ThirdPartySupplierID
					
			-------Product HCPCs----------------------------------------------------------------------------
			UPDATE ProductHCPCS
				Set IsActive = 0,
				ModifiedUserID = @ModifiedUserID,
				ModifiedDate = GETDATE() 
			from
				ProductHCPCS PH
				Inner Join PracticeCatalogProduct PCP
					on PH.PracticeCatalogProductID = PCP.PracticeCatalogProductID
				Inner Join PracticeCatalogSupplierBrand PCSB
					on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
				Inner Join ThirdPartySupplier TPS
					on TPS.ThirdPartySupplierID = PCSB.ThirdPartySupplierID
				Where
					TPS.ThirdPartySupplierID = @ThirdPartySupplierID
						
			-------Third Party Product-------------------------------------------------------------------------		
			UPDATE ThirdPartyProduct
				SET IsActive = 0,
				ModifiedUserID = @ModifiedUserID,
				ModifiedDate = GETDATE() 
			from 
				ThirdPartyProduct TPP
				Inner Join PracticeCatalogProduct PCP
					on TPP.ThirdPartyProductID = PCP.ThirdPartyProductID
				Inner Join PracticeCatalogSupplierBrand PCSB
					on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
				Inner Join ThirdPartySupplier TPS
					on TPS.ThirdPartySupplierID = PCSB.ThirdPartySupplierID
				Where
					TPS.ThirdPartySupplierID = @ThirdPartySupplierID		
					
			-------PracticeCatalogProduct-----------------------------------------------------		
			UPDATE PracticeCatalogProduct
				SET IsActive = 0,
				ModifiedUserID = @ModifiedUserID,
				ModifiedDate = GETDATE() 
			from 
				PracticeCatalogProduct PCP
				Inner Join ThirdPartyProduct TPP
					on TPP.ThirdPartyProductID = PCP.ThirdPartyProductID
				Inner Join PracticeCatalogSupplierBrand PCSB
					on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
				Inner Join ThirdPartySupplier TPS
					on TPS.ThirdPartySupplierID = PCSB.ThirdPartySupplierID
				Where
					TPS.ThirdPartySupplierID = @ThirdPartySupplierID		
					
			-------PracticeCatalogSupplierBrand--------------------------------------------------------------------------------------------		
					
			UPDATE PracticeCatalogSupplierBrand
				SET IsActive = 0,
				ModifiedUserID = @ModifiedUserID,
				ModifiedDate = GETDATE() 
			from 
				PracticeCatalogSupplierBrand PCSB
				Inner Join ThirdPartySupplier TPS
					on TPS.ThirdPartySupplierID = PCSB.ThirdPartySupplierID
				Where
					TPS.ThirdPartySupplierID = @ThirdPartySupplierID		
					
					
			----------------------------------------------------------------------------------------------------------------------------------		
			UPDATE ThirdPartySupplier
				SET IsActive = 0,
				ModifiedUserID = @ModifiedUserID,
				ModifiedDate = GETDATE()  
			from 
				ThirdPartySupplier TPS
				Where
					TPS.ThirdPartySupplierID = @ThirdPartySupplierID
					
			/*
			Select * from ThirdPartySupplier TPS where TPS.PracticeID = 1
			
			*/
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		SELECT ISNULL(ERROR_NUMBER(), 0) as ErrorNumber
		IF (XACT_STATE() <> 0)
			ROLLBACK TRANSACTION

	END CATCH
End