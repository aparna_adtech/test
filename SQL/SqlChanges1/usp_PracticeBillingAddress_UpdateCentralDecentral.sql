USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_PracticeBillingAddress_Update]    Script Date: 04/21/2010 19:26:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeBillingAddress_UpdateCentralDecentral
   
   Description:  Update a record in the table Address
				  and update the record in the table PracticeBillingAddress 
   
   AUTHOR:       Mike Sneen 4/21/10  - John Bongiorni 7/16/2007 5:07:54 PM
   
   Modifications:  
   ------------------------------------------------------------ */  
   
Create PROCEDURE [dbo].[usp_PracticeBillingAddress_UpdateCentralDecentral]
(
	  @PracticeID					 INT
	, @ModifiedUserID                INT
)
AS
BEGIN

	-- JB Modified 20070818  to update all practice location billing addresses when the practice billing address is updated.	
	--  uPDATE ALL Practice Location if Centralized.
	DECLARE @IsBillingCentralized BIT	
	SELECT @IsBillingCentralized = IsBillingCentralized FROM dbo.Practice WHERE PracticeID = @PracticeID
	
--	SELECT TOP 1 * FROM dbo.Practice 
	
	
	IF ( @IsBillingCentralized = 1 )
		begin
			EXEC dbo.usp_PracticeLocationBillingAddresses_Update_From_Centralized_PracticeBillingAddress  
					@PracticeID	= @PracticeID
					, @UserID	= @ModifiedUserID
		end
	ELSE
		begin
			EXEC dbo.usp_PracticeLocationBillingAddresses_Update_From_Decentralized_PracticeBillingAddress
			  @PracticeID = @PracticeID
			, @UserID	= @ModifiedUserID

		end	


END


