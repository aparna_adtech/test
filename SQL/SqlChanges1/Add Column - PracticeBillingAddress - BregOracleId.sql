USE [BregVision]
GO

ALTER TABLE dbo.PracticeBillingAddress ADD
	BregOracleID varchar(50) NULL
GO

ALTER TABLE dbo.PracticeBillingAddress ADD
	BregOracleIDInvalid bit default(0)
GO