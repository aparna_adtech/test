USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_TransferFromPracticeCatalogtoProductInventory]    Script Date: 04/08/2012 18:22:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Greg Ross
-- Create date: 10/01/07
-- Description:	Transfers products from the practice catalog to the product inventory
-- Modifications:  
--      2008.01.31  John Bongiorni
--		Set PI.IsSetToActive to 0, PI.IsActive to 1 on insert.
--      2008.02.15  John Bongiorni 
--		Set PI.IsSetToActive to 1, PI.IsActive to 1 on insert.
-- =============================================
ALTER PROCEDURE [dbo].[usp_TransferFromPracticeCatalogtoProductInventory]
	@PracticeLocationID int,
	@PracticeCatalogProductID int,
	@UserID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @RecordCount int
declare @RecordCountInActive int

SELECT  @RecordCount = COUNT(1)
                FROM    dbo.productinventory
                WHERE   PracticeLocationID =@PracticeLocationID
                       AND practicecatalogproductid=@PracticeCatalogProductID	
--print @recordcount

if @recordcount = 0 -- No record exists
begin

	--Insert new record
	insert into ProductInventory
		(PracticeLocationID,
		PracticeCatalogProductID,
		QuantityOnHandPerSystem,
		ParLevel,
		ReorderLevel,
		CriticalLevel,
		IsLogoAblePart,
		CreatedUserID,
		CreatedDate,
		IsSetToActive,
		isactive)
		Values
		(@PracticeLocationID,
		@PracticeCatalogProductID,
		0,
		0,
		0,
		0,
		dbo.GetPCPItemLogoValue(@PracticeCatalogProductID),
		@UserID,
		getdate(),
		1,
		1)
	
end
ELSE
	--  Check if there is one and only one record that is InActive, 
	--		if so, then update to Active.
	SELECT  @RecordCountInActive = COUNT(1)
    FROM    dbo.productinventory
    WHERE   PracticeLocationID =@PracticeLocationID
            AND practicecatalogproductid=@PracticeCatalogProductID	
			AND IsActive = 0

	
	if @RecordCountInActive = 1 -- One InActive record exists, so set it to Active.
	BEGIN
		
		UPDATE dbo.productinventory
		SET IsSetToActive = 1  --  JB  		2008.02.15 
			, IsActive = 1
	    WHERE   PracticeLocationID = @PracticeLocationID
            AND practicecatalogproductid = @PracticeCatalogProductID	
			AND IsActive = 0
	
	END
	
END
