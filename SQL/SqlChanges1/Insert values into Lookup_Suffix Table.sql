/****** Script for SelectTopNRows command from SSMS  ******/
USE BregVision_Beta
SELECT TOP 100 [Lookup_SuffixID]
      ,[Suffix]
  FROM [dbo].[Lookup_Suffix]

BEGIN TRAN
IF NOT EXISTS (SELECT * FROM dbo.Lookup_Suffix WHERE  suffix = 'CHT')	BEGIN INSERT into dbo.Lookup_Suffix( Suffix )VALUES  ( 'CHT'  ) end
IF NOT EXISTS (SELECT * FROM dbo.Lookup_Suffix WHERE  suffix = 'CNA')	BEGIN INSERT into dbo.Lookup_Suffix( Suffix )VALUES  ( 'CNA'  ) end
IF NOT EXISTS (SELECT * FROM dbo.Lookup_Suffix WHERE  suffix = 'CRNP')	BEGIN INSERT into dbo.Lookup_Suffix( Suffix )VALUES  ( 'CRNP'  ) end
IF NOT EXISTS (SELECT * FROM dbo.Lookup_Suffix WHERE  suffix = 'CRT')	BEGIN INSERT into dbo.Lookup_Suffix( Suffix )VALUES  ( 'CRT'  ) end
IF NOT EXISTS (SELECT * FROM dbo.Lookup_Suffix WHERE  suffix = 'LPN')	BEGIN INSERT into dbo.Lookup_Suffix( Suffix )VALUES  ( 'LPN'  ) end
IF NOT EXISTS (SELECT * FROM dbo.Lookup_Suffix WHERE  suffix = 'NP')	BEGIN INSERT into dbo.Lookup_Suffix( Suffix )VALUES  ( 'NP'  ) end
IF NOT EXISTS (SELECT * FROM dbo.Lookup_Suffix WHERE  suffix = 'RN')	BEGIN INSERT into dbo.Lookup_Suffix( Suffix )VALUES  ( 'RN'  ) end
IF NOT EXISTS (SELECT * FROM dbo.Lookup_Suffix WHERE  suffix = 'PTA')	BEGIN INSERT into dbo.Lookup_Suffix( Suffix )VALUES  ( 'PTA'  ) end
ROLLBACK
--commit

--CHT
--CNA
--CRNP
--CRT
--LPN
--NP
--RN
--PTA

