USE [BregVision_BETA]
GO
/****** Object:  StoredProcedure [dbo].[usp_Report_ProductsDispensed_Special]    Script Date: 04/10/2014 02:59:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--  [usp_Report_ProductsDispensed_Special]  6, '13, 15, 16, 17, 18, 19, 20, 22, 23, 24', '1/1/2007', '12/31/2007'

--  Added Supplier Brand.
--  Added Sequence
--  Needs where isActive
-- JB 2007.12.13  will work for insert summary of third party.

ALTER PROC [dbo].[usp_Report_ProductsDispensed_Special]  
		  @PracticeID	INT
		, @PracticeLocationID	VARCHAR(2000)
		, @StartDate		DATETIME
		, @EndDate			DATETIME
AS
BEGIN


--DECLARE @Output VARCHAR(50)
--SET @Output = ''
--
--SELECT @Output =
--	CASE WHEN @Output = '' THEN CAST(PracticeLocationID AS VARCHAR(50))
--		 ELSE @Output + ', ' + CAST(PracticeLocationID AS VARCHAR(50))
--		 END
--FROM PracticeLocation
--WHERE PracticeID = 6
--
--SELECT @Output AS Output



--	DECLARE @PracticeID INT
--	SET @PracticeID = 6

DECLARE @PracticeLocationIDString VARCHAR(2000)


--  SET @EndDate to 11:59:59 PM
SELECT @EndDate = DATEADD(DAY, 1, @EndDate)


SET @PracticeLocationIDString = @PracticeLocationID

--DECLARE @PhysicianIDString VARCHAR(2000)
--SET @PhysicianIDString = @PhysicianID
---  Insert select here ----

DECLARE @DetailTable TABLE
 (
		  Practice						VARCHAR(50)
		, Location						VARCHAR(50)
		, SupplierBrand					VARCHAR(200)
		, Supplier						VARCHAR(50)
		, Brand							VARCHAR(50)
		, IsBrandSuppressed				INT
		, ThirdPartySupplierID			INT
		, PracticeCatalogSupplierBrandID INT
		, IsThirdPartySupplier			INT
		, IsVendor						INT
		, Sequence						INT
		, Product						VARCHAR(50)
		, SideSizeGender				VARCHAR(200)
		, Code							VARCHAR(50)
		, HCPCS							VARCHAR(2000)
		, Physician						VARCHAR(200)
		, PatientCode					VARCHAR(255)
		, DateDispensed					VARCHAR(50)
		, Quantity 						INT
		, ActualWholesaleCostLineTotal	DECIMAL(15,2)
		, DMEDepositLineTotal			DECIMAL(15,2)
		, ActualChargeBilledLineTotal	DECIMAL(15,2)
	)


INSERT INTO @DetailTable
	(
		Practice
		, Location
		, SupplierBrand
		, Supplier
		, Brand
		, IsBrandSuppressed
		, ThirdPartySupplierID
		, PracticeCatalogSupplierBrandID
		, IsThirdPartySupplier
		, IsVendor
		, Sequence
		, Product
		, SideSizeGender
		, Code
		, HCPCS
		, Physician
		, PatientCode			
		, DateDispensed		
		, Quantity 		
		, ActualWholesaleCostLineTotal
		, DMEDepositLineTotal
		, ActualChargeBilledLineTotal
	)

SELECT 
		  SUB.PracticeName		AS Practice
		, SUB.PracticeLocation	AS Location

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
			ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
			END					AS SupplierBrand


		, SUB.SupplierShortName	AS Supplier

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
		ELSE SUB.BrandShortName
		END						AS Brand

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN 1
		ELSE 0
		END AS IsBrandSuppressed

		, SUB.ThirdPartySupplierID
		, SUB.PracticeCatalogSupplierBrandID
		, SUB.IsThirdPartySupplier
		, SUB.IsVendor

		, SUB.Sequence	

		, SUB.ProductName			AS Product

		, SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender

		, SUB.Code

		, dbo.ConcatHCPCS(SUB.PracticeCatalogProductID) AS HCPCS -- JB 20071004 Added Column for UDF Concat of HCPCS
	
		
		, SUB.Physician
		, SUB.PatientCode			
		, CONVERT(VARCHAR(10), SUB.DateDispensed, 101) AS DateDispensed		

		, SUB.Quantity 		--, SUM( SUB.Quantity )

--		, CAST( SUB.ActualWholesaleCost AS DECIMAL(15,2) )	AS ActualWholesaleCost
--		, CAST( SUB.ActualChargeBilled AS DECIMAL(15,2) )	AS ActualChargeBilled
--		, CAST( SUB.DMEDeposit AS DECIMAL(15,2) )			AS DMEDeposit

		, CAST(  SUB.ActualWholesaleCostLineTotal  AS DECIMAL(15,2) )	AS ActualWholesaleCostLineTotal
		, CAST(  SUB.DMEDepositLineTotal  AS DECIMAL(15,2) )			AS DMEDepositLineTotal
		, CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )    AS ActualChargeBilledLineTotal
--		
--		, SUB.Comment  -- Not Displayed for Version 1.
FROM


		(
		SELECT 

			  P.PracticeName
			, PL.Name AS PracticeLocation
			, IsNULL(RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix), '') AS Physician

			, D.PatientCode
			
			, -999 AS ThirdPartySupplierID
			, PCSB.PracticeCatalogSupplierBrandID
			, PCSB.IsThirdPartySupplier

			, 0 AS IsVendor

			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) AS Sequence

			, PCP.PracticeCatalogProductID

			, MCP.ShortName AS ProductName

			, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
			
			, ISNULL(MCP.Code, '') AS Code

			, DD.CreatedDate AS DateDispensed

			, DD.Quantity
			, PI_DD.ActualWholesaleCost 
			, DD.ActualChargeBilled
			, DD.DMEDeposit	

			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit * DD.Quantity AS DMEDepositLineTotal
			, PI_DD.ActualWholesaleCost * DD.Quantity AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.
	
		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOin DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID 

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
		INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
			ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1

		WHERE P.PracticeID = @PracticeID		
		
		AND 
		PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))
		


--		AND 
--		Ph.PhysicianID IN 
--			(SELECT ID 
--			 FROM dbo.udf_ParseArrayToTable(@PhysicianIDString, ','))
	
		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM
			
		UNION

		SELECT 

			 P.PracticeName
			, PL.Name AS PracticeLocation
			, C.FirstName + ' ' + C.LastName AS Physician  --  Get proper line for this!!

			, D.PatientCode
			
			, PCSB.ThirdPartySupplierID AS ThirdPartySupplierID
			, PCSB.PracticeCatalogSupplierBrandID
			, PCSB.IsThirdPartySupplier

			, TPS.IsVendor

			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) + 1000 AS Sequence

			, PCP.PracticeCatalogProductID

			, TPP.[Name] AS ProductName

			, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender


			, ISNULL(TPP.Code, '') AS Code

			, DD.CreatedDate AS DateDispensed

			, DD.Quantity
			, PI_DD.ActualWholesaleCost 
			, DD.ActualChargeBilled
			, DD.DMEDeposit	

			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit * DD.Quantity AS DMEDepositLineTotal
			, PI_DD.ActualWholesaleCost * DD.Quantity AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.

		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOin DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID 

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 

		INNER JOIN dbo.ThirdPartySupplier AS TPS WITH (NOLOCK)
			ON PCSB.ThirdPartySupplierID = TPS.ThirdPartySupplierID

		INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
			ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1	
	
		WHERE P.PracticeID = @PracticeID		
		
		AND 
		PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))
	
		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM	

		) AS Sub

----
------GROUP BY
------		SUB.PracticeName
------		, SUB.PracticeLocation
------		, SUB.Physician
------		, SUB.IsThirdPartySupplier
------		, SUB.Sequence
------		, SUB.SupplierShortName
------		, SUB.BrandShortName
------		
------		, SUB.ProductName
------		, SUB.Side
------		, SUB.Size
------		, SUB.Gender
------		, SUB.Code
------		
------		, SUB.PatientCode
------
------		, SUB.DateDispensed
------
------		, SUB.ActualWholesaleCost
------		, SUB.ActualChargeBilled
------		, Sub.DMEDeposit
------		, Sub.Comment
----
----		
	ORDER BY
		SUB.PracticeName
		, SUB.PracticeLocation

--		, SUB.Physician

		, SUB.Sequence		--  Note 3rd Party has 100 added to it sequence for this query.
--		, SUB.IsThirdPartySupplier
		, SUB.SupplierShortName
		, SUB.BrandShortName

		, SUB.ProductName
		, SUB.Code

		, SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender


DECLARE @3rdSupplierSummary TABLE
	(
		  Practice	VARCHAR(50)
		, Location	VARCHAR(50)
		, Supplier	Varchar(50)
		, ThirdPartySupplierID	INT
		, SupplierBrandcount	INT
		, PracticeCatalogSupplierBrandIDCount	INT
	)

INSERT INTO @3rdSupplierSummary
	(
		  Practice	
		, Location
		, Supplier
		, ThirdPartySupplierID
		, SupplierBrandcount
		, PracticeCatalogSupplierBrandIDCount
	)
SELECT 
	  D.Practice
	, D.Location
	, D.Supplier
	, D.ThirdPartySupplierID
	, Count(D.SupplierBrand) AS SupplierBrandCount
	, COUNT(PracticeCatalogSupplierBrandID)

FROM
(SELECT DISTINCT 
	  Practice
	, Location
	, Supplier
	, ThirdPartySupplierID
	, SupplierBrand
	, PracticeCatalogSupplierBrandID
FROM @DetailTable
WHERE 
	IsThirdPartySupplier = 1
	AND IsVendor = 1
) AS D
Group BY  
	  D.Practice
	, D.Location 
	, D.Supplier
	, D.ThirdPartySupplierID
HAVING Count(D.SupplierBrand) > 2


SELECT * FROM @3rdSupplierSummary

SELECT * FROM @DetailTable




SELECT 
	  SS.Practice
	, SS.Location
	, SS.Supplier AS SupplierBrand
	, SS.ThirdPartySupplierID
	, SUM(DT.ActualWholesaleCostLineTotal)	AS SUMActualWholesaleCostLineTotal
	, SUM(DT.DMEDepositLineTotal)			AS SUMDMEDepositLineTotal
	, SUM(DT.ActualChargeBilledLineTotal)   AS SUMActualChargeBilledLineTotal
	, 1 AS SpecialLine
FROM @3rdSupplierSummary AS SS
INNER JOIN @DetailTable AS DT
	ON      SS.Practice = DT.Practice
		AND SS.Location = DT.Location
		AND SS.ThirdPartySupplierID = DT.ThirdPartySupplierID
	
GROUP BY
      SS.Practice
	, SS.Location
	, SS.Supplier
	, SS.ThirdPartySupplierID
	
END
GO
