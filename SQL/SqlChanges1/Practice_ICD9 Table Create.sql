USE [BregVision]
GO

/****** Object:  Table [dbo].[Practice_ICD9]    Script Date: 11/10/2009 22:58:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Practice_ICD9](
	[ICD9ID] [int] NOT NULL,
	[PracticeID] [int] NOT NULL,
	[CreatedUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedUserID] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Practice_ICD9] PRIMARY KEY CLUSTERED 
(
	[ICD9ID] ASC,
	[PracticeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Practice_ICD9]  WITH CHECK ADD  CONSTRAINT [FK_Practice_ICD9_ICD9] FOREIGN KEY([ICD9ID])
REFERENCES [dbo].[ICD9] ([ICD9ID])
GO

ALTER TABLE [dbo].[Practice_ICD9] CHECK CONSTRAINT [FK_Practice_ICD9_ICD9]
GO

ALTER TABLE [dbo].[Practice_ICD9]  WITH CHECK ADD  CONSTRAINT [FK_Practice_ICD9_Practice] FOREIGN KEY([PracticeID])
REFERENCES [dbo].[Practice] ([PracticeID])
GO

ALTER TABLE [dbo].[Practice_ICD9] CHECK CONSTRAINT [FK_Practice_ICD9_Practice]
GO

ALTER TABLE [dbo].[Practice_ICD9] ADD  DEFAULT ((1)) FOR [CreatedUserID]
GO

ALTER TABLE [dbo].[Practice_ICD9] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[Practice_ICD9] ADD  DEFAULT ((1)) FOR [IsActive]
GO


