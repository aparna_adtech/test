USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_SuperbillCategory_Insert]    Script Date: 03/12/2009 23:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author: Mike Sneen>
-- Create date: <Create Date: 02/28/09>
-- Description:	<Description: Used to get all a superbill>
-- modification:
--  [dbo].[usp_SuperbillCategory_Insert]  

-- =============================================
Create PROCEDURE [dbo].[usp_SuperbillCategory_Insert]  --3 --10  --  _With_Brands
	@PracticeLocationID int,
	@ColumnNumber int,
	@CategoryName varchar(30)
AS 
    BEGIN
    DECLARE @Err INT
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
--        SET NOCOUNT ON ;
     
     INSERT into SuperBillCategory
     (
		PracticeLocationID,
		ColumnNumber,
		Name,
		CreatedUserID,
		CreatedDate,
		IsActive
		
     ) 
     values
     (
		@PracticeLocationID,
		@ColumnNumber,
		@CategoryName,
		1,
		GETDATE(),
		1
     )
     
     	SET @Err = @@ERROR

	RETURN @Err

    END

--Exec usp_SuperbillCategory_Insert 3, 3