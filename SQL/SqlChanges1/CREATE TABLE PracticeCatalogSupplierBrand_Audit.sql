CREATE TABLE [dbo].[PracticeCatalogSupplierBrand_Audit](
	[PracticeCatalogSupplierBrand_AuditID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PracticeCatalogSupplierBrandID] [int] NOT NULL,
	[PracticeID] [int] NOT NULL,
	[MasterCatalogSupplierID] [int] NULL,
	[ThirdPartySupplierID] [int] NULL,
	[SupplierName] [varchar](50) NOT NULL,
	[SupplierShortName] [varchar](50) NOT NULL,
	[BrandName] [varchar](50) NOT NULL,
	[BrandShortName] [varchar](50) NOT NULL,
	[IsThirdPartySupplier] [bit] NOT NULL,
	[Sequence] [int] NULL,
	[CreatedUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedUserID] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
	[SupplierWarrantyID] [int] NULL,
	[AuditAction] [varchar](50) NULL,
	[AuditCreatedDate] [datetime] NULL,
 CONSTRAINT [PracticeCatalogSupplierBrand_Audit_PK] PRIMARY KEY CLUSTERED 
(
	[PracticeCatalogSupplierBrand_AuditID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

