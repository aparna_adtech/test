USE [BregVisionEMR]
GO

/****** Object:  Table [dbo].[outbound_hl7_dispensement_item]    Script Date: 5/1/2014 5:57:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[outbound_hl7_dispensement_item](
	[outbound_hl7_dispensement_item_id] [uniqueidentifier] NOT NULL,
	[outbound_hl7_item_queue_id] [uniqueidentifier] NOT NULL,
	[item_code] [varchar](50) NOT NULL,
	[dispense_quantity] [int] NOT NULL,
	[unit_price] [money] NOT NULL,
	[hcpcs_code] [varchar](20) NULL,
	[modifier1] [varchar](2) NULL,
	[modifier2] [varchar](2) NULL,
	[modifier3] [varchar](2) NULL,
	[modifier4] [varchar](2) NULL,
	[patient_signature] [image] NULL,
	[physician_signature] [image] NULL,
	[dispense_date] [datetime] NOT NULL,
	[creation_date] [datetime] NOT NULL,
 CONSTRAINT [PK_outbound_hl7_dispensement_item] PRIMARY KEY CLUSTERED 
(
	[outbound_hl7_dispensement_item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[outbound_hl7_dispensement_item] ADD  CONSTRAINT [DF_outbound_hl7_dispensement_item_outbound_hl7_dispensement_item_id]  DEFAULT (newid()) FOR [outbound_hl7_dispensement_item_id]
GO

ALTER TABLE [dbo].[outbound_hl7_dispensement_item] ADD  CONSTRAINT [DF_outbound_hl7_dispensement_item_dispense_quantity]  DEFAULT ((1)) FOR [dispense_quantity]
GO

ALTER TABLE [dbo].[outbound_hl7_dispensement_item] ADD  CONSTRAINT [DF_outbound_hl7_dispensement_item_unit_price]  DEFAULT ((0)) FOR [unit_price]
GO

ALTER TABLE [dbo].[outbound_hl7_dispensement_item] ADD  CONSTRAINT [DF_outbound_hl7_dispensement_item_dispense_date]  DEFAULT (getdate()) FOR [dispense_date]
GO

ALTER TABLE [dbo].[outbound_hl7_dispensement_item] ADD  CONSTRAINT [DF_outbound_hl7_dispensement_item_creation_date]  DEFAULT (getdate()) FOR [creation_date]
GO

ALTER TABLE [dbo].[outbound_hl7_dispensement_item]  WITH CHECK ADD  CONSTRAINT [FK_outbound_hl7_dispensement_item_outbound_hl7_item_queue] FOREIGN KEY([outbound_hl7_item_queue_id])
REFERENCES [dbo].[outbound_hl7_item_queue] ([outbound_hl7_item_queue_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[outbound_hl7_dispensement_item] CHECK CONSTRAINT [FK_outbound_hl7_dispensement_item_outbound_hl7_item_queue]
GO


