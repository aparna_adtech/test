USE [BregVision]
GO

/****** Object:  Table [dbo].[DispenseBatch]    Script Date: 11/10/2009 22:47:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DispenseBatch](
	[DispenseBatchID] [int] IDENTITY(1,1) NOT NULL,
	[PracticeLocationID] [int] NOT NULL,
	[DispenseDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedUser] [int] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_DispenseBatch] PRIMARY KEY CLUSTERED 
(
	[DispenseBatchID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[DispenseBatch]  WITH CHECK ADD  CONSTRAINT [FK_DispenseBatch_PracticeLocation] FOREIGN KEY([PracticeLocationID])
REFERENCES [dbo].[PracticeLocation] ([PracticeLocationID])
GO

ALTER TABLE [dbo].[DispenseBatch] CHECK CONSTRAINT [FK_DispenseBatch_PracticeLocation]
GO

ALTER TABLE [dbo].[DispenseBatch] ADD  CONSTRAINT [DF_DispenseBatch_IsActive]  DEFAULT ((0)) FOR [IsActive]
GO


