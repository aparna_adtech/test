USE [BregVision]
GO

/****** Object:  Table [dbo].[DeviceAuthenticationRequest]    Script Date: 06/01/2012 19:02:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DeviceAuthenticationRequest](
	[RequestID] [uniqueidentifier] NOT NULL,
	[DeviceIdentifier] [varchar](255) NOT NULL,
	[PracticeID] [int] NULL,
	[PracticeLocationID] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_DeviceAuthenticationRequest] PRIMARY KEY CLUSTERED 
(
	[RequestID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[DeviceAuthenticationRequest] ADD  CONSTRAINT [DF_DeviceAuthenticationRequest_RequestID]  DEFAULT (newid()) FOR [RequestID]
GO

ALTER TABLE [dbo].[DeviceAuthenticationRequest] ADD  CONSTRAINT [DF_DeviceAuthenticationRequest_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

