
---------131-------------------
UPDATE   HelpArticles
SET            
        Title ='Delete All', 
        Description ='Click this button to delete all items in shopping cart'
where ID=127
---------132---------------
UPDATE   HelpArticles
SET            
        Title ='Update Quantity', 
        Description ='Increment/Decrement order quantity of one or more items in shopping cart to modify order'
where ID=128

--------133------------------------

UPDATE   HelpArticles
SET            
        Title ='Purchase', 
        Description ='click this button to place order as displayed in table'
where ID=129