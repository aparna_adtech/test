use [BregVision]
go



if not exists (select * from dbo.syscolumns where id = object_id(N'FaxMessage') and name=(N'SendAttempts'))
alter table [FaxMessage] add SendAttempts int DEFAULT 0 NOT NULL
GO