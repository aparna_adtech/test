USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_Transfer_InsertUpdate]    Script Date: 03/21/2009 23:28:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--EXEC usp_Transfer_InsertUpdate 2, 58580, 3, 42, 1, 1, 43, ''
-- =============================================
-- Author:		Mike Sneen
-- Create date: 03/20/09
-- Description:	Used to insert and update table when performing a Transfer
-- =============================================
Alter PROCEDURE [dbo].[usp_Transfer_InsertUpdate]
 @PracticeID INT,	
 @PracticeCatalogProductID INT,
 @ShoppingCartItemID int,
 @PracticeLocationID_TransferFrom int,
 @PracticeLocationID_TransferTo int,
 @UserID INT,
 @Quantity int,
 @ActualWholesaleCost smallmoney,
 @Comments varchar(200)

AS 
    BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON 
			
		declare @Msg varchar(4000)
        DECLARE @TransactionCountOnEntry INT 
        

        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

        SELECT  @TransactionCountOnEntry = @@TRANCOUNT
        BEGIN TRANSACTION      	

			
        
            
			Declare @From_ProductInventoryID int
			declare @QOH_at_From_PracticeLocation int
			declare @To_ProductInventoryID int
			declare @QOH_at_TO_PracticeLocation int
			Declare @StockingUnits int
				
			IF @Quantity = 0 
			BEGIN
				Set @Msg = 'Application Error - Quantity Must be more than 0'
				RAISERROR(@Msg, 17, 1)
			END
							    
			--find the product inventory id at the from location			    
		    Set @From_ProductInventoryID = (select ProductInventoryID from ProductInventory where PracticeLocationID = @PracticeLocationID_TransferFrom AND PracticeCatalogProductID = @PracticeCatalogProductID AND IsActive = 1)
			IF ISNull(@From_ProductInventoryID, 0) = 0 
			BEGIN
				Set @Msg = 'Application Error - Error Finding From_ProductInventoryID'
				RAISERROR(@Msg, 17, 1)
			END  
			  

				
			--find the Quantity on hand at the from location for this catalog item
			set @QOH_at_From_PracticeLocation = (select QuantityOnHandPerSystem from ProductInventory where ProductInventoryID = @From_ProductInventoryID	)
			IF ISNull(@QOH_at_From_PracticeLocation, 0) = 0 
			BEGIN
				Set @Msg = 'Application Error - Error Finding QOH_at_From_PracticeLocation'
				RAISERROR(@Msg, 17, 1)
			END	
				
				
			--print '@QOH_at_From_PracticeLocation ' 			
			--print @QOH_at_From_PracticeLocation
				
			--Subtract Quantity from  the 'from' Practice Location Update ProductInventory
			set @QOH_at_From_PracticeLocation = @QOH_at_From_PracticeLocation - @Quantity
			IF @QOH_at_From_PracticeLocation is null OR @QOH_at_From_PracticeLocation < 0 
			BEGIN
				Set @Msg = 'Application Error - Error Subtracting QOH_at_From_PracticeLocation'
				RAISERROR(@Msg, 17, 1)
			END	
							
			--print '@QOH_at_From_PracticeLocation - Q ' 		
			--print @QOH_at_From_PracticeLocation
				
				
			--Find the product inventory ID at the to location for this catalog item
			set @To_ProductInventoryID = (select ProductInventoryID from ProductInventory where PracticeLocationID = @PracticeLocationID_TransferTo AND PracticeCatalogProductID = @PracticeCatalogProductID)
		
               
           --if the To Product Inventory does not have a Product Inventory ID at the Location for this Catalog Item, then Make one
           IF @To_ProductInventoryID IS NULL
			BEGIN
				EXEC usp_TransferFromPracticeCatalogtoProductInventory @PracticeLocationID_TransferTo, @PracticeCatalogProductID, @UserID
				EXEC usp_UpdateInventoryLevels @PracticeCatalogProductID, @PracticeLocationID_TransferTo, 0, 0, 0, 0, 0, @UserID
				
				set @To_ProductInventoryID = (select ProductInventoryID from ProductInventory where PracticeLocationID = @PracticeLocationID_TransferTo AND PracticeCatalogProductID = @PracticeCatalogProductID)

			END
			
			IF ISNull(@To_ProductInventoryID, 0) = 0 
			BEGIN
				Set @Msg = 'Application Error - Error Finding To_ProductInventoryID'
				RAISERROR(@Msg, 17, 1)
			END 
				
			--find the Quantity on hand at the TO location for this catalog item
			set @QOH_at_TO_PracticeLocation = (select QuantityOnHandPerSystem from ProductInventory where ProductInventoryID = @To_ProductInventoryID)
			IF @QOH_at_TO_PracticeLocation is null OR @QOH_at_TO_PracticeLocation < 0  
			BEGIN
				Set @Msg = 'Application Error - Error Finding QOH_at_TO_PracticeLocation'
				RAISERROR(@Msg, 17, 1)
			END	
			--Add Quantity to  the 'To' Practice Location Update ProductInventory
			set @QOH_at_TO_PracticeLocation = @QOH_at_TO_PracticeLocation + @Quantity
			IF @QOH_at_TO_PracticeLocation is null OR @QOH_at_TO_PracticeLocation < 0 
			BEGIN
				Set @Msg = 'Application Error - Error Adding QOH_at_TO_PracticeLocation'
				RAISERROR(@Msg, 17, 1)
			END	
				
			--print @QOH_at_From_PracticeLocation
			--print @From_ProductInventoryID
			
			--print @QOH_at_TO_PracticeLocation
			--print @To_ProductInventoryID
			
			Set @StockingUnits = (Select pcp_s.StockingUnits from PracticeCatalogProduct pcp_s where pcp_s.PracticeCatalogProductID = @PracticeCatalogProductID)	
				
			--Update the from location with the subtracted Quantity
			update ProductInventory	
				set QuantityOnHandPerSystem = @QOH_at_From_PracticeLocation,
					ModifiedUserID = @UserID,
					ModifiedDate = getdate()
				WHERE   ProductInventoryID = @From_ProductInventoryID
                    AND IsActive = 1

           --Update the TO location with the subtracted Quantity
		   update ProductInventory	
				set QuantityOnHandPerSystem = @QOH_at_TO_PracticeLocation,
					ModifiedUserID = @UserID,
					ModifiedDate = getdate()
				WHERE   ProductInventoryID = @To_ProductInventoryID
					AND IsActive = 1
			
        
			
			
			
			--Insert record into ProductInventory_Transfer for Auditing and Reporting
			insert into ProductInventory_Transfer  
				(PracticeID,
				 ProductInventoryID_TransferFrom,
				 ProductInventoryID_TransferTo,
				 PracticeCatalogProductID,
				 PracticeLocationID_TransferFrom,
				 PracticeLocationID_TransferTo,
				 ActualWholesaleCost,
				 Quantity,
				 TransferDate,
				 Comments,
				 CreatedUserID,
				 CreatedDate,
				 IsActive)
				Values
				 (@PracticeID,
				  @From_ProductInventoryID,
				  @To_ProductInventoryID,
				  @PracticeCatalogProductID,
				  @PracticeLocationID_TransferFrom,
				  @PracticeLocationID_TransferTo,
				  (@ActualWholesaleCost / @StockingUnits),
				  @Quantity,
				  getdate(),
				  @Comments,
				  @UserID,
				  getdate(),
				  1)

                        
            --Delete Item from shopping cart
            EXEC usp_DeleteItemfromShoppingCartItem @ShoppingCartItemID

  
  
			IF @@TRANCOUNT > @TransactionCountOnEntry 
				COMMIT TRANSACTION				
   END TRY	
   BEGIN CATCH
		-- Whoops, there was an error
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION
		
				-- Raise an error with the details of the exception
		DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
		SELECT @ErrMsg = ERROR_MESSAGE(),
			@ErrSeverity = ERROR_SEVERITY()	
		
	    EXECUTE [dbo].[uspLogError]
	    EXECUTE [dbo].[uspPrintError]
		RAISERROR(@ErrMsg, @ErrSeverity, 1)
   END CATCH


