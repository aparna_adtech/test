




/*

--  [usp_Report_ProductsDispensed_By_PatientCode]  6, '13, 15, 16, 17, 18, 19, 20, 22, 23, 24', '1/1/2007', '12/31/2007'
--  Added Supplier Brand.
--  Added Sequence
--  Add IsActive = 1

--  Modified from usp_Report_ProductsDispensed

--  Created by:  John Bongiorni
--  Date:		 2008.01.04 
--  Modified:  2008.02.12 JB , D.DateDispensed AS DateDispensed	--, DD.CreatedDate AS DateDispensed  --JB 2008.02.12
	usp_Report_ProductsDispensed_By_PatientCode 1, '3', '12/1/2009', '12/4/2009', 1, 1, null,1
*/

ALTER PROC [dbo].[usp_Report_ProductsDispensed_By_PatientCode]  
		  @PracticeID	INT
		, @PracticeLocationID	VARCHAR(2000)
		, @StartDate		DATETIME
		, @EndDate			DATETIME
		, @OrderBy				int
		, @OrderDirection		bit
		, @IsMedicare           bit = null
		, @MedicareOrderDirection bit = null
AS
BEGIN
print @MedicareOrderDirection

--DECLARE @Output VARCHAR(50)
--SET @Output = ''
--
--SELECT @Output =
--	CASE WHEN @Output = '' THEN CAST(PracticeLocationID AS VARCHAR(50))
--		 ELSE @Output + ', ' + CAST(PracticeLocationID AS VARCHAR(50))
--		 END
--FROM PracticeLocation
--WHERE PracticeID = 6
--
--SELECT @Output AS Output



--	DECLARE @PracticeID INT
--	SET @PracticeID = 6

DECLARE @PracticeLocationIDString VARCHAR(2000)


--  SET @EndDate to 11:59:59 PM
SELECT @EndDate = DATEADD(DAY, 1, @EndDate)


SET @PracticeLocationIDString = @PracticeLocationID

--DECLARE @PhysicianIDString VARCHAR(2000)
--SET @PhysicianIDString = @PhysicianID
---  Insert select here ----

SELECT 
		  SUB.PracticeName		AS Practice
		, SUB.PracticeLocation	AS Location

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
			ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
			END					AS SupplierBrand


		, SUB.SupplierShortName	AS Supplier

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
		ELSE SUB.BrandShortName
		END						AS Brand

--		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN 1
--		ELSE 0
--		END AS IsBrandSuppressed
--		, SUB.IsThirdPartySupplier
		, SUB.Sequence	

		, SUB.ProductName			AS Product

		, SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender

		, SUB.Code

		, COALESCE(SUB.HCPCs, dbo.ConcatHCPCS(SUB.PracticeCatalogProductID)) AS HCPCS -- JB 20071004 Added Column for UDF Concat of HCPCS
		--, if (SUB.ISCashCollection=1)  SUB.BillingChargeCash 
		, case SUB.IsCashCollection
			when 1 then Sub.BillingChargeCash
			when 0 then 0
			end
			 as BillingChargeCash

		, SUB.Physician
		, SUB.PatientCode	
		, Sub.DispenseID		
		, CONVERT(VARCHAR(10), SUB.DateDispensed, 101) AS DateDispensed		
		, CASE SUB.IsMedicare
			When 1 then 'Y'
			When 0 then 'N'
		  End 
		  as IsMedicare
		, CASE SUB.ABNForm
			When 1 then 'Y'
			When 0 then 'N'
		  End 
		  as ABNForm	
		  , SUB.ICD9Code	
		  , SUB.Quantity 		--, SUM( SUB.Quantity )

		  ,SUB.FitterName
--		, CAST( SUB.ActualWholesaleCost AS DECIMAL(15,2) )	AS ActualWholesaleCost
--		, CAST( SUB.ActualChargeBilled AS DECIMAL(15,2) )	AS ActualChargeBilled
--		, CAST( SUB.DMEDeposit AS DECIMAL(15,2) )			AS DMEDeposit

		, CAST(  SUB.ActualWholesaleCostLineTotal  AS DECIMAL(15,2) )	AS ActualWholesaleCostLineTotal
		, CAST(  SUB.DMEDepositLineTotal  AS DECIMAL(15,2) )			AS DMEDepositLineTotal
		, SUB.IsCashCollection
		--, CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )    AS ActualChargeBilledLineTotal
		,  case SUB.IsCashCollection
			when 1 then 0
			when 0 then CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )	
			end
			AS ActualChargeBilledLineTotal
--		, SUB.Comment  -- Not Displayed for Version 1.

		, ISNULL(SUB.PayerName, 'N/A') AS PayerName
		, SUB.DispenseNote
		, SUB.OriginalActualChargeBilled
		, LTRIM(RTRIM(ISNULL(SUB.PatientFirstName,''))) AS PatientFirstName
		, LTRIM(RTRIM(ISNULL(SUB.PatientLastName,''))) AS PatientLastName
		, SUB.IsCustomFit AS IsCustomFit
FROM


		(
		SELECT 

			  P.PracticeName
			, PL.Name AS PracticeLocation
			, IsNULL(RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix), '') AS Physician

			, D.PatientCode
			, D.DispenseID
			
			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) AS Sequence

			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash

			, MCP.[Name] AS ProductName

			--, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
			
			, ISNULL(MCP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed	--, DD.CreatedDate AS DateDispensed  --JB 2008.02.12
			, DD.IsMedicare
			, DD.ABNForm
			, DD.ICD9Code
			, DD.Quantity
			, FC.FirstName + ' ' + FC.LastName as FitterName
			, PI_DD.ActualWholesaleCost 
			, DD.ActualChargeBilled as ActualChargeBilled
			, DD.DMEDeposit	
			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit AS DMEDepositLineTotal
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.

			, PP.Name AS PayerName
			, D.Notes AS DispenseNote
			, DD.ActualChargeBilled as OriginalActualChargeBilled
			, D.PatientFirstName as PatientFirstName
			, D.PatientLastName as PatientLastName
			, DQ.HCPCs
			, DD.IsCustomFit AS IsCustomFit
		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOin DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID 

		INNER JOIN Clinician AS CP
            ON CP.ClinicianID = DD.FitterID
        INNER JOIN Contact AS FC
            ON CP.ContactID = FC.ContactID

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
		INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
			ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID		    --  Can be inactive

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1

		LEFT JOIN PracticePayer PP
			on PP.PracticePayerID = D.PracticePayerID

		LEFT JOIN DispenseQueue AS DQ
			on DQ.DispenseQueueID = DD.DispenseQueueID

		WHERE 
			P.PracticeID = @PracticeID	
			AND DD.IsMedicare = Coalesce(@IsMedicare, DD.IsMedicare)	
			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			--AND PCP.IsActive = 1
			--AND PCSB.IsActive = 1
			--AND MCP.IsActive = 1
		
		AND 
		PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))
		


--		AND 
--		Ph.PhysicianID IN 
--			(SELECT ID 
--			 FROM dbo.udf_ParseArrayToTable(@PhysicianIDString, ','))
	
		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM
			
		UNION All

		SELECT 

			 P.PracticeName
			, PL.Name AS PracticeLocation
			, C.FirstName + ' ' + C.LastName AS Physician  --  Get proper line for this!!

			, D.PatientCode
			, D.DispenseID
			
			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) + 1000 AS Sequence

			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash

			, TPP.[Name] AS ProductName

			--, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender


			, ISNULL(TPP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed	--, DD.CreatedDate AS DateDispensed  --JB 2008.02.12
			, DD.IsMedicare
			, DD.ABNForm
			, DD.ICD9Code
			, DD.Quantity
			, FC.FirstName + ' ' + FC.LastName as FitterName
			, PI_DD.ActualWholesaleCost 
			, DD.ActualChargeBilled as ActualChargeBilled
			, DD.DMEDeposit	

			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit AS DMEDepositLineTotal
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.

			, PP.Name AS PayerName
			, D.Notes AS DispenseNote
			, DD.ActualChargeBilled as OriginalActualChargeBilled
			, D.PatientFirstName as PatientFirstName
			, D.PatientLastName as PatientLastName
			, DQ.HCPCs
			, DD.IsCustomFit AS IsCustomFit
		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOin DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN Clinician AS CP
            ON CP.ClinicianID = DD.FitterID
        INNER JOIN Contact AS FC
            ON CP.ContactID = FC.ContactID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID 

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
		INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
			ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID			--  Can be inactive

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1	

		LEFT JOIN PracticePayer PP
			on PP.PracticePayerID = D.PracticePayerID
			
		LEFT JOIN DispenseQueue AS DQ
			on DQ.DispenseQueueID = DD.DispenseQueueID

		WHERE 
			P.PracticeID = @PracticeID		
			AND DD.IsMedicare = Coalesce(@IsMedicare, DD.IsMedicare)
			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			--AND PCP.IsActive = 1
			--AND PCSB.IsActive = 1
			--AND TPP.IsActive = 1

		AND 
		PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))
	
		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM	

		) AS Sub

----
------GROUP BY
------		SUB.PracticeName
------		, SUB.PracticeLocation
------		, SUB.Physician
------		, SUB.IsThirdPartySupplier
------		, SUB.Sequence
------		, SUB.SupplierShortName
------		, SUB.BrandShortName
------		
------		, SUB.ProductName
------		, SUB.Side
------		, SUB.Size
------		, SUB.Gender
------		, SUB.Code
------		
------		, SUB.PatientCode
------
------		, SUB.DateDispensed
------
------		, SUB.ActualWholesaleCost
------		, SUB.ActualChargeBilled
------		, Sub.DMEDeposit
------		, Sub.Comment
----

	 order by
	 --/*
		CASE 
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName DESC))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName))
			
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName DESC))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName))
			
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName DESC ))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName ))
			
			
			WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed))
			
			WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed))
			
			WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed))
			
		END
		--*/
		
		/*
		CASE WHEN @OrderBy = 1 and @OrderDirection = 0 THEN SUB.ProductName End DESC,
		CASE WHEN @OrderBy = 1 and @OrderDirection = 1 THEN SUB.ProductName END,
		CASE WHEN @OrderBy = 2 and @OrderDirection = 0 THEN SUB.DateDispensed End DESC,
		CASE WHEN @OrderBy = 2 and @OrderDirection = 1 THEN SUB.DateDispensed END,
		CASE WHEN @OrderBy = 3 and @OrderDirection = 0 THEN SUB.ProductName End DESC,
		CASE WHEN @OrderBy = 3 and @OrderDirection = 1 THEN SUB.ProductName END
		*/	

		

----		
----	ORDER BY
----		SUB.PracticeName
----		, SUB.PracticeLocation
----
------		, SUB.Physician
----
----		, SUB.Sequence		--  Note 3rd Party has 100 added to it sequence for this query.
------		, SUB.IsThirdPartySupplier
----		, SUB.SupplierShortName
----		, SUB.BrandShortName
----
----		--  Sort here by patient code, date dispensed, and physician  --JB  2008.01.04
----		, SUB.PatientCode			
----		, CONVERT(VARCHAR(10), SUB.DateDispensed, 101) --AS DateDispensed		
----		, SUB.Physician
----
----		, SUB.ProductName
----		, SUB.Code
----
----		, SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender

END
