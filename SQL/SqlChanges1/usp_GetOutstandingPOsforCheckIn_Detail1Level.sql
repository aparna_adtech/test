USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOutstandingPOsforCheckIn_Detail1Level]    Script Date: 03/01/2010 19:07:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Greg Ross
-- Create date: 08/20/2007
-- Description:	Used in the Oustanding Purchase Order section of the Check-in UI. 
-- This is a query for the Detail table
-- Modification:  2007-10-03 JB Add Column  SO.SupplierOrderID
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetOutstandingPOsforCheckIn_Detail1Level]
		
	@BregVisionOrderID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select  SO.BregVisionOrderID,
			SO.SupplierOrderID,		--  JB Added this column 20071003
			SO.InvoicePaid,		--added by u= 12/31/09
			SO.InvoiceNumber,	--added by u= 12/31/09
			PCSB.SupplierShortName, 
			PCSB.BrandShortName,
			SO.PurchaseOrder,	
			SOS.[Status],
			SO.Total
		
	 from SupplierOrder SO
	inner join PracticeCatalogSupplierBrand PCSB 
		on SO.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
	inner join SupplierOrderStatus SOS 
		on SO.SupplierOrderStatusID = SOS.SupplierOrderStatusID

	where SO.BregVisionOrderID = @BregVisionOrderID 
	and SO.SupplierOrderStatusID in (1,3)
	and SO.isactive = 1
	order by SupplierShortName
END
