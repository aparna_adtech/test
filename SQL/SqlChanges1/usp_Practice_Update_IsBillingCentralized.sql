USE [BregVision]
GO

/****** Object:  StoredProcedure [dbo].[usp_Practice_Update_IsBillingCentralized]    Script Date: 06-12-2016 05:32:06 ******/
DROP PROCEDURE [dbo].[usp_Practice_Update_IsBillingCentralized]
GO

/****** Object:  StoredProcedure [dbo].[usp_Practice_Update_IsBillingCentralized]    Script Date: 06-12-2016 05:32:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Practice_Update_IsBillingCentralized
   
   Description:  Updates a record in the table Practice.
			Specifically updates the column IsBilling Centralized
   
   AUTHOR:       John Bongiorni 7/30/2007 8:48:21 PM
   
   Modifications:  
   ------------------------------------------------------------ */  

CREATE PROCEDURE [dbo].[usp_Practice_Update_IsBillingCentralized]
(
	  @PracticeID                    INT
	, @IsBillingCentralized          BIT
	, @ModifiedUserID                INT
	, @AddressID					 INT
)
AS
BEGIN

	DECLARE @Err INT

	UPDATE dbo.Practice
	
	SET
		  IsBillingCentralized = @IsBillingCentralized
		, ModifiedUserID = @ModifiedUserID
		, ModifiedDate = GETDATE()
		
	WHERE 
		PracticeID = @PracticeID


	SET @Err = @@ERROR


		-- JB Modified 20070818  to update all practice location billing addresses when a practice billing address is inserted.	
	--  uPDATE ALL Practice Location if Centralized.
	--DECLARE @IsBillingCentralized BIT	
	--SELECT @IsBillingCentralized = IsBillingCentralized FROM dbo.Practice WHERE PracticeID = @PracticeID
	
	--	SELECT TOP 1 * FROM dbo.Practice 
	
	
	IF ( @IsBillingCentralized = 0 )
		begin
			EXEC dbo.usp_PracticeLocationBillingAddresses_Update_From_Decentralized_PracticeBillingAddress
			  @PracticeID = @PracticeID
			, @UserID	= @ModifiedUserID

		end	
	ELSE
		BEGIN
			UPDATE dbo.Address
				SET 
				   ModifiedUserID = @ModifiedUserID
				 , ModifiedDate = GETDATE()
				 , IsActive = 1

			WHERE AddressID = @AddressID
        END

	RETURN @Err
	
End



GO


