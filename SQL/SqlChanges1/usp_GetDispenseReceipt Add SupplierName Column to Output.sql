USE [BregVision]
GO

/****** Object:  StoredProcedure [dbo].[usp_GetDispenseReceipt]    Script Date: 06/23/2011 10:42:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Greg Ross
-- Create date: Sept. 24, 2007
-- Description:	Gets all data necessary to prioduce Dispense Receipt
-- Modifications:  20071002 JB modify for Dispense Receipts without a Physician.
--
--		--20071116 Add Size -- may need to remove Packaging
-- usp_GetDispenseReceipt 259863
--		--20110623 Added SupplierName (by hchan@breg.com)
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetDispenseReceipt]  -- 46
	@DispenseID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



		select 
			D.PatientCode,
			ISNULL( D.ReceiptCode, '')	AS ReceiptCode,  -- D.ReceiptCode
			D.Total as Grandtotal,
			ISNULL( C.Title, '')	AS Title,
			ISNULL( C.FirstName, '') AS FirstName,
			ISNULL( C.LastName, '')  AS LastName,
			ISNULL( C.Suffix, '')	 AS Suffix,
			MCS.SupplierName,
			MCP.ShortName,
			MCP.Code,
			MCP.IsCustomBrace,
			MCP.Packaging,
			
			--MCP.LeftRightSide as Side,  --20091026 MWS Replaced MCP Side with User Chosen Side
			DD.LRModifier as Side,
			MCP.Size,						--20071116 Add Size -- may need to remove Packaging
			
			
			ISNULL( MCP.Color, '') AS Color,
			MCP.Gender,
			DD.ICD9Code,
			DD.ActualChargeBilled as Total,
			DD.DMEDeposit,
			DD.Quantity,
			DD.LineTotal,
			dbo.ConcatHCPCS(PCP.PracticeCatalogProductID) AS HCPCSString -- JB 20071004 Added Column for UDF Concat of HCPCS

			
		from DispenseDetail DD
		inner join dispense D on D.DispenseID= DD.DispenseID
		inner join PracticeCatalogProduct PCP on DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID
		inner join MasterCatalogProduct MCP on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
		left join MasterCatalogSupplier MCS on MCP.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
		left join Physician P on DD.PhysicianID = P.PhysicianID
		left join Contact C on P.ContactID = C.ContactID
		where D.DispenseID=@DispenseID 
		and D.isactive=1
		and DD.isactive=1
		--and P.isactive=1
		--and C.isactive = 1
		and PCP.isactive=1
		and MCP.isactive=1 
		
	UNION ALL
	
			select 
			D.PatientCode,
			ISNULL( D.ReceiptCode, '')	AS ReceiptCode,  -- D.ReceiptCode
			D.Total as Grandtotal,
			ISNULL( C.Title, '')	AS Title,
			ISNULL( C.FirstName, '') AS FirstName,
			ISNULL( C.LastName, '')  AS LastName,
			ISNULL( C.Suffix, '')	 AS Suffix,
			PCSB.SupplierName,
			TPP.ShortName,
			TPP.Code,
			CAST(0 as bit) AS IsCustomBrace,
			TPP.Packaging,
			
			--TPP.LeftRightSide as Side,  --20091026 MWS Replaced TPP Side with User Chosen Side
			DD.LRModifier as Side,
			TPP.Size,					--20071116 Add Size -- may need to remove Packaging
			
			ISNULL(TPP.Color, '') AS Color,
			TPP.Gender,
			DD.ICD9Code,
			DD.ActualChargeBilled as Total,
			DD.DMEDeposit,
			DD.Quantity,
			DD.LineTotal,
			dbo.ConcatHCPCS(PCP.PracticeCatalogProductID) AS HCPCSString -- JB 20071004 Added Column for UDF Concat of HCPCS

			
		from DispenseDetail DD
		inner join dispense D on D.DispenseID= DD.DispenseID
		inner join PracticeCatalogProduct PCP on DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID
		inner join ThirdPartyProduct TPP on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
		left join PracticeCatalogSupplierBrand PCSB on TPP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		left join Physician P on DD.PhysicianID = P.PhysicianID
		left join Contact C on P.ContactID = C.ContactID
		where D.DispenseID=@DispenseID 
		and D.isactive=1
		and DD.isactive=1
		--and P.isactive=1
		--and C.isactive = 1
		and PCP.isactive=1
		and TPP.isactive=1 
		
	--) AS Sub
		
END


SET ANSI_NULLS ON




GO


