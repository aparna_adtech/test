USE [BregVision]
GO

/****** Object:  StoredProcedure [dbo].[usp_ShoppingCart_Update_CustomPurchaseOrderCode]    Script Date: 01/19/2011 12:37:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[usp_ShoppingCart_Update_CustomPurchaseOrderCodeByPracticeLocationID]
	  @PracticeLocationID INT
	, @CustomPurchaseOrderCode VARCHAR(75)
AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE ShoppingCart
	SET CustomPurchaseOrderCode = @CustomPurchaseOrderCode
	WHERE PracticeLocationID = @PracticeLocationID AND IsActive = 1

END

GO


