USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_PracticeLocationShippingAddress_Update]    Script Date: 04/22/2012 18:00:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocationShippingAddress_Update
   
   Description:  Update a record in the table Address
				  and update the record in the table PracticeLocationShippingAddress 
   
   AUTHOR:       John Bongiorni 8/20/2007 5:07:54 PM
   
   Modifications:  
   ------------------------------------------------------------ */  
   
ALTER PROCEDURE [dbo].[usp_PracticeLocationShippingAddress_Update]
(
	  @PracticeLocationID			 INT
--    , @AddressID                     INT
	, @AttentionOf					 VARCHAR(50)    	 
	, @AddressLine1                  VARCHAR(50)
	, @AddressLine2                  VARCHAR(50)
	, @City                          VARCHAR(50)
	, @State                         CHAR(2)
	, @ZipCode                       CHAR(5)
	, @ZipCodePlus4                  CHAR(4)
	, @ModifiedUserID                INT
	, @OracleCode					 VARCHAR(10) = NULL
    , @IsOracleCodeValid			 Bit = 0
)
AS
BEGIN
	DECLARE @AddressID					INT
	Declare @BregOracleID				VARCHAR(50)
	DECLARE @TransactionCountOnEntry    INT        -- Transaction Count before the transaction begins
		  , @Err						INT        --  holds the @@Error code returned by SQL Server

	SELECT @Err = @@ERROR
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @Err = 0
	BEGIN    
		SELECT @TransactionCountOnEntry = @@TRANCOUNT
		BEGIN TRANSACTION
	END        

	IF @Err = 0
	BEGIN
		-- Check if PracticeLocationShippingAddress already has a record for the practiceLocation	
		SELECT @AddressID = AddressID
		FROM PracticeLocationShippingAddress 
		WHERE PracticeLocationID = @PracticeLocationID
	
	END	
	
	IF (@AddressID > 0 )
	BEGIN	

		IF @Err = 0
		BEGIN	

			UPDATE dbo.Address
			SET
				  AddressLine1 = @AddressLine1
				, AddressLine2 = @AddressLine2
				, City = @City
				, State = @State
				, ZipCode = @ZipCode
				, ZipCodePlus4 = @ZipCodePlus4
				, ModifiedUserID = @ModifiedUserID
				, ModifiedDate = GETDATE()
				, IsActive = 1

			WHERE 
				AddressID = @AddressID
		
				SET @Err = @@ERROR
	     
		END

		IF @Err = 0
		BEGIN

			UPDATE dbo.PracticeLocationShippingAddress
			SET
				  AttentionOf = @AttentionOf
				, ModifiedUserID = @ModifiedUserID
				, ModifiedDate = GETDATE()
				, IsActive = 1
				, BregOracleID = @OracleCode
				, BregOracleIDInvalid = ~@IsOracleCodeValid --mws note bitwise shift (~)

			WHERE 
					PracticeLocationID = @PracticeLocationID 
				AND AddressID  = @AddressID
			
			SET @Err = @@ERROR

		END
		
	END	--  IF (@Address > 0)

	IF @@TranCount > @TransactionCountOnEntry
	BEGIN

		IF @Err = 0
			COMMIT TRANSACTION
		ELSE
			ROLLBACK TRANSACTION
			--  Add any database logging here      
	END
			
	RETURN @Err

END


