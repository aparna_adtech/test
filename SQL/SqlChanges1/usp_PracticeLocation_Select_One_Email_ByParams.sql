USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_PracticeLocation_Select_One_Email_ByParams]    Script Date: 12/27/2011 21:10:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocation_Select_One_Email_ByParams
   
   Description:  Selects the email columns of a record from table dbo.PracticeLocation
   				   and puts values into parameters
   
   AUTHOR:       John Bongiorni 9/24/2007 11:41:17 AM
   
   Modifications:  
  
   
   
   ------------------------------------------------------------ */  ---------------------SELECT PARAMS-----------

ALTER PROCEDURE [dbo].[usp_PracticeLocation_Select_One_Email_ByParams]
(
      @PracticeLocationID            INT
    , @EmailForOrderApproval         VARCHAR(100)   OUTPUT
    , @EmailForOrderConfirmation     VARCHAR(100)   OUTPUT
    , @EmailForFaxConfirmation       VARCHAR(100)   OUTPUT
    , @EmailForBillingDispense		 VARCHAR(100)   OUTPUT
    , @EmailForLowInventoryAlerts    VARCHAR(100)   OUTPUT
    , @IsEmailForLowInventoryAlertsOn BIT           OUTPUT
    , @EmailForPrintDispense		 VARCHAR(100)=NULL   OUTPUT
)
AS
BEGIN
	DECLARE @Err INT

	SELECT
		  @EmailForOrderApproval = ISNULL(EmailForOrderApproval, '')
        , @EmailForOrderConfirmation = ISNULL(EmailForOrderConfirmation, '')
        , @EmailForFaxConfirmation = ISNULL(EmailForFaxConfirmation, '')
        , @EmailForBillingDispense = ISNULL(EmailForBillingDispense, '')
        , @EmailForLowInventoryAlerts = ISNULL(EmailForLowInventoryAlerts, '')
        , @IsEmailForLowInventoryAlertsOn = ISNULL(IsEmailForLowInventoryAlertsOn, 0)
        , @EmailForPrintDispense = ISNULL(EmailForPrintDispense, '')

	FROM dbo.PracticeLocation
	WHERE 
		PracticeLocationID = @PracticeLocationID
		AND IsActive = 1

	SET @Err = @@ERROR

	RETURN @Err
End