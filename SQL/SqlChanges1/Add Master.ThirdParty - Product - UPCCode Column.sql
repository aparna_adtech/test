use [BregVision]
go

if not exists (select * from dbo.syscolumns where id = object_id(N'MasterCatalogProduct') and name=(N'UPCCode'))
alter table [MasterCatalogProduct] add [UPCCode] varchar(50) NULL
GO

if not exists (select * from dbo.syscolumns where id = object_id(N'ThirdPartyProduct') and name=(N'UPCCode'))
alter table [ThirdPartyProduct] add [UPCCode] varchar(50) NULL
GO

/*
Update MasterCatalogProduct Set UPCCode = Code
where Code is not null

Update ThirdPartyProduct Set UPCCode = Code
where Code is not null
*/
