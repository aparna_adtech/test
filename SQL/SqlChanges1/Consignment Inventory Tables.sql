
/*
Also create folders under the root of Vision
DispenseReceipts
OracleOrders 


*/
/*
USE [aspnetdb]
GO
-----------------------Add the new Role
Insert into [aspnetdb].[dbo].[aspnet_Roles]
([ApplicationId] ,[RoleId] ,[RoleName] ,[LoweredRoleName] ,[Description])
values
('49139D10-3241-4393-B620-62D2323B9281',	'4E8DB5D8-DA70-4A6B-8E6A-58E2D78685A4',	'BregConsignment',	'bregconsignment', NULL)
GO
------------------------------------
*/

USE [BregVision]
GO



/****** Object:  Table [dbo].[Device]    Script Date: 11/21/2011 12:00:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Device](
	[DeviceID] [int] IDENTITY(1,1) NOT NULL,
	[DeviceIdentifier] [varchar](255) NULL,
	[LastLocation] [varchar](255) NULL,
	[PracticeID] [int] NULL,
	[PracticeLocationID] [int] NULL,
	[CreatedUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedUserID] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
	[timestamp] [timestamp] NOT NULL,
 CONSTRAINT [PK_Device] PRIMARY KEY CLUSTERED 
(
	[DeviceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Device]  WITH CHECK ADD  CONSTRAINT [FK_Device_Practice] FOREIGN KEY([PracticeID])
REFERENCES [dbo].[Practice] ([PracticeID])
GO

ALTER TABLE [dbo].[Device] CHECK CONSTRAINT [FK_Device_Practice]
GO

ALTER TABLE [dbo].[Device]  WITH CHECK ADD  CONSTRAINT [FK_Device_PracticeLocation] FOREIGN KEY([PracticeLocationID])
REFERENCES [dbo].[PracticeLocation] ([PracticeLocationID])
GO

ALTER TABLE [dbo].[Device] CHECK CONSTRAINT [FK_Device_PracticeLocation]
GO

ALTER TABLE [dbo].[Device] ADD  DEFAULT ((1)) FOR [CreatedUserID]
GO

ALTER TABLE [dbo].[Device] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[Device] ADD  DEFAULT ((1)) FOR [IsActive]
GO

CREATE TABLE [dbo].[ConsignmentRep_Practice](
	[UserID] [uniqueidentifier] NOT NULL,
	[PracticeID] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_ConsignmentRep_Practice] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[PracticeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ConsignmentRep_Practice]  WITH CHECK ADD  CONSTRAINT [FK_ConsignmentRep_Practice_Practice] FOREIGN KEY([PracticeID])
REFERENCES [dbo].[Practice] ([PracticeID])
GO

ALTER TABLE [dbo].[ConsignmentRep_Practice] CHECK CONSTRAINT [FK_ConsignmentRep_Practice_Practice]
GO

ALTER TABLE [dbo].[ConsignmentRep_Practice] ADD  CONSTRAINT [DF_ConsignmentRep_Practice_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
/*
CREATE TABLE [dbo].[InventoryCycleType](
	[InventoryCycleTypeID] [int] IDENTITY(1,1) NOT NULL,
	[CycleName] [nvarchar](50) NOT NULL,
	[CreatedUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedUserID] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_InventoryCycleType] PRIMARY KEY CLUSTERED 
(
	[InventoryCycleTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[InventoryCycleType] ADD  CONSTRAINT [DF_InventoryCycleType_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[InventoryCycleType] ADD  CONSTRAINT [DF_InventoryCycleType_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO

Insert into InventoryCycleType
(
CycleName, 
CreatedUserID
) values 
(
'Standard Inventory',
1)
GO

Insert into InventoryCycleType
(
CycleName, 
CreatedUserID
) values 
(
'Consignment Inventory',
1)
GO

------------------------------------


CREATE TABLE [dbo].[InventoryCycleTypePermission](
	[InventoryCycleTypeID] [int] NOT NULL,
	[Role] [nvarchar](50) NOT NULL,
	[CreatedUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedUserID] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_InventoryCycleTypePermission] PRIMARY KEY CLUSTERED 
(
	[InventoryCycleTypeID] ASC,
	[Role] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[InventoryCycleTypePermission]  WITH CHECK ADD  CONSTRAINT [FK_InventoryCycleTypePermission_InventoryCycleType] FOREIGN KEY([InventoryCycleTypeID])
REFERENCES [dbo].[InventoryCycleType] ([InventoryCycleTypeID])
GO

ALTER TABLE [dbo].[InventoryCycleTypePermission] CHECK CONSTRAINT [FK_InventoryCycleTypePermission_InventoryCycleType]
GO

ALTER TABLE [dbo].[InventoryCycleTypePermission] ADD  CONSTRAINT [DF_InventoryCycleTypePermission_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[InventoryCycleTypePermission] ADD  CONSTRAINT [DF_InventoryCycleTypePermission_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO



Insert into InventoryCycleTypePermission
(
InventoryCycleTypeID,
[Role],
CreatedUserID
)
Values
(
	(Select top 1 InventoryCycleTypeID from InventoryCycleType where CycleName = 'Standard Inventory'),
	'PracticeLocationUser',
	1
)
Go

Insert into InventoryCycleTypePermission
(
InventoryCycleTypeID,
[Role],
CreatedUserID
)
Values
(
	(Select top 1 InventoryCycleTypeID from InventoryCycleType where CycleName = 'Standard Inventory'),
	'PracticeAdmin',
	1
)
Go

Insert into InventoryCycleTypePermission
(
InventoryCycleTypeID,
[Role],
CreatedUserID
)
Values
(
	(Select top 1 InventoryCycleTypeID from InventoryCycleType where CycleName = 'Standard Inventory'),
	'BregAdmin',
	1
)
Go

--------consignment

Insert into InventoryCycleTypePermission
(
InventoryCycleTypeID,
[Role],
CreatedUserID
)
Values
(
	(Select top 1 InventoryCycleTypeID from InventoryCycleType where CycleName = 'Consignment Inventory'),
	'BregAdmin',
	1
)
Go

Insert into InventoryCycleTypePermission
(
InventoryCycleTypeID,
[Role],
CreatedUserID
)
Values
(
	(Select top 1 InventoryCycleTypeID from InventoryCycleType where CycleName = 'Consignment Inventory'),
	'BregConsignment',
	1
)
Go
----------------------------------------------

CREATE TABLE [dbo].[InventoryCycle](
	[InventoryCycleID] [int] IDENTITY(1,1) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[PracticeLocationID] [int] NOT NULL,
	[InventoryCycleTypeID] [int] NOT NULL,
	[CreatedUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedUserID] [int] NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_InventoryCycle] PRIMARY KEY CLUSTERED 
(
	[InventoryCycleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[InventoryCycle]  WITH CHECK ADD  CONSTRAINT [FK_InventoryCycle_InventoryCycleType] FOREIGN KEY([InventoryCycleTypeID])
REFERENCES [dbo].[InventoryCycleType] ([InventoryCycleTypeID])
GO

ALTER TABLE [dbo].[InventoryCycle] CHECK CONSTRAINT [FK_InventoryCycle_InventoryCycleType]
GO

ALTER TABLE [dbo].[InventoryCycle]  WITH CHECK ADD  CONSTRAINT [FK_InventoryCycle_PracticeLocation] FOREIGN KEY([PracticeLocationID])
REFERENCES [dbo].[PracticeLocation] ([PracticeLocationID])
GO

ALTER TABLE [dbo].[InventoryCycle] CHECK CONSTRAINT [FK_InventoryCycle_PracticeLocation]
GO

ALTER TABLE [dbo].[InventoryCycle] ADD  CONSTRAINT [DF_InventoryCycle_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[InventoryCycle] ADD  CONSTRAINT [DF_InventoryCycle_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO

-----------InventoryItemCount-------------------------

CREATE TABLE [dbo].[InventoryItemCount](
	[InventoryItemCountID] [int] IDENTITY(1,1) NOT NULL,
	[InventoryCycleID] [int] NOT NULL,
	[ProductInventoryID] [int] NOT NULL,
	[Count] [int] NOT NULL,
	[CountDate] [datetime] NOT NULL,
	[DeviceIdentifier] [varchar](255) NOT NULL,
	[UserID] [int] NOT NULL,
	[CreatedUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedUserID] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
	[MembershipUserID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_InventoryItemCount] PRIMARY KEY CLUSTERED 
(
	[InventoryItemCountID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[InventoryItemCount]  WITH CHECK ADD  CONSTRAINT [FK_InventoryItemCount_InventoryItemCount] FOREIGN KEY([InventoryCycleID])
REFERENCES [dbo].[InventoryCycle] ([InventoryCycleID])
GO

ALTER TABLE [dbo].[InventoryItemCount] CHECK CONSTRAINT [FK_InventoryItemCount_InventoryItemCount]
GO

ALTER TABLE [dbo].[InventoryItemCount]  WITH CHECK ADD  CONSTRAINT [FK_InventoryItemCount_ProductInventory] FOREIGN KEY([ProductInventoryID])
REFERENCES [dbo].[ProductInventory] ([ProductInventoryID])
GO

ALTER TABLE [dbo].[InventoryItemCount] CHECK CONSTRAINT [FK_InventoryItemCount_ProductInventory]
GO

ALTER TABLE [dbo].[InventoryItemCount] ADD  CONSTRAINT [DF_InventoryItemCount_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[InventoryItemCount] ADD  CONSTRAINT [DF_InventoryItemCount_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO


--------------------------------------------------

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT

----------------------------------------------------


BEGIN TRANSACTION
GO
ALTER TABLE dbo.[ProductInventory_QuantityOnHandPhysical] ADD
	InventoryCycleID int NOT NULL 
GO

ALTER TABLE [dbo].[ProductInventory_QuantityOnHandPhysical]  WITH CHECK ADD  CONSTRAINT [FK_ProductInventory_QuantityOnHandPhysical_InventoryCycle] FOREIGN KEY([InventoryCycleID])
REFERENCES [dbo].[InventoryCycle] ([InventoryCycleID])
GO

ALTER TABLE [dbo].[ProductInventory_QuantityOnHandPhysical] CHECK CONSTRAINT [FK_ProductInventory_QuantityOnHandPhysical_InventoryCycle]
GO


ALTER TABLE dbo.[ProductInventory_QuantityOnHandPhysical] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

-----------------------------------------------------------



BEGIN TRANSACTION
GO
ALTER TABLE dbo.[ProductInventory_ResetQuantityOnHandPerSystem] ADD
	InventoryCycleID int NOT NULL 
GO

ALTER TABLE [dbo].[ProductInventory_ResetQuantityOnHandPerSystem]  WITH CHECK ADD  CONSTRAINT [FK_ProductInventory_ResetQuantityOnHandPerSystem_InventoryCycle] FOREIGN KEY([InventoryCycleID])
REFERENCES [dbo].[InventoryCycle] ([InventoryCycleID])
GO

ALTER TABLE [dbo].[ProductInventory_ResetQuantityOnHandPerSystem] CHECK CONSTRAINT [FK_ProductInventory_ResetQuantityOnHandPerSystem_InventoryCycle]
GO


ALTER TABLE dbo.[ProductInventory_ResetQuantityOnHandPerSystem] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

-----------------------------------------------------------

BEGIN TRANSACTION
GO
ALTER TABLE dbo.ProductInventory ADD
	ConsignmentQuantity int NOT NULL Default (0)
GO
	
ALTER TABLE dbo.ProductInventory ADD
	IsConsignment bit NOT NULL Default (0)
GO
ALTER TABLE dbo.ProductInventory SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.ShoppingCart ADD
	IsConsignment bit NOT NULL Default (0)
	
ALTER TABLE dbo.BregVisionOrder ADD
	IsConsignment bit NOT NULL Default (0)
	
	
COMMIT




/****** Object:  Table [dbo].[DispenseSignature]    Script Date: 09/13/2011 01:26:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DispenseSignature](
	[DispenseSignatureID] [int] IDENTITY(1,1) NOT NULL,
	[PhysicianSigned] [bit] NULL,
	[PhysicianSignedDate] [datetime] NULL,
	[PatientSignature] [image] NULL,
	[CreatedUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedUserID] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
	[timestamp] [timestamp] NOT NULL,
 CONSTRAINT [PK_DispenseSignature] PRIMARY KEY CLUSTERED 
(
	[DispenseSignatureID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


ALTER TABLE [dbo].[DispenseSignature] ADD  DEFAULT ((1)) FOR [CreatedUserID]
GO

ALTER TABLE [dbo].[DispenseSignature] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[DispenseSignature] ADD  DEFAULT ((1)) FOR [IsActive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ABNReason](
	[ABNReasonID] [int] IDENTITY(1,1) NOT NULL,
	[ABNReasonText] [varchar](max) NOT NULL,
	[SortOrder] [int] NOT NULL,
	[CreatedUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedUserID] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
	[timestamp] [timestamp] NOT NULL,
 CONSTRAINT [PK_ABNReason] PRIMARY KEY CLUSTERED 
(
	[ABNReasonID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ABNReason] ADD  DEFAULT ((1)) FOR [CreatedUserID]
GO

ALTER TABLE [dbo].[ABNReason] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[ABNReason] ADD  DEFAULT ((1)) FOR [IsActive]
GO

INSERT INTO ABNReason
	(ABNReasonText, SortOrder, CreatedUserID, CreatedDate, IsActive)
VALUES				
	('User entry (blank for user to define)', 1, 1, GETDATE(), 1)
	
INSERT INTO ABNReason
	(ABNReasonText, SortOrder, CreatedUserID, CreatedDate, IsActive)
VALUES				
	('Non-covered item', 2, 1, GETDATE(), 1)
	
INSERT INTO ABNReason
	(ABNReasonText, SortOrder, CreatedUserID, CreatedDate, IsActive)
VALUES				
	('Not medically necessary per LCD/Policy Article', 3, 1, GETDATE(), 1)
	
INSERT INTO ABNReason
	(ABNReasonText, SortOrder, CreatedUserID, CreatedDate, IsActive)
VALUES				
	('Reasonable useful timeframe limitation', 4, 1, GETDATE(), 1)
------------------------------

ALTER TABLE dbo.DispenseQueue ADD
	ABNReason varchar(500) NULL
GO

ALTER TABLE dbo.DispenseQueue ADD
	DispenseSignatureID int NULL
GO

ALTER TABLE dbo.DispenseQueue ADD
	PatientEmail varchar(50) NULL
GO

ALTER TABLE dbo.DispenseQueue ADD
	IsRental bit NOT NULL Default (0)
GO

*/

ALTER TABLE dbo.DispenseQueue ADD
	DeclineReason varchar(50) NULL
GO

ALTER TABLE dbo.DispenseQueue ADD
	MedicareOption int NOT NULL DEFAULT (0)
GO

/*
ALTER TABLE [dbo].[DispenseQueue]  WITH CHECK ADD  CONSTRAINT [FK_DispenseQueue_DispenseSignature] FOREIGN KEY([DispenseSignatureID])
REFERENCES [dbo].[DispenseSignature] ([DispenseSignatureID])
GO

ALTER TABLE [dbo].[DispenseQueue] CHECK CONSTRAINT [FK_DispenseQueue_DispenseSignature]
GO

-- live db deployed to here

*/

ALTER TABLE dbo.Dispense ADD
	PatientEmail varchar(50) NULL
GO

ALTER TABLE dbo.PracticeCatalogSupplierBrand ADD
	SupplierWarrantyID int NULL
GO

CREATE TABLE [dbo].[SupplierWarranty](
	[SupplierWarrantyID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierName] [nchar](50) NOT NULL,
	[ProductWarranty] [varchar](2000) NOT NULL,
 CONSTRAINT [PK_SupplierWarranty] PRIMARY KEY CLUSTERED 
(
	[SupplierWarrantyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PracticeCatalogSupplierBrand]  WITH NOCHECK ADD  CONSTRAINT [FK_PracticeCatalogSupplierBrand_SupplierWarranty] FOREIGN KEY([SupplierWarrantyID])
REFERENCES [dbo].[SupplierWarranty] ([SupplierWarrantyID])
GO

ALTER TABLE [dbo].[PracticeCatalogSupplierBrand] NOCHECK CONSTRAINT [FK_PracticeCatalogSupplierBrand_SupplierWarranty]
GO

Update PracticeCatalogSupplierBrand 
Set SupplierWarrantyID =1
WHERE     (SupplierName LIKE '%Breg%')
GO

--breg oracle
ALTER TABLE dbo.ShippingType ADD
	BregServiceLevel varchar(100) NULL
GO

ALTER TABLE dbo.PracticeLocationShippingAddress ADD
	BregOracleID varchar(50) NULL
GO

ALTER TABLE dbo.PracticeLocationBillingAddress ADD
	BregOracleID varchar(50) NULL
GO

ALTER TABLE dbo.PracticeLocationShippingAddress ADD
	BregOracleIDInvalid bit default(0)
GO

ALTER TABLE dbo.PracticeLocationBillingAddress ADD
	BregOracleIDInvalid bit default(0)
GO

