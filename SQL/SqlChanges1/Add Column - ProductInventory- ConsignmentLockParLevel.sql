USE [BregVision]


Alter Table [ProductInventory] Add ConsignmentLockParLevel bit not null default 0
GO

Alter Table [ProductInventory] Add IsBuyout bit not null default 0
GO

Alter Table [ProductInventory] Add IsRMA bit not null default 0
GO

Alter Table [ProductInventory] Add BuyoutRMAInventoryCycleID int null
GO

Insert into InventoryCycleTypePermission
(
InventoryCycleTypeID,
[Role],
CreatedUserID
)
Values
(
	(Select top 1 InventoryCycleTypeID from InventoryCycleType where CycleName = 'Consignment Inventory'),
	'PracticeAdmin',
	1
)
Go