USE [BregVisionEMR]
GO

/****** Object:  Table [dbo].[outbound_file_item_queue_log]    Script Date: 4/11/2014 2:57:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[outbound_file_item_queue_log](
	[outbound_file_item_queue_log_id] [uniqueidentifier] NOT NULL,
	[outbound_file_item_queue_id] [uniqueidentifier] NOT NULL,
	[log_datetime] [datetime] NOT NULL,
	[log_message] [ntext] NOT NULL,
 CONSTRAINT [PK_outbound_file_item_queue_log] PRIMARY KEY NONCLUSTERED 
(
	[outbound_file_item_queue_log_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[outbound_file_item_queue_log] ADD  CONSTRAINT [DF_outbound_file_item_queue_log_outbound_file_item_queue_log_id]  DEFAULT (newid()) FOR [outbound_file_item_queue_log_id]
GO

ALTER TABLE [dbo].[outbound_file_item_queue_log] ADD  CONSTRAINT [DF_outbound_file_item_queue_log_log_datetime]  DEFAULT (getdate()) FOR [log_datetime]
GO

ALTER TABLE [dbo].[outbound_file_item_queue_log]  WITH CHECK ADD  CONSTRAINT [FK_outbound_file_item_queue_log_outbound_file_item_queue] FOREIGN KEY([outbound_file_item_queue_id])
REFERENCES [dbo].[outbound_file_item_queue] ([outbound_file_item_queue_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[outbound_file_item_queue_log] CHECK CONSTRAINT [FK_outbound_file_item_queue_log_outbound_file_item_queue]
GO


