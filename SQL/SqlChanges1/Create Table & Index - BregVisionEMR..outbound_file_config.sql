USE [BregVisionEMR]
GO

/****** Object:  Table [dbo].[outbound_file_config]    Script Date: 5/28/2014 3:22:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[outbound_file_config](
	[outbound_file_config_id] [int] IDENTITY(1,1) NOT NULL,
	[practice_id] [int] NOT NULL,
	[file_dropoff_path] [nvarchar](255) NOT NULL,
	[is_autoqueue_hl7_mdm] [bit] NOT NULL,
	[is_guid_filename_override] [bit] NOT NULL,
 CONSTRAINT [pk__outbound_file_config] PRIMARY KEY CLUSTERED 
(
	[outbound_file_config_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[outbound_file_config] ADD  CONSTRAINT [DF_outbound_file_config_is_autoqueue_hl7_mdm]  DEFAULT ((0)) FOR [is_autoqueue_hl7_mdm]
GO

ALTER TABLE [dbo].[outbound_file_config] ADD  CONSTRAINT [DF_outbound_file_config_is_guid_filename_override]  DEFAULT ((0)) FOR [is_guid_filename_override]
GO

/****** Object:  Index [ix__outbound_file_config__practice_id]    Script Date: 5/28/2014 3:24:28 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ix__outbound_file_config__practice_id] ON [dbo].[outbound_file_config]
(
	[practice_id] ASC
)
INCLUDE ( 	[file_dropoff_path]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

