USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_Contact_Update]    Script Date: 7/21/2015 10:38:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Contact_Update
   
   Description:  Updates a record in the table Contact
   
   AUTHOR:       John Bongiorni 7/14/2007 8:20:29 PM
   
   Modifications:  
   ------------------------------------------------------------ */  

ALTER PROCEDURE [dbo].[usp_Contact_Update]
(
	  @ContactID                     INT
	, @Title                         VARCHAR(50)
	, @Salutation                    VARCHAR(10)
	, @FirstName                     VARCHAR(50)
	, @MiddleName                    VARCHAR(50)
	, @LastName                      VARCHAR(50)
	, @Suffix                        VARCHAR(10)
	, @EmailAddress                  VARCHAR(100)
	, @PhoneWork                     VARCHAR(25)
	, @PhoneWorkExtension            VARCHAR(10)
	, @PhoneCell                     VARCHAR(25)
	, @PhoneHome                     VARCHAR(25)
	, @Fax                           VARCHAR(25)
	, @ModifiedUserID                INT
	, @IsSpecificContact			 BIT
)
AS
BEGIN
	DECLARE @Err INT

	UPDATE dbo.Contact
	SET
		  Title = @Title
		, Salutation = @Salutation
		, FirstName = @FirstName
		, MiddleName = @MiddleName
		, LastName = @LastName
		, Suffix = @Suffix
		, EmailAddress = @EmailAddress
		, PhoneWork = @PhoneWork
		, PhoneWorkExtension = @PhoneWorkExtension
		, PhoneCell = @PhoneCell
		, PhoneHome = @PhoneHome
		, Fax = @Fax
		, IsSpecificContact = @IsSpecificContact
		, ModifiedUserID = @ModifiedUserID
		, ModifiedDate = GETDATE()

	WHERE 
		ContactID = @ContactID

	SET @Err = @@ERROR

	RETURN @Err
End
