CREATE TABLE [dbo].[BregVisionMonthlyFeeLevel](
	[BregVisionMonthlyFeeLevelID] [int] IDENTITY(1,1) NOT NULL,
	[LevelName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_BregVisionMonthlyFeeLevel] PRIMARY KEY CLUSTERED 
(
	[BregVisionMonthlyFeeLevelID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE dbo.Practice_MonthlyFeeLevel
	(
	PracticeID int NOT NULL,
	BregVisionMonthlyFeeLevelID int NOT NULL,
	StartDate datetime NOT NULL,
	Discount decimal(10,4) DEFAULT ((0)) NOT NULL,
	CreatedUserID int NOT NULL,
	CreatedDate datetime NOT NULL,
	ModifiedUserID int NULL,
	ModifiedDate datetime NULL,
	IsActive bit NOT NULL
	)  ON [PRIMARY]
GO

ALTER TABLE dbo.Practice_MonthlyFeeLevel ADD CONSTRAINT
	PK_Practice_MonthlyFeeLevel PRIMARY KEY CLUSTERED 
	(
	PracticeID,
	BregVisionMonthlyFeeLevelID,
	StartDate
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO

ALTER TABLE dbo.Practice_MonthlyFeeLevel ADD CONSTRAINT
	FK_Practice_MonthlyFeeLevel_Practice FOREIGN KEY
	(
	PracticeID
	) REFERENCES dbo.Practice
	(
	PracticeID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Practice_MonthlyFeeLevel ADD CONSTRAINT
	FK_Practice_MonthlyFeeLevel_BregVisionMonthlyFeeLevel FOREIGN KEY
	(
	BregVisionMonthlyFeeLevelID
	) REFERENCES dbo.BregVisionMonthlyFeeLevel
	(
	BregVisionMonthlyFeeLevelID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO


if not exists (select * from dbo.syscolumns where id = object_id(N'BregVisionMonthlyFee') and name=(N'BregVisionMonthlyFeeID'))
ALTER TABLE [dbo].[BregVisionMonthlyFee] ADD [BregVisionMonthlyFeeID] int IDENTITY(1,1) NOT NULL 
GO


/****** Object:  Index [PK_BregVisionMonthlyFee]    Script Date: 08/02/2009 12:37:53 ******/
ALTER TABLE [dbo].[BregVisionMonthlyFee] ADD  CONSTRAINT [PK_BregVisionMonthlyFee] PRIMARY KEY CLUSTERED 
(
	[BregVisionMonthlyFeeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO



if not exists (select * from dbo.syscolumns where id = object_id(N'BregVisionMonthlyFee') and name=(N'BregVisionMonthlyFeeLevelID'))
ALTER TABLE [dbo].[BregVisionMonthlyFee] ADD [BregVisionMonthlyFeeLevelID] int NULL 
GO

--Insert the initial Fee Level
Declare @BVMFLID int
Insert into [BregVisionMonthlyFeeLevel] ([LevelName]) values('Level1');
Select @BVMFLID = SCOPE_IDENTITY()

--Foreign Key BregVisionMonthlyFee to BregVisionMonthlyFeeLevel
Update BregVisionMonthlyFee Set BregVisionMonthlyFeeLevelID = @BVMFLID --or @@Identity from the previous table

--Create an initial Entry for each Practice in the Practice_MonthlyFeeLevel Table
Insert into Practice_MonthlyFeeLevel --(PracticeID, BregVisionMonthlyFeeLevelID, StartDate, CreatedDate, CreatedUserID, ModifiedDate, ModifiedUserID, IsActive)
	Select PracticeId, @BVMFLID, '1/1/2006', 0, 1, GETDATE(), 1, GETDATE(), 1 from Practice
GO

Alter Table BregVisionMonthlyFee Alter Column BregVisionMonthlyFeeLevelID int not null
Go


ALTER TABLE [dbo].[BregVisionMonthlyFee]  WITH CHECK ADD  CONSTRAINT [FK_BregVisionMonthlyFee_BregVisionMonthlyFeeLevel] FOREIGN KEY([BregVisionMonthlyFeeLevelID])
REFERENCES [dbo].[BregVisionMonthlyFeeLevel] ([BregVisionMonthlyFeeLevelID])
GO

ALTER TABLE [dbo].[BregVisionMonthlyFee] CHECK CONSTRAINT [FK_BregVisionMonthlyFee_BregVisionMonthlyFeeLevel]
GO



