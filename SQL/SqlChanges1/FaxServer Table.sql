USE [BregVision]
GO

/****** Object:  Table [dbo].[FaxServer]    Script Date: 08/23/2009 21:22:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FaxServer](
	[FaxServerID] [int] IDENTITY(1,1) NOT NULL,
	[FaxEnabled] [bit] NOT NULL,
	[Password] [varchar](50) NOT NULL,
 CONSTRAINT [PK_FaxServer] PRIMARY KEY CLUSTERED 
(
	[FaxServerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[FaxServer] ADD  CONSTRAINT [DF_FaxServer_FaxEnabled]  DEFAULT ((1)) FOR [FaxEnabled]
GO

ALTER TABLE [dbo].[FaxServer] ADD  CONSTRAINT [DF_FaxServer_Password]  DEFAULT ('#1limes') FOR [Password]
GO

insert into FaxServer(FaxEnabled, [Password]) values (1,'#1limes')
Go


