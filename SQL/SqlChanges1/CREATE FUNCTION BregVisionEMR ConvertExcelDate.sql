USE [BregVisionEMR]
GO

/****** Object:  UserDefinedFunction [dbo].[ConvertExcelDate]    Script Date: 05/14/2014 13:13:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[ConvertExcelDate](@excel_date varchar(10), @excel_time varchar(10))
RETURNS DateTime
AS
BEGIN
DECLARE @Output DateTime
	SET @Output = getDate()

	SELECT @Output =	dateadd(day,-2,convert(datetime,parse(@excel_date as decimal) + parse(@excel_time as decimal(10,10))))

	RETURN @Output
	--SELECT @Output AS output

END


GO


