USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_SearchForItemsInInventory_ByProductCode_With_ThirdPartyProducts]    Script Date: 11/21/2011 17:16:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




/* ------------------------------------------------------------
   
   
   
   AUTHOR:       Greg Ross 10/31/07
   
   Modifications:  
   ------------------------------------------------------------ */   


ALTER PROCEDURE [dbo].[usp_SearchForItemsInInventory_ByProductCode_With_ThirdPartyProducts]

	@PracticeLocationID int
	, @SearchText  varchar(50)
	, @SearchCriteria varchar(50) = 'Code'


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--   NOCOUNT ON


			SELECT 
				  SUB.ProductInventoryID	
				, SUB.PracticeCatalogProductID	
				, SUB.BrandName
				, SUB.SupplierName
				, SUB.SupplierID
--				, SUB.Category
				, SUB.Product as ProductName
				, SUB.Code

				, ISNULL(SUB.Packaging, '') AS Packaging
				, ISNULL(SUB.LeftRightSide, '') AS Side
				, ISNULL(SUB.Size, '') AS Size
				, ISNULL(SUB.Gender, '') AS Gender
				
				, SUB.QOH
				, SUB.ParLevel
				, SUB.ReorderLevel
				, SUB.CriticalLevel
				, SUB.WholesaleCost
				, SUB.IsActive
				, SUB.IsThirdPartyProduct
							
			FROM
			(
					SELECT	-- 249 active and inactive mastercatalogproducts in inventory WORKS!!!
 
							PCP.PracticeCatalogProductID,		

                            PCSB.MasterCatalogSupplierID AS SupplierID, 
                            PCSB.BrandShortName AS BrandName,   --  This column should be unneccessary
							PCSB.SupplierShortName AS SupplierName,	
								--  Only here to preserve the existing application code.
						    MCP.[Name]			AS Product,
                            
                            MCP.[Code]			AS Code,
                            MCP.[Packaging]		AS Packaging,
                            MCP.[LeftRightSide] AS LeftRightSide,
                            MCP.[Size]			AS Size,
                            MCP.[Gender]		AS Gender,
                            MCP.UPCCode			AS UPCCode,
							
							PI.ProductInventoryID,
							PI.QuantityOnHandPerSystem AS QOH,
							PI.ParLevel,
							PI.ReorderLevel,
							PI.CriticalLevel,
						
							PCP.WholesaleCost,
							PI.Isactive,
							0 AS IsThirdPartyProduct
                     
                    FROM dbo.PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
							                    
                    INNER JOIN [PracticeCatalogProduct] AS PCP WITH(NOLOCK) 
						ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
							
                    INNER JOIN [ProductInventory] AS PI  WITH(NOLOCK)
							ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
             
                    INNER JOIN [MasterCatalogProduct] AS MCP  WITH(NOLOCK)
							ON PCP.[MasterCatalogProductID] = MCP.[MasterCatalogProductID]
                                                 
                     WHERE  PI.[PracticeLocationID] = @PracticeLocationID
							AND PCP.IsThirdPartyProduct = 0  -- MasterCatalogProductsOnly
							AND MCP.IsActive = 1
							and MCP.IsDiscontinued = 0
							AND PCP.IsActive = 1
							AND PI.IsActive = 1
							AND (
									(@SearchCriteria = 'Code' and MCP.Code LIKE @SearchText)
									OR
									(
										@SearchCriteria = 'UPCCode' --AND MCP.UPCCode like @SearchText)
										AND
										(
											 (@SearchText IS NULL)
											 OR EXISTS
											 (  
												Select 
													1 
												 from 
													UPCCode upc 
												 where 
													(
														upc.MasterCatalogProductID = MCP.MasterCatalogProductID 
														and upc.Code like @SearchText 
													) 
											  )
										 )		
									)
								)

				UNION

					--  Third Party Product Info
					SELECT		-- 249 active and inactive mastercatalogproducts in inventory WORKS!!!
 
							PCP.PracticeCatalogProductID,		       
                            PCSB.ThirdPartySupplierID AS SupplierID,                        
                            PCSB.BrandShortName AS BrandName,   --  This column should be unneccessary
							PCSB.SupplierShortName AS SupplierName,
									--  Only here to preserve the existing application code.
						    TPP.[Name]			AS Product,                            
                            TPP.[Code]			AS Code,
                            TPP.[Packaging]		AS Packaging,
                            TPP.[LeftRightSide] AS LeftRightSide,
                            TPP.[Size]			AS Size,
                            TPP.[Gender]		AS Gender,
                            TPP.UPCCode			AS UPCCode,
							
							PI.ProductInventoryID,
							PI.QuantityOnHandPerSystem AS QOH,
							PI.ParLevel,
							PI.ReorderLevel,
							PI.CriticalLevel,
						
							PCP.WholesaleCost,						
							PI.Isactive,							
							1 AS IsThirdPartyProduct 
                     
                    FROM dbo.PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
                    
                    INNER JOIN [PracticeCatalogProduct] AS PCP WITH(NOLOCK) 
						ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
							
                    INNER JOIN [ProductInventory] AS PI  WITH(NOLOCK)
							ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
             
                    INNER JOIN [ThirdPartyProduct] AS TPP  WITH(NOLOCK)
							ON PCP.[ThirdPartyProductID] = TPP.[ThirdPartyProductID]
                                                 
                     WHERE  PI.[PracticeLocationID] = @PracticeLocationID
							AND PCP.IsThirdPartyProduct = 1		-- ThirdPartyProductsOnly 
							AND TPP.IsActive = 1
							AND TPP.IsDiscontinued = 0
							AND PCP.IsActive = 1
							AND PI.IsActive = 1
							AND (
									(@SearchCriteria = 'Code' AND TPP.Code LIKE @SearchText)
									OR
									(
										@SearchCriteria = 'UPCCode' --AND TPP.UPCCode like @SearchText)									
										AND
										(
											 (@SearchText IS NULL)
											 OR EXISTS
											 (  
												Select 
													1 
												 from 
													UPCCode upc 
												 where 
													(
														upc.ThirdPartyProductID = TPP.ThirdPartyProductID 
														and upc.Code like @SearchText 
													) 
											  )
										 )	
									)
								)	
				) AS Sub
			
			ORDER BY Sub.IsThirdPartyProduct
			
END

