USE [BregVision]
GO

/****** Object:  StoredProcedure [dbo].[usp_PracticeLocationBillingAddressExists]    Script Date: 06-12-2016 07:37:59 ******/
DROP PROCEDURE [dbo].[usp_PracticeLocationBillingAddressExists]
GO

/****** Object:  StoredProcedure [dbo].[usp_PracticeLocationBillingAddressExists]    Script Date: 06-12-2016 07:37:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Aditya M S K
-- Create date: Dec 5th 2016
-- Description:	This stored procedure will be utilised to check if the given practice location has a billing address or not
-- Modified Date: Mar 17 2017
-- Replacing the RETURN with SELECT as RETURN is returning null at times
-- =============================================
CREATE PROCEDURE [dbo].[usp_PracticeLocationBillingAddressExists] 
	
	@PracticeLocationId INT
		
	AS
BEGIN
	
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT TOP 1 AddressId FROM PracticeLocationBillingAddress WHERE PracticeLocationID = @PracticeLocationId)
	SELECT CAST(1 AS bit)
	ELSE
	SELECT CAST(0 AS bit)
	
END

GO


