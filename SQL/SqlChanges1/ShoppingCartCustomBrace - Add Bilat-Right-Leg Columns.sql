/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ShoppingCartCustomBrace ADD
	Instability_BilatRightLeg varchar(50) NULL,
	X2K_Counterforce_BilatRightLeg varchar(50) NULL,
	X2K_Counterforce_Degrees_BilatRightLeg varchar(50) NULL,
	ThighCircumference_BilatRightLeg varchar(50) NULL,
	CalfCircumference_BilatRightLeg varchar(50) NULL,
	KneeOffset_BilatRightLeg varchar(50) NULL,
	KneeWidth_BilatRightLeg varchar(50) NULL,
	Extension_BilatRightLeg varchar(50) NULL,
	Flexion_BilatRightLeg varchar(50) NULL
GO
ALTER TABLE dbo.ShoppingCartCustomBrace SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
