USE [BregVision]
GO

GO
SET IDENTITY_INSERT [dbo].[ForumGroup] ON
INSERT [dbo].[ForumGroup] ([Id], [Name], [ParentId], [Created], [SortOrder], [IsActive]) VALUES (1, N'Top', NULL, CAST(0x00009D9C00FCBC71 AS DateTime), 0, 0)
INSERT [dbo].[ForumGroup] ([Id], [Name], [ParentId], [Created], [SortOrder], [IsActive]) VALUES (2, N'Group 1', 1, CAST(0x00009D9C00FCD421 AS DateTime), 0, 1)
INSERT [dbo].[ForumGroup] ([Id], [Name], [ParentId], [Created], [SortOrder], [IsActive]) VALUES (3, N'Group 2', 1, CAST(0x00009D9C00FCDFCC AS DateTime), 1, 1)
INSERT [dbo].[ForumGroup] ([Id], [Name], [ParentId], [Created], [SortOrder], [IsActive]) VALUES (4, N'Group 3', 1, CAST(0x00009D9C00FCF51C AS DateTime), 2, 1)
INSERT [dbo].[ForumGroup] ([Id], [Name], [ParentId], [Created], [SortOrder], [IsActive]) VALUES (5, N'Group 4', 1, CAST(0x00009D9C00FD01E4 AS DateTime), 3, 1)
SET IDENTITY_INSERT [dbo].[ForumGroup] OFF

GO
SET IDENTITY_INSERT [dbo].[ForumArticle] ON
INSERT [dbo].[ForumArticle] ([Id], [ParentId], [Subject], [Body], [ForumGroupId], [MasterCatalogProductID], [UserId], [Created], [IsActive], [IsSticky]) VALUES (1, NULL, N'Article 1', N'Article 1', 2, NULL, 23, CAST(0x00009DA400B048D4 AS DateTime), 1, 0)
INSERT [dbo].[ForumArticle] ([Id], [ParentId], [Subject], [Body], [ForumGroupId], [MasterCatalogProductID], [UserId], [Created], [IsActive], [IsSticky]) VALUES (2, 1, N'Reply 1 Article 1', N'Reply 1 Article 1', 2, NULL, 23, CAST(0x00009DA400B048D7 AS DateTime), 1, 0)
SET IDENTITY_INSERT [dbo].[ForumArticle] OFF
