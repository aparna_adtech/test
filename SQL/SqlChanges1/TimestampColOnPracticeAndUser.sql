use [BregVision]
go

if not exists (select * from dbo.syscolumns where id = object_id(N'User') and name=(N'timestamp'))
alter table [User] add [timestamp] [timestamp] NOT NULL
GO

if not exists (select * from dbo.syscolumns where id = object_id(N'Practice') and name=(N'timestamp'))
alter table [Practice] add [timestamp] [timestamp] NOT NULL
GO