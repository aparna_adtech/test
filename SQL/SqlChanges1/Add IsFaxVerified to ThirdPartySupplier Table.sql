use [BregVision]
Go

if not exists (select * from dbo.syscolumns where id = object_id(N'TableName') and name=(N'IsActive'))
alter table [ThirdPartySupplier] add IsFaxVerified [bit] DEFAULT 0 NOT NULL
GO

Update [ThirdPartySupplier] set IsFaxVerified = 1