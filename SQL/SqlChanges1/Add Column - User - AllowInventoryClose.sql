USE [BregVision]

Alter Table [User] Add AllowInventoryClose bit not null default 0

Alter Table [InventoryCycle] Add Title Varchar(255) null