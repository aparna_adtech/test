USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAllProductDatabyPracticeOrLocation]    Script Date: 08/23/2010 14:27:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/* ------------------------------------------------------------
   PROCEDURE:    [dbo].[usp_GetAllProductDatabyLocation]
   
   Description:  Gets all Child data for Inventory on Inventory.aspx page.
		
	Dependencies:  	udf_GetSuggestedReorder_Given_PracticeLocationID	 
   
   AUTHOR:       John Bongiorni 10/18/2007 5:45:17 PM
   
   Modifications:  Mike Sneen 6/15/2010 reworked for global search
   Exec [usp_GetAllProductDatabyPracticeOrLocation] 3, 1, 'Code', '10085'
   Exec [usp_GetAllProductDatabyPracticeOrLocation] 3, 0, 'Browse', ''
   ------------------------------------------------------------ */ 


ALTER PROCEDURE [dbo].[usp_GetAllProductDatabyPracticeOrLocation]   
		@PracticeLocationID			INT
	  , @PracticeID					INT = 0			--  For backward compatibility with application code, @PracticeID is never used.
	  , @SearchCriteria varchar(50)
	  , @SearchText  varchar(50)
AS 
    BEGIN
    
    	/*These variables are used in conjunction with the coalesce statements in the where clause.
	each of these variables will be null except one is @SearchCriteria matches.*/
	Declare @ProductName varchar(50)			--NULL   These default to null
	Declare @ProductCode varchar(50)			--NULL   These default to null
	Declare @ProductBrand varchar(50)			--NULL   These default to null
	Declare @ProductHCPCs varchar(50)			--NULL   These default to null

	if @SearchCriteria='Name'					-- ProductName
		Set @ProductName = '%' + @SearchText + '%'		--put the percent sign on both sides of the search text for product name
	else if @SearchCriteria='Code'
		Set @ProductCode =  @SearchText + '%'	
	else if @SearchCriteria='Brand'
		Set @ProductBrand = @SearchText + '%'	
	else if @SearchCriteria='HCPCs'
		Set @ProductHCPCs = @SearchText + '%'
    
    
    

	-- Table Variable used as a working table
        DECLARE @WorkingTable TABLE
            (
              Category VARCHAR(50)
            , SubCategory VARCHAR(50)
            , Product VARCHAR(100)
            , Code VARCHAR(50)
            , Packaging VARCHAR(50)
            , LeftRightSide VARCHAR(50)
            , Size VARCHAR(50)
            , Gender VARCHAR(50)
            , Color VARCHAR(50)
            , SupplierID INT
            , PracticeCatalogProductID INT

            , QuantityOnHand INT DEFAULT 0
            , QuantityOnOrder INT DEFAULT 0
            , PARLevel INT DEFAULT 0
            , ReorderLevel INT DEFAULT 0
            , CriticalLevel INT DEFAULT 0
            , SuggestedReorderLevel INT DEFAULT 0
            , IsThirdPartyProduct BIT
			, StockingUnits int default 1
			, PracticeLocationName varchar(50)
            )


         --     2.a  Create a list of all locations to loop through
         Declare @ProductLocations TABLE
         (
			PracticeLocationID int,
			PracticeLocationName varchar(50),
			fUsed bit default 0
         )
         insert into @ProductLocations(PracticeLocationID, PracticeLocationName, fUsed)
         (
			select pl.PracticeLocationID, pl.Name, 0 from PracticeLocation pl where (@PracticeID > 0 AND pl.PracticeID = @PracticeID) OR (@PracticeID < 1 AND pl.PracticeLocationID=@PracticeLocationID)
         )
         
         --Select * from @ProductLocations -- For debug
         
         Declare @CurrentPracticeLocationID int
         Declare @CurrentPracticeLocationName Varchar(50)
         
         Select TOP 1 @CurrentPracticeLocationID = PracticeLocationID from @ProductLocations where fUsed = 0
         Select TOP 1 @CurrentPracticeLocationName = PracticeLocationName from @ProductLocations where fUsed = 0
         
         WHILE @@ROWCOUNT <> 0 AND @CurrentPracticeLocationID IS NOT NULL
         BEGIN
         
         		-- 1.  Insert the PracticeCatalogProductID for the location's products that are in inventory and active.

		

			--  3.  Get Product Info for PracticeCatalogProduct in inventory. 
		--Select * from @WorkingTable -- for debug
	
		INSERT INTO
            @WorkingTable
            (
				  PracticeCatalogProductID 
				, StockingUnits 
        
				, Category 
				, SubCategory 
				, Product
				, Code
				, Packaging
				, LeftRightSide
				, Size
				, Gender
				, Color
				, SupplierID
				, IsThirdPartyProduct
		  		  
  				,QuantityOnHand 
				, QuantityOnOrder

				, ParLevel
				, ReorderLevel
				, CriticalLevel
				, PracticeLocationName
				, SuggestedReorderLevel
            )
        Select 
			CheckIn.PracticeCatalogProductID
		  , CheckIn.StockingUnits
        
          , CheckIn.Category
          , CheckIn.SubCategory
          , CheckIn.Product
          , CheckIn.Code
          , CheckIn.Packaging
          , CheckIn.LeftRightSide
          , CheckIn.Size
          , CheckIn.Gender
          , CheckIn.Color
          , CheckIn.SupplierID
          , CheckIn.IsThirdPartyProduct
  		  
  		  , Levels.QuantityOnHand
		  , Levels.QuantityOnOrder

		  , Levels.ParLevel
		  , Levels.ReorderLevel
		  , Levels.CriticalLevel	
		  , @CurrentPracticeLocationName
		  , (Levels.SuggestedReorderLevel	/ CheckIn.StockingUnits)
        FROM
            (
                         SELECT
                            Sub.PracticeCatalogProductID
                          , Sub.CATEGORY
                          , Sub.SUBCATEGORY
                          , Sub.PRODUCT
                          , Sub.Code
                          , Sub.Packaging
                          , Sub.LeftRightSide
                          , Sub.Size
                          , Sub.Gender
						  , Sub.Color
                          , Sub.SupplierID
                          , Sub.IsThirdPartyProduct
                          , Sub.StockingUnits
                         FROM
                            (
                              SELECT
                                PCP.[PracticeCatalogProductID]
                              , mCC.NAME AS cATEGORY
                              , mcsc.[Name] AS sUBcATEGORY
                              , MCP.[Name] AS pRODUCT
                              , MCP.[Code] AS Code
                              , MCP.[Packaging] AS Packaging
                              , MCP.[LeftRightSide] AS LeftRightSide
                              , mcp.[Size] AS Size
                              , mcp.[Gender] AS Gender
                              , mcp.[Color] AS Color
                              , PCP.PracticeCatalogSupplierBrandID AS SupplierID
                              , PCP.IsThirdPartyProduct
                              , PCP.StockingUnits
                              FROM
                                [MasterCatalogSupplier] AS MCS WITH ( NOLOCK )
                                INNER JOIN [MasterCatalogCategory] AS MCC WITH ( NOLOCK )
                                                                               ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
                                INNER JOIN [MasterCatalogSubCategory] AS MCSC
                                WITH ( NOLOCK )
                                     ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
                                INNER JOIN [MasterCatalogProduct] AS MCP WITH ( NOLOCK )
                                                                              ---ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
                                                                              ON MCP.MasterCatalogSubCategoryID = MCSC.MasterCatalogSubCategoryID
                                INNER JOIN [PracticeCatalogProduct] AS PCP
                                WITH ( NOLOCK )
                                     ON MCP.[MasterCatalogProductID] = PCP.[MasterCatalogProductID]
                                INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
                                WITH ( NOLOCK )
                                     ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
                                INNER JOIN [ProductInventory] AS PI WITH ( NOLOCK )
                                                                         ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
                              WHERE
								MCP.IsActive = 1
								AND PCP.IsActive = 1
								AND PI.IsActive = 1
                                AND PI.[PracticeLocationID] = @CurrentPracticeLocationID --@PracticeLocationID
                                AND PCP.IsThirdPartyProduct = 0
								--and (MCP.Name like @SearchText or MCP.ShortName like  @SearchText)
								AND
								(
									--ProductName || ShortName
									COALESCE(MCP.Name, '') like COALESCE(@ProductName, MCP.Name, '')
									OR
									COALESCE(MCP.ShortName, '') like COALESCE(@ProductName, MCP.ShortName, '')
								)
	                   			AND
								(
									COALESCE(MCP.Code, '') like COALESCE(@ProductCode , MCP.Code, '')
								)
								AND
								(
									COALESCE(PCSB.BrandName, '') like COALESCE(@ProductBrand, PCSB.BrandName, '')
									OR
									COALESCE(PCSB.BrandShortName, '') like COALESCE(@ProductBrand, PCSB.BrandShortName, '')
								)
								AND 
								(   /* use this type of statement for Child tables of the target to avoid duplicate rows.
										If we use a join for this, we would return duplicate rows, so we just check the existence
										of one record that matches the search parameters in the child table
										*/
									(
										 (@ProductHCPCs IS NULL)
										 OR EXISTS
										 (  
											Select 
												1 
											 from 
												ProductHCPCS PHCPCS 
											 where 
												PHCPCS.PracticeCatalogProductID = PI.PracticeCatalogProductID 
												and PHCPCS.HCPCS like @ProductHCPCs  
										  )
									 )
								)								
								
								
                   --  20071011 JB Union the thirdpartyproducts.
                              UNION
                              SELECT
                                PCP.[PracticeCatalogProductID]
                              , 'NA' AS cATEGORY
                              , 'NA' AS sUBcATEGORY
                              , TPP.[Name] AS pRODUCT
                              , TPP.[Code] AS Code
                              , TPP.[Packaging] AS Packaging
                              , TPP.[LeftRightSide] AS LeftRightSide
                              , TPP.[Size] AS Size
                              , TPP.[Gender] AS Gender
							  , TPP.[Color] AS Color
                              , PCP.PracticeCatalogSupplierBrandID AS SupplierID
                              , PCP.IsThirdPartyProduct
                              , PCP.StockingUnits
                              FROM                              
                                dbo.ThirdPartyProduct AS TPP WITH ( NOLOCK )
                                INNER JOIN [PracticeCatalogProduct] PCP WITH ( NOLOCK )
                                                                             ON PCP.[ThirdPartyProductID] = TPP.[ThirdPartyProductID]
                                INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
                                WITH ( NOLOCK )
                                     ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
                                INNER JOIN [ProductInventory] PI WITH ( NOLOCK )
                                                                      ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
                              WHERE
                                TPP.IsActive = 1
								AND PCP.IsActive = 1
								AND PI.IsActive = 1
                                AND PI.[PracticeLocationID] = @CurrentPracticeLocationID --@PracticeLocationID
                                AND PCP.IsThirdPartyProduct = 1
								--and (TPP.Name like @SearchText or TPP.ShortName like  @SearchText)
								AND
								(
									--ProductName || ShortName
									COALESCE(TPP.Name, '') like COALESCE(@ProductName, TPP.Name, '')
									OR
									COALESCE(TPP.ShortName, '') like COALESCE(@ProductName, TPP.ShortName, '')								
								)
	                   			AND
								(
									COALESCE(TPP.Code, '') like COALESCE(@ProductCode , TPP.Code, '')
								)	
								AND
								(
									COALESCE(PCSB.BrandName, '') like COALESCE(@ProductBrand, PCSB.BrandName, '')
									OR
									COALESCE(PCSB.BrandShortName, '') like COALESCE(@ProductBrand, PCSB.BrandShortName, '')
								)
								AND 
								(   /* use this type of statement for Child tables of the target to avoid duplicate rows.
										If we use a join for this, we would return duplicate rows, so we just check the existence
										of one record that matches the search parameters in the child table
										*/
									(
										 (@ProductHCPCs IS NULL)
										 OR EXISTS
										 (  
											Select 
												1 
											 from 
												ProductHCPCS PHCPCS 
											 where 
												PHCPCS.PracticeCatalogProductID = PI.PracticeCatalogProductID 
												and PHCPCS.HCPCS like @ProductHCPCs  
										  )
									 )
								)								
								
																
															
                            ) AS Sub
                       ) AS CheckIn
                    INNER JOIN dbo.udf_GetSuggestedReorder_Given_PracticeLocationID( @CurrentPracticeLocationID )	AS Levels
						ON CheckIn.PracticeCatalogProductID = Levels.PracticeCatalogProductID

			-- end of While Loop
			Update @ProductLocations SET fUsed = 1 where PracticeLocationID = @CurrentPracticeLocationID
			
			Select TOP 1 @CurrentPracticeLocationID = PracticeLocationID from @ProductLocations where fUsed = 0
			Select TOP 1 @CurrentPracticeLocationName = PracticeLocationName from @ProductLocations where fUsed = 0
		END     -- end of While Loop                           
--  Below get counts


        SELECT
				Category
			  , SubCategory
			  , Product
			  , Code
			  , Packaging
			  , LeftRightSide
			  , Size
			  , Gender
			  , Color
	          
			  , SupplierID
			  , PracticeCatalogProductID

			  , QuantityOnHand
			  , QuantityOnOrder
	                    
			  , PARLevel
			  , ReorderLevel
			  , CriticalLevel
	          
			  , Coalesce(SuggestedReorderLevel, 0) as SuggestedReorderLevel
	          , PracticeLocationName
			  --, IsThirdPartyProduct
        
        FROM
            @WorkingTable
        ORDER BY
			PracticeLocationName
          , Category
          , Subcategory
          , Product
          , Code


    END




