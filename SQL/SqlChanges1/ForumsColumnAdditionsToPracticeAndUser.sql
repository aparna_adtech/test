use [BregVision]
go

if not exists (select * from dbo.syscolumns where id = object_id(N'Practice') and name=(N'PublicDescription'))
alter table [Practice] add [PublicDescription] [nvarchar](500) NULL
GO

if not exists (select * from dbo.syscolumns where id = object_id(N'User') and name=(N'PublicUserName'))
alter table [User] add [PublicUserName] [nvarchar](100) NULL
GO

if not exists (select * from dbo.syscolumns where id = object_id(N'User') and name=(N'PublicRole'))
alter table [User] add [PublicRole] [nvarchar](100) NULL
GO

if not exists (select * from dbo.syscolumns where id = object_id(N'User') and name=(N'PublicDescription'))
alter table [User] add [PublicDescription] [nvarchar](100) NULL
GO

