GO
USE [BregVision];


GO
PRINT N'Altering [dbo].[Contact]...';


GO
ALTER TABLE [dbo].[Contact]
    ADD [IsSpecificContact] BIT CONSTRAINT [DF_Contact_IsSpecificContact] DEFAULT ((0)) NOT NULL;


GO