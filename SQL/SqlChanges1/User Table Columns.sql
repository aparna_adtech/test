USE BregVision
GO


if not exists (select * from dbo.syscolumns where id = object_id(N'User') and name=(N'AllowDispense'))
ALTER TABLE [dbo].[User] ADD [AllowDispense] [bit] DEFAULT ((1)) NOT NULL 
GO

if not exists (select * from dbo.syscolumns where id = object_id(N'User') and name=(N'AllowInventory'))
ALTER TABLE [dbo].[User] ADD [AllowInventory] [bit] DEFAULT ((1)) NOT NULL 
GO

if not exists (select * from dbo.syscolumns where id = object_id(N'User') and name=(N'AllowCheckIn'))
ALTER TABLE [dbo].[User] ADD [AllowCheckIn] [bit] DEFAULT ((1)) NOT NULL 
GO

if not exists (select * from dbo.syscolumns where id = object_id(N'User') and name=(N'AllowReports'))
ALTER TABLE [dbo].[User] ADD [AllowReports] [bit] DEFAULT ((0)) NOT NULL 
GO

if not exists (select * from dbo.syscolumns where id = object_id(N'User') and name=(N'AllowCart'))
ALTER TABLE [dbo].[User] ADD [AllowCart] [bit] DEFAULT ((1)) NOT NULL 
GO

if not exists (select * from dbo.syscolumns where id = object_id(N'User') and name=(N'AllowDispenseModification'))
ALTER TABLE [dbo].[User] ADD [AllowDispenseModification] [bit] DEFAULT ((0)) NOT NULL 
GO

if not exists (select * from dbo.syscolumns where id = object_id(N'User') and name=(N'DefaultLocationID'))
ALTER TABLE [dbo].[User] ADD [DefaultLocationID] int DEFAULT ((0)) NOT NULL 
GO

if not exists (select * from dbo.syscolumns where id = object_id(N'User') and name=(N'DefaultRecordsPerPage'))
ALTER TABLE [dbo].[User] ADD [DefaultRecordsPerPage] [int] DEFAULT ((10)) NOT NULL 
GO

if not exists (select * from dbo.syscolumns where id = object_id(N'User') and name=(N'DefaultDispenseSearchCriteria'))
ALTER TABLE [dbo].[User] ADD [DefaultDispenseSearchCriteria] [varchar] (20) DEFAULT ('Code') NOT NULL 
GO

if not exists (select * from dbo.syscolumns where id = object_id(N'User') and name=(N'ShowProductInfo'))
ALTER TABLE [dbo].[User] ADD [ShowProductInfo] [bit] DEFAULT ((1)) NOT NULL 
GO
