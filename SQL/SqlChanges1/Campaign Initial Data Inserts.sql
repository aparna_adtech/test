use [BregVision]
--AdMediaType
-- 1,'Image'

--AdLocation
-- 1,'Horizontal Banner Ad'
-- 2,'Vertical Banner Ad'
-- 3,'Large Horizontal Featured Products Ad'


--AdMedia
SET IDENTITY_INSERT [AdMedia] ON
INSERT INTO [BregVision].[dbo].[AdMedia]
           ([Id]
           ,[Name]
           ,[AdMediaTypeId]
           ,[Url]
           ,[AdLocationId]
           ,[CreatedUserId]
           ,[CreatedDateTime]
           ,[ModifiedUserId]
           ,[ModifiedDateTime]
           ,[IsActive])
     VALUES
           (1
           ,'Lateral Stabilizer'
           ,1
           ,'LateralStabilizerHorz.jpg'
           ,1
           ,1
           ,GETDATE()
           ,1
           ,GETDATE()
           ,1)
           
INSERT INTO [BregVision].[dbo].[AdMedia]
           ([Id]
           ,[Name]
           ,[AdMediaTypeId]
           ,[Url]
           ,[AdLocationId]
           ,[CreatedUserId]
           ,[CreatedDateTime]
           ,[ModifiedUserId]
           ,[ModifiedDateTime]
           ,[IsActive])
     VALUES
           (2
           ,'Silver Pro'
           ,1
           ,'SilverProHorz.jpg'
           ,1
           ,1
           ,GETDATE()
           ,1
           ,GETDATE()
           ,1)

SET IDENTITY_INSERT [AdMedia] ON          
INSERT INTO [BregVision].[dbo].[AdMedia]
           ([Id]
           ,[Name]
           ,[AdMediaTypeId]
           ,[Url]
           ,[AdLocationId]
           ,[CreatedUserId]
           ,[CreatedDateTime]
           ,[ModifiedUserId]
           ,[ModifiedDateTime]
           ,[IsActive])
     VALUES
           (3
           ,'Ortho Find'
           ,1
           ,'OrthoFindAdVert.jpg'
           ,2
           ,1
           ,GETDATE()
           ,1
           ,GETDATE()
           ,1)
           
INSERT INTO [BregVision].[dbo].[AdMedia]
           ([Id]
           ,[Name]
           ,[AdMediaTypeId]
           ,[Url]
           ,[AdLocationId]
           ,[CreatedUserId]
           ,[CreatedDateTime]
           ,[ModifiedUserId]
           ,[ModifiedDateTime]
           ,[IsActive])
     VALUES
           (4
           ,'Vectra Air'
           ,1
           ,'VectraAirAdVert1.jpg'
           ,2
           ,1
           ,GETDATE()
           ,1
           ,GETDATE()
           ,1)
           
SET IDENTITY_INSERT [AdMedia] OFF

--Campaign

SET IDENTITY_INSERT [Campaign] ON
INSERT INTO [BregVision].[dbo].[Campaign]
           ([Id]
           ,[Name]
           ,[ImpressionDuration]
           ,[AdMediaId]
           ,[ContactEmail]
           ,[StartDateTime]
           ,[EndDateTime]
           ,[CreatedUserId]
           ,[CreatedDateTime]
           ,[ModifiedDateTime]
           ,[ModifiedUserId]
           ,[IsActive])
     VALUES
           (1
           ,'Lateral Stabilizer'
           ,120
           ,1
           ,'gross@breg.com'
           ,GETDATE()
           ,DATEADD(DAY, 360, GETDATE())
           ,1
           ,GETDATE()
           ,GETDATE()
           ,1
           ,1)
           
INSERT INTO [BregVision].[dbo].[Campaign]
           ([Id]
           ,[Name]
           ,[ImpressionDuration]
           ,[AdMediaId]
           ,[ContactEmail]
           ,[StartDateTime]
           ,[EndDateTime]
           ,[CreatedUserId]
           ,[CreatedDateTime]
           ,[ModifiedDateTime]
           ,[ModifiedUserId]
           ,[IsActive])
     VALUES
           (2
           ,'Silver Pro'
           ,120
           ,2
           ,'gross@breg.com'
           ,GETDATE()
           ,DATEADD(DAY, 360, GETDATE())
           ,1
           ,GETDATE()
           ,GETDATE()
           ,1
           ,1)
INSERT INTO [BregVision].[dbo].[Campaign]
           ([Id]
           ,[Name]
           ,[ImpressionDuration]
           ,[AdMediaId]
           ,[ContactEmail]
           ,[StartDateTime]
           ,[EndDateTime]
           ,[CreatedUserId]
           ,[CreatedDateTime]
           ,[ModifiedDateTime]
           ,[ModifiedUserId]
           ,[IsActive])
     VALUES
           (3
           ,'Ortho Find'
           ,120
           ,3
           ,'gross@breg.com'
           ,GETDATE()
           ,DATEADD(DAY, 360, GETDATE())
           ,1
           ,GETDATE()
           ,GETDATE()
           ,1
           ,1)
           
INSERT INTO [BregVision].[dbo].[Campaign]
           ([Id]
           ,[Name]
           ,[ImpressionDuration]
           ,[AdMediaId]
           ,[ContactEmail]
           ,[StartDateTime]
           ,[EndDateTime]
           ,[CreatedUserId]
           ,[CreatedDateTime]
           ,[ModifiedDateTime]
           ,[ModifiedUserId]
           ,[IsActive])
     VALUES
           (4
           ,'Vectra Air'
           ,120
           ,4
           ,'gross@breg.com'
           ,GETDATE()
           ,DATEADD(DAY, 360, GETDATE())
           ,1
           ,GETDATE()
           ,GETDATE()
           ,1
           ,1)
           
SET IDENTITY_INSERT [Campaign] OFF
--[CampaignTargetProduct]
INSERT INTO [BregVision].[dbo].[CampaignTargetProduct]
           ([CampaignId]
           ,[MasterCatalogProductId])
     VALUES
           (1
           ,723)
           
INSERT INTO [BregVision].[dbo].[CampaignTargetProduct]
           ([CampaignId]
           ,[MasterCatalogProductId])
     VALUES
           (2
           ,12265)
           
/*     ********************greg will gis us another code next week ***********************      
INSERT INTO [BregVision].[dbo].[CampaignTargetProduct]
           ([CampaignId]
           ,[MasterCatalogProductId])
     VALUES
           (3
           ,0) --greg will gis us another code next week
*/      
     
INSERT INTO [BregVision].[dbo].[CampaignTargetProduct]
           ([CampaignId]
           ,[MasterCatalogProductId])
     VALUES
           (4
           ,12248)
           
GO


