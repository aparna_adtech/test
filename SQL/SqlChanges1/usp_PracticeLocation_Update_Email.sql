USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_PracticeLocation_Update_Email]    Script Date: 12/27/2011 21:25:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocation_Update_Email
   
   Description:  Updates the email columns for a record in the table PracticeLocation
   
   AUTHOR:       John Bongiorni 9/24/2007 11:41:17 AM
   
   Modifications:  old proc is dbo.usp_PracticeLocation_Update_Email_OLDDONOTUSE
   ------------------------------------------------------------ */  ------------------ UPDATE -------------------

ALTER PROCEDURE [dbo].[usp_PracticeLocation_Update_Email]
(
	  @PracticeLocationID            INT
	, @EmailForOrderApproval         VARCHAR(100)
	, @EmailForOrderConfirmation     VARCHAR(100)
	, @EmailForFaxConfirmation       VARCHAR(100)
	, @EmailForBillingDispense       VARCHAR(100)
	, @EmailForLowInventoryAlerts    VARCHAR(100)
	, @IsEmailForLowInventoryAlertsOn BIT
	, @ModifiedUserID                 INT
	, @EmailForPrintDispense		 VARCHAR(100) = NULL
)
AS
BEGIN
	DECLARE @Err INT

	UPDATE dbo.PracticeLocation
	SET
		  EmailForOrderApproval = @EmailForOrderApproval
		, EmailForOrderConfirmation = @EmailForOrderConfirmation
		, EmailForFaxConfirmation = @EmailForFaxConfirmation
		, EmailForBillingDispense = @EmailForBillingDispense
		, EmailForPrintDispense = @EmailForPrintDispense
		, EmailForLowInventoryAlerts = @EmailForLowInventoryAlerts
		, IsEmailForLowInventoryAlertsOn = @IsEmailForLowInventoryAlertsOn
		, ModifiedUserID = @ModifiedUserID
		, ModifiedDate = GETDATE()

	WHERE 
		PracticeLocationID = @PracticeLocationID

	SET @Err = @@ERROR

	RETURN @Err
End