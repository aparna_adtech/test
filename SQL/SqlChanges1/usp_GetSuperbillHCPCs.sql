USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetSuperbillHCPCs]    Script Date: 03/09/2009 22:33:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author: Mike Sneen>
-- Create date: <Create Date: 02/28/09>
-- Description:	<Description: Used to get all a superbill>
-- modification:
--  [dbo].[usp_GetSuperbillHCPCs]  

-- =============================================
Create PROCEDURE [dbo].[usp_GetSuperbillHCPCs]  --3 --10  --  _With_Brands
	@PracticeLocationID int
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
--        SET NOCOUNT ON ;
     
Select PracticeCatalogProductID, HCPCs as HCPCsCode from SuperBillProduct where PracticeLocationID = @PracticeLocationID

UNION

Select PH.PracticeCatalogProductID, PH.HCPCS  as HCPCsCode from ProductHCPCS PH
join PracticeCatalogProduct PCP on PH.PracticeCatalogProductID = PCP.PracticeCatalogProductID 
	and PCP.PracticeID = (Select PL.PracticeID from PracticeLocation PL where PL.PracticeLocationID = @PracticeLocationID)
	
UNION

Select PCP.PracticeCatalogProductID, TH.HCPCS as HCPCsCode from ThirdPartyProductHCPCS TH
join ThirdPartyProduct TPP on TH.ThirdPartyProductID = TPP.ThirdPartyProductID
join PracticeCatalogProduct PCP on TPP.ThirdPartyProductID = PCP.ThirdPartyProductID and PCP.IsThirdPartyProduct = 1
	AND PCP.PracticeID = (Select PL.PracticeID from PracticeLocation PL where PL.PracticeLocationID = @PracticeLocationID)
	
UNION

Select PCP.PracticeCatalogProductID, BH.HCPCS as HCPCsCode from BregProductHCPCS BH
join MasterCatalogProduct MCP on BH.MasterCatalogProductID = MCP.MasterCatalogProductID
join PracticeCatalogProduct PCP on MCP.MasterCatalogProductID = PCP.MasterCatalogProductID and PCP.IsThirdPartyProduct = 0
	AND PCP.PracticeID = (Select PL.PracticeID from PracticeLocation PL where PL.PracticeLocationID = @PracticeLocationID)
    END

--Exec usp_GetSuperbillHCPCs 3