USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_Practice_SelectAll_BillingStartDate_PracticeCode]    Script Date: 05/03/2010 23:30:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
	dbo.usp_Practice_SelectAll_BillingStartDate_PracticeCode
	
	Get all Practices (except test practices) with their billingstartdates
	and oracle practice codes (supplied by Accounting)
	Send this to Bobby Haywood monthly to update
	
	John Bongiorni
	2008.01.28
	
	EXEC usp_Practice_SelectAll_BillingStartDate_PracticeCode 1

*/


ALTER PROC [dbo].[usp_Practice_SelectAll_BillingStartDate_PracticeCode]
	@AppType INT = NULL
AS 
BEGIN

	SELECT
		 P.PracticeID
		 , P.PracticeName
		 , CONVERT(VARCHAR(50), P.BillingStartDate, 101) AS BillingStartDate  
		 , BVPC.PracticeCode
		 , CASE WHEN P.IsVisionLite = 1 THEN 'Express' ELSE 'Vision' END as App
	FROM Practice AS P	WITH (NOLOCK)
	LEFT JOIN dbo.BregVisionPracticeCode AS BVPC	WITH (NOLOCK)
		ON P.PracticeID = BVPC.PracticeID
	WHERE P.IsActive = 1
		AND P.PracticeID NOT IN (1,3)  -- Test Practices
		AND @AppType IS NULL OR (P.IsVisionLite = @AppType)
	ORDER BY 
		  P.BillingStartDate
		, P.PracticeName
	
END 	