USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_SearchForPracticeLocaionIDAndProductInventoryID]    Script Date: 02/04/2010 15:27:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		 Yuiko Sneen-Itazawa
-- Create date:  02/04/2010
-- Description:	 Used for the Save function in Dispense page
-- =============================================
ALTER PROCEDURE  [dbo].[usp_SearchForPracticeLocaionIDAndProductInventoryID]
	-- Add the parameters for the stored procedure here
	@PracticeLocationID int,
	@ProductInventoryID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *  FROM [BregVision].[dbo].[ProductInventory] 
	where PracticeLocationID = @PracticeLocationID and ProductInventoryID = @ProductInventoryID
END
