USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetPracticeCatalogProductInfoMCToPC_All_With_3rdPartyProducts]    Script Date: 04/07/2012 11:23:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Greg Ross
-- Create date: 09/27/07
-- Description:	Used to get the detail for a Practice. Used in the MCToPC.aspx page
-- Modification: 20071008 2033 JB Add Third Party and code to make this generic
--  (FOR MCProducts and ThirdPartyProducts)
--				20071009 1203 JB Add WITH(NOLOCK) cLAUSES to Tables.
--			2007.12.12 JB Order By ProductShortName, Code
--			2008.01.23 JB Add LeftRightSide, Size and SideSize
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetPracticeCatalogProductInfoMCToPC_All_With_3rdPartyProducts] 

	@PracticeID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


select
	Sub.PracticeID, 
	Sub.PracticecatalogProductID,
	Sub.MasterCatalogProductID	AS MasterCatalogProductID,  -- Needs to be called ProductID (to handle MC and ThirdParty)
	Sub.PracticeCatalogSupplierBrandID,

--	SUB.Sequence,  -- jb

	Sub.ShortName,
	Sub.Code,
	ISNULL(Sub.LeftRightSide, ' ') + ', ' + ISNULL(Sub.Size, ' ') AS SideSize,
	ISNULL(Sub.LeftRightSide, ' ') AS LeftRightSide,
	ISNULL(Sub.Size, ' ') AS Size,
	ISNULL(Sub.Gender, ' ') AS Gender,
	Sub.WholesaleCost,
	Sub.BillingCharge,
	Sub.DMEDeposit,
	Sub.BillingChargeCash,
	Sub.StockingUnits,
	Sub.BuyingUnits,
	Sub.IsThirdPartyProduct,
	Sub.isLogoAblePart,
	Sub.McpIsLogoAblePart,
	dbo.ConcatHCPCS(Sub.PracticeCatalogProductID) AS HCPCSString -- JB 20071004 Added Column for UDF Concat of HCPCS

						-- Need to be put in app code and grid to distinguish 
												--  Master Catalog Product and Third Party Product.
												
	, CASE (SELECT COUNT(1) 
	FROM productinventory AS PI WITH (NOLOCK) 
	WHERE PI.practiceCatalogProductID = sub.practiceCatalogProductID
    ) WHEN 0 THEN 1 ELSE 0 END AS IsDeletable   --  JB  20080225  Added Column for "Deletable" PCPs
												
	
FROM
(
		select
			PCP.PracticeID, 
			PCP.PracticecatalogProductID,
			PCP.MasterCatalogProductID	AS MasterCatalogProductID,  -- Needs to be ProductID
			PCP.PracticeCatalogSupplierBrandID,

			--ISNULL(PCSB.Sequence, 999) AS Sequence,  -- jb

			MCP.ShortName,
			MCP.Code,
			MCP.LeftRightSide,
			MCP.Size,
			MCP.Gender,
			PCP.WholesaleCost,
			PCP.BillingCharge,
			PCP.DMEDeposit,
			PCP.BillingChargeCash,
			PCP.StockingUnits,
			PCP.BuyingUnits,
			0 AS IsThirdPartyProduct,					-- Needs to know if this is a Third Party Product.
			PCP.IsLogoAblePart,
			dbo.isPcpItemLogoAblePart(PCP.PracticeCatalogProductID) AS McpIsLogoAblePart
		 from PracticeCatalogProduct AS PCP WITH(NOLOCK) 
		inner join MasterCatalogProduct AS MCP WITH(NOLOCK) 
			on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
		where PCP.practiceID =				@PracticeID
			AND PCP.IsThirdPartyProduct = 0		-- MasterCatalogProduct
			and PCP.isactive = 1
			and MCP.isactive = 1
			

		UNION

			select
			PCP.PracticeID, 
			PCP.PracticecatalogProductID,
			PCP.ThirdPartyProductID		 AS MasterCatalogProductID,  -- Needs to be ProductID
			PCP.PracticeCatalogSupplierBrandID,

			--ISNULL(PCSB.Sequence, 999) AS Sequence,  -- jb

			TPP.ShortName,
			TPP.Code,
			TPP.LeftRightSide,
			TPP.Size,
			TPP.Gender,
			PCP.WholesaleCost,
			PCP.BillingCharge,
			PCP.DMEDeposit,
			PCP.BillingChargeCash,
			PCP.StockingUnits,
			PCP.BuyingUnits,
			1 AS IsThirdPartyProduct,					-- Needs to know if this is a Third Party Product.
			0 AS IsLogoAblePart,
			0 AS McpIsLogoAblePart
		 from PracticeCatalogProduct AS PCP  WITH(NOLOCK) 
		inner join ThirdPartyProduct AS TPP  WITH(NOLOCK) 
			on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID 
		where PCP.practiceID =			@PracticeID
			AND PCP.IsThirdPartyProduct = 1		-- ThirdPartyProduct
			and PCP.isactive = 1
			and TPP.isactive = 1

) AS Sub
ORDER BY
--	Sub.IsThirdPartyProduct, -- jb
--	SUB.Sequence,  -- jb
	UPPER(RTRIM(LTRIM(Sub.ShortName))) -- jb 
	, UPPER(RTRIM(LTRIM(Sub.Code)))		-- jb

END





