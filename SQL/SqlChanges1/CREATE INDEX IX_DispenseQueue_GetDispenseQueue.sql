USE [BregVision]
GO
CREATE NONCLUSTERED INDEX [IX_DispenseQueue_GetDispenseQueue] 
ON [dbo].[DispenseQueue]([PracticeLocationID] ASC,[IsActive] ASC,[IsDispensed] ASC)
INCLUDE ([DispenseQueueID],[ProductInventoryID],[PatientCode],[PhysicianID],[ICD9_Code],[IsMedicare],[ABNForm],[Side],[DMEDeposit],[QuantityToDispense],[DispenseDate],[CreatedDate],[DispenseSignatureID],[PatientEmail],[IsRental],[PracticePayerID],[IsCustomFit],[IsCustomFabricated],[HCPCs]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = ON, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

