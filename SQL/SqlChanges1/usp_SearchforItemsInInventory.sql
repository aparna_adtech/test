USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_SearchforItemsInInventory]    Script Date: 11/16/2015 10:14:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ==============================================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
--Exec usp_SearchforItemsInInventory 'Slingshot Neutral', '', 3

--===============================================================
--Updated by			Updated Date		Description
--Yuiko Sneen-Itazawa	02/11/2010			Added 'Vectra Air'
-- ==============================================================

ALTER PROCEDURE [dbo].[usp_SearchforItemsInInventory]
	-- Add the parameters for the stored procedure here
	@SearchCriteria varchar(50),
	@SearchText varchar(50),
	@PracticeLocationID int


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--All, Code, Brand

IF @SearchCriteria <> 'CampaignId'
BEGIN
	set @SearchText = @SearchText + '%'
END

if @SearchCriteria = 'All' 
	begin
		EXEC	[dbo].usp_SearchForItemsInInventory_ALL_With_ThirdPartyProducts
		@PracticeLocationID = @PracticeLocationID,
		@ISActive = 1
	end 
else if( @SearchCriteria = 'Code' OR @SearchCriteria = 'UPCCode')
	begin
	set @SearchText = '%' + @SearchText
	EXEC	[dbo].usp_SearchForItemsInInventory_ByProductCode_With_ThirdPartyProducts
	@PracticeLocationID = @PracticeLocationID,
	@SearchText = @SearchText,
	@SearchCriteria = @SearchCriteria
 


	end 
else if @SearchCriteria = 'Brand'
	begin
	EXEC	[dbo].usp_SearchForItemsInInventory_ByProductBrand_With_ThirdPartyProducts
	@PracticeLocationID = @PracticeLocationID,
	@SearchText = @SearchText	  

	end 
else if @SearchCriteria = 'Name'
	begin
	set @SearchText = '%' + @SearchText    
	EXEC	[dbo].usp_SearchForItemsInInventory_ByProductName_With_ThirdPartyProducts
	@PracticeLocationID = @PracticeLocationID,
	@SearchText = @SearchText


	end 
else if @SearchCriteria = 'Category'
	begin
	   select PI.ProductInventoryID, 
		PCSB.SupplierName,
		PCSB.BrandName,
		MCP.Name as ProductName,
		ISNULL(MCP.LeftRightSide, '') as Side,
		ISNULL(MCP.Size, '') as Size,		
		MCP.Code,
		PI.PracticeCatalogProductID,
		
		ISNULL(PI.QuantityOnHandPerSystem, 0) AS QOH
		
		 from productInventory PI 
		inner join practicecatalogproduct PCP on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
		inner join practicecatalogsupplierbrand PCSB on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID
		inner join MasterCatalogProduct MCP on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
		inner join MasterCatalogSubCategory MCSC on MCP.MasterCatalogSubCategoryID = MCSC.MasterCatalogSubCategoryID
		inner join MasterCatalogCategory MCC on MCSC.MasterCatalogCategoryID = MCC.MasterCatalogCategoryID
		where PI.isactive =1 
		and PCP.isactive = 1
		and PCSB.isactive = 1
		and MCP.isactive = 1
		and MCP.IsDiscontinued = 0
		and MCSC.isactive = 1
		and MCC.isactive = 1
		and MCC.Name like @SearchText
		and PI.PracticeLocationID = @PracticeLocationID
		group by PCSB.BrandName,PCSB.SupplierName,MCP.Name,Code, PI.ProductInventoryID,PI.PracticeCatalogProductID,
		MCP.LeftRightSide,
		MCP.Size,
		ISNULL(PI.QuantityOnHandPerSystem, 0)
	end 
else if @SearchCriteria = 'HCPCs'

	begin
		set @SearchText = '%' + @SearchText    
		EXEC	[dbo].usp_SearchForItemsInInventory_ByHCPCs_With_ThirdPartyProducts
		@PracticeLocationID = @PracticeLocationID,
		@SearchText = @SearchText
	end 


else if @SearchCriteria = 'CampaignId'
BEGIN
	DECLARE @CampaignId int
	set @CampaignId = CAST(@SearchText as int)
		
	select 
		MCP.Name as ProductName,
		ISNULL(MCP.LeftRightSide, '') as Side,
		ISNULL(MCP.Size, '') as Size,		
		MCP.Code,
		MCS.SupplierName as SupplierName,
		MCS.SupplierShortName as BrandName,
		MCP.MasterCatalogProductID,
		ISNULL(PI.ProductInventoryID, 0) AS ProductInventoryID,
		ISNULL(PI.PracticeCatalogProductID, 0) AS PracticeCatalogProductID,
		ISNULL(PI.QuantityOnHandPerSystem, 0) AS QOH
		from 
			dbo.MasterCatalogProduct MCP 
			left outer join MasterCatalogSubCategory MCSC on MCP.MasterCatalogSubCategoryID = MCSC.MasterCatalogSubCategoryID
			Left outer join MasterCatalogCategory MCC on MCSC.MasterCatalogCategoryID = MCC.MasterCatalogCategoryID
			left outer join MasterCatalogSupplier MCS on MCC.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
			Left Outer Join PracticeCatalogProduct PCP on MCP.MasterCatalogProductID=PCP.MasterCatalogProductID and PCP.PracticeID=(Select PL.PracticeID from PracticeLocation PL where PL.PracticeLocationID = @PracticeLocationID)
			Left Outer Join ProductInventory PI on PCP.PracticeCatalogProductID = pi.PracticeCatalogProductID and PI.PracticeLocationID = @PracticeLocationID
			Left Outer join practicecatalogsupplierbrand PCSB on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID
		where 
		MCP.isactive = 1
		and MCP.IsDiscontinued = 0
		and MCSC.isactive = 1
		and MCC.isactive = 1
		and MCS.IsActive = 1
		and	MCP.MasterCatalogProductID in
			(
				Select MasterCatalogProductID 
					from CampaignTargetProduct 
					where CampaignId=@CampaignId
			)
			
END
	
	else if @SearchCriteria in ('Ankle')

	begin
select 
		MCP.Name as ProductName,
		ISNULL(MCP.LeftRightSide, '') as Side,
		ISNULL(MCP.Size, '') as Size,		
		MCP.Code,
		MCS.SupplierName as SupplierName,
		MCS.SupplierShortName as BrandName,
		MCP.MasterCatalogProductID,
		ISNULL(PI.ProductInventoryID, 0) AS ProductInventoryID,
		ISNULL(PI.PracticeCatalogProductID, 0) AS PracticeCatalogProductID,
		ISNULL(PI.QuantityOnHandPerSystem, 0) AS QOH
		from 
			dbo.MasterCatalogProduct MCP 
			left outer join MasterCatalogSubCategory MCSC on MCP.MasterCatalogSubCategoryID = MCSC.MasterCatalogSubCategoryID
			Left outer join MasterCatalogCategory MCC on MCSC.MasterCatalogCategoryID = MCC.MasterCatalogCategoryID
			left outer join MasterCatalogSupplier MCS on MCC.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
			Left Outer Join PracticeCatalogProduct PCP on MCP.MasterCatalogProductID=PCP.MasterCatalogProductID and PCP.PracticeID=(Select PL.PracticeID from PracticeLocation PL where PL.PracticeLocationID = @PracticeLocationID)
			Left Outer Join ProductInventory PI on PCP.PracticeCatalogProductID = pi.PracticeCatalogProductID and PI.PracticeLocationID = @PracticeLocationID
			Left Outer join practicecatalogsupplierbrand PCSB on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID
		where 
		MCP.isactive = 1
		and MCP.IsDiscontinued = 0
		and MCSC.isactive = 1
		and MCC.isactive = 1
		and MCS.IsActive = 1
		AND 
			(
				(
					@SearchCriteria = 'Ankle'
					AND
					MCP.[Code] in
						(
							'00012', '00013', '00014', '00022', '00023', '00024',

							'90060', '90061', '90062', '90063', '90064', '90065', '90066',

							'10151', '10152', '10153', '10154', '10155', '10156'

						)
				)
					
			)
	end

END









/*
ALTER PROCEDURE [dbo].[usp_SearchforItemsInInventory]
	-- Add the parameters for the stored procedure here
	@SearchCriteria varchar(50),
	@SearchText varchar(50),
	@PracticeLocationID int


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--All, Code, Brand

IF @SearchCriteria <> 'CampaignId'
BEGIN
	set @SearchText = @SearchText + '%'
END

if @SearchCriteria = 'All' 
	begin
		EXEC	[dbo].usp_SearchForItemsInInventory_ALL_With_ThirdPartyProducts
		@PracticeLocationID = @PracticeLocationID,
		@ISActive = 1
	end 
else if @SearchCriteria = 'Code'
	begin
	EXEC	[dbo].usp_SearchForItemsInInventory_ByProductCode_With_ThirdPartyProducts
	@PracticeLocationID = @PracticeLocationID,
	@SearchText = @SearchText
 


	end 
else if @SearchCriteria = 'Brand'
	begin
	EXEC	[dbo].usp_SearchForItemsInInventory_ByProductBrand_With_ThirdPartyProducts
	@PracticeLocationID = @PracticeLocationID,
	@SearchText = @SearchText	  

	end 
else if @SearchCriteria = 'Name'
	begin
	set @SearchText = '%' + @SearchText    
	EXEC	[dbo].usp_SearchForItemsInInventory_ByProductName_With_ThirdPartyProducts
	@PracticeLocationID = @PracticeLocationID,
	@SearchText = @SearchText


	end 
else if @SearchCriteria = 'Category'
	begin
	   select PI.ProductInventoryID, 
		PCSB.SupplierName,
		PCSB.BrandName,
		MCP.Name as ProductName,
		ISNULL(MCP.LeftRightSide, '') as Side,
		ISNULL(MCP.Size, '') as Size,		
		MCP.Code,
		PI.PracticeCatalogProductID,
		
		ISNULL(PI.QuantityOnHandPerSystem, 0) AS QOH
		
		 from productInventory PI 
		inner join practicecatalogproduct PCP on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
		inner join practicecatalogsupplierbrand PCSB on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID
		inner join MasterCatalogProduct MCP on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
		inner join MasterCatalogSubCategory MCSC on MCP.MasterCatalogSubCategoryID = MCSC.MasterCatalogSubCategoryID
		inner join MasterCatalogCategory MCC on MCSC.MasterCatalogCategoryID = MCC.MasterCatalogCategoryID
		where PI.isactive =1 
		and PCP.isactive = 1
		and PCSB.isactive = 1
		and MCP.isactive = 1
		and MCSC.isactive = 1
		and MCC.isactive = 1
		and MCC.Name like @SearchText
		and PI.PracticeLocationID = @PracticeLocationID
		group by PCSB.BrandName,PCSB.SupplierName,MCP.Name,Code, PI.ProductInventoryID,PI.PracticeCatalogProductID,
		MCP.LeftRightSide,
		MCP.Size,
		ISNULL(PI.QuantityOnHandPerSystem, 0)
	end 
else if @SearchCriteria = 'HCPCs'

	begin
		set @SearchText = '%' + @SearchText    
		EXEC	[dbo].usp_SearchForItemsInInventory_ByHCPCs_With_ThirdPartyProducts
		@PracticeLocationID = @PracticeLocationID,
		@SearchText = @SearchText
	end 


else if @SearchCriteria = 'CampaignId'
BEGIN
	DECLARE @CampaignId int
	set @CampaignId = CAST(@SearchText as int)
		
	select 
		MCP.Name as ProductName,
		ISNULL(MCP.LeftRightSide, '') as Side,
		ISNULL(MCP.Size, '') as Size,		
		MCP.Code,
		MCS.SupplierName as SupplierName,
		MCS.SupplierShortName as BrandName,
		MCP.MasterCatalogProductID,
		ISNULL(PI.ProductInventoryID, 0) AS ProductInventoryID,
		ISNULL(PI.PracticeCatalogProductID, 0) AS PracticeCatalogProductID,
		ISNULL(PI.QuantityOnHandPerSystem, 0) AS QOH
		from 
			dbo.MasterCatalogProduct MCP 
			left outer join MasterCatalogSubCategory MCSC on MCP.MasterCatalogSubCategoryID = MCSC.MasterCatalogSubCategoryID
			Left outer join MasterCatalogCategory MCC on MCSC.MasterCatalogCategoryID = MCC.MasterCatalogCategoryID
			left outer join MasterCatalogSupplier MCS on MCC.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
			Left Outer Join PracticeCatalogProduct PCP on MCP.MasterCatalogProductID=PCP.MasterCatalogProductID and PCP.PracticeID=(Select PL.PracticeID from PracticeLocation PL where PL.PracticeLocationID = @PracticeLocationID)
			Left Outer Join ProductInventory PI on PCP.PracticeCatalogProductID = pi.PracticeCatalogProductID and PI.PracticeLocationID = @PracticeLocationID
			Left Outer join practicecatalogsupplierbrand PCSB on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID
		where 
		MCP.isactive = 1
		and MCSC.isactive = 1
		and MCC.isactive = 1
		and MCS.IsActive = 1
		and	MCP.MasterCatalogProductID in
			(
				Select MasterCatalogProductID 
					from CampaignTargetProduct 
					where CampaignId=@CampaignId
			)
			
END
	
	else if @SearchCriteria in ('Ankle')

	begin
select 
		MCP.Name as ProductName,
		ISNULL(MCP.LeftRightSide, '') as Side,
		ISNULL(MCP.Size, '') as Size,		
		MCP.Code,
		MCS.SupplierName as SupplierName,
		MCS.SupplierShortName as BrandName,
		MCP.MasterCatalogProductID,
		ISNULL(PI.ProductInventoryID, 0) AS ProductInventoryID,
		ISNULL(PI.PracticeCatalogProductID, 0) AS PracticeCatalogProductID,
		ISNULL(PI.QuantityOnHandPerSystem, 0) AS QOH
		from 
			dbo.MasterCatalogProduct MCP 
			left outer join MasterCatalogSubCategory MCSC on MCP.MasterCatalogSubCategoryID = MCSC.MasterCatalogSubCategoryID
			Left outer join MasterCatalogCategory MCC on MCSC.MasterCatalogCategoryID = MCC.MasterCatalogCategoryID
			left outer join MasterCatalogSupplier MCS on MCC.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
			Left Outer Join PracticeCatalogProduct PCP on MCP.MasterCatalogProductID=PCP.MasterCatalogProductID and PCP.PracticeID=(Select PL.PracticeID from PracticeLocation PL where PL.PracticeLocationID = @PracticeLocationID)
			Left Outer Join ProductInventory PI on PCP.PracticeCatalogProductID = pi.PracticeCatalogProductID and PI.PracticeLocationID = @PracticeLocationID
			Left Outer join practicecatalogsupplierbrand PCSB on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID
		where 
		MCP.isactive = 1
		and MCSC.isactive = 1
		and MCC.isactive = 1
		and MCS.IsActive = 1
		AND 
			(
				(
					@SearchCriteria = 'Ankle'
					AND
					MCP.[Code] in
						(
							'00012', '00013', '00014', '00022', '00023', '00024',

							'90060', '90061', '90062', '90063', '90064', '90065', '90066',

							'10151', '10152', '10153', '10154', '10155', '10156'

						)
				)
					
			)
	end

END






*/