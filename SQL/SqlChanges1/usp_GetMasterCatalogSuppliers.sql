USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMasterCatalogSuppliers]    Script Date: 1/28/2016 3:29:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
--  Modifications:  John Bongiorni  2007.12.12
--		Show Breg First.
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetMasterCatalogSuppliers]
	
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON ;
     SELECT     DISTINCT 
				
				MCS.[SupplierName] AS Brand,
                MCS.[MasterCatalogSupplierID] AS SupplierID,
				c.[FirstName] AS Name,
                C.[LastName],
                c.[PhoneWork],
                A.[AddressLine1] AS Address,
                A.City AS City,
                A.[ZipCode] AS Zipcode,
				MCS.MasterCatalogSupplierID
				--MCC.[Name] AS Category
        FROM    [MasterCatalogSupplier] MCS
                INNER JOIN address A ON MCS.[AddressID] = A.[AddressID]
                INNER JOIN [Contact] C ON mcs.[ContactID] = c.[ContactID]
				INNER JOIN [MasterCatalogCategory] MCC ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
				INNER JOIN [MasterCatalogSubCategory] MCSC ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
				INNER JOIN [MasterCatalogProduct] MCP ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
		WHERE	MCS.IsActive = 1
				AND MCP.IsActive = 1
		ORDER BY
				MCS.MasterCatalogSupplierID   -- JB Puts Breg first
					--  MCS.suppliername  -- JB commented out.
END


