use [BregVision]
go

if not exists (select * from dbo.syscolumns where id = object_id(N'SupplierOrder') and name=(N'InvoicePaid'))
alter table [SupplierOrder] add InvoicePaid bit DEFAULT 0 NOT NULL
GO

if not exists (select * from dbo.syscolumns where id = object_id(N'SupplierOrder') and name=(N'InvoiceNumber'))
alter table [SupplierOrder] add InvoiceNumber Varchar(50) NULL
GO
