USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetSuperbillCategories]    Script Date: 03/09/2009 22:33:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author: Mike Sneen>
-- Create date: <Create Date: 02/28/09>
-- Description:	<Description: Used to get all a superbill>
-- modification:
--  [dbo].[usp_GetSuperbillCategories]  

-- =============================================
create PROCEDURE [dbo].[usp_GetSuperbillCategories]  --3 --10  --  _With_Brands
	@PracticeLocationID int
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
--        SET NOCOUNT ON ;
     
     SELECT 
			sbc.SuperBillCategoryID as CategoryID,
			sbc.[Name] as CategoryName ,
			sbc.ColumnNumber,
			sbc.Sequence
		from  SuperBillCategory sbc 
				
		where 
			sbc.PracticeLocationID = @PracticeLocationID
			AND sbc.IsActive = 1
		
		order by sbc.ColumnNumber, sbc.[Name]

    END

--Exec usp_GetSuperbillCategories 3



