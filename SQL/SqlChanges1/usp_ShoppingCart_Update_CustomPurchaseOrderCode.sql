USE [BregVision]
GO

/****** Object:  StoredProcedure [dbo].[usp_ShoppingCart_Update_ShippingType]    Script Date: 01/19/2011 10:27:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[usp_ShoppingCart_Update_CustomPurchaseOrderCode]
	  @ShoppingCartID INT
	, @CustomPurchaseOrderCode VARCHAR(75)
AS
BEGIN

	UPDATE ShoppingCart
	SET CustomPurchaseOrderCode = @CustomPurchaseOrderCode
	WHERE ShoppingCartID = @ShoppingCartID

END
GO


