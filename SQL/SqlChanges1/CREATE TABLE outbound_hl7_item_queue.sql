USE [BregVisionEMR]
GO

/****** Object:  Table [dbo].[outbound_hl7_item_queue]    Script Date: 5/1/2014 5:57:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[outbound_hl7_item_queue](
	[outbound_hl7_item_queue_id] [uniqueidentifier] NOT NULL,
	[message_type] [varchar](10) NOT NULL,
	[practice_id] [int] NOT NULL,
	[remote_schedule_item_id] [nvarchar](50) NULL,
	[hcpcs_transform_enabled] [bit] NOT NULL,
	[creation_date] [datetime] NOT NULL,
	[send_success_date] [datetime] NULL,
 CONSTRAINT [PK_outbound_hl7_item_queue] PRIMARY KEY CLUSTERED 
(
	[outbound_hl7_item_queue_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[outbound_hl7_item_queue] ADD  CONSTRAINT [DF_outbound_hl7_item_queue_outbound_hl7_item_queue_id]  DEFAULT (newid()) FOR [outbound_hl7_item_queue_id]
GO

ALTER TABLE [dbo].[outbound_hl7_item_queue] ADD  CONSTRAINT [DF_outbound_hl7_item_queue_message_type]  DEFAULT ('DFT') FOR [message_type]
GO

ALTER TABLE [dbo].[outbound_hl7_item_queue] ADD  CONSTRAINT [DF_outbound_hl7_item_queue_hcpcs_transform_enabled]  DEFAULT ((0)) FOR [hcpcs_transform_enabled]
GO

ALTER TABLE [dbo].[outbound_hl7_item_queue] ADD  CONSTRAINT [DF_outbound_hl7_item_queue_creation_date]  DEFAULT (getdate()) FOR [creation_date]
GO


