USE [BregVision]
GO
/****** Object:  Table [dbo].[AdLocation]    Script Date: 05/25/2010 15:23:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdLocation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_AdLocations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AdMediaType]    Script Date: 05/25/2010 15:23:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdMediaType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_AdImageType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AdMedia]    Script Date: 05/25/2010 15:23:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdMedia](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[AdMediaTypeId] [int] NOT NULL,
	[Url] [nvarchar](200) NOT NULL,
	[AdLocationId] [int] NOT NULL,
	[CreatedUserId] [int] NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[ModifiedUserId] [int] NULL,
	[ModifiedDateTime] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
	[timestamp] [timestamp] NOT NULL,
 CONSTRAINT [PK_AdImage1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Campaign]    Script Date: 05/25/2010 15:23:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Campaign](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[ImpressionDuration] [int] NOT NULL,
	[AdMediaId] [int] NOT NULL,
	[ContactEmail] [nvarchar](100) NOT NULL,
	[StartDateTime] [datetime] NOT NULL,
	[EndDateTime] [datetime] NOT NULL,
	[CreatedUserId] [int] NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[ModifiedDateTime] [datetime] NULL,
	[ModifiedUserId] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[timestamp] [timestamp] NOT NULL,
 CONSTRAINT [PK_Campaign] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CampaignUserState]    Script Date: 05/25/2010 15:23:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CampaignUserState](
	[CampaignId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[AdLocationId] [int] NOT NULL,
	[ModifiedDateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_CampaignUserState_1] PRIMARY KEY CLUSTERED 
(
	[CampaignId] ASC,
	[UserId] ASC,
	[AdLocationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CampaignImpression]    Script Date: 05/25/2010 15:23:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CampaignImpression](
	[CampaignId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_CampaignImpression_1] PRIMARY KEY CLUSTERED 
(
	[CampaignId] ASC,
	[UserId] ASC,
	[CreatedDateTime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CampaignClickThrough]    Script Date: 05/25/2010 15:23:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CampaignClickThrough](
	[CampaignId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_CampaignClickThrough_1] PRIMARY KEY CLUSTERED 
(
	[CampaignId] ASC,
	[UserId] ASC,
	[CreatedDateTime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CampaignTargetProduct]    Script Date: 05/25/2010 15:23:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CampaignTargetProduct](
	[CampaignId] [int] NOT NULL,
	[MasterCatalogProductId] [int] NOT NULL,
 CONSTRAINT [PK_CampaignTargetProduct_1] PRIMARY KEY CLUSTERED 
(
	[CampaignId] ASC,
	[MasterCatalogProductId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CampaignRelatedProduct]    Script Date: 05/25/2010 15:23:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CampaignRelatedProduct](
	[CampaignId] [int] NOT NULL,
	[MasterCatalogProductId] [int] NOT NULL,
 CONSTRAINT [PK_CampaignRelatedProduct] PRIMARY KEY CLUSTERED 
(
	[CampaignId] ASC,
	[MasterCatalogProductId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_AdMedia_UserId]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[AdMedia] ADD  CONSTRAINT [DF_AdMedia_UserId]  DEFAULT ((0)) FOR [CreatedUserId]
GO
/****** Object:  Default [DF_AdMedia_CreatedDateTime]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[AdMedia] ADD  CONSTRAINT [DF_AdMedia_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
/****** Object:  Default [DF_AdMedia_ModifiedUserId]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[AdMedia] ADD  CONSTRAINT [DF_AdMedia_ModifiedUserId]  DEFAULT ((0)) FOR [ModifiedUserId]
GO
/****** Object:  Default [DF_AdMedia_ModifiedDateTime]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[AdMedia] ADD  CONSTRAINT [DF_AdMedia_ModifiedDateTime]  DEFAULT (getdate()) FOR [ModifiedDateTime]
GO
/****** Object:  Default [DF_AdMedia_IsActive]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[AdMedia] ADD  CONSTRAINT [DF_AdMedia_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF_Campaign_UserId]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[Campaign] ADD  CONSTRAINT [DF_Campaign_UserId]  DEFAULT ((0)) FOR [CreatedUserId]
GO
/****** Object:  Default [DF_Campaign_UpdatedDateTime]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[Campaign] ADD  CONSTRAINT [DF_Campaign_UpdatedDateTime]  DEFAULT (getdate()) FOR [ModifiedDateTime]
GO
/****** Object:  Default [DF_CampaignClickThrough_CreatedDateTime]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[CampaignClickThrough] ADD  CONSTRAINT [DF_CampaignClickThrough_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
/****** Object:  Default [DF_CampaignImpression_CreatedDateTime]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[CampaignImpression] ADD  CONSTRAINT [DF_CampaignImpression_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
/****** Object:  ForeignKey [FK_AdMedia_AdLocation]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[AdMedia]  WITH CHECK ADD  CONSTRAINT [FK_AdMedia_AdLocation] FOREIGN KEY([AdLocationId])
REFERENCES [dbo].[AdLocation] ([Id])
GO
ALTER TABLE [dbo].[AdMedia] CHECK CONSTRAINT [FK_AdMedia_AdLocation]
GO
/****** Object:  ForeignKey [FK_AdMedia_AdMediaType]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[AdMedia]  WITH CHECK ADD  CONSTRAINT [FK_AdMedia_AdMediaType] FOREIGN KEY([AdMediaTypeId])
REFERENCES [dbo].[AdMediaType] ([Id])
GO
ALTER TABLE [dbo].[AdMedia] CHECK CONSTRAINT [FK_AdMedia_AdMediaType]
GO
/****** Object:  ForeignKey [FK_AdMedia_User]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[AdMedia]  WITH CHECK ADD  CONSTRAINT [FK_AdMedia_User] FOREIGN KEY([CreatedUserId])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[AdMedia] CHECK CONSTRAINT [FK_AdMedia_User]
GO
/****** Object:  ForeignKey [FK_AdMedia_User1]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[AdMedia]  WITH CHECK ADD  CONSTRAINT [FK_AdMedia_User1] FOREIGN KEY([ModifiedUserId])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[AdMedia] CHECK CONSTRAINT [FK_AdMedia_User1]
GO
/****** Object:  ForeignKey [FK_Campaign_AdMedia]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[Campaign]  WITH CHECK ADD  CONSTRAINT [FK_Campaign_AdMedia] FOREIGN KEY([AdMediaId])
REFERENCES [dbo].[AdMedia] ([Id])
GO
ALTER TABLE [dbo].[Campaign] CHECK CONSTRAINT [FK_Campaign_AdMedia]
GO
/****** Object:  ForeignKey [FK_Campaign_User]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[Campaign]  WITH CHECK ADD  CONSTRAINT [FK_Campaign_User] FOREIGN KEY([CreatedUserId])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Campaign] CHECK CONSTRAINT [FK_Campaign_User]
GO
/****** Object:  ForeignKey [FK_Campaign_User1]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[Campaign]  WITH CHECK ADD  CONSTRAINT [FK_Campaign_User1] FOREIGN KEY([ModifiedUserId])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Campaign] CHECK CONSTRAINT [FK_Campaign_User1]
GO
/****** Object:  ForeignKey [FK_CampaignClickThrough_Campaign]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[CampaignClickThrough]  WITH CHECK ADD  CONSTRAINT [FK_CampaignClickThrough_Campaign] FOREIGN KEY([CampaignId])
REFERENCES [dbo].[Campaign] ([Id])
GO
ALTER TABLE [dbo].[CampaignClickThrough] CHECK CONSTRAINT [FK_CampaignClickThrough_Campaign]
GO
/****** Object:  ForeignKey [FK_CampaignClickThrough_User]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[CampaignClickThrough]  WITH CHECK ADD  CONSTRAINT [FK_CampaignClickThrough_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[CampaignClickThrough] CHECK CONSTRAINT [FK_CampaignClickThrough_User]
GO
/****** Object:  ForeignKey [FK_CampaignImpression_Campaign]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[CampaignImpression]  WITH CHECK ADD  CONSTRAINT [FK_CampaignImpression_Campaign] FOREIGN KEY([CampaignId])
REFERENCES [dbo].[Campaign] ([Id])
GO
ALTER TABLE [dbo].[CampaignImpression] CHECK CONSTRAINT [FK_CampaignImpression_Campaign]
GO
/****** Object:  ForeignKey [FK_CampaignImpression_User]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[CampaignImpression]  WITH CHECK ADD  CONSTRAINT [FK_CampaignImpression_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[CampaignImpression] CHECK CONSTRAINT [FK_CampaignImpression_User]
GO
/****** Object:  ForeignKey [FK_CampaignRelatedProduct_Campaign]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[CampaignRelatedProduct]  WITH CHECK ADD  CONSTRAINT [FK_CampaignRelatedProduct_Campaign] FOREIGN KEY([CampaignId])
REFERENCES [dbo].[Campaign] ([Id])
GO
ALTER TABLE [dbo].[CampaignRelatedProduct] CHECK CONSTRAINT [FK_CampaignRelatedProduct_Campaign]
GO
/****** Object:  ForeignKey [FK_CampaignRelatedProduct_MasterCatalogProduct]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[CampaignRelatedProduct]  WITH CHECK ADD  CONSTRAINT [FK_CampaignRelatedProduct_MasterCatalogProduct] FOREIGN KEY([MasterCatalogProductId])
REFERENCES [dbo].[MasterCatalogProduct] ([MasterCatalogProductID])
GO
ALTER TABLE [dbo].[CampaignRelatedProduct] CHECK CONSTRAINT [FK_CampaignRelatedProduct_MasterCatalogProduct]
GO
/****** Object:  ForeignKey [FK_CampaignTargetProduct_Campaign]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[CampaignTargetProduct]  WITH CHECK ADD  CONSTRAINT [FK_CampaignTargetProduct_Campaign] FOREIGN KEY([CampaignId])
REFERENCES [dbo].[Campaign] ([Id])
GO
ALTER TABLE [dbo].[CampaignTargetProduct] CHECK CONSTRAINT [FK_CampaignTargetProduct_Campaign]
GO
/****** Object:  ForeignKey [FK_CampaignTargetProduct_MasterCatalogProduct]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[CampaignTargetProduct]  WITH CHECK ADD  CONSTRAINT [FK_CampaignTargetProduct_MasterCatalogProduct] FOREIGN KEY([MasterCatalogProductId])
REFERENCES [dbo].[MasterCatalogProduct] ([MasterCatalogProductID])
GO
ALTER TABLE [dbo].[CampaignTargetProduct] CHECK CONSTRAINT [FK_CampaignTargetProduct_MasterCatalogProduct]
GO
/****** Object:  ForeignKey [FK_CampaignUserState_AdLocation]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[CampaignUserState]  WITH CHECK ADD  CONSTRAINT [FK_CampaignUserState_AdLocation] FOREIGN KEY([AdLocationId])
REFERENCES [dbo].[AdLocation] ([Id])
GO
ALTER TABLE [dbo].[CampaignUserState] CHECK CONSTRAINT [FK_CampaignUserState_AdLocation]
GO
/****** Object:  ForeignKey [FK_CampaignUserState_Campaign]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[CampaignUserState]  WITH CHECK ADD  CONSTRAINT [FK_CampaignUserState_Campaign] FOREIGN KEY([CampaignId])
REFERENCES [dbo].[Campaign] ([Id])
GO
ALTER TABLE [dbo].[CampaignUserState] CHECK CONSTRAINT [FK_CampaignUserState_Campaign]
GO
/****** Object:  ForeignKey [FK_CampaignUserState_User]    Script Date: 05/25/2010 15:23:41 ******/
ALTER TABLE [dbo].[CampaignUserState]  WITH CHECK ADD  CONSTRAINT [FK_CampaignUserState_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[CampaignUserState] CHECK CONSTRAINT [FK_CampaignUserState_User]
GO
