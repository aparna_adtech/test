INSERT
INTO            AdMedia(Name, AdMediaTypeId, Url, AdLocationId, CreatedUserId, CreatedDateTime, ModifiedUserId, ModifiedDateTime, IsActive)
VALUES     ('Fusion Knee Brace',1,'FusionKneeBraceHorz.gif',1,1,GetDate(),1,GetDate(),1)

----------------------------------------

INSERT INTO Campaign(	Name, 
						ImpressionDuration, 
						AdMediaId, 
						ContactEmail, 
						StartDateTime, 
						EndDateTime, 
						CreatedUserId, 
						CreatedDateTime, 
						ModifiedDateTime, 
						ModifiedUserId, 
						IsActive, 
						URL)
VALUES     ('Fusion Knee Brace',
			120,
			5,
			'gross@breg.com',
			'2010-09-24',
			'2019-09-24',
			1,
			GETDATE(),
			GETDATE(),
			1,
			1,
			'http://www.breg.com/knee-bracing/ligament/fusion-xt.html')
			
----------------------------------------------		
INSERT     
INTO            CampaignTargetProduct(CampaignId, MasterCatalogProductId)
VALUES     ( 5,469)
	