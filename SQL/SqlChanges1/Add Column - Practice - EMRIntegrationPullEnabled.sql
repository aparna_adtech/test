/*
   Sunday, March 24, 20133:08:25 PM
   User: sa
   Server: vision.breg.breginc.com
   Database: BregVision
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Practice ADD
	EMRIntegrationPullEnabled bit NOT NULL CONSTRAINT DF_Practice_EMRIntegrationPullEnabled DEFAULT 0
GO
ALTER TABLE dbo.Practice SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
