USE [BregVisionEMR]
GO

/****** Object:  Table [dbo].[outbound_hl7_config]    Script Date: 5/1/2014 5:57:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[outbound_hl7_config](
	[outbound_hl7_config_id] [uniqueidentifier] NOT NULL,
	[practice_id] [int] NOT NULL,
	[hl7_message_type] [varchar](10) NOT NULL,
 CONSTRAINT [PK_outbound_hl7_config] PRIMARY KEY CLUSTERED 
(
	[outbound_hl7_config_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[outbound_hl7_config] ADD  CONSTRAINT [DF_outbound_hl7_config_outbound_hl7_config_id]  DEFAULT (newid()) FOR [outbound_hl7_config_id]
GO

ALTER TABLE [dbo].[outbound_hl7_config] ADD  CONSTRAINT [DF_outbound_hl7_config_hl7_message_type]  DEFAULT ('DFT') FOR [hl7_message_type]
GO


