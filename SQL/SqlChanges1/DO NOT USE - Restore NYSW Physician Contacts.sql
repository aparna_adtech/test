Update Contact set FirstName='Anne', LastName='Calkins', MiddleName='M ', Suffix='M.D.' where ContactID = 2102
Update Contact set FirstName='Joseph', LastName='Catania', MiddleName='A', Suffix='M.D.' where ContactID = 2103
Update Contact set FirstName='Rina', LastName='Davis', MiddleName='C', Suffix='M.D.' where ContactID = 2104
Update Contact set FirstName='Eric', LastName='Tallarico', MiddleName='A', Suffix='M.D.' where ContactID = 2105
Update Contact set FirstName='Robert', LastName='Tiso', MiddleName='L', Suffix='M.D.' where ContactID = 2106
Update Contact set FirstName='Mary', LastName='Trusilo', MiddleName='C', Suffix='M.D.' where ContactID = 2107
Update Contact set FirstName='Robert', LastName='Richman, RPAC ATC', MiddleName='C', Suffix='' where ContactID = 2121
Update Contact set FirstName='Carol', LastName='Rolince, MS ANP', MiddleName='A', Suffix='' where ContactID = 2122
Update Contact set FirstName='H. Jean ', LastName='Landcastle, MS ANP', MiddleName='', Suffix='' where ContactID = 2123
Update Contact set FirstName='Zoryana ', LastName='Moreau, MS FNP', MiddleName='', Suffix='' where ContactID = 2124
Update Contact set FirstName='Anne Marie ', LastName='Arendt, RPA', MiddleName='', Suffix='' where ContactID = 2125
Update Contact set FirstName='Jill', LastName='Malinowski, MS FNP', MiddleName='', Suffix='' where ContactID = 2126
Update Contact set FirstName='Suzanne', LastName='Shafer, MS ANP', MiddleName='', Suffix='' where ContactID = 2127
Update Contact set FirstName='Mark', LastName='Profetto, MS FNP', MiddleName='', Suffix='' where ContactID = 2128
Update Contact set FirstName='Mara', LastName='Morabito, FNP', MiddleName='M', Suffix='' where ContactID = 2129
Update Contact set FirstName='Karen', LastName='Cleveland, ANP', MiddleName='M', Suffix='' where ContactID = 2130
Update Contact set FirstName='Daniel', LastName='Bailey, DC', MiddleName='T', Suffix='' where ContactID = 2131
Update Contact set FirstName='Kimberly', LastName='Brown, DC', MiddleName='S', Suffix='' where ContactID = 2132
Update Contact set FirstName='Clinton', LastName='Conger, DC', MiddleName='C', Suffix='' where ContactID = 2133
Update Contact set FirstName='Denise', LastName='Karsten, DC', MiddleName='A', Suffix='' where ContactID = 2134
Update Contact set FirstName='Nicole', LastName='Mitchell, DC', MiddleName='L', Suffix='' where ContactID = 2135

Update Contact set FirstName='Select', LastName='Provider', MiddleName='', Suffix='' where ContactID = 2566
Update Contact set FirstName='', LastName='LaFave', MiddleName='', Suffix='' where ContactID = 2567
Update Contact set FirstName='Shrinkage', LastName='Taft', MiddleName='', Suffix='' where ContactID = 2568
Update Contact set FirstName='Shrinkage', LastName='WW', MiddleName='', Suffix='' where ContactID = 2616
GO
------------------------------------------------------------
SELECT *
  FROM [BregVision].[dbo].[DispenseDetail]
  where 
  
  
  DispenseID in
  (
  
		Select 
			DispenseID 	
		from 
			Dispense 
		where 
			(
				DateDispensed between '8/4/2010' and '9/5/2010'
				and PracticeLocationID = 470 /* widewaters */
			)
			OR
			(
				DateDispensed between '8/6/2010' and '9/5/2010'
				and PracticeLocationID = 471 /* Taft */	
			)
		  
  )

----------------------------------------------------
/*
Update [DispenseDetail] Set PhysicianID = 0 where PhysicianID = 0 AND DispenseID in ( Select D.DispenseID from Dispense D where ( D.DateDispensed between '8/4/2010' and '9/5/2010' and D.PracticeLocationID = 470 /* widewaters */ ) OR ( D.DateDispensed between '8/6/2010' and '9/5/2010' and D.PracticeLocationID = 471 /* Taft */))
*/