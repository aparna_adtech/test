USE [BregVision]
GO
CREATE NONCLUSTERED INDEX IX_DispenseQueue_BregVisionOrderID
ON [dbo].[DispenseQueue] ([BregVisionOrderID])
INCLUDE ([ProductInventoryID])
GO

