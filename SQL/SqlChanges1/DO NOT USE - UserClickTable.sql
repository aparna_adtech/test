USE [BregVision]
GO

/****** Object:  Table [dbo].[UserClick]    Script Date: 05/05/2009 12:15:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UserClick](
	[UserClickID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[UserName] [nvarchar](100) NOT NULL,
	[PracticeID] [int] NOT NULL,
	[PracticeLocationID] [int] NULL,
	[PageName] [nvarchar](50) NOT NULL,
	[Area] [nvarchar](50) NOT NULL,
	[Item] [nvarchar](50) NOT NULL,
	[Action] [nvarchar](50) NOT NULL,
	[Promotion] [nvarchar](50) NULL,
	[UserDefined1] [nvarchar](50) NULL,
	[UserDefined2] [nvarchar](50) NULL,
	[UserDefined3] [nvarchar](50) NULL,
	[UserDefined4] [nvarchar](50) NULL,
 CONSTRAINT [PK_UserClick] PRIMARY KEY CLUSTERED 
(
	[UserClickID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The area on the page that was clicked' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserClick', @level2type=N'COLUMN',@level2name=N'Area'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The item on the page that was clicked' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserClick', @level2type=N'COLUMN',@level2name=N'Item'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The specific action that caused the logging' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserClick', @level2type=N'COLUMN',@level2name=N'Action'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A Breg promotion or special to which the clicked item belongs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserClick', @level2type=N'COLUMN',@level2name=N'Promotion'
GO

