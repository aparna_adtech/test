USE [BregVision]
GO

/****** Object:  Table [dbo].[ShippingCarrier]    Script Date: 06/23/2009 12:16:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ShippingCarrier]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ShippingCarrier](
	[ShippingCarrierID] [int] NOT NULL,
	[ShippingCarrierName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ShippingCarrier] PRIMARY KEY CLUSTERED 
(
	[ShippingCarrierID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO


Insert into ShippingCarrier(ShippingCarrierID, ShippingCarrierName) Values(1, 'UPS')
  go      
Insert into ShippingCarrier(ShippingCarrierID, ShippingCarrierName) Values(2, 'Fed Ex')
  go