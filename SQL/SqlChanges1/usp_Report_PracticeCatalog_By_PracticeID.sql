
alter PROC [dbo].[usp_Report_PracticeCatalog_By_PracticeID]

@PracticeID int = -1
AS
BEGIN

SELECT 
	  pcsb.BrandName AS Manufacturer
	  ,COALESCE(mcp.Code ,tpp.Code) AS 'Manuf prod #'
	  ,pcsb.SupplierName AS 'Vendor'
	  ,COALESCE(mcp.Size, tpp.Size) AS 'Size'
	  ,COALESCE(mcp.LeftRightSide, tpp.LeftRightSide) AS 'Side'
		,BregVision.dbo.ConcatHCPCS(pcp.PracticeCatalogProductID) AS HCPCS
		,COALESCE(mcp.Name, tpp.Name) AS 'Product Description'
      ,[WholesaleCost] AS 'Cost'
      ,[BillingCharge] AS 'Bill Chg'
      ,[BillingChargeCash] AS 'Bill Chg Cash'
  FROM [BregVision].[dbo].[PracticeCatalogProduct] pcp
  LEFT JOIN BregVision.dbo.PracticeCatalogSupplierBrand pcsb ON pcsb.PracticeCatalogSupplierBrandID = pcp.PracticeCatalogSupplierBrandID
  LEFT JOIN BregVision.dbo.MasterCatalogProduct mcp ON mcp.MasterCatalogProductID = pcp.MasterCatalogProductID
  LEFT JOIN BregVision.dbo.MasterCatalogSupplier mcs ON mcs.MasterCatalogSupplierID = pcsb.MasterCatalogSupplierID
  LEFT JOIN BregVision.dbo.ThirdPartyProduct tpp ON tpp.ThirdPartyProductID = pcp.ThirdPartyProductID
  LEFT JOIN BregVision.dbo.ThirdPartySupplier tps ON tps.ThirdPartySupplierID = pcsb.ThirdPartySupplierID
  WHERE pcp.PracticeID = @PracticeID
  --AND pcp.IsActive = 1
  --AND (mcp.IsActive = 1 OR tpp.IsActive = 1)
  --AND pcsb.IsActive = 1
 

END
