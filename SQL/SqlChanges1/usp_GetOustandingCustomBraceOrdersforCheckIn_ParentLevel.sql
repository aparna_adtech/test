USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOustandingCustomBraceOrdersforCheckIn_ParentLevel]    Script Date: 06/13/2009 14:35:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mike Sneen
-- Create date: 06/13/09
-- Description:	Used to get the parent table for the oustanding Custom Brace Orders in the Outstanding Custom Brace Order section of the Check-in UI. 
-- Exec usp_GetOustandingCustomBraceOrdersforCheckIn_ParentLevel 3
-- =============================================
Create PROCEDURE [dbo].[usp_GetOustandingCustomBraceOrdersforCheckIn_ParentLevel]
	@PracticeLocationID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	SELECT 
	  BVO.BregVisionOrderID
	, BVO.CustomPurchaseOrderCode
	, BVO.PurchaseOrder
	, BVO.CreatedUserID
	, BVOS.Status
	, ST.ShippingTypeName as ShippingType
	, BVO.Total
	, BVO.CreatedDate 
	, SCCB.PatientName
	FROM BregVisionOrder AS BVO
	INNER JOIN BregVisionOrderStatus AS BVOS 
		ON BVO.BregVisionOrderStatusID = BVOS.BregVisionOrderStatusID
	INNER JOIN ShoppingCartCustomBrace SCCB
		ON SCCB.BregVisionOrderID = BVO.BregVisionOrderID
	INNER JOIN ShippingType AS ST 
		ON BVO.ShippingTypeID = ST.ShippingTypeID
	WHERE BVO.BregVisionOrderStatusID IN (1,3) 
		AND BVO.isactive = 1 
		AND BVO.PracticeLocationID = @PracticeLocationID 
	ORDER BY CreatedDate

END