USE [BregVision]
GO

/****** Object:  Table [dbo].[FaxMessage]    Script Date: 08/23/2009 22:03:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FaxMessage](
	[FaxMessageID] [int] IDENTITY(1,1) NOT NULL,
	[FaxNumber] [varchar](50) NOT NULL,
	[FaxFileName] [varchar](250) NOT NULL,
	[Sent] [bit] NOT NULL,
	[Skip] [bit] NOT NULL,
	[SentTime] [datetime] NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_FaxMessage] PRIMARY KEY CLUSTERED 
(
	[FaxMessageID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

--SET ANSI_PADDING OFF
--GO

ALTER TABLE [dbo].[FaxMessage] ADD  CONSTRAINT [DF_FaxMessage_Sent]  DEFAULT ((0)) FOR [Sent]
GO

ALTER TABLE [dbo].[FaxMessage] ADD  CONSTRAINT [DF_FaxMessage_Skip]  DEFAULT ((0)) FOR [Skip]
GO

ALTER TABLE [dbo].[FaxMessage] ADD  CONSTRAINT [DF_FaxMessage_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO


