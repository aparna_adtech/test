USE [BregVision]
GO
/****** Object:  UserDefinedFunction [dbo].[CompareDelimitedStrings]    Script Date: 11/17/2015 4:59:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--  SELECT * FROM dbo.ProductHCPCS AS ph
--  SELECT [dbo].[ConcatHCPCS](1072) 

CREATE FUNCTION [dbo].[CompareDelimitedStrings](@Source VARCHAR(max), @Target VARCHAR(max), @Delimiter VARCHAR(10))
RETURNS BIT
AS
BEGIN
	DECLARE @SourceCount INT
	DECLARE @SourceItem VARCHAR(50)
	DECLARE @SourceTable TABLE ( TextValue VARCHAR(50) ) 
	DECLARE @TargetTable TABLE ( TextValue VARCHAR(50) ) 
	DECLARE @FoundTable TABLE ( Found BIT ) 

	INSERT INTO @SourceTable SELECT RTRIM(LTRIM(TextValue)) FROM dbo.udf_ParseStringArrayToTable(@Source, @Delimiter)
	INSERT INTO @TargetTable SELECT RTRIM(LTRIM(TextValue)) FROM dbo.udf_ParseStringArrayToTable(@Target, @Delimiter)

	SELECT @SourceCount = COUNT(1) from @SourceTable;

	WHILE @SourceCount > 0
	BEGIN
		SELECT TOP 1 @SourceItem = TextValue FROM @SourceTable
		INSERT INTO @FoundTable SELECT 1 WHERE @SourceItem IN (Select TextValue from @TargetTable)
		IF EXISTS (SELECT Found FROM @FoundTable)
			RETURN 1

		DELETE FROM @SourceTable WHERE TextValue = @SourceItem
		SELECT @SourceCount = COUNT(1) from @SourceTable;
	END

	RETURN 0
END
