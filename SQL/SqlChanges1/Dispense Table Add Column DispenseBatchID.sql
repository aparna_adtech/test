use [BregVision]
go

if not exists (select * from dbo.syscolumns where id = object_id(N'Dispense') and name=(N'DispenseBatchID'))
alter table [Dispense] add DispenseBatchID int NULL
GO