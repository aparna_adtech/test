﻿Delete from ICD9
Go

Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('337.22', 'L1843', 'Reflex sympathetic dystrophy of the lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('343.9', 'L1843', 'Unspecified infantile cerebral palsy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.9', 'L1843', 'Unspecified paralysis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L1843', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.1', 'L1843', 'Felty''s syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.2', 'L1843', 'Other rheumatoid arthritis with visceral or systemic involvement');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.3', 'L1843', 'Polyarticular juvenile rheumatoid arthritis, chronic or unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.31', 'L1843', 'Polyarticular juvenile rheumatoid arthritis, acute');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.32', 'L1843', 'Pauciarticular juvenile rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.33', 'L1843', 'Monoarticular juvenile rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.4', 'L1843', 'Chronic postrheumatic arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.89', 'L1843', 'Other specified inflammatory polyarthropathies');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.9', 'L1843', 'Unspecified inflammatory polyarthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L1843', 'Osteoarthrosis, generalized');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.16', 'L1843', 'Primary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.26', 'L1843', 'Secondary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.36', 'L1843', 'Localized osteoarthrosis not specified whether primary or secondary, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.96', 'L1843', 'Osteoarthrosis, unspecified whether generalized or localized, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.06', 'L1843', 'Kaschin-Beck disease, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.16', 'L1843', 'Traumatic arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.26', 'L1843', 'Allergic arthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.36', 'L1843', 'Climacteric arthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.46', 'L1843', 'Transient arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.56', 'L1843', 'Unspecified polyarthropathy or polyarthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.66', 'L1843', 'Unspecified monoarthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.86', 'L1843', 'Other specified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.96', 'L1843', 'Unspecified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717', 'L1843', 'Old bucket handle tear of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.1', 'L1843', 'Derangement of anterior horn of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.2', 'L1843', 'Derangement of posterior horn of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.3', 'L1843', 'Other and unspecified derangement of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.4', 'L1843', 'Unspecified derangement of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.41', 'L1843', 'Bucket handle tear of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.42', 'L1843', 'Derangement of anterior horn of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.43', 'L1843', 'Derangement of posterior horn of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.49', 'L1843', 'Other derangement of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.5', 'L1843', 'Derangement of meniscus, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.6', 'L1843', 'Loose body in knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.7', 'L1843', 'Chondromalacia of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.81', 'L1843', 'Old disruption of lateral collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.82', 'L1843', 'Old disruption of medial collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.83', 'L1843', 'Old disruption of anterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.84', 'L1843', 'Old disruption of posterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.85', 'L1843', 'Old disruption of other ligament of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.89', 'L1843', 'Other internal derangement of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.9', 'L1843', 'Unspecified internal derangement of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.26', 'L1843', 'Pathological dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.29', 'L1843', 'Pathological dislocation of joint of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.36', 'L1843', 'Recurrent dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.39', 'L1843', 'Recurrent dislocation of joint of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.46', 'L1843', 'Contracture of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.49', 'L1843', 'Contracture of joint of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.56', 'L1843', 'Ankylosis of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.59', 'L1843', 'Ankylosis of joint of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.76', 'L1843', 'Developmental dislocation of joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.86', 'L1843', 'Other joint derangement, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.06', 'L1843', 'Effusion of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.09', 'L1843', 'Effusion of joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.16', 'L1843', 'Hemarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.19', 'L1843', 'Hemarthrosis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.26', 'L1843', 'Villonodular synovitis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.29', 'L1843', 'Villonodular synovitis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.36', 'L1843', 'Palindromic rheumatism, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.39', 'L1843', 'Palindromic rheumatism, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.46', 'L1843', 'Pain in joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.49', 'L1843', 'Pain in joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.56', 'L1843', 'Stiffness of joint, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.59', 'L1843', 'Stiffness of joints, not elsewhere classified, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.66', 'L1843', 'Other symptoms referable to lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.69', 'L1843', 'Other symptoms referable to joints of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.7', 'L1843', 'Difficulty in walking');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.8', 'L1843', 'Other specified disorders of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.86', 'L1843', 'Other specified disorders of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.89', 'L1843', 'Other specified disorders of joints of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.96', 'L1843', 'Unspecified disorder of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.6', 'L1843', 'Unspecified enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.61', 'L1843', 'Pes anserinus tendinitis or bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.62', 'L1843', 'Tibial collateral ligament bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.63', 'L1843', 'Fibular collateral ligament bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.64', 'L1843', 'Patellar tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.65', 'L1843', 'Prepatellar bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.69', 'L1843', 'Other enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.7', 'L1843', 'Unspecified enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.71', 'L1843', 'Achilles bursitis or tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.72', 'L1843', 'Tibialis tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L1843', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.91', 'L1843', 'Exostosis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L1843', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.6', 'L1843', 'Residual foreign body in soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.81', 'L1843', 'Swelling of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.82', 'L1843', 'Cramp of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.89', 'L1843', 'Other musculoskeletal symptoms referable to limbs');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.9', 'L1843', 'Other and unspecified disorders of soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.18', 'L1843', 'Osteoarthritis nonspecific site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732', 'L1843', 'Osteochondrosis, juvenile');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733', 'L1843', 'Osteoporosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.92', 'L1843', 'Chondromalacia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.93', 'L1843', 'Stress fracture of tibia or fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.94', 'L1843', 'Stress fracture of the metatarsals');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.95', 'L1843', 'Stress fracture of other bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.99', 'L1843', 'Other disorders of bone and cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.69', 'L1843', 'Other congenital anomaly of lower limb, including pelvic girdle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.8', 'L1843', 'Other specified congenital anomalies of unspecified limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.9', 'L1843', 'Unspecified congenital anomaly of unspecified limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('822', 'L1843', 'Closed fracture of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('822.1', 'L1843', 'Open fracture of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836', 'L1843', 'Tear of medial cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.1', 'L1843', 'Tear of lateral cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.2', 'L1843', 'Other tear of cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.3', 'L1843', 'Closed dislocation of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.4', 'L1843', 'Open dislocation of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.5', 'L1843', 'Closed dislocation of knee, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.51', 'L1843', 'Closed anterior dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.52', 'L1843', 'Closed posterior dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.53', 'L1843', 'Closed medial dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.54', 'L1843', 'Closed lateral dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.59', 'L1843', 'Other closed dislocation of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.6', 'L1843', 'Open dislocation of knee unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.61', 'L1843', 'Open anterior dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.62', 'L1843', 'Open posterior dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.63', 'L1843', 'Open medial dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.64', 'L1843', 'Open lateral dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.69', 'L1843', 'Other open dislocation of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.1', 'L1843', 'Sprain and strain of medial collateral ligament of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.2', 'L1843', 'Sprain and strain of cruciate ligament of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.3', 'L1843', 'Sprain and strain of tibiofibular (joint) (ligament) superior, of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.8', 'L1843', 'Sprain and strain of other specified sites of knee and leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.9', 'L1843', 'Sprain and strain of unspecified site of knee and leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.1', 'L1843', 'Contusion of lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.11', 'L1843', 'Contusion of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.4', 'L1843', 'Contusion of multiple sites of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.5', 'L1843', 'Contusion of unspecified part of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.8', 'L1843', 'Contusion of multiple sites, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.9', 'L1843', 'Contusion of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.7', 'L1843', 'Injury, other and unspecified, knee, leg, ankle, and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.8', 'L1843', 'Injury, other and unspecified, other specified sites, including multiple');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.9', 'L1843', 'Injury, other and unspecified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.59', 'L1843', 'Other immobilization, pressure, and attention to wound');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('337.22', 'L1844', 'Reflex sympathetic dystrophy of the lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('343.9', 'L1844', 'Unspecified infantile cerebral palsy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.9', 'L1844', 'Unspecified paralysis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L1844', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.1', 'L1844', 'Felty''s syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.2', 'L1844', 'Other rheumatoid arthritis with visceral or systemic involvement');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.3', 'L1844', 'Polyarticular juvenile rheumatoid arthritis, chronic or unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.31', 'L1844', 'Polyarticular juvenile rheumatoid arthritis, acute');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.32', 'L1844', 'Pauciarticular juvenile rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.33', 'L1844', 'Monoarticular juvenile rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.4', 'L1844', 'Chronic postrheumatic arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.89', 'L1844', 'Other specified inflammatory polyarthropathies');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.9', 'L1844', 'Unspecified inflammatory polyarthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L1844', 'Osteoarthrosis, generalized');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.16', 'L1844', 'Primary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.26', 'L1844', 'Secondary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.36', 'L1844', 'Localized osteoarthrosis not specified whether primary or secondary, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.96', 'L1844', 'Osteoarthrosis, unspecified whether generalized or localized, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.06', 'L1844', 'Kaschin-Beck disease, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.16', 'L1844', 'Traumatic arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.26', 'L1844', 'Allergic arthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.36', 'L1844', 'Climacteric arthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.46', 'L1844', 'Transient arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.56', 'L1844', 'Unspecified polyarthropathy or polyarthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.66', 'L1844', 'Unspecified monoarthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.86', 'L1844', 'Other specified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.96', 'L1844', 'Unspecified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717', 'L1844', 'Old bucket handle tear of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.1', 'L1844', 'Derangement of anterior horn of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.2', 'L1844', 'Derangement of posterior horn of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.3', 'L1844', 'Other and unspecified derangement of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.4', 'L1844', 'Unspecified derangement of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.41', 'L1844', 'Bucket handle tear of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.42', 'L1844', 'Derangement of anterior horn of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.43', 'L1844', 'Derangement of posterior horn of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.49', 'L1844', 'Other derangement of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.5', 'L1844', 'Derangement of meniscus, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.6', 'L1844', 'Loose body in knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.7', 'L1844', 'Chondromalacia of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.81', 'L1844', 'Old disruption of lateral collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.82', 'L1844', 'Old disruption of medial collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.83', 'L1844', 'Old disruption of anterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.84', 'L1844', 'Old disruption of posterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.85', 'L1844', 'Old disruption of other ligament of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.89', 'L1844', 'Other internal derangement of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.9', 'L1844', 'Unspecified internal derangement of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.26', 'L1844', 'Pathological dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.29', 'L1844', 'Pathological dislocation of joint of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.36', 'L1844', 'Recurrent dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.39', 'L1844', 'Recurrent dislocation of joint of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.46', 'L1844', 'Contracture of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.49', 'L1844', 'Contracture of joint of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.56', 'L1844', 'Ankylosis of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.59', 'L1844', 'Ankylosis of joint of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.76', 'L1844', 'Developmental dislocation of joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.86', 'L1844', 'Other joint derangement, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.06', 'L1844', 'Effusion of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.09', 'L1844', 'Effusion of joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.16', 'L1844', 'Hemarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.19', 'L1844', 'Hemarthrosis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.26', 'L1844', 'Villonodular synovitis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.29', 'L1844', 'Villonodular synovitis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.36', 'L1844', 'Palindromic rheumatism, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.39', 'L1844', 'Palindromic rheumatism, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.46', 'L1844', 'Pain in joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.49', 'L1844', 'Pain in joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.56', 'L1844', 'Stiffness of joint, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.59', 'L1844', 'Stiffness of joints, not elsewhere classified, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.66', 'L1844', 'Other symptoms referable to lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.69', 'L1844', 'Other symptoms referable to joints of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.7', 'L1844', 'Difficulty in walking');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.8', 'L1844', 'Other specified disorders of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.86', 'L1844', 'Other specified disorders of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.89', 'L1844', 'Other specified disorders of joints of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.96', 'L1844', 'Unspecified disorder of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.6', 'L1844', 'Unspecified enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.61', 'L1844', 'Pes anserinus tendinitis or bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.62', 'L1844', 'Tibial collateral ligament bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.63', 'L1844', 'Fibular collateral ligament bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.64', 'L1844', 'Patellar tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.65', 'L1844', 'Prepatellar bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.69', 'L1844', 'Other enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.7', 'L1844', 'Unspecified enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.71', 'L1844', 'Achilles bursitis or tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.72', 'L1844', 'Tibialis tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L1844', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.91', 'L1844', 'Exostosis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L1844', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.6', 'L1844', 'Residual foreign body in soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.81', 'L1844', 'Swelling of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.82', 'L1844', 'Cramp of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.89', 'L1844', 'Other musculoskeletal symptoms referable to limbs');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.9', 'L1844', 'Other and unspecified disorders of soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.18', 'L1844', 'Osteoarthritis nonspecific site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732', 'L1844', 'Osteochondrosis, juvenile');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733', 'L1844', 'Osteoporosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.95', 'L1844', 'Stress fracture of other bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.99', 'L1844', 'Other disorders of bone and cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.69', 'L1844', 'Other congenital anomaly of lower limb, including pelvic girdle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.8', 'L1844', 'Other specified congenital anomalies of unspecified limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.9', 'L1844', 'Unspecified congenital anomaly of unspecified limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('822', 'L1844', 'Closed fracture of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('822.1', 'L1844', 'Open fracture of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836', 'L1844', 'Tear of medial cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.1', 'L1844', 'Tear of lateral cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.2', 'L1844', 'Other tear of cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.3', 'L1844', 'Closed dislocation of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.4', 'L1844', 'Open dislocation of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.5', 'L1844', 'Closed dislocation of knee, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.51', 'L1844', 'Closed anterior dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.52', 'L1844', 'Closed posterior dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.53', 'L1844', 'Closed medial dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.54', 'L1844', 'Closed lateral dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.59', 'L1844', 'Other closed dislocation of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.6', 'L1844', 'Open dislocation of knee unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.61', 'L1844', 'Open anterior dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.62', 'L1844', 'Open posterior dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.63', 'L1844', 'Open medial dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.64', 'L1844', 'Open lateral dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.69', 'L1844', 'Other open dislocation of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.1', 'L1844', 'Sprain and strain of medial collateral ligament of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.2', 'L1844', 'Sprain and strain of cruciate ligament of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.3', 'L1844', 'Sprain and strain of tibiofibular (joint) (ligament) superior, of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.8', 'L1844', 'Sprain and strain of other specified sites of knee and leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.9', 'L1844', 'Sprain and strain of unspecified site of knee and leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.1', 'L1844', 'Contusion of lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.11', 'L1844', 'Contusion of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.4', 'L1844', 'Contusion of multiple sites of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.5', 'L1844', 'Contusion of unspecified part of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.8', 'L1844', 'Contusion of multiple sites, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.9', 'L1844', 'Contusion of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.7', 'L1844', 'Injury, other and unspecified, knee, leg, ankle, and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.8', 'L1844', 'Injury, other and unspecified, other specified sites, including multiple');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.9', 'L1844', 'Injury, other and unspecified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.59', 'L1844', 'Other immobilization, pressure, and attention to wound');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.23', 'L1844', 'Fitting of orthotic device');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('337.22', 'L1846', 'Reflex sympathetic dystrophy of the lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('343.9', 'L1846', 'Unspecified infantile cerebral palsy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.9', 'L1846', 'Unspecified paralysis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L1846', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.1', 'L1846', 'Felty''s syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.2', 'L1846', 'Other rheumatoid arthritis with visceral or systemic involvement');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.3', 'L1846', 'Polyarticular juvenile rheumatoid arthritis, chronic or unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.31', 'L1846', 'Polyarticular juvenile rheumatoid arthritis, acute');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.32', 'L1846', 'Pauciarticular juvenile rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.33', 'L1846', 'Monoarticular juvenile rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.4', 'L1846', 'Chronic postrheumatic arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.89', 'L1846', 'Other specified inflammatory polyarthropathies');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.9', 'L1846', 'Unspecified inflammatory polyarthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L1846', 'Osteoarthrosis, generalized');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.16', 'L1846', 'Primary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.26', 'L1846', 'Secondary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.36', 'L1846', 'Localized osteoarthrosis not specified whether primary or secondary, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.96', 'L1846', 'Osteoarthrosis, unspecified whether generalized or localized, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.06', 'L1846', 'Kaschin-Beck disease, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.16', 'L1846', 'Traumatic arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.26', 'L1846', 'Allergic arthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.36', 'L1846', 'Climacteric arthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.46', 'L1846', 'Transient arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.56', 'L1846', 'Unspecified polyarthropathy or polyarthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.66', 'L1846', 'Unspecified monoarthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.86', 'L1846', 'Other specified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.96', 'L1846', 'Unspecified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717', 'L1846', 'Old bucket handle tear of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.1', 'L1846', 'Derangement of anterior horn of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.2', 'L1846', 'Derangement of posterior horn of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.3', 'L1846', 'Other and unspecified derangement of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.4', 'L1846', 'Unspecified derangement of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.41', 'L1846', 'Bucket handle tear of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.42', 'L1846', 'Derangement of anterior horn of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.43', 'L1846', 'Derangement of posterior horn of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.49', 'L1846', 'Other derangement of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.5', 'L1846', 'Derangement of meniscus, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.6', 'L1846', 'Loose body in knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.7', 'L1846', 'Chondromalacia of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.81', 'L1846', 'Old disruption of lateral collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.82', 'L1846', 'Old disruption of medial collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.83', 'L1846', 'Old disruption of anterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.84', 'L1846', 'Old disruption of posterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.85', 'L1846', 'Old disruption of other ligament of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.89', 'L1846', 'Other internal derangement of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.9', 'L1846', 'Unspecified internal derangement of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.26', 'L1846', 'Pathological dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.29', 'L1846', 'Pathological dislocation of joint of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.36', 'L1846', 'Recurrent dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.39', 'L1846', 'Recurrent dislocation of joint of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.46', 'L1846', 'Contracture of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.49', 'L1846', 'Contracture of joint of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.56', 'L1846', 'Ankylosis of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.59', 'L1846', 'Ankylosis of joint of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.76', 'L1846', 'Developmental dislocation of joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.86', 'L1846', 'Other joint derangement, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.06', 'L1846', 'Effusion of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.09', 'L1846', 'Effusion of joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.16', 'L1846', 'Hemarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.19', 'L1846', 'Hemarthrosis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.26', 'L1846', 'Villonodular synovitis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.29', 'L1846', 'Villonodular synovitis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.36', 'L1846', 'Palindromic rheumatism, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.39', 'L1846', 'Palindromic rheumatism, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.46', 'L1846', 'Pain in joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.49', 'L1846', 'Pain in joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.56', 'L1846', 'Stiffness of joint, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.59', 'L1846', 'Stiffness of joints, not elsewhere classified, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.66', 'L1846', 'Other symptoms referable to lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.69', 'L1846', 'Other symptoms referable to joints of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.7', 'L1846', 'Difficulty in walking');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.8', 'L1846', 'Other specified disorders of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.86', 'L1846', 'Other specified disorders of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.89', 'L1846', 'Other specified disorders of joints of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.96', 'L1846', 'Unspecified disorder of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.6', 'L1846', 'Unspecified enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.61', 'L1846', 'Pes anserinus tendinitis or bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.62', 'L1846', 'Tibial collateral ligament bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.63', 'L1846', 'Fibular collateral ligament bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.64', 'L1846', 'Patellar tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.65', 'L1846', 'Prepatellar bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.69', 'L1846', 'Other enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.7', 'L1846', 'Unspecified enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.71', 'L1846', 'Achilles bursitis or tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.72', 'L1846', 'Tibialis tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L1846', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.91', 'L1846', 'Exostosis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L1846', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.6', 'L1846', 'Residual foreign body in soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.81', 'L1846', 'Swelling of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.82', 'L1846', 'Cramp of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.89', 'L1846', 'Other musculoskeletal symptoms referable to limbs');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.9', 'L1846', 'Other and unspecified disorders of soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.18', 'L1846', 'Osteoarthritis nonspecific site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732', 'L1846', 'Osteochondrosis, juvenile');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733', 'L1846', 'Osteoporosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.95', 'L1846', 'Stress fracture of other bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.99', 'L1846', 'Other disorders of bone and cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.69', 'L1846', 'Other congenital anomaly of lower limb, including pelvic girdle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.8', 'L1846', 'Other specified congenital anomalies of unspecified limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.9', 'L1846', 'Unspecified congenital anomaly of unspecified limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('822', 'L1846', 'Closed fracture of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('822.1', 'L1846', 'Open fracture of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836', 'L1846', 'Tear of medial cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.1', 'L1846', 'Tear of lateral cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.2', 'L1846', 'Other tear of cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.3', 'L1846', 'Closed dislocation of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.4', 'L1846', 'Open dislocation of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.5', 'L1846', 'Closed dislocation of knee, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.51', 'L1846', 'Closed anterior dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.52', 'L1846', 'Closed posterior dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.53', 'L1846', 'Closed medial dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.54', 'L1846', 'Closed lateral dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.59', 'L1846', 'Other closed dislocation of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.6', 'L1846', 'Open dislocation of knee unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.61', 'L1846', 'Open anterior dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.62', 'L1846', 'Open posterior dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.63', 'L1846', 'Open medial dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.64', 'L1846', 'Open lateral dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.69', 'L1846', 'Other open dislocation of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.1', 'L1846', 'Sprain and strain of medial collateral ligament of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.2', 'L1846', 'Sprain and strain of cruciate ligament of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.3', 'L1846', 'Sprain and strain of tibiofibular (joint) (ligament) superior, of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.8', 'L1846', 'Sprain and strain of other specified sites of knee and leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.9', 'L1846', 'Sprain and strain of unspecified site of knee and leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.1', 'L1846', 'Contusion of lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.11', 'L1846', 'Contusion of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.4', 'L1846', 'Contusion of multiple sites of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.5', 'L1846', 'Contusion of unspecified part of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.8', 'L1846', 'Contusion of multiple sites, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.9', 'L1846', 'Contusion of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.7', 'L1846', 'Injury, other and unspecified, knee, leg, ankle, and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.8', 'L1846', 'Injury, other and unspecified, other specified sites, including multiple');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.9', 'L1846', 'Injury, other and unspecified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.59', 'L1846', 'Other immobilization, pressure, and attention to wound');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('335.1', 'L2800', 'Unspecified spinal muscular atrophy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('343.9', 'L2800', 'Unspecified infantile cerebral palsy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.01', 'L2800', 'Quadriplegia and quadriparesis, C1-C4, complete');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.1', 'L2800', 'Paraplegia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.2', 'L2800', 'Diplegia of upper limbs');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('451', 'L2800', 'Phlebitis and thrombophlebitis of superficial vessels of lower extremities');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.16', 'L2800', 'Primary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.36', 'L2800', 'Localized osteoarthrosis not specified whether primary or secondary, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.96', 'L2800', 'Osteoarthrosis, unspecified whether generalized or localized, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.14', 'L2800', 'Traumatic arthropathy, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.96', 'L2800', 'Unspecified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.99', 'L2800', 'Unspecified arthropathy, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.3', 'L2800', 'Other and unspecified derangement of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.7', 'L2800', 'Chondromalacia of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.83', 'L2800', 'Old disruption of anterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.36', 'L2800', 'Recurrent dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.46', 'L2800', 'Contracture of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.86', 'L2800', 'Other joint derangement, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.06', 'L2800', 'Effusion of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.46', 'L2800', 'Pain in joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.86', 'L2800', 'Other specified disorders of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.61', 'L2800', 'Pes anserinus tendinitis or bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.64', 'L2800', 'Patellar tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.69', 'L2800', 'Other enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L2800', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727', 'L2800', 'Unspecified synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.01', 'L2800', 'Synovitis and tenosynovitis in diseases classified elsewhere');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L2800', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.2', 'L2800', 'Specific bursitides often of occupational origin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.89', 'L2800', 'Other disorders of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.3', 'L2800', 'Other specific muscle disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.9', 'L2800', 'Unspecified disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L2800', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.4', 'L2800', 'Juvenile osteochondrosis of lower extremity, excluding foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.6', 'L2800', 'Other juvenile osteochondrosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.7', 'L2800', 'Osteochondritis dissecans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.92', 'L2800', 'Chondromalacia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.5', 'L2800', 'Genu recurvatum (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.79', 'L2800', 'Other acquired deformity of ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.81', 'L2800', 'Unequal leg length (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('741', 'L2800', 'Spina bifida with hydrocephalus, unspecified region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('741.91', 'L2800', 'Spina bifida without mention of hydrocephalus, cervical region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.64', 'L2800', 'Congenital deformity of knee (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.69', 'L2800', 'Other congenital anomaly of lower limb, including pelvic girdle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('805.4', 'L2800', 'Closed fracture of lumbar vertebra without mention of spinal cord injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('822', 'L2800', 'Closed fracture of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823', 'L2800', 'Closed fracture of upper end of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836', 'L2800', 'Tear of medial cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.5', 'L2800', 'Closed dislocation of knee, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.9', 'L2800', 'Sprain and strain of unspecified site of knee and leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845', 'L2800', 'Unspecified site of ankle sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.1', 'L2800', 'Sprain and strain of unspecified site of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847.2', 'L2800', 'Lumbar sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.9', 'L2800', 'Unspecified site of sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.11', 'L2800', 'Contusion of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.7', 'L2800', 'Injury, other and unspecified, knee, leg, ankle, and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('337.22', 'L1845', 'Reflex sympathetic dystrophy of the lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('343.9', 'L1845', 'Unspecified infantile cerebral palsy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.9', 'L1845', 'Unspecified paralysis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L1845', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.1', 'L1845', 'Felty''s syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.2', 'L1845', 'Other rheumatoid arthritis with visceral or systemic involvement');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.3', 'L1845', 'Polyarticular juvenile rheumatoid arthritis, chronic or unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.31', 'L1845', 'Polyarticular juvenile rheumatoid arthritis, acute');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.32', 'L1845', 'Pauciarticular juvenile rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.33', 'L1845', 'Monoarticular juvenile rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.4', 'L1845', 'Chronic postrheumatic arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.89', 'L1845', 'Other specified inflammatory polyarthropathies');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.9', 'L1845', 'Unspecified inflammatory polyarthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L1845', 'Osteoarthrosis, generalized');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.16', 'L1845', 'Primary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.26', 'L1845', 'Secondary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.36', 'L1845', 'Localized osteoarthrosis not specified whether primary or secondary, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.96', 'L1845', 'Osteoarthrosis, unspecified whether generalized or localized, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.06', 'L1845', 'Kaschin-Beck disease, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.16', 'L1845', 'Traumatic arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.26', 'L1845', 'Allergic arthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.36', 'L1845', 'Climacteric arthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.46', 'L1845', 'Transient arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.56', 'L1845', 'Unspecified polyarthropathy or polyarthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.66', 'L1845', 'Unspecified monoarthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.86', 'L1845', 'Other specified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.96', 'L1845', 'Unspecified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717', 'L1845', 'Old bucket handle tear of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.1', 'L1845', 'Derangement of anterior horn of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.2', 'L1845', 'Derangement of posterior horn of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.3', 'L1845', 'Other and unspecified derangement of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.4', 'L1845', 'Unspecified derangement of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.41', 'L1845', 'Bucket handle tear of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.42', 'L1845', 'Derangement of anterior horn of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.43', 'L1845', 'Derangement of posterior horn of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.49', 'L1845', 'Other derangement of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.5', 'L1845', 'Derangement of meniscus, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.6', 'L1845', 'Loose body in knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.7', 'L1845', 'Chondromalacia of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.81', 'L1845', 'Old disruption of lateral collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.82', 'L1845', 'Old disruption of medial collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.83', 'L1845', 'Old disruption of anterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.84', 'L1845', 'Old disruption of posterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.85', 'L1845', 'Old disruption of other ligament of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.89', 'L1845', 'Other internal derangement of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.9', 'L1845', 'Unspecified internal derangement of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.26', 'L1845', 'Pathological dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.29', 'L1845', 'Pathological dislocation of joint of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.36', 'L1845', 'Recurrent dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.39', 'L1845', 'Recurrent dislocation of joint of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.46', 'L1845', 'Contracture of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.49', 'L1845', 'Contracture of joint of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.56', 'L1845', 'Ankylosis of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.59', 'L1845', 'Ankylosis of joint of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.76', 'L1845', 'Developmental dislocation of joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.86', 'L1845', 'Other joint derangement, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.06', 'L1845', 'Effusion of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.09', 'L1845', 'Effusion of joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.16', 'L1845', 'Hemarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.19', 'L1845', 'Hemarthrosis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.26', 'L1845', 'Villonodular synovitis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.29', 'L1845', 'Villonodular synovitis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.36', 'L1845', 'Palindromic rheumatism, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.39', 'L1845', 'Palindromic rheumatism, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.46', 'L1845', 'Pain in joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.49', 'L1845', 'Pain in joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.56', 'L1845', 'Stiffness of joint, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.59', 'L1845', 'Stiffness of joints, not elsewhere classified, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.66', 'L1845', 'Other symptoms referable to lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.69', 'L1845', 'Other symptoms referable to joints of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.7', 'L1845', 'Difficulty in walking');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.8', 'L1845', 'Other specified disorders of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.86', 'L1845', 'Other specified disorders of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.89', 'L1845', 'Other specified disorders of joints of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.96', 'L1845', 'Unspecified disorder of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.6', 'L1845', 'Unspecified enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.61', 'L1845', 'Pes anserinus tendinitis or bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.62', 'L1845', 'Tibial collateral ligament bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.63', 'L1845', 'Fibular collateral ligament bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.64', 'L1845', 'Patellar tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.65', 'L1845', 'Prepatellar bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.69', 'L1845', 'Other enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.7', 'L1845', 'Unspecified enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.71', 'L1845', 'Achilles bursitis or tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.72', 'L1845', 'Tibialis tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L1845', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.91', 'L1845', 'Exostosis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L1845', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.6', 'L1845', 'Residual foreign body in soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.81', 'L1845', 'Swelling of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.82', 'L1845', 'Cramp of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.89', 'L1845', 'Other musculoskeletal symptoms referable to limbs');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.9', 'L1845', 'Other and unspecified disorders of soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.18', 'L1845', 'Osteoarthritis nonspecific site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732', 'L1845', 'Osteochondrosis, juvenile');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733', 'L1845', 'Osteoporosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.95', 'L1845', 'Stress fracture of other bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.99', 'L1845', 'Other disorders of bone and cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.69', 'L1845', 'Other congenital anomaly of lower limb, including pelvic girdle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.8', 'L1845', 'Other specified congenital anomalies of unspecified limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.9', 'L1845', 'Unspecified congenital anomaly of unspecified limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('822', 'L1845', 'Closed fracture of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('822.1', 'L1845', 'Open fracture of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836', 'L1845', 'Tear of medial cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.1', 'L1845', 'Tear of lateral cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.2', 'L1845', 'Other tear of cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.3', 'L1845', 'Closed dislocation of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.4', 'L1845', 'Open dislocation of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.5', 'L1845', 'Closed dislocation of knee, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.51', 'L1845', 'Closed anterior dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.52', 'L1845', 'Closed posterior dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.53', 'L1845', 'Closed medial dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.54', 'L1845', 'Closed lateral dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.59', 'L1845', 'Other closed dislocation of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.6', 'L1845', 'Open dislocation of knee unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.61', 'L1845', 'Open anterior dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.62', 'L1845', 'Open posterior dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.63', 'L1845', 'Open medial dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.64', 'L1845', 'Open lateral dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.69', 'L1845', 'Other open dislocation of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.1', 'L1845', 'Sprain and strain of medial collateral ligament of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.2', 'L1845', 'Sprain and strain of cruciate ligament of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.3', 'L1845', 'Sprain and strain of tibiofibular (joint) (ligament) superior, of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.8', 'L1845', 'Sprain and strain of other specified sites of knee and leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.9', 'L1845', 'Sprain and strain of unspecified site of knee and leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.1', 'L1845', 'Contusion of lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.11', 'L1845', 'Contusion of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.4', 'L1845', 'Contusion of multiple sites of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.5', 'L1845', 'Contusion of unspecified part of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.8', 'L1845', 'Contusion of multiple sites, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.9', 'L1845', 'Contusion of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.7', 'L1845', 'Injury, other and unspecified, knee, leg, ankle, and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.8', 'L1845', 'Injury, other and unspecified, other specified sites, including multiple');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.9', 'L1845', 'Injury, other and unspecified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.59', 'L1845', 'Other immobilization, pressure, and attention to wound');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('213.7', 'L1832', 'Benign neoplasm of long bones of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('215.9', 'L1832', 'Other benign neoplasm of connective and other soft tissue of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('238', 'L1832', 'Neoplasm of uncertain behavior of bone and articular cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('239.2', 'L1832', 'Neoplasms of unspecified nature of bone, soft tissue, and skin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('336.1', 'L1832', 'Vascular myelopathies');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('343.9', 'L1832', 'Unspecified infantile cerebral palsy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.3', 'L1832', 'Monoplegia of lower limb affecting unspecified side');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.9', 'L1832', 'Unspecified paralysis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('682.6', 'L1832', 'Cellulitis and abscess of leg, except foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('713.8', 'L1832', 'Arthropathy associated with other conditions classifiable elsewhere');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L1832', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.09', 'L1832', 'Generalized osteoarthrosis, involving multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.16', 'L1832', 'Primary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.26', 'L1832', 'Secondary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.36', 'L1832', 'Localized osteoarthrosis not specified whether primary or secondary, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.96', 'L1832', 'Osteoarthrosis, unspecified whether generalized or localized, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.98', 'L1832', 'Osteoarthrosis, unspecified whether generalized or localized, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.16', 'L1832', 'Traumatic arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.66', 'L1832', 'Unspecified monoarthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.96', 'L1832', 'Unspecified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717', 'L1832', 'Old bucket handle tear of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.3', 'L1832', 'Other and unspecified derangement of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.4', 'L1832', 'Unspecified derangement of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.5', 'L1832', 'Derangement of meniscus, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.6', 'L1832', 'Loose body in knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.7', 'L1832', 'Chondromalacia of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.81', 'L1832', 'Old disruption of lateral collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.82', 'L1832', 'Old disruption of medial collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.83', 'L1832', 'Old disruption of anterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.84', 'L1832', 'Old disruption of posterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.89', 'L1832', 'Other internal derangement of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718', 'L1832', 'Articular cartilage disorder, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.1', 'L1832', 'Loose body in joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.26', 'L1832', 'Pathological dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.36', 'L1832', 'Recurrent dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.38', 'L1832', 'Recurrent dislocation of joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.46', 'L1832', 'Contracture of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.56', 'L1832', 'Ankylosis of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.8', 'L1832', 'Other joint derangement, not elsewhere classified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.86', 'L1832', 'Other joint derangement, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719', 'L1832', 'Effusion of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.06', 'L1832', 'Effusion of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.08', 'L1832', 'Effusion of joint, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.16', 'L1832', 'Hemarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.26', 'L1832', 'Villonodular synovitis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.36', 'L1832', 'Palindromic rheumatism, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.4', 'L1832', 'Pain in joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.46', 'L1832', 'Pain in joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.48', 'L1832', 'Pain in joint, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.49', 'L1832', 'Pain in joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.56', 'L1832', 'Stiffness of joint, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.66', 'L1832', 'Other symptoms referable to lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.7', 'L1832', 'Difficulty in walking');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.86', 'L1832', 'Other specified disorders of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.96', 'L1832', 'Unspecified disorder of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.6', 'L1832', 'Unspecified enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.61', 'L1832', 'Pes anserinus tendinitis or bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.64', 'L1832', 'Patellar tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.65', 'L1832', 'Prepatellar bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.69', 'L1832', 'Other enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L1832', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.91', 'L1832', 'Exostosis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727', 'L1832', 'Unspecified synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.01', 'L1832', 'Synovitis and tenosynovitis in diseases classified elsewhere');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L1832', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.2', 'L1832', 'Specific bursitides often of occupational origin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.3', 'L1832', 'Other bursitis disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.6', 'L1832', 'Nontraumatic rupture of unspecified tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.66', 'L1832', 'Nontraumatic rupture of patellar tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.81', 'L1832', 'Contracture of tendon (sheath)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.83', 'L1832', 'Plica syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.89', 'L1832', 'Other disorders of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.4', 'L1832', 'Laxity of ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.5', 'L1832', 'Hypermobility syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.71', 'L1832', 'Plantar fascial fibromatosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.85', 'L1832', 'Spasm of muscle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.9', 'L1832', 'Unspecified disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.2', 'L1832', 'Unspecified neuralgia, neuritis, and radiculitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L1832', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.81', 'L1832', 'Swelling of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.27', 'L1832', 'Unspecified osteomyelitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.4', 'L1832', 'Juvenile osteochondrosis of lower extremity, excluding foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.7', 'L1832', 'Osteochondritis dissecans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.9', 'L1832', 'Unspecified osteochondropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.1', 'L1832', 'Pathologic fracture, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.81', 'L1832', 'Malunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.82', 'L1832', 'Nonunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.92', 'L1832', 'Chondromalacia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.99', 'L1832', 'Other disorders of bone and cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.6', 'L1832', 'Other acquired deformities of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('738.8', 'L1832', 'Acquired musculoskeletal deformity of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.3', 'L1832', 'Congenital unspecified reduction deformity of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.64', 'L1832', 'Congenital deformity of knee (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821', 'L1832', 'Closed fracture of unspecified part of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.01', 'L1832', 'Closed fracture of shaft of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.1', 'L1832', 'Open fracture of unspecified part of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.2', 'L1832', 'Closed fracture of unspecified part of lower end of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.21', 'L1832', 'Closed fracture of femoral condyle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.22', 'L1832', 'Closed fracture of lower epiphysis of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.23', 'L1832', 'Closed supracondylar fracture of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.29', 'L1832', 'Other closed fracture of lower end of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.31', 'L1832', 'Open fracture of femoral condyle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('822', 'L1832', 'Closed fracture of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823', 'L1832', 'Closed fracture of upper end of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.01', 'L1832', 'Closed fracture of upper end of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.02', 'L1832', 'Closed fracture of upper end of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.1', 'L1832', 'Open fracture of upper end of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.11', 'L1832', 'Open fracture of upper end of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.2', 'L1832', 'Closed fracture of shaft of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.21', 'L1832', 'Closed fracture of shaft of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.22', 'L1832', 'Closed fracture of shaft of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.3', 'L1832', 'Open fracture of shaft of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.32', 'L1832', 'Open fracture of shaft of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.8', 'L1832', 'Closed fracture of unspecified part of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.81', 'L1832', 'Closed fracture of unspecified part of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.82', 'L1832', 'Closed fracture of unspecified part of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.9', 'L1832', 'Open fracture of unspecified part of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.91', 'L1832', 'Open fracture of unspecified part of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.92', 'L1832', 'Open fracture of unspecified part of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('829', 'L1832', 'Closed fracture of unspecified bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.5', 'L1832', 'Closed dislocation of knee, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.51', 'L1832', 'Closed anterior dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.52', 'L1832', 'Closed posterior dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.9', 'L1832', 'Sprain and strain of unspecified site of knee and leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.8', 'L1832', 'Other specified sites of sprains and strains');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.9', 'L1832', 'Unspecified site of sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('879.8', 'L1832', 'Open wound(s) (multiple) of unspecified site(s), without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('916', 'L1832', 'Hip, thigh, leg, and ankle, abrasion or friction burn, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('916.8', 'L1832', 'Other and unspecified superficial injury of hip, thigh, leg, and ankle, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.1', 'L1832', 'Contusion of lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.11', 'L1832', 'Contusion of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.4', 'L1832', 'Contusion of multiple sites of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.5', 'L1832', 'Contusion of unspecified part of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.9', 'L1832', 'Contusion of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('956.3', 'L1832', 'Injury to peroneal nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('958.8', 'L1832', 'Other early complications of trauma');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.7', 'L1832', 'Injury, other and unspecified, knee, leg, ankle, and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.9', 'L1832', 'Injury, other and unspecified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.78', 'L1832', 'Other complications due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.59', 'L1832', 'Other immobilization, pressure, and attention to wound');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('170.9', 'L1830', 'Malignant neoplasm of bone and articular cartilage, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('213.7', 'L1830', 'Benign neoplasm of long bones of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('215.3', 'L1830', 'Other benign neoplasm of connective and other soft tissue of lower limb, including hip');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('216.7', 'L1830', 'Benign neoplasm of skin of lower limb, including hip');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.01', 'L1830', 'Diabetes mellitus without mention of complication, type I [juvenile type], not stated as  uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.8', 'L1830', 'Diabetes with other specified manifestations, type II or unspecified type, not stated as  uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('274', 'L1830', 'Gouty arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('300.02', 'L1830', 'Generalized anxiety disorder');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('343', 'L1830', 'Diplegic infantile cerebral palsy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('343.2', 'L1830', 'Quadriplegic infantile cerebral palsy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('343.8', 'L1830', 'Other specified infantile cerebral palsy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('343.9', 'L1830', 'Unspecified infantile cerebral palsy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.12', 'L1902', 'Sprain and strain of metatarsaophalangeal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.19', 'L1902', 'Other foot sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('846', 'L1902', 'Sprain and strain of lumbosacral (joint) (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('846.1', 'L1902', 'Sprain and strain of sacroiliac (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847', 'L1902', 'Neck sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847.1', 'L1902', 'Thoracic sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847.2', 'L1902', 'Lumbar sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848', 'L1902', 'Sprain and strain of septal cartilage of nose');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.3', 'L1902', 'Sprain and strain of ribs');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.5', 'L1902', 'Pelvic sprain and strains');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.8', 'L1902', 'Other specified sites of sprains and strains');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.9', 'L1902', 'Unspecified site of sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('890', 'L1902', 'Open wound of hip and thigh, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('892', 'L1902', 'Open wound of foot except toe(s) alone, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('905.7', 'L1902', 'Late effect of sprain and strain without mention of tendon injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('905.8', 'L1902', 'Late effect of tendon injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('916', 'L1902', 'Hip, thigh, leg, and ankle, abrasion or friction burn, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('916.8', 'L1902', 'Other and unspecified superficial injury of hip, thigh, leg, and ankle, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('917.8', 'L1902', 'Other and unspecified superficial injury of foot and toes, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924', 'L1902', 'Contusion of thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.01', 'L1902', 'Contusion of hip');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.1', 'L1902', 'Contusion of lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.11', 'L1902', 'Contusion of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.2', 'L1902', 'Contusion of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.21', 'L1902', 'Contusion of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.3', 'L1902', 'Contusion of toe');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.4', 'L1902', 'Contusion of multiple sites of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.5', 'L1902', 'Contusion of unspecified part of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.8', 'L1902', 'Contusion of multiple sites, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.9', 'L1902', 'Contusion of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('928.1', 'L1902', 'Crushing injury of lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('928.2', 'L1902', 'Crushing injury of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('928.21', 'L1902', 'Crushing injury of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('928.9', 'L1902', 'Crushing injury of unspecified site of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('945', 'L1902', 'Burn of unspecified degree of unspecified site of lower limb (leg)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('945.24', 'L1902', 'Blisters with epidermal loss due to burn (second degree) of lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('956.3', 'L1902', 'Injury to peroneal nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('956.9', 'L1902', 'Injury to unspecified nerve of pelvic girdle and lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.7', 'L1902', 'Injury, other and unspecified, knee, leg, ankle, and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.8', 'L1902', 'Injury, other and unspecified, other specified sites, including multiple');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.9', 'L1902', 'Injury, other and unspecified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.67', 'L1902', 'Infection and inflammatory reaction due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.77', 'L1902', 'Other complications due to internal joint prosthesis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.78', 'L1902', 'Other complications due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.59', 'L1902', 'Other immobilization, pressure, and attention to wound');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('274', 'L4360', 'Gouty arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('335.2', 'L4360', 'Amyotrophic lateral sclerosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('337.9', 'L4360', 'Unspecified disorder of autonomic nervous system');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.5', 'L4360', 'Tarsal tunnel syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.6', 'L4360', 'Lesion of plantar nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.71', 'L4360', 'Causalgia of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.79', 'L4360', 'Other mononeuritis of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.8', 'L4360', 'Unspecified mononeuritis of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.9', 'L4360', 'Mononeuritis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('356.9', 'L4360', 'Unspecified hereditary and idiopathic peripheral neuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('459.81', 'L4360', 'Unspecified venous (peripheral) insufficiency');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('473.9', 'L4360', 'Unspecified sinusitis (chronic)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('682.6', 'L4360', 'Cellulitis and abscess of leg, except foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('682.7', 'L4360', 'Cellulitis and abscess of foot, except toes');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('686.9', 'L4360', 'Unspecified local infection of skin and subcutaneous tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707', 'L4360', 'Decubitus ulcer');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707.12', 'L4360', 'Ulcer of calf');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707.13', 'L4360', 'Ulcer of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707.14', 'L4360', 'Ulcer of heel and midfoot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707.15', 'L4360', 'Ulcer of other part of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('713.5', 'L4360', 'Arthropathy associated with neurological disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L4360', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.9', 'L4360', 'Unspecified inflammatory polyarthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L4360', 'Generalized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.16', 'L4360', 'Primary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.17', 'L4360', 'Primary localized osteoarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.18', 'L4360', 'Primary localized osteoarthrosis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.2', 'L4360', 'Secondary localized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.26', 'L4360', 'Secondary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.27', 'L4360', 'Secondary localized osteoarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.28', 'L4360', 'Secondary localized osteoarthrosis, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.3', 'L4360', 'Localized osteoarthrosis not specified whether primary or secondary, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.36', 'L4360', 'Localized osteoarthrosis not specified whether primary or secondary, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.37', 'L4360', 'Localized osteoarthrosis not specified whether primary or secondary, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.38', 'L4360', 'Localized osteoarthrosis not specified whether primary or secondary, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.8', 'L4360', 'Osteoarthrosis involving more than one site, but not specified as generalized, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.89', 'L4360', 'Osteoarthrosis involving multiple sites, but not specified as generalized');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.9', 'L4360', 'Osteoarthrosis, unspecified whether generalized or localized, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.96', 'L4360', 'Osteoarthrosis, unspecified whether generalized or localized, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.97', 'L4360', 'Osteoarthrosis, unspecified whether generalized or localized, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.98', 'L4360', 'Osteoarthrosis, unspecified whether generalized or localized, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.1', 'L4360', 'Traumatic arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.17', 'L4360', 'Traumatic arthropathy, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.9', 'L4360', 'Unspecified arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.96', 'L4360', 'Unspecified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.97', 'L4360', 'Unspecified arthropathy, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718', 'L4360', 'Articular cartilage disorder, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.17', 'L4360', 'Loose body in ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.37', 'L4360', 'Recurrent dislocation of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.57', 'L4360', 'Ankylosis of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.8', 'L4360', 'Other joint derangement, not elsewhere classified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.86', 'L4360', 'Other joint derangement, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.87', 'L4360', 'Other joint derangement, not elsewhere classified, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.89', 'L4360', 'Other joint derangement, not elsewhere classified, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.9', 'L4360', 'Unspecified derangement, joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.97', 'L4360', 'Unspecified ankle and foot joint derangement');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719', 'L4360', 'Effusion of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.06', 'L4360', 'Effusion of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.07', 'L4360', 'Effusion of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.08', 'L4360', 'Effusion of joint, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.09', 'L4360', 'Effusion of joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.1', 'L4360', 'Hemarthrosis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.16', 'L4360', 'Hemarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.17', 'L4360', 'Hemarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.18', 'L4360', 'Hemarthrosis, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.19', 'L4360', 'Hemarthrosis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.26', 'L4360', 'Villonodular synovitis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.27', 'L4360', 'Villonodular synovitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.28', 'L4360', 'Villonodular synovitis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.29', 'L4360', 'Villonodular synovitis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.3', 'L4360', 'Palindromic rheumatism, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.36', 'L4360', 'Palindromic rheumatism, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.37', 'L4360', 'Palindromic rheumatism, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.38', 'L4360', 'Palindromic rheumatism, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.39', 'L4360', 'Palindromic rheumatism, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.4', 'L4360', 'Pain in joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.46', 'L4360', 'Pain in joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.47', 'L4360', 'Pain in joint, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.48', 'L4360', 'Pain in joint, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.49', 'L4360', 'Pain in joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.66', 'L4360', 'Other symptoms referable to lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.67', 'L4360', 'Other symptoms referable to ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.68', 'L4360', 'Other symptoms referable to joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.69', 'L4360', 'Other symptoms referable to joints of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.8', 'L4360', 'Other specified disorders of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.86', 'L4360', 'Other specified disorders of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.87', 'L4360', 'Other specified disorders of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.88', 'L4360', 'Other specified disorders of joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.89', 'L4360', 'Other specified disorders of joints of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.9', 'L4360', 'Unspecified disorder of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.96', 'L4360', 'Unspecified disorder of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.97', 'L4360', 'Unspecified disorder of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.98', 'L4360', 'Unspecified joint disorder of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.99', 'L4360', 'Unspecified joint disorder of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.7', 'L4360', 'Unspecified enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.71', 'L4360', 'Achilles bursitis or tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.72', 'L4360', 'Tibialis tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.73', 'L4360', 'Calcaneal spur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.79', 'L4360', 'Other enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L4360', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727', 'L4360', 'Unspecified synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.06', 'L4360', 'Tenosynovitis of foot and ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L4360', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.1', 'L4360', 'Bunion');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.2', 'L4360', 'Specific bursitides often of occupational origin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.3', 'L4360', 'Other bursitis disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.4', 'L4360', 'Unspecified synovial cyst');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.41', 'L4360', 'Ganglion of joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.42', 'L4360', 'Ganglion of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.43', 'L4360', 'Unspecified ganglion');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.49', 'L4360', 'Other ganglion and cyst of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.5', 'L4360', 'Unspecified rupture of synovium');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.59', 'L4360', 'Other rupture of synovium');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.6', 'L4360', 'Nontraumatic rupture of unspecified tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.67', 'L4360', 'Nontraumatic rupture of Achilles tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.68', 'L4360', 'Nontraumatic rupture of other tendons of foot and ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.69', 'L4360', 'Nontraumatic rupture of other tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.81', 'L4360', 'Contracture of tendon (sheath)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.82', 'L4360', 'Calcium deposits in tendon and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.83', 'L4360', 'Plica syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.89', 'L4360', 'Other disorders of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.9', 'L4360', 'Unspecified disorder of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.2', 'L4360', 'Muscular wasting and disuse atrophy, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.4', 'L4360', 'Laxity of ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.6', 'L4360', 'Contracture of palmar fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.71', 'L4360', 'Plantar fascial fibromatosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.85', 'L4360', 'Spasm of muscle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.89', 'L4360', 'Other disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.9', 'L4360', 'Unspecified disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729', 'L4360', 'Rheumatism, unspecified and fibrositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.1', 'L4360', 'Unspecified myalgia and myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.2', 'L4360', 'Unspecified neuralgia, neuritis, and radiculitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L4360', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.6', 'L4360', 'Residual foreign body in soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.81', 'L4360', 'Swelling of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.9', 'L4360', 'Other and unspecified disorders of soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730', 'L4360', 'Acute osteomyelitis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.07', 'L4360', 'Acute osteomyelitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.27', 'L4360', 'Unspecified osteomyelitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.37', 'L4360', 'Periostitis, without mention of osteomyelitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.4', 'L4360', 'Juvenile osteochondrosis of lower extremity, excluding foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.5', 'L4360', 'Juvenile osteochondrosis of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.7', 'L4360', 'Osteochondritis dissecans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.9', 'L4360', 'Unspecified osteochondropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.16', 'L4360', 'Pathologic fracture of tibia and fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.19', 'L4360', 'Pathologic fracture of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.2', 'L4360', 'Unspecified cyst of bone (localized)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.44', 'L4360', 'Aseptic necrosis of talus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.82', 'L4360', 'Nonunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.9', 'L4360', 'Disorder of bone and cartilage, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.91', 'L4360', 'Arrest of bone development or growth');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.99', 'L4360', 'Other disorders of bone and cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('735', 'L4360', 'Hallux valgus (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('735.2', 'L4360', 'Hallux rigidus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('735.4', 'L4360', 'Other hammer toe (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736', 'L4360', 'Unspecified deformity of forearm, excluding fingers');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.42', 'L4360', 'Genu varum (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.7', 'L4360', 'Unspecified deformity of ankle and foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.72', 'L4360', 'Equinus deformity of foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.73', 'L4360', 'Cavus deformity of foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.79', 'L4360', 'Other acquired deformity of ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.51', 'L4360', 'Congenital talipes equinovarus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.52', 'L4360', 'Congenital metatarsus primus varus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.53', 'L4360', 'Congenital metatarsus varus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.6', 'L4360', 'Congenital talipes valgus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.61', 'L4360', 'Congenital pes planus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.7', 'L4360', 'Unspecified talipes');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.1', 'L4360', 'Syndactyly of multiple and unspecified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.6', 'L4360', 'Unspecified congenital anomaly of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.67', 'L4360', 'Congenital anomalies of foot, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.4', 'L4360', 'Chondrodystrophy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.51', 'L4360', 'Osteogenesis imperfecta');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.52', 'L4360', 'Osteopetrosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.2', 'L4360', 'Closed fracture of shaft of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.21', 'L4360', 'Closed fracture of shaft of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.22', 'L4360', 'Closed fracture of shaft of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.3', 'L4360', 'Open fracture of shaft of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.32', 'L4360', 'Open fracture of shaft of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.8', 'L4360', 'Closed fracture of unspecified part of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.81', 'L4360', 'Closed fracture of unspecified part of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.82', 'L4360', 'Closed fracture of unspecified part of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.9', 'L4360', 'Open fracture of unspecified part of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.91', 'L4360', 'Open fracture of unspecified part of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.92', 'L4360', 'Open fracture of unspecified part of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824', 'L4360', 'Closed fracture of medial malleolus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.1', 'L4360', 'Open fracture of medial malleolus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.2', 'L4360', 'Closed fracture of lateral malleolus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.3', 'L4360', 'Open fracture of lateral malleolus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.4', 'L4360', 'Closed bimalleolar fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.5', 'L4360', 'Open bimalleolar fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.6', 'L4360', 'Closed trimalleolar fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.7', 'L4360', 'Open trimalleolar fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.8', 'L4360', 'Unspecified closed fracture of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.9', 'L4360', 'Unspecified open fracture of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825', 'L4360', 'Closed fracture of calcaneus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.1', 'L4360', 'Open fracture of calcaneus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.2', 'L4360', 'Closed fracture of unspecified bone(s) of foot (except toes)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.21', 'L4360', 'Closed fracture of astragalus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.22', 'L4360', 'Closed fracture of navicular (scaphoid) bone of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.23', 'L4360', 'Closed fracture of cuboid bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.24', 'L4360', 'Closed fracture of cuneiform bone of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.25', 'L4360', 'Closed fracture of metatarsal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.29', 'L4360', 'Other closed fracture of tarsal and metatarsal bones');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.3', 'L4360', 'Open fracture of unspecified bone(s) of foot (except toes)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.31', 'L4360', 'Open fracture of astragalus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.32', 'L4360', 'Open fracture of navicular (scaphoid) bone of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.33', 'L4360', 'Open fracture of cuboid bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.34', 'L4360', 'Open fracture of cuneiform bone of foot,');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.35', 'L4360', 'Open fracture of metatarsal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.39', 'L4360', 'Other open fractures of tarsal and metatarsal bones');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('826', 'L4360', 'Closed fracture of one or more phalanges of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('826.1', 'L4360', 'Open fracture of one or more phalanges of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('827', 'L4360', 'Other, multiple and ill-defined closed fractures of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('827.1', 'L4360', 'Other, multiple and ill-defined open fractures of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('828', 'L4360', 'Multiple closed fractures involving both lower limbs, lower with upper limb, and lower limb(s) with rib(s) and sternum');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('837', 'L4360', 'Closed dislocation of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838', 'L4360', 'Closed dislocation of foot, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838.01', 'L4360', 'Closed dislocation of tarsal (bone), joint unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838.03', 'L4360', 'Closed dislocation of tarsometatarsal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838.06', 'L4360', 'Closed dislocation of interphalangeal (joint), foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845', 'L4360', 'Unspecified site of ankle sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.01', 'L4360', 'Sprain and strain of deltoid (ligament) of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.02', 'L4360', 'Sprain and strain of calcaneofibular (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.03', 'L4360', 'Sprain and strain of tibiofibular (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.09', 'L4360', 'Other ankle sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.1', 'L4360', 'Sprain and strain of unspecified site of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.11', 'L4360', 'Sprain and strain of tarsometatarsal (joint) (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.12', 'L4360', 'Sprain and strain of metatarsaophalangeal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.13', 'L4360', 'Sprain and strain of interphalangeal (joint), of toe');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.19', 'L4360', 'Other foot sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('892', 'L4360', 'Open wound of foot except toe(s) alone, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('893', 'L4360', 'Open wound of toe(s), without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('894', 'L4360', 'Multiple and unspecified open wound of lower limb, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('904.8', 'L4360', 'Injury to unspecified blood vessel of lower extremity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('905.7', 'L4360', 'Late effect of sprain and strain without mention of tendon injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('905.8', 'L4360', 'Late effect of tendon injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('917.8', 'L4360', 'Other and unspecified superficial injury of foot and toes, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('917.9', 'L4360', 'Other and unspecified superficial injury of foot and toes, infected');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('919', 'L4360', 'Abrasion or friction burn of other, multiple, and unspecified sites, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('919.8', 'L4360', 'Other and unspecified superficial injury of other, multiple, and unspecified sites, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.1', 'L4360', 'Contusion of lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.11', 'L4360', 'Contusion of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.2', 'L4360', 'Contusion of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.21', 'L4360', 'Contusion of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.3', 'L4360', 'Contusion of toe');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.4', 'L4360', 'Contusion of multiple sites of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.5', 'L4360', 'Contusion of unspecified part of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.8', 'L4360', 'Contusion of multiple sites, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.9', 'L4360', 'Contusion of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('928.2', 'L4360', 'Crushing injury of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('928.21', 'L4360', 'Crushing injury of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('929.9', 'L4360', 'Crushing injury of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('945', 'L4360', 'Burn of unspecified degree of unspecified site of lower limb (leg)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('945.02', 'L4360', 'Burn of unspecified degree of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('945.03', 'L4360', 'Burn of unspecified degree of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('945.2', 'L4360', 'Blisters with epidermal loss due to burn (second degree) of unspecified site of lower limb (leg)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.78', 'L4360', 'Other complications due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.79', 'L4360', 'Other complications due to other internal prosthetic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.23', 'L4360', 'Fitting of orthotic device');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('274', 'L4386', 'Gouty arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('335.2', 'L4386', 'Amyotrophic lateral sclerosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('337.9', 'L4386', 'Unspecified disorder of autonomic nervous system');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.5', 'L4386', 'Tarsal tunnel syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.6', 'L4386', 'Lesion of plantar nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.71', 'L4386', 'Causalgia of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.79', 'L4386', 'Other mononeuritis of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.8', 'L4386', 'Unspecified mononeuritis of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.9', 'L4386', 'Mononeuritis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('356.9', 'L4386', 'Unspecified hereditary and idiopathic peripheral neuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('459.81', 'L4386', 'Unspecified venous (peripheral) insufficiency');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('473.9', 'L4386', 'Unspecified sinusitis (chronic)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('682.6', 'L4386', 'Cellulitis and abscess of leg, except foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('682.7', 'L4386', 'Cellulitis and abscess of foot, except toes');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('686.9', 'L4386', 'Unspecified local infection of skin and subcutaneous tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707', 'L4386', 'Decubitus ulcer');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707.12', 'L4386', 'Ulcer of calf');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707.13', 'L4386', 'Ulcer of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707.14', 'L4386', 'Ulcer of heel and midfoot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707.15', 'L4386', 'Ulcer of other part of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('713.5', 'L4386', 'Arthropathy associated with neurological disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L4386', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.9', 'L4386', 'Unspecified inflammatory polyarthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L4386', 'Generalized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.16', 'L4386', 'Primary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.17', 'L4386', 'Primary localized osteoarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.18', 'L4386', 'Primary localized osteoarthrosis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.2', 'L4386', 'Secondary localized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.26', 'L4386', 'Secondary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.27', 'L4386', 'Secondary localized osteoarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.28', 'L4386', 'Secondary localized osteoarthrosis, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.3', 'L4386', 'Localized osteoarthrosis not specified whether primary or secondary, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.36', 'L4386', 'Localized osteoarthrosis not specified whether primary or secondary, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.37', 'L4386', 'Localized osteoarthrosis not specified whether primary or secondary, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.38', 'L4386', 'Localized osteoarthrosis not specified whether primary or secondary, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.8', 'L4386', 'Osteoarthrosis involving more than one site, but not specified as generalized, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.89', 'L4386', 'Osteoarthrosis involving multiple sites, but not specified as generalized');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.9', 'L4386', 'Osteoarthrosis, unspecified whether generalized or localized, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.96', 'L4386', 'Osteoarthrosis, unspecified whether generalized or localized, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.97', 'L4386', 'Osteoarthrosis, unspecified whether generalized or localized, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.98', 'L4386', 'Osteoarthrosis, unspecified whether generalized or localized, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.1', 'L4386', 'Traumatic arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.17', 'L4386', 'Traumatic arthropathy, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.9', 'L4386', 'Unspecified arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.96', 'L4386', 'Unspecified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.97', 'L4386', 'Unspecified arthropathy, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718', 'L4386', 'Articular cartilage disorder, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.17', 'L4386', 'Loose body in ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.37', 'L4386', 'Recurrent dislocation of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.57', 'L4386', 'Ankylosis of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.8', 'L4386', 'Other joint derangement, not elsewhere classified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.86', 'L4386', 'Other joint derangement, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.87', 'L4386', 'Other joint derangement, not elsewhere classified, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.89', 'L4386', 'Other joint derangement, not elsewhere classified, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.9', 'L4386', 'Unspecified derangement, joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.97', 'L4386', 'Unspecified ankle and foot joint derangement');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719', 'L4386', 'Effusion of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.06', 'L4386', 'Effusion of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.07', 'L4386', 'Effusion of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.08', 'L4386', 'Effusion of joint, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.09', 'L4386', 'Effusion of joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.1', 'L4386', 'Hemarthrosis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.16', 'L4386', 'Hemarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.17', 'L4386', 'Hemarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.18', 'L4386', 'Hemarthrosis, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.19', 'L4386', 'Hemarthrosis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.26', 'L4386', 'Villonodular synovitis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.27', 'L4386', 'Villonodular synovitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.28', 'L4386', 'Villonodular synovitis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.29', 'L4386', 'Villonodular synovitis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.3', 'L4386', 'Palindromic rheumatism, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.36', 'L4386', 'Palindromic rheumatism, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.37', 'L4386', 'Palindromic rheumatism, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.38', 'L4386', 'Palindromic rheumatism, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.39', 'L4386', 'Palindromic rheumatism, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.4', 'L4386', 'Pain in joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.46', 'L4386', 'Pain in joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.47', 'L4386', 'Pain in joint, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.48', 'L4386', 'Pain in joint, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.49', 'L4386', 'Pain in joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.66', 'L4386', 'Other symptoms referable to lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.67', 'L4386', 'Other symptoms referable to ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.68', 'L4386', 'Other symptoms referable to joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.69', 'L4386', 'Other symptoms referable to joints of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.8', 'L4386', 'Other specified disorders of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.86', 'L4386', 'Other specified disorders of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.87', 'L4386', 'Other specified disorders of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.88', 'L4386', 'Other specified disorders of joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.89', 'L4386', 'Other specified disorders of joints of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.9', 'L4386', 'Unspecified disorder of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.96', 'L4386', 'Unspecified disorder of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.97', 'L4386', 'Unspecified disorder of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.98', 'L4386', 'Unspecified joint disorder of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.99', 'L4386', 'Unspecified joint disorder of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.7', 'L4386', 'Unspecified enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.71', 'L4386', 'Achilles bursitis or tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.72', 'L4386', 'Tibialis tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.73', 'L4386', 'Calcaneal spur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.79', 'L4386', 'Other enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L4386', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727', 'L4386', 'Unspecified synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.06', 'L4386', 'Tenosynovitis of foot and ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L4386', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.1', 'L4386', 'Bunion');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.2', 'L4386', 'Specific bursitides often of occupational origin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.3', 'L4386', 'Other bursitis disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.4', 'L4386', 'Unspecified synovial cyst');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.41', 'L4386', 'Ganglion of joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.42', 'L4386', 'Ganglion of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.43', 'L4386', 'Unspecified ganglion');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.49', 'L4386', 'Other ganglion and cyst of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.5', 'L4386', 'Unspecified rupture of synovium');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.59', 'L4386', 'Other rupture of synovium');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.6', 'L4386', 'Nontraumatic rupture of unspecified tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.67', 'L4386', 'Nontraumatic rupture of Achilles tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.68', 'L4386', 'Nontraumatic rupture of other tendons of foot and ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.69', 'L4386', 'Nontraumatic rupture of other tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.81', 'L4386', 'Contracture of tendon (sheath)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.82', 'L4386', 'Calcium deposits in tendon and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.83', 'L4386', 'Plica syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.89', 'L4386', 'Other disorders of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.9', 'L4386', 'Unspecified disorder of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.2', 'L4386', 'Muscular wasting and disuse atrophy, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.4', 'L4386', 'Laxity of ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.6', 'L4386', 'Contracture of palmar fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.71', 'L4386', 'Plantar fascial fibromatosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.85', 'L4386', 'Spasm of muscle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.89', 'L4386', 'Other disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.9', 'L4386', 'Unspecified disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729', 'L4386', 'Rheumatism, unspecified and fibrositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.1', 'L4386', 'Unspecified myalgia and myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.2', 'L4386', 'Unspecified neuralgia, neuritis, and radiculitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L4386', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.6', 'L4386', 'Residual foreign body in soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.81', 'L4386', 'Swelling of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.9', 'L4386', 'Other and unspecified disorders of soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730', 'L4386', 'Acute osteomyelitis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.07', 'L4386', 'Acute osteomyelitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.27', 'L4386', 'Unspecified osteomyelitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.37', 'L4386', 'Periostitis, without mention of osteomyelitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.4', 'L4386', 'Juvenile osteochondrosis of lower extremity, excluding foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.5', 'L4386', 'Juvenile osteochondrosis of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.7', 'L4386', 'Osteochondritis dissecans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.9', 'L4386', 'Unspecified osteochondropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.16', 'L4386', 'Pathologic fracture of tibia and fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.19', 'L4386', 'Pathologic fracture of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.2', 'L4386', 'Unspecified cyst of bone (localized)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.44', 'L4386', 'Aseptic necrosis of talus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.82', 'L4386', 'Nonunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.9', 'L4386', 'Disorder of bone and cartilage, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.91', 'L4386', 'Arrest of bone development or growth');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.99', 'L4386', 'Other disorders of bone and cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('735', 'L4386', 'Hallux valgus (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('735.2', 'L4386', 'Hallux rigidus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('735.4', 'L4386', 'Other hammer toe (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736', 'L4386', 'Unspecified deformity of forearm, excluding fingers');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.42', 'L4386', 'Genu varum (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.7', 'L4386', 'Unspecified deformity of ankle and foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.72', 'L4386', 'Equinus deformity of foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.73', 'L4386', 'Cavus deformity of foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.79', 'L4386', 'Other acquired deformity of ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.51', 'L4386', 'Congenital talipes equinovarus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.52', 'L4386', 'Congenital metatarsus primus varus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.53', 'L4386', 'Congenital metatarsus varus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.6', 'L4386', 'Congenital talipes valgus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.61', 'L4386', 'Congenital pes planus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.7', 'L4386', 'Unspecified talipes');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.1', 'L4386', 'Syndactyly of multiple and unspecified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.6', 'L4386', 'Unspecified congenital anomaly of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.67', 'L4386', 'Congenital anomalies of foot, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.4', 'L4386', 'Chondrodystrophy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.51', 'L4386', 'Osteogenesis imperfecta');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.52', 'L4386', 'Osteopetrosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.2', 'L4386', 'Closed fracture of shaft of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.21', 'L4386', 'Closed fracture of shaft of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.22', 'L4386', 'Closed fracture of shaft of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.3', 'L4386', 'Open fracture of shaft of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.32', 'L4386', 'Open fracture of shaft of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.8', 'L4386', 'Closed fracture of unspecified part of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.81', 'L4386', 'Closed fracture of unspecified part of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.82', 'L4386', 'Closed fracture of unspecified part of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.9', 'L4386', 'Open fracture of unspecified part of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.91', 'L4386', 'Open fracture of unspecified part of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.92', 'L4386', 'Open fracture of unspecified part of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824', 'L4386', 'Closed fracture of medial malleolus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.1', 'L4386', 'Open fracture of medial malleolus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.2', 'L4386', 'Closed fracture of lateral malleolus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.3', 'L4386', 'Open fracture of lateral malleolus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.4', 'L4386', 'Closed bimalleolar fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.5', 'L4386', 'Open bimalleolar fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.6', 'L4386', 'Closed trimalleolar fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.7', 'L4386', 'Open trimalleolar fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.8', 'L4386', 'Unspecified closed fracture of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.9', 'L4386', 'Unspecified open fracture of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825', 'L4386', 'Closed fracture of calcaneus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.1', 'L4386', 'Open fracture of calcaneus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.2', 'L4386', 'Closed fracture of unspecified bone(s) of foot (except toes)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.21', 'L4386', 'Closed fracture of astragalus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.22', 'L4386', 'Closed fracture of navicular (scaphoid) bone of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.23', 'L4386', 'Closed fracture of cuboid bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.24', 'L4386', 'Closed fracture of cuneiform bone of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.25', 'L4386', 'Closed fracture of metatarsal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.29', 'L4386', 'Other closed fracture of tarsal and metatarsal bones');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.3', 'L4386', 'Open fracture of unspecified bone(s) of foot (except toes)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.31', 'L4386', 'Open fracture of astragalus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.32', 'L4386', 'Open fracture of navicular (scaphoid) bone of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.33', 'L4386', 'Open fracture of cuboid bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.34', 'L4386', 'Open fracture of cuneiform bone of foot,');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.35', 'L4386', 'Open fracture of metatarsal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.39', 'L4386', 'Other open fractures of tarsal and metatarsal bones');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('826', 'L4386', 'Closed fracture of one or more phalanges of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('826.1', 'L4386', 'Open fracture of one or more phalanges of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('827', 'L4386', 'Other, multiple and ill-defined closed fractures of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('827.1', 'L4386', 'Other, multiple and ill-defined open fractures of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('828', 'L4386', 'Multiple closed fractures involving both lower limbs, lower with upper limb, and lower limb(s) with rib(s) and sternum');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('837', 'L4386', 'Closed dislocation of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838', 'L4386', 'Closed dislocation of foot, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838.01', 'L4386', 'Closed dislocation of tarsal (bone), joint unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838.03', 'L4386', 'Closed dislocation of tarsometatarsal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838.06', 'L4386', 'Closed dislocation of interphalangeal (joint), foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845', 'L4386', 'Unspecified site of ankle sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.01', 'L4386', 'Sprain and strain of deltoid (ligament) of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.02', 'L4386', 'Sprain and strain of calcaneofibular (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.03', 'L4386', 'Sprain and strain of tibiofibular (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.09', 'L4386', 'Other ankle sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.1', 'L4386', 'Sprain and strain of unspecified site of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.11', 'L4386', 'Sprain and strain of tarsometatarsal (joint) (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.12', 'L4386', 'Sprain and strain of metatarsaophalangeal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.13', 'L4386', 'Sprain and strain of interphalangeal (joint), of toe');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.19', 'L4386', 'Other foot sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('892', 'L4386', 'Open wound of foot except toe(s) alone, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('893', 'L4386', 'Open wound of toe(s), without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('894', 'L4386', 'Multiple and unspecified open wound of lower limb, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('904.8', 'L4386', 'Injury to unspecified blood vessel of lower extremity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('905.7', 'L4386', 'Late effect of sprain and strain without mention of tendon injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('905.8', 'L4386', 'Late effect of tendon injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('917.8', 'L4386', 'Other and unspecified superficial injury of foot and toes, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('917.9', 'L4386', 'Other and unspecified superficial injury of foot and toes, infected');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('919', 'L4386', 'Abrasion or friction burn of other, multiple, and unspecified sites, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('919.8', 'L4386', 'Other and unspecified superficial injury of other, multiple, and unspecified sites, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.1', 'L4386', 'Contusion of lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.11', 'L4386', 'Contusion of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.2', 'L4386', 'Contusion of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.21', 'L4386', 'Contusion of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.3', 'L4386', 'Contusion of toe');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.4', 'L4386', 'Contusion of multiple sites of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.5', 'L4386', 'Contusion of unspecified part of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.8', 'L4386', 'Contusion of multiple sites, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.9', 'L4386', 'Contusion of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('928.2', 'L4386', 'Crushing injury of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('928.21', 'L4386', 'Crushing injury of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('929.9', 'L4386', 'Crushing injury of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('945', 'L4386', 'Burn of unspecified degree of unspecified site of lower limb (leg)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('945.02', 'L4386', 'Burn of unspecified degree of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('945.03', 'L4386', 'Burn of unspecified degree of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('945.2', 'L4386', 'Blisters with epidermal loss due to burn (second degree) of unspecified site of lower limb (leg)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.78', 'L4386', 'Other complications due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.79', 'L4386', 'Other complications due to other internal prosthetic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('215.9', 'E0218', 'Other benign neoplasm of connective and other soft tissue of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('353', 'E0218', 'Brachial plexus lesions');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354', 'E0218', 'Carpal tunnel syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('553.8', 'E0218', 'Hernia of other specified sites of abdominal cavity without mention of obstruction or gangrene');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.16', 'E0218', 'Primary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.96', 'E0218', 'Osteoarthrosis, unspecified whether generalized or localized, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.98', 'E0218', 'Osteoarthrosis, unspecified whether generalized or localized, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.41', 'E0218', 'Transient arthropathy, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.46', 'E0218', 'Transient arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.96', 'E0218', 'Unspecified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717', 'E0218', 'Old bucket handle tear of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.3', 'E0218', 'Other and unspecified derangement of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.43', 'E0218', 'Derangement of posterior horn of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.5', 'E0218', 'Derangement of meniscus, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.7', 'E0218', 'Chondromalacia of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.83', 'E0218', 'Old disruption of anterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.31', 'E0218', 'Recurrent dislocation of shoulder joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.81', 'E0218', 'Other joint derangement, not elsewhere classified, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.04', 'E0218', 'Effusion of hand joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.26', 'E0218', 'Villonodular synovitis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.41', 'E0218', 'Pain in joint, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.44', 'E0218', 'Pain in joint, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.46', 'E0218', 'Pain in joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.66', 'E0218', 'Other symptoms referable to lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('721', 'E0218', 'Cervical spondylosis without myelopathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('721.3', 'E0218', 'Lumbosacral spondylosis without myelopathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722', 'E0218', 'Displacement of cervical intervertebral disc without myelopathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.1', 'E0218', 'Displacement of lumbar intervertebral disc without myelopathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.2', 'E0218', 'Displacement of intervertebral disc, site unspecified, without myelopathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.52', 'E0218', 'Degeneration of lumbar or lumbosacral intervertebral disc');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.83', 'E0218', 'Postlaminectomy syndrome, lumbar region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.93', 'E0218', 'Other and unspecified disc disorder of lumbar region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.02', 'E0218', 'Spinal stenosis of lumbar region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.2', 'E0218', 'Lumbago');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.4', 'E0218', 'Thoracic or lumbosacral neuritis or radiculitis, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726', 'E0218', 'Adhesive capsulitis of shoulder');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.1', 'E0218', 'Unspecified disorders of bursae and tendons in shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.19', 'E0218', 'Other specified disorders of rotator cuff syndrome of shoulder and allied disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.2', 'E0218', 'Other affections of shoulder region, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.32', 'E0218', 'Lateral epicondylitis of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.79', 'E0218', 'Other enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.04', 'E0218', 'Radial styloid tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.05', 'E0218', 'Other tenosynovitis of hand and wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.61', 'E0218', 'Complete rupture of rotator cuff');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.89', 'E0218', 'Other disorders of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.7', 'E0218', 'Osteochondritis dissecans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.9', 'E0218', 'Unspecified osteochondropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.16', 'E0218', 'Pathologic fracture of tibia and fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.92', 'E0218', 'Chondromalacia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.99', 'E0218', 'Other disorders of bone and cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('802.1', 'E0218', 'Nasal bones, open fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.8', 'E0218', 'Closed fracture of unspecified part of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.81', 'E0218', 'Closed fracture of unspecified part of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.82', 'E0218', 'Closed fracture of unspecified part of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.01', 'E0218', 'Closed anterior dislocation of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836', 'E0218', 'Tear of medial cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.9', 'E0218', 'Sprain and strain of unspecified site of knee and leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845', 'E0218', 'Unspecified site of ankle sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.02', 'E0218', 'Sprain and strain of calcaneofibular (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847', 'E0218', 'Neck sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847.2', 'E0218', 'Lumbar sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.11', 'E0218', 'Contusion of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('956.4', 'E0218', 'Injury to cutaneous sensory nerve, lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.7', 'E0218', 'Injury, other and unspecified, knee, leg, ankle, and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('214.1', 'L1810', 'Lipoma of other skin and subcutaneous tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('214.8', 'L1810', 'Lipoma of other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('215.3', 'L1810', 'Other benign neoplasm of connective and other soft tissue of lower limb, including hip');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('238.1', 'L1810', 'Neoplasm of uncertain behavior of connective and other soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('238.2', 'L1810', 'Neoplasm of uncertain behavior of skin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('274', 'L1810', 'Gouty arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('289.3', 'L1810', 'Lymphadenitis, unspecified, except mesenteric');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.9', 'L1810', 'Unspecified paralysis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('353', 'L1810', 'Brachial plexus lesions');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('213.7', 'L1810', 'Benign neoplasm of long bones of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('214.1', 'L1810', 'Lipoma of other skin and subcutaneous tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('214.8', 'L1810', 'Lipoma of other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('215.3', 'L1810', 'Other benign neoplasm of connective and other soft tissue of lower limb, including hip');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('238.1', 'L1810', 'Neoplasm of uncertain behavior of connective and other soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('238.2', 'L1810', 'Neoplasm of uncertain behavior of skin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('274', 'L1810', 'Gouty arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('289.3', 'L1810', 'Lymphadenitis, unspecified, except mesenteric');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.9', 'L1810', 'Unspecified paralysis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('353', 'L1810', 'Brachial plexus lesions');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.8', 'L1810', 'Unspecified mononeuritis of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('712.36', 'L1810', 'Chondrocalcinosis, cause unspecified, involving lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('713.8', 'L1810', 'Arthropathy associated with other conditions classifiable elsewhere');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L1810', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.3', 'L1810', 'Polyarticular juvenile rheumatoid arthritis, chronic or unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L1810', 'Generalized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.09', 'L1810', 'Generalized osteoarthrosis, involving multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.1', 'L1810', 'Primary localized osteoarthrosis, specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.11', 'L1810', 'Primary localized osteoarthrosis, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.16', 'L1810', 'Primary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.18', 'L1810', 'Primary localized osteoarthrosis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.36', 'L1810', 'Localized osteoarthrosis not specified whether primary or secondary, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.9', 'L1810', 'Osteoarthrosis, unspecified whether generalized or localized, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.96', 'L1810', 'Osteoarthrosis, unspecified whether generalized or localized, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.98', 'L1810', 'Osteoarthrosis, unspecified whether generalized or localized, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.1', 'L1810', 'Traumatic arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.16', 'L1810', 'Traumatic arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.18', 'L1810', 'Traumatic arthropathy, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.4', 'L1810', 'Transient arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.46', 'L1810', 'Transient arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.66', 'L1810', 'Unspecified monoarthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.9', 'L1810', 'Unspecified arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.96', 'L1810', 'Unspecified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.98', 'L1810', 'Unspecified arthropathy, other unspecified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717', 'L1810', 'Old bucket handle tear of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.3', 'L1810', 'Other and unspecified derangement of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.4', 'L1810', 'Unspecified derangement of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.41', 'L1810', 'Bucket handle tear of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.42', 'L1810', 'Derangement of anterior horn of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.43', 'L1810', 'Derangement of posterior horn of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.49', 'L1810', 'Other derangement of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.5', 'L1810', 'Derangement of meniscus, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.6', 'L1810', 'Loose body in knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.7', 'L1810', 'Chondromalacia of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.81', 'L1810', 'Old disruption of lateral collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.82', 'L1810', 'Old disruption of medial collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.83', 'L1810', 'Old disruption of anterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.84', 'L1810', 'Old disruption of posterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.89', 'L1810', 'Other internal derangement of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718', 'L1810', 'Articular cartilage disorder, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.09', 'L1810', 'Articular cartilage disorder, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.2', 'L1810', 'Pathological dislocation of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.26', 'L1810', 'Pathological dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.3', 'L1810', 'Recurrent dislocation of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.36', 'L1810', 'Recurrent dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.38', 'L1810', 'Recurrent dislocation of joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.39', 'L1810', 'Recurrent dislocation of joint of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.46', 'L1810', 'Contracture of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.5', 'L1810', 'Ankylosis of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.56', 'L1810', 'Ankylosis of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.8', 'L1810', 'Other joint derangement, not elsewhere classified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.86', 'L1810', 'Other joint derangement, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.88', 'L1810', 'Other joint derangement, not elsewhere classified, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.89', 'L1810', 'Other joint derangement, not elsewhere classified, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719', 'L1810', 'Effusion of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.06', 'L1810', 'Effusion of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.08', 'L1810', 'Effusion of joint, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.1', 'L1810', 'Hemarthrosis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.16', 'L1810', 'Hemarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.19', 'L1810', 'Hemarthrosis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.21', 'L1810', 'Villonodular synovitis, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.26', 'L1810', 'Villonodular synovitis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.4', 'L1810', 'Pain in joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.46', 'L1810', 'Pain in joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.48', 'L1810', 'Pain in joint, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.49', 'L1810', 'Pain in joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.5', 'L1810', 'Stiffness of joint, not elsewhere classified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.56', 'L1810', 'Stiffness of joint, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.58', 'L1810', 'Stiffness of joint, not elsewhere classified, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.59', 'L1810', 'Stiffness of joints, not elsewhere classified, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.64', 'L1810', 'Other symptoms referable to hand joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.66', 'L1810', 'Other symptoms referable to lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.86', 'L1810', 'Other specified disorders of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.9', 'L1810', 'Unspecified disorder of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.96', 'L1810', 'Unspecified disorder of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.98', 'L1810', 'Unspecified joint disorder of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.61', 'L1810', 'Pes anserinus tendinitis or bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.62', 'L1810', 'Tibial collateral ligament bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.64', 'L1810', 'Patellar tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.1', 'L1830', 'Paraplegia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.89', 'L1830', 'Other specified paralytic syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.9', 'L1830', 'Unspecified paralysis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.8', 'L1830', 'Unspecified mononeuritis of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.9', 'L1830', 'Mononeuritis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('453.9', 'L1830', 'Embolism and thrombosis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('459.81', 'L1830', 'Unspecified venous (peripheral) insufficiency');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('459.89', 'L1830', 'Other specified circulatory system disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('682.6', 'L1830', 'Cellulitis and abscess of leg, except foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('709.9', 'L1830', 'Unspecified disorder of skin and subcutaneous tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('711.06', 'L1830', 'Pyogenic arthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L1830', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.3', 'L1830', 'Polyarticular juvenile rheumatoid arthritis, chronic or unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L1830', 'Generalized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.09', 'L1830', 'Generalized osteoarthrosis, involving multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.16', 'L1830', 'Primary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.17', 'L1830', 'Primary localized osteoarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.18', 'L1830', 'Primary localized osteoarthrosis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.2', 'L1830', 'Secondary localized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.26', 'L1830', 'Secondary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.28', 'L1830', 'Secondary localized osteoarthrosis, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.3', 'L1830', 'Localized osteoarthrosis not specified whether primary or secondary, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.36', 'L1830', 'Localized osteoarthrosis not specified whether primary or secondary, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.38', 'L1830', 'Localized osteoarthrosis not specified whether primary or secondary, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.8', 'L1830', 'Osteoarthrosis involving more than one site, but not specified as generalized, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.89', 'L1830', 'Osteoarthrosis involving multiple sites, but not specified as generalized');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.9', 'L1830', 'Osteoarthrosis, unspecified whether generalized or localized, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.96', 'L1830', 'Osteoarthrosis, unspecified whether generalized or localized, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716', 'L1830', 'Kaschin-Beck disease, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.06', 'L1830', 'Kaschin-Beck disease, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.1', 'L1830', 'Traumatic arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.16', 'L1830', 'Traumatic arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.46', 'L1830', 'Transient arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.66', 'L1830', 'Unspecified monoarthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.9', 'L1830', 'Unspecified arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.96', 'L1830', 'Unspecified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.98', 'L1830', 'Unspecified arthropathy, other unspecified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717', 'L1830', 'Old bucket handle tear of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.1', 'L1830', 'Derangement of anterior horn of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.2', 'L1830', 'Derangement of posterior horn of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.3', 'L1830', 'Other and unspecified derangement of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.4', 'L1830', 'Unspecified derangement of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.41', 'L1830', 'Bucket handle tear of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.42', 'L1830', 'Derangement of anterior horn of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.43', 'L1830', 'Derangement of posterior horn of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.49', 'L1830', 'Other derangement of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.5', 'L1830', 'Derangement of meniscus, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.6', 'L1830', 'Loose body in knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.7', 'L1830', 'Chondromalacia of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.81', 'L1830', 'Old disruption of lateral collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.82', 'L1830', 'Old disruption of medial collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.83', 'L1830', 'Old disruption of anterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.84', 'L1830', 'Old disruption of posterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.85', 'L1830', 'Old disruption of other ligament of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.89', 'L1830', 'Other internal derangement of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.9', 'L1830', 'Unspecified internal derangement of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.1', 'L1830', 'Loose body in joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.26', 'L1830', 'Pathological dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.27', 'L1830', 'Pathological dislocation of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.28', 'L1830', 'Pathological dislocation of joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.29', 'L1830', 'Pathological dislocation of joint of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.3', 'L1830', 'Recurrent dislocation of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.36', 'L1830', 'Recurrent dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.38', 'L1830', 'Recurrent dislocation of joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.39', 'L1830', 'Recurrent dislocation of joint of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.4', 'L1830', 'Contracture of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.46', 'L1830', 'Contracture of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.48', 'L1830', 'Contracture of joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.49', 'L1830', 'Contracture of joint of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.5', 'L1830', 'Ankylosis of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.56', 'L1830', 'Ankylosis of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.86', 'L1830', 'Other joint derangement, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719', 'L1830', 'Effusion of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.06', 'L1830', 'Effusion of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.1', 'L1830', 'Hemarthrosis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.16', 'L1830', 'Hemarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.26', 'L1830', 'Villonodular synovitis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.36', 'L1830', 'Palindromic rheumatism, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.46', 'L1830', 'Pain in joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.56', 'L1830', 'Stiffness of joint, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.66', 'L1830', 'Other symptoms referable to lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.7', 'L1830', 'Difficulty in walking');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.86', 'L1830', 'Other specified disorders of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.96', 'L1830', 'Unspecified disorder of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.6', 'L1830', 'Unspecified enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.61', 'L1830', 'Pes anserinus tendinitis or bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.64', 'L1830', 'Patellar tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.65', 'L1830', 'Prepatellar bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.69', 'L1830', 'Other enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.71', 'L1830', 'Achilles bursitis or tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.72', 'L1830', 'Tibialis tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L1830', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727', 'L1830', 'Unspecified synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L1830', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.3', 'L1830', 'Other bursitis disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.4', 'L1830', 'Unspecified synovial cyst');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.43', 'L1830', 'Unspecified ganglion');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.51', 'L1830', 'Synovial cyst of popliteal space');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.66', 'L1830', 'Nontraumatic rupture of patellar tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.81', 'L1830', 'Contracture of tendon (sheath)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.89', 'L1830', 'Other disorders of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.71', 'L1830', 'Plantar fascial fibromatosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.85', 'L1830', 'Spasm of muscle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.89', 'L1830', 'Other disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.9', 'L1830', 'Unspecified disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.81', 'L1830', 'Swelling of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.82', 'L1830', 'Cramp of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.89', 'L1830', 'Other musculoskeletal symptoms referable to limbs');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.9', 'L1830', 'Other and unspecified disorders of soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.4', 'L1830', 'Juvenile osteochondrosis of lower extremity, excluding foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.6', 'L1830', 'Other juvenile osteochondrosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.7', 'L1830', 'Osteochondritis dissecans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.8', 'L1830', 'Other specified forms of osteochondropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.1', 'L1830', 'Pathologic fracture, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.14', 'L1830', 'Pathologic fracture of neck of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.16', 'L1830', 'Pathologic fracture of tibia and fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.42', 'L1830', 'Aseptic necrosis of head and neck of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.6', 'L1830', 'Tietze''s disease');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.82', 'L1830', 'Nonunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.9', 'L1830', 'Disorder of bone and cartilage, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.92', 'L1830', 'Chondromalacia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.6', 'L1830', 'Other acquired deformities of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.75', 'L1830', 'Cavovarus deformity of foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.89', 'L1830', 'Other acquired deformity of other parts of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('745.69', 'L1830', 'Other congenital endocardial cushion defect');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.41', 'L1830', 'Congenital dislocation of knee (with genu recurvatum)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.3', 'L1830', 'Congenital unspecified reduction deformity of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.6', 'L1830', 'Unspecified congenital anomaly of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.64', 'L1830', 'Congenital deformity of knee (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.69', 'L1830', 'Other congenital anomaly of lower limb, including pelvic girdle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('757.39', 'L1830', 'Other specified congenital anomaly of skin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821', 'L1830', 'Closed fracture of unspecified part of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.01', 'L1830', 'Closed fracture of shaft of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.2', 'L1830', 'Closed fracture of unspecified part of lower end of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.21', 'L1830', 'Closed fracture of femoral condyle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.22', 'L1830', 'Closed fracture of lower epiphysis of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.23', 'L1830', 'Closed supracondylar fracture of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.31', 'L1830', 'Open fracture of femoral condyle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('822', 'L1830', 'Closed fracture of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('822.1', 'L1830', 'Open fracture of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823', 'L1830', 'Closed fracture of upper end of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.01', 'L1830', 'Closed fracture of upper end of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.02', 'L1830', 'Closed fracture of upper end of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.11', 'L1830', 'Open fracture of upper end of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.12', 'L1830', 'Open fracture of upper end of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836', 'L1830', 'Tear of medial cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.5', 'L1830', 'Closed dislocation of knee, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.52', 'L1830', 'Closed posterior dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.59', 'L1830', 'Other closed dislocation of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.6', 'L1830', 'Open dislocation of knee unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844', 'L1830', 'Sprain and strain of lateral collateral ligament of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.1', 'L1830', 'Sprain and strain of medial collateral ligament of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.2', 'L1830', 'Sprain and strain of cruciate ligament of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.3', 'L1830', 'Sprain and strain of tibiofibular (joint) (ligament) superior, of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.8', 'L1830', 'Sprain and strain of other specified sites of knee and leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.9', 'L1830', 'Sprain and strain of unspecified site of knee and leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('894', 'L1830', 'Multiple and unspecified open wound of lower limb, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('904.8', 'L1830', 'Injury to unspecified blood vessel of lower extremity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('916', 'L1830', 'Hip, thigh, leg, and ankle, abrasion or friction burn, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('916.8', 'L1830', 'Other and unspecified superficial injury of hip, thigh, leg, and ankle, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('919', 'L1830', 'Abrasion or friction burn of other, multiple, and unspecified sites, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924', 'L1830', 'Contusion of thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.1', 'L1830', 'Contusion of lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.11', 'L1830', 'Contusion of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.4', 'L1830', 'Contusion of multiple sites of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.5', 'L1830', 'Contusion of unspecified part of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('957.9', 'L1830', 'Injury to nerves, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('958.3', 'L1830', 'Posttraumatic wound infection not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('958.8', 'L1830', 'Other early complications of trauma');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.7', 'L1830', 'Injury, other and unspecified, knee, leg, ankle, and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.8', 'L1830', 'Injury, other and unspecified, other specified sites, including multiple');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.9', 'L1830', 'Injury, other and unspecified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.59', 'L1830', 'Mechanical complication due to other implant and internal device, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.66', 'L1830', 'Infection and inflammatory reaction due to internal joint prosthesis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.7', 'L1830', 'Other complications due to unspecified device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.77', 'L1830', 'Other complications due to internal joint prosthesis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.79', 'L1830', 'Other complications due to other internal prosthetic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.59', 'L1830', 'Other immobilization, pressure, and attention to wound');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.23', 'L1830', 'Fitting of orthotic device');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('213.7', 'L1820', 'Benign neoplasm of long bones of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('214.1', 'L1820', 'Lipoma of other skin and subcutaneous tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('214.8', 'L1820', 'Lipoma of other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('215.3', 'L1820', 'Other benign neoplasm of connective and other soft tissue of lower limb, including hip');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('238.1', 'L1820', 'Neoplasm of uncertain behavior of connective and other soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('238.2', 'L1820', 'Neoplasm of uncertain behavior of skin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('274', 'L1820', 'Gouty arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('289.3', 'L1820', 'Lymphadenitis, unspecified, except mesenteric');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.9', 'L1820', 'Unspecified paralysis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('353', 'L1820', 'Brachial plexus lesions');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('213.7', 'L1820', 'Benign neoplasm of long bones of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('214.1', 'L1820', 'Lipoma of other skin and subcutaneous tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('214.8', 'L1820', 'Lipoma of other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('215.3', 'L1820', 'Other benign neoplasm of connective and other soft tissue of lower limb, including hip');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('238.1', 'L1820', 'Neoplasm of uncertain behavior of connective and other soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('238.2', 'L1820', 'Neoplasm of uncertain behavior of skin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('274', 'L1820', 'Gouty arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('289.3', 'L1820', 'Lymphadenitis, unspecified, except mesenteric');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.9', 'L1820', 'Unspecified paralysis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('353', 'L1820', 'Brachial plexus lesions');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.8', 'L1820', 'Unspecified mononeuritis of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('712.36', 'L1820', 'Chondrocalcinosis, cause unspecified, involving lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('713.8', 'L1820', 'Arthropathy associated with other conditions classifiable elsewhere');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L1820', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.3', 'L1820', 'Polyarticular juvenile rheumatoid arthritis, chronic or unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L1820', 'Generalized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.09', 'L1820', 'Generalized osteoarthrosis, involving multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.1', 'L1820', 'Primary localized osteoarthrosis, specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.11', 'L1820', 'Primary localized osteoarthrosis, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.16', 'L1820', 'Primary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.18', 'L1820', 'Primary localized osteoarthrosis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.36', 'L1820', 'Localized osteoarthrosis not specified whether primary or secondary, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.9', 'L1820', 'Osteoarthrosis, unspecified whether generalized or localized, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.96', 'L1820', 'Osteoarthrosis, unspecified whether generalized or localized, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.98', 'L1820', 'Osteoarthrosis, unspecified whether generalized or localized, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.1', 'L1820', 'Traumatic arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.16', 'L1820', 'Traumatic arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.18', 'L1820', 'Traumatic arthropathy, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.4', 'L1820', 'Transient arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.46', 'L1820', 'Transient arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.66', 'L1820', 'Unspecified monoarthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.9', 'L1820', 'Unspecified arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.96', 'L1820', 'Unspecified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.98', 'L1820', 'Unspecified arthropathy, other unspecified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717', 'L1820', 'Old bucket handle tear of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.3', 'L1820', 'Other and unspecified derangement of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.4', 'L1820', 'Unspecified derangement of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.41', 'L1820', 'Bucket handle tear of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.42', 'L1820', 'Derangement of anterior horn of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.43', 'L1820', 'Derangement of posterior horn of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.49', 'L1820', 'Other derangement of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.5', 'L1820', 'Derangement of meniscus, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.6', 'L1820', 'Loose body in knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.7', 'L1820', 'Chondromalacia of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.81', 'L1820', 'Old disruption of lateral collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.82', 'L1820', 'Old disruption of medial collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.83', 'L1820', 'Old disruption of anterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.84', 'L1820', 'Old disruption of posterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.89', 'L1820', 'Other internal derangement of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718', 'L1820', 'Articular cartilage disorder, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.09', 'L1820', 'Articular cartilage disorder, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.2', 'L1820', 'Pathological dislocation of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.26', 'L1820', 'Pathological dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.3', 'L1820', 'Recurrent dislocation of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.36', 'L1820', 'Recurrent dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.38', 'L1820', 'Recurrent dislocation of joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.39', 'L1820', 'Recurrent dislocation of joint of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.46', 'L1820', 'Contracture of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.5', 'L1820', 'Ankylosis of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.56', 'L1820', 'Ankylosis of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.8', 'L1820', 'Other joint derangement, not elsewhere classified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.86', 'L1820', 'Other joint derangement, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.88', 'L1820', 'Other joint derangement, not elsewhere classified, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.89', 'L1820', 'Other joint derangement, not elsewhere classified, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719', 'L1820', 'Effusion of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.06', 'L1820', 'Effusion of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.08', 'L1820', 'Effusion of joint, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.1', 'L1820', 'Hemarthrosis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.16', 'L1820', 'Hemarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.19', 'L1820', 'Hemarthrosis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.21', 'L1820', 'Villonodular synovitis, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.26', 'L1820', 'Villonodular synovitis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.4', 'L1820', 'Pain in joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.46', 'L1820', 'Pain in joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.48', 'L1820', 'Pain in joint, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.49', 'L1820', 'Pain in joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.5', 'L1820', 'Stiffness of joint, not elsewhere classified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.56', 'L1820', 'Stiffness of joint, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.58', 'L1820', 'Stiffness of joint, not elsewhere classified, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.59', 'L1820', 'Stiffness of joints, not elsewhere classified, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.64', 'L1820', 'Other symptoms referable to hand joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.66', 'L1820', 'Other symptoms referable to lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.86', 'L1820', 'Other specified disorders of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.9', 'L1820', 'Unspecified disorder of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.96', 'L1820', 'Unspecified disorder of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.98', 'L1820', 'Unspecified joint disorder of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.61', 'L1820', 'Pes anserinus tendinitis or bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.62', 'L1820', 'Tibial collateral ligament bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.64', 'L1820', 'Patellar tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.65', 'L1820', 'Prepatellar bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.69', 'L1820', 'Other enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.7', 'L1820', 'Unspecified enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.71', 'L1820', 'Achilles bursitis or tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.79', 'L1820', 'Other enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L1820', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.91', 'L1820', 'Exostosis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727', 'L1820', 'Unspecified synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L1820', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.41', 'L1820', 'Ganglion of joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.42', 'L1820', 'Ganglion of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.5', 'L1820', 'Unspecified rupture of synovium');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.51', 'L1820', 'Synovial cyst of popliteal space');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.6', 'L1820', 'Nontraumatic rupture of unspecified tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.66', 'L1820', 'Nontraumatic rupture of patellar tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.81', 'L1820', 'Contracture of tendon (sheath)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.83', 'L1820', 'Plica syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.89', 'L1820', 'Other disorders of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.12', 'L1820', 'Traumatic myositis ossificans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.89', 'L1820', 'Other disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.9', 'L1820', 'Unspecified disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.1', 'L1820', 'Unspecified myalgia and myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L1820', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.81', 'L1820', 'Swelling of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.89', 'L1820', 'Other musculoskeletal symptoms referable to limbs');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.9', 'L1820', 'Other and unspecified disorders of soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.4', 'L1820', 'Juvenile osteochondrosis of lower extremity, excluding foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.6', 'L1820', 'Other juvenile osteochondrosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.7', 'L1820', 'Osteochondritis dissecans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.9', 'L1820', 'Unspecified osteochondropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733', 'L1820', 'Unspecified osteoporosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.2', 'L1820', 'Unspecified cyst of bone (localized)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.4', 'L1820', 'Aseptic necrosis of bone, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.6', 'L1820', 'Tietze''s disease');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.82', 'L1820', 'Nonunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.9', 'L1820', 'Disorder of bone and cartilage, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.91', 'L1820', 'Arrest of bone development or growth');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.92', 'L1820', 'Chondromalacia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.99', 'L1820', 'Other disorders of bone and cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.41', 'L1820', 'Genu valgum (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('738.8', 'L1820', 'Acquired musculoskeletal deformity of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('739.6', 'L1820', 'Nonallopathic lesion of lower extremities, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('739.6', 'L1820', 'Nonallopathic lesion of lower extremities, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.6', 'L1820', 'Unspecified congenital anomaly of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.64', 'L1820', 'Congenital deformity of knee (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.67', 'L1820', 'Congenital anomalies of foot, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.69', 'L1820', 'Other congenital anomaly of lower limb, including pelvic girdle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.8', 'L1820', 'Other specified congenital anomalies of unspecified limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('780.79', 'L1820', 'Other malaise and fatigue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.06', 'L1820', 'Closed fracture of neck of radius');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.42', 'L1820', 'Other closed fractures of distal end of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821', 'L1820', 'Closed fracture of unspecified part of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.2', 'L1820', 'Closed fracture of unspecified part of lower end of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.21', 'L1820', 'Closed fracture of femoral condyle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('822', 'L1820', 'Closed fracture of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823', 'L1820', 'Closed fracture of upper end of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.01', 'L1820', 'Closed fracture of upper end of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.02', 'L1820', 'Closed fracture of upper end of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.11', 'L1820', 'Open fracture of upper end of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.12', 'L1820', 'Open fracture of upper end of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.2', 'L1820', 'Closed fracture of shaft of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.22', 'L1820', 'Closed fracture of shaft of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.8', 'L1820', 'Closed fracture of unspecified part of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.81', 'L1820', 'Closed fracture of unspecified part of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('829', 'L1820', 'Closed fracture of unspecified bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836', 'L1820', 'Tear of medial cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.5', 'L1820', 'Closed dislocation of knee, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.59', 'L1820', 'Other closed dislocation of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.9', 'L1820', 'Sprain and strain of unspecified site of knee and leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.8', 'L1820', 'Other specified sites of sprains and strains');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('904.8', 'L1820', 'Injury to unspecified blood vessel of lower extremity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('916', 'L1820', 'Hip, thigh, leg, and ankle, abrasion or friction burn, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('919', 'L1820', 'Abrasion or friction burn of other, multiple, and unspecified sites, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('919.8', 'L1820', 'Other and unspecified superficial injury of other, multiple, and unspecified sites, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.1', 'L1820', 'Contusion of lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.11', 'L1820', 'Contusion of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.4', 'L1820', 'Contusion of multiple sites of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.5', 'L1820', 'Contusion of unspecified part of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.8', 'L1820', 'Contusion of multiple sites, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.9', 'L1820', 'Contusion of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('928.11', 'L1820', 'Crushing injury of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('958.8', 'L1820', 'Other early complications of trauma');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.7', 'L1820', 'Injury, other and unspecified, knee, leg, ankle, and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.8', 'L1820', 'Injury, other and unspecified, other specified sites, including multiple');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.9', 'L1820', 'Injury, other and unspecified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.66', 'L1820', 'Infection and inflammatory reaction due to internal joint prosthesis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.67', 'L1820', 'Infection and inflammatory reaction due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.77', 'L1820', 'Other complications due to internal joint prosthesis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.78', 'L1820', 'Other complications due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.59', 'L1820', 'Other immobilization, pressure, and attention to wound');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.23', 'L1820', 'Fitting of orthotic device');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('335.1', 'L2795', 'Unspecified spinal muscular atrophy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('343.9', 'L2795', 'Unspecified infantile cerebral palsy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.01', 'L2795', 'Quadriplegia and quadriparesis, C1-C4, complete');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.1', 'L2795', 'Paraplegia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.2', 'L2795', 'Diplegia of upper limbs');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('451', 'L2795', 'Phlebitis and thrombophlebitis of superficial vessels of lower extremities');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.16', 'L2795', 'Primary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.36', 'L2795', 'Localized osteoarthrosis not specified whether primary or secondary, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.96', 'L2795', 'Osteoarthrosis, unspecified whether generalized or localized, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.14', 'L2795', 'Traumatic arthropathy, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.96', 'L2795', 'Unspecified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.99', 'L2795', 'Unspecified arthropathy, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.3', 'L2795', 'Other and unspecified derangement of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.7', 'L2795', 'Chondromalacia of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.83', 'L2795', 'Old disruption of anterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.36', 'L2795', 'Recurrent dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.46', 'L2795', 'Contracture of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.86', 'L2795', 'Other joint derangement, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.06', 'L2795', 'Effusion of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.46', 'L2795', 'Pain in joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.86', 'L2795', 'Other specified disorders of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.61', 'L2795', 'Pes anserinus tendinitis or bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.64', 'L2795', 'Patellar tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.69', 'L2795', 'Other enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L2795', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727', 'L2795', 'Unspecified synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.01', 'L2795', 'Synovitis and tenosynovitis in diseases classified elsewhere');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L2795', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.2', 'L2795', 'Specific bursitides often of occupational origin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.89', 'L2795', 'Other disorders of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.3', 'L2795', 'Other specific muscle disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.9', 'L2795', 'Unspecified disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L2795', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.4', 'L2795', 'Juvenile osteochondrosis of lower extremity, excluding foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.6', 'L2795', 'Other juvenile osteochondrosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.7', 'L2795', 'Osteochondritis dissecans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.92', 'L2795', 'Chondromalacia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.5', 'L2795', 'Genu recurvatum (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.79', 'L2795', 'Other acquired deformity of ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.81', 'L2795', 'Unequal leg length (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('741', 'L2795', 'Spina bifida with hydrocephalus, unspecified region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('741.91', 'L2795', 'Spina bifida without mention of hydrocephalus, cervical region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.64', 'L2795', 'Congenital deformity of knee (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.69', 'L2795', 'Other congenital anomaly of lower limb, including pelvic girdle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('805.4', 'L2795', 'Closed fracture of lumbar vertebra without mention of spinal cord injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('822', 'L2795', 'Closed fracture of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823', 'L2795', 'Closed fracture of upper end of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836', 'L2795', 'Tear of medial cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.5', 'L2795', 'Closed dislocation of knee, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.9', 'L2795', 'Sprain and strain of unspecified site of knee and leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845', 'L2795', 'Unspecified site of ankle sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.1', 'L2795', 'Sprain and strain of unspecified site of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847.2', 'L2795', 'Lumbar sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.9', 'L2795', 'Unspecified site of sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.11', 'L2795', 'Contusion of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.7', 'L2795', 'Injury, other and unspecified, knee, leg, ankle, and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('335.1', 'L2795', 'Unspecified spinal muscular atrophy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('343.9', 'L2795', 'Unspecified infantile cerebral palsy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.1', 'L2795', 'Paraplegia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.3', 'L2795', 'Monoplegia of lower limb affecting unspecified side');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.5', 'L2795', 'Tarsal tunnel syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('357.9', 'L2795', 'Unspecified inflammatory and toxic neuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.16', 'L2795', 'Primary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.96', 'L2795', 'Osteoarthrosis, unspecified whether generalized or localized, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.3', 'L2795', 'Other and unspecified derangement of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.7', 'L2795', 'Chondromalacia of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.82', 'L2795', 'Old disruption of medial collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.83', 'L2795', 'Old disruption of anterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.86', 'L2795', 'Other joint derangement, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.88', 'L2795', 'Other joint derangement, not elsewhere classified, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.06', 'L2795', 'Effusion of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.46', 'L2795', 'Pain in joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.47', 'L2795', 'Pain in joint, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.96', 'L2795', 'Unspecified disorder of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.5', 'L2795', 'Enthesopathy of hip region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.64', 'L2795', 'Patellar tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L2795', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.7', 'L2795', 'Osteochondritis dissecans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.5', 'L2795', 'Genu recurvatum (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('741.91', 'L2795', 'Spina bifida without mention of hydrocephalus, cervical region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.89', 'L2795', 'Other specified nonteratogenic anomalies');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('759.81', 'L2795', 'Prader-Willi syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.11', 'L2795', 'Open fracture of olecranon process of ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821', 'L2795', 'Closed fracture of unspecified part of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.2', 'L2795', 'Closed fracture of unspecified part of lower end of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('822', 'L2795', 'Closed fracture of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823', 'L2795', 'Closed fracture of upper end of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836', 'L2795', 'Tear of medial cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.5', 'L2795', 'Closed dislocation of knee, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.59', 'L2795', 'Other closed dislocation of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('843', 'L2795', 'Iliofemoral (ligament) sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.9', 'L2795', 'Sprain and strain of unspecified site of knee and leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.09', 'L2795', 'Other ankle sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('846.9', 'L2795', 'Unspecified site of sacroiliac region sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.11', 'L2795', 'Contusion of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.7', 'L2795', 'Injury, other and unspecified, knee, leg, ankle, and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('174.9', 'L2795', 'Malignant neoplasm of breast (female), unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('215.3', 'L2795', 'Other benign neoplasm of connective and other soft tissue of lower limb, including hip');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250', 'L2795', 'Diabetes mellitus without mention of complication, type II or unspecified type, not stated as uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('268.2', 'L2795', 'Osteomalacia, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('274', 'L2795', 'Gouty arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('275.4', 'L2795', 'Unspecified disorder of calcium metabolism');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('343.8', 'L2795', 'Other specified infantile cerebral palsy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.9', 'L2795', 'Unspecified paralysis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('346', 'L2795', 'Classical migraine without mention of intractable migraine');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('353.8', 'L2795', 'Other nerve root and plexus disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.5', 'L2795', 'Tarsal tunnel syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.6', 'L2795', 'Lesion of plantar nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.71', 'L2795', 'Causalgia of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.8', 'L2795', 'Unspecified mononeuritis of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('367.9', 'L2795', 'Unspecified disorder of refraction and accommodation');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('384.2', 'L2795', 'Unspecified perforation of tympanic membrane');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('385.3', 'L2795', 'Unspecified cholesteatoma');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('386.12', 'L2795', 'Vestibular neuronitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('453.9', 'L2795', 'Embolism and thrombosis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('459.81', 'L2795', 'Unspecified venous (peripheral) insufficiency');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('608.9', 'L2795', 'Unspecified disorder of male genital organs');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('680.9', 'L2795', 'Carbuncle and furuncle of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('682.6', 'L2795', 'Cellulitis and abscess of leg, except foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('711.06', 'L2795', 'Pyogenic arthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L2795', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.3', 'L2795', 'Polyarticular juvenile rheumatoid arthritis, chronic or unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.33', 'L2795', 'Monoarticular juvenile rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.9', 'L2795', 'Unspecified inflammatory polyarthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L2795', 'Generalized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.1', 'L2795', 'Primary localized osteoarthrosis, specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.16', 'L2795', 'Primary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.17', 'L2795', 'Primary localized osteoarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.18', 'L2795', 'Primary localized osteoarthrosis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.26', 'L2795', 'Secondary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.3', 'L2795', 'Localized osteoarthrosis not specified whether primary or secondary, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.36', 'L2795', 'Localized osteoarthrosis not specified whether primary or secondary, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.89', 'L2795', 'Osteoarthrosis involving multiple sites, but not specified as generalized');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.9', 'L2795', 'Osteoarthrosis, unspecified whether generalized or localized, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.95', 'L2795', 'Osteoarthrosis, unspecified whether generalized or localized, pelvic region and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.96', 'L2795', 'Osteoarthrosis, unspecified whether generalized or localized, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.98', 'L2795', 'Osteoarthrosis, unspecified whether generalized or localized, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.15', 'L2795', 'Traumatic arthropathy, pelvic region and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.16', 'L2795', 'Traumatic arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.36', 'L2795', 'Climacteric arthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.46', 'L2795', 'Transient arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.6', 'L2795', 'Unspecified monoarthritis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.66', 'L2795', 'Unspecified monoarthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.86', 'L2795', 'Other specified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.9', 'L2795', 'Unspecified arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.95', 'L2795', 'Unspecified arthropathy, pelvic region and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.96', 'L2795', 'Unspecified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.98', 'L2795', 'Unspecified arthropathy, other unspecified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.99', 'L2795', 'Unspecified arthropathy, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717', 'L2795', 'Old bucket handle tear of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.3', 'L2795', 'Other and unspecified derangement of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.4', 'L2795', 'Unspecified derangement of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.41', 'L2795', 'Bucket handle tear of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.42', 'L2795', 'Derangement of anterior horn of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.43', 'L2795', 'Derangement of posterior horn of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.49', 'L2795', 'Other derangement of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.5', 'L2795', 'Derangement of meniscus, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.6', 'L2795', 'Loose body in knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.7', 'L2795', 'Chondromalacia of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.81', 'L2795', 'Old disruption of lateral collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.82', 'L2795', 'Old disruption of medial collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.83', 'L2795', 'Old disruption of anterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.84', 'L2795', 'Old disruption of posterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.85', 'L2795', 'Old disruption of other ligament of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.89', 'L2795', 'Other internal derangement of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718', 'L2795', 'Articular cartilage disorder, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.08', 'L2795', 'Articular cartilage disorder, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.2', 'L2795', 'Pathological dislocation of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.26', 'L2795', 'Pathological dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.3', 'L2795', 'Recurrent dislocation of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.36', 'L2795', 'Recurrent dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.5', 'L2795', 'Ankylosis of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.56', 'L2795', 'Ankylosis of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.8', 'L2795', 'Other joint derangement, not elsewhere classified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.86', 'L2795', 'Other joint derangement, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.87', 'L2795', 'Other joint derangement, not elsewhere classified, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.88', 'L2795', 'Other joint derangement, not elsewhere classified, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719', 'L2795', 'Effusion of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.06', 'L2795', 'Effusion of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.08', 'L2795', 'Effusion of joint, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.16', 'L2795', 'Hemarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.26', 'L2795', 'Villonodular synovitis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.28', 'L2795', 'Villonodular synovitis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.36', 'L2795', 'Palindromic rheumatism, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.4', 'L2795', 'Pain in joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.45', 'L2795', 'Pain in joint, pelvic region and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.46', 'L2795', 'Pain in joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.47', 'L2795', 'Pain in joint, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.48', 'L2795', 'Pain in joint, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.56', 'L2795', 'Stiffness of joint, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.66', 'L2795', 'Other symptoms referable to lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.68', 'L2795', 'Other symptoms referable to joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.7', 'L2795', 'Difficulty in walking');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.8', 'L2795', 'Other specified disorders of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.86', 'L2795', 'Other specified disorders of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.9', 'L2795', 'Unspecified disorder of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.96', 'L2795', 'Unspecified disorder of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('721.1', 'L2795', 'Cervical spondylosis with myelopathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('721.8', 'L2795', 'Other allied disorders of spine');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.3', 'L2795', 'Sciatica');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.4', 'L2795', 'Thoracic or lumbosacral neuritis or radiculitis, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.5', 'L2795', 'Unspecified backache');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.6', 'L2795', 'Disorders of sacrum');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.8', 'L2795', 'Other symptoms referable to back');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.6', 'L2795', 'Unspecified enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.62', 'L2795', 'Tibial collateral ligament bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.64', 'L2795', 'Patellar tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.65', 'L2795', 'Prepatellar bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.69', 'L2795', 'Other enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.7', 'L2795', 'Unspecified enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.71', 'L2795', 'Achilles bursitis or tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.72', 'L2795', 'Tibialis tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.79', 'L2795', 'Other enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L2795', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.91', 'L2795', 'Exostosis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727', 'L2795', 'Unspecified synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.01', 'L2795', 'Synovitis and tenosynovitis in diseases classified elsewhere');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.02', 'L2795', 'Giant cell tumor of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.06', 'L2795', 'Tenosynovitis of foot and ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L2795', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.3', 'L2795', 'Other bursitis disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.41', 'L2795', 'Ganglion of joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.42', 'L2795', 'Ganglion of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.49', 'L2795', 'Other ganglion and cyst of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.5', 'L2795', 'Unspecified rupture of synovium');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.51', 'L2795', 'Synovial cyst of popliteal space');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.81', 'L2795', 'Contracture of tendon (sheath)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.82', 'L2795', 'Calcium deposits in tendon and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.83', 'L2795', 'Plica syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.89', 'L2795', 'Other disorders of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.4', 'L2795', 'Laxity of ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.6', 'L2795', 'Contracture of palmar fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.71', 'L2795', 'Plantar fascial fibromatosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.81', 'L2795', 'Interstitial myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.89', 'L2795', 'Other disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.9', 'L2795', 'Unspecified disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729', 'L2795', 'Rheumatism, unspecified and fibrositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.1', 'L2795', 'Unspecified myalgia and myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.2', 'L2795', 'Unspecified neuralgia, neuritis, and radiculitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.31', 'L2795', 'Hypertrophy of fat pad, knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L2795', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.81', 'L2795', 'Swelling of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.82', 'L2795', 'Cramp of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.36', 'L2795', 'Periostitis, without mention of osteomyelitis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.4', 'L2795', 'Juvenile osteochondrosis of lower extremity, excluding foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.7', 'L2795', 'Osteochondritis dissecans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.21', 'L2795', 'Solitary bone cyst');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.42', 'L2795', 'Aseptic necrosis of head and neck of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.7', 'L2795', 'Algoneurodystrophy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.81', 'L2795', 'Malunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.82', 'L2795', 'Nonunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.9', 'L2795', 'Disorder of bone and cartilage, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.91', 'L2795', 'Arrest of bone development or growth');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.92', 'L2795', 'Chondromalacia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.99', 'L2795', 'Other disorders of bone and cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736', 'L2795', 'Unspecified deformity of forearm, excluding fingers');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.42', 'L2795', 'Genu varum (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.6', 'L2795', 'Other acquired deformities of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('737.3', 'L2795', 'Scoliosis (and kyphoscoliosis), idiopathic');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('738.8', 'L2795', 'Acquired musculoskeletal deformity of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('739.6', 'L2795', 'Nonallopathic lesion of lower extremities, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.6', 'L2795', 'Unspecified congenital anomaly of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.63', 'L2795', 'Other congenital deformity of hip (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.64', 'L2795', 'Congenital deformity of knee (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.69', 'L2795', 'Other congenital anomaly of lower limb, including pelvic girdle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.83', 'L2795', 'Ehlers-Danlos syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('820.21', 'L2795', 'Closed fracture of intertrochanteric section of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821', 'L2795', 'Closed fracture of unspecified part of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.01', 'L2795', 'Closed fracture of shaft of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.2', 'L2795', 'Closed fracture of unspecified part of lower end of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.21', 'L2795', 'Closed fracture of femoral condyle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('822', 'L2795', 'Closed fracture of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823', 'L2795', 'Closed fracture of upper end of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.01', 'L2795', 'Closed fracture of upper end of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.02', 'L2795', 'Closed fracture of upper end of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.1', 'L2795', 'Open fracture of upper end of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.2', 'L2795', 'Closed fracture of shaft of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.8', 'L2795', 'Closed fracture of unspecified part of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.81', 'L2795', 'Closed fracture of unspecified part of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.82', 'L2795', 'Closed fracture of unspecified part of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836', 'L2795', 'Tear of medial cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.5', 'L2795', 'Closed dislocation of knee, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.54', 'L2795', 'Closed lateral dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.59', 'L2795', 'Other closed dislocation of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('843', 'L2795', 'Iliofemoral (ligament) sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('843.9', 'L2795', 'Sprain and strain of unspecified site of hip and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.9', 'L2795', 'Sprain and strain of unspecified site of knee and leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845', 'L2795', 'Unspecified site of ankle sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.01', 'L2795', 'Sprain and strain of deltoid (ligament) of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.02', 'L2795', 'Sprain and strain of calcaneofibular (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.03', 'L2795', 'Sprain and strain of tibiofibular (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.09', 'L2795', 'Other ankle sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('904.8', 'L2795', 'Injury to unspecified blood vessel of lower extremity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('905.7', 'L2795', 'Late effect of sprain and strain without mention of tendon injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('916', 'L2795', 'Hip, thigh, leg, and ankle, abrasion or friction burn, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('916.8', 'L2795', 'Other and unspecified superficial injury of hip, thigh, leg, and ankle, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924', 'L2795', 'Contusion of thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.01', 'L2795', 'Contusion of hip');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.1', 'L2795', 'Contusion of lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.11', 'L2795', 'Contusion of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.4', 'L2795', 'Contusion of multiple sites of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.5', 'L2795', 'Contusion of unspecified part of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('958.8', 'L2795', 'Other early complications of trauma');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.7', 'L2795', 'Injury, other and unspecified, knee, leg, ankle, and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.8', 'L2795', 'Injury, other and unspecified, other specified sites, including multiple');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.9', 'L2795', 'Injury, other and unspecified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.59', 'L2795', 'Mechanical complication due to other implant and internal device, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.77', 'L2795', 'Other complications due to internal joint prosthesis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.78', 'L2795', 'Other complications due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('998.12', 'L2795', 'Hematoma complicating a procedure');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('213.7', 'L1825', 'Benign neoplasm of long bones of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('214.1', 'L1825', 'Lipoma of other skin and subcutaneous tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('214.8', 'L1825', 'Lipoma of other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('215.3', 'L1825', 'Other benign neoplasm of connective and other soft tissue of lower limb, including hip');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('238.1', 'L1825', 'Neoplasm of uncertain behavior of connective and other soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('238.2', 'L1825', 'Neoplasm of uncertain behavior of skin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('274', 'L1825', 'Gouty arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('289.3', 'L1825', 'Lymphadenitis, unspecified, except mesenteric');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.9', 'L1825', 'Unspecified paralysis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('353', 'L1825', 'Brachial plexus lesions');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('213.7', 'L1825', 'Benign neoplasm of long bones of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('214.1', 'L1825', 'Lipoma of other skin and subcutaneous tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('214.8', 'L1825', 'Lipoma of other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('215.3', 'L1825', 'Other benign neoplasm of connective and other soft tissue of lower limb, including hip');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('238.1', 'L1825', 'Neoplasm of uncertain behavior of connective and other soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('238.2', 'L1825', 'Neoplasm of uncertain behavior of skin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('274', 'L1825', 'Gouty arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('289.3', 'L1825', 'Lymphadenitis, unspecified, except mesenteric');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.9', 'L1825', 'Unspecified paralysis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('353', 'L1825', 'Brachial plexus lesions');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.8', 'L1825', 'Unspecified mononeuritis of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('712.36', 'L1825', 'Chondrocalcinosis, cause unspecified, involving lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('713.8', 'L1825', 'Arthropathy associated with other conditions classifiable elsewhere');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L1825', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.3', 'L1825', 'Polyarticular juvenile rheumatoid arthritis, chronic or unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L1825', 'Generalized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.09', 'L1825', 'Generalized osteoarthrosis, involving multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.1', 'L1825', 'Primary localized osteoarthrosis, specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.11', 'L1825', 'Primary localized osteoarthrosis, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.16', 'L1825', 'Primary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.18', 'L1825', 'Primary localized osteoarthrosis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.36', 'L1825', 'Localized osteoarthrosis not specified whether primary or secondary, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.9', 'L1825', 'Osteoarthrosis, unspecified whether generalized or localized, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.96', 'L1825', 'Osteoarthrosis, unspecified whether generalized or localized, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.98', 'L1825', 'Osteoarthrosis, unspecified whether generalized or localized, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.1', 'L1825', 'Traumatic arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.16', 'L1825', 'Traumatic arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.18', 'L1825', 'Traumatic arthropathy, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.4', 'L1825', 'Transient arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.46', 'L1825', 'Transient arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.66', 'L1825', 'Unspecified monoarthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.9', 'L1825', 'Unspecified arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.96', 'L1825', 'Unspecified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.98', 'L1825', 'Unspecified arthropathy, other unspecified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717', 'L1825', 'Old bucket handle tear of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.3', 'L1825', 'Other and unspecified derangement of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.4', 'L1825', 'Unspecified derangement of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.41', 'L1825', 'Bucket handle tear of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.42', 'L1825', 'Derangement of anterior horn of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.43', 'L1825', 'Derangement of posterior horn of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.49', 'L1825', 'Other derangement of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.5', 'L1825', 'Derangement of meniscus, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.6', 'L1825', 'Loose body in knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.7', 'L1825', 'Chondromalacia of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.81', 'L1825', 'Old disruption of lateral collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.82', 'L1825', 'Old disruption of medial collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.83', 'L1825', 'Old disruption of anterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.84', 'L1825', 'Old disruption of posterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.89', 'L1825', 'Other internal derangement of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718', 'L1825', 'Articular cartilage disorder, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.09', 'L1825', 'Articular cartilage disorder, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.2', 'L1825', 'Pathological dislocation of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.26', 'L1825', 'Pathological dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.65', 'L1810', 'Prepatellar bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.69', 'L1810', 'Other enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.7', 'L1810', 'Unspecified enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.71', 'L1810', 'Achilles bursitis or tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.79', 'L1810', 'Other enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L1810', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.91', 'L1810', 'Exostosis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727', 'L1810', 'Unspecified synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L1810', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.41', 'L1810', 'Ganglion of joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.42', 'L1810', 'Ganglion of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.5', 'L1810', 'Unspecified rupture of synovium');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.51', 'L1810', 'Synovial cyst of popliteal space');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.6', 'L1810', 'Nontraumatic rupture of unspecified tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.66', 'L1810', 'Nontraumatic rupture of patellar tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.81', 'L1810', 'Contracture of tendon (sheath)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.83', 'L1810', 'Plica syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.89', 'L1810', 'Other disorders of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.12', 'L1810', 'Traumatic myositis ossificans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.89', 'L1810', 'Other disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.9', 'L1810', 'Unspecified disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.1', 'L1810', 'Unspecified myalgia and myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L1810', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.81', 'L1810', 'Swelling of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.89', 'L1810', 'Other musculoskeletal symptoms referable to limbs');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.9', 'L1810', 'Other and unspecified disorders of soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.4', 'L1810', 'Juvenile osteochondrosis of lower extremity, excluding foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.6', 'L1810', 'Other juvenile osteochondrosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.7', 'L1810', 'Osteochondritis dissecans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.9', 'L1810', 'Unspecified osteochondropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733', 'L1810', 'Unspecified osteoporosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.2', 'L1810', 'Unspecified cyst of bone (localized)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.4', 'L1810', 'Aseptic necrosis of bone, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.6', 'L1810', 'Tietze''s disease');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.82', 'L1810', 'Nonunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.9', 'L1810', 'Disorder of bone and cartilage, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.91', 'L1810', 'Arrest of bone development or growth');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.92', 'L1810', 'Chondromalacia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.99', 'L1810', 'Other disorders of bone and cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.41', 'L1810', 'Genu valgum (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('738.8', 'L1810', 'Acquired musculoskeletal deformity of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('739.6', 'L1810', 'Nonallopathic lesion of lower extremities, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('739.6', 'L1810', 'Nonallopathic lesion of lower extremities, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.6', 'L1810', 'Unspecified congenital anomaly of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.64', 'L1810', 'Congenital deformity of knee (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.67', 'L1810', 'Congenital anomalies of foot, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.69', 'L1810', 'Other congenital anomaly of lower limb, including pelvic girdle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.8', 'L1810', 'Other specified congenital anomalies of unspecified limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('780.79', 'L1810', 'Other malaise and fatigue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.06', 'L1810', 'Closed fracture of neck of radius');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.42', 'L1810', 'Other closed fractures of distal end of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821', 'L1810', 'Closed fracture of unspecified part of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.2', 'L1810', 'Closed fracture of unspecified part of lower end of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.21', 'L1810', 'Closed fracture of femoral condyle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('822', 'L1810', 'Closed fracture of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823', 'L1810', 'Closed fracture of upper end of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.01', 'L1810', 'Closed fracture of upper end of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.02', 'L1810', 'Closed fracture of upper end of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.11', 'L1810', 'Open fracture of upper end of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.12', 'L1810', 'Open fracture of upper end of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.2', 'L1810', 'Closed fracture of shaft of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.22', 'L1810', 'Closed fracture of shaft of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.8', 'L1810', 'Closed fracture of unspecified part of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.81', 'L1810', 'Closed fracture of unspecified part of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('829', 'L1810', 'Closed fracture of unspecified bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836', 'L1810', 'Tear of medial cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.5', 'L1810', 'Closed dislocation of knee, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.59', 'L1810', 'Other closed dislocation of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.9', 'L1810', 'Sprain and strain of unspecified site of knee and leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.8', 'L1810', 'Other specified sites of sprains and strains');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('904.8', 'L1810', 'Injury to unspecified blood vessel of lower extremity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('916', 'L1810', 'Hip, thigh, leg, and ankle, abrasion or friction burn, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('919', 'L1810', 'Abrasion or friction burn of other, multiple, and unspecified sites, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('919.8', 'L1810', 'Other and unspecified superficial injury of other, multiple, and unspecified sites, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.1', 'L1810', 'Contusion of lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.11', 'L1810', 'Contusion of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.4', 'L1810', 'Contusion of multiple sites of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.5', 'L1810', 'Contusion of unspecified part of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.8', 'L1810', 'Contusion of multiple sites, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.9', 'L1810', 'Contusion of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('928.11', 'L1810', 'Crushing injury of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('958.8', 'L1810', 'Other early complications of trauma');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.7', 'L1810', 'Injury, other and unspecified, knee, leg, ankle, and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.8', 'L1810', 'Injury, other and unspecified, other specified sites, including multiple');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.9', 'L1810', 'Injury, other and unspecified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.66', 'L1810', 'Infection and inflammatory reaction due to internal joint prosthesis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.67', 'L1810', 'Infection and inflammatory reaction due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.77', 'L1810', 'Other complications due to internal joint prosthesis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.78', 'L1810', 'Other complications due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.59', 'L1810', 'Other immobilization, pressure, and attention to wound');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.23', 'L1810', 'Fitting of orthotic device');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722', 'L0621', 'Cervical HNP');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('723.4', 'L0621', 'Cervical Radiculitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('721', 'L0621', 'Cervical spondylosis without myelopathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.79', 'L0621', 'Coccygodnia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('922.3', 'L0621', 'Contusion back');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.4', 'L0621', 'DDD cervical');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.52', 'L0621', 'DDD lumbar');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.51', 'L0621', 'DDD thoracolumbar');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.03', 'L0621', 'Disuse osteoporosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('805', 'L0621', 'Fx cervical vertebra');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('806', 'L0621', 'Fx cervical w/spinal cord injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('805.4', 'L0621', 'Fx lumbar, closed');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('805.2', 'L0621', 'Fx thoracic, closed');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('737.1', 'L0621', 'Kyphosis, acquired (postural)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.1', 'L0621', 'Lumbar HNP');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.2', 'L0621', 'Lumbar pain syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('721.3', 'L0621', 'Lumbar spondylosis w/o myelopathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.18', 'L0621', 'Osteoarthritis nonspecific site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732', 'L0621', 'Osteochondrosis, juvenile');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733', 'L0621', 'Osteoporosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.8', 'L0621', 'Post laminectomy syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('720.2', 'L0621', 'Sacroilitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.3', 'L0621', 'Sciatica');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.2', 'L0621', 'Scoliosis, congenital');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('737.3', 'L0621', 'Scoliosis, idiopathic');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.02', 'L0621', 'Spinal stenosis, lumbar');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('738.4', 'L0621', 'Spondylolisthesis, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.12', 'L0621', 'Spondylolisthesis, congenital');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.11', 'L0621', 'Spondylosis, lumbar, congenital');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847', 'L0621', 'Sprain/strain cervical');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847.2', 'L0621', 'Sprain/strain lumbar');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847.1', 'L0621', 'Sprain/strain thoracic');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722', 'L0626', 'Cervical HNP');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('723.4', 'L0626', 'Cervical Radiculitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('721', 'L0626', 'Cervical spondylosis without myelopathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.79', 'L0626', 'Coccygodnia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('922.3', 'L0626', 'Contusion back');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.4', 'L0626', 'DDD cervical');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.52', 'L0626', 'DDD lumbar');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.51', 'L0626', 'DDD thoracolumbar');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.03', 'L0626', 'Disuse osteoporosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('805', 'L0626', 'Fx cervical vertebra');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('806', 'L0626', 'Fx cervical w/spinal cord injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('805.4', 'L0626', 'Fx lumbar, closed');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('805.2', 'L0626', 'Fx thoracic, closed');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('737.1', 'L0626', 'Kyphosis, acquired (postural)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.1', 'L0626', 'Lumbar HNP');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.2', 'L0626', 'Lumbar pain syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('721.3', 'L0626', 'Lumbar spondylosis w/o myelopathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.18', 'L0626', 'Osteoarthritis nonspecific site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732', 'L0626', 'Osteochondrosis, juvenile');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733', 'L0626', 'Osteoporosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.8', 'L0626', 'Post laminectomy syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('720.2', 'L0626', 'Sacroilitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.3', 'L0626', 'Sciatica');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.2', 'L0626', 'Scoliosis, congenital');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('737.3', 'L0626', 'Scoliosis, idiopathic');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.02', 'L0626', 'Spinal stenosis, lumbar');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('738.4', 'L0626', 'Spondylolisthesis, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.12', 'L0626', 'Spondylolisthesis, congenital');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.11', 'L0626', 'Spondylosis, lumbar, congenital');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847', 'L0626', 'Sprain/strain cervical');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847.2', 'L0626', 'Sprain/strain lumbar');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847.1', 'L0626', 'Sprain/strain thoracic');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722', 'L0637', 'Cervical HNP');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('723.4', 'L0637', 'Cervical Radiculitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('721', 'L0637', 'Cervical spondylosis without myelopathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.79', 'L0637', 'Coccygodnia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('922.3', 'L0637', 'Contusion back');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.4', 'L0637', 'DDD cervical');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.52', 'L0637', 'DDD lumbar');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.51', 'L0637', 'DDD thoracolumbar');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.03', 'L0637', 'Disuse osteoporosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('805', 'L0637', 'Fx cervical vertebra');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('806', 'L0637', 'Fx cervical w/spinal cord injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('805.4', 'L0637', 'Fx lumbar, closed');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('805.2', 'L0637', 'Fx thoracic, closed');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('737.1', 'L0637', 'Kyphosis, acquired (postural)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.1', 'L0637', 'Lumbar HNP');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.2', 'L0637', 'Lumbar pain syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('721.3', 'L0637', 'Lumbar spondylosis w/o myelopathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.18', 'L0637', 'Osteoarthritis nonspecific site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732', 'L0637', 'Osteochondrosis, juvenile');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733', 'L0637', 'Osteoporosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.8', 'L0637', 'Post laminectomy syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('720.2', 'L0637', 'Sacroilitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.3', 'L0637', 'Sciatica');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.2', 'L0637', 'Scoliosis, congenital');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('737.3', 'L0637', 'Scoliosis, idiopathic');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.02', 'L0637', 'Spinal stenosis, lumbar');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('738.4', 'L0637', 'Spondylolisthesis, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.12', 'L0637', 'Spondylolisthesis, congenital');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.11', 'L0637', 'Spondylosis, lumbar, congenital');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847', 'L0637', 'Sprain/strain cervical');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847.2', 'L0637', 'Sprain/strain lumbar');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847.1', 'L0637', 'Sprain/strain thoracic');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722', 'L0627', 'Cervical HNP');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('723.4', 'L0627', 'Cervical Radiculitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('721', 'L0627', 'Cervical spondylosis without myelopathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.79', 'L0627', 'Coccygodnia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('922.3', 'L0627', 'Contusion back');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.4', 'L0627', 'DDD cervical');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.52', 'L0627', 'DDD lumbar');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.51', 'L0627', 'DDD thoracolumbar');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.03', 'L0627', 'Disuse osteoporosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('805', 'L0627', 'Fx cervical vertebra');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('806', 'L0627', 'Fx cervical w/spinal cord injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('805.4', 'L0627', 'Fx lumbar, closed');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('805.2', 'L0627', 'Fx thoracic, closed');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('737.1', 'L0627', 'Kyphosis, acquired (postural)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.1', 'L0627', 'Lumbar HNP');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.2', 'L0627', 'Lumbar pain syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('721.3', 'L0627', 'Lumbar spondylosis w/o myelopathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.18', 'L0627', 'Osteoarthritis nonspecific site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732', 'L0627', 'Osteochondrosis, juvenile');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733', 'L0627', 'Osteoporosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.8', 'L0627', 'Post laminectomy syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('720.2', 'L0627', 'Sacroilitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.3', 'L0627', 'Sciatica');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.2', 'L0627', 'Scoliosis, congenital');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('737.3', 'L0627', 'Scoliosis, idiopathic');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.02', 'L0627', 'Spinal stenosis, lumbar');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('738.4', 'L0627', 'Spondylolisthesis, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.12', 'L0627', 'Spondylolisthesis, congenital');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.11', 'L0627', 'Spondylosis, lumbar, congenital');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847', 'L0627', 'Sprain/strain cervical');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847.2', 'L0627', 'Sprain/strain lumbar');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847.1', 'L0627', 'Sprain/strain thoracic');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722', 'L0460', 'Cervical HNP');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('723.4', 'L0460', 'Cervical Radiculitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('721', 'L0460', 'Cervical spondylosis without myelopathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.79', 'L0460', 'Coccygodnia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('922.3', 'L0460', 'Contusion back');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.4', 'L0460', 'DDD cervical');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.52', 'L0460', 'DDD lumbar');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.51', 'L0460', 'DDD thoracolumbar');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.03', 'L0460', 'Disuse osteoporosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('805', 'L0460', 'Fx cervical vertebra');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('806', 'L0460', 'Fx cervical w/spinal cord injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('805.4', 'L0460', 'Fx lumbar, closed');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('805.2', 'L0460', 'Fx thoracic, closed');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('737.1', 'L0460', 'Kyphosis, acquired (postural)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.1', 'L0460', 'Lumbar HNP');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.2', 'L0460', 'Lumbar pain syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('721.3', 'L0460', 'Lumbar spondylosis w/o myelopathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.18', 'L0460', 'Osteoarthritis nonspecific site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732', 'L0460', 'Osteochondrosis, juvenile');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733', 'L0460', 'Osteoporosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.8', 'L0460', 'Post laminectomy syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('720.2', 'L0460', 'Sacroilitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.3', 'L0460', 'Sciatica');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.2', 'L0460', 'Scoliosis, congenital');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('737.3', 'L0460', 'Scoliosis, idiopathic');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.02', 'L0460', 'Spinal stenosis, lumbar');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('738.4', 'L0460', 'Spondylolisthesis, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.12', 'L0460', 'Spondylolisthesis, congenital');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.11', 'L0460', 'Spondylosis, lumbar, congenital');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847', 'L0460', 'Sprain/strain cervical');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847.2', 'L0460', 'Sprain/strain lumbar');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847.1', 'L0460', 'Sprain/strain thoracic');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('274', 'L4396', 'Gouty arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('335.2', 'L4396', 'Amyotrophic lateral sclerosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('337.9', 'L4396', 'Unspecified disorder of autonomic nervous system');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.5', 'L4396', 'Tarsal tunnel syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.6', 'L4396', 'Lesion of plantar nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.71', 'L4396', 'Causalgia of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.79', 'L4396', 'Other mononeuritis of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.8', 'L4396', 'Unspecified mononeuritis of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.9', 'L4396', 'Mononeuritis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('356.9', 'L4396', 'Unspecified hereditary and idiopathic peripheral neuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('459.81', 'L4396', 'Unspecified venous (peripheral) insufficiency');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('473.9', 'L4396', 'Unspecified sinusitis (chronic)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('682.6', 'L4396', 'Cellulitis and abscess of leg, except foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('682.7', 'L4396', 'Cellulitis and abscess of foot, except toes');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('686.9', 'L4396', 'Unspecified local infection of skin and subcutaneous tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707', 'L4396', 'Decubitus ulcer');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707.12', 'L4396', 'Ulcer of calf');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707.13', 'L4396', 'Ulcer of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707.14', 'L4396', 'Ulcer of heel and midfoot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707.15', 'L4396', 'Ulcer of other part of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('713.5', 'L4396', 'Arthropathy associated with neurological disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L4396', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.9', 'L4396', 'Unspecified inflammatory polyarthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L4396', 'Generalized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.16', 'L4396', 'Primary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.17', 'L4396', 'Primary localized osteoarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.18', 'L4396', 'Primary localized osteoarthrosis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.2', 'L4396', 'Secondary localized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.26', 'L4396', 'Secondary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.27', 'L4396', 'Secondary localized osteoarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.28', 'L4396', 'Secondary localized osteoarthrosis, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.3', 'L4396', 'Localized osteoarthrosis not specified whether primary or secondary, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.36', 'L4396', 'Localized osteoarthrosis not specified whether primary or secondary, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.37', 'L4396', 'Localized osteoarthrosis not specified whether primary or secondary, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.38', 'L4396', 'Localized osteoarthrosis not specified whether primary or secondary, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.8', 'L4396', 'Osteoarthrosis involving more than one site, but not specified as generalized, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.89', 'L4396', 'Osteoarthrosis involving multiple sites, but not specified as generalized');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.9', 'L4396', 'Osteoarthrosis, unspecified whether generalized or localized, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.96', 'L4396', 'Osteoarthrosis, unspecified whether generalized or localized, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.97', 'L4396', 'Osteoarthrosis, unspecified whether generalized or localized, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.98', 'L4396', 'Osteoarthrosis, unspecified whether generalized or localized, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.1', 'L4396', 'Traumatic arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.17', 'L4396', 'Traumatic arthropathy, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.9', 'L4396', 'Unspecified arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.96', 'L4396', 'Unspecified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.97', 'L4396', 'Unspecified arthropathy, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718', 'L4396', 'Articular cartilage disorder, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.17', 'L4396', 'Loose body in ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.37', 'L4396', 'Recurrent dislocation of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.57', 'L4396', 'Ankylosis of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.8', 'L4396', 'Other joint derangement, not elsewhere classified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.86', 'L4396', 'Other joint derangement, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.87', 'L4396', 'Other joint derangement, not elsewhere classified, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.89', 'L4396', 'Other joint derangement, not elsewhere classified, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.9', 'L4396', 'Unspecified derangement, joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.97', 'L4396', 'Unspecified ankle and foot joint derangement');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719', 'L4396', 'Effusion of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.06', 'L4396', 'Effusion of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.07', 'L4396', 'Effusion of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.08', 'L4396', 'Effusion of joint, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.09', 'L4396', 'Effusion of joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.1', 'L4396', 'Hemarthrosis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.16', 'L4396', 'Hemarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.17', 'L4396', 'Hemarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.18', 'L4396', 'Hemarthrosis, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.19', 'L4396', 'Hemarthrosis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.26', 'L4396', 'Villonodular synovitis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.27', 'L4396', 'Villonodular synovitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.28', 'L4396', 'Villonodular synovitis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.29', 'L4396', 'Villonodular synovitis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.3', 'L4396', 'Palindromic rheumatism, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.36', 'L4396', 'Palindromic rheumatism, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.37', 'L4396', 'Palindromic rheumatism, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.38', 'L4396', 'Palindromic rheumatism, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.39', 'L4396', 'Palindromic rheumatism, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.4', 'L4396', 'Pain in joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.46', 'L4396', 'Pain in joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.47', 'L4396', 'Pain in joint, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.48', 'L4396', 'Pain in joint, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.49', 'L4396', 'Pain in joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.66', 'L4396', 'Other symptoms referable to lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.67', 'L4396', 'Other symptoms referable to ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.68', 'L4396', 'Other symptoms referable to joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.69', 'L4396', 'Other symptoms referable to joints of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.8', 'L4396', 'Other specified disorders of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.86', 'L4396', 'Other specified disorders of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.87', 'L4396', 'Other specified disorders of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.88', 'L4396', 'Other specified disorders of joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.89', 'L4396', 'Other specified disorders of joints of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.9', 'L4396', 'Unspecified disorder of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.96', 'L4396', 'Unspecified disorder of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.97', 'L4396', 'Unspecified disorder of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.98', 'L4396', 'Unspecified joint disorder of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.99', 'L4396', 'Unspecified joint disorder of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.7', 'L4396', 'Unspecified enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.71', 'L4396', 'Achilles bursitis or tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.72', 'L4396', 'Tibialis tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.73', 'L4396', 'Calcaneal spur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.79', 'L4396', 'Other enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L4396', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727', 'L4396', 'Unspecified synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.06', 'L4396', 'Tenosynovitis of foot and ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L4396', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.1', 'L4396', 'Bunion');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.2', 'L4396', 'Specific bursitides often of occupational origin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.3', 'L4396', 'Other bursitis disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.4', 'L4396', 'Unspecified synovial cyst');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.41', 'L4396', 'Ganglion of joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.42', 'L4396', 'Ganglion of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.43', 'L4396', 'Unspecified ganglion');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.49', 'L4396', 'Other ganglion and cyst of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.5', 'L4396', 'Unspecified rupture of synovium');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.59', 'L4396', 'Other rupture of synovium');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.6', 'L4396', 'Nontraumatic rupture of unspecified tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.67', 'L4396', 'Nontraumatic rupture of Achilles tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.68', 'L4396', 'Nontraumatic rupture of other tendons of foot and ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.69', 'L4396', 'Nontraumatic rupture of other tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.81', 'L4396', 'Contracture of tendon (sheath)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.82', 'L4396', 'Calcium deposits in tendon and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.83', 'L4396', 'Plica syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.89', 'L4396', 'Other disorders of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.9', 'L4396', 'Unspecified disorder of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.2', 'L4396', 'Muscular wasting and disuse atrophy, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.4', 'L4396', 'Laxity of ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.6', 'L4396', 'Contracture of palmar fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.71', 'L4396', 'Plantar fascial fibromatosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.85', 'L4396', 'Spasm of muscle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.89', 'L4396', 'Other disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.9', 'L4396', 'Unspecified disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729', 'L4396', 'Rheumatism, unspecified and fibrositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.1', 'L4396', 'Unspecified myalgia and myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.2', 'L4396', 'Unspecified neuralgia, neuritis, and radiculitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L4396', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.6', 'L4396', 'Residual foreign body in soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.81', 'L4396', 'Swelling of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.9', 'L4396', 'Other and unspecified disorders of soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730', 'L4396', 'Acute osteomyelitis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.07', 'L4396', 'Acute osteomyelitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.27', 'L4396', 'Unspecified osteomyelitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.37', 'L4396', 'Periostitis, without mention of osteomyelitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.4', 'L4396', 'Juvenile osteochondrosis of lower extremity, excluding foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.5', 'L4396', 'Juvenile osteochondrosis of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.7', 'L4396', 'Osteochondritis dissecans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.9', 'L4396', 'Unspecified osteochondropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.16', 'L4396', 'Pathologic fracture of tibia and fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.19', 'L4396', 'Pathologic fracture of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.2', 'L4396', 'Unspecified cyst of bone (localized)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.44', 'L4396', 'Aseptic necrosis of talus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.82', 'L4396', 'Nonunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.9', 'L4396', 'Disorder of bone and cartilage, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.91', 'L4396', 'Arrest of bone development or growth');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.99', 'L4396', 'Other disorders of bone and cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('735', 'L4396', 'Hallux valgus (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('735.2', 'L4396', 'Hallux rigidus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('735.4', 'L4396', 'Other hammer toe (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736', 'L4396', 'Unspecified deformity of forearm, excluding fingers');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.42', 'L4396', 'Genu varum (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.7', 'L4396', 'Unspecified deformity of ankle and foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.72', 'L4396', 'Equinus deformity of foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.73', 'L4396', 'Cavus deformity of foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.79', 'L4396', 'Other acquired deformity of ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.51', 'L4396', 'Congenital talipes equinovarus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.52', 'L4396', 'Congenital metatarsus primus varus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.53', 'L4396', 'Congenital metatarsus varus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.6', 'L4396', 'Congenital talipes valgus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.61', 'L4396', 'Congenital pes planus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.7', 'L4396', 'Unspecified talipes');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.1', 'L4396', 'Syndactyly of multiple and unspecified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.6', 'L4396', 'Unspecified congenital anomaly of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.67', 'L4396', 'Congenital anomalies of foot, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.4', 'L4396', 'Chondrodystrophy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.51', 'L4396', 'Osteogenesis imperfecta');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.52', 'L4396', 'Osteopetrosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.2', 'L4396', 'Closed fracture of shaft of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.21', 'L4396', 'Closed fracture of shaft of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.22', 'L4396', 'Closed fracture of shaft of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.3', 'L4396', 'Open fracture of shaft of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.32', 'L4396', 'Open fracture of shaft of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.8', 'L4396', 'Closed fracture of unspecified part of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.81', 'L4396', 'Closed fracture of unspecified part of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.82', 'L4396', 'Closed fracture of unspecified part of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.9', 'L4396', 'Open fracture of unspecified part of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.91', 'L4396', 'Open fracture of unspecified part of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.92', 'L4396', 'Open fracture of unspecified part of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824', 'L4396', 'Closed fracture of medial malleolus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.1', 'L4396', 'Open fracture of medial malleolus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.2', 'L4396', 'Closed fracture of lateral malleolus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.3', 'L4396', 'Open fracture of lateral malleolus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.4', 'L4396', 'Closed bimalleolar fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.5', 'L4396', 'Open bimalleolar fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.6', 'L4396', 'Closed trimalleolar fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.7', 'L4396', 'Open trimalleolar fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.8', 'L4396', 'Unspecified closed fracture of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.9', 'L4396', 'Unspecified open fracture of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825', 'L4396', 'Closed fracture of calcaneus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.1', 'L4396', 'Open fracture of calcaneus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.2', 'L4396', 'Closed fracture of unspecified bone(s) of foot (except toes)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.21', 'L4396', 'Closed fracture of astragalus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.22', 'L4396', 'Closed fracture of navicular (scaphoid) bone of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.23', 'L4396', 'Closed fracture of cuboid bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.24', 'L4396', 'Closed fracture of cuneiform bone of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.25', 'L4396', 'Closed fracture of metatarsal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.29', 'L4396', 'Other closed fracture of tarsal and metatarsal bones');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.3', 'L4396', 'Open fracture of unspecified bone(s) of foot (except toes)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.31', 'L4396', 'Open fracture of astragalus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.32', 'L4396', 'Open fracture of navicular (scaphoid) bone of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.33', 'L4396', 'Open fracture of cuboid bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.34', 'L4396', 'Open fracture of cuneiform bone of foot,');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.35', 'L4396', 'Open fracture of metatarsal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.39', 'L4396', 'Other open fractures of tarsal and metatarsal bones');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('826', 'L4396', 'Closed fracture of one or more phalanges of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('826.1', 'L4396', 'Open fracture of one or more phalanges of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('827', 'L4396', 'Other, multiple and ill-defined closed fractures of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('827.1', 'L4396', 'Other, multiple and ill-defined open fractures of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('828', 'L4396', 'Multiple closed fractures involving both lower limbs, lower with upper limb, and lower limb(s) with');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('837', 'L4396', 'Closed dislocation of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838', 'L4396', 'Closed dislocation of foot, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838.01', 'L4396', 'Closed dislocation of tarsal (bone), joint unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838.03', 'L4396', 'Closed dislocation of tarsometatarsal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838.06', 'L4396', 'Closed dislocation of interphalangeal (joint), foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845', 'L4396', 'Unspecified site of ankle sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.01', 'L4396', 'Sprain and strain of deltoid (ligament) of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.02', 'L4396', 'Sprain and strain of calcaneofibular (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.03', 'L4396', 'Sprain and strain of tibiofibular (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.09', 'L4396', 'Other ankle sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.1', 'L4396', 'Sprain and strain of unspecified site of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.11', 'L4396', 'Sprain and strain of tarsometatarsal (joint) (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.12', 'L4396', 'Sprain and strain of metatarsaophalangeal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.13', 'L4396', 'Sprain and strain of interphalangeal (joint), of toe');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.19', 'L4396', 'Other foot sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('892', 'L4396', 'Open wound of foot except toe(s) alone, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('893', 'L4396', 'Open wound of toe(s), without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('894', 'L4396', 'Multiple and unspecified open wound of lower limb, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('904.8', 'L4396', 'Injury to unspecified blood vessel of lower extremity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('905.7', 'L4396', 'Late effect of sprain and strain without mention of tendon injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('905.8', 'L4396', 'Late effect of tendon injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('917.8', 'L4396', 'Other and unspecified superficial injury of foot and toes, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('917.9', 'L4396', 'Other and unspecified superficial injury of foot and toes, infected');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('919', 'L4396', 'Abrasion or friction burn of other, multiple, and unspecified sites, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('919.8', 'L4396', 'Other and unspecified superficial injury of other, multiple, and unspecified sites, without mention');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.1', 'L4396', 'Contusion of lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.11', 'L4396', 'Contusion of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.2', 'L4396', 'Contusion of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.21', 'L4396', 'Contusion of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.3', 'L4396', 'Contusion of toe');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.4', 'L4396', 'Contusion of multiple sites of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.5', 'L4396', 'Contusion of unspecified part of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.8', 'L4396', 'Contusion of multiple sites, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.9', 'L4396', 'Contusion of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('928.2', 'L4396', 'Crushing injury of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('928.21', 'L4396', 'Crushing injury of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('929.9', 'L4396', 'Crushing injury of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('945', 'L4396', 'Burn of unspecified degree of unspecified site of lower limb (leg)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('945.02', 'L4396', 'Burn of unspecified degree of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('945.03', 'L4396', 'Burn of unspecified degree of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('945.2', 'L4396', 'Blisters with epidermal loss due to burn (second degree) of unspecified site of lower limb (leg)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.78', 'L4396', 'Other complications due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.79', 'L4396', 'Other complications due to other internal prosthetic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('214.9', 'L3730', 'Lipoma of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('238.8', 'L3730', 'Neoplasm of uncertain behavior of other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('342.1', 'L3730', 'Spastic hemiplegia affecting unspecified side');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('353', 'L3730', 'Brachial plexus lesions');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354', 'L3730', 'Carpal tunnel syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.8', 'L3730', 'Other mononeuritis of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.9', 'L3730', 'Unspecified mononeuritis of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('359', 'L3730', 'Congenital hereditary muscular dystrophy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L3730', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L3730', 'Generalized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.14', 'L3730', 'Primary localized osteoarthrosis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.93', 'L3730', 'Osteoarthrosis, unspecified whether generalized or localized, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.32', 'L3730', 'Climacteric arthritis, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.9', 'L3730', 'Unspecified arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.93', 'L3730', 'Unspecified arthropathy, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.12', 'L3730', 'Loose body in upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.4', 'L3730', 'Contracture of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.42', 'L3730', 'Contracture of upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.43', 'L3730', 'Contracture of forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.8', 'L3730', 'Other joint derangement, not elsewhere classified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.82', 'L3730', 'Other joint derangement, not elsewhere classified, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.92', 'L3730', 'Unspecified derangement, upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.02', 'L3730', 'Effusion of upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.22', 'L3730', 'Villonodular synovitis, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.42', 'L3730', 'Pain in joint, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.43', 'L3730', 'Pain in joint, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.48', 'L3730', 'Pain in joint, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.88', 'L3730', 'Other specified disorders of joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.93', 'L3730', 'Unspecified disorder of forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.12', 'L3730', 'Bicipital tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.2', 'L3730', 'Other affections of shoulder region, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.3', 'L3730', 'Unspecified enthesopathy of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.31', 'L3730', 'Medial epicondylitis of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.32', 'L3730', 'Lateral epicondylitis of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.33', 'L3730', 'Olecranon bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.39', 'L3730', 'Other enthesopathy of elbow region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.4', 'L3730', 'Enthesopathy of wrist and carpus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L3730', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.2', 'L3730', 'Specific bursitides often of occupational origin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.3', 'L3730', 'Other bursitis disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.43', 'L3730', 'Unspecified ganglion');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.62', 'L3730', 'Nontraumatic rupture of tendons of biceps (long head)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.81', 'L3730', 'Contracture of tendon (sheath)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.82', 'L3730', 'Calcium deposits in tendon and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.71', 'L3730', 'Plantar fascial fibromatosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.85', 'L3730', 'Spasm of muscle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729', 'L3730', 'Rheumatism, unspecified and fibrositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.1', 'L3730', 'Unspecified myalgia and myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.2', 'L3730', 'Unspecified neuralgia, neuritis, and radiculitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.4', 'L3730', 'Unspecified fasciitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L3730', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.81', 'L3730', 'Malunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.82', 'L3730', 'Nonunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.99', 'L3730', 'Other disorders of bone and cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812', 'L3730', 'Closed fracture of unspecified part of upper end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.4', 'L3730', 'Closed fracture of unspecified part of lower end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.41', 'L3730', 'Closed fracture of supracondylar humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.42', 'L3730', 'Closed fracture of lateral condyle of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.43', 'L3730', 'Closed fracture of medial condyle of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.44', 'L3730', 'Closed fracture of unspecified condyle(s) of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.49', 'L3730', 'Other closed fracture of lower end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813', 'L3730', 'Unspecified fracture of radius and ulna, upper end of forearm, closed');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.01', 'L3730', 'Closed fracture of olecranon process of ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.02', 'L3730', 'Closed fracture of coronoid process of ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.03', 'L3730', 'Closed Monteggia''s fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.04', 'L3730', 'Other and unspecified closed fractures of proximal end of ulna (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.05', 'L3730', 'Closed fracture of head of radius');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.06', 'L3730', 'Closed fracture of neck of radius');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.11', 'L3730', 'Open fracture of olecranon process of ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.12', 'L3730', 'Open fracture of coronoid process of ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.17', 'L3730', 'Other and unspecified open fractures of proximal end of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832', 'L3730', 'Closed unspecified dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832.02', 'L3730', 'Closed posterior dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.8', 'L3730', 'Other specified sites of sprains and strains');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.9', 'L3730', 'Unspecified site of sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('880.03', 'L3730', 'Open wound of upper arm, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('881.01', 'L3730', 'Open wound of elbow, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('881.11', 'L3730', 'Open wound of elbow, complicated');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('905.7', 'L3730', 'Late effect of sprain and strain without mention of tendon injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.1', 'L3730', 'Contusion of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.11', 'L3730', 'Contusion of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('927.8', 'L3730', 'Crushing injury of multiple sites of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('953', 'L3730', 'Injury to cervical nerve root');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('955.1', 'L3730', 'Injury to median nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('955.2', 'L3730', 'Injury to ulnar nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('955.3', 'L3730', 'Injury to radial nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.2', 'L3730', 'Injury, other and unspecified, shoulder and upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.3', 'L3730', 'Injury, other and unspecified, elbow, forearm, and wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.9', 'L3730', 'Injury, other and unspecified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.23', 'L3730', 'Fitting of orthotic device');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250', 'L3923', 'Diabetes mellitus without mention of complication, type II or unspecified type, not stated as   uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.01', 'L3923', 'Diabetes mellitus without mention of complication, type I [juvenile type], not stated as  uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.02', 'L3923', 'Diabetes mellitus without mention of complication, type II or unspecified type, uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.03', 'L3923', 'Diabetes mellitus without mention of complication, type I [juvenile type], uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.4', 'L3923', 'Diabetes with renal manifestations, type II or unspecified type, not stated as uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.5', 'L3923', 'Diabetes with ophthalmic manifestations, type II or unspecified type, not stated as uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.62', 'L3923', 'Diabetes with neurological manifestations, type II or unspecified type, uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('274', 'L3923', 'Gouty arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('337', 'L3923', 'Idiopathic peripheral autonomic neuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('337.21', 'L3923', 'Reflex sympathetic dystrophy of the upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('337.9', 'L3923', 'Unspecified disorder of autonomic nervous system');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('341.9', 'L3923', 'Unspecified demyelinating disease of central nervous system');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354', 'L3923', 'Carpal tunnel syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.1', 'L3923', 'Other lesion of median nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.2', 'L3923', 'Lesion of ulnar nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.3', 'L3923', 'Lesion of radial nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.4', 'L3923', 'Causalgia of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.5', 'L3923', 'Mononeuritis multiplex');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.8', 'L3923', 'Other mononeuritis of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.9', 'L3923', 'Unspecified mononeuritis of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.9', 'L3923', 'Mononeuritis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('356.4', 'L3923', 'Idiopathic progressive polyneuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('356.8', 'L3923', 'Other specified idiopathic peripheral neuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('356.9', 'L3923', 'Unspecified hereditary and idiopathic peripheral neuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('357.4', 'L3923', 'Polyneuropathy in other diseases classified elsewhere');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('357.9', 'L3923', 'Unspecified inflammatory and toxic neuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('453.8', 'L3923', 'Embolism and thrombosis of other specified veins');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('453.9', 'L3923', 'Embolism and thrombosis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('459.81', 'L3923', 'Unspecified venous (peripheral) insufficiency');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('459.89', 'L3923', 'Other specified circulatory system disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('711.03', 'L3923', 'Pyogenic arthritis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('711.04', 'L3923', 'Pyogenic arthritis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('711.94', 'L3923', 'Unspecified infective arthritis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('712.93', 'L3923', 'Unspecified crystal arthropathy, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('713.2', 'L3923', 'Arthropathy associated with hematological disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L3923', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.3', 'L3923', 'Polyarticular juvenile rheumatoid arthritis, chronic or unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.32', 'L3923', 'Pauciarticular juvenile rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.89', 'L3923', 'Other specified inflammatory polyarthropathies');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.9', 'L3923', 'Unspecified inflammatory polyarthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L3923', 'Generalized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.04', 'L3923', 'Generalized osteoarthrosis, involving hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.09', 'L3923', 'Generalized osteoarthrosis, involving multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.1', 'L3923', 'Primary localized osteoarthrosis, specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.13', 'L3923', 'Primary localized osteoarthrosis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.14', 'L3923', 'Primary localized osteoarthrosis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.18', 'L3923', 'Primary localized osteoarthrosis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.2', 'L3923', 'Secondary localized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.23', 'L3923', 'Secondary localized osteoarthrosis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.24', 'L3923', 'Secondary localized osteoarthrosis, involving hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.33', 'L3923', 'Localized osteoarthrosis not specified whether primary or secondary, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.34', 'L3923', 'Localized osteoarthrosis not specified whether primary or secondary, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.89', 'L3923', 'Osteoarthrosis involving multiple sites, but not specified as generalized');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.9', 'L3923', 'Osteoarthrosis, unspecified whether generalized or localized, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.93', 'L3923', 'Osteoarthrosis, unspecified whether generalized or localized, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.94', 'L3923', 'Osteoarthrosis, unspecified whether generalized or localized, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.03', 'L3923', 'Kaschin-Beck disease, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.04', 'L3923', 'Kaschin-Beck disease, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.12', 'L3923', 'Traumatic arthropathy, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.13', 'L3923', 'Traumatic arthropathy, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.14', 'L3923', 'Traumatic arthropathy, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.23', 'L3923', 'Allergic arthritis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.24', 'L3923', 'Allergic arthritis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.29', 'L3923', 'Allergic arthritis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.3', 'L3923', 'Climacteric arthritis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.33', 'L3923', 'Climacteric arthritis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.34', 'L3923', 'Climacteric arthritis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.39', 'L3923', 'Climacteric arthritis involving multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.4', 'L3923', 'Transient arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.43', 'L3923', 'Transient arthropathy, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.44', 'L3923', 'Transient arthropathy, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.48', 'L3923', 'Transient arthropathy, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.49', 'L3923', 'Transient arthropathy, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.5', 'L3923', 'Unspecified polyarthropathy or polyarthritis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.53', 'L3923', 'Unspecified polyarthropathy or polyarthritis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.54', 'L3923', 'Unspecified polyarthropathy or polyarthritis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.58', 'L3923', 'Unspecified polyarthropathy or polyarthritis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.59', 'L3923', 'Unspecified polyarthropathy or polyarthritis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.63', 'L3923', 'Unspecified monoarthritis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.64', 'L3923', 'Unspecified monoarthritis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.68', 'L3923', 'Unspecified monoarthritis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.8', 'L3923', 'Other specified arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.83', 'L3923', 'Other specified arthropathy, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.84', 'L3923', 'Other specified arthropathy, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.88', 'L3923', 'Other specified arthropathy, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.89', 'L3923', 'Other specified arthropathy, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.92', 'L3923', 'Unspecified arthropathy, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.93', 'L3923', 'Unspecified arthropathy, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.94', 'L3923', 'Unspecified arthopathy, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.99', 'L3923', 'Unspecified arthropathy, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.03', 'L3923', 'Articular cartilage disorder, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.13', 'L3923', 'Loose body in forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.24', 'L3923', 'Pathological dislocation of hand joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.33', 'L3923', 'Recurrent dislocation of forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.42', 'L3923', 'Contracture of upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.43', 'L3923', 'Contracture of forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.44', 'L3923', 'Contracture of hand joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.48', 'L3923', 'Contracture of joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.83', 'L3923', 'Other joint derangement, not elsewhere classified, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.84', 'L3923', 'Other joint derangement, not elsewhere classified, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.94', 'L3923', 'Unspecified derangement of hand joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.99', 'L3923', 'Unspecified derangement of joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.02', 'L3923', 'Effusion of upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.03', 'L3923', 'Effusion of forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.04', 'L3923', 'Effusion of hand joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.13', 'L3923', 'Hemarthrosis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.14', 'L3923', 'Hemarthrosis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.19', 'L3923', 'Hemarthrosis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.2', 'L3923', 'Villonodular synovitis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.23', 'L3923', 'Villonodular synovitis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.24', 'L3923', 'Villonodular synovitis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.28', 'L3923', 'Villonodular synovitis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.29', 'L3923', 'Villonodular synovitis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.33', 'L3923', 'Palindromic rheumatism, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.34', 'L3923', 'Palindromic rheumatism, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.39', 'L3923', 'Palindromic rheumatism, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.43', 'L3923', 'Pain in joint, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.44', 'L3923', 'Pain in joint, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.48', 'L3923', 'Pain in joint, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.49', 'L3923', 'Pain in joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.62', 'L3923', 'Other symptoms referable to upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.63', 'L3923', 'Other symptoms referable to forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.64', 'L3923', 'Other symptoms referable to hand joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.68', 'L3923', 'Other symptoms referable to joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.69', 'L3923', 'Other symptoms referable to joints of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.83', 'L3923', 'Other specified disorders of forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.84', 'L3923', 'Other specified disorders of hand joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.88', 'L3923', 'Other specified disorders of joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.89', 'L3923', 'Other specified disorders of joints of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.93', 'L3923', 'Unspecified disorder of forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.94', 'L3923', 'Unspecified disorder of hand joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.98', 'L3923', 'Unspecified joint disorder of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.99', 'L3923', 'Unspecified joint disorder of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('723.4', 'L3923', 'Brachial neuritis or radiculitis nos');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.4', 'L3923', 'Enthesopathy of wrist and carpus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.5', 'L3923', 'Enthesopathy of hip region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L3923', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.91', 'L3923', 'Exostosis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727', 'L3923', 'Unspecified synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.3', 'L1825', 'Recurrent dislocation of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.36', 'L1825', 'Recurrent dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.38', 'L1825', 'Recurrent dislocation of joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.39', 'L1825', 'Recurrent dislocation of joint of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.46', 'L1825', 'Contracture of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.5', 'L1825', 'Ankylosis of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.56', 'L1825', 'Ankylosis of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.8', 'L1825', 'Other joint derangement, not elsewhere classified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.86', 'L1825', 'Other joint derangement, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.88', 'L1825', 'Other joint derangement, not elsewhere classified, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.89', 'L1825', 'Other joint derangement, not elsewhere classified, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719', 'L1825', 'Effusion of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.06', 'L1825', 'Effusion of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.08', 'L1825', 'Effusion of joint, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.1', 'L1825', 'Hemarthrosis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.16', 'L1825', 'Hemarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.19', 'L1825', 'Hemarthrosis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.21', 'L1825', 'Villonodular synovitis, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.26', 'L1825', 'Villonodular synovitis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.4', 'L1825', 'Pain in joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.46', 'L1825', 'Pain in joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.48', 'L1825', 'Pain in joint, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.49', 'L1825', 'Pain in joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.5', 'L1825', 'Stiffness of joint, not elsewhere classified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.56', 'L1825', 'Stiffness of joint, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.58', 'L1825', 'Stiffness of joint, not elsewhere classified, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.59', 'L1825', 'Stiffness of joints, not elsewhere classified, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.64', 'L1825', 'Other symptoms referable to hand joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.66', 'L1825', 'Other symptoms referable to lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.86', 'L1825', 'Other specified disorders of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.9', 'L1825', 'Unspecified disorder of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.96', 'L1825', 'Unspecified disorder of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.98', 'L1825', 'Unspecified joint disorder of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.61', 'L1825', 'Pes anserinus tendinitis or bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.62', 'L1825', 'Tibial collateral ligament bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.64', 'L1825', 'Patellar tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.65', 'L1825', 'Prepatellar bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.69', 'L1825', 'Other enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.7', 'L1825', 'Unspecified enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.71', 'L1825', 'Achilles bursitis or tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.79', 'L1825', 'Other enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L1825', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.91', 'L1825', 'Exostosis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727', 'L1825', 'Unspecified synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L1825', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.41', 'L1825', 'Ganglion of joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.42', 'L1825', 'Ganglion of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.5', 'L1825', 'Unspecified rupture of synovium');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.51', 'L1825', 'Synovial cyst of popliteal space');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.6', 'L1825', 'Nontraumatic rupture of unspecified tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.66', 'L1825', 'Nontraumatic rupture of patellar tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.81', 'L1825', 'Contracture of tendon (sheath)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.83', 'L1825', 'Plica syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.89', 'L1825', 'Other disorders of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.12', 'L1825', 'Traumatic myositis ossificans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.89', 'L1825', 'Other disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.9', 'L1825', 'Unspecified disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.1', 'L1825', 'Unspecified myalgia and myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L1825', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.81', 'L1825', 'Swelling of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.89', 'L1825', 'Other musculoskeletal symptoms referable to limbs');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.9', 'L1825', 'Other and unspecified disorders of soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.4', 'L1825', 'Juvenile osteochondrosis of lower extremity, excluding foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.6', 'L1825', 'Other juvenile osteochondrosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.7', 'L1825', 'Osteochondritis dissecans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.9', 'L1825', 'Unspecified osteochondropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733', 'L1825', 'Unspecified osteoporosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.2', 'L1825', 'Unspecified cyst of bone (localized)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.4', 'L1825', 'Aseptic necrosis of bone, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.6', 'L1825', 'Tietze''s disease');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.82', 'L1825', 'Nonunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.9', 'L1825', 'Disorder of bone and cartilage, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.91', 'L1825', 'Arrest of bone development or growth');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.92', 'L1825', 'Chondromalacia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.99', 'L1825', 'Other disorders of bone and cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.41', 'L1825', 'Genu valgum (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('738.8', 'L1825', 'Acquired musculoskeletal deformity of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('739.6', 'L1825', 'Nonallopathic lesion of lower extremities, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.6', 'L1825', 'Unspecified congenital anomaly of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.64', 'L1825', 'Congenital deformity of knee joint')
;
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.67', 'L1825', 'Congenital anomalies of foot, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.69', 'L1825', 'Other congenital anomaly of lower limb, including pelvic girdle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.8', 'L1825', 'Other specified congenital anomalies of unspecified limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('780.79', 'L1825', 'Other malaise and fatigue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.06', 'L1825', 'Closed fracture of neck of radius');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.42', 'L1825', 'Other closed fractures of distal end of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821', 'L1825', 'Closed fracture of unspecified part of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.2', 'L1825', 'Closed fracture of unspecified part of lower end of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.21', 'L1825', 'Closed fracture of femoral condyle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('822', 'L1825', 'Closed fracture of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823', 'L1825', 'Closed fracture of upper end of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.01', 'L1825', 'Closed fracture of upper end of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.02', 'L1825', 'Closed fracture of upper end of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.11', 'L1825', 'Open fracture of upper end of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.12', 'L1825', 'Open fracture of upper end of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.2', 'L1825', 'Closed fracture of shaft of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.22', 'L1825', 'Closed fracture of shaft of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.8', 'L1825', 'Closed fracture of unspecified part of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.81', 'L1825', 'Closed fracture of unspecified part of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('829', 'L1825', 'Closed fracture of unspecified bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836', 'L1825', 'Tear of medial cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.5', 'L1825', 'Closed dislocation of knee, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.59', 'L1825', 'Other closed dislocation of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.9', 'L1825', 'Sprain and strain of unspecified site of knee and leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.8', 'L1825', 'Other specified sites of sprains and strains');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('904.8', 'L1825', 'Injury to unspecified blood vessel of lower extremity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('916', 'L1825', 'Hip, thigh, leg, and ankle, abrasion or friction burn, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('919', 'L1825', 'Abrasion or friction burn of other, multiple, and unspecified sites, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('919.8', 'L1825', 'Other and unspecified superficial injury of other, multiple, and unspecified sites, without mention infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.1', 'L1825', 'Contusion of lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.11', 'L1825', 'Contusion of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.4', 'L1825', 'Contusion of multiple sites of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.5', 'L1825', 'Contusion of unspecified part of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.8', 'L1825', 'Contusion of multiple sites, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.9', 'L1825', 'Contusion of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('928.11', 'L1825', 'Crushing injury of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('958.8', 'L1825', 'Other early complications of trauma');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.7', 'L1825', 'Injury, other and unspecified, knee, leg, ankle, and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.8', 'L1825', 'Injury, other and unspecified, other specified sites, including multiple');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.9', 'L1825', 'Injury, other and unspecified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.66', 'L1825', 'Infection and inflammatory reaction due to internal joint prosthesis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.67', 'L1825', 'Infection and inflammatory reaction due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.77', 'L1825', 'Other complications due to internal joint prosthesis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.78', 'L1825', 'Other complications due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.59', 'L1825', 'Other immobilization, pressure, and attention to wound');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('170.7', 'L2999', 'Malignant neoplasm of long bones of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('171.7', 'L2999', 'Malignant neoplasm of connective and other soft tissue of trunk, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('174.8', 'L2999', 'Malignant neoplasm of other specified sites of female breast');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('174.9', 'L2999', 'Malignant neoplasm of breast (female), unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('198.81', 'L2999', 'Secondary malignant neoplasm of breast');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('213.8', 'L2999', 'Benign neoplasm of short bones of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('215.3', 'L2999', 'Other benign neoplasm of connective and other soft tissue of lower limb, including hip');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('228.01', 'L2999', 'Hemangioma of skin and subcutaneous tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250', 'L2999', 'Diabetes mellitus without mention of complication, type II or unspecified type, not stated as uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.01', 'L2999', 'Diabetes mellitus without mention of complication, type I [juvenile type], not stated as');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.02', 'L2999', 'Diabetes mellitus without mention of complication, type II or unspecified type, uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.6', 'L2999', 'Diabetes with neurological manifestations, type II or unspecified type, not stated as uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.61', 'L2999', 'Diabetes with neurological manifestations, type I [juvenile type], not stated as uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.7', 'L2999', 'Diabetes with peripheral circulatory disorders, type II or unspecified type, not stated as  uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.71', 'L2999', 'Diabetes with peripheral circulatory disorders, type I [juvenile type], not stated as uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.8', 'L2999', 'Diabetes with other specified manifestations, type II or unspecified type, not stated as  uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.9', 'L2999', 'Diabetes with unspecified complication, type II or unspecified type, not stated as uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('272', 'L2999', 'Pure hypercholesterolemia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('274', 'L2999', 'Gouty arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('274.11', 'L2999', 'Uric acid nephrolithiasis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('276.6', 'L2999', 'Fluid overload');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('277.5', 'L2999', 'Mucopolysaccharidosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('278', 'L2999', 'Obesity, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('285.9', 'L2999', 'Unspecified anemia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('289.3', 'L2999', 'Lymphadenitis, unspecified, except mesenteric');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('296.2', 'L2999', 'Major depressive disorder, single episode, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('300', 'L2999', 'Anxiety state, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('307.89', 'L2999', 'Other pain disorder related to psychological factors');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('310.2', 'L2999', 'Postconcussion syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('330.8', 'L2999', 'Other specified cerebral degenerations in childhood');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('331.4', 'L2999', 'Obstructive hydrocephalus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('331.9', 'L2999', 'Unspecified cerebral degeneration');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('332', 'L2999', 'Paralysis agitans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('335.2', 'L2999', 'Amyotrophic lateral sclerosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('337.22', 'L2999', 'Reflex sympathetic dystrophy of the lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('337.9', 'L2999', 'Unspecified disorder of autonomic nervous system');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('342.9', 'L2999', 'Unspecified hemiplegia affecting unspecified side');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('343', 'L2999', 'Diplegic infantile cerebral palsy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('343.1', 'L2999', 'Hemiplegic infantile cerebral palsy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('343.2', 'L2999', 'Quadriplegic infantile cerebral palsy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('343.3', 'L2999', 'Monoplegic infantile cerebral palsy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('343.4', 'L2999', 'Infantile hemiplegia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('343.9', 'L2999', 'Unspecified infantile cerebral palsy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344', 'L2999', 'Unspecified quadriplegia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.1', 'L2999', 'Paraplegia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.9', 'L2999', 'Unspecified paralysis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('345.9', 'L2999', 'Unspecified epilepsy without mention of intractable epilepsy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('346.1', 'L2999', 'Common migraine without mention of intractable migraine');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.5', 'L2999', 'Tarsal tunnel syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.6', 'L2999', 'Lesion of plantar nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.8', 'L2999', 'Unspecified mononeuritis of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.9', 'L2999', 'Mononeuritis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('356.4', 'L2999', 'Idiopathic progressive polyneuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('356.9', 'L2999', 'Unspecified hereditary and idiopathic peripheral neuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('357.9', 'L2999', 'Unspecified inflammatory and toxic neuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('382', 'L2999', 'Acute suppurative otitis media without spontaneous rupture of eardrum');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('434.91', 'L2999', 'Unspecified cerebral artery occlusion with cerebral infarction');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('437.9', 'L2999', 'Unspecified cerebrovascular disease');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('440.9', 'L2999', 'Generalized and unspecified atherosclerosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('451', 'L2999', 'Phlebitis and thrombophlebitis of superficial vessels of lower extremities');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('454', 'L2999', 'Varicose veins of lower extremities with ulcer');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('459.81', 'L2999', 'Unspecified venous (peripheral) insufficiency');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('459.9', 'L2999', 'Unspecified circulatory system disorder');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('562.1', 'L2999', 'Diverticulosis of colon (without mention of hemorrhage)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('581.3', 'L2999', 'Nephrotic syndrome with lesion of minimal change glomerulonephritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('671.03', 'L2999', 'Varicose veins of legs, antepartum');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('681.1', 'L2999', 'Unspecified cellulitis and abscess of toe');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('681.11', 'L2999', 'Onychia and paronychia of toe');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('682.7', 'L2999', 'Cellulitis and abscess of foot, except toes');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('686.9', 'L2999', 'Unspecified local infection of skin and subcutaneous tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('691.8', 'L2999', 'Other atopic dermatitis and related conditions');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('696', 'L2999', 'Psoriatic arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707', 'L2999', 'Decubitus ulcer');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707.14', 'L2999', 'Ulcer of heel and midfoot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707.15', 'L2999', 'Ulcer of other part of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707.8', 'L2999', 'Chronic ulcer of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('709.4', 'L2999', 'Foreign body granuloma of skin and subcutaneous tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('709.9', 'L2999', 'Unspecified disorder of skin and subcutaneous tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('710', 'L2999', 'Systemic lupus erythematosus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('710.4', 'L2999', 'Polymyositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('713.4', 'L2999', 'Arthropathy associated with respiratory disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('713.5', 'L2999', 'Arthropathy associated with neurological disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L2999', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.3', 'L2999', 'Polyarticular juvenile rheumatoid arthritis, chronic or unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.4', 'L2999', 'Chronic postrheumatic arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L2999', 'Generalized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.15', 'L2999', 'Primary localized osteoarthrosis, pelvic region and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.16', 'L2999', 'Primary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.17', 'L2999', 'Primary localized osteoarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.27', 'L2999', 'Secondary localized osteoarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.36', 'L2999', 'Localized osteoarthrosis not specified whether primary or secondary, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.37', 'L2999', 'Localized osteoarthrosis not specified whether primary or secondary, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.9', 'L2999', 'Osteoarthrosis, unspecified whether generalized or localized, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.95', 'L2999', 'Osteoarthrosis, unspecified whether generalized or localized, pelvic region and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.96', 'L2999', 'Osteoarthrosis, unspecified whether generalized or localized, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.97', 'L2999', 'Osteoarthrosis, unspecified whether generalized or localized, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.98', 'L2999', 'Osteoarthrosis, unspecified whether generalized or localized, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.16', 'L2999', 'Traumatic arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.17', 'L2999', 'Traumatic arthropathy, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.9', 'L2999', 'Unspecified arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.96', 'L2999', 'Unspecified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.97', 'L2999', 'Unspecified arthropathy, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717', 'L2999', 'Old bucket handle tear of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.3', 'L2999', 'Other and unspecified derangement of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.42', 'L2999', 'Derangement of anterior horn of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.5', 'L2999', 'Derangement of meniscus, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.7', 'L2999', 'Chondromalacia of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.81', 'L2999', 'Old disruption of lateral collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.83', 'L2999', 'Old disruption of anterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.89', 'L2999', 'Other internal derangement of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.04', 'L2999', 'Articular cartilage disorder, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.17', 'L2999', 'Loose body in ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.27', 'L2999', 'Pathological dislocation of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.36', 'L2999', 'Recurrent dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.4', 'L2999', 'Contracture of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.45', 'L2999', 'Contracture of pelvic joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.46', 'L2999', 'Contracture of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.47', 'L2999', 'Contracture of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.56', 'L2999', 'Ankylosis of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.8', 'L2999', 'Other joint derangement, not elsewhere classified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.85', 'L2999', 'Other joint derangement, not elsewhere classified, pelvic region and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.86', 'L2999', 'Other joint derangement, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.87', 'L2999', 'Other joint derangement, not elsewhere classified, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.9', 'L2999', 'Unspecified derangement, joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.97', 'L2999', 'Unspecified ankle and foot joint derangement');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719', 'L2999', 'Effusion of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.06', 'L2999', 'Effusion of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.07', 'L2999', 'Effusion of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.16', 'L2999', 'Hemarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.26', 'L2999', 'Villonodular synovitis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.27', 'L2999', 'Villonodular synovitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.46', 'L2999', 'Pain in joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.47', 'L2999', 'Pain in joint, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.48', 'L2999', 'Pain in joint, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.49', 'L2999', 'Pain in joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.57', 'L2999', 'Stiffness of joint, not elsewhere classified, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.66', 'L2999', 'Other symptoms referable to lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.7', 'L2999', 'Difficulty in walking');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.8', 'L2999', 'Other specified disorders of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.87', 'L2999', 'Other specified disorders of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.96', 'L2999', 'Unspecified disorder of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.97', 'L2999', 'Unspecified disorder of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('720', 'L2999', 'Ankylosing spondylitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('720.2', 'L2999', 'Sacroiliitis, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.5', 'L2999', 'Enthesopathy of hip region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.6', 'L2999', 'Unspecified enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.61', 'L2999', 'Pes anserinus tendinitis or bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.64', 'L2999', 'Patellar tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.65', 'L2999', 'Prepatellar bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.69', 'L2999', 'Other enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.7', 'L2999', 'Unspecified enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.71', 'L2999', 'Achilles bursitis or tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.72', 'L2999', 'Tibialis tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.73', 'L2999', 'Calcaneal spur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.79', 'L2999', 'Other enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.8', 'L2999', 'Other peripheral enthesopathies');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L2999', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.91', 'L2999', 'Exostosis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727', 'L2999', 'Unspecified synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.03', 'L2999', 'Trigger finger (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.04', 'L2999', 'Radial styloid tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.05', 'L2999', 'Other tenosynovitis of hand and wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.6', 'L2999', 'Rupture of tendon, nontraumatic');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L2999', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.2', 'L2999', 'Specific bursitides often of occupational origin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.3', 'L2999', 'Other bursitis disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.41', 'L2999', 'Ganglion of joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.42', 'L2999', 'Ganglion of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.43', 'L2999', 'Unspecified ganglion');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.49', 'L2999', 'Other ganglion and cyst of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.51', 'L2999', 'Synovial cyst of popliteal space');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.66', 'L2999', 'Nontraumatic rupture of patellar tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.67', 'L2999', 'Nontraumatic rupture of Achilles tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.68', 'L2999', 'Nontraumatic rupture of other tendons of foot and ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.81', 'L2999', 'Contracture of tendon (sheath)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.82', 'L2999', 'Calcium deposits in tendon and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.89', 'L2999', 'Other disorders of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728', 'L2999', 'Infective myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.3', 'L2999', 'Other specific muscle disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.4', 'L2999', 'Laxity of ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.5', 'L2999', 'Hypermobility syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.6', 'L2999', 'Contracture of palmar fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.71', 'L2999', 'Plantar fascial fibromatosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.79', 'L2999', 'Other fibromatoses of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.82', 'L2999', 'Foreign body granuloma of muscle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.83', 'L2999', 'Rupture of muscle, nontraumatic');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.85', 'L2999', 'Spasm of muscle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.9', 'L2999', 'Unspecified disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729', 'L2999', 'Rheumatism, unspecified and fibrositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.1', 'L2999', 'Unspecified myalgia and myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.2', 'L2999', 'Unspecified neuralgia, neuritis, and radiculitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.4', 'L2999', 'Unspecified fasciitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L2999', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.6', 'L2999', 'Residual foreign body in soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.81', 'L2999', 'Swelling of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.89', 'L2999', 'Other musculoskeletal symptoms referable to limbs');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.07', 'L2999', 'Acute osteomyelitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.27', 'L2999', 'Unspecified osteomyelitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.37', 'L2999', 'Periostitis, without mention of osteomyelitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('731', 'L2999', 'Osteitis deformans without mention of bone tumor');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732', 'L2999', 'Juvenile osteochondrosis of spine');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.4', 'L2999', 'Juvenile osteochondrosis of lower extremity, excluding foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.5', 'L2999', 'Juvenile osteochondrosis of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.7', 'L2999', 'Osteochondritis dissecans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733', 'L2999', 'Unspecified osteoporosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.1', 'L2999', 'Pathologic fracture, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.16', 'L2999', 'Pathologic fracture of tibia and fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.19', 'L2999', 'Pathologic fracture of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.4', 'L2999', 'Aseptic necrosis of bone, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.49', 'L2999', 'Aseptic necrosis of other bone site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.81', 'L2999', 'Malunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.82', 'L2999', 'Nonunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.9', 'L2999', 'Disorder of bone and cartilage, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.92', 'L2999', 'Chondromalacia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.99', 'L2999', 'Other disorders of bone and cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('735', 'L2999', 'Hallux valgus (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('735.2', 'L2999', 'Hallux rigidus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('735.4', 'L2999', 'Other hammer toe (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('735.5', 'L2999', 'Claw toe (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('735.8', 'L2999', 'Other acquired deformity of toe');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736', 'L2999', 'Unspecified deformity of forearm, excluding fingers');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.1', 'L2999', 'Mallet finger');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.21', 'L2999', 'Boutonniere deformity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.31', 'L2999', 'Coxa valga (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.42', 'L2999', 'Genu varum (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.6', 'L2999', 'Other acquired deformities of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.7', 'L2999', 'Unspecified deformity of ankle and foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.71', 'L2999', 'Acquired equinovarus deformity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.72', 'L2999', 'Equinus deformity of foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.73', 'L2999', 'Cavus deformity of foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.75', 'L2999', 'Cavovarus deformity of foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.76', 'L2999', 'Other acquired calcaneus deformity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.79', 'L2999', 'Other acquired deformity of ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.81', 'L2999', 'Unequal leg length (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.89', 'L2999', 'Other acquired deformity of other parts of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.9', 'L2999', 'Acquired deformity of limb, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('737.3', 'L2999', 'Scoliosis (and kyphoscoliosis), idiopathic');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('738', 'L2999', 'Acquired deformity of nose');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('739.6', 'L2999', 'Nonallopathic lesion of lower extremities, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('741', 'L2999', 'Spina bifida with hydrocephalus, unspecified region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('741.02', 'L2999', 'Spina bifida with hydrocephalus, dorsal (thoracic) region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('741.03', 'L2999', 'Spina bifida with hydrocephalus, lumbar region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('741.9', 'L2999', 'Spina bifida without mention of hydrocephalus, unspecified region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('741.93', 'L2999', 'Spina bifida without mention of hydrocephalus, lumbar region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('742.2', 'L2999', 'Congenital reduction deformities of brain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('747.64', 'L2999', 'Congenital lower limb vessel anomaly');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.3', 'L2999', 'Congenital dislocation of hip, unilateral');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.31', 'L2999', 'Congenital dislocation of hip, bilateral');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.33', 'L2999', 'Congenital subluxation of hip, bilateral');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.5', 'L2999', 'Congenital talipes varus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.51', 'L2999', 'Congenital talipes equinovarus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.52', 'L2999', 'Congenital metatarsus primus varus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.53', 'L2999', 'Congenital metatarsus varus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.59', 'L2999', 'Other congenital varus deformity of feet');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.6', 'L2999', 'Congenital talipes valgus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.61', 'L2999', 'Congenital pes planus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.62', 'L2999', 'Talipes calcaneovalgus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.69', 'L2999', 'Other congenital valgus deformity of feet');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.7', 'L2999', 'Unspecified talipes');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.71', 'L2999', 'Talipes cavus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.79', 'L2999', 'Other congenital deformity of feet');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.89', 'L2999', 'Other specified nonteratogenic anomalies');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.26', 'L2999', 'Congenital longitudinal deficiency, radial, complete or partial (with or without distal');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.3', 'L2999', 'Congenital unspecified reduction deformity of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.32', 'L2999', 'Congenital longitudinal deficiency of lower limb, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.38', 'L2999', 'Congenital longitudinal deficiency, tarsals or metatarsals, complete or partial (with or without incomplete phalangeal deficiency');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.4', 'L2999', 'Congenital reduction deformities, unspecified limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.56', 'L2999', 'Accessory carpal bones');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.6', 'L2999', 'Unspecified congenital anomaly of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.63', 'L2999', 'Other congenital deformity of hip (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.66', 'L2999', 'Other congenital anomaly of toes');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.67', 'L2999', 'Congenital anomalies of foot, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.69', 'L2999', 'Other congenital anomaly of lower limb, including pelvic girdle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756', 'L2999', 'Congenital anomalies of skull and face bones');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.51', 'L2999', 'Osteogenesis imperfecta');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.83', 'L2999', 'Ehlers-Danlos syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.89', 'L2999', 'Other specified congenital anomaly of muscle, tendon, fascia, and connective tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('757', 'L2999', 'Hereditary edema of legs');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('757.39', 'L2999', 'Other specified congenital anomaly of skin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('758', 'L2999', 'Down''s syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('758.3', 'L2999', 'Autosomal deletion syndromes');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('768.5', 'L2999', 'Severe birth asphyxia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('780.5', 'L2999', 'Unspecified sleep disturbance');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('780.79', 'L2999', 'Other malaise and fatigue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('783.42', 'L2999', 'Delayed milestones');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('820.8', 'L2999', 'Closed fracture of unspecified part of neck of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821', 'L2999', 'Closed fracture of unspecified part of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.01', 'L2999', 'Closed fracture of shaft of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.21', 'L2999', 'Closed fracture of femoral condyle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.22', 'L2999', 'Closed fracture of lower epiphysis of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823', 'L2999', 'Closed fracture of upper end of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.01', 'L2999', 'Closed fracture of upper end of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.02', 'L2999', 'Closed fracture of upper end of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.2', 'L2999', 'Closed fracture of shaft of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.21', 'L2999', 'Closed fracture of shaft of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.22', 'L2999', 'Closed fracture of shaft of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.32', 'L2999', 'Open fracture of shaft of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.8', 'L2999', 'Closed fracture of unspecified part of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.81', 'L2999', 'Closed fracture of unspecified part of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.82', 'L2999', 'Closed fracture of unspecified part of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.92', 'L2999', 'Open fracture of unspecified part of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824', 'L2999', 'Closed fracture of medial malleolus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.7', 'L2999', 'Open trimalleolar fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.8', 'L2999', 'Unspecified closed fracture of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.9', 'L2999', 'Unspecified open fracture of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825', 'L2999', 'Closed fracture of calcaneus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.2', 'L2999', 'Closed fracture of unspecified bone(s) of foot (except toes)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.21', 'L2999', 'Closed fracture of astragalus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.22', 'L2999', 'Closed fracture of navicular (scaphoid) bone of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.23', 'L2999', 'Closed fracture of cuboid bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.24', 'L2999', 'Closed fracture of cuneiform bone of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.25', 'L2999', 'Closed fracture of metatarsal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.29', 'L2999', 'Other closed fracture of tarsal and metatarsal bones');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.3', 'L2999', 'Open fracture of unspecified bone(s) of foot (except toes)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.31', 'L2999', 'Open fracture of astragalus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.35', 'L2999', 'Open fracture of metatarsal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.39', 'L2999', 'Other open fractures of tarsal and metatarsal bones');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('826', 'L2999', 'Closed fracture of one or more phalanges of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831', 'L2999', 'Closed dislocation of shoulder, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836', 'L2999', 'Tear of medial cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.5', 'L2999', 'Other closed dislocation of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.62', 'L2999', 'Open posterior dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('837', 'L2999', 'Closed dislocation of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838', 'L2999', 'Closed dislocation of foot, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838.01', 'L2999', 'Closed dislocation of tarsal (bone), joint unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838.03', 'L2999', 'Closed dislocation of tarsometatarsal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838.04', 'L2999', 'Closed dislocation of metatarsal (bone), joint unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838.05', 'L2999', 'Closed dislocation of metatarsophalangeal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('839', 'L2999', 'Closed dislocation, unspecified cervical vertebra');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('839.2', 'L2999', 'Closed dislocation, lumbar vertebra');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('839.42', 'L2999', 'Closed dislocation, sacrum');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('843', 'L2999', 'Iliofemoral (ligament) sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('843.1', 'L2999', 'Ischiocapsular (ligament) sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('843.8', 'L2999', 'Sprain and strain of other specified sites of hip and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('843.9', 'L2999', 'Sprain and strain of unspecified site of hip and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.9', 'L2999', 'Sprain and strain of unspecified site of knee and leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845', 'L2999', 'Unspecified site of ankle sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.01', 'L2999', 'Sprain and strain of deltoid (ligament) of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.02', 'L2999', 'Sprain and strain of calcaneofibular (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.03', 'L2999', 'Sprain and strain of tibiofibular (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.09', 'L2999', 'Other ankle sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.1', 'L2999', 'Sprain and strain of unspecified site of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.11', 'L2999', 'Sprain and strain of tarsometatarsal (joint) (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.12', 'L2999', 'Sprain and strain of metatarsaophalangeal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.13', 'L2999', 'Sprain and strain of interphalangeal (joint), of toe');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.19', 'L2999', 'Other foot sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.9', 'L2999', 'Unspecified site of sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('887', 'L2999', 'Traumatic amputation of arm and hand (complete) (partial), unilateral, below elbow, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('892', 'L2999', 'Open wound of foot except toe(s) alone, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('897', 'L2999', 'Traumatic amputation of leg(s) (complete) (partial), unilateral, below knee, without mention of  complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('897.1', 'L2999', 'Traumatic amputation of leg(s) (complete) (partial), unilateral, below knee, complicated');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('897.2', 'L2999', 'Traumatic amputation of leg(s) (complete) (partial), unilateral, at or above knee, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('900.03', 'L2999', 'Internal carotid artery injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('907', 'L2999', 'Late effect of intracranial injury without mention of skull fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('916.8', 'L2999', 'Other and unspecified superficial injury of hip, thigh, leg, and ankle, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('917', 'L2999', 'Abrasion or friction burn of foot and toe(s), without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924', 'L2999', 'Contusion of thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.01', 'L2999', 'Contusion of hip');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.1', 'L2999', 'Contusion of lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.11', 'L2999', 'Contusion of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.2', 'L2999', 'Contusion of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.21', 'L2999', 'Contusion of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.3', 'L2999', 'Contusion of toe');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.5', 'L2999', 'Contusion of unspecified part of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.9', 'L2999', 'Contusion of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('928', 'L2999', 'Crushing injury of thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('928.1', 'L2999', 'Crushing injury of lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('928.2', 'L2999', 'Crushing injury of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('928.3', 'L2999', 'Crushing injury of toe(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('952', 'L2999', 'C1-C4 level spinal cord injury, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('956.9', 'L2999', 'Injury to unspecified nerve of pelvic girdle and lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('957.9', 'L2999', 'Injury to nerves, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('958.8', 'L2999', 'Other early complications of trauma');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.7', 'L2999', 'Injury, other and unspecified, knee, leg, ankle, and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.8', 'L2999', 'Injury, other and unspecified, other specified sites, including multiple');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.9', 'L2999', 'Injury, other and unspecified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.79', 'L2999', 'Other complications due to other internal prosthetic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('174.9', 'L1800', 'Malignant neoplasm of breast (female), unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('215.3', 'L1800', 'Other benign neoplasm of connective and other soft tissue of lower limb, including hip');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250', 'L1800', 'Diabetes mellitus without mention of complication, type II or unspecified type, not stated as  uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('268.2', 'L1800', 'Osteomalacia, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('274', 'L1800', 'Gouty arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('275.4', 'L1800', 'Unspecified disorder of calcium metabolism');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('343.8', 'L1800', 'Other specified infantile cerebral palsy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.9', 'L1800', 'Unspecified paralysis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('346', 'L1800', 'Classical migraine without mention of intractable migraine');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('353.8', 'L1800', 'Other nerve root and plexus disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.5', 'L1800', 'Tarsal tunnel syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.6', 'L1800', 'Lesion of plantar nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.71', 'L1800', 'Causalgia of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.8', 'L1800', 'Unspecified mononeuritis of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('367.9', 'L1800', 'Unspecified disorder of refraction and accommodation');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('384.2', 'L1800', 'Unspecified perforation of tympanic membrane');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('385.3', 'L1800', 'Unspecified cholesteatoma');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('386.12', 'L1800', 'Vestibular neuronitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('453.9', 'L1800', 'Embolism and thrombosis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('459.81', 'L1800', 'Unspecified venous (peripheral) insufficiency');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('608.9', 'L1800', 'Unspecified disorder of male genital organs');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('680.9', 'L1800', 'Carbuncle and furuncle of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('682.6', 'L1800', 'Cellulitis and abscess of leg, except foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('711.06', 'L1800', 'Pyogenic arthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L1800', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.3', 'L1800', 'Polyarticular juvenile rheumatoid arthritis, chronic or unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.33', 'L1800', 'Monoarticular juvenile rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.9', 'L1800', 'Unspecified inflammatory polyarthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L1800', 'Generalized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.1', 'L1800', 'Primary localized osteoarthrosis, specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.16', 'L1800', 'Primary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.17', 'L1800', 'Primary localized osteoarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.18', 'L1800', 'Primary localized osteoarthrosis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.26', 'L1800', 'Secondary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.3', 'L1800', 'Localized osteoarthrosis not specified whether primary or secondary, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.36', 'L1800', 'Localized osteoarthrosis not specified whether primary or secondary, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.89', 'L1800', 'Osteoarthrosis involving multiple sites, but not specified as generalized');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.9', 'L1800', 'Osteoarthrosis, unspecified whether generalized or localized, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.95', 'L1800', 'Osteoarthrosis, unspecified whether generalized or localized, pelvic region and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.96', 'L1800', 'Osteoarthrosis, unspecified whether generalized or localized, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.98', 'L1800', 'Osteoarthrosis, unspecified whether generalized or localized, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.15', 'L1800', 'Traumatic arthropathy, pelvic region and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.16', 'L1800', 'Traumatic arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.36', 'L1800', 'Climacteric arthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.46', 'L1800', 'Transient arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.6', 'L1800', 'Unspecified monoarthritis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.66', 'L1800', 'Unspecified monoarthritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.86', 'L1800', 'Other specified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.9', 'L1800', 'Unspecified arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.95', 'L1800', 'Unspecified arthropathy, pelvic region and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.96', 'L1800', 'Unspecified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.98', 'L1800', 'Unspecified arthropathy, other unspecified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.99', 'L1800', 'Unspecified arthropathy, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717', 'L1800', 'Old bucket handle tear of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.3', 'L1800', 'Other and unspecified derangement of medial meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.4', 'L1800', 'Unspecified derangement of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.41', 'L1800', 'Bucket handle tear of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.42', 'L1800', 'Derangement of anterior horn of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.43', 'L1800', 'Derangement of posterior horn of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.49', 'L1800', 'Other derangement of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.5', 'L1800', 'Derangement of meniscus, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.6', 'L1800', 'Loose body in knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.7', 'L1800', 'Chondromalacia of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.81', 'L1800', 'Old disruption of lateral collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.82', 'L1800', 'Old disruption of medial collateral ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.83', 'L1800', 'Old disruption of anterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.84', 'L1800', 'Old disruption of posterior cruciate ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.85', 'L1800', 'Old disruption of other ligament of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.89', 'L1800', 'Other internal derangement of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718', 'L1800', 'Articular cartilage disorder, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.08', 'L1800', 'Articular cartilage disorder, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.2', 'L1800', 'Pathological dislocation of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.26', 'L1800', 'Pathological dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.3', 'L1800', 'Recurrent dislocation of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.36', 'L1800', 'Recurrent dislocation of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.5', 'L1800', 'Ankylosis of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.56', 'L1800', 'Ankylosis of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.8', 'L1800', 'Other joint derangement, not elsewhere classified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.86', 'L1800', 'Other joint derangement, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.87', 'L1800', 'Other joint derangement, not elsewhere classified, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.88', 'L1800', 'Other joint derangement, not elsewhere classified, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719', 'L1800', 'Effusion of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.06', 'L1800', 'Effusion of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.08', 'L1800', 'Effusion of joint, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.16', 'L1800', 'Hemarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.26', 'L1800', 'Villonodular synovitis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.28', 'L1800', 'Villonodular synovitis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.36', 'L1800', 'Palindromic rheumatism, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.4', 'L1800', 'Pain in joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.45', 'L1800', 'Pain in joint, pelvic region and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.46', 'L1800', 'Pain in joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.47', 'L1800', 'Pain in joint, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.48', 'L1800', 'Pain in joint, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.56', 'L1800', 'Stiffness of joint, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.66', 'L1800', 'Other symptoms referable to lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.68', 'L1800', 'Other symptoms referable to joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.7', 'L1800', 'Difficulty in walking');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.8', 'L1800', 'Other specified disorders of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.86', 'L1800', 'Other specified disorders of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.9', 'L1800', 'Unspecified disorder of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.96', 'L1800', 'Unspecified disorder of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('721.1', 'L1800', 'Cervical spondylosis with myelopathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('721.8', 'L1800', 'Other allied disorders of spine');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.3', 'L1800', 'Sciatica');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.4', 'L1800', 'Thoracic or lumbosacral neuritis or radiculitis, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.5', 'L1800', 'Unspecified backache');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.6', 'L1800', 'Disorders of sacrum');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.8', 'L1800', 'Other symptoms referable to back');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.6', 'L1800', 'Unspecified enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.62', 'L1800', 'Tibial collateral ligament bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.64', 'L1800', 'Patellar tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.65', 'L1800', 'Prepatellar bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.69', 'L1800', 'Other enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.7', 'L1800', 'Unspecified enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.71', 'L1800', 'Achilles bursitis or tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.72', 'L1800', 'Tibialis tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.79', 'L1800', 'Other enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L1800', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.91', 'L1800', 'Exostosis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727', 'L1800', 'Unspecified synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.01', 'L1800', 'Synovitis and tenosynovitis in diseases classified elsewhere');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.02', 'L1800', 'Giant cell tumor of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.06', 'L1800', 'Tenosynovitis of foot and ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L1800', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.3', 'L1800', 'Other bursitis disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.41', 'L1800', 'Ganglion of joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.42', 'L1800', 'Ganglion of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.49', 'L1800', 'Other ganglion and cyst of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.5', 'L1800', 'Unspecified rupture of synovium');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.51', 'L1800', 'Synovial cyst of popliteal space');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.81', 'L1800', 'Contracture of tendon (sheath)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.82', 'L1800', 'Calcium deposits in tendon and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.83', 'L1800', 'Plica syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.89', 'L1800', 'Other disorders of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.4', 'L1800', 'Laxity of ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.6', 'L1800', 'Contracture of palmar fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.71', 'L1800', 'Plantar fascial fibromatosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.81', 'L1800', 'Interstitial myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.89', 'L1800', 'Other disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.9', 'L1800', 'Unspecified disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729', 'L1800', 'Rheumatism, unspecified and fibrositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.1', 'L1800', 'Unspecified myalgia and myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.2', 'L1800', 'Unspecified neuralgia, neuritis, and radiculitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.31', 'L1800', 'Hypertrophy of fat pad, knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L1800', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.81', 'L1800', 'Swelling of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.82', 'L1800', 'Cramp of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.36', 'L1800', 'Periostitis, without mention of osteomyelitis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.4', 'L1800', 'Juvenile osteochondrosis of lower extremity, excluding foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.7', 'L1800', 'Osteochondritis dissecans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.21', 'L1800', 'Solitary bone cyst');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.42', 'L1800', 'Aseptic necrosis of head and neck of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.7', 'L1800', 'Algoneurodystrophy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.81', 'L1800', 'Malunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.82', 'L1800', 'Nonunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.9', 'L1800', 'Disorder of bone and cartilage, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.91', 'L1800', 'Arrest of bone development or growth');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.92', 'L1800', 'Chondromalacia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.99', 'L1800', 'Other disorders of bone and cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736', 'L1800', 'Unspecified deformity of forearm, excluding fingers');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.42', 'L1800', 'Genu varum (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.6', 'L1800', 'Other acquired deformities of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('737.3', 'L1800', 'Scoliosis (and kyphoscoliosis), idiopathic');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('738.8', 'L1800', 'Acquired musculoskeletal deformity of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('739.6', 'L1800', 'Nonallopathic lesion of lower extremities, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.6', 'L1800', 'Unspecified congenital anomaly of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.63', 'L1800', 'Other congenital deformity of hip (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.64', 'L1800', 'Congenital deformity of knee (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.69', 'L1800', 'Other congenital anomaly of lower limb, including pelvic girdle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.83', 'L1800', 'Ehlers-Danlos syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('820.21', 'L1800', 'Closed fracture of intertrochanteric section of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821', 'L1800', 'Closed fracture of unspecified part of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.01', 'L1800', 'Closed fracture of shaft of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.2', 'L1800', 'Closed fracture of unspecified part of lower end of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('821.21', 'L1800', 'Closed fracture of femoral condyle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('822', 'L1800', 'Closed fracture of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823', 'L1800', 'Closed fracture of upper end of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.01', 'L1800', 'Closed fracture of upper end of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.02', 'L1800', 'Closed fracture of upper end of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.1', 'L1800', 'Open fracture of upper end of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.2', 'L1800', 'Closed fracture of shaft of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.8', 'L1800', 'Closed fracture of unspecified part of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.81', 'L1800', 'Closed fracture of unspecified part of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.82', 'L1800', 'Closed fracture of unspecified part of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836', 'L1800', 'Tear of medial cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.5', 'L1800', 'Closed dislocation of knee, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.54', 'L1800', 'Closed lateral dislocation of tibia, proximal end');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836.59', 'L1800', 'Other closed dislocation of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('843', 'L1800', 'Iliofemoral (ligament) sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('843.9', 'L1800', 'Sprain and strain of unspecified site of hip and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.9', 'L1800', 'Sprain and strain of unspecified site of knee and leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845', 'L1800', 'Unspecified site of ankle sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.01', 'L1800', 'Sprain and strain of deltoid (ligament) of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.02', 'L1800', 'Sprain and strain of calcaneofibular (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.03', 'L1800', 'Sprain and strain of tibiofibular (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.09', 'L1800', 'Other ankle sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('904.8', 'L1800', 'Injury to unspecified blood vessel of lower extremity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('905.7', 'L1800', 'Late effect of sprain and strain without mention of tendon injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('916', 'L1800', 'Hip, thigh, leg, and ankle, abrasion or friction burn, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('916.8', 'L1800', 'Other and unspecified superficial injury of hip, thigh, leg, and ankle, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924', 'L1800', 'Contusion of thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.01', 'L1800', 'Contusion of hip');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.1', 'L1800', 'Contusion of lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.11', 'L1800', 'Contusion of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.4', 'L1800', 'Contusion of multiple sites of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.5', 'L1800', 'Contusion of unspecified part of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('958.8', 'L1800', 'Other early complications of trauma');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.7', 'L1800', 'Injury, other and unspecified, knee, leg, ankle, and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.8', 'L1800', 'Injury, other and unspecified, other specified sites, including multiple');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.9', 'L1800', 'Injury, other and unspecified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.59', 'L1800', 'Mechanical complication due to other implant and internal device, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.77', 'L1800', 'Other complications due to internal joint prosthesis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.78', 'L1800', 'Other complications due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('998.12', 'L1800', 'Hematoma complicating a procedure');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.59', 'L1800', 'Other immobilization, pressure, and attention to wound');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('353', 'L3660', 'Brachial plexus lesions');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354', 'L3660', 'Carpal tunnel syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.9', 'L3660', 'Unspecified mononeuritis of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.4', 'L3660', 'Chronic postrheumatic arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L3660', 'Generalized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.11', 'L3660', 'Primary localized osteoarthrosis, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.31', 'L3660', 'Localized osteoarthrosis not specified whether primary or secondary, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.91', 'L3660', 'Osteoarthrosis, unspecified whether generalized or localized, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.1', 'L3660', 'Traumatic arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.91', 'L3660', 'Unspecified arthropathy, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.21', 'L3660', 'Pathological dislocation of shoulder joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.3', 'L3660', 'Recurrent dislocation of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.31', 'L3660', 'Recurrent dislocation of shoulder joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.41', 'L3660', 'Contracture of shoulder joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.42', 'L3660', 'Contracture of upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.8', 'L3660', 'Other joint derangement, not elsewhere classified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.81', 'L3660', 'Other joint derangement, not elsewhere classified, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.82', 'L3660', 'Other joint derangement, not elsewhere classified, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.01', 'L3923', 'Synovitis and tenosynovitis in diseases classified elsewhere');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.02', 'L3923', 'Giant cell tumor of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.03', 'L3923', 'Trigger finger (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.04', 'L3923', 'Radial styloid tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.05', 'L3923', 'Other tenosynovitis of hand and wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.06', 'L3923', 'Tenosynovitis of foot and ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L3923', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.4', 'L3923', 'Unspecified synovial cyst');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.41', 'L3923', 'Ganglion of joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.42', 'L3923', 'Ganglion of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.49', 'L3923', 'Other ganglion and cyst of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.5', 'L3923', 'Unspecified rupture of synovium');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.51', 'L3923', 'Synovial cyst of popliteal space');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.59', 'L3923', 'Other rupture of synovium');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.6', 'L3923', 'Nontraumatic rupture of unspecified tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.63', 'L3923', 'Nontraumatic rupture of extensor tendons of hand and wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.64', 'L3923', 'Nontraumatic rupture of flexor tendons of hand and wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.65', 'L3923', 'Nontraumatic rupture of quadriceps tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.81', 'L3923', 'Contracture of tendon (sheath)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.82', 'L3923', 'Calcium deposits in tendon and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.4', 'L3923', 'Laxity of ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.6', 'L3923', 'Contracture of palmar fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.71', 'L3923', 'Plantar fascial fibromatosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.81', 'L3923', 'Interstitial myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.85', 'L3923', 'Spasm of muscle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.9', 'L3923', 'Unspecified disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729', 'L3923', 'Rheumatism, unspecified and fibrositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.1', 'L3923', 'Unspecified myalgia and myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.2', 'L3923', 'Unspecified neuralgia, neuritis, and radiculitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.23', 'L3923', 'Unspecified osteomyelitis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.24', 'L3923', 'Unspecified osteomyelitis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.3', 'L3923', 'Juvenile osteochondrosis of upper extremity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.7', 'L3923', 'Osteochondritis dissecans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.8', 'L3923', 'Other specified forms of osteochondropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.12', 'L3923', 'Pathologic fracture of distal radius and ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.81', 'L3923', 'Malunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.82', 'L3923', 'Nonunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.9', 'L3923', 'Disorder of bone and cartilage, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.05', 'L3923', 'Wrist drop (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.06', 'L3923', 'Claw hand (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.07', 'L3923', 'Club hand, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.09', 'L3923', 'Other acquired deformities of forearm, excluding fingers');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.1', 'L3923', 'Mallet finger');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.2', 'L3923', 'Unspecified deformity of finger');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.2', 'L3923', 'Congenital musculoskeletal deformity of spine');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.5', 'L3923', 'Unspecified congenital anomaly of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.54', 'L3923', 'Madelung''s deformity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.56', 'L3923', 'Accessory carpal bones');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.59', 'L3923', 'Other congenital anomaly of upper limb, including shoulder girdle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.4', 'L3923', 'Unspecified closed fracture of lower end of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.41', 'L3923', 'Closed Colles'' fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.42', 'L3923', 'Other closed fractures of distal end of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.43', 'L3923', 'Closed fracture of distal end of ulna (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.44', 'L3923', 'Closed fracture of lower end of radius with ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.45', 'L3923', 'Torus fracture of lower end of radius');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.5', 'L3923', 'Unspecified open fracture of lower end of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.51', 'L3923', 'Open Colles'' fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.52', 'L3923', 'Other open fractures of distal end of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.53', 'L3923', 'Open fracture of distal end of ulna (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.54', 'L3923', 'Open fracture of lower end of radius with ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.8', 'L3923', 'Closed fracture of unspecified part of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.81', 'L3923', 'Closed fracture of unspecified part of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.82', 'L3923', 'Closed fracture of unspecified part of ulna (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.83', 'L3923', 'Closed fracture of unspecified part of radius with ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.9', 'L3923', 'Open fracture of unspecified part of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.91', 'L3923', 'Open fracture of unspecified part of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.92', 'L3923', 'Open fracture of unspecified part of ulna (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.93', 'L3923', 'Open fracture of unspecified part of radius with ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814', 'L3923', 'Unspecified closed fracture of carpal bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.01', 'L3923', 'Closed fracture of navicular (scaphoid) bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.02', 'L3923', 'Closed fracture of lunate (semilunar) bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.03', 'L3923', 'Closed fracture of triquetral (cuneiform) bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.04', 'L3923', 'Closed fracture of pisiform bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.05', 'L3923', 'Closed fracture of trapezium bone (larger multangular) of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.06', 'L3923', 'Closed fracture of trapezoid bone (smaller multangular) of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.07', 'L3923', 'Closed fracture of capitate bone (os magnum) of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.08', 'L3923', 'Closed fracture of hamate (unciform) bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.09', 'L3923', 'Closed fracture of other bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.1', 'L3923', 'Unspecified open fracture of carpal bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.11', 'L3923', 'Open fracture of navicular (scaphoid) bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.13', 'L3923', 'Open fracture of triquetral (cuneiform) bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815', 'L3923', 'Closed fracture of metacarpal bone(s), site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.01', 'L3923', 'Closed fracture of base of thumb (first) metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.02', 'L3923', 'Closed fracture of base of other metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.03', 'L3923', 'Closed fracture of shaft of metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.04', 'L3923', 'Closed fracture of neck of metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.09', 'L3923', 'Closed fracture of multiple sites of metacarpus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.1', 'L3923', 'Open fracture of metacarpal bone(s), site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.11', 'L3923', 'Open fracture of base of thumb (first) metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.12', 'L3923', 'Open fracture of base of other metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.13', 'L3923', 'Open fracture of shaft of metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.14', 'L3923', 'Open fracture of neck of metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.19', 'L3923', 'Open fracture of multiple sites of metacarpus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('816', 'L3923', 'Closed fracture of unspecified phalanx or phalanges of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('816.01', 'L3923', 'Closed fracture of middle or proximal phalanx or phalanges of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('816.02', 'L3923', 'Closed fracture of distal phalanx or phalanges of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('816.03', 'L3923', 'Closed fracture of multiple sites of phalanx or phalanges of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('816.1', 'L3923', 'Open fracture of phalanx or phalanges of hand, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('816.11', 'L3923', 'Open fracture of middle or proximal phalanx or phalanges of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('816.12', 'L3923', 'Open fracture of distal phalanx or phalanges of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('817', 'L3923', 'Multiple closed fractures of hand bones');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833', 'L3923', 'Closed dislocation of wrist, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.01', 'L3923', 'Closed dislocation of distal radioulnar (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.03', 'L3923', 'Closed dislocation of midcarpal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.04', 'L3923', 'Closed dislocation of carpometacarpal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.05', 'L3923', 'Closed dislocation of proximal end of metacarpal (bone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.09', 'L3923', 'Closed dislocation of other part of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.14', 'L3923', 'Open dislocation of carpometacarpal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('834', 'L3923', 'Closed dislocation of finger, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('834.01', 'L3923', 'Closed dislocation of metacarpophalangeal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('834.02', 'L3923', 'Closed dislocation of interphalangeal (joint), hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('842', 'L3923', 'Sprain and strain of unspecified site of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('842.01', 'L3923', 'Sprain and strain of carpal (joint) of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('881.02', 'L3923', 'Open wound of wrist, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('881.1', 'L3923', 'Open wound of forearm, complicated');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('881.12', 'L3923', 'Open wound of wrist, complicated');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('881.2', 'L3923', 'Open wound of forearm, with tendon involvement');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('881.22', 'L3923', 'Open wound of wrist, with tendon involvement');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('882', 'L3923', 'Open wound of hand except finger(s) alone, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('883', 'L3923', 'Open wound of finger(s), without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('893', 'L3923', 'Open wound of toe(s), without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('903.8', 'L3923', 'Injury to specified blood vessels of upper extremity, other');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('904.8', 'L3923', 'Injury to unspecified blood vessel of lower extremity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('905.8', 'L3923', 'Late effect of tendon injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('912', 'L3923', 'Shoulder and upper arm, abrasion or friction burn, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('913.8', 'L3923', 'Other and unspecified superficial injury of elbow, forearm, and wrist, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('914', 'L3923', 'Hand(s) except finger(s) alone, abrasion or friction burn, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.2', 'L3923', 'Contusion of hand(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.21', 'L3923', 'Contusion of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.3', 'L3923', 'Contusion of finger');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.8', 'L3923', 'Contusion of multiple sites of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.9', 'L3923', 'Contusion of unspecified part of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('927.1', 'L3923', 'Crushing injury of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('927.2', 'L3923', 'Crushing injury of hand(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('927.21', 'L3923', 'Crushing injury of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('927.3', 'L3923', 'Crushing injury of finger(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('927.8', 'L3923', 'Crushing injury of multiple sites of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('943', 'L3923', 'Burn of unspecified degree of unspecified site of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('943.01', 'L3923', 'Burn of unspecified degree of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('943.21', 'L3923', 'Blisters with epidermal loss due to burn (second degree) of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('944', 'L3923', 'Burn of unspecified degree of unspecified site of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('944.1', 'L3923', 'Erythema due to burn (first degree) of unspecified site of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('944.2', 'L3923', 'Blisters with epidermal loss due to burn (second degree) of unspecified site of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('944.3', 'L3923', 'Full-thickness skin loss due to burn (third degree nos) of unspecified site of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('949', 'L3923', 'Burn of unspecified site, unspecified degree');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('954', 'L3923', 'Injury to cervical sympathetic nerve, excluding shoulder and pelvic girdles');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('955.1', 'L3923', 'Injury to median nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('955.2', 'L3923', 'Injury to ulnar nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('955.3', 'L3923', 'Injury to radial nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('955.6', 'L3923', 'Injury to digital nerve, upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('957.9', 'L3923', 'Injury to nerves, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('958.3', 'L3923', 'Posttraumatic wound infection not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('958.8', 'L3923', 'Other early complications of trauma');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.3', 'L3923', 'Injury, other and unspecified, elbow, forearm, and wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.4', 'L3923', 'Injury, other and unspecified, hand, except finger');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.5', 'L3923', 'Injury, other and unspecified, finger');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.8', 'L3923', 'Injury, other and unspecified, other specified sites, including multiple');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.9', 'L3923', 'Injury, other and unspecified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.59', 'L3923', 'Mechanical complication due to other implant and internal device, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.67', 'L3923', 'Infection and inflammatory reaction due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.78', 'L3923', 'Other complications due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.79', 'L3923', 'Other complications due to other internal prosthetic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('998.59', 'L3923', 'Other postoperative infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.54', 'L3923', 'Application of splint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.23', 'L3923', 'Fitting of orthotic device');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.83', 'L3660', 'Other joint derangement, not elsewhere classified, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.93', 'L3660', 'Unspecified derangement, forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.03', 'L3660', 'Effusion of forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.4', 'L3660', 'Pain in joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.41', 'L3660', 'Pain in joint, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.42', 'L3660', 'Pain in joint, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.43', 'L3660', 'Pain in joint, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.91', 'L3660', 'Unspecified disorder of shoulder joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726', 'L3660', 'Adhesive capsulitis of shoulder');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.1', 'L3660', 'Unspecified disorders of bursae and tendons in shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.11', 'L3660', 'Calcifying tendinitis of shoulder');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.12', 'L3660', 'Bicipital tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.19', 'L3660', 'Other specified disorders of rotator cuff syndrome of shoulder and allied disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.2', 'L3660', 'Other affections of shoulder region, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.31', 'L3660', 'Medial epicondylitis of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.32', 'L3660', 'Lateral epicondylitis of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.33', 'L3660', 'Olecranon bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L3660', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.91', 'L3660', 'Exostosis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727', 'L3660', 'Unspecified synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.2', 'L3660', 'Specific bursitides often of occupational origin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.3', 'L3660', 'Other bursitis disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.43', 'L3660', 'Unspecified ganglion');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.61', 'L3660', 'Complete rupture of rotator cuff');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.1', 'L3660', 'Unspecified myalgia and myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L3660', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730', 'L3660', 'Acute osteomyelitis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.3', 'L3660', 'Juvenile osteochondrosis of upper extremity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.4', 'L3660', 'Juvenile osteochondrosis of lower extremity, excluding foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.1', 'L3660', 'Pathologic fracture, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.11', 'L3660', 'Pathologic fracture of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.82', 'L3660', 'Nonunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.89', 'L3660', 'Other acquired deformity of other parts of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('737.1', 'L3660', 'Kyphosis (acquired) (postural)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.59', 'L3660', 'Other congenital anomaly of upper limb, including shoulder girdle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.63', 'L3660', 'Other congenital deformity of hip (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('805', 'L3660', 'Closed fracture of cervical vertebra, unspecified level without mention of spinal cord injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('805.02', 'L3660', 'Closed fracture of second cervical vertebra without mention of spinal cord injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('805.8', 'L3660', 'Closed fracture of unspecified part of vertebral column without mention of spinal cord injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('807.01', 'L3660', 'Closed fracture of one rib');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('810', 'L3660', 'Unspecified part of closed fracture of clavicle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('810.01', 'L3660', 'Closed fracture of sternal end of clavicle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('810.02', 'L3660', 'Closed fracture of shaft of clavicle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('810.03', 'L3660', 'Closed fracture of acromial end of clavicle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('810.1', 'L3660', 'Unspecified part of open fracture of clavicle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('810.13', 'L3660', 'Open fracture of acromial end of clavicle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('811', 'L3660', 'Closed fracture of unspecified part of scapula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('811.01', 'L3660', 'Closed fracture of acromial process of scapula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812', 'L3660', 'Closed fracture of unspecified part of upper end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.01', 'L3660', 'Closed fracture of surgical neck of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.02', 'L3660', 'Closed fracture of anatomical neck of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.03', 'L3660', 'Closed fracture of greater tuberosity of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.09', 'L3660', 'Other closed fractures of upper end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.11', 'L3660', 'Open fracture of surgical neck of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.2', 'L3660', 'Closed fracture of unspecified part of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.21', 'L3660', 'Closed fracture of shaft of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.4', 'L3660', 'Closed fracture of unspecified part of lower end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.41', 'L3660', 'Closed fracture of supracondylar humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.42', 'L3660', 'Closed fracture of lateral condyle of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.43', 'L3660', 'Closed fracture of medial condyle of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.44', 'L3660', 'Closed fracture of unspecified condyle(s) of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.49', 'L3660', 'Other closed fracture of lower end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.01', 'L3660', 'Closed fracture of olecranon process of ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.04', 'L3660', 'Other and unspecified closed fractures of proximal end of ulna (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.05', 'L3660', 'Closed fracture of head of radius');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.06', 'L3660', 'Closed fracture of neck of radius');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.07', 'L3660', 'Other and unspecified closed fractures of proximal end of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.11', 'L3660', 'Open fracture of olecranon process of ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.15', 'L3660', 'Open fracture of head of radius');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.21', 'L3660', 'Closed fracture of shaft of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.22', 'L3660', 'Closed fracture of shaft of ulna (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.23', 'L3660', 'Closed fracture of shaft of radius with ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.33', 'L3660', 'Open fracture of shaft of radius with ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.4', 'L3660', 'Unspecified closed fracture of lower end of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.44', 'L3660', 'Closed fracture of lower end of radius with ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('819', 'L3660', 'Multiple closed fractures involving both upper limbs, and upper limb with rib(s) and sternum');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831', 'L3660', 'Closed dislocation of shoulder, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.01', 'L3660', 'Closed anterior dislocation of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.02', 'L3660', 'Closed posterior dislocation of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.04', 'L3660', 'Closed dislocation of acromioclavicular (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.09', 'L3660', 'Closed dislocation of other site of shoulder');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.11', 'L3660', 'Open anterior dislocation of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832', 'L3660', 'Closed unspecified dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('839.21', 'L3660', 'Closed dislocation, thoracic vertebra');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('839.61', 'L3660', 'Closed dislocation, sternum');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847', 'L3660', 'Neck sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847.1', 'L3660', 'Thoracic sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847.2', 'L3660', 'Lumbar sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.41', 'L3660', 'Sprain and strain of sternoclavicular (joint) (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('912', 'L3660', 'Shoulder and upper arm, abrasion or friction burn, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('912.8', 'L3660', 'Other and unspecified superficial injury of shoulder and upper arm, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('913', 'L3660', 'Elbow, forearm, and wrist, abrasion or friction burn, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923', 'L3660', 'Contusion of shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.01', 'L3660', 'Contusion of scapular region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.09', 'L3660', 'Contusion of multiple sites of shoulder and upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.1', 'L3660', 'Contusion of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.11', 'L3660', 'Contusion of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.2', 'L3660', 'Contusion of hand(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.8', 'L3660', 'Contusion of multiple sites, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('927.03', 'L3660', 'Crushing injury of upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('927.21', 'L3660', 'Crushing injury of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('927.3', 'L3660', 'Crushing injury of finger(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('943.05', 'L3660', 'Burn of unspecified degree of shoulder');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('948.3', 'L3660', 'Burn (any degree) involving 30-39% of body surface with third degree burn of less than 10% or');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.19', 'L3660', 'Other injury of other sites of trunk');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.2', 'L3660', 'Injury, other and unspecified, shoulder and upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.8', 'L3660', 'Injury, other and unspecified, other specified sites, including multiple');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.9', 'L3660', 'Injury, other and unspecified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.54', 'L3660', 'Application of splint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.59', 'L3660', 'Other immobilization, pressure, and attention to wound');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.23', 'L3660', 'Fitting of orthotic device');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354', 'A4565', 'Carpal tunnel syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.1', 'A4565', 'Other lesion of median nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.2', 'A4565', 'Lesion of ulnar nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.3', 'A4565', 'Lesion of radial nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.4', 'A4565', 'Causalgia of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.5', 'A4565', 'Mononeuritis multiplex');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.8', 'A4565', 'Other mononeuritis of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.9', 'A4565', 'Unspecified mononeuritis of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('681', 'A4565', 'Unspecified cellulitis and abscess of finger');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('682.4', 'A4565', 'Cellulitis and abscess of hand, except fingers and thumb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'A4565', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.11', 'A4565', 'Primary localized osteoarthrosis, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.9', 'A4565', 'Osteoarthrosis, unspecified whether generalized or localized, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.91', 'A4565', 'Osteoarthrosis, unspecified whether generalized or localized, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716', 'A4565', 'Kaschin-Beck disease, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.01', 'A4565', 'Kaschin-Beck disease, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.02', 'A4565', 'Kaschin-Beck disease, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.03', 'A4565', 'Kaschin-Beck disease, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.04', 'A4565', 'Kaschin-Beck disease, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.08', 'A4565', 'Kaschin-Beck disease, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.09', 'A4565', 'Kaschin-Beck disease, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.1', 'A4565', 'Traumatic arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.11', 'A4565', 'Traumatic arthropathy, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.12', 'A4565', 'Traumatic arthropathy, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.13', 'A4565', 'Traumatic arthropathy, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.14', 'A4565', 'Traumatic arthropathy, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.18', 'A4565', 'Traumatic arthropathy, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.19', 'A4565', 'Traumatic arthropathy, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.21', 'A4565', 'Allergic arthritis, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.22', 'A4565', 'Allergic arthritis, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.23', 'A4565', 'Allergic arthritis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.24', 'A4565', 'Allergic arthritis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.28', 'A4565', 'Allergic arthritis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.29', 'A4565', 'Allergic arthritis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.3', 'A4565', 'Climacteric arthritis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.31', 'A4565', 'Climacteric arthritis, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.32', 'A4565', 'Climacteric arthritis, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.33', 'A4565', 'Climacteric arthritis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.34', 'A4565', 'Climacteric arthritis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.38', 'A4565', 'Climacteric arthritis involving other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.39', 'A4565', 'Climacteric arthritis involving multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.4', 'A4565', 'Transient arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.41', 'A4565', 'Transient arthropathy, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.42', 'A4565', 'Transient arthropathy, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.43', 'A4565', 'Transient arthropathy, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.44', 'A4565', 'Transient arthropathy, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.48', 'A4565', 'Transient arthropathy, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.49', 'A4565', 'Transient arthropathy, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.5', 'A4565', 'Unspecified polyarthropathy or polyarthritis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.51', 'A4565', 'Unspecified polyarthropathy or polyarthritis, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.52', 'A4565', 'Unspecified polyarthropathy or polyarthritis, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.53', 'A4565', 'Unspecified polyarthropathy or polyarthritis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.54', 'A4565', 'Unspecified polyarthropathy or polyarthritis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.58', 'A4565', 'Unspecified polyarthropathy or polyarthritis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.59', 'A4565', 'Unspecified polyarthropathy or polyarthritis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.6', 'A4565', 'Unspecified monoarthritis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.61', 'A4565', 'Unspecified monoarthritis, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.62', 'A4565', 'Unspecified monoarthritis, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.63', 'A4565', 'Unspecified monoarthritis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.64', 'A4565', 'Unspecified monoarthritis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.68', 'A4565', 'Unspecified monoarthritis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.8', 'A4565', 'Other specified arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.81', 'A4565', 'Other specified arthropathy, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.82', 'A4565', 'Other specified arthropathy, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.83', 'A4565', 'Other specified arthropathy, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.84', 'A4565', 'Other specified arthropathy, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.88', 'A4565', 'Other specified arthropathy, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.89', 'A4565', 'Other specified arthropathy, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.9', 'A4565', 'Unspecified arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.91', 'A4565', 'Unspecified arthropathy, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.92', 'A4565', 'Unspecified arthropathy, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.93', 'A4565', 'Unspecified arthropathy, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.94', 'A4565', 'Unspecified arthopathy, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.98', 'A4565', 'Unspecified arthropathy, other unspecified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.99', 'A4565', 'Unspecified arthropathy, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.01', 'A4565', 'Articular cartilage disorder, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.31', 'A4565', 'Recurrent dislocation of shoulder joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.81', 'A4565', 'Other joint derangement, not elsewhere classified, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.82', 'A4565', 'Other joint derangement, not elsewhere classified, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.91', 'A4565', 'Unspecified derangement, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719', 'A4565', 'Effusion of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.01', 'A4565', 'Effusion of shoulder joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.02', 'A4565', 'Effusion of upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.03', 'A4565', 'Effusion of forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.04', 'A4565', 'Effusion of hand joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.08', 'A4565', 'Effusion of joint, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.09', 'A4565', 'Effusion of joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.1', 'A4565', 'Hemarthrosis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.11', 'A4565', 'Hemarthrosis, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.12', 'A4565', 'Hemarthorsis, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.13', 'A4565', 'Hemarthrosis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.14', 'A4565', 'Hemarthrosis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.15', 'A4565', 'Hemarthrosis, pelvic region and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.16', 'A4565', 'Hemarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.17', 'A4565', 'Hemarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.18', 'A4565', 'Hemarthrosis, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.19', 'A4565', 'Hemarthrosis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.2', 'A4565', 'Villonodular synovitis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.21', 'A4565', 'Villonodular synovitis, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.22', 'A4565', 'Villonodular synovitis, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.23', 'A4565', 'Villonodular synovitis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.24', 'A4565', 'Villonodular synovitis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.28', 'A4565', 'Villonodular synovitis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.29', 'A4565', 'Villonodular synovitis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.3', 'A4565', 'Palindromic rheumatism, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.31', 'A4565', 'Palindromic rheumatism, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.32', 'A4565', 'Palindromic rheumatism, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.33', 'A4565', 'Palindromic rheumatism, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.34', 'A4565', 'Palindromic rheumatism, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.38', 'A4565', 'Palindromic rheumatism, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.39', 'A4565', 'Palindromic rheumatism, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.4', 'A4565', 'Pain in joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.41', 'A4565', 'Pain in joint, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.42', 'A4565', 'Pain in joint, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.43', 'A4565', 'Pain in joint, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.44', 'A4565', 'Pain in joint, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.48', 'A4565', 'Pain in joint, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.49', 'A4565', 'Pain in joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.5', 'A4565', 'Stiffness of joint, not elsewhere classified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.51', 'A4565', 'Stiffness of joint, not elsewhere classified, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.52', 'A4565', 'Stiffness of joint, not elsewhere classified, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.53', 'A4565', 'Stiffness of joint, not elsewhere classified, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.54', 'A4565', 'Stiffness of joint, not elsewhere classified, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.58', 'A4565', 'Stiffness of joint, not elsewhere classified, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.59', 'A4565', 'Stiffness of joints, not elsewhere classified, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.6', 'A4565', 'Other symptoms referable to joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.61', 'A4565', 'Other symptoms referable to shoulder joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.62', 'A4565', 'Other symptoms referable to upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.63', 'A4565', 'Other symptoms referable to forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.64', 'A4565', 'Other symptoms referable to hand joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.68', 'A4565', 'Other symptoms referable to joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.69', 'A4565', 'Other symptoms referable to joints of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.8', 'A4565', 'Other specified disorders of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.81', 'A4565', 'Other specified disorders of shoulder joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.82', 'A4565', 'Other specified disorders of upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.83', 'A4565', 'Other specified disorders of forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.84', 'A4565', 'Other specified disorders of hand joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.88', 'A4565', 'Other specified disorders of joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.89', 'A4565', 'Other specified disorders of joints of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.9', 'A4565', 'Unspecified disorder of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.91', 'A4565', 'Unspecified disorder of shoulder joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.92', 'A4565', 'Unspecified disorder of upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.93', 'A4565', 'Unspecified disorder of forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.94', 'A4565', 'Unspecified disorder of hand joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.98', 'A4565', 'Unspecified joint disorder of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.99', 'A4565', 'Unspecified joint disorder of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('723.3', 'A4565', 'Cervicobrachial syndrome (diffuse)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('723.4', 'A4565', 'Brachial neuritis or radiculitis nos');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726', 'A4565', 'Adhesive capsulitis of shoulder');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.1', 'A4565', 'Unspecified disorders of bursae and tendons in shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.11', 'A4565', 'Calcifying tendinitis of shoulder');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.12', 'A4565', 'Bicipital tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.19', 'A4565', 'Other specified disorders of rotator cuff syndrome of shoulder and allied disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.2', 'A4565', 'Other affections of shoulder region, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.3', 'A4565', 'Unspecified enthesopathy of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.31', 'A4565', 'Medial epicondylitis of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.32', 'A4565', 'Lateral epicondylitis of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.33', 'A4565', 'Olecranon bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.39', 'A4565', 'Other enthesopathy of elbow region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.4', 'A4565', 'Enthesopathy of wrist and carpus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.5', 'A4565', 'Enthesopathy of hip region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'A4565', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.91', 'A4565', 'Exostosis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727', 'A4565', 'Unspecified synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.01', 'A4565', 'Synovitis and tenosynovitis in diseases classified elsewhere');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.02', 'A4565', 'Giant cell tumor of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.03', 'A4565', 'Trigger finger (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.04', 'A4565', 'Radial styloid tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.05', 'A4565', 'Other tenosynovitis of hand and wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.06', 'A4565', 'Tenosynovitis of foot and ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'A4565', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.2', 'A4565', 'Specific bursitides often of occupational origin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.3', 'A4565', 'Other bursitis disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.4', 'A4565', 'Ganglion and cyst of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.41', 'A4565', 'Ganglion of joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.42', 'A4565', 'Ganglion of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.43', 'A4565', 'Unspecified ganglion');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.49', 'A4565', 'Other ganglion and cyst of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.5', 'A4565', 'Unspecified rupture of synovium');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.59', 'A4565', 'Other rupture of synovium');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.6', 'A4565', 'Nontraumatic rupture of unspecified tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.61', 'A4565', 'Complete rupture of rotator cuff');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.62', 'A4565', 'Nontraumatic rupture of tendons of biceps (long head)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.63', 'A4565', 'Nontraumatic rupture of extensor tendons of hand and wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.64', 'A4565', 'Nontraumatic rupture of flexor tendons of hand and wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.65', 'A4565', 'Nontraumatic rupture of quadriceps tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.69', 'A4565', 'Nontraumatic rupture of other tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.81', 'A4565', 'Contracture of tendon (sheath)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.82', 'A4565', 'Calcium deposits in tendon and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.83', 'A4565', 'Plica syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.89', 'A4565', 'Other disorders of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.9', 'A4565', 'Unspecified disorder of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.85', 'A4565', 'Spasm of muscle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.1', 'A4565', 'Unspecified myalgia and myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.2', 'A4565', 'Unspecified neuralgia, neuritis, and radiculitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.3', 'A4565', 'Panniculitis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.39', 'A4565', 'Panniculitis of other sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.4', 'A4565', 'Unspecified fasciitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'A4565', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.6', 'A4565', 'Residual foreign body in soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.81', 'A4565', 'Swelling of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.82', 'A4565', 'Cramp of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.89', 'A4565', 'Other musculoskeletal symptoms referable to limbs');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.9', 'A4565', 'Other and unspecified disorders of soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.82', 'A4565', 'Nonunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.05', 'A4565', 'Wrist drop (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.06', 'A4565', 'Claw hand (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.07', 'A4565', 'Club hand, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.09', 'A4565', 'Other acquired deformities of forearm, excluding fingers');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.1', 'A4565', 'Mallet finger');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.2', 'A4565', 'Unspecified deformity of finger');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.21', 'A4565', 'Boutonniere deformity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.22', 'A4565', 'Swan-neck deformity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.29', 'A4565', 'Other acquired deformity of finger');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.31', 'A4565', 'Coxa valga (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.32', 'A4565', 'Coxa vara (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.89', 'A4565', 'Other acquired deformity of other parts of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.9', 'A4565', 'Acquired deformity of limb, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.59', 'A4565', 'Other congenital anomaly of upper limb, including shoulder girdle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('782.3', 'A4565', 'Edema');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('782.5', 'A4565', 'Cyanosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('782.61', 'A4565', 'Pallor');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('782.62', 'A4565', 'Flushing');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('782.7', 'A4565', 'Spontaneous ecchymoses');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('782.8', 'A4565', 'Changes in skin texture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('782.9', 'A4565', 'Other symptoms involving skin and integumentary tissues');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('810', 'A4565', 'Unspecified part of closed fracture of clavicle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('810.01', 'A4565', 'Closed fracture of sternal end of clavicle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('810.02', 'A4565', 'Closed fracture of shaft of clavicle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('810.03', 'A4565', 'Closed fracture of acromial end of clavicle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('810.1', 'A4565', 'Unspecified part of open fracture of clavicle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('810.11', 'A4565', 'Open fracture of sternal end of clavicle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('810.12', 'A4565', 'Open fracture of shaft of clavicle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('810.13', 'A4565', 'Open fracture of acromial end of clavicle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('811', 'A4565', 'Closed fracture of unspecified part of scapula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('811.01', 'A4565', 'Closed fracture of acromial process of scapula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('811.03', 'A4565', 'Closed fracture of glenoid cavity and neck of scapula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812', 'A4565', 'Closed fracture of unspecified part of upper end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.01', 'A4565', 'Closed fracture of surgical neck of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.02', 'A4565', 'Closed fracture of anatomical neck of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.03', 'A4565', 'Closed fracture of greater tuberosity of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.09', 'A4565', 'Other closed fractures of upper end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.1', 'A4565', 'Open fracture of unspecified part of upper end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.11', 'A4565', 'Open fracture of surgical neck of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.12', 'A4565', 'Open fracture of anatomical neck of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.13', 'A4565', 'Open fracture of greater tuberosity of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.19', 'A4565', 'Other open fracture of upper end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.2', 'A4565', 'Closed fracture of unspecified part of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.21', 'A4565', 'Closed fracture of shaft of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.3', 'A4565', 'Open fracture of unspecified part of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.31', 'A4565', 'Open fracture of shaft of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.4', 'A4565', 'Closed fracture of unspecified part of lower end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.41', 'A4565', 'Closed fracture of supracondylar humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.42', 'A4565', 'Closed fracture of lateral condyle of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.43', 'A4565', 'Closed fracture of medial condyle of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.44', 'A4565', 'Closed fracture of unspecified condyle(s) of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.49', 'A4565', 'Other closed fracture of lower end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.5', 'A4565', 'Open fracture of unspecified part of lower end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.51', 'A4565', 'Open fracture of supracondylar humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.52', 'A4565', 'Open fracture of lateral condyle of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.53', 'A4565', 'Open fracture of medial condyle of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.54', 'A4565', 'Open fracture of unspecified condyle(s) of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.59', 'A4565', 'Other open fracture of lower end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813', 'A4565', 'Unspecified fracture of radius and ulna, upper end of forearm, closed');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.01', 'A4565', 'Closed fracture of olecranon process of ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.02', 'A4565', 'Closed fracture of coronoid process of ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.03', 'A4565', 'Closed Monteggia''s fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.04', 'A4565', 'Other and unspecified closed fractures of proximal end of ulna (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.05', 'A4565', 'Closed fracture of head of radius');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.06', 'A4565', 'Closed fracture of neck of radius');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.07', 'A4565', 'Other and unspecified closed fractures of proximal end of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.08', 'A4565', 'Closed fracture of radius with ulna, upper end (any part)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.1', 'A4565', 'Unspecified open fracture of upper end of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.11', 'A4565', 'Open fracture of olecranon process of ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.12', 'A4565', 'Open fracture of coronoid process of ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.13', 'A4565', 'Open Monteggia''s fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.14', 'A4565', 'Other and unspecified open fractures of proximal end of ulna (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.15', 'A4565', 'Open fracture of head of radius');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.16', 'A4565', 'Open fracture of neck of radius');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.17', 'A4565', 'Other and unspecified open fractures of proximal end of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.18', 'A4565', 'Open fracture of radius with ulna, upper end (any part)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.2', 'A4565', 'Unspecified closed fracture of shaft of radius or ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.21', 'A4565', 'Closed fracture of shaft of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.22', 'A4565', 'Closed fracture of shaft of ulna (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.23', 'A4565', 'Closed fracture of shaft of radius with ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.3', 'A4565', 'Unspecified open fracture of shaft of radius or ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.31', 'A4565', 'Open fracture of shaft of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.32', 'A4565', 'Open fracture of shaft of ulna (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.33', 'A4565', 'Open fracture of shaft of radius with ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.4', 'A4565', 'Unspecified closed fracture of lower end of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.41', 'A4565', 'Closed Colles'' fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.42', 'A4565', 'Other closed fractures of distal end of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.43', 'A4565', 'Closed fracture of distal end of ulna (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.44', 'A4565', 'Closed fracture of lower end of radius with ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.5', 'A4565', 'Unspecified open fracture of lower end of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.51', 'A4565', 'Open Colles'' fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.52', 'A4565', 'Other open fractures of distal end of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.53', 'A4565', 'Open fracture of distal end of ulna (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.54', 'A4565', 'Open fracture of lower end of radius with ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.8', 'A4565', 'Closed fracture of unspecified part of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.81', 'A4565', 'Closed fracture of unspecified part of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.82', 'A4565', 'Closed fracture of unspecified part of ulna (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.83', 'A4565', 'Closed fracture of unspecified part of radius with ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.9', 'A4565', 'Open fracture of unspecified part of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.91', 'A4565', 'Open fracture of unspecified part of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.92', 'A4565', 'Open fracture of unspecified part of ulna (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.93', 'A4565', 'Open fracture of unspecified part of radius with ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814', 'A4565', 'Unspecified closed fracture of carpal bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.01', 'A4565', 'Closed fracture of navicular (scaphoid) bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.02', 'A4565', 'Closed fracture of lunate (semilunar) bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.03', 'A4565', 'Closed fracture of triquetral (cuneiform) bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.04', 'A4565', 'Closed fracture of pisiform bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.05', 'A4565', 'Closed fracture of trapezium bone (larger multangular) of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.06', 'A4565', 'Closed fracture of trapezoid bone (smaller multangular) of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.07', 'A4565', 'Closed fracture of capitate bone (os magnum) of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.08', 'A4565', 'Closed fracture of hamate (unciform) bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.09', 'A4565', 'Closed fracture of other bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.1', 'A4565', 'Unspecified open fracture of carpal bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.11', 'A4565', 'Open fracture of navicular (scaphoid) bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.12', 'A4565', 'Open fracture of lunate (semilunar) bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.13', 'A4565', 'Open fracture of triquetral (cuneiform) bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.14', 'A4565', 'Open fracture of pisiform bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.15', 'A4565', 'Open fracture of trapezium bone (larger multangular) of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.16', 'A4565', 'Open fracture of trapezoid bone (smaller multangular) of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.17', 'A4565', 'Open fracture of capitate bone (os magnum) of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.18', 'A4565', 'Open fracture of hamate (unciform) bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.19', 'A4565', 'Open fracture of other bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815', 'A4565', 'Closed fracture of metacarpal bone(s), site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.01', 'A4565', 'Closed fracture of base of thumb (first) metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.02', 'A4565', 'Closed fracture of base of other metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.03', 'A4565', 'Closed fracture of shaft of metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.04', 'A4565', 'Closed fracture of neck of metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.09', 'A4565', 'Closed fracture of multiple sites of metacarpus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.1', 'A4565', 'Open fracture of metacarpal bone(s), site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.11', 'A4565', 'Open fracture of base of thumb (first) metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.12', 'A4565', 'Open fracture of base of other metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.13', 'A4565', 'Open fracture of shaft of metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.14', 'A4565', 'Open fracture of neck of metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.19', 'A4565', 'Open fracture of multiple sites of metacarpus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('816', 'A4565', 'Closed fracture of unspecified phalanx or phalanges of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('816.01', 'A4565', 'Closed fracture of middle or proximal phalanx or phalanges of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('816.02', 'A4565', 'Closed fracture of distal phalanx or phalanges of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('816.1', 'A4565', 'Open fracture of phalanx or phalanges of hand, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('816.12', 'A4565', 'Open fracture of distal phalanx or phalanges of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('817', 'A4565', 'Multiple closed fractures of hand bones');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.2', 'A4565', 'Closed fracture of unspecified bone(s) of foot (except toes)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.25', 'A4565', 'Closed fracture of metatarsal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('829', 'A4565', 'Closed fracture of unspecified bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831', 'A4565', 'Closed dislocation of shoulder, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.01', 'A4565', 'Closed anterior dislocation of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.02', 'A4565', 'Closed posterior dislocation of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.03', 'A4565', 'Closed inferior dislocation of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.04', 'A4565', 'Closed dislocation of acromioclavicular (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.09', 'A4565', 'Closed dislocation of other site of shoulder');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.1', 'A4565', 'Open unspecified dislocation of shoulder');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.11', 'A4565', 'Open anterior dislocation of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.12', 'A4565', 'Open posterior dislocation of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.13', 'A4565', 'Open inferior dislocation of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.14', 'A4565', 'Open dislocation of acromioclavicular (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.19', 'A4565', 'Open dislocation of other site of shoulder');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832', 'A4565', 'Closed unspecified dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832.01', 'A4565', 'Closed anterior dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832.02', 'A4565', 'Closed posterior dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832.03', 'A4565', 'Closed medial dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832.04', 'A4565', 'Closed lateral dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832.09', 'A4565', 'Closed dislocation of other site of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832.1', 'A4565', 'Open unspecified dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832.11', 'A4565', 'Open anterior dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832.12', 'A4565', 'Open posterior dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832.13', 'A4565', 'Open medial dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832.14', 'A4565', 'Open lateral dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832.19', 'A4565', 'Open dislocation of other site of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833', 'A4565', 'Closed dislocation of wrist, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.01', 'A4565', 'Closed dislocation of distal radioulnar (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.02', 'A4565', 'Closed dislocation of radiocarpal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.03', 'A4565', 'Closed dislocation of midcarpal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.04', 'A4565', 'Closed dislocation of carpometacarpal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.05', 'A4565', 'Closed dislocation of proximal end of metacarpal (bone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.09', 'A4565', 'Closed dislocation of other part of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.1', 'A4565', 'Open dislocation of wrist, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.11', 'A4565', 'Open dislocation of distal radioulnar (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.12', 'A4565', 'Open dislocation of radiocarpal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.13', 'A4565', 'Open dislocation of midcarpal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.14', 'A4565', 'Open dislocation of carpometacarpal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.15', 'A4565', 'Open dislocation of proximal end of metacarpal (bone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.19', 'A4565', 'Open dislocation of other part of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('840.1', 'A4565', 'Coracoclavicular (ligament) sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('840.2', 'A4565', 'Coracohumeral (ligament) sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('840.3', 'A4565', 'Infraspinatus (muscle) (tendon) sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('840.4', 'A4565', 'Rotator cuff (capsule) sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('840.5', 'A4565', 'Subscapularis (muscle) sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('840.6', 'A4565', 'Supraspinatus (muscle) (tendon) sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('840.7', 'A4565', 'Superior glenoid labrum lesions (SLAP)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('840.8', 'A4565', 'Sprain and strain of other specified sites of shoulder and upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('840.9', 'A4565', 'Sprain and strain of unspecified site of shoulder and upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('841', 'A4565', 'Radial collateral ligament sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('841.1', 'A4565', 'Ulnar collateral ligament sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('841.2', 'A4565', 'Radiohumeral (joint) sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('841.3', 'A4565', 'Ulnohumeral (joint) sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('841.8', 'A4565', 'Sprain and strain of other specified sites of elbow and forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('841.9', 'A4565', 'Sprain and strain of unspecified site of elbow and forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('842', 'A4565', 'Sprain and strain of unspecified site of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('842.01', 'A4565', 'Sprain and strain of carpal (joint) of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('842.02', 'A4565', 'Sprain and strain of radiocarpal (joint) (ligament) of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('842.09', 'A4565', 'Other wrist sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('842.1', 'A4565', 'Sprain and strain of unspecified site of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('842.11', 'A4565', 'Sprain and strain of carpometacarpal (joint) of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('842.12', 'A4565', 'Sprain and strain of metacarpophalangeal (joint) of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('842.13', 'A4565', 'Sprain and strain of interphalangeal (joint) of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('842.19', 'A4565', 'Other hand sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847', 'A4565', 'Neck sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.8', 'A4565', 'Other specified sites of sprains and strains');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.9', 'A4565', 'Unspecified site of sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('879.8', 'A4565', 'Open wound(s) (multiple) of unspecified site(s), without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('880', 'A4565', 'Open wound of shoulder region, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('881', 'A4565', 'Open wound of forearm, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('881.01', 'A4565', 'Open wound of elbow, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('881.11', 'A4565', 'Open wound of elbow, complicated');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('882', 'A4565', 'Open wound of hand except finger(s) alone, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('883', 'A4565', 'Open wound of finger(s), without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('912', 'A4565', 'Shoulder and upper arm, abrasion or friction burn, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('912.8', 'A4565', 'Other and unspecified superficial injury of shoulder and upper arm, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('913', 'A4565', 'Elbow, forearm, and wrist, abrasion or friction burn, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('913.8', 'A4565', 'Other and unspecified superficial injury of elbow, forearm, and wrist, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('919', 'A4565', 'Abrasion or friction burn of other, multiple, and unspecified sites, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923', 'A4565', 'Contusion of shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.01', 'A4565', 'Contusion of scapular region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.02', 'A4565', 'Contusion of axillary region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.03', 'A4565', 'Contusion of upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.09', 'A4565', 'Contusion of multiple sites of shoulder and upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.1', 'A4565', 'Contusion of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.11', 'A4565', 'Contusion of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.2', 'A4565', 'Contusion of hand(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.21', 'A4565', 'Contusion of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.3', 'A4565', 'Contusion of finger');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.8', 'A4565', 'Contusion of multiple sites of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.9', 'A4565', 'Contusion of unspecified part of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.8', 'A4565', 'Contusion of multiple sites, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.9', 'A4565', 'Contusion of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('927.2', 'A4565', 'Crushing injury of hand(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('927.3', 'A4565', 'Crushing injury of finger(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('943.2', 'A4565', 'Blisters with epidermal loss due to burn (second degree) of unspecified site of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('955.3', 'A4565', 'Injury to radial nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('955.9', 'A4565', 'Injury to unspecified nerve of shoulder girdle and upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.2', 'A4565', 'Injury, other and unspecified, shoulder and upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.3', 'A4565', 'Injury, other and unspecified, elbow, forearm, and wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.4', 'A4565', 'Injury, other and unspecified, hand, except finger');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.5', 'A4565', 'Injury, other and unspecified, finger');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.8', 'A4565', 'Injury, other and unspecified, other specified sites, including multiple');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.9', 'A4565', 'Injury, other and unspecified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354', 'L3960', 'Carpal tunnel syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.3', 'L3960', 'Lesion of radial nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.5', 'L3960', 'Mononeuritis multiplex');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.8', 'L3960', 'Other mononeuritis of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.11', 'L3960', 'Primary localized osteoarthrosis, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.13', 'L3960', 'Primary localized osteoarthrosis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.31', 'L3960', 'Localized osteoarthrosis not specified whether primary or secondary, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.9', 'L3960', 'Osteoarthrosis, unspecified whether generalized or localized, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.91', 'L3960', 'Osteoarthrosis, unspecified whether generalized or localized, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.94', 'L3960', 'Osteoarthrosis, unspecified whether generalized or localized, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.91', 'L3960', 'Unspecified arthropathy, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.01', 'L3960', 'Articular cartilage disorder, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.21', 'L3960', 'Pathological dislocation of shoulder joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.31', 'L3960', 'Recurrent dislocation of shoulder joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.4', 'L3960', 'Contracture of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.41', 'L3960', 'Contracture of shoulder joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.81', 'L3960', 'Other joint derangement, not elsewhere classified, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.82', 'L3960', 'Other joint derangement, not elsewhere classified, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.83', 'L3960', 'Other joint derangement, not elsewhere classified, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.91', 'L3960', 'Unspecified derangement, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.41', 'L3960', 'Pain in joint, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.42', 'L3960', 'Pain in joint, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.43', 'L3960', 'Pain in joint, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.44', 'L3960', 'Pain in joint, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.52', 'L3960', 'Stiffness of joint, not elsewhere classified, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.53', 'L3960', 'Stiffness of joint, not elsewhere classified, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.59', 'L3960', 'Stiffness of joints, not elsewhere classified, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.81', 'L3960', 'Other specified disorders of shoulder joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726', 'L3960', 'Adhesive capsulitis of shoulder');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.1', 'L3960', 'Unspecified disorders of bursae and tendons in shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.11', 'L3960', 'Calcifying tendinitis of shoulder');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.12', 'L3960', 'Bicipital tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.19', 'L3960', 'Other specified disorders of rotator cuff syndrome of shoulder and allied disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.2', 'L3960', 'Other affections of shoulder region, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.31', 'L3960', 'Medial epicondylitis of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.32', 'L3960', 'Lateral epicondylitis of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.33', 'L3960', 'Olecranon bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.4', 'L3960', 'Enthesopathy of wrist and carpus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.79', 'L3960', 'Other enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L3960', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.91', 'L3960', 'Exostosis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.04', 'L3960', 'Radial styloid tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.05', 'L3960', 'Other tenosynovitis of hand and wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L3960', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.61', 'L3960', 'Complete rupture of rotator cuff');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.62', 'L3960', 'Nontraumatic rupture of tendons of biceps (long head)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.64', 'L3960', 'Nontraumatic rupture of flexor tendons of hand and wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.2', 'L3960', 'Muscular wasting and disuse atrophy, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.4', 'L3960', 'Laxity of ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.83', 'L3960', 'Rupture of muscle, nontraumatic');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.81', 'L3960', 'Malunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.82', 'L3960', 'Nonunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('811.02', 'L3960', 'Closed fracture of coracoid process of scapula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812', 'L3960', 'Closed fracture of unspecified part of upper end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.01', 'L3960', 'Closed fracture of surgical neck of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.02', 'L3960', 'Closed fracture of anatomical neck of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.03', 'L3960', 'Closed fracture of greater tuberosity of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.09', 'L3960', 'Other closed fractures of upper end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.2', 'L3960', 'Closed fracture of unspecified part of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.21', 'L3960', 'Closed fracture of shaft of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.3', 'L3960', 'Open fracture of unspecified part of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.4', 'L3960', 'Closed fracture of unspecified part of lower end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.41', 'L3960', 'Closed fracture of supracondylar humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.5', 'L3960', 'Open fracture of unspecified part of lower end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813', 'L3960', 'Unspecified fracture of radius and ulna, upper end of forearm, closed');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.01', 'L3960', 'Closed fracture of olecranon process of ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.03', 'L3960', 'Closed Monteggia''s fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.05', 'L3960', 'Closed fracture of head of radius');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.07', 'L3960', 'Other and unspecified closed fractures of proximal end of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.21', 'L3960', 'Closed fracture of shaft of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.23', 'L3960', 'Closed fracture of shaft of radius with ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.41', 'L3960', 'Closed Colles'' fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.42', 'L3960', 'Other closed fractures of distal end of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.43', 'L3960', 'Closed fracture of distal end of ulna (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.44', 'L3960', 'Closed fracture of lower end of radius with ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.54', 'L3960', 'Open fracture of lower end of radius with ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.81', 'L3960', 'Closed fracture of unspecified part of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.91', 'L3960', 'Open fracture of unspecified part of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.01', 'L3960', 'Closed fracture of navicular (scaphoid) bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.03', 'L3960', 'Closed fracture of triquetral (cuneiform) bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815', 'L3960', 'Closed fracture of metacarpal bone(s), site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.11', 'L3960', 'Open fracture of base of thumb (first) metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('816', 'L3960', 'Closed fracture of unspecified phalanx or phalanges of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('819', 'L3960', 'Multiple closed fractures involving both upper limbs, and upper limb with rib(s) and sternum');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('829', 'L3960', 'Closed fracture of unspecified bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831', 'L3960', 'Closed dislocation of shoulder, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.01', 'L3960', 'Closed anterior dislocation of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.02', 'L3960', 'Closed posterior dislocation of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.04', 'L3960', 'Closed dislocation of acromioclavicular (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832', 'L3960', 'Closed unspecified dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('839.61', 'L3960', 'Closed dislocation, sternum');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('842', 'L3960', 'Sprain and strain of unspecified site of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('842.01', 'L3960', 'Sprain and strain of carpal (joint) of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('842.02', 'L3960', 'Sprain and strain of radiocarpal (joint) (ligament) of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('842.09', 'L3960', 'Other wrist sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('842.12', 'L3960', 'Sprain and strain of metacarpophalangeal (joint) of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845', 'L3960', 'Unspecified site of ankle sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('846', 'L3960', 'Sprain and strain of lumbosacral (joint) (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.8', 'L3960', 'Other specified sites of sprains and strains');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.9', 'L3960', 'Unspecified site of sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923', 'L3960', 'Contusion of shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.03', 'L3960', 'Contusion of upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.1', 'L3960', 'Contusion of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.11', 'L3960', 'Contusion of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.2', 'L3960', 'Contusion of hand(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.21', 'L3960', 'Contusion of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('927.3', 'L3960', 'Crushing injury of finger(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('943.05', 'L3960', 'Burn of unspecified degree of shoulder');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('955.2', 'L3960', 'Injury to ulnar nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.3', 'L3960', 'Injury, other and unspecified, elbow, forearm, and wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.9', 'L3960', 'Injury, other and unspecified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.78', 'L3960', 'Other complications due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('998.89', 'L3960', 'Other specified complications');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('171.2', 'L3675', 'Malignant neoplasm of connective and other soft tissue of upper limb, including shoulder');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('238', 'L3675', 'Neoplasm of uncertain behavior of bone and articular cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('337.21', 'L3675', 'Reflex sympathetic dystrophy of the upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('353', 'L3675', 'Brachial plexus lesions');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.8', 'L3675', 'Other mononeuritis of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('709.9', 'L3675', 'Unspecified disorder of skin and subcutaneous tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('711.02', 'L3675', 'Pyogenic arthritis, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L3675', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L3675', 'Generalized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.09', 'L3675', 'Generalized osteoarthrosis, involving multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.11', 'L3675', 'Primary localized osteoarthrosis, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.21', 'L3675', 'Secondary localized osteoarthrosis, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.22', 'L3675', 'Secondary localized osteoarthrosis, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.31', 'L3675', 'Localized osteoarthrosis not specified whether primary or secondary, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.91', 'L3675', 'Osteoarthrosis, unspecified whether generalized or localized, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.91', 'L3675', 'Unspecified arthropathy, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.99', 'L3675', 'Unspecified arthropathy, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718', 'L3675', 'Articular cartilage disorder, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.01', 'L3675', 'Articular cartilage disorder, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.11', 'L3675', 'Loose body in shoulder joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.12', 'L3675', 'Loose body in upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.18', 'L3675', 'Loose body in joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.21', 'L3675', 'Pathological dislocation of shoulder joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.31', 'L3675', 'Recurrent dislocation of shoulder joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.81', 'L3675', 'Other joint derangement, not elsewhere classified, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.82', 'L3675', 'Other joint derangement, not elsewhere classified, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.83', 'L3675', 'Other joint derangement, not elsewhere classified, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.91', 'L3675', 'Unspecified derangement, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.01', 'L3675', 'Effusion of shoulder joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.1', 'L3675', 'Hemarthrosis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.22', 'L3675', 'Villonodular synovitis, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.4', 'L3675', 'Pain in joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.41', 'L3675', 'Pain in joint, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.42', 'L3675', 'Pain in joint, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.43', 'L3675', 'Pain in joint, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.48', 'L3675', 'Pain in joint, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.49', 'L3675', 'Pain in joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.63', 'L3675', 'Other symptoms referable to forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.81', 'L3675', 'Other specified disorders of shoulder joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.91', 'L3675', 'Unspecified disorder of shoulder joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.92', 'L3675', 'Unspecified disorder of upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('723.4', 'L3675', 'Brachial neuritis or radiculitis nos');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726', 'L3675', 'Adhesive capsulitis of shoulder');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.1', 'L3675', 'Unspecified disorders of bursae and tendons in shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.11', 'L3675', 'Calcifying tendinitis of shoulder');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.12', 'L3675', 'Bicipital tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.19', 'L3675', 'Other specified disorders of rotator cuff syndrome of shoulder and allied disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.2', 'L3675', 'Other affections of shoulder region, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.31', 'L3675', 'Medial epicondylitis of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.32', 'L3675', 'Lateral epicondylitis of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.33', 'L3675', 'Olecranon bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.39', 'L3675', 'Other enthesopathy of elbow region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.4', 'L3675', 'Enthesopathy of wrist and carpus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L3675', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727', 'L3675', 'Unspecified synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L3675', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.2', 'L3675', 'Specific bursitides often of occupational origin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.3', 'L3675', 'Other bursitis disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.41', 'L3675', 'Ganglion of joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.43', 'L3675', 'Unspecified ganglion');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.61', 'L3675', 'Complete rupture of rotator cuff');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.62', 'L3675', 'Nontraumatic rupture of tendons of biceps (long head)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.2', 'L3675', 'Muscular wasting and disuse atrophy, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.5', 'L3675', 'Hypermobility syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.6', 'L3675', 'Contracture of palmar fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.71', 'L3675', 'Plantar fascial fibromatosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.83', 'L3675', 'Rupture of muscle, nontraumatic');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.85', 'L3675', 'Spasm of muscle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729', 'L3675', 'Rheumatism, unspecified and fibrositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.1', 'L3675', 'Unspecified myalgia and myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.2', 'L3675', 'Unspecified neuralgia, neuritis, and radiculitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L3675', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.81', 'L3675', 'Swelling of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.9', 'L3675', 'Other and unspecified disorders of soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.01', 'L3675', 'Acute osteomyelitis, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.11', 'L3675', 'Chronic osteomyelitis, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.3', 'L3675', 'Juvenile osteochondrosis of upper extremity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733', 'L3675', 'Unspecified osteoporosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.11', 'L3675', 'Pathologic fracture of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.19', 'L3675', 'Pathologic fracture of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.82', 'L3675', 'Nonunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.9', 'L3675', 'Disorder of bone and cartilage, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.89', 'L3675', 'Other acquired deformity of other parts of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('737.1', 'L3675', 'Kyphosis (acquired) (postural)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.59', 'L3675', 'Other congenital anomaly of upper limb, including shoulder girdle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.5', 'L3675', 'Unspecified congenital osteodystrophy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('757.39', 'L3675', 'Other specified congenital anomaly of skin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('805', 'L3675', 'Closed fracture of cervical vertebra, unspecified level without mention of spinal cord injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('805.6', 'L3675', 'Closed fracture of sacrum and coccyx without mention of spinal cord injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('807.01', 'L3675', 'Closed fracture of one rib');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('807.03', 'L3675', 'Closed fracture of three ribs');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('807.09', 'L3675', 'Closed fracture of multiple ribs, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('810', 'L3675', 'Unspecified part of closed fracture of clavicle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('810.01', 'L3675', 'Closed fracture of sternal end of clavicle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('810.02', 'L3675', 'Closed fracture of shaft of clavicle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('810.03', 'L3675', 'Closed fracture of acromial end of clavicle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('810.1', 'L3675', 'Unspecified part of open fracture of clavicle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('811', 'L3675', 'Closed fracture of unspecified part of scapula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('811.01', 'L3675', 'Closed fracture of acromial process of scapula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('811.03', 'L3675', 'Closed fracture of glenoid cavity and neck of scapula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('811.09', 'L3675', 'Closed fracture of other part of scapula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812', 'L3675', 'Closed fracture of unspecified part of upper end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.01', 'L3675', 'Closed fracture of surgical neck of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.02', 'L3675', 'Closed fracture of anatomical neck of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.03', 'L3675', 'Closed fracture of greater tuberosity of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.09', 'L3675', 'Other closed fractures of upper end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.1', 'L3675', 'Open fracture of unspecified part of upper end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.2', 'L3675', 'Closed fracture of unspecified part of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.21', 'L3675', 'Closed fracture of shaft of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.4', 'L3675', 'Closed fracture of unspecified part of lower end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.41', 'L3675', 'Closed fracture of supracondylar humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.42', 'L3675', 'Closed fracture of lateral condyle of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.43', 'L3675', 'Closed fracture of medial condyle of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.44', 'L3675', 'Closed fracture of unspecified condyle(s) of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.49', 'L3675', 'Other closed fracture of lower end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.54', 'L3675', 'Open fracture of unspecified condyle(s) of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813', 'L3675', 'Unspecified fracture of radius and ulna, upper end of forearm, closed');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.01', 'L3675', 'Closed fracture of olecranon process of ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.02', 'L3675', 'Closed fracture of coronoid process of ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.04', 'L3675', 'Other and unspecified closed fractures of proximal end of ulna (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.05', 'L3675', 'Closed fracture of head of radius');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.06', 'L3675', 'Closed fracture of neck of radius');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.07', 'L3675', 'Other and unspecified closed fractures of proximal end of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.08', 'L3675', 'Closed fracture of radius with ulna, upper end (any part)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.15', 'L3675', 'Open fracture of head of radius');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831', 'L3675', 'Closed dislocation of shoulder, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.01', 'L3675', 'Closed anterior dislocation of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.02', 'L3675', 'Closed posterior dislocation of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.03', 'L3675', 'Closed inferior dislocation of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.04', 'L3675', 'Closed dislocation of acromioclavicular (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.09', 'L3675', 'Closed dislocation of other site of shoulder');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.1', 'L3675', 'Open unspecified dislocation of shoulder');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.14', 'L3675', 'Open dislocation of acromioclavicular (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832', 'L3675', 'Closed unspecified dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832.02', 'L3675', 'Closed posterior dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832.09', 'L3675', 'Closed dislocation of other site of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('839.61', 'L3675', 'Closed dislocation, sternum');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.41', 'L3675', 'Sprain and strain of sternoclavicular (joint) (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.8', 'L3675', 'Other specified sites of sprains and strains');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.9', 'L3675', 'Unspecified site of sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('880', 'L3675', 'Open wound of shoulder region, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('911', 'L3675', 'Trunk abrasion or friction burn, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('912', 'L3675', 'Shoulder and upper arm, abrasion or friction burn, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('912.8', 'L3675', 'Other and unspecified superficial injury of shoulder and upper arm, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('913', 'L3675', 'Elbow, forearm, and wrist, abrasion or friction burn, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923', 'L3675', 'Contusion of shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.01', 'L3675', 'Contusion of scapular region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.03', 'L3675', 'Contusion of upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.09', 'L3675', 'Contusion of multiple sites of shoulder and upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.1', 'L3675', 'Contusion of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.11', 'L3675', 'Contusion of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.8', 'L3675', 'Contusion of multiple sites, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.9', 'L3675', 'Contusion of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('929.9', 'L3675', 'Crushing injury of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('949', 'L3675', 'Burn of unspecified site, unspecified degree');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('955.7', 'L3675', 'Injury to other specified nerve(s) of shoulder girdle and upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('955.9', 'L3675', 'Injury to unspecified nerve of shoulder girdle and upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.19', 'L3675', 'Other injury of other sites of trunk');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.2', 'L3675', 'Injury, other and unspecified, shoulder and upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.8', 'L3675', 'Injury, other and unspecified, other specified sites, including multiple');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.9', 'L3675', 'Injury, other and unspecified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.67', 'L3675', 'Infection and inflammatory reaction due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.59', 'L3675', 'Other immobilization, pressure, and attention to wound');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.23', 'L3675', 'Fitting of orthotic device');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('214.9', 'L3760', 'Lipoma of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('238.8', 'L3760', 'Neoplasm of uncertain behavior of other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('342.1', 'L3760', 'Spastic hemiplegia affecting unspecified side');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('353', 'L3760', 'Brachial plexus lesions');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354', 'L3760', 'Carpal tunnel syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.8', 'L3760', 'Other mononeuritis of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.9', 'L3760', 'Unspecified mononeuritis of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('359', 'L3760', 'Congenital hereditary muscular dystrophy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L3760', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L3760', 'Generalized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.14', 'L3760', 'Primary localized osteoarthrosis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.93', 'L3760', 'Osteoarthrosis, unspecified whether generalized or localized, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.32', 'L3760', 'Climacteric arthritis, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.9', 'L3760', 'Unspecified arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.93', 'L3760', 'Unspecified arthropathy, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.12', 'L3760', 'Loose body in upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.4', 'L3760', 'Contracture of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.42', 'L3760', 'Contracture of upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.43', 'L3760', 'Contracture of forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.8', 'L3760', 'Other joint derangement, not elsewhere classified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.82', 'L3760', 'Other joint derangement, not elsewhere classified, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.92', 'L3760', 'Unspecified derangement, upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.02', 'L3760', 'Effusion of upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.22', 'L3760', 'Villonodular synovitis, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.42', 'L3760', 'Pain in joint, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.43', 'L3760', 'Pain in joint, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.48', 'L3760', 'Pain in joint, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.88', 'L3760', 'Other specified disorders of joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.93', 'L3760', 'Unspecified disorder of forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.12', 'L3760', 'Bicipital tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.2', 'L3760', 'Other affections of shoulder region, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.3', 'L3760', 'Unspecified enthesopathy of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.31', 'L3760', 'Medial epicondylitis of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.32', 'L3760', 'Lateral epicondylitis of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.33', 'L3760', 'Olecranon bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.39', 'L3760', 'Other enthesopathy of elbow region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.4', 'L3760', 'Enthesopathy of wrist and carpus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L3760', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.2', 'L3760', 'Specific bursitides often of occupational origin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.3', 'L3760', 'Other bursitis disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.43', 'L3760', 'Unspecified ganglion');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.62', 'L3760', 'Nontraumatic rupture of tendons of biceps (long head)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.81', 'L3760', 'Contracture of tendon (sheath)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.82', 'L3760', 'Calcium deposits in tendon and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.71', 'L3760', 'Plantar fascial fibromatosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.85', 'L3760', 'Spasm of muscle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729', 'L3760', 'Rheumatism, unspecified and fibrositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.1', 'L3760', 'Unspecified myalgia and myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.2', 'L3760', 'Unspecified neuralgia, neuritis, and radiculitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.4', 'L3760', 'Unspecified fasciitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L3760', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.81', 'L3760', 'Malunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.82', 'L3760', 'Nonunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.99', 'L3760', 'Other disorders of bone and cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812', 'L3760', 'Closed fracture of unspecified part of upper end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.4', 'L3760', 'Closed fracture of unspecified part of lower end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.41', 'L3760', 'Closed fracture of supracondylar humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.42', 'L3760', 'Closed fracture of lateral condyle of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.43', 'L3760', 'Closed fracture of medial condyle of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.44', 'L3760', 'Closed fracture of unspecified condyle(s) of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.49', 'L3760', 'Other closed fracture of lower end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813', 'L3760', 'Unspecified fracture of radius and ulna, upper end of forearm, closed');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.01', 'L3760', 'Closed fracture of olecranon process of ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.02', 'L3760', 'Closed fracture of coronoid process of ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.03', 'L3760', 'Closed Monteggia''s fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.04', 'L3760', 'Other and unspecified closed fractures of proximal end of ulna (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.05', 'L3760', 'Closed fracture of head of radius');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.06', 'L3760', 'Closed fracture of neck of radius');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.11', 'L3760', 'Open fracture of olecranon process of ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.12', 'L3760', 'Open fracture of coronoid process of ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.17', 'L3760', 'Other and unspecified open fractures of proximal end of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832', 'L3760', 'Closed unspecified dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832.02', 'L3760', 'Closed posterior dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.8', 'L3760', 'Other specified sites of sprains and strains');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.9', 'L3760', 'Unspecified site of sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('880.03', 'L3760', 'Open wound of upper arm, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('881.01', 'L3760', 'Open wound of elbow, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('881.11', 'L3760', 'Open wound of elbow, complicated');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('905.7', 'L3760', 'Late effect of sprain and strain without mention of tendon injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.1', 'L3760', 'Contusion of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.11', 'L3760', 'Contusion of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('927.8', 'L3760', 'Crushing injury of multiple sites of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('953', 'L3760', 'Injury to cervical nerve root');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('955.1', 'L3760', 'Injury to median nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('955.2', 'L3760', 'Injury to ulnar nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('955.3', 'L3760', 'Injury to radial nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.2', 'L3760', 'Injury, other and unspecified, shoulder and upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.3', 'L3760', 'Injury, other and unspecified, elbow, forearm, and wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.9', 'L3760', 'Injury, other and unspecified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.23', 'L3760', 'Fitting of orthotic device');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('214.1', 'L3701', 'Lipoma of other skin and subcutaneous tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('274', 'L3701', 'Gouty arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('289.3', 'L3701', 'Lymphadenitis, unspecified, except mesenteric');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('332', 'L3701', 'Paralysis agitans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('333.5', 'L3701', 'Other choreas');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('333.83', 'L3701', 'Spasmodic torticollis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('337.9', 'L3701', 'Unspecified disorder of autonomic nervous system');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('344.9', 'L3701', 'Unspecified paralysis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('353', 'L3701', 'Brachial plexus lesions');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('353.8', 'L3701', 'Other nerve root and plexus disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.8', 'L3701', 'Other mononeuritis of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.9', 'L3701', 'Unspecified mononeuritis of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('356.9', 'L3701', 'Unspecified hereditary and idiopathic peripheral neuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('357.9', 'L3701', 'Unspecified inflammatory and toxic neuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('686.8', 'L3701', 'Other specified local infections of skin and subcutaneous tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('710', 'L3701', 'Systemic lupus erythematosus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('711.92', 'L3701', 'Unspecified infective arthritis, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L3701', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.9', 'L3701', 'Unspecified inflammatory polyarthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L3701', 'Generalized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.09', 'L3701', 'Generalized osteoarthrosis, involving multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.12', 'L3701', 'Primary localized osteoarthrosis, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.13', 'L3701', 'Primary localized osteoarthrosis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.32', 'L3701', 'Localized osteoarthrosis not specified whether primary or secondary, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.33', 'L3701', 'Localized osteoarthrosis not specified whether primary or secondary, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.92', 'L3701', 'Osteoarthrosis, unspecified whether generalized or localized, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.12', 'L3701', 'Traumatic arthropathy, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.18', 'L3701', 'Traumatic arthropathy, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.31', 'L3701', 'Climacteric arthritis, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.32', 'L3701', 'Climacteric arthritis, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.91', 'L3701', 'Unspecified arthropathy, shoulder region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.92', 'L3701', 'Unspecified arthropathy, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.93', 'L3701', 'Unspecified arthropathy, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.99', 'L3701', 'Unspecified arthropathy, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.03', 'L3701', 'Articular cartilage disorder, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.09', 'L3701', 'Articular cartilage disorder, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.12', 'L3701', 'Loose body in upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.28', 'L3701', 'Pathological dislocation of joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.31', 'L3701', 'Recurrent dislocation of shoulder joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.32', 'L3701', 'Recurrent dislocation of upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.33', 'L3701', 'Recurrent dislocation of forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.42', 'L3701', 'Contracture of upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.53', 'L3701', 'Ankylosis of forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.82', 'L3701', 'Other joint derangement, not elsewhere classified, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.83', 'L3701', 'Other joint derangement, not elsewhere classified, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.93', 'L3701', 'Unspecified derangement, forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.02', 'L3701', 'Effusion of upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.03', 'L3701', 'Effusion of forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.12', 'L3701', 'Hemarthorsis, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.22', 'L3701', 'Villonodular synovitis, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.23', 'L3701', 'Villonodular synovitis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.92', 'L3701', 'Unspecified disorder of upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.12', 'L3701', 'Bicipital tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.3', 'L3701', 'Unspecified enthesopathy of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.31', 'L3701', 'Medial epicondylitis of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.32', 'L3701', 'Lateral epicondylitis of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.33', 'L3701', 'Olecranon bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.39', 'L3701', 'Other enthesopathy of elbow region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L3701', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727', 'L3701', 'Unspecified synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.02', 'L3701', 'Giant cell tumor of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L3701', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.2', 'L3701', 'Specific bursitides often of occupational origin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.3', 'L3701', 'Other bursitis disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.41', 'L3701', 'Ganglion of joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.42', 'L3701', 'Ganglion of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.43', 'L3701', 'Unspecified ganglion');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.49', 'L3701', 'Other ganglion and cyst of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.51', 'L3701', 'Synovial cyst of popliteal space');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.62', 'L3701', 'Nontraumatic rupture of tendons of biceps (long head)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.82', 'L3701', 'Calcium deposits in tendon and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.89', 'L3701', 'Other disorders of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.3', 'L3701', 'Other specific muscle disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.4', 'L3701', 'Laxity of ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.6', 'L3701', 'Contracture of palmar fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.71', 'L3701', 'Plantar fascial fibromatosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.85', 'L3701', 'Spasm of muscle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.89', 'L3701', 'Other disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.9', 'L3701', 'Unspecified disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729', 'L3701', 'Rheumatism, unspecified and fibrositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.1', 'L3701', 'Unspecified myalgia and myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.2', 'L3701', 'Unspecified neuralgia, neuritis, and radiculitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L3701', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.6', 'L3701', 'Residual foreign body in soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.89', 'L3701', 'Other musculoskeletal symptoms referable to limbs');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.27', 'L3701', 'Unspecified osteomyelitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('731.2', 'L3701', 'Hypertrophic pulmonary osteoarthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.3', 'L3701', 'Juvenile osteochondrosis of upper extremity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.9', 'L3701', 'Unspecified osteochondropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733', 'L3701', 'Unspecified osteoporosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.1', 'L3701', 'Pathologic fracture, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.82', 'L3701', 'Nonunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.9', 'L3701', 'Disorder of bone and cartilage, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.91', 'L3701', 'Arrest of bone development or growth');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736', 'L3701', 'Unspecified deformity of forearm, excluding fingers');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.4', 'L3701', 'Closed fracture of unspecified part of lower end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.41', 'L3701', 'Closed fracture of supracondylar humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.42', 'L3701', 'Closed fracture of lateral condyle of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.43', 'L3701', 'Closed fracture of medial condyle of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.44', 'L3701', 'Closed fracture of unspecified condyle(s) of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.49', 'L3701', 'Other closed fracture of lower end of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('812.54', 'L3701', 'Open fracture of unspecified condyle(s) of humerus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813', 'L3701', 'Unspecified fracture of radius and ulna, upper end of forearm, closed');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.01', 'L3701', 'Closed fracture of olecranon process of ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.02', 'L3701', 'Closed fracture of coronoid process of ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.05', 'L3701', 'Closed fracture of head of radius');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.06', 'L3701', 'Closed fracture of neck of radius');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.07', 'L3701', 'Other and unspecified closed fractures of proximal end of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.2', 'L3701', 'Unspecified closed fracture of shaft of radius or ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.21', 'L3701', 'Closed fracture of shaft of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('831.04', 'L3701', 'Closed dislocation of acromioclavicular (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832', 'L3701', 'Closed unspecified dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832.01', 'L3701', 'Closed anterior dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832.02', 'L3701', 'Closed posterior dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832.03', 'L3701', 'Closed medial dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832.09', 'L3701', 'Closed dislocation of other site of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832.1', 'L3701', 'Open unspecified dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('832.14', 'L3701', 'Open lateral dislocation of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('881', 'L3701', 'Open wound of forearm, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('881.01', 'L3701', 'Open wound of elbow, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('881.1', 'L3701', 'Open wound of forearm, complicated');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('881.2', 'L3701', 'Open wound of forearm, with tendon involvement');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('881.21', 'L3701', 'Open wound of elbow, with tendon involvement');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('905.7', 'L3701', 'Late effect of sprain and strain without mention of tendon injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('913', 'L3701', 'Elbow, forearm, and wrist, abrasion or friction burn, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('919.8', 'L3701', 'Other and unspecified superficial injury of other, multiple, and unspecified sites, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.03', 'L3701', 'Contusion of upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.09', 'L3701', 'Contusion of multiple sites of shoulder and upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.1', 'L3701', 'Contusion of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.11', 'L3701', 'Contusion of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.8', 'L3701', 'Contusion of multiple sites, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('927.1', 'L3701', 'Crushing injury of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('949.2', 'L3701', 'Blisters with epidermal loss due to burn (second degree), unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('955.1', 'L3701', 'Injury to median nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('955.2', 'L3701', 'Injury to ulnar nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('955.3', 'L3701', 'Injury to radial nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('957.1', 'L3701', 'Injury to other specified nerve(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('958.3', 'L3701', 'Posttraumatic wound infection not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.2', 'L3701', 'Injury, other and unspecified, shoulder and upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.3', 'L3701', 'Injury, other and unspecified, elbow, forearm, and wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.8', 'L3701', 'Injury, other and unspecified, other specified sites, including multiple');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.9', 'L3701', 'Injury, other and unspecified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.7', 'L3701', 'Other complications due to unspecified device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.78', 'L3701', 'Other complications due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('998.59', 'L3701', 'Other postoperative infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250', 'L3908', 'Diabetes mellitus without mention of complication, type II or unspecified type, not stated as   uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.01', 'L3908', 'Diabetes mellitus without mention of complication, type I [juvenile type], not stated as  uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.02', 'L3908', 'Diabetes mellitus without mention of complication, type II or unspecified type, uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.03', 'L3908', 'Diabetes mellitus without mention of complication, type I [juvenile type], uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.4', 'L3908', 'Diabetes with renal manifestations, type II or unspecified type, not stated as uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.5', 'L3908', 'Diabetes with ophthalmic manifestations, type II or unspecified type, not stated as uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.62', 'L3908', 'Diabetes with neurological manifestations, type II or unspecified type, uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('274', 'L3908', 'Gouty arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('337', 'L3908', 'Idiopathic peripheral autonomic neuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('337.21', 'L3908', 'Reflex sympathetic dystrophy of the upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('337.9', 'L3908', 'Unspecified disorder of autonomic nervous system');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('341.9', 'L3908', 'Unspecified demyelinating disease of central nervous system');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354', 'L3908', 'Carpal tunnel syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.1', 'L3908', 'Other lesion of median nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.2', 'L3908', 'Lesion of ulnar nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.3', 'L3908', 'Lesion of radial nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.4', 'L3908', 'Causalgia of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.5', 'L3908', 'Mononeuritis multiplex');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.8', 'L3908', 'Other mononeuritis of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.9', 'L3908', 'Unspecified mononeuritis of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.9', 'L3908', 'Mononeuritis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('356.4', 'L3908', 'Idiopathic progressive polyneuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('356.8', 'L3908', 'Other specified idiopathic peripheral neuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('356.9', 'L3908', 'Unspecified hereditary and idiopathic peripheral neuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('357.4', 'L3908', 'Polyneuropathy in other diseases classified elsewhere');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('357.9', 'L3908', 'Unspecified inflammatory and toxic neuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('453.8', 'L3908', 'Embolism and thrombosis of other specified veins');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('453.9', 'L3908', 'Embolism and thrombosis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('459.81', 'L3908', 'Unspecified venous (peripheral) insufficiency');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('459.89', 'L3908', 'Other specified circulatory system disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('711.03', 'L3908', 'Pyogenic arthritis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('711.04', 'L3908', 'Pyogenic arthritis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('711.94', 'L3908', 'Unspecified infective arthritis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('712.93', 'L3908', 'Unspecified crystal arthropathy, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('713.2', 'L3908', 'Arthropathy associated with hematological disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L3908', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.3', 'L3908', 'Polyarticular juvenile rheumatoid arthritis, chronic or unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.32', 'L3908', 'Pauciarticular juvenile rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.89', 'L3908', 'Other specified inflammatory polyarthropathies');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.9', 'L3908', 'Unspecified inflammatory polyarthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L3908', 'Generalized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.04', 'L3908', 'Generalized osteoarthrosis, involving hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.09', 'L3908', 'Generalized osteoarthrosis, involving multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.1', 'L3908', 'Primary localized osteoarthrosis, specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.13', 'L3908', 'Primary localized osteoarthrosis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.14', 'L3908', 'Primary localized osteoarthrosis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.18', 'L3908', 'Primary localized osteoarthrosis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.2', 'L3908', 'Secondary localized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.23', 'L3908', 'Secondary localized osteoarthrosis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.24', 'L3908', 'Secondary localized osteoarthrosis, involving hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.33', 'L3908', 'Localized osteoarthrosis not specified whether primary or secondary, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.34', 'L3908', 'Localized osteoarthrosis not specified whether primary or secondary, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.89', 'L3908', 'Osteoarthrosis involving multiple sites, but not specified as generalized');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.9', 'L3908', 'Osteoarthrosis, unspecified whether generalized or localized, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.93', 'L3908', 'Osteoarthrosis, unspecified whether generalized or localized, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.94', 'L3908', 'Osteoarthrosis, unspecified whether generalized or localized, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.03', 'L3908', 'Kaschin-Beck disease, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.04', 'L3908', 'Kaschin-Beck disease, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.12', 'L3908', 'Traumatic arthropathy, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.13', 'L3908', 'Traumatic arthropathy, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.14', 'L3908', 'Traumatic arthropathy, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.23', 'L3908', 'Allergic arthritis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.24', 'L3908', 'Allergic arthritis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.29', 'L3908', 'Allergic arthritis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.3', 'L3908', 'Climacteric arthritis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.33', 'L3908', 'Climacteric arthritis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.34', 'L3908', 'Climacteric arthritis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.39', 'L3908', 'Climacteric arthritis involving multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.4', 'L3908', 'Transient arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.43', 'L3908', 'Transient arthropathy, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.44', 'L3908', 'Transient arthropathy, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.48', 'L3908', 'Transient arthropathy, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.49', 'L3908', 'Transient arthropathy, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.5', 'L3908', 'Unspecified polyarthropathy or polyarthritis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.53', 'L3908', 'Unspecified polyarthropathy or polyarthritis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.54', 'L3908', 'Unspecified polyarthropathy or polyarthritis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.58', 'L3908', 'Unspecified polyarthropathy or polyarthritis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.59', 'L3908', 'Unspecified polyarthropathy or polyarthritis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.63', 'L3908', 'Unspecified monoarthritis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.64', 'L3908', 'Unspecified monoarthritis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.68', 'L3908', 'Unspecified monoarthritis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.8', 'L3908', 'Other specified arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.83', 'L3908', 'Other specified arthropathy, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.84', 'L3908', 'Other specified arthropathy, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.88', 'L3908', 'Other specified arthropathy, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.89', 'L3908', 'Other specified arthropathy, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.92', 'L3908', 'Unspecified arthropathy, upper arm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.93', 'L3908', 'Unspecified arthropathy, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.94', 'L3908', 'Unspecified arthopathy, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.99', 'L3908', 'Unspecified arthropathy, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.03', 'L3908', 'Articular cartilage disorder, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.13', 'L3908', 'Loose body in forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.24', 'L3908', 'Pathological dislocation of hand joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.33', 'L3908', 'Recurrent dislocation of forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.42', 'L3908', 'Contracture of upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.43', 'L3908', 'Contracture of forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.44', 'L3908', 'Contracture of hand joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.48', 'L3908', 'Contracture of joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.83', 'L3908', 'Other joint derangement, not elsewhere classified, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.84', 'L3908', 'Other joint derangement, not elsewhere classified, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.94', 'L3908', 'Unspecified derangement of hand joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.99', 'L3908', 'Unspecified derangement of joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.02', 'L3908', 'Effusion of upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.03', 'L3908', 'Effusion of forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.04', 'L3908', 'Effusion of hand joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.13', 'L3908', 'Hemarthrosis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.14', 'L3908', 'Hemarthrosis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.19', 'L3908', 'Hemarthrosis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.2', 'L3908', 'Villonodular synovitis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.23', 'L3908', 'Villonodular synovitis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.24', 'L3908', 'Villonodular synovitis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.28', 'L3908', 'Villonodular synovitis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.29', 'L3908', 'Villonodular synovitis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.33', 'L3908', 'Palindromic rheumatism, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.34', 'L3908', 'Palindromic rheumatism, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.39', 'L3908', 'Palindromic rheumatism, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.43', 'L3908', 'Pain in joint, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.44', 'L3908', 'Pain in joint, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.48', 'L3908', 'Pain in joint, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.49', 'L3908', 'Pain in joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.62', 'L3908', 'Other symptoms referable to upper arm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.63', 'L3908', 'Other symptoms referable to forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.64', 'L3908', 'Other symptoms referable to hand joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.68', 'L3908', 'Other symptoms referable to joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.69', 'L3908', 'Other symptoms referable to joints of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.83', 'L3908', 'Other specified disorders of forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.84', 'L3908', 'Other specified disorders of hand joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.88', 'L3908', 'Other specified disorders of joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.89', 'L3908', 'Other specified disorders of joints of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.93', 'L3908', 'Unspecified disorder of forearm joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.94', 'L3908', 'Unspecified disorder of hand joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.98', 'L3908', 'Unspecified joint disorder of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.99', 'L3908', 'Unspecified joint disorder of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('723.4', 'L3908', 'Brachial neuritis or radiculitis nos');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.4', 'L3908', 'Enthesopathy of wrist and carpus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.5', 'L3908', 'Enthesopathy of hip region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L3908', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.91', 'L3908', 'Exostosis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727', 'L3908', 'Unspecified synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.01', 'L3908', 'Synovitis and tenosynovitis in diseases classified elsewhere');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.02', 'L3908', 'Giant cell tumor of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.03', 'L3908', 'Trigger finger (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.04', 'L3908', 'Radial styloid tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.05', 'L3908', 'Other tenosynovitis of hand and wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.06', 'L3908', 'Tenosynovitis of foot and ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L3908', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.4', 'L3908', 'Unspecified synovial cyst');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.41', 'L3908', 'Ganglion of joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.42', 'L3908', 'Ganglion of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.49', 'L3908', 'Other ganglion and cyst of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.5', 'L3908', 'Unspecified rupture of synovium');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.51', 'L3908', 'Synovial cyst of popliteal space');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.59', 'L3908', 'Other rupture of synovium');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.6', 'L3908', 'Nontraumatic rupture of unspecified tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.63', 'L3908', 'Nontraumatic rupture of extensor tendons of hand and wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.64', 'L3908', 'Nontraumatic rupture of flexor tendons of hand and wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.65', 'L3908', 'Nontraumatic rupture of quadriceps tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.81', 'L3908', 'Contracture of tendon (sheath)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.82', 'L3908', 'Calcium deposits in tendon and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.4', 'L3908', 'Laxity of ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.6', 'L3908', 'Contracture of palmar fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.71', 'L3908', 'Plantar fascial fibromatosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.81', 'L3908', 'Interstitial myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.85', 'L3908', 'Spasm of muscle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.9', 'L3908', 'Unspecified disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729', 'L3908', 'Rheumatism, unspecified and fibrositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.1', 'L3908', 'Unspecified myalgia and myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.2', 'L3908', 'Unspecified neuralgia, neuritis, and radiculitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.23', 'L3908', 'Unspecified osteomyelitis, forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.24', 'L3908', 'Unspecified osteomyelitis, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.3', 'L3908', 'Juvenile osteochondrosis of upper extremity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.7', 'L3908', 'Osteochondritis dissecans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.8', 'L3908', 'Other specified forms of osteochondropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.12', 'L3908', 'Pathologic fracture of distal radius and ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.81', 'L3908', 'Malunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.82', 'L3908', 'Nonunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.9', 'L3908', 'Disorder of bone and cartilage, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.05', 'L3908', 'Wrist drop (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.06', 'L3908', 'Claw hand (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.07', 'L3908', 'Club hand, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.09', 'L3908', 'Other acquired deformities of forearm, excluding fingers');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.1', 'L3908', 'Mallet finger');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.2', 'L3908', 'Unspecified deformity of finger');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.2', 'L3908', 'Congenital musculoskeletal deformity of spine');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.5', 'L3908', 'Unspecified congenital anomaly of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.54', 'L3908', 'Madelung''s deformity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.56', 'L3908', 'Accessory carpal bones');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.59', 'L3908', 'Other congenital anomaly of upper limb, including shoulder girdle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.4', 'L3908', 'Unspecified closed fracture of lower end of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.41', 'L3908', 'Closed Colles'' fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.42', 'L3908', 'Other closed fractures of distal end of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.43', 'L3908', 'Closed fracture of distal end of ulna (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.44', 'L3908', 'Closed fracture of lower end of radius with ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.45', 'L3908', 'Torus fracture of lower end of radius');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.5', 'L3908', 'Unspecified open fracture of lower end of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.51', 'L3908', 'Open Colles'' fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.52', 'L3908', 'Other open fractures of distal end of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.53', 'L3908', 'Open fracture of distal end of ulna (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.54', 'L3908', 'Open fracture of lower end of radius with ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.8', 'L3908', 'Closed fracture of unspecified part of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.81', 'L3908', 'Closed fracture of unspecified part of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.82', 'L3908', 'Closed fracture of unspecified part of ulna (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.83', 'L3908', 'Closed fracture of unspecified part of radius with ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.9', 'L3908', 'Open fracture of unspecified part of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.91', 'L3908', 'Open fracture of unspecified part of radius (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.92', 'L3908', 'Open fracture of unspecified part of ulna (alone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.93', 'L3908', 'Open fracture of unspecified part of radius with ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814', 'L3908', 'Unspecified closed fracture of carpal bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.01', 'L3908', 'Closed fracture of navicular (scaphoid) bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.02', 'L3908', 'Closed fracture of lunate (semilunar) bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.03', 'L3908', 'Closed fracture of triquetral (cuneiform) bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.04', 'L3908', 'Closed fracture of pisiform bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.05', 'L3908', 'Closed fracture of trapezium bone (larger multangular) of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.06', 'L3908', 'Closed fracture of trapezoid bone (smaller multangular) of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.07', 'L3908', 'Closed fracture of capitate bone (os magnum) of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.08', 'L3908', 'Closed fracture of hamate (unciform) bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.09', 'L3908', 'Closed fracture of other bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.1', 'L3908', 'Unspecified open fracture of carpal bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.11', 'L3908', 'Open fracture of navicular (scaphoid) bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('814.13', 'L3908', 'Open fracture of triquetral (cuneiform) bone of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815', 'L3908', 'Closed fracture of metacarpal bone(s), site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.01', 'L3908', 'Closed fracture of base of thumb (first) metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.02', 'L3908', 'Closed fracture of base of other metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.03', 'L3908', 'Closed fracture of shaft of metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.04', 'L3908', 'Closed fracture of neck of metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.09', 'L3908', 'Closed fracture of multiple sites of metacarpus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.1', 'L3908', 'Open fracture of metacarpal bone(s), site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.11', 'L3908', 'Open fracture of base of thumb (first) metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.12', 'L3908', 'Open fracture of base of other metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.13', 'L3908', 'Open fracture of shaft of metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.14', 'L3908', 'Open fracture of neck of metacarpal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('815.19', 'L3908', 'Open fracture of multiple sites of metacarpus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('816', 'L3908', 'Closed fracture of unspecified phalanx or phalanges of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('816.01', 'L3908', 'Closed fracture of middle or proximal phalanx or phalanges of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('816.02', 'L3908', 'Closed fracture of distal phalanx or phalanges of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('816.03', 'L3908', 'Closed fracture of multiple sites of phalanx or phalanges of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('816.1', 'L3908', 'Open fracture of phalanx or phalanges of hand, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('816.11', 'L3908', 'Open fracture of middle or proximal phalanx or phalanges of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('816.12', 'L3908', 'Open fracture of distal phalanx or phalanges of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('817', 'L3908', 'Multiple closed fractures of hand bones');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833', 'L3908', 'Closed dislocation of wrist, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.01', 'L3908', 'Closed dislocation of distal radioulnar (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.03', 'L3908', 'Closed dislocation of midcarpal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.04', 'L3908', 'Closed dislocation of carpometacarpal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.05', 'L3908', 'Closed dislocation of proximal end of metacarpal (bone)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.09', 'L3908', 'Closed dislocation of other part of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('833.14', 'L3908', 'Open dislocation of carpometacarpal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('834', 'L3908', 'Closed dislocation of finger, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('834.01', 'L3908', 'Closed dislocation of metacarpophalangeal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('834.02', 'L3908', 'Closed dislocation of interphalangeal (joint), hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('842', 'L3908', 'Sprain and strain of unspecified site of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('842.01', 'L3908', 'Sprain and strain of carpal (joint) of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('881.02', 'L3908', 'Open wound of wrist, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('881.1', 'L3908', 'Open wound of forearm, complicated');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('881.12', 'L3908', 'Open wound of wrist, complicated');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('881.2', 'L3908', 'Open wound of forearm, with tendon involvement');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('881.22', 'L3908', 'Open wound of wrist, with tendon involvement');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('882', 'L3908', 'Open wound of hand except finger(s) alone, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('883', 'L3908', 'Open wound of finger(s), without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('893', 'L3908', 'Open wound of toe(s), without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('903.8', 'L3908', 'Injury to specified blood vessels of upper extremity, other');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('904.8', 'L3908', 'Injury to unspecified blood vessel of lower extremity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('905.8', 'L3908', 'Late effect of tendon injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('912', 'L3908', 'Shoulder and upper arm, abrasion or friction burn, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('913.8', 'L3908', 'Other and unspecified superficial injury of elbow, forearm, and wrist, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('914', 'L3908', 'Hand(s) except finger(s) alone, abrasion or friction burn, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.2', 'L3908', 'Contusion of hand(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.21', 'L3908', 'Contusion of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.3', 'L3908', 'Contusion of finger');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.8', 'L3908', 'Contusion of multiple sites of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.9', 'L3908', 'Contusion of unspecified part of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('927.1', 'L3908', 'Crushing injury of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('927.2', 'L3908', 'Crushing injury of hand(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('927.21', 'L3908', 'Crushing injury of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('927.3', 'L3908', 'Crushing injury of finger(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('927.8', 'L3908', 'Crushing injury of multiple sites of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('943', 'L3908', 'Burn of unspecified degree of unspecified site of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('943.01', 'L3908', 'Burn of unspecified degree of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('943.21', 'L3908', 'Blisters with epidermal loss due to burn (second degree) of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('944', 'L3908', 'Burn of unspecified degree of unspecified site of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('944.1', 'L3908', 'Erythema due to burn (first degree) of unspecified site of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('944.2', 'L3908', 'Blisters with epidermal loss due to burn (second degree) of unspecified site of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('944.3', 'L3908', 'Full-thickness skin loss due to burn (third degree nos) of unspecified site of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('949', 'L3908', 'Burn of unspecified site, unspecified degree');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('954', 'L3908', 'Injury to cervical sympathetic nerve, excluding shoulder and pelvic girdles');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('955.1', 'L3908', 'Injury to median nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('955.2', 'L3908', 'Injury to ulnar nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('955.3', 'L3908', 'Injury to radial nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('955.6', 'L3908', 'Injury to digital nerve, upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('957.9', 'L3908', 'Injury to nerves, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('958.3', 'L3908', 'Posttraumatic wound infection not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('958.8', 'L3908', 'Other early complications of trauma');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.3', 'L3908', 'Injury, other and unspecified, elbow, forearm, and wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.4', 'L3908', 'Injury, other and unspecified, hand, except finger');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.5', 'L3908', 'Injury, other and unspecified, finger');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.8', 'L3908', 'Injury, other and unspecified, other specified sites, including multiple');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.9', 'L3908', 'Injury, other and unspecified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.59', 'L3908', 'Mechanical complication due to other implant and internal device, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.67', 'L3908', 'Infection and inflammatory reaction due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.78', 'L3908', 'Other complications due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.79', 'L3908', 'Other complications due to other internal prosthetic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('998.59', 'L3908', 'Other postoperative infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.54', 'L3908', 'Application of splint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.23', 'L3908', 'Fitting of orthotic device');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('342.9', 'L3807', 'Unspecified hemiplegia affecting unspecified side');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354', 'L3807', 'Carpal tunnel syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.44', 'L3807', 'Pain in joint, hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.32', 'L3807', 'Lateral epicondylitis of elbow');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.05', 'L3807', 'Other tenosynovitis of hand and wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.42', 'L3807', 'Ganglion of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('813.8', 'L3807', 'Closed fracture of unspecified part of forearm');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('842.12', 'L3807', 'Sprain and strain of metacarpophalangeal (joint) of hand');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('923.21', 'L3807', 'Contusion of wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.4', 'L3807', 'Injury, other and unspecified, hand, except finger');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.23', 'L3807', 'Fitting of orthotic device');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('110.1', 'L1906', 'Dermatophytosis of nail');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('110.4', 'L1906', 'Dermatophytosis of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('170.7', 'L1906', 'Malignant neoplasm of long bones of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('170.8', 'L1906', 'Malignant neoplasm of short bones of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('213.9', 'L1906', 'Benign neoplasm of bone and articular cartilage, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('215.3', 'L1906', 'Other benign neoplasm of connective and other soft tissue of lower limb, including hip');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('238', 'L1906', 'Neoplasm of uncertain behavior of bone and articular cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('238.2', 'L1906', 'Neoplasm of uncertain behavior of skin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('239.2', 'L1906', 'Neoplasms of unspecified nature of bone, soft tissue, and skin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250', 'L1906', 'Diabetes mellitus without mention of complication, type II or unspecified type, not stated as  uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.6', 'L1906', 'Diabetes with neurological manifestations, type II or unspecified type, not stated as uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.61', 'L1906', 'Diabetes with neurological manifestations, type I [juvenile type], not stated as uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.8', 'L1906', 'Diabetes with other specified manifestations, type II or unspecified type, not stated as');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('274', 'L1906', 'Gouty arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('337.22', 'L1906', 'Reflex sympathetic dystrophy of the lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.8', 'L1906', 'Other mononeuritis of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.5', 'L1906', 'Tarsal tunnel syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.6', 'L1906', 'Lesion of plantar nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.71', 'L1906', 'Causalgia of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.8', 'L1906', 'Unspecified mononeuritis of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.9', 'L1906', 'Mononeuritis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('356.9', 'L1906', 'Unspecified hereditary and idiopathic peripheral neuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('357.9', 'L1906', 'Unspecified inflammatory and toxic neuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('440.2', 'L1906', 'Atherosclerosis of native arteries of the extremities, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('443', 'L1906', 'Raynaud''s syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('454', 'L1906', 'Varicose veins of lower extremities with ulcer');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('680.9', 'L1906', 'Carbuncle and furuncle of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('681.1', 'L1906', 'Unspecified cellulitis and abscess of toe');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('681.11', 'L1906', 'Onychia and paronychia of toe');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('682.7', 'L1906', 'Cellulitis and abscess of foot, except toes');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('686.9', 'L1906', 'Unspecified local infection of skin and subcutaneous tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('696.1', 'L1906', 'Other psoriasis and similar disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707', 'L1906', 'Decubitus ulcer');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('709.4', 'L1906', 'Foreign body granuloma of skin and subcutaneous tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('711.16', 'L1906', 'Arthropathy associated with Reiter''s disease and nonspecific urethritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('713.5', 'L1906', 'Arthropathy associated with neurological disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L1906', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.9', 'L1906', 'Unspecified inflammatory polyarthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L1906', 'Generalized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.09', 'L1906', 'Generalized osteoarthrosis, involving multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.15', 'L1906', 'Primary localized osteoarthrosis, pelvic region and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.16', 'L1906', 'Primary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.17', 'L1906', 'Primary localized osteoarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.27', 'L1906', 'Secondary localized osteoarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.36', 'L1906', 'Localized osteoarthrosis not specified whether primary or secondary, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.37', 'L1906', 'Localized osteoarthrosis not specified whether primary or secondary, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.9', 'L1906', 'Osteoarthrosis, unspecified whether generalized or localized, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.96', 'L1906', 'Osteoarthrosis, unspecified whether generalized or localized, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.97', 'L1906', 'Osteoarthrosis, unspecified whether generalized or localized, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.98', 'L1906', 'Osteoarthrosis, unspecified whether generalized or localized, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.16', 'L1906', 'Traumatic arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.17', 'L1906', 'Traumatic arthropathy, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.87', 'L1906', 'Other specified arthropathy, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.9', 'L1906', 'Unspecified arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.95', 'L1906', 'Unspecified arthropathy, pelvic region and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.96', 'L1906', 'Unspecified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.97', 'L1906', 'Unspecified arthropathy, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.98', 'L1906', 'Unspecified arthropathy, other unspecified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.49', 'L1906', 'Other derangement of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.6', 'L1906', 'Loose body in knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.7', 'L1906', 'Chondromalacia of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.07', 'L1906', 'Articular cartilage disorder, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.1', 'L1906', 'Loose body in joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.17', 'L1906', 'Loose body in ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.31', 'L1906', 'Recurrent dislocation of shoulder joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.37', 'L1906', 'Recurrent dislocation of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.47', 'L1906', 'Contracture of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.8', 'L1906', 'Other joint derangement, not elsewhere classified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.87', 'L1906', 'Other joint derangement, not elsewhere classified, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.88', 'L1906', 'Other joint derangement, not elsewhere classified, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.89', 'L1906', 'Other joint derangement, not elsewhere classified, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.97', 'L1906', 'Unspecified ankle and foot joint derangement');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719', 'L1906', 'Effusion of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.06', 'L1906', 'Effusion of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.07', 'L1906', 'Effusion of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.17', 'L1906', 'Hemarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.26', 'L1906', 'Villonodular synovitis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.27', 'L1906', 'Villonodular synovitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.4', 'L1906', 'Pain in joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.45', 'L1906', 'Pain in joint, pelvic region and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.46', 'L1906', 'Pain in joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.47', 'L1906', 'Pain in joint, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.48', 'L1906', 'Pain in joint, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.49', 'L1906', 'Pain in joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.57', 'L1906', 'Stiffness of joint, not elsewhere classified, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.67', 'L1906', 'Other symptoms referable to ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.7', 'L1906', 'Difficulty in walking');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.97', 'L1906', 'Unspecified disorder of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('721.3', 'L1906', 'Lumbosacral spondylosis without myelopathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.1', 'L1906', 'Displacement of lumbar intervertebral disc without myelopathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.2', 'L1906', 'Displacement of intervertebral disc, site unspecified, without myelopathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.52', 'L1906', 'Degeneration of lumbar or lumbosacral intervertebral disc');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.71', 'L1906', 'Intervertebral cervical disc disorder with myelopathy, cervical region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.73', 'L1906', 'Intervertebral lumbar disc disorder with myelopathy, lumbar region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.83', 'L1906', 'Postlaminectomy syndrome, lumbar region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('723', 'L1906', 'Spinal stenosis in cervical region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('723.3', 'L1906', 'Cervicobrachial syndrome (diffuse)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('723.4', 'L1906', 'Brachial neuritis or radiculitis nos');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.1', 'L1906', 'Pain in thoracic spine');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.2', 'L1906', 'Lumbago');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.3', 'L1906', 'Sciatica');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.4', 'L1906', 'Thoracic or lumbosacral neuritis or radiculitis, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.5', 'L1906', 'Unspecified backache');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.6', 'L1906', 'Disorders of sacrum');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.8', 'L1906', 'Other symptoms referable to back');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.6', 'L1906', 'Unspecified enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.61', 'L1906', 'Pes anserinus tendinitis or bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.64', 'L1906', 'Patellar tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.7', 'L1906', 'Unspecified enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.71', 'L1906', 'Achilles bursitis or tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.72', 'L1906', 'Tibialis tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.73', 'L1906', 'Calcaneal spur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.79', 'L1906', 'Other enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.8', 'L1906', 'Other peripheral enthesopathies');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L1906', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.91', 'L1906', 'Exostosis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727', 'L1906', 'Unspecified synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.02', 'L1906', 'Giant cell tumor of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.05', 'L1906', 'Other tenosynovitis of hand and wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.06', 'L1906', 'Tenosynovitis of foot and ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L1906', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.2', 'L1906', 'Specific bursitides often of occupational origin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.3', 'L1906', 'Other bursitis disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.4', 'L1906', 'Unspecified synovial cyst');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.41', 'L1906', 'Ganglion of joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.42', 'L1906', 'Ganglion of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.43', 'L1906', 'Unspecified ganglion');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.5', 'L1906', 'Unspecified rupture of synovium');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.51', 'L1906', 'Synovial cyst of popliteal space');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.6', 'L1906', 'Nontraumatic rupture of unspecified tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.61', 'L1906', 'Complete rupture of rotator cuff');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.65', 'L1906', 'Nontraumatic rupture of quadriceps tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.67', 'L1906', 'Nontraumatic rupture of Achilles tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.68', 'L1906', 'Nontraumatic rupture of other tendons of foot and ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.69', 'L1906', 'Nontraumatic rupture of other tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.81', 'L1906', 'Contracture of tendon (sheath)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.82', 'L1906', 'Calcium deposits in tendon and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.89', 'L1906', 'Other disorders of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.2', 'L1906', 'Muscular wasting and disuse atrophy, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.4', 'L1906', 'Laxity of ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.71', 'L1906', 'Plantar fascial fibromatosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.81', 'L1906', 'Interstitial myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.85', 'L1906', 'Spasm of muscle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.89', 'L1906', 'Other disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.9', 'L1906', 'Unspecified disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729', 'L1906', 'Rheumatism, unspecified and fibrositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.1', 'L1906', 'Unspecified myalgia and myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.2', 'L1906', 'Unspecified neuralgia, neuritis, and radiculitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.31', 'L1906', 'Hypertrophy of fat pad, knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.4', 'L1906', 'Unspecified fasciitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L1906', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.6', 'L1906', 'Residual foreign body in soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.81', 'L1906', 'Swelling of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.07', 'L1906', 'Acute osteomyelitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.26', 'L1906', 'Unspecified osteomyelitis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.28', 'L1906', 'Unspecified osteomyelitis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.37', 'L1906', 'Periostitis, without mention of osteomyelitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.38', 'L1906', 'Periostitis, without mention of osteomyelitis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.96', 'L1906', 'Unspecified infection of bone, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.4', 'L1906', 'Juvenile osteochondrosis of lower extremity, excluding foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.5', 'L1906', 'Juvenile osteochondrosis of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.6', 'L1906', 'Other juvenile osteochondrosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.7', 'L1906', 'Osteochondritis dissecans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733', 'L1906', 'Unspecified osteoporosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.1', 'L1906', 'Pathologic fracture, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.12', 'L1906', 'Pathologic fracture of distal radius and ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.16', 'L1906', 'Pathologic fracture of tibia and fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.19', 'L1906', 'Pathologic fracture of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.21', 'L1906', 'Solitary bone cyst');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.81', 'L1906', 'Malunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.82', 'L1906', 'Nonunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.9', 'L1906', 'Disorder of bone and cartilage, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.91', 'L1906', 'Arrest of bone development or growth');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.92', 'L1906', 'Chondromalacia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.99', 'L1906', 'Other disorders of bone and cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('735', 'L1906', 'Hallux valgus (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('735.2', 'L1906', 'Hallux rigidus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('735.4', 'L1906', 'Other hammer toe (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.6', 'L1906', 'Other acquired deformities of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.7', 'L1906', 'Unspecified deformity of ankle and foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.72', 'L1906', 'Equinus deformity of foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.73', 'L1906', 'Cavus deformity of foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.75', 'L1906', 'Cavovarus deformity of foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.79', 'L1906', 'Other acquired deformity of ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.9', 'L1906', 'Acquired deformity of limb, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('737.1', 'L1906', 'Kyphosis (acquired) (postural)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('737.2', 'L1906', 'Lordosis (acquired) (postural)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('737.3', 'L1906', 'Scoliosis (and kyphoscoliosis), idiopathic');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('738.8', 'L1906', 'Acquired musculoskeletal deformity of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('738.9', 'L1906', 'Acquired musculoskeletal deformity of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('739', 'L1906', 'Nonallopathic lesion of head region, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('739.6', 'L1906', 'Nonallopathic lesion of lower extremities, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.51', 'L1906', 'Congenital talipes equinovarus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.52', 'L1906', 'Congenital metatarsus primus varus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.6', 'L1906', 'Congenital talipes valgus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.61', 'L1906', 'Congenital pes planus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.69', 'L1906', 'Other congenital valgus deformity of feet');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.7', 'L1906', 'Unspecified talipes');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.71', 'L1906', 'Talipes cavus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.79', 'L1906', 'Other congenital deformity of feet');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.56', 'L1906', 'Accessory carpal bones');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.59', 'L1906', 'Other congenital anomaly of upper limb, including shoulder girdle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.64', 'L1906', 'Congenital deformity of knee (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.67', 'L1906', 'Congenital anomalies of foot, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.69', 'L1906', 'Other congenital anomaly of lower limb, including pelvic girdle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.11', 'L1906', 'Congenital spondylolysis, lumbosacral region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('820.02', 'L1906', 'Closed fracture of midcervical section of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('820.21', 'L1906', 'Closed fracture of intertrochanteric section of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823', 'L1906', 'Closed fracture of upper end of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.01', 'L1906', 'Closed fracture of upper end of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.02', 'L1906', 'Closed fracture of upper end of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.1', 'L1906', 'Open fracture of upper end of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.2', 'L1906', 'Closed fracture of shaft of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.21', 'L1906', 'Closed fracture of shaft of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.22', 'L1906', 'Closed fracture of shaft of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.3', 'L1906', 'Open fracture of shaft of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.31', 'L1906', 'Open fracture of shaft of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.32', 'L1906', 'Open fracture of shaft of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.8', 'L1906', 'Closed fracture of unspecified part of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.81', 'L1906', 'Closed fracture of unspecified part of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.82', 'L1906', 'Closed fracture of unspecified part of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.91', 'L1906', 'Open fracture of unspecified part of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824', 'L1906', 'Closed fracture of medial malleolus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.7', 'L1906', 'Open trimalleolar fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.8', 'L1906', 'Unspecified closed fracture of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.9', 'L1906', 'Unspecified open fracture of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825', 'L1906', 'Closed fracture of calcaneus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.2', 'L1906', 'Closed fracture of unspecified bone(s) of foot (except toes)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.21', 'L1906', 'Closed fracture of astragalus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.22', 'L1906', 'Closed fracture of navicular (scaphoid) bone of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.23', 'L1906', 'Closed fracture of cuboid bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.24', 'L1906', 'Closed fracture of cuneiform bone of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.25', 'L1906', 'Closed fracture of metatarsal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.29', 'L1906', 'Other closed fracture of tarsal and metatarsal bones');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.3', 'L1906', 'Open fracture of unspecified bone(s) of foot (except toes)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.31', 'L1906', 'Open fracture of astragalus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.35', 'L1906', 'Open fracture of metatarsal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('826', 'L1906', 'Closed fracture of one or more phalanges of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('827', 'L1906', 'Other, multiple and ill-defined closed fractures of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('829', 'L1906', 'Closed fracture of unspecified bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836', 'L1906', 'Tear of medial cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('837', 'L1906', 'Closed dislocation of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838', 'L1906', 'Closed dislocation of foot, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838.01', 'L1906', 'Closed dislocation of tarsal (bone), joint unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838.03', 'L1906', 'Closed dislocation of tarsometatarsal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('839.07', 'L1906', 'Closed dislocation, seventh cervical vertebra');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('839.08', 'L1906', 'Closed dislocation, multiple cervical vertebrae');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('839.2', 'L1906', 'Closed dislocation, lumbar vertebra');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('839.42', 'L1906', 'Closed dislocation, sacrum');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('843', 'L1906', 'Iliofemoral (ligament) sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('843.8', 'L1906', 'Sprain and strain of other specified sites of hip and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('843.9', 'L1906', 'Sprain and strain of unspecified site of hip and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.9', 'L1906', 'Sprain and strain of unspecified site of knee and leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845', 'L1906', 'Unspecified site of ankle sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.01', 'L1906', 'Sprain and strain of deltoid (ligament) of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.02', 'L1906', 'Sprain and strain of calcaneofibular (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.03', 'L1906', 'Sprain and strain of tibiofibular (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.09', 'L1906', 'Other ankle sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.1', 'L1906', 'Sprain and strain of unspecified site of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.11', 'L1906', 'Sprain and strain of tarsometatarsal (joint) (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.12', 'L1906', 'Sprain and strain of metatarsaophalangeal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.19', 'L1906', 'Other foot sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('846', 'L1906', 'Sprain and strain of lumbosacral (joint) (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('846.1', 'L1906', 'Sprain and strain of sacroiliac (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847', 'L1906', 'Neck sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847.1', 'L1906', 'Thoracic sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('847.2', 'L1906', 'Lumbar sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848', 'L1906', 'Sprain and strain of septal cartilage of nose');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.3', 'L1906', 'Sprain and strain of ribs');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.5', 'L1906', 'Pelvic sprain and strains');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.8', 'L1906', 'Other specified sites of sprains and strains');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('848.9', 'L1906', 'Unspecified site of sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('890', 'L1906', 'Open wound of hip and thigh, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('892', 'L1906', 'Open wound of foot except toe(s) alone, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('905.7', 'L1906', 'Late effect of sprain and strain without mention of tendon injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('905.8', 'L1906', 'Late effect of tendon injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('916', 'L1906', 'Hip, thigh, leg, and ankle, abrasion or friction burn, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('916.8', 'L1906', 'Other and unspecified superficial injury of hip, thigh, leg, and ankle, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('917.8', 'L1906', 'Other and unspecified superficial injury of foot and toes, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924', 'L1906', 'Contusion of thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.01', 'L1906', 'Contusion of hip');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.1', 'L1906', 'Contusion of lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.11', 'L1906', 'Contusion of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.2', 'L1906', 'Contusion of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.21', 'L1906', 'Contusion of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.3', 'L1906', 'Contusion of toe');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.4', 'L1906', 'Contusion of multiple sites of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.5', 'L1906', 'Contusion of unspecified part of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.8', 'L1906', 'Contusion of multiple sites, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.9', 'L1906', 'Contusion of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('928.1', 'L1906', 'Crushing injury of lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('928.2', 'L1906', 'Crushing injury of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('928.21', 'L1906', 'Crushing injury of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('928.9', 'L1906', 'Crushing injury of unspecified site of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('945', 'L1906', 'Burn of unspecified degree of unspecified site of lower limb (leg)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('945.24', 'L1906', 'Blisters with epidermal loss due to burn (second degree) of lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('956.3', 'L1906', 'Injury to peroneal nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('956.9', 'L1906', 'Injury to unspecified nerve of pelvic girdle and lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.7', 'L1906', 'Injury, other and unspecified, knee, leg, ankle, and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.8', 'L1906', 'Injury, other and unspecified, other specified sites, including multiple');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('959.9', 'L1906', 'Injury, other and unspecified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.67', 'L1906', 'Infection and inflammatory reaction due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.77', 'L1906', 'Other complications due to internal joint prosthesis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.78', 'L1906', 'Other complications due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.59', 'L1906', 'Other immobilization, pressure, and attention to wound');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.23', 'L1906', 'Fitting of orthotic device');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('274', 'L4350', 'Gouty arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('335.2', 'L4350', 'Amyotrophic lateral sclerosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('337.9', 'L4350', 'Unspecified disorder of autonomic nervous system');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.5', 'L4350', 'Tarsal tunnel syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.6', 'L4350', 'Lesion of plantar nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.71', 'L4350', 'Causalgia of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.79', 'L4350', 'Other mononeuritis of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.8', 'L4350', 'Unspecified mononeuritis of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.9', 'L4350', 'Mononeuritis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('356.9', 'L4350', 'Unspecified hereditary and idiopathic peripheral neuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('459.81', 'L4350', 'Unspecified venous (peripheral) insufficiency');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('473.9', 'L4350', 'Unspecified sinusitis (chronic)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('682.6', 'L4350', 'Cellulitis and abscess of leg, except foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('682.7', 'L4350', 'Cellulitis and abscess of foot, except toes');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('686.9', 'L4350', 'Unspecified local infection of skin and subcutaneous tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707', 'L4350', 'Decubitus ulcer');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707.12', 'L4350', 'Ulcer of calf');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707.13', 'L4350', 'Ulcer of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707.14', 'L4350', 'Ulcer of heel and midfoot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707.15', 'L4350', 'Ulcer of other part of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('713.5', 'L4350', 'Arthropathy associated with neurological disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L4350', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.9', 'L4350', 'Unspecified inflammatory polyarthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L4350', 'Generalized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.16', 'L4350', 'Primary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.17', 'L4350', 'Primary localized osteoarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.18', 'L4350', 'Primary localized osteoarthrosis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.2', 'L4350', 'Secondary localized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.26', 'L4350', 'Secondary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.27', 'L4350', 'Secondary localized osteoarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.28', 'L4350', 'Secondary localized osteoarthrosis, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.3', 'L4350', 'Localized osteoarthrosis not specified whether primary or secondary, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.36', 'L4350', 'Localized osteoarthrosis not specified whether primary or secondary, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.37', 'L4350', 'Localized osteoarthrosis not specified whether primary or secondary, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.38', 'L4350', 'Localized osteoarthrosis not specified whether primary or secondary, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.8', 'L4350', 'Osteoarthrosis involving more than one site, but not specified as generalized, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.89', 'L4350', 'Osteoarthrosis involving multiple sites, but not specified as generalized');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.9', 'L4350', 'Osteoarthrosis, unspecified whether generalized or localized, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.96', 'L4350', 'Osteoarthrosis, unspecified whether generalized or localized, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.97', 'L4350', 'Osteoarthrosis, unspecified whether generalized or localized, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.98', 'L4350', 'Osteoarthrosis, unspecified whether generalized or localized, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.1', 'L4350', 'Traumatic arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.17', 'L4350', 'Traumatic arthropathy, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.9', 'L4350', 'Unspecified arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.96', 'L4350', 'Unspecified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.97', 'L4350', 'Unspecified arthropathy, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718', 'L4350', 'Articular cartilage disorder, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.17', 'L4350', 'Loose body in ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.37', 'L4350', 'Recurrent dislocation of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.57', 'L4350', 'Ankylosis of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.8', 'L4350', 'Other joint derangement, not elsewhere classified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.86', 'L4350', 'Other joint derangement, not elsewhere classified, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.87', 'L4350', 'Other joint derangement, not elsewhere classified, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.89', 'L4350', 'Other joint derangement, not elsewhere classified, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.9', 'L4350', 'Unspecified derangement, joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.97', 'L4350', 'Unspecified ankle and foot joint derangement');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719', 'L4350', 'Effusion of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.06', 'L4350', 'Effusion of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.07', 'L4350', 'Effusion of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.08', 'L4350', 'Effusion of joint, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.09', 'L4350', 'Effusion of joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.1', 'L4350', 'Hemarthrosis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.16', 'L4350', 'Hemarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.17', 'L4350', 'Hemarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.18', 'L4350', 'Hemarthrosis, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.19', 'L4350', 'Hemarthrosis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.26', 'L4350', 'Villonodular synovitis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.27', 'L4350', 'Villonodular synovitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.28', 'L4350', 'Villonodular synovitis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.29', 'L4350', 'Villonodular synovitis, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.3', 'L4350', 'Palindromic rheumatism, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.36', 'L4350', 'Palindromic rheumatism, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.37', 'L4350', 'Palindromic rheumatism, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.38', 'L4350', 'Palindromic rheumatism, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.39', 'L4350', 'Palindromic rheumatism, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.4', 'L4350', 'Pain in joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.46', 'L4350', 'Pain in joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.47', 'L4350', 'Pain in joint, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.48', 'L4350', 'Pain in joint, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.49', 'L4350', 'Pain in joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.66', 'L4350', 'Other symptoms referable to lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.67', 'L4350', 'Other symptoms referable to ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.68', 'L4350', 'Other symptoms referable to joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.69', 'L4350', 'Other symptoms referable to joints of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.8', 'L4350', 'Other specified disorders of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.86', 'L4350', 'Other specified disorders of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.87', 'L4350', 'Other specified disorders of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.88', 'L4350', 'Other specified disorders of joint of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.89', 'L4350', 'Other specified disorders of joints of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.9', 'L4350', 'Unspecified disorder of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.96', 'L4350', 'Unspecified disorder of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.97', 'L4350', 'Unspecified disorder of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.98', 'L4350', 'Unspecified joint disorder of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.99', 'L4350', 'Unspecified joint disorder of multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.7', 'L4350', 'Unspecified enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.71', 'L4350', 'Achilles bursitis or tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.72', 'L4350', 'Tibialis tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.73', 'L4350', 'Calcaneal spur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.79', 'L4350', 'Other enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L4350', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727', 'L4350', 'Unspecified synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.06', 'L4350', 'Tenosynovitis of foot and ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L4350', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.1', 'L4350', 'Bunion');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.2', 'L4350', 'Specific bursitides often of occupational origin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.3', 'L4350', 'Other bursitis disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.4', 'L4350', 'Unspecified synovial cyst');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.41', 'L4350', 'Ganglion of joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.42', 'L4350', 'Ganglion of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.43', 'L4350', 'Unspecified ganglion');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.49', 'L4350', 'Other ganglion and cyst of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.5', 'L4350', 'Unspecified rupture of synovium');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.59', 'L4350', 'Other rupture of synovium');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.6', 'L4350', 'Nontraumatic rupture of unspecified tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.67', 'L4350', 'Nontraumatic rupture of Achilles tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.68', 'L4350', 'Nontraumatic rupture of other tendons of foot and ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.69', 'L4350', 'Nontraumatic rupture of other tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.81', 'L4350', 'Contracture of tendon (sheath)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.82', 'L4350', 'Calcium deposits in tendon and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.83', 'L4350', 'Plica syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.89', 'L4350', 'Other disorders of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.9', 'L4350', 'Unspecified disorder of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.2', 'L4350', 'Muscular wasting and disuse atrophy, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.4', 'L4350', 'Laxity of ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.6', 'L4350', 'Contracture of palmar fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.71', 'L4350', 'Plantar fascial fibromatosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.85', 'L4350', 'Spasm of muscle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.89', 'L4350', 'Other disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.9', 'L4350', 'Unspecified disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729', 'L4350', 'Rheumatism, unspecified and fibrositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.1', 'L4350', 'Unspecified myalgia and myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.2', 'L4350', 'Unspecified neuralgia, neuritis, and radiculitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L4350', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.6', 'L4350', 'Residual foreign body in soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.81', 'L4350', 'Swelling of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.9', 'L4350', 'Other and unspecified disorders of soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730', 'L4350', 'Acute osteomyelitis, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.07', 'L4350', 'Acute osteomyelitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.27', 'L4350', 'Unspecified osteomyelitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.37', 'L4350', 'Periostitis, without mention of osteomyelitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.4', 'L4350', 'Juvenile osteochondrosis of lower extremity, excluding foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.5', 'L4350', 'Juvenile osteochondrosis of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.7', 'L4350', 'Osteochondritis dissecans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.9', 'L4350', 'Unspecified osteochondropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.16', 'L4350', 'Pathologic fracture of tibia and fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.19', 'L4350', 'Pathologic fracture of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.2', 'L4350', 'Unspecified cyst of bone (localized)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.44', 'L4350', 'Aseptic necrosis of talus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.82', 'L4350', 'Nonunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.9', 'L4350', 'Disorder of bone and cartilage, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.91', 'L4350', 'Arrest of bone development or growth');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.99', 'L4350', 'Other disorders of bone and cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('735', 'L4350', 'Hallux valgus (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('735.2', 'L4350', 'Hallux rigidus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('735.4', 'L4350', 'Other hammer toe (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736', 'L4350', 'Unspecified deformity of forearm, excluding fingers');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.42', 'L4350', 'Genu varum (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.7', 'L4350', 'Unspecified deformity of ankle and foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.72', 'L4350', 'Equinus deformity of foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.73', 'L4350', 'Cavus deformity of foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.79', 'L4350', 'Other acquired deformity of ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.51', 'L4350', 'Congenital talipes equinovarus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.52', 'L4350', 'Congenital metatarsus primus varus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.53', 'L4350', 'Congenital metatarsus varus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.6', 'L4350', 'Congenital talipes valgus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.61', 'L4350', 'Congenital pes planus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.7', 'L4350', 'Unspecified talipes');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.1', 'L4350', 'Syndactyly of multiple and unspecified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.6', 'L4350', 'Unspecified congenital anomaly of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.67', 'L4350', 'Congenital anomalies of foot, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.4', 'L4350', 'Chondrodystrophy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.51', 'L4350', 'Osteogenesis imperfecta');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.52', 'L4350', 'Osteopetrosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.2', 'L4350', 'Closed fracture of shaft of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.21', 'L4350', 'Closed fracture of shaft of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.22', 'L4350', 'Closed fracture of shaft of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.3', 'L4350', 'Open fracture of shaft of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.32', 'L4350', 'Open fracture of shaft of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.8', 'L4350', 'Closed fracture of unspecified part of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.81', 'L4350', 'Closed fracture of unspecified part of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.82', 'L4350', 'Closed fracture of unspecified part of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.9', 'L4350', 'Open fracture of unspecified part of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.91', 'L4350', 'Open fracture of unspecified part of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.92', 'L4350', 'Open fracture of unspecified part of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824', 'L4350', 'Closed fracture of medial malleolus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.1', 'L4350', 'Open fracture of medial malleolus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.2', 'L4350', 'Closed fracture of lateral malleolus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.3', 'L4350', 'Open fracture of lateral malleolus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.4', 'L4350', 'Closed bimalleolar fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.5', 'L4350', 'Open bimalleolar fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.6', 'L4350', 'Closed trimalleolar fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.7', 'L4350', 'Open trimalleolar fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.8', 'L4350', 'Unspecified closed fracture of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.9', 'L4350', 'Unspecified open fracture of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825', 'L4350', 'Closed fracture of calcaneus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.1', 'L4350', 'Open fracture of calcaneus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.2', 'L4350', 'Closed fracture of unspecified bone(s) of foot (except toes)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.21', 'L4350', 'Closed fracture of astragalus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.22', 'L4350', 'Closed fracture of navicular (scaphoid) bone of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.23', 'L4350', 'Closed fracture of cuboid bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.24', 'L4350', 'Closed fracture of cuneiform bone of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.25', 'L4350', 'Closed fracture of metatarsal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.29', 'L4350', 'Other closed fracture of tarsal and metatarsal bones');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.3', 'L4350', 'Open fracture of unspecified bone(s) of foot (except toes)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.31', 'L4350', 'Open fracture of astragalus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.32', 'L4350', 'Open fracture of navicular (scaphoid) bone of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.33', 'L4350', 'Open fracture of cuboid bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.34', 'L4350', 'Open fracture of cuneiform bone of foot,');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.35', 'L4350', 'Open fracture of metatarsal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.39', 'L4350', 'Other open fractures of tarsal and metatarsal bones');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('826', 'L4350', 'Closed fracture of one or more phalanges of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('826.1', 'L4350', 'Open fracture of one or more phalanges of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('827', 'L4350', 'Other, multiple and ill-defined closed fractures of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('827.1', 'L4350', 'Other, multiple and ill-defined open fractures of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('828', 'L4350', 'Multiple closed fractures involving both lower limbs, lower with upper limb, and lower limb(s) with rib(s) and sternum');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('837', 'L4350', 'Closed dislocation of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838', 'L4350', 'Closed dislocation of foot, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838.01', 'L4350', 'Closed dislocation of tarsal (bone), joint unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838.03', 'L4350', 'Closed dislocation of tarsometatarsal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838.06', 'L4350', 'Closed dislocation of interphalangeal (joint), foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845', 'L4350', 'Unspecified site of ankle sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.01', 'L4350', 'Sprain and strain of deltoid (ligament) of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.02', 'L4350', 'Sprain and strain of calcaneofibular (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.03', 'L4350', 'Sprain and strain of tibiofibular (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.09', 'L4350', 'Other ankle sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.1', 'L4350', 'Sprain and strain of unspecified site of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.11', 'L4350', 'Sprain and strain of tarsometatarsal (joint) (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.12', 'L4350', 'Sprain and strain of metatarsaophalangeal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.13', 'L4350', 'Sprain and strain of interphalangeal (joint), of toe');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.19', 'L4350', 'Other foot sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('892', 'L4350', 'Open wound of foot except toe(s) alone, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('893', 'L4350', 'Open wound of toe(s), without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('894', 'L4350', 'Multiple and unspecified open wound of lower limb, without mention of complication');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('904.8', 'L4350', 'Injury to unspecified blood vessel of lower extremity');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('905.7', 'L4350', 'Late effect of sprain and strain without mention of tendon injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('905.8', 'L4350', 'Late effect of tendon injury');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('917.8', 'L4350', 'Other and unspecified superficial injury of foot and toes, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('917.9', 'L4350', 'Other and unspecified superficial injury of foot and toes, infected');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('919', 'L4350', 'Abrasion or friction burn of other, multiple, and unspecified sites, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('919.8', 'L4350', 'Other and unspecified superficial injury of other, multiple, and unspecified sites, without mention of infection');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.1', 'L4350', 'Contusion of lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.11', 'L4350', 'Contusion of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.2', 'L4350', 'Contusion of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.21', 'L4350', 'Contusion of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.3', 'L4350', 'Contusion of toe');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.4', 'L4350', 'Contusion of multiple sites of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.5', 'L4350', 'Contusion of unspecified part of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.8', 'L4350', 'Contusion of multiple sites, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('924.9', 'L4350', 'Contusion of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('928.2', 'L4350', 'Crushing injury of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('928.21', 'L4350', 'Crushing injury of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('929.9', 'L4350', 'Crushing injury of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('945', 'L4350', 'Burn of unspecified degree of unspecified site of lower limb (leg)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('945.02', 'L4350', 'Burn of unspecified degree of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('945.03', 'L4350', 'Burn of unspecified degree of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('945.2', 'L4350', 'Blisters with epidermal loss due to burn (second degree) of unspecified site of lower limb (leg)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.78', 'L4350', 'Other complications due to other internal orthopedic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('996.79', 'L4350', 'Other complications due to other internal prosthetic device, implant, and graft');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('93.23', 'L4350', 'Fitting of orthotic device');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('110.1', 'L1902', 'Dermatophytosis of nail');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('110.4', 'L1902', 'Dermatophytosis of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('170.7', 'L1902', 'Malignant neoplasm of long bones of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('170.8', 'L1902', 'Malignant neoplasm of short bones of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('213.9', 'L1902', 'Benign neoplasm of bone and articular cartilage, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('215.3', 'L1902', 'Other benign neoplasm of connective and other soft tissue of lower limb, including hip');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('238', 'L1902', 'Neoplasm of uncertain behavior of bone and articular cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('238.2', 'L1902', 'Neoplasm of uncertain behavior of skin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('239.2', 'L1902', 'Neoplasms of unspecified nature of bone, soft tissue, and skin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250', 'L1902', 'Diabetes mellitus without mention of complication, type II or unspecified type, not stated as uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.6', 'L1902', 'Diabetes with neurological manifestations, type II or unspecified type, not stated as uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.61', 'L1902', 'Diabetes with neurological manifestations, type I [juvenile type], not stated as uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('250.8', 'L1902', 'Diabetes with other specified manifestations, type II or unspecified type, not stated as uncontrolled');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('274', 'L1902', 'Gouty arthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('337.22', 'L1902', 'Reflex sympathetic dystrophy of the lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('354.8', 'L1902', 'Other mononeuritis of upper limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.5', 'L1902', 'Tarsal tunnel syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.6', 'L1902', 'Lesion of plantar nerve');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.71', 'L1902', 'Causalgia of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.8', 'L1902', 'Unspecified mononeuritis of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('355.9', 'L1902', 'Mononeuritis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('356.9', 'L1902', 'Unspecified hereditary and idiopathic peripheral neuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('357.9', 'L1902', 'Unspecified inflammatory and toxic neuropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('440.2', 'L1902', 'Atherosclerosis of native arteries of the extremities, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('443', 'L1902', 'Raynaud''s syndrome');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('454', 'L1902', 'Varicose veins of lower extremities with ulcer');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('680.9', 'L1902', 'Carbuncle and furuncle of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('681.1', 'L1902', 'Unspecified cellulitis and abscess of toe');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('681.11', 'L1902', 'Onychia and paronychia of toe');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('682.7', 'L1902', 'Cellulitis and abscess of foot, except toes');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('686.9', 'L1902', 'Unspecified local infection of skin and subcutaneous tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('696.1', 'L1902', 'Other psoriasis and similar disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('707', 'L1902', 'Decubitus ulcer');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('709.4', 'L1902', 'Foreign body granuloma of skin and subcutaneous tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('711.16', 'L1902', 'Arthropathy associated with Reiter''s disease and nonspecific urethritis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('713.5', 'L1902', 'Arthropathy associated with neurological disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714', 'L1902', 'Rheumatoid arthritis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('714.9', 'L1902', 'Unspecified inflammatory polyarthropathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715', 'L1902', 'Generalized osteoarthrosis, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.09', 'L1902', 'Generalized osteoarthrosis, involving multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.15', 'L1902', 'Primary localized osteoarthrosis, pelvic region and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.16', 'L1902', 'Primary localized osteoarthrosis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.17', 'L1902', 'Primary localized osteoarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.27', 'L1902', 'Secondary localized osteoarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.36', 'L1902', 'Localized osteoarthrosis not specified whether primary or secondary, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.37', 'L1902', 'Localized osteoarthrosis not specified whether primary or secondary, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.9', 'L1902', 'Osteoarthrosis, unspecified whether generalized or localized, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.96', 'L1902', 'Osteoarthrosis, unspecified whether generalized or localized, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.97', 'L1902', 'Osteoarthrosis, unspecified whether generalized or localized, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('715.98', 'L1902', 'Osteoarthrosis, unspecified whether generalized or localized, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.16', 'L1902', 'Traumatic arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.17', 'L1902', 'Traumatic arthropathy, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.87', 'L1902', 'Other specified arthropathy, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.9', 'L1902', 'Unspecified arthropathy, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.95', 'L1902', 'Unspecified arthropathy, pelvic region and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.96', 'L1902', 'Unspecified arthropathy, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.97', 'L1902', 'Unspecified arthropathy, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('716.98', 'L1902', 'Unspecified arthropathy, other unspecified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.49', 'L1902', 'Other derangement of lateral meniscus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.6', 'L1902', 'Loose body in knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('717.7', 'L1902', 'Chondromalacia of patella');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.07', 'L1902', 'Articular cartilage disorder, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.1', 'L1902', 'Loose body in joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.17', 'L1902', 'Loose body in ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.31', 'L1902', 'Recurrent dislocation of shoulder joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.37', 'L1902', 'Recurrent dislocation of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.47', 'L1902', 'Contracture of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.8', 'L1902', 'Other joint derangement, not elsewhere classified, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.87', 'L1902', 'Other joint derangement, not elsewhere classified, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.88', 'L1902', 'Other joint derangement, not elsewhere classified, other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.89', 'L1902', 'Other joint derangement, not elsewhere classified, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('718.97', 'L1902', 'Unspecified ankle and foot joint derangement');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719', 'L1902', 'Effusion of joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.06', 'L1902', 'Effusion of lower leg joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.07', 'L1902', 'Effusion of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.17', 'L1902', 'Hemarthrosis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.26', 'L1902', 'Villonodular synovitis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.27', 'L1902', 'Villonodular synovitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.4', 'L1902', 'Pain in joint, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.45', 'L1902', 'Pain in joint, pelvic region and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.46', 'L1902', 'Pain in joint, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.47', 'L1902', 'Pain in joint, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.48', 'L1902', 'Pain in joint, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.49', 'L1902', 'Pain in joint, multiple sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.57', 'L1902', 'Stiffness of joint, not elsewhere classified, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.67', 'L1902', 'Other symptoms referable to ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.7', 'L1902', 'Difficulty in walking');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('719.97', 'L1902', 'Unspecified disorder of ankle and foot joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('721.3', 'L1902', 'Lumbosacral spondylosis without myelopathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.1', 'L1902', 'Displacement of lumbar intervertebral disc without myelopathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.2', 'L1902', 'Displacement of intervertebral disc, site unspecified, without myelopathy');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.52', 'L1902', 'Degeneration of lumbar or lumbosacral intervertebral disc');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.71', 'L1902', 'Intervertebral cervical disc disorder with myelopathy, cervical region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.73', 'L1902', 'Intervertebral lumbar disc disorder with myelopathy, lumbar region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('722.83', 'L1902', 'Postlaminectomy syndrome, lumbar region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('723', 'L1902', 'Spinal stenosis in cervical region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('723.3', 'L1902', 'Cervicobrachial syndrome (diffuse)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('723.4', 'L1902', 'Brachial neuritis or radiculitis nos');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.1', 'L1902', 'Pain in thoracic spine');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.2', 'L1902', 'Lumbago');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.3', 'L1902', 'Sciatica');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.4', 'L1902', 'Thoracic or lumbosacral neuritis or radiculitis, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.5', 'L1902', 'Unspecified backache');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.6', 'L1902', 'Disorders of sacrum');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('724.8', 'L1902', 'Other symptoms referable to back');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.6', 'L1902', 'Unspecified enthesopathy of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.61', 'L1902', 'Pes anserinus tendinitis or bursitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.64', 'L1902', 'Patellar tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.7', 'L1902', 'Unspecified enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.71', 'L1902', 'Achilles bursitis or tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.72', 'L1902', 'Tibialis tendinitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.73', 'L1902', 'Calcaneal spur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.79', 'L1902', 'Other enthesopathy of ankle and tarsus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.8', 'L1902', 'Other peripheral enthesopathies');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.9', 'L1902', 'Enthesopathy of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('726.91', 'L1902', 'Exostosis of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727', 'L1902', 'Unspecified synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.02', 'L1902', 'Giant cell tumor of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.05', 'L1902', 'Other tenosynovitis of hand and wrist');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.06', 'L1902', 'Tenosynovitis of foot and ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.09', 'L1902', 'Other synovitis and tenosynovitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.2', 'L1902', 'Specific bursitides often of occupational origin');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.3', 'L1902', 'Other bursitis disorders');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.4', 'L1902', 'Unspecified synovial cyst');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.41', 'L1902', 'Ganglion of joint');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.42', 'L1902', 'Ganglion of tendon sheath');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.43', 'L1902', 'Unspecified ganglion');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.5', 'L1902', 'Unspecified rupture of synovium');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.51', 'L1902', 'Synovial cyst of popliteal space');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.6', 'L1902', 'Nontraumatic rupture of unspecified tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.61', 'L1902', 'Complete rupture of rotator cuff');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.65', 'L1902', 'Nontraumatic rupture of quadriceps tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.67', 'L1902', 'Nontraumatic rupture of Achilles tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.68', 'L1902', 'Nontraumatic rupture of other tendons of foot and ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.69', 'L1902', 'Nontraumatic rupture of other tendon');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.81', 'L1902', 'Contracture of tendon (sheath)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.82', 'L1902', 'Calcium deposits in tendon and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('727.89', 'L1902', 'Other disorders of synovium, tendon, and bursa');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.2', 'L1902', 'Muscular wasting and disuse atrophy, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.4', 'L1902', 'Laxity of ligament');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.71', 'L1902', 'Plantar fascial fibromatosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.81', 'L1902', 'Interstitial myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.85', 'L1902', 'Spasm of muscle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.89', 'L1902', 'Other disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('728.9', 'L1902', 'Unspecified disorder of muscle, ligament, and fascia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729', 'L1902', 'Rheumatism, unspecified and fibrositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.1', 'L1902', 'Unspecified myalgia and myositis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.2', 'L1902', 'Unspecified neuralgia, neuritis, and radiculitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.31', 'L1902', 'Hypertrophy of fat pad, knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.4', 'L1902', 'Unspecified fasciitis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.5', 'L1902', 'Pain in soft tissues of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.6', 'L1902', 'Residual foreign body in soft tissue');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('729.81', 'L1902', 'Swelling of limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.07', 'L1902', 'Acute osteomyelitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.26', 'L1902', 'Unspecified osteomyelitis, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.28', 'L1902', 'Unspecified osteomyelitis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.37', 'L1902', 'Periostitis, without mention of osteomyelitis, ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.38', 'L1902', 'Periostitis, without mention of osteomyelitis, other specified sites');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('730.96', 'L1902', 'Unspecified infection of bone, lower leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.4', 'L1902', 'Juvenile osteochondrosis of lower extremity, excluding foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.5', 'L1902', 'Juvenile osteochondrosis of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.6', 'L1902', 'Other juvenile osteochondrosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('732.7', 'L1902', 'Osteochondritis dissecans');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733', 'L1902', 'Unspecified osteoporosis');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.1', 'L1902', 'Pathologic fracture, unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.12', 'L1902', 'Pathologic fracture of distal radius and ulna');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.16', 'L1902', 'Pathologic fracture of tibia and fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.19', 'L1902', 'Pathologic fracture of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.21', 'L1902', 'Solitary bone cyst');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.81', 'L1902', 'Malunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.82', 'L1902', 'Nonunion of fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.9', 'L1902', 'Disorder of bone and cartilage, unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.91', 'L1902', 'Arrest of bone development or growth');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.92', 'L1902', 'Chondromalacia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('733.99', 'L1902', 'Other disorders of bone and cartilage');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('735', 'L1902', 'Hallux valgus (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('735.2', 'L1902', 'Hallux rigidus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('735.4', 'L1902', 'Other hammer toe (acquired)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.6', 'L1902', 'Other acquired deformities of knee');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.7', 'L1902', 'Unspecified deformity of ankle and foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.72', 'L1902', 'Equinus deformity of foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.73', 'L1902', 'Cavus deformity of foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.75', 'L1902', 'Cavovarus deformity of foot, acquired');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.79', 'L1902', 'Other acquired deformity of ankle and foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('736.9', 'L1902', 'Acquired deformity of limb, site unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('737.1', 'L1902', 'Kyphosis (acquired) (postural)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('737.2', 'L1902', 'Lordosis (acquired) (postural)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('737.3', 'L1902', 'Scoliosis (and kyphoscoliosis), idiopathic');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('738.8', 'L1902', 'Acquired musculoskeletal deformity of other specified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('738.9', 'L1902', 'Acquired musculoskeletal deformity of unspecified site');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('739', 'L1902', 'Nonallopathic lesion of head region, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('739.6', 'L1902', 'Nonallopathic lesion of lower extremities, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.51', 'L1902', 'Congenital talipes equinovarus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.52', 'L1902', 'Congenital metatarsus primus varus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.6', 'L1902', 'Congenital talipes valgus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.61', 'L1902', 'Congenital pes planus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.69', 'L1902', 'Other congenital valgus deformity of feet');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.7', 'L1902', 'Unspecified talipes');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.71', 'L1902', 'Talipes cavus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('754.79', 'L1902', 'Other congenital deformity of feet');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.56', 'L1902', 'Accessory carpal bones');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.59', 'L1902', 'Other congenital anomaly of upper limb, including shoulder girdle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.64', 'L1902', 'Congenital deformity of knee (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.67', 'L1902', 'Congenital anomalies of foot, not elsewhere classified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('755.69', 'L1902', 'Other congenital anomaly of lower limb, including pelvic girdle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('756.11', 'L1902', 'Congenital spondylolysis, lumbosacral region');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('820.02', 'L1902', 'Closed fracture of midcervical section of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('820.21', 'L1902', 'Closed fracture of intertrochanteric section of femur');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823', 'L1902', 'Closed fracture of upper end of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.01', 'L1902', 'Closed fracture of upper end of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.02', 'L1902', 'Closed fracture of upper end of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.1', 'L1902', 'Open fracture of upper end of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.2', 'L1902', 'Closed fracture of shaft of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.21', 'L1902', 'Closed fracture of shaft of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.22', 'L1902', 'Closed fracture of shaft of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.3', 'L1902', 'Open fracture of shaft of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.31', 'L1902', 'Open fracture of shaft of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.32', 'L1902', 'Open fracture of shaft of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.8', 'L1902', 'Closed fracture of unspecified part of tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.81', 'L1902', 'Closed fracture of unspecified part of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.82', 'L1902', 'Closed fracture of unspecified part of fibula with tibia');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('823.91', 'L1902', 'Open fracture of unspecified part of fibula');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824', 'L1902', 'Closed fracture of medial malleolus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.7', 'L1902', 'Open trimalleolar fracture');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.8', 'L1902', 'Unspecified closed fracture of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('824.9', 'L1902', 'Unspecified open fracture of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825', 'L1902', 'Closed fracture of calcaneus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.2', 'L1902', 'Closed fracture of unspecified bone(s) of foot (except toes)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.21', 'L1902', 'Closed fracture of astragalus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.22', 'L1902', 'Closed fracture of navicular (scaphoid) bone of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.23', 'L1902', 'Closed fracture of cuboid bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.24', 'L1902', 'Closed fracture of cuneiform bone of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.25', 'L1902', 'Closed fracture of metatarsal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.29', 'L1902', 'Other closed fracture of tarsal and metatarsal bones');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.3', 'L1902', 'Open fracture of unspecified bone(s) of foot (except toes)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.31', 'L1902', 'Open fracture of astragalus');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('825.35', 'L1902', 'Open fracture of metatarsal bone(s)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('826', 'L1902', 'Closed fracture of one or more phalanges of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('827', 'L1902', 'Other, multiple and ill-defined closed fractures of lower limb');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('829', 'L1902', 'Closed fracture of unspecified bone');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('836', 'L1902', 'Tear of medial cartilage or meniscus of knee, current');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('837', 'L1902', 'Closed dislocation of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838', 'L1902', 'Closed dislocation of foot, unspecified part');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838.01', 'L1902', 'Closed dislocation of tarsal (bone), joint unspecified');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('838.03', 'L1902', 'Closed dislocation of tarsometatarsal (joint)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('839.07', 'L1902', 'Closed dislocation, seventh cervical vertebra');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('839.08', 'L1902', 'Closed dislocation, multiple cervical vertebrae');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('839.2', 'L1902', 'Closed dislocation, lumbar vertebra');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('839.42', 'L1902', 'Closed dislocation, sacrum');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('843', 'L1902', 'Iliofemoral (ligament) sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('843.8', 'L1902', 'Sprain and strain of other specified sites of hip and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('843.9', 'L1902', 'Sprain and strain of unspecified site of hip and thigh');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('844.9', 'L1902', 'Sprain and strain of unspecified site of knee and leg');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845', 'L1902', 'Unspecified site of ankle sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.01', 'L1902', 'Sprain and strain of deltoid (ligament) of ankle');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.02', 'L1902', 'Sprain and strain of calcaneofibular (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.03', 'L1902', 'Sprain and strain of tibiofibular (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.09', 'L1902', 'Other ankle sprain and strain');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.1', 'L1902', 'Sprain and strain of unspecified site of foot');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('845.11', 'L1902', 'Sprain and strain of tarsometatarsal (joint) (ligament)');
Insert into ICD9(ICD9Code, HCPCs, ICD9Description) Values ('337.22', 'L1843', 'Reflex sympathetic dystrophy of the lower limb');

