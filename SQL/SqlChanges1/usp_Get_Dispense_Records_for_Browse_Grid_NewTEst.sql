USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_Get_Dispense_Records_for_Browse_Grid_NewTEst]    Script Date: 11/10/2010 21:11:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mike Sneen - rewrite of the original proc to increase speed
-- Create date: 8/7/09
-- Description:	This proc will return all rows without a filter unless the Filter(@SearchCriteria)
--matches one of the items in the if statement below, in which case the result set will be narrowed
--by that criteria.

-- Exec [usp_Get_Dispense_Records_for_Browse_Grid_NewTEst] 3, 1, 'HCPCs', 'H1202'
-- Exec [usp_Get_Dispense_Records_for_Browse_Grid_NewTEst] 3, 1, 'HCPCs', 'H'
-- Exec [usp_Get_Dispense_Records_for_Browse_Grid_NewTEst] 47, 1, 'HCPCs', 'L1999'
-- Exec [usp_Get_Dispense_Records_for_Browse_Grid_NewTEst] 3, 1, 'Brand', 'breg'
-- =============================================
ALTER PROCEDURE [dbo].[usp_Get_Dispense_Records_for_Browse_Grid_NewTEst] -- 3
		@PracticeLocationID			INT
	  , @PracticeID					INT = 0			--  For backward compatibility with application code, @PracticeID is never used.
	  , @SearchCriteria varchar(50)
	  , @SearchText  varchar(50)
AS
BEGIN

	SET NOCOUNT ON;

	/*These variables are used in conjunction with the coalesce statements in the where clause.
	each of these variables will be null except one is @SearchCriteria matches.*/
	Declare @ProductName varchar(50)			--NULL   These default to null
	Declare @ProductCode varchar(50)			--NULL   These default to null
	Declare @ProductBrand varchar(50)			--NULL   These default to null
	Declare @ProductHCPCs varchar(50)			--NULL   These default to null
	Declare @ProductUPCCode varchar(50)

	if @SearchCriteria='Name'					-- ProductName
		Set @ProductName = '%' + @SearchText + '%'		--put the percent sign on both sides of the search text for product name
	else if @SearchCriteria='Code'
		Set @ProductCode = '%' +  @SearchText + '%'	
	else if @SearchCriteria='Brand'
		Set @ProductBrand = @SearchText + '%'	
	else if @SearchCriteria='HCPCs'
		Set @ProductHCPCs = @SearchText + '%'	
	else if @SearchCriteria='UPCCODE'
		Set @ProductUPCCode = @SearchText
	--exact search on @ProductUPCCode

	SELECT
	    
		PI.ProductInventoryID, 
		PCP.practicecatalogsupplierbrandID as SupplierID,
		PCSB.SupplierName,
		PCSB.BrandName,
		ProductName = COALESCE(MCP.Name,TPP.Name),
		Code = COALESCE(MCP.Code, TPP.Code),
		Side = COALESCE(MCP.LeftRightSide, TPP.LeftRightSide, ''),
		Size = COALESCE(MCP.Size, TPP.Size, ''), 
		Gender = COALESCE(MCP.Gender, TPP.Gender, ''),
		QuantityOnHand = PI.QuantityOnHandPerSystem,
		PCP.DMEDeposit,
		ABN_Needed = CAST(IsNull((Select top 1 a.ABN_Needed from ProductHCPCS p join ABN_HCPCs a on p.HCPCS=a.HCPCs where a.ABN_Needed = 1 and p.PracticeCatalogProductID = PCP.PracticeCatalogProductID), 0) as bit)

	from 
		ProductInventory PI			WITH(NOLOCK)
		inner join PracticeCatalogProduct PCP WITH(NOLOCK)
			on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID   
		inner join PracticeCatalogSupplierBrand PCSB    WITH (NOLOCK)
			on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID   
		Left Outer join MasterCatalogProduct MCP		WITH (NOLOCK)
			on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID  
		Left Outer Join ThirdPartyProduct TPP WITH (NOLOCK)
			on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID  
	where 
		PI.isactive =1 
		and PCP.isactive = 1
		and PCSB.isactive = 1
		and COALESCE(MCP.isactive, TPP.isactive) = 1
		and PI.PracticeLocationID = @PracticeLocationID
		AND (   /*
				This part is similar to an Optional Parameter Search.
				if the Parameter is not Null, then narrow the search by it, otherwise
				return all rows.  If all parameters are null, then return all rows.
				Use these lines for target or parent tables. 
				This has the effect of saying "if @variable is not null then try to match it to the " 
				left hand expression.  If @variable is null then match the left hand field to itself
				if the left hand field is null, then match '' = ''(This last part is because NULL = NULL) 
				does not return a match. */
				COALESCE(MCP.Name, '') like COALESCE(@ProductName, MCP.Name, '')
				OR
				COALESCE(TPP.Name, '') like COALESCE(@ProductName, TPP.Name, '')
				OR
				COALESCE(MCP.ShortName, '') like COALESCE(@ProductName, MCP.ShortName, '')
				OR
				COALESCE(TPP.ShortName, '') like COALESCE(@ProductName, TPP.ShortName, '')
			)
		AND (
				COALESCE(MCP.Code, '') like COALESCE(@ProductCode , MCP.Code, '')
				OR
				COALESCE(TPP.Code, '') like COALESCE(@ProductCode , TPP.Code, '')			
			)
		--AND (
		--		COALESCE(MCP.UPCCode, '') like COALESCE(@ProductUPCCode , MCP.UPCCode, '')
		--		OR
		--		COALESCE(TPP.UPCCode, '') like COALESCE(@ProductUPCCode , TPP.UPCCode, '')			
		--	)
		AND (
				COALESCE(PCSB.BrandName, '') like COALESCE(@ProductBrand, PCSB.BrandName, '')
				OR
				COALESCE(PCSB.BrandShortName, '') like COALESCE(@ProductBrand, PCSB.BrandShortName, '')
				OR
				COALESCE(PCSB.SupplierName, '') like COALESCE(@ProductBrand, PCSB.SupplierName, '')
				OR
				COALESCE(PCSB.SupplierShortName, '') like COALESCE(@ProductBrand, PCSB.SupplierShortName, '')			
			)
				
		AND (   /* use this type of statement for Child tables of the target to avoid duplicate rows.
				If we use a join for this, we would return duplicate rows, so we just check the existence
				of one record that matches the search parameters in the child table
				*/
				(
					 (@ProductHCPCs IS NULL)
					 OR EXISTS
					 (  
						Select 
							1 
						 from 
							ProductHCPCS PHCPCS 
						 where 
							PHCPCS.PracticeCatalogProductID = PCP.PracticeCatalogProductID 
						    and PHCPCS.HCPCS like @ProductHCPCs  
					  )
				 )			 
			)

		AND (  
				(
					 (@ProductUPCCode IS NULL)
					 OR EXISTS
					 (  
						Select 
							1 
						 from 
							UPCCode upc 
						 where 
							(
								upc.MasterCatalogProductID = MCP.MasterCatalogProductID 
								and upc.Code like @ProductUPCCode 
							)
							OR
							(
								upc.ThirdPartyProductID = TPP.ThirdPartyProductID 
								and upc.Code like @ProductUPCCode 
							) 
					  )
				 )			 
			)			
			
	ORDER BY
		SupplierName,
		BrandName,
		ProductName,
		Code				
END

