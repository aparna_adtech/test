use BregVisionEMR
go

create table patientcode_format_strings (
	patientcode_format_strings_id int identity(1,1) not null,
	practice_id int not null,
	format_string varchar(255),
	constraint pk_patientcode_format_strings primary key (patientcode_format_strings_id)
)
