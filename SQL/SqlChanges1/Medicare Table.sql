USE [BregVision]
GO

/****** Object:  Table [dbo].[Medicare]    Script Date: 06/21/2009 21:14:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Medicare]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Medicare](
	[HCPCs] [nvarchar](8) NULL,
	[State] [nvarchar](2) NULL,
	[MedicareCharge] [smallmoney] NULL,
	[Description] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO


USE [BregVision]
/****** Object:  Index [IX_Medicare]    Script Date: 06/21/2009 21:14:01 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Medicare]') AND name = N'IX_Medicare')
CREATE NONCLUSTERED INDEX [IX_Medicare] ON [dbo].[Medicare] 
(
	[HCPCs] ASC,
	[State] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


