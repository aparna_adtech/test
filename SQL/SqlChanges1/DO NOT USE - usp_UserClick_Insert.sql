-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[usp_UserClick_Insert]
	-- Add the parameters for the stored procedure here
	@UserID int,
	@UserName nvarchar(100),
	@PracticeID int,
	@PracticeLocationID int = null,
	@PageName nvarchar(50),
	@Area nvarchar(50),
	@Item nvarchar(50),
	@Action nvarchar(50),
	@Promotion nvarchar(50) = null,
	@UserDefined1 nvarchar(50) = null,
	@UserDefined2 nvarchar(50) = null,
	@UserDefined3 nvarchar(50) = null,
	@UserDefined4 nvarchar(50) = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Insert into UserClick
		(UserID, UserName, PracticeID, PracticeLocationID, PageName, Area, Item,  [Action], Promotion, UserDefined1, UserDefined2, UserDefined3, UserDefined4)
	Values
		(@UserID, @UserName, @PracticeID, @PracticeLocationID, @PageName, @Area, @Item, @Action, @Promotion, @UserDefined1, @UserDefined2, @UserDefined3, @UserDefined4)
	
END
GO
