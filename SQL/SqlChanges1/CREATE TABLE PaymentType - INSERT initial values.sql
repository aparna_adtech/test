USE [BregVision]
GO

/****** Object:  Table [dbo].[PaymentType]    Script Date: 05/12/2010 00:41:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PaymentType]') AND type in (N'U'))
DROP TABLE [dbo].[PaymentType]
GO

USE [BregVision]
GO

/****** Object:  Table [dbo].[PaymentType]    Script Date: 05/12/2010 00:41:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PaymentType](
	[PaymentTypeID] [int] NOT NULL,
	[PaymentTypeName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_PaymentType] PRIMARY KEY CLUSTERED 
(
	[PaymentTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

If NOT Exists(Select 1 from PaymentType where PaymentTypeID = 1) 
Insert into PaymentType(PaymentTypeID, PaymentTypeName) Values (1, 'Breg Invoice')

If NOT Exists(Select 1 from PaymentType where PaymentTypeID = 2)
Insert into PaymentType(PaymentTypeID, PaymentTypeName) Values (2, 'Pay Pal')