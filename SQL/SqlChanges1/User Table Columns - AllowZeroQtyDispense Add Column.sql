use [BregVision]
go

if not exists (select * from dbo.syscolumns where id = object_id(N'User') and name=(N'AllowZeroQtyDispense'))
alter table [User] add AllowZeroQtyDispense [bit] DEFAULT 1 NOT NULL
GO

Update  dbo.[User]
	Set AllowZeroQtyDispense = 1

from dbo.[User] U
join aspnetdb.dbo.aspnet_Users AU
	on U.AspNet_UserID = AU.UserId
join [aspnetdb].[dbo].[aspnet_UsersInRoles] UR
	on AU.UserId = UR.UserId
join [aspnetdb].[dbo].aspnet_Roles  R
	on UR.RoleId = R.RoleId
	
where R.LoweredRoleName = 'practiceadmin' OR R.LoweredRoleName = 'bregadmin'

GO
