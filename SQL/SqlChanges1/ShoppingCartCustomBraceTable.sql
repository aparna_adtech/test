USE [BregVision]
GO

/****** Object:  Table [dbo].[ShoppingCartCustomBrace]    Script Date: 06/15/2009 15:40:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ShoppingCartCustomBrace](
	[ShoppingCartID] [int] NOT NULL,
	[BregVisionOrderID] [int] NULL,
	[DispenseID] [int] NULL,
	[Billing_CustomerNumber] [varchar](50) NULL,
	[Billing_Phone] [varchar](50) NULL,
	[Billing_Attention] [varchar](50) NULL,
	[Billing_Address1] [varchar](50) NULL,
	[Billing_Address2] [varchar](50) NULL,
	[Billing_City] [varchar](50) NULL,
	[Billing_State] [varchar](50) NULL,
	[Billing_Zip] [varchar](50) NULL,
	[Shipping_Attention] [varchar](50) NULL,
	[Shipping_Address1] [varchar](50) NULL,
	[Shipping_Address2] [varchar](50) NULL,
	[Shipping_City] [varchar](50) NULL,
	[Shipping_State] [varchar](50) NULL,
	[Shipping_Zip] [varchar](50) NULL,
	[PatientName] [varchar](50) NULL,
	[PhysicianID] [int] NULL,
	[Patient_Age] [int] NULL,
	[Patient_Weight] [int] NULL,
	[Patient_Height] [varchar](50) NULL,
	[Patient_Sex] [varchar](8) NULL,
	[BraceFor] [varchar](50) NULL,
	[Instability] [varchar](50) NULL,
	[BilatCastReform] [varchar](50) NULL,
	[ThighCircumference] [varchar](50) NULL,
	[CalfCircumference] [varchar](50) NULL,
	[KneeOffset] [varchar](50) NULL,
	[KneeWidth] [varchar](50) NULL,
	[Extension] [varchar](50) NULL,
	[Flexion] [varchar](50) NULL,
	[MeasurementsTakenBy] [varchar](150) NULL,
	[X2K_Counterforce] [varchar](50) NULL,
	[X2K_Counterforce_Degrees] [varchar](50) NULL,
	[X2K_FramePadColor1] [varchar](50) NULL,
	[X2K_FramePadColor2] [varchar](50) NULL,
	[Fusion_Enhancement] [varchar](50) NULL,
	[Fusion_Notes] [varchar](1000) NULL,
	[Fusion_Color] [varchar](50) NULL,
	[Fusion_Pantone] [varchar](50) NULL,
	[Fusion_Pattern] [varchar](50) NULL,
	[Fusion_Pattern_Notes] [varchar](250) NULL,
	[ShippingTypeID] [int] NULL,
	[ShippingCarrierID] [int] NULL,
 CONSTRAINT [PK_ShoppingCartCustomBrace] PRIMARY KEY CLUSTERED 
(
	[ShoppingCartID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING on
GO


/****** Object:  Index [IX_ShoppingCartCustomBrace]    Script Date: 06/10/2009 21:14:51 ******/
CREATE NONCLUSTERED INDEX [IX_ShoppingCartCustomBrace] ON [dbo].[ShoppingCartCustomBrace] 
(
	[BregVisionOrderID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO