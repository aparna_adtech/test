
USE [BregVision]
GO

/****** Object:  Table [dbo].[InventoryCycleType]    Script Date: 06/01/2011 13:25:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


BEGIN TRANSACTION
GO
ALTER TABLE dbo.Practice ADD
	LogoPartNumber nvarchar(255) NULL 
GO
	

ALTER TABLE dbo.Practice SET (LOCK_ESCALATION = TABLE)
GO
COMMIT