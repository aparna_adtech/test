USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_SearchForDispensementsWithClaimNumber]    Script Date: 05-04-2017 04:26:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author: Chaitanya kintali>
-- Create date: <Create Date: 5/4/17>
-- Description:	<Description: Used for the Claim Number search in the dispensement modification section of the admin pages>
-- =============================================
ALTER PROCEDURE [dbo].[usp_SearchForDispensementsWithClaimNumber]

	@PracticeLocationID int,
	@SearchText varchar(50)
AS
BEGIN

if @SearchText <> ''
begin
	set @SearchText = @SearchText
end

			SELECT
				  Sub.BrandShortName
				, Sub.DispenseID
				, Sub.PatientCode
				, Sub.PatientEmail

				, SUB.Total
				, SUB.DateDispensed			--.PracticeCatalogSupplierBrandID
				, SUB.DispenseDetailID
				, SUB.PhysicianName
				, SUB.PhysicianID
				, SUB.ActualChargeBilled
				, SUB.DMEDeposit
				, SUB. Quantity
				, SUB.LineTotal
				, SUB.Product
				, SUB.Code

				, ISNULL(SUB.Packaging, '') AS Packaging
				, ISNULL(SUB.LeftRightSide, '') AS Side
				, ISNULL(SUB.Size, '') AS Size
				, ISNULL(SUB.Gender, '') AS Gender
				, ISNULL(SUB.PatientFirstName,'') As PatientFirstName
				, ISNULL(SUB.PatientLastName,'') As PatientLastName

				,SUB.Mod1
				,SUB.Mod2
				,SUB.Mod3
				,SUB.BCSClaimID
			FROM
			(



				select PCSB.BrandShortName
					, D.DispenseID
					, D.PatientCode
					, D.PatientEmail
					, D.Total
					, D.DateDispensed
					, DD.DispenseDetailID
					, C.FirstName + ' '  + C.LastName as PhysicianName
					, P.PhysicianID
					, DD.ActualChargeBilled
					, DD.DMEDeposit
					, DD.Quantity
					, DD.LineTotal
 				    , MCP.[Name]			AS Product
                    , MCP.[Code]			AS Code
                    , MCP.[Packaging]		AS Packaging
                    --, MCP.[LeftRightSide] AS LeftRightSide
                    , DD.LRModifier			AS LeftRightSide
                    , MCP.[Size]			AS Size
                    , MCP.[Gender]		AS Gender
					, D.PatientFirstName
					, D.PatientLastName
					, DD.Mod1
					, DD.Mod2
					, DD.Mod3
					, bcs.BCSClaimID
				from dispense D

					inner join Dispensedetail DD
						on DD.DispenseID = D.DispenseID
					inner join PracticeCatalogPRoduct PCP
						on PCP.PracticeCatalogProductID=DD.PracticeCatalogProductID
                    INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
						ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
					inner join MasterCatalogProduct MCP
						on MCP.mastercatalogproductid=PCP.mastercatalogproductid
					inner join Physician P
						on DD.PhysicianID = P.PhysicianID
					inner join Contact C
						on P.ContactID = C.ContactID
					left outer join [BregVisionBCSClaims].[dbo].[DispensementBCSClaimMap] bcs
						on D.DispenseID = bcs.DispenseID
				where D.practicelocationid= @PracticeLocationID
					and (bcs.BCSClaimID = @SearchText)
					AND PCP.IsThirdPartyProduct = 0  -- MasterCatalogProductsOnly
					--AND PCSB.IsActive =	1
					--AND PCP.IsActive = 1
					--AND MCP.IsActive = 1
					--and P.IsActive = 1
					--and C.IsActive = 1
					and D.IsActive = 1
					and DD.IsActive = 1

union

				select PCSB.BrandShortName
					, D.DispenseID
					, D.PatientCode
					, D.PatientEmail
					, D.Total
					, D.DateDispensed
					, DD.DispenseDetailID
					, C.FirstName + ' '  + C.LastName as PhysicianName
					, P.PhysicianID
					, DD.ActualChargeBilled
					, DD.DMEDeposit
					, DD.Quantity
					, DD.LineTotal
 				    , TPP.[Name]			AS Product
                    , TPP.[Code]			AS Code
                    , TPP.[Packaging]		AS Packaging
                    --, TPP.[LeftRightSide] AS LeftRightSide
                    , DD.LRModifier			AS LeftRightSide
                    , TPP.[Size]			AS Size
                    , TPP.[Gender]		AS Gender
					, D.PatientFirstName
					, D.PatientLastName
					, DD.Mod1
					, DD.Mod2
					, DD.Mod3
					, bcs.BCSClaimID
				from dispense D

					inner join Dispensedetail DD
						on DD.DispenseID = D.DispenseID
					inner join PracticeCatalogPRoduct PCP
						on PCP.PracticeCatalogProductID=DD.PracticeCatalogProductID
                    INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
						ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
					inner join ThirdPartyProduct TPP
						on TPP.ThirdPartyProductid=PCP.ThirdPartyProductid
					inner join Physician P
						on DD.PhysicianID = P.PhysicianID
					inner join Contact C
						on P.ContactID = C.ContactID
					left outer join [BregVisionBCSClaims].[dbo].[DispensementBCSClaimMap] bcs
						on D.DispenseID = bcs.DispenseID

				where D.practicelocationid= @PracticeLocationID
					and (bcs.BCSClaimID = @SearchText)
					AND PCP.IsThirdPartyProduct = 1  -- ThirdPartyProductsOnly
					--AND PCSB.IsActive =	1
					--AND PCP.IsActive = 1
					--AND TPP.IsActive = 1
					--and P.IsActive = 1
					--and C.IsActive = 1
					and D.IsActive = 1
					and DD.IsActive = 1


			) AS SUB
				Order By
				  --Sub.IsThirdPartyProduct
				 --Sub.PatientCode
				 Sub.DateDispensed DESC
--				, Sub.PracticeCatalogProductID
--				, SUB.Product
--				, SUB.Code

END

