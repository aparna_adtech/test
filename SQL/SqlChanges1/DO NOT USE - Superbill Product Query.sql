Select 
	JoinID = ROW_NUMBER() OVER (ORDER BY SBC1.ColumnNumber, SBC1.Sequence,SBP1.Sequence),
	SBP1.Sequence as Sequence, 
	SBP1.HCPCs As HCPCs,
	ProductName = Coalesce(SBP1.Name,  MCP1.Name, TPT1.Name, 'Name Not Found'),
	SBC1.Name as CategoryName, 
	SBC1.Sequence as CategorySequence,
	PCP1.DMEDeposit,
	SBC1.ColumnNumber,
	SBP1.PracticeLocationID
from 
	SuperbillProduct SBP1
	join SuperbillCategory SBC1 on SBP1.SuperbillCategoryID = SBC1.SuperBillCategoryID 
	join PracticeCatalogProduct PCP1 on SBP1.PracticeCatalogProductID = PCP1.PracticeCatalogProductID
	left outer join MasterCatalogProduct MCP1 on PCP1.MasterCatalogProductID = MCP1.MasterCatalogProductID AND PCP1.IsThirdPartyProduct = 0
	left outer join ThirdPartyProduct TPT1 on PCP1.ThirdPartyProductID = TPT1.ThirdPartyProductID AND PCP1.IsThirdPartyProduct = 1
	
where 
	SBP1.PracticeLocationID = 3
	order by SBC1.ColumnNumber, SBC1.Sequence,SBP1.Sequence