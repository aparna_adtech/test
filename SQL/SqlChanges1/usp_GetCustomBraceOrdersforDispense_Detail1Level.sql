USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCustomBraceOrdersforDispense_Detail1Level]    Script Date: 1/28/2016 3:02:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--SELECT * FROM dbo.SupplierOrder AS so INNER JOIN dbo.BregVisionOrder  AS bvo --po 100139  --soid 222
--	ON so.BregVisionOrderID = bvo.BregVisionOrderID 
--	WHERE bvo.PracticeLocationID = 3




-- =============================================
-- Author:		Michael Sneen
-- Create date: 08/20/2007
-- Description:	Used in the Oustanding Purchase Order section of the Check-in UI. 
-- This is a query for the Detail table

--  Modifications:  2007.11.13 JB  Comment out correct code for quantities.
--				Add BOGUS code for quantities for compatibility for current version.
--
--				JB 2007.11.15 fix query to put all quantity checked-in for the supplier order line item
--exec usp_GetCustomBraceOrdersforDispense_Detail1Level 3, '', ''
--exec usp_GetCustomBraceOrdersforDispense_Detail1Level 3, 'PONumber', '109001'
--exec usp_GetCustomBraceOrdersforDispense_Detail1Level 3, 'PatientID', 'greg'
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetCustomBraceOrdersforDispense_Detail1Level] --162, 15  -- Prod 222, 3  -- BimsDev 169, 3   -- BimsDev 162, 15

	@PracticeLocationID int,
	@SearchCriteria varchar(50), -- we only care if this is 'PONumber' OR 'PatientID' but could also be 'Browse', 'All', '',
	@SearchText  varchar(50)

AS
BEGIN

	SET NOCOUNT ON;

	Declare @BregVisionOrderID int
	Declare @PatientID varchar(50)

	set @bregVisionOrderID = null
	Set @PatientID = null

	if (@SearchCriteria='PONumber' AND ISNUMERIC(@SearchText) = 1)
	begin
	  set @BregVisionOrderID = (Cast( @SearchText as int) - 100000 )  --need to convert this to a number and do any 100000 suberaction necessary
	end
	else if(@SearchCriteria='PatientID') 
	begin
	  set @PatientID = '%' + @SearchText + '%'
	end

	select 
		BVO.BregVisionOrderID,
		PI.ProductInventoryID,
		MCP.Code,
		MCP.LeftRightSide as Side,
		MCP.Size,
		MCP.Gender,
		ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityOnHand,
		SOLI.ActualWholesaleCost,
		MCP.ShortName as ProductName	
	
	from SupplierOrderLineItem SOLI
	inner join SupplierOrder SO on SOLI.SupplierOrderID = SO.SupplierOrderID
	inner join BregVisionOrder BVO on SO.BregVisionOrderID= BVO.BregVisionOrderID
	inner join PracticeCatalogPRoduct PCP 
		on SOLI.PracticeCatalogProductID = 	PCP.PracticeCatalogPRoductID
	inner join MasterCatalogProduct MCP
		on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
	inner join ProductInventory PI
		on SOLI.PracticeCatalogProductID = PI.PracticeCatalogProductID
	inner join SupplierOrderLineItemStatus SOLIS 
		on SOLI.SupplierOrderLineItemStatusID = SOLIS.SupplierOrderLineItemStatusID
	INNER JOIN ShoppingCartCustomBrace SCCB
		ON SCCB.BregVisionOrderID = BVO.BregVisionOrderID
	LEFT JOIN (SELECT SupplierOrderLineItemID, SUM(Quantity) AS QuantityCheckedIn
				FROM dbo.ProductInventory_OrderCheckIn 
				WHERE IsActive = 1
				GROUP BY SupplierOrderLineItemID)
				AS SubPIOCI
					ON SOLI.SupplierOrderLineItemID = SubPIOCI.SupplierOrderLineItemID
	where 
		PI.PracticeLocationID = @PracticeLocationID
		AND BVO.BregVisionOrderStatusID IN (2) 
		AND BVO.isactive = 1 
		AND BVO.PracticeLocationID = @PracticeLocationID 
		and SO.SupplierOrderStatusID in (2)
		and SO.isactive = 1
		AND PCP.IsThirdPartyProduct = 0		-- MasterCatalogProduct
		--and PCP.isactive = 1
		--and MCP.isactive = 1
		AND SCCB.DispenseID is null
		
		--this, in conjunction with the definition of these variables above, handles custom search
		and SCCB.BregVisionOrderID = Coalesce(@BregVisionOrderID, SCCB.BregVisionOrderID)
		AND ((@PatientID Is Not NULL) AND  (SCCB.PatientName in(Select Distinct PatientName from ShoppingCartCustomBrace where PatientName Like @PatientID)) OR (@PatientID IS NULL))		
	ORDER BY
		ProductName,
		Code,
		Side
		

END







