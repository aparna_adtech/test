USE [BregVision]
GO

/****** Object:  Table [dbo].[PracticeApplication]    Script Date: 04/07/2010 14:20:42 ******/

--DROP Table [dbo].[PracticeApplication]
--GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PracticeApplication](
	[PracticeApplicationID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NULL,
	[Salutation] [varchar](10) NULL,
	[FirstName] [varchar](50) NOT NULL,
	[MiddleName] [varchar](50) NULL,
	[LastName] [varchar](50) NOT NULL,
	[Suffix] [varchar](10) NULL,
	[EmailAddress] [varchar](100) NULL,
	[PhoneWork] [varchar](25) NULL,
	[PhoneWorkExtension] [varchar](10) NULL,
	[PhoneCell] [varchar](25) NULL,
	[PracticeName] [varchar](50) NOT NULL,
	[PracticeID] [int] NULL,
	[UserName] [varchar](50) NOT NULL,
	[BregAccountNumber] [varchar](50) NULL,
	[IsPracticeCreated] [bit] NOT NULL,
	[IsWelcomeEmailSent] [bit] NOT NULL,
	[IsAdminUserCreated] [bit] NOT NULL,
	[CreatedUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedUserID] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
	[PracticeAppAgreementTermsID] [int] NULL,
	[Notes] [nvarchar](max) NULL,
	[timestamp] [timestamp] NOT NULL,
 CONSTRAINT [PK_PracticeApplication] PRIMARY KEY CLUSTERED 
(
	[PracticeApplicationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[PracticeApplication] ADD  CONSTRAINT [DF_PracticeApplication_IsPracticeCreated]  DEFAULT ((0)) FOR [IsPracticeCreated]
GO

ALTER TABLE [dbo].[PracticeApplication] ADD  CONSTRAINT [DF_PracticeApplication_IsWelcomeEmailSent]  DEFAULT ((0)) FOR [IsWelcomeEmailSent]
GO

ALTER TABLE [dbo].[PracticeApplication] ADD  CONSTRAINT [DF_PracticeApplication_IsAdminUserCreated]  DEFAULT ((0)) FOR [IsAdminUserCreated]
GO

ALTER TABLE [dbo].[PracticeApplication] ADD  CONSTRAINT [DF__PracticeA__Creat__18E19391]  DEFAULT ((1)) FOR [CreatedUserID]
GO

ALTER TABLE [dbo].[PracticeApplication] ADD  CONSTRAINT [DF__PracticeA__Creat__19D5B7CA]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[PracticeApplication] ADD  CONSTRAINT [DF__PracticeA__IsAct__1AC9DC03]  DEFAULT ((1)) FOR [IsActive]
GO


