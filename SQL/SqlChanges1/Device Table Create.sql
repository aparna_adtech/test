USE [BregVision]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Device](
	[DeviceID] [int] IDENTITY(1,1) NOT NULL,
	[DeviceIdentifier] [varchar](255) NULL,
	[LastLocation] [varchar](255) NULL,
	[PracticeID] [int] NULL,
	[PracticeLocationID] [int] NULL,
	[CreatedUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedUserID] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
	[timestamp] [timestamp] NOT NULL,
 CONSTRAINT [PK_Device] PRIMARY KEY CLUSTERED 
(
	[DeviceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/*
I'm putting nullable foreign keys to Practice and PracticeLocation so the device can be attached to either the practice,
or a specific location
*/

-------Practice FK-------

ALTER TABLE [dbo].[Device]  WITH CHECK ADD  CONSTRAINT [FK_Device_Practice] FOREIGN KEY([PracticeID])
REFERENCES [dbo].[Practice] ([PracticeID])
GO

ALTER TABLE [dbo].[Device] CHECK CONSTRAINT [FK_Device_Practice]
GO

-------------------

-------Practice Location FK-------

ALTER TABLE [dbo].[Device]  WITH CHECK ADD  CONSTRAINT [FK_Device_PracticeLocation] FOREIGN KEY([PracticeLocationID])
REFERENCES [dbo].[PracticeLocation] ([PracticeLocationID])
GO

ALTER TABLE [dbo].[Device] CHECK CONSTRAINT [FK_Device_PracticeLocation]
GO

-------------------

ALTER TABLE [dbo].[Device] ADD  DEFAULT ((1)) FOR [CreatedUserID]
GO

ALTER TABLE [dbo].[Device] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[Device] ADD  DEFAULT ((1)) FOR [IsActive]
GO