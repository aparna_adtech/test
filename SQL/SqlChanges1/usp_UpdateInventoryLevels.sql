USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateInventoryLevels]    Script Date: 04/08/2012 18:40:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Greg Ross
-- Create date: 10/01/07
-- Description:	Updates Inventory Levels
-- Modifications:  2008.01.30 John Bongiorni
--		When IsActive is set to true, also set IsSetToActive to true.
--		2008.02.05 jOHN Bongiorni
--			Add Column @IsNotFlaggedForReorder
--      2008.02.15 JB remove if statement add the AND IsSetToActive = 0 clause 
--			so that the IsSetToActive = 0 products in limbo can be active.
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateInventoryLevels]
	@PracticeCatalogProductID int,
	@PracticeLocationID int,
	@QOH int,
	@ParLevel int,
	@ReorderLevel int,
	@CriticalLevel int,
	@IsNotFlaggedForReorder INT = 0,
	@UserID int,
	@IsConsignment bit = 0,
	@ConsignmentQuantity INT = 0,
	@ConsignmentLockParLevel bit = 0,
	@IsLogoAblePart bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;
update ProductInventory
	Set
	 QuantityOnHandPerSystem = @QOH,
	 ParLevel = @ParLevel,
	 ReorderLevel = @ReorderLevel,
	 CriticalLevel = @CriticalLevel,
	 IsNotFlaggedForReorder = @IsNotFlaggedForReorder,
	 IsConsignment = @IsConsignment,
	 ConsignmentQuantity = @ConsignmentQuantity,
	 ModifiedUserID = @UserID,
	 ModifiedDate = getdate(),
	 ConsignmentLockParLevel = @ConsignmentLockParLevel,
	 IsLogoAblePart = @IsLogoAblePart
	where PracticeCatalogProductID = @PracticeCatalogProductID
	and PracticeLocationID = @PracticeLocationID

--if @ParLevel > 0 and @ReorderLevel > 0 and @CriticalLevel > 0 
--begin
	update ProductInventory
	set
		isactive = 1
		, IsSetToActive = 1
	where PracticeCatalogProductID = @PracticeCatalogProductID
	and PracticeLocationID = @PracticeLocationID
	AND IsSetToActive = 0
--end 
	

END
