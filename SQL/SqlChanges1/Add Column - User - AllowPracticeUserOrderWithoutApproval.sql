/*
   Wednesday, January 26, 20118:56:21 AM
   User: sa
   Server: www.bregvision.com
   Database: BregVision
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[User] ADD
	AllowPracticeUserOrderWithoutApproval bit NOT NULL CONSTRAINT DF_User_AllowPracticeUserOrderWithoutApproval DEFAULT 0
GO
ALTER TABLE dbo.[User] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
