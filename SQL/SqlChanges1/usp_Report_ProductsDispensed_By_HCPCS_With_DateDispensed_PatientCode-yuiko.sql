USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_Report_ProductsDispensed_By_Physician_With_DateDispensed_PatientCode]    Script Date: 12/03/2009 12:20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
  Exec [usp_Report_ProductsDispensed_By_HCPCS_With_DateDispensed_PatientCode]  6, '13, 15, 16, 17, 18, 19, 20, 22, 23, 24','1/1/2009', '12/31/2009',1,0
*/
----

--  [usp_Report_ProductsDispensed_By_Physician_With_DateDispensed_PatientCode]  18, '56,57', '1', '1/1/2007', '12/31/2007'
--    [usp_Report_ProductsDispensed_By_Physician_With_DateDispensed_PatientCode]  1, '3', '2,-999', '1/1/2007', '12/31/2007'  --94

--  Added Supplier Brand.
--  Added Sequence
--  Add IsActive = 1

-- Modification:  , D.DateDispensed AS DateDispensed  -- , DD.CreatedDate AS DateDispensed  --  2008.02.12  JB

Alter PROC [dbo].[usp_Report_ProductsDispensed_By_HCPCS_With_DateDispensed_PatientCode]  
		  @PracticeID			INT
		, @PracticeLocationID	VARCHAR(2000)
		--, @PhysicianID			VARCHAR(2000)
		, @StartDate			DATETIME
		, @EndDate				DATETIME
		, @OrderBy				int
		, @OrderDirection		bit
		, @IsMedicare           bit = null
		, @MedicareOrderDirection bit = null
		
AS
BEGIN



SELECT @EndDate = DATEADD(DAY, 1, @EndDate)

DECLARE @PracticeLocationIDString VARCHAR(2000)

SET @PracticeLocationIDString = @PracticeLocationID




SELECT 
		Cast(ISNULL(SUB.HCPCS, '<None>') as Varchar(25)) as HCPCS
		--ISNULL(SUB.PhysicianID, -999) AS PhysicianID
		, SUB.Physician			AS Physician
		, SUB.PracticeName		AS Practice
		, SUB.PracticeLocation	AS Location

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
			ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
			END					AS SupplierBrand


		, SUB.SupplierShortName	AS Supplier

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
		ELSE SUB.BrandShortName
		END						AS Brand

--		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN 1
--		ELSE 0
--		END AS IsBrandSuppressed
--		, SUB.IsThirdPartySupplier
		, SUB.Sequence	

		, SUB.ProductName			AS Product

		, SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender

		, SUB.Code

		--, dbo.ConcatHCPCS(SUB.PracticeCatalogProductID) AS HCPCS -- JB 20071004 Added Column for UDF Concat of HCPCS
		, case SUB.IsCashCollection
			when 1 then Sub.BillingChargeCash
			when 0 then 0
			end
			 as BillingChargeCash
		
		--, CASE WHEN LEN(SUB.Physician) > 0 THEN SUB.Physician ELSE '<blank>' END AS Physician
		, CONVERT(VARCHAR(10), SUB.DateDispensed, 101) AS DateDispensed		
		, SUB.PatientCode			

		, CASE SUB.IsMedicare
			When 1 then 'Y'
			When 0 then 'N'
		  End 
		  as IsMedicare
		, CASE SUB.ABNForm
			When 1 then 'Y'
			When 0 then 'N'
		  End 
		  as ABNForm
		  
		, SUB.ICD9Code		  
		, SUB.Quantity 		--, SUM( SUB.Quantity )
		, CAST(  SUB.ActualWholesaleCostLineTotal  AS DECIMAL(15,2) )	AS ActualWholesaleCostLineTotal
		
		,  case SUB.IsCashCollection
			when 1 then 0
			when 0 then CAST(  SUB.DMEDepositLineTotal  AS DECIMAL(15,2) )	
			end
			AS DMEDepositLineTotal
		, SUB.IsCashCollection

		,  case SUB.IsCashCollection
			when 1 then 0
			when 0 then CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )	
			end
			AS ActualChargeBilledLineTotal

FROM


		(
		SELECT 
			  
			  PH.HCPCS
			, P.PracticeName
			, PL.Name AS PracticeLocation
			, CAST(IsNULL(RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix), '') AS VARCHAR(20) ) AS Physician

			, D.PatientCode
			
			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) AS Sequence

			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash

			, MCP.[Name] AS ProductName

			, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
			
			, ISNULL(MCP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed  -- , DD.CreatedDate AS DateDispensed  --  2008.02.12  JB  --, DD.CreatedDate AS DateDispensed
			, DD.IsMedicare
			, DD.ABNForm
			, DD.ICD9Code
			, DD.Quantity
			, PI_DD.ActualWholesaleCost 
			, DD.ActualChargeBilled
			, DD.DMEDeposit	

			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit * DD.Quantity AS DMEDepositLineTotal
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.
	
		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOin DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID 

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
		INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
			ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
			
		LEFT OUTER JOIN ProductHCPCS PH
			on  PH.PracticeCatalogProductID = PCP.PracticeCatalogProductID
/*	*/	
		LEFT JOIN Physician AS Phy
			ON DD.PhysicianID = Phy.PhysicianID		    --  Can be inactive

		LEFT JOIN Contact AS C
			ON Phy.ContactID = C.ContactID
			AND C.IsActive = 1


		WHERE 
			P.PracticeID = @PracticeID
			AND DD.IsMedicare = Coalesce(@IsMedicare, DD.IsMedicare)		
			AND P.IsActive = 1
			AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			AND PCP.IsActive = 1
			AND PCSB.IsActive = 1
			AND MCP.IsActive = 1
		
		AND 
		PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))

--		AND 
--		Ph.PhysicianID IN 
--			(SELECT ID 
--			 FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianIDString, ','))
		
/*	AND
			(       
					Phy.PhysicianID 
					IN (
						 SELECT ID 
						 FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianIDString, ',')
						)
					OR 
				( 
					(Phy.PhysicianID IS NULL) 
						AND 
					( 1 = (SELECT CASE WHEN ID = -999 THEN 1 ELSE 0 END --AS IsNoPhysician 
					 FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianIDString, ',')
					 WHERE ID = -999)
					) 
				)
			)
	*/	
		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM
			
		UNION ALL

		SELECT 
			  PH.HCPCS
			, P.PracticeName
			, PL.Name AS PracticeLocation
			, CAST(IsNULL(RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix), '') AS VARCHAR(20) ) AS Physician

			, D.PatientCode
			
			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) + 1000 AS Sequence

			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash

			, TPP.[Name] AS ProductName

			, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender


			, ISNULL(TPP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed  -- , DD.CreatedDate AS DateDispensed  --  2008.02.12  JB
			, DD.IsMedicare
			, DD.ABNForm
			, DD.ICD9Code
			, DD.Quantity
			, PI_DD.ActualWholesaleCost 
			, DD.ActualChargeBilled
			, DD.DMEDeposit	

			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit * DD.Quantity AS DMEDepositLineTotal
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.




		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOin DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID 

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
		INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
			ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

		LEFT OUTER JOIN ProductHCPCS PH
			on  PH.PracticeCatalogProductID = PCP.PracticeCatalogProductID
	/**/
		LEFT JOIN Physician AS Phy
			ON DD.PhysicianID = Phy.PhysicianID			--  Can be inactive

		LEFT JOIN Contact AS C
			ON Phy.ContactID = C.ContactID
			AND C.IsActive = 1	
	
		WHERE 
			P.PracticeID = @PracticeID	
			AND DD.IsMedicare = Coalesce(@IsMedicare, DD.IsMedicare)	
			AND P.IsActive = 1
			AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			AND PCP.IsActive = 1
			AND PCSB.IsActive = 1
			AND TPP.IsActive = 1

		AND 
		PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ',')
			 )
	/*
		AND
			( 
				  
					Phy.PhysicianID 
					IN (
						 SELECT ID 
						 FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianIDString, ',')
						)
					OR  
				
				( 
					(Phy.PhysicianID IS NULL) 
						AND 
					( 1 = (SELECT CASE WHEN ID = -999 THEN 1 ELSE 0 END --AS IsNoPhysician 
					 FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianIDString, ',')
					 WHERE ID = -999)
					) 
				)
*/
--				( 
--					(PH.PhysicianID IS NULL) 
--						AND 
--					( 1 = (SELECT CASE WHEN ISNULL(ID, -999) = -999 THEN 1 ELSE 0 END --AS IsNoPhysician 
--					 FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianIDString, ',')
--					 WHERE ISNULL(ID, -999) = -999)
--					) 
--				)
	--		)


		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM	

		) AS Sub


	 order by


		CASE 
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName DESC))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName))
			
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName DESC))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName))
			
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName DESC ))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName ))
			
			
			WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed))
			
			WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed))
			
			WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed))
			
		END
	


END

