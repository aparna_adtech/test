USE [BregVision]
GO

/****** Object:  Table [dbo].[ProductInventory_Transfer]    Script Date: 07/06/2009 19:12:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ProductInventory_Transfer](
	[ProductInventory_TransferID] [int] IDENTITY(1,1) NOT NULL,
	[PracticeID] [int] NOT NULL,
	[ProductInventoryID_TransferFrom] [int] NOT NULL,
	[ProductInventoryID_TransferTo] [int] NOT NULL,
	[PracticeCatalogProductID] [int] NOT NULL,
	[PracticeLocationID_TransferTo] [int] NOT NULL,
	[PracticeLocationID_TransferFrom] [int] NOT NULL,
	[ActualWholesaleCost] [smallmoney] NOT NULL,
	[Quantity] [int] NOT NULL,
	[TransferDate] [datetime] NOT NULL,
	[Comments] [varchar](200) NULL,
	[CreatedUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedUserID] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_ProductInventory_Transfer] PRIMARY KEY CLUSTERED 
(
	[ProductInventory_TransferID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ProductInventory_Transfer]  WITH CHECK ADD  CONSTRAINT [FK_ProductInventory_Transfer_PracticeCatalogProduct] FOREIGN KEY([PracticeCatalogProductID])
REFERENCES [dbo].[PracticeCatalogProduct] ([PracticeCatalogProductID])
GO

ALTER TABLE [dbo].[ProductInventory_Transfer] CHECK CONSTRAINT [FK_ProductInventory_Transfer_PracticeCatalogProduct]
GO

ALTER TABLE [dbo].[ProductInventory_Transfer]  WITH CHECK ADD  CONSTRAINT [FK_ProductInventory_Transfer_PracticeLocation_From] FOREIGN KEY([PracticeLocationID_TransferFrom])
REFERENCES [dbo].[PracticeLocation] ([PracticeLocationID])
GO

ALTER TABLE [dbo].[ProductInventory_Transfer] CHECK CONSTRAINT [FK_ProductInventory_Transfer_PracticeLocation_From]
GO

ALTER TABLE [dbo].[ProductInventory_Transfer]  WITH CHECK ADD  CONSTRAINT [FK_ProductInventory_Transfer_PracticeLocation_To] FOREIGN KEY([PracticeLocationID_TransferTo])
REFERENCES [dbo].[PracticeLocation] ([PracticeLocationID])
GO

ALTER TABLE [dbo].[ProductInventory_Transfer] CHECK CONSTRAINT [FK_ProductInventory_Transfer_PracticeLocation_To]
GO

ALTER TABLE [dbo].[ProductInventory_Transfer]  WITH CHECK ADD  CONSTRAINT [FK_ProductInventory_Transfer_ProductInventory_Transfer_TransferTo] FOREIGN KEY([ProductInventoryID_TransferTo])
REFERENCES [dbo].[ProductInventory] ([ProductInventoryID])
GO

ALTER TABLE [dbo].[ProductInventory_Transfer] CHECK CONSTRAINT [FK_ProductInventory_Transfer_ProductInventory_Transfer_TransferTo]
GO

ALTER TABLE [dbo].[ProductInventory_Transfer]  WITH CHECK ADD  CONSTRAINT [FK_ProductInventory_Transfer_ProductInventory_TransferFrom] FOREIGN KEY([ProductInventoryID_TransferFrom])
REFERENCES [dbo].[ProductInventory] ([ProductInventoryID])
GO

ALTER TABLE [dbo].[ProductInventory_Transfer] CHECK CONSTRAINT [FK_ProductInventory_Transfer_ProductInventory_TransferFrom]
GO


