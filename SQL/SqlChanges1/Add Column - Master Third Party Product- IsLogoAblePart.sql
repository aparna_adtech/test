USE [BregVision]

Alter Table [MasterCatalogProduct] Add IsLogoAblePart bit not null default 0
GO

Alter Table [ThirdPartyProduct] Add IsLogoAblePart bit not null default 0
GO

Alter Table [PracticeCatalogProduct] Add IsLogoAblePart bit not null default 0
GO

Alter Table [ProductInventory] Add IsLogoAblePart bit not null default 0
GO

ALTER TABLE dbo.ShoppingCartItem ADD IsLogoPart bit not null default 0
GO

ALTER TABLE dbo.SupplierOrderLineItem ADD IsLogoPart bit not null default 0
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mike Sneen
-- Create date: 4/8/2012
-- Description:	Determines if a part is logo-able by examining the master catalog (mcp.IsLogoAblePart)
--and Practice (practice.LogoPartNumber not null or empty)
-- =============================================
/*
select dbo.IsPCPItemLogoAblePart(25976)
select dbo.IsPCPItemLogoAblePart(25977)
 */
create FUNCTION IsPCPItemLogoAblePart 
(
	@PracticeCatalogProductID int
)
RETURNS bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @IsPcpItemLogoable bit = 0
	Set @IsPcpItemLogoable = 0
	
	select 
		@IsPcpItemLogoable = case when mcp.IsLogoAblePart = 1  AND p.LogoPartNumber IS Not Null AND p.LogoPartNumber <> '' THEN		
		1		
		else 
		0
		end
	from 
		PracticeCatalogProduct pcp 
		inner join MasterCatalogProduct mcp 
			 on pcp.MasterCatalogProductID = mcp.MasterCatalogProductID
		inner join Practice p
			on pcp.PracticeID = p.practiceId
		Where
			pcp.practiceCatalogProductID = @PracticeCatalogProductID

	-- Return the result of the function
	RETURN @IsPcpItemLogoable

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mike Sneen
-- Create date: 4/8/2012
-- Description:	Determines if a part is logo-able by examining the master catalog (mcp.IsLogoAblePart)
--and Practice (practice.LogoPartNumber not null or empty)
/*
	select dbo.IsPiItemLogoAblePart(142501)
	select dbo.IsPiItemLogoAblePart(142502)
*/
-- =============================================
Create FUNCTION IsPIItemLogoAblePart  
(
	@ProductInventoryID int
)
RETURNS bit
AS
BEGIN
	DECLARE @IsPcpItemLogoable bit
	
	Select @IsPcpItemLogoable = dbo.IsPCPItemLogoAblePart(PCP.PracticeCatalogProductID) 
	From
		ProductInventory PI
		Inner Join PracticeCatalogProduct PCP
			on PI.practiceCatalogProductID = PCP.PracticeCatalogProductID
		Where PI.ProductInventoryID = @ProductInventoryID

	-- Return the result of the function
	RETURN @IsPcpItemLogoable

END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mike Sneen
-- Create date: 4/8/12
-- Description:	Determines if a part is a logo part by pcp.IsLogoAblePart cross verifying by examining the master catalog (mcp.IsLogoAblePart)
--and Practice (practice.LogoPartNumber not null or empty)
-- =============================================
CREATE FUNCTION GetPCPItemLogoValue 
(
	@PracticeCatalogProductID int
)
RETURNS bit
AS
BEGIN
	Declare @PcpItemLogoValue bit
	
	Select @PcpItemLogoValue = case when pcp.IsLogoAblePart = 1  AND dbo.IsPCPItemLogoAblePart(@PracticeCatalogProductID) = 1 THEN		
		1		
		else 
		0
		end
		 
	from 
		PracticeCatalogProduct pcp
	where pcp.PracticeCatalogProductID = @PracticeCatalogProductID
	
	return @PcpItemLogoValue

END
GO



