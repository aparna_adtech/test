/*
   Wednesday, January 19, 201110:48:30 AM
   User: sa
   Server: www.bregvision.com
   Database: BregVision
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ShoppingCart ADD
	CustomPurchaseOrderCode varchar(75) NULL
GO
ALTER TABLE dbo.ShoppingCart SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
