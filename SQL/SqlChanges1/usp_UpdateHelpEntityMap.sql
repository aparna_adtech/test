USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateHelpEntityMap]    Script Date: 03/15/2010 17:23:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author: Tom Thomas>
-- Create date: <Create Date: 03/12/10>
-- Description:	<Description: Update HelpEntityMapping
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateHelpEntityMap]
	@Id int 
	,@ParentId int
	,@SortOrder int = 0
		
AS 
    BEGIN
	
	SET NOCOUNT ON;
	
	
	
	Update HelpEntityTree Set ParentId=@ParentId,SortOrder=@SortOrder
	Where Id=@Id
	
	END



