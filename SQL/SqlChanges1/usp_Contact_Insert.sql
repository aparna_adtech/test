USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_Contact_Insert]    Script Date: 7/21/2015 10:31:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Contact_Insert
   
   Description:  Inserts a record into table Contact
   
   AUTHOR:       John Bongiorni 7/14/2007 7:05 PM
   
   Modifications:  
   ------------------------------------------------------------ */  
   
ALTER PROCEDURE [dbo].[usp_Contact_Insert]
(
      @ContactID                     INT = NULL OUTPUT
    , @Title                         VARCHAR(50) = NULL
    , @Salutation                    VARCHAR(10) = NULL
    , @FirstName                     VARCHAR(50)
    , @MiddleName                    VARCHAR(50) = NULL
    , @LastName                      VARCHAR(50)
    , @Suffix                        VARCHAR(10) = NULL
    , @EmailAddress                  VARCHAR(100) = NULL
    , @PhoneWork                     VARCHAR(25) = NULL
    , @PhoneWorkExtension            VARCHAR(10) = NULL
    , @PhoneCell                     VARCHAR(25) = NULL
    , @PhoneHome                     VARCHAR(25) = NULL
    , @Fax                           VARCHAR(25) = NULL
	, @IsSpecificContact			 BIT    
	, @CreatedUserID                 INT
)
AS
BEGIN
	DECLARE @Err INT

	INSERT
	INTO dbo.Contact
	(
          Title
        , Salutation
        , FirstName
        , MiddleName
        , LastName
        , Suffix
        , EmailAddress
        , PhoneWork
        , PhoneWorkExtension
        , PhoneCell
        , PhoneHome
        , Fax
		, IsSpecificContact
        , CreatedUserID
        , IsActive
	)
	VALUES
	(
          @Title
        , @Salutation
        , @FirstName
        , @MiddleName
        , @LastName
        , @Suffix
        , @EmailAddress
        , @PhoneWork
		, @PhoneWorkExtension
        , @PhoneCell
        , @PhoneHome
        , @Fax
		, @IsSpecificContact
        , @CreatedUserID
        , 1
	)

	SET @Err = @@ERROR
	SELECT @ContactID = SCOPE_IDENTITY()

	RETURN @Err
END
