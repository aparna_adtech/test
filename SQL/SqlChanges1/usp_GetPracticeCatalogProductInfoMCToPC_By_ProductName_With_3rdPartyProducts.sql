USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetPracticeCatalogProductInfoMCToPC_By_ProductName_With_3rdPartyProducts]    Script Date: 04/07/2012 11:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		John Bongiorni
-- Create date: 2007 10 09
-- Description:	Gets ProductCatalogProduct Infor for MC and 3rdParty Products,
--				Given the Product Name or portion of the product name.
--					(or Product Display Name or portion of the Product Display Name)
--			Used to get the detail for a Practice. Used in the MCToPC.aspx page
-- Modification: 20071008 2033 JB Add Third Party and code to make this generic
--  (FOR MCProducts and ThirdPartyProducts)
--				20071009 1203 JB Add WITH(NOLOCK) cLAUSES to Tables.
--
--		2007.12.12  John Bongiorni  	Order by Sub.ShortName, Sub.Code
--		2008.01.23 JB					Add LeftRightSide, Size and SideSize
--	    2008.02.25 JB Added Column for "Deletable" PCPs

-- =============================================
ALTER PROCEDURE [dbo].[usp_GetPracticeCatalogProductInfoMCToPC_By_ProductName_With_3rdPartyProducts]

	@PracticeID int
	, @SearchText  varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


select
	Sub.PracticeID, 
	Sub.PracticecatalogProductID,
	Sub.MasterCatalogProductID	AS MasterCatalogProductID,  -- Needs to be called ProductID (to handle MC and ThirdParty)
	Sub.PracticeCatalogSupplierBrandID,

	Sub.SupplierShortName,
	Sub.BrandShortName,			

	Sub.ShortName,
	Sub.Code,
	
	ISNULL(Sub.LeftRightSide, ' ') + ', ' + ISNULL(Sub.Size, ' ') AS SideSize,
	ISNULL(Sub.LeftRightSide, ' ') AS LeftRightSide,
	ISNULL(Sub.Size, ' ') AS Size,
	ISNULL(Sub.Gender, ' ') AS Gender
	
	, ISNULL( Sub.WholesaleCost, 0 )	AS WholesaleCost
	, ISNULL( Sub.BillingCharge, 0 )	AS BillingCharge
	, ISNULL( Sub.DMEDeposit, 0 )		AS DMEDeposit
	, ISNULL( Sub.BillingChargeCash, 0 ) AS BillingChargeCash
	, Sub.StockingUnits
	, Sub.BuyingUnits


	
	, Sub.IsThirdPartyProduct
	, Sub.IsLogoAblePart
	, Sub.McpIsLogoAblePart
	,dbo.ConcatHCPCS(Sub.PracticeCatalogProductID) AS HCPCSString -- JB 20071004 Added Column for UDF Concat of HCPCS						-- Need to be put in app code and grid to distinguish 
												--  Master Catalog Product and Third Party Product.
	, CASE (SELECT COUNT(1) 
		FROM productinventory AS PI WITH (NOLOCK) 
		WHERE PI.practiceCatalogProductID = sub.practiceCatalogProductID
	    ) WHEN 0 THEN 1 ELSE 0 END AS IsDeletable   --  JB  20080225  Added Column for "Deletable" PCPs

FROM
(
		select
			PCP.PracticeID, 
			PCP.PracticecatalogProductID,
			PCP.MasterCatalogProductID	AS MasterCatalogProductID,  -- Needs to be ProductID
			PCP.PracticeCatalogSupplierBrandID,
			
			PCSB.SupplierShortName,
			PCSB.BrandShortName,			
			
			MCP.ShortName,
			MCP.Code,
			
			MCP.LeftRightSide,
			MCP.Size,
			MCP.Gender,
			
			PCP.WholesaleCost,
			PCP.BillingCharge,
			PCP.DMEDeposit,
			PCP.BillingChargeCash,
			PCP.StockingUnits,
			PCP.BuyingUnits,

			0 AS IsThirdPartyProduct,					-- Needs to know if this is a Third Party Product.
			PCP.IsLogoAblePart,
			dbo.isPcpItemLogoAblePart(PCP.PracticeCatalogProductID) AS McpIsLogoAblePart
		 from PracticeCatalogProduct AS PCP WITH(NOLOCK) 
		 
		 INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK) 
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

		inner join MasterCatalogProduct AS MCP WITH(NOLOCK) 
			on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
		where PCP.practiceID =				@PracticeID
			AND PCP.IsThirdPartyProduct = 0		-- MasterCatalogProduct
			AND PCSB.IsActive = 1
			and PCP.isactive = 1
			and MCP.isactive = 1
			AND (MCP.Name LIKE @SearchText OR MCP.ShortName LIKE  @SearchText)
			

		UNION

			select
			PCP.PracticeID, 
			PCP.PracticecatalogProductID,
			PCP.ThirdPartyProductID		 AS MasterCatalogProductID,  -- Needs to be ProductID
			PCP.PracticeCatalogSupplierBrandID,
			
			PCSB.SupplierShortName,
			PCSB.BrandShortName,			

			TPP.ShortName,
			TPP.Code,

			TPP.LeftRightSide,
			TPP.Size,
			TPP.Gender,
						
			PCP.WholesaleCost,
			PCP.BillingCharge,
			PCP.DMEDeposit,
			PCP.BillingChargeCash,
			PCP.StockingUnits,
			PCP.BuyingUnits,
	
			1 AS IsThirdPartyProduct,					-- Needs to know if this is a Third Party Product.
			0 AS IsLogoAblePart,
			0 AS McpIsLogoAblePart
		 from PracticeCatalogProduct AS PCP  WITH(NOLOCK) 

		 INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK) 
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

		inner join ThirdPartyProduct AS TPP  WITH(NOLOCK) 
			on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID 
		where PCP.practiceID =			@PracticeID
			AND PCP.IsThirdPartyProduct = 1		-- ThirdPartyProduct
			AND PCSB.IsActive = 1
			and PCP.isactive = 1
			and TPP.isactive = 1
			AND (TPP.Name LIKE @SearchText OR TPP.ShortName LIKE  @SearchText)

) AS Sub
ORDER BY
	--  Sub.IsThirdPartyProduct,  --jb
	UPPER(RTRIM(LTRIM(Sub.ShortName))) -- jb 
	, UPPER(RTRIM(LTRIM(Sub.Code)))		-- jb


END





