USE [BregVisionEMR]
GO

/****** Object:  StoredProcedure [dbo].[InsertOrUpdateScheduleItem]    Script Date: 6/8/2014 9:26:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[InsertOrUpdateScheduleItem]
	@remote_schedule_item_id nvarchar(50),
	@practice_id int,
	@practice_location_id int,
	@patient_id nvarchar(20),
	@patient_name nvarchar(50),
	@patient_email nvarchar(50),
	@physician_name nvarchar(50),
	@diagnosis_icd9 nvarchar(50),
	@scheduled_visit_datetime datetime,
	@scheduled_visit_datetime_timezoneid nvarchar(50),
	@patient_dob datetime = null,
	@is_checked_in bit = 0,
	@message ntext = null
	@patient_first_name nvarchar(50)=null,
	@patient_last_name nvarchar(50)=null

AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
    BEGIN TRANSACTION;
	UPDATE [dbo].[schedule_item]
	SET
		[patient_id]=@patient_id
		,[patient_name]=@patient_name
		,[patient_first_name]=@patient_first_name
		,[patient_last_name]=@patient_last_name
		,[patient_email]=@patient_email
		,[physician_name]=@physician_name
		,[diagnosis_icd9]=@diagnosis_icd9
		,[scheduled_visit_datetime]=@scheduled_visit_datetime
		,[scheduled_visit_datetime_timezoneid]=@scheduled_visit_datetime_timezoneid
		,[patient_dob]=@patient_dob
		,[is_checked_in]=@is_checked_in
		,[message]=@message
	WHERE
		[remote_schedule_item_id]=@remote_schedule_item_id
		AND [practice_id]=@practice_id
		AND [practice_location_id]=@practice_location_id;
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [dbo].[schedule_item]
		(
			[remote_schedule_item_id]
			,[practice_id]
			,[practice_location_id]
			,[patient_id]
			,[patient_name]
			,[patient_first_name]
			,[patient_last_name]
			,[patient_email]
			,[physician_name]
			,[diagnosis_icd9]
			,[scheduled_visit_datetime]
			,[creation_date]
			,[is_active]
			,[scheduled_visit_datetime_timezoneid]
			,[patient_dob]
			,[is_checked_in]
			,[message]
		)
		VALUES
		(
			@remote_schedule_item_id
			,@practice_id
			,@practice_location_id
			,@patient_id
			,@patient_name
			,isnull(@patient_first_name,@patient_name)
			,@patient_last_name
			,@patient_email
			,@physician_name
			,@diagnosis_icd9
			,@scheduled_visit_datetime
			,GETDATE()
			,1
			,@scheduled_visit_datetime_timezoneid
			,@patient_dob
			,@is_checked_in
			,@message
		);
	END
	COMMIT TRANSACTION;
END


GO


