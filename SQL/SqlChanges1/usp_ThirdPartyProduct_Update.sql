USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_ThirdPartyProduct_Update]    Script Date: 01/01/2012 18:13:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER proc [dbo].[usp_ThirdPartyProduct_Update]
	  @ThirdPartyProductID INT
	, @PracticeCatalogSupplierBrandID INT
	, @ProductCode			VARCHAR(25)
	, @ProductName			VARCHAR(50)
	, @Packaging			VARCHAR(10) = ''
	, @LeftRightSide		VARCHAR(10) = ''
	, @Size					VARCHAR(10) = ''
	, @Color				VARCHAR(10) = ''
	, @Gender				VARCHAR(10) = ''
	, @WholesaleCost		SMALLMONEY  = 0
	, @HCPCSString			VARCHAR(100) = ''
	, @IsHCPCSUpdated		BIT = 0
	, @UserID				INT			= 92210
	, @UPCCODE				varchar(75) = ''
	, @UserMessage			VARCHAR(50)	= '' OUTPUT
as
begin
--  TODO:  
--  !!!change to order by hcpcs alter FUNCTION [dbo].[ConcatHCPCS](@PracticeCatalogProductID INT)

--  TODO:  Check to make sure that nothing is overwritten!!!!.
DECLARE @CurrentProductName			VARCHAR(50)
DECLARE @DuplicateRecordCount		INT
DECLARE @PracticeCatalogProductID	INT
DECLARE @ExistingProductCount			BIT   --  Check if Record was deleted prior to calling update.

SET @CurrentProductName = ''
SET @UserMessage = ''


Select 
	@ExistingProductCount = COUNT(1)
FROM ThirdPartyProduct AS TPP					WITH (NOLOCK)
INNER JOIN PracticeCatalogProduct AS PCP		WITH (NOLOCK)
	ON TPP.ThirdPartyProductID = PCP.ThirdPartyProductID
WHERE 
	TPP.IsActive = 1
	AND PCP.IsActive = 1
	AND PCP.PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID
	AND TPP.ThirdPartyProductID = @ThirdPartyProductID
	

Select 
	@DuplicateRecordCount = COUNT(1)
FROM ThirdPartyProduct AS TPP					WITH (NOLOCK)
INNER JOIN PracticeCatalogProduct AS PCP		WITH (NOLOCK)
	ON TPP.ThirdPartyProductID = PCP.ThirdPartyProductID
WHERE 
	TPP.IsActive = 1
	AND PCP.IsActive = 1
	AND PCP.PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID
	AND RTRIM(LTRIM(Code)) = @ProductCode
	AND RTRIM(LTRIM(ShortName)) = @ProductName 
	AND ISNULL(Packaging, '') = ISNULL(@Packaging, '')
	AND ISNULL(LeftRightSide, '') = ISNULL(@LeftRightSide, '')
	AND ISNULL(Size, '') = ISNULL(@Size, '')
	AND ISNULL(Color, '') = ISNULL(@Color, '')
	AND ISNULL(Gender, '') = ISNULL(@Gender, '')

	AND TPP.ThirdPartyProductID <> @ThirdPartyProductID
	

	
IF (@ExistingProductCount = 0)
BEGIN
	SET @UserMessage = 'Deleted'
END
ELSE
BEGIN

	IF (@DuplicateRecordCount = 0)
	BEGIN

	BEGIN TRAN

	SELECT 
		@CurrentProductName = RTRIM(LTRIM(TPP.ShortName))
	FROM ThirdPartyProduct AS TPP WITH (NOLOCK)
	WHERE TPP.ThirdPartyProductID = @ThirdPartyProductID


	UPDATE ThirdPartyProduct WITH (ROWLOCK)
		--  PracticeCatalogSupplierBrandID  //Change Supplier
		SET Code = @ProductCode
		, NAME = @ProductName
		, ShortName= @ProductName

		, Packaging = @Packaging
		, LeftRightSide = @LeftRightSide
		, Size = @Size
		, Color = @Color
		, Gender = @Gender
		, WholesaleListCost = @WholesaleCost
		--, UPCCode = @UPCCODE
		, ModifiedUserID = @UserID
		, ModifiedDate = GETDATE()
		, IsActive = 1
	WHERE ThirdPartyProductID = @ThirdPartyProductID
		AND PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID
		AND IsActive = 1
		

	UPDATE PracticeCatalogProduct WITH (ROWLOCK)
		SET WholesaleCost = @WholesaleCost
		, ModifiedUserID = @UserID
		, ModifiedDate = GETDATE()
	WHERE
			PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID
		AND ThirdPartyProductID = @ThirdPartyProductID
		AND MasterCatalogProductID IS NULL
		AND IsActive = 1

		-- DELECT and INSERT HCPCs for product.
		SELECT @HCPCSstring = RTRIM(LTRIM(REPLACE(@HCPCSstring, ', ', ',')))

		IF (@IsHCPCSUpdated = 1)
		BEGIN

			-- Check if the HCPCs really need to be updated
			DECLARE @OLDHCPCSString VARCHAR(100)

			SELECT TOP 1
				@PracticeCatalogProductID = PracticeCatalogProductID
			FROM PracticeCatalogProduct WITH (ROWLOCK)
			WHERE
					PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID
				AND ThirdPartyProductID = @ThirdPartyProductID
				AND MasterCatalogProductID IS NULL
				AND IsActive = 1

			SELECT @OLDHCPCSString = [dbo].[ConcatHCPCS](@PracticeCatalogProductID)
			SELECT @OLDHCPCSString = RTRIM(LTRIM(REPLACE(@OLDHCPCSString, ', ', ',')))
			
			-- If New HCPCSString is different than the current HCPCSString then remove and insert.
			IF (@HCPCSstring <> @OLDHCPCSString)
			BEGIN
			
				DELETE FROM productHCPCs 
				WHERE PracticeCatalogProductID = @PracticeCatalogProductID

				INSERT INTO productHCPCs
					(PracticeCatalogProductID, HCPCS, CreatedUserID, CreatedDate, ModifiedUserID, ModifiedDate, IsActive)
				SELECT 
					  @PracticeCatalogProductID AS PracticeCatalogProductID
					, LTRIM(RTRIM(TextValue)) AS HCPCS
					, @UserID AS CreatedUserID
					, GETDATE() AS CreatedDate
					, @UserID AS ModifiedUserID  
					, GETDATE() AS ModifiedDate
					, 1 AS Active
				FROM dbo.[udf_ParseStringArrayToTable]( @HCPCSstring, ',') 
				WHERE LEN(LTRIM(RTRIM(TextValue))) > 0 
				ORDER BY TextValue
				
			END	
		END
		
		-- Delete and Insert UPCCodes
		SELECT @UPCCODE = RTRIM(LTRIM(REPLACE(@UPCCODE, ', ', ',')))

		IF (@UPCCODE is not null)
		BEGIN

			-- Check if the HCPCs really need to be updated
			DECLARE @OLDUPCCodeString VARCHAR(100)

			SELECT TOP 1
				@PracticeCatalogProductID = PracticeCatalogProductID
			FROM PracticeCatalogProduct WITH (ROWLOCK)
			WHERE
					PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID
				AND ThirdPartyProductID = @ThirdPartyProductID
				AND MasterCatalogProductID IS NULL
				AND IsActive = 1

			SELECT @OLDUPCCodeString = [dbo].[ConcatUPCCodes](@ThirdPartyProductID)
			SELECT @OLDUPCCodeString = RTRIM(LTRIM(REPLACE(@OLDUPCCodeString, ', ', ',')))
			
			-- If New HCPCSString is different than the current HCPCSString then remove and insert.
			IF (@UPCCODE <> @OLDUPCCodeString)
			BEGIN
			
				DELETE FROM UPCCode 
				WHERE ThirdPartyProductID = @ThirdPartyProductID

				INSERT INTO UPCCode
					(ThirdPartyProductID, Code, CreatedUserID, CreatedDate, ModifiedUserID, ModifiedDate, IsActive)
				SELECT 
					  @ThirdPartyProductID AS ThirdPartyProductID
					, LTRIM(RTRIM(TextValue)) AS Code
					, @UserID AS CreatedUserID
					, GETDATE() AS CreatedDate
					, @UserID AS ModifiedUserID  
					, GETDATE() AS ModifiedDate
					, 1 AS Active
				FROM dbo.[udf_ParseStringArrayToTable]( @UPCCODE, ',') 
				WHERE LEN(LTRIM(RTRIM(TextValue))) > 0 
				ORDER BY TextValue
				
			END	
		END
		
		-- End Delete and Insert UPCCodes


	IF (@CurrentProductName <> @ProductName)
	BEGIN
		SET @UserMessage = 'UpdatedProductName'
	END

	COMMIT TRAN

	END
	ELSE
	BEGIN
		SET @UserMessage = 'Duplicate'
	END
END


end
