USE [BregVision]
GO

/****** Object:  StoredProcedure [dbo].[usp_Report_ProductsDispensed_By_HCPCS_With_DateDispensed_PatientCode]    Script Date: 23-03-2017 11:41:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*
  exec [usp_Report_ProductsDispensed_By_HCPCS_With_DateDispensed_PatientCode]  1, '3','1/1/2009', '12/31/2009',1,0, 'H1202,H1202A,L1111'
Select * from dbo.udf_ParseStringArrayToTable('H1202,H1202A,L1111,<none>', ',')
*/
----

--  [usp_Report_ProductsDispensed_By_Physician_With_DateDispensed_PatientCode]  18, '56,57', '1', '1/1/2007', '12/31/2007'
--    [usp_Report_ProductsDispensed_By_Physician_With_DateDispensed_PatientCode]  1, '3', '2,-999', '1/1/2007', '12/31/2007'  --94

--  Added Supplier Brand.
--  Added Sequence
--  Add IsActive = 1

-- Modification:  , D.DateDispensed AS DateDispensed  -- , DD.CreatedDate AS DateDispensed  --  2008.02.12  JB
--Modification: Adding the fittet column to the report dataset VSN 170

ALTER PROC [dbo].[usp_Report_ProductsDispensed_By_HCPCS_With_DateDispensed_PatientCode]
		  @PracticeID			INT
		, @PracticeLocationID	VARCHAR(2000)
		--, @PhysicianID			VARCHAR(2000)
		, @StartDate			DATETIME
		, @EndDate				DATETIME
		, @OrderBy				int
		, @OrderDirection		bit
		, @HCPCsList			Varchar(2000) = null
		, @IsMedicare           bit = null
		, @MedicareOrderDirection bit = null
		, @InternalPracticeLocationID VARCHAR(2000)
		, @InternalHCPCsList	VARCHAR(2000) = NULL
AS
BEGIN

DECLARE @PracticeLocationIDString VARCHAR(2000)

-- Parse Practice Location List
DECLARE @locations TABLE([PracticeLocationID] INT);

IF(@InternalPracticeLocationID = 'ALL')
BEGIN
	DECLARE @tempLocations TABLE (
			PracticeLocationID INT
			,PracticeLocationName VARCHAR(50)
			,IsPrimaryLocation BIT
			);

		INSERT @tempLocations
		EXEC dbo.usp_PracticeLocation_Select_All_ID_Name_Given_PracticeID @PracticeID = @PracticeID;

		INSERT @locations ([PracticeLocationID])
		SELECT PracticeLocationID
		FROM @tempLocations;
END;
ELSE
BEGIN
	INSERT @locations ([PracticeLocationID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable(@PracticeLocationID, ',');
END;

-- Get All HCPCs if called for
IF(@InternalHCPCsList = 'ALL')
BEGIN
	DECLARE @locationList NVARCHAR(MAX);
	IF(@InternalPracticeLocationID = 'ALL')
	BEGIN
		SELECT @locationList = COALESCE(@locationList + CAST(PracticeLocationID AS NVARCHAR(10)) + ',', '')
		FROM @locations;

		IF(LEN(@locationList) > 0)
			SET @locationList = LEFT(@locationList, LEN(@locationList) - 1);
	END;
	ELSE
	BEGIN
		SET @locationList = @PracticeLocationID;
	END;

	DECLARE @hcpcs TABLE (HCPCS VARCHAR(50));

	INSERT @hcpcs
	EXEC dbo.usp_GetPracticeLocationHCPCs @PracticeLocationIDString = @locationList;

	SET @HCPCsList = '';

	SELECT @HCPCsList = COALESCE(@HCPCsList + HCPCS + ',','')
	FROM @hcpcs;

	IF(LEN(@HCPCsList) > 0)
		SET @HCPCsList = LEFT(@HCPCsList, LEN(@HCPCsList) - 1);
END;

IF NOT EXISTS (
  select * from (
          select distinct dq.HCPCs
            from DispenseQueue dq
            where dq.PracticeLocationID in (select ID from dbo.udf_ParseArrayToTable(@PracticeLocationID, ','))
              and nullif(dq.HCPCs, '') is not null
          union
          select distinct ph.HCPCS
            from
              ProductInventory pi
              inner join PracticeCatalogProduct pcp on pi.PracticeCatalogProductID = pcp.PracticeCatalogProductID
              inner join ProductHCPCS ph on pcp.PracticeCatalogProductID = ph.PracticeCatalogProductID
            where
              pi.PracticeLocationID in (select ID from dbo.udf_ParseArrayToTable(@PracticeLocationID, ','))
        ) as T
        where ltrim(rtrim(t.HCPCS)) not in (select ltrim(rtrim(TextValue)) from dbo.udf_ParseStringArrayToTable(@HCPCsList, ','))
	) SET @HCPCsList = NULL

IF @HCPCsList = '' set @HCPCsList = NULL

DECLARE @HCPCSTable TABLE ( TextValue VARCHAR(50) ) 
INSERT INTO @HCPCSTable SELECT RTRIM(LTRIM(TextValue)) FROM dbo.udf_ParseStringArrayToTable(@HCPCsList, ',')

SELECT @EndDate = DATEADD(DAY, 1, @EndDate)

SELECT
		SUB.HCPCS

		, SUB.PracticeName		AS Practice
		, SUB.PracticeLocation	AS Location

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
			ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
			END					AS SupplierBrand


		, SUB.SupplierShortName	AS Supplier

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
		ELSE SUB.BrandShortName
		END						AS Brand


		, SUB.Sequence

		, SUB.ProductName			AS Product

		, SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender

		,SUB.FitterName

		, SUB.Code


		, case SUB.IsCashCollection
			when 1 then Sub.BillingChargeCash
			when 0 then 0
			end
			 as BillingChargeCash


		, CONVERT(VARCHAR(10), SUB.DateDispensed, 101) AS DateDispensed
		, SUB.PatientCode

		, CASE SUB.IsMedicare
			When 1 then 'Y'
			When 0 then 'N'
		  End
		  as IsMedicare
		, CASE CAST(SUB.ABNType AS varchar(50))
			When 'Medicare' then 'M' + ',' +  CAST(SUB.MedicareOption AS varchar(50))
			When 'Commercial' then 'C' + ',' + CAST(SUB.MedicareOption AS varchar(50))
		  End
		  as ABNType
		, SUB.ICD9Code
		, SUB.Quantity 		--, SUM( SUB.Quantity )

		, CAST(  SUB.ActualWholesaleCostLineTotal  AS DECIMAL(15,2) )	AS ActualWholesaleCostLineTotal
		, CAST(  SUB.DMEDepositLineTotal  AS DECIMAL(15,2) )			AS DMEDepositLineTotal
		, SUB.IsCashCollection
		,  case SUB.IsCashCollection
			when 1 then 0
			when 0 then CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )
			end
			AS ActualChargeBilledLineTotal

		, ISNULL(SUB.PayerName, 'N/A') AS PayerName
		, SUB.DispenseNote
		, SUB.OriginalActualChargeBilled
		, LTRIM(RTRIM(ISNULL(SUB.PatientFirstName,''))) AS PatientFirstName
		, LTRIM(RTRIM(ISNULL(SUB.PatientLastName,''))) AS PatientLastName
		, SUB.Mod1
		, SUB.Mod2
		, SUB.Mod3
FROM


		(
		SELECT
			  --PH.HCPCS
			ISNULL(NULLIF(
				ISNULL(DQ.HCPCs,
					IIF(DQ.IsCustomFit = NULL or (DQ.IsCustomFit = 0 AND pcp.IsSplitCode=1),
						dbo.ConcatHCPCSOTS(pcp.PracticeCatalogProductID),
						dbo.ConcatHCPCS(pcp.PracticeCatalogProductID)))
				,''), 'No HCPC') AS HCPCS
			, P.PracticeName
			, PL.Name AS PracticeLocation

			, D.PatientCode

			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) AS Sequence

			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash

			, MCP.[Name] AS ProductName

			--, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender

			, ISNULL(MCP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed  -- , DD.CreatedDate AS DateDispensed  --  2008.02.12  JB  --, DD.CreatedDate AS DateDispensed
			, DD.IsMedicare
			, DD.ABNType
			, DD.ICD9Code
			, DD.Quantity
			, FC.FirstName + ' ' + FC.LastName as FitterName
			, PI_DD.ActualWholesaleCost
			, DD.ActualChargeBilled as ActualChargeBilled
			, DD.DMEDeposit
			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit AS DMEDepositLineTotal
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.

			, PP.Name AS PayerName
			, D.Notes AS DispenseNote
			, DD.ActualChargeBilled as OriginalActualChargeBilled
			, D.PatientFirstName as PatientFirstName
			, D.PatientLastName as PatientLastName
			, DD.Mod1
			, DD.Mod2
			, DD.Mod3
			, DQ.MedicareOption

		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOin DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID

		INNER JOIN Clinician AS CP
            ON CP.ClinicianID = DD.FitterID
        INNER JOIN Contact AS FC
            ON CP.ContactID = FC.ContactID

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
			ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
		
		INNER JOIN Clinician AS CP
			ON CP.ClinicianID = DD.FitterID
		INNER JOIN Contact AS FC
		ON CP.ContactID = FC.ContactID

		LEFT OUTER JOIN DispenseQueue DQ
			on  DQ.DispenseQueueID = DD.DispenseQueueID

		--LEFT OUTER JOIN ProductHCPCS PH
		--	on  PH.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		LEFT JOIN PracticePayer PP
			on PP.PracticePayerID = D.PracticePayerID

		WHERE
			P.PracticeID = @PracticeID
			AND DD.IsMedicare = Coalesce(@IsMedicare, DD.IsMedicare)
			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			--AND PCP.IsActive = 1
			--AND PCSB.IsActive = 1
			--AND MCP.IsActive = 1

		AND
		PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			FROM @locations)

		--AND
		--	PH.HCPCS IN (Select TextValue as HCPC s from dbo.udf_ParseStringArrayToTable(@HCPCsList, ','))


		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM

		UNION ALL

		SELECT
			  --PH.HCPCS
			ISNULL(NULLIF(
				ISNULL(DQ.HCPCs,
					IIF(DQ.IsCustomFit = NULL or (DQ.IsCustomFit = 0 AND pcp.IsSplitCode=1),
						dbo.ConcatHCPCSOTS(pcp.PracticeCatalogProductID),
						dbo.ConcatHCPCS(pcp.PracticeCatalogProductID)))
				,''), 'No HCPC') AS HCPCS
			  --Ph.PhysicianID
			, P.PracticeName
			, PL.Name AS PracticeLocation
			--, IsNULL(RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix), '') AS Physician

			, D.PatientCode

			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) + 1000 AS Sequence

			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash

			, TPP.[Name] AS ProductName

			--, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender


			, ISNULL(TPP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed  -- , DD.CreatedDate AS DateDispensed  --  2008.02.12  JB
			, DD.IsMedicare
			, DD.ABNType
			, DD.ICD9Code
			, DD.Quantity
			, FC.FirstName + ' ' + FC.LastName as FitterName
			, PI_DD.ActualWholesaleCost
			, DD.ActualChargeBilled as ActualChargeBilled
			, DD.DMEDeposit
			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit AS DMEDepositLineTotal
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.

			, PP.Name AS PayerName
			, D.Notes AS DispenseNote
			, DD.ActualChargeBilled as OriginalActualChargeBilled
			, D.PatientFirstName as PatientFirstName
			, D.PatientLastName as PatientLastName
			, DD.Mod1
			, DD.Mod2
			, DD.Mod3
			, DQ.MedicareOption

		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOin DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN Clinician AS CP
            ON CP.ClinicianID = DD.FitterID
        INNER JOIN Contact AS FC
            ON CP.ContactID = FC.ContactID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
			ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
		
		INNER JOIN Clinician AS CP
			ON CP.ClinicianID = DD.FitterID
		INNER JOIN Contact AS FC
		ON CP.ContactID = FC.ContactID

		LEFT OUTER JOIN DispenseQueue DQ
			on  DQ.DispenseQueueID = DD.DispenseQueueID

		--LEFT OUTER JOIN ProductHCPCS PH
		--	on  PH.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		LEFT JOIN PracticePayer PP
			on PP.PracticePayerID = D.PracticePayerID

		WHERE
			P.PracticeID = @PracticeID
			AND DD.IsMedicare = Coalesce(@IsMedicare, DD.IsMedicare)
			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			--AND PCP.IsActive = 1
			--AND PCSB.IsActive = 1
			--AND TPP.IsActive = 1

		AND
		PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			FROM @locations)

		--AND
		--	PH.HCPCS IN (Select TextValue as HCPC s from dbo.udf_ParseStringArrayToTable(@HCPCsList, ','))


		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM

		) AS Sub

--	where (@HCPCsList IS NULL OR ISNULL(SUB.HCPCS,'No HCPC') in(Select TextValue as HCPCs from dbo.udf_ParseStringArrayToTable(@HCPCsList, ',')))
	where (@HCPCsList IS NULL OR SUB.HCPCS = 'No HCPC' OR @InternalHCPCsList = 'ALL' OR (
		--dbo.CompareDelimitedStrings(SUB.HCPCS, @HCPCsList, ',') = 1)
		EXISTS (
			SELECT *
			FROM dbo.udf_ParseStringArrayToTable(SUB.HCPCS, ',') S
			JOIN @HCPCSTable T ON T.TextValue = S.TextValue
			)
		)
	)

	 order by


		CASE
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName DESC))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName))

			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName DESC))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName))

			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName DESC ))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName ))


			WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed))

			WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed))

			WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed))

		END



END

GO


