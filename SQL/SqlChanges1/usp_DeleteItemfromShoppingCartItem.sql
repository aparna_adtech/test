USE [BregVision]
GO

/****** Object:  StoredProcedure [dbo].[usp_DeleteItemfromShoppingCartItem]    Script Date: 07/16/2012 11:16:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Greg Ross
-- Create date: 08/02/2007	
-- Description:	Deletes item from shopping cart items table
--
-- 07/16/2012 - Updated logic so that the custom brace measurements 
--				in ShoppingCartCustomBrace is not removed unless it's
--				the actual brace that is being removed from the cart.
--				Users were deleting the accessories and it was wiping
--				out the measurements by accident. (by hchan@breg.com)
-- =============================================

ALTER PROCEDURE [dbo].[usp_DeleteItemfromShoppingCartItem]

	@ShoppingCartItemID int	

AS
BEGIN

	set nocount on;

	-- get shopping cart id and whether or not we should try to remove custom brace measurements
	-- NOTE: only time custom brace measurements are deleted is when a custom brace is deleted
	-- from the cart. Deletion of an accessory should not affect the measurement data.
	declare @ShoppingCartID int;
	declare @ShouldRemoveCustomBraceMeasurements bit;
	select top 1
		@ShoppingCartID=sci.ShoppingCartID
		,@ShouldRemoveCustomBraceMeasurements=cast(isnull(mcp.IsCustomBrace, 0) as bit)
	from
		ShoppingCartItem sci
		left join PracticeCatalogProduct pcp on pcp.PracticeCatalogProductID=sci.PracticeCatalogProductID
		left join MasterCatalogProduct mcp on mcp.MasterCatalogProductID=pcp.MasterCatalogProductID
	where
		sci.ShoppingCartItemID=@ShoppingCartItemID;
	
	-- remove item from cart
	delete from ShoppingCartItem where ShoppingCartItemID=@ShoppingCartItemID;
	
	-- remove brace measurements, if needed
	if @ShouldRemoveCustomBraceMeasurements=1
		delete from ShoppingCartCustomBrace where ShoppingCartID=@ShoppingCartID;

END
GO
