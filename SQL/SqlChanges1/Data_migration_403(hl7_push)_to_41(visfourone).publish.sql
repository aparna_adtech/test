begin transaction

set identity_insert FrankenVision.dbo.HCPCS on;

delete from FrankenVision.dbo.HCPCS;

insert into FrankenVision.dbo.HCPCS
(HCPCSID,Code,[Description])
select HCPCSID,Code,[Description] from BregVision_BETA.[dbo].[HCPCS]
;

set identity_insert FrankenVision.dbo.HCPCS off;

rollback

-- commit

/*
select * from FrankenVision.dbo.HCPCS
*/



begin transaction

truncate table FrankenEMR.dbo.xxSampleData;

insert into FrankenEMR.dbo.xxSampleData
select * from BregVisionEMR_BETA.dbo.xxSampleData;

rollback
-- commit

/*
select * from FrankenEMR.dbo.xxSampleData
*/

select @@trancount