USE [BregVision]
GO

/****** Object:  StoredProcedure [dbo].[usp_DispenseProductsNew]    Script Date: 4/6/2015 12:54:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Greg Ross
-- Create date: Aug. 16, 20076
-- Description:	Used to do all jobs related to dispensing products. Hits 4 tables (Product Inventory, PI_DispenseDetail, Dispense,DispenseDetail)
-- Modification: 2007-10-02 JB If PhysicianID = -then null.
-- Modification: 2011-09-14 MWS Added PatientEmail
-- Modification: 2014-12-04 BJ Added FitterID
-- =============================================
CREATE PROCEDURE [dbo].[usp_DispenseProductsNew]
	
	@UserID INT,
	@PracticeLocationID INT, 
	@ProductInventoryID INT,
	@PatientCode VARCHAR(255),
	@PracticeCatalogProductID INT,	
		
	@PhysicianID INT,
	@ICD9Code VARCHAR(50),
	@LRModifier VARCHAR(2),
	@IsMedicare BIT,
	@ABNForm BIT,
	@ActualChargeBilled SMALLMONEY,	
	@Quantity INT,
	@IsCashCollection BIT,
	@WholeSaleCost SMALLMONEY ,
	@DMEDeposit SMALLMONEY ,
	@DispenseIDIn INT,
	@DispenseDate DATETIME,
	@CreatedWithHandheld BIT = 0,
	@DispenseBatchID INT,
	@DispenseQueueID INT,
	@PatientEmail VARCHAR(50) = '',
	@DispenseID   INT  OUTPUT,
	@PatientFirstName VARCHAR(50)='', 
	@PatientLastName VARCHAR(50)='',
	@PracticePayerID INT,
	@Note VARCHAR(250),
	@IsCustomFit BIT,
	@FitterID INT

AS
BEGIN

	SET NOCOUNT ON;

       
--		set @UserID = 1
--		set @PracticeLocationID = 3
--		set @PatientCode = 'A12345'
--		set @PracticeCatalogProductID = 2
--		set @PhysicianID = 3
--		set @ActualChargeBilled = '30.00'
--		set @Quantity = 2
--		set @IsCashCollection = 1
--		set @ProductInventoryID = 1
--		set @WholeSaleCost = '41.50'
		
		DECLARE @RecordCount INT
        DECLARE @Err INT 
        DECLARE @TransactionCountOnEntry INT 
		Declare @DispenseDetailID int


SELECT  @TransactionCountOnEntry = @@TRANCOUNT
        SELECT  @Err = @@ERROR
        BEGIN TRANSACTION      	

     IF @Err = 0 
            BEGIN
				
   
        --PRINT @ReOrderQuanity 	
                SELECT  @RecordCount = COUNT(1)
                FROM    dbo.[Dispense]
                WHERE   DispenseID = @DispenseIDIn
                        AND IsActive = 1
                SET @Err = @@ERROR

            END

		IF @Err = 0 
            BEGIN
                IF ( @RecordCount = 0 ) 
				
					begin
						--Initial insert into Dispense Table
						insert into Dispense(PracticeLocationID,PatientCode,Total,dateDispensed, CreatedUserID, CreatedDate, CreatedWithHandheld, DispenseBatchID, PatientEmail, PatientFirstName, PatientLastName, PracticePayerID, Note, IsActive) 
						Values (@PracticeLocationID,@PatientCode,0,@DispenseDate,@UserID,getdate(),@CreatedWithHandheld, @DispenseBatchID, @PatientEmail, @PatientFirstName, @PatientLastName, 
						CASE WHEN (@PracticePayerID = -1 OR @PracticePayerID = 0) THEN NULL ELSE @PracticePayerID END ,@Note, 1)
						SELECT  @DispenseID = SCOPE_IDENTITY()
						--print @DispenseID
					END 
				ELSE 
					BEGIN
						set @DispenseID=@DispenseIDIn
						UPDATE Dispense
							SET
							 ModifiedUserID = @UserID,
							 ModifiedDate = GETDATE()
							WHERE DispenseID = @DispenseID
					END		
			END 
	SET @Err = @@ERROR	
	--Insert into  Dispense Detail
    iF @Err = 0 
            BEGIN
				
					declare @lineTotal smallmoney
					set @lineTotal = @ActualChargeBilled * @Quantity
--					print 'Line Total'
--					print @linetotal
					insert into DispenseDetail
					(DispenseID,
					PracticeCatalogProductID,
					PhysicianID,
					ICD9Code,
					LRModifier,
					IsMedicare,
					ABNForm, 
					ActualChargeBilled,
					DMEDeposit,
					Quantity,
					LineTotal,
					IsCashCollection,
					IsCustomFit,
					FitterID,
					DispenseQueueID,
					CreatedUserID,
					CreatedDate,
					IsActive)
					Values
					(@DispenseID,
					 @PracticeCatalogProductID,
					 
					 CASE WHEN (@PhysicianID = -1 OR @PhysicianID = 0) THEN NULL ELSE @PhysicianID END,  --IF physicianID IS -1 INSERT NULL.
					 @ICD9Code,
					 @LRModifier,
					 @IsMedicare,
					 @ABNForm,
					 @ActualChargeBilled,
					 @DMEDeposit, 
					 @Quantity,
					 @lineTotal,
					 @IsCashCollection,
					 @IsCustomFit,
					 @FitterID,
					 @DispenseQueueID,
					 @UserID,
					 GetDate(),
					 1)
					SELECT  @DispenseDetailID = SCOPE_IDENTITY()

	END

	SET @Err = @@ERROR	
	--Update Quantities in ProductInventory
    IF @Err = 0 
            BEGIN
				declare @QOHTemp int
				set @QOHTemp = (select QuantityOnHandPerSystem from ProductInventory where ProductInventoryID = @ProductInventoryID	)
--				print 'QOHTemp'				
--				print @QOHTemp
				set @QOHTemp = @QOHTemp - @Quantity
--				print 'Adjusted QOH'				
--				print @QOHTemp
				if @QOHTemp < 0 
					begin
						set @QOHTemp = 0
					end 
				
					UPDATE productinventory 
						SET
						 QuantityOnHandPerSystem=@QOHTemp,
						 ModifiedUserID=@UserID,
						 ModifiedDate = GETDATE()	
						 WHERE ProductInventoryID = @ProductInventoryID	

			END 

	SET @Err = @@ERROR	
	--Update/Insert date into ProductInventory_DispenseDetail
    IF @Err = 0 
            BEGIN
				INSERT INTO ProductInventory_DispenseDetail
					(ProductInventoryID, 
					 DispenseDetailID,
					 ActualWholesaleCost,
					 CreatedUserID,
					 CreatedDate,
					 IsActive)
					VALUES
					(@ProductInventoryID,
					 @DispenseDetailID,
					 @WholeSaleCost,
					 @UserID,
					 GETDATE(),
					 1)
					 
						
			END 
			SET @Err = @@ERROR	
                        IF @@TranCount > @TransactionCountOnEntry 
                            BEGIN

                                IF @Err = 0 
                                    BEGIN 
                                        COMMIT TRANSACTION
                                        RETURN @Err
                               -- PRINT @err
                                    END
                                ELSE 
                                    BEGIN
                                        ROLLBACK TRANSACTION
                                        RETURN @Err
                                -- PRINT @err
					--  Add any database logging here      
                                    END
								END


END
