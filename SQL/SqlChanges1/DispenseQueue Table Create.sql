USE [BregVision]
GO

/****** Object:  Table [dbo].[DispenseQueue]    Script Date: 11/10/2009 22:45:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DispenseQueue](
	[DispenseQueueID] [int] IDENTITY(1,1) NOT NULL,
	[PracticeLocationID] [int] NOT NULL,
	[ProductInventoryID] [int] NOT NULL,
	[PatientCode] [varchar](50) NULL,
	[PhysicianID] [int] NULL,
	[ICD9_Code] [varchar](50) NULL,
	[IsMedicare] [bit] NOT NULL,
	[ABNForm] [bit] NOT NULL,
	[Side] [varchar](2) NULL,
	[DMEDeposit] [money] NOT NULL,
	[QuantityToDispense] [int] NOT NULL,
	[DispenseDate] [datetime] NULL,
	[IsCashCollection] [bit] NOT NULL,
	[IsDispensed] [bit] NOT NULL,
	[BregVisionOrderID] [int] NULL,
	[CreatedUser] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedUser] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_DispenseQueue] PRIMARY KEY CLUSTERED 
(
	[DispenseQueueID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[DispenseQueue] ADD  CONSTRAINT [DF_DispenseQueue_IsMedicare]  DEFAULT ((0)) FOR [IsMedicare]
GO

ALTER TABLE [dbo].[DispenseQueue] ADD  CONSTRAINT [DF_DispenseQueue_ABMForm]  DEFAULT ((0)) FOR [ABNForm]
GO

ALTER TABLE [dbo].[DispenseQueue] ADD  CONSTRAINT [DF_DispenseQueue_DMEDeposit]  DEFAULT ((0)) FOR [DMEDeposit]
GO

ALTER TABLE [dbo].[DispenseQueue] ADD  CONSTRAINT [DF_DispenseQueue_QuantityToDispense]  DEFAULT ((1)) FOR [QuantityToDispense]
GO

ALTER TABLE [dbo].[DispenseQueue] ADD  CONSTRAINT [DF_DispenseQueue_IsCashCollection]  DEFAULT ((0)) FOR [IsCashCollection]
GO

ALTER TABLE [dbo].[DispenseQueue] ADD  CONSTRAINT [DF_DispenseQueue_IsDispensed]  DEFAULT ((0)) FOR [IsDispensed]
GO

ALTER TABLE [dbo].[DispenseQueue] ADD  CONSTRAINT [DF_DispenseQueue_CreatedUser]  DEFAULT ((1)) FOR [CreatedUser]
GO

ALTER TABLE [dbo].[DispenseQueue] ADD  CONSTRAINT [DF_DispenseQueue_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[DispenseQueue] ADD  CONSTRAINT [DF_DispenseQueue_ModifiedUser]  DEFAULT ((1)) FOR [ModifiedUser]
GO

ALTER TABLE [dbo].[DispenseQueue] ADD  CONSTRAINT [DF_DispenseQueue_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO

ALTER TABLE [dbo].[DispenseQueue] ADD  CONSTRAINT [DF_DispenseQueue_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO


