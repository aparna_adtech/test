use [BregVision]
go

if not exists (select * from dbo.syscolumns where id = object_id(N'Physcican') and name=(N'SortOrder'))
alter table [Physician] add [SortOrder] int DEFAULT 0 NOT NULL
GO