USE [BregVision_BETA]
GO
/****** Object:  StoredProcedure [dbo].[usp_Practice_Clinician_Insert]    Script Date: 11/23/2015 11:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* ------------------------------------------------------------
   PROCEDURE:    usp_Practice_Clinician_Insert
   
   Description:   Inserts a Clinician for a Practice. 
				Inserts a record into table Contact
				and then insert a record into the table Clinician
				given the PracticeID
				
				Returns: the ClinicianID and the ContactID   

   AUTHOR:       Basil Juan 11/5/2014 4:00 PM
   
   Modifications:  
   ------------------------------------------------------------ */  
   
ALTER PROCEDURE [dbo].[usp_Practice_Clinician_Insert]
(
	  @PracticeID					 INT
    , @ClinicianID					 INT	OUTPUT
    , @ContactID                     INT	OUTPUT
    , @Salutation                         VARCHAR(10) = NULL
    , @FirstName                     VARCHAR(50)
    , @MiddleName                    VARCHAR(50) = NULL
    , @LastName                      VARCHAR(50)
    , @Suffix                        VARCHAR(10) = NULL
    , @CreatedUserID                 INT
    , @SortOrder					 INT = 0
    , @Pin							 VARCHAR(4) = null
	, @IsProvider					 Bit = 0
	, @IsFitter						 Bit = 0
    , @CloudConnectID                VARCHAR(50)

)
AS
BEGIN

	DECLARE @TransactionCountOnEntry    INT,       -- Transaction Count before the transaction begins
			@Err						INT        --  holds the @@Error code returned by SQL Server

			
	IF NULLIF(@Pin, '') IS NULL
		set @Pin = NULL
		
			
	SELECT @Err = @@ERROR
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @Err = 0
	BEGIN    
		SELECT @TransactionCountOnEntry = @@TRANCOUNT
		BEGIN TRANSACTION
	END        

	IF @Err = 0
	BEGIN

		INSERT
		INTO dbo.Contact
		(
			  Salutation
			, FirstName
			, MiddleName
			, LastName
			, Suffix
			, CreatedUserID
			, IsActive
		)
		VALUES
		(
			  @Salutation
			, @FirstName
			, @MiddleName
			, @LastName
			, @Suffix
			, @CreatedUserID
			, 1
		)

		SET @Err = @@ERROR

	END
	IF @Err = 0
	BEGIN

		SELECT @ContactID = SCOPE_IDENTITY()

	END
	IF @Err = 0
	BEGIN

		INSERT
		INTO dbo.Clinician
		(
			  PracticeID
			, ContactID
			, CreatedUserID
			, IsActive
			, SortOrder
			, Pin
			, IsProvider
			, IsFitter
			, CloudConnectId
		)
		VALUES
		(
			  @PracticeID
			, @ContactID
			, @CreatedUserID
			, 1
			, @SortOrder
			, @Pin 
			, @IsProvider
			, @IsFitter
			, @CloudConnectID
		)

		SET @Err = @@ERROR
		SELECT @ClinicianID = SCOPE_IDENTITY()

	END

	IF @@TranCount > @TransactionCountOnEntry
	BEGIN

		IF @Err = 0
			COMMIT TRANSACTION
		ELSE
			ROLLBACK TRANSACTION
			--  Add any database logging here      
	END
			
	RETURN @Err

END
