USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteSuperbill]    Script Date: 03/12/2009 23:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author: Mike Sneen>
-- Create date: <Create Date: 02/28/09>
-- Description:	<Description: Used to get all a superbill>
-- modification:
--  [dbo].[usp_DeleteSuperbill]  

-- =============================================
Create PROCEDURE [dbo].[usp_DeleteSuperbill]  --3 --10  --  _With_Brands
	@SuperBillProductID int
	
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
--        SET NOCOUNT ON ;
     
     Update SuperBillProduct 
     Set IsActive = 0
     Where
     /*if the superbillproductid is for a superbill item, this will also set ISActive to 0 for any add on items
       if the id is for an add on item, it should just set that item's ISActive to o*/
		SuperBillProductID = @SuperBillProductID
		OR
		SuperBillProductID_FK = @SuperBillProductID
		

    END

--Exec usp_DeleteSuperbill 3, 1851



