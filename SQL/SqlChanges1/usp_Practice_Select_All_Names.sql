USE [BregVision_STG]
GO
/****** Object:  StoredProcedure [dbo].[usp_Practice_Select_All_Names]    Script Date: 11/22/2014 8:56:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Practice_Select_All_Names
   
   Description:  Selects all records from the table dbo.Practice
   				returns the PracticeID and the PracticeName.
   
   AUTHOR:       John Bongiorni 7/25/2007 9:45:21 PM
   
   Modifications:  Added EMRIntegrationPullEnabled				Mike Sneen		2/15/2013
                   Added FaxDispenseReceiptEnabled				Ysi             09/07/2013
   ------------------------------------------------------------ */  -------------------SELECT ALL----------------

ALTER PROCEDURE [dbo].[usp_Practice_Select_All_Names]
AS
BEGIN
	DECLARE @Err INT
	SELECT     
		P.PracticeID, 
		P.PracticeName, 
		P.IsOrthoSelect, 
		P.IsVisionLite, 
		P.IsActive, 
		PA.PracticeApplicationID,
		P.PaymentTypeID,
		PT.PaymentTypeName,
		P.BillingStartDate,
		BVPC.PracticeCode,
		P.CameraScanEnabled,
		P.EMRIntegrationPullEnabled,
		P.FaxDispenseReceiptEnabled,
		P.IsSecureMessagingInternalEnabled,
		P.IsSecureMessagingExternalEnabled
	FROM         
		Practice P 
        LEFT OUTER JOIN PracticeApplication PA 
			ON P.PracticeID = PA.PracticeID
		LEFT OUTER JOIN PaymentType PT
			ON P.PaymentTypeID = PT.PaymentTypeID
		LEFT OUTER JOIN BregVisionPracticeCode BVPC
			ON P.PracticeID = BVPC.PracticeID
	
	ORDER BY IsActive desc, PracticeName
	
	SET @Err = @@ERROR

	RETURN @Err
END

