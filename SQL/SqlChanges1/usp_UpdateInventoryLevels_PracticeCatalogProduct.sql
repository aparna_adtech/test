USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateInventoryLevels_PracticeCatalogProduct]    Script Date: 10/14/2014 17:20:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Greg Ross
-- Create date: 08/07/08
-- Description:	Used to update inventory levels based stocking units changed
--Simply multiples QOH by stocking units to reflect change in quantity
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateInventoryLevels_PracticeCatalogProduct]
		@practiceCatalogProductID int,
		@PracticeID int,
		@StockingUnits int,
		@UserID int = 1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- check for valid input
	IF (@StockingUnits<1)
	BEGIN
		RAISERROR(N'StockingUnits is less than 1. Invalid value.', 10, 1)
		RETURN
	END
	
	-- get old value for comparison
	DECLARE @originalvalue int;
	SELECT @originalvalue=StockingUnits
	  FROM PracticeCatalogProduct
	 WHERE PracticeCatalogProductID=@practiceCatalogProductID;

	-- update database table
	UPDATE PracticeCatalogProduct
	   SET StockingUnits=@StockingUnits, ModifiedUserID=@UserID, ModifiedDate=GETDATE()
	 WHERE PracticeCatalogProductID=@practiceCatalogProductID;

	-- update quantity on hand if stocking units value has changed
	IF (@originalvalue<>@StockingUnits)
		UPDATE productinventory 
		   SET QuantityOnHandPerSystem=(SELECT QuantityOnHandPerSystem * @StockingUnits)
		 WHERE practiceCatalogProductID=@practiceCatalogProductID
		       AND practicelocationid IN (SELECT PracticeLocationID FROM PracticeLocation WHERE PracticeID=@PracticeID);

 
--select * from Productinventory
--where practiceCatalogProductID=@practiceCatalogProductID
--and practicelocationid 
--in (select practicelocationid from practicelocation where practiceid=@PracticeID)



END

GO
