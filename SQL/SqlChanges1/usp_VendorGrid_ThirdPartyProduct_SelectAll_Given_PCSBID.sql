USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_VendorGrid_ThirdPartyProduct_SelectAll_Given_PCSBID]    Script Date: 5/7/2015 11:14:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
This replaces usp_ThirdPartyProduct_SelectAll_Given_PCSBID
This needs to pull from the PracticeCatalogProduct table!!!! as much as possible, 
   since updates have been made to the pcp table but not the thirdpartysupplierbrand table.
   select * from practicecatalogproduct where thirdpartyProductID is not null and isactive = 1
*/


CREATE PROCEDURE [dbo].[usp_VendorGrid_ThirdPartyProduct_SelectAll_Given_PCSBID] --337
		@PracticeCatalogSupplierBrandID INT 
AS
BEGIN

	SELECT 
		   TPP.ThirdPartyProductID,
		   TPP.PracticeCatalogSupplierBrandID,
		   PCP.PracticeCatalogProductID,
--		   TPP.[Name],
		   TPP.ShortName,
		   TPP.Code,
		   ISNULL(TPP.Packaging, '') AS Packaging,
		   ISNULL(TPP.LeftRightSide, '')  AS Side,
		   ISNULL(TPP.Size, '') AS Size,
		   ISNULL(TPP.Color, '') AS Color,
		   ISNULL(TPP.Gender, '') AS Gender,
		   CAST(ISNULL(PCP.WholesaleCost, 0) AS DECIMAL(8,2)) AS WholesaleCost   --  Get from PracticeCatalog
		   , dbo.ConcatHCPCS(PCP.PracticeCatalogProductID) AS HCPCSString -- JB 20071004 Added Column for UDF Concat of HCPCS
					
														
			, CASE (SELECT COUNT(1) 
			FROM productinventory AS PI WITH (NOLOCK) 
			WHERE PI.practiceCatalogProductID = PCP.practiceCatalogProductID
			) WHEN 0 THEN 1 ELSE 0 END AS IsDeletable   --  JB  20080225  Added Column for "Deletable" PCPs

		   , TPP.IsDiscontinued
		   , '' AS UserMessage
		   , 0 AS RowNumber
		   , UPCCode = (Select Top 1 upc.Code from UPCCode upc where upc.ThirdPartyProductID = TPP.ThirdPartyProductID )
--		   TPP.Description,
--		   TPP.DateDiscontinued,
--		   TPP.OldMasterCatalogProductID,
--		   TPP.CreatedUserID,
--		   TPP.CreatedDate,
--		   TPP.ModifiedUserID,
--		   TPP.ModifiedDate,
--		   TPP.IsActive,
--		   TPP.Source 
	FROM dbo.ThirdPartyProduct AS TPP WITH (NOLOCK)
	INNER JOIN dbo.PracticeCatalogProduct AS PCP WITH (NOLOCK)
		ON TPP.ThirdPartyProductID = PCP.ThirdPartyProductID
		AND PCP.IsActive = 1
	WHERE TPP.PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID
		AND TPP.IsActive = 1
		AND PCP.IsActive = 1
	ORDER BY
		   TPP.ShortName,
		   TPP.Code,
		   TPP.Packaging,
		   TPP.LeftRightSide,
		   TPP.Size,
		   TPP.Gender,
		   TPP.Color

END
