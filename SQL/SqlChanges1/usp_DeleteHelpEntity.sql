USE [BregVision]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author: Tom Thomas>
-- Create date: <Create Date: 03/12/10>
-- Description:	<Description: Delete from HelpEntityTree only if no children exist in HelpEntityTree
--    Optionally: Delete from Respective HelpEntity for example HelpArticle only if there are no references 
--    to it in the HelpEntityTree
--    for instance, it could be in use as a related article or belong to another group.
-- =============================================
ALTER PROCEDURE [dbo].[usp_DeleteHelpEntity]  
	@Id int OUT
	,@EntityId int 
	,@EntityTypeId int
	,@RemoveEntity bit
AS 

BEGIN
	
	SET NOCOUNT ON;
	
	IF EXISTS (SELECT * FROM HelpEntityTree WHERE ParentId = @Id) 
	BEGIN 
		RAISERROR('This node contains children and cannot be deleted.', 1, 101)
	END
	ELSE 
	BEGIN 
		--Delete from HelpEntityTree but necesseraly required to be here if we are cleaning up orphaned entities in their respective table in the next step
		Delete from HelpEntityTree WHERE Id = @Id
		
		IF @RemoveEntity=1 --if it where 0 then the user would just be removing the reference in the tree, for instance a related article removal
		BEGIN
			--we leave it if it is being referenced elsewhere in the HelpEntityTree
			IF NOT EXISTS (SELECT * FROM HelpEntityTree WHERE ParentId = @Id) 
			BEGIN
				IF @EntityTypeId = 1 --Group
				BEGIN
					Delete from HelpGroups Where Id = @EntityId
				END
				ELSE IF @EntityTypeId = 2 --Article
				BEGIN
					Delete from HelpArticles Where Id = @EntityId
				END
				ELSE IF @EntityTypeId = 4 --Media
				BEGIN
					Delete from HelpMedia Where Id = @EntityId
				END
			END
		END
	END 
END
    
    






