USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_Contact_Select_One_ByParams]    Script Date: 7/21/2015 11:22:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Contact_Select_One_ByParams
   
   Description:  Selects a record from table dbo.Contact
   				   and puts values into parameters.
   
   AUTHOR:       John Bongiorni 7/14/2007 8:05:01 PM
   
   Modifications:  
   ------------------------------------------------------------ */  
ALTER PROCEDURE [dbo].[usp_Contact_Select_One_ByParams]
(
      @ContactID                     INT
    , @Title                         VARCHAR(50)    OUTPUT
    , @Salutation                    VARCHAR(10)    OUTPUT
    , @FirstName                     VARCHAR(50)    OUTPUT
    , @MiddleName                    VARCHAR(50)    OUTPUT
    , @LastName                      VARCHAR(50)    OUTPUT
    , @Suffix                        VARCHAR(10)    OUTPUT
    , @EmailAddress                  VARCHAR(100)   OUTPUT
    , @PhoneWork                     VARCHAR(25)    OUTPUT
    , @PhoneWorkExtension            VARCHAR(10)    OUTPUT
    , @PhoneCell                     VARCHAR(25)    OUTPUT
    , @PhoneHome                     VARCHAR(25)    OUTPUT
    , @Fax                           VARCHAR(25)    OUTPUT
    , @IsSpecificContact             BIT		    OUTPUT
)
AS
BEGIN
	DECLARE @Err INT

	SELECT
          @ContactID = ContactID
        , @Title = Title
        , @Salutation = Salutation
        , @FirstName = FirstName
        , @MiddleName = MiddleName
        , @LastName = LastName
        , @Suffix = Suffix
        , @EmailAddress = EmailAddress
        , @PhoneWork = PhoneWork
        , @PhoneWorkExtension = PhoneWorkExtension
        , @PhoneCell = PhoneCell
        , @PhoneHome = PhoneHome
        , @Fax = Fax
		, @IsSpecificContact = IsSpecificContact
	FROM dbo.Contact
	WHERE 
		ContactID = @ContactID
		AND IsActive = 1

	SET @Err = @@ERROR

	RETURN @Err
End
