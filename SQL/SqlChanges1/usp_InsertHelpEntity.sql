USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetHelpEntityTree]    Script Date: 03/11/2010 15:48:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author: Tom Thomas>
-- Create date: <Create Date: 03/05/11>
-- Description:	<Description: Insert HelpEntity by EntityType then insert into HelpEntityTree. Return EntityId and Id as out params
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertHelpEntity]  
	@Id int OUT
	,@EntityId int OUT
	,@ParentId int
	,@EntityTypeId int
	,@SortOrder int = 0
	,@Title nvarchar(200)
	,@Description nvarchar(500) = null
	,@Url nvarchar(200)= null
	
AS 
    BEGIN
	
	SET NOCOUNT ON;
	
	IF @EntityTypeId = 1 --Group
	BEGIN
		insert into HelpGroups ( HelpEntityTypeId,Title )  values (@EntityTypeId,@Title)
		Set @EntityId = @@IDENTITY
	END
	ELSE IF @EntityTypeId = 2 --Article
	BEGIN
		insert into HelpArticles ( HelpEntityTypeId,Title,[Description] )  values ( @EntityTypeId,@Title,@Description)
		Set @EntityId = @@IDENTITY
	END
	ELSE IF @EntityTypeId = 4 --Media
	BEGIN
		insert HelpMedia ( HelpEntityTypeId,Title,Description,Url )  values ( @EntityTypeId,@Title,@Description, @Url)
		Set @EntityId = @@IDENTITY
	END
	
	insert into HelpEntityTree ( ParentId,EntityId,EntityTypeId,SortOrder )  values (@ParentId,@EntityId,@EntityTypeId,@SortOrder)
	Set @Id = @@IDENTITY
	
	END
    
    






