USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_PracticeLocation_Select_Contact_ByParams]    Script Date: 7/21/2015 1:06:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocation_Select_Contact_ByParams
   
   Description:  Selects a Practice Location Contact from tables dbo.Practice
   				   and dbo.Contact puts values into a parameter.
   
   AUTHOR:       John Bongiorni 8/03/2007 10:27 PM
   
   Modifications:  
   ------------------------------------------------------------ */  ---------------------select params-----------

ALTER PROCEDURE [dbo].[usp_PracticeLocation_Select_Contact_ByParams]
(
	  @PracticeLocationID            INT
    , @ContactID                     INT			OUTPUT
    , @Title                         VARCHAR(50)    OUTPUT
    , @Salutation                    VARCHAR(10)    OUTPUT
    , @FirstName                     VARCHAR(50)    OUTPUT
    , @MiddleName                    VARCHAR(50)    OUTPUT
    , @LastName                      VARCHAR(50)    OUTPUT
    , @Suffix                        VARCHAR(10)    OUTPUT
    , @EmailAddress                  VARCHAR(100)   OUTPUT
    , @PhoneWork                     VARCHAR(25)    OUTPUT
    , @PhoneWorkExtension            VARCHAR(10)    OUTPUT
    , @PhoneCell                     VARCHAR(25)    OUTPUT
    , @PhoneHome                     VARCHAR(25)    OUTPUT
    , @Fax                           VARCHAR(25)    OUTPUT
    , @IsSpecificContact             BIT		    OUTPUT
)
AS
BEGIN
	DECLARE @Err INT

	SELECT
		  @ContactID = C.ContactID
        , @Title = C.Title
        , @Salutation = C.Salutation
        , @FirstName = C.FirstName
        , @MiddleName = C.MiddleName
        , @LastName = C.LastName
        , @Suffix = C.Suffix
        , @EmailAddress = C.EmailAddress
        , @PhoneWork = C.PhoneWork
        , @PhoneWorkExtension = C.PhoneWorkExtension
        , @PhoneCell = C.PhoneCell
        , @PhoneHome = C.PhoneHome
        , @Fax = C.Fax
		, @IsSpecificContact = C.IsSpecificContact
	FROM dbo.Contact AS C
	INNER JOIN dbo.PracticeLocation AS PL
		ON C.ContactID = PL.ContactID
		
	WHERE 
		PL.PracticeLocationID = @PracticeLocationID
		AND C.IsActive = 1

	SET @Err = @@ERROR

	RETURN @Err
End
