USE [BregVision]
GO

/****** Object:  Table [dbo].[SetupStatus]    Script Date: 03/24/2010 19:17:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[SetupStatus](
	[PracticeID] [int] NOT NULL,
	[PracticeLocationID] [int] NOT NULL,
	[UserControlName] [varchar](100) NOT NULL,
	[PageStatus] [int] NOT NULL,
 CONSTRAINT [PK_SetupStatus_1] PRIMARY KEY CLUSTERED 
(
	[PracticeID] ASC,
	[PracticeLocationID] ASC,
	[UserControlName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 Incomplete, 1 Complete' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SetupStatus', @level2type=N'COLUMN',@level2name=N'PageStatus'
GO

ALTER TABLE [dbo].[SetupStatus] ADD  CONSTRAINT [DF_Table_1_SetupStatus]  DEFAULT ((0)) FOR [PageStatus]
GO



/****** Object:  Table [dbo].[SetupStatusTitle]    Script Date: 03/24/2010 19:18:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[SetupStatusTitle](
	[UserControlName] [varchar](100) NOT NULL,
	[PageTitle] [varchar](100) NULL,
	[DisplayOrder] [int] NOT NULL,
	[IsPracticeLocation] [bit] NOT NULL,
 CONSTRAINT [PK_SetupStatusTitle_1] PRIMARY KEY CLUSTERED 
(
	[UserControlName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

ALTER TABLE [dbo].[SetupStatusTitle] ADD  CONSTRAINT [DF_SetupStatusTitle_DisplayOrder]  DEFAULT ((0)) FOR [DisplayOrder]
GO

ALTER TABLE [dbo].[SetupStatusTitle] ADD  CONSTRAINT [DF_SetupStatusTitle_IsPracticeLocation]  DEFAULT ((0)) FOR [IsPracticeLocation]
GO

--This needs to go to bimsdev and to Vision Server
 insert SetupStatusTitle ( UserControlName,PageTitle,DisplayOrder,IsPracticeLocation )  select 'ASP.admin_usercontrols_practice_practicecreditapp_ascx','Credit Application',0,0
 
 insert SetupStatusTitle ( UserControlName,PageTitle,DisplayOrder,IsPracticeLocation )  select 'ASP.admin_usercontrols_practice_practicebillingaddress_ascx','Billing Address',1,0
 insert SetupStatusTitle ( UserControlName,PageTitle,DisplayOrder,IsPracticeLocation )  select 'ASP.admin_usercontrols_practice_practicecatalog_ascx','Catalog',6,0
 insert SetupStatusTitle ( UserControlName,PageTitle,DisplayOrder,IsPracticeLocation )  select 'ASP.admin_usercontrols_practice_practicecontact_ascx','Contacts',2,0
 insert SetupStatusTitle ( UserControlName,PageTitle,DisplayOrder,IsPracticeLocation )  select 'ASP.admin_usercontrols_practice_practiceicd9s_ascx','ICD-9s',8,0
 insert SetupStatusTitle ( UserControlName,PageTitle,DisplayOrder,IsPracticeLocation )  select 'ASP.admin_usercontrols_practice_practicelocations_ascx','Locations',9,0
 insert SetupStatusTitle ( UserControlName,PageTitle,DisplayOrder,IsPracticeLocation )  select 'ASP.admin_usercontrols_practice_practicephysicians_ascx','Physicians',3,0
 insert SetupStatusTitle ( UserControlName,PageTitle,DisplayOrder,IsPracticeLocation )  select 'ASP.admin_usercontrols_practice_practiceproducts_ascx','Products',5,0
 insert SetupStatusTitle ( UserControlName,PageTitle,DisplayOrder,IsPracticeLocation )  select 'ASP.admin_usercontrols_practice_practicesuppliers_ascx','Suppliers',4,0
 insert SetupStatusTitle ( UserControlName,PageTitle,DisplayOrder,IsPracticeLocation )  select 'ASP.admin_usercontrols_practice_practiceusers_ascx','Users',7,0
 insert SetupStatusTitle ( UserControlName,PageTitle,DisplayOrder,IsPracticeLocation )  select 'ASP.admin_usercontrols_practicelocation_practicelocationaddress_ascx','Location Addresses',10,1
 insert SetupStatusTitle ( UserControlName,PageTitle,DisplayOrder,IsPracticeLocation )  select 'ASP.admin_usercontrols_practicelocation_practicelocationcontacts_ascx','Location Contacts',11,1
 insert SetupStatusTitle ( UserControlName,PageTitle,DisplayOrder,IsPracticeLocation )  select 'ASP.admin_usercontrols_practicelocation_practicelocationdispensementmodifications_ascx','Dispensement Modifications',16,1
 insert SetupStatusTitle ( UserControlName,PageTitle,DisplayOrder,IsPracticeLocation )  select 'ASP.admin_usercontrols_practicelocation_practicelocationemails_ascx','Location Emails',13,1
 insert SetupStatusTitle ( UserControlName,PageTitle,DisplayOrder,IsPracticeLocation )  select 'ASP.admin_usercontrols_practicelocation_practicelocationinventorylevels_ascx','Inventory Levels',15,1
 insert SetupStatusTitle ( UserControlName,PageTitle,DisplayOrder,IsPracticeLocation )  select 'ASP.admin_usercontrols_practicelocation_practicelocationphysicians_ascx','Location Physicians',12,1
 insert SetupStatusTitle ( UserControlName,PageTitle,DisplayOrder,IsPracticeLocation )  select 'ASP.admin_usercontrols_practicelocation_practicelocationsuppliercodes_ascx','Supplier Codes',14,1
