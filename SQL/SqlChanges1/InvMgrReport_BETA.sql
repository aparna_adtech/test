USE BregVision_stg
SELECT 
'INV'									as     'DOCUMENT TYPE',
'Import from Vision'					AS     'DESCRIPTION',
'Vision '  
+ CONVERT(VARCHAR, DATEPART(mm,GETDATE()))							
+ CONVERT(VARCHAR, DATEPART(dd,GETDATE()))							
+ CONVERT(VARCHAR, DATEPART(yyyy,GETDATE()))							
										AS     'BATCH ID',
CONVERT(VARCHAR(10), inv.Date,101)		as     'DOC. DATE',
CONVERT(VARCHAR(10), getDate()	,101)	as     'POSTING DATE',
COALESCE(
	NULLIF(
	so.CustomPurchaseOrderCode,''
	)
	,CAST(
	so.PurchaseOrder AS VARCHAR(75)))	AS		'PO NUMBER',
COALESCE(mcs.SupplierShortName,
	tps.SupplierShortName)				AS	'VENDOR ID',
'MAIN'									AS	'VENDOR ADDRESS ID',
inv.Number								AS	'INVOICE NUMBER',
inv.Total								AS	'INVOICE TOTAL',
COALESCE(
	ploc.GeneralLedgerNumber,'N/A')				AS	'LOCATION GL#',
COALESCE(p.GeneralLedgerNumber,'N/A')			AS	'PAYABLE GL#'
--,
--p.PracticeID,
--p.PracticeName,
--inv.InvoiceID

  FROM [dbo].[Invoice]					inv
  left join dbo.MasterCatalogSupplier   mcs				on	mcs.MasterCatalogSupplierID = inv.MasterCatalogSupplierID
  left join dbo.ThirdPartySupplier      tps				on	tps.ThirdPartySupplierID =    inv.ThirdPartySupplierID            
  JOIN dbo.SupplierOrder				so				ON	so.SupplierOrderID = 
	(SELECT TOP 1 soli.SupplierOrderID FROM dbo.SupplierOrder   so1
		JOIN dbo.InvoiceLineItem              invLineItem	on	invLineItem.InvoiceID = inv.InvoiceID
		join dbo.SupplierOrderLineItem        soli			ON	soli.SupplierOrderLineItemID = invLineItem.SupplierOrderLineItemID
		ORDER BY invLineItem.InvoiceLineItemID
	)
  JOIN dbo.BregVisionOrder				bvo				ON	bvo.BregVisionOrderID = so.BregVisionOrderID
  JOIN dbo.PracticeLocation				ploc			ON	ploc.PracticeLocationID = bvo.PracticeLocationID
  JOIN dbo.Practice						p				ON	p.PracticeID = inv.PracticeID
  WHERE inv.PracticeID = 254-- 302 --
  order by inv.Date
