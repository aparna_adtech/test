CREATE trigger [dbo].[trg_PracticeCatalogSupplierBrand_Inserted] ON dbo.PracticeCatalogSupplierBrand
    FOR INSERT
AS
    begin

		INSERT INTO [BregVision].[dbo].[PracticeCatalogSupplierBrand_Audit]
				   ([PracticeCatalogSupplierBrandID]
				   ,[PracticeID]
				   ,[MasterCatalogSupplierID]
				   ,[ThirdPartySupplierID]
				   ,[SupplierName]
				   ,[SupplierShortName]
				   ,[BrandName]
				   ,[BrandShortName]
				   ,[IsThirdPartySupplier]
				   ,[Sequence]
				   ,[CreatedUserID]
				   ,[CreatedDate]
				   ,[ModifiedUserID]
				   ,[ModifiedDate]
				   ,[IsActive]
				   ,[SupplierWarrantyID]
				   ,[AuditAction]
				   ,[AuditCreatedDate])
		SELECT
					[PracticeCatalogSupplierBrandID]
				   ,[PracticeID]
				   ,[MasterCatalogSupplierID]
				   ,[ThirdPartySupplierID]
				   ,[SupplierName]
				   ,[SupplierShortName]
				   ,[BrandName]
				   ,[BrandShortName]
				   ,[IsThirdPartySupplier]
				   ,[Sequence]
				   ,[CreatedUserID]
				   ,[CreatedDate]
				   ,[ModifiedUserID]
				   ,[ModifiedDate]
				   ,[IsActive]
				   ,[SupplierWarrantyID]
				   ,'Inserted'
				   ,GETDATE()
		FROM
					inserted;
  
    END

