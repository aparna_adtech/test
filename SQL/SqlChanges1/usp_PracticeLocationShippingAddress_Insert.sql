USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_PracticeLocationShippingAddress_Insert]    Script Date: 04/21/2012 15:13:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocationShippingAddress_Insert
   
   Description:  Inserts a record into table Address
				  , Gets the new AddressID
				  , and Inserts a record into the PracticeLocationShippingAddress table.
   
   AUTHOR:       John Bongiorni 8/06/2007 7:30 PM
   
   Modifications:  
   ------------------------------------------------------------ */  
   
ALTER PROCEDURE [dbo].[usp_PracticeLocationShippingAddress_Insert]
(
	  @PracticeLocationID			 INT
    , @AddressID                     INT = NULL OUTPUT
	, @AttentionOf					 VARCHAR(50) = NULL
    , @AddressLine1                  VARCHAR(50)
    , @AddressLine2                  VARCHAR(50) = NULL
    , @City                          VARCHAR(50)
    , @State                         CHAR(2)
    , @ZipCode                       CHAR(5)
    , @ZipCodePlus4                  CHAR(4) = NULL
    , @CreatedUserID                 INT
    , @OracleCode					 VARCHAR(10) = NULL
    , @IsOracleCodeValid			 Bit = 0
)
AS
BEGIN

	DECLARE @TransactionCountOnEntry    INT,       -- Transaction Count before the transaction begins
			@Err						INT        --  holds the @@Error code returned by SQL Server

	SELECT @Err = @@ERROR
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @Err = 0
	BEGIN    
		SELECT @TransactionCountOnEntry = @@TRANCOUNT
		BEGIN TRANSACTION
	END        

--	IF @Err = 0
--	BEGIN
--
--		UPDATE dbo.Practice
--		SET IsShippingCentralized = @IsShippingCentralized
--		WHERE PracticeLocationID = @PracticeLocationID 
--		
--		SET @Err = @@ERROR
--
--	END

	IF @Err = 0
	BEGIN

		INSERT
		INTO dbo.Address
		(
			  AddressLine1
			, AddressLine2
			, City
			, State
			, ZipCode
			, ZipCodePlus4
			, CreatedUserID
			, CreatedDate
			, IsActive
		)
		VALUES
		(
			  @AddressLine1
			, @AddressLine2
			, @City
			, @State
			, @ZipCode
			, @ZipCodePlus4
			, @CreatedUserID
			, GETDATE()
			, 1
		)

		SET @Err = @@ERROR
	END

	IF @Err = 0
	BEGIN

		SELECT @AddressID = SCOPE_IDENTITY()

		SELECT @Err = @@ERROR
	END


	IF @Err = 0
	BEGIN

		INSERT
		INTO dbo.PracticeLocationShippingAddress
		(
			  PracticeLocationID
			, AddressID
			, AttentionOf
			, BregOracleID
			, BregOracleIDInvalid
			, CreatedUserID
			, CreatedDate
			, IsActive
		)
		VALUES
		(
			  @PracticeLocationID
			, @AddressID
			, @AttentionOf
			, @OracleCode
			, ~@IsOracleCodeValid --mws Note Bitwise Shift (~)
			, @CreatedUserID
			, GETDATE()
			, 1
		)

		SELECT @Err = @@ERROR
	END


	IF @@TranCount > @TransactionCountOnEntry
	BEGIN

		IF @Err = 0
			COMMIT TRANSACTION
		ELSE
			ROLLBACK TRANSACTION
			--  Add any database logging here      
	END
			
		RETURN @Err
END


