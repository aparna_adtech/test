USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_PracticeBillingAddress_Update]    Script Date: 04/22/2012 14:55:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeBillingAddress_Update
   
   Description:  Update a record in the table Address
				  and update the record in the table PracticeBillingAddress 
   
   AUTHOR:       John Bongiorni 7/16/2007 5:07:54 PM
   
   Modifications:  
   ------------------------------------------------------------ */  
   
ALTER PROCEDURE [dbo].[usp_PracticeBillingAddress_Update]
(
	  @PracticeID					 INT
    , @AddressID                     INT
	, @AttentionOf					 VARCHAR(50)    	 
	, @AddressLine1                  VARCHAR(50)
	, @AddressLine2                  VARCHAR(50)
	, @City                          VARCHAR(50)
	, @State                         CHAR(2)
	, @ZipCode                       CHAR(5)
	, @ZipCodePlus4                  CHAR(4)
	, @ModifiedUserID                INT
	, @OracleCode					 VARCHAR(10) = ''
	, @OracleCodeValid				 Bit = 0
)
AS
BEGIN

	DECLARE @TransactionCountOnEntry    INT        -- Transaction Count before the transaction begins
		  , @Err						INT        --  holds the @@Error code returned by SQL Server

	SELECT @Err = @@ERROR
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @Err = 0
	BEGIN    
		SELECT @TransactionCountOnEntry = @@TRANCOUNT
		BEGIN TRANSACTION
	END        

	IF @Err = 0
	BEGIN	

		UPDATE dbo.Address
		SET
			  AddressLine1 = @AddressLine1
			, AddressLine2 = @AddressLine2
			, City = @City
			, State = @State
			, ZipCode = @ZipCode
			, ZipCodePlus4 = @ZipCodePlus4
			, ModifiedUserID = @ModifiedUserID
			, ModifiedDate = GETDATE()

		WHERE 
			AddressID = @AddressID
	
			SET @Err = @@ERROR
     
	END

	IF @Err = 0
	BEGIN

		UPDATE dbo.PracticeBillingAddress
		SET
			  AttentionOf = @AttentionOf
			, ModifiedUserID = @ModifiedUserID
			, ModifiedDate = GETDATE()
			, BregOracleID = @OracleCode
			, BregOracleIDInvalid = ~@OracleCodeValid --mws note negation(~)

		WHERE 
			    PracticeID = @PracticeID 
			AND AddressID  = @AddressID
		
		SET @Err = @@ERROR

	END
	
	-- JB Modified 20070818  to update all practice location billing addresses when the practice billing address is updated.	
	--  uPDATE ALL Practice Location if Centralized.
	DECLARE @IsBillingCentralized BIT	
	SELECT @IsBillingCentralized = IsBillingCentralized FROM dbo.Practice WHERE PracticeID = @PracticeID
	
--	SELECT TOP 1 * FROM dbo.Practice 
	
	
	IF ( @IsBillingCentralized = 1 )
		begin
			EXEC dbo.usp_PracticeLocationBillingAddresses_Update_From_Centralized_PracticeBillingAddress  
					@PracticeID	= @PracticeID
					, @UserID	= @ModifiedUserID
		end
	ELSE
		begin
			EXEC dbo.usp_PracticeLocationBillingAddresses_Update_From_Decentralized_PracticeBillingAddress
			  @PracticeID = @PracticeID
			, @UserID	= @ModifiedUserID

		end	



	IF @@TranCount > @TransactionCountOnEntry
	BEGIN

		IF @Err = 0
			COMMIT TRANSACTION
		ELSE
			ROLLBACK TRANSACTION
			--  Add any database logging here      
	END
			
	RETURN @Err

END


