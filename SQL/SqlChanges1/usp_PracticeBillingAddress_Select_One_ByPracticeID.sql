USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_PracticeBillingAddress_Select_One_ByPracticeID]    Script Date: 04/22/2012 14:37:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeBillingAddress_Select_One_ByPracticeID
   
   Description:  Input: PracticeID
				 Selects a record from table dbo.Address, and also selects the
					PracticeBillingAddress.AttentionOf column
   				     and puts values into parameters
   
   AUTHOR:       John Bongiorni 7/16/2007 5:07:54 PM
   
   Modifications:  
   ------------------------------------------------------------ */  ---------------------SELECT PARAMS-----------

ALTER PROCEDURE [dbo].[usp_PracticeBillingAddress_Select_One_ByPracticeID]
(
	  @PracticeID					 INT
	, @AttentionOf					 VARCHAR(50)		OUTPUT
    , @AddressID                     INT				OUTPUT
    , @AddressLine1                  VARCHAR(50)		OUTPUT
    , @AddressLine2                  VARCHAR(50)		OUTPUT
    , @City                          VARCHAR(50)		OUTPUT
    , @State                         CHAR(2)			OUTPUT
    , @ZipCode                       CHAR(5)			OUTPUT
    , @ZipCodePlus4                  CHAR(4)			OUTPUT
    , @OracleCode					 VARCHAR(10) = ''	OUTPUT
    , @OracleCodeValid				 bit = 0			OUTPUT
)
AS
BEGIN
	DECLARE @Err INT

	SELECT
		  @AttentionOf = PBA.AttentionOf 
        , @AddressID = A.AddressID
        , @AddressLine1 = A.AddressLine1
        , @AddressLine2 = A.AddressLine2
        , @City = A.City
        , @State = A.State
        , @ZipCode = A.ZipCode
        , @ZipCodePlus4 = A.ZipCodePlus4
        , @OracleCode = PBA.BregOracleId
        , @OracleCodeValid = ~PBA.BregOracleIDInvalid --mws note that this field is negated by(~)

	FROM	
			dbo.PracticeBillingAddress AS PBA
	INNER JOIN	
			dbo.Address AS A
				ON PBA.AddressID = A.AddressID

	WHERE 
		PBA.PracticeID = @PracticeID
		AND PBA.IsActive = 1
		AND A.IsActive = 1

	SET @Err = @@ERROR

	RETURN @Err
End


