USE [BregVision]
GO
/****** Object:  Table [dbo].[ForumGroup]    Script Date: 06/30/2010 10:45:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ForumGroup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ForumGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[ParentId] [int] NULL,
	[Created] [datetime] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_ForumGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ProductReview]    Script Date: 06/30/2010 10:45:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductReview]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ProductReview](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MasterCatalogProductID] [int] NOT NULL,
	[Pros] [nvarchar](max) NULL,
	[Cons] [nvarchar](max) NULL,
	[Thoughts] [nvarchar](max) NULL,
	[StarRating] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[Created] [datetime] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_ProductReview] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ForumArticle]    Script Date: 06/30/2010 10:45:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ForumArticle]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ForumArticle](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NULL,
	[Subject] [nvarchar](100) NULL,
	[Body] [nvarchar](max) NULL,
	[ForumGroupId] [int] NOT NULL,
	[MasterCatalogProductID] [int] NULL,
	[UserId] [int] NOT NULL,
	[Created] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsSticky] [bit] NOT NULL,
 CONSTRAINT [PK_ForumArticle] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetForumArticleTree]    Script Date: 06/30/2010 10:45:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetForumArticleTree]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author: Tom Thomas>
-- Create date: <Create Date: 06/17/10>
-- Description:	<Description: Get''s article thread
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetForumArticleTree]  
	@Id int
	,@Depth int = 1000
	
AS 
    BEGIN
	
	SET NOCOUNT ON;
    
    WITH Tree ([Id],[ParentId],[Subject],[Body],[ForumGroupId],[MasterCatalogProductID],[UserId],[Created],[IsActive], [IsSticky], [Level], [TopParentId]) AS
	(      
		SELECT
			[Id]
			,[ParentId]
			,[Subject]
			,[Body]
			,[ForumGroupId]
			,[MasterCatalogProductID]
			,[UserId]
			,[Created]
			,[IsActive]
			,[IsSticky]
			,0 as ''Level'',
			Id as ''TopParentId''
		FROM dbo.ForumArticle   
		UNION ALL       
		SELECT 
			t.[Id]
			,t.[ParentId]
			,t.[Subject]
			,t.[Body]
			,t.[ForumGroupId]
			,t.[MasterCatalogProductID]
			,t.[UserId]
			,t.[Created]
			,t.[IsActive]
			,t.[IsSticky]
			,[Level] + 1 as ''Level''
			,[TopParentId]
			FROM dbo.ForumArticle t          
				INNER JOIN Tree ON t.ParentId = Tree.Id
	)

	SELECT 
		t.*
		, u.PublicUserName as ''PublicUserName''
		, u.PublicRole as ''PublicUserRole''
		, u.PublicDescription as ''PublicUserDescription''
		, p.PracticeName as ''PracticeName''
		, p.PublicDescription as ''PracticeDescription''
		, g.Name as ''ForumGroupName''
	FROM Tree t
	LEFT OUTER JOIN dbo.[User] u ON t.UserId = u.UserId
	LEFT OUTER JOIN dbo.[Practice] p ON u.PracticeID = p.PracticeID
	LEFT OUTER JOIN dbo.[ForumGroup] g ON t.ForumGroupId = g.Id
	WHERE [TopParentId] = @Id 
	And [Level] < @Depth
	ORDER BY [Level], ParentId, IsSticky, Created desc

    END

-- exec [usp_GetForumArticleTree] 1
' 
END
GO
/****** Object:  Table [dbo].[ForumArticleImpression]    Script Date: 06/30/2010 10:45:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ForumArticleImpression]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ForumArticleImpression](
	[ArticleId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[Created] [datetime] NOT NULL,
 CONSTRAINT [PK_ForumArticleImpression] PRIMARY KEY CLUSTERED 
(
	[ArticleId] ASC,
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Default [DF_ForumArticle_Created]    Script Date: 06/30/2010 10:45:03 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ForumArticle_Created]') AND parent_object_id = OBJECT_ID(N'[dbo].[ForumArticle]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ForumArticle_Created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ForumArticle] ADD  CONSTRAINT [DF_ForumArticle_Created]  DEFAULT (getdate()) FOR [Created]
END


End
GO
/****** Object:  Default [DF_ForumArticle_IsActive]    Script Date: 06/30/2010 10:45:03 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ForumArticle_IsActive]') AND parent_object_id = OBJECT_ID(N'[dbo].[ForumArticle]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ForumArticle_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ForumArticle] ADD  CONSTRAINT [DF_ForumArticle_IsActive]  DEFAULT ((1)) FOR [IsActive]
END


End
GO
/****** Object:  Default [DF_ForumArticle_IsSticky]    Script Date: 06/30/2010 10:45:03 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ForumArticle_IsSticky]') AND parent_object_id = OBJECT_ID(N'[dbo].[ForumArticle]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ForumArticle_IsSticky]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ForumArticle] ADD  CONSTRAINT [DF_ForumArticle_IsSticky]  DEFAULT ((0)) FOR [IsSticky]
END


End
GO
/****** Object:  Default [DF_ForumArticleImpression_Created]    Script Date: 06/30/2010 10:45:03 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ForumArticleImpression_Created]') AND parent_object_id = OBJECT_ID(N'[dbo].[ForumArticleImpression]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ForumArticleImpression_Created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ForumArticleImpression] ADD  CONSTRAINT [DF_ForumArticleImpression_Created]  DEFAULT (getdate()) FOR [Created]
END


End
GO
/****** Object:  Default [DF_ForumGroup_Created]    Script Date: 06/30/2010 10:45:03 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ForumGroup_Created]') AND parent_object_id = OBJECT_ID(N'[dbo].[ForumGroup]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ForumGroup_Created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ForumGroup] ADD  CONSTRAINT [DF_ForumGroup_Created]  DEFAULT (getdate()) FOR [Created]
END


End
GO
/****** Object:  Default [DF_ForumGroup_SortOrder]    Script Date: 06/30/2010 10:45:03 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ForumGroup_SortOrder]') AND parent_object_id = OBJECT_ID(N'[dbo].[ForumGroup]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ForumGroup_SortOrder]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ForumGroup] ADD  CONSTRAINT [DF_ForumGroup_SortOrder]  DEFAULT ((0)) FOR [SortOrder]
END


End
GO
/****** Object:  Default [DF_ForumGroup_IsActive]    Script Date: 06/30/2010 10:45:03 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ForumGroup_IsActive]') AND parent_object_id = OBJECT_ID(N'[dbo].[ForumGroup]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ForumGroup_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ForumGroup] ADD  CONSTRAINT [DF_ForumGroup_IsActive]  DEFAULT ((1)) FOR [IsActive]
END


End
GO
/****** Object:  Default [DF_ProductReview_Rating]    Script Date: 06/30/2010 10:45:03 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ProductReview_Rating]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductReview]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ProductReview_Rating]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ProductReview] ADD  CONSTRAINT [DF_ProductReview_Rating]  DEFAULT ((0)) FOR [StarRating]
END


End
GO
/****** Object:  Default [DF_ProductReview_Created]    Script Date: 06/30/2010 10:45:03 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ProductReview_Created]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductReview]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ProductReview_Created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ProductReview] ADD  CONSTRAINT [DF_ProductReview_Created]  DEFAULT (getdate()) FOR [Created]
END


End
GO
/****** Object:  Default [DF_ProductReview_SortOrder]    Script Date: 06/30/2010 10:45:03 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ProductReview_SortOrder]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductReview]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ProductReview_SortOrder]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ProductReview] ADD  CONSTRAINT [DF_ProductReview_SortOrder]  DEFAULT ((0)) FOR [SortOrder]
END


End
GO
/****** Object:  Default [DF_ProductReview_IsActive]    Script Date: 06/30/2010 10:45:03 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ProductReview_IsActive]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductReview]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ProductReview_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ProductReview] ADD  CONSTRAINT [DF_ProductReview_IsActive]  DEFAULT ((1)) FOR [IsActive]
END


End
GO
/****** Object:  ForeignKey [FK_ForumArticle_ForumArticle]    Script Date: 06/30/2010 10:45:03 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ForumArticle_ForumArticle]') AND parent_object_id = OBJECT_ID(N'[dbo].[ForumArticle]'))
ALTER TABLE [dbo].[ForumArticle]  WITH CHECK ADD  CONSTRAINT [FK_ForumArticle_ForumArticle] FOREIGN KEY([ParentId])
REFERENCES [dbo].[ForumArticle] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ForumArticle_ForumArticle]') AND parent_object_id = OBJECT_ID(N'[dbo].[ForumArticle]'))
ALTER TABLE [dbo].[ForumArticle] CHECK CONSTRAINT [FK_ForumArticle_ForumArticle]
GO
/****** Object:  ForeignKey [FK_ForumArticle_ForumGroup]    Script Date: 06/30/2010 10:45:03 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ForumArticle_ForumGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[ForumArticle]'))
ALTER TABLE [dbo].[ForumArticle]  WITH CHECK ADD  CONSTRAINT [FK_ForumArticle_ForumGroup] FOREIGN KEY([ForumGroupId])
REFERENCES [dbo].[ForumGroup] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ForumArticle_ForumGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[ForumArticle]'))
ALTER TABLE [dbo].[ForumArticle] CHECK CONSTRAINT [FK_ForumArticle_ForumGroup]
GO
/****** Object:  ForeignKey [FK_ForumArticle_MasterCatalogProduct]    Script Date: 06/30/2010 10:45:03 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ForumArticle_MasterCatalogProduct]') AND parent_object_id = OBJECT_ID(N'[dbo].[ForumArticle]'))
ALTER TABLE [dbo].[ForumArticle]  WITH CHECK ADD  CONSTRAINT [FK_ForumArticle_MasterCatalogProduct] FOREIGN KEY([MasterCatalogProductID])
REFERENCES [dbo].[MasterCatalogProduct] ([MasterCatalogProductID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ForumArticle_MasterCatalogProduct]') AND parent_object_id = OBJECT_ID(N'[dbo].[ForumArticle]'))
ALTER TABLE [dbo].[ForumArticle] CHECK CONSTRAINT [FK_ForumArticle_MasterCatalogProduct]
GO
/****** Object:  ForeignKey [FK_ForumArticle_User]    Script Date: 06/30/2010 10:45:03 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ForumArticle_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[ForumArticle]'))
ALTER TABLE [dbo].[ForumArticle]  WITH CHECK ADD  CONSTRAINT [FK_ForumArticle_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ForumArticle_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[ForumArticle]'))
ALTER TABLE [dbo].[ForumArticle] CHECK CONSTRAINT [FK_ForumArticle_User]
GO
/****** Object:  ForeignKey [FK_ForumArticleImpression_ForumArticle]    Script Date: 06/30/2010 10:45:03 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ForumArticleImpression_ForumArticle]') AND parent_object_id = OBJECT_ID(N'[dbo].[ForumArticleImpression]'))
ALTER TABLE [dbo].[ForumArticleImpression]  WITH CHECK ADD  CONSTRAINT [FK_ForumArticleImpression_ForumArticle] FOREIGN KEY([ArticleId])
REFERENCES [dbo].[ForumArticle] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ForumArticleImpression_ForumArticle]') AND parent_object_id = OBJECT_ID(N'[dbo].[ForumArticleImpression]'))
ALTER TABLE [dbo].[ForumArticleImpression] CHECK CONSTRAINT [FK_ForumArticleImpression_ForumArticle]
GO
/****** Object:  ForeignKey [FK_ForumArticleImpression_User]    Script Date: 06/30/2010 10:45:03 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ForumArticleImpression_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[ForumArticleImpression]'))
ALTER TABLE [dbo].[ForumArticleImpression]  WITH CHECK ADD  CONSTRAINT [FK_ForumArticleImpression_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ForumArticleImpression_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[ForumArticleImpression]'))
ALTER TABLE [dbo].[ForumArticleImpression] CHECK CONSTRAINT [FK_ForumArticleImpression_User]
GO
/****** Object:  ForeignKey [FK_ForumGroup_ForumGroup]    Script Date: 06/30/2010 10:45:03 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ForumGroup_ForumGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[ForumGroup]'))
ALTER TABLE [dbo].[ForumGroup]  WITH CHECK ADD  CONSTRAINT [FK_ForumGroup_ForumGroup] FOREIGN KEY([ParentId])
REFERENCES [dbo].[ForumGroup] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ForumGroup_ForumGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[ForumGroup]'))
ALTER TABLE [dbo].[ForumGroup] CHECK CONSTRAINT [FK_ForumGroup_ForumGroup]
GO
/****** Object:  ForeignKey [FK_ProductReview_MasterCatalogProduct]    Script Date: 06/30/2010 10:45:03 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductReview_MasterCatalogProduct]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductReview]'))
ALTER TABLE [dbo].[ProductReview]  WITH CHECK ADD  CONSTRAINT [FK_ProductReview_MasterCatalogProduct] FOREIGN KEY([MasterCatalogProductID])
REFERENCES [dbo].[MasterCatalogProduct] ([MasterCatalogProductID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductReview_MasterCatalogProduct]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductReview]'))
ALTER TABLE [dbo].[ProductReview] CHECK CONSTRAINT [FK_ProductReview_MasterCatalogProduct]
GO
/****** Object:  ForeignKey [FK_ProductReview_User]    Script Date: 06/30/2010 10:45:03 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductReview_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductReview]'))
ALTER TABLE [dbo].[ProductReview]  WITH CHECK ADD  CONSTRAINT [FK_ProductReview_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductReview_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductReview]'))
ALTER TABLE [dbo].[ProductReview] CHECK CONSTRAINT [FK_ProductReview_User]
GO
