USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetSuppliers]    Script Date: 10/05/2009 09:02:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author: Greg Ross>
-- Create date: <Create Date: 07/23/07>
-- Description:	<Description: Used to get all suppliers/Brands in database>
-- modification:  jb 10072007 fix query to be effient and add thirdpartysuppliers.
--  [dbo].[usp_GetSuppliers_2]  10
--
--     SupplierID by itself (using both master catalogsupplierid and thirdpartysupplierID
--		  will not distinguish between MCS Bird and Cronin and TPS DeRoyal
--			leading to B&C products being placed under DeRoyal

--  Modification:
--	2007.11.09 JB  Add to WHERE Clause "AND PCSB.ISACTIVE = 1" ; Add to LEFT JOIN ON CONTACT "AND C.ISACTIVE = 1"
--  20080317 JB  Format the Supplier-Brand when supplier is a third party vendor.  
--				Format the phone number, combine the firstandlast name of the contact.
--				Add state to city.
--
--                CASE WHEN (PCSB.IsThirdPartySupplier = 1 )AND (PCSB.SupplierShortName <> PCSB.BrandShortName) 		
--							THEN PCSB.SupplierShortName + ' - ' + PCSB.BrandShortName 
--							ELSE PCSB.SupplierShortName END AS Brand,      --AS SupplierName,  -- 20080317 JB
--Copied from usp_GetSuppliers, which is called from pages: 
--		Inventory.aspx
--		.aspx
--		.aspx
-- =============================================
Alter PROCEDURE [dbo].[usp_GetSuppliers_Searchable]  --3 --10  --  _With_Brands
	@PracticeLocationID int
	, @SearchCriteria varchar(50)
	, @SearchText  varchar(50)
AS 
    BEGIN
    
    SET NOCOUNT ON;

	/*These variables are used in conjunction with the coalesce statements in the where clause.
	each of these variables will be null except one is @SearchCriteria matches.*/
	Declare @ProductName varchar(50)			--NULL   These default to null
	Declare @ProductCode varchar(50)			--NULL   These default to null
	Declare @ProductBrand varchar(50)			--NULL   These default to null
	Declare @ProductHCPCs varchar(50)			--NULL   These default to null

	if @SearchCriteria='Name'					-- ProductName
		Set @ProductName = '%' + @SearchText + '%'		--put the percent sign on both sides of the search text for product name
	else if @SearchCriteria='Code'
		Set @ProductCode = '%' +  @SearchText + '%'	
	else if @SearchCriteria='Brand'
		Set @ProductBrand = @SearchText + '%'	
	else if @SearchCriteria='HCPCs'
		Set @ProductHCPCs = @SearchText + '%'	

     
     SELECT 
		Sub.Brand,		
		Sub.SupplierID,      
        Sub.Name + ' ' + Sub.LastName + CHAR(13)+ CHAR(10) + Sub.PhoneWork AS FullName,        
        Sub.Name + ' ' + Sub.LastName AS NAME, --AS FullName,        
		'' as lastname, --        Sub.LastName,		
      CASE WHEN LEN(Sub.PhoneWork) = 10   
			THEN '(' + LEFT(Sub.PhoneWork, 3) + ') ' + LEFT(right(Sub.PhoneWork, 7), 3) + '-' + RIGHT(Sub.PhoneWork, 4)
			ELSE ''
			END AS PhoneWork,
        Sub.Address,
        CASE WHEN LEN(LTRIM(RTRIM(Sub.City))) = 0 THEN ''
        ELSE RTRIM(Sub.City) + ', ' + Sub.State
        END AS City, 
        Sub.Zipcode,        
        Sub.IsThirdPartySupplier 
	FROM     
     
     (
		 SELECT DISTINCT  

			PCSB.BrandShortName AS Brand,  --JB display name for brand
			PCSB.PracticeCatalogSupplierBrandID AS supplierID,           
			ISNULL(c.[FirstName], '') AS Name,
			ISNULL(C.[LastName], '')  AS LastName,
			ISNULL(c.[PhoneWork], '')  AS PhoneWork,
	        
			ISNULL(A.[AddressLine1], '')   AS Address,
			ISNULL(A.City, '')  AS City,  
			ISNULL(A.State, '')  AS State,  
			ISNULL(A.[ZipCode], '')   AS Zipcode,
			0 AS IsThirdPartySupplier,
			PCSB.Sequence
		FROM    
			[MasterCatalogSupplier] AS MCS  WITH (NOLOCK)
            LEFT JOIN address A			  WITH (NOLOCK)
					ON MCS.[AddressID] = A.[AddressID]
            LEFT JOIN [Contact] C			  WITH (NOLOCK)
					ON mcs.[ContactID] = c.[ContactID]
					AND C.ISACTIVE = 1
			INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB		  WITH (NOLOCK)
				ON MCS.MasterCatalogSupplierID = pcsb.MasterCatalogSupplierID

			INNER JOIN [PracticeCatalogProduct] PCP				  WITH (NOLOCK)
				ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID	
				
			Inner join MasterCatalogProduct MCP		WITH (NOLOCK)
			on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID			
									
			INNER JOIN [ProductInventory] PI		  WITH (NOLOCK)
				ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]

			WHERE PI.[PracticeLocationID] = @PracticeLocationID
				AND    MCS.ISACTIVE = 1
				AND A.ISACTIVE = 1
				AND PCSB.IsActive = 1
				AND PCP.ISACTIVE = 1
				AND IsThirdPartySupplier = 0 -- MasterCatalogSuppliers.
		AND (   /*
				This part is similar to an Optional Parameter Search.
				if the Parameter is not Null, then narrow the search by it, otherwise
				return all rows.  If all parameters are null, then return all rows.
				Use these lines for target or parent tables. 
				This has the effect of saying "if @variable is not null then try to match it to the " 
				left hand expression.  If @variable is null then match the left hand field to itself
				if the left hand field is null, then match '' = ''(This last part is because NULL = NULL) 
				does not return a match. */
				COALESCE(MCP.Name, '') like COALESCE(@ProductName, MCP.Name, '')
				OR
				COALESCE(MCP.ShortName, '') like COALESCE(@ProductName, MCP.ShortName, '')
			)
		AND (
				COALESCE(MCP.Code, '') like COALESCE(@ProductCode , MCP.Code, '')
			)
		AND (
				COALESCE(PCSB.BrandName, '') like COALESCE(@ProductBrand, PCSB.BrandName, '')
				OR
				COALESCE(PCSB.BrandShortName, '') like COALESCE(@ProductBrand, PCSB.BrandShortName, '')
				OR
				COALESCE(PCSB.SupplierName, '') like COALESCE(@ProductBrand, PCSB.SupplierName, '')
				OR
				COALESCE(PCSB.SupplierShortName, '') like COALESCE(@ProductBrand, PCSB.SupplierShortName, '')			
			)
				
		AND (   /* use this type of statement for Child tables of the target to avoid duplicate rows.
				If we use a join for this, we would return duplicate rows, so we just check the existence
				of one record that matches the search parameters in the child table
				*/
				(
					 (@ProductHCPCs IS NULL)
					 OR EXISTS
					 (  
						Select 
							1 
						 from 
							ProductHCPCS PHCPCS 
						 where 
							PHCPCS.PracticeCatalogProductID = PCP.PracticeCatalogProductID 
						    and PHCPCS.HCPCS like @ProductHCPCs  
					  )
				 )
			)
					
UNION

     SELECT     DISTINCT 
                
                CASE WHEN (PCSB.IsThirdPartySupplier = 1 )AND (PCSB.SupplierShortName <> PCSB.BrandShortName) 		
							THEN PCSB.SupplierShortName + ' - ' + PCSB.BrandShortName 
							ELSE PCSB.SupplierShortName END AS Brand,      --AS SupplierName,  -- 20080317 JB

                PCSB.PracticeCatalogSupplierBrandID AS supplierID,    --  SupplierBrandID is neccessary to associate
				ISNULL(c.[FirstName], '') AS Name,
                ISNULL(C.[LastName], '')  AS LastName,
                ISNULL(c.[PhoneWork], '')  AS PhoneWork,
                ISNULL(A.[AddressLine1], '')   AS Address,
                ISNULL(A.City, '') AS City,  
                ISNULL(A.State, '') AS State,  
                ISNULL(A.[ZipCode], '')   AS Zipcode,
                1 AS IsThirdPartySupplier,
                PCSB.Sequence
        FROM    
			[dbo].[ThirdPartySupplier] AS TPS    WITH (NOLOCK)
            LEFT JOIN address A					  WITH (NOLOCK)
					ON TPS.[AddressID] = A.[AddressID]
            lefT JOIN [Contact] C				  WITH (NOLOCK)
					ON TPS.[ContactID] = c.[ContactID]
					AND C.ISACTIVE = 1
			INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	  WITH (NOLOCK)
				ON TPS.ThirdPartySupplierID = pcsb.ThirdPartySupplierID

			INNER JOIN [PracticeCatalogProduct] PCP   WITH (NOLOCK)
				ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID				
									
			INNER JOIN [ProductInventory] PI   WITH (NOLOCK)
				ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
				
			Inner Join ThirdPartyProduct TPP WITH (NOLOCK)
				on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID 	

			WHERE PI.[PracticeLocationID] = @PracticeLocationID
				AND    tps.ISACTIVE = 1
				AND PCSB.IsActive = 1
				AND PCP.ISACTIVE = 1
				AND PI.ISACTIVE = 1
				AND IsThirdPartySupplier = 1 -- ThirdPartySuppliers.
						AND (   /*
				This part is similar to an Optional Parameter Search.
				if the Parameter is not Null, then narrow the search by it, otherwise
				return all rows.  If all parameters are null, then return all rows.
				Use these lines for target or parent tables. 
				This has the effect of saying "if @variable is not null then try to match it to the " 
				left hand expression.  If @variable is null then match the left hand field to itself
				if the left hand field is null, then match '' = ''(This last part is because NULL = NULL) 
				does not return a match. */

				COALESCE(TPP.Name, '') like COALESCE(@ProductName, TPP.Name, '')
				OR
				COALESCE(TPP.ShortName, '') like COALESCE(@ProductName, TPP.ShortName, '')
			)
		AND (
				COALESCE(TPP.Code, '') like COALESCE(@ProductCode , TPP.Code, '')			)
		AND (
				COALESCE(PCSB.BrandName, '') like COALESCE(@ProductBrand, PCSB.BrandName, '')
				OR
				COALESCE(PCSB.BrandShortName, '') like COALESCE(@ProductBrand, PCSB.BrandShortName, '')
				OR
				COALESCE(PCSB.SupplierName, '') like COALESCE(@ProductBrand, PCSB.SupplierName, '')
				OR
				COALESCE(PCSB.SupplierShortName, '') like COALESCE(@ProductBrand, PCSB.SupplierShortName, '')			
			)
				
		AND (   /* use this type of statement for Child tables of the target to avoid duplicate rows.
				If we use a join for this, we would return duplicate rows, so we just check the existence
				of one record that matches the search parameters in the child table
				*/
				(
					 (@ProductHCPCs IS NULL)
					 OR EXISTS
					 (  
						Select 
							1 
						 from 
							ProductHCPCS PHCPCS 
						 where 
							PHCPCS.PracticeCatalogProductID = PCP.PracticeCatalogProductID 
						    and PHCPCS.HCPCS like @ProductHCPCs  
					  )
				 )
			)
			) AS Sub
			
		ORDER BY
			SUB.IsThirdPartySupplier
			, SUB.Sequence
			, SUB.Brand

    END





