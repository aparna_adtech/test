USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_SuperbillCategory_Insert]    Script Date: 03/26/2009 18:56:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author: Mike Sneen>
-- Create date: <Create Date: 02/28/09>
-- Description:	<Description: Used to get all a superbill>
-- modification:
--  [dbo].[usp_SuperbillCategory_Update]  

-- =============================================
Create PROCEDURE [dbo].[usp_SuperbillCategory_Update]  --3 --10  --  _With_Brands
	@CategoryID int,
	@PracticeLocationID int,
	@ColumnNumber int,
	@CategoryName varchar(30)
AS 
    BEGIN
    DECLARE @Err INT
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
--        SET NOCOUNT ON ;
     
     Update SuperBillCategory
     SET
		PracticeLocationID = @PracticeLocationID,
		ColumnNumber = @ColumnNumber,
		[Name] = @CategoryName,
		ModifiedUserID = 1,
		ModifiedDate = GETDATE(),
		IsActive = 1
	WHERE
		SuperBillCategoryID = @CategoryID
     

     
     	SET @Err = @@ERROR

	RETURN @Err

    END

--Exec usp_SuperbillCategory_Update 3, 3