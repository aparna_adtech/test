USE [BregVision]
GO

ALTER TABLE dbo.PracticePayer ADD
	IsActive bit default(1)
GO
UPDATE dbo.PracticePayer
	SET IsActive = 1
GO