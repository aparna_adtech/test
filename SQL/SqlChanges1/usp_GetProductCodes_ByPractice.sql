--drop procedure [dbo].[usp_GetProductCodes_ByPractice]
--go

create procedure [dbo].[usp_GetProductCodes_ByPractice]
	@PracticeID as int
as
begin
	select distinct
		[SUB].Code
	from
		(
			select
				[MCP].Code,
				[PCP].IsThirdPartyProduct
			from
				PracticeCatalogProduct [PCP]
					inner join MasterCatalogProduct [MCP]
						on [PCP].MasterCatalogProductID = [MCP].MasterCatalogProductID
			where
				[PCP].IsActive = 1
				and [PCP].PracticeID = @PracticeID
				and [MCP].IsActive = 1
				and [PCP].IsThirdPartyProduct = 0
			
			union
			
			select
				[TPP].Code,
				[PCP].IsThirdPartyProduct
			from
				PracticeCatalogProduct [PCP]
					inner join ThirdPartyProduct [TPP]
						on [PCP].ThirdPartyProductID = [TPP].ThirdPartyProductID
			where
				[PCP].IsActive = 1
				and [PCP].PracticeID = @PracticeID
				and [TPP].IsActive = 1
				and [PCP].IsThirdPartyProduct = 1
		) as [SUB]
	order by
		[SUB].Code asc
end