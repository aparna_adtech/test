use BregVision

ALTER TABLE DispenseDetail
ALTER COLUMN ICD9Code varchar(50)

ALTER TABLE ICD9
ALTER COLUMN ICD9Code varchar(50)