alter PROCEDURE [dbo].[usp_GetSuppliers_MultiLocations]
	@PracticeLocationID NVARCHAR(MAX)
AS 
BEGIN

SET NOCOUNT ON

--- begin multi-value @PracticeLocationID parsing
DECLARE @locations AS TABLE ( [seq] INT, [location] INT );
WITH pieces([seq], [start], [stop])
AS
(
	SELECT 1, 1, CHARINDEX(',', @PracticeLocationID)
	UNION ALL
	SELECT CAST([seq] + 1 AS INT), CAST([stop] + 1 AS INT), CHARINDEX(',', @PracticeLocationID, [stop] + 1)
	FROM pieces
	WHERE [stop] > 0
)
INSERT INTO @locations
SELECT
	[seq],
	CAST(SUBSTRING(@PracticeLocationID, [start], CASE WHEN [stop] > 0 THEN [stop] - start ELSE 512 END) AS INT)
FROM
	pieces;
--- end multi-value @PracticeLocationID parsing

SET NOCOUNT OFF

SELECT
	Sub.Brand,		
	Sub.SupplierID,
	Sub.Name + ' ' + Sub.LastName + CHAR(13)+ CHAR(10) + Sub.PhoneWork AS FullName,        
	Sub.Name + ' ' + Sub.LastName AS NAME,   
	'' as lastname,
	CASE
		WHEN LEN(Sub.PhoneWork) = 10 THEN '(' + LEFT(Sub.PhoneWork, 3) + ') ' + LEFT(right(Sub.PhoneWork, 7), 3) + '-' + RIGHT(Sub.PhoneWork, 4)
		ELSE ''
	END AS PhoneWork,
	Sub.Address,
	CASE
		WHEN LEN(LTRIM(RTRIM(Sub.City))) = 0 THEN ''
		ELSE RTRIM(Sub.City) + ', ' + Sub.State
	END AS City,
	Sub.Zipcode,        
	Sub.IsThirdPartySupplier
FROM     
	(
		SELECT DISTINCT  
			PCSB.BrandShortName AS Brand,
			PCSB.PracticeCatalogSupplierBrandID AS supplierID,           
			ISNULL(c.[FirstName], '') AS Name,
			ISNULL(C.[LastName], '') AS LastName,
			ISNULL(c.[PhoneWork], '') AS PhoneWork,
			ISNULL(A.[AddressLine1], '') AS Address,
			ISNULL(A.City, '') AS City,  
			ISNULL(A.State, '') AS State,  
			ISNULL(A.[ZipCode], '') AS Zipcode,
			0 AS IsThirdPartySupplier,
			PCSB.Sequence
		FROM
			[MasterCatalogSupplier] AS MCS WITH (NOLOCK)
				LEFT JOIN address A WITH (NOLOCK)
					ON MCS.[AddressID] = A.[AddressID]
				LEFT JOIN [Contact] C WITH (NOLOCK)
					ON
						MCS.[ContactID] = c.[ContactID]
						AND C.ISACTIVE = 1
				INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB WITH (NOLOCK)
					ON MCS.MasterCatalogSupplierID = pcsb.MasterCatalogSupplierID
				INNER JOIN [PracticeCatalogProduct] PCP WITH (NOLOCK)
					ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID				
				INNER JOIN [ProductInventory] PI WITH (NOLOCK)
					ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
		WHERE
			PI.[PracticeLocationID] IN (SELECT location FROM @locations)
			--AND MCS.ISACTIVE = 1
			AND A.ISACTIVE = 1
			--AND PCSB.IsActive = 1
			--AND PCP.ISACTIVE = 1
			AND IsThirdPartySupplier = 0

		UNION

		SELECT DISTINCT
			CASE
				WHEN (PCSB.IsThirdPartySupplier = 1 ) AND (PCSB.SupplierShortName <> PCSB.BrandShortName) THEN PCSB.SupplierShortName + ' - ' + PCSB.BrandShortName
				ELSE PCSB.SupplierShortName
			END AS Brand,
			PCSB.PracticeCatalogSupplierBrandID AS supplierID,	--  SupplierBrandID is neccessary to associate
																--  the child grid without criss-crossing.              
			ISNULL(c.[FirstName], '') AS Name,
			ISNULL(C.[LastName], '') AS LastName,
			ISNULL(c.[PhoneWork], '') AS PhoneWork,
			ISNULL(A.[AddressLine1], '') AS Address,
			ISNULL(A.City, '') AS City,  
			ISNULL(A.State, '') AS State,  
			ISNULL(A.[ZipCode], '') AS Zipcode,
			1 AS IsThirdPartySupplier,
			PCSB.Sequence
		FROM
			[dbo].[ThirdPartySupplier] AS TPS WITH (NOLOCK)
				LEFT JOIN address A WITH (NOLOCK)
					ON TPS.[AddressID] = A.[AddressID]
				LEFT JOIN [Contact] C WITH (NOLOCK)
					ON
						TPS.[ContactID] = c.[ContactID]
						AND C.ISACTIVE = 1
				INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB WITH (NOLOCK)
					ON TPS.ThirdPartySupplierID = pcsb.ThirdPartySupplierID
				INNER JOIN [PracticeCatalogProduct] PCP WITH (NOLOCK)
					ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
				INNER JOIN [ProductInventory] PI WITH (NOLOCK)
					ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
		WHERE
			PI.[PracticeLocationID] IN (SELECT location FROM @locations)
			--AND tps.ISACTIVE = 1
			--AND PCSB.IsActive = 1
			--AND PCP.ISACTIVE = 1
			AND PI.ISACTIVE = 1
			AND IsThirdPartySupplier = 1
	) AS Sub
ORDER BY
	SUB.IsThirdPartySupplier ASC,
	SUB.Sequence ASC,
	SUB.Brand ASC

END
