use [BregVision]
go

if not exists (select * from dbo.syscolumns where id = object_id(N'PracticeApplication') and name=(N'Password'))
alter table [PracticeApplication] add [Password] [varchar](50) NULL
GO
