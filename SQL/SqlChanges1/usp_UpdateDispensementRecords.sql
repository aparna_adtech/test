USE [BregVision_BETA]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateDispensementRecords]    Script Date: 04/10/2014 02:56:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author: Greg Ross>
-- Create date: <Create Date: 12/12/08>
-- Description:	<Description: Update all dispensment records. 
-- Used in PracticeLocation_DispensementModification Page.
-- =============================================
ALTER PROCEDURE [dbo].[usp_UpdateDispensementRecords]

	@DispenseID int,
	@DispenseDetailID int, 
	@PatientCode varchar(255),
	@DateDispensed datetime,
	@PhysicianID int,
	@DMEDeposit smallmoney

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update dispense set patientcode=@PatientCode, dateDispensed=@DateDispensed where dispenseid=@DispenseID

	update dispensedetail set PhysicianID = @PhysicianID, DMEDeposit = @DMEDeposit where dispensedetailid=@DispenseDetailID
	


END

GO
