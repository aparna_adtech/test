USE [BregVision]
GO

/****** Object:  StoredProcedure [dbo].[usp_ShoppingCart_Update_ShippingType]    Script Date: 01/19/2011 10:27:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[usp_ShoppingCart_Select_CustomPurchaseOrderCodeByPracticeLocationID]
	  @PracticeLocationID INT
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT TOP 1
		CustomPurchaseOrderCode
	FROM
		ShoppingCart
	WHERE
		PracticeLocationID = @PracticeLocationID
		AND
		IsActive = 1
	ORDER BY
		CreatedDate DESC;

END
GO


