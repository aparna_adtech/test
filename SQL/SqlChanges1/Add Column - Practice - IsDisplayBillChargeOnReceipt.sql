GO
USE [BregVision];


GO
PRINT N'Altering [dbo].[Practice]...';


GO
ALTER TABLE [dbo].[Practice]
    ADD [IsDisplayBillChargeOnReceipt] BIT CONSTRAINT [DF_Practice_IsDisplayBillChargeOnReceipt] DEFAULT ((1)) NOT NULL;


GO