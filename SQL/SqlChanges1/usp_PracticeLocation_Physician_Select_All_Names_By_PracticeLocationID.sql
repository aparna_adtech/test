USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_PracticeLocation_Physician_Select_All_Names_By_PracticeLocationID]    Script Date: 03/12/2013 22:31:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_PracticeLocation_Physician_Select_All_Names_By_PracticeLocationID]
  @PracticeLocationID INT
AS
BEGIN 

SELECT
	  PH.PhysicianID
	   , C.ContactID

	-- , C.Salutation
	-- , C.FirstName
	-- , C.MiddleName
	-- , C.LastName
	-- , C.Suffix

	, RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix) AS PhysicianName
	, PH.SortOrder
	, PH.Pin
	 
	 FROM  
	  dbo.Contact AS C
	 INNER JOIN 
	  dbo.Physician AS PH
	   ON C.ContactID = PH.ContactID
	 
	 INNER JOIN 
	  dbo.PracticeLocation_Physician AS PLP  
	   ON PH.PhysicianID = PLP.PhysicianID
	 WHERE
	  PLP.PracticeLocationID = @PracticeLocationID
	  AND PH.IsActive = 1
	  AND C.IsActive = 1
	  AND PLP.IsActive = 1
	  
	  Order by PH.SortOrder
END

