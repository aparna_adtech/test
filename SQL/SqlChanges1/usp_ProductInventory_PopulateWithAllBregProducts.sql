USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMasterCatalogProductInfo]    Script Date: 03/19/2010 11:22:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Mike Sneen
-- Create date: 03/19/2010
-- Description:	Populates the Vision Lite PracticeLocation - Product Inventory with all Breg Products
-- Modifications: 
--
-- EXEC usp_ProductInventory_PopulateWithAllBregProducts 146, 489, 1
-- =============================================
Alter PROCEDURE [dbo].[usp_ProductInventory_PopulateWithAllBregProducts]

@PracticeID int,
@PracticeLocationID int,
@UserID int = 1

AS


BEGIN

	
	
	Set NoCount on;
	
	if Exists(Select 1 from PracticeLocation PL where PL.PracticeID = @PracticeID AND PL.PracticeLocationID=@PracticeLocationID)
	BEGIN
	
		DECLARE @BregProductsFromProc TABLE
		(
			PracticeID int,
			PracticecatalogProductID int,
			MasterCatalogProductID int,
			PracticeCatalogSupplierBrandID int,
			SupplierShortName varchar(50),
			BrandShortName varchar(50),
			ShortName	Varchar(50),
			Code varchar(50),
			SideSize varchar(50),
			LeftRightSide varchar(50),
			Size varchar(50),
			Gender varchar(50),
			WholesaleCost smallMoney,
			BillingCharge smallmoney,
			DMEDeposit smallmoney,
			BillingChargeCash smallmoney,
			StockingUnits int,
			BuyingUnits int,
			IsThirdPartyProduct bit,
			HCPCSString varchar(8000),
			IsDeletable bit
		)

		Declare @BregProducts TABLE
		(
			PracticeCatalogProductID int,
			fUsed bit default 0
		)

		--to get breg products
		INSERT INTO @BregProductsFromProc
		EXEC usp_GetPracticeCatalogProductInfoMCToPC_By_BrandName_With_3rdPartyProducts @PracticeID, 'Breg'

		Insert into @BregProducts(PracticecatalogProductID, fUsed) 
		(
			Select BPFP.PracticecatalogProductID, 0 from @BregProductsFromProc BPFP
			where BPFP.Code IN
			(
				'97063','97062','97064','97061','97065','08402','08404','08403','07334','07335','60044','21704','21703','21713','11233',
				'11234','11214','11213','10155','10154','10240','10220','96510','96500','96510','96520','07043','07042','11180','96534',
				'96533','96532','11174','11172','11173','10601','10604','10602','10606','10607','11183','21732','21735','21734','21733',
				'08494','08493','11093','11094','08504','08503','08502','96543','96544','96542','07250','07714','11286','28452','28453',
				'28454','28443','28444','28442','28423','28424','28422','28403','28404','28402','10271','10281','10272','10283','10273',
				'10282','10271','10281','10372','10373','10363','10362','10393','10383','10392','10382','10394','10384','10392','10393',
				'10342','10352','10442','10432','10443','10433','10442','10432','10433','10443'
			)
		)
			
		Declare @PracticecatalogProductID int

		Select TOP 1 @PracticecatalogProductID = PracticecatalogProductID from @BregProducts where fUsed = 0

		WHILE @@ROWCOUNT <> 0 AND @PracticecatalogProductID IS NOT NULL
		BEGIN

			EXEC usp_TransferFromPracticeCatalogtoProductInventory 
														@PracticeLocationID, @PracticecatalogProductID, @Userid
														
			Update @BregProducts SET fUsed = 1 where PracticecatalogProductID = @PracticecatalogProductID
				
			Select TOP 1 @PracticecatalogProductID = PracticecatalogProductID from @BregProducts where fUsed = 0											
		END

	END --If Exists

END