USE [BregVision]
GO

/****** Object:  Table [dbo].[zzzSalesReportExclusions]    Script Date: 01/16/2010 10:58:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[zzzSalesReportExclusions](
	[ExclusionID] [int] IDENTITY(1,1) NOT NULL,
	[PracticeCatalogSupplierBrandID] [int] NOT NULL,
	[SupplierShortName] [varchar](50) NULL,
	[BrandShortName] [varchar](50) NULL,
	[IsExcluded] [bit] NOT NULL,
 CONSTRAINT [PK_zzzSalesReportExclusions] PRIMARY KEY CLUSTERED 
(
	[ExclusionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[zzzSalesReportExclusions] ADD  CONSTRAINT [DF_zzzSalesReportExclusions_IsExcluded]  DEFAULT ((0)) FOR [IsExcluded]
GO


