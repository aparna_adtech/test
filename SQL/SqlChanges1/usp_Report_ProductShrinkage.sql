

--exec usp_Report_ProductShrinkage 1,3, '1', '112,111,110,109,108'

ALTER PROC [dbo].[usp_Report_ProductShrinkage] 
		  @PracticeID	INT
		, @PracticeLocationID int
		, @SupplierBrandID  VARCHAR(2000)   --  ID String.
		, @InventoryCycleID VARCHAR(2000)

AS
BEGIN
/*
Declare @StartOfCycleDate DateTime
Declare @StartDate DateTime
Declare @EndDate DateTime

Select @EndDate = ICE.EndDate from InventoryCycle ICE where ICE.InventoryCycleID = @InventoryCycleID
--Print @EndDate

Select @StartOfCycleDate = ICE1.StartDate from InventoryCycle ICE1 where ICE1.InventoryCycleID = @InventoryCycleID

Select TOP 1 @StartDate =  IC2.EndDate 
						from InventoryCycle IC2 
						where 
							IC2.InventoryCycleID < @InventoryCycleID
							AND IC2.PracticeLocationID = @PracticeLocationID
						Order By IC2.InventoryCycleID DESC


SELECT @EndDate = DATEADD(DAY, 1, @EndDate)
Print 'StartDate'
print @StartDate 
Print 'EndDate'
Print @EndDate
*/

DECLARE @SupplierBrandIDString VARCHAR(2000)


SET @SupplierBrandIDString = @SupplierBrandID

SELECT
	PI.ProductInventoryID,
	S.InventoryCycleID,
	Code = COALESCE(MCP.Code, TPP.Code, 'N/A'),
	S.OldQuantityOnHandPerSystem,
	S.NewQuantityOnHandPerSystem,
	S.QuantityOfShrinkage,
	TotalCost = 0,
	PI.QuantityOnHandPerSystem,
	PCSB.SupplierShortName,
	PCSB.BrandShortName,
	SupplierBrand = PCSB.SupplierShortName + '(' + PCSB.BrandShortName + ')',	
	Product = COALESCE(MCP.Name, TPP.Name, 'N/A'),  --COALESCE(MCP., TPP., 'N/A')
	Side = COALESCE(MCP.LeftRightSide, TPP.LeftRightSide, 'N/A'),
	Size = COALESCE(MCP.Size, TPP.Size, 'N/A'),
	Gender = COALESCE(MCP.Gender, TPP.Gender, 'N/A'),
	SideSizeGender = (COALESCE(MCP.LeftRightSide, TPP.LeftRightSide, 'N/A')  + ', ' + COALESCE(MCP.Size, TPP.Size, 'N/A') + ', ' + COALESCE(MCP.Gender, TPP.Gender, 'N/A')),
	HCPCsString = dbo.ConcatHCPCS(PCP.PracticeCatalogProductID),
	Sequence = ISNULL(PCSB.Sequence, 999),
	ActualWholesaleCost = (PCP.WholesaleCost / PCP.StockingUnits),
	PracticeLocation = pl.Name,
	PracticeName = P.PracticeName,
	PI.ParLevel,
	StartOfCycleDate = Convert(Varchar, IC.StartDate) + (CASE WHEN IC.Title IS NULL THEN '' ELSE '-' + IC.Title END),
	StartDate= IC.StartDate, 
	Title = IC.Title,
	
	-- OnHand
	WholesaleCostOnHand = (Convert(money,PCP.WholesaleCost) / PCP.StockingUnits) * PI.QuantityOnHandPerSystem,
	
	--Purchased
	WholesaleCostPurchased = Coalesce((Select (SUM(SOLI.ActualWholesaleCost) / PCP.StockingUnits) -- This should be divided by stocking units
							  From dbo.SupplierOrderLineItem AS SOLI	WITH (NOLOCK)
							  	INNER JOIN dbo.SupplierOrder AS SO	WITH (NOLOCK)
									ON SOLI.SupplierOrderID = SO.SupplierOrderID
								INNER JOIN dbo.BregVisionOrder AS BVO	WITH (NOLOCK)
									ON BVO.BregVisionOrderID = SO.BregVisionOrderID
								Where SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
								AND BVO.PracticeLocationID = PL.PracticeLocationID
								Group By SOLI.PracticeCatalogProductID
							  ), 0),
	
	
	--ProductsDispensed
	WholesaleCostDispensed = Coalesce((Select (SUM(PI_DD.ActualWholesaleCost) / PCP.StockingUnits)
								FROM Dispense AS D  WITH (NOLOCK)
								INNER JOin DispenseDetail AS DD  WITH (NOLOCK)
									ON D.DispenseID = DD.DispenseID

								INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
									ON DD.DispenseDetailID = PI_DD.DispenseDetailID 
								Where
								D.PracticeLocationID = PL.PracticeLocationID
								AND PI_DD.ProductInventoryID = PI.ProductInventoryID
								AND D.IsActive = 1
								AND DD.IsActive = 1
								AND PI_DD.IsActive = 1
								Group By PI_DD.ProductInventoryID
							  ), 0),
							  
	--Shrinkage
	WholesaleCostShrinkage = (S.QuantityOfShrinkage * (PCP.WholesaleCost / PCP.StockingUnits)),
							  
	--Total Cost
	TotalSalesRetail =		0   /* 		 Coalesce((Select SUM(DD.LineTotal)
								FROM DispenseDetail AS DD  WITH (NOLOCK)
									
								INNER JOin Dispense AS D  WITH (NOLOCK)
									ON DD.DispenseID = D.DispenseID
									
								INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
									ON DD.DispenseDetailID = PI_DD.DispenseDetailID 
								Where									
									PI_DD.ProductInventoryID = PI.ProductInventoryID
									AND D.PracticeLocationID = @PracticeLocationID
									AND DD.IsActive = 1
									AND D.IsActive = 1
									AND PI_DD.IsActive = 1
									AND PI_DD.CreatedDate >= @StartDate
									AND PI_DD.CreatedDate < @EndDate
								Group By PI_DD.ProductInventoryID
							  ), 0)	*/			  
	
	From dbo.ProductInventory_ResetQuantityOnHandPerSystem as S  with (NOLOCK)
	
	Inner Join dbo.ProductInventory as PI with (NOLOCK) on S.ProductInventoryID = PI.ProductInventoryID

	INNER JOIN dbo.PracticeCatalogProduct AS PCP	WITH (NOLOCK)
		on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID

	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON PI.PracticeLocationID = PL.PracticeLocationID

	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID
		
	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		
	LEFT OUTER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
		ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID AND PCP.IsThirdPartyProduct = 0
		
	LEFT OUTER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
		ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID AND PCP.IsThirdPartyProduct = 1
	
	Inner Join InventoryCycle IC
		ON S.InventoryCycleID = IC.InventoryCycleID
		
Where
		--S.InventoryCycleID = @InventoryCycleID
		--AND 
		S.OldQuantityOnHandPerSystem <> S.NewQuantityOnHandPerSystem
		AND P.PracticeID = @PracticeID	
		AND PL.PracticeLocationID = @PracticeLocationID	
		--AND P.IsActive = 1
		--AND PL.IsActive = 1
		--AND PCP.IsActive = 1
		and PI.IsSetToActive=1
		and PI.IsActive=1
		--AND (Coalesce(MCP.IsActive, TPP.IsActive) = 1)
		
		AND S.InventoryCycleID IN 
			(Select ID 
			 FROM dbo.udf_ParseArrayToTable( @InventoryCycleID, ',') )
			 
		AND PCSB.PracticeCatalogSupplierBrandID IN 			
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable( @SupplierBrandIDString, ',') )
			 
		
		Order By S.InventoryCycleID Desc,  COALESCE(MCP.Code, TPP.Code, 'N/A')
		
End