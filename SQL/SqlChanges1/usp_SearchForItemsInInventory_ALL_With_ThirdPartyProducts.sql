USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_SearchForItemsInInventory_ALL_With_ThirdPartyProducts]    Script Date: 08/23/2010 12:16:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




/* ------------------------------------------------------------
   PROCEDURE:    [dbo].[usp_GetAllInventoryDataforPCToIventory_ALL_With_ThirdPartyProducts]
   
   Description:  Selects all products (mc and 3rdParty) that are in inventory given a practiceID
					and IsActice of 0 or 1 (false or true)
					This is called from the proc:
						usp_GetAllInventoryDataforPCToIventory						
   
   AUTHOR:       John Bongiorni 10/9/2007 2:36:40 PM
   
   Modifications:  Get the PCSBID and the swicth the alias supplierID to PracticeCatalogSupplierBrandID 
					Added column dbo.ConcatHCPCS(SUB.PracticeCatalogProductID) AS HCPCSString
   ------------------------------------------------------------ */   


ALTER PROCEDURE [dbo].[usp_SearchForItemsInInventory_ALL_With_ThirdPartyProducts]

	@PracticeLocationID int,
	@IsActive bit

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--   NOCOUNT ON


			SELECT 
				  Sub.ProductInventoryID
				, Sub.SupplierShortName
				, Sub.BrandShortName
				
				, SUB.PracticeCatalogProductID			
				, SUB.SupplierID			--.PracticeCatalogSupplierBrandID					
--				, SUB.OldSupplierID				
				, SUB.Category
				, SUB.Product
				, SUB.Code
				
				, ISNULL(SUB.Packaging, '') AS Packaging
				, ISNULL(SUB.LeftRightSide, '') AS Side
				, ISNULL(SUB.Size, '') AS Size
				, ISNULL(SUB.Gender, '') AS Gender
				
				, SUB.QOH
				, SUB.ParLevel
				, SUB.ReorderLevel
				, SUB.CriticalLevel
				, SUB.WholesaleCost
				, SUB.IsActive
				, SUB.IsThirdPartyProduct
				, dbo.ConcatHCPCS(SUB.PracticeCatalogProductID) AS HCPCSString
							
			FROM
			(
					SELECT	-- 249 active and inactive mastercatalogproducts in inventory WORKS!!!
 
							PCSB.SupplierShortName, 
							PCSB.BrandShortName, 
							
							PCSB.PracticeCatalogSupplierBrandID AS SupplierID, 
							
							PCP.PracticeCatalogProductID,		
							
                            PCSB.MasterCatalogSupplierID AS OldSupplierID, 
                            MCC.[NAME] AS Category,
                            --PCSB.BrandShortName AS Category,   --  This column should be unneccessary
																--  Only here to preserve the existing application code.
						    MCP.[Name]			AS Product,
                            
                            MCP.[Code]			AS Code,
                            MCP.[Packaging]		AS Packaging,
                            MCP.[LeftRightSide] AS LeftRightSide,
                            MCP.[Size]			AS Size,
                            MCP.[Gender]		AS Gender,
							
							PI.QuantityOnHandPerSystem AS QOH,
							PI.ParLevel,
							PI.ReorderLevel,
							PI.CriticalLevel,
							PI.ProductInventoryID,
							PCP.WholesaleCost,
							PI.Isactive,
							0 AS IsThirdPartyProduct
                     
                    FROM dbo.PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
							                    
                    INNER JOIN [PracticeCatalogProduct] AS PCP WITH(NOLOCK) 
						ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
							
                    INNER JOIN [ProductInventory] AS PI  WITH(NOLOCK)
							ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
             
                    INNER JOIN [MasterCatalogProduct] AS MCP  WITH(NOLOCK)
							ON PCP.[MasterCatalogProductID] = MCP.[MasterCatalogProductID]
							
					-- the following two inner join can be removed later		
					INNER JOIN dbo.MasterCatalogsubCategory AS MCSC
							ON MCP.MasterCatalogsubCategoryID = MCSC.MasterCatalogsubCategoryID

					INNER JOIN dbo.MasterCatalogCategory AS MCC
							ON MCSC.MasterCatalogCategoryID = MCC.MasterCatalogCategoryID
                                                 
                     WHERE  PI.[PracticeLocationID] = @PracticeLocationID
							AND PCP.IsThirdPartyProduct = 0  -- MasterCatalogProductsOnly
							AND PCSB.IsActive =	1
							AND MCP.IsActive = 1
							AND PCP.IsActive = 1							
							AND PI.IsActive = @IsActive
							AND PCP.IsActive = @IsActive
					

				UNION

					--  Third Party Product Info
					SELECT		-- 249 active and inactive mastercatalogproducts in inventory WORKS!!!
 
							PCSB.SupplierShortName, 
							PCSB.BrandShortName, 
							
							PCSB.PracticeCatalogSupplierBrandID AS SupplierID, 
				
							PCP.PracticeCatalogProductID,		       
                            PCSB.ThirdPartySupplierID AS OldSupplierID,                        
                            'NA' AS Category,
                            --PCSB.BrandShortName AS Category,   --  This column should be unneccessary
																--  Only here to preserve the existing application code.
						    TPP.[Name]			AS Product,                            
                            TPP.[Code]			AS Code,
                            TPP.[Packaging]		AS Packaging,
                            TPP.[LeftRightSide] AS LeftRightSide,
                            TPP.[Size]			AS Size,
                            TPP.[Gender]		AS Gender,
							
							PI.QuantityOnHandPerSystem AS QOH,
							PI.ParLevel,
							PI.ReorderLevel,
							PI.CriticalLevel,
							PI.ProductInventoryID,
							PCP.WholesaleCost,						
							PI.Isactive,							
							1 AS IsThirdPartyProduct 
                     
                    FROM dbo.PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
                    
                    INNER JOIN [PracticeCatalogProduct] AS PCP WITH(NOLOCK) 
						ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
							
                    INNER JOIN [ProductInventory] AS PI  WITH(NOLOCK)
							ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
             
                    INNER JOIN [ThirdPartyProduct] AS TPP  WITH(NOLOCK)
							ON PCP.[ThirdPartyProductID] = TPP.[ThirdPartyProductID]
                                                 
                     WHERE  PI.[PracticeLocationID] = @PracticeLocationID
							AND PCP.IsThirdPartyProduct = 1		-- ThirdPartyProductsOnly 
							AND PCSB.IsActive =	1
							AND PCP.IsActive = 1
							AND TPP.IsActive = 1
							AND PI.IsActive = @IsActive
							AND PCP.IsActive = @IsActive
					
				) AS Sub
			
			ORDER BY 
				  Sub.IsThirdPartyProduct
				, Sub.BrandShortName
				, Sub.PracticeCatalogProductID
				, SUB.Product
				, SUB.Code
			
END

