USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_Report_ProductsPurchased_All_1]    Script Date: 05/03/2010 23:48:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- exec [usp_Report_ProductsPurchased_all_1]  '1/1/2009', '12/31/2009', 1, 1, null, null, 1
--  Added Supplier Brand.
--  Added Sequence
--  Add IsActive = 1

ALTER PROC [dbo].[usp_Report_ProductsPurchased_All_1]  
		  @StartDate		DATETIME
		, @EndDate			DATETIME
		, @OrderBy				int
		, @OrderDirection		bit
		, @IsMedicare           bit = null
		, @MedicareOrderDirection bit = null
		, @AppType				int = Null
AS
BEGIN



SELECT @EndDate = DATEADD(DAY, 1, @EndDate)




SELECT 

		 CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
			ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
			END					AS SupplierBrand

		, SUB.ProductName		AS Product

		, SUB.Code				As Code
		
		, SUM(sub.WholesaleCost)/SUM(sub.Quantity)	 AS WholesaleCost 
	  
		, SUM(SUB.Quantity ) as Quantity		

		, SUM(CAST( SUB.ActualWholesaleCostLineTotal  AS DECIMAL(15,2)) )	AS ActualWholesaleCostLineTotal --Not used in report
		
		, SUM( CAST( SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2))) AS ActualChargeBilledLineTotal
		
		, CASE 
			WHEN MIN(CAST(Sub.IsVisionLite as int)) = 0 AND MAX(CAST(Sub.IsVisionLite as int)) = 0 THEN 'Vision'
			WHEN MIN(CAST(Sub.IsVisionLite as int)) = 1 AND MAX(CAST(Sub.IsVisionLite as int)) = 1 THEN 'Express'
			ELSE 'Both'
		  END as AppType
		
FROM


		(
		SELECT 

			  ISNULL(SRE.SupplierShortName,  PCSB.SupplierShortName) AS SupplierShortName
			, ISNULL(SRE.BrandShortName, PCSB.BrandShortName) AS BrandShortName
			, MCP.[Name] AS ProductName
			, ISNULL(MCP.Code, '') AS Code
			, PCP.WholesaleCost * QuantityOrdered AS WholesaleCost 
			
			, SOLI.QuantityOrdered as Quantity
			, pcp.WholesaleCost * SOLI.QuantityOrdered AS ActualChargeBilledLineTotal  
			, 0 as IsCashCollection
			, 0 AS ActualWholesaleCostLineTotal --Not used in report
			, P.IsVisionLite
	
		from SupplierOrderLineItem SOLI
			
		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID	
			
		INNER JOIN dbo.Practice as P with (NoLock)	
			ON pcp.PracticeID = P.PracticeID	

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
			
		LEFT OUTER JOIN dbo.zzzSalesReportExclusions SRE
			ON 	PCSB.PracticeCatalogSupplierBrandID = SRE.PracticeCatalogSupplierBrandID
			
		INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
			ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

		WHERE 

			ISNULL(SRE.IsExcluded, 0) = 0

	
		AND SOLI.CreatedDate >= @StartDate
		AND SOLI.CreatedDate < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM
		AND ((@AppType IS NULL) OR (P.IsVisionLite = @AppType))
			
		UNION ALL

		SELECT 

			  ISNULL(SRE.SupplierShortName,  PCSB.SupplierShortName) AS SupplierShortName
			, ISNULL(SRE.BrandShortName, PCSB.BrandShortName) AS BrandShortName
			, TPP.[Name] AS ProductName
			, ISNULL(TPP.Code, '') AS Code
			, PCP.WholesaleCost * QuantityOrdered AS WholesaleCost 
			
			, SOLI.QuantityOrdered as Quantity
			, pcp.WholesaleCost * SOLI.QuantityOrdered AS ActualChargeBilledLineTotal  
			, 0 as IsCashCollection
			, 0 AS ActualWholesaleCostLineTotal --Not used in report
			, P.IsVisionLite

		from SupplierOrderLineItem SOLI

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
			
		INNER JOIN dbo.Practice as P with (NoLock)	
			ON pcp.PracticeID = P.PracticeID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
						
		LEFT OUTER JOIN dbo.zzzSalesReportExclusions SRE
			ON 	PCSB.PracticeCatalogSupplierBrandID = SRE.PracticeCatalogSupplierBrandID
						
		INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
			ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

	
	
		WHERE 

			ISNULL(SRE.IsExcluded, 0) = 0

			AND SOLI.CreatedDate >= @StartDate
			AND SOLI.CreatedDate < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM	

			AND ((@AppType IS NULL) OR (P.IsVisionLite = @AppType))
			
		) AS Sub
		
		Group by
			Sub.SupplierShortName,
			Sub.BrandShortName,
			Sub.ProductName,
			Sub.Code

ORDER BY
	CASE 
		--1 = ProductName
		WHEN (@OrderBy = 1) and @OrderDirection = 0 THEN (RANK() OVER (ORDER BY Sub.SupplierShortName,Sub.BrandShortName,SUB.ProductName DESC))
		WHEN (@OrderBy = 1) and @OrderDirection = 1 THEN (RANK() OVER (ORDER BY Sub.SupplierShortName,Sub.BrandShortName,SUB.ProductName     ))
		WHEN (@OrderBy = 2) and @OrderDirection = 0 THEN (RANK() OVER (ORDER BY Sub.SupplierShortName,Sub.BrandShortName,SUM(SUB.Quantity) DESC))
		WHEN (@OrderBy = 2) and @OrderDirection = 1 THEN (RANK() OVER (ORDER BY Sub.SupplierShortName,Sub.BrandShortName,SUM(SUB.Quantity)     ))
		
		WHEN (@OrderBy = 3) and @OrderDirection = 0 THEN (RANK() OVER (ORDER BY Sub.SupplierShortName,Sub.BrandShortName,SUM(case SUB.IsCashCollection
			when 1 then 0
			when 0 then CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )	
			end) DESC))
		WHEN (@OrderBy = 3) and @OrderDirection = 1 THEN (RANK() OVER (ORDER BY Sub.SupplierShortName,Sub.BrandShortName,SUM(case SUB.IsCashCollection
			when 1 then 0
			when 0 then CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )	
			end)))
	END

END

