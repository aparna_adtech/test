USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetBVOSupplierLineItemInfoForFaxEmail_New]    Script Date: 04/12/2012 15:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DECLARE @SupplierOrderID INT
--SET @SupplierOrderID = 201  --198   --204  --  198


--			string ShortName;
--            string Code;
--            string Packaging;
--            string Side;
--            string Color;
--            string Gender;
--            decimal ActualWholesaleCost;
--            Int32 QuantityOrdered;
--            decimal LineTotal;
--            string SupplierName;

/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_GetBVOSupplierLineItemInfoForFaxEmail_2
   
   Description:  Select the SupplierOrder Line Items for a given SupplierOrderID.
				Used to Get SupplierLineItemdetails for fax and emailed POS.
				Called by class:  FaxEmailPOs   					
					
   AUTHOR:       Greg Ross 08/29/07
   
   Modifications:  John Bongiorni 2007.11.07  13:55
			Make sure that only one record per product order is selected.
   
   Note:  The commented fields will work for testing and debugging.
			There are commented as they are not  neccessary for the query.
			If a product info field is null then pass back empty string.
			
   For Vendors the SupplierOrderID is sent in for the first brand of the Vendor.
   However, all	Supplier Order info should be pulled for the Vendor.
   TODO: Make this work.		
   ------------------------------------------------------------ */  

--SELECT * FROM SupplierOrder AS SO 
--INNER JOIN PracticeCatalogSupplierBrand AS PCSB
-- ON SO.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
-- WHERE PCSB.supplierName LIKE '%STATE%' 
-- ORDER BY SupplierOrderID DESC 

--  1114	543	45	28.00	1	6	NULL	9	Tri-State Orthopedics	Tri-State Orthopedics	CoreFlex	CoreFlex	
--  1113	543	43	15.00	1	6	NULL	9	Tri-State Orthopedics	Tri-State Orthopedics	Medline	Medline	
--  1112	543	41	0.00	1	6	NULL	9	Tri-State Orthopedics	Tri-State Orthopedics	Hely Weber	Hely Weber	


--                          EXEC [usp_GetBVOSupplierLineItemInfoForFaxEmail] 1112
--                          EXEC [usp_GetBVOSupplierLineItemInfoForFaxEmail] 1113
--                          EXEC [usp_GetBVOSupplierLineItemInfoForFaxEmail] 1114
--				test above is the old proc, below is the new.
--						  EXEC [usp_GetBVOSupplierLineItemInfoForFaxEmail_New]   1112


ALTER PROCEDURE [dbo].[usp_GetBVOSupplierLineItemInfoForFaxEmail_New]   --  1112
		@SupplierOrderID int
AS
BEGIN



		--  The following code is to be able to pull all supplier orders for a given vendor.
		--  The vendor and all of its related PCSBIDs are obtained via the given supplierOrderID.
		--  If the vendor has multiple supplierOrders for the BregVisionOrder (Purchase Number.)
		
		DECLARE @BregVisionOrderID INT   --  The BregVisionOrderID for the given SupplierOrderID
		DECLARE @PracticeCatalogSupplierBrandID INT  --  For Vendors, this is a PCSBID that is representative all 
													 --    PCSBIDs for the the the Supplier Orders have the same 
													 --    Vendor (and therefore the same ThirdPartyID)
		
		--  Get the PCSBID for the given SupplierOrder
		SELECT @PracticeCatalogSupplierBrandID = 
			  (
				SELECT PracticeCatalogSupplierBrandID
				FROM SupplierOrder AS SO WITH (NOLOCK)
				WHERE SO.SupplierOrderID = @SupplierOrderID
			  )


		--  Vendor with its ThirdPartySupplierID and a list of its PracticeCatalogSupplierBrandID
		DECLARE @VendorList AS TABLE
		(
			  ThirdPartySupplierID				INT
			, PracticeCatalogSupplierBrandID	INT
		) 

		--  Get the ThirdPartySupplierID for the representative PracticeCatalogSupplierBrandID
		--	Then get all of the PCSupplierBrands for the ThirdPartySupplierID
		INSERT INTO @VendorList
			(
			  ThirdPartySupplierID				
			, PracticeCatalogSupplierBrandID	
		) 
		SELECT
			ThirdPartySupplierID
		  , PracticeCatalogSupplierBrandID
		FROM
			dbo.PracticeCatalogSupplierBrand AS PCSB WITH (NOLOCK)
		WHERE
			PCSB.IsActive = 1
			AND PCSB.ThirdPartySupplierID = 
				(
				  SELECT
					PCSB2.ThirdPartySupplierID
				  FROM
					PracticeCatalogSupplierBrand AS PCSB2 WITH (NOLOCK)
				  WHERE
					PCSB2.PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID
					AND PCSB2.IsActive = 1
					AND PCSB2.IsThirdPartySupplier = 1
				)
	        
		--  For Testing Purposes only.	
	--	SELECT * 
	--	FROM @VendorList


		--  Get the BregVisionOrderID for the given SupplierOrderID
		SELECT @BregVisionOrderID = 
			(
			SELECT BVO.BregVisionOrderID
			FROM BregVisionOrder AS BVO WITH (NOLOCK)
			INNER JOIN SupplierOrder AS SO WITH (NOLOCK)
				ON BVO.BregVisionOrderID = SO.BregVisionOrderID
			WHERE SO.SupplierOrderID = @SupplierOrderID
			)

		--  All Supplier Orders within the Breg Vision Order with the same Supplier.
		--    Will be multiple for the Vendor.  (One per Brand for Vendors.)
		DECLARE @VendorSupplierOrderList AS TABLE
			(
				SupplierOrderID INT
			)

		INSERT INTO @VendorSupplierOrderList 
		SELECT SO.SupplierOrderID
		FROM SupplierOrder AS SO WITH (NOLOCK)
		WHERE SO.BregVisionOrderID = @BregVisionOrderID
			AND SO.PracticeCatalogSupplierBrandID 
						IN (
								SELECT PracticeCatalogSupplierBrandID 
								FROM @VendorList
							)
								
		
		--  For Testing Only						
--		SELECT SupplierOrderID 
--		FROM @VendorSupplierOrderList


			SELECT 

				SUB.PracticeCatalogSupplierBrandID AS SupplierID

				, SUB.SupplierName							 --  This is the Supplier
				, SUB.BrandName 			--AS SupplierName  --  This is the Brand		

				, SUB.Product					AS ShortName
				, SUB.Code
				
				, ISNULL(SUB.Packaging, '')		AS Packaging
				, ISNULL(SUB.LeftRightSide, '')		AS Side
				, ISNULL(SUB.Size, '')			AS Size
				, ISNULL(SUB.Gender, '')		AS Gender
				, ISNULL(SUB.Color, '')			AS Color
				, SUB.IsCustomBrace				AS IsCustomBrace

				, SUB.ActualWholesaleCost
				, SUB.QuantityOrdered
				, SUB.LineTotal
				
				, SUB.IsThirdPartySupplier
				, SUB.IsVendor
				, CAST(SUB.IsLogoPart as Bit) as IsLogoPart
			FROM

				(
					SELECT 
--							SOLI.SupplierOrderID,
--							SOLI.SupplierOrderLineItemID,
--							MCP.MasterCatalogProductID AS ProductID,
--							

							PCSB.PracticeCatalogSupplierBrandID,

							PCSB.SupplierName, 
							PCSB.BrandName,
							
--							PCSB.SupplierShortName, 
--							PCSB.BrandShortName,
							
							MCP.[Name]			AS Product,      
							MCP.[Code]			AS Code,
							MCP.[Packaging]		AS Packaging,
							MCP.[LeftRightSide] AS LeftRightSide,
							MCP.[Size]			AS Size,
							MCP.[Gender]		AS Gender,
							MCP.[Color]			AS Color,
							MCP.IsCustomBrace	AS IsCustomBrace,
							
							SOLI.ActualWholesaleCost,
							SOLI.QuantityOrdered,
							SOLI.LineTotal,
							0					AS IsThirdPartySupplier,
							0					AS IsVendor,
							SOLI.IsLogoPart

					FROM 
						dbo.SupplierOrderLineItem AS SOLI		WITH(NOLOCK)
						
					INNER JOIN PracticeCatalogProduct AS PCP		WITH(NOLOCK)
						ON SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
					
					INNER JOIN dbo.MasterCatalogProduct AS MCP		WITH(NOLOCK)
						ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
					
					INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK) 
						ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
					
					WHERE SOLI.SupplierOrderID = @SupplierOrderID
						AND PCP.IsThirdPartyProduct = 0	
						AND SOLI.IsActive = 1
						AND PCP.IsActive = 1
						AND MCP.IsActive = 1
						AND PCSB.IsActive = 1
						
				UNION
						--  ThirdPartySupplier that are MFGs.
						SELECT 
							
--							SOLI.SupplierOrderID,
--							SOLI.SupplierOrderLineItemID,
--							TPP.ThirdPartyProductID AS ProductID,
--														

							PCSB.PracticeCatalogSupplierBrandID,
							
							PCSB.SupplierName, 
							PCSB.BrandName,
							
--							PCSB.SupplierShortName, 
--							PCSB.BrandShortName, 

							TPP.[Name]			AS Product,      
							TPP.[Code]			AS Code,
							TPP.[Packaging]		AS Packaging,
							TPP.[LeftRightSide] AS LeftRightSide,
							TPP.[Size]			AS Size,
							TPP.[Gender]		AS Gender,
							TPP.[Color]			AS Color,
							0					AS IsCustomBrace,
							
							SOLI.ActualWholesaleCost,
							SOLI.QuantityOrdered,
							SOLI.LineTotal,
							1					AS IsThirdPartySupplier,
							0					AS IsVendor,
							0					AS IsLogoPart

					FROM 
						dbo.SupplierOrderLineItem AS SOLI	WITH(NOLOCK)
					
					INNER JOIN PracticeCatalogProduct AS PCP   WITH(NOLOCK)
						ON SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
				
					INNER JOIN dbo.ThirdPartyProduct AS TPP    WITH(NOLOCK)
						ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
						
					INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK) 
						ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
							
					INNER JOIN dbo.ThirdPartySupplier AS TPS     WITH(NOLOCK)
						ON PCSB.ThirdPartySupplierID = TPS.ThirdPartySupplierID
							
					WHERE SOLI.SupplierOrderID = @SupplierOrderID
						AND PCP.IsThirdPartyProduct = 1
						AND TPP.IsActive = 1
						AND TPS.IsActive = 1
						AND SOLI.IsActive = 1
						AND PCSB.IsActive = 1
						AND TPS.IsVendor = 0  --  A Manufacture, NOT a Vendor


				UNION
						--  ThirdPartySupplier that are Vendors.
						SELECT 
							
--							SOLI.SupplierOrderID,
--							SOLI.SupplierOrderLineItemID,
--							TPP.ThirdPartyProductID AS ProductID,
--														
							PCSB.PracticeCatalogSupplierBrandID,
							
							PCSB.SupplierName, 
							PCSB.BrandName,
							
--							PCSB.SupplierShortName, 
--							PCSB.BrandShortName, 

							TPP.[Name]			AS Product,      
							TPP.[Code]			AS Code,
							TPP.[Packaging]		AS Packaging,
							TPP.[LeftRightSide] AS LeftRightSide,
							TPP.[Size]			AS Size,
							TPP.[Gender]		AS Gender,
							TPP.[Color]			AS Color,
							0					AS IsCustomBrace,
							
							SOLI.ActualWholesaleCost,
							SOLI.QuantityOrdered,
							SOLI.LineTotal,
							1					AS IsThirdPartySupplier,
							1					AS IsVendor,
							0					AS IsLogoPart

					FROM 
						dbo.SupplierOrderLineItem AS SOLI	WITH(NOLOCK)
					
					INNER JOIN PracticeCatalogProduct AS PCP   WITH(NOLOCK)
						ON SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
				
					INNER JOIN dbo.ThirdPartyProduct AS TPP    WITH(NOLOCK)
						ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
						
					INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK) 
						ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
							
					INNER JOIN dbo.ThirdPartySupplier AS TPS     WITH(NOLOCK)
						ON PCSB.ThirdPartySupplierID = TPS.ThirdPartySupplierID
					
					--INNER JOIN @VendorList
							
					WHERE SOLI.SupplierOrderID IN (SELECT SupplierOrderID FROM @VendorSupplierOrderList)
						AND PCP.IsThirdPartyProduct = 1
						AND TPP.IsActive = 1
						AND TPS.IsActive = 1
						AND SOLI.IsActive = 1
						AND PCSB.IsActive = 1
						AND TPS.IsVendor = 1  --  A Vendor, NOT a Manufacturer.

				) AS Sub
			
			ORDER BY 
				  SUB.BrandName
				, SUB.Product	
				, SUB.Code


END