use [BregVision]
Go
/****** Object:  Table [dbo].[SuperBillCategory]    Script Date: 06/23/2009 22:03:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SuperBillCategory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SuperBillCategory](
	[SuperBillCategoryID] [int] IDENTITY(1,1) NOT NULL,
	[PracticeLocationID] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Sequence] [int] NULL,
	[ColumnNumber] [int] NULL,
	[CreatedUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedUserID] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_SuperBillCategory] PRIMARY KEY CLUSTERED 
(
	[SuperBillCategoryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuperBillCategory_PracticeLocation]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuperBillCategory]'))
ALTER TABLE [dbo].[SuperBillCategory]  WITH CHECK ADD  CONSTRAINT [FK_SuperBillCategory_PracticeLocation] FOREIGN KEY([PracticeLocationID])
REFERENCES [dbo].[PracticeLocation] ([PracticeLocationID])
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuperBillCategory_PracticeLocation]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuperBillCategory]'))
ALTER TABLE [dbo].[SuperBillCategory] CHECK CONSTRAINT [FK_SuperBillCategory_PracticeLocation]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_SuperBillCategory_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[SuperBillCategory] ADD  CONSTRAINT [DF_SuperBillCategory_IsActive]  DEFAULT ((1)) FOR [IsActive]
END


GO

/****** Object:  Table [dbo].[SuperBillProduct]    Script Date: 06/23/2009 22:03:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SuperBillProduct]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SuperBillProduct](
	[SuperBillProductID] [int] IDENTITY(1,1) NOT NULL,
	[PracticeCatalogProductID] [int] NOT NULL,
	[PracticeLocationID] [int] NOT NULL,
	[SuperBillCategoryID] [int] NOT NULL,
	[Name] [varchar](50) NULL,
	[HCPCs] [varchar](10) NULL,
	[Deposit] [smallmoney] NULL,
	[Cost] [smallmoney] NULL,
	[Charge] [smallmoney] NULL,
	[FlowsheetOnly] [bit] NOT NULL,
	[Sequence] [int] NULL,
	[CreateUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedUserID] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[IsAddOn] [bit] NOT NULL,
	[SuperBillProductID_FK] [int] NULL,
 CONSTRAINT [PK_SuperBillProduct] PRIMARY KEY CLUSTERED 
(
	[SuperBillProductID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

--SET ANSI_PADDING OFF
--GO



/****** Object:  Index [IX_SuperBillProduct]    Script Date: 06/23/2009 22:03:03 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SuperBillProduct]') AND name = N'IX_SuperBillProduct')
CREATE NONCLUSTERED INDEX [IX_SuperBillProduct] ON [dbo].[SuperBillProduct] 
(
	[PracticeCatalogProductID] ASC,
	[PracticeLocationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuperBillProduct_PracticeCatalogProduct]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuperBillProduct]'))
ALTER TABLE [dbo].[SuperBillProduct]  WITH CHECK ADD  CONSTRAINT [FK_SuperBillProduct_PracticeCatalogProduct] FOREIGN KEY([PracticeCatalogProductID])
REFERENCES [dbo].[PracticeCatalogProduct] ([PracticeCatalogProductID])
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuperBillProduct_PracticeCatalogProduct]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuperBillProduct]'))
ALTER TABLE [dbo].[SuperBillProduct] CHECK CONSTRAINT [FK_SuperBillProduct_PracticeCatalogProduct]
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuperBillProduct_PracticeLocation]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuperBillProduct]'))
ALTER TABLE [dbo].[SuperBillProduct]  WITH CHECK ADD  CONSTRAINT [FK_SuperBillProduct_PracticeLocation] FOREIGN KEY([PracticeLocationID])
REFERENCES [dbo].[PracticeLocation] ([PracticeLocationID])
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuperBillProduct_PracticeLocation]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuperBillProduct]'))
ALTER TABLE [dbo].[SuperBillProduct] CHECK CONSTRAINT [FK_SuperBillProduct_PracticeLocation]
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuperBillProduct_SuperBillCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuperBillProduct]'))
ALTER TABLE [dbo].[SuperBillProduct]  WITH CHECK ADD  CONSTRAINT [FK_SuperBillProduct_SuperBillCategory] FOREIGN KEY([SuperBillCategoryID])
REFERENCES [dbo].[SuperBillCategory] ([SuperBillCategoryID])
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuperBillProduct_SuperBillCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuperBillProduct]'))
ALTER TABLE [dbo].[SuperBillProduct] CHECK CONSTRAINT [FK_SuperBillProduct_SuperBillCategory]
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuperBillProduct_SuperBillProduct]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuperBillProduct]'))
ALTER TABLE [dbo].[SuperBillProduct]  WITH CHECK ADD  CONSTRAINT [FK_SuperBillProduct_SuperBillProduct] FOREIGN KEY([SuperBillProductID_FK])
REFERENCES [dbo].[SuperBillProduct] ([SuperBillProductID])
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuperBillProduct_SuperBillProduct]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuperBillProduct]'))
ALTER TABLE [dbo].[SuperBillProduct] CHECK CONSTRAINT [FK_SuperBillProduct_SuperBillProduct]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_SuperBillProduct_FlowsheetOnly]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[SuperBillProduct] ADD  CONSTRAINT [DF_SuperBillProduct_FlowsheetOnly]  DEFAULT ((0)) FOR [FlowsheetOnly]
END

GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_SuperBillProduct_IsAddOn]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[SuperBillProduct] ADD  CONSTRAINT [DF_SuperBillProduct_IsAddOn]  DEFAULT ((0)) FOR [IsAddOn]
END

GO


