use [BregVision]
go

if not exists (select * from dbo.syscolumns where id = object_id(N'Practice') and name=(N'PaymentTypeID'))
alter table [Practice] add [PaymentTypeID] int DEFAULT 1 NOT NULL
GO
