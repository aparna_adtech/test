USE [BregVision]
GO

/****** Object:  Table [dbo].[PracticeApplicationAgreementTerms]    Script Date: 04/01/2010 12:46:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PracticeApplicationAgreementTerms](
	[PracticeAppAgreementTermsID] [int] IDENTITY(1,1) NOT NULL,
	[Terms] [varchar](max) NULL,
 CONSTRAINT [PK_PracticeApplicationAgreementTerms] PRIMARY KEY CLUSTERED 
(
	[PracticeAppAgreementTermsID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO




