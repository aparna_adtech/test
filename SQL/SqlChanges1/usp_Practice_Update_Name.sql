USE [BregVision_STG]
GO
/****** Object:  StoredProcedure [dbo].[usp_Practice_Update_Name]    Script Date: 11/22/2014 9:01:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Practice_Update_Name
   
   Description:  Updates a record in the table Practice
   				 Sets the Name column.
				
   AUTHOR:       John Bongiorni 7/25/2007 9:45:21 PM
   
   Modifications:  
   Exec [usp_Practice_Update_Name] 1, 'TEST PRACTICE', 1, 0, 2
   Exec [usp_Practice_Update_Name] 1, 'TEST PRACTICE', 2, 0, NULL
   Exec [usp_Practice_Update_Name] 1, 'TEST PRACTICE', 1, 0, 1
   
   Added FaxDispenseReceiptEnabled				Ysi             09/07/2013
   ------------------------------------------------------------ */  

ALTER PROCEDURE [dbo].[usp_Practice_Update_Name]
(
	  @PracticeID                    INT
	, @PracticeName                  VARCHAR(50)
	, @ModifiedUserID                INT
	, @IsOrthoSelect				 bit
	, @PaymentTypeID				 Int = Null
	, @PracticeCode					 varchar(50) = Null
	, @BillingStartDate				 DateTime = Null
	, @CameraScanEnabled			 bit = 0
	, @EMRIntegrationPullEnabled	 bit = 0
	, @FaxDispenseReceiptEnabled	 bit = 0
	, @IsSecureMessagingInternalEnabled bit = 0
	, @IsSecureMessagingExternalEnabled bit = 0
)
AS
BEGIN
	DECLARE @Err INT

	UPDATE dbo.Practice
	SET
		  PracticeName = @PracticeName
		, IsOrthoSelect = @IsOrthoSelect
		, CameraScanEnabled = @CameraScanEnabled
		, EMRIntegrationPullEnabled = @EMRIntegrationPullEnabled
		, FaxDispenseReceiptEnabled = @FaxDispenseReceiptEnabled
		, IsSecureMessagingInternalEnabled = @IsSecureMessagingInternalEnabled
		, IsSecureMessagingExternalEnabled = @IsSecureMessagingExternalEnabled
		, ModifiedUserID = @ModifiedUserID
		, ModifiedDate = GETDATE()
		, PaymentTypeID = Coalesce(@PaymentTypeID, Practice.PaymentTypeID)
		, BillingStartDate = Coalesce(@BillingStartDate, Practice.BillingStartDate)
	FROM Practice

	WHERE 
		PracticeID = @PracticeID
		
	------------------------------------------	
	IF EXISTS(Select 1 from BregVisionPracticeCode BVPC Where BVPC.PracticeID = @PracticeID)
		UPDATE dbo.BregVisionPracticeCode
		SET 
			PracticeCode = Coalesce(@PracticeCode, BregVisionPracticeCode.PracticeCode)
		From BregVisionPracticeCode
		
		Where 
			PracticeID = @PracticeID
	ELSE
		Insert into BregVisionPracticeCode(PracticeID, PracticeCode) Values(@PracticeID, @PracticeCode)


	SET @Err = @@ERROR

	RETURN @Err
End

