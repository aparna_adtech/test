	
	--Add these to Create Table Definition
	/*
	CreatedUserID int NOT NULL,
	CreatedDate datetime NOT NULL,
	ModifiedUserID int NULL,
	ModifiedDate datetime NULL,
	IsActive bit NOT NULL
	*/
	
--Add these when table already exists
if not exists (select * from dbo.syscolumns where id = object_id(N'TableName') and name=(N'CreatedUserID'))
alter table [TableName] add CreatedUserID [int] DEFAULT 1 NOT NULL
GO

if not exists (select * from dbo.syscolumns where id = object_id(N'TableName') and name=(N'CreatedDate'))
alter table [TableName] add CreatedDate [DateTime] DEFAULT GetDate() NOT NULL
GO

if not exists (select * from dbo.syscolumns where id = object_id(N'TableName') and name=(N'ModifiedUserID'))
alter table [TableName] add ModifiedUserID [int] NULL
GO

if not exists (select * from dbo.syscolumns where id = object_id(N'TableName') and name=(N'ModifiedDate'))
alter table [TableName] add ModifiedDate [DateTime] NULL
GO

if not exists (select * from dbo.syscolumns where id = object_id(N'TableName') and name=(N'IsActive'))
alter table [TableName] add IsActive [bit] DEFAULT 1 NOT NULL
GO

if not exists (select * from dbo.syscolumns where id = object_id(N'TableName') and name=(N'timestamp'))
alter table [TableName] add timestamp [timestamp] NOT NULL
GO

