use [BregVision]
go

if not exists (select * from dbo.syscolumns where id = object_id(N'Campaign') and name=(N'Url'))
alter table [Campaign] add [Url] [nvarchar](100) NULL
GO
