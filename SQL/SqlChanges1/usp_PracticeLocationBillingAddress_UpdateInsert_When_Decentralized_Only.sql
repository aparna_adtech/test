USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_PracticeLocationBillingAddress_UpdateInsert_When_Decentralized_Only]    Script Date: 04/21/2012 17:40:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_PracticeLocationBillingAddress_UpdateInsert_When_Decentralized_Only]
(
	  @PracticeLocationID			 INT
    , @AddressID                     INT = NULL OUTPUT
	, @AttentionOf					 VARCHAR(50) = NULL
    , @AddressLine1                  VARCHAR(50)
    , @AddressLine2                  VARCHAR(50) = NULL
    , @City                          VARCHAR(50)
    , @State                         CHAR(2)
    , @ZipCode                       CHAR(5)
    , @ZipCodePlus4                  CHAR(4) = NULL
    , @UserID						 INT
    , @OracleCode					 VARCHAR(10) = NULL
    , @IsOracleCodeValid			 Bit = 0
)
AS
BEGIN

--  DECLARE @AddressID INT
DECLARE @Err INT
DECLARE	@IsBillingCentralized BIT

		--  Check if the Practice Location's Parent (Practice) has Decentralized Billing is so continue else do nothing.
		
		EXEC	@Err = [dbo].[usp_Practice_Select_IsBillingCentralized_By_PracticeLocationID_By_Params]
				@PracticeLocationID = @PracticeLocationID,
				@IsBillingCentralized = @IsBillingCentralized OUTPUT

		
		
		IF ( @IsBillingCentralized = 0 )
		BEGIN

			-- Check if PracticeLocationBillingAddress already has a record for the practiceLocation	
			--  Also make sure that the record is active (may have inactive records left over from Centralized Billing.)
			SELECT @AddressID = AddressID 
			FROM PracticeLocationBillingAddress 
			WHERE PracticeLocationID = @PracticeLocationID	
				AND IsActive = 1
			
			SELECT  @Err = @@Error
			
			IF ( @AddressID IS NOT NULL ) --so UPDATE
				BEGIN
					
					DECLARE	@return_value int

					EXEC	@Err = [dbo].[usp_PracticeLocationBillingAddress_Update_When_Decentralized_Only]
							@PracticeLocationID = @PracticeLocationID
							, @AttentionOf = @AttentionOf
							, @AddressLine1 = @AddressLine1
							, @AddressLine2 = @AddressLine2
							, @City = @City
							, @State = @State
							, @ZipCode = @ZipCode
							, @ZipCodePlus4 = @ZipCodePlus4 
							, @ModifiedUserID = @UserID
							, @OracleCode = @OracleCode
							, @IsOracleCodeValid = @IsOracleCodeValid
					
					SELECT  @Err = @@Error
					
				END
			
			IF ( @AddressID IS NULL ) --so INSERT
				BEGIN

					EXEC	@Err = [dbo].[usp_PracticeLocationBillingAddress_Insert_When_Decentralized_Only]
								  @PracticeLocationID = @PracticeLocationID
								, @AddressID = @AddressID OUTPUT
								, @AttentionOf = @AttentionOf
								, @AddressLine1 = @AddressLine1
								, @AddressLine2 = @AddressLine2
								, @City = @City
								, @State = @State
								, @ZipCode = @ZipCode
								, @ZipCodePlus4 = @ZipCodePlus4 
								, @CreatedUserID = @UserID
								, @OracleCode = @OracleCode
								, @IsOracleCodeValid = @IsOracleCodeValid
								
					SELECT  @Err = @@Error

					SELECT	@AddressID = @AddressID
					
					SELECT  @Err = @@Error
					
				END   
		
			END  --  IF ( @IsBillingCentralized = 0 )
		
		RETURN @Err
		
	END

