SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mike Sneen
-- Create date: 5/12/2010
-- Description:	Returns Similar Breg Products By ProductInventoryID

-- EXEC GetSimilarBregProducts_ByProductInventoryID '6930'
-- =============================================
Create PROCEDURE GetSimilarBregProducts_ByProductInventoryID
	@ProductInventoryIDString Varchar(Max)
AS
BEGIN

	SET NOCOUNT ON;
	
Select 
	*
from 
	MasterCatalogProduct MCP
	Inner Join (MasterCatalogProduct sMCP
	Inner Join PracticeCatalogProduct sPCP
		ON sMCP.MasterCatalogProductID = sPCP.MasterCatalogProductID
	Inner Join ProductInventory sPI
		ON sPCP.PracticeCatalogProductID = sPI.PracticeCatalogProductID
		   AND sPI.ProductInventoryID in(SELECT ID FROM dbo.udf_ParseArrayToTable(@ProductInventoryIDString, ',')) )
		ON MCP.MasterCatalogSubCategoryID = sMCP.MasterCatalogSubCategoryID  --Restricting Results
		   AND MCP.Size = sMCP.Size											 --Restricting Results
			
Where 
MCP.IsActive = 1


AND MCP.MasterCatalogProductID in
(
	Select 
		mPCP.MasterCatalogProductID 
	FROM
		
	PracticeCatalogProduct mPCP
	Inner Join ProductHCPCs mPH    --Get the HCPCs for the Selected MCPs
		on mPCP.PracticeCatalogProductID = mPH.PracticeCatalogProductID
		
	Where 
		mPCP.IsThirdPartyProduct = 0
		AND mPCP.IsActive = 1

		AND mPH.HCPCS IN
		(
			--get the Matching HCPCs from the PID passed in
			Select 
				PH.HCPCS
			from 
			
			ProductHCPCS PH
			Inner Join ProductInventory PI
				ON PI.PracticeCatalogProductID = PH.PracticeCatalogProductID
			Where PI.ProductInventoryID = (SELECT ID FROM dbo.udf_ParseArrayToTable(@ProductInventoryIDString, ','))
			Group By PH.HCPCS
		)
)


END
GO

