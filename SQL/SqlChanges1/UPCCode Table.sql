USE [BregVision]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UPCCode_MasterCatalogProduct]') AND parent_object_id = OBJECT_ID(N'[dbo].[UPCCode]'))
ALTER TABLE [dbo].[UPCCode] DROP CONSTRAINT [FK_UPCCode_MasterCatalogProduct]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UPCCode_ThirdPartyProduct]') AND parent_object_id = OBJECT_ID(N'[dbo].[UPCCode]'))
ALTER TABLE [dbo].[UPCCode] DROP CONSTRAINT [FK_UPCCode_ThirdPartyProduct]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__UPCCode__Created__55016A90]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[UPCCode] DROP CONSTRAINT [DF__UPCCode__Created__55016A90]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__UPCCode__Created__55F58EC9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[UPCCode] DROP CONSTRAINT [DF__UPCCode__Created__55F58EC9]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__UPCCode__IsActiv__56E9B302]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[UPCCode] DROP CONSTRAINT [DF__UPCCode__IsActiv__56E9B302]
END

GO

USE [BregVision]
GO

/****** Object:  Table [dbo].[UPCCode]    Script Date: 12/31/2011 18:30:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UPCCode]') AND type in (N'U'))
DROP TABLE [dbo].[UPCCode]
GO

USE [BregVision]
GO

/****** Object:  Table [dbo].[UPCCode]    Script Date: 12/31/2011 18:30:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UPCCode](
	[UPCCodeID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[MasterCatalogProductID] [int] NULL,
	[ThirdPartyProductID] [int] NULL,
	[CreatedUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedUserID] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
	[timestamp] [timestamp] NOT NULL,
 CONSTRAINT [PK_UPCCode] PRIMARY KEY CLUSTERED 
(
	[UPCCodeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[UPCCode]  WITH CHECK ADD  CONSTRAINT [FK_UPCCode_MasterCatalogProduct] FOREIGN KEY([MasterCatalogProductID])
REFERENCES [dbo].[MasterCatalogProduct] ([MasterCatalogProductID])
GO

ALTER TABLE [dbo].[UPCCode] CHECK CONSTRAINT [FK_UPCCode_MasterCatalogProduct]
GO

ALTER TABLE [dbo].[UPCCode]  WITH CHECK ADD  CONSTRAINT [FK_UPCCode_ThirdPartyProduct] FOREIGN KEY([ThirdPartyProductID])
REFERENCES [dbo].[ThirdPartyProduct] ([ThirdPartyProductID])
GO

ALTER TABLE [dbo].[UPCCode] CHECK CONSTRAINT [FK_UPCCode_ThirdPartyProduct]
GO

ALTER TABLE [dbo].[UPCCode] ADD  DEFAULT ((1)) FOR [CreatedUserID]
GO

ALTER TABLE [dbo].[UPCCode] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[UPCCode] ADD  DEFAULT ((1)) FOR [IsActive]
GO


------------------------  Move Codes (MCP) ---------------------------------

Insert into UPCCode
(Code,
 CreatedUserID,
 CreatedDate,
 IsActive,
 MasterCatalogProductID
 ) 

	Select 
		mcp.UPCCode,
		1,
		GETDATE(),
		1,
		mcp.MasterCatalogProductID
	From 
		MasterCatalogProduct mcp
	where 
		mcp.UPCCode is not null AND mcp.UPCCode <> ''
		
GO
----------------- Delete UPCCode field from MasterCatalog Product
/*
ALTER TABLE dbo.MasterCatalogProduct DROP COLUMN UPCCode ;
GO
*/

------------------------  Move Codes (TPP) ---------------------------------
 Insert into UPCCode
(Code,
 CreatedUserID,
 CreatedDate,
 IsActive,
 ThirdPartyProductID
 ) 

	Select 
		tpp.UPCCode,
		1,
		GETDATE(),
		1,
		tpp.ThirdPartyProductID
	From 
		ThirdPartyProduct tpp
	where 
		tpp.UPCCode is not null AND tpp.UPCCode <> ''
		
		
	----------------- Delete UPCCode field from Third Party Product
	/*
ALTER TABLE dbo.ThirdPartyProduct DROP COLUMN UPCCode ;
GO
*/
	
	
	
	