USE BregVision
GO


if not exists (select * from dbo.syscolumns where id = object_id(N'Practice') and name=(N'IsOrthoSelect'))
ALTER TABLE [dbo].[Practice] ADD [IsOrthoSelect] [bit] DEFAULT ((0)) NOT NULL 
GO