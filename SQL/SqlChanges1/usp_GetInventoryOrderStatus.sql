USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetInventoryOrderStatus]    Script Date: 08/12/2011 14:29:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Greg Ross>
-- Create date: <07/19/07>
-- Description:	<Proc to get order, reorder critical status on items to display on the Breg Vision dashboard>
--  Modification:  
--          20071016 John Bongiorni 
--					Rewritten to correct faulty logic.
--					Add ThirdPartyProducts.
--			SELECT * FROM pROductinventory where practicelocationid = 3 and isactive = 1
--			2007.11.14 John Bongiorni
--				Add LeftRightSide and Size columns.

--	2008.01.28  JB  Test No Flagging for home page when the ParLevel is set to zero.
--  Remove if this does not work.
--  2008.02.05  John Bongiorni
--	Use the column ProductInventory.isNotFlaggedForReorder to determine if the product is highlighted on the home InvOrderStatus grid.
--  as critical or reorder.  ProductInventory.isNotFlaggedForReorder is derived from the udf.
--  2008.02.15  John Bongiorni  update:
--				(
--					IsNotFlaggedForReorder = 0		--  2008.02.05 JB
--					OR
--					( ParLevel > 0)	--  2008.01.28  JB  Test No Flagging for home page  -- Can Remove This after release on 2008.02.06!!!
--				)
-- Exec [usp_GetInventoryOrderStatus] 6, 296
-- 12/31/2011  Removed UPCCode Column and put in a separate Table
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetInventoryOrderStatus]  --  1,3   --6, 18  --1,3 
	-- Add the parameters for the stored procedure here
	@PracticeID INT = 0 
	,@PracticeLocationID INT
	
--SELECT * FROM dbo.PracticeLocation AS pl  --6;  24 18

AS 
BEGIN


--DECLARE @PracticeID INT
--DECLARE @PracticeLocationID INT

--SET @PracticeID = 1 ;
--SET @PracticeLocationID = 3 ;

DECLARE @WorkingTable TABLE
    (
      PracticeCatalogProductID INT,
      ProductInventoryID INT,
      MCName				VARCHAR(100),
      Code					VARCHAR(50),
      UPCCode				VARCHAR(50),
      LeftRightSide			VARCHAR(10),
	  Size					VARCHAR(50),
      SupplierName			VARCHAR(110),
      
      IsThirdPartySupplier	BIT,
      SupplierSequence		INT,
      
      --QuantityNotFullyCheckin INT DEFAULT 0,
      --QuantityCheckIn INT DEFAULT 0,
      --QuantityBaseLine INT DEFAULT 0,
            
      QuantityOnHand		INT DEFAULT 0,
      QuantityOnOrder		INT DEFAULT 0,
      QuantityOnHandPlusOnOrder INT DEFAULT 0,
      
      ParLevel				INT DEFAULT 0,
      ReorderLevel			INT DEFAULT 0,
      CriticalLevel			INT DEFAULT 0,
      IsNotFlaggedForReorder BIT DEFAULT 0,
      
      SuggestedReorderLevel	INT DEFAULT 0,
      
      Priority INT DEFAULT 4,
	  StockingUnits int default 1
	
    )
	

/*Get items in PCProductIDs that are in inventory*/


		-- 1.  Insert the PracticeCatalogProductID for the location's products that are in inventory and active.
		
		INSERT  INTO @WorkingTable
        (
				PracticeCatalogProductID,
				ProductInventoryID,
				StockingUnits

        )
        SELECT  
				PI.PracticeCatalogProductID ,
				PI.ProductInventoryID,
				PCP.StockingUnits
  
        FROM    
                dbo.ProductInventory AS PI  WITH( NOLOCK )
                INNER JOIN dbo.PracticeCatalogProduct AS PCP  WITH( NOLOCK )
					ON PCP.PracticeCatalogProductID = PI.PracticeCatalogProductID
				
				--
				inner join PracticeCatalogSupplierBrand PCSB
					on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
				--
        WHERE   
                PI.PracticeLocationID = @PracticeLocationID
                AND PI.IsActive = 1
                AND PCP.IsActive = 1
				and PCSB.isactive=1
        GROUP BY 
				PI.ProductInventoryID,
                PI.PracticeCatalogProductID,
				PCP.StockingUNits



		--  2. GetS QuAntity on Hand, Quntity on Order, Inventory Counts, and Suggested Reorder Level        
         UPDATE @WorkingTable		
		 SET   
			  WT.QuantityOnHand = Levels.QuantityOnHand
			, WT.QuantityOnOrder = Levels.QuantityOnOrder
			, WT.QuantityOnHandPlusOnOrder = Levels.QuantityOnHand + (Levels.QuantityOnOrder * WT.StockingUnits)

			, WT.ParLevel = Levels.ParLevel
			, WT.ReorderLevel = Levels.ReorderLevel
			, WT.CriticalLevel = Levels.CriticalLevel	
			
			, WT.IsNotFlaggedForReorder = Levels.IsNotFlaggedForReorder 			--  2008.02.05 JB
			
			, WT.SuggestedReorderLevel = (Levels.SuggestedReorderLevel	/ WT.StockingUnits)			
			
		 FROM @WorkingTable AS WT 
		 INNER JOIN dbo.udf_GetSuggestedReorder_Given_PracticeLocationID( @PracticeLocationID )	AS Levels
				ON WT.PracticeCatalogProductID = Levels.PracticeCatalogProductID


		--  Set Priority to On Order (3) if the product is on order.
		UPDATE  @WorkingTable
		SET     Priority = 3
		WHERE   QuantityOnOrder > 0
--		 	AND  IsNotFlaggedForReorder = 0		--  2008.02.05 JB
--			AND ParLevel > 0	--  2008.01.28  JB  Test No Flagging for home page  -- Can Remove This after release on 2008.02.06!!!
				
				
		--  Set Priorty to ReOrder (2) if the product's QuantityOnHandPlusOnOrder is below it's Reorder Level.
		UPDATE  @WorkingTable
		SET     Priority = 2
		WHERE   QuantityOnHandPlusOnOrder <= ReorderLevel
		  	AND   IsNotFlaggedForReorder = 0		--  2008.02.05 JB
			AND ParLevel > 0	--  2008.01.28  JB  Test No Flagging for home page  -- Can Remove This after release on 2008.02.06!!!
				
				
		--  Set Priorty to Critical (1) if the product's QuantityOnHandPlusOnOrder is below it's Critical Level.
		UPDATE  @WorkingTable
		SET     Priority = 1
		WHERE   QuantityOnHandPlusOnOrder <= CriticalLevel							
			AND   IsNotFlaggedForReorder = 0		--  2008.02.05 JB
			AND  ParLevel > 0 	--  2008.01.28  JB  Test No Flagging for home page  -- Can Remove This after release on 2008.02.06!!!
				
				
--------------------------------------------------------------------------------------------------------------------------
		-- Update ProductName, Product Code, Side, Size, and Supplier Name for Master Catalog Products.

		UPDATE  @WorkingTable
		SET     WT.MCName = Positive.MCName,
				WT.Code = Positive.Code,
				WT.LeftRightSide = Positive.LeftRightSide,
				WT.Size  = Positive.Size,
				WT.SupplierName = Positive.SupplierName,
				WT.IsThirdPartySupplier = Positive.IsThirdPartySupplier,
				WT.SupplierSequence = Positive.SupplierSequence,
				WT.UPCCode = Positive.UPCCode
				  -- Sets this so this will be ordered third in the grid
		FROM    @WorkingTable AS WT
        INNER JOIN ( SELECT PCP.PracticeCatalogProductid AS PracticeCatalogProductid,
                            MCP.Name			AS MCNAME,
                            MCP.Code			AS Code,
                            UPCCode = (Select Top 1 upc.Code from UPCCode upc where upc.MasterCatalogProductID = MCP.MasterCatalogProductID),
                            MCP.LeftRightSide	AS LeftRightSide,
                            MCP.Size			AS Size,
                            MCS.SupplierName	AS SupplierName,
                            0 AS IsThirdPartySupplier,
							ISNULL(PCSB.[Sequence], 999) AS SupplierSequence
                            
                     FROM   dbo.PracticeCatalogProduct AS PCP
                            
                            INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
								ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
                            
                            INNER JOIN dbo.MasterCatalogProduct AS MCP 
								ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
                            
                            INNER JOIN dbo.MasterCatalogSubCategory AS MCSC 
								ON MCP.MasterCatalogSubCategoryID = MCSC.MasterCatalogSubCategoryID
                            
                            INNER JOIN dbo.MasterCatalogCategory AS MCC 
								ON MCSC.MasterCatalogCategoryID = MCC.MasterCatalogCategoryID
                            
                            INNER JOIN dbo.MasterCatalogSupplier AS MCS 
								ON MCC.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
                     
                     WHERE  
								PCP.IsActive = 1
                            AND PCP.IsThirdPartyProduct = 0  -- Master Catalog Product
                            AND MCP.IsActive = 1
                            AND MCC.IsActive = 1
                            AND MCSC.IsActive = 1
                            AND MCS.IsActive = 1

							And PCSB.Isactive=1
                   
                   ) AS Positive 
						ON WT.PracticeCatalogProductid = Positive.PracticeCatalogProductid
			

		-- Update ProductName, Product Code, and Supplier Name for Third Party Products.

		UPDATE  @WorkingTable
		SET     WT.MCName = Positive.MCName,
				WT.Code = Positive.Code,
				WT.UPCCode = Positive.UPCCode,
				WT.LeftRightSide = Positive.LeftRightSide,
				WT.Size  = Positive.Size,
				WT.SupplierName = Positive.SupplierName,
				WT.IsThirdPartySupplier = Positive.IsThirdPartySupplier,
				WT.SupplierSequence = Positive.SupplierSequence
				  -- Sets this so this will be ordered third in the grid
		FROM    @WorkingTable AS WT
        INNER JOIN ( SELECT PCP.PracticeCatalogProductid AS PracticeCatalogProductid,
                            TPP.Name AS MCNAME,
                            TPP.Code AS Code,
                            TPP.UPCCode AS UPCCode,
                            TPP.LeftRightSide	AS LeftRightSide,
                            TPP.Size			AS Size,
                            CASE WHEN (PCSB.IsThirdPartySupplier = 1 )AND (PCSB.SupplierShortName <> PCSB.BrandShortName) 		
							THEN PCSB.SupplierShortName + ' - ' + PCSB.BrandShortName 
							ELSE PCSB.SupplierShortName END AS SupplierName,
							1 AS IsThirdPartySupplier,
							ISNULL(PCSB.[Sequence], 999) AS SupplierSequence

				
                            --PCSB.SupplierName AS SupplierName
                     FROM   
							dbo.PracticeCatalogProduct AS PCP
                            
                            INNER JOIN dbo.ThirdPartyProduct AS TPP 
								ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
                            
                            INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
								ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
                     
                     WHERE  
								PCP.IsActive = 1
                            AND TPP.IsActive = 1
And PCSB.Isactive=1
                            AND PCP.IsThirdPartyProduct = 1  -- Third Party Product
                   
                   ) AS Positive 
						ON WT.PracticeCatalogProductid = Positive.PracticeCatalogProductid
			
---------------------------------------------------------------------------------------------------------------			
			

SELECT
    [@WorkingTable].PracticeCatalogProductID
  , [@WorkingTable].ProductInventoryID
  , [@WorkingTable].MCName
  , [@WorkingTable].Code
  , [@WorkingTable].UPCCode
  , [@WorkingTable].LeftRightSide
  , [@WorkingTable].Size
  , [@WorkingTable].SupplierName
  , [@WorkingTable].IsThirdPartySupplier
  , [@WorkingTable].SupplierSequence
  , [@WorkingTable].QuantityOnHand
  , [@WorkingTable].QuantityOnOrder
  , [@WorkingTable].QuantityOnHandPlusOnOrder
  , [@WorkingTable].ParLevel
  , [@WorkingTable].ReorderLevel
  , [@WorkingTable].CriticalLevel
  , [@WorkingTable].SuggestedReorderLevel
  , [@WorkingTable].Priority
  , [@WorkingTable].StockingUnits
FROM
    @WorkingTable
ORDER BY
    Priority
  , IsNotFlaggedForReorder
  , IsThirdPartySupplier
  , SupplierSequence
  , SupplierName
  , MCName
  , Code
  , LeftRightSide
  , Size

        
END


