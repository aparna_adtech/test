create table outbound_file_item_queue (
	outbound_file_item_queue_id uniqueidentifier not null default newid(),
	practice_id int not null,
	filestore_path nvarchar(255) not null,
	filename nvarchar(255) null,
	creation_date datetime not null default getdate(),
	send_success_date datetime null,
	constraint pk__outbound_file_item_queue primary key (outbound_file_item_queue_id)
)