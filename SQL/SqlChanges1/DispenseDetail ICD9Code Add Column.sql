use [BregVision]
go

if not exists (select * from dbo.syscolumns where id = object_id(N'DispenseDetail') and name=(N'ICD9Code'))
alter table [DispenseDetail] add ICD9Code VarChar(20) NULL
GO

if not exists (select * from dbo.syscolumns where id = object_id(N'DispenseDetail') and name=(N'DispenseQueueID'))
alter table [DispenseDetail] add DispenseQueueID int NULL
GO
--new
if not exists (select * from dbo.syscolumns where id = object_id(N'DispenseDetail') and name=(N'LRModifier'))
alter table [DispenseDetail] add LRModifier VarChar(2) NULL
GO

if not exists (select * from dbo.syscolumns where id = object_id(N'DispenseDetail') and name=(N'IsMedicare'))
alter table [DispenseDetail] add IsMedicare [bit] DEFAULT 0 NOT NULL
GO

if not exists (select * from dbo.syscolumns where id = object_id(N'DispenseDetail') and name=(N'ABNForm'))
alter table [DispenseDetail] add ABNForm [bit] DEFAULT 0 NOT NULL
GO