USE [BregVision]
GO

/****** Object:  Table [dbo].[ICD9]    Script Date: 11/10/2009 22:57:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ICD9](
	[ICD9ID] [int] IDENTITY(1,1) NOT NULL,
	[HCPCs] [varchar](8) NOT NULL,
	[ICD9Code] [varchar](20) NOT NULL,
	[ICD9Description] [varchar](250) NULL,
	[PracticeID] [int] NULL,
	[CreatedUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedUserID] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_ICD9] PRIMARY KEY CLUSTERED 
(
	[ICD9ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ICD9] ADD  CONSTRAINT [DF__ICD9__CreatedUse__74F938D6]  DEFAULT ((1)) FOR [CreatedUserID]
GO

ALTER TABLE [dbo].[ICD9] ADD  CONSTRAINT [DF__ICD9__CreatedDat__75ED5D0F]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[ICD9] ADD  CONSTRAINT [DF__ICD9__IsActive__76E18148]  DEFAULT ((1)) FOR [IsActive]
GO


