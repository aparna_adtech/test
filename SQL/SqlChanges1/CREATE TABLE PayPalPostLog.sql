USE [BregVision]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__PayPalPos__Creat__7A280247]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PayPalPostLog] DROP CONSTRAINT [DF__PayPalPos__Creat__7A280247]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__PayPalPos__Creat__7B1C2680]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PayPalPostLog] DROP CONSTRAINT [DF__PayPalPos__Creat__7B1C2680]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__PayPalPos__IsAct__7C104AB9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PayPalPostLog] DROP CONSTRAINT [DF__PayPalPos__IsAct__7C104AB9]
END

GO

USE [BregVision]
GO

/****** Object:  Table [dbo].[PayPalPostLog]    Script Date: 04/27/2010 13:48:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PayPalPostLog]') AND type in (N'U'))
DROP TABLE [dbo].[PayPalPostLog]
GO

USE [BregVision]
GO

/****** Object:  Table [dbo].[PayPalPostLog]    Script Date: 04/27/2010 13:48:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PayPalPostLog](
	[PayPalPostLogID] [int] IDENTITY(1,1) NOT NULL,
	[Request] [varchar](max) NOT NULL,
	[Item_Number] [varchar](10) NULL,
	[Transaction_Type] [varchar](30) NULL,
	[CreatedUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedUserID] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
	[timestamp] [timestamp] NOT NULL,
 CONSTRAINT [PK_PayPalPostLog] PRIMARY KEY CLUSTERED 
(
	[PayPalPostLogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PayPalPostLog] ADD  DEFAULT ((1)) FOR [CreatedUserID]
GO

ALTER TABLE [dbo].[PayPalPostLog] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[PayPalPostLog] ADD  DEFAULT ((1)) FOR [IsActive]
GO


