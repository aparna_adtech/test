USE [BregVision]
GO
/****** Object:  Table [dbo].[HelpEntityTypes]    Script Date: 03/10/2010 16:35:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HelpEntityTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_HelpEntityTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HelpMedia]    Script Date: 03/10/2010 16:35:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HelpMedia](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HelpEntityTypeId] [int] NOT NULL,
	[Title] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[Url] [nvarchar](200) NULL,
 CONSTRAINT [PK_HelpMedia] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HelpGroups]    Script Date: 03/10/2010 16:35:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HelpGroups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HelpEntityTypeId] [int] NOT NULL,
	[Title] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_HelpGroups] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HelpEntityTree]    Script Date: 03/10/2010 16:35:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HelpEntityTree](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NULL,
	[EntityId] [int] NOT NULL,
	[EntityTypeId] [int] NOT NULL,
	[SortOrder] [int] NOT NULL,
 CONSTRAINT [PK_HelpEntityTree] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HelpArticles]    Script Date: 03/10/2010 16:35:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HelpArticles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HelpEntityTypeId] [int] NOT NULL,
	[Title] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_HelpArticles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_HelpArticles_HelpEntityTypeId]    Script Date: 03/10/2010 16:35:47 ******/
ALTER TABLE [dbo].[HelpArticles] ADD  CONSTRAINT [DF_HelpArticles_HelpEntityTypeId]  DEFAULT ((2)) FOR [HelpEntityTypeId]
GO
/****** Object:  Default [DF_HelpEntityTree_SortOrder]    Script Date: 03/10/2010 16:35:47 ******/
ALTER TABLE [dbo].[HelpEntityTree] ADD  CONSTRAINT [DF_HelpEntityTree_SortOrder]  DEFAULT ((0)) FOR [SortOrder]
GO
/****** Object:  Default [DF_HelpGroups_HelpEntityTypeId]    Script Date: 03/10/2010 16:35:47 ******/
ALTER TABLE [dbo].[HelpGroups] ADD  CONSTRAINT [DF_HelpGroups_HelpEntityTypeId]  DEFAULT ((1)) FOR [HelpEntityTypeId]
GO
/****** Object:  Default [DF_HelpMedia_HelpEntityTypeId]    Script Date: 03/10/2010 16:35:47 ******/
ALTER TABLE [dbo].[HelpMedia] ADD  CONSTRAINT [DF_HelpMedia_HelpEntityTypeId]  DEFAULT ((4)) FOR [HelpEntityTypeId]
GO
/****** Object:  ForeignKey [FK_HelpArticles_HelpEntityTypes]    Script Date: 03/10/2010 16:35:47 ******/
ALTER TABLE [dbo].[HelpArticles]  WITH CHECK ADD  CONSTRAINT [FK_HelpArticles_HelpEntityTypes] FOREIGN KEY([HelpEntityTypeId])
REFERENCES [dbo].[HelpEntityTypes] ([Id])
GO
ALTER TABLE [dbo].[HelpArticles] CHECK CONSTRAINT [FK_HelpArticles_HelpEntityTypes]
GO
/****** Object:  ForeignKey [FK_HelpEntityTree_HelpEntityTree]    Script Date: 03/10/2010 16:35:47 ******/
ALTER TABLE [dbo].[HelpEntityTree]  WITH CHECK ADD  CONSTRAINT [FK_HelpEntityTree_HelpEntityTree] FOREIGN KEY([ParentId])
REFERENCES [dbo].[HelpEntityTree] ([Id])
GO
ALTER TABLE [dbo].[HelpEntityTree] CHECK CONSTRAINT [FK_HelpEntityTree_HelpEntityTree]
GO
/****** Object:  ForeignKey [FK_HelpEntityTree_HelpEntityTypes]    Script Date: 03/10/2010 16:35:47 ******/
ALTER TABLE [dbo].[HelpEntityTree]  WITH CHECK ADD  CONSTRAINT [FK_HelpEntityTree_HelpEntityTypes] FOREIGN KEY([EntityTypeId])
REFERENCES [dbo].[HelpEntityTypes] ([Id])
GO
ALTER TABLE [dbo].[HelpEntityTree] CHECK CONSTRAINT [FK_HelpEntityTree_HelpEntityTypes]
GO
/****** Object:  ForeignKey [FK_HelpGroups_HelpEntityTypes]    Script Date: 03/10/2010 16:35:47 ******/
ALTER TABLE [dbo].[HelpGroups]  WITH CHECK ADD  CONSTRAINT [FK_HelpGroups_HelpEntityTypes] FOREIGN KEY([HelpEntityTypeId])
REFERENCES [dbo].[HelpEntityTypes] ([Id])
GO
ALTER TABLE [dbo].[HelpGroups] CHECK CONSTRAINT [FK_HelpGroups_HelpEntityTypes]
GO
/****** Object:  ForeignKey [FK_HelpMedia_HelpEntityTypes]    Script Date: 03/10/2010 16:35:47 ******/
ALTER TABLE [dbo].[HelpMedia]  WITH CHECK ADD  CONSTRAINT [FK_HelpMedia_HelpEntityTypes] FOREIGN KEY([HelpEntityTypeId])
REFERENCES [dbo].[HelpEntityTypes] ([Id])
GO
ALTER TABLE [dbo].[HelpMedia] CHECK CONSTRAINT [FK_HelpMedia_HelpEntityTypes]
GO
