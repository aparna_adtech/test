USE [BregVisionEMR]
GO

/****** Object:  Table [dbo].[outbound_hl7_diagnosis]    Script Date: 5/1/2014 5:57:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[outbound_hl7_diagnosis](
	[outbound_hl7_diagnosis_id] [uniqueidentifier] NOT NULL,
	[outbound_hl7_item_queue_id] [uniqueidentifier] NOT NULL,
	[diagnosis_code] [nvarchar](50) NOT NULL,
	[coding_method] [nvarchar](50) NOT NULL,
	[creation_date] [datetime] NOT NULL,
 CONSTRAINT [PK_outbound_hl7_diagnosis] PRIMARY KEY CLUSTERED 
(
	[outbound_hl7_diagnosis_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[outbound_hl7_diagnosis] ADD  CONSTRAINT [DF_outbound_hl7_diagnosis_coding_method]  DEFAULT ('HCPCS') FOR [coding_method]
GO

ALTER TABLE [dbo].[outbound_hl7_diagnosis] ADD  CONSTRAINT [DF_outbound_hl7_diagnosis_creation_date]  DEFAULT (getdate()) FOR [creation_date]
GO

ALTER TABLE [dbo].[outbound_hl7_diagnosis]  WITH CHECK ADD  CONSTRAINT [FK_outbound_hl7_diagnosis_outbound_hl7_item_queue] FOREIGN KEY([outbound_hl7_item_queue_id])
REFERENCES [dbo].[outbound_hl7_item_queue] ([outbound_hl7_item_queue_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[outbound_hl7_diagnosis] CHECK CONSTRAINT [FK_outbound_hl7_diagnosis_outbound_hl7_item_queue]
GO


