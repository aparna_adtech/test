USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_PracticeLocation_Delete]    Script Date: 08/18/2010 16:45:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocation_Delete
   
   Description:  "Deletes" a record from table dbo.PracticeLocation
   					This actual flags the IsActive column to false.
					
   AUTHOR:       John Bongiorni 7/23/2007 11:05:42 AM
   
   Modifications:  20071017 John Bongiorni  do not allow delete of primary location.
			 Cannot delete the practice location if it has products 
				in the practice location's inventory (Active or InActive)
   ------------------------------------------------------------ */ 
   
ALTER PROCEDURE [dbo].[usp_PracticeLocation_Delete]
    (
      @PracticeLocationID INT
    )
AS 
    BEGIN
        DECLARE @Err INT
		DECLARE @PracticeLocationInventoryProductCount INT

		Set @PracticeLocationInventoryProductCount = 0
		--  Check if the PracticeLocation has products in inventory
		--		active or inactive.

		--  PLInventoryCount

		/*
		SELECT @PracticeLocationInventoryProductCount = COUNT(1)
		FROM dbo.ProductInventory AS PI
		INNER JOIN PracticeLocation AS PL
			ON PI.PracticeLocationID = PL.PracticeLocationID
			AND PL.PracticeLocationID = @PracticeLocationID
		*/
		IF (@PracticeLocationInventoryProductCount = 0)
		BEGIN
			
			UPDATE
				dbo.PracticeLocation
	            
			SET IsActive = 0
	        
			WHERE
				PracticeLocationID = @PracticeLocationID
				AND IsPrimaryLocation = 0
	
		END
        SET @Err = @@ERROR

        RETURN @Err
    End


