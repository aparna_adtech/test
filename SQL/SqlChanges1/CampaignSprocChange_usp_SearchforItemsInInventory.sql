USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_SearchforItemsInInventory]    Script Date: 05/25/2010 11:24:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ==============================================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
--Exec usp_SearchforItemsInInventory 'Slingshot Neutral', '', 3

--===============================================================
--Updated by			Updated Date		Description
--Yuiko Sneen-Itazawa	02/11/2010			Added 'Vectra Air'
-- ==============================================================

ALTER PROCEDURE [dbo].[usp_SearchforItemsInInventory]
	-- Add the parameters for the stored procedure here
	@SearchCriteria varchar(50),
	@SearchText varchar(50),
	@PracticeLocationID int


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--All, Code, Brand

IF @SearchCriteria <> 'CampaignId'
BEGIN
	set @SearchText = @SearchText + '%'
END

if @SearchCriteria = 'All'
	begin
		EXEC	[dbo].usp_SearchForItemsInInventory_ALL_With_ThirdPartyProducts
		@PracticeLocationID = @PracticeLocationID,
		@ISActive = 1
--   select PI.ProductInventoryID, 
--		PCSB.SupplierName,
--		PCSB.BrandName,
--		MCP.Name as ProductName,
--		MCP.Code,
--		MCP.LeftRightSide as Side,
--		MCP.Size,
--		PI.PracticeCatalogProductID
--		 from productInventory PI 
--		inner join practicecatalogproduct PCP on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
--		inner join practicecatalogsupplierbrand PCSB on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID
--		inner join MasterCatalogProduct MCP on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
--		where PI.isactive =1 
--		and PCP.isactive = 1
--		and PCSB.isactive = 1
--		and MCP.isactive = 1
--		and PI.PracticeLocationID = @PracticeLocationID
--		group by PCSB.BrandName,PCSB.SupplierName,MCP.Name,Code, 
--		PI.ProductInventoryID,PI.PracticeCatalogProductID,
--		MCP.LeftRightSide,
--		MCP.Size
	end 
else if @SearchCriteria = 'Code'
	begin
	EXEC	[dbo].usp_SearchForItemsInInventory_ByProductCode_With_ThirdPartyProducts
	@PracticeLocationID = @PracticeLocationID,
	@SearchText = @SearchText
 

--select PI.ProductInventoryID, 
--		PCSB.SupplierName,
--		PCSB.BrandName,
--		MCP.Name as ProductName,
--		MCP.Code,
--		MCP.LeftRightSide as Side,
--		MCP.Size,
--		PI.PracticeCatalogProductID
--		 from productInventory PI 
--		inner join practicecatalogproduct PCP on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
--		inner join practicecatalogsupplierbrand PCSB on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID
--		inner join MasterCatalogProduct MCP on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
--		where PI.isactive =1 
--		and PCP.isactive = 1
--		and PCSB.isactive = 1
--		and MCP.isactive = 1
--		and MCP.code like @SearchText
--		and PI.PracticeLocationID = @PracticeLocationID
--		group by PCSB.BrandName,PCSB.SupplierName,MCP.Name,Code, PI.ProductInventoryID,PI.PracticeCatalogProductID,
--		MCP.LeftRightSide,
--		MCP.Size
	end 
else if @SearchCriteria = 'Brand'
	begin
	EXEC	[dbo].usp_SearchForItemsInInventory_ByProductBrand_With_ThirdPartyProducts
	@PracticeLocationID = @PracticeLocationID,
	@SearchText = @SearchText	  
-- select PI.ProductInventoryID, 
--		PCSB.SupplierName,
--		PCSB.BrandName,
--		MCP.Name as ProductName,
--		MCP.Code,
--		MCP.LeftRightSide as Side,
--		MCP.Size,
--		PI.PracticeCatalogProductID
--		 from productInventory PI 
--		inner join practicecatalogproduct PCP on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
--		inner join practicecatalogsupplierbrand PCSB on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID
--		inner join MasterCatalogProduct MCP on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
--		where PI.isactive =1 
--		and PCP.isactive = 1
--		and PCSB.isactive = 1
--		and MCP.isactive = 1
--		and PCSB.BrandName like @SearchText
--		and PI.PracticeLocationID = @PracticeLocationID
--		group by PCSB.BrandName,PCSB.SupplierName,MCP.Name,Code, PI.ProductInventoryID,PI.PracticeCatalogProductID,
--		MCP.LeftRightSide,
--		MCP.Size
	end 
else if @SearchCriteria = 'Name'
	begin
	set @SearchText = '%' + @SearchText    
	EXEC	[dbo].usp_SearchForItemsInInventory_ByProductName_With_ThirdPartyProducts
	@PracticeLocationID = @PracticeLocationID,
	@SearchText = @SearchText

--	select PI.ProductInventoryID, 
--		PCSB.SupplierName,
--		PCSB.BrandName,
--		MCP.Name as ProductName,
--		MCP.Code,
--		MCP.LeftRightSide as Side,
--		MCP.Size,
--		PI.PracticeCatalogProductID
--		 from productInventory PI 
--		inner join practicecatalogproduct PCP on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
--		inner join practicecatalogsupplierbrand PCSB on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID
--		inner join MasterCatalogProduct MCP on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
--		where PI.isactive =1 
--		and PCP.isactive = 1
--		and PCSB.isactive = 1
--		and MCP.isactive = 1
--		and (MCP.Name like @SearchText or MCP.ShortName like @SearchText)
--		and PI.PracticeLocationID = @PracticeLocationID
--		group by PCSB.BrandName,PCSB.SupplierName,MCP.Name,Code, PI.ProductInventoryID,PI.PracticeCatalogProductID,
--		MCP.LeftRightSide,
--		MCP.Size
	end 
else if @SearchCriteria = 'Category'
	begin
	   select PI.ProductInventoryID, 
		PCSB.SupplierName,
		PCSB.BrandName,
		MCP.Name as ProductName,
		ISNULL(MCP.LeftRightSide, '') as Side,
		ISNULL(MCP.Size, '') as Size,		
		MCP.Code,
		PI.PracticeCatalogProductID,
		
		ISNULL(PI.QuantityOnHandPerSystem, 0) AS QOH
		
		 from productInventory PI 
		inner join practicecatalogproduct PCP on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
		inner join practicecatalogsupplierbrand PCSB on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID
		inner join MasterCatalogProduct MCP on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
		inner join MasterCatalogSubCategory MCSC on MCP.MasterCatalogSubCategoryID = MCSC.MasterCatalogSubCategoryID
		inner join MasterCatalogCategory MCC on MCSC.MasterCatalogCategoryID = MCC.MasterCatalogCategoryID
		where PI.isactive =1 
		and PCP.isactive = 1
		and PCSB.isactive = 1
		and MCP.isactive = 1
		and MCSC.isactive = 1
		and MCC.isactive = 1
		and MCC.Name like @SearchText
		and PI.PracticeLocationID = @PracticeLocationID
		group by PCSB.BrandName,PCSB.SupplierName,MCP.Name,Code, PI.ProductInventoryID,PI.PracticeCatalogProductID,
		MCP.LeftRightSide,
		MCP.Size,
		ISNULL(PI.QuantityOnHandPerSystem, 0)
	end 
else if @SearchCriteria = 'HCPCs'

	begin
		set @SearchText = '%' + @SearchText    
		EXEC	[dbo].usp_SearchForItemsInInventory_ByHCPCs_With_ThirdPartyProducts
		@PracticeLocationID = @PracticeLocationID,
		@SearchText = @SearchText
	end 

else if @SearchCriteria in ('BOA', 'SlingShot', 'SlingShot Neutral')

	begin
select 
		MCP.Name as ProductName,
		ISNULL(MCP.LeftRightSide, '') as Side,
		ISNULL(MCP.Size, '') as Size,		
		MCP.Code,
		MCS.SupplierName as SupplierName,
		MCS.SupplierShortName as BrandName,
		MCP.MasterCatalogProductID,
		ISNULL(PI.ProductInventoryID, 0) AS ProductInventoryID,
		ISNULL(PI.PracticeCatalogProductID, 0) AS PracticeCatalogProductID,
		ISNULL(PI.QuantityOnHandPerSystem, 0) AS QOH
		from 
			dbo.MasterCatalogProduct MCP 
			left outer join MasterCatalogSubCategory MCSC on MCP.MasterCatalogSubCategoryID = MCSC.MasterCatalogSubCategoryID
			Left outer join MasterCatalogCategory MCC on MCSC.MasterCatalogCategoryID = MCC.MasterCatalogCategoryID
			left outer join MasterCatalogSupplier MCS on MCC.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
			Left Outer Join PracticeCatalogProduct PCP on MCP.MasterCatalogProductID=PCP.MasterCatalogProductID and PCP.PracticeID=(Select PL.PracticeID from PracticeLocation PL where PL.PracticeLocationID = @PracticeLocationID)
			Left Outer Join ProductInventory PI on PCP.PracticeCatalogProductID = pi.PracticeCatalogProductID and PI.PracticeLocationID = @PracticeLocationID
			Left Outer join practicecatalogsupplierbrand PCSB on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID
		where 
		MCP.isactive = 1
		and MCSC.isactive = 1
		and MCC.isactive = 1
		and MCS.IsActive = 1
		AND 
			(
				(
					@SearchCriteria = 'BOA'
					AND
			 		MCP.[Code] in
						(
							'60002','60003','60004','60005','60006','60012','60013','60014','60015','60016','60017','60022','60023','60024','60025','60026',
							'60027','60032','60033','60034','60035','60036','60037','60042','60043','60044','60045','60046','60047','60051','60052','60053',
							'60054','60055','60056','60057','60062','60063','60064','60065','60072','60073','60074','60082','60083','60084','60085','60093',
							'60094','60095'
						)
				)
				OR
				(
					@SearchCriteria = 'SlingShot'
					AND
					MCP.[Code] in
						(
							'08502', '08503', '08504', '08505'
						)
				)
				OR
				(
					@SearchCriteria = 'SlingShot Neutral'
					AND
					MCP.[Code] in
						(
							'01820', '01830', '01840', '01855'
						)
				)
					
			)
	end
else if @SearchCriteria = 'CampaignId'
BEGIN
	DECLARE @CampaignId int
	set @CampaignId = CAST(@SearchText as int)
		
	select 
		MCP.Name as ProductName,
		ISNULL(MCP.LeftRightSide, '') as Side,
		ISNULL(MCP.Size, '') as Size,		
		MCP.Code,
		MCS.SupplierName as SupplierName,
		MCS.SupplierShortName as BrandName,
		MCP.MasterCatalogProductID,
		ISNULL(PI.ProductInventoryID, 0) AS ProductInventoryID,
		ISNULL(PI.PracticeCatalogProductID, 0) AS PracticeCatalogProductID,
		ISNULL(PI.QuantityOnHandPerSystem, 0) AS QOH
		from 
			dbo.MasterCatalogProduct MCP 
			left outer join MasterCatalogSubCategory MCSC on MCP.MasterCatalogSubCategoryID = MCSC.MasterCatalogSubCategoryID
			Left outer join MasterCatalogCategory MCC on MCSC.MasterCatalogCategoryID = MCC.MasterCatalogCategoryID
			left outer join MasterCatalogSupplier MCS on MCC.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
			Left Outer Join PracticeCatalogProduct PCP on MCP.MasterCatalogProductID=PCP.MasterCatalogProductID and PCP.PracticeID=(Select PL.PracticeID from PracticeLocation PL where PL.PracticeLocationID = @PracticeLocationID)
			Left Outer Join ProductInventory PI on PCP.PracticeCatalogProductID = pi.PracticeCatalogProductID and PI.PracticeLocationID = @PracticeLocationID
			Left Outer join practicecatalogsupplierbrand PCSB on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID
		where 
		MCP.isactive = 1
		and MCSC.isactive = 1
		and MCC.isactive = 1
		and MCS.IsActive = 1
		and	MCP.MasterCatalogProductID in
			(
				Select MasterCatalogProductID 
					from CampaignTargetProduct 
					where CampaignId=@CampaignId
			)
			
END
else if @SearchCriteria in ('X2K', 'PTO', 'PTO-HP', 'Vectra Air')

	begin
select 
		MCP.Name as ProductName,
		ISNULL(MCP.LeftRightSide, '') as Side,
		ISNULL(MCP.Size, '') as Size,		
		MCP.Code,
		MCS.SupplierName as SupplierName,
		MCS.SupplierShortName as BrandName,
		MCP.MasterCatalogProductID,
		ISNULL(PI.ProductInventoryID, 0) AS ProductInventoryID,
		ISNULL(PI.PracticeCatalogProductID, 0) AS PracticeCatalogProductID,
		ISNULL(PI.QuantityOnHandPerSystem, 0) AS QOH
		from 
			dbo.MasterCatalogProduct MCP 
			left outer join MasterCatalogSubCategory MCSC on MCP.MasterCatalogSubCategoryID = MCSC.MasterCatalogSubCategoryID
			Left outer join MasterCatalogCategory MCC on MCSC.MasterCatalogCategoryID = MCC.MasterCatalogCategoryID
			left outer join MasterCatalogSupplier MCS on MCC.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
			Left Outer Join PracticeCatalogProduct PCP on MCP.MasterCatalogProductID=PCP.MasterCatalogProductID and PCP.PracticeID=(Select PL.PracticeID from PracticeLocation PL where PL.PracticeLocationID = @PracticeLocationID)
			Left Outer Join ProductInventory PI on PCP.PracticeCatalogProductID = pi.PracticeCatalogProductID and PI.PracticeLocationID = @PracticeLocationID
			Left Outer join practicecatalogsupplierbrand PCSB on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID
		where 
		MCP.isactive = 1
		and MCSC.isactive = 1
		and MCC.isactive = 1
		and MCS.IsActive = 1
		AND 
			(
				(
					@SearchCriteria = 'X2K'
					AND
			 		MCP.[Code] in
						(
							'20025','20029','20033','20037','20056'
						
						)
				)
				OR
				(
					@SearchCriteria = 'PTO'
					AND
					MCP.[Code] in
						(
							'14191', '14192', '14193', '14194', '14195', '14196', '14181', '14182', '14183', '14184',
							'14185', '14186', '14231', '14232', '14233', '14234', '14235', '14236', '14221', '14222',
							'14223', '14224', '14225', '14226', '14171', '14172', '14173', '14174', '14175', '14176',
							'14161', '14162', '14163', '14164', '14165', '14166', '14211', '14212', '14213', '14214',
							'14215', '14216', '14201', '14202', '14203', '14204', '14205', '14206'
						)
				)
				OR
				(
					@SearchCriteria = 'PTO-HP'
					AND
					MCP.[Code] in
						(
							'14271', '14272', '14273', '14274', '14275', '14276', '14261', '14262', '14263', '14264',
							'14265', '14266', '14311', '14312', '14313', '14314', '14315', '14316', '14301', '14302',
							'14303', '14304', '14305', '14306', '14251', '14252', '14253', '14254', '14255', '14256',
							'14241', '14242', '14243', '14244', '14245', '14246', '14291', '14292', '14293', '14294',
							'14295', '14296', '14281', '14282', '14283', '14284', '14285', '14286'
						)
				)
				OR
				(
					@SearchCriteria = 'Vectra Air'
					AND
					MCP.[Code] in
						(
							'28441', '28442', '28443', '28444', '28445', '28451', '28452', '28453', '28454', '28455'
						)
				)
				

					
			)
	end
	
	else if @SearchCriteria in ('SilverPro')

	begin
select 
		MCP.Name as ProductName,
		ISNULL(MCP.LeftRightSide, '') as Side,
		ISNULL(MCP.Size, '') as Size,		
		MCP.Code,
		MCS.SupplierName as SupplierName,
		MCS.SupplierShortName as BrandName,
		MCP.MasterCatalogProductID,
		ISNULL(PI.ProductInventoryID, 0) AS ProductInventoryID,
		ISNULL(PI.PracticeCatalogProductID, 0) AS PracticeCatalogProductID,
		ISNULL(PI.QuantityOnHandPerSystem, 0) AS QOH
		from 
			dbo.MasterCatalogProduct MCP 
			left outer join MasterCatalogSubCategory MCSC on MCP.MasterCatalogSubCategoryID = MCSC.MasterCatalogSubCategoryID
			Left outer join MasterCatalogCategory MCC on MCSC.MasterCatalogCategoryID = MCC.MasterCatalogCategoryID
			left outer join MasterCatalogSupplier MCS on MCC.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
			Left Outer Join PracticeCatalogProduct PCP on MCP.MasterCatalogProductID=PCP.MasterCatalogProductID and PCP.PracticeID=(Select PL.PracticeID from PracticeLocation PL where PL.PracticeLocationID = @PracticeLocationID)
			Left Outer Join ProductInventory PI on PCP.PracticeCatalogProductID = pi.PracticeCatalogProductID and PI.PracticeLocationID = @PracticeLocationID
			Left Outer join practicecatalogsupplierbrand PCSB on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID
		where 
		MCP.isactive = 1
		and MCSC.isactive = 1
		and MCC.isactive = 1
		and MCS.IsActive = 1
		AND 
			(
				(
					@SearchCriteria = 'SilverPro'
					AND
					MCP.[Code] in
						(
							'20221', '20222', '20223', '20224', 
							
							'20121', '20122', '20123', '20124', '20125', '20126',
							'20141', '20142', '20143', '20144', '20145', '20146',
							'20151', '20152', '20153', '20154', '20155', '20156',
							'20171', '20172', '20173', '20174', '20175', '20176'

						)
				)
					
			)
	end

END






