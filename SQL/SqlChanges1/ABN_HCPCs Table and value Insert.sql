/****** Script for SelectTopNRows command from SSMS  ******/
USE [BregVision]
GO

/****** Object:  Table [dbo].[ABN_HCPCs]    Script Date: 10/12/2009 12:17:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ABN_HCPCs](
	[HCPCs] [varchar](20) NOT NULL,
	[ABN_Needed] [bit] NOT NULL,
 CONSTRAINT [PK_ABN_HCPCs] PRIMARY KEY CLUSTERED 
(
	[HCPCs] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ABN_HCPCs] ADD  CONSTRAINT [DF_ABN_HCPCs_ABN_Needed]  DEFAULT ((0)) FOR [ABN_Needed]
GO

--------------------------Add Values----------------------------------------------------
  
  Delete from [BregVision].[dbo].[ABN_HCPCs]
  Go
  
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3000', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3001', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3002', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3003', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3010', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3020', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3030', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3031', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3040', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3050', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3060', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3070', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3080', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3090', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3100', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3140', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3150', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3170', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3224', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3225', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3300', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3310', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3330', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3332', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3334', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3340', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3350', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3360', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3370', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3380', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3390', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3400', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3410', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3420', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3430', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3440', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3450', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3455', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3460', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3465', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3470', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3480', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3500', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3510', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3520', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3530', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3540', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3550', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3560', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3570', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3580', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3590', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3595', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3600', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3610', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3620', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3630', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3640', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3649', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L0210', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L1800', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L1815', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L1825', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L1901', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3651', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3652', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3700', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3701', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3909', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3911', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L3923', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L0450', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L0454', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L0621', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L0625', 1)
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed) values ('L0628', 1)
Go

/*
Insert into [BregVision].[dbo].[ABN_HCPCs](HCPCs, ABN_Needed)
  values ('', 1)
Go
 */ 
  
  SELECT TOP 200 [HCPCs]
      ,[ABN_Needed]
  FROM [BregVision].[dbo].[ABN_HCPCs]
  