USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetConsignmentRepsAll]    Script Date: 02/09/2012 10:47:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- exec usp_GetConsignmentRepsAll
ALTER PROCEDURE [dbo].[usp_GetConsignmentRepsAll]

AS
BEGIN


	SELECT 
		U.[UserId]
		,U.[UserName]
		,M.IsLockedOut
	FROM 
		aspnetdb.dbo.aspnet_Membership M
		Inner Join [aspnetdb].[dbo].[aspnet_Users] U on M.UserId = U.UserId
		left outer join [aspnetdb].[dbo].[aspnet_UsersInRoles] UR on U.UserId = UR.UserId 
		left outer join [aspnetdb].[dbo].[aspnet_Roles] R on UR.RoleId = R.RoleId 
	where R.RoleName = 'BregConsignment'
END
