/*

	Author:  John Bongiorni
	Created:  2007.12.18

	EXEC dbo.[usp_Report_ProductReconciliation] 6
				, '13, 15, 16, 17, 18, 19, 20, 22, 23, 24'
				, '34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 74, 75, 76, 81, 82, 83, 99, 101' 
				, '1/1/2007'
				, '12/31/2007'

	Modifications:
		
	Late Updated:  2007.12.18 14:05
*/


ALTER PROC [dbo].[usp_Report_ProductReconciliation]
		  @PracticeID			INT
		, @PracticeLocationID	VARCHAR(2000)
		, @SupplierBrandID		VARCHAR(2000)   --  ID String.
		, @StartDate			DATETIME
		, @EndDate				DATETIME
AS
BEGIN

--	DECLARE @PracticeID INT
--	SET @PracticeID = 6


	SELECT @EndDate = DATEADD(DAY, 1, @EndDate)

	DECLARE @PracticeLocationIDString VARCHAR(2000)
	DECLARE @SupplierBrandIDString VARCHAR(2000)


	SET @PracticeLocationIDString = @PracticeLocationID
	SET @SupplierBrandIDString = @SupplierBrandID


	SELECT 
			 SUB.PracticeName						AS Practice
			, SUB.PurchaseOrder						AS PO
			, SUB.CustomPurchaseOrderCode			AS CustomPO
			, SUB.InvoiceNumber
			, CASE SUB.InvoicePaid
				WHEN 1 then 'Yes'
				ELSE 'No'
			  END									AS InvoicePaid
			  
			, SUB.PracticeLocation					AS Location

			, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
				ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
				END									AS SupplierBrand

			, SUB.ProductShortName					AS Product
			, SUB.Side + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender
			, SUB.Code

			, SUB.QuantityOrdered								AS QuantityOrdered	
--			, SUB.ActualWholesaleCost							AS ActualWholesale
			, CAST(SUB.LineTotal  AS DECIMAL(15, 2))			AS QuantityOrderedCost	

			, SUB.QuantityCheckedIn		
			, CAST((SUB.QuantityCheckedIn * SUB.ActualWholesaleCost)  
											AS DECIMAL(15, 2))  AS QuantityCheckInCost

			, SUB.QuantityToCheckIn								AS QuantityOutstanding
			, CAST((SUB.QuantityToCheckIn * SUB.ActualWholesaleCost)  
											AS DECIMAL(15, 2)) AS QuantityOutstandingCost
			
			, SUB.OrderedDate									AS OrderedDate
			, SUB.Sequence	

	FROM

		(
		--  MC SUPPLIER PRODUCTS --

		SELECT 
			   P.PracticeID
			, P.PracticeName
			, SO.PurchaseOrder		
			, SO.CustomPurchaseOrderCode
			, SO.InvoiceNumber
			, SO.InvoicePaid
    		, PL.PracticeLocationID	
			, PL.NAME AS PracticeLocation

--			, CASE WHEN PCSB.SupplierShortName = PCSB.BrandShortName THEN PCSB.SupplierShortName
--				ELSE PCSB.SupplierShortName + ' (' + PCSB.BrandShortName + ')'
--				END					AS SupplierBrand

			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) AS Sequence

			, PCP.PracticeCatalogProductID
			, MCP.[ShortName] AS ProductShortName
			, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS Side
			, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender		
			, ISNULL(MCP.Code, '') AS Code

			, SOLI.ActualWholesaleCost
			, SOLI.QuantityOrdered
			, SOLI.LineTotal	

			, ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityCheckedIn
			, CASE WHEN ( SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) ) < 0 THEN 0
				ELSE  SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) 
				END									AS QuantityToCheckIn					--  FUDGED due to bad checkin data.
		--	, SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0)  AS QuantityToCheckIn  --Correct


			, CONVERT(VARCHAR(10), SOLI.CreatedDate, 101)  AS OrderedDate

		FROM SupplierOrderLineItem SOLI  WITH (NOLOCK)

		INNER JOIN SupplierOrder AS SO  WITH (NOLOCK)
			ON SOLI.SupplierOrderID = SO.SupplierOrderID

		INNER JOIN BregVisionOrder AS BVO  WITH (NOLOCK)
			ON SO.BregVisionOrderID = BVO.BregVisionOrderID

		INNER JOIN PracticeLocation AS PL  WITH (NOLOCK)
			ON BVO.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P  WITH (NOLOCK)
			ON P.PracticeID = PL.PracticeID

		INNER JOIN PracticeCatalogPRoduct PCP  WITH (NOLOCK)
			on SOLI.PracticeCatalogProductID = 	PCP.PracticeCatalogPRoductID


		INNER JOIN MasterCatalogProduct MCP  WITH (NOLOCK)
			on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID


		INNER JOIN PracticeCatalogSupplierBrand AS PCSB  WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

		LEFT JOIN (SELECT SupplierOrderLineItemID, SUM(Quantity) AS QuantityCheckedIn
					FROM dbo.ProductInventory_OrderCheckIn 
					WHERE IsActive = 1
					GROUP BY SupplierOrderLineItemID)AS SubPIOCI
						ON SOLI.SupplierOrderLineItemID = SubPIOCI.SupplierOrderLineItemID

		WHERE 
			P.PracticeID = @PracticeID
			
			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND BVO.IsActive = 1
			AND SO.IsActive = 1
			AND SOLI.IsActive = 1
			--AND PCP.isactive = 1
			--AND PCSB.isactive = 1
			--AND MCP.isactive = 1

			AND PL.PracticeLocationID IN 
				(SELECT ID 
				FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))

			AND PCSB.PracticeCatalogSupplierBrandID IN 			
				(SELECT ID 
				 FROM dbo.udf_ParseArrayToTable( @SupplierBrandIDString, ',') )	

			AND SOLI.CreatedDate >= @StartDate
			AND SOLI.CreatedDate < @EndDate --  Less than since @EndDate has one day

	UNION

	--  THIRD PARTY SUPPLIER PRODUCTS --
	--DECLARE @PracticeID INT
	--SET @PracticeID = 6
		SELECT 
				   P.PracticeID
				, P.PracticeName
				, SO.PurchaseOrder		
				, SO.CustomPurchaseOrderCode
				, SO.InvoiceNumber
				, SO.InvoicePaid
    			, PL.PracticeLocationID	
				, PL.NAME AS PracticeLocation

				, PCSB.SupplierShortName
				, PCSB.BrandShortName
				, ISNULL(PCSB.Sequence, 999) + 1000 AS Sequence

				, PCP.PracticeCatalogProductID
				, TPP.[ShortName] AS ProductShortName
				, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS Side
				, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )			AS Size
				, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender		
				, ISNULL(TPP.Code, '') AS Code
			 
				, SOLI.ActualWholesaleCost
				, SOLI.QuantityOrdered
				, SOLI.LineTotal	

				, ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityCheckedIn
				, CASE WHEN ( SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) ) < 0 THEN 0
					ELSE  SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) 
					END									AS QuantityToCheckIn
			--	, SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityToCheckIn

				, CONVERT(VARCHAR(10), SOLI.CreatedDate, 101)  AS OrderedDate


		FROM SupplierOrderLineItem SOLI  WITH (NOLOCK)

		INNER JOIN SupplierOrder AS SO  WITH (NOLOCK)
			ON SOLI.SupplierOrderID = SO.SupplierOrderID

		INNER JOIN BregVisionOrder AS BVO  WITH (NOLOCK)
			ON SO.BregVisionOrderID = BVO.BregVisionOrderID

		INNER JOIN PracticeLocation AS PL  WITH (NOLOCK)
			ON BVO.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P  WITH (NOLOCK)
			ON P.PracticeID = PL.PracticeID

		INNER JOIN PracticeCatalogPRoduct PCP  WITH (NOLOCK)
			on SOLI.PracticeCatalogProductID = 	PCP.PracticeCatalogPRoductID

		INNER JOIN ThirdPartyProduct TPP  WITH (NOLOCK)
			on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

		INNER JOIN PracticeCatalogSupplierBrand AS PCSB  WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

		LEFT JOIN (SELECT SupplierOrderLineItemID, SUM(Quantity) AS QuantityCheckedIn
					FROM dbo.ProductInventory_OrderCheckIn 
					WHERE IsActive = 1
					GROUP BY SupplierOrderLineItemID)AS SubPIOCI
						ON SOLI.SupplierOrderLineItemID = SubPIOCI.SupplierOrderLineItemID
		
		WHERE 
			P.PracticeID = @PracticeID

			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND BVO.IsActive = 1
			AND SO.IsActive = 1
			AND SOLI.IsActive = 1
			--AND PCP.isactive = 1
			--AND PCSB.isactive = 1
			--and TPP.isactive = 1

			AND PL.PracticeLocationID IN 
				(SELECT ID 
				FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))

			AND PCSB.PracticeCatalogSupplierBrandID IN 			
				(SELECT ID 
				 FROM dbo.udf_ParseArrayToTable( @SupplierBrandIDString, ',') )	

			AND SOLI.CreatedDate >= @StartDate
			AND SOLI.CreatedDate < @EndDate --  Less than since @EndDate has one day

		) AS SUB


	ORDER BY
			 SUB.PracticeName
			, SUB.PurchaseOrder
			, SUB.PracticeLocation
			, SUB.Sequence	

			, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
				ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
				END								--AS SupplierBrand


			, SUB.ProductShortName
			, SUB.Code							 
			, SUB.Side + ', ' + SUB.Size  + ', ' + SUB.Gender --AS SideSizeGender
			, SUB.QuantityOrdered

END