USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetLast10ItemsDispensed]    Script Date: 5/6/2015 3:09:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		<Author: Greg Ross>
-- Create date: <Create Date: 07/20/07>
-- Description:	<Description: Used to populate the last 10 items dispensed >
--  MODIFICATION:  20071015 JB Left JOin Physician and Contact remove is Active for these columns.
--				20071015 JB Add Third Party Products
--				20080218 JB Format Date Dispensed
--				20080219 JB Display DataDispensed, 
--					but order by full date and time of the CreateDate.
--				20080219 GR Changed sorting from DateDispensed to Created Date throughout proc
--				20080229 JB Add the Patient Code to the proc.
--				20080423 JB Added the RowNumber and changed to last twenty dispensed.
-- =============================================


CREATE PROCEDURE [dbo].[usp_GetLast10ItemsDispensed]   -- 1, 3   --6, 15  -- 1, 3
	-- Add the parameters for the stored procedure here
    @PracticeID INT = 0,
    @PracticeLocationID int
AS 
    BEGIN
SELECT 
	 --  TOP 10  
	     TOP 20
	SUB.DISPENSEID,
				
	SUB.PatientCode,  -- 2008.02.29 JB
				
	SUB.DispenseDetailID,
    SUB.SupplierName,
    SUB.name,
    SUB.Code,
	SUB.Side,
	SUB.Size,

    SUB.Quantity,

    SUB.FirstName + ' ' + SUB.LastName AS FirstName,
    
    SUB.PatientCode AS LastName,   -- 2008.02.29 JB  This makes no sense this is to match the front end with a change to the application.
    
    SUB.FirstName + ' ' + SUB.LastName AS PhysicianName,
	SUB.FirstName as PhysicianFirstName,
	Sub.LastName as PhysicianLastName,
       
    SUB.LineTotal,
    SUB.Total,			                
--    SUB.DateDispensed,

	CONVERT(CHAR(10), DateDispensed,110) AS DateDispensed,  --  20080218 JB  

	SUB.WholesaleCost,
	
	ROW_NUMBER() OVER (ORDER BY SUB.CreatedDate DESC, SUB.suppliername, SUB.name) AS 'RowNumber',
	SUB.DMEDeposit,
	isnull(sub.Payer,'N/A') as Payer,
	ltrim(rtrim(isnull(SUB.PatientFirstName,'') + ' ' + isnull(SUB.PatientLastName,''))) as PatientName,
	SUB.PatientFirstName,
	SUB.PatientLastName

FROM
   ( SELECT TOP 20
				D.DISPENSEID,
				
				D.PatientCode,  -- 2008.02.29 JB
				
				dd.DispenseDetailID,
                MCS.[SupplierName],
                MCP.name,
                MCP.[Code],
				--MCP.LeftRightSide as Side,
				DD.LRModifier as Side,
				MCP.Size,

                DD.[Quantity],
                
                ISNULL(C.[FirstName], '') AS FirstName,
                ISNULL(C.[LastName], '') AS LastName,
               
                DD.ActualChargeBilled AS Total,
                DD.[LineTotal] AS LineTotal,			                
                [DateDispensed],
				D.[CreatedDate],
				(PCP.WholesaleCost / PCP.StockingUnits) as WholesaleCost,
				DD.DMEDeposit,
				PracPayer.Name as Payer,
                ISNULL(D.[PatientFirstName], '')	AS PatientFirstName,
                ISNULL(D.[PatientLastName], '')		AS PatientLastName
                
        FROM    dispense D
        
                INNER JOIN DispenseDetail DD 
					ON D.[DispenseID] = DD.[DispenseID]
					
                LEFT JOIN [Physician] Phy 
					ON Dd.[PhysicianID] = Phy.[PhysicianID]
					AND Phy.IsActive = 1
					
                LEFT  JOIN [Contact] 
					C ON Phy.[ContactID] = c.[ContactID]
					AND C.IsActive = 1
					
                INNER JOIN [PracticeCatalogProduct] PCP ON 
					DD.[PracticeCatalogProductID] = PCP.[PracticeCatalogProductID]
					
--                INNER JOIN [Practice] P 
--					ON PCP.[PracticeID] = P.[PracticeID]

                INNER JOIN [PracticeLocation] PL 
					ON D.[PracticeLocationID] = PL.[PracticeLocationID]
					
                INNER JOIN [MasterCatalogProduct] MCP 
					ON PCP.[MasterCatalogProductID] = MCP.[MasterCatalogProductID]
					
                INNER JOIN [MasterCatalogSubCategory] MCSC 
					ON MCP.[MasterCatalogSubCategoryID] = MCSC.[MasterCatalogSubCategoryID]
					
                INNER JOIN [MasterCatalogCategory] MCC 
					ON MCSC.[MasterCatalogCategoryID] = MCC.[MasterCatalogCategoryID]
					
                INNER JOIN [MasterCatalogSupplier] MCS 
					ON MCc.[MasterCatalogSupplierID] = MCS.[MasterCatalogSupplierID]
				
				LEFT JOIN [PracticePayer] PracPayer on PracPayer.PracticePayerID = D.PracticePayerID

        WHERE   
				--  p.[PracticeID] = @PracticeID
                --  AND 
                pl.[PracticeLocationID] = @PracticeLocationID
                AND D.[IsActive] = 1
                AND DD.[IsActive] = 1
                --  AND PHY.[IsActive] = 1
                --  AND C.[IsActive] = 1
                AND PCP.[IsActive] = 1
                --AND P.[IsActive] = 1
                AND PL.[IsActive] = 1
                AND MCSC.[IsActive] = 1
                AND MCC.[IsActive] = 1
                AND MCS.[IsActive] = 1
        --ORDER BY [DateDispensed] DESC,
		  ORDER BY [CreatedDate] DESC,
                suppliername,
                [MCP].name 
                
UNION


SELECT TOP 20
				D.DISPENSEID,
				
				D.PatientCode,  -- 2008.02.29 JB
				
				dd.DispenseDetailID,
                PCSB.[SupplierName],
                TPP.name,
                TPP.[Code],
				--TPP.LeftRightSide as Side,
				DD.LRModifier as Side,
				TPP.Size,


                DD.[Quantity],
                
                ISNULL(C.[FirstName], '') AS FirstName,
                ISNULL(C.[LastName], '') AS LastName,
               
                DD.ActualChargeBilled AS Total,
                DD.[LineTotal] AS LineTotal,			                
                [DateDispensed],
				D.[CreatedDate],
				
				(PCP.WholesaleCost/PCP.StockingUnits) as  WholesaleCost,
				DD.DMEDeposit,
				PracPayer.Name as Payer,
                ISNULL(D.[PatientFirstName], '')	AS PatientFirstName,
                ISNULL(D.[PatientLastName], '')		AS PatientLastName
                
        FROM    
				dispense D
				
                INNER JOIN DispenseDetail DD 
					ON D.[DispenseID] = DD.[DispenseID]
					
                LEFT JOIN [Physician] Phy 
					ON Dd.[PhysicianID] = Phy.[PhysicianID]
					AND Phy.IsActive = 1
					
                LEFT  JOIN [Contact] 
					C ON Phy.[ContactID] = c.[ContactID]
					AND C.IsActive = 1
					
                INNER JOIN [PracticeCatalogProduct] PCP ON 
					DD.[PracticeCatalogProductID] = PCP.[PracticeCatalogProductID]

				
				INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
					ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
					
--                INNER JOIN [Practice] P 
--					ON PCP.[PracticeID] = P.[PracticeID]
					
                INNER JOIN [PracticeLocation] PL 
					ON D.[PracticeLocationID] = PL.[PracticeLocationID]
					
					
                INNER JOIN [ThirdPartyProduct] TPP 
					ON PCP.[ThirdPartyProductID] = TPP.[ThirdPartyProductID]
					
				LEFT JOIN [PracticePayer] PracPayer on PracPayer.PracticePayerID = D.PracticePayerID

        WHERE   
				--p.[PracticeID] = @PracticeID
                --AND 
                pl.[PracticeLocationID] = @PracticeLocationID
                AND D.[IsActive] = 1
                AND DD.[IsActive] = 1
                --  AND PHY.[IsActive] = 1
                --  AND C.[IsActive] = 1
                AND PCP.[IsActive] = 1
                AND PCSB.IsActive = 1
                -- AND P.[IsActive] = 1
                AND PL.[IsActive] = 1
                
        
		--ORDER BY [DateDispensed] DESC,
		  ORDER BY [CreatedDate] DESC,
                suppliername,
                [TPP].name                 
                
      ) AS SUB
      ORDER BY
			--SUB.DateDispensed DESC,
			SUB.CreatedDate DESC,
            SUB.suppliername,
            SUB.name    	
    END
    
--SELECT * FROM DISPENSE WHERE DISPENSEid = 57
--SELECT * FROM DISPENSEDETAIL WHERE DISPENSEid = 57
