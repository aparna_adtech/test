alter table Practice add EMRIntegrationDispenseReceiptPushEnabled bit not null default 0;
alter table DispenseQueue alter column PatientCode varchar(255) null;
alter table Dispense alter column PatientCode varchar(255) null;
