USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProductOnHandKPI]    Script Date: 04/01/2009 12:14:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC [usp_ProductOnHandKPI] 3
-- =============================================
-- Author:		Greg Ross
-- Create date: 11/28/07
-- Description:	Used to get wholesale cost for Product On Hand KPI
-- =============================================
ALTER PROCEDURE [dbo].[usp_ProductOnHandKPI]

	@PracticeLocationID int
	
AS
BEGIN
	SET NOCOUNT ON;
	
	Declare @PracticeID int
	Set @PracticeID = (Select TOP 1 PracticeID from PracticeLocation where PracticeLocationID = @PracticeLocationID)

Select 
	SUM(tot.TotalCost) as WholesaleCost,
	'Products On Hand' as Name
from
(
SELECT 
		  SUB.PracticeName
		, SUB.PracticeLocation

		, SUB.PracticeCatalogSupplierBrandID

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
			ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
			END					AS SupplierBrand

		, SUB.SupplierShortName	AS Supplier

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
		ELSE SUB.BrandShortName
		END						AS Brand

		, SUB.Sequence	

		, SUB.ProductName			AS Product
		, SUB.Side + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender
		, SUB.Code

		
		, SUM(SUB.QuantityOnHandPerSystem)					AS QuantityOnHandPerSystem
		, SUB.ParLevel										AS ParLevel
		, CAST(SUB.WholesaleCost AS DECIMAL(15,2) )			AS ActualWholesaleCost
		, CAST( SUM(SUB.LineTotal)  AS DECIMAL(15,2) )		AS TotalCost

FROM
	(SELECT 
		  P.PracticeName
		, PL.NAME													AS PracticeLocation

		, PCSB.IsThirdPartySupplier

		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierShortName
		, PCSB.BrandShortName

		, ISNULL(PCSB.Sequence, 999)								AS Sequence
		, PCP.PracticeCatalogProductID

		, MCP.[Name] AS ProductName
		, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) )		AS Side
		, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )				AS Size
		, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
		, ISNULL(MCP.Code, '')										AS Code
		
		, (PCP.WholesaleCost / PCP.StockingUnits) as WholesaleCost
		, PI.QuantityOnHandPerSystem
		, PI.ParLevel
		, (PCP.WholesaleCost / PCP.StockingUnits) * PI.QuantityOnHandPerSystem as LineTotal

		
	From dbo.ProductInventory as PI with (NOLOCK)

	INNER JOIN dbo.PracticeCatalogProduct AS PCP	WITH (NOLOCK)
		on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID

	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON PI.PracticeLocationID = PL.PracticeLocationID

	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID

	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 

	INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
		ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

	WHERE 
		P.PracticeID = @PracticeID		
		AND P.IsActive = 1
		AND PL.IsActive = 1
		AND PCP.IsActive = 1
		AND PCSB.IsActive = 1
		AND MCP.IsActive = 1
		and PI.IsSetToActive=1
		and PI.IsActive=1
					
		AND PL.PracticeLocationID  = @practicelocationid 			
			
	
		
	UNION
	
		SELECT 
		  P.PracticeName
		, PL.NAME													AS PracticeLocation

		, PCSB.IsThirdPartySupplier

		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierShortName
		, PCSB.BrandShortName
		
		, ISNULL(PCSB.Sequence, 999) + 1000							AS Sequence
		, PCP.PracticeCatalogProductID

		, TPP.[Name]												AS ProductName
		, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) )		AS Side
		, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )				AS Size
		, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
		, ISNULL(TPP.Code, '')										AS Code
		
		, (PCP.WholesaleCost / PCP.StockingUnits) as WholesaleCost
		, PI.QuantityOnHandPerSystem
		, PI.ParLevel
		, (PCP.WholesaleCost / PCP.StockingUnits) * PI.QuantityOnHandPerSystem			as LineTotal
				
		
	From dbo.ProductInventory as PI with (NOLOCK)

	INNER JOIN dbo.PracticeCatalogProduct AS PCP	WITH (NOLOCK)
		on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID

	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON PI.PracticeLocationID = PL.PracticeLocationID

	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID

	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 

	INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
		ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

	WHERE 
		P.PracticeID = @PracticeID		
		AND P.IsActive = 1
		AND PL.IsActive = 1
		AND PCP.IsActive = 1
		AND PCSB.IsActive = 1
		AND TPP.IsActive = 1
		and PI.IsSetToActive=1
		and PI.IsActive=1

		AND PL.PracticeLocationID = @practiceLocationID 
			

	)		
		AS SUB	
		
	GROUP BY
		  SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.PracticeCatalogSupplierBrandID
		, SUB.IsThirdPartySupplier
		, SUB.Sequence
		, SUB.SupplierShortName
		, SUB.BrandShortName
		
		, SUB.ProductName
		, SUB.Side
		, SUB.Size
		, SUB.Gender
		, SUB.Code
		
		, SUB.WholesaleCost
		, SUB.ParLevel
		
	/*ORDER BY
		  SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.Sequence
		, SUB.SupplierShortName
		, SUB.BrandShortName
		, SUB.ProductName
		, SUB.Code
		, SUB.Side + ', ' + SUB.Size  + ', ' + SUB.Gender*/
		) as tot
END
