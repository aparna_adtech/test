use [BregVision]
go

if not exists (select * from dbo.syscolumns where id = object_id(N'Practice') and name=(N'ABNCostIsCash'))
alter table [Practice] add ABNCostIsCash [bit] DEFAULT 0 NOT NULL
GO