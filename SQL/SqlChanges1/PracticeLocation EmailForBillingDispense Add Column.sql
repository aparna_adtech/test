use [BregVision]
go

if not exists (select * from dbo.syscolumns where id = object_id(N'PracticeLocation') and name=(N'EmailForBillingDispense'))
alter table [PracticeLocation] add EmailForBillingDispense VarChar(100) NULL
GO