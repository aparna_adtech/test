USE [BregVision]
GO
/****** Object:  StoredProcedure [dbo].[usp_Report_ProductsDispensed_By_Physician_With_DateDispensed_PatientCode]    Script Date: 3/26/2017 10:38:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--  [usp_Report_ProductsDispensed_By_Physician_With_DateDispensed_PatientCode]  6, '13, 15, 16, 17, 18, 19, 20, 22, 23, 24',
--		'34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 58, 59, 60, 61, 62, 63, 64, 65, 66'
--		,'1/1/2007', '12/31/2007'  -- 1110

----

--  [usp_Report_ProductsDispensed_By_Physician_With_DateDispensed_PatientCode]  18, '56,57', '1', '1/1/2007', '12/31/2007'
--    [usp_Report_ProductsDispensed_By_Physician_With_DateDispensed_PatientCode]  1, '3', '2,-999', '1/1/2007', '12/31/2007'  --94

--  Added Supplier Brand.
--  Added Sequence
--  Add IsActive = 1

-- Modification:  , D.DateDispensed AS DateDispensed  -- , DD.CreatedDate AS DateDispensed  --  2008.02.12  JB

ALTER PROC [dbo].[usp_Report_ProductsDispensed_By_Physician_With_DateDispensed_PatientCode]
		  @PracticeID			INT
		, @PracticeLocationID	VARCHAR(2000)
		, @PhysicianID			VARCHAR(2000)
		, @StartDate			DATETIME
		, @EndDate				DATETIME
		, @OrderBy				int
		, @OrderDirection		bit
		, @IsMedicare           bit = null
		, @MedicareOrderDirection bit = null
		, @InternalPracticeLocationID VARCHAR(2000)
		, @InternalPhysicianID	VARCHAR(2000)

AS
BEGIN


--SELECT * FROM PracticelOCATION WHERE PRACTICEID = 18
--select * from Practice WHERE PRACTICEID = 18


--DECLARE @Output VARCHAR(500)
--SET @Output = ''
--
--SELECT @Output =
--	CASE WHEN @Output = '' THEN CAST(PracticeLocationID AS VARCHAR(50))
--		 ELSE @Output + ', ' + CAST(PracticeLocationID AS VARCHAR(50))
--		 END
--FROM PracticeLocation
--WHERE PracticeID = 6

--
--SELECT @Output AS Output

--	DECLARE @PracticeID INT
--	SET @PracticeID = 6

--  SET @EndDate to 11:59:59 PM
SELECT @EndDate = DATEADD(DAY, 1, @EndDate)

DECLARE @locations TABLE([PracticeLocationID] INT);

IF(@InternalPracticeLocationID = 'ALL')
BEGIN
	DECLARE @tempLocations TABLE (
		PracticeLocationID INT
		,PracticeLocationName VARCHAR(50)
		,IsPrimaryLocation BIT
		);

	INSERT @tempLocations
	EXEC dbo.usp_PracticeLocation_Select_All_ID_Name_Given_PracticeID @PracticeID = @PracticeID;

	INSERT @locations ([PracticeLocationID])
	SELECT PracticeLocationID
	FROM @tempLocations;
END;
ELSE
BEGIN
	INSERT @locations ([PracticeLocationID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable(@PracticeLocationID, ',');
END;

DECLARE @physicians TABLE([PhysicianID] INT);

IF(@InternalPhysicianID = 'ALL')
BEGIN
	DECLARE @tempPhysicians TABLE (
		PracticeID INT
		,PhysicianID INT
		,ContactID INT
		,PhysicianName VARCHAR(100)
		);

	INSERT @tempPhysicians
	EXEC dbo.usp_Physician_SelectALL_Names_ByPracticeID @PracticeID = @PracticeID;

	INSERT @physicians ([PhysicianID])
	SELECT PhysicianID
	FROM @tempPhysicians;

	INSERT @physicians ([PhysicianID])
	SELECT NULL;
END;
ELSE
BEGIN
	INSERT @physicians ([PhysicianID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianID, ',');
END;


--DECLARE @PhysicianIDString VARCHAR(2000)
--SET @PhysicianIDString = @PhysicianID
---  Insert select here ----

INSErt into dbo.zzzReportMessage
		(MESSAGE)
SELECT @PhysicianID



SELECT

		  ISNULL(SUB.PhysicianID, -999) AS PhysicianID
		, SUB.PracticeName		AS Practice
		, SUB.PracticeLocation	AS Location

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
			ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
			END					AS SupplierBrand


		, SUB.SupplierShortName	AS Supplier

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
		ELSE SUB.BrandShortName
		END						AS Brand

--		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN 1
--		ELSE 0
--		END AS IsBrandSuppressed
--		, SUB.IsThirdPartySupplier
		, SUB.Sequence

		, SUB.ProductName			AS Product

		, SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender

		, SUB.FitterName

		, SUB.Code

		, COALESCE(SUB.HCPCs, dbo.ConcatHCPCS(SUB.PracticeCatalogProductID)) AS HCPCS -- JB 20071004 Added Column for UDF Concat of HCPCS
		, case SUB.IsCashCollection
			when 1 then Sub.BillingChargeCash
			when 0 then 0
			end
			 as BillingChargeCash

		, CASE WHEN LEN(SUB.Physician) > 0 THEN SUB.Physician ELSE '<blank>' END AS Physician
		, CONVERT(VARCHAR(10), SUB.DateDispensed, 101) AS DateDispensed
		, SUB.PatientCode

		, CASE SUB.IsMedicare
			When 1 then 'Y'
			When 0 then 'N'
		  End
		  as IsMedicare
		, CASE CAST(SUB.ABNType AS varchar(50))
			When 'Medicare' then 'M' + ',' + CAST(SUB.MedicareOption AS varchar(50))
			When 'Commercial' then 'C' + ',' + CAST(SUB.MedicareOption AS varchar(50))
		  End
		  as ABNType

		, SUB.ICD9Code
		, SUB.Quantity 		--, SUM( SUB.Quantity )

--		, CAST( SUB.ActualWholesaleCost AS DECIMAL(15,2) )	AS ActualWholesaleCost
--		, CAST( SUB.ActualChargeBilled AS DECIMAL(15,2) )	AS ActualChargeBilled
--		, CAST( SUB.DMEDeposit AS DECIMAL(15,2) )			AS DMEDeposit

		, CAST(  SUB.ActualWholesaleCostLineTotal  AS DECIMAL(15,2) )	AS ActualWholesaleCostLineTotal
		, CAST(  SUB.DMEDepositLineTotal  AS DECIMAL(15,2) )			AS DMEDepositLineTotal
		, SUB.IsCashCollection
		--, CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )    AS ActualChargeBilledLineTotal
		,  case SUB.IsCashCollection
			when 1 then 0
			when 0 then CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )
			end
			AS ActualChargeBilledLineTotal

--		, SUB.Comment  -- Not Displayed for Version 1.

		, ISNULL(SUB.PayerName, 'N/A') AS PayerName
		, SUB.DispenseNote
		, SUB.OriginalActualChargeBilled
		, LTRIM(RTRIM(ISNULL(SUB.PatientFirstName,''))) AS PatientFirstName
		, LTRIM(RTRIM(ISNULL(SUB.PatientLastName,''))) AS PatientLastName
		, SUB.Mod1
		, SUB.Mod2
		, SUB.Mod3
FROM


		(
		SELECT
			  Ph.PhysicianID
			, P.PracticeName
			, PL.Name AS PracticeLocation
			, IsNULL(RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix), '') AS Physician

			, D.PatientCode

			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) AS Sequence

			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash

			, MCP.[Name] AS ProductName

			--, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender

			, ISNULL(MCP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed  -- , DD.CreatedDate AS DateDispensed  --  2008.02.12  JB  --, DD.CreatedDate AS DateDispensed
			, DD.IsMedicare
			, DD.ABNType
			, DD.ICD9Code
			, DD.Quantity
			, FC.FirstName + ' ' + FC.LastName as FitterName
			, PI_DD.ActualWholesaleCost
			, DD.ActualChargeBilled as ActualChargeBilled
			, DD.DMEDeposit
			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit AS DMEDepositLineTotal
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.

			, PP.Name AS PayerName
			, D.Notes AS DispenseNote
			, DQ.HCPCs
			, DD.ActualChargeBilled as OriginalActualChargeBilled
			, D.PatientFirstName as PatientFirstName
			, D.PatientLastName as PatientLastName
			, DD.Mod1
			, DD.Mod2
			, DD.Mod3
			, dq.MedicareOption

		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOin DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
			ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID		    --  Can be inactive

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1

		LEFT JOIN PracticePayer PP
			on PP.PracticePayerID = D.PracticePayerID

		LEFT JOIN DispenseQueue DQ
			on DQ.DispenseQueueID = DD.DispenseQueueID

		INNER JOIN Clinician AS CP
			ON CP.ClinicianID = DD.FitterID
		INNER JOIN Contact AS FC
			ON CP.ContactID = FC.ContactID

		WHERE
			P.PracticeID = @PracticeID
			AND DD.IsMedicare = Coalesce(@IsMedicare, DD.IsMedicare)
			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			--AND PCP.IsActive = 1
			--AND PCSB.IsActive = 1
			--AND MCP.IsActive = 1

		AND
		PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			FROM @locations)

--		AND
--		Ph.PhysicianID IN
--			(SELECT ID
--			 FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianIDString, ','))

		AND
			(
					PH.PhysicianID
					IN (
						 SELECT PhysicianID
						 FROM @physicians
						)
					OR
				(
					(PH.PhysicianID IS NULL)
						AND
					( 1 = (SELECT CASE WHEN PhysicianID = -999 THEN 1 ELSE 0 END --AS IsNoPhysician
					 FROM @physicians
					 WHERE PhysicianID = -999)
					)
				)
			)

		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM

		UNION ALL

		SELECT

			  Ph.PhysicianID
			, P.PracticeName
			, PL.Name AS PracticeLocation
			, IsNULL(RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix), '') AS Physician

			, D.PatientCode

			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) + 1000 AS Sequence

			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash

			, TPP.[Name] AS ProductName

			--, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender


			, ISNULL(TPP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed  -- , DD.CreatedDate AS DateDispensed  --  2008.02.12  JB
			, DD.IsMedicare
			, DD.ABNType
			, DD.ICD9Code
			, DD.Quantity
			, FC.FirstName + ' ' + FC.LastName as FitterName
			, PI_DD.ActualWholesaleCost
			, DD.ActualChargeBilled as ActualChargeBilled
			, DD.DMEDeposit
			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit AS DMEDepositLineTotal
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.

			, PP.Name AS PayerName
			, D.Notes AS DispenseNote
			, DQ.HCPCs
			, DD.ActualChargeBilled as OriginalActualChargeBilled
			, D.PatientFirstName as PatientFirstName
			, D.PatientLastName as PatientLastName
			, DD.Mod1
			, DD.Mod2
			, DD.Mod3
			, dq.MedicareOption

		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOin DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
			ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID			--  Can be inactive

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1

		LEFT JOIN PracticePayer PP
			on PP.PracticePayerID = D.PracticePayerID
		
		INNER JOIN Clinician AS CP
			ON CP.ClinicianID = DD.FitterID
		INNER JOIN Contact AS FC
		ON CP.ContactID = FC.ContactID

		LEFT JOIN DispenseQueue DQ
			on DQ.DispenseQueueID = DD.DispenseQueueID

		WHERE
			P.PracticeID = @PracticeID
			AND DD.IsMedicare = Coalesce(@IsMedicare, DD.IsMedicare)
			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			--AND PCP.IsActive = 1
			--AND PCSB.IsActive = 1
			--AND TPP.IsActive = 1

		AND
		PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			FROM @locations)

		AND
			(
					PH.PhysicianID
					IN (
						 SELECT PhysicianID
						 FROM @physicians
						)
					OR

				(
					(PH.PhysicianID IS NULL)
						AND
					( 1 = (SELECT CASE WHEN PhysicianID = -999 THEN 1 ELSE 0 END --AS IsNoPhysician
					 FROM @physicians
					 WHERE PhysicianID = -999)
					)
				)

--				(
--					(PH.PhysicianID IS NULL)
--						AND
--					( 1 = (SELECT CASE WHEN ISNULL(ID, -999) = -999 THEN 1 ELSE 0 END --AS IsNoPhysician
--					 FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianIDString, ',')
--					 WHERE ISNULL(ID, -999) = -999)
--					)
--				)
			)


		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM

		) AS Sub

----
------GROUP BY
------		SUB.PracticeName
------		, SUB.PracticeLocation
------		, SUB.Physician
------		, SUB.IsThirdPartySupplier
------		, SUB.Sequence
------		, SUB.SupplierShortName
------		, SUB.BrandShortName
------
------		, SUB.ProductName
------		, SUB.Side
------		, SUB.Size
------		, SUB.Gender
------		, SUB.Code
------
------		, SUB.PatientCode
------
------		, SUB.DateDispensed
------
------		, SUB.ActualWholesaleCost
------		, SUB.ActualChargeBilled
------		, Sub.DMEDeposit
------		, Sub.Comment
----
----

--if @OrderBy	= 1
--begin
	 order by

	 --/*
		CASE
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName DESC))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName))

			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName DESC))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName))

			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName DESC ))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName ))


			WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed))

			WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed))

			WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed DESC))
			WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed))

		END
		--*/
/*
		CASE WHEN @OrderBy = 1 and @OrderDirection = 0  THEN SUB.ProductName End DESC,
		CASE WHEN @OrderBy = 1 and @OrderDirection = 1  THEN SUB.ProductName END,
		CASE WHEN @OrderBy = 2 and @OrderDirection = 0  THEN SUB.DateDispensed End DESC,
		CASE WHEN @OrderBy = 2 and @OrderDirection = 1  THEN SUB.DateDispensed END,
		CASE WHEN @OrderBy = 3 and @OrderDirection = 0  THEN SUB.ProductName End DESC,
		CASE WHEN @OrderBy = 3 and @OrderDirection = 1  THEN SUB.ProductName END
*/

--	if @OrderDirection = 0
--	begin
--		select ' desc'
--	end
--	CASE
--		WHEN @OrderDirection = 0 THEN ' desc'
--	END
--SUB.ProductName
--end
----	ORDER BY

----		SUB.PracticeName
----		, SUB.PracticeLocation
----
------		, SUB.Physician
----
----		, SUB.Sequence		--  Note 3rd Party has 100 added to it sequence for this query.
------		, SUB.IsThirdPartySupplier
----		, SUB.SupplierShortName
----		, SUB.BrandShortName
----
----		, SUB.ProductName
----		, SUB.Code
----
----		, SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender
----		, SUB.DateDispensed		--  JB  2008.01.09
----		, SUB.PatientCode		--  JB  2008.01.09

END
