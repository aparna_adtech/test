--AdMediaType
SET IDENTITY_INSERT [AdMediaType] ON
insert AdMediaType ( Id,Name )  select 1,'Image'
SET IDENTITY_INSERT [AdMediaType] OFF

--AdLocation
SET IDENTITY_INSERT [AdLocation] ON
insert AdLocation ( Id,Name )  select 1,'Horizontal Banner Ad'
insert AdLocation ( Id,Name )  select 2,'Vertical Banner Ad'
insert AdLocation ( Id,Name )  select 3,'Large Horizontal Featured Products Ad'
SET IDENTITY_INSERT [AdLocation] OFF