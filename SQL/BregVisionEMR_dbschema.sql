USE [BregVisionEMR]
GO
/****** Object:  Table [dbo].[schedule_item]    Script Date: 02/10/2013 14:51:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[schedule_item](
	[schedule_item_id] [uniqueidentifier] NOT NULL,
	[practice_id] [int] NOT NULL,
	[practice_location_id] [int] NOT NULL,
	[patient_id] [nvarchar](20) NULL,
	[patient_name] [nvarchar](50) NULL,
	[patient_email] [nvarchar](50) NULL,
	[physician_name] [nvarchar](50) NULL,
	[diagnosis_icd9] [nvarchar](50) NULL,
	[scheduled_visit_datetime] [datetime] NOT NULL,
	[creation_date] [datetime] NOT NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_schedule_item] PRIMARY KEY CLUSTERED 
(
	[schedule_item_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_schedule_item] ON [dbo].[schedule_item] 
(
	[practice_id] ASC,
	[practice_location_id] ASC,
	[scheduled_visit_datetime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[schedule_item_incoming]    Script Date: 02/10/2013 14:51:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[schedule_item_incoming](
	[patient_id] [nvarchar](255) NULL,
	[patient_last_name] [nvarchar](255) NULL,
	[patient_first_name] [nvarchar](255) NULL,
	[physician_last_name] [nvarchar](255) NULL,
	[physician_first_name] [nvarchar](255) NULL,
	[scheduled_visit_datetime] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Trigger [schedule_item_incoming_insert]    Script Date: 02/10/2013 14:51:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Henry Chan
-- Create date: 12/3/2012
-- Description:	
-- =============================================
CREATE TRIGGER [dbo].[schedule_item_incoming_insert] 
   ON  [dbo].[schedule_item_incoming] 
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @patient_id nvarchar(20);
	declare @patient_name nvarchar(50);
	declare @patient_email nvarchar(50);
	declare @physician_name nvarchar(50);
	declare @diagnosis_icd9 nvarchar(50);
	declare @scheduled_visit_datetime datetime;
	
	select
		@patient_id = patient_id,
		@patient_name = patient_first_name + ' ' + patient_last_name,
		@patient_email = null,
		@physician_name = physician_first_name + ' ' + physician_last_name,
		@scheduled_visit_datetime = scheduled_visit_datetime
	from
		inserted;

    -- Insert statements for trigger here
	insert into schedule_item
	values
	(
	NEWID(),
	1,
	3,
	@patient_id,
	@patient_name,
	@patient_email,
	@physician_name,
	@diagnosis_icd9,
	@scheduled_visit_datetime,
	GETDATE(),
	1
	);
END
GO
/****** Object:  Default [DF_schedule_item_schedule_item_id]    Script Date: 02/10/2013 14:51:38 ******/
ALTER TABLE [dbo].[schedule_item] ADD  CONSTRAINT [DF_schedule_item_schedule_item_id]  DEFAULT (newid()) FOR [schedule_item_id]
GO
/****** Object:  Default [DF_schedule_item_creation_date]    Script Date: 02/10/2013 14:51:38 ******/
ALTER TABLE [dbo].[schedule_item] ADD  CONSTRAINT [DF_schedule_item_creation_date]  DEFAULT (getdate()) FOR [creation_date]
GO
/****** Object:  Default [DF_schedule_item_is_active]    Script Date: 02/10/2013 14:51:38 ******/
ALTER TABLE [dbo].[schedule_item] ADD  CONSTRAINT [DF_schedule_item_is_active]  DEFAULT ((1)) FOR [is_active]
GO
