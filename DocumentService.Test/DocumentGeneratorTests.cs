﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace DocumentService.Test
{
    [TestClass]
    public class DocumentGeneratorTests
    {
        private const string testTemplate = "<html><body><div style='color:red;font-size:16pt;'>Test {0}</div></body></html>";
        private const string testPathTemplate = "{0}/../../testResults/{1}";

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            var path = String.Format(testPathTemplate, Environment.CurrentDirectory,"");
            bool exists = Directory.Exists(path);
            if (!exists)
                Directory.CreateDirectory(path);
        }

        [TestMethod]
        public void CanGeneratePdf()
        {
            //Arrange
            var template = String.Format(testTemplate, 1);
            var path = String.Format(testPathTemplate, Environment.CurrentDirectory, "test.pdf");
            //Act
            var success = DocumentGenerator.CreatePdf(template, path);
            //Assert
            Assert.IsTrue(success, "Unable to create file");
            Assert.IsTrue(File.Exists(path), "Generate method returned success, but file not created");
        }

        [TestMethod]
        public void CanConcatenatePdfs()
        {
            //Arrange
            var formTemplate = String.Format(testTemplate, "This is the original form, onto which attachments are concatenated.");
            var mergedFormPath = String.Format(testPathTemplate, Environment.CurrentDirectory, "testMergedForm.pdf");

            var template = String.Format(testTemplate, "Attachment 1");
            var path = String.Format(testPathTemplate, Environment.CurrentDirectory, "testAttachment.pdf");
            var template2 = String.Format(testTemplate, "Attachment 2");
            var path2 = String.Format(testPathTemplate, Environment.CurrentDirectory, "testAttachment2.pdf");
            var template3 = String.Format(testTemplate, "Attachment 3");
            var path3 = String.Format(testPathTemplate, Environment.CurrentDirectory, "testAttachment3.pdf");

            //Act
            //Create Attachments
            var result1 = DocumentGenerator.CreatePdf(template, path);
            var result2 = DocumentGenerator.CreatePdf(template2, path2);
            var result3 = DocumentGenerator.CreatePdf(template3, path3);
            Assert.IsTrue(result1); Assert.IsTrue(result2); Assert.IsTrue(result3);
            byte[] file = File.ReadAllBytes(path), file2 = File.ReadAllBytes(path2), file3 = File.ReadAllBytes(path3);
            var attachments = new byte[][] { file, file2, file3 };
            //End: Create Attachments

            var result = DocumentGenerator.CreatePdfWithAttachments(formTemplate, mergedFormPath, attachments);

            Assert.IsTrue(result, "Unable to create concatenated file");
            Assert.IsTrue(File.Exists(mergedFormPath), "Generate method returned success, but concatenated file not created");
        }
    }


    #region Helper Methods

    #endregion Helper Methods
}
