﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using ClassLibrary.DAL;

namespace ClassLibrary.BLL.Forums
{
    [DataContract]
    public class UserPublicInfo : EntityBase
    {
        #region Properties

        int _UserId;
        [DataMember]
        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; OnPropertyChanged("UserId"); }
        }

        string _PublicUserName;
        [DataMember]
        public string PublicUserName
        {
            get { return _PublicUserName; }
            set { _PublicUserName = value; OnPropertyChanged("PublicUserName"); }
        }

        string _PublicRole;
        [DataMember]
        public string PublicRole
        {
            get { return _PublicRole; }
            set { _PublicRole = value; OnPropertyChanged("PublicRole"); }
        }

        string _PublicDescription;
        [DataMember]
        public string PublicDescription
        {
            get { return _PublicDescription; }
            set { _PublicDescription = value; OnPropertyChanged("PublicDescription"); }
        }

        #endregion

        #region Methods

        public static UserPublicInfoResponse UpdateUserPublicInfo(UserPublicInfo userPublicInfo)
        {
            UserPublicInfoResponse ret = new UserPublicInfoResponse();

            try
            {
                VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

                using (visionDB)
                {
                    //get the entity we are going to update
                    var query = from u in visionDB.Users
                                where u.UserID == userPublicInfo.UserId
                                select u;

                    DAL.User entityUser = query.FirstOrDefault();

                    if (entityUser != null)
                    {
                        //update it
                        entityUser.PublicUserName = userPublicInfo.PublicUserName;
                        entityUser.PublicRole = userPublicInfo.PublicRole;
                        entityUser.PublicDescription = userPublicInfo.PublicDescription;

                        visionDB.SubmitChanges();
                        ret.UserPublicInfo = userPublicInfo;
                    }
                }
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public static UserPublicInfoResponse GetUserPublicInfo(int userId)
        {
            UserPublicInfoResponse ret = new UserPublicInfoResponse();

            try
            {
                VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

                using (visionDB)
                {
                    var query = from u in visionDB.Users
                                where u.UserID == userId
                                select u;

                    DAL.User entityUser = query.FirstOrDefault();

                    if (entityUser != null)
                    {
                        ret.UserPublicInfo = new UserPublicInfo();
                        ret.UserPublicInfo.UserId = userId;

                        ret.UserPublicInfo.PublicUserName = entityUser.PublicUserName;
                        ret.UserPublicInfo.PublicRole = entityUser.PublicRole;
                        ret.UserPublicInfo.PublicDescription = entityUser.PublicDescription;
                    }
                }
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        #endregion

    }
}
