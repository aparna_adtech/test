﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ClassLibrary.BLL.Forums
{
    [DataContract]
    public class UserPublicInfoResponse
    {
        Exception _Exception;
        [DataMember]
        public Exception Exception
        {
            get { return _Exception; }
            set { _Exception = value; }
        }

        UserPublicInfo _UserPublicInfo;
        [DataMember]
        public UserPublicInfo UserPublicInfo
        {
            get { return _UserPublicInfo; }
            set { _UserPublicInfo = value; }
        }
    }

}
