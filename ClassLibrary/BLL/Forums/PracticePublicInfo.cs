﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClassLibrary.DAL;
using System.Runtime.Serialization;

namespace ClassLibrary.BLL.Forums
{
    [DataContract]
    public class PracticePublicInfo : EntityBase
    {
        #region Properties

        int _UserId;
        [DataMember]
        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; OnPropertyChanged("UserId"); }
        }

        int _PracticeId;
        [DataMember]
        public int PracticeId
        {
            get { return _PracticeId; }
            set { _PracticeId = value; OnPropertyChanged("PracticeId"); }
        }

        string _PracticeName;
        [DataMember]
        public string PracticeName
        {
            get { return _PracticeName; }
            set { _PracticeName = value; OnPropertyChanged("PracticeName"); }
        }

        string _PublicDescription;
        [DataMember]
        public string PublicDescription
        {
            get { return _PublicDescription; }
            set { _PublicDescription = value; OnPropertyChanged("PublicDescription"); }
        }

        #endregion

        #region Methods

        public static PracticePublicInfoResponse UpdatePracticePublicInfo(PracticePublicInfo practicePublicInfo)
        {
            PracticePublicInfoResponse ret = new PracticePublicInfoResponse();

            try
            {
                VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

                using (visionDB)
                {
                    //get the entity we are going to update
                    var query = from p in visionDB.Practices
                                where p.PracticeID == practicePublicInfo.PracticeId
                                select p;

                    DAL.Practice entityPractice = query.FirstOrDefault();

                    if (entityPractice != null)
                    {
                        //update it
                        entityPractice.PublicDescription = practicePublicInfo.PublicDescription;

                        visionDB.SubmitChanges();
                        ret.PracticePublicInfo = practicePublicInfo;
                    }
                }
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public static PracticePublicInfoResponse GetPracticePublicInfo(int userId)
        {
            PracticePublicInfoResponse ret = new PracticePublicInfoResponse();

            try
            {
                VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

                using (visionDB)
                {
                    var query = from p in visionDB.Practices
                                join u in visionDB.Users on p.PracticeID equals u.PracticeID
                                where u.UserID == userId
                                select p;
                    
                    DAL.Practice entityPractice = query.FirstOrDefault();

                    if (entityPractice != null)
                    {
                        ret.PracticePublicInfo = new PracticePublicInfo();
                        ret.PracticePublicInfo.PracticeId = entityPractice.PracticeID; ;
                        ret.PracticePublicInfo.UserId = userId;
                        ret.PracticePublicInfo.PracticeName = entityPractice.PracticeName;
                        ret.PracticePublicInfo.PublicDescription = entityPractice.PublicDescription;
                    }
                }
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        #endregion
    }
}
