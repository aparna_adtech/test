﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ClassLibrary.BLL.Forums
{
    
    [DataContract]
    public class PracticePublicInfoResponse
    {
        Exception _Exception;
        [DataMember]
        public Exception Exception
        {
            get { return _Exception; }
            set { _Exception = value; }
        }

        PracticePublicInfo _PracticePublicInfo;
        [DataMember]
        public PracticePublicInfo PracticePublicInfo
        {
            get { return _PracticePublicInfo; }
            set { _PracticePublicInfo = value; }
        }
    }

}
