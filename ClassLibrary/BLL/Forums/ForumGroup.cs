﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Runtime.Serialization;
using ClassLibrary.DAL;

namespace ClassLibrary.BLL.Forums
{
    
    public class ForumGroup : EntityBase
    {

        #region Properties
        
        List<ForumArticle> _ForumArticles;
        [DataMember]
        public List<ForumArticle> ForumArticles
        {
            get { return _ForumArticles; }
            set { _ForumArticles = value; OnPropertyChanged("ForumArticles"); }
        }

        int _Id;
        [DataMember]
        public int Id
        {
            get { return _Id; }
            set { _Id = value; OnPropertyChanged("Id"); }
        }

        int? _ParentId;
        [DataMember]
        public int? ParentId
        {
            get { return _ParentId; }
            set { _ParentId = value; OnPropertyChanged("ParentId"); }
        }

        string _Name;
        [DataMember]
        public string Name
        {
            get { return _Name; }
            set { _Name = value; OnPropertyChanged("Name"); }
        }

      

        DateTime _Created;
        [DataMember]
        public DateTime Created
        {
            get { return _Created; }
            set { _Created = value; OnPropertyChanged("Created"); }
        }

        bool _IsActive;
        [DataMember]
        public bool IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; OnPropertyChanged("IsActive"); }
        }
       
        int _SortOrder;
        [DataMember]
        public int SortOrder
        {
            get { return _SortOrder; }
            set { _SortOrder = value; OnPropertyChanged("SortOrder"); }
        }

        #endregion

        public static ForumGroupsResponse GetForumGroups(bool includeArticles, int depth)
        {
            ForumGroupsResponse ret = new ForumGroupsResponse();

            try
            {
                ret.ForumGroups = new List<ClassLibrary.BLL.Forums.ForumGroup>();

                List<ClassLibrary.DAL.ForumGroup> entities;

                VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
                using (visionDB)
                {
                    var query = from fg in visionDB.ForumGroups
                                where fg.IsActive == true
                                orderby fg.SortOrder
                                select fg;

                    entities = query.ToList<ClassLibrary.DAL.ForumGroup>();
                }

                foreach (ClassLibrary.DAL.ForumGroup entity in entities)
                {
                    //translate
                    ClassLibrary.BLL.Forums.ForumGroup bll = new ClassLibrary.BLL.Forums.ForumGroup();
                    bll.Id = entity.Id;
                    bll.ParentId = entity.ParentId;
                    bll.Name = entity.Name;
                    bll.Created = entity.Created;
                    bll.SortOrder = entity.SortOrder;

                    ret.ForumGroups.Add(bll);
                }


                if (includeArticles)
                {
                    foreach (ClassLibrary.BLL.Forums.ForumGroup bll in ret.ForumGroups)
                    {
                        ForumArticlesResponse resp = ForumArticle.GetForumArticlesByForumGroup(bll.Id,depth, true);
                        bll.ForumArticles = resp.ForumArticles;
                    }
                }
                

            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;

        }
    }
}
