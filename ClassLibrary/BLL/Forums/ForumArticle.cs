﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Runtime.Serialization;
using ClassLibrary.DAL;

namespace ClassLibrary.BLL.Forums
{
    [DataContract]
    public class ForumArticle : EntityBase
    {

#region Properties
        List<ForumArticle> _ForumArticles = new List<ForumArticle>();
        [DataMember]
        public List<ForumArticle> ForumArticles
        {
            get { return _ForumArticles; }
            set { _ForumArticles = value; OnPropertyChanged("ForumArticles"); }
        }

        int _Id;
        [DataMember]
        public int Id
        {
            get { return _Id; }
            set { _Id = value; OnPropertyChanged("Id"); }
        }

        int? _ParentId;
        [DataMember]
        public int? ParentId
        {
            get { return _ParentId; }
            set { _ParentId = value; OnPropertyChanged("ParentId"); }
        }

        string _Subject;
        [DataMember]
        public string Subject
        {
            get { return _Subject; }
            set { _Subject = value; OnPropertyChanged("Subject"); }
        }
        
        string _Body;
        [DataMember]
        public string Body
        {
            get { return _Body; }
            set { _Body = value; OnPropertyChanged("Body"); }
        }

        int? _ForumGroupId;
        [DataMember]
        public int? ForumGroupId
        {
            get { return _ForumGroupId; }
            set { _ForumGroupId = value; OnPropertyChanged("ForumGroupId"); }
        }

        string _ForumGroupName;

        /// <summary>
        /// used to show the group name in the recient articles list
        /// </summary>
        public string ForumGroupName
        {
            get { return _ForumGroupName; }
            set { _ForumGroupName = value; OnPropertyChanged("ForumGroupName"); }
        }

        int? _MasterCatalogProductId;
        [DataMember]
        public int? MasterCatalogProductId
        {
            get { return _MasterCatalogProductId; }
            set { _MasterCatalogProductId = value; OnPropertyChanged("MasterCatalogProductId"); }
        }

        int _UserId;
        [DataMember]
        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; OnPropertyChanged("UserId"); }
        }

        DateTime _Created;
        [DataMember]
        public DateTime Created
        {
            get { return _Created; }
            set { _Created = value; OnPropertyChanged("Created"); }
        }

        bool _IsActive;
        [DataMember]
        public bool IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; OnPropertyChanged("IsActive"); }
        }

        bool _IsSticky;
        [DataMember]
        public bool IsSticky
        {
            get { return _IsSticky; }
            set { _IsSticky = value; OnPropertyChanged("IsSticky"); }
        }

        int _Level;
        [DataMember]
        public int Level
        {
            get { return _Level; }
            set { _Level = value; OnPropertyChanged("Level"); }
        }

        int _TopParentId;
        [DataMember]
        public int TopParentId
        {
            get { return _TopParentId; }
            set { _TopParentId = value; OnPropertyChanged("TopParentId"); }
        }

        string _PublicUserName;
        [DataMember]
        public string PublicUserName
        {
          get { return _PublicUserName; }
          set { _PublicUserName = value; OnPropertyChanged("PublicUserName");}
        }

        string _PublicUserRole;
        [DataMember]
        public string PublicUserRole
        {
          get { return _PublicUserRole; }
          set { _PublicUserRole = value; OnPropertyChanged("PublicUserRole");}
        }

        string _PublicUserDescription;
        [DataMember]
        public string PublicUserDescription
        {
          get { return _PublicUserDescription; }
          set { _PublicUserDescription = value; OnPropertyChanged("PublicUserDescription");}
        }

        string _PracticeName;
        [DataMember]
        public string PracticeName
        {
          get { return _PracticeName; }
          set { _PracticeName = value; OnPropertyChanged("PracticeName");}
        }

        string _PracticeDescription;
        [DataMember]
        public string PracticeDescription
        {
          get { return _PracticeDescription; }
          set { _PracticeDescription = value; OnPropertyChanged("PracticeDescription");}
        }

        // recurse all child ForumArticles to get this number
        int _TotalChildrenCount;
        [DataMember]
        public int TotalChildrenCount
        {
            get { return _TotalChildrenCount; }
            set { _TotalChildrenCount = value; OnPropertyChanged("TotalChildrenCount"); }
        }

#endregion

#region Methods

        public static ForumArticlesResponse GetForumArticlesByForumGroup(int id, int depth, bool isActive)
        {
            ForumArticlesResponse ret = new ForumArticlesResponse();
            try
            {
                DAL.Forums.ForumArticle cls = new ClassLibrary.DAL.Forums.ForumArticle();
                ret.ForumArticles = cls.GetForumArticleTreesByForumGroup(id,depth, isActive);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public static ForumArticlesResponse GetForumArticlesByUserId(int userId, int depth, bool isActive)
        {
            ForumArticlesResponse ret = new ForumArticlesResponse();
            try
            {
                DAL.Forums.ForumArticle cls = new ClassLibrary.DAL.Forums.ForumArticle();
                ret.ForumArticles = cls.GetForumArticlesByUserId(userId, depth, isActive);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public static ForumArticlesResponse GetForumArticlesBySearchText(string searchText, int depth, bool isActive)
        {
            ForumArticlesResponse ret = new ForumArticlesResponse();
            try
            {
                DAL.Forums.ForumArticle cls = new ClassLibrary.DAL.Forums.ForumArticle();
                ret.ForumArticles = cls.GetForumArticlesBySearchText(searchText, depth, isActive);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public static ForumArticleResponse Insert(BLL.Forums.ForumArticle forumArticle)
        {
            ForumArticleResponse ret = new ForumArticleResponse();

            try
            {
                DAL.ForumArticle entityArticle = new ClassLibrary.DAL.ForumArticle();

                entityArticle.ParentId = forumArticle.ParentId;
                entityArticle.Subject = forumArticle.Subject;
                entityArticle.Body = forumArticle.Body;
                entityArticle.IsActive = forumArticle.IsActive;
                entityArticle.Created = DateTime.Now.ToUniversalTime();

                if (forumArticle.MasterCatalogProductId != null)
                    entityArticle.MasterCatalogProductID = (int)forumArticle.MasterCatalogProductId;

                entityArticle.ForumGroupId = (int)forumArticle.ForumGroupId;
                entityArticle.UserId = forumArticle.UserId;
                entityArticle.IsSticky = forumArticle.IsSticky;

                VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

                using (visionDB)
                {
                    visionDB.ForumArticles.InsertOnSubmit(entityArticle);
                    visionDB.SubmitChanges();
                }

                forumArticle.Id = entityArticle.Id;

                ret.ForumArticle = forumArticle;
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public static ForumArticleResponse Update(BLL.Forums.ForumArticle forumArticle)
        {
            ForumArticleResponse ret = new ForumArticleResponse();

            try
            {
                VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

                using (visionDB)
                {
                    //get the entity we are going to update
                    var query = from a in visionDB.ForumArticles
                                where a.Id == forumArticle.Id
                                select a;

                    DAL.ForumArticle entityArticle = query.FirstOrDefault();

                    if (entityArticle != null)
                    {
                        //update it
                        entityArticle.ParentId = forumArticle.ParentId;
                        entityArticle.Subject = forumArticle.Subject;
                        entityArticle.Body = forumArticle.Body;

                        entityArticle.ForumGroupId = (int)forumArticle.ForumGroupId;

                        if (forumArticle.MasterCatalogProductId != null)
                            entityArticle.MasterCatalogProductID = (int)forumArticle.MasterCatalogProductId;

                        entityArticle.UserId = forumArticle.UserId;
                        entityArticle.IsSticky = forumArticle.IsSticky;

                        visionDB.SubmitChanges();
                        ret.ForumArticle = forumArticle;
                    }
                }
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }
            
            return ret;
        }

        public static ForumArticlesResponse GetRecentArticles(int count, int depth, bool isActive)
        {
            ForumArticlesResponse ret = new ForumArticlesResponse();
            try
            {
                DAL.Forums.ForumArticle cls = new ClassLibrary.DAL.Forums.ForumArticle();
                ret.ForumArticles = cls.GetRecentArticles(count,depth, isActive);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public static bool SetForumArticleIsActive(int articleId,bool isActive)
        {
            bool ret = false;
            try
            {
                DAL.Forums.ForumArticle cls = new ClassLibrary.DAL.Forums.ForumArticle();
                ret = cls.SetForumArticleIsActive(articleId, isActive);
            }
            catch (Exception ex)
            {
                ret = false;
            }

            return ret;
        }

        public static bool DeleteForumArticle(int articleId)
        {
            bool ret = false;
            try
            {
                DAL.Forums.ForumArticle cls = new ClassLibrary.DAL.Forums.ForumArticle();
                ret = cls.DeleteForumArticle(articleId);
            }
            catch (Exception ex)
            {
                ret = false;
            }

            return ret;
        }

        public static ForumArticlesResponse SearchArticles(string searchText, int count, int depth, bool isActive)
        {
            ForumArticlesResponse ret = new ForumArticlesResponse();
            try
            {
                DAL.Forums.ForumArticle cls = new ClassLibrary.DAL.Forums.ForumArticle();
                ret.ForumArticles = cls.SearchArticles(searchText, count, depth, isActive);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }


        public static ForumArticleResponse GetForumArticleTreeByTopArticle(int id, int depth, bool isActiveOnly)
        {
            ForumArticleResponse ret = new ForumArticleResponse();
            try
            {
                DAL.Forums.ForumArticle cls = new ClassLibrary.DAL.Forums.ForumArticle();
                ret.ForumArticle = cls.GetForumArticleTreeByTopArticle(id, depth, isActiveOnly);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }


        public static ForumArticlesResponse GetRecentArticlesByGroupId(int groupId, int count, int depth, bool isActive)
        {
            ForumArticlesResponse ret = new ForumArticlesResponse();
            try
            {
                DAL.Forums.ForumArticle cls = new ClassLibrary.DAL.Forums.ForumArticle();
                ret.ForumArticles = cls.GetRecentArticlesByGroupId(groupId, count, depth, isActive);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public static void InsertForumArticleImpression(int userId, int forumArticleId)
        {
            try
            {
                DAL.ForumArticleImpression entity = new ClassLibrary.DAL.ForumArticleImpression();

                entity.UserId = userId;
                entity.ArticleId = forumArticleId;

                VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

                using (visionDB)
                {
                    visionDB.ForumArticleImpressions.Attach(entity, true);
                    visionDB.SubmitChanges();
                }
               
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

#endregion

    }
}
