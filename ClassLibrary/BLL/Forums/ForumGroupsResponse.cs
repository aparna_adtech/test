﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ClassLibrary.BLL.Forums
{
    [DataContract]
    public class ForumGroupsResponse
    {
        List<ClassLibrary.BLL.Forums.ForumGroup> _ForumGroups;
        [DataMember]
        public List<ClassLibrary.BLL.Forums.ForumGroup> ForumGroups
        {
            get { return _ForumGroups; }
            set { _ForumGroups = value; }
        }

        Exception _Exception;
        [DataMember]
        public Exception Exception
        {
            get { return _Exception; }
            set { _Exception = value; }
        }
    }
}
