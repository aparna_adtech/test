﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ClassLibrary.BLL.Forums
{
    [DataContract]
    public class ForumArticleResponse
    {
        ForumArticle _ForumArticle;
        [DataMember]
        public ForumArticle ForumArticle
        {
            get { return _ForumArticle; }
            set { _ForumArticle = value; }
        }

        Exception _Exception;
        [DataMember]
        public Exception Exception
        {
            get { return _Exception; }
            set { _Exception = value; }
        }
    }
}
