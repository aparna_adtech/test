﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ClassLibrary.BLL.Forums
{
    [DataContract]
    public class ForumArticlesResponse
    {
        List<ForumArticle> _ForumArticles;
        [DataMember]
        public List<ForumArticle> ForumArticles
        {
            get { return _ForumArticles; }
            set { _ForumArticles = value; }
        }

        Exception _Exception;
        [DataMember]
        public Exception Exception
        {
            get { return _Exception; }
            set { _Exception = value; }
        }
    }
}
