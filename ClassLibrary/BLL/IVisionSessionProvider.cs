﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public interface IVisionSessionProvider
    {
        Int32 GetCurrentPracticeID();
        Int32 GetCurrentPracticeLocationID();
    }
}
