using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
//using DAL;


namespace BLL
{
	/// <summary>
	/// Summary description for Address
	/// </summary>
	public class Address
	{

		#region Fields

		//private int _addressID;
		private string _addressLine1;
		private string _addressLine2;
		private string _city;
		private string _state;
		private string _zipCode;
		private string _zipCodePlus4;
		//private int _userID;

		#endregion


		#region Properties

		//public int AddressID
		//{
		//    get { return _addressID; }
		//    set { _addressID = value; }
		//}

		public string AddressLine1
		{
			get { return _addressLine1; }
			set { _addressLine1 = value; }
		}

		public string AddressLine2
		{
			get { return _addressLine2; }
			set { _addressLine2 = value; }
		}

		public string City
		{
			get { return _city; }
			set { _city = value; }
		}

		public string State
		{
			get { return _state; }
			set { _state = value; }
		}

		public string ZipCode
		{
			get { return _zipCode; }
			set { _zipCode = value; }
		}

		public string ZipCodePlus4
		{
			get { return _zipCodePlus4; }
			set { _zipCodePlus4 = value; }
		}

		//public int UserID
		//{
		//    get { return _userID; }
		//    set { _userID = value; }
		//}


		#endregion


		#region Constructors

		public Address()
		{
			_addressLine1 = "";
			_addressLine2 = "";
			_city = "";
			_state = "";
			_zipCode = "";
			_zipCodePlus4 = "";
			
		}
		#endregion


		////////////#region Methods

		//////////////#region SelectAll
		//////////////public DataTable SelectAll( )
		//////////////{
		//////////////    DAL.Address dalAddress = new DAL.Address();
		//////////////    DataTable dt = dalAddress.SelectAll( );
		//////////////    return dt;
		//////////////}

		//////////////#endregion


		//////////////#region Select

		//////////////public DataTable Select( int addressID )
		//////////////{
		//////////////    DAL.Address dalAddress = new DAL.Address();
		//////////////    DataTable dt = dalAddress.Select( addressID  );
		//////////////    return dt;
		//////////////}

		//////////////#endregion


		//////////////#region Insert

		//////////////public int Insert (BLL.Address bllAddress)
		//////////////{
		//////////////    int newAddressID;
		//////////////    DAL.Address dalAddress = new DAL.Address();
		//////////////    newAddressID = dalAddress.Insert( bllAddress );
		//////////////    return newAddressID;
		//////////////}

		//////////////#endregion


		//////////////#region Update

		//////////////public void Update( BLL.Address bllAddress)
		//////////////{
		//////////////    DAL.Address dalAddress = new DAL.Address();
		//////////////    dalAddress.Update(bllAddress);
		//////////////}

		//////////////#endregion


		//////////////#region Delete

		//////////////public void Delete( BLL.Address bllAddress)
		//////////////{
		//////////////    DAL.Address dalAddress = new DAL.Address();
		//////////////    dalAddress.Delete(bllAddress);
		//////////////}

		//////////////#endregion	

		//////////////#endregion
	}
}


