using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Common;

using DAL;



namespace BLL.PracticeLocation
{

	public class BillingAddress
	{

		#region Fields

		private int _practiceLocationID;
		private int _addressID;
		private string _attentionOf;
		private int _userID;
		public Address _address;  //BLL.Address

		#endregion


		#region Properties

		public int PracticeLocationID
		{
			get { return _practiceLocationID; }
			set { _practiceLocationID = value; }
		}

		public int AddressID
		{
			get { return _addressID; }
			set { _addressID = value; }
		}

		public string AttentionOf
		{
			get { return _attentionOf; }
			set { _attentionOf = value; }
		}

		public int UserID
		{
			get { return _userID; }
			set { _userID = value; }
		}

		// set Address Class!!!;
		public BLL.Address Address
		{
			get
			{
				return _address;
			}
		}

		public Address getAddress()
		{ return _address; }

		public void setAddress
			(
				String addressLine1
				, String addressLine2
				, String city
				, String state
				, String zipCode
				, String zipCodePlus4 )
		{
			_address.AddressLine1 = addressLine1;
			_address.AddressLine2 = addressLine2;
			_address.City = city;
			_address.State = state;
			_address.ZipCode = zipCode;
			_address.ZipCodePlus4 = zipCodePlus4;
		}

		#endregion


		#region Constructors

		public BillingAddress()
		{
			_address = new Address();
		}
		#endregion


		#region Methods


		#region UpdateInsert

		public void UpdateInsert( BLL.PracticeLocation.BillingAddress bllBillingAddress )
		{
			DAL.PracticeLocation.BillingAddress dalBillingAddress = new DAL.PracticeLocation.BillingAddress();
			dalBillingAddress.UpdateInsert( bllBillingAddress );
		}

		#endregion

		#endregion
	}
}
