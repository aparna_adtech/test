﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BLL.PracticeLocation
{
    public class ShoppingCart
    {
        public static DataSet GetShoppingCart(int practiceLocationID)
        {
            return DAL.PracticeLocation.ShoppingCart.GetShoppingCart(practiceLocationID);
        }
        public static Int32 GetShoppingCartCount(DataSet dsCart)
        {
            //DataSet dsCart = DAL.PracticeLocation.ShoppingCart.GetShoppingCart(practiceLocationID);
            var query = from c in dsCart.Tables[0].AsEnumerable()
                        select c.Field<Int32>("Quantity");
            return query.Sum();

           // return (DAL.PracticeLocation.ShoppingCart.GetShoppingCart(practiceLocationID)).Tables[0].Rows.Count;
        }

        public static Decimal GetShoppingCartTotal(DataSet dsCart)
        {
           // DataSet dsCart = DAL.PracticeLocation.ShoppingCart.GetShoppingCart(practiceLocationID);
            var query = from c in dsCart.Tables[0].AsEnumerable()
                        select c.Field<Decimal>("LineTotal");
            return query.Sum();
        }
    }
}
