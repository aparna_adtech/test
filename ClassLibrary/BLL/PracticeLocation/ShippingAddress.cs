using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Common;


//using DAL;

// jb


namespace BLL.PracticeLocation
{
	public class ShippingAddress
	{

		#region Fields

		private int _practiceLocationID;
		private int _addressID;
		private string _attentionOf;
		private int _userID;
		public Address _address;  //BLL.Address

		#endregion


		#region Properties

		public int PracticeLocationID
		{
			get { return _practiceLocationID; }
			set { _practiceLocationID = value; }
		}

		public int AddressID
		{
			get { return _addressID; }
			set { _addressID = value; }
		}

		public string AttentionOf
		{
			get { return _attentionOf; }
			set { _attentionOf = value; }
		}

		public int UserID
		{
			get { return _userID; }
			set { _userID = value; }
		}

		// set Address Class!!!;
		public BLL.Address Address
		{
			get
			{
				return _address;
			}
		}

		public Address getAddress()
		{ return _address; }

		public void setAddress
			(
				String addressLine1
				, String addressLine2
				, String city
				, String state
				, String zipCode
				, String zipCodePlus4 )
		{
			_address.AddressLine1 = addressLine1;
			_address.AddressLine2 = addressLine2;
			_address.City = city;
			_address.State = state;
			_address.ZipCode = zipCode;
			_address.ZipCodePlus4 = zipCodePlus4;
		}

		#endregion


		#region Constructors

		public ShippingAddress()
		{
			// Add constructor logic here
			_address = new Address();
		}
		#endregion




		#region Methods

		#region SelectAll
		//public DataTable SelectAll( )
		//{
		//    DAL.PracticeLocationShippingAddress dalPracticeLocationShippingAddress = new DAL.PracticeLocationShippingAddress();
		//    DataTable dt = dalPracticeLocationShippingAddress.SelectAll( );
		//    return dt;
		//}

		#endregion


		#region Select

		//public BLL.PracticeLocation.ShippingAddress Select( int practiceLocationID )

		public BLL.PracticeLocation.ShippingAddress Select( BLL.PracticeLocation.ShippingAddress bllShippingAddress )
		{
			DAL.PracticeLocation.ShippingAddress dalPLShippingAddress = new DAL.PracticeLocation.ShippingAddress();
			bllShippingAddress = dalPLShippingAddress.Select( bllShippingAddress );

			return bllShippingAddress;
		}

		#endregion

		#region UpdateInsert

		public void UpdateInsert( BLL.PracticeLocation.ShippingAddress bllShippingAddress )
		{
			DAL.PracticeLocation.ShippingAddress dalShippingAddress = new DAL.PracticeLocation.ShippingAddress();
			dalShippingAddress.UpdateInsert( bllShippingAddress );
		}

		#endregion

		#region Insert

		//public int Insert (BLL.PracticeLocationShippingAddress bllPracticeLocationShippingAddress)
		//{
		//    int newPracticeLocationShippingAddressID;
		//    DAL.PracticeLocationShippingAddress dalPracticeLocationShippingAddress = new DAL.PracticeLocationShippingAddress();
		//    newPracticeLocationShippingAddressID = dalPracticeLocationShippingAddress.Insert( bllPracticeLocationShippingAddress );
		//    return newPracticeLocationShippingAddressID;
		//}

		#endregion


		#region Update

		//public void Update( BLL.PracticeLocationShippingAddress bllPracticeLocationShippingAddress)
		//{
		//    DAL.PracticeLocationShippingAddress dalPracticeLocationShippingAddress = new DAL.PracticeLocationShippingAddress();
		//    dalPracticeLocationShippingAddress.Update(bllPracticeLocationShippingAddress);
		//}

		#endregion


		#region Delete

		//public void Delete( BLL.PracticeLocationShippingAddress bllPracticeLocationShippingAddress )
		//{
		//    DAL.PracticeLocationShippingAddress dalPracticeLocationShippingAddress = new DAL.PracticeLocationShippingAddress();
		//    dalPracticeLocationShippingAddress.Delete( bllPracticeLocationShippingAddress );
		//}

		#endregion

		#endregion
	}
}

