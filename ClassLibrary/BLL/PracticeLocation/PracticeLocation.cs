using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using System.Web;


//using DAL;

// jb uncomment dal

namespace BLL.PracticeLocation
{

	public class PracticeLocation
	{

		#region Fields

		private int _practiceLocationID;
		private int _practiceID;
		private int _addressID;
		private int _contactID;
		private int _timeZoneID;
		private string _name;
		private bool _isPrimaryLocation;
        private string _isPrimaryLocationYesNo;
		private string _emailForOrderApproval;
		private string _emailForLowInventoryAlerts;
		private bool _isEmailForLowInventoryAlertsOn;
		private int _userID;
        private int _sortOrder;
	    private string _generalLedgerNumber;
	    private bool _IsHCPSignatureRequired;

        #endregion


        #region Properties

        public int PracticeLocationID
		{
			get { return _practiceLocationID; }
			set { _practiceLocationID = value; }
		}

		public int PracticeID
		{
			get { return _practiceID; }
			set { _practiceID = value; }
		}

		public int AddressID
		{
			get { return _addressID; }
			set { _addressID = value; }
		}

		public int ContactID
		{
			get { return _contactID; }
			set { _contactID = value; }
		}

		public int TimeZoneID
		{
			get { return _timeZoneID; }
			set { _timeZoneID = value; }
		}

		public string Name
		{
			get { return _name; }
			set { _name = value; }
		}

		public bool IsPrimaryLocation
		{
			get { return _isPrimaryLocation; }
			set { _isPrimaryLocation = value; }
		}
        //IsPrimaryLocationYesNo

		public string IsPrimaryLocationYesNo
		{
			get { return _isPrimaryLocationYesNo; }
			set { _isPrimaryLocationYesNo = value; }
		}
        
        public string EmailForOrderApproval
		{
			get { return _emailForOrderApproval; }
			set { _emailForOrderApproval = value; }
		}

		public string EmailForLowInventoryAlerts
		{
			get { return _emailForLowInventoryAlerts; }
			set { _emailForLowInventoryAlerts = value; }
		}

		public bool IsEmailForLowInventoryAlertsOn
		{
			get { return _isEmailForLowInventoryAlertsOn; }
			set { _isEmailForLowInventoryAlertsOn = value; }
		}

		public int UserID
		{
			get { return _userID; }
			set { _userID = value; }
		}

        public int SortOrder
        {
            get { return _sortOrder; }
            set { _sortOrder = value; }
        }

        public string GeneralLedgerNumber
        {
            get { return _generalLedgerNumber; }
            set { _generalLedgerNumber = value; }
        }

        public bool IsHCPSignatureRequired { 
            get { return _IsHCPSignatureRequired; }
            set { _IsHCPSignatureRequired = value; }
        }

        #endregion


            #region Constructors

        public PracticeLocation()
		{
			// Add constructor logic here
		}
		#endregion


		#region Methods

        #region SelectAccessibleToUser

        public DataTable SelectAccessibleToUser(int practiceID, string UserName)
		{
		    DAL.PracticeLocation.PracticeLocation dalPracticeLocation = new DAL.PracticeLocation.PracticeLocation();
            DataTable dt = dalPracticeLocation.SelectAccessibleToUser(practiceID, UserName);
		    return dt;
		}

		#endregion

		#region SelectAll

		public DataTable SelectAll( int practiceID )
		{
		    DAL.PracticeLocation.PracticeLocation dalPracticeLocation = new DAL.PracticeLocation.PracticeLocation();
		    DataTable dt = dalPracticeLocation.SelectAll( practiceID );
		    return dt;
		}

		#endregion

		#region SelectName

		//public string SelectName( int practiceID )
		//{
		//    DAL.Practice.Practice dalPractice = new DAL.Practice.Practice();
		//    string practiceName = dalPractice.SelectName( practiceID );
		//    return practiceName; 
		//}

		static public string SelectName( int practiceID )
		{
		    // calls static method
		    //DAL.Practice.Practice dalPractice = new DAL.Practice.Practice();
		    string practiceName = BLL.PracticeLocation.PracticeLocation.SelectName( practiceID );
		    return practiceName;
		}

		#endregion

        #region Select User Permissions

        public DataTable SelectUserPermissions(int practiceID, int UserID)
        {
            DAL.PracticeLocation.PracticeLocation dalPracticeLocation = new DAL.PracticeLocation.PracticeLocation();
            DataTable dt = dalPracticeLocation.SelectUserPermissions(practiceID, UserID);
            return dt;
        }

        #endregion

        #region SetUserPermissions

        public bool SetUserPermissions(int practiceLocationID, int UserID, bool AllowAccess)
        {
            DAL.PracticeLocation.PracticeLocation dalPracticeLocation = new DAL.PracticeLocation.PracticeLocation();
            return dalPracticeLocation.SetUserPermissions(practiceLocationID, UserID, AllowAccess);

        }

        #endregion

        //#region Select

		//public DataTable Select( int practiceLocationID )
		//{
		//    DAL.PracticeLocation dalPracticeLocation = new DAL.PracticeLocation();
		//    DataTable dt = dalPracticeLocation.Select( practiceLocationID );
		//    return dt;
		//}

		//#endregion


		#region Insert

		public int Insert( BLL.PracticeLocation.PracticeLocation bllPracticeLocation )
		{
		    int newPracticeLocationID;
		    DAL.PracticeLocation.PracticeLocation dalPracticeLocation = new DAL.PracticeLocation.PracticeLocation();
		    newPracticeLocationID = dalPracticeLocation.Insert( bllPracticeLocation );
            if (BLL.Practice.User.IsVisionLiteUser() && newPracticeLocationID > 0)
            {
                PracticeLocation.PopulateProductInventory(bllPracticeLocation.PracticeID, newPracticeLocationID, 1);
            }
		    return newPracticeLocationID;
		}

		#endregion


		#region Update

		public void Update( BLL.PracticeLocation.PracticeLocation bllPracticeLocation )
		{
		    DAL.PracticeLocation.PracticeLocation dalPracticeLocation = new DAL.PracticeLocation.PracticeLocation();
		    dalPracticeLocation.Update( bllPracticeLocation );
		}

		#endregion


		#region Delete

		public Int32 Delete( BLL.PracticeLocation.PracticeLocation bllPracticeLocation )
		{
		    DAL.PracticeLocation.PracticeLocation dalPracticeLocation = new DAL.PracticeLocation.PracticeLocation();
		    return dalPracticeLocation.Delete( bllPracticeLocation );
		}

		#endregion

		#endregion

        public static DataRow GetSetupPageStatus(string currentPage, int practiceID, int practiceLocationID)
        {
            return DAL.PracticeLocation.PracticeLocation.GetSetupPageStatus(currentPage, practiceID, practiceLocationID);
        }

        public static DataTable GetPracticeLocationSetupStatus(int practiceID)
        {
            DataTable dtPractice =  DAL.PracticeLocation.PracticeLocation.GetPracticeLocationSetupStatus(practiceID);
            return RemoveVisionLitePages(dtPractice);
        }

        private static DataTable RemoveVisionLitePages(DataTable dtPractice)
        {
            if (BLL.Practice.User.IsVisionLiteUser())
            {
                DataRow[] drICD9s = dtPractice.Select("Page='Dispensement Modifications'");
                if (drICD9s.GetUpperBound(0) > -1)
                {
                    dtPractice.Rows.Remove(drICD9s[0]);
                }
            }
            return dtPractice;
        }

        public static void PopulateProductInventory(int practiceId, int practiceLocationId, int userId)
        {
            DAL.PracticeLocation.PracticeLocation.PopulateProductInventory(practiceId, practiceLocationId, userId);
        }
    }
}

