using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;


using DAL;



namespace BLL.PracticeLocation
{
	public class Contact
	{

		#region Fields

		private int _practiceLocationID;
		private int _contactID;
		private string _title;
		private string _salutation;
		private string _firstName;
		private string _middleName;
		private string _lastName;
		private string _suffix;
		private string _emailAddress;
		private string _phoneWork;
		private string _phoneCell;
		private string _phoneHome;
		private string _fax;
		private int _userID;
	    private string _phoneWorkExtension;
	    private bool _isSpecificContact;

	    #endregion


		#region Properties

		public int PracticeLocationID
		{
			get { return _practiceLocationID; }
			set { _practiceLocationID = value; }
		}

		public int ContactID
		{
			get { return _contactID; }
			set { _contactID = value; }
		}

		public string Title
		{
			get { return _title; }
			set { _title = value; }
		}

		public string Salutation
		{
			get { return _salutation; }
			set { _salutation = value; }
		}

		public string FirstName
		{
			get { return _firstName; }
			set { _firstName = value; }
		}

		public string MiddleName
		{
			get { return _middleName; }
			set { _middleName = value; }
		}

		public string LastName
		{
			get { return _lastName; }
			set { _lastName = value; }
		}

		public string Suffix
		{
			get { return _suffix; }
			set { _suffix = value; }
		}

		public string EmailAddress
		{
			get { return _emailAddress; }
			set { _emailAddress = value; }
		}

		public string PhoneWork
		{
			get { return _phoneWork; }
			set { _phoneWork = value; }
		}

        public string PhoneWorkExtension
		{
            get { return _phoneWorkExtension; }
            set { _phoneWorkExtension = value; }
		}

		public string PhoneCell
		{
			get { return _phoneCell; }
			set { _phoneCell = value; }
		}

		public string PhoneHome
		{
			get { return _phoneHome; }
			set { _phoneHome = value; }
		}

		public string Fax
		{
			get { return _fax; }
			set { _fax = value; }
		}

		public int UserID
		{
			get { return _userID; }
			set { _userID = value; }
		}

	    public bool IsSpecificContact
	    {
            get { return _isSpecificContact; }
	        set { _isSpecificContact = value; }
	    }

		#endregion


		#region Constructors

		public Contact()
		{
			
		}
		#endregion


		#region Methods

		//#region SelectAll
		//public DataTable SelectAll( )
		//{
		//    DAL.Contact dalContact = new DAL.Contact();
		//    DataTable dt = dalContact.SelectAll( );
		//    return dt;
		//}

		//#endregion


		#region Select

		public BLL.PracticeLocation.Contact Select( int practiceLocationID )
		{
			BLL.PracticeLocation.Contact bllPLContact = new BLL.PracticeLocation.Contact();
			DAL.PracticeLocation.Contact dalPLContact = new DAL.PracticeLocation.Contact();
			bllPLContact = dalPLContact.Select( practiceLocationID );
			return bllPLContact;
		}

		#endregion


		#region Insert

		public int Insert( BLL.PracticeLocation.Contact bllPLContact )
		{
			int newContactID;
			DAL.PracticeLocation.Contact dalPLContact = new DAL.PracticeLocation.Contact();
			newContactID = dalPLContact.Insert( bllPLContact );
			return newContactID;
		}

		#endregion


		#region Update

		public void Update( BLL.PracticeLocation.Contact bllContact )
		{
			DAL.PracticeLocation.Contact dalContact = new DAL.PracticeLocation.Contact();
			dalContact.Update( bllContact );
		}

		#endregion


		//#region Delete

		//public void Delete( BLL.Contact bllContact)
		//{
		//    DAL.Contact dalContact = new DAL.Contact();
		//    dalContact.Delete(bllContact);
		//}

		//#endregion	

		#endregion
	}
}
