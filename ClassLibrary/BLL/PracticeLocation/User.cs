using System.Data;

namespace BLL.PracticeLocation
{
	public class User
	{
	
		#region Fields
		
		private int _practiceLocationID;
		private int _userUserID;    //  either the CreatedUserID  and  ModifiedUserID
		private int _userID;  //  UserID in the User table as opposed to the common field userID in most tables.
	
		#endregion
		
		
		#region Properties
		
        public int PracticeLocationID
        {
        	get {return _practiceLocationID;}
        	set {_practiceLocationID = value;}
        }
        
        public int UserUserID
        {
            get { return _userUserID; }
            set { _userUserID = value; }
        }
        
        public int UserID
        {
        	get {return _userID;}
        	set {_userID = value;}
        }
        
	
		#endregion
		
		
		#region Constructors
	
		public User()
		{
			// Add constructor logic here
		}
		#endregion
				
				
		#region Methods

            #region SelectAllNames

            public DataTable SelectAllNames(int practiceLocationID )
            {
                DAL.PracticeLocation.User dalPracticeLocationUser = new DAL.PracticeLocation.User();
                DataTable dt = dalPracticeLocationUser.SelectAllNames(  practiceLocationID ); 
                return dt;
            }

            #endregion



            #region SelectAllNonLocationNames

            public DataTable SelectAllNonLocationNames(int practiceLocationID)
            {
                DAL.PracticeLocation.User dalPLU = new DAL.PracticeLocation.User();
                DataTable dt = dalPLU.SelectAllNonLocationNames(practiceLocationID);
                return dt;
            }

            #endregion
    		
            //#region Select
    		
            //public DataTable Select( int practiceLocation_UserID )
            //{
            //    DAL.PracticeLocation.User dalPracticeLocationUser = new DAL.PracticeLocationUser();
            //    DataTable dt = dalPracticeLocationUser.Select( practiceLocationUser.UserID  );
            //    return dt;
            //}
    		
            //#endregion


            #region Insert

            public int Insert(BLL.PracticeLocation.User bllPLU)
            {
                int newPracticeLocation_UserID;
                DAL.PracticeLocation.User dalPLU = new DAL.PracticeLocation.User();
                newPracticeLocation_UserID = dalPLU.Insert(bllPLU);
                return newPracticeLocation_UserID;
            }

            #endregion



            //#region Update

            //public void Update(BLL.PracticeLocation.User bllPLU)
            //{
            //    DAL.PracticeLocation.User dalPracticeLocation_User = new DAL.PracticeLocation.User();
            //    dalPracticeLocation_User.Update(bllPLU);
            //}

            //#endregion


            #region Delete

            public void Delete( BLL.PracticeLocation.User bllPLU )
            {
                DAL.PracticeLocation.User dalPLU = new DAL.PracticeLocation.User();
                dalPLU.Delete(bllPLU);
            }

            #endregion	
						
		#endregion
	}
}
