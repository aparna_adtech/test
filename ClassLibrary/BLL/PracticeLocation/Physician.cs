using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Common;

using DAL;

namespace BLL.PracticeLocation
{
	public class Physician
	{

		#region Fields

		private int _practiceLocationID;
		private int _physicianID;
		private int _contactID;
		//private string _title;
		private string _salutation;
		private string _firstName;
		private string _middleName;
		private string _lastName;
		private string _suffix;
		private int _userID;

		#endregion


		#region Properties

		public int PracticeLocationID
		{
			get { return _practiceLocationID; }
			set { _practiceLocationID = value; }
		}

		public int PhysicianID
		{
			get { return _physicianID; }
			set { _physicianID = value; }
		}

		public int ContactID
		{
			get { return _contactID; }
			set { _contactID = value; }
		}

		public string Salutation
		{
			get { return _salutation; }
			set { _salutation = value; }
		}

		public string FirstName
		{
			get { return _firstName; }
			set { _firstName = value; }
		}

		public string MiddleName
		{
			get { return _middleName; }
			set { _middleName = value; }
		}

		public string LastName
		{
			get { return _lastName; }
			set { _lastName = value; }
		}

		public string Suffix
		{
			get { return _suffix; }
			set { _suffix = value; }
		}

		public int UserID
		{
			get { return _userID; }
			set { _userID = value; }
		}

		#endregion


		#region Constructor

		public Physician()
		{
			// Add constructor logic here
		}

		#endregion




		public DataTable SelectAllNames( int practiceLocationID )
		{
			DAL.PracticeLocation.Physician dalPLP = new DAL.PracticeLocation.Physician();
			DataTable dt = dalPLP.SelectAllNames( practiceLocationID );
			return dt;
		}


		public DataTable SelectAllNonLocationNames( int practiceLocationID )
		{
			DAL.PracticeLocation.Physician dalPLP = new DAL.PracticeLocation.Physician();
			DataTable dt = dalPLP.SelectAllNonLocationNames( practiceLocationID );
			return dt;
		}
		//usp_PracticeLocation_Physician_Select_All_NonLocation_Names_By_PracticeLocationID

		public int Insert( BLL.PracticeLocation.Physician bllPhysician )
		{
			int newPhysicianID;
			DAL.PracticeLocation.Physician dalPLP = new DAL.PracticeLocation.Physician();
			newPhysicianID = dalPLP.Insert( bllPhysician );
			return newPhysicianID;  //return has to be last statement.
		}

		public void Delete( BLL.PracticeLocation.Physician bllPhysician )
		{
			int rowsAffected;
			DAL.PracticeLocation.Physician dalPLP = new DAL.PracticeLocation.Physician();
			rowsAffected = dalPLP.Delete( bllPhysician );
			//return newPhysicianID;  //return has to be last statement.
		}

	}
}
