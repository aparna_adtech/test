﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClassLibrary.DAL;

namespace BLL.PracticeLocation
{
    public class Dispensement
    {
        public static bool GetABNCostType(Int32 practiceID)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                var abnCost = from p in visionDB.Practices
                              where p.PracticeID == practiceID
                              select p.ABNCostIsCash;
                return abnCost.FirstOrDefault();
            }
        }
    }
}
