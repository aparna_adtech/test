using System;
using System.Data;
using System.Configuration;
using DAL.Practice;

namespace ClassLibrary.BLL.PracticeLocation
{
	public class EntityDocumentMapping
	{
		public int EntityDocumentMappingID { get; set; }
		public int EntityTypeID { get; set; }
        public int EntityID { get; set; }
        public int DocumentTypeID { get; set; }
        public int DocumentID { get; set; }
        public int DocumentSourceID { get; set; }
        public int CreatedUserID { get; set; }
        public DateTime CreatedDate { get; set; }

		public int Create(int entityTypeId, int entityId, int documentTypeId, int documentId, int documentSourceId, int userId)
		{
            this.EntityTypeID = EntityTypeID;
            this.EntityID = EntityID;
            this.DocumentTypeID = DocumentTypeID;
            this.DocumentID = DocumentID;
            this.DocumentSourceID = DocumentSourceID;
            this.CreatedUserID = userId;

            DAL.PracticeLocation.EntityDocumentMapping entity = new DAL.PracticeLocation.EntityDocumentMapping();

			int newId = entity.Add(this);
			return newId;
		}
	}
}

