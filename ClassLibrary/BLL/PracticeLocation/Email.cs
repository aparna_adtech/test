using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using DAL;
using ClassLibrary.DAL;
using System.Linq;

namespace BLL.PracticeLocation
{
	public class Email
	{
	
		#region Fields
		
		private int _practiceLocationID;
		private string _emailForOrderApproval;
		private string _emailForOrderConfirmation;
		private string _emailForFaxConfirmation;
        private string _emailForBillingDispense;
		private string _emailForLowInventoryAlerts;
		private bool _isEmailForLowInventoryAlertsOn;
		private int _userID;
	
		#endregion
		
		
		#region Properties
		
        public int PracticeLocationID
        {
        	get {return _practiceLocationID;}
        	set {_practiceLocationID = value;}
        }
        
        public string EmailForOrderApproval
        {
        	get {return _emailForOrderApproval;}
        	set {_emailForOrderApproval = value;}
        }
        
        public string EmailForOrderConfirmation
        {
        	get {return _emailForOrderConfirmation;}
        	set {_emailForOrderConfirmation = value;}
        }
        
        public string EmailForFaxConfirmation
        {
        	get {return _emailForFaxConfirmation;}
        	set {_emailForFaxConfirmation = value;}
        }

        //EmailForBillingDispense
        public string EmailForBillingDispense
        {
            get { return _emailForBillingDispense; }
            set { _emailForBillingDispense = value; }
        }

        public string EmailForPrintDispense { get; set; }

        public string FaxForPrintDispense { get; set; }

        public string EmailForLowInventoryAlerts
        {
        	get {return _emailForLowInventoryAlerts;}
        	set {_emailForLowInventoryAlerts = value;}
        }
        
        public bool IsEmailForLowInventoryAlertsOn
        {
        	get {return _isEmailForLowInventoryAlertsOn;}
        	set {_isEmailForLowInventoryAlertsOn = value;}
        }
        
        public int UserID
        {
        	get {return _userID;}
        	set {_userID = value;}
        }
        
	
		#endregion
		
		
		#region Constructors
	
		public Email()
		{
			// Add constructor logic here
		}
		#endregion
				
				
		#region Methods

        //  Not Applicable
        //#region SelectAll
        //public DataTable SelectAll( )
        //{
        //    DAL.PracticeLocation dalPracticeLocation = new DAL.PracticeLocation();
        //    DataTable dt = dalPracticeLocation.SelectAll( );
        //    return dt;
        //}

        //#endregion



        #region Select

        public BLL.PracticeLocation.Email SelectOne(BLL.PracticeLocation.Email bllPLE)
        {
            DAL.PracticeLocation.Email dalPracticeLocationEmail = new DAL.PracticeLocation.Email();
            bllPLE = dalPracticeLocationEmail.SelectOne(bllPLE);
            return bllPLE;
        }

        #endregion

        #region Billing Email

        public static string GetBillingEmail(int PracticeLocationID)
        {
            BLL.PracticeLocation.Email bllPLEmail = new BLL.PracticeLocation.Email();
            bllPLEmail.PracticeLocationID = PracticeLocationID;
            bllPLEmail = bllPLEmail.SelectOne(bllPLEmail);
            string MailTo = bllPLEmail.EmailForBillingDispense; //"Michael_Sneen@Yahoo.com";
            //if (string.IsNullOrEmpty(MailTo.Trim()))
            //{
            //    MailTo = GetCentralizedBillingEmail(PracticeLocationID);
            //}

            return MailTo;
        }

        public static string GetCentralizedBillingEmail(Int32 PracticeLocationID)
        {
            VisionDataContext visionDB = null;
            visionDB = VisionDataContext.GetVisionDataContext();

            var practiceEmail = from plCurrent in visionDB.PracticeLocations
                                join p in visionDB.Practices on plCurrent.PracticeID equals p.PracticeID
                                join plAll in visionDB.PracticeLocations on p.PracticeID equals plAll.PracticeID
                                where plCurrent.PracticeLocationID == PracticeLocationID
                                && plAll.IsPrimaryLocation == true
                                select plAll.EmailForBillingDispense;

            return practiceEmail.SingleOrDefault();
        }

        #endregion

        //  Not Applicable
        //#region Insert
		
        //public int Insert (BLL.PracticeLocation bllPracticeLocation)
        //{
        //    int newPracticeLocationID;
        //    DAL.PracticeLocation dalPracticeLocation = new DAL.PracticeLocation();
        //    newPracticeLocationID = dalPracticeLocation.Insert( bllPracticeLocation );
        //    return newPracticeLocationID;
        //}
		
        //#endregion
		

        #region Update

        public void Update(BLL.PracticeLocation.Email bllPLE)
        {
            DAL.PracticeLocation.Email  dalPracticeLocation = new DAL.PracticeLocation.Email();
            dalPracticeLocation.Update(bllPLE);
        }

        #endregion
		
		//  Not applicable yet
        //#region Delete
		
        //public void Delete( BLL.PracticeLocation bllPracticeLocation)
        //{
        //    DAL.PracticeLocation dalPracticeLocation = new DAL.PracticeLocation();
        //    dalPracticeLocation.Delete(bllPracticeLocation);
        //}
		
        //#endregion	
						
		#endregion
	}
}



