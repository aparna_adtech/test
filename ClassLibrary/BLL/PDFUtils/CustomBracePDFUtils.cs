﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using System.ComponentModel;

namespace BLL.PDFUtils
{
	public static class CustomBracePDFUtils
	{
		public static byte[] BuildPDF(int practiceID, int practiceLocationID, int bregVisionOrderID, int shoppingCartID, int imageDPI, bool flatten, string displayType, int page)
		{
			byte[] pdfContents = null;
			DataSet dsMappedPDFInfo = DAL.PracticeLocation.Inventory.GetMappedPDFInfo(bregVisionOrderID == 0, bregVisionOrderID == 0 ? null : (int?)bregVisionOrderID);
			int mappedPDFID;
			DataTable mappedPDFFields = GetMappedPDFFields(dsMappedPDFInfo, out mappedPDFID);
			if (dsMappedPDFInfo.Tables.Count >= 1 && dsMappedPDFInfo.Tables[0].Rows.Count > 0)
			{
				DataRow mappedPDFRow = dsMappedPDFInfo.Tables[0].Rows[0];
				string pdfFile = mappedPDFRow["PDFURL"].ToString();
				string jsFile = mappedPDFRow["JSURL"].ToString().Trim();
				int buttonPage = ToInt(mappedPDFRow["SubmitButtonPage"]);
				int renderDPI = ToInt(mappedPDFRow["RenderDPI"]);
				float buttonX = ToFloat(mappedPDFRow["SubmitButtonUpperLeftX"]);
				float buttonY = ToFloat(mappedPDFRow["SubmitButtonUpperLeftY"]);
				float buttonWidth = ToFloat(mappedPDFRow["SubmitButtonWidth"]);
				float buttonHeight = ToFloat(mappedPDFRow["SubmitButtonHeight"]);
				string buttonText = mappedPDFRow["SubmitButtonText"] == DBNull.Value ? null : mappedPDFRow["SubmitButtonText"].ToString();
				string buttonImageFile = mappedPDFRow["SubmitButtonImageFile"] == DBNull.Value ? null : mappedPDFRow["SubmitButtonImageFile"].ToString();
				if (buttonImageFile != null)
					buttonImageFile = HttpContext.Current.Request.MapPath(buttonImageFile);
				string pdfPath = Path.Combine(HttpContext.Current.Server.MapPath(pdfFile));

				Dictionary<string, string> fdf = PopulateFDF(practiceID, practiceLocationID, bregVisionOrderID, shoppingCartID, mappedPDFFields);

				string redirectURL = null;
				string userAgent = HttpContext.Current.Request.UserAgent;
				if (userAgent != null && userAgent.IndexOf("Edge") == -1 && userAgent.IndexOf("MSIE") == -1 && userAgent.IndexOf(") like Gecko") == -1)
				{
					redirectURL = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + VirtualPathUtility.ToAbsolute("~/InvoiceSnapshot.aspx");
				}

				if (imageDPI == 0)
				{
					if (renderDPI == 0)
						imageDPI = 300;
					else
						imageDPI = renderDPI;
				}

				if (jsFile != string.Empty)
					jsFile = HttpContext.Current.Request.MapPath(jsFile);

				pdfContents = PDFHelper.GenerateWithSubmitButton(pdfPath, fdf,
					buttonPage, buttonX, buttonY, buttonWidth, buttonHeight, "SubmitButton", buttonText, buttonImageFile,
					HttpContext.Current.Request.Url.AbsoluteUri + "&submit=", flatten, displayType, imageDPI, page, redirectURL, jsFile);
				//HttpContext.Current.Request.MapPath("~/UserControls/DispensePage/CustomBracePDFHandler.js"));
			}
			return pdfContents;
		}

		public static DataTable GetMappedPDFFields(DataSet dsMappedPDFInfo, out int mappedPDFID)
		{
			mappedPDFID = 0;
			if (dsMappedPDFInfo.Tables.Count >= 1 && dsMappedPDFInfo.Tables[0].Rows.Count > 0)
			{
				DataTable mappedPDF = dsMappedPDFInfo.Tables[0];
				object id = mappedPDF.Rows[0]["MappedPDFID"];
				if (id != null && id != DBNull.Value)
					mappedPDFID = (int)id;
			}
			if (dsMappedPDFInfo.Tables.Count >= 2 && dsMappedPDFInfo.Tables[0].Rows.Count > 0)
			{
				DataTable mappedPDFFields = dsMappedPDFInfo.Tables[1];
				return mappedPDFFields;
			}
			return null;
		}
		private enum GetCustomBraceInfoIDType
		{
			BregVisionOrderID,
			ShoppingCartID
		}

		private static Dictionary<string, string> GetCustomBraceInfo(int brace_id, GetCustomBraceInfoIDType id_type, DataTable mappedPDFFields)
		{
			DataSet dsCustomBraceInfo = null;
			if (id_type == GetCustomBraceInfoIDType.BregVisionOrderID)
				dsCustomBraceInfo = DAL.PracticeLocation.Inventory.GetCustomBraceExtendedInfoFromOrder(null, brace_id);
			else
				dsCustomBraceInfo = DAL.PracticeLocation.Inventory.GetCustomBraceExtendedInfoFromOrder(brace_id);

			if (dsCustomBraceInfo.Tables[0].Rows.Count == 1)
			{
				DataRow drCustomBraceInfo = dsCustomBraceInfo.Tables[0].Rows[0];
				Dictionary<string, string> fdf = new Dictionary<string, string>();
				object fdfJson = drCustomBraceInfo["FDF"];
				if (fdfJson == DBNull.Value)
				{
					fdf = CreateDefaultFDF(mappedPDFFields);
				}
				else
				{
					fdf = JSONToDictionary(fdfJson);
				}
				SetFormFields(mappedPDFFields, fdf, drCustomBraceInfo, true, false);
				return fdf;
			}
			else
			{
				Dictionary<string, string> fdf = CreateDefaultFDF(mappedPDFFields);
				return fdf;
			}
		}

		private static Dictionary<string, string> GetProductInfo(int practiceLocationID, int shoppingCartID, DataTable mappedPDFFields)
		{
			Dictionary<string, string> fdf = new Dictionary<string, string>();
			var dsProduct = DAL.PracticeLocation.Inventory.GetShoppingCart(practiceLocationID);
			var dtProduct = dsProduct.Tables[0];
			fdf = GetCustomBraceInfo(shoppingCartID, GetCustomBraceInfoIDType.ShoppingCartID, mappedPDFFields);
			if (dtProduct.Columns.Contains("Code"))
			{
				foreach (DataRow row in dtProduct.Rows)
				{
					string code = row["Code"].ToString();
					SetFormField(mappedPDFFields, fdf, null, "On", true, true, code);
				}
			}
			return fdf;
		}

		private static Dictionary<string, string> GetProductInfoByOrder(int practiceLocationID, int order_id, DataTable mappedPDFFields)
		{
			Dictionary<string, string> fdf = GetCustomBraceInfo(order_id, GetCustomBraceInfoIDType.BregVisionOrderID, mappedPDFFields);
			DataSet dsProduct = null;
			dsProduct = DAL.PracticeLocation.Inventory.GetCustomBraceProductInfoFromOrder(order_id);

			var dtProduct = dsProduct.Tables[0];
			if (dtProduct.Columns.Contains("Code"))
			{
				foreach (DataRow row in dtProduct.Rows)
				{
					string code = row["Code"].ToString();
					SetFormField(mappedPDFFields, fdf, null, "On", true, true, code);
				}
			}
			return fdf;
		}

		private static void GetBregAccountCode(int practiceLocationID, DataTable mappedPDFFields, Dictionary<string, string> fdf)
		{
			DAL.PracticeLocation.SupplierCode dalPLSupplierCode = new DAL.PracticeLocation.SupplierCode();
			DataTable dt = dalPLSupplierCode.SelectAll(practiceLocationID);
			DataRow[] rows = dt.Select("SupplierShortName='Breg'");
			if (rows.GetUpperBound(0) > -1)
			{
				DataRow bregRow = rows[0];
				string bregCode = bregRow["AccountCode"].ToString();
				SetFormField(mappedPDFFields, fdf, "Billing_CustomerNumber", bregCode, true, true);
			}
		}

		public static Dictionary<string, string> PopulateFDF(int practiceID, int practiceLocationID, int bregVisionOrderID, int shoppingCartID, DataTable mappedPDFFields)
		{
			Dictionary<string, string> fdf = new Dictionary<string, string>();

			if (bregVisionOrderID != 0)
				fdf = GetProductInfoByOrder(practiceLocationID, bregVisionOrderID, mappedPDFFields);
			else
				fdf = GetProductInfo(practiceLocationID, shoppingCartID, mappedPDFFields);

			PopulateBillingAddress(practiceID, practiceLocationID, mappedPDFFields, fdf);
			PopulateShippingAddress(practiceLocationID, mappedPDFFields, fdf);
			PopulateContactTelephoneNumber(practiceLocationID, mappedPDFFields, fdf);
			GetBregAccountCode(practiceLocationID, mappedPDFFields, fdf);

			return fdf;
		}

		private static void SetFormFields(DataTable mappedPDFFields, Dictionary<string, string> fdf, IDataReader reader, bool overwiteFDF, bool useGetName)
		{
			for (int i = 0; i < reader.FieldCount; i++)
			{
				SetFormField(mappedPDFFields, fdf, reader.GetName(i), reader[i], overwiteFDF, useGetName);
			}
		}

		private static void SetFormFields(DataTable mappedPDFFields, Dictionary<string, string> fdf, DataRow row, bool overwriteFDF, bool useGetName)
		{
			foreach (DataColumn column in row.Table.Columns)
			{
				SetFormField(mappedPDFFields, fdf, column.ColumnName, row[column], overwriteFDF, useGetName);
			}
		}

		private static Dictionary<string, string> CreateDefaultFDF(DataTable mappedPDFFields)
		{
			Dictionary<string, string> fdf = new Dictionary<string, string>();
			foreach (DataRow row in mappedPDFFields.Rows)
			{
				string pdfFieldPath = row["PDFFieldPath"].ToString();
				object dbFieldDefault = row["DBFieldDefault"];
				if (dbFieldDefault != DBNull.Value)
				{
					object dbFieldValue = row["DBFieldValue"];
					if (dbFieldValue != DBNull.Value)
					{
						bool selected = dbFieldValue.ToString() == dbFieldDefault.ToString();
						fdf[pdfFieldPath] = selected ? "On" : "Off";
					}
					else if (!fdf.ContainsKey(pdfFieldPath))
					{
						fdf[pdfFieldPath] = dbFieldDefault.ToString();
					}
				}
			}
			return fdf;
		}

		private static void SetFormField(DataTable mappedPDFFields, Dictionary<string, string> fdf, string name, object value, bool overwriteFDF, bool useGetName, string mcpCode = null)
		{
			if (name != null)
				name = name.ToUpper();

			string val = value == null ? string.Empty : value.ToString();
			foreach (DataRow row in mappedPDFFields.Rows)
			{
				string dbFieldName = row["DBFieldNameGet"].ToString().ToUpper();
				if (!useGetName)
					dbFieldName = row["DBFieldNameSet"].ToString().ToUpper();
				string dbFieldCode = row["DBFieldCode"].ToString().ToUpper();
				string pdfFieldPath = row["PDFFieldPath"].ToString();

				bool codeMatches = false;
				if (mcpCode != null && dbFieldCode != string.Empty)
				{
					Regex regex = new Regex(dbFieldCode, RegexOptions.IgnoreCase);
					codeMatches = regex.IsMatch(mcpCode);
				}

				if (pdfFieldPath != string.Empty && ((name != null && dbFieldName == name) || codeMatches))
				{
					if (overwriteFDF || !fdf.ContainsKey(pdfFieldPath))
					{
						object dbFieldValue = row["DBFieldValue"];
						fdf[pdfFieldPath] = string.Empty;

						if (dbFieldValue == DBNull.Value)
						{
							if (value != null && value != DBNull.Value)
								fdf[pdfFieldPath] = val;
						}
						else if (value != DBNull.Value)
						{
							if (val == dbFieldValue.ToString())
								fdf[pdfFieldPath] = "On";
							else
								fdf[pdfFieldPath] = "Off";
						}
					}
				}
			}
		}

		private static void PopulateBillingAddress(int practiceID, int practiceLocationID, DataTable mappedPDFFields, Dictionary<string, string> fdf)
		{
			int addressID;
			bool isBillingCentralized;
			DAL.Practice.BillingAddress dalPBA = new DAL.Practice.BillingAddress();
			dalPBA.GetPracticeBillingAddressID(practiceID, out addressID, out isBillingCentralized);
			if (isBillingCentralized)
			{
				string attentionOf;
				string addressLine1;
				string addressLine2;
				string city;
				string state;
				string zipCode;
				string zipCodePlus4;
				dalPBA.SelectOneByPractice(practiceID, out attentionOf, out addressID, out addressLine1, out addressLine2, out city, out state, out zipCode, out zipCodePlus4);
				SetFormField(mappedPDFFields, fdf, "BillAttentionOf", attentionOf, false, true);
				SetFormField(mappedPDFFields, fdf, "BillAddress1", addressLine1, false, true);
				SetFormField(mappedPDFFields, fdf, "BillAddress2", addressLine2, false, true);
				SetFormField(mappedPDFFields, fdf, "BillCity", city, false, true);
				SetFormField(mappedPDFFields, fdf, "BillState", state, false, true);
				SetFormField(mappedPDFFields, fdf, "BillZipCode", zipCode, false, true);
			}
			else
			{
				Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
				DbCommand dbCommand = db.GetStoredProcCommand("dbo.USP_GetBillingAddressByPracticeLocation");
				db.AddInParameter(dbCommand, "PracticeLocationID", DbType.String, practiceLocationID);
				IDataReader reader = db.ExecuteReader(dbCommand);
				if (reader.Read())
				{
					SetFormFields(mappedPDFFields, fdf, reader, false, true);
				}
			}
		}

		private static void PopulateContactTelephoneNumber(int practiceLocationID, DataTable mappedPDFFields, Dictionary<string, string> fdf)
		{
			BLL.PracticeLocation.Contact bllPLContact = new BLL.PracticeLocation.Contact();
			bllPLContact = bllPLContact.Select(practiceLocationID);

			if (bllPLContact.ContactID > 0)   //Always true;
			{
				SetFormField(mappedPDFFields, fdf, "Billing_Phone", bllPLContact.PhoneWork, true, false);
			}
		}

		private static void PopulateShippingAddress(int practiceLocationID, DataTable mappedPDFFields, Dictionary<string, string> fdf)
		{
			Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
			DbCommand dbCommand = db.GetStoredProcCommand("dbo.USP_GetShippingAddressByPracticeLocation");
			db.AddInParameter(dbCommand, "PracticeLocationID", DbType.String, practiceLocationID.ToString());
			IDataReader reader = db.ExecuteReader(dbCommand);

			if (HttpContext.Current != null)
			{
				var shippingTypeID = HttpContext.Current.Session["ShippingTypeID"];
				SetFormField(mappedPDFFields, fdf, "ShippingTypeID", shippingTypeID, false, true);
			}

			if (reader.Read())
			{
				SetFormFields(mappedPDFFields, fdf, reader, false, true);
			}
		}

		public static void SavePDFData(int practiceLocationID, int shoppingCartID, Dictionary<string, string> fdf, bool flatten)
		{
			DataSet dsMappedPDFInfo = DAL.PracticeLocation.Inventory.GetMappedPDFInfo(true);
			SaveCustomBraceInfo(practiceLocationID, shoppingCartID, fdf, dsMappedPDFInfo, flatten);

			// SaveCustomBraceInfo sets the session variable for ShippingTypeID based on the
			// data entered in the form in either the fusion or x2k; so, we just pick it up
			// here and update/override the shopping cart shipping type so that this selection
			// overrides the value from before.
			if (HttpContext.Current != null)
			{
				var shippingType = ToInt(HttpContext.Current.Session["ShippingTypeID"].ToString());
				DAL.PracticeLocation.ShoppingCart.UpdateShippingType(practiceLocationID, shippingType);
			}
		}

		private static void SaveCustomBraceInfo(int practiceLocationID, int shoppingCartID,
			Dictionary<string, string> fdf, DataSet dsMappedPDFInfo, bool flatten)
		{
			// NOTE: make sure we don't save anything if we're in read-only mode
			if (!flatten)
			{
				int mappedPDFID;
				DataTable mappedPDFFields = GetMappedPDFFields(dsMappedPDFInfo, out mappedPDFID);

				Dictionary<string, string> fdfToSave = new Dictionary<string, string>();
				foreach (string key in fdf.Keys)
				{
					bool hasCode = false;
					foreach (DataRow row in mappedPDFFields.Rows)
					{
						string pdfFieldPath = row["PDFFieldPath"].ToString();
						if (pdfFieldPath == key)
						{
							string dbFieldCode = row["DBFieldCode"].ToString();
							if (dbFieldCode != string.Empty)
								hasCode = true;
							break;
						}
					}
					if (!hasCode)
						fdfToSave.Add(key, fdf[key]);
				}

				Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
				DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_CreateCustomBraceOrderNew");

				FillInCommandParameters(db, dbCommand, mappedPDFFields, fdf);
				db.AddParameter(dbCommand, "@return_value", DbType.Int32, ParameterDirection.ReturnValue, null, DataRowVersion.Default, null);

				dbCommand.Parameters["@MappedPDFID"].Value = mappedPDFID;
				dbCommand.Parameters["@ShoppingCartID"].Value = shoppingCartID;
				dbCommand.Parameters["@PhysicianID"].Value = 0;
				dbCommand.Parameters["@FDF"].Value = DictionaryToJSON(fdfToSave);

				if (HttpContext.Current != null)
				{
					HttpContext.Current.Session["ShippingTypeID"] = dbCommand.Parameters["@ShippingTypeID"].Value;
				}
				//string str = GetGeneratedQuery(dbCommand);

				try
				{
					db.ExecuteNonQuery(dbCommand);

					string poNumber = string.Empty;
					if (dbCommand.Parameters.Contains("@PONumber"))
						poNumber = dbCommand.Parameters["@PONumber"].Value.ToString().Trim();
					UpdateShoppingCartCustomPOCode(practiceLocationID, poNumber);
				}
				catch (Exception ex)
				{
					throw new ApplicationException("Custom brace insert failed. Error was:\n\n" + ex.Message + ".\n\n" + ex.StackTrace);
				}

				var return_value = ToInt(db.GetParameterValue(dbCommand, "@return_value"));
				if (return_value != 0)
					throw new ApplicationException("Custom brace insert failed. Return value was: " + return_value.ToString() + ".");
			}
		}

		private static string GetCustomPONumber(int practiceLocationID)
		{
			try
			{
				var db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
				var cmd = db.GetStoredProcCommand("usp_ShoppingCart_Select_CustomPurchaseOrderCodeByPracticeLocationID");
				db.AddInParameter(cmd, "PracticeLocationID", DbType.Int32, practiceLocationID);
				return db.ExecuteScalar(cmd).ToString();
			}
			catch { }
			return null;
		}

		private static void UpdateShoppingCartCustomPOCode(int practiceLocationID, string customPONumber)
		{
			// strip leading/trailing white space and replace double-space with single space
			var custPONum = Regex.Replace(customPONumber.Trim(), @"\s+", " ");

			var db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
			var cmd = db.GetStoredProcCommand("usp_ShoppingCart_Update_CustomPurchaseOrderCodeByPracticeLocationID");
			db.AddInParameter(cmd, "PracticeLocationID", DbType.Int32, practiceLocationID);
			db.AddInParameter(cmd, "CustomPurchaseOrderCode", DbType.String, string.IsNullOrEmpty(custPONum) ? (object)DBNull.Value : custPONum);
			db.ExecuteNonQuery(cmd);
		}

		private static string GetGeneratedQuery(DbCommand dbCommand)
		{
			var query = dbCommand.CommandText;
			string prefix = " ";
			foreach (DbParameter parameter in dbCommand.Parameters.Cast<DbParameter>().ToArray())
			{
				if (parameter.ParameterName != "@FDF" && parameter.ParameterName != "@RETURN_VALUE")
				{
					string value = parameter.Value.ToString();
					switch (parameter.DbType)
					{
						case DbType.AnsiString:
						case DbType.AnsiStringFixedLength:
						case DbType.Date:
						case DbType.DateTime:
						case DbType.DateTime2:
						case DbType.DateTimeOffset:
						case DbType.Guid:
						case DbType.String:
						case DbType.StringFixedLength:
						case DbType.Xml:
							value = "'" + value + "'";
							break;
					}
					query = query + prefix + parameter.ParameterName + "=" + value;
					prefix = ", ";
				}
			}
			return query;
		}

		private static void FillInCommandParameters(Database db, DbCommand dbCommand, DataTable mappedPDFFields, Dictionary<string, string> fdf)
		{
			db.DiscoverParameters(dbCommand);
			if (dbCommand.Parameters.Contains("@RETURN_VALUE"))
			{
				dbCommand.Parameters.Remove(dbCommand.Parameters["@RETURN_VALUE"]);
			}

			foreach (DbParameter parameter in dbCommand.Parameters.Cast<DbParameter>().ToArray())
			{
				string fdfValue = string.Empty;
				string dbFieldDefault = string.Empty;
				bool dbFieldRequired = false;
				string paramName = parameter.ParameterName.Replace("@", "").ToUpper();
				foreach (DataRow row in mappedPDFFields.Rows)
				{
					string dbFieldNameSet = row["DBFieldNameSet"].ToString().ToUpper();
					if (dbFieldNameSet == paramName)
					{
						string pdfFieldPath = row["PDFFieldPath"].ToString();
						object dbFieldValue = row["DBFieldValue"];
						bool dbFieldRequiredTemp = (bool)row["DBFieldRequired"];
						dbFieldRequired |= dbFieldRequiredTemp;
						if (dbFieldRequiredTemp)
							dbFieldDefault = row["DBFieldDefault"].ToString();

						if (fdf.ContainsKey(pdfFieldPath))
						{
							string fdfValueTemp = fdf[pdfFieldPath];
							if (fdfValueTemp == "/Male")
							{
								fdfValue = "Male";
								break;
							}
							else if (fdfValueTemp == "/Female")
							{
								fdfValue = "Female";
								break;
							}
							else if (dbFieldValue != DBNull.Value && (fdfValueTemp == "/On" || fdfValueTemp == "On"))
							{
								fdfValue = dbFieldValue.ToString();
								break;
							}
							else if (dbFieldValue != DBNull.Value && (fdfValueTemp == "/Off" || fdfValueTemp == "Off"))
							{
								continue;
							}
							else
							{
								fdfValue = fdfValueTemp;
								break;
							}
						}
					}
				}
				if ((dbFieldRequired && fdfValue.Trim() == string.Empty) || !ValidateParameter(fdfValue.ToString(), GetClrType(parameter.DbType)))
					fdfValue = dbFieldDefault;

				dbCommand.Parameters["@" + paramName].Value = fdfValue;
			}
		}

		private static string DictionaryToJSON(Dictionary<string, string> dict)
		{
			string json = string.Empty;
			try
			{
				json = JsonConvert.SerializeObject(dict, Formatting.None,
					new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
			}
			catch (Exception ex)
			{

				Console.WriteLine(ex);
			}
			return json;
		}

		private static Dictionary<string, string> JSONToDictionary(object json)
		{
			Dictionary<string, string> dict = null;
			if (json != null && json != DBNull.Value)
			{
				try
				{
					JObject jobject = (JObject)JsonConvert.DeserializeObject(json.ToString());
					dict = jobject.ToObject<Dictionary<string, string>>();
				}
				catch (Exception ex)
				{

				}
			}
			if (dict == null)
				dict = new Dictionary<string, string>();
			return dict;
		}

		public static string GetString(Dictionary<string, string> fdf, string key)
		{
			if (fdf.ContainsKey(key))
			{
				return fdf[key];
			}
			return string.Empty;
		}

		public static int GetInt(Dictionary<string, string> fdf, string key)
		{
			string value = GetString(fdf, key);
			int result = 0;
			Int32.TryParse(value, out result);
			return result;
		}

		public static int ToInt(object value)
		{
			int intValue = 0;
			if (value != null)
				Int32.TryParse(value.ToString(), out intValue);
			return intValue;
		}

		public static float ToFloat(object value)
		{
			float floatValue = 0;
			if (value != null)
				float.TryParse(value.ToString(), out floatValue);
			return floatValue;
		}

		public static string ToString(object value)
		{
			if (value != null)
				return value.ToString();
			return string.Empty;
		}

		public static bool ToBool(object value)
		{
			bool boolValue = false;
			if (value != null)
				Boolean.TryParse(value.ToString(), out boolValue);
			return boolValue;
		}

		private static bool ValidateParameter(string value, Type type)
		{
			object result;
			try
			{
				var converter = TypeDescriptor.GetConverter(type);
				result = converter.ConvertFromString(value);
			}
			catch (Exception exception)
			{
				return false;
			}
			return true;
		}

		private static Type GetClrType(DbType type)
		{
			switch (type)
			{
				case DbType.Int64:
				case DbType.UInt64:
					return typeof(long?);

				case DbType.Binary:
				case DbType.Byte:
					return typeof(byte[]);

				case DbType.Boolean:
					return typeof(bool?);

				case DbType.AnsiString:
				case DbType.AnsiStringFixedLength:
				case DbType.String:
				case DbType.StringFixedLength:
				case DbType.Xml:
					return typeof(string);

				case DbType.Date:
				case DbType.Time:
				case DbType.DateTime:
				case DbType.DateTime2:
					return typeof(DateTime?);

				case DbType.Decimal:
					return typeof(decimal?);

				case DbType.Double:
					return typeof(double?);

				case DbType.Int32:
				case DbType.UInt32:
					return typeof(int?);

				case DbType.Guid:
					return typeof(Guid?);

				case DbType.Int16:
				case DbType.UInt16:
					return typeof(short?);

				case DbType.SByte:
					return typeof(byte?);

				case DbType.VarNumeric:
					return typeof(object);

				case DbType.DateTimeOffset:
					return typeof(DateTimeOffset?);

				default:
					throw new ArgumentOutOfRangeException("sqlType");
			}
		}
		public static string RootURL
		{
			get
			{
				var _with1 = System.Web.HttpContext.Current.Request;
				string appPath = _with1.ApplicationPath.Trim();
				if (appPath == "/")
					appPath = string.Empty;
				return _with1.Url.Scheme + "://" + _with1.Url.Host + appPath;
			}
		}
	}
}