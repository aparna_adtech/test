﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Text;
using Ghostscript.NET;
using Ghostscript.NET.Rasterizer;
using System.Reflection;

namespace BLL.PDFUtils
{
	public class PDFHelper
	{
		public static Dictionary<string, string> GetFormFieldNames(string pdfPath)
		{
			var fields = new Dictionary<string, string>();

			var reader = new PdfReader(pdfPath);
			foreach (var entry in reader.AcroFields.Fields)
				fields.Add(entry.Key.ToString(), string.Empty);
			reader.Close();

			return fields;
		}

		public static byte[] GeneratePDF(string pdfPath, Dictionary<string, string> formFieldMap)
		{
			var output = new MemoryStream();
			var reader = new PdfReader(pdfPath);
			var stamper = new PdfStamper(reader, output);
			var formFields = stamper.AcroFields;

			foreach (var fieldName in formFieldMap.Keys)
				formFields.SetField(fieldName, formFieldMap[fieldName]);

			stamper.FormFlattening = true;
			stamper.Close();
			reader.Close();

			return output.ToArray();
		}

		// See http://stackoverflow.com/questions/4491156/get-the-export-value-of-a-checkbox-using-itextsharp/
		public static string GetExportValue(AcroFields.Item item)
		{
			var valueDict = item.GetValue(0);
			var appearanceDict = valueDict.GetAsDict(PdfName.AP);

			if (appearanceDict != null)
			{
				var normalAppearances = appearanceDict.GetAsDict(PdfName.N);
				// /D is for the "down" appearances.

				// if there are normal appearances, one key will be "Off", and the other
				// will be the export value... there should only be two.
				if (normalAppearances != null)
				{
					foreach (var curKey in normalAppearances.Keys)
						if (!PdfName.OFF.Equals(curKey))
							return curKey.ToString().Substring(1); // string will have a leading '/' character, so remove it!
				}
			}

			// if that doesn't work, there might be an /AS key, whose value is a name with 
			// the export value, again with a leading '/', so remove it!
			var curVal = valueDict.GetAsName(PdfName.AS);
			if (curVal != null)
				return curVal.ToString().Substring(1);
			else
				return string.Empty;
		}

		public static void ReturnPDF(byte[] contents)
		{
			ReturnPDF(contents, null);
		}

		public static void ReturnPDF(byte[] contents, string attachmentFilename)
		{
			var response = HttpContext.Current.Response;

			if (!string.IsNullOrEmpty(attachmentFilename))
				response.AddHeader("Content-Disposition", "inline; filename=" + attachmentFilename);

			response.ContentType = "application/pdf";
			response.BinaryWrite(contents);
			response.End();
		}

		public static byte[] GenerateWithSubmitButton(string inputFile, Dictionary<string, string> formFieldMap,
			int buttonPage, float buttonX, float buttonY, float buttonWidth, float buttonHeight, string buttonName, string buttonText, string buttonImageFile,
			string submitURL, bool flatten, string displayType, int imageDpi, int imagePage, string redirectURL, string javascriptFile)
		{
			bool generateImage = (displayType == "JPG" || displayType == "PNG");
			byte[] document = null;
			using (MemoryStream ms = new MemoryStream())
			{
				using (PdfReader reader = new PdfReader(inputFile))
				{
					using (PdfStamper stamper = new PdfStamper(reader, ms, '\0', !flatten))
					{
						stamper.FormFlattening = flatten;

						var formFields = stamper.AcroFields;
						foreach (var fieldName in formFieldMap.Keys)
						{
							string value = formFieldMap[fieldName];
							if (value == "/On" || value == "/Off")
								value = value.Substring(1);
							formFields.SetField(fieldName, value);
						}

						if (!flatten)
						{
							Image image = null;
							if (buttonImageFile != null)
							{
								try
								{
									image = Image.GetInstance(buttonImageFile);
									if (buttonWidth == 0)
										buttonWidth = image.Width;
									if (buttonHeight == 0)
										buttonHeight = image.Height;
									if (buttonWidth != image.Width)
										buttonHeight = (image.Height / image.Width) * buttonWidth;
								}
								catch (Exception ex)
								{
								}
							}

							int pageStart = 1;
							int pageEnd = stamper.Reader.NumberOfPages;
							if (buttonPage != 0)
							{
								pageStart = buttonPage;
								pageEnd = buttonPage;
							}
							for (int page = pageStart; page <= pageEnd; page++)
							{
								Rectangle pageRect = stamper.Reader.GetPageSize(page);
								if (buttonX < 0)
									buttonX = pageRect.Width + buttonX - buttonWidth;
								if (buttonX < 0)
									buttonX = 0;
								if (buttonX + buttonWidth > pageRect.Width)
									buttonX = pageRect.Width - buttonWidth;

								if (buttonY < 0)
									buttonY = pageRect.Height + buttonY - buttonHeight;
								if (buttonY < 0)
									buttonY = 0;
								if (buttonY + buttonHeight > pageRect.Height)
									buttonY = pageRect.Height - buttonHeight;

								Rectangle buttonPosition = new Rectangle(buttonX, buttonY, buttonX + buttonWidth, buttonY + buttonHeight);
								PushbuttonField buttonField = new PushbuttonField(stamper.Writer, buttonPosition, buttonName);
								buttonField.Visibility = PushbuttonField.VISIBLE_BUT_DOES_NOT_PRINT;

								if (buttonText != null)
									buttonField.Text = buttonText;

								if (image != null)
								{
									buttonField.Layout = PushbuttonField.LAYOUT_ICON_ONLY;
									buttonField.ProportionalIcon = true;
									buttonField.Image = image;
								}
								else
								{
									buttonField.BackgroundColor = BaseColor.DARK_GRAY;
									buttonField.BorderColor = GrayColor.BLUE;
									buttonField.BorderWidth = 1f;
									buttonField.BorderStyle = PdfBorderDictionary.STYLE_BEVELED;
									buttonField.TextColor = GrayColor.WHITE;
									buttonField.FontSize = 10f;
								}

								PdfFormField submit = buttonField.Field;
								int flags = PdfAction.SUBMIT_XFDF | PdfAction.SUBMIT_INCLUDE_NO_VALUE_FIELDS;
								PdfAction action = PdfAction.CreateSubmitForm(submitURL, null, flags);
								submit.Action = action;

								if (redirectURL != null)
									action.Next(new PdfAction(redirectURL));

								if (javascriptFile != null && javascriptFile != string.Empty)
								{
									string js = Utilities.ReadFileToString(javascriptFile);
									var jAction = PdfAction.JavaScript(js, stamper.Writer);
									stamper.Writer.AddJavaScript(jAction);
								}

								stamper.AddAnnotation(submit, page);
							}
						}
						stamper.Close();
					}
				}

				document = ms.ToArray();
				if (generateImage)
				{
					using (MemoryStream pdfMS = new MemoryStream(document))
					{
						MemoryStream imageMS = null;
						ms.Flush();
						PDFToImage(displayType, pdfMS, out imageMS, imagePage, imageDpi);
						if (imageMS != null)
						{
							document = imageMS.ToArray();
							imageMS.Close();
							imageMS.Dispose();
						}
					}
				}

				return document;
			}
		}

		public static void InsertHTMLIntoPDF(string inputPDF, string outputPDF, string html, int insertPage)
		{
			bool inputPDFExists = !string.IsNullOrEmpty(inputPDF) && File.Exists(inputPDF);

			// Write out the html as a PDF
			using (MemoryStream ms = new MemoryStream())
			{
				Rectangle pageSize = PageSize.LETTER;
				if (inputPDFExists)
				{
					using (var pageSizeReader = new PdfReader(inputPDF))
					{
						pageSize = pageSizeReader.GetPageSize(1);
						pageSizeReader.Close();
					}
				}

				TextReader reader = new StringReader(html);
				Document document = new Document(pageSize, 30, 30, 30, 30);
				PdfWriter writer = PdfWriter.GetInstance(document, ms);
				HTMLWorker worker = new HTMLWorker(document);
				document.Open();
				worker.StartDocument();
				worker.Parse(reader);
				worker.EndDocument();
				worker.Close();
				document.Close();

				using (FileStream pdfFile = File.OpenWrite(outputPDF))
				{
					byte[] pdfData = ms.ToArray();
					pdfFile.Write(pdfData, 0, pdfData.Length);
					pdfFile.Close();
				}
			}

			// Append the html PDF to the input PDF and write it out as the outputPDF
			if (inputPDFExists)
			{
				using (var output = new MemoryStream())
				{
					var document = new Document();
					var writer = new PdfCopy(document, output);
					document.Open();
					foreach (var file in new[] { outputPDF, inputPDF })
					{
						using (var reader = new PdfReader(file))
						{
							int n = reader.NumberOfPages;
							PdfImportedPage page;
							for (int p = 1; p <= n; p++)
							{
								page = writer.GetImportedPage(reader, p);
								writer.AddPage(page);
							}
							reader.Close();
						}
					}
					document.Close();
					File.WriteAllBytes(outputPDF, output.ToArray());
				}
			}
		}

		private static void PDFToImage(string displayType, MemoryStream pdfMS, out MemoryStream imageMS, int page, int dpi)
		{
			GhostscriptRasterizer rasterizer = null;
			GhostscriptVersionInfo version = new GhostscriptVersionInfo(new System.Version(0, 0, 0), GhostscriptDLLPath, string.Empty, GhostscriptLicense.GPL);
			using (rasterizer = new GhostscriptRasterizer())
			{
				rasterizer.Open(pdfMS, version, true);

				using (imageMS = new MemoryStream())
				{
					System.Drawing.Bitmap fullImage = null;
					System.Drawing.Graphics canvas = null;

					int startPage = page;
					if (startPage <= 0)
						startPage = 1;
					int endPage = rasterizer.PageCount;
					if (startPage > endPage)
						startPage = endPage;
					if (page != 0)
						endPage = startPage;

					int pageOffset = 0;
					for (; startPage <= endPage; startPage++, pageOffset++)
					{
						System.Drawing.Image pageImage = rasterizer.GetPage(dpi, dpi, startPage);
						if (canvas == null)
						{
							fullImage = new System.Drawing.Bitmap(pageImage.Width, pageImage.Height * (endPage - startPage + 1));
							canvas = System.Drawing.Graphics.FromImage(fullImage);
						}
						canvas.DrawImage(pageImage, new System.Drawing.Point(0, pageOffset * pageImage.Height));
					}
					System.Drawing.Imaging.ImageFormat format = System.Drawing.Imaging.ImageFormat.Jpeg;
					if (displayType == "PNG")
						format = System.Drawing.Imaging.ImageFormat.Png;
					fullImage.Save(imageMS, format);
					if (canvas != null)
						canvas.Dispose();
					if (fullImage != null)
						fullImage.Dispose();
				}
				rasterizer.Close();
			}
		}

		private static string GhostscriptDLLPath
		{
			get
			{
				string binPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", ""));
				string gsDllPath = Path.Combine(binPath, Environment.Is64BitProcess ? "gsdll64.dll" : "gsdll32.dll");
				return gsDllPath;
			}
		}

		public static Dictionary<string, string> ParseFdf(Stream fdfStream)
		{
			Dictionary<string, string> fdfValues = new Dictionary<string, string>();

			var data = new byte[4];
			fdfStream.Read(data, 0, 4);
			fdfStream.Seek(0, SeekOrigin.Begin);
			var fdfType = Encoding.Default.GetString(data);

			if (fdfType == "%FDF")
			{
				FdfReader reader = new FdfReader(fdfStream);
				IDictionary<string, PdfDictionary> fields = reader.Fields;
				foreach (string key in fields.Keys)
				{
					PdfDictionary field = fields[key];
					foreach (PdfName fieldKey in field.Keys)
					{
						object value = field.GetAsString(fieldKey);
						if (value == null)
							value = field.GetAsName(fieldKey);

						if (value != null)
						{
							fdfValues.Add(key, value.ToString());
						}
						break;
					}
				}
			}
			else
			{
				XfdfReader reader = new XfdfReader(fdfStream);
				fdfValues = reader.Fields;
			}
			return fdfValues;
		}
	}
}