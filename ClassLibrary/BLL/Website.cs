﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Configuration;
namespace BLL
{
    public class Website
    {
        public static bool IsVisionExpress()
        {
            //this is for code merge.  Examine the url to determine if it is Vision or Express.
            string visionExpressCriteria = ConfigurationManager.AppSettings["VisionExpressCriteria"];
            string url = HttpContext.Current.Request.Url.OriginalString.ToLower();

            if (url.Contains("localhost"))
            {
                #if(IsExpress)
                    return true;
                #else
                    return false;
                #endif
            }
            else if (url.Contains(visionExpressCriteria))
            {
                return true;
            }

            return false;

        }

        public static Uri GetBaseUri()
        {
            
            Uri currentUri = HttpContext.Current.Request.Url;
            return new Uri(currentUri.GetLeftPart(UriPartial.Authority)); 
        }

        public static Uri GetOppositeBaseUri()
        {
            //this is for code merge.  Examine the url to determine if it is Vision or Express.
            string visionUrlForThisServer = ConfigurationManager.AppSettings["VisionUrlForThisServer"];
            string expressUrlForThisServer = ConfigurationManager.AppSettings["ExpressUrlForThisServer"];

            string visionExpressCriteria = ConfigurationManager.AppSettings["VisionExpressCriteria"];
            string url = HttpContext.Current.Request.Url.OriginalString.ToLower();

            if (url.Contains("localhost"))
            {
#if(IsExpress)
                return new Uri("http://localhost");
#else
                return new Uri("http://localhost");
#endif
            }
            else if (url.Contains(visionExpressCriteria))
            {
                return new Uri(visionUrlForThisServer);
            }

            return new Uri(expressUrlForThisServer);
        }

        public static string GetOppositeSiteLoginUrl()
        {
            string oppositeSiteRedirectUrl = "";
            string defaultLoginPage = "~/default.aspx";
            Uri baseUri = GetOppositeBaseUri();

            System.Web.UI.Control control = new System.Web.UI.Control();
            string defaultPageUrl = control.ResolveUrl(defaultLoginPage);
            Uri correctSite = new Uri(baseUri, defaultPageUrl);
            oppositeSiteRedirectUrl = correctSite.AbsoluteUri.ToString();
            return oppositeSiteRedirectUrl;
        }

        public static string GetAdminPath(bool isVisionExpress)
        {
            if (isVisionExpress)
            {
                return GetAbsPath("~/Admin/PracticeSetup/PracticeSetup.aspx");
            }
            else
            {
                return GetAbsPath("~/Admin/PracticeLocation_All.aspx");
            }
        }

        public static string GetAbsPath(string imagePath)
        {
            System.Web.UI.Control control = new System.Web.UI.Control();
            string virtualPath = control.ResolveUrl(imagePath);
            Uri uriPath = new Uri(HttpContext.Current.Request.Url, virtualPath);
            return uriPath.AbsoluteUri;
        }
    }
}
