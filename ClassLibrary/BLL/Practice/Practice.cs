using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using System.Web;

using DAL;
//using DAL;  // jb

namespace BLL.Practice
{
	public class Practice
    {



        #region Fields

        private int _practiceID;
		private string _practiceName;
		private int _userID;
        private bool _isOrthoSelect;
        private bool _isVisionLite;

		#endregion

		//#region Constants

		//private const string PRACTICEID = "PracticeID";
		//private const string PRACTICENAME = "PracticeName";
		//private const string USERID = "UserID";

		//#endregion

		#region Properties
		public int PracticeID
		{
			get { return _practiceID; }
			set { _practiceID = value; }
		}

		public string PracticeName
		{
			get { return _practiceName; }
			set { _practiceName = value; }
		}
        public bool IsOrthoSelect
        {
            get { return _isOrthoSelect; }
            set { _isOrthoSelect = value; }
        }
        public bool IsVisionLite
        {
            get { return _isVisionLite; }
            set { _isVisionLite = value; }
        }

		public int UserID
		{
			get { return _userID; }
			set { _userID = value; }
		}

		public bool CameraScanEnabled { get; set; }

		public bool IsBregBilling { get; set; }

		public bool FaxDispenseReceiptEnabled { get; set; }

        public bool EMRIntegrationPullEnabled { get; set; }
        
        public bool IsCustomFitEnabled { get; set; }

        public bool SecureMessagingInternalEnabled { get; set; }

        public bool SecureMessagingExternalEnabled { get; set; }

        public BLL.Practice.PaymentType.PaymentTypeEnum PaymentType { get; set; }

        public DateTime BillingStartDate { get; set; }

        public string PracticeCode { get; set; }

		#endregion

		#region Constructors
		public Practice()
		{
			// Add constructor logic here
		}
		#endregion



//jb uncomment methods
		#region Methods

		#region Insert

		public int Insert( BLL.Practice.Practice bllPractice )
		{
			int newPracticeID;
			DAL.Practice.Practice dalPractice = new DAL.Practice.Practice();
			newPracticeID = dalPractice.Insert( bllPractice );
			return newPracticeID;
		}
		#endregion

		#region Update
		public void Update( BLL.Practice.Practice bllPractice )
		{
			DAL.Practice.Practice dalPractice = new DAL.Practice.Practice();
			dalPractice.Update( bllPractice );
		}
		#endregion

		#region Delete
		public void Delete( BLL.Practice.Practice bllPractice )
		{
			DAL.Practice.Practice dalPractice = new DAL.Practice.Practice();
			dalPractice.Delete( bllPractice );
		}
		#endregion

        #region Select
        public static ClassLibrary.DAL.Practice Select(int practiceID)
        {
            return DAL.Practice.Practice.Select(practiceID);
        }
        #endregion

        #region SelectName

        //-------------------added by u= 09/03/2013
        static public bool SelectFaxDispenseEnabled(int practiceID)
        {
            bool bFaxDispensedEnabled = DAL.Practice.Practice.SelectFaxDispense(practiceID);
            return bFaxDispensedEnabled;
        }       
        //-----------------


        //public string SelectName( int practiceID )
		//{
		//    DAL.Practice.Practice dalPractice = new DAL.Practice.Practice();
		//    string practiceName = dalPractice.SelectName( practiceID ); 
		//    return practiceName; 
		//}

		static public string SelectName( int practiceID )
		{
			//calls static method.
			//DAL.Practice.Practice dalPractice = new DAL.Practice.Practice();
			string practiceName = DAL.Practice.Practice.SelectName( practiceID );
			return practiceName;
		}

		#endregion

		#region GetAllPractices
		public DataSet GetAllPractices()
		{
			DAL.Practice.Practice dalPractice = new DAL.Practice.Practice();
			DataSet ds = new DataSet();
			ds = dalPractice.SelectAll();
			return ds;
		}

		public DataSet GetPractice()
		{
			DAL.Practice.Practice dalPractice = new DAL.Practice.Practice();
			DataSet ds = new DataSet();
			ds = dalPractice.SelectAll();
			return ds;
		}
		#endregion


        public static bool IsVisionLitePractice(int practiceID)
        {
            ClassLibrary.DAL.Practice practice = Practice.Select(practiceID);
            if (practice != null)
            {
                return practice.IsVisionLite;
            }
            throw new ApplicationException(string.Format("The Practice with Practice ID {0} was not found", practiceID));
        }

        public static DataRow GetSetupPageStatus(string currentPage, int practiceID)
        {
            return DAL.Practice.Practice.GetSetupPageStatus(currentPage, practiceID);
        }


        public static DataTable GetPracticeSetupStatus(int practiceID)
        {
            DataTable dtPractice =  DAL.Practice.Practice.GetPracticeSetupStatus(practiceID);
            return RemoveVisionLitePages(dtPractice);

        }

        public static bool IsSetupComplete(int practiceID)
        {
            DataTable practiceStatus = GetPracticeSetupStatus(practiceID);
            DataTable practiceLocationStatus = BLL.PracticeLocation.PracticeLocation.GetPracticeLocationSetupStatus(practiceID);
           

            int practiceQuery = practiceStatus.AsEnumerable().Count(p => p.Field<string>("PageStatus") != "Y");

            int practiceLocationQuery = 0;
            foreach (DataColumn column in practiceLocationStatus.Columns)
            {
                if (column.ColumnName != "Page")
                {
                    int practiceLocationCount = practiceLocationStatus.AsEnumerable().Count(p => p.Field<string>(column.ColumnName) != "Y");
                    practiceLocationQuery += practiceLocationCount;
                }
            }
            if ((practiceQuery + practiceLocationQuery) < 1)
            {
                return true;
            }
            return false;


        }

        private static DataTable RemoveVisionLitePages(DataTable dtPractice)
        {
            //if (BLL.Practice.User.IsVisionLiteUser())
            //{
                DataRow[] drICD9s = dtPractice.Select("Page='ICD-9s'");
                if (drICD9s.GetUpperBound(0) > -1)
                {
                    dtPractice.Rows.Remove(drICD9s[0]);
                }
            //}
            return dtPractice;
        }

        public static bool CreateMinimumPracticeInfo(ref ClassLibrary.DAL.PracticeApplication practiceApplication, BLL.Practice.PaymentType.PaymentTypeEnum paymentType)
        {
            // NOTE: this method is here to maintain compatibility with the old 3.0 BregVision site
            return CreateMinimumPracticeInfo(ref practiceApplication, paymentType, true);
        }

        public static bool CreateMinimumPracticeInfo(ref ClassLibrary.DAL.PracticeApplication practiceApplication, BLL.Practice.PaymentType.PaymentTypeEnum paymentType, bool populateInventory)
        {

            string email = practiceApplication.EmailAddress;
            //string password = ConfigurationManager.AppSettings["VisionLiteInitialPassword"];
            string password = practiceApplication.Password;
            string userName = practiceApplication.UserName;

            //create their account
            int practiceID = CreatePractice(practiceApplication, paymentType);
            if (practiceID > 0)
            {
                practiceApplication.IsPracticeCreated = true;
                practiceApplication.PracticeID = practiceID;
                DAL.Practice.PracticeApplication.UpdatePracticeApplication(practiceApplication);

                int practiceLocationID = CreatePracticeLocation(practiceID, populateInventory);

                if (practiceLocationID > 0)
                {

                    int userId = BLL.Practice.User.CreateVisionLiteUser(ref userName, password, practiceID, email, practiceLocationID);

                    if (userId > 0)
                    {
                        practiceApplication.UserName = userName;
                        practiceApplication.IsAdminUserCreated = true;

                        DAL.Practice.PracticeApplication.UpdatePracticeApplication(practiceApplication);

                        return true;
                    }
                }
            }
            return false;

        }

        /// <summary>
        /// This should only be used to Create the Initial Default PracticeLocation of a VisionLitePractice
        /// </summary>
        /// <param name="practiceID"></param>
        /// <returns></returns>
        private static int CreatePracticeLocation(int practiceID, bool populateInventory)
        {
            int practiceLocationID = 0;
            BLL.PracticeLocation.PracticeLocation practiceLocation = new BLL.PracticeLocation.PracticeLocation();
            practiceLocation.Name = "Default Location";
            practiceLocation.PracticeID = practiceID;
            practiceLocation.IsPrimaryLocation = true;
            practiceLocationID =  practiceLocation.Insert(practiceLocation);
            if (practiceLocationID > 0 && populateInventory == true)
            {
                PracticeLocation.PracticeLocation.PopulateProductInventory(practiceID, practiceLocationID, 1);
            }
            return practiceLocationID;
        }

        /// <summary>
        /// This should only be used to create VisionLite practices
        /// </summary>
        /// <param name="practiceApplication"></param>
        /// <returns></returns>
        private static int CreatePractice(ClassLibrary.DAL.PracticeApplication practiceApplication, BLL.Practice.PaymentType.PaymentTypeEnum paymentType)
        {

            int practiceID = 0;
            BLL.Practice.Practice practice = new BLL.Practice.Practice();
            practice.PracticeName = practiceApplication.PracticeName;
            practice.IsOrthoSelect = false;
            practice.IsVisionLite = true;
            practice.PaymentType = paymentType;
            practiceID = practice.Insert(practice);
            if (practiceID > 0)
            {
                //Populate the PracticeCatalog with Breg Products
                PopulateCatalog(practiceID, 1);
            }
            return practiceID;
        }

        public static void PopulateCatalog(int practiceId, int userId)
        {
            DAL.Practice.Practice.PopulateCatalog(practiceId, userId);
        }

        #region PracticeFees
        public static List<ClassLibrary.DAL.BregVisionMonthlyFeeLevel> GetExpressPracticeFees()
        {
            return DAL.Practice.Practice.GetExpressPracticeFees();
        }

        public static void SetPracticeFee(Int32 practiceID, Int32 bregVisionMonthlyFeeLevelID, DateTime startDate, Decimal discount)
        {
            DAL.Practice.Practice.SetPracticeFee(practiceID, bregVisionMonthlyFeeLevelID, startDate, discount);
        }
        #endregion


        #endregion

        #region practice catalog products
        public bool CheckLateralityExemptions(List<int> practiceCatalogProductIds)
        {
            if (practiceCatalogProductIds.Count>0)
                return DAL.Practice.Practice.CheckLateralityExemptions(practiceCatalogProductIds);
            return false;
        }
        #endregion
    }
}



