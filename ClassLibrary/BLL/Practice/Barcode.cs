﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using DAL.Practice;  

namespace BLL.Practice
{
    public class Barcode
    {
        public static DataSet GetPracticeCatalogDataBySupplier1(int PracticeID, string SearchText, string SearchCriteria)
        {
            return DAL.Practice.Barcode.GetPracticeCatalogDataBySupplier1(SearchCriteria, SearchText, PracticeID);
        }
    }
}
