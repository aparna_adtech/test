using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Common;

using DAL;
using System.Web;

namespace BLL.Practice
{
    public class User
    {
        #region Fields

        private int _practiceID;
        private int _userUserID;  // UserId from the user table as opposed to the aspnetdb tables.
        private int _contactID;
        //private string _title;
        private string _salutation;
        private string _firstName;
        private string _middleName;
        private string _lastName;
        private string _suffix;
        private int _userID;

        #endregion


        #region Properties

        public int PracticeID
        {
            get { return _practiceID; }
            set { _practiceID = value; }
        }

        public int PhysicianID
        {
            get { return _userUserID; }
            set { _userUserID = value; }
        }

        public int ContactID
        {
            get { return _contactID; }
            set { _contactID = value; }
        }

        public string Salutation
        {
            get { return _salutation; }
            set { _salutation = value; }
        }

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        public string MiddleName
        {
            get { return _middleName; }
            set { _middleName = value; }
        }

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        public string Suffix
        {
            get { return _suffix; }
            set { _suffix = value; }
        }

        /// <value>0</value>
        public int UserID
        {
            get { return _userID; }
            set { _userID = value; }
            //set { _userID = 0; }
        }

        #endregion


        public User()
        {
            // Add constructor logic here
        }

        public static bool IsVisionLiteUser()
        {
            if (HttpContext.Current.User.IsInRole("VisionLite"))
            {
                return true;
            }
            return false;
        }

        public bool IsAllowedZeroQtyDispense(string UserName)
        {
            //DAL.Practice.User dalPU = new DAL.Practice.User();
            //return dalPU.IsAllowedZeroQtyDispense(UserName);
            
            //NOTE: always return true to maintain backwards compatibility
            return true;
        }

        public DataTable SelectUser(string UserName)
        {
            DAL.Practice.User dalPU = new DAL.Practice.User();
            DataTable dt = dalPU.SelectUser(UserName);
            return dt;
        }

        public DataTable SelectUser(string UserName, int PracticeID)
        {
            DAL.Practice.User dalPU = new DAL.Practice.User();
            DataTable dt = dalPU.SelectUser(UserName, PracticeID);
            return dt;
        }


        public DataTable SelectAllUserFullNames(int practiceID)
        {
            DAL.Practice.User dalPU = new DAL.Practice.User();
            DataTable dt = dalPU.SelectAllUserFullNames(practiceID);
            return dt;
        }

        public DataTable SelectAllUserLogins(int practiceID)
        {
            DAL.Practice.User dalPU = new DAL.Practice.User();
            DataTable dt = dalPU.SelectAllUserLogins(practiceID);
            return dt;
        }

        /// <summary>
        /// This method is here to provide backwards compatibility with 3.0.
        /// </summary>
        //public bool UpdateDefaultPreferences(
        //    int PracticeID, int UserID, bool ShowProductInfo, bool AllowDispense, bool AllowInventory,
        //    bool AllowCheckIn, bool AllowReports, bool AllowCart, bool AllowDispenseModification,
        //    int DefaultLocationID, int DefaultRecordsPerPage, string DefaultDispenseSearchCriteria,
        //    bool AllowZeroQtyDispense)
        //{
        //    return UpdateDefaultPreferences(PracticeID, UserID, ShowProductInfo, AllowDispense, AllowInventory, AllowCheckIn, AllowReports, AllowCart, AllowDispenseModification, DefaultLocationID, DefaultRecordsPerPage, DefaultDispenseSearchCriteria, AllowZeroQtyDispense, false, false, false);
        //}

        public bool UpdateDefaultPreferences(
                                                int PracticeID, 
                                                int UserID, 
                                                bool ShowProductInfo,  
                                                bool AllowDispense, 
                                                bool AllowInventory, 
                                                bool AllowCheckIn, 
                                                bool AllowReports, 
                                                bool AllowCart, 
                                                bool AllowDispenseModification,
                                                int DefaultLocationID, 
                                                int DefaultRecordsPerPage, 
                                                string DefaultDispenseSearchCriteria,
                                                bool ScrollEntirePage = false,  // Begin Optional params
                                                bool DispenseQueueSortDescending = false,
                                                bool AllowPracticeUserOrderWithoutApproval = false, 
                                                bool allowInventoryClose = false)
        {
            var dalPU = new DAL.Practice.User();
            return dalPU.UpdateDefaultPreferences(
                                                    PracticeID, 
                                                    UserID, 
                                                    ShowProductInfo, 
                                                    AllowDispense, 
                                                    AllowInventory, 
                                                    AllowCheckIn,
                                                    AllowReports, 
                                                    AllowCart, 
                                                    AllowDispenseModification, 
                                                    DefaultLocationID, 
                                                    DefaultRecordsPerPage,
                                                    DefaultDispenseSearchCriteria, 
                                                    ScrollEntirePage,
                                                    DispenseQueueSortDescending, 
                                                    AllowPracticeUserOrderWithoutApproval, 
                                                    allowInventoryClose);
        }

        public int InsertUser(int practiceID, string UserName, int CreatedUserID)
        {
            DAL.Practice.User dalPU = new DAL.Practice.User();
            return dalPU.InsertUser(practiceID, UserName, CreatedUserID);
        }

        public int DeleteUser(int practiceID, int UserID)
        {
             DAL.Practice.User dalPU = new DAL.Practice.User();
             return dalPU.DeleteUser(practiceID, UserID);
        }

        public int ActivateUser(int practiceID, int UserID)
        {
            DAL.Practice.User dalPU = new DAL.Practice.User();
            return dalPU.ActivateUser(practiceID, UserID);
        }

        public static int CreateVisionLiteUser(ref string userName, string password, int practiceID, string email, int practiceLocationID)
        {
            
            return DAL.Practice.User.CreateVisionLiteUser(ref userName, password, practiceID, email, practiceLocationID, userName);
        }

    }

}

