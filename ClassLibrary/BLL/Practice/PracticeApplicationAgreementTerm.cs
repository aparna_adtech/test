﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Practice
{
    public class PracticeApplicationAgreementTerm
    {
        public static ClassLibrary.DAL.PracticeApplicationAgreementTerm GetTopPracticeApplicationAgreementTerm()
        {
            return DAL.Practice.PracticeApplicationAgreementTerm.GetTopPracticeApplicationAgreementTerm();
        }

        public static void SavePracticeApplicationAgreementTerm(ClassLibrary.DAL.PracticeApplicationAgreementTerm practiceApplicationAgreementTerm)
        {
            DAL.Practice.PracticeApplicationAgreementTerm.GetTopPracticeApplicationAgreementTerm(practiceApplicationAgreementTerm);
        }

    }
}
