using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;

using DAL;
//using DAL;


namespace BLL.Practice
{
	/// <summary>
	/// Summary description for ThirdPartySupplier
	/// </summary>
	public class ThirdPartySupplier
	{
	
		#region Fields
		
		private int _thirdPartySupplierID;
		//private int _addressID;
		private int _practiceID;
		private string _supplierName;
		private string _supplierShortName;
		private string _faxOrderPlacement;
		private string _emailOrderPlacement;
		private bool _isEmailOrderPlacement;
		private bool _isVendor;
		private int _sequence;
		private int _userID;
	
		#endregion
		
		
		#region Properties
		
        public int ThirdPartySupplierID
        {
        	get {return _thirdPartySupplierID;}
        	set {_thirdPartySupplierID = value;}
        }
        
		//public int AddressID
		//{
		//    get {return _addressID;}
		//    set {_addressID = value;}
		//}
        
        public int PracticeID
        {
        	get {return _practiceID;}
        	set {_practiceID = value;}
        }
        
        public string SupplierName
        {
        	get {return _supplierName;}
        	set {_supplierName = value;}
        }
        
        public string SupplierShortName
        {
        	get {return _supplierShortName;}
        	set {_supplierShortName = value;}
        }
        
        public string FaxOrderPlacement
        {
        	get {return _faxOrderPlacement;}
        	set {_faxOrderPlacement = value;}
        }
        
        public string EmailOrderPlacement
        {
        	get {return _emailOrderPlacement;}
        	set {_emailOrderPlacement = value;}
        }
        
        public bool IsEmailOrderPlacement
        {
        	get {return _isEmailOrderPlacement;}
        	set {_isEmailOrderPlacement = value;}
        }
        
        public bool IsVendor
        {
        	get {return _isVendor;}
        	set {_isVendor = value;}
        }
        
        public int Sequence
        {
        	get {return _sequence;}
        	set {_sequence = value;}
        }
        
        public int UserID
        {
        	get {return _userID;}
        	set {_userID = value;}
        }
        
	
		#endregion
		
		
		#region Constructors
	
		public ThirdPartySupplier()
		{
			// Add constructor logic here
		}
		#endregion
				
//jb only commented select all				
		#region Methods

		#region SelectAll
		public DataTable SelectAll( int practiceID )
		{
			DAL.Practice.ThirdPartySupplier dalThirdPartySupplier = new DAL.Practice.ThirdPartySupplier();
			DataTable dt = dalThirdPartySupplier.SelectAll( practiceID );

			//dt.Columns.Remove("ThirdPartySupplierID");
			//dt.Columns.Remove( "PracticeID" );
			//dt.Columns.Remove( "ShortName" );
			//dt.Columns.Remove( "Contact" );
			return dt;
		}

		#endregion


		//#region Select

		//public DataTable Select( int thirdPartySupplierID )
		//{
		//    DAL.Practice.ThirdPartySupplier dalThirdPartySupplier = new DAL.Practice.ThirdPartySupplier();
		//    DataTable dt = dalThirdPartySupplier.Select( thirdPartySupplierID );
		//    return dt;
		//}

		//#endregion


		//#region Insert

		//public int Insert( BLL.Practice.ThirdPartySupplier bllThirdPartySupplier )
		//{
		//    int newThirdPartySupplierID;
		//    DAL.Practice.ThirdPartySupplier dalThirdPartySupplier = new DAL.Practice.ThirdPartySupplier();
		//    newThirdPartySupplierID = dalThirdPartySupplier.Insert( bllThirdPartySupplier );
		//    return newThirdPartySupplierID;
		//}

		//#endregion


		//#region Update

		//public void Update( BLL.Practice.ThirdPartySupplier bllThirdPartySupplier )
		//{
		//    DAL.Practice.ThirdPartySupplier dalThirdPartySupplier = new DAL.Practice.ThirdPartySupplier();
		//    dalThirdPartySupplier.Update( bllThirdPartySupplier );
		//}

		//#endregion


		//#region Delete

		//public void Delete( BLL.Practice.ThirdPartySupplier bllThirdPartySupplier )
		//{
		//    DAL.Practice.ThirdPartySupplier dalThirdPartySupplier = new DAL.Practice.ThirdPartySupplier();
		//    dalThirdPartySupplier.Delete( bllThirdPartySupplier );
		//}

		//#endregion

		#endregion
	}
}
