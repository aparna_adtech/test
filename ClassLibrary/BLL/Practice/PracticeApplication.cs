﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Practice
{
    public class PracticeApplication
    {
        public static ClassLibrary.DAL.PracticeApplication SavePracticeApplication(ClassLibrary.DAL.PracticeApplication practiceApplication)
        {
            return DAL.Practice.PracticeApplication.SavePracticeApplication(practiceApplication);
        }

        public static void UpdatePracticeApplication(ClassLibrary.DAL.PracticeApplication practiceApplication)
        {
            DAL.Practice.PracticeApplication.UpdatePracticeApplication(practiceApplication);
        }

        public static ClassLibrary.DAL.PracticeApplication GetPracticeApplication(int practiceApplicationID)
        {
            return DAL.Practice.PracticeApplication.GetPracticeApplication(practiceApplicationID);
        }

        public static string GetBregAccountNumber(int practiceId)
        {
            return DAL.Practice.PracticeApplication.GetBregAccountNumber(practiceId);
        }

        public static List<ClassLibrary.DAL.PracticeApplication> GetPracticeApplications()
        {
            return DAL.Practice.PracticeApplication.GetPracticeApplications();
        }

    }
}
