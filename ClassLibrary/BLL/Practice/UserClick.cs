﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Common;

using DAL;

namespace BLL.Practice
{
    public class UserClick
    {
         public UserClick()
        {
            // Add constructor logic here
        }
         public UserClick(int UserID, string UserName, int PracticeID, int? PracticeLocationID, string PageName, string Area, string Item, string Action, string Promotion, string UserDefined1, string UserDefined2, string UserDefined3, string UserDefined4)
         {
             this.InsertUserClick(UserID, UserName, PracticeID, PracticeLocationID, PageName, Area, Item,  Action, Promotion, UserDefined1, UserDefined2, UserDefined3, UserDefined4);
         }

         public int InsertUserClick(int UserID, string UserName, int PracticeID, int? PracticeLocationID, string PageName, string Area, string Item,  string Action, string Promotion, string UserDefined1, string UserDefined2, string UserDefined3, string UserDefined4)
         {

             DAL.Practice.UserClick dalPU = new DAL.Practice.UserClick();
             return dalPU.InsertUserClick(UserID, UserName, PracticeID, PracticeLocationID, PageName, Area, Item, Action, Promotion, UserDefined1, UserDefined2, UserDefined3, UserDefined4);
         }
    }
}
