using System;
using System.Data;
using System.Configuration;



using DAL.Practice;  //added

//  JB uncomment all dal include load from id.

namespace BLL.Practice
{
	public class Contact
	{
		private int _ContactID;
		private string _Title;
		private string _Salutation;
		private string _FirstName;
		private string _MiddleName;
		private string _LastName;
		private string _Suffix;
		private string _EmailAddress;
		private string _PhoneWork;
		private string _PhoneWorkExtension;
		private string _PhoneCell;
		private string _PhoneHome;
		private string _Fax;
		private int _UserID;
	    private bool _IsSpecificContact;
	    //private int _CreatedUserID;
		//private DateTime _CreatedDate;
		//private int _ModifiedUserID;
		//private DateTime _ModifiedDate;
		//private bool _IsActive;

		// constructors
		public Contact()
		{
			ResetProperties();
		}

		public Contact( int existingContactID )
		{
			_ContactID = existingContactID;
			LoadFromID();		//jb  
		}

		public Contact( Contact existingContactID )
		{
			if ( existingContactID != null )
			{
				_ContactID = existingContactID.ContactID;
				LoadFromID();		//jb  
			}
			else
			{
				// throw exception
			}
		}

		// #region PROPERTIES DEFINED BELOW
		[System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Naming", "CA1706:ShortAcronymsShouldBeUppercase", MessageId = "Member" )]
		public int ContactID
		{
			get { return this._ContactID; }
			set { this._ContactID = value; }
		}
		public string Title
		{
			get { return _Title; }
			set { _Title = value; }
		}

		public string Salutation
		{
			get { return _Salutation; }
			set { _Salutation = value; }
		}

		public string FirstName
		{
			get { return _FirstName; }
			set { _FirstName = value; }
		}

		public string MiddleName
		{
			get { return _MiddleName; }
			set { _MiddleName = value; }
		}

		public string LastName
		{
			get { return _LastName; }
			set { _LastName = value; }
		}

		public string Suffix
		{
			get { return _Suffix; }
			set { _Suffix = value; }
		}

		public string EmailAddress
		{
			get { return _EmailAddress; }
			set { _EmailAddress = value; }
		}

		public string PhoneWork
		{
			get { return _PhoneWork; }
			set { _PhoneWork = value; }
		}

		public string PhoneCell
		{
			get { return _PhoneCell; }
			set { _PhoneCell = value; }
		}

		public string PhoneHome
		{
			get { return _PhoneHome; }
			set { _PhoneHome = value; }
		}

		public string Fax
		{
			get { return _Fax; }
			set { _Fax = value; }
		}

		//unsure about this.
		public int UserID
		{
			get { return _UserID; }
			set { _UserID = value; }
		}

	    public string PhoneWorkExtension
	    {
	        get { return _PhoneWorkExtension;}
	        set { _PhoneWorkExtension = value; }
	    }

	    public bool IsSpecificContact {
	        get { return _IsSpecificContact; }
	        set { _IsSpecificContact = value; } 
        }

//jb
		public bool Update()
		{
			DAL.Practice.Contact c = new DAL.Practice.Contact();
			return c.Update( _ContactID, _Title, _Salutation, _FirstName, _MiddleName, _LastName, _Suffix, _EmailAddress, _PhoneWork, _PhoneWorkExtension, _PhoneHome, _PhoneCell, _Fax, _UserID, _IsSpecificContact);
		}

		public bool Delete()
		{
			DAL.Practice.Contact c = new DAL.Practice.Contact();
			bool rowsdeleted = c.Delete( _ContactID, _UserID );
			ResetProperties();
			return rowsdeleted;
		}

		private void LoadFromID()
		{
			DAL.Practice.Contact c = new DAL.Practice.Contact();
			DAL.Practice.ContactDetails cdetails = c.GetDetails( ContactID );

			_ContactID = cdetails.ContactID;
			_Title = cdetails.Title;
			_Salutation = cdetails.Salutation;
			_FirstName = cdetails.FirstName;
			_MiddleName = cdetails.MiddleName;
			_LastName = cdetails.LastName;
			_Suffix = cdetails.Suffix;
			_EmailAddress = cdetails.EmailAddress;
			_PhoneWork = cdetails.PhoneWork;
			_PhoneWorkExtension = cdetails.PhoneWorkExtension;
			_PhoneCell = cdetails.PhoneCell;
			_PhoneHome = cdetails.PhoneHome;
			_Fax = cdetails.Fax;
		    _IsSpecificContact = cdetails.IsSpecificContact;
		}

		public int LoadFromID( int existingcontactid )
		{
			_ContactID = existingcontactid;
			LoadFromID();
			return _ContactID;
		}

		public int Create(string title, string salutation, string firstname, string middlename, string lastname, string suffix, string emailaddress, string phonework, string phoneWorkExtension, string phonecell, string phonehome, string fax, int userid, bool isSpecificContact)
		{
			int newcontactid;
			//int returnvalue;
			DAL.Practice.Contact c = new DAL.Practice.Contact();

			newcontactid = c.Add( title, salutation, firstname, middlename, lastname, suffix, emailaddress, phonework, phoneWorkExtension, phonecell, phonehome, fax, userid, isSpecificContact);
			//  _contactid = newcontactid;
			return newcontactid;
		}

		private void ResetProperties()
		{
			_ContactID = -1;
			_Title = "";
			_Salutation = "";
			_FirstName = "";
			_MiddleName = "";
			_LastName = "";
			_Suffix = "";
			_EmailAddress = "";
			_PhoneWork = "";
			_PhoneCell = "";
			_PhoneHome = "";
			_Fax = "";
		    _IsSpecificContact = false;
			_UserID = -1;

			//_CreatedUserID;
			//_CreatedDate;
			//_ModifiedUserID;
			//_ModifiedDate;
			//_IsActive;

		}
	}
}

