﻿using System.Collections.Generic;
using System.Linq;
using ClassLibrary.DAL;

namespace BLL.Practice
{
    public class PaymentType
    {

        #region Enumerations
        public enum PaymentTypeEnum : int
        {
            BregInvoice = 1,
            PayPal
        }
        #endregion


        public static List<ClassLibrary.DAL.PaymentType> GetPaymentTypes()
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var query = from pt in visionDB.PaymentTypes
                            orderby pt.PaymentTypeID
                            select pt;

                return query.ToList<ClassLibrary.DAL.PaymentType>();
            }
        }
    }
}
