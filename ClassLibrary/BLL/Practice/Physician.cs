using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Common;

using DAL;
using ClassLibrary.DAL;

namespace BLL.Practice
{
	public class Physician
	{
		#region Fields

		private int _practiceID;
		private int _physicianID;
		private int _contactID;
		//private string _title;
		private string _salutation;
		private string _firstName;
		private string _middleName;
		private string _lastName;
		private string _suffix;
        private bool _isProvider;
        private bool _isFitter;
	    private string _cloudConnectId;
		private int _userID;

		#endregion


		#region Properties

		public int PracticeID
		{
			get { return _practiceID; }
			set { _practiceID = value; }
		}

		public int PhysicianID
		{
			get { return _physicianID; }
			set { _physicianID = value; }
		}

		public int ContactID
		{
			get { return _contactID; }
			set { _contactID = value; }
		}

		public string Salutation
		{
			get { return _salutation; }
			set { _salutation = value; }
		}

		public string FirstName
		{
			get { return _firstName; }
			set { _firstName = value; }
		}

		public string MiddleName
		{
			get { return _middleName; }
			set { _middleName = value; }
		}

		public string LastName
		{
			get { return _lastName; }
			set { _lastName = value; }
		}

		public string Suffix
		{
			get { return _suffix; }
			set { _suffix = value; }
		}

        public bool IsProvider
        {
            get { return _isProvider; }
            set { _isProvider = value; }
        }

        public bool IsFitter
        {
            get { return _isFitter; }
            set { _isFitter = value; }
        }

	    public string CloudConnectId
	    {
            get { return _cloudConnectId;}
            set { _cloudConnectId = value; }
	    }

        public string Pin { get; set; }
        public bool ClearPin { get; set; }

		/// <value>0</value>
		public int UserID
		{
			get { return _userID; }
			set { _userID = value; }
			//set { _userID = 0; }
		}

        public int SortOrder { get; set; }

		#endregion


		public Physician()
		{
			// Add constructor logic here
		}



		public int Insert( BLL.Practice.Physician bllPhysician )
		{
		    int newPhysicianID;
		    DAL.Practice.Physician dalPP = new DAL.Practice.Physician();
		    newPhysicianID = dalPP.Insert( bllPhysician );
		    return newPhysicianID;  //return has to be last statement.
		}


		public void Update( BLL.Practice.Physician bllPhysician )
		{
		    DAL.Practice.Physician dalPP = new DAL.Practice.Physician();
		    dalPP.Update( bllPhysician );
		}


		public void Delete( BLL.Practice.Physician bllPhysician )
		{
		    DAL.Practice.Physician dalPP = new DAL.Practice.Physician();
		    dalPP.Delete( bllPhysician );
		}

		public DataTable GetPhysician( int physicianID )
		{
		    DAL.Practice.Physician dalPP = new DAL.Practice.Physician();
		    DataTable dt = dalPP.SelectOne( physicianID );
		    return dt;
		}

		public DataTable GetAllPhysicians( int practiceID )
		{
		    DAL.Practice.Physician dalPP = new DAL.Practice.Physician();
		    DataTable dt = dalPP.SelectAll( practiceID );
		    return dt;
		}


		public DataTable GetAllPhysicianNames( int practiceID )
		{
		    DAL.Practice.Physician dalPP = new DAL.Practice.Physician();
		    DataTable dt = dalPP.SelectAllNames( practiceID );
		    return dt;
		}

        public void SortClinicians(int PracticeID)
        {
            DAL.Practice.Physician dalPP = new DAL.Practice.Physician();
            dalPP.SortClinicians(PracticeID);
        }

        public void ReorderRows(int selectedPhysicianId, int PracticeId, int indexOffset, List<int> Physicians)
        {
            var dalPP = new DAL.Practice.Physician();
            dalPP.ReorderRows(selectedPhysicianId, PracticeId, indexOffset, Physicians);
        }
    }
}


