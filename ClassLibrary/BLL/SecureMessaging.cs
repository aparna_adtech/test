﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class SecureMessaging
    {
        public enum MessageClass
        {
            Orders,
            Reports,
            PrintDispensement,
            Billing,
            Patient,
            BregMaintenance
        }

        private ClassLibrary.DAL.Practice _CurrentPractice;
        private ClassLibrary.DAL.Practice CurrentPractice
        {
            get
            {
                if (_CurrentPractice == null)
                {
                    if (_visionSessionProvider != null)
                        _CurrentPractice = BLL.Practice.Practice.Select(_visionSessionProvider.GetCurrentPracticeID());
                    else
                        // default to new object, assuming reasonable defaults are enforced during object creation
                        _CurrentPractice = new ClassLibrary.DAL.Practice();
                }
                return _CurrentPractice;
            }
        }

        private string SecureMessageSubjectTag
        {
            get
            {
                return
                    System.Configuration.ConfigurationManager.AppSettings["SecureMessageSubjectTag"]
                        ?? "[visionsecure]";
            }
        }

        private IVisionSessionProvider _visionSessionProvider;

        public SecureMessaging(IVisionSessionProvider visionSessionProvider)
        {
            _visionSessionProvider = visionSessionProvider;
        }

        public void ApplySecureMessagingAdjustments(MessageClass messageClass, ref string sender, ref string recipient, ref string body, ref string subject)
        {
            var add_tag = false;
            switch (messageClass)
            {
                // internal messages
                case MessageClass.Billing:
                case MessageClass.PrintDispensement:
                    if (CurrentPractice.IsSecureMessagingInternalEnabled)
                        add_tag = true;
                    break;
                // external messages
                case MessageClass.Patient:
                    if (CurrentPractice.IsSecureMessagingExternalEnabled)
                        add_tag = true;
                    break;
                // no encryption
                case MessageClass.Orders:
                case MessageClass.Reports:
                case MessageClass.BregMaintenance:
                    break;
            }

            if (add_tag)
                subject = string.Format(@"{0} {1}", SecureMessageSubjectTag, subject);
        }
    }
}
