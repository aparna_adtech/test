﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace ClassLibrary.BLL.HelpSystem
{
    [DataContract]
    public class HelpEntity : INotifyPropertyChanged
    {
        int _Id;
        [DataMember]
        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        int _ParentId;
        [DataMember]
        public int ParentId
        {
            get { return _ParentId; }
            set
            {
                _ParentId = value;
                OnPropertyChanged("ParentId");
            }
        }

        int _EntityId;
        [DataMember]
        public int EntityId
        {
            get { return _EntityId; }
            set
            {
                _EntityId = value;
                OnPropertyChanged("EntityId");
            }
        }

        HelpEntityType _HelpEntityType = HelpEntityType.NotSet;
        [DataMember]
        public HelpEntityType HelpEntityType
        {
            get { return _HelpEntityType; }
            set
            {
                _HelpEntityType = value;
                OnPropertyChanged("HelpEntityType");
            }
        }

        int _SortOrder;
        [DataMember]
        public int SortOrder
        {
            get { return _SortOrder; }
            set
            {
                _SortOrder = value;
                OnPropertyChanged("SortOrder");
            }
        }

        int _Level;
        [DataMember]
        public int Level
        {
            get { return _Level; }
            set
            {
                _Level = value;
                OnPropertyChanged("Level");
            }
        }

        int _TopParentId;
        [DataMember]
        public int TopParentId
        {
            get { return _TopParentId; }
            set
            {
                _TopParentId = value;
                OnPropertyChanged("TopParentId");
            }
        }

        string _Title;
        [DataMember]
        public string Title
        {
            get { return _Title; }
            set
            {
                _Title = value;
                OnPropertyChanged("Title");
            }
        }

        public static HelpEntity DeleteHelpEntity(HelpEntity entity, bool removeEntity)
        {
            ClassLibrary.DAL.HelpSystem.HelpEntity helpEntity = new ClassLibrary.DAL.HelpSystem.HelpEntity();

            return (HelpEntity)helpEntity.DeleteHelpEntity(entity, removeEntity);
        }

        public static HelpEntity InsertHelpEntityMap(HelpEntity entity)
        {
            ClassLibrary.DAL.HelpSystem.HelpEntity helpEntity = new ClassLibrary.DAL.HelpSystem.HelpEntity();

            return (HelpEntity)helpEntity.InsertHelpEntityMap(entity);
        }

        public static HelpEntity UpdateHelpEntityMap(HelpEntity entity)
        {
            ClassLibrary.DAL.HelpSystem.HelpEntity helpEntity = new ClassLibrary.DAL.HelpSystem.HelpEntity();

            return (HelpEntity)helpEntity.UpdateHelpEntityMap(entity);
        }
        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
