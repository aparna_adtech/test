﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ClassLibrary.BLL.HelpSystem
{
    [DataContract]
    public class HelpArticle : HelpEntity
    {
        string _Description;
        [DataMember]
        public string Description
        {
            get { return _Description; }
            set
            {
                _Description = value;
                OnPropertyChanged("Description");
            }
        }

        List<HelpArticle> _RelatedArticles = new List<HelpArticle>();
        [DataMember]
        public List<HelpArticle> RelatedArticles
        {
            get { return _RelatedArticles; }
            set { _RelatedArticles = value; }
        }

        List<HelpMedia> _RelatedMedia = new List<HelpMedia>();
        [DataMember]
        public List<HelpMedia> RelatedMedia
        {
            get { return _RelatedMedia; }
            set { _RelatedMedia = value; }
        }

        public static HelpArticle Insert(HelpArticle entity)
        {
            ClassLibrary.DAL.HelpSystem.HelpEntity helpEntity = new ClassLibrary.DAL.HelpSystem.HelpEntity();

            return (HelpArticle)helpEntity.InsertHelpEntity(entity);
        }

        public static HelpArticle Update(HelpArticle entity)
        {
            ClassLibrary.DAL.HelpSystem.HelpEntity helpEntity = new ClassLibrary.DAL.HelpSystem.HelpEntity();

            return (HelpArticle)helpEntity.UpdateHelpEntity(entity);
        }

        public static HelpArticle GetHelpArticle(int id)
        {
            HelpArticle ret = new HelpArticle();
            ret.Id = id;
            ClassLibrary.DAL.HelpSystem.HelpArticle helpArticle = new ClassLibrary.DAL.HelpSystem.HelpArticle();

            return helpArticle.PopulateArticle(ret);
        }

    }
}
