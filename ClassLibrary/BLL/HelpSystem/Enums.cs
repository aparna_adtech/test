﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ClassLibrary.BLL.HelpSystem
{
    [DataContract]
    public enum HelpEntityType
    {
        [EnumMember]
        NotSet = 0,
        [EnumMember]
        Group,
        [EnumMember]
        Article,
        [EnumMember]
        RelatedArticle,
        [EnumMember]
        Media,
        [EnumMember]
        RelatedMedia
    }
}
