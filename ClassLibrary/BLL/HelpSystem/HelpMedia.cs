﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ClassLibrary.BLL.HelpSystem
{
    [DataContract]
    public class HelpMedia : HelpEntity
    {
        string _Url;
        [DataMember]
        public string Url
        {
            get { return _Url; }
            set
            {
                _Url = value;
                OnPropertyChanged("Url");
            }
        }

        string _Description;
        [DataMember]
        public string Description
        {
            get { return _Description; }
            set
            {
                _Description = value;
                OnPropertyChanged("Description");
            }
        }

        List<HelpArticle> _RelatedArticles = new List<HelpArticle>();
        [DataMember]
        public List<HelpArticle> RelatedArticles
        {
            get { return _RelatedArticles; }
            set { _RelatedArticles = value; }
        }

        List<HelpMedia> _RelatedMedia = new List<HelpMedia>();
        [DataMember]
        public List<HelpMedia> RelatedMedia
        {
            get { return _RelatedMedia; }
            set { _RelatedMedia = value; }
        }

        public static HelpMedia Insert(HelpMedia entity)
        {
            ClassLibrary.DAL.HelpSystem.HelpEntity helpEntity = new ClassLibrary.DAL.HelpSystem.HelpEntity();

            return (HelpMedia)helpEntity.InsertHelpEntity(entity);
        }

        public static HelpMedia Update(HelpMedia entity)
        {
            ClassLibrary.DAL.HelpSystem.HelpEntity helpEntity = new ClassLibrary.DAL.HelpSystem.HelpEntity();

            return (HelpMedia)helpEntity.UpdateHelpEntity(entity);
        }
        
    }
}
