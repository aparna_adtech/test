﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Runtime.Serialization;
 
namespace ClassLibrary.BLL.HelpSystem
{
    [DataContract]
    public class HelpGroup : HelpEntity
    {

        List<HelpGroup> _HelpGroups = new List<HelpGroup>();
        [DataMember]
        public List<HelpGroup> HelpGroups
        {
            get { return _HelpGroups; }
            set { _HelpGroups = value; }
        }

        List<HelpArticle> _HelpArticles = new List<HelpArticle>();
        [DataMember]
        public List<HelpArticle> HelpArticles
        {
            get { return _HelpArticles; }
            set { _HelpArticles = value; }
        }

        List<HelpMedia> _HelpMedias = new List<HelpMedia>();
        [DataMember]
        public List<HelpMedia> HelpMedias
        {
            get { return _HelpMedias; }
            set { _HelpMedias = value; }
        }

        public HelpGroup()
        {
            
        }

       
        public static HelpGroup GetHelpGroup(int Id, int depth)
        {
            HelpGroup ret = new HelpGroup();
            ret.Id = Id;
            ClassLibrary.DAL.HelpSystem.HelpGroup helpGroup = new ClassLibrary.DAL.HelpSystem.HelpGroup();

            return helpGroup.PopulateGroup(ret, depth);
        }

        public static HelpGroup SearchForHelpArticlesAndHelpMeida(string searchText)
        {
            ClassLibrary.DAL.HelpSystem.HelpGroup helpGroup = new ClassLibrary.DAL.HelpSystem.HelpGroup();

            return helpGroup.SearchForHelpArticlesAndHelpMeida(searchText);
        }

        public static HelpGroup Insert(HelpGroup entity)
        {
            ClassLibrary.DAL.HelpSystem.HelpEntity helpEntity = new ClassLibrary.DAL.HelpSystem.HelpEntity();

            return (HelpGroup)helpEntity.InsertHelpEntity(entity);
        }

        public static HelpGroup Update(HelpGroup entity)
        {
            ClassLibrary.DAL.HelpSystem.HelpEntity helpEntity = new ClassLibrary.DAL.HelpSystem.HelpEntity();

            return (HelpGroup)helpEntity.UpdateHelpEntity(entity);
        }
        
    }
}
