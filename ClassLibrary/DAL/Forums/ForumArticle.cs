﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;

namespace ClassLibrary.DAL.Forums
{
    public class ForumArticle
    {
        
        ClassLibrary.BLL.Forums.ForumArticle _Active = null;

        public ForumArticle()
        {

        }

        private List<ClassLibrary.DAL.ForumArticle> GetTopLevelArticesByGroupId(int id)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                var query = from fa in visionDB.ForumArticles
                            where fa.IsActive == true && fa.ParentId == null && fa.ForumGroupId == id
                            select fa;

                return query.OrderByDescending(fa => fa.IsSticky).OrderByDescending(fa => fa.Created).ToList<ClassLibrary.DAL.ForumArticle>();
            }

        }
        
       

        private List<ClassLibrary.DAL.ForumArticle> GetRecientArticlesForAllGroups(int count, bool isActive)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                var query = from fa in visionDB.ForumArticles
                            where fa.IsActive == isActive
                            select fa;

                return query.OrderByDescending(fa => fa.Created).Take(count).ToList<ClassLibrary.DAL.ForumArticle>();
            }
        }

        private List<ClassLibrary.DAL.ForumArticle> SearchArticlesForAllGroups(string searchText, int count)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                var query = from fa in visionDB.ForumArticles
                            where fa.IsActive == true && fa.Body.Contains(searchText)
                            select fa;

                return query.OrderByDescending(fa => fa.Created).Take(count).ToList<ClassLibrary.DAL.ForumArticle>();
            }
        }

        private List<ClassLibrary.DAL.ForumArticle> GetRecientArticlesByGroupId(int groupId, int count)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                var query = from fa in visionDB.ForumArticles
                            where fa.IsActive == true && fa.ForumGroupId == groupId
                            select fa;

                return query.OrderByDescending(fa => fa.Created).Take(count).ToList<ClassLibrary.DAL.ForumArticle>();
            }
        }

        public List<BLL.Forums.ForumArticle> GetForumArticleTreesByForumGroup(int id, int depth, bool isActive)
        {
            List<BLL.Forums.ForumArticle> ret = new List<ClassLibrary.BLL.Forums.ForumArticle>();

            foreach (ClassLibrary.DAL.ForumArticle entityArticle in GetTopLevelArticesByGroupId(id))
            {
                ret.Add(GetForumArticleTreeByArticleId(entityArticle.Id,depth, isActive));
            }

            return ret;
        }

        public List<BLL.Forums.ForumArticle> GetForumArticlesByUserId(int userId, int depth, bool isActive)
        {
            List<BLL.Forums.ForumArticle> ret = new List<ClassLibrary.BLL.Forums.ForumArticle>();

            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                var query = from fa in visionDB.ForumArticles
                            where fa.IsActive == true && fa.UserId == userId && fa.IsActive == isActive
                            select fa;
                
                foreach (ClassLibrary.DAL.ForumArticle entityArticle in query.OrderByDescending(fa => fa.IsSticky).OrderByDescending(fa => fa.Created).ToList<ClassLibrary.DAL.ForumArticle>())
                {
                    ret.Add(GetForumArticleTreeByArticleId(entityArticle.Id, depth, isActive)); 
                }

            }

            return ret;
        }

        public List<BLL.Forums.ForumArticle> GetForumArticlesBySearchText(string searchText, int depth, bool isActive)
        {
            List<BLL.Forums.ForumArticle> ret = new List<ClassLibrary.BLL.Forums.ForumArticle>();

            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                var query = from fa in visionDB.ForumArticles
                            where fa.IsActive == true && fa.Body.ToLower().Contains(searchText.ToLower())  && fa.IsActive == isActive
                            select fa;

                foreach (ClassLibrary.DAL.ForumArticle entityArticle in query.OrderByDescending(fa => fa.IsSticky).OrderByDescending(fa => fa.Created).ToList<ClassLibrary.DAL.ForumArticle>())
                {
                    ret.Add(GetForumArticleTreeByArticleId(entityArticle.Id, depth, isActive));
                }

            }

            return ret;
        }

        public List<BLL.Forums.ForumArticle> GetRecentArticles(int count, int depth, bool isActive)
        {
            List<BLL.Forums.ForumArticle> ret = new List<ClassLibrary.BLL.Forums.ForumArticle>();

            foreach (ClassLibrary.DAL.ForumArticle entityArticle in GetRecientArticlesForAllGroups(count,isActive))
            {
                ret.Add(GetForumArticleTreeByArticleId(entityArticle.Id, depth,isActive));
            }

            return ret;
        }

       
        public bool SetForumArticleIsActive(int articleId, bool isActive)
        {
            bool ret = false;

            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                var query = from fa in visionDB.ForumArticles
                            where fa.Id == articleId
                            select fa;

                DAL.ForumArticle entityArticle = query.FirstOrDefault();

                if (entityArticle != null)
                {
                    //update it
                    entityArticle.IsActive = isActive;

                    visionDB.SubmitChanges();
                    ret = true;
                }
                else
                {
                    ret = false;
                }
            }

            return ret;
        }

        public bool DeleteForumArticle(int articleId)
        {
            bool ret = false;

            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                var query = from fa in visionDB.ForumArticles
                            where fa.ParentId == articleId
                            select fa;

                List<ClassLibrary.DAL.ForumArticle> tmp = query.ToList<ClassLibrary.DAL.ForumArticle>();

                if (tmp.Count() == 0)
                {

                    query = from fa in visionDB.ForumArticles
                                where fa.Id == articleId
                                select fa;

                    DAL.ForumArticle entityArticle = query.FirstOrDefault();

                    if (entityArticle != null)
                    {
                        visionDB.ForumArticles.DeleteOnSubmit(visionDB.ForumArticles.Where(fa => fa.Id == articleId).Select(fa => fa).Single());
                        visionDB.SubmitChanges();
                        ret = true;
                    }
                    else
                    {
                        ret = false;
                    }
                }
                else
                {
                    ret = false; //has children so cannot delete
                }
            }

            return ret;
        }
        public List<BLL.Forums.ForumArticle> SearchArticles(string searchText, int count, int depth, bool isActive)
        {
            List<BLL.Forums.ForumArticle> ret = new List<ClassLibrary.BLL.Forums.ForumArticle>();

            foreach (ClassLibrary.DAL.ForumArticle entityArticle in SearchArticlesForAllGroups(searchText, count))
            {
                ret.Add(GetForumArticleTreeByArticleId(entityArticle.Id, depth, isActive));
            }

            return ret;
        }

        public List<BLL.Forums.ForumArticle> GetRecentArticlesByGroupId(int groupId, int count, int depth, bool isActive)
        {
            List<BLL.Forums.ForumArticle> ret = new List<ClassLibrary.BLL.Forums.ForumArticle>();

            foreach (ClassLibrary.DAL.ForumArticle entityArticle in GetRecientArticlesByGroupId(groupId, count))
            {
                ret.Add(GetForumArticleTreeByArticleId(entityArticle.Id, depth, isActive));
            }

            return ret;
        }

        public BLL.Forums.ForumArticle GetForumArticleTreeByArticleId(int id, int depth, bool isActive)
        {
            //users the forumArticleId from the linq query to return the tree by depth

            ClassLibrary.BLL.Forums.ForumArticle top = new ClassLibrary.BLL.Forums.ForumArticle();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetForumArticleTree"))
                {
                    db.AddInParameter(dbCommand, "@Id", DbType.Int32, id);
                    db.AddInParameter(dbCommand, "@Depth", DbType.Int32, depth);

                    DataTable dt = new DataTable();
                    dt = db.ExecuteDataSet(dbCommand).Tables[0];
                    dbCommand.Dispose();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];

                        if (i == 0)
                        {
                            top = PopulateForumArticleFromDataRow(dr);
                            _Active = top;
                        }
                        else
                        {
                            if (bool.Parse(dr["IsActive"].ToString()) == isActive)
                            {
                                if (_Active.ParentId != (int.Parse(dr["ParentId"].ToString())))
                                {
                                    //find the parent for the active DataRow and set it as the activeEntity
                                    //always start from the top and look down
                                    SetActiveArticle(top, int.Parse(dr["ParentId"].ToString()));
                                }

                                ClassLibrary.BLL.Forums.ForumArticle thisArticle = new ClassLibrary.BLL.Forums.ForumArticle();
                                thisArticle = PopulateForumArticleFromDataRow(dr);
                                _Active.ForumArticles.Add(thisArticle);
                            }
                        }
                    }
                }
            }
            catch (SqlException se)
            {
                // add correct logging
                throw se;
            }
            catch (Exception ex)
            {
                // add correct logging
                throw ex;
            }
            return top;
        }

        public BLL.Forums.ForumArticle GetForumArticleTreeByTopArticle(int id, int depth, bool isActiveOnly)
        {

            ClassLibrary.BLL.Forums.ForumArticle top = new ClassLibrary.BLL.Forums.ForumArticle();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetForumArticleTreeByTopArticle"))
                {
                    db.AddInParameter(dbCommand, "@Id", DbType.Int32, id);
                    db.AddInParameter(dbCommand, "@Depth", DbType.Int32, depth);

                    DataTable dt = new DataTable();
                    dt = db.ExecuteDataSet(dbCommand).Tables[0];
                    dbCommand.Dispose();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];

                        if (i == 0)
                        {
                            top = PopulateForumArticleFromDataRow(dr);
                            _Active = top;
                        }
                        else
                        {
                            if (_Active.ParentId != (int.Parse(dr["ParentId"].ToString())))
                            {
                                //find the parent for the active DataRow and set it as the activeEntity
                                //always start from the top and look down
                                SetActiveArticle(top, int.Parse(dr["ParentId"].ToString()));
                            }

                           
                            ClassLibrary.BLL.Forums.ForumArticle thisArticle = new ClassLibrary.BLL.Forums.ForumArticle();
                            thisArticle = PopulateForumArticleFromDataRow(dr);

                            if (isActiveOnly && thisArticle.IsActive == false)
                            {
                                //don't show inactive article to the user.
                            }
                            else
                            {
                                //this is a breg admin user viewing the article thread
                                _Active.ForumArticles.Add(thisArticle);
                            }
                        }
                    }
                }
            }
            catch (SqlException se)
            {
                // add correct logging
                throw se;
            }
            catch (Exception ex)
            {
                // add correct logging
                throw ex;
            }
            return top;
        }


        private bool SetActiveArticle(BLL.Forums.ForumArticle entity, int Id)
        {
            if (entity.Id == Id)
            {
                _Active = entity;
                return true;
            }
            else
            {
                foreach (BLL.Forums.ForumArticle article in entity.ForumArticles)
                {
                    if (SetActiveArticle(article, Id))
                        return true;
                }
            }

            return false;
        }

        /// <summary>
        /// This is the DAL transform to the BL object used by the client.  
        /// It is used by all calls a as it contains all the display info needed for an article
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        private static ClassLibrary.BLL.Forums.ForumArticle PopulateForumArticleFromDataRow(DataRow dr)
        {
            ClassLibrary.BLL.Forums.ForumArticle ret = new ClassLibrary.BLL.Forums.ForumArticle();

            ret.Id = int.Parse(dr["Id"].ToString());

            if (!Convert.IsDBNull(dr["ParentId"]))
            {
                ret.ParentId = int.Parse(dr["ParentId"].ToString());
            }

            ret.Subject = dr["Subject"].ToString();
            ret.Body = dr["Body"].ToString();

            ret.ForumGroupId = int.Parse(dr["ForumGroupId"].ToString());

            if (!Convert.IsDBNull(dr["MasterCatalogProductID"]))
            {
                ret.MasterCatalogProductId = int.Parse(dr["MasterCatalogProductID"].ToString());
            }

            ret.UserId = int.Parse(dr["UserId"].ToString());
            ret.Created = (DateTime)(dr["Created"]);
            ret.IsActive = bool.Parse(dr["IsActive"].ToString());
            ret.IsSticky = bool.Parse(dr["IsSticky"].ToString());
            ret.Level = int.Parse(dr["Level"].ToString());
            ret.TopParentId = int.Parse(dr["TopParentId"].ToString());

            ret.PublicUserName = dr["PublicUserName"].ToString();
            ret.PublicUserRole = dr["PublicUserRole"].ToString();
            ret.PublicUserDescription = dr["PublicUserDescription"].ToString();
            
            ret.PracticeName = dr["PracticeName"].ToString();
            ret.PracticeDescription = dr["PracticeDescription"].ToString();

            ret.ForumGroupName = dr["ForumGroupName"].ToString();

            return ret;
        }
    }
}
