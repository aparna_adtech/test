﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary.DAL
{
    public static class UpdateExtensions
    {


        // Copyright (C) 2007 Troy Magennis. All Rights Reserved.
        // You are free to use this material, however you do so AT YOUR OWN risk. 
        // You are prohibited from removing this disclaimer or copyright notice from any derivitive works.
        // Remember to visit http://www.hookedonlinq.com - The LINQ wiki community project.
        public delegate void Func<TArg0>(TArg0 element);

        /// <summary>
        /// Executes an Update statement block on all elements in an IEnumerable<T> sequence.
        /// </summary>
        /// <typeparam name="TSource">The source element type.</typeparam>
        /// <param name="source">The source sequence.</param>
        /// <param name="update">The update statement to execute for each element.</param>
        /// <returns>The numer of records affected.</returns>
        public static int Update<TSource>(this IEnumerable<TSource> source, Func<TSource> update)
        {
            if (source == null) throw new ArgumentNullException("source");
            if (update == null) throw new ArgumentNullException("update");
            if (typeof(TSource).IsValueType)
                throw new NotSupportedException("value type elements are not supported by update.");

            int count = 0;
            foreach (TSource element in source)
            {
                update(element);
                count++;
            }
            return count;
        }
    }

    public class DispenseQueueInterface
    {
        public int DispenseQueueID { get; set; }

        public string DispenseIdentifier { get; set; }

        public int PracticeLocationID { get; set; }

        public int ProductInventoryID { get; set; }

        public int? PhysicianID { get; set; }

        public int? PracticePayorID { get; set; }

        public string PatientCode { get; set; }

        public string PatientEmail { get; set; }

        public string ICD9Code { get; set; }

        public DateTime? DispenseDate { get; set; }

        public DateTime? CreatedDate { get; set; }

        public bool IsMedicare { get; set; }

        public bool IsRental { get; set; }

        public bool ABNForm { get; set; }

        public decimal DMEDeposit { get; set; }

        public int QuantityToDispense { get; set; }

        public int PracticeCatalogProductID { get; set; }

        public string SupplierShortName { get; set; }

        public string BrandShortName { get; set; }

        public string ShortName { get; set; }

        public string Code { get; set; }

        public string Side { get; set; }

        public string Mod1 { get; set; }
        public string Mod2 { get; set; }
        public string Mod3 { get; set; }

        public string Size { get; set; }

        public string HCPCsString { get; set; }

        public string OTSHCPCsString { get; set; }

        public decimal WholesaleCost { get; set; }

        public decimal BillingCharge { get; set; }
        
        public decimal BillingChargeOffTheShelf { get; set; }

        public decimal BillingChargeCash { get; set; }

        public decimal hdnDMEDeposit { get; set; }

        public int QuantityOnHandPerSystem { get; set; }

        public bool ABN_Needed { get; set; }

        public string DeclineReason { get; set; }

        public int MedicareOption { get; set; }

        public int DispenseSignatureID { get; set; }

        public string PatientFirstName { get; set; }
        
        public string PatientLastName { get; set; }
        
        public string Note { get; set; }

        public bool IsSplitCode { get; set; }

        public bool IsAlwaysOffTheShelf { get; set; }

        public bool? IsCustomFit { get; set; }

        public int? FitterID { get; set; }

        public bool? IsCustomBrace { get; set; }

        public bool? IsCustomFabricated { get; set; }

        public string HCPCs { get; set; }

        public string ABNType { get; set; }

        public bool IsPreAuthorization { get; set; }

        public string QueuedReason { get; set; }

        public IQueryable<DQCustomFitProcedure> CustomFitProcedures { get; set; }

        public int? BregVisionOrderID { get; set; }
        public string DispenseGroupingID { get; set; }

        public bool CustomFitEnabled { get; set; }

        public bool EnableSide { get; set; }

        public bool PreAuthorized { get; set; }

        public decimal hdnBillingCharge{ get; set; }//this value is used on the UI to reset the value to the original value before user modification
        public bool IsCashCollection { get; internal set; }
    }

    public class SupplierOrderInterface
    {
        public int SupplierOrderId { get; set; }
        public int BregVisionOrderId { get; set; }
        public int PurchaseOrder { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class DQCustomFitProcedure
    {
        public int CustomFitProcedureID { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsUserEntered { get; set; }
    }

    public partial class GenericReturn
    {
        public Int32 ID { get; set; }
        public string Name { get; set; }
    }
}
