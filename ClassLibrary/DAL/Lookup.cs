using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
//using BLL;

namespace DAL
{
    /// <summary>
    /// Summary description for PracticePhysician
    /// </summary>
    public class Lookup
    {
        public Lookup()
        {
            //
            // Add constructor logic here
            //
        }

        public DataTable Salutation_SelectAll( )
        {   
            Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            using ( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_Lookup_Salutation_Select_All" ) )
            {
                DataTable dt = new DataTable();
                dt = db.ExecuteDataSet( dbCommand ).Tables[0];
                dbCommand.Dispose();       
                return dt;
            }
        }

        public DataTable Suffix_SelectAll()
        {
            Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            using ( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_Lookup_Suffix_Select_All" ) )
            {
                DataTable dt = new DataTable();
                dt = db.ExecuteDataSet( dbCommand ).Tables[0];
                dbCommand.Dispose();
                return dt;
            }
        }
    }
}
