using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Configuration;

namespace DAL
{
    public class LoggedInUser
    {
        public LoggedInUser()
        { 
        }

        public static int SelectUserID( string userLoginName )
        {
            int userID;
            
            int rowsAffected = 0;
            
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_User_Select_UserID_By_LoginName");
            
            db.AddInParameter(dbCommand, "UserLoginName", DbType.String, userLoginName);
            db.AddOutParameter(dbCommand, "UserID", DbType.Int32, 4);
            
            rowsAffected = db.ExecuteNonQuery(dbCommand);
            
            userID = Convert.ToInt32(db.GetParameterValue(dbCommand, "UserID"));
            
            //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
        
            return userID;
        }

        public static void SelectUserIDandPractice(string userLoginName, out int userID, out int practiceID, out string practiceName)
        {
            int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_Practice_Select_By_LoginName");

            db.AddInParameter(dbCommand, "UserLoginName", DbType.String, userLoginName);
            db.AddOutParameter(dbCommand, "UserID", DbType.Int32, 4);
            db.AddOutParameter(dbCommand, "PracticeID", DbType.Int32, 4);
            db.AddOutParameter(dbCommand, "PracticeName", DbType.String, 50);

            rowsAffected = db.ExecuteNonQuery(dbCommand);

            userID = Convert.ToInt32(db.GetParameterValue(dbCommand, "UserID"));
            practiceID = Convert.ToInt32(db.GetParameterValue(dbCommand, "PracticeID"));
            practiceName = Convert.ToString(db.GetParameterValue(dbCommand, "PracticeName"));
            //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
        }

        public static int SelectPrimaryLocation(int practiceID )
        {
            int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_Select_PrimaryLocation_Given_PracticeID");

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            db.AddOutParameter(dbCommand, "PracticeLocationID", DbType.Int32, 4);

            rowsAffected = db.ExecuteNonQuery(dbCommand);

            int practiceLocationID = Convert.ToInt32(db.GetParameterValue(dbCommand, "PracticeLocationID"));

            return practiceLocationID;
        }
    }
}
