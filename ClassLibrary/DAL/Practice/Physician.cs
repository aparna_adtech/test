using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using BLL;
using ClassLibrary.DAL;

namespace DAL.Practice
{
    /// <summary>
    /// Summary description for Physician
    /// </summary>
    public class Physician
    {
        public Physician()
        {
            //
            // Add constructor logic here
            //
        }

        public DataTable SelectAll( int practiceID )
        {
            Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            using( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_Clinician_SelectALL_ByPracticeID" ) )
            {
                db.AddInParameter( dbCommand, "PracticeID", DbType.Int32, practiceID );

                DataTable dt = new DataTable();
                dt = db.ExecuteDataSet( dbCommand ).Tables[0];
                dbCommand.Dispose();        
                return dt;
            }
        }


		public DataTable SelectAllNames( int practiceID )
		{
			Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
			using ( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.[usp_Physician_SelectALL_Names_ByPracticeID]" ) )
			{
				db.AddInParameter( dbCommand, "PracticeID", DbType.Int32, practiceID );

				DataTable dt = new DataTable();
				dt = db.ExecuteDataSet( dbCommand ).Tables[0];
				dbCommand.Dispose();
				return dt;
			}
		}
		//  


        public DataTable SelectOne( int physicianID )
        {
            Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            using ( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_Physician_Select_One" ) )
            {
                db.AddInParameter( dbCommand, "PhysicianID", DbType.Int32, physicianID );

                DataTable dt = new DataTable();
                dt = db.ExecuteDataSet( dbCommand ).Tables[0];
                dbCommand.Dispose();
                return dt;
            }
        }


        public int Insert( BLL.Practice.Physician bllPP ) //int practiceID, out int physicianID, out int contactID, 
        {                                                           //  string title, string firstName, string middleName, 
                                                                //  string lastName, string suffix, int createdUserID )
            int newPhysicianID = -1;

            int rowsAffected = 0;
            
            int practiceID = bllPP.PracticeID;
            int physicianID = bllPP.PhysicianID;
            int contactID = bllPP.ContactID;
            string salutation = bllPP.Salutation;
            string firstName = bllPP.FirstName;
            string middleName = bllPP.MiddleName;
            string lastName = bllPP.LastName;
            string suffix = bllPP.Suffix;
            bool isProvider = bllPP.IsProvider;
            bool isFitter = bllPP.IsFitter;
            string cloudConnectId = bllPP.CloudConnectId;
            string pin = bllPP.Pin;
            int userID = bllPP.UserID;

            Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            using ( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_Practice_Clinician_Insert" ) ) //MWS_TODO:  Save this to git
            {
                db.AddInParameter( dbCommand, "PracticeID", DbType.Int32, practiceID );
                db.AddOutParameter( dbCommand, "ClinicianID", DbType.Int32, 4 );
                db.AddOutParameter( dbCommand, "ContactID", DbType.Int32, 4 );
                db.AddInParameter( dbCommand, "Salutation", DbType.String, salutation );
                db.AddInParameter( dbCommand, "FirstName", DbType.String, firstName );
                db.AddInParameter( dbCommand, "MiddleName", DbType.String, middleName );
                db.AddInParameter( dbCommand, "LastName", DbType.String, lastName );
                db.AddInParameter( dbCommand, "Suffix", DbType.String, suffix );
                db.AddInParameter( dbCommand, "IsProvider", DbType.Boolean, isProvider);
                db.AddInParameter( dbCommand, "IsFitter", DbType.Boolean, isFitter);
                db.AddInParameter( dbCommand, "CloudConnectID", DbType.String, cloudConnectId );
                db.AddInParameter( dbCommand, "Pin", DbType.String, pin);
                db.AddInParameter( dbCommand, "CreatedUserID", DbType.Int32, userID );
                if (bllPP.SortOrder > 0)
                { 
                    db.AddInParameter(dbCommand, "SortOrder", DbType.Int32, bllPP.SortOrder);
                }
                rowsAffected = db.ExecuteNonQuery( dbCommand );

                newPhysicianID = Convert.ToInt32( db.GetParameterValue( dbCommand, "ClinicianID" ) );
                contactID = Convert.ToInt32( db.GetParameterValue( dbCommand, "ContactID" ) );

                dbCommand.Dispose();
            }
                return newPhysicianID;
            
                //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
        }

        public void Update( BLL.Practice.Physician bllPP )
        {
            int rowsAffected = 0;

            //int practiceID = pp.PracticeID;
            //int physicianID = pp.PhysicianID;
            int contactID = bllPP.ContactID;
            string salutation = bllPP.Salutation;
            string firstName = bllPP.FirstName;
            string middleName = bllPP.MiddleName;
            string lastName = bllPP.LastName;
            string suffix = bllPP.Suffix;
            bool isProvider = bllPP.IsProvider;
            bool isFitter = bllPP.IsFitter;
            string cloudConnectId = bllPP.CloudConnectId;
            string pin = bllPP.Pin;
            bool clearPin = bllPP.ClearPin;
            int userID = bllPP.UserID;

            Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            using ( DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_Practice_Clinician_Update" ) ) //MWS_TODO: make sure this gets saved in git.
            {
                //db.AddInParameter( dbCommand, "PracticeID", DbType.Int32, practiceID );
                //db.AddOutParameter( dbCommand, "PhysicianID", DbType.Int32, 4 );
                db.AddInParameter( dbCommand, "ContactID", DbType.Int32, contactID );
                db.AddInParameter( dbCommand, "Salutation", DbType.String, salutation );
                db.AddInParameter( dbCommand, "FirstName", DbType.String, firstName );
                db.AddInParameter( dbCommand, "MiddleName", DbType.String, middleName );
                db.AddInParameter( dbCommand, "LastName", DbType.String, lastName );
                db.AddInParameter( dbCommand, "Suffix", DbType.String, suffix );
                db.AddInParameter( dbCommand, "IsProvider", DbType.Boolean, isProvider );
                db.AddInParameter( dbCommand, "IsFitter", DbType.Boolean, isFitter );
                db.AddInParameter( dbCommand, "CloudConnectID", DbType.String, cloudConnectId);
                db.AddInParameter( dbCommand, "Pin", DbType.String, pin);
                db.AddInParameter( dbCommand, "ClearPin", DbType.Boolean, clearPin);
                db.AddInParameter( dbCommand, "ModifiedUserID", DbType.Int32, userID );
                {
                    db.AddInParameter(dbCommand, "SortOrder", DbType.Int32, bllPP.SortOrder);
                }

                rowsAffected = db.ExecuteNonQuery( dbCommand );

                dbCommand.Dispose();  
                //int newPhysicianID = Convert.ToInt32( db.GetParameterValue( dbCommand, "PhysicianID" ) );
                //contactID = Convert.ToInt32( db.GetParameterValue( dbCommand, "ContactID" ) );

                //return newPhysicianID;
                //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
            }
        }

        public void Delete( BLL.Practice.Physician bllPP )
        {
            int clinicianID = bllPP.PhysicianID;
            int userID = bllPP.UserID;

            int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            using ( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_Practice_Clinician_Delete" ) )
            {
                db.AddInParameter( dbCommand, "ClinicianID", DbType.Int32, clinicianID );
                db.AddInParameter( dbCommand, "ModifiedUserID", DbType.Int32, userID );

                rowsAffected = db.ExecuteNonQuery( dbCommand );

                dbCommand.Dispose();
            }
        }

        public void SortClinicians(int PracticeID)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var clinicians = (from p in visionDB.Clinicians
                                  where p.PracticeID == PracticeID
                                  orderby p.Contact.LastName
                                  select p).ToList<Clinician>();

                for (int i = 1; i <= clinicians.Count; i++)
                    clinicians[i - 1].SortOrder = i;

                visionDB.SubmitChanges();
            }
        }

        //public DataTable GetSalutation( physicianID )



        ////public void SelectOneByParams( int physicianID, int contactID, out string salutation, 
        //      out string firstName, out string middleName, out string lastName, out string suffix )
        //{
        //    int rowsAffected = 0;

        //    Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
        //    DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_Physician_Select_One_By_Params" );

        //    db.AddInParameter( dbCommand, "PhysicianID", DbType.Int32, physicianID );
        //    db.AddInParameter( dbCommand, "ContactID", DbType.Int32, contactID );
        //    db.AddOutParameter( dbCommand, "Salutation", DbType.String, 10 );
        //    db.AddOutParameter( dbCommand, "FirstName", DbType.String, 50 );
        //    db.AddOutParameter( dbCommand, "MiddleName", DbType.String, 50 );
        //    db.AddOutParameter( dbCommand, "LastName", DbType.String, 50 );
        //    db.AddOutParameter( dbCommand, "Suffix", DbType.String, 10 );

        //    rowsAffected = db.ExecuteNonQuery( dbCommand );

        //    salutation = Convert.ToString( db.GetParameterValue( dbCommand, "Salutation" ) );
        //    firstName = Convert.ToString( db.GetParameterValue( dbCommand, "FirstName" ) );
        //    middleName = Convert.ToString( db.GetParameterValue( dbCommand, "MiddleName" ) );
        //    lastName = Convert.ToString( db.GetParameterValue( dbCommand, "LastName" ) );
        //    suffix = Convert.ToString( db.GetParameterValue( dbCommand, "Suffix" ) );
        //    //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
        //}


        //protected internal int InsertVendor( BusinessLogicLayer.Vendor.Vendor vendor )
        //{
        //    DbCommand cmd = dbVendor.GetStoredProcCommand( "dbo.usp_Vendor_Insert" );

        //    int vendorID = vendor.VendorID;
        //    string vendorCode = vendor.VendorCode;
        //    string vendorShortName = vendor.VendorShortName;
        //    string vendorLongName = vendor.VendorLongName;

        //    dbVendor.AddOutParameter( cmd, PARAM_VENDOR_ID, System.Data.DbType.Int32, vendorID );
        //    dbVendor.AddInParameter( cmd, PARAM_VENDOR_CODE, System.Data.DbType.String, vendorCode );
        //    dbVendor.AddInParameter( cmd,  PARAM_VENDOR_SHORT_NAME, System.Data.DbType.String, vendorShortName );
        //    dbVendor.AddInParameter( cmd, PARAM_VENDOR_LONG_NAME, System.Data.DbType.String, vendorLongName );
        //    dbVendor.ExecuteNonQuery( cmd );

        //    int newVendorID = ( int ) cmd.Parameters["@" + PARAM_VENDOR_ID].Value;
        //    return newVendorID;
        //}

        //public void Insert( int practiceID, out int physicianID, out int contactID, string title, 
        //                  string firstName, string middleName, string lastName, string suffix, int createdUserID )
        //{
        //    int rowsAffected = 0;

        //    Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
        //    DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_Practice_Physician_Insert" );

        //    db.AddInParameter( dbCommand, "PracticeID", DbType.Int32, practiceID );
        //    db.AddOutParameter( dbCommand, "PhysicianID", DbType.Int32, 4 );
        //    db.AddOutParameter( dbCommand, "ContactID", DbType.Int32, 4 );
        //    db.AddInParameter( dbCommand, "Title", DbType.String, title );
        //    db.AddInParameter( dbCommand, "FirstName", DbType.String, firstName );
        //    db.AddInParameter( dbCommand, "MiddleName", DbType.String, middleName );
        //    db.AddInParameter( dbCommand, "LastName", DbType.String, lastName );
        //    db.AddInParameter( dbCommand, "Suffix", DbType.String, suffix );
        //    db.AddInParameter( dbCommand, "CreatedUserID", DbType.Int32, createdUserID );

        //    rowsAffected = db.ExecuteNonQuery( dbCommand );

        //    physicianID = Convert.ToInt32( db.GetParameterValue( dbCommand, "PhysicianID" ) );
        //    contactID = Convert.ToInt32( db.GetParameterValue( dbCommand, "ContactID" ) );

        //    //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
        //}


        //    public void Update( int contactID, string title, string salutation, string firstName, 
        //                      string middleName, string lastName, string suffix, int modifiedUserID )
        //    {
        //        int rowsAffected = 0;

        //        Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
        //        DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_Practice_Physician_Update" );

        //        db.AddInParameter( dbCommand, "ContactID", DbType.Int32, contactID );
        //        db.AddInParameter( dbCommand, "Title", DbType.String, title );
        //        db.AddInParameter( dbCommand, "Salutation", DbType.String, salutation );
        //        db.AddInParameter( dbCommand, "FirstName", DbType.String, firstName );
        //        db.AddInParameter( dbCommand, "MiddleName", DbType.String, middleName );
        //        db.AddInParameter( dbCommand, "LastName", DbType.String, lastName );
        //        db.AddInParameter( dbCommand, "Suffix", DbType.String, suffix );
        //        db.AddInParameter( dbCommand, "ModifiedUserID", DbType.Int32, modifiedUserID );

        //        rowsAffected = db.ExecuteNonQuery( dbCommand );

        //        //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
        //    }
        //}
        public void ReorderRows(int selectedPhysicianId, int PracticeId, int indexOffset, List<int> Physicians)
        {
            int sortOrder = 1;
            int currentPhysicianIndex = -1;

            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {

                var orderedPhysicians = new List<Clinician>();
                var Clinicians = (from p in visionDB.Clinicians
                                  where p.PracticeID == PracticeId
                                  select p).ToList<Clinician>();

                foreach (var Physician in Physicians)
                {
                    var currentClinician = Clinicians.Find(s => s.ClinicianID == Physician);
                    currentClinician.SortOrder = sortOrder;
                    orderedPhysicians.Add(currentClinician);

                    if (selectedPhysicianId == Physician)
                        currentPhysicianIndex = orderedPhysicians.Count - 1;

                    sortOrder += 1;
                }

                if (currentPhysicianIndex + indexOffset > -1 && currentPhysicianIndex + indexOffset < orderedPhysicians.Count)
                {
                    orderedPhysicians[currentPhysicianIndex].SortOrder += indexOffset;
                    orderedPhysicians[currentPhysicianIndex + indexOffset].SortOrder -= indexOffset;
                }
                visionDB.SubmitChanges();
            }
        }
    }
}
