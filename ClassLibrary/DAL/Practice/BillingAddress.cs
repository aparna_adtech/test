using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;


namespace DAL.Practice
{

    /// <summary>
    /// Summary description for Practice.BillingAddress
    /// </summary>
    public class BillingAddress
    {
        public BillingAddress()
        {
            //
            // 
            //
        }

        public void GetPracticeBillingAddressID( int practiceID, out int addressID, out bool isBillingCentralized )
        {
            int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_PracticeBillingAddress_Select_AddressID" );

			

            db.AddInParameter( dbCommand, "PracticeID", DbType.Int32, practiceID );
            db.AddOutParameter( dbCommand, "AddressID", DbType.Int32, 4 );
            db.AddOutParameter( dbCommand, "IsBillingCentralized", DbType.Boolean, 1 );
            //db.AddParameter( dbCommand, "RETURN_VALUE", DbType.Int32, 4, ParameterDirection.ReturnValue, false, 0, 0, "", DataRowVersion.Default, 0 ); //ParameterDirection.ReturnValue, );

            rowsAffected = db.ExecuteNonQuery( dbCommand );


			isBillingCentralized = ( ( db.GetParameterValue( dbCommand, "IsBillingCentralized" ) != DBNull.Value ) ? Convert.ToBoolean( db.GetParameterValue( dbCommand, "IsBillingCentralized" ) ) : false );
			addressID = ( ( db.GetParameterValue( dbCommand, "AddressID" ) != DBNull.Value ) ? Convert.ToInt32( db.GetParameterValue( dbCommand, "AddressID" ) ) : -1 );
			//if ( db.GetParameterValue( dbCommand, "AddressID" ) != DBNull.Value )
			//{
			//    addressID = Convert.ToInt32( db.GetParameterValue( dbCommand, "AddressID" ) );
			//}
			//else
			//{
			//    addressID = -1;
			//}
        }


        public void SelectOneByPractice( int practiceID, out string attentionOf, out int addressID, out string addressLine1, out string addressLine2, out string city, out string state, out string zipCode, out string zipCodePlus4 )
        {
            int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_PracticeBillingAddress_Select_One_ByPracticeID" );

            db.AddInParameter( dbCommand, "PracticeID", DbType.Int32, practiceID );
            db.AddOutParameter( dbCommand, "AttentionOf", DbType.String, 50 );
            db.AddOutParameter( dbCommand, "AddressID", DbType.String, 4 );
            db.AddOutParameter( dbCommand, "AddressLine1", DbType.String, 50 );
            db.AddOutParameter( dbCommand, "AddressLine2", DbType.String, 50 );
            db.AddOutParameter( dbCommand, "City", DbType.String, 50 );

			//db.AddOutParameter( dbCommand, "StateID", DbType.Byte, 1 );
			db.AddOutParameter( dbCommand, "State", DbType.String, 2 );

			db.AddOutParameter( dbCommand, "ZipCode", DbType.String, 5 );
            db.AddOutParameter( dbCommand, "ZipCodePlus4", DbType.String, 4 );

            rowsAffected = db.ExecuteNonQuery( dbCommand );

            attentionOf = ( ( db.GetParameterValue( dbCommand, "AttentionOf" ) != DBNull.Value ) ? Convert.ToString( db.GetParameterValue( dbCommand, "AttentionOf" ) ) : "" );
            addressID = ( ( db.GetParameterValue( dbCommand, "AddressID" ) != DBNull.Value ) ? Convert.ToInt32( db.GetParameterValue( dbCommand, "AddressID" ) ) : -1 );
            addressLine1 = ( ( db.GetParameterValue( dbCommand, "AddressLine1" ) != DBNull.Value ) ? Convert.ToString( db.GetParameterValue( dbCommand, "AddressLine1" ) ) : "" );
            addressLine2 = ( ( db.GetParameterValue( dbCommand, "AddressLine2" ) != DBNull.Value ) ? Convert.ToString( db.GetParameterValue( dbCommand, "AddressLine2" ) ) : "" );
			//city = ( ( db.GetParameterValue( dbCommand, "City" ) != DBNull.Value ) ? Convert.ToString( db.GetParameterValue( dbCommand, "City" ) ) : "" );
			city = GetParameterValueToString( db, dbCommand, "City" );


            //stateID = Convert.ToByte( db.GetParameterValue( dbCommand, "StateID" ) );
			state = ( ( db.GetParameterValue( dbCommand, "State" ) != DBNull.Value ) ? Convert.ToString( db.GetParameterValue( dbCommand, "State" ) ) : "" );


			//db.AddOutParameter( dbCommand, "StateID", DbType.Byte, 1 );
			//db.AddOutParameter( dbCommand, "State", DbType.String, 2 );

			zipCode = ( ( db.GetParameterValue( dbCommand, "ZipCode" ) != DBNull.Value ) ? Convert.ToString( db.GetParameterValue( dbCommand, "ZipCode" ) ) : "" );
			zipCodePlus4 = ( ( db.GetParameterValue( dbCommand, "ZipCodePlus4" ) != DBNull.Value ) ? Convert.ToString( db.GetParameterValue( dbCommand, "ZipCodePlus4" ) ) : "" );
            //int return_value  = Convert.SqlInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE);
        }


		

		public string GetParameterValueToString( Database db, DbCommand dbCommand, string parameterName )
		{
			//Database db = dbCommand.Connection; 
			return ( ( db.GetParameterValue( dbCommand, parameterName ) != DBNull.Value ) ? Convert.ToString( db.GetParameterValue( dbCommand, parameterName ) ) : "" );
		}




        public void Insert( int practiceID, bool isBillingCentralized, out int addressID, string attentionOf, string addressLine1, string addressLine2, string city, string state, string zipCode, string zipCodePlus4, int createdUserID )
        {
            int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_PracticeBillingAddress_Insert" );

            db.AddInParameter( dbCommand, "PracticeID", DbType.Int32, practiceID );
            db.AddInParameter( dbCommand, "IsBillingCentralized", DbType.Boolean, isBillingCentralized );

            db.AddOutParameter( dbCommand, "AddressID", DbType.Int32, 4 );
            db.AddInParameter( dbCommand, "AttentionOf", DbType.String, attentionOf );
            db.AddInParameter( dbCommand, "AddressLine1", DbType.String, addressLine1 );
            db.AddInParameter( dbCommand, "AddressLine2", DbType.String, addressLine2 );
            db.AddInParameter( dbCommand, "City", DbType.String, city );
            
			//  db.AddInParameter( dbCommand, "StateID", DbType.Byte, stateID );
			db.AddInParameter( dbCommand, "State", DbType.String, state );
			
			
			db.AddInParameter( dbCommand, "ZipCode", DbType.String, zipCode );
            db.AddInParameter( dbCommand, "ZipCodePlus4", DbType.String, zipCodePlus4 );
            db.AddInParameter( dbCommand, "CreatedUserID", DbType.Int32, createdUserID );

            rowsAffected = db.ExecuteNonQuery( dbCommand );

            addressID = ( ( db.GetParameterValue( dbCommand, "AddressID" ) != DBNull.Value ) ? Convert.ToInt32( db.GetParameterValue( dbCommand, "AddressID" ) ) : -1 );
        }


        public void UpdatePracticeBillingAddress( int practiceID, int addressID, string attentionOf, string addressLine1, string addressLine2
                                               , string city, string state, string zipCode, string zipCodePlus4, int userID )
        {
            int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_PracticeBillingAddress_Update" );

            db.AddInParameter( dbCommand, "PracticeID", DbType.Int32, practiceID );
            db.AddInParameter( dbCommand, "AddressID", DbType.Int32, addressID );
            db.AddInParameter( dbCommand, "AttentionOf", DbType.String, attentionOf );
            db.AddInParameter( dbCommand, "AddressLine1", DbType.String, addressLine1 );
            db.AddInParameter( dbCommand, "AddressLine2", DbType.String, addressLine2 );
            db.AddInParameter( dbCommand, "City", DbType.String, city );
            //  db.AddInParameter( dbCommand, "StateID", DbType.Byte, stateID );

			//  db.AddInParameter( dbCommand, "StateID", DbType.Byte, stateID );
			db.AddInParameter( dbCommand, "State", DbType.String, state );
			
			db.AddInParameter( dbCommand, "ZipCode", DbType.String, zipCode );
            db.AddInParameter( dbCommand, "ZipCodePlus4", DbType.String, zipCodePlus4 );
            db.AddInParameter( dbCommand, "ModifiedUserID", DbType.Int32, userID );

            rowsAffected = db.ExecuteNonQuery( dbCommand );
        }


		public static void UpdatePracticeLocationBillingAddresses( int practiceID, int userID )
		{

		    Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeBillingAddress_UpdateCentralDecentral");

		    db.AddInParameter( dbCommand, "PracticeID", DbType.Int32, practiceID );
		    db.AddInParameter( dbCommand, "ModifiedUserID", DbType.Int32, userID );

		    int rowsAffected = db.ExecuteNonQuery( dbCommand );
		}


		// called when Practice Billing address has be set to false.
        public void UpdatePracticeIsBillingCentralized(int practiceID, bool isBillingCentralized, int userID,int addressId)
        {
            int rowsAffected = 0;
            
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_Practice_Update_IsBillingCentralized");
            
            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            db.AddInParameter(dbCommand, "IsBillingCentralized", DbType.Boolean, isBillingCentralized);
            db.AddInParameter(dbCommand, "ModifiedUserID", DbType.Int32, userID);
            db.AddInParameter(dbCommand, "AddressID", DbType.Int32, addressId);
            
            rowsAffected = db.ExecuteNonQuery(dbCommand);
            
            //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
        }

        public bool AddressExistsForPracticeLocation(int practiceLocationId)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("usp_PracticeLocationBillingAddressExists");

            db.AddInParameter(dbCommand, "PracticeLocationId", DbType.Int32, practiceLocationId);
            
            return (bool)db.ExecuteScalar(dbCommand);
        }
        
    }
}