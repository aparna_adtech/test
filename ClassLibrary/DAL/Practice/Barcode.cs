﻿using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;  //needed for the dbCommand object!!!!
using BLL.Practice;

namespace DAL.Practice
{
    public class Barcode
    {
        public static DataSet GetPracticeCatalogDataBySupplier1(string SearchCriteria, string SearchText, int PracticeID)
        {

            DataSet ds = null;
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetPracticeCatalogProductInfoMCTOPC");
            db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
            db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, PracticeID);

            ds = db.ExecuteDataSet(dbCommand);

            return ds;
        }
    }
}
