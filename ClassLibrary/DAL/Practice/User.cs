using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using BLL;
using System.Web.Security;

namespace DAL.Practice
{
    /// <summary>
    /// Summary description for Physician
    /// </summary>
    public class User
    {
        public User()
        {
            //
            // Add constructor logic here
            //
        }

        public bool IsAllowedZeroQtyDispense(string UserName)
        {
            //DataTable userData = SelectUser(UserName);

            //if (userData.Rows.Count == 1)
            //{
            //    DataRow currentUser = userData.Rows[0];

            //    return Convert.ToBoolean(currentUser["AllowZeroQtyDispense"]);
            //}
            //return false;

            // NOTE: always return true for backwards compatibility
            return true;
        }

        public DataTable SelectUser(string UserName)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.USP_User_SelectByUserName"))
            {
                db.AddInParameter(dbCommand, "UserName", DbType.String, UserName);

                DataTable dt = new DataTable();
                dt = db.ExecuteDataSet(dbCommand).Tables[0];
                dbCommand.Dispose();
                return dt;
            }
        }

        public DataTable SelectUser(string UserName, int PracticeID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.USP_User_SelectByUserNameAndPracticeID"))
            {
                db.AddInParameter(dbCommand, "UserName", DbType.String, UserName);
                db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, PracticeID);

                DataTable dt = new DataTable();
                dt = db.ExecuteDataSet(dbCommand).Tables[0];
                dbCommand.Dispose();
                return dt;
            }
        }

        public DataTable SelectAllUserFullNames(int practiceID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_User_Select_All_FullNames_ByPracticeID"))
            {
                db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);

                DataTable dt = new DataTable();
                dt = db.ExecuteDataSet(dbCommand).Tables[0];
                dbCommand.Dispose();
                return dt;
            }
        }


        public DataTable SelectAllUserLogins(int practiceID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_User_Select_All_Logins_ByPracticeID"))
            {
                db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);

                DataTable dt = new DataTable();
                dt = db.ExecuteDataSet(dbCommand).Tables[0];
                dbCommand.Dispose();
                return dt;
            }
        }

        //public bool UpdateDefaultPreferences(
        //    int PracticeID, int UserID, bool ShowProductInfo, bool AllowDispense, bool AllowInventory,
        //    bool AllowCheckIn, bool AllowReports, bool AllowCart, bool AllowDispenseModification,
        //    int DefaultLocationID, int DefaultRecordsPerPage, string DefaultDispenseSearchCriteria,
        //    bool AllowZeroQtyDispense, bool ScrollEntirePage)
        //{
        //    return UpdateDefaultPreferences(PracticeID, UserID, ShowProductInfo, AllowDispense, AllowInventory, AllowCheckIn, AllowReports, AllowCart, AllowDispenseModification, DefaultLocationID, DefaultRecordsPerPage, DefaultDispenseSearchCriteria, AllowZeroQtyDispense, ScrollEntirePage, false, false);
        //}

        public bool UpdateDefaultPreferences(
                                                int PracticeID, 
                                                int UserID, 
                                                bool ShowProductInfo,  
                                                bool AllowDispense, 
                                                bool AllowInventory,
                                                bool AllowCheckIn, 
                                                bool AllowReports, 
                                                bool AllowCart, 
                                                bool AllowDispenseModification,
                                                int DefaultLocationID, 
                                                int DefaultRecordsPerPage, 
                                                string DefaultDispenseSearchCriteria,
                                                bool ScrollEntirePage, 
                                                bool DispenseQueueSortDescending = false, //begin optional Params
                                                bool AllowPracticeUserOrderWithoutApproval = false, 
                                                bool allowInventoryClose = false)
        {
            var db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            using (var dbCommand = db.GetStoredProcCommand("dbo.usp_User_Update_DefaultPreferences"))
            {
                db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, PracticeID);
                db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
                db.AddInParameter(dbCommand, "ShowProductInfo", DbType.Boolean, ShowProductInfo);
                db.AddInParameter(dbCommand, "AllowDispense", DbType.Boolean, AllowDispense);
                db.AddInParameter(dbCommand, "AllowInventory", DbType.Boolean, AllowInventory);
                db.AddInParameter(dbCommand, "AllowCheckIn", DbType.Boolean, AllowCheckIn);
                db.AddInParameter(dbCommand, "AllowReports", DbType.Boolean, AllowReports);
                db.AddInParameter(dbCommand, "AllowCart", DbType.Boolean, AllowCart);
                db.AddInParameter(dbCommand, "AllowDispenseModification", DbType.Boolean, AllowDispenseModification);
                db.AddInParameter(dbCommand, "DefaultLocationID", DbType.Int32, DefaultLocationID);
                db.AddInParameter(dbCommand, "DefaultRecordsPerPage", DbType.Int32, DefaultRecordsPerPage);
                db.AddInParameter(dbCommand, "DefaultDispenseSearchCriteria", DbType.String, DefaultDispenseSearchCriteria);
                db.AddInParameter(dbCommand, "AllowZeroQtyDispense", DbType.Boolean, true); // NOTE: setting to always true for backwards compatibility
                db.AddInParameter(dbCommand, "ScrollEntirePage", DbType.Boolean, ScrollEntirePage);
                db.AddInParameter(dbCommand, "DispenseQueueSortDescending", DbType.Boolean, DispenseQueueSortDescending);
                db.AddInParameter(dbCommand, "AllowPracticeUserOrderWithoutApproval", DbType.Boolean, AllowPracticeUserOrderWithoutApproval);
                db.AddInParameter(dbCommand, "AllowInventoryClose", DbType.Boolean, allowInventoryClose);
                return (db.ExecuteNonQuery(dbCommand) > 0);
            }
        }

        public int InsertUser(int practiceID, string UserName, int CreatedUserID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_User_Insert"))
            {
                db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
                db.AddInParameter(dbCommand, "UserName", DbType.String, UserName);
                db.AddInParameter(dbCommand, "CreatedUserID", DbType.Int32, CreatedUserID);
                db.AddOutParameter(dbCommand, "UserID", DbType.Int32, 4);
                
                
                int ret = db.ExecuteNonQuery(dbCommand);
                
                return Convert.ToInt32(db.GetParameterValue(dbCommand, "UserID"));
            }
        }

        //[usp_User_Insert]
        //  @PracticeID AS INT
        //, @UserName AS NVARCHAR (256)
        //, @CreatedUserID AS INT
        //, @UserID INT OUTPUT

        //usp_User_Delete
        public int DeleteUser(int practiceID, int UserID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_User_Delete"))
            {
                db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
                db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);


                int ret = db.ExecuteNonQuery(dbCommand);

                return ret;
            }
        }


        public int ActivateUser(int practiceID, int UserID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_User_Activate"))
            {
                db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
                db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);


                int ret = db.ExecuteNonQuery(dbCommand);

                return ret;
            }
        }

        /// <summary>
        /// Creates a user in Aspnetdb and the Vision User Table
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="practiceID"></param>
        /// <returns></returns>
        public static int CreateVisionLiteUser(ref string userName, string password, int practiceID, string email, int practiceLocationID, string originalUserName)
        {

            string errString = string.Empty;
            int creatingUserID = 1;
            string adminRole = "PracticeAdmin";
            string visionLiteRole = "VisionLite";
            int userID = 0;
            try
            {
                BLL.Practice.User editUser = new BLL.Practice.User();
                MembershipUser editMembershipUser = null;

                try
                {
                    //the user is required to choose a unique name during signup so we know it's valid here
                    //userName = GetUniqueUsername(userName, originalUserName);
                    editMembershipUser = Membership.CreateUser(userName, password);

                    editMembershipUser.Email = email;
                    Membership.UpdateUser(editMembershipUser);

                    //We add to the bregvision user's table here

                    userID = editUser.InsertUser(practiceID, userName, creatingUserID);

                    string[] AllRoles = Roles.GetAllRoles();
                    foreach (string roleName in AllRoles)
                    {
                        if (Roles.IsUserInRole(userName, roleName))
                        {
                            Roles.RemoveUserFromRole(userName, roleName);
                        }
                    }
                    Roles.AddUserToRole(userName, adminRole);
                    Roles.AddUserToRole(userName, visionLiteRole);

                    editUser.UpdateDefaultPreferences(practiceID, userID, true, true, true, true,
                                                      true, true, true, practiceLocationID, 10,
                                                      "code", true, false, false, false);

                    BLL.PracticeLocation.PracticeLocation updatePracticeLocation = new BLL.PracticeLocation.PracticeLocation();
                    updatePracticeLocation.SetUserPermissions(practiceLocationID, userID, true);
                }
                catch (MembershipCreateUserException ex)
                {
                    errString = ex.Message;
                    throw new ApplicationException(errString, ex);
                }
                return userID;
            }
            catch (System.ArgumentException exa)
            {
                switch (exa.Message)
                {
                    case "The length of parameter 'newPassword' needs to be greater or equal to '7'.":
                        errString = "The new password must contain at least 7 characters with one of the characters alpha-numeric";
                        break;
                    case "Non alpha numeric characters in 'newPassword' needs to be greater than or equal to '1'.":
                        errString = "The new password must contain at least 7 characters with one of the characters alpha-numeric";
                        break;
                    default:
                        errString = exa.Message;
                        break;
                }
                throw new ApplicationException(errString, exa);
            }
            catch (Exception ex)
            {
                string errMessage = "The new password must contain at least 7 characters with one of the characters alpha-numeric";

                if (ex.Message.Contains("password") && ex.Message.Contains("invalid"))
                {
                    errString = errMessage;
                }
                else
                {
                    errString = ex.Message;
                }
                throw new ApplicationException(errString, ex);
            }
        }

        private static string GetUniqueUsername(string userName, string originalUserName)
        {
            MembershipUser existingMembershipUser  = Membership.GetUser(userName);
   
            while (existingMembershipUser != null)
            {
                userName = GenerateUniqueName(userName, originalUserName);
                existingMembershipUser = Membership.GetUser(userName);
            }
 
            return userName;
        }

        private static string GenerateUniqueName(string userName, string originalUserName)
        {
            int numericEnding = 0;
            string ending = userName.Replace(originalUserName, "");
            int.TryParse(ending, out numericEnding);
            numericEnding += 1;
            return originalUserName + numericEnding.ToString();  
        }
    }
}