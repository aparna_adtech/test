﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using BLL;

namespace DAL.Practice
{
    class UserClick
    {
        public UserClick()
        {

        }

            //@UserID int,
            //@UserName nvarchar(100),
            //@PracticeID int,
            //@PracticeLocationID int = null,
            //@PageName nvarchar(50),
            //@Area nvarchar(50),
            //@Action nvarchar(50),
            //@Promotion nvarchar(50) = null,
            //@UserDefined1 nvarchar(50) = null,
            //@UserDefined2 nvarchar(50) = null,
            //@UserDefined3 nvarchar(50) = null,
            //@UserDefined4 nvarchar(50) = null

        public int InsertUserClick(int UserID, string UserName, int PracticeID, int? PracticeLocationID, string PageName, string Area, string Item, string Action, string Promotion, string UserDefined1, string UserDefined2, string UserDefined3, string UserDefined4)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_UserClick_Insert"))
            {
                db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
                db.AddInParameter(dbCommand, "UserName", DbType.String, UserName);
                db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, PracticeID);
                db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);
                db.AddInParameter(dbCommand, "PageName", DbType.String, PageName);

                db.AddInParameter(dbCommand, "Area", DbType.String, Area);
                db.AddInParameter(dbCommand, "Item", DbType.String, Item);
                db.AddInParameter(dbCommand, "Action", DbType.String, Action);
                db.AddInParameter(dbCommand, "Promotion", DbType.String, Promotion);

                db.AddInParameter(dbCommand, "UserDefined1", DbType.String, UserDefined1);
                db.AddInParameter(dbCommand, "UserDefined2", DbType.String, UserDefined2);
                db.AddInParameter(dbCommand, "UserDefined3", DbType.String, UserDefined3);
                db.AddInParameter(dbCommand, "UserDefined4", DbType.String, UserDefined4);

                int ret = db.ExecuteNonQuery(dbCommand);

                return Convert.ToInt32(db.GetParameterValue(dbCommand, "UserID"));
            }
        }
    }
}
