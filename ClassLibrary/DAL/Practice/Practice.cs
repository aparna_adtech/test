using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.Security;
using ClassLibrary.DAL;
using ClassLibrary.DAL.Helpers;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DAL.Practice
{
	public class Practice
	{
		#region Constructors

		public Practice()
		{
			// Add constructor logic here
		}

		#endregion

		#region Methods

        public static bool IsConsignment(Int32 practiceLocationID) //this will be IsConsignment
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var query = from pi in visionDB.ProductInventories
                            where pi.PracticeLocationID == practiceLocationID
                            && pi.IsConsignment == true
                            && pi.ConsignmentQuantity > 0
                            select pi;

                var isConsignment = query.FirstOrDefault();
                if (isConsignment != null)
                {
                    return true;
                }
            }
            return false;
        }

		public static bool IsSpark(Int32 practiceID)
		{
			VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

			using (visionDB)
			{
				var query = from bl in visionDB.BregVisionMonthlyFeeLevels
							join pbl in visionDB.Practice_MonthlyFeeLevels on bl.BregVisionMonthlyFeeLevelID equals pbl.BregVisionMonthlyFeeLevelID
							where pbl.PracticeID == practiceID
							&& bl.LevelName.ToLower().Contains("spark")
							select bl;

				var billingLevel = query.FirstOrDefault();
				if (billingLevel != null)
				{
					return true;                    
				}
			}
			return false;
		}

		#region SelectAll

		public DataSet SelectAll()
		{
			Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
			using( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_Practice_Select_All_Names" ) )
			{
				DataSet ds = new DataSet();
				ds = db.ExecuteDataSet( dbCommand ); 
				return ds;
			}
		}

		public static IDataReader GetPracticeLocationInfo(string username)
		{
			Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
			DbCommand dbCommand = db.GetStoredProcCommand("dbo.USP_GetPracticeAndPracticeLocation2");

			db.AddInParameter(dbCommand, "UserName", DbType.String, username);

			IDataReader reader = db.ExecuteReader(dbCommand);
			return reader;
		}

		#endregion

		#region Select One
		public static ClassLibrary.DAL.Practice Select(int practiceID)
		{
			VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
			using (visionDB)
			{
				var practiceQuery = from p in visionDB.Practices
									where p.PracticeID == practiceID
									select p;

				return practiceQuery.FirstOrDefault();
			}
		}
		#endregion

        //-------------------added by u= 09/03/2013
        static public bool SelectFaxDispense(int practiceID) 
        {
        	Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_Practice_Select_FaxDispenseEnabled"))
            {
                db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
                db.AddOutParameter(dbCommand, "FaxDispenseReceiptEnabled", DbType.Boolean, 0);

                db.ExecuteNonQuery(dbCommand);

                bool bFaxDispenseEnabled = Convert.ToBoolean(db.GetParameterValue(dbCommand, "FaxDispenseReceiptEnabled"));
                return bFaxDispenseEnabled;
            }
        }

        //------------------

		#region SelectName

		static public string SelectName( int practiceID)
		{
			Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
			using ( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_Practice_Select_Name" ) )
			{
				db.AddInParameter( dbCommand, "PracticeID", DbType.Int32, practiceID );
				db.AddOutParameter( dbCommand, "PracticeName", DbType.String, 50 );

				db.ExecuteNonQuery( dbCommand );

				string practiceName = Convert.ToString( db.GetParameterValue( dbCommand, "PracticeName" ) );

				dbCommand.Dispose();

				return practiceName;
			}
		}

		#endregion


		#region Insert

		public int Insert( BLL.Practice.Practice bllP )
		//public int Insert( string practiceName, int userID)
		//public int Insert( string practiceName)
		{
			int rowsAffected = 0;
			int practiceID = -1;

			//int practiceID;
			string practiceName = bllP.PracticeName;
			int userID = bllP.UserID;
			bool isOrthoSelect = bllP.IsOrthoSelect;
			
			Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
			using ( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_Practice_Insert" ) )           
			{
		 
				db.AddOutParameter(dbCommand, "PracticeID", DbType.Int32, 4);
				db.AddInParameter(dbCommand, "PracticeName", DbType.String, practiceName);
				//db.AddInParameter(dbCommand, "CreatedUserID", DbType.Int32, userID);
				db.AddInParameter( dbCommand, "CreatedUserID", DbType.Int32, userID );
				db.AddInParameter(dbCommand, "IsOrthoSelect", DbType.Boolean, isOrthoSelect);
				db.AddInParameter(dbCommand, "IsVisionLite", DbType.Boolean, bllP.IsVisionLite);
				db.AddInParameter(dbCommand, "PaymentTypeID", DbType.Int32, (int)bllP.PaymentType);
				db.AddInParameter(dbCommand, "CameraScanEnabled", DbType.Boolean, bllP.CameraScanEnabled);
				db.AddInParameter(dbCommand, "IsBregBilling", DbType.Boolean, bllP.IsBregBilling);
				db.AddInParameter(dbCommand, "FaxDispenseReceiptEnabled", DbType.Boolean, bllP.FaxDispenseReceiptEnabled);
                db.AddInParameter(dbCommand, "EMRIntegrationPullEnabled", DbType.Boolean, bllP.EMRIntegrationPullEnabled);
                db.AddInParameter(dbCommand, "IsCustomFitEnabled", DbType.Boolean, bllP.IsCustomFitEnabled);
                db.AddInParameter(dbCommand, "IsSecureMessagingInternalEnabled", DbType.Boolean, bllP.SecureMessagingInternalEnabled);
                db.AddInParameter(dbCommand, "IsSecureMessagingExternalEnabled", DbType.Boolean, bllP.SecureMessagingExternalEnabled);

				rowsAffected = db.ExecuteNonQuery( dbCommand );
				
				practiceID = Convert.ToInt32(db.GetParameterValue(dbCommand, "PracticeID"));
				//int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE")); 
				dbCommand.Dispose();
			}
			return practiceID;
		}

		#endregion


		#region Update

		//public void Update( int practiceID, string practiceName, int userID )
		//public void Update( int practiceID, string practiceName )
		public void Update( BLL.Practice.Practice bllP )
		{
			int rowsAffected = 0;

			int practiceID = bllP.PracticeID;
			string practiceName = bllP.PracticeName;
			int userID = bllP.UserID;
			bool isOrthoSelect = bllP.IsOrthoSelect;
			
			Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );


			using ( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_Practice_Update_Name" ) )
			{
				db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID );
				db.AddInParameter(dbCommand, "PracticeName", DbType.String, practiceName );
				db.AddInParameter(dbCommand, "ModifiedUserID", DbType.Int32, userID );
				db.AddInParameter(dbCommand, "IsOrthoSelect", DbType.Boolean, isOrthoSelect);
				db.AddInParameter(dbCommand, "PaymentTypeID", DbType.Int32, (int)bllP.PaymentType);
				db.AddInParameter(dbCommand, "PracticeCode", DbType.String, bllP.PracticeCode);
				db.AddInParameter(dbCommand, "BillingStartDate", DbType.DateTime, bllP.BillingStartDate);
				db.AddInParameter(dbCommand, "CameraScanEnabled", DbType.Boolean, bllP.CameraScanEnabled);
				db.AddInParameter(dbCommand, "IsBregBilling", DbType.Boolean, bllP.IsBregBilling);
				db.AddInParameter(dbCommand, "FaxDispenseReceiptEnabled", DbType.Boolean, bllP.FaxDispenseReceiptEnabled);
                db.AddInParameter(dbCommand, "EMRIntegrationPullEnabled", DbType.Boolean, bllP.EMRIntegrationPullEnabled);
                db.AddInParameter(dbCommand, "IsCustomFitEnabled", DbType.Boolean, bllP.IsCustomFitEnabled);
                db.AddInParameter(dbCommand, "IsSecureMessagingInternalEnabled", DbType.Boolean, bllP.SecureMessagingInternalEnabled);
                db.AddInParameter(dbCommand, "IsSecureMessagingExternalEnabled", DbType.Boolean, bllP.SecureMessagingExternalEnabled);

				rowsAffected = db.ExecuteNonQuery( dbCommand ); 
			}
		}

		public static void SetIsVisionLite(int practiceId, bool val)
		{
			VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

			using (visionDB)
			{
				var practiceQuery = from p in visionDB.Practices
									where p.PracticeID == practiceId
									select p;

				ClassLibrary.DAL.Practice practice = practiceQuery.FirstOrDefault();
				practice.IsVisionLite = val;
				visionDB.SubmitChanges();
			}
		}

		public static void UpdatePracticeViaLinq(ClassLibrary.DAL.Practice practice)
		{
			VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

			using (visionDB)
			{
				visionDB.Practices.Attach(practice, true);
				visionDB.SubmitChanges();
			}
		}
		public void UpdateIsActive(BLL.Practice.Practice bllP, bool isActive)
		{
			//if isActive state changes from orig value then update practice table 
			//and also all the users for the practice. 
			VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

			using (visionDB)
			{
				var query = from p in visionDB.Practices
					where p.PracticeID == bllP.PracticeID
					select p;

				ClassLibrary.DAL.Practice practice = query.FirstOrDefault<ClassLibrary.DAL.Practice>();

				if (practice.IsActive != isActive)
				{
					practice.IsActive = isActive;
					visionDB.SubmitChanges();
					
					//now update all the users for the practice
					DAL.Practice.User dalPU = new DAL.Practice.User();
					DataTable dt = dalPU.SelectAllUserLogins(bllP.PracticeID);

					foreach (DataRow dr in dt.Rows)
					{
						string userName = dr["UserName"].ToString(); //AU.UserName
						int userId = int.Parse(dr["UserID"].ToString()); //U.UserID

						//update our user table
						var queryUsers = from u in visionDB.Users
							where u.UserID == userId
							select u;
						
						ClassLibrary.DAL.User user = queryUsers.FirstOrDefault<ClassLibrary.DAL.User>();
						user.IsActive = isActive;
						visionDB.SubmitChanges();

						//update asp.net user table
						MembershipUser editMembershipUser = null;
						editMembershipUser = Membership.GetUser(userName);
						editMembershipUser.IsApproved = isActive;

						Membership.UpdateUser(editMembershipUser);
					}
				}
			}
		}

		public void UpgradeDowngradePractice(BLL.Practice.Practice bllP, bool isVisionLite)
		{
			//if isVisionLite state changes from orig value then update practice table 
			//and also the users role for the practice. 
			VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

			using (visionDB)
			{
				var query = from p in visionDB.Practices
							where p.PracticeID == bllP.PracticeID
							select p;

				ClassLibrary.DAL.Practice practice = query.FirstOrDefault<ClassLibrary.DAL.Practice>();

				if (practice.IsVisionLite != isVisionLite)
				{
					practice.IsVisionLite = isVisionLite;
					visionDB.SubmitChanges();

					//now update all the users for the practice
					DAL.Practice.User dalPU = new DAL.Practice.User();
					DataTable dt = dalPU.SelectAllUserLogins(bllP.PracticeID);

					foreach (DataRow dr in dt.Rows)
					{
						string userName = dr["UserName"].ToString(); //AU.UserName
						
						//update asp.net user role
						if (isVisionLite)
						{
							Roles.AddUserToRole(userName, "VisionLite");
						}
						else
						{
							try
							{
								Roles.RemoveUserFromRole(userName, "VisionLite");
							}
							catch (Exception ex)
							{
								//do nothing
							}
						}
					}
				}
			}
		}
		#endregion


		#region Delete

		//public void Delete( int practiceID, int userID)
		public void Delete( BLL.Practice.Practice bllP )
		{
			int rowsAffected = 0;
			int practiceID = bllP.PracticeID;
			int userID = bllP.UserID;

			Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );

			using ( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_Practice_Delete" ) )
			{
				db.AddInParameter( dbCommand, "PracticeID", DbType.Int32, practiceID );
				//db.AddInParameter( dbCommand, "ModifiedUserID", DbType.Int32, userID );
				db.AddInParameter( dbCommand, "ModifiedUserID", DbType.Int32, userID );

				rowsAffected = db.ExecuteNonQuery( dbCommand ); 
				dbCommand.Dispose();
			}
		}

		#endregion

		#region SetPracticeSession
		public static void SetPracticeSession()
		{
			//HttpContext.Current.Session
			string username = HttpContext.Current.User.Identity.Name;
			IDataReader reader;

			reader = GetPractice(username);

			if (reader.Read())
			{
				HttpContext.Current.Session["PracticeName"] = reader["PracticeName"].ToString();
				HttpContext.Current.Session["PracticeID"] = reader["PracticeID"].ToString();
                HttpContext.Current.Session["PracticeIsCustomFitEnabled"] = reader["PracticeIsCustomFitEnabled"].ToString();

				reader.Close();
			}
		}

		public static IDataReader GetPractice(string username)
		{
			IDataReader reader;
			Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
			DbCommand dbCommand = db.GetStoredProcCommand("dbo.USP_GetPracticeAndPracticeLocation2");

			db.AddInParameter(dbCommand, "UserName", DbType.String, username);

			reader = db.ExecuteReader(dbCommand);
			return reader;
		}
		#endregion

		#region Practice Setup Wizard

		public static DataRow GetSetupPageStatus(string currentPage, int practiceID)
		{
			DataTable statusTable = new DataTable();

			VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
			using (visionDB)
			{
				var setupStatusTitles = from st in visionDB.SetupStatusTitles
										where st.IsPracticeLocation == false
										&& st.UserControlName == currentPage
										select st;


				var practiceQuery = from practicePageStatus in visionDB.SetupStatus
									where practicePageStatus.PracticeID == practiceID
									&& practicePageStatus.UserControlName == currentPage
									&& practicePageStatus.PracticeLocationID < 1
									select new
									{
										practicePageStatus.UserControlName,
										PageStatus = ((practicePageStatus == null) ? 0 : practicePageStatus.PageStatus) == 0 ? "N" : "Y"
									};

				var pageStatus = setupStatusTitles.FirstOrDefault();

				DataTableHelper.CreateColumn("Page", "Page", "System.String", statusTable);
				DataTableHelper.CreateColumn("PageStatus", "PageStatus", "System.String", statusTable);


				var statusItem = practiceQuery.FirstOrDefault();
				string statusValue = (statusItem != null) ? statusItem.PageStatus : "N";
				if (pageStatus != null)
				{
					DataRow row = statusTable.NewRow();
					row["Page"] = pageStatus.PageTitle;
					row["PageStatus"] = statusValue;
					statusTable.Rows.Add(row);
					return statusTable.Rows[0];
				}

				return null;
			}
			
		}

		public static DataTable GetPracticeSetupStatus(int practiceID)
		{
			DataTable statusTable = new DataTable();

			VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
			using (visionDB)
			{
				var setupStatusTitles = from st in visionDB.SetupStatusTitles
										where st.IsPracticeLocation == false
										orderby st.DisplayOrder
										select st;


				var practiceQuery = from practicePageStatus in visionDB.SetupStatus 
									where practicePageStatus.PracticeID == practiceID
									&& practicePageStatus.PracticeLocationID < 1
									select new
									{
										practicePageStatus.UserControlName,
										PageStatus = ((practicePageStatus == null) ? 0 : practicePageStatus.PageStatus) == 0 ? "N" : "Y"
									};



				// add a column for the page 
				DataTableHelper.CreateColumn("Page", "Page", "System.String", statusTable);
				DataTableHelper.CreateColumn("PageStatus", "PageStatus", "System.String", statusTable);
				

				foreach (var pageStatus in setupStatusTitles)
				{
					var queryPageStatus = from currentPageStatus in practiceQuery
										  where currentPageStatus.UserControlName == pageStatus.UserControlName
										  select currentPageStatus;

					var statusItem = queryPageStatus.FirstOrDefault();
					string statusValue = (statusItem != null) ? statusItem.PageStatus : "N";

					DataRow row = statusTable.NewRow();
					row["Page"] = pageStatus.PageTitle;
					row["PageStatus"] = statusValue;
					statusTable.Rows.Add(row);
				}
			}

			return statusTable;
		}

		/// <summary>
		/// Populates the Practice Catalog with all Breg Products
		/// </summary>
		/// <param name="practiceId"></param>
		/// <param name="userId"></param>
		public static void PopulateCatalog(int practiceId, int userId)
		{
			int rowsAffected = 0;

			Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");

			using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeCatalogProduct_PopulateWithAllBregProducts"))
			{
				db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceId);
				db.AddInParameter(dbCommand, "UserID", DbType.Int32, userId);

				rowsAffected = db.ExecuteNonQuery(dbCommand);
				dbCommand.Dispose();
			}
		}

		#endregion

		#region PracticeFees
		public static List<ClassLibrary.DAL.BregVisionMonthlyFeeLevel> GetExpressPracticeFees()
		{
			VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

			using (visionDB)
			{
				var query = from fl in visionDB.BregVisionMonthlyFeeLevels
							where fl.LevelName.ToLower().Contains("express")
							select fl;

				return query.ToList<ClassLibrary.DAL.BregVisionMonthlyFeeLevel>();
			}
		}

		public static void SetPracticeFee(Int32 practiceID, Int32 bregVisionMonthlyFeeLevelID, DateTime startDate, Decimal discount)
		{
			VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

			using (visionDB)
			{
				Practice_MonthlyFeeLevel practiceFeeLevel = new Practice_MonthlyFeeLevel();
				practiceFeeLevel.PracticeID = practiceID;
				practiceFeeLevel.BregVisionMonthlyFeeLevelID = bregVisionMonthlyFeeLevelID;
				practiceFeeLevel.StartDate = startDate;
				practiceFeeLevel.Discount = discount;

				practiceFeeLevel.CreatedDate = DateTime.Now;
				practiceFeeLevel.CreatedUserID = 1;
				practiceFeeLevel.ModifiedDate = DateTime.Now;
				practiceFeeLevel.ModifiedUserID = 1;
				practiceFeeLevel.IsActive = true;

				visionDB.Practice_MonthlyFeeLevels.InsertOnSubmit(practiceFeeLevel);
				visionDB.SubmitChanges();
				//VisionDataContext.CloseConnection(visionDB);
			}
			
		}
		#endregion
		#endregion

        public static bool IsHandheldPrintEnabled(int practiceId)
	    {
            using (var visionDb = VisionDataContext.GetVisionDataContext())
            {
                using (visionDb)
                {
                    return visionDb.Practices.Single(practice => practice.PracticeID == practiceId).IsHandheldPrintEnabled;
                }
            }
	    }

        public static bool IsBregBillingEnabled(int practiceLocationId)
        {
            using (var visionDb = VisionDataContext.GetVisionDataContext())
            {
                using (visionDb)
                {
                    int practiceId = visionDb.PracticeLocations.Single(practiceLocation => practiceLocation.PracticeLocationID == practiceLocationId).PracticeID;

                    if (practiceId > 0)
                        return visionDb.Practices.Single(practice => practice.PracticeID == practiceId).IsBregBilling;
                    else
                        return false;
                }
            }
        }
        public static bool IsCustomFitEnabled(int practiceId)
	    {
            using (var visionDb = VisionDataContext.GetVisionDataContext())
            {
                using (visionDb)
                {
                    return visionDb.Practices.Single(practice => practice.PracticeID == practiceId).IsCustomFitEnabled;
                }
            }
        }

	    public static bool IsDisplayBillChargeOnReceipt(int practiceId)
	    {
            using (var visionDb = VisionDataContext.GetVisionDataContext())
            {
                using (visionDb)
                {
                    return visionDb.Practices.Single(practice => practice.PracticeID == practiceId).IsDisplayBillChargeOnReceipt;
                }
            }
        }

		public static ClassLibrary.DAL.Practice GetPracticeById(int practiceId)
		{
			using (var visionDb = VisionDataContext.GetVisionDataContext())
			{
				using (visionDb)
				{
					return visionDb.Practices.Single(practice => practice.PracticeID == practiceId);
				}
			}
		}

		public static ClassLibrary.DAL.Practice GetPracticeByPracticeLocationId(int practiceLocationId)
		{
			using (var visionDb = VisionDataContext.GetVisionDataContext())
			{
				using (visionDb)
				{
					var query = from pl in visionDb.PracticeLocations
								join p in visionDb.Practices on pl.PracticeID equals p.PracticeID
								where pl.PracticeLocationID == practiceLocationId
								select p;

					ClassLibrary.DAL.Practice practice = query.FirstOrDefault<ClassLibrary.DAL.Practice>();
					return practice;
				}
			}
		}

		public static ClassLibrary.DAL.Contact GetContact(int practiceId)
	    {
            using (var visionDb = VisionDataContext.GetVisionDataContext())
            {
                using (visionDb)
                {
                    return visionDb.Practices.Single(practice => practice.PracticeID == practiceId).Contact;
                }
            }

	    }

	    public static string GeneralLedgerNumber(int practiceId)
	    {
            using (var visionDb = VisionDataContext.GetVisionDataContext())
            {
                using (visionDb)
                {
                    return visionDb.Practices.Single(practice => practice.PracticeID == practiceId).GeneralLedgerNumber;
                }
            }
        }

        public static bool CheckLateralityExemptions(List<int> practiceCatalogProductIds)
        {
            using (var visionDb = VisionDataContext.GetVisionDataContext())
            {
                return visionDb.PracticeCatalogProducts.Any(x => practiceCatalogProductIds.Contains(x.PracticeCatalogProductID) && !(x.IsLateralityExempt.HasValue? x.IsLateralityExempt.Value: false));
            }
         }

        public static bool IsLateralityRequired(int practiceId)
        {
            using (var visionDb = VisionDataContext.GetVisionDataContext())
            {
				var practice = visionDb.Practices.Where(p => p.PracticeID == practiceId).FirstOrDefault();
				if (practice == null)
					return false;
				
				var isLateralityFlag = practice.IsLateralityRequired;
				return (isLateralityFlag != null) ? isLateralityFlag.Value : false;
            }
        }
    }
}