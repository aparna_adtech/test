using System;
using System.Data;
using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;


namespace DAL.Practice
{
	/// <summary>
	/// Summary description for ThirdPartySupplier
	/// </summary>
	public class ThirdPartySupplier
	{

		#region Constructors

		public ThirdPartySupplier()
        {
            // Add constructor logic here
        }
		
		#endregion
		
		
		#region Methods
		
		
		    #region SelectAll

		    public DataTable SelectAll( int practiceID)
		    {
			    //int practiceID = bllThirdPartySupplier.PracticeID;
        
			    Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_ThirdPartySupplier_Select_All_By_PracticeID"))
			    {
				    db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
    		        
				    DataTable dtThirdPartySupplier = new DataTable();
				    dtThirdPartySupplier = db.ExecuteDataSet( dbCommand ).Tables[0];
				    dbCommand.Dispose();
    		        
				    //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
    		        
				    return dtThirdPartySupplier;
			    }

		    }
    		

		    #endregion


            #region SelectAllMCSuppliers

                public DataTable SelectAllMCSuppliers(int practiceID)
                {
                    Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                    using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeCatalogSupplierBrand_Select_All_MCSuppliers"))
                    {
                        db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);

                        DataTable dtMCSupplier = new DataTable();
                        dtMCSupplier = db.ExecuteDataSet(dbCommand).Tables[0];
                        dbCommand.Dispose();

                        return dtMCSupplier;
                    }
                }

            #endregion

            

		    #region SelectOne
    		
		    //public DataTable SelectOne( int ThirdPartySupplierID )
		    //{
		    //    //Add code to call to SelectOne proc
		    //}
    		
		    #endregion

    		
		    #region Insert

                public void Insert(out int thirdPartySupplierID, int practiceID, string supplierName, string supplierShortName, string faxOrderPlacement
                                                            , string emailOrderPlacement, bool isEmailOrderPlacement, bool isVendor, int createdUserID)
                {
                    int rowsAffected = 0;

                    Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                    DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_ThirdPartySupplier_Insert");

                    db.AddOutParameter(dbCommand, "ThirdPartySupplierID", DbType.Int32, 4);
                    db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
                    db.AddInParameter(dbCommand, "SupplierName", DbType.String, supplierName);
                    db.AddInParameter(dbCommand, "SupplierShortName", DbType.String, supplierShortName);
                    db.AddInParameter(dbCommand, "FaxOrderPlacement", DbType.String, faxOrderPlacement);
                    db.AddInParameter(dbCommand, "EmailOrderPlacement", DbType.String, emailOrderPlacement);
                    db.AddInParameter(dbCommand, "IsEmailOrderPlacement", DbType.Boolean, isEmailOrderPlacement);
                    db.AddInParameter(dbCommand, "IsVendor", DbType.Boolean, isVendor);
                    db.AddInParameter(dbCommand, "CreatedUserID", DbType.Int32, createdUserID);

                    rowsAffected = db.ExecuteNonQuery(dbCommand);

                    thirdPartySupplierID = Convert.ToInt32(db.GetParameterValue(dbCommand, "ThirdPartySupplierID"));
                    //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
                }
    		
		    #endregion


		    #region Update
    		
		    //public void Update( BLL.ThirdPartySupplier bllThirdPartySupplier )
		    //{
		    //    //Add code to call to Update proc
		    //}
                public void Update(int thirdPartySupplierID, int practiceID, string supplierName, string supplierShortName, string faxOrderPlacement
                                                        , string emailOrderPlacement, bool isEmailOrderPlacement, bool isVendor, int modifiedUserID)
                {
                    int rowsAffected = 0;

                    Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                    DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_ThirdPartySupplier_Update");

                    db.AddInParameter(dbCommand, "ThirdPartySupplierID", DbType.Int32, thirdPartySupplierID);
                    db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
                    db.AddInParameter(dbCommand, "SupplierName", DbType.String, supplierName);
                    db.AddInParameter(dbCommand, "SupplierShortName", DbType.String, supplierShortName);
                    db.AddInParameter(dbCommand, "FaxOrderPlacement", DbType.String, faxOrderPlacement);
                    db.AddInParameter(dbCommand, "EmailOrderPlacement", DbType.String, emailOrderPlacement);
                    db.AddInParameter(dbCommand, "IsEmailOrderPlacement", DbType.Boolean, isEmailOrderPlacement);
                    db.AddInParameter(dbCommand, "IsVendor", DbType.Boolean, isVendor);
                    db.AddInParameter(dbCommand, "ModifiedUserID", DbType.Int32, modifiedUserID);

                    rowsAffected = db.ExecuteNonQuery(dbCommand);

                    //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
                }
    		
		    #endregion


		    #region Delete
    		
		    //public void Delete( BLL.ThirdPartySupplier bllThirdPartySupplier )
		    //{
		    //    //Update call to Delete proc
		    //}
        public Int32 Delete(int thirdPartySupplierID, int modifiedUserID)
        {
            int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_ThirdPartySupplier_Delete");

            db.AddInParameter(dbCommand, "ThirdPartySupplierID", DbType.Int32, thirdPartySupplierID);
            db.AddInParameter(dbCommand, "ModifiedUserID", DbType.Int32, modifiedUserID);

            rowsAffected = db.ExecuteNonQuery(dbCommand);

            return rowsAffected;
        }
    		
		    #endregion

		
		#endregion

		}


	} 

