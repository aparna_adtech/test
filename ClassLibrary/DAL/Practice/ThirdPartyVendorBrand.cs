using System;
using System.Data;
using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;


namespace DAL.Practice
{
	/// <summary>
	/// Summary description for ThirdPartySupplier
	/// </summary>
	public class ThirdPartyVendorBrand
	{

		#region Constructors

		public ThirdPartyVendorBrand()
        {
            // Add constructor logic here
        }
		
		#endregion
		
		
		#region Methods
		
		
		    #region SelectAll

		    public DataTable SelectAll( int thirdPartySupplierID)
		    {
			    //int practiceID = bllThirdPartyVendorBrand.PracticeID;
        
			    Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeCatalogSupplierBrand_Select_All_Brands_By_Vendor"))
			    {
				    db.AddInParameter(dbCommand, "ThirdPartySupplierID", DbType.Int32, thirdPartySupplierID);
    		        
				    DataTable dtThirdPartyVendorBrand = new DataTable();
				    dtThirdPartyVendorBrand = db.ExecuteDataSet( dbCommand ).Tables[0];
				    dbCommand.Dispose();
    		        
				    //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
    		        
				    return dtThirdPartyVendorBrand;
			    }

		    }
    		
		    #endregion



            #region GetVendorNames

                public void GetVendorNames(int thirdPartySupplierID, out string supplierName, out string supplierShortName)
                {
                    int rowsAffected = 0;

                    Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                    DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_ThirdPartySupplier_VendorName_Select_One_ByParams");

                    db.AddInParameter(dbCommand, "ThirdPartySupplierID", DbType.Int32, thirdPartySupplierID);
                    db.AddOutParameter(dbCommand, "SupplierName", DbType.String, 50);
                    db.AddOutParameter(dbCommand, "SupplierShortName", DbType.String, 50);

                    rowsAffected = db.ExecuteNonQuery(dbCommand);

                    supplierName = Convert.ToString(db.GetParameterValue(dbCommand, "SupplierName"));
                    supplierShortName = Convert.ToString(db.GetParameterValue(dbCommand, "SupplierShortName"));
                    //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
                }

            #endregion

                //    #region SelectOne
    		
            //    //public DataTable SelectOne( int ThirdPartySupplierID )
            //    //{
            //    //    //Add code to call to SelectOne proc
            //    //}
        	        	

            //    #endregion

        		
            #region Update

            public void Update(int practiceCatalogSupplierBrandID, string brandName, string brandShortName, int modifiedUserID)
            {
                int rowsAffected = 0;

                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeCatalogSupplierBrand_Update_Brands_By_Vendor");

                db.AddInParameter(dbCommand, "PracticeCatalogSupplierBrandID", DbType.Int32, practiceCatalogSupplierBrandID);
                db.AddInParameter(dbCommand, "BrandName", DbType.String, brandName);
                db.AddInParameter(dbCommand, "BrandShortName", DbType.String, brandShortName);
                db.AddInParameter(dbCommand, "ModifiedUserID", DbType.Int32, modifiedUserID);

                rowsAffected = db.ExecuteNonQuery(dbCommand);

                //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
            }

        		
            #endregion


            #region Insert

            public void Insert(out int practiceCatalogSupplierBrandID, int practiceID, int thirdPartySupplierID, string supplierName, string supplierShortName, string brandName, string brandShortName, int createdUserID)
            {
                int rowsAffected = 0;

                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeCatalogSupplierBrand_Insert_Brand");

                db.AddOutParameter(dbCommand, "PracticeCatalogSupplierBrandID", DbType.Int32, 4);
                db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
                db.AddInParameter(dbCommand, "ThirdPartySupplierID", DbType.Int32, thirdPartySupplierID);
                db.AddInParameter(dbCommand, "SupplierName", DbType.String, supplierName);
                db.AddInParameter(dbCommand, "SupplierShortName", DbType.String, supplierShortName);
                db.AddInParameter(dbCommand, "BrandName", DbType.String, brandName);
                db.AddInParameter(dbCommand, "BrandShortName", DbType.String, brandShortName);
                db.AddInParameter(dbCommand, "CreatedUserID", DbType.Int32, createdUserID);

                rowsAffected = db.ExecuteNonQuery(dbCommand);

                practiceCatalogSupplierBrandID = Convert.ToInt32(db.GetParameterValue(dbCommand, "PracticeCatalogSupplierBrandID"));
                //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
            }

        		
           #endregion


            #region Delete
            
            public int Delete(int practiceCatalogSupplierBrandID, int modifiedUserID, out int brandProductCount)
            {
                int rowsAffected = 0;

                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeCatalogSupplierBrand_Delete_Brand");

                db.AddInParameter(dbCommand, "PracticeCatalogSupplierBrandID", DbType.Int32, practiceCatalogSupplierBrandID);
                db.AddInParameter(dbCommand, "ModifiedUserID", DbType.Int32, modifiedUserID);
                db.AddOutParameter(dbCommand, "BrandProductCount", DbType.Int32, 4);

                rowsAffected = db.ExecuteNonQuery(dbCommand);

                brandProductCount = Convert.ToInt32(db.GetParameterValue(dbCommand, "BrandProductCount"));
                //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));

                return brandProductCount;  // if > 0 then this brand cannot be deleted.
            }

            #endregion

		
		#endregion

		}


	} 


