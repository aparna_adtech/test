﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

namespace DAL.Practice
{
	/// <summary>
	/// Summary description for ThirdPartySupplier
	/// </summary>
	public class ThirdPartyProduct
	{

		#region Constructors

		public ThirdPartyProduct()
        {
            // Add constructor logic here
        }
		
		#endregion
		
		
		#region Methods
		
            // SelectVendors
            public DataTable GetVendors(int practiceID)
            {
                //int rowsAffected = 0;

                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
               // DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_ThirdPartySupplier_SelectAll_Vendors_And_NonVendorSuppliers");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_VendorGrid_Vendor_SelectAll_Given_PracticeIDB");

                db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);

                DataSet ds = db.ExecuteDataSet(dbCommand);
                return ds.Tables[0];
            }

            public DataTable GetBrands(int thirdPartySupplierID)
            {
                //int rowsAffected = 0;

                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                //  DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_VendorGrid_PracticeCatalogSupplierBrand_SelectAll_Given_Vendor");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_VendorGrid_PracticeCatalogSupplierBrand_SelectAll_Given_VendorB");


                db.AddInParameter(dbCommand, "LinkThirdPartySupplierID", DbType.Int32, thirdPartySupplierID);

                DataSet ds = db.ExecuteDataSet(dbCommand);
                return ds.Tables[0];
            }


            public DataTable GetProducts(int practiceCatalogSupplierBrandID)
            {
                //int rowsAffected = 0;

                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_VendorGrid_ThirdPartyProduct_SelectAll_Given_PCSBID");

                db.AddInParameter(dbCommand, "PracticeCatalogSupplierBrandID", DbType.Int32, practiceCatalogSupplierBrandID);

                DataSet ds = db.ExecuteDataSet(dbCommand);
                return ds.Tables[0];
            }

            public int Insert(out int newThirdPartyProductID, int practiceCatalogSupplierBrandID, string productCode, string productName, string packaging
                        , string leftRightSide, string size, string color, string gender, string mod1, string mod2, string mod3, decimal wholesaleCost, string HCPCSString
                        , Boolean isHCPCSInserted, int userID, out string userMessage)
            {
                return Insert(out newThirdPartyProductID,  practiceCatalogSupplierBrandID,  productCode,  productName,  packaging
                        ,  leftRightSide,  size,  color,  gender, mod1, mod2, mod3, wholesaleCost,  HCPCSString
                        ,  isHCPCSInserted,  userID, "", out  userMessage);
            }
        
            public int Insert(out int newThirdPartyProductID, int practiceCatalogSupplierBrandID, string productCode, string productName, string packaging
                                , string leftRightSide, string size, string color, string gender, string mod1, string mod2, string mod3, decimal wholesaleCost, string HCPCSString
                                , Boolean isHCPCSInserted, int userID, string upcCode, out string userMessage)
            {
                int rowsInserted;
                userMessage = "";
                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_ThirdPartyProduct_Insert");

                //for testing only
                userID = 9876;

                db.AddOutParameter(dbCommand, "NewThirdPartyProductID", DbType.Int32, 4);
                db.AddInParameter(dbCommand, "practiceCatalogSupplierBrandID", DbType.Int32, practiceCatalogSupplierBrandID);
                db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
                db.AddInParameter(dbCommand, "ProductName", DbType.String, productName);
                db.AddInParameter(dbCommand, "Packaging", DbType.String, packaging);
                db.AddInParameter(dbCommand, "LeftRightSide", DbType.String, leftRightSide);
                db.AddInParameter(dbCommand, "Size", DbType.String, size);
                db.AddInParameter(dbCommand, "Color", DbType.String, color);
                db.AddInParameter(dbCommand, "Gender", DbType.String, gender);
                db.AddInParameter(dbCommand, "Mod1", DbType.String, mod1);
                db.AddInParameter(dbCommand, "Mod2", DbType.String, mod2);
                db.AddInParameter(dbCommand, "Mod3", DbType.String, mod3);
                db.AddInParameter(dbCommand, "WholesaleCost", DbType.Currency, wholesaleCost);
                db.AddInParameter(dbCommand, "HCPCSString", DbType.String, HCPCSString);
                db.AddInParameter(dbCommand, "IsHCPCSInserted", DbType.Boolean, isHCPCSInserted);
                db.AddInParameter(dbCommand, "userID", DbType.Int32, userID);
                db.AddInParameter(dbCommand, "UPCCODE", DbType.String, upcCode);
                db.AddOutParameter(dbCommand, "UserMessage", DbType.String, 50);

                rowsInserted = db.ExecuteNonQuery(dbCommand);

                userMessage = db.GetParameterValue(dbCommand, "UserMessage").ToString();
                newThirdPartyProductID = (db.GetParameterValue(dbCommand, "NewThirdPartyProductID").ToString() != null) ? Convert.ToInt32(db.GetParameterValue(dbCommand, "newThirdPartyProductID").ToString()) : -1;
                
                //dependencyMessage = db.GetParameterValue(dbCommand, "DependencyMessage").ToString();
                //userMessage = (dbCommand.Parameters["UserMessage"] != null) ? dbCommand.Parameters["UserMessage"].Value.ToString() : "";

                return rowsInserted;
            }

            public int Update(int thirdPartyProductID, int practiceCatalogSupplierBrandID, string productCode, string productName
                        , string packaging, string leftRightSide, string size, string color, string gender, string mod1, string mod2, string mod3
                        , decimal wholesaleCost, string HCPCSString, bool isHCPCSUpdated, int userID, out string userMessage)
            {
                return Update(thirdPartyProductID, practiceCatalogSupplierBrandID, productCode, productName
                                , packaging, leftRightSide, size, color, gender, mod1, mod2, mod3
                                , wholesaleCost, HCPCSString, isHCPCSUpdated, userID, "", out  userMessage);
            }

        
            public int Update(int thirdPartyProductID, int practiceCatalogSupplierBrandID, string productCode, string productName
                                , string packaging, string leftRightSide, string size, string color, string gender, string mod1, string mod2, string mod3
                                , decimal wholesaleCost, string HCPCSString, bool isHCPCSUpdated, int userID, string upcCode, out string userMessage)
            {
                int rowsUpdated;
                userMessage = "";
                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_ThirdPartyProduct_Update");

                //for testing only
                userID = 9876;

                db.AddInParameter(dbCommand, "ThirdPartyProductID", DbType.Int32, thirdPartyProductID);
                db.AddInParameter(dbCommand, "PracticeCatalogSupplierBrandID", DbType.Int32, practiceCatalogSupplierBrandID);
                db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
                db.AddInParameter(dbCommand, "ProductName", DbType.String, productName);
                db.AddInParameter(dbCommand, "Packaging", DbType.String, packaging);
                db.AddInParameter(dbCommand, "LeftRightSide", DbType.String, leftRightSide);
                db.AddInParameter(dbCommand, "Size", DbType.String, size);
                db.AddInParameter(dbCommand, "Color", DbType.String, color);
                db.AddInParameter(dbCommand, "Gender", DbType.String, gender);
                db.AddInParameter(dbCommand, "Mod1", DbType.String, mod1);
                db.AddInParameter(dbCommand, "Mod2", DbType.String, mod2);
                db.AddInParameter(dbCommand, "Mod3", DbType.String, mod3);
                db.AddInParameter(dbCommand, "WholesaleCost", DbType.Currency, wholesaleCost);
                db.AddInParameter(dbCommand, "HCPCSString", DbType.String, HCPCSString);
                db.AddInParameter(dbCommand, "IsHCPCSUpdated", DbType.Boolean, isHCPCSUpdated);
                db.AddInParameter(dbCommand, "userID", DbType.Int32, userID);
                db.AddInParameter(dbCommand, "UPCCODE", DbType.String, upcCode);
                db.AddOutParameter(dbCommand, "UserMessage", DbType.String, 50);

                rowsUpdated = db.ExecuteNonQuery(dbCommand);

                userMessage = db.GetParameterValue(dbCommand, "UserMessage").ToString();
                return rowsUpdated;
            }

        #endregion

		}
	} 

