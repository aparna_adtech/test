﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using ClassLibrary.DAL.Helpers;
using Microsoft.Practices.EnterpriseLibrary.Data;
using ClassLibrary.DAL;
using System.Data.Common;
using System.Linq;

namespace DAL.Practice
{
    public class Catalog
    {

        #region Constructors

        public Catalog()
        {

        }

        #endregion

        #region Methods

        public static void UpdateIsLogoAblePart(int masterCatalogProductID, bool enabled)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var piQuery = (from pcp in visionDB.PracticeCatalogProducts
                               where pcp.MasterCatalogProductID == masterCatalogProductID
                               && pcp.IsLogoAblePart != enabled
                               select pcp);

                piQuery.Update(rhi => rhi.IsLogoAblePart = enabled);

                piQuery.Update(a => DAL.PracticeLocation.Inventory.UpdateIsLogoAblePart(a.PracticeCatalogProductID, enabled));
                visionDB.SubmitChanges();
            }
        }

		#region Get Master Catalog

		public static DataSet GetMasterCatalogSuppliers()
		{
			Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
			DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetMasterCatalogSuppliers");

			DataSet ds = db.ExecuteDataSet(dbCommand);
			return ds;
		}

		public static DataSet GetCombinedCatalogSuppliers()
		{
			Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
			DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetMasterCatalogSuppliers");

			DataSet ds = db.ExecuteDataSet(dbCommand);
			return ds;
		}

		//possibly unused. moved here from Practice Catalog
		public static DataTable GetMasterCatalogData()
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetMasterCatalogProductInfo");

            DataTable dt = db.ExecuteDataSet(dbCommand).Tables[0];
            return dt;
        }


        public static DataSet GetMasterCatalogDataBySupplier(string SearchCriteria, string SearchText)
        {
            DataSet ds = null;
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetMasterCatalogProductInfo");

            db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
            db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);

            ds = db.ExecuteDataSet(dbCommand);

            return ds;

        }

        public class MasterCatalogProductInfo
        {
            public int SupplierID { get; set; }
            public int MastercatalogProductID { get; set; }
            public int MasterCatalogSubCategoryID { get; set; }
            public string Code { get; set; }
            public string ShortName { get; set; }
            public string Packaging { get; set; }
            public string LeftRightSide { get; set; }
            public string Mod1 { get; set; }
            public string Mod2 { get; set; }
            public string Mod3 { get; set; }
            public string Modifiers { get; set; }
            public string Size { get; set; }
            public string Color { get; set; }
            public string Gender { get; set; }
            public decimal? WholesaleListCost { get; set; }
			public bool IsLogoAblePart { get; set; }
			public bool IsReplaced { get; set; }
		}

        public List<MasterCatalogProductInfo> MasterCatalogProductInfoCache = null;
		public IEnumerable<MasterCatalogProductInfo> GetMasterCatalogDataBySupplier2(string SearchCriteria, string SearchText, int supplierID)
        {
            if (MasterCatalogProductInfoCache == null)
            {
                using (var c = new SqlConnection(ConfigurationManager.ConnectionStrings["BregVisionConnectionString"].ConnectionString))
                {
                    c.Open();
                    var cmd = c.CreateCommand();
                    cmd.CommandText = "usp_GetMasterCatalogProductInfo";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("SearchCriteria", SearchCriteria));
                    cmd.Parameters.Add(new SqlParameter("SearchText", SearchText));

                    var r = cmd.ExecuteReader();
                    var results = new List<MasterCatalogProductInfo>();
                    while (r.Read())
                    {
                        var supplier_id = 0;
                        if (r["SupplierID"] != DBNull.Value)
                            int.TryParse(r["SupplierID"].ToString(), out supplier_id);
                        if (supplier_id == 0)
                            continue;

                        object isReplaced = false;
                        try
                        {
                            isReplaced = r["IsReplaced"] != DBNull.Value ? r["IsReplaced"] : false;
                        }
                        catch
                        {
                        }

                        results.Add(new MasterCatalogProductInfo
                        {
                            SupplierID = supplier_id,
                            MastercatalogProductID = int.Parse(r["MastercatalogProductID"].ToString()),
                            MasterCatalogSubCategoryID = int.Parse(r["MasterCatalogSubCategoryID"].ToString()),
                            Code = r["Code"].ToString(),
                            ShortName = r["ShortName"].ToString(),
                            Packaging = r["Packaging"].ToString(),
                            LeftRightSide = r["LeftRightSide"].ToString(),
                            Mod1 = r["Mod1"].ToString(),
                            Mod2 = r["Mod2"].ToString(),
                            Mod3 = r["Mod3"].ToString(),
                            Modifiers = CreateModifierString(r["Mod1"].ToString(), r["Mod2"].ToString(), r["Mod3"].ToString()),
                            Size = r["Size"].ToString(),
                            Color = r["Color"].ToString(),
                            Gender = r["Gender"].ToString(),
                            WholesaleListCost = (r["WholesaleListCost"] is DBNull) ? (decimal?)null : decimal.Parse(r["WholesaleListCost"].ToString()),
                            IsLogoAblePart = Convert.ToBoolean(r["IsLogoAblePart"]),
                            IsReplaced = Convert.ToBoolean(isReplaced)
                        });
                    }

                    MasterCatalogProductInfoCache = results;
                }
            }

            return MasterCatalogProductInfoCache.Where(x => x.SupplierID == supplierID).ToList();
        }

        #endregion

        #region Add - Update Master Catalog

        public static void DiscontinueMasterCatalogItem(int masterCatalogProductID)
        {
            VisionDataContext visionDB = null;
            visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var mcpQuery = from mcp in visionDB.MasterCatalogProducts
                                    where mcp.MasterCatalogProductID == masterCatalogProductID
                                    select mcp as ClassLibrary.DAL.MasterCatalogProduct;

                ClassLibrary.DAL.MasterCatalogProduct masterCatalogItem = mcpQuery.FirstOrDefault<ClassLibrary.DAL.MasterCatalogProduct>();
                if (masterCatalogItem != null)
                {
                    masterCatalogItem.IsDiscontinued = true;
                    masterCatalogItem.ModifiedDate = DateTime.Now;
                    masterCatalogItem.ModifiedUserID = 1;
                    visionDB.SubmitChanges();
                }
            }
        }

        public static void PromoteToMasterCatalog(int thirdPartySupplierID, string userName)
        {
            VisionDataContext visionDB = null;
            visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var SupplierQuery = from tpp in visionDB.ThirdPartySuppliers
                                    where tpp.ThirdPartySupplierID == thirdPartySupplierID
                                    select tpp as ClassLibrary.DAL.ThirdPartySupplier;

                ClassLibrary.DAL.ThirdPartySupplier thirdPartySupplier = SupplierQuery.FirstOrDefault<ClassLibrary.DAL.ThirdPartySupplier>();
                if (thirdPartySupplier != null)
                {
                    List<ClassLibrary.DAL.PracticeCatalogSupplierBrand> supplierBrands = thirdPartySupplier.PracticeCatalogSupplierBrands.ToList<ClassLibrary.DAL.PracticeCatalogSupplierBrand>();
                    CreateMasterCatalogSupplier(thirdPartySupplier, supplierBrands, userName);

                }
            }
        }

        public static void CreateMasterCatalogSupplier(ClassLibrary.DAL.ThirdPartySupplier thirdPartySupplier, List<ClassLibrary.DAL.PracticeCatalogSupplierBrand> supplierBrands, string userName)
        {
            int userid = DAL.LoggedInUser.SelectUserID(userName);
            VisionDataContext visionDB = null;
            visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                //Make an empty contact.  Use a popup to fill it.

                //Make an address.  Use a popup to fill it


                //Make the Supplier
                MasterCatalogSupplier newSupplier = new MasterCatalogSupplier();

                newSupplier.SupplierName = thirdPartySupplier.SupplierName;
                newSupplier.SupplierShortName = thirdPartySupplier.SupplierShortName;
                newSupplier.Sequence = 999;
                newSupplier.IsEmailOrderPlacement = thirdPartySupplier.IsEmailOrderPlacement;
                newSupplier.EmailOrderPlacement = thirdPartySupplier.EmailOrderPlacement;
                newSupplier.FaxOrderPlacement = thirdPartySupplier.FaxOrderPlacement;

                newSupplier.CreatedUserID = userid;
                newSupplier.ModifiedUserID = userid;
                newSupplier.CreatedDate = DateTime.Now;
                newSupplier.ModifiedDate = DateTime.Now;
                newSupplier.IsActive = true;


                visionDB.MasterCatalogSuppliers.InsertOnSubmit(newSupplier);
                visionDB.SubmitChanges();

                CreatePracticeCatalogSupplierBrands(newSupplier, supplierBrands, userid);

                CreateCategories(newSupplier, userid);

            }
        }

        public static void CreateCategories(MasterCatalogSupplier newSupplier, int userid)
        {
            VisionDataContext visionDB = null;
            visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var templateCategoryQuery = (from c in visionDB.MasterCatalogCategories
                                             select new
                                             {
                                                 c.Name /*,
                                                SubCategories = from sc in visionDB.MasterCatalogSubCategories
                                                                where c.MasterCatalogCategoryID == sc.MasterCatalogCategoryID
                                                                select sc.Name */
                                             }).Distinct();

                var templateSubCategoryQuery = from sc in visionDB.MasterCatalogSubCategories
                                               join c in visionDB.MasterCatalogCategories on sc.MasterCatalogCategoryID equals c.MasterCatalogCategoryID
                                               select new
                                               {
                                                   CategoryName = c.Name,
                                                   SubCategoryName = sc.Name
                                               };

                var templateQuery = from c in templateCategoryQuery
                                    select new
                                    {
                                        c.Name,
                                        SubCategories = ((from sc in templateSubCategoryQuery where sc.CategoryName == c.Name select sc.SubCategoryName).Distinct())
                                    };

                var categories = templateQuery.ToList();

                foreach (var oldCategory in categories)
                {
                    MasterCatalogCategory newCategory = new MasterCatalogCategory();

                    newCategory.Name = oldCategory.Name;
                    newCategory.MasterCatalogSupplierID = newSupplier.MasterCatalogSupplierID;
                    newCategory.CreatedUserID = userid;
                    newCategory.ModifiedUserID = userid;
                    newCategory.CreatedDate = DateTime.Now;
                    newCategory.ModifiedDate = DateTime.Now;
                    newCategory.IsActive = true;
                    newCategory.Sequence = 0;

                    visionDB.MasterCatalogCategories.InsertOnSubmit(newCategory);
                    visionDB.SubmitChanges();

                    foreach (var oldSubCategory in oldCategory.SubCategories)
                    {
                        MasterCatalogSubCategory newSubCategory = new MasterCatalogSubCategory();
                        newSubCategory.Name = oldSubCategory;

                        newSubCategory.MasterCatalogCategory = newCategory;

                        newSubCategory.CreatedUserID = userid;
                        newSubCategory.ModifiedUserID = userid;
                        newSubCategory.CreatedDate = DateTime.Now;
                        newSubCategory.ModifiedDate = DateTime.Now;
                        newSubCategory.IsActive = true;
                        newSubCategory.Sequence = 0;

                        visionDB.MasterCatalogSubCategories.InsertOnSubmit(newSubCategory);
                        visionDB.SubmitChanges();

                    }
                }
            }


        }

        public static void CreatePracticeCatalogSupplierBrands(MasterCatalogSupplier newSupplier, List<PracticeCatalogSupplierBrand> supplierBrands, int userid)
        {
            VisionDataContext visionDB = null;
            visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                //Make the supplier brands for each Practice
                var practicesQuery = from p in visionDB.Practices
                                     where p.IsActive == true
                                     select new
                                     {
                                         PracticeID = p.PracticeID
                                     };

                foreach (var practice in practicesQuery.ToList())
                {


                    foreach (PracticeCatalogSupplierBrand thirdPartyBrand in supplierBrands)
                    {
                        PracticeCatalogSupplierBrand newMasterCatalogBrand = new PracticeCatalogSupplierBrand();

                        newMasterCatalogBrand.BrandName = thirdPartyBrand.BrandName;
                        newMasterCatalogBrand.BrandShortName = thirdPartyBrand.BrandShortName;
                        newMasterCatalogBrand.IsThirdPartySupplier = false;
                        newMasterCatalogBrand.Sequence = 999;
                        newMasterCatalogBrand.SupplierName = thirdPartyBrand.SupplierName;
                        newMasterCatalogBrand.SupplierShortName = thirdPartyBrand.SupplierShortName;
                        newMasterCatalogBrand.PracticeID = practice.PracticeID; //**********

                        newMasterCatalogBrand.MasterCatalogSupplierID = newSupplier.MasterCatalogSupplierID;

                        newMasterCatalogBrand.CreatedUserID = userid;
                        newMasterCatalogBrand.ModifiedUserID = userid;
                        newMasterCatalogBrand.CreatedDate = DateTime.Now;
                        newMasterCatalogBrand.ModifiedDate = DateTime.Now;
                        newMasterCatalogBrand.IsActive = true;

                        visionDB.PracticeCatalogSupplierBrands.InsertOnSubmit(newMasterCatalogBrand);
                        visionDB.SubmitChanges();

                    }
                }
            }

        }

        public static void CopyMcHcpcsToPc(int masterCatalogProductID, int practiceCatalogProductID, int userID)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                // if masterCatalogProductID has a BregProductHCPCs entry, then copy it over if it doesn't already exist

                // existing HCPCS at PCP level
                var PCP_existingHCPCS =
                    visionDB.ProductHCPCs
                    .Where(x => x.PracticeCatalogProductID == practiceCatalogProductID)
                    .Select(x => x.HCPCS)
                    .ToArray();

                // list of MCP HCPCS that aren't already present
                var MCP_HCPCSToAdd =
                    visionDB.BregProductHCPCs
                    .Where(x => x.MasterCatalogProductID == masterCatalogProductID)
                    .Select(x => new { x.HCPCS, x.IsOffTheShelf })
                    .ToArray()
                    .Where(x => !PCP_existingHCPCS.Contains(x.HCPCS))
                    .ToArray();

                // do the work of transferring the "missing" HCPCS
                foreach (var mcpHCPCS in MCP_HCPCSToAdd)
                {
                    ProductHCPC newProductHcpc = new ProductHCPC();

                    newProductHcpc.HCPCS = mcpHCPCS.HCPCS;
                    newProductHcpc.IsOffTheShelf = mcpHCPCS.IsOffTheShelf;
                    newProductHcpc.PracticeCatalogProductID = practiceCatalogProductID;
                    newProductHcpc.IsActive = true;
                    newProductHcpc.CreatedDate = DateTime.Now;
                    newProductHcpc.ModifiedDate = DateTime.Now;
                    newProductHcpc.CreatedUserID = userID;
                    newProductHcpc.ModifiedUserID = userID;

                    visionDB.ProductHCPCs.InsertOnSubmit(newProductHcpc);
                }

                visionDB.SubmitChanges();
            }
        }

        public static int SaveToMasterCatalog(int masterCatalogProductID, string productName, string productCode, string packaging, string side, string size, string mod1, string mod2, string mod3, string gender, string color, decimal wholesaleCost, string HCPCSString, string hcpcsStringOts, int mcSupplierID, int categoryID, int subCategoryID, bool isCustomBrace, string upcCode, int userID, bool isLogoAblePart = false)
        {
            
            VisionDataContext visionDB = null;
            visionDB = VisionDataContext.GetVisionDataContext();

            
            using (visionDB)
            {
                MasterCatalogProduct newProduct = null;
                if (masterCatalogProductID > 0)
                {
                    newProduct = (from mcp in visionDB.MasterCatalogProducts
                                where mcp.MasterCatalogProductID == masterCatalogProductID
                                select mcp).FirstOrDefault<MasterCatalogProduct>();
                    if (newProduct == null)
                    {
                        throw new ApplicationException(string.Format("Unable to find MasterCatalogProductID in SaveToMasterCatalog.  ID={0}", masterCatalogProductID));
                    }
                }
                else
                {
                    newProduct = new MasterCatalogProduct();
                }
                newProduct.MasterCatalogSubCategoryID = subCategoryID;
                newProduct.MasterCatalogSupplierID = mcSupplierID;
                newProduct.Name = productName;
                newProduct.ShortName = productName;
                newProduct.Code = productCode;
                newProduct.Packaging = packaging;
                newProduct.LeftRightSide = side;
                newProduct.Size = size;
                newProduct.Mod1 = mod1;
                newProduct.Mod2 = mod2;
                newProduct.Mod3 = mod3;
                newProduct.Gender = gender;
                newProduct.Color = color;
                newProduct.WholesaleListCost = wholesaleCost;
                newProduct.DateDiscontinued = DateTime.Now.AddYears(10);
                newProduct.IsDiscontinued = false;
                newProduct.IsCustomBrace = isCustomBrace;
                newProduct.IsLogoAblePart = isLogoAblePart;
                //newProduct.UPCCode = upcCode;

                newProduct.CreatedUserID = userID;
                newProduct.CreatedDate = DateTime.Now;
                newProduct.ModifiedUserID = userID;
                newProduct.ModifiedDate = DateTime.Now;
                newProduct.IsActive = true;

                if (masterCatalogProductID < 1)
                {
                    visionDB.MasterCatalogProducts.InsertOnSubmit(newProduct);
                }
                visionDB.SubmitChanges();
                masterCatalogProductID = newProduct.MasterCatalogProductID; 
                //hcpcs;
                if (mcSupplierID == 1) //1 is breg (Only breg hcpcs can be saved)
                {
                    SaveMasterCatalogHcpcs(masterCatalogProductID, HCPCSString, userID, isOffTheShelf:false);
                    SaveMasterCatalogHcpcs(masterCatalogProductID, hcpcsStringOts, userID, isOffTheShelf:true);
                }

                SaveMasterCatalogUPCCodes(masterCatalogProductID, upcCode, userID);

                if (isLogoAblePart == false)
                {
                    UpdateIsLogoAblePart(masterCatalogProductID, enabled: false);
                }
            }

            return masterCatalogProductID;
        }

        public static void SaveMasterCatalogUPCCodes(Int32 masterCatalogProductID, string upcCodes, Int32 userID)
        {
            string[] upcCodeList = upcCodes.Trim().Trim(',').Trim().Split(',');

            //clean the strings
            for (int i = 0; i < upcCodeList.GetLength(0); i++)
            {
                upcCodeList[i] = upcCodeList[i].Trim();
            }


            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                //get all Hcpcs for this item
                var fullSavedUPCCodes = (from upc in visionDB.UPCCodes
                                      where upc.MasterCatalogProductID == masterCatalogProductID
                                      select upc).ToList();

                //get a string array of just the HCPCs field
                string[] savedUPCCodes = (from upc in fullSavedUPCCodes
                                       select upc.Code.Trim()).ToArray<string>();

                //find the  HCPCs that should be deleted
                var deletedItems = savedUPCCodes.Except(upcCodeList);

                //find the hcpcs that should be added
                var newItems = upcCodeList.Except(savedUPCCodes);

                //add the new hcpcs
                foreach (string newUPCCode in newItems)
                {
                    if (newUPCCode.Trim() != string.Empty)
                    {
                        UPCCode newCode = new UPCCode();
                        newCode.MasterCatalogProductID = masterCatalogProductID;
                        newCode.Code = newUPCCode.Trim();

                        newCode.IsActive = true;
                        newCode.CreatedUserID = userID;
                        newCode.ModifiedUserID = userID;
                        newCode.CreatedDate = DateTime.Now;
                        newCode.ModifiedDate = DateTime.Now;

                        visionDB.UPCCodes.InsertOnSubmit(newCode);
                    }
                }

                //delete the hcpcs that should be deleted
                foreach (UPCCode upc in fullSavedUPCCodes)
                {
                    if (deletedItems.Contains(upc.Code.Trim()))
                    {
                        visionDB.UPCCodes.DeleteOnSubmit(upc);
                    }
                }
                //submit the changes to the database
                if (deletedItems.Count() > 0 || newItems.Count() > 0)
                {
                    visionDB.SubmitChanges();
                }
            }
        }

        public static void SaveMasterCatalogHcpcs(int masterCatalogProductID, string hcpcs, int userID, bool isOffTheShelf)
        {
            string[] hcpcsList = hcpcs.Trim().Trim(',').Trim().Split(',');

            //clean the strings
            for (int i = 0; i < hcpcsList.GetLength(0); i++)
            {
                hcpcsList[i] = hcpcsList[i].Trim();
            }


            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                //get all Hcpcs for this item
                var fullSavedHCPCs = (from ph in visionDB.BregProductHCPCs
                                      where ph.MasterCatalogProductID == masterCatalogProductID
                                      && ph.IsOffTheShelf == isOffTheShelf
                                      select ph).ToList();

                //get a string array of just the HCPCs field
                string[] savedHcpcs = (from ph in fullSavedHCPCs
                                       select ph.HCPCS.Trim()).ToArray<string>();

                //find the  HCPCs that should be deleted
                var deletedItems = savedHcpcs.Except(hcpcsList);

                //find the hcpcs that should be added
                var newItems = hcpcsList.Except(savedHcpcs);

                //add the new hcpcs
                foreach (string newHcpc in newItems)
                {
                    if (newHcpc.Trim() != string.Empty)
                    {
                        BregProductHCPC newHCPC = new BregProductHCPC();
                        newHCPC.MasterCatalogProductID = masterCatalogProductID;
                        newHCPC.HCPCS = newHcpc.Trim();
                        newHCPC.IsOffTheShelf = isOffTheShelf;

                        newHCPC.IsActive = true;
                        newHCPC.CreatedUserID = userID;
                        newHCPC.ModifiedUserID = userID;
                        newHCPC.CreatedDate = DateTime.Now;
                        newHCPC.ModifiedDate = DateTime.Now;

                        visionDB.BregProductHCPCs.InsertOnSubmit(newHCPC);
                    }
                }

                //delete the hcpcs that should be deleted
                foreach (BregProductHCPC hcpc in fullSavedHCPCs)
                {
                    if (deletedItems.Contains(hcpc.HCPCS.Trim()))
                    {
                        visionDB.BregProductHCPCs.DeleteOnSubmit(hcpc);
                    }
                }
                //submit the changes to the database
                if (deletedItems.Count() > 0 || newItems.Count() > 0)
                {
                    visionDB.SubmitChanges();
                }
            }
        }

        public static void SavePracticeCatalogHcpcs(Int32 practiceCatalogProductID, string hcpcs, Int32 userID, bool isOffTheShelf)
        {
            string[] hcpcsList = hcpcs.Trim().Trim(',').Trim().Split(',');

            //clean the strings
            for (int i = 0; i < hcpcsList.GetLength(0); i++)
                hcpcsList[i] = hcpcsList[i].Trim();

            using (VisionDataContext visionDB = VisionDataContext.GetVisionDataContext())
            {
                //get all hcpcs for this item
                var fullSavedHCPCs = (from ph in visionDB.ProductHCPCs
                                      where ph.PracticeCatalogProductID == practiceCatalogProductID
                                        && ph.IsOffTheShelf == isOffTheShelf
                                      select ph).ToList();

                //purge hcpcs
                foreach (ProductHCPC hcpc in fullSavedHCPCs)
                    visionDB.ProductHCPCs.DeleteOnSubmit(hcpc);

                //add the new hcpcs
                foreach (string newHcpc in hcpcsList)
                {
                    if (newHcpc.Trim() != string.Empty)
                    {
                        ProductHCPC newHCPC = new ProductHCPC();
                        newHCPC.PracticeCatalogProductID = practiceCatalogProductID;
                        newHCPC.HCPCS = newHcpc.Trim();
                        newHCPC.IsOffTheShelf = isOffTheShelf;

                        newHCPC.IsActive = true;
                        newHCPC.CreatedUserID = userID;
                        newHCPC.ModifiedUserID = userID;
                        newHCPC.CreatedDate = DateTime.Now;
                        newHCPC.ModifiedDate = DateTime.Now;

                        visionDB.ProductHCPCs.InsertOnSubmit(newHCPC);
                    }
                }

                //submit the changes to the database
                if (fullSavedHCPCs.Count() > 0 || hcpcsList.Count() > 0)
                    visionDB.SubmitChanges();
            }
        }

        public static List<GenericReturn> GetActiveMasterCatalogSuppliers()
        {
            VisionDataContext visionDB = null;
            visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var supplierQuery = from s in visionDB.MasterCatalogSuppliers
                                    where s.IsActive == true
                                    select new GenericReturn
                                    {
                                        ID = s.MasterCatalogSupplierID,
                                        Name = s.SupplierName
                                    };
                return supplierQuery.ToList<GenericReturn>();
            }
        }

        public static List<GenericReturn> GetMasterCatalogSupplierCategories(Int32 masterCatalogSupplierID)
        {
            VisionDataContext visionDB = null;
            visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var supplierQuery = from c in visionDB.MasterCatalogCategories
                                    where c.MasterCatalogSupplierID == masterCatalogSupplierID
                                    && c.IsActive == true
                                    select new GenericReturn
                                    {
                                        ID = c.MasterCatalogCategoryID,
                                        Name = c.Name
                                    };
                return supplierQuery.ToList<GenericReturn>();
            }
        }
        public static List<GenericReturn> GetMasterCatalogSupplierSubCategories(Int32 masterCatalogCategoryID)
        {
            VisionDataContext visionDB = null;
            visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var supplierQuery = from sc in visionDB.MasterCatalogSubCategories
                                    where sc.MasterCatalogCategoryID == masterCatalogCategoryID
                                    && sc.IsActive == true
                                    select new GenericReturn
                                    {
                                        ID = sc.MasterCatalogSubCategoryID,
                                        Name = sc.Name
                                    };
                return supplierQuery.ToList<GenericReturn>();
            }
        }

        public static Int32 GetMasterCatalogCategoryFromSubCategoryID(Int32 subCategoryID)
        {
            VisionDataContext visionDB = null;
            visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var supplierQuery = from sc in visionDB.MasterCatalogSubCategories
                                    where sc.MasterCatalogSubCategoryID == subCategoryID
                                    && sc.IsActive == true
                                    select sc.MasterCatalogCategoryID;
                return supplierQuery.FirstOrDefault<Int32>();
            }
        }

        #endregion

        #region Get Practice Catalog Suppliers
        //  Practice Catalog
        public static DataSet GetPracticeCatalogSuppliers(string SearchCriteria, string SearchText, Int32 practiceID)
        {
            ////int rowsAffected = 0;

            //Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            ////DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetPracticeCatalogSupplierBrands");
            //DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeCatalogSupplierBrand_Select_Unique");

            //db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            //db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
            //db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);

            //DataSet ds = db.ExecuteDataSet(dbCommand);
            //return ds;

            using (var c = new SqlConnection(ConfigurationManager.ConnectionStrings["BregVisionConnectionString"].ConnectionString))
            {
                c.Open();
                var cmd = c.CreateCommand();
                cmd.CommandText = "usp_PracticeCatalogSupplierBrand_Select_Unique";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter { ParameterName = "PracticeID", DbType = DbType.Int32, Value = practiceID });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "SearchCriteria", DbType = DbType.String, Value = SearchCriteria });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "SearchText", DbType = DbType.String, Value = SearchText });
                var r = cmd.ExecuteReader();
                var dt = new DataTable();
                dt.Load(r);
                var ds = new DataSet();
                ds.Tables.Add(dt);
                return ds;
            }
        }
        #endregion

        #region Get Practice Catalog Data

        public static DataSet GetPracticeCatalogDataBySupplier1(string SearchCriteria, string SearchText, int practiceID)
        {
            DataSet ds = null;
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetPracticeCatalogProductInfoMCTOPC");

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
            db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);

            ds = db.ExecuteDataSet(dbCommand);

            return ds;

        }

        public class PracticeCatalogProductInfo
        {
            public int PracticeID { get; set; }
            public int PracticecatalogProductID { get; set; }
            public int? MasterCatalogProductID { get; set; }
            public int PracticeCatalogSupplierBrandID { get; set; }
            public string SupplierShortName { get; set; }   // only used in some stored procs
            public string BrandShortName { get; set; }      // only used in some stored procs
            public int? Sequence { get; set; }              // only used in some stored procs
            public string ShortName { get; set; }
            public string Code { get; set; }
            public string SideSize { get; set; }
            public string LeftRightSide { get; set; }
            public string Mod1 { get; set; }
            public string Mod2 { get; set; }
            public string Mod3 { get; set; }
            public string Size { get; set; }
            public string Gender { get; set; }
            public decimal WholesaleCost { get; set; }
            public decimal BillingCharge { get; set; }
            public decimal BillingChargeOTS { get; set; }
            public decimal DMEDeposit { get; set; }
            public decimal BillingChargeCash { get; set; }
            public int StockingUnits { get; set; }
            public int BuyingUnits { get; set; }
            public bool IsThirdPartyProduct { get; set; }
            public string HCPCSString { get; set; }
            public string HCPCSStringOTS { get; set; }
            public bool IsDeletable { get; set; }
            public bool IsLogoAblePart { get; set; } 
            public bool McpIsLogoAblePart { get; set; }
            public bool IsSplitCode { get; set; } 
            public bool IsAlwaysOffTheShelf { get; set; }
            public bool IsCustomFabricated { get; set; }
			public bool IsPreAuthorization { get; set; }
			public bool IsReplaced { get; set; }
            public bool IsLateralityExempt { get; set;}
		}

		public static IEnumerable<PracticeCatalogProductInfo> GetPracticeCatalogDataBySupplier2(string SearchCriteria, string SearchText, int practiceID, int practiceCatalogSupplierBrandID)
        {
            using (
                var c =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["BregVisionConnectionString"].ConnectionString))
            {
                c.Open();
                var cmd = c.CreateCommand();
                cmd.CommandText = "usp_GetPracticeCatalogProductInfoMCTOPC";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("PracticeID", practiceID));
                cmd.Parameters.Add(new SqlParameter("SearchCriteria", SearchCriteria));
                cmd.Parameters.Add(new SqlParameter("SearchText", SearchText));
                var r = cmd.ExecuteReader();
                var results = new List<PracticeCatalogProductInfo>();
                var col_to_index_map = new Dictionary<string, int>();
                for (var i = 0; i < r.FieldCount; i++)
                    col_to_index_map.Add(r.GetName(i).ToLower(), i);
                var func_get_column =
                    (Func<SqlDataReader, string, object>)
                        delegate(SqlDataReader reader, string key)
                        {
                            if (col_to_index_map.ContainsKey(key.ToLower()))
                                return reader[key];
                            else
                                return string.Empty;
                        };
                var func_get_bool =
                    (Func<object, bool>)
                        delegate(object columnobject)
                        {
                            int o;
                            if (!int.TryParse(columnobject.ToString(), out o))
                                return false;
                            else
                                return (o == 1);
                        };
                while (r.Read())
                {
                    if (int.Parse(func_get_column(r, "PracticeCatalogSupplierBrandID").ToString()) !=
                        practiceCatalogSupplierBrandID)
                        continue;
                    results.Add(new PracticeCatalogProductInfo
                    {
                        PracticeID = int.Parse(func_get_column(r, "PracticeID").ToString()),
                        PracticecatalogProductID = int.Parse(func_get_column(r, "PracticecatalogProductID").ToString()),
                        MasterCatalogProductID =
                            (func_get_column(r, "MasterCatalogProductID") is DBNull)
                                ? (int?) null
                                : int.Parse(func_get_column(r, "MasterCatalogProductID").ToString()),
                        PracticeCatalogSupplierBrandID =
                            int.Parse(func_get_column(r, "PracticeCatalogSupplierBrandID").ToString()),
                        SupplierShortName = func_get_column(r, "SupplierShortName").ToString(),
                        BrandShortName = func_get_column(r, "BrandShortName").ToString(),
                        ShortName = func_get_column(r, "ShortName").ToString(),
                        Code = func_get_column(r, "Code").ToString(),
                        SideSize = func_get_column(r, "SideSize").ToString(),
                        LeftRightSide = func_get_column(r, "LeftRightSide").ToString(),
                        Mod1 = func_get_column(r, "Mod1").ToString(),
                        Mod2 = func_get_column(r, "Mod2").ToString(),
                        Mod3 = func_get_column(r, "Mod3").ToString(),
                        Size = func_get_column(r, "Size").ToString(),
                        Gender = func_get_column(r, "Gender").ToString(),
                        WholesaleCost = decimal.Parse(func_get_column(r, "WholesaleCost").ToString()),
                        BillingCharge = decimal.Parse(func_get_column(r, "BillingCharge").ToString()),
                        DMEDeposit = decimal.Parse(func_get_column(r, "DMEDeposit").ToString()),
                        BillingChargeCash = decimal.Parse(func_get_column(r, "BillingChargeCash").ToString()),
                        StockingUnits = int.Parse(func_get_column(r, "StockingUnits").ToString()),
                        BuyingUnits = int.Parse(func_get_column(r, "BuyingUnits").ToString()),
                        IsThirdPartyProduct = func_get_bool(func_get_column(r, "IsThirdPartyProduct")),
                        HCPCSString = func_get_column(r, "HCPCSString").ToString(),
                        IsDeletable = func_get_bool(func_get_column(r, "IsDeletable")),
                        IsLogoAblePart = func_get_bool(func_get_column(r, "IsLogoAblePart")),
                        McpIsLogoAblePart = func_get_bool(func_get_column(r, "McpIsLogoAblePart")),
                        IsSplitCode = (bool) func_get_column(r, "IsSplitCode"),
                        IsAlwaysOffTheShelf = (bool) func_get_column(r, "IsAlwaysOffTheShelf"),
						IsPreAuthorization = func_get_column(r, "IsPreAuthorization").ToString() != "" && Boolean.Parse(func_get_column(r, "IsPreAuthorization").ToString()),
                        IsLateralityExempt = func_get_column(r, "IsLateralityExempt").ToString() != "" && Boolean.Parse(func_get_column(r, "IsLateralityExempt").ToString()),
                        IsReplaced = (bool)(func_get_column(r, "IsReplaced").ToString() == "1")
					});
                }
                using (var visionDb = VisionDataContext.GetVisionDataContext())
                {

                    foreach (var practiceCatalogProductInfo in results.Where(practiceCatalogProductInfo => practiceCatalogProductInfo.IsSplitCode))
                    {
                        var billingChargeOts = visionDb.OffTheShelfBillingCharges.SingleOrDefault(otsb =>
                                otsb.PracticeCatalogProductID == practiceCatalogProductInfo.PracticecatalogProductID);
                        if (billingChargeOts == null)
                        {
                            billingChargeOts = new OffTheShelfBillingCharge
                            {
                                PracticeCatalogProductID = practiceCatalogProductInfo.PracticecatalogProductID
                            };
                            visionDb.OffTheShelfBillingCharges.InsertOnSubmit(billingChargeOts);
                            visionDb.SubmitChanges();
                        }
                        practiceCatalogProductInfo.BillingChargeOTS = billingChargeOts.BillingCharge;
                        practiceCatalogProductInfo.HCPCSStringOTS = visionDb.ConcatHCPCSOTS(practiceCatalogProductInfo.PracticecatalogProductID);
                    }

                    foreach (var practiceCatalogProductInfo in results.Where(pcp => pcp.HCPCSString != null && pcp.HCPCSString.Length > 4))
                    {
                        practiceCatalogProductInfo.IsCustomFabricated =
                            HcpcsHelper.IsHcpcCustomFabricated(practiceCatalogProductInfo.HCPCSString, visionDb);
                    }
                }
            return results;
            }
        }

        public static string CreateModifierString(string mod1, string mod2, string mod3)
        {
            var modifiersList = new List<string>();
            if (!string.IsNullOrEmpty(mod1))
                modifiersList.Add(mod1);
            if (!string.IsNullOrEmpty(mod2))
                modifiersList.Add(mod2);
            if (!string.IsNullOrEmpty(mod3))
                modifiersList.Add(mod3);
            var modArray = modifiersList.ToArray();

            return string.Join(",", modArray);
        }


        #endregion

        #region "Update Practice Catalog(Add new item to catalog from master catalog)"

        public static Int32 UpdatePracticeCatalog(Int32 PracticeID, Int32 MasterCatalogProductID, Int32 UserID, bool transferModifiers)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeCatalogProduct_Insert_By_MasterCatalogProductID");

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, PracticeID);
            db.AddInParameter(dbCommand, "MasterCatalogProductID", DbType.Int32, MasterCatalogProductID);
            db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
            db.AddOutParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, 4);
            db.AddInParameter(dbCommand, "TransferModifiers", DbType.Boolean, transferModifiers);

            int SQLReturn = db.ExecuteNonQuery(dbCommand);
            //return ds;
            Int32 return_value = Convert.ToInt32(db.GetParameterValue(dbCommand, "PracticeCatalogProductID"));
            return return_value;
        }

        #endregion

        #region Update Pricing

        public static Int32 UpdatePricing_PracticeCatalog(Int32 PracticeCatalogProductID, decimal WholesSaleCost, decimal BillingCharge, decimal BillingChargeCash, decimal DMEDeposit, string mod1, string mod2, string mod3)
        {
            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_UpdatePricing_PracticeCatalogProduct");

            db.AddInParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, PracticeCatalogProductID);
            db.AddInParameter(dbCommand, "WholeSaleCost", DbType.Decimal, WholesSaleCost);
            db.AddInParameter(dbCommand, "BillingCharge", DbType.Decimal, BillingCharge);
            db.AddInParameter(dbCommand, "BillingChargeCash", DbType.Decimal, BillingChargeCash);
            db.AddInParameter(dbCommand, "DMEDeposit", DbType.Decimal, DMEDeposit);
            db.AddInParameter(dbCommand, "Mod1", DbType.String, mod1);
            db.AddInParameter(dbCommand, "Mod2", DbType.String, mod2);
            db.AddInParameter(dbCommand, "Mod3", DbType.String, mod3);
            //db.AddOutParameter(dbCoimand, "ShoppingCartIDReturn", DbType.Int32, rowsAffected);

            int return_value = db.ExecuteNonQuery(dbCommand);

            return return_value;
        }

        #endregion

        #region Update Inventory Levels

        public static Int32 UpdateInventoryLevels_PracticeCatalog(Int32 PracticeCatalogProductID, Int32 StockingUnits, Int32 practiceID, Int32 userID)
        {
            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_UpdateInventoryLevels_PracticeCatalogProduct");

            db.AddInParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, PracticeCatalogProductID);
            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            db.AddInParameter(dbCommand, "StockingUnits", DbType.Int32, StockingUnits);
            db.AddInParameter(dbCommand, "UserID", DbType.Int32, userID);

            int return_value = db.ExecuteNonQuery(dbCommand);
            //return ds;
            //Int32 return_value = Convert.ToInt32(db.GetParameterValue(dbCommand, "ShoppingCartIDReturn"));
            return return_value;
        }

        #endregion

        #region Delete

        public int Delete(int practiceCatalogProductID, int userID, out int removalType, out string dependencyMessage)
        {
            return Delete(practiceCatalogProductID, userID, out removalType, out dependencyMessage, false);
        }

        public int Delete(int practiceCatalogProductID, int userID, out int removalType, out string dependencyMessage, bool isAdmin)
            {
                int rowsAffected = 0;
                removalType = -1;
                dependencyMessage = "";
                // Note: RemovalType is 0 not removed; 1 deleted; 2 soft delete (inactive NOTE: THIS IS NOT USED HERE.)

                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeCatalogProduct_Delete"))
                {
                    db.AddInParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, practiceCatalogProductID);
                    db.AddInParameter(dbCommand, "UserID", DbType.Int32, userID);
                    db.AddOutParameter(dbCommand, "RemovalType", DbType.Int32, 4);
                    db.AddOutParameter(dbCommand, "DependencyMessage", DbType.String, 2000);
                    db.AddInParameter(dbCommand, "IsAdmin", DbType.Boolean, isAdmin);

                    rowsAffected = db.ExecuteNonQuery(dbCommand);

                    removalType = Convert.ToInt32(db.GetParameterValue(dbCommand, "RemovalType"));
                    dependencyMessage = db.GetParameterValue(dbCommand, "DependencyMessage").ToString();

                    //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE")); 

                    dbCommand.Dispose();
                }

                return rowsAffected;
            }

            #endregion

        #endregion

        /// <summary>
        /// Returns true if the item is a custom Brace or Accessory
        /// </summary>
        /// <param name="practiceCatalogProductID"></param>
        /// <returns></returns>
        public static bool IsCustomBraceOrAccessory(int practiceCatalogProductID)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var numItemsFound = (from pcp in visionDB.PracticeCatalogProducts 
                                     join mcp in visionDB.MasterCatalogProducts on pcp.MasterCatalogProductID equals mcp.MasterCatalogProductID

                                     where pcp.IsThirdPartyProduct == false
                                     && pcp.PracticeCatalogProductID == practiceCatalogProductID
                                     && (
                                             mcp.IsCustomBrace == true
                                             || mcp.isCustomBraceAccessory == true
                                       )
                                     select pcp).Count();

                if (numItemsFound > 0)
                {
                    return true;
                }
                return false;
            }
        }

        public static void UpdateCatalogItem(int practiceCatalogProductID, bool isLogoAblePart, bool isSplitCode, bool isAlwaysOffTheShelf, bool isPreAuthorization, bool IsSideReq)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var pcpQuery = from pcp in visionDB.PracticeCatalogProducts
                    where pcp.PracticeCatalogProductID == practiceCatalogProductID
                    select pcp;

                PracticeCatalogProduct practiceCatalogProduct = pcpQuery.FirstOrDefault();
                if (practiceCatalogProduct != null)
                {
                    practiceCatalogProduct.IsSplitCode = isSplitCode;
                    practiceCatalogProduct.IsPreAuthorization = isPreAuthorization;
                    practiceCatalogProduct.IsLateralityExempt = IsSideReq;
                    // Clearing out value of AlwaysOffTheShelf when a product is not a split code product.
                    if (isSplitCode)
                    {
                        practiceCatalogProduct.IsAlwaysOffTheShelf = isAlwaysOffTheShelf;
                    }
                    else
                    {
                        practiceCatalogProduct.IsAlwaysOffTheShelf = false;
                    }

                    practiceCatalogProduct.IsLogoAblePart = isLogoAblePart;
                    visionDB.SubmitChanges();
                    if (isLogoAblePart == false)
                    {
                        DAL.PracticeLocation.Inventory.UpdateIsLogoAblePart(practiceCatalogProductID, enabled: false);
                    }
                }
            }
        }

        public static void SavePracticeCatalogBillingOTS(int practiceCatalogProductID, decimal billingChargeOTS)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var otsbcQuery = from otsbc in visionDB.OffTheShelfBillingCharges
                                 where otsbc.PracticeCatalogProductID == practiceCatalogProductID
                                 select otsbc;

                OffTheShelfBillingCharge offTheShelfBillingCharge = otsbcQuery.FirstOrDefault();
                if (offTheShelfBillingCharge != null)
                {
                    offTheShelfBillingCharge.BillingCharge = billingChargeOTS;
                }
                else
                {
                    offTheShelfBillingCharge = new OffTheShelfBillingCharge();
                    offTheShelfBillingCharge.PracticeCatalogProductID = practiceCatalogProductID;
                    offTheShelfBillingCharge.BillingCharge = billingChargeOTS;

                    visionDB.OffTheShelfBillingCharges.InsertOnSubmit(offTheShelfBillingCharge);
                }

                visionDB.SubmitChanges();
            }
        }

        public static void DeletePracticeCatalogBillingOTS(int practiceCatalogProductID)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var otsbcQuery = from otsbc in visionDB.OffTheShelfBillingCharges
                                 where otsbc.PracticeCatalogProductID == practiceCatalogProductID
                                 select otsbc;

                List<OffTheShelfBillingCharge> offTheShelfBillingCharges = otsbcQuery.ToList();

                foreach(var offTheShelfBillingCharge in offTheShelfBillingCharges)
                {
                    visionDB.OffTheShelfBillingCharges.DeleteOnSubmit(offTheShelfBillingCharge);
                }

                visionDB.SubmitChanges();
            }
        }
    }
}
