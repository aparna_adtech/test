﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClassLibrary.DAL;

namespace DAL.Practice
{
    public class PracticeApplication
    {
        public static ClassLibrary.DAL.PracticeApplication SavePracticeApplication(ClassLibrary.DAL.PracticeApplication practiceApplication)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                visionDB.PracticeApplications.InsertOnSubmit(practiceApplication);
                visionDB.SubmitChanges();
            }

            return practiceApplication;
        }

        
        public static List<ClassLibrary.DAL.PracticeApplication> GetPracticeApplications()
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var query = from pas in visionDB.PracticeApplications
                            select pas;

                return query.OrderByDescending(i => i.PracticeApplicationID).ToList<ClassLibrary.DAL.PracticeApplication>();
            }
        }

        public static ClassLibrary.DAL.PracticeApplication GetPracticeApplication(int practiceApplicationID)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var query = from pa in visionDB.PracticeApplications
                            where pa.PracticeApplicationID == practiceApplicationID
                            select pa;

                return query.FirstOrDefault<ClassLibrary.DAL.PracticeApplication>();
                
            }
        }

        public static string GetBregAccountNumber(int practiceId)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var query = from pa in visionDB.PracticeApplications
                    where pa.PracticeID == practiceId
                    select pa.BregAccountNumber;
                return query.FirstOrDefault();
            }
        }

        public static void UpdatePracticeApplication(ClassLibrary.DAL.PracticeApplication practiceApplication)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            
            using (visionDB)
            {
                visionDB.PracticeApplications.Attach(practiceApplication,true);
                
                visionDB.SubmitChanges();
            }
        }
    }
}
