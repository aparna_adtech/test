﻿using System.Linq;
using System.Runtime.Serialization;
using ClassLibrary.DAL;

namespace DAL.Practice
{
    [DataContract]
    public class Payer
    {
        [DataMember]
        public int PayerId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        public static Payer[] SelectAll(int practiceId)
        {
            using (var db = VisionDataContext.GetVisionDataContext())
            {
                var payers = db.PracticePayers.Where(p => p.PracticeID == practiceId && p.IsActive)
                    .Select(x => new Payer
                    {
                        Name = x.Name,
                        PayerId = x.PracticePayerID
                    })
                    .OrderBy(y => y.Name);

                return payers.ToArray();
            }

        }

        public static Payer[] SelectAllForPractice(int practiceId)
        {
            using (var db = VisionDataContext.GetVisionDataContext())
            {
                var payers = db.PracticePayers.Where(p => p.PracticeID == practiceId)
                    .Select(x => new Payer
                    {
                        Name = x.Name,
                        PayerId = x.PracticePayerID,
                        IsActive = x.IsActive
                    })
                    .OrderBy(y => y.Name);

                return payers.ToArray();
            }
        }
    }
}