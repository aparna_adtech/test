﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClassLibrary.DAL;

namespace DAL.Practice
{
    public class PracticeApplicationAgreementTerm
    {
        public static ClassLibrary.DAL.PracticeApplicationAgreementTerm GetTopPracticeApplicationAgreementTerm()
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var query = (from pa in visionDB.PracticeApplicationAgreementTerms
                                orderby pa.PracticeAppAgreementTermsID descending
                                select pa).Take(1);

                return query.FirstOrDefault<ClassLibrary.DAL.PracticeApplicationAgreementTerm>();
            }
        }

        public static void GetTopPracticeApplicationAgreementTerm(ClassLibrary.DAL.PracticeApplicationAgreementTerm practiceApplicationAgreementTerm)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                visionDB.PracticeApplicationAgreementTerms.InsertOnSubmit(practiceApplicationAgreementTerm);
                visionDB.SubmitChanges();
            }
        }


    }
}
