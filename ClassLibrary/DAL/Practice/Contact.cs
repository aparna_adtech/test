using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;  //needed for the dbCommand object!!!!
using BLL.Practice;


namespace DAL.Practice
{
    //// jb comment out ContactDetails class.
    #region ContactDetails Class
  
	public class ContactDetails
    {

        #region FIELDS
        private int _ContactID;
        private string _Title;
        private string _Salutation;
        private string _FirstName;
        private string _MiddleName;
        private string _LastName;
        private string _Suffix;
        private string _EmailAddress;
        private string _PhoneWork;
        private string _PhoneCell;
        private string _PhoneHome;
        private string _Fax;
        private int _UserID;
	    private string _PhoneWorkExtension;
	    private bool _IsSpecificContact;

	    #endregion

        #region PROPERTIES
        public int ContactID
        {
            get { return this._ContactID; }
            set { this._ContactID = value; }
        }
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        public string Salutation
        {
            get { return _Salutation; }
            set { _Salutation = value; }
        }

        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }

        public string MiddleName
        {
            get { return _MiddleName; }
            set { _MiddleName = value; }
        }

        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }

        public string Suffix
        {
            get { return _Suffix; }
            set { _Suffix = value; }
        }

        public string EmailAddress
        {
            get { return _EmailAddress; }
            set { _EmailAddress = value; }
        }

        public string PhoneWork
        {
            get { return _PhoneWork; }
            set { _PhoneWork = value; }
        }

        public string PhoneWorkExtension
        {
            get { return _PhoneWorkExtension; }
            set { _PhoneWorkExtension = value; }
        }

        public string PhoneCell
        {
            get { return _PhoneCell; }
            set { _PhoneCell = value; }
        }

        public string PhoneHome
        {
            get { return _PhoneHome; }
            set { _PhoneHome = value; }
        }

        public string Fax
        {
            get { return _Fax; }
            set { _Fax = value; }
        }

        
        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

	    public bool IsSpecificContact
	    {
	        get { return _IsSpecificContact; }
            set { _IsSpecificContact = value; }
	    }

	    #endregion
    }
    #endregion


    #region Contact class for DAL
    public class Contact
    {
        //public DataSet GetContact(int contactID)
        //{
        //  

        //}
        public ContactDetails GetDetails(int contactID)
        {
            int rowAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");  //use key in web config for the connection string        
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_Contact_Select_One_ByParams");

            db.AddInParameter(dbCommand, "ContactID", DbType.Int32, contactID);
            db.AddOutParameter(dbCommand, "Title", DbType.String, 50);
            db.AddOutParameter(dbCommand, "Salutation", DbType.String, 10);
            db.AddOutParameter(dbCommand, "FirstName", DbType.String, 50);
            db.AddOutParameter(dbCommand, "MiddleName", DbType.String, 50);
            db.AddOutParameter(dbCommand, "LastName", DbType.String, 50);
            db.AddOutParameter(dbCommand, "Suffix", DbType.String, 10);
            db.AddOutParameter(dbCommand, "EmailAddress", DbType.String, 100);
            db.AddOutParameter(dbCommand, "PhoneWork", DbType.String, 25);
            db.AddOutParameter(dbCommand, "PhoneWorkExtension", DbType.String, 25);
            db.AddOutParameter(dbCommand, "PhoneCell", DbType.String, 25);
            db.AddOutParameter(dbCommand, "PhoneHome", DbType.String, 25);
            db.AddOutParameter(dbCommand, "Fax", DbType.String, 25);
            db.AddOutParameter(dbCommand, "IsSpecificContact", DbType.Boolean, 1);
            //db.AddParameter(dbCommand, "@RETURN_VALUE", SqlDbType.Int, ParameterDirection.ReturnValue);
            
            rowAffected = db.ExecuteNonQuery(dbCommand);
            ContactDetails cDetails = new ContactDetails();
            cDetails.ContactID = contactID;

            cDetails.Title = Convert.ToString(db.GetParameterValue(dbCommand, "Title"));
            cDetails.Salutation = Convert.ToString(db.GetParameterValue(dbCommand, "Salutation"));
            cDetails.FirstName = Convert.ToString(db.GetParameterValue(dbCommand, "FirstName"));
            cDetails.MiddleName = Convert.ToString(db.GetParameterValue(dbCommand, "MiddleName"));
            cDetails.LastName = Convert.ToString(db.GetParameterValue(dbCommand, "LastName"));
            cDetails.Suffix = Convert.ToString(db.GetParameterValue(dbCommand, "Suffix"));
            cDetails.EmailAddress = Convert.ToString(db.GetParameterValue(dbCommand, "EmailAddress"));
            cDetails.PhoneWork = Convert.ToString(db.GetParameterValue(dbCommand, "PhoneWork"));
            cDetails.PhoneWorkExtension = Convert.ToString(db.GetParameterValue(dbCommand, "PhoneWorkExtension"));
            cDetails.PhoneCell = Convert.ToString(db.GetParameterValue(dbCommand, "PhoneCell"));
            cDetails.PhoneHome = Convert.ToString(db.GetParameterValue(dbCommand, "PhoneHome"));
            cDetails.Fax = Convert.ToString(db.GetParameterValue(dbCommand, "Fax"));
            var isSpecificContactValue = db.GetParameterValue(dbCommand, "IsSpecificContact");
            cDetails.IsSpecificContact = !(isSpecificContactValue is DBNull) && Convert.ToBoolean(isSpecificContactValue);

            return cDetails;
        }


        public DataRow GetDetailRow(int contactID)
        {
            // If neccessary
            DataTable dt = new DataTable();

            return dt.Rows[0];
        }

        public bool Delete(int contactID, int userID)
        {
            int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");  //use key in web config for the connection string        
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_Contact_Delete");

            db.AddInParameter(dbCommand, "ContactID", DbType.Int32, contactID);
            db.AddInParameter(dbCommand, "ModifiedUserID", DbType.String, userID);

            rowsAffected = db.ExecuteNonQuery(dbCommand);
            return (rowsAffected == 1);
        }

        public int Add(string title, string salutation, string firstName, string middleName, string lastName, string suffix, string emailAddress, string phoneWork, string phoneWorkExtension, string phoneCell, string phoneHome, string fax, int userID, bool isSpecificContact)
        {
            int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");  //use key in web config for the connection string        
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_Contact_Insert");

            //db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            db.AddOutParameter(dbCommand, "ContactID", DbType.Int32, 4);
            db.AddInParameter(dbCommand, "Title", DbType.String, title);
            db.AddInParameter(dbCommand, "Salutation", DbType.String, salutation);
            db.AddInParameter(dbCommand, "FirstName", DbType.String, firstName);
            db.AddInParameter(dbCommand, "MiddleName", DbType.String, middleName);
            db.AddInParameter(dbCommand, "LastName", DbType.String, lastName);
            db.AddInParameter(dbCommand, "Suffix", DbType.String, suffix);
            db.AddInParameter(dbCommand, "EmailAddress", DbType.String, emailAddress);
            db.AddInParameter(dbCommand, "PhoneWork", DbType.String, phoneWork);
            db.AddInParameter(dbCommand, "PhoneWorkExtension", DbType.String, phoneWorkExtension);
            db.AddInParameter(dbCommand, "PhoneCell", DbType.String, phoneCell);
            db.AddInParameter(dbCommand, "PhoneHome", DbType.String, phoneHome);
            db.AddInParameter(dbCommand, "Fax", DbType.String, fax);
            db.AddInParameter(dbCommand, "CreatedUserID", DbType.Int32, userID);
            db.AddInParameter(dbCommand, "IsSpecificContact", DbType.Boolean, isSpecificContact);
            //db.AddParameter(dbCommand, "@RETURN_VALUE", SqlDbType.Int, ParameterDirection.ReturnValue);

            rowsAffected = db.ExecuteNonQuery(dbCommand);
            return Convert.ToInt32(db.GetParameterValue(dbCommand, "ContactID"));            
        }

        public bool Update(int contactID, string title, string salutation, string firstName, string middleName, string lastName, string suffix, string emailAddress, string phoneWork, string phoneWorkExtension, string phoneCell, string phoneHome, string fax, int userID, bool isSpecificContact)
        {          
            int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");  //use key in web config for the connection string        
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_Contact_Update");

            db.AddInParameter(dbCommand, "ContactID", DbType.Int32, contactID);
            db.AddInParameter(dbCommand, "Title", DbType.String, title);
            db.AddInParameter(dbCommand, "Salutation", DbType.String, salutation);
            db.AddInParameter(dbCommand, "FirstName", DbType.String, firstName);
            db.AddInParameter(dbCommand, "MiddleName", DbType.String, middleName);
            db.AddInParameter(dbCommand, "LastName", DbType.String, lastName);
            db.AddInParameter(dbCommand, "Suffix", DbType.String, suffix);
            db.AddInParameter(dbCommand, "EmailAddress", DbType.String, emailAddress);
            db.AddInParameter(dbCommand, "PhoneWork", DbType.String, phoneWork);
            db.AddInParameter(dbCommand, "PhoneWorkExtension", DbType.String, phoneWorkExtension);
            db.AddInParameter(dbCommand, "PhoneCell", DbType.String, phoneCell);
            db.AddInParameter(dbCommand, "PhoneHome", DbType.String, phoneHome);
            db.AddInParameter(dbCommand, "Fax", DbType.String, fax);
            db.AddInParameter(dbCommand, "ModifiedUserID", DbType.Int32, userID);
            db.AddInParameter(dbCommand, "IsSpecificContact", DbType.Boolean, isSpecificContact);
            //db.AddParameter(dbCommand, "@RETURN_VALUE", SqlDbType.Int, ParameterDirection.ReturnValue);

            rowsAffected = db.ExecuteNonQuery(dbCommand);
            return (rowsAffected == 1);
        }

    }
    
    #endregion
}
