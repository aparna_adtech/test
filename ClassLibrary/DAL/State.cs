using System;
using System.Data;
using System.Configuration;
//using System.Web;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Collections.Generic;

namespace DAL
{
    /// <summary>
    /// Summary description for State
    /// </summary>
    public class State
    {
        //public byte _stateID;
		public string _stateAbbr;
		//public string _stateName;

		//public byte StateID
		//{
		//    get {return _stateID;}
		//    set {_stateID = value;}
		//}

		public string StateAbbr
		{
			get { return _stateAbbr; }
			set { _stateAbbr = value; }
		}

		//public string StateName
		//{
		//    get {return _stateName;}
		//    set {_stateName= value;}
		//}


	    public State()
	    {
		    //
		    // Add constructor logic here
		    //
	    }

        public List<State> GetStates()
        {
            Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_State_Select_All" );  
            IDataReader reader = db.ExecuteReader(dbCommand);

            List<State> list = new List<State>();
			State blank = new State();
			blank.StateAbbr = "  ";
			list.Add( blank );
            while (reader.Read())
            {
                State s = new State();
                //s.StateID =  reader.GetByte(0);
                s.StateAbbr = reader.GetString(1);
                list.Add(s);
            }
			
            reader.Close();         // CommandBehavior.CloseConnection 
            reader.Dispose();
            dbCommand.Dispose();
            return list;
        }
    }
}