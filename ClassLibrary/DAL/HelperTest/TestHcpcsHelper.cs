﻿using System.Collections.Generic;
using System.Linq;
using System.Management.Instrumentation;
using ClassLibrary.DAL.Helpers;
using NUnit.Framework;

namespace ClassLibrary.DAL.HelperTest
{
    [TestFixture]
    class TestHcpcsHelper
    {

        [Test]
        [Ignore]
        public static void TestGetAllowableAmounts()
        {
            const string connectionString = "Data Source=vision;Initial Catalog=BregVision_beta;Persist Security Info=True;User ID=sa;Password=#1loveims";
            var db = VisionDataContext.GetVisionDataContext(connectionString);

            var result = HcpcsHelper.GetCustomFabricatedCodes(db);

            Assert.AreEqual(2,result.Count());
            Assert.Contains("L1846",result);
            Assert.Contains("L1844",result);
 

        }

        [Test]
        public static void TestIsHcpcCM_True_L1846()
        {
            var productHcpc = "L1846";

            var result = HcpcsHelper.IsHcpcCustomFabricated(productHcpc, GetCustomFabricatedCodes())
                ;

            Assert.IsTrue(result);
        }

        [Test]
        public static void TestIsHcpcCM_False_L0000()
        {
            var productHcpc = "L0000";

            var result = HcpcsHelper.IsHcpcCustomFabricated(productHcpc, GetCustomFabricatedCodes())
                ;

            Assert.IsFalse(result);
        }

        [Test]
        public static void TestIsHcpcCM_False_Empty()
        {
            var productHcpc = "";

            var result = HcpcsHelper.IsHcpcCustomFabricated(productHcpc, GetCustomFabricatedCodes())
                ;

            Assert.IsFalse(result);
        }

        [Test]
        public static void TestIsHcpcCM_False_Null()
        {
            string productHcpc = null;

            var result = HcpcsHelper.IsHcpcCustomFabricated(productHcpc, GetCustomFabricatedCodes())
                ;

            Assert.IsFalse(result);
        }

        [Test]
        public static void TestIsHcpcCM_True_L1846A()
        {
            string productHcpc = "L1846A";

            var result = HcpcsHelper.IsHcpcCustomFabricated(productHcpc, GetCustomFabricatedCodes())
                ;

            Assert.IsTrue(result);
        }

        [Test]
        public static void TestIsHcpcCM_True_L1846A_L0000()
        {
            string productHcpc = "L1846A,L0000";

            var result = HcpcsHelper.IsHcpcCustomFabricated(productHcpc, GetCustomFabricatedCodes())
                ;

            Assert.IsTrue(result);
        }

        [Test]
        public static void TestIsHcpcCM_True_CG_L1846A()
        {
            string productHcpc = "CG,L1846A";

            var result = HcpcsHelper.IsHcpcCustomFabricated(productHcpc, GetCustomFabricatedCodes())
                ;

            Assert.IsTrue(result);
        }

        [Test]
        public static void TestIsHcpcCM_True_CG_L1846A_With_a_Space()
        {
            string productHcpc = "CG, L1846A";

            var result = HcpcsHelper.IsHcpcCustomFabricated(productHcpc, GetCustomFabricatedCodes())
                ;

            Assert.IsTrue(result);
        }

        private static List<string> GetCustomFabricatedCodes()
        {
            var customManufacturedCodes = new List<string>() {"L1846", "L1844"};
            return customManufacturedCodes;
        }
    }
}
