﻿using System.Collections.Generic;
using System.Linq;
using System.Management.Instrumentation;
using ClassLibrary.DAL.Helpers;
using NUnit.Framework;

namespace ClassLibrary.DAL.HelperTest
{
    [TestFixture]
    class TestBillingChargeHelper
    {

        [Test]
        public static void TestGetAllowableAmounts()
        {
            const string connectionString = "Data Source=vision;Initial Catalog=BregVision_beta;Persist Security Info=True;User ID=sa;Password=#1loveims";
            var db = VisionDataContext.GetVisionDataContext(connectionString);

            var result = BillingChargeHelper.GetAllowableAmounts(db, false, 49, 207501, 1416);

            Assert.AreEqual(1,result.Count());
            Assert.AreEqual(4.00,result[0]);


        }

        [Test]
        public static void TestGetAllowableAmountWhenOneFound()
        {
            var allowableList = new List<decimal?>{(decimal) 4.00};
            var result = BillingChargeHelper.GetAllowableAmount(allowableList);

            Assert.AreEqual(4.00,result);


        }

        [Test]
        public static void TestGetAllowableAmountWhenTwoFound()
        {
            var allowableList = new List<decimal?>
            {
                (decimal) 4.00,
                (decimal) 6.00
            };
            var result = BillingChargeHelper.GetAllowableAmount(allowableList);

            Assert.AreEqual(10,result);


        }

        [Test]
        public static void TestGetAllowableAmountWhenNoneFound()
        {
            var allowableList = new List<decimal?>
            {
            };
            var result = BillingChargeHelper.GetAllowableAmount(allowableList);

            Assert.AreEqual(null,result);


        }

        [Test]
        public static void TestGetAllowableAmountWhenOneFoundIsZero()
        {
            var allowableList = new List<decimal?>
            {
                0
            };
            var result = BillingChargeHelper.GetAllowableAmount(allowableList);

            Assert.AreEqual(null,result);


        }

//        private static decimal? GetAllowableAmount(List<decimal?> allowableList)
//        {
//            if (allowableList.Count == 1 && allowableList[0] > 0)
//            {
//                return allowableList[0];
//            }
//            return null;
//        }

//        private static List<decimal?> GetAllowableAmounts(VisionDataContext db, int practiceId, bool isOffTheShelf, int practicePayerId, int practiceCatalogProductId, int practiceLocationId)
//        {
//            var result = from pinv in db.ProductInventories
//                join pcp in db.PracticeCatalogProducts on pinv.PracticeCatalogProductID equals pcp.PracticeCatalogProductID
//                join ph in db.ProductHCPCs on pcp.PracticeCatalogProductID equals ph.PracticeCatalogProductID
//                join mh in db.HCPCs on ph.HCPCS.ToUpper().Substring(0, 5) equals mh.Code.ToUpper()
//                join pp in db.PracticePayers on pcp.PracticeID equals pp.PracticeID
//                join ppa in db.PracticePayerAllowables on mh.HCPCSID equals ppa.HcpcsID into mhppa
//                from mhppai in mhppa.Where(ppa => pp.PracticePayerID == ppa.PracticePayerID)
//                where mhppai.AllowableAmount != null
//                      && mhppai.AllowableAmount > 0
//                      && pcp.PracticeID == practiceId
//                      && ph.IsOffTheShelf == isOffTheShelf
//                      && pp.PracticePayerID == practicePayerId
//                      && pcp.PracticeCatalogProductID == practiceCatalogProductId
//                      && pinv.PracticeLocationID == practiceLocationId
//                select mhppai.AllowableAmount;
//            return result.ToList();
//        }

//        private static List<decimal?> GetAllowableAmounts(VisionDataContext db, bool isOffTheShelf, int practicePayerId, int practiceCatalogProductId, int practiceLocationId)
//        {
//
//            var result = from pinv in db.ProductInventories
//                         join pcp in db.PracticeCatalogProducts on pinv.PracticeCatalogProductID equals pcp.PracticeCatalogProductID
//                         join ph in db.ProductHCPCs on pcp.PracticeCatalogProductID equals ph.PracticeCatalogProductID
//                         join mh in db.HCPCs on ph.HCPCS.ToUpper().Substring(0, 5) equals mh.Code.ToUpper()
//                         join pp in db.PracticePayers on pcp.PracticeID equals pp.PracticeID
//                         join ppa in db.PracticePayerAllowables on mh.HCPCSID equals ppa.HcpcsID into mhppa
//                         from mhppai in mhppa.Where(ppa => pp.PracticePayerID == ppa.PracticePayerID)
//                         where mhppai.AllowableAmount != null
//                               && mhppai.AllowableAmount > 0
//                               && ph.IsOffTheShelf == isOffTheShelf
//                               && pp.PracticePayerID == practicePayerId
//                               && pcp.PracticeCatalogProductID == practiceCatalogProductId
//                               && pinv.PracticeLocationID == practiceLocationId
//                         select mhppai.AllowableAmount;
//            return result.ToList();
//        }

    }
}
