﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using ClassLibrary.DAL;
using ClassLibrary.DAL.Helpers;

namespace DAL.ProductReplacement
{
	public class ProductReplacement
	{
		#region Constructors

		public ProductReplacement()
		{
			// Add constructor logic here
		}

		#endregion


		#region Methods

		// UpdateProductReplacement
		public static void UpdateProductReplacement(
			int? oldMasterCatalogProductID, int? oldThirdPartyProductID,
			int? newMasterCatalogProductID, int? newThirdPartyProductID,
			int? practiceID, DateTime? startDate, DateTime? endDate, int userID, bool isActive
			)
		{
			VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
			using (visionDB)
			{
				// Remove any records for the given source product IDs
				var query = (from pr in visionDB.ProductReplacements
							 where
								(((practiceID == null) && (pr.PracticeID == null)) || (pr.PracticeID == practiceID)) &&
								(((oldMasterCatalogProductID != null && pr.OldMasterCatalogProductID == oldMasterCatalogProductID) ||
								(oldThirdPartyProductID != null && pr.OldThirdPartyProductID == oldThirdPartyProductID)))
							 select pr);
				foreach (var r in query)
				{
					visionDB.ProductReplacements.DeleteOnSubmit(r);
				}

				// Add the new record
				ClassLibrary.DAL.ProductReplacement newReplacement = new ClassLibrary.DAL.ProductReplacement
				{
					OldMasterCatalogProductID = oldMasterCatalogProductID,
					NewMasterCatalogProductID = newMasterCatalogProductID,
					OldThirdPartyProductID = oldThirdPartyProductID,
					NewThirdPartyProductID = newThirdPartyProductID,
					PracticeID = practiceID,
					StartDate = startDate,
					EndDate = endDate,
					CreatedUserID = userID,
					CreatedDate = DateTime.Now,
					ModifiedUserID = 0,
					ModifiedDate = null,
					IsActive = isActive
				};
				visionDB.ProductReplacements.InsertOnSubmit(newReplacement);
				visionDB.SubmitChanges();

				BuildProductReplacementProductTable();
			}
		}

		private static void BuildProductReplacementProductTable()
		{
			VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
			using (visionDB)
			{
				visionDB.usp_BuildProductReplacementProductTable();
			}
		}

		// UpdateProductReplacement
		public static void DeleteProductReplacement(int productReplacementID)
		{
			VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
			using (visionDB)
			{
				var query = (from pr in visionDB.ProductReplacements
							 where pr.ProductReplacementID == productReplacementID
							 select pr);

				// Remove any records for the given source product IDs
				foreach (var replacement in query)
				{
					visionDB.ProductReplacements.DeleteOnSubmit(replacement);
				}
				visionDB.SubmitChanges();
			}
		}

		// GetProductReplacements
		public static DataTable GetProductReplacements(
			int? owningMasterCatalogProductID, int? owningThirdPartyProductID, int? practiceID)
		{
			VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
			using (visionDB)
			{
				// Add the appropriate columns
				DataTable table = new DataTable();

				if (owningMasterCatalogProductID != null || owningThirdPartyProductID != null)
				{
					var query = (from pr in visionDB.ProductReplacements
								 join mcpNew in visionDB.MasterCatalogProducts on pr.NewMasterCatalogProductID equals mcpNew.MasterCatalogProductID into newmcp
								 from subnewmcp in newmcp.DefaultIfEmpty()
								 join tppNew in visionDB.ThirdPartyProducts on pr.NewThirdPartyProductID equals tppNew.ThirdPartyProductID into newtpp
								 from subnewtpp in newtpp.DefaultIfEmpty()
								 where
									pr.IsActive &&
									(((pr.PracticeID == null) || (pr.PracticeID == practiceID)) &&
									((owningThirdPartyProductID != null && pr.OldThirdPartyProductID == owningThirdPartyProductID) ||
									(owningMasterCatalogProductID != null && pr.OldMasterCatalogProductID == owningMasterCatalogProductID)))
								 select new { subnewmcp, subnewtpp, pr });
					foreach (var replacement in query)
					{
						AddReplacementRow(table, practiceID, replacement.subnewmcp, replacement.subnewtpp, replacement.pr);
					}
				}
				return table;
			}
		}

		private static void AddReplacementRow(DataTable table, int? practiceID, ClassLibrary.DAL.MasterCatalogProduct mcp, ClassLibrary.DAL.ThirdPartyProduct tpp, ClassLibrary.DAL.ProductReplacement pr)
		{
			if (table.Columns.Count == 0)
			{
				DataTableHelper.CreateColumn("Name", "Name", "System.String", table);
				DataTableHelper.CreateColumn("Code", "Code", "System.String", table);
				DataTableHelper.CreateColumn("Side", "Side", "System.String", table);
				DataTableHelper.CreateColumn("Size", "Size", "System.String", table);
				DataTableHelper.CreateColumn("PracticeID", "PracticeID", "System.String", table);
				DataTableHelper.CreateColumn("ProductReplacementID", "ProductReplacementID", "System.String", table);
				DataTableHelper.CreateColumn("OldMasterCatalogProductID", "OldMasterCatalogProductID", "System.String", table);
				DataTableHelper.CreateColumn("NewMasterCatalogProductID", "NewMasterCatalogProductID", "System.String", table);
				DataTableHelper.CreateColumn("OldThirdPartyProductID", "OldThirdPartyProductID", "System.String", table);
				DataTableHelper.CreateColumn("NewThirdPartyProductID", "NewThirdPartyProductID", "System.String", table);
				DataTableHelper.CreateColumn("StartDate", "StartDate", "System.String", table);
				DataTableHelper.CreateColumn("EndDate", "EndDate", "System.String", table);
				DataTableHelper.CreateColumn("CreatedUserID", "CreatedUserID", "System.String", table);
				DataTableHelper.CreateColumn("CreatedDate", "CreatedDate", "System.String", table);
				DataTableHelper.CreateColumn("ModifiedUserID", "ModifiedUserID", "System.String", table);
				DataTableHelper.CreateColumn("ModifiedDate", "ModifiedDate", "System.String", table);
				DataTableHelper.CreateColumn("IsActive", "IsActive", "System.String", table);
				DataTableHelper.CreateColumn("IsDeletable", "IsDeletable", "System.Boolean", table);
			}

			if (practiceID == null && pr.PracticeID != null)
				return;

			DataRow row = null;
			foreach (DataRow r in table.Rows)
			{
				object mcpObj = r["OldMasterCatalogProductID"];
				int? mcpVal = null;
				if (mcpObj != null)
					mcpVal = Convert.ToInt32(mcpObj);
				if (mcpVal == pr.OldMasterCatalogProductID && pr.OldMasterCatalogProductID != null)
				{
					row = r;
					break;
				}

				object tppObj = r["OldThirdPartyProductID"];
				int? tppVal = null;
				if (tppObj != null)
					tppVal = Convert.ToInt32(tppObj);
				if (tppVal == pr.OldThirdPartyProductID && pr.OldThirdPartyProductID != null)
				{
					row = r;
					break;
				}
			}

			object pObj = null;
			if (row != null)
				pObj = row["PracticeID"];

			// if Practice ID passed in is null then only return rows for null practice IDs
			// if Practice ID passed in is non-null then return any rows that match the practice ID or the row of Practice ID null, if no non-null practice ID rows are available
			if (
				(practiceID == null && pr.PracticeID == null && pObj == null) ||
				(practiceID != null && (pr.PracticeID == practiceID || pr.PracticeID == null) && pObj == null)
				)
			{
				row = table.NewRow();
				table.Rows.Add(row);
			}

			if (row != null)
			{
				bool isDeletable = true;
				if (practiceID != null && pr.PracticeID == null)
					isDeletable = false;

				if (mcp != null)
				{
					row["Name"] = mcp.Name;
					row["Code"] = mcp.Code;
					row["Side"] = mcp.LeftRightSide;
					row["Size"] = mcp.Size;
				}
				if (tpp != null)
				{
					row["Name"] = tpp.Name;
					row["Code"] = tpp.Code;
					row["Side"] = tpp.LeftRightSide;
					row["Size"] = tpp.Size;
				}

				if (!isDeletable)
					row["Name"] = "[MCP] " + row["Name"];

				row["PracticeID"] = pr.PracticeID;
				row["ProductReplacementID"] = pr.ProductReplacementID;
				row["OldMasterCatalogProductID"] = pr.OldMasterCatalogProductID;
				row["NewMasterCatalogProductID"] = pr.NewMasterCatalogProductID;
				row["OldThirdPartyProductID"] = pr.OldThirdPartyProductID;
				row["NewThirdPartyProductID"] = pr.NewThirdPartyProductID;
				row["StartDate"] = pr.StartDate;
				row["EndDate"] = pr.EndDate;
				row["CreatedUserID"] = pr.CreatedUserID;
				row["CreatedDate"] = pr.CreatedDate;
				row["ModifiedUserID"] = pr.ModifiedUserID;
				row["ModifiedDate"] = pr.ModifiedDate;
				row["IsActive"] = pr.IsActive;
				row["IsDeletable"] = isDeletable;
			}
		}
		#endregion
	}
}
