﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary.DAL
{
    public partial class FaxMessage
    {

        public static IEnumerable<FaxMessage> GetUnsentFaxMessages()
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext(); 
            
            var query = from fm in visionDB.FaxMessages
                        where fm.Sent == false
                        && fm.Skip == false
                        orderby fm.CreatedDate
                        select fm;

            VisionDataContext.CloseConnection(visionDB);

            return query.AsEnumerable<FaxMessage>();
        }

        public static IEnumerable<FaxMessage> GetAllFaxesSince(DateTime dtDateFrom, DateTime dtDateTo)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            var query = from fm in visionDB.FaxMessages
                        where fm.CreatedDate >= dtDateFrom && fm.CreatedDate <= dtDateTo 
                        select fm;

            VisionDataContext.CloseConnection(visionDB);

            return query.AsEnumerable<FaxMessage>();
        }

        public static IEnumerable<FaxMessage> GetFaxMessagesByTransmissionID(string TransmissionID)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            //Yuiko, The transmission ID will have the first letter of the server name now.
            //before "002347773812333"
            //after  "V02347773812333"
            //Don't use the first letter in Transmission ID

            var query = from fm in visionDB.FaxMessages
                        where fm.FaxFileName.Contains(TransmissionID) //Get rid of first character before you  try to query //Already set up validator in the page
                        orderby fm.CreatedDate
                        select fm;

            VisionDataContext.CloseConnection(visionDB);

            return query.AsEnumerable<FaxMessage>();
        }

        public static bool UpdateUnsentFaxMessagesByTransmissionID(string TransmissionID)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            IEnumerable<FaxMessage> faxMessages = FaxMessage.GetFaxMessagesByTransmissionID(TransmissionID);

            int counter = 0;
            foreach (FaxMessage faxMessage in faxMessages)
            {
                if (faxMessage.Skip == false)
                {
                    visionDB.FaxMessages.Attach(faxMessage);
                    faxMessage.Sent = false;
                    counter += 1;
                }
                else if(faxMessage.Skip == true) //added by u= 01/15/2010
                {
                    visionDB.FaxMessages.Attach(faxMessage);
                    faxMessage.Skip = false;
                    counter += 1;
                }
            }
            
            visionDB.SubmitChanges();

            VisionDataContext.CloseConnection(visionDB);

            if (counter > 0)
            {
                return true;
            }

            return false;
                    
        }
    }
}
