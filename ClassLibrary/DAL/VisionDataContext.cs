﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary.DAL
{
    public partial class VisionDataContext
    {

        public static VisionDataContext GetVisionDataContext()
        {

                return new VisionDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["BregVisionConnectionString"].ToString());
            
        }

        public static void CloseConnection(VisionDataContext visionDB)
        {
            if (visionDB != null)
            {
                if (visionDB.Connection != null && visionDB.Connection.State != System.Data.ConnectionState.Closed)
                {
                    visionDB.Connection.Close();
                    visionDB.Connection.Dispose();
                    visionDB.Dispose();
                    visionDB = null;
                }
            }
        }

        public static VisionDataContext GetVisionDataContext(string connectionString)
        {
            return new VisionDataContext(connectionString
                );
        }
    }
}
