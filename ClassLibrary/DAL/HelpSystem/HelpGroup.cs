﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using ClassLibrary.BLL.HelpSystem;

namespace ClassLibrary.DAL.HelpSystem
{
    internal class HelpGroup
    {
        BLL.HelpSystem.HelpEntity activeEntity;

        
        //for now we only query the database for groups containing any other entity type but the top level will be a group.  We'll expand this in the future to return any entity type as the top level node.
        public BLL.HelpSystem.HelpGroup PopulateGroup(BLL.HelpSystem.HelpGroup topHelpGroup, int depth)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetHelpEntityTree"))
                {
                    db.AddInParameter(dbCommand, "@Id", DbType.Int32, topHelpGroup.Id);

                    if(depth !=1000)
                    {
                        db.AddInParameter(dbCommand, "@Depth", DbType.Int32, depth);
                    }

                    DataTable dt = new DataTable();
                    dt = db.ExecuteDataSet(dbCommand).Tables[0];
                    dbCommand.Dispose();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];

                        if (i == 0 && (ClassLibrary.BLL.HelpSystem.HelpEntityType)int.Parse(dr["EntityTypeId"].ToString()) == ClassLibrary.BLL.HelpSystem.HelpEntityType.Group)
                        {
                            //Top Node is always a group for now
                            topHelpGroup = PopulateHelpGroupFromDataRow(dr);
                            activeEntity = topHelpGroup;
                        }
                        else
                        {
                            if (activeEntity.ParentId != (int.Parse(dr["ParentId"].ToString())))
                            {
                                //find the parent for the active DataRow and set it as the activeEntity
                                SetActiveEntity(topHelpGroup, int.Parse(dr["ParentId"].ToString()));
                            }

                            switch ((ClassLibrary.BLL.HelpSystem.HelpEntityType)int.Parse(dr["EntityTypeId"].ToString()))
                            {
                                case ClassLibrary.BLL.HelpSystem.HelpEntityType.Group:
                                    BLL.HelpSystem.HelpGroup helpGroup = PopulateHelpGroupFromDataRow(dr);
                                    ((BLL.HelpSystem.HelpGroup)activeEntity).HelpGroups.Add(helpGroup);
                                    break;
                                case ClassLibrary.BLL.HelpSystem.HelpEntityType.Article:
                                    BLL.HelpSystem.HelpArticle helpArticle = HelpArticle.PopulateHelpArticleFromDataRow(dr, false);
                                    ((BLL.HelpSystem.HelpGroup)activeEntity).HelpArticles.Add(helpArticle);
                                    break;
                                case ClassLibrary.BLL.HelpSystem.HelpEntityType.Media:
                                    BLL.HelpSystem.HelpMedia helpMedia = HelpMedia.PopulateHelpMediaFromDataRow(dr, false);
                                    ((BLL.HelpSystem.HelpGroup)activeEntity).HelpMedias.Add(helpMedia);
                                    break;
                                case ClassLibrary.BLL.HelpSystem.HelpEntityType.RelatedArticle:
                                    BLL.HelpSystem.HelpArticle releatedArticle = HelpArticle.PopulateHelpArticleFromDataRow(dr, true);

                                    if ((ClassLibrary.BLL.HelpSystem.HelpEntityType)activeEntity.HelpEntityType == ClassLibrary.BLL.HelpSystem.HelpEntityType.Article)
                                    {
                                        ((BLL.HelpSystem.HelpArticle)activeEntity).RelatedArticles.Add(releatedArticle);
                                    }
                                    else if ((ClassLibrary.BLL.HelpSystem.HelpEntityType)activeEntity.HelpEntityType == ClassLibrary.BLL.HelpSystem.HelpEntityType.Media) 
                                    {
                                        ((BLL.HelpSystem.HelpMedia)activeEntity).RelatedArticles.Add(releatedArticle);
                                    }
                                    break;
                                case ClassLibrary.BLL.HelpSystem.HelpEntityType.RelatedMedia:
                                    BLL.HelpSystem.HelpMedia releatedMedia = HelpMedia.PopulateHelpMediaFromDataRow(dr, true);

                                    if ((ClassLibrary.BLL.HelpSystem.HelpEntityType)activeEntity.HelpEntityType == ClassLibrary.BLL.HelpSystem.HelpEntityType.Article)
                                    {
                                        ((BLL.HelpSystem.HelpArticle)activeEntity).RelatedMedia.Add(releatedMedia);
                                    }
                                    else if ((ClassLibrary.BLL.HelpSystem.HelpEntityType)activeEntity.HelpEntityType == ClassLibrary.BLL.HelpSystem.HelpEntityType.Media)
                                    {
                                        ((BLL.HelpSystem.HelpMedia)activeEntity).RelatedMedia.Add(releatedMedia);
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            catch (SqlException se)
            {
                // add correct logging
                throw se;
            }
            catch (Exception ex)
            {
                // add correct logging
                throw ex;
            }

            return topHelpGroup;
        }


        public BLL.HelpSystem.HelpGroup SearchForHelpArticlesAndHelpMeida(string searchText)
        {
            BLL.HelpSystem.HelpGroup searchResultsGroup = new BLL.HelpSystem.HelpGroup();
            searchResultsGroup.Id = -1; //for correct placement in the treeview
            searchResultsGroup.HelpEntityType = ClassLibrary.BLL.HelpSystem.HelpEntityType.NotSet;
            searchResultsGroup.Title = "Search Results: " + searchText;

            try
            {
                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_SearchForHelpArticlesAndHelpMeida"))
                {
                    db.AddInParameter(dbCommand, "@searchText", DbType.String, searchText);

                    DataTable dtArticles = new DataTable();
                    DataTable dtMedias = new DataTable();
                    dtArticles = db.ExecuteDataSet(dbCommand).Tables[0];
                    dtMedias = db.ExecuteDataSet(dbCommand).Tables[1];
                    dbCommand.Dispose();

                    for (int i = 0; i < dtArticles.Rows.Count; i++)
                    {
                        DataRow dr = dtArticles.Rows[i];
                        BLL.HelpSystem.HelpArticle helpArticle = HelpArticle.PopulateHelpArticleFromDataRow(dr, false);
                        helpArticle.ParentId = -1; //for correct placement in the treeview
                        searchResultsGroup.HelpArticles.Add(helpArticle);
                    }

                    for (int i = 0; i < dtMedias.Rows.Count; i++)
                    {
                        DataRow dr = dtMedias.Rows[i];
                        BLL.HelpSystem.HelpMedia helpMedia = HelpMedia.PopulateHelpMediaFromDataRow(dr, false);
                        helpMedia.ParentId = -1; //for correct placement in the treeview
                        searchResultsGroup.HelpMedias.Add(helpMedia);
                    }

                }
            }
            catch (SqlException se)
            {
                // add correct logging
                throw se;
            }
            catch (Exception ex)
            {
                // add correct logging
                throw ex;
            }

            return searchResultsGroup;
        }

        private bool SetActiveEntity(BLL.HelpSystem.HelpEntity entity, int Id)
        {
            if (entity.Id == Id)
            {
                activeEntity = entity;
                return true;
            }
            else
            {
                switch (entity.HelpEntityType)
                {
                    case ClassLibrary.BLL.HelpSystem.HelpEntityType.Group:
                        foreach (BLL.HelpSystem.HelpGroup group in ((BLL.HelpSystem.HelpGroup)entity).HelpGroups)
                        {
                            if (SetActiveEntity(group, Id))
                                return true;
                        }

                        foreach (BLL.HelpSystem.HelpArticle article in ((BLL.HelpSystem.HelpGroup)entity).HelpArticles)
                        {
                            if(SetActiveEntity(article, Id))
                                return true;
                        }

                        foreach (BLL.HelpSystem.HelpMedia media in ((BLL.HelpSystem.HelpGroup)entity).HelpMedias)
                        {
                            if(SetActiveEntity(media, Id))
                                return true;
                        }
                        break;

                    case ClassLibrary.BLL.HelpSystem.HelpEntityType.Article:
                        foreach (BLL.HelpSystem.HelpArticle article in ((BLL.HelpSystem.HelpArticle)entity).RelatedArticles)
                        {
                            if(SetActiveEntity(article, Id))
                                return true;
                        }

                        foreach (BLL.HelpSystem.HelpMedia media in ((BLL.HelpSystem.HelpArticle)entity).RelatedMedia)
                        {
                            if(SetActiveEntity(media, Id))
                                return true;
                        }

                        break;

                    case ClassLibrary.BLL.HelpSystem.HelpEntityType.Media:
                        foreach (BLL.HelpSystem.HelpArticle article in ((BLL.HelpSystem.HelpMedia)entity).RelatedArticles)
                        {
                            if(SetActiveEntity(article, Id))
                                return true;
                        }

                        foreach (BLL.HelpSystem.HelpMedia media in ((BLL.HelpSystem.HelpMedia)entity).RelatedMedia)
                        {
                            if(SetActiveEntity(media, Id))
                                return true;
                        }

                        break;

                    case ClassLibrary.BLL.HelpSystem.HelpEntityType.RelatedArticle:
                    case ClassLibrary.BLL.HelpSystem.HelpEntityType.RelatedMedia:
                        //these guys don't have children
                        break;
                    default:
                        break;
                }
            }

            return false;
        }

        public static BLL.HelpSystem.HelpGroup PopulateHelpGroupFromDataRow(DataRow dr)
        {
            BLL.HelpSystem.HelpGroup helpGroup = new ClassLibrary.BLL.HelpSystem.HelpGroup();

            helpGroup.Id = int.Parse(dr["Id"].ToString());
            
            if (!Convert.IsDBNull(dr["ParentId"]))
            {
                helpGroup.ParentId = int.Parse(dr["ParentId"].ToString());
            }
            
            helpGroup.EntityId = int.Parse(dr["EntityId"].ToString());
            helpGroup.HelpEntityType = (ClassLibrary.BLL.HelpSystem.HelpEntityType)int.Parse(dr["EntityTypeId"].ToString());
            helpGroup.SortOrder = int.Parse(dr["SortOrder"].ToString());
            helpGroup.Level = int.Parse(dr["Level"].ToString());
            helpGroup.TopParentId = int.Parse(dr["TopParentId"].ToString());
            
            helpGroup.Title = dr["GroupTitle"].ToString();

            return helpGroup;
        }

        
        
    }
}
