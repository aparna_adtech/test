﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClassLibrary.BLL.HelpSystem;
using System.Data;

namespace ClassLibrary.DAL.HelpSystem
{
    internal class HelpMedia
    {
        public static BLL.HelpSystem.HelpMedia PopulateHelpMediaFromDataRow(DataRow dr, bool IsRelatedMedia)
        {
            BLL.HelpSystem.HelpMedia helpMedia = new ClassLibrary.BLL.HelpSystem.HelpMedia();

            helpMedia.Id = int.Parse(dr["Id"].ToString());
            helpMedia.ParentId = int.Parse(dr["ParentId"].ToString());
            helpMedia.EntityId = int.Parse(dr["EntityId"].ToString());
            helpMedia.HelpEntityType = (ClassLibrary.BLL.HelpSystem.HelpEntityType)int.Parse(dr["EntityTypeId"].ToString());
            helpMedia.SortOrder = int.Parse(dr["SortOrder"].ToString());
            helpMedia.Level = int.Parse(dr["Level"].ToString());
            helpMedia.TopParentId = int.Parse(dr["TopParentId"].ToString());

            if (IsRelatedMedia)
            {
                helpMedia.Title = dr["RelatedMediaTitle"].ToString();
                helpMedia.Description = dr["RelatedMediaDescription"].ToString();
                helpMedia.Url = dr["RelatedMediaUrl"].ToString();
            }
            else
            {
                helpMedia.Title = dr["MediaTitle"].ToString();
                helpMedia.Description = dr["MediaDescription"].ToString();
                helpMedia.Url = dr["MediaUrl"].ToString();
            }
            return helpMedia;
        }
    }
}
