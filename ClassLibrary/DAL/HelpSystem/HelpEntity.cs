﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace ClassLibrary.DAL.HelpSystem
{
    internal class HelpEntity
    {
        public BLL.HelpSystem.HelpEntity InsertHelpEntity(BLL.HelpSystem.HelpEntity entity)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_InsertHelpEntity"))
                {
                    db.AddOutParameter(dbCommand, "@Id", DbType.Int32, 4);
                    db.AddOutParameter(dbCommand, "@EntityId", DbType.Int32, 4);

                    db.AddInParameter(dbCommand, "@ParentId", DbType.Int32,entity.ParentId);
                    db.AddInParameter(dbCommand, "@EntityTypeId", DbType.Int32, entity.HelpEntityType);
                    db.AddInParameter(dbCommand, "@SortOrder", DbType.Int32, entity.SortOrder);
                    db.AddInParameter(dbCommand, "@Title", DbType.String, entity.Title);

                    if (entity.HelpEntityType == ClassLibrary.BLL.HelpSystem.HelpEntityType.Article)
                    {
                        db.AddInParameter(dbCommand, "@Description", DbType.String, ((BLL.HelpSystem.HelpArticle)entity).Description);
                    }

                    if (entity.HelpEntityType == ClassLibrary.BLL.HelpSystem.HelpEntityType.Media)
                    {
                        db.AddInParameter(dbCommand, "@Description", DbType.String, ((BLL.HelpSystem.HelpMedia)entity).Description);
                        db.AddInParameter(dbCommand, "@Url", DbType.String, ((BLL.HelpSystem.HelpMedia)entity).Url);
                    }

                    db.ExecuteNonQuery(dbCommand);

                    entity.Id = (int)db.GetParameterValue(dbCommand, "@Id");
                    entity.EntityId = (int)db.GetParameterValue(dbCommand, "@EntityId");
                    
                    dbCommand.Dispose();
                }
            }
            catch (SqlException se)
            {
                // add correct logging
                throw se;
            }
            catch (Exception ex)
            {
                // add correct logging
                throw ex;
            }

            return entity;
        }

        public BLL.HelpSystem.HelpEntity InsertHelpEntityMap(BLL.HelpSystem.HelpEntity entity)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_InsertHelpEntityMap"))
                {
                    db.AddOutParameter(dbCommand, "@Id", DbType.Int32, 4);
                    db.AddInParameter(dbCommand, "@EntityId", DbType.Int32, entity.EntityId);

                    db.AddInParameter(dbCommand, "@ParentId", DbType.Int32, entity.ParentId);
                    db.AddInParameter(dbCommand, "@EntityTypeId", DbType.Int32, entity.HelpEntityType);
                    db.AddInParameter(dbCommand, "@SortOrder", DbType.Int32, entity.SortOrder);
                    
                    db.ExecuteNonQuery(dbCommand);

                    entity.Id = (int)db.GetParameterValue(dbCommand, "@Id");
                    
                    dbCommand.Dispose();
                }
            }
            catch (SqlException se)
            {
                // add correct logging
                throw se;
            }
            catch (Exception ex)
            {
                // add correct logging
                throw ex;
            }

            return entity;
        }

        public BLL.HelpSystem.HelpEntity UpdateHelpEntity(BLL.HelpSystem.HelpEntity entity)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_UpdateHelpEntity"))
                {
                    
                    db.AddInParameter(dbCommand, "@Id", DbType.Int32, entity.Id);
                    db.AddInParameter(dbCommand, "@EntityId", DbType.Int32, entity.EntityId);
                    db.AddInParameter(dbCommand, "@ParentId", DbType.Int32, entity.ParentId);
                    db.AddInParameter(dbCommand, "@EntityTypeId", DbType.Int32, entity.HelpEntityType);
                    db.AddInParameter(dbCommand, "@SortOrder", DbType.Int32, entity.SortOrder);
                    db.AddInParameter(dbCommand, "@Title", DbType.String, entity.Title);

                    if (entity.HelpEntityType == ClassLibrary.BLL.HelpSystem.HelpEntityType.Article)
                    {
                        db.AddInParameter(dbCommand, "@Description", DbType.String, ((BLL.HelpSystem.HelpArticle)entity).Description);
                    }

                    if (entity.HelpEntityType == ClassLibrary.BLL.HelpSystem.HelpEntityType.Media)
                    {
                        db.AddInParameter(dbCommand, "@Description", DbType.String, ((BLL.HelpSystem.HelpMedia)entity).Description);
                        db.AddInParameter(dbCommand, "@Url", DbType.String, ((BLL.HelpSystem.HelpMedia)entity).Url);
                    }

                    db.ExecuteNonQuery(dbCommand);

                    dbCommand.Dispose();
                }
            }
            catch (SqlException se)
            {
                // add correct logging
                throw se;
            }
            catch (Exception ex)
            {
                // add correct logging
                throw ex;
            }

            return entity;
        }

        public BLL.HelpSystem.HelpEntity UpdateHelpEntityMap(BLL.HelpSystem.HelpEntity entity)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_UpdateHelpEntityMap"))
                {

                    db.AddInParameter(dbCommand, "@Id", DbType.Int32, entity.Id);
                    db.AddInParameter(dbCommand, "@ParentId", DbType.Int32, entity.ParentId);
                    db.AddInParameter(dbCommand, "@SortOrder", DbType.Int32, entity.SortOrder);
                                       
                    db.ExecuteNonQuery(dbCommand);

                    dbCommand.Dispose();
                }
            }
            catch (SqlException se)
            {
                // add correct logging
                throw se;
            }
            catch (Exception ex)
            {
                // add correct logging
                throw ex;
            }

            return entity;
        }

        public BLL.HelpSystem.HelpEntity DeleteHelpEntity(BLL.HelpSystem.HelpEntity entity, bool removeEntity)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_DeleteHelpEntity"))
                {
                    db.AddInParameter(dbCommand, "@Id", DbType.Int32, entity.Id);
                    db.AddInParameter(dbCommand, "@EntityId", DbType.Int32, entity.EntityId);
                    db.AddInParameter(dbCommand, "@EntityTypeId", DbType.Int32, entity.HelpEntityType);
                    db.AddInParameter(dbCommand, "@RemoveEntity", DbType.Boolean, removeEntity);
                   
                    db.ExecuteNonQuery(dbCommand);

                    dbCommand.Dispose();
                }
            }
            catch (SqlException se)
            {
                // add correct logging
                throw se;
            }
            catch (Exception ex)
            {
                // add correct logging
                throw ex;
            }

            return entity;
        }

        
    }
}
