﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using ClassLibrary.BLL.HelpSystem;
using System.Data.SqlClient;

namespace ClassLibrary.DAL.HelpSystem
{
    internal class HelpArticle
    {
        BLL.HelpSystem.HelpEntity activeEntity;

        public BLL.HelpSystem.HelpArticle PopulateArticle(BLL.HelpSystem.HelpArticle topHelpArticle)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetHelpEntityTree"))
                {
                    db.AddInParameter(dbCommand, "@Id", DbType.Int32, topHelpArticle.Id);
                    
                    DataTable dt = new DataTable();
                    dt = db.ExecuteDataSet(dbCommand).Tables[0];
                    dbCommand.Dispose();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];

                        if (i == 0 && (ClassLibrary.BLL.HelpSystem.HelpEntityType)int.Parse(dr["EntityTypeId"].ToString()) == ClassLibrary.BLL.HelpSystem.HelpEntityType.Article)
                        {
                            //we assume this is an article and not a related article, hence false on the 2nd param
                            topHelpArticle = HelpArticle.PopulateHelpArticleFromDataRow(dr, false);
                            activeEntity = topHelpArticle;
                        }
                        else
                        {
                            if (activeEntity.ParentId != (int.Parse(dr["ParentId"].ToString())))
                            {
                                //find the parent for the active DataRow and set it as the activeEntity
                                SetActiveEntity(topHelpArticle, int.Parse(dr["ParentId"].ToString()));
                            }

                            switch ((ClassLibrary.BLL.HelpSystem.HelpEntityType)int.Parse(dr["EntityTypeId"].ToString()))
                            {
                                case ClassLibrary.BLL.HelpSystem.HelpEntityType.Group:
                                    BLL.HelpSystem.HelpGroup helpGroup = HelpGroup.PopulateHelpGroupFromDataRow(dr);
                                    ((BLL.HelpSystem.HelpGroup)activeEntity).HelpGroups.Add(helpGroup);
                                    break;
                                case ClassLibrary.BLL.HelpSystem.HelpEntityType.Article:
                                    BLL.HelpSystem.HelpArticle helpArticle = HelpArticle.PopulateHelpArticleFromDataRow(dr, false);
                                    ((BLL.HelpSystem.HelpGroup)activeEntity).HelpArticles.Add(helpArticle);
                                    break;
                                case ClassLibrary.BLL.HelpSystem.HelpEntityType.Media:
                                    BLL.HelpSystem.HelpMedia helpMedia = HelpMedia.PopulateHelpMediaFromDataRow(dr, false);
                                    ((BLL.HelpSystem.HelpGroup)activeEntity).HelpMedias.Add(helpMedia);
                                    break;
                                case ClassLibrary.BLL.HelpSystem.HelpEntityType.RelatedArticle:
                                    BLL.HelpSystem.HelpArticle releatedArticle = PopulateHelpArticleFromDataRow(dr, true);

                                    if ((ClassLibrary.BLL.HelpSystem.HelpEntityType)activeEntity.HelpEntityType == ClassLibrary.BLL.HelpSystem.HelpEntityType.Article)
                                    {
                                        ((BLL.HelpSystem.HelpArticle)activeEntity).RelatedArticles.Add(releatedArticle);
                                    }
                                    else if ((ClassLibrary.BLL.HelpSystem.HelpEntityType)activeEntity.HelpEntityType == ClassLibrary.BLL.HelpSystem.HelpEntityType.Media)
                                    {
                                        ((BLL.HelpSystem.HelpMedia)activeEntity).RelatedArticles.Add(releatedArticle);
                                    }
                                    break;
                                case ClassLibrary.BLL.HelpSystem.HelpEntityType.RelatedMedia:
                                    BLL.HelpSystem.HelpMedia releatedMedia = HelpMedia.PopulateHelpMediaFromDataRow(dr, true);

                                    if ((ClassLibrary.BLL.HelpSystem.HelpEntityType)activeEntity.HelpEntityType == ClassLibrary.BLL.HelpSystem.HelpEntityType.Article)
                                    {
                                        ((BLL.HelpSystem.HelpArticle)activeEntity).RelatedMedia.Add(releatedMedia);
                                    }
                                    else if ((ClassLibrary.BLL.HelpSystem.HelpEntityType)activeEntity.HelpEntityType == ClassLibrary.BLL.HelpSystem.HelpEntityType.Media)
                                    {
                                        ((BLL.HelpSystem.HelpMedia)activeEntity).RelatedMedia.Add(releatedMedia);
                                    }
                                    break;
                                default:
                                    break;
                            }


                        }
                    }
                }
            }
            catch (SqlException se)
            {
                // add correct logging
                throw se;
            }
            catch (Exception ex)
            {
                // add correct logging
                throw ex;
            }

            return topHelpArticle;
        }

        public static BLL.HelpSystem.HelpArticle PopulateHelpArticleFromDataRow(DataRow dr, bool IsRelatedArticle)
        {
            BLL.HelpSystem.HelpArticle helpArticle = new ClassLibrary.BLL.HelpSystem.HelpArticle();

            helpArticle.Id = int.Parse(dr["Id"].ToString());
            helpArticle.ParentId = int.Parse(dr["ParentId"].ToString());
            helpArticle.EntityId = int.Parse(dr["EntityId"].ToString());
            helpArticle.HelpEntityType = (ClassLibrary.BLL.HelpSystem.HelpEntityType)int.Parse(dr["EntityTypeId"].ToString());
            helpArticle.SortOrder = int.Parse(dr["SortOrder"].ToString());
            helpArticle.Level = int.Parse(dr["Level"].ToString());
            helpArticle.TopParentId = int.Parse(dr["TopParentId"].ToString());

            if (IsRelatedArticle)
            {
                helpArticle.Title = dr["RelatedArticleTitle"].ToString();
                helpArticle.Description = dr["RelatedArticleDescription"].ToString();
            }
            else
            {
                helpArticle.Title = dr["ArticleTitle"].ToString();
                helpArticle.Description = dr["ArticleDescription"].ToString();
            }

            return helpArticle;
        }

        private bool SetActiveEntity(BLL.HelpSystem.HelpEntity entity, int Id)
        {
            if (entity.Id == Id)
            {
                activeEntity = entity;
                return true;
            }
            else
            {
                switch (entity.HelpEntityType)
                {
                    case ClassLibrary.BLL.HelpSystem.HelpEntityType.Group:
                        foreach (BLL.HelpSystem.HelpGroup group in ((BLL.HelpSystem.HelpGroup)entity).HelpGroups)
                        {
                            if (SetActiveEntity(group, Id))
                                return true;
                        }

                        foreach (BLL.HelpSystem.HelpArticle article in ((BLL.HelpSystem.HelpGroup)entity).HelpArticles)
                        {
                            if (SetActiveEntity(article, Id))
                                return true;
                        }

                        foreach (BLL.HelpSystem.HelpMedia media in ((BLL.HelpSystem.HelpGroup)entity).HelpMedias)
                        {
                            if (SetActiveEntity(media, Id))
                                return true;
                        }
                        break;

                    case ClassLibrary.BLL.HelpSystem.HelpEntityType.Article:
                        foreach (BLL.HelpSystem.HelpArticle article in ((BLL.HelpSystem.HelpArticle)entity).RelatedArticles)
                        {
                            if (SetActiveEntity(article, Id))
                                return true;
                        }

                        foreach (BLL.HelpSystem.HelpMedia media in ((BLL.HelpSystem.HelpArticle)entity).RelatedMedia)
                        {
                            if (SetActiveEntity(media, Id))
                                return true;
                        }

                        break;

                    case ClassLibrary.BLL.HelpSystem.HelpEntityType.Media:
                        foreach (BLL.HelpSystem.HelpArticle article in ((BLL.HelpSystem.HelpMedia)entity).RelatedArticles)
                        {
                            if (SetActiveEntity(article, Id))
                                return true;
                        }

                        foreach (BLL.HelpSystem.HelpMedia media in ((BLL.HelpSystem.HelpMedia)entity).RelatedMedia)
                        {
                            if (SetActiveEntity(media, Id))
                                return true;
                        }

                        break;

                    case ClassLibrary.BLL.HelpSystem.HelpEntityType.RelatedArticle:
                    case ClassLibrary.BLL.HelpSystem.HelpEntityType.RelatedMedia:
                        //these guys don't have children
                        break;
                    default:
                        break;
                }
            }

            return false;
        }

    }
}
