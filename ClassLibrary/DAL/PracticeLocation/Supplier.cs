﻿using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

namespace DAL.PracticeLocation
{
    public class Supplier
    {

        public static DataSet GetSuppliers(int practiceLocationID)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetSuppliers");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;


        }

        public static DataSet GetSuppliers(int practiceLocationID, string SearchCriteria, string SearchText)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetSuppliers_Searchable");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
            db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);
            DataSet ds = db.ExecuteDataSet(dbCommand);

            return ds;
        }        
        
        public static DataSet GetPracticeCatalogSuppliers(int practiceLocationID, string searchCriteria, string searchText)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_SearchforDispensements");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, searchCriteria);
            db.AddInParameter(dbCommand, "SearchText", DbType.String, searchText);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;

        }

        public static DataSet SearchForPOsBySupplierAndOrDateRange(Int32 practiceLocationID, int searchCriteria, string searchText, DateTime FromDate,DateTime ToDate)
        {
            if (!IsDate(FromDate))
            {
                FromDate = DateTime.Now;
            }
            if (!IsDate(ToDate))
            {
                ToDate = DateTime.Now;
            }

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_SearchForPOsBySupplierAndOrDateRange");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            db.AddInParameter(dbCommand, "SearchCriteria", DbType.Int16, searchCriteria);
            db.AddInParameter(dbCommand, "SearchText", DbType.String, searchText);
            db.AddInParameter(dbCommand, "SearchFromDate", DbType.DateTime, FromDate);
            db.AddInParameter(dbCommand, "SearchToDate", DbType.DateTime, ToDate);
            DataSet ds = db.ExecuteDataSet(dbCommand);

            return ds;
        }

        public static bool IsDate(Object obj)
        {
            string strDate = obj.ToString();
            try
            {
                DateTime dt = DateTime.Parse(strDate);
                if (dt != DateTime.MinValue && dt != DateTime.MaxValue)
                    return true;
                return false;
            }
            catch
            {
                return false;
            }
        }

    }
}
