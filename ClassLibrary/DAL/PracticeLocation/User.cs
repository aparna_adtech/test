using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using BLL;

namespace DAL.PracticeLocation
{

	/// <summary>
	/// Summary description for PracticeLocation User
	/// </summary>
	public class User
	{
		public User()
		{
			//
			// Add constructor logic here
			//
		}

		#region SelectAllNames

		public DataTable SelectAllNames( int practiceLocationID )
		{
			Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_User_Select_All_FullNames_By_PracticeLocationID"))
			{
				db.AddInParameter( dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID );

				DataSet ds = new DataSet();
				DataTable dt = new DataTable();
				ds = db.ExecuteDataSet( dbCommand );
				dt = ds.Tables[0];
				int count = dt.Rows.Count;
				dbCommand.Dispose();
				return dt;
			}
		}

		#endregion

		#region SelectAllNonLocationNames

		public DataTable SelectAllNonLocationNames( int practiceLocationID )
		{
			Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_User_Select_All_NonLocation_FullNames_By_PracticeLocationID"))
			{
				db.AddInParameter( dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID );

				DataSet ds = new DataSet();
				DataTable dt = new DataTable();
				ds = db.ExecuteDataSet( dbCommand );
				dt = ds.Tables[0];
				int count = dt.Rows.Count;
				dbCommand.Dispose();
				return dt;
			}
		}

		#endregion


		#region Insert

		public int Insert( BLL.PracticeLocation.User bllPLU )
		//public void Insert( int practiceLocationID, int physicianID, int userID )
		{
			int rowsAffected = 0;

			int practiceLocationID = bllPLU.PracticeLocationID;
            int userUserID = bllPLU.UserUserID;
			int userID = bllPLU.UserID;

			Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_User_Insert"))
			{
				db.AddInParameter( dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID );
				db.AddInParameter( dbCommand, "UserID", DbType.Int32, userUserID );
				db.AddInParameter( dbCommand, "CreatedUserID", DbType.Int32, userID );

				rowsAffected = db.ExecuteNonQuery( dbCommand );

				dbCommand.Dispose();
			}

			return rowsAffected;
		}

		#endregion


		#region Delete

		public int Delete( BLL.PracticeLocation.User bllPLU )
		{
			int rowsAffected = 0;

			int practiceLocationID = bllPLU.PracticeLocationID;
            int userUserID = bllPLU.UserUserID;
            int userID = bllPLU.UserID;

			Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_User_Delete"))
			{
				db.AddInParameter( dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID );
				db.AddInParameter( dbCommand, "UserID", DbType.Int32, userUserID );
				db.AddInParameter( dbCommand, "ModifiedUserID", DbType.Int32, userID );

				rowsAffected = db.ExecuteNonQuery( dbCommand );

				//practiceID = Convert.ToInt32( db.GetParameterValue( dbCommand, "PracticeID" ) );
				//int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE")); 

				dbCommand.Dispose();
			}

			return rowsAffected;
		}

		#endregion

	}

}

