﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using ClassLibrary.DAL;
using Microsoft.Practices.EnterpriseLibrary.Data;


namespace DAL.PracticeLocation
{
    public class ShoppingCart
    {

        public static string GetConsignmentAdminEmail()
        {
            return ConfigurationManager.AppSettings["ConsignmentAdminEmailAddress"];
        }

        public static bool GetConsignment(int shoppingCartID, int bregVisionOrderID)
        {
            //Set IsConsignment = true here.
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var shoppingCartQuery = from sc in visionDB.ShoppingCarts
                                        where sc.ShoppingCartID == shoppingCartID
                                        select sc;
                ClassLibrary.DAL.ShoppingCart cart = shoppingCartQuery.FirstOrDefault<ClassLibrary.DAL.ShoppingCart>();
                if (cart != null)
                {
                    return cart.IsConsignment;

                }
                else
                {
                    if (bregVisionOrderID > 0)
                    {
                        var bvoQuery = from bvo in visionDB.BregVisionOrders
                                       where bvo.BregVisionOrderID == bregVisionOrderID
                                       select bvo;
                        ClassLibrary.DAL.BregVisionOrder order = bvoQuery.FirstOrDefault<ClassLibrary.DAL.BregVisionOrder>();
                        if (order != null)
                        {
                            return order.IsConsignment;
                        }
                    }
                }

                throw new System.ApplicationException("Shopping Cart Item Not Found");
            }
        }

        public static void SetConsignment(int shoppingCartID)
        {
            //Set IsConsignment = true here.
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var shoppingCartQuery = from sc in visionDB.ShoppingCarts
                                        where sc.ShoppingCartID == shoppingCartID
                                        select sc;
                ClassLibrary.DAL.ShoppingCart cart = shoppingCartQuery.FirstOrDefault<ClassLibrary.DAL.ShoppingCart>();
                if (cart != null)
                {
                    cart.IsConsignment = true;
                    visionDB.SubmitChanges();
                }
            }
        }

        public static void SetShoppingCartAsConsignment(int shoppingCartID)
        {
            //Set IsConsignment = true here.
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var shoppingCartQuery = from sc in visionDB.ShoppingCarts
                                        where sc.ShoppingCartID == shoppingCartID
                                        select sc;
                ClassLibrary.DAL.ShoppingCart cart = shoppingCartQuery.FirstOrDefault<ClassLibrary.DAL.ShoppingCart>();
                if (cart != null)
                {
                    cart.IsConsignment = true;
                    visionDB.SubmitChanges();
                }
            }
        }


        public static int UpdateCart(int ShoppingCartItemID, int Quantity)
        {
            // Only update if the Quantity is more than zero.
            //if (Quantity > 0)
            //{
            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_UpdateShoppingCartQuantities");

            db.AddInParameter(dbCommand, "ShoppingCartItemID", DbType.Int32, ShoppingCartItemID);
            db.AddInParameter(dbCommand, "Quantity", DbType.Int32, Quantity);
            //    db.AddOutParameter(dbCommand, "ShoppingCartIDReturn", DbType.Int32, rowsAffected);

            int SQLReturn = db.ExecuteNonQuery(dbCommand);
            //return ds;
            //Int32 return_value = Convert.ToInt32(db.GetParameterValue(dbCommand, "ShoppingCartIDReturn"));
            return Quantity;            
            //}
        }

        public static DataSet GetShoppingCart(int practiceLocationID)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetShoppingCart");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            //db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public static void UpdateShippingType(int practiceLocationID, int shippingTypeID)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var cartQuery = from sc in visionDB.ShoppingCarts
                                where (sc.PracticeLocationID == practiceLocationID) && sc.IsActive
                                select sc;

                var shoppingCart = cartQuery.FirstOrDefault();

                if (shoppingCart == null)
                {
                    throw new System.ApplicationException(
                        string.Format(
                            "UpdateShippingType called when there is no active shopping cart (practiceLocationID={0}).",
                            practiceLocationID));
                }

                shoppingCart.ShippingTypeID = System.Convert.ToByte(shippingTypeID);
                visionDB.SubmitChanges();
            }
        }

        public static int GetShippingType(int practiceLocationID)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var cartQuery = from sc in visionDB.ShoppingCarts
                                where sc.PracticeLocationID == practiceLocationID
                                select sc;

                ClassLibrary.DAL.ShoppingCart shoppingCart = cartQuery.FirstOrDefault();

                return System.Convert.ToInt32(shoppingCart.ShippingTypeID == null ? 0 : shoppingCart.ShippingTypeID);
            }
        }

        public static bool ContainsCustomBrace(int practiceLocationID)
        {
            DataSet dsCart = ShoppingCart.GetShoppingCart(practiceLocationID);

            IEnumerable<DataRow> query = (from sci in dsCart.Tables[0].AsEnumerable()
                                         where sci.Field<bool>("IsThirdPartyProduct") == false
                                         && (
                                                sci.Field<bool>("IsCustomBrace") == true
                                                || sci.Field<bool>("IsCustomBraceAccessory") == true
                                            )
                                         select sci);

            if (query.Count() > 0)
                return true;

            return false;

        }
    }
}
