﻿using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

namespace DAL.PracticeLocation
{
    public class Receipt
    {


        public static DataSet GetReceiptHistoryParent(int practiceLocationID, string SearchCriteria, string SearchText)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_ReceiptHistory");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
            db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);
            DataSet ds = db.ExecuteDataSet(dbCommand);

            return ds;
        }

        public static DataSet GetAllReceiptHistoryDataByLocation(int practiceLocationID, string SearchCriteria, string SearchText)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_ReceiptHistory_Detail");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
            db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
    }
}
