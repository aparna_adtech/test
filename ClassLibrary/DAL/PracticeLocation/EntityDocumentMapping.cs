﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;

namespace ClassLibrary.DAL.PracticeLocation
{
    public class EntityDocumentMapping
    {
        public EntityDocumentMapping() { }

        public int Insert(ClassLibrary.BLL.PracticeLocation.EntityDocumentMapping bllEntityMapping)
        {
            int rowsAffected = 0;
            int newEntityMappingID = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_Insert_EntityDocumentMapping"))
            {
                db.AddInParameter(dbCommand, "EntityTypeID", DbType.Int32, bllEntityMapping);
                db.AddInParameter(dbCommand, "EntityID", DbType.Int32, bllEntityMapping);
                db.AddInParameter(dbCommand, "DocumentTypeID", DbType.Int32, bllEntityMapping);
                db.AddInParameter(dbCommand, "DocumentID", DbType.Int32, bllEntityMapping);
                db.AddInParameter(dbCommand, "DocumentSourceID", DbType.Int32, bllEntityMapping);
                db.AddInParameter(dbCommand, "CreatedUserID", DbType.Int32, bllEntityMapping);
                db.AddOutParameter(dbCommand, "EntityDocumentMappingID", DbType.Int32, newEntityMappingID);

                rowsAffected = db.ExecuteNonQuery(dbCommand);

                newEntityMappingID = Convert.ToInt32(db.GetParameterValue(dbCommand, "EntityDocumentMappingID"));

                return newEntityMappingID;
            }
        }
    }
}
