﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Linq;
using ClassLibrary.DAL;

namespace DAL.PracticeLocation
{

    public class Inventory
    {
        #region Constructors

        public Inventory()
        {
            // Add constructor logic here
        }

        #endregion



        #region Methods

        public static void UpdateIsLogoAblePart(int practiceCatalogProductID, bool enabled)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var piQuery = (from pi in visionDB.ProductInventories
                               where pi.PracticeCatalogProductID == practiceCatalogProductID
                               && pi.IsLogoAblePart != enabled
                               select pi);

                piQuery.Update(rhi => rhi.IsLogoAblePart = enabled);
                visionDB.SubmitChanges();
            }
        }

        public static void UpdateConsignmentLevelLocks(int practiceLocationID, bool lockParLevels)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var piQuery = (from pi in visionDB.ProductInventories
                            where pi.PracticeLocationID == practiceLocationID
                            && pi.ConsignmentLockParLevel != lockParLevels
                            && pi.IsConsignment == true
                            select pi);  

                piQuery.Update(rhi => rhi.ConsignmentLockParLevel = lockParLevels);
                piQuery.Update(rhi => rhi.ParLevel = rhi.ConsignmentQuantity);

                visionDB.SubmitChanges();
            }
        }

        public static Int32 UpdateGrdInventoryData(Int32 practiceLocationID, Int32 practiceCatalogProductID, Int32 UserID, Int32 Quantity)
        {
            int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_Inventory_Update_Insert");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            db.AddInParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, practiceCatalogProductID);
            db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
            db.AddInParameter(dbCommand, "Quantity", DbType.Int32, Quantity);
            db.AddOutParameter(dbCommand, "ShoppingCartIDReturn", DbType.Int32, rowsAffected);

            int SQLReturn = db.ExecuteNonQuery(dbCommand);
            //return ds;
            Int32 return_value = Convert.ToInt32(db.GetParameterValue(dbCommand, "ShoppingCartIDReturn"));
            return return_value;
        }


        public static DataSet GetAllProductDatabyLocation(int practiceID, int practiceLocationID, string SearchCriteria, string SearchText)
        {

            //DataSet ds = null;
            //Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            //DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetAllProductDatabyPracticeOrLocation");

            //db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            //db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            //db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
            //db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);

            //ds = db.ExecuteDataSet(dbCommand);

            //return ds;

            using (var c = new SqlConnection(ConfigurationManager.ConnectionStrings["BregVisionConnectionString"].ConnectionString))
            {
                c.Open();
                var cmd = c.CreateCommand();
                cmd.CommandText = "usp_GetAllProductDatabyPracticeOrLocation";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter { ParameterName = "PracticeLocationID", DbType = DbType.Int32, Value = practiceLocationID });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "PracticeID", DbType = DbType.Int32, Value = practiceID });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "SearchCriteria", DbType = DbType.String, Value = SearchCriteria });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "SearchText", DbType = DbType.String, Value = SearchText });
                var r = cmd.ExecuteReader();
                var dt = new DataTable();
                dt.Load(r);
                var ds = new DataSet();
                ds.Tables.Add(dt);
                return ds;
            }
        }

        //This function has a capital B in ByLocation
        public static DataSet GetAllProductDataByLocation(int practiceID, int practiceLocationID, string SearchCriteria, string SearchText)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_Get_Dispense_Records_for_Browse_Grid_NewTEst");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
            db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        #region "Get Custom Brace Info from Order (Cart)"

        public static DataSet GetAllInfoFromOrder(int BregVisionOrderID)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetBVOSupplierInfoForFaxEmail_New");

            db.AddInParameter(dbCommand, "BregVisionOrderID", DbType.Int32, BregVisionOrderID);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public static DataSet GetLineItemInfoFromOrder(int SupplierOrderID)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetBVOSupplierLineItemInfoForFaxEmail_New");

            db.AddInParameter(dbCommand, "SupplierOrderID", DbType.Int32, SupplierOrderID);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        /// <summary>
        /// Gets custom Brace info from the Order
        /// </summary>
        /// <param name="BregVisionOrderID"></param>
        /// <param name="GetBrace">send true if you want the brace info, false if you want the accessories</param>
        /// <returns></returns>
        public static DataSet GetCustomBraceProductInfoFromOrder(int BregVisionOrderID)
        {
			int supplierOrderID = BregVisionOrderID;
			DataSet dsOrder = GetAllInfoFromOrder(BregVisionOrderID);
			if (dsOrder != null && dsOrder.Tables.Count > 0 && dsOrder.Tables[0].Rows.Count > 0)
			{
				object id = dsOrder.Tables[0].Rows[0]["SupplierOrderID"];
				if (id is int)
					supplierOrderID = (int)id;
			}

			//int isCustomBrace = GetBrace == true ? 1 : 0;

            DataSet dsProduct = DAL.PracticeLocation.Inventory.GetLineItemInfoFromOrder(supplierOrderID);
            //DataTable dtProduct = dsProduct.Tables[0];

            //DataRow[] drProductRows = dtProduct.Select(string.Format("IsCustomBrace={0}", isCustomBrace));

            //DataTable dtProductsInCart = null;
            //if (drProductRows.GetUpperBound(0) > -1)
            //{
            //    dtProductsInCart = drProductRows.CopyToDataTable();
            //}
			return dsProduct;// dtProductsInCart;
        }

        public static DataSet GetCustomBraceExtendedInfoFromOrder(int ShoppingCartID)
        {
            return GetCustomBraceExtendedInfoFromOrder(ShoppingCartID, null);
        }

        public static DataSet GetCustomBraceExtendedInfoFromOrder(int? ShoppingCartID, int? BregVisionOrderID)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetCustomBraceOrder");

            if (ShoppingCartID != null)
            {
                db.AddInParameter(dbCommand, "ShoppingCartID", DbType.Int32, ShoppingCartID);
            }
            if (BregVisionOrderID != null)
            {
                db.AddInParameter(dbCommand, "BregVisionOrderID", DbType.Int32, BregVisionOrderID);
            }

            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public static bool AddBilatProductToCart(int practiceLocationID)
        {

            //Added  a second item to the cart.
            DataTable dtCustomBraceInCart = GetCustomBraceFromShoppingCart(practiceLocationID, true);
            if (dtCustomBraceInCart.Rows.Count == 1)
            {
                //add a second brace to the cart.
                int practiceCatalogProductID = Convert.ToInt32(dtCustomBraceInCart.Rows[0]["PracticeCatalogProductID"]);
                UpdateInventoryData(Convert.ToInt16(practiceLocationID), practiceCatalogProductID, 1, 1, true);
                return true;
            }
            return false;
        }

        public static bool CorrectNonBilatProductInCart(int practiceLocationID)
        {
            //remove  the second item from the cart.
            DataTable dtCustomBraceInCart = GetCustomBraceFromShoppingCart(practiceLocationID, true);
            if (dtCustomBraceInCart.Rows.Count == 1)
            {
                //add a second brace to the cart.
                int shoppingCartItemID = Convert.ToInt32(dtCustomBraceInCart.Rows[0]["ShoppingCartItemID"]);
                int practiceCatalogProductID = Convert.ToInt32(dtCustomBraceInCart.Rows[0]["PracticeCatalogProductID"]);
                int quantity = Convert.ToInt32(dtCustomBraceInCart.Rows[0]["Quantity"]);
                if (quantity > 1)
                {
                    VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
                    var shoppingCart = from c in visionDB.ShoppingCartItems
                                       where c.ShoppingCartItemID == shoppingCartItemID
                                       select c;
                    ShoppingCartItem cartItem = shoppingCart.Single<ShoppingCartItem>();
                    cartItem.Quantity = 1;
                    visionDB.SubmitChanges();

                    return true;
                }
            }
            return false;
        }

        public static DataSet GetShoppingCart(int practiceLocationID)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetShoppingCart");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        /// <summary>
        /// Gets custom brace records from the shopping cart
        /// </summary>
        /// <param name="practiceLocationID"></param>
        /// <param name="GetBrace">send true if you want the brace info, false if you want the accessories</param>
        /// <returns></returns>
        public static DataTable GetCustomBraceFromShoppingCart(int practiceLocationID, bool GetBrace)
        {
            var dsProduct = DAL.PracticeLocation.Inventory.GetShoppingCart(practiceLocationID);
            var dtProduct = dsProduct.Tables[0];
            var drProductRows = dtProduct.Select(string.Format("IsCustomBrace={0}", (GetBrace ? 1 : 0)));

            return
                (drProductRows.GetUpperBound(0) > -1)
                    ? drProductRows.CopyToDataTable()
                    : (DataTable)null;
        }

        public static DataSet GetMappedPDFInfo(
			bool? isNewOrdersVersion = null, int? bregVisionOrderID = null,
			int? mappedPDFID = null, string mappedPDFName = null
			)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetMappedPDFInfo");

			if (isNewOrdersVersion != null)
				db.AddInParameter(dbCommand, "IsNewOrdersVersion", DbType.Boolean, isNewOrdersVersion.Value);

			if (bregVisionOrderID != null)
				db.AddInParameter(dbCommand, "BregVisionOrderID", DbType.Int32, bregVisionOrderID.Value);

			if (mappedPDFID != null)
				db.AddInParameter(dbCommand, "MappedPDFID", DbType.Int32, mappedPDFID.Value);

			if (mappedPDFName != null)
				db.AddInParameter(dbCommand, "MappedPDFName", DbType.String, mappedPDFName);

			DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        #endregion

        #region "Transfer to Product Inventory"

        public static Int32 TransferToProductInventory(Int32 PracticeLocationID, Int32 PracticeCatalogProductID, Int32 UserID)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_TransferFromPracticeCatalogtoProductInventory");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);
            db.AddInParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, PracticeCatalogProductID);
            db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);

            Int32 SQLReturn = db.ExecuteNonQuery(dbCommand);

            return SQLReturn;

        }

        #endregion

        #region "Update Inventory for current product"
        public static Int32 UpdateInventoryData(Int32 practiceLocationID, Int32 practiceCatalogProductID, Int32 UserID, Int32 Quantity)
        {
            return UpdateInventoryData(practiceLocationID, practiceCatalogProductID, UserID, Quantity, false);
        }


        /// <summary>
        /// Updates the inventory(Shopping Cart) for an item that is already in the catalog and in inventory
        /// (This adds an item to the Shopping Cart)
        /// </summary>
        /// <param name="practiceLocationID"></param>
        /// <param name="practiceCatalogProductID"></param>
        /// <param name="UserID"></param>
        /// <param name="Quantity"></param>
        /// <returns>ShoppingCartIDReturn</returns>
        public static Int32 UpdateInventoryData(Int32 practiceLocationID, Int32 practiceCatalogProductID, Int32 UserID, Int32 Quantity, bool Bilat)
        {
            int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_Inventory_Update_Insert");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            db.AddInParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, practiceCatalogProductID);
            db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
            db.AddInParameter(dbCommand, "Quantity", DbType.Int32, Quantity);
            if (Bilat == true)
            {
                db.AddInParameter(dbCommand, "Bilat", DbType.Boolean, Bilat);
            }
            db.AddOutParameter(dbCommand, "ShoppingCartIDReturn", DbType.Int32, rowsAffected);

            int SQLReturn = db.ExecuteNonQuery(dbCommand);
            
            Int32 return_value = Convert.ToInt32(db.GetParameterValue(dbCommand, "ShoppingCartIDReturn"));
            return return_value;
        }

        #endregion

        #region "Update Inventory(Add to Shopping Cart) for a product not in the catalog or not in inventory"

        /// <summary>
        /// Adds an item to inventory(Shopping Cart) even if it didn't previously exist in the practice catalog or the inventory
        /// (This adds an item to the Shopping Cart)
        /// </summary>
        /// <param name="PracticeID"></param>
        /// <param name="PracticeLocationID"></param>
        /// <param name="MasterCatalogProductID"></param>
        /// <param name="ProductInventoryID"></param>
        /// <param name="PracticeCatalogProductID"></param>
        /// <param name="UserID"></param>
        /// <param name="Quantity"></param>
        /// <returns>ShoppingCartItemID</returns>
        public static Int32 UpdateInventoryData( Int32 PracticeID, Int16 PracticeLocationID, 
                                                                Int32 MasterCatalogProductID, Int32 ProductInventoryID, Int32 PracticeCatalogProductID, 
                                                                Int16 UserID, Int16 Quantity)
        {
            Int32 practiceCatalogProductID = PracticeCatalogProductID;
            Int32 productInventoryID = ProductInventoryID;

            if (PracticeCatalogProductID == 0)     //the item hasn't been added to the practice Catalog yet
            { 
                if (MasterCatalogProductID > 0)  //This should always have a value unless there is an error
                {
                    //we got a masterCatalogID so add to the Practice Catalog
                    practiceCatalogProductID = DAL.Practice.Catalog.UpdatePracticeCatalog(PracticeID, MasterCatalogProductID, UserID, false);
                }
            }

            if (productInventoryID == 0)
            {
                //we don't have a productInventoryID so add to product inventory
                TransferToProductInventory(PracticeLocationID, practiceCatalogProductID, UserID);
            }

            if (practiceCatalogProductID > 0)
            {
                return UpdateInventoryData(PracticeLocationID, practiceCatalogProductID, UserID, Quantity);
            }

            return 0;

        }

        #endregion

        #region Delete

        public int Delete(int productInventoryID, int userID, out int removalType, out string dependencyMessage)
        {
            return Delete( productInventoryID,  userID, out removalType, out dependencyMessage, false);
        }

        public int Delete(int productInventoryID, int userID, out int removalType, out string dependencyMessage, bool isAdmin)
        {
            int rowsAffected = 0;
            removalType = -1;
            dependencyMessage = ""; 
            // Note: RemovalType is 0 not removed; 1 deleted; 2 soft delete (inactive)

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            using ( DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_ProductInventory_Delete") )
            {
                db.AddInParameter(dbCommand, "ProductInventoryID", DbType.Int32, productInventoryID);
                db.AddInParameter(dbCommand, "UserID", DbType.Int32, userID);
                db.AddOutParameter(dbCommand, "RemovalType", DbType.Int32, 4);
                db.AddOutParameter(dbCommand, "DependencyMessage", DbType.String, 2000);
                db.AddInParameter(dbCommand, "IsAdmin", DbType.Boolean, isAdmin);

                rowsAffected = db.ExecuteNonQuery(dbCommand);

                removalType = Convert.ToInt32(db.GetParameterValue(dbCommand, "RemovalType"));
                dependencyMessage = db.GetParameterValue(dbCommand, "DependencyMessage").ToString();

                
                //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE")); 



                dbCommand.Dispose();
            }

            return rowsAffected;
        }

        public static void DeleteItemFromShoppingCart(Int32 shoppingCartItemID)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_DeleteItemfromShoppingCartItem");

            db.AddInParameter(dbCommand, "ShoppingCartItemID", DbType.Int32, shoppingCartItemID);

            int SQLReturn = db.ExecuteNonQuery(dbCommand);

        }

        #endregion

        public static DataSet GetInventoryOrderStatus(int practiceID, int practiceLocationID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetInventoryOrderStatus");

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);

            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public static DataSet SearchForInventoryItems(int practiceLocationID, string searchCriteria, string searchText)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_SearchforItemsInInventory");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, searchCriteria);
            db.AddInParameter(dbCommand, "SearchText", DbType.String, searchText);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public static DataSet GetDeadStock(int practiceID, int practiceLocationID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_DeadStock");

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);

            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        #endregion


        /// <summary>
        ///
        /// </summary>
        /// <param name="inventoryItems"></param>
        public static bool IsCustomBraceOrAccessory(int[] inventoryItems)
        {
           VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var numItemsFound = (from pi in visionDB.ProductInventories
                            join pcp in visionDB.PracticeCatalogProducts on pi.PracticeCatalogProductID equals pcp.PracticeCatalogProductID
                            join mcp in visionDB.MasterCatalogProducts on pcp.MasterCatalogProductID equals mcp.MasterCatalogProductID
                    
                            where pcp.IsThirdPartyProduct == false
                            && inventoryItems.Contains(pi.ProductInventoryID)
                            && (
                                    mcp.IsCustomBrace == true
                                    || mcp.isCustomBraceAccessory == true
                              )
                            select pi).Count();

                if (numItemsFound > 0)
                {
                    return true;
                }
                return false;
            }
        }
    }

    public class ProductInventoryDataRow
    {
        public int Priority { get; set; }

        public int SupplierID { get; set; }
        public bool IsShoppingCart { get; set; }
        public string Product { get; set; }
        public string Code { get; set; }
        public string Packaging { get; set; }
        public string LeftRightSide { get; set; }
        public string Size { get; set; }
        public string Gender { get; set; }
        public string Color { get; set; }
        public int ParLevel { get; set; }
        public int QuantityOnHand { get; set; }
        public int QuantityOnOrder { get; set; }
        public int SuggestedReorderLevel { get; set; }
        public bool IsNotFlaggedForReorder { get; set; }
        public int CriticalLevel { get; set; }
        public string PracticeLocationName { get; set; }
        public int practicecatalogproductid { get; set; }
		public int NewPracticeCatalogProductID { get; set; }
		public string NewProductCode { get; set; }
		public string NewProductName { get; set; }
    }    
  }
