using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using BLL;

namespace DAL.PracticeLocation
{
	public class Email
	{
	
		#region Constructors
		
		public Email()
        {
            // Add constructor logic here
        }
		
		#endregion
		
		
		#region Methods
		
		
            //#region SelectAll
    		
            //public DataTable SelectAll( )
            //{
            //    //Insert call to SelectAll proc
            //}
    		
            //#endregion


		    #region SelectOne
    		
		    //public BLL.PracticeLocation.Email SelectOne( int PracticeLocationID )
            public BLL.PracticeLocation.Email SelectOne (BLL.PracticeLocation.Email bllPLE)
		    {
			    //Insert call to SelectOne proc
                //public void CALLPROC(int practiceLocationID, out string emailForOrderApproval, out string emailForOrderConfirmation, out string emailForFaxConfirmation, out string emailForLowInventoryAlerts, out bool isEmailForLowInventoryAlertsOn)
                //{
                    int rowsAffected = 0;
                    
                    Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                    using ( DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_Select_One_Email_ByParams" ) )
			        {
                        db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, bllPLE.PracticeLocationID );
                        db.AddOutParameter(dbCommand, "EmailForOrderApproval", DbType.String, 200);
                        db.AddOutParameter(dbCommand, "EmailForOrderConfirmation", DbType.String, 200);
                        db.AddOutParameter(dbCommand, "EmailForFaxConfirmation", DbType.String, 200);
                        db.AddOutParameter(dbCommand, "EmailForBillingDispense", DbType.String, 200);
                        db.AddOutParameter(dbCommand, "EmailForLowInventoryAlerts", DbType.String, 200);
                        db.AddOutParameter(dbCommand, "IsEmailForLowInventoryAlertsOn", DbType.Boolean, 1);
                        db.AddOutParameter(dbCommand, "EmailForPrintDispense", DbType.String, 200);
                        db.AddOutParameter(dbCommand, "FaxForPrintDispense", DbType.String, 100); // added by u=

                        rowsAffected = db.ExecuteNonQuery(dbCommand);
                        
                        bllPLE.EmailForOrderApproval =  ( ( db.GetParameterValue( dbCommand, "EmailForOrderApproval" ) == DBNull.Value ) ? "" : Convert.ToString( db.GetParameterValue( dbCommand, "EmailForOrderApproval" ) ) );

                        //emailForOrderConfirmation = Convert.ToString(db.GetParameterValue(dbCommand, "EmailForOrderConfirmation"));
                        bllPLE.EmailForOrderConfirmation =  ( ( db.GetParameterValue( dbCommand, "EmailForOrderConfirmation" ) == DBNull.Value ) ? "" : Convert.ToString( db.GetParameterValue( dbCommand, "EmailForOrderConfirmation" ) ) );

                        //emailForFaxConfirmation = Convert.ToString(db.GetParameterValue(dbCommand, "EmailForFaxConfirmation"));
                        bllPLE.EmailForFaxConfirmation = ((db.GetParameterValue(dbCommand, "EmailForFaxConfirmation") == DBNull.Value) ? "" : Convert.ToString(db.GetParameterValue(dbCommand, "EmailForFaxConfirmation")));

                        //emailForFaxConfirmation = Convert.ToString(db.GetParameterValue(dbCommand, "EmailForFaxConfirmation"));
                        bllPLE.EmailForBillingDispense = ((db.GetParameterValue(dbCommand, "EmailForBillingDispense") == DBNull.Value) ? "" : Convert.ToString(db.GetParameterValue(dbCommand, "EmailForBillingDispense")));


                        bllPLE.EmailForPrintDispense = ((db.GetParameterValue(dbCommand, "EmailForPrintDispense") == DBNull.Value) ? "" : Convert.ToString(db.GetParameterValue(dbCommand, "EmailForPrintDispense")));

                        bllPLE.FaxForPrintDispense = ((db.GetParameterValue(dbCommand, "FaxForPrintDispense") == DBNull.Value) ? "" : Convert.ToString(db.GetParameterValue(dbCommand, "FaxForPrintDispense"))); // added by u= 09/04/2013

                        //emailForLowInventoryAlerts = Convert.ToString(db.GetParameterValue(dbCommand, "EmailForLowInventoryAlerts"));
                        bllPLE.EmailForLowInventoryAlerts =  ( ( db.GetParameterValue( dbCommand, "EmailForLowInventoryAlerts" ) == DBNull.Value ) ? "" : Convert.ToString( db.GetParameterValue( dbCommand, "EmailForLowInventoryAlerts" ) ) );
                        
                        //  isEmailForLowInventoryAlertsOn = Convert.ToBoolean(db.GetParameterValue(dbCommand, "IsEmailForLowInventoryAlertsOn"));
                        bllPLE.IsEmailForLowInventoryAlertsOn =  ( ( db.GetParameterValue( dbCommand, "IsEmailForLowInventoryAlertsOn" ) == DBNull.Value ) ? false : Convert.ToBoolean( db.GetParameterValue( dbCommand, "IsEmailForLowInventoryAlertsOn" ) ) );
                        
                        //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
				        dbCommand.Dispose();
			        }

                    return bllPLE;

                }

    	    #endregion
    		


    		
            //#region Insert
    		
            //public int Insert( BLL.PracticeLocation bllPracticeLocation )
            //{
            //    //Insert call to Insert proc
            //    return -1;
            //}
    		
            //#endregion


		    #region Update   
    		
		    public void Update( BLL.PracticeLocation.Email bllPLE )   
		    {
			    //Insert call to Update proc
                
                 int rowsAffected = 0;
        
                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                using ( DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_Update_Email" ) )
			    {
                   
                    db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, bllPLE.PracticeLocationID);
                    db.AddInParameter(dbCommand, "EmailForOrderApproval", DbType.String, bllPLE.EmailForOrderApproval);
                    db.AddInParameter(dbCommand, "EmailForOrderConfirmation", DbType.String, bllPLE.EmailForOrderConfirmation);
                    db.AddInParameter(dbCommand, "EmailForFaxConfirmation", DbType.String, bllPLE.EmailForFaxConfirmation);
                    db.AddInParameter(dbCommand, "EmailForBillingDispense", DbType.String, bllPLE.EmailForBillingDispense); 
                    db.AddInParameter(dbCommand, "EmailForLowInventoryAlerts", DbType.String, bllPLE.EmailForLowInventoryAlerts);
                    db.AddInParameter(dbCommand, "IsEmailForLowInventoryAlertsOn", DbType.Boolean, bllPLE.IsEmailForLowInventoryAlertsOn);
                    db.AddInParameter(dbCommand, "ModifiedUserID", DbType.Int32, bllPLE.UserID );
                    db.AddInParameter(dbCommand, "EmailForPrintDispense", DbType.String, bllPLE.EmailForPrintDispense);
                    db.AddInParameter(dbCommand, "FaxForPrintDispense", DbType.String, bllPLE.FaxForPrintDispense); // added by u= 09/04/2013
                    
                    rowsAffected = db.ExecuteNonQuery(dbCommand);

                    //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));

                     dbCommand.Dispose();
                }
               }
    		
		        #endregion


                //#region Delete
        		
                //public void Delete( BLL.PracticeLocation bllPracticeLocation )
                //{
                //    //Update call to Delete proc
                //}
        		
                //#endregion
    		
		    #endregion
		
	}
}

