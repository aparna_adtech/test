using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using BLL;
using ClassLibrary.DAL;
using System.Linq;
using ClassLibrary.DAL.Helpers;

namespace DAL.PracticeLocation
{
    public class PracticeLocation
    {

        #region Constructors

        public PracticeLocation()
        {
            // Add constructor logic here
        }

        #endregion


        #region Methods


        #region SelectByUser

        public DataTable SelectAccessibleToUser(int practiceID, string UserName)
        {
            //int practiceID = bllPracticeLocation.PracticeID;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_Select_All_By_PracticeID_AND_UserName"))
            {
                db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
                db.AddInParameter(dbCommand, "UserName", DbType.String, UserName);

                DataSet ds = db.ExecuteDataSet(dbCommand);

                return ds.Tables[0];
            }
        }

        #endregion

        #region SelectAll

        public DataTable SelectAll( int practiceID )
        {
            //int practiceID = bllPracticeLocation.PracticeID;

            Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            using ( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_PracticeLocation_Select_All_By_PracticeID" ) )
            {
                db.AddInParameter( dbCommand, "PracticeID", DbType.Int32, practiceID );

				DataSet ds = db.ExecuteDataSet( dbCommand );

                return ds.Tables[0];
            }
        }

        #endregion

		#region SelectName

		static public string SelectName( int practiceID)
		{
			Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
			using ( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_PracticeLocation_Select_Name_ByParams" ) )
			{
				db.AddInParameter( dbCommand, "PracticeLocationID", DbType.Int32, practiceID );
				db.AddOutParameter( dbCommand, "Name", DbType.String, 50 );

				db.ExecuteNonQuery( dbCommand );

				string practiceName = Convert.ToString( db.GetParameterValue( dbCommand, "Name" ) );

				dbCommand.Dispose();

				return practiceName;
			}
		}

		#endregion

        #region Select User Permissions
        //usp_PracticeLocation_Select_Permissions
        public DataTable SelectUserPermissions(int practiceID, int UserID)
        {
            //int practiceID = bllPracticeLocation.PracticeID;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_Select_Permissions"))
            {
                db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
                db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);

                //DataTable dtPracticeLocation = new DataTable();
                DataSet ds = db.ExecuteDataSet(dbCommand);
                int count = ds.Tables[0].Rows.Count;
                DataTable dtPracticeLocation = new DataTable();  // db.ExecuteDataSet( dbCommand ).Tables[0];
                dtPracticeLocation = ds.Tables[0];

                return dtPracticeLocation;
            }
        }
        #endregion

        #region SetUserPermissions

        public bool SetUserPermissions(int practiceLocationID, int UserID, bool AllowAccess)
        {
            //int practiceID = bllPracticeLocation.PracticeID;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_Set_Permissions"))
            {
                db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
                db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
                db.AddInParameter(dbCommand, "AllowAccess", DbType.Boolean, AllowAccess);

                //DataTable dtPracticeLocation = new DataTable();
                int ret = db.ExecuteNonQuery(dbCommand);

                return true;
            }
        }
        #endregion


        //#region SelectOne

        //public DataTable SelectOne( int PracticeLocationID )
        //{
        //    //Insert call to SelectOne proc
        //}

        //#endregion


        #region Insert

        public int Insert( BLL.PracticeLocation.PracticeLocation bllPracticeLocation )
        {
            int rowsAffected = 0;

            int newPracticeLocationID;
            int practiceID = bllPracticeLocation.PracticeID;
            string name = bllPracticeLocation.Name;
            string generalLedgerNumber = bllPracticeLocation.GeneralLedgerNumber;
            bool isPrimaryLocation = bllPracticeLocation.IsPrimaryLocation;
            int createdUserID = bllPracticeLocation.UserID;
            bool isHCPSignatureRequired = bllPracticeLocation.IsHCPSignatureRequired;
            

            Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            using ( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_PracticeLocation_Insert" ) )
            {
                db.AddOutParameter( dbCommand, "PracticeLocationID", DbType.Int32, 4 );
                db.AddInParameter( dbCommand, "PracticeID", DbType.Int32, practiceID );
                db.AddInParameter( dbCommand, "Name", DbType.String, name );
                db.AddInParameter(dbCommand, "GeneralLedgerNumber", DbType.String, generalLedgerNumber);
                db.AddInParameter( dbCommand, "IsPrimaryLocation", DbType.Boolean, isPrimaryLocation );
                db.AddInParameter( dbCommand, "CreatedUserID", DbType.Int32, createdUserID );
                db.AddInParameter( dbCommand, "IsHCPSignatureRequired", DbType.Boolean, isHCPSignatureRequired);
                if (bllPracticeLocation.SortOrder > 0)
                {
                    db.AddInParameter(dbCommand, "SortOrder", DbType.Int32, bllPracticeLocation.SortOrder);
                }

                rowsAffected = db.ExecuteNonQuery( dbCommand );



                newPracticeLocationID = Convert.ToInt32( db.GetParameterValue( dbCommand, "PracticeLocationID" ) );
                //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));

                return newPracticeLocationID;
            }
        }

        #endregion


        #region Update

        public void Update( BLL.PracticeLocation.PracticeLocation bllPracticeLocation )
        {
            int rowsAffected = 0;

            int practiceLocationID = bllPracticeLocation.PracticeLocationID;
            int practiceID = bllPracticeLocation.PracticeID;
            string name = bllPracticeLocation.Name;
            bool isPrimaryLocation = bllPracticeLocation.IsPrimaryLocation;
            int modifiedUserID = bllPracticeLocation.UserID;
            bool isHCPSignatureRequired = bllPracticeLocation.IsHCPSignatureRequired;

            Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            using ( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_PracticeLocation_Update" ) )
            {
                db.AddInParameter( dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID );
                db.AddInParameter( dbCommand, "PracticeID", DbType.Int32, practiceID );
                db.AddInParameter( dbCommand, "Name", DbType.String, name );
                db.AddInParameter( dbCommand, "IsPrimaryLocation", DbType.Boolean, isPrimaryLocation );
                db.AddInParameter( dbCommand, "ModifiedUserID", DbType.Int32, modifiedUserID );
                db.AddInParameter(dbCommand, "IsHCPSignatureRequired", DbType.Int32, isHCPSignatureRequired);

                if (bllPracticeLocation.SortOrder > 0)
                {
                    db.AddInParameter(dbCommand, "SortOrder", DbType.Int32, bllPracticeLocation.SortOrder);
                }

                rowsAffected = db.ExecuteNonQuery( dbCommand );

                //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
            }
        }

        public static void UpdateIsHCPSignatureRequired(int practiceplocationId, bool isHCPSignatureRequired)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                var query = from p in visionDB.PracticeLocations
                            where p.PracticeLocationID == practiceplocationId
                            select p;

                //ClassLibrary.DAL.PracticeLocation loc = query.Single(ClassLibrary.DAL.PracticeLocation>();
                ClassLibrary.DAL.PracticeLocation loc = query.Single();
                if (loc.IsHcpSignatureRequired != isHCPSignatureRequired)
                {
                    loc.IsHcpSignatureRequired = isHCPSignatureRequired;
                    visionDB.SubmitChanges();
                }
            }
        }

        #endregion


        #region Delete

        public Int32 Delete( BLL.PracticeLocation.PracticeLocation bllPracticeLocation )
        {
            int rowsAffected = 0;

            int practiceLocationID = bllPracticeLocation.PracticeLocationID;

            Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            using ( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_PracticeLocation_Delete" ) )
            {
                db.AddInParameter( dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID );

                rowsAffected = db.ExecuteNonQuery( dbCommand );

                return rowsAffected;
            }
        }

        #endregion

        public static DataRow GetSetupPageStatus(string currentPage, int practiceID, int practiceLocationID)
        {
                        DataTable statusTable = new DataTable();

            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                var practiceLocations = from pl in visionDB.PracticeLocations
                                        where pl.PracticeID == practiceID
                                        && pl.PracticeLocationID == practiceLocationID
                                        select new
                                        {
                                            pl.PracticeLocationID,
                                            pl.Name
                                        };

                var setupStatusTitles = from st in visionDB.SetupStatusTitles
                                        where st.IsPracticeLocation == true
                                        && st.UserControlName == currentPage
                                        select st;

                var practiceLocationQuery = from st in visionDB.SetupStatusTitles
                                            join ss in visionDB.SetupStatus on st.UserControlName equals ss.UserControlName into s
                                            from ss in s.DefaultIfEmpty()
                                            where ss.PracticeID == practiceID
                                            && ss.PracticeLocationID == practiceLocationID
                                            && st.IsPracticeLocation == true
                                            && st.UserControlName == currentPage
                                            orderby st.DisplayOrder
                                            select new
                                            {
                                                ss.PracticeLocationID,
                                                ss.UserControlName,
                                                st.PageTitle,
                                                PageStatus = ((ss == null) ? 0 : ss.PageStatus) == 0 ? "N" : "Y"
                                            };



                var location = practiceLocations.FirstOrDefault();
                if (location != null)
                {
                    var pageStatus = setupStatusTitles.FirstOrDefault();
                    if (pageStatus != null)
                    {
                        DataTableHelper.CreateColumn("Page", "Page", "System.String", statusTable);
                        DataTableHelper.CreateColumn("PageStatus", "PageStatus", "System.String", statusTable);

                        DataRow row = statusTable.NewRow();
                        row["Page"] = pageStatus.PageTitle;

                        var practiceLocationPageStatus = from plps in practiceLocationQuery
                                                         where plps.PracticeLocationID == location.PracticeLocationID
                                                         && plps.UserControlName == pageStatus.UserControlName
                                                         select plps;
                        var statusItem = practiceLocationPageStatus.FirstOrDefault();
                        string statusValue = (statusItem != null) ? statusItem.PageStatus : "N";

                        row["PageStatus"] = statusValue;
                        //row[location.Name] = statusValue;

                        return row;
                    }
                }
            }
            return null;
        }

        public static DataTable GetPracticeLocationSetupStatus(int practiceID)
        {
            DataTable statusTable = new DataTable();

            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                var practiceLocations = from pl in visionDB.PracticeLocations
                                        where pl.PracticeID == practiceID
                                        && pl.IsActive == true
                                        select new
                                        {
                                            pl.PracticeLocationID,
                                            pl.Name
                                        };

                var setupStatusTitles = from st in visionDB.SetupStatusTitles
                                        where st.IsPracticeLocation == true
                                        orderby st.DisplayOrder
                                        select st;


                var practiceLocationQuery = from st in visionDB.SetupStatusTitles
                                            join ss in visionDB.SetupStatus on st.UserControlName equals ss.UserControlName into s
                                            from ss in s.DefaultIfEmpty()
                                            where ss.PracticeID == practiceID
                                            && st.IsPracticeLocation == true
                                            orderby st.DisplayOrder
                                            select new
                                            {
                                                ss.PracticeLocationID,
                                                ss.UserControlName,
                                                st.PageTitle,
                                                PageStatus = ((ss == null) ? 0 : ss.PageStatus) == 0 ? "N" : "Y"
                                            };



                // add a column for the page                   
                DataTableHelper.CreateColumn("Page", "Page", "System.String", statusTable);

                foreach (var location in practiceLocations)
                {
                    DataTableHelper.CreateColumn(location.PracticeLocationID.ToString(), location.Name, "System.String", statusTable);
                    //ClassLibrary.DAL.Helpers.DataTableHelper.CreateColumn("PracticeLocationID", "System.Int32", statusTable);
                }

                foreach (var pageStatus in setupStatusTitles)
                {
                    DataRow row = statusTable.NewRow();
                    row["Page"] = pageStatus.PageTitle;
                    foreach (var location in practiceLocations)
                    {
                        var practiceLocationPageStatus = from plps in practiceLocationQuery
                                                         where plps.PracticeLocationID == location.PracticeLocationID
                                                         && plps.UserControlName == pageStatus.UserControlName
                                                         select plps;
                        var statusItem = practiceLocationPageStatus.FirstOrDefault();
                        string statusValue = (statusItem != null) ? statusItem.PageStatus : "N";

                        row[location.PracticeLocationID.ToString()] = statusValue;
                        

                    }
                    statusTable.Rows.Add(row);
                }

            }
            return statusTable;
        }

        public static void PopulateProductInventory(int practiceId, int practiceLocationId, int userId)
        {
            int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");

            using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_ProductInventory_PopulateWithAllBregProducts"))
            {
                db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceId);
                db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationId);
                db.AddInParameter(dbCommand, "UserID", DbType.Int32, userId);

                rowsAffected = db.ExecuteNonQuery(dbCommand);
                dbCommand.Dispose();
            }
        }

        #endregion

        public static ClassLibrary.DAL.Contact GetContact(int practiceLocationId)
        {
            using (var db = VisionDataContext.GetVisionDataContext())
            {
                return db.PracticeLocations.Single(pl => pl.PracticeLocationID == practiceLocationId).Contact;
            }
        }

        public static bool IsHCPSignatureRequiredForLocation(int practiceLocationId)
        {
            using (var db = VisionDataContext.GetVisionDataContext())
            {
                return db.PracticeLocations.Single(pl => pl.PracticeLocationID == practiceLocationId).IsHcpSignatureRequired;
            }
        }
    }
}

