using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using BLL;
using BLL.PracticeLocation;

namespace DAL.PracticeLocation
{
    /// <summary>
    /// Summary description for PracticeLocation_Contact
    /// </summary>
    public class Contact
    {
        public Contact()
        {
            //

            //
        }
        public BLL.PracticeLocation.Contact Select( int practiceLocationID )
        {
            BLL.PracticeLocation.Contact bllPLContact = new BLL.PracticeLocation.Contact();

            //int practiceLocationID = bllPLContact.PracticeLocationID;
			//int contactID;
			//string title;
			//string salutation;
			//string firstName;
			//string middleName;
			//string lastName;
			//string suffix;
			//string emailAddress;
			//string phoneWork;
			//string phoneCell;
			//string phoneHome;
			//string fax;

            int rowsAffected = -1;

            Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            using ( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_PracticeLocation_Select_Contact_ByParams" ) )
            {

                db.AddInParameter( dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID );
                db.AddOutParameter( dbCommand, "ContactID", DbType.Int32, 4 );
                db.AddOutParameter( dbCommand, "Title", DbType.String, 50 );
                db.AddOutParameter( dbCommand, "Salutation", DbType.String, 10 );
                db.AddOutParameter( dbCommand, "FirstName", DbType.String, 50 );
                db.AddOutParameter( dbCommand, "MiddleName", DbType.String, 50 );
                db.AddOutParameter( dbCommand, "LastName", DbType.String, 50 );
                db.AddOutParameter( dbCommand, "Suffix", DbType.String, 10 );
                db.AddOutParameter( dbCommand, "EmailAddress", DbType.String, 100 );
                db.AddOutParameter( dbCommand, "PhoneWork", DbType.String, 25 );
                db.AddOutParameter( dbCommand, "PhoneWorkExtension", DbType.String, 25);
                db.AddOutParameter( dbCommand, "PhoneCell", DbType.String, 25 );
                db.AddOutParameter( dbCommand, "PhoneHome", DbType.String, 25 );
                db.AddOutParameter( dbCommand, "Fax", DbType.String, 25 );
                db.AddOutParameter(dbCommand, "IsSpecificContact", DbType.Boolean, 1);

                //DataTable dtPracticeLocation = new DataTable();
                rowsAffected = db.ExecuteNonQuery( dbCommand );

                bllPLContact.PracticeLocationID = practiceLocationID;

                //ordinal = dr.GetOrdinal( ADDRESSID );
                //p.AddressID = ( dr.IsDBNull( ordinal ) ) ? -1 : dr.GetInt32( ordinal );
              
                bllPLContact.ContactID = ( (db.GetParameterValue( dbCommand, "ContactID" ) == DBNull.Value ) ? -1 : Convert.ToInt32( db.GetParameterValue( dbCommand, "ContactID" ) ) );
                
                //  bllPLContact.ContactID  = Convert.ToInt32( db.GetParameterValue( dbCommand, "ContactID" ) );

                bllPLContact.Title = Convert.ToString( db.GetParameterValue( dbCommand, "Title" ) );
                bllPLContact.Salutation = Convert.ToString( db.GetParameterValue( dbCommand, "Salutation" ) );
                bllPLContact.FirstName = Convert.ToString( db.GetParameterValue( dbCommand, "FirstName" ) );
                bllPLContact.MiddleName = Convert.ToString( db.GetParameterValue( dbCommand, "MiddleName" ) );
                bllPLContact.LastName = Convert.ToString( db.GetParameterValue( dbCommand, "LastName" ) );
                bllPLContact.Suffix = Convert.ToString( db.GetParameterValue( dbCommand, "Suffix" ) );
                bllPLContact.EmailAddress = Convert.ToString( db.GetParameterValue( dbCommand, "EmailAddress" ) );
                bllPLContact.PhoneWork = Convert.ToString( db.GetParameterValue( dbCommand, "PhoneWork" ) );
                bllPLContact.PhoneWorkExtension = Convert.ToString(db.GetParameterValue(dbCommand, "PhoneWorkExtension"));
                bllPLContact.PhoneCell = Convert.ToString( db.GetParameterValue( dbCommand, "PhoneCell" ) );
                bllPLContact.PhoneHome = Convert.ToString( db.GetParameterValue( dbCommand, "PhoneHome" ) );
                bllPLContact.Fax = Convert.ToString( db.GetParameterValue( dbCommand, "Fax" ) );
                var isSpecificContactValue = db.GetParameterValue(dbCommand, "IsSpecificContact");
                bllPLContact.IsSpecificContact = !(isSpecificContactValue is DBNull) && Convert.ToBoolean(isSpecificContactValue);

                //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));

                dbCommand.Dispose();

                return bllPLContact;
            }
        }


        public int Insert( BLL.PracticeLocation.Contact  bllPLContact )
        {
            int rowsAffected = 0;

            int practiceLocationID = bllPLContact.PracticeLocationID;
            int newContactID;
            string title = bllPLContact.Title;
            string salutation = bllPLContact.Salutation;
            string firstName = bllPLContact.FirstName;
            string middleName = bllPLContact.MiddleName;
            string lastName = bllPLContact.LastName;
            string suffix = bllPLContact.Suffix;
            string emailAddress = bllPLContact.EmailAddress;
            string phoneWork = bllPLContact.PhoneWork;
            string phoneWorkExtension = bllPLContact.PhoneWorkExtension;
            string phoneCell = bllPLContact.PhoneCell;
            string phoneHome = bllPLContact.PhoneHome;
            string fax = bllPLContact.Fax;
            int createdUserID = bllPLContact.UserID;

            Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            using ( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_PracticeLocation_Insert_Contact" ) )
            {
                db.AddInParameter( dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID );
                db.AddOutParameter( dbCommand, "ContactID", DbType.Int32, 4 );
                db.AddInParameter( dbCommand, "Title", DbType.String, title );
                db.AddInParameter( dbCommand, "Salutation", DbType.String, salutation );
                db.AddInParameter( dbCommand, "FirstName", DbType.String, firstName );
                db.AddInParameter( dbCommand, "MiddleName", DbType.String, middleName );
                db.AddInParameter( dbCommand, "LastName", DbType.String, lastName );
                db.AddInParameter( dbCommand, "Suffix", DbType.String, suffix );
                db.AddInParameter( dbCommand, "EmailAddress", DbType.String, emailAddress );
                db.AddInParameter( dbCommand, "PhoneWork", DbType.String, phoneWork );
                db.AddInParameter(dbCommand, "PhoneWorkExtension", DbType.String, phoneWorkExtension);
                db.AddInParameter( dbCommand, "PhoneCell", DbType.String, phoneCell );
                db.AddInParameter( dbCommand, "PhoneHome", DbType.String, phoneHome );
                db.AddInParameter( dbCommand, "Fax", DbType.String, fax );
                db.AddInParameter(dbCommand, "IsSpecificContact", DbType.Boolean, bllPLContact.IsSpecificContact);
                db.AddInParameter(dbCommand, "CreatedUserID", DbType.Int32, createdUserID);

                rowsAffected = db.ExecuteNonQuery( dbCommand );

                newContactID = Convert.ToInt32( db.GetParameterValue( dbCommand, "ContactID" ) );
                //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));

                return newContactID;
            }
        }


        public void Update( BLL.PracticeLocation.Contact bllPLContact )
        {
            int rowsAffected = 0;

            int contactID = bllPLContact.ContactID;
            string title = bllPLContact.Title;
            string salutation = bllPLContact.Salutation;
            string firstName = bllPLContact.FirstName;
            string middleName = bllPLContact.MiddleName;
            string lastName = bllPLContact.LastName;
            string suffix = bllPLContact.Suffix;
            string emailAddress = bllPLContact.EmailAddress;
            string phoneWork = bllPLContact.PhoneWork;
            string phoneWorkExtension = bllPLContact.PhoneWorkExtension;
            string phoneCell = bllPLContact.PhoneCell;
            string phoneHome = bllPLContact.PhoneHome;
            string fax = bllPLContact.Fax;
            int modifiedUserID = bllPLContact.UserID;

            Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
            using ( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_Contact_Update" ) )
            {
                db.AddInParameter( dbCommand, "ContactID", DbType.Int32, contactID );
                db.AddInParameter( dbCommand, "Title", DbType.String, title );
                db.AddInParameter( dbCommand, "Salutation", DbType.String, salutation );
                db.AddInParameter( dbCommand, "FirstName", DbType.String, firstName );
                db.AddInParameter( dbCommand, "MiddleName", DbType.String, middleName );
                db.AddInParameter( dbCommand, "LastName", DbType.String, lastName );
                db.AddInParameter( dbCommand, "Suffix", DbType.String, suffix );
                db.AddInParameter( dbCommand, "EmailAddress", DbType.String, emailAddress );
                db.AddInParameter( dbCommand, "PhoneWork", DbType.String, phoneWork );
                db.AddInParameter( dbCommand, "PhoneWorkExtension", DbType.String, phoneWorkExtension );
                db.AddInParameter( dbCommand, "PhoneCell", DbType.String, phoneCell );
                db.AddInParameter( dbCommand, "PhoneHome", DbType.String, phoneHome );
                db.AddInParameter( dbCommand, "Fax", DbType.String, fax );
                db.AddInParameter(dbCommand, "IsSpecificContact", DbType.Boolean, bllPLContact.IsSpecificContact);
                db.AddInParameter( dbCommand, "ModifiedUserID", DbType.Int32, modifiedUserID );

                rowsAffected = db.ExecuteNonQuery( dbCommand );

                //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
            }
        }
    }
}