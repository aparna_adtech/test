using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using BLL;

namespace DAL.PracticeLocation
{

	/// <summary>
	/// Summary description for PracticeLocation_Physician
	/// </summary>
	public class Physician
	{
		public Physician()
		{
			//
			// Add constructor logic here
			//
		}


        public static DataSet GetPhysicians(int practiceLocationID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_Clinician_Select_All_Names_By_PracticeLocationID");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            DataSet ds = db.ExecuteDataSet(dbCommand);

            return ds;
        }

        public static DataSet PopulatePhysicians(Int32 practiceLocationID)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_Clinician_Select_All_By_PracticeLocationID");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            DataSet ds = db.ExecuteDataSet(dbCommand);

            return ds;


        }


		#region SelectAllNames

		public DataTable SelectAllNames( int practiceLocationID )
		{
			Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
			using ( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_PracticeLocation_Clinician_Select_All_Names_By_PracticeLocationID" ) )
			{
				db.AddInParameter( dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID );

				DataSet ds = new DataSet();
				DataTable dt = new DataTable();
				ds = db.ExecuteDataSet( dbCommand );
				dt = ds.Tables[0];
				int count = dt.Rows.Count;
				dbCommand.Dispose();
				return dt;
			}
		}

		#endregion

		#region SelectAllNonLocationNames

		public DataTable SelectAllNonLocationNames( int practiceLocationID )
		{
			Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
			using ( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_PracticeLocation_Clinician_Select_All_NonLocation_Names_By_PracticeLocationID" ) )
			{
				db.AddInParameter( dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID );

				DataSet ds = new DataSet();
				DataTable dt = new DataTable();
				ds = db.ExecuteDataSet( dbCommand );
				dt = ds.Tables[0];
				int count = dt.Rows.Count;
				dbCommand.Dispose();
				return dt;
			}
		}

		#endregion

		//usp_PracticeLocation_Physician_Select_All_NonLocation_Names_By_PracticeLocationID

		//#region InsertArray

		////public int Insert( BLL.Practice.Practice bllP )
		////
		////public int Insert( string practiceName)
		//public void Insert( int practiceLocationID, string arrayPhysicianID, int userID )
		//{
		//    int rowsAffected = 0;
		//    //int practiceID = -1;

		//    //  int practiceID;
		//    //  string practiceName = bllP.PracticeName;
		//    //  int userID = bllP.UserID;

		//    Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
		//    using ( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_PracticeLocation_Physician_Insert_Array" ) )
		//    {
		//        db.AddInParameter( dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID );
		//        db.AddInParameter( dbCommand, "ArrayPhysicianID", DbType.String, arrayPhysicianID );
		//        db.AddInParameter( dbCommand, "CreatedUserID", DbType.Int32, userID );

		//        rowsAffected = db.ExecuteNonQuery( dbCommand );

		//        //practiceID = Convert.ToInt32( db.GetParameterValue( dbCommand, "PracticeID" ) );
		//        //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE")); 

		//        dbCommand.Dispose();
		//    }
		//    //return practiceID;
		//}

		//#endregion



		#region Insert

		public int Insert( BLL.PracticeLocation.Physician bllPLP )
		//public void Insert( int practiceLocationID, int physicianID, int userID )
		{
			int rowsAffected = 0;

			int practiceLocationID = bllPLP.PracticeLocationID;
			int physicianID = bllPLP.PhysicianID;
			int userID = bllPLP.UserID;

			Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
			using ( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_PracticeLocation_Physician_Insert" ) )
			{
				db.AddInParameter( dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID );
				db.AddInParameter( dbCommand, "PhysicianID", DbType.Int32, physicianID );
				db.AddInParameter( dbCommand, "CreatedUserID", DbType.Int32, userID );

				rowsAffected = db.ExecuteNonQuery( dbCommand );

				dbCommand.Dispose();
			}

			return rowsAffected;
		}

		#endregion


		#region Delete

		public int Delete( BLL.PracticeLocation.Physician bllPLP )
		{
			int rowsAffected = 0;

			int practiceLocationID = bllPLP.PracticeLocationID;
			int physicianID = bllPLP.PhysicianID;
			int userID = bllPLP.UserID;

			Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
			using ( DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_PracticeLocation_Physician_Delete" ) )
			{
				db.AddInParameter( dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID );
				db.AddInParameter( dbCommand, "PhysicianID", DbType.Int32, physicianID );
				db.AddInParameter( dbCommand, "ModifiedUserID", DbType.Int32, userID );

				rowsAffected = db.ExecuteNonQuery( dbCommand );

				//practiceID = Convert.ToInt32( db.GetParameterValue( dbCommand, "PracticeID" ) );
				//int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE")); 

				dbCommand.Dispose();
			}

			return rowsAffected;
		}

		#endregion

	}

}
