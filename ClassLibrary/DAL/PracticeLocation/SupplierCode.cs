using System;
using System.Collections;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using BLL;

namespace DAL.PracticeLocation
{
    public class SupplierCode
	{
	
		#region Constructors

        public SupplierCode()
        {
            // Add constructor logic here
        }
		
		#endregion
		
		
		#region Methods

            #region SelectAll

            public DataTable SelectAll( int practiceLocationID )
            {
                //int practiceLocationID = bllPracticeLocation_Supplier_AccountCode.PracticeLocationID;

                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_Supplier_AccountCode_Select_All_By_PracticeLocationID"))
                {
                    db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);

                    DataTable dtPracticeLocation_Supplier_AccountCode = new DataTable();
                    dtPracticeLocation_Supplier_AccountCode = db.ExecuteDataSet(dbCommand).Tables[0];
                    dbCommand.Dispose();

                    //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));

                    return dtPracticeLocation_Supplier_AccountCode;
                }
            }

            #endregion


            #region Update

            public void Update(int practiceLocationID, int practiceCatalogSupplierBrandID, string accountCode, int modifiedUserID)
            {
                int rowsAffected = 0;

                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_Supplier_AccountCode_Update");

                db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
                db.AddInParameter(dbCommand, "PracticeCatalogSupplierBrandID", DbType.Int32, practiceCatalogSupplierBrandID);
                db.AddInParameter(dbCommand, "AccountCode", DbType.String, accountCode);
                db.AddInParameter(dbCommand, "ModifiedUserID", DbType.Int32, modifiedUserID);

                rowsAffected = db.ExecuteNonQuery(dbCommand);

                //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
            }


            #endregion


            #region Insert

            public void Insert(int practiceLocationID, int practiceCatalogSupplierBrandID, string accountCode, int createdUserID)
                {
                    int rowsAffected = 0;

                    Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                    DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_Supplier_AccountCode_Insert");

                    db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
                    db.AddInParameter(dbCommand, "PracticeCatalogSupplierBrandID", DbType.Int32, practiceCatalogSupplierBrandID);
                    db.AddInParameter(dbCommand, "AccountCode", DbType.String, accountCode);
                    db.AddInParameter(dbCommand, "CreatedUserID", DbType.Int32, createdUserID);

                    rowsAffected = db.ExecuteNonQuery(dbCommand);

                    //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
                }

            #endregion

                #region Delete

                public void Delete(int practiceLocationID, int practiceCatalogSupplierBrandID, int modifiedUserID)
                {
                    int rowsAffected = 0;

                    Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                    DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_Supplier_AccountCode_Delete");

                    db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
                    db.AddInParameter(dbCommand, "PracticeCatalogSupplierBrandID", DbType.Int32, practiceCatalogSupplierBrandID);
                    db.AddInParameter(dbCommand, "ModifiedUserID", DbType.Int32, modifiedUserID);

                    rowsAffected = db.ExecuteNonQuery(dbCommand);

                    //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
                }

                #endregion


        #endregion
            }
}
