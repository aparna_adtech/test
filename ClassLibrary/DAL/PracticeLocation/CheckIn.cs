﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClassLibrary.DAL;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;


namespace DAL.PracticeLocation
{
    public class CheckIn
    {
        public static DataSet GetOustandingCustomBracesforCheckIn_ParentLevel(int practiceLocationID)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetOustandingCustomBraceOrdersforCheckIn_ParentLevel");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);

            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        #region OutstandingCustomBracesforCheckIn Dataset
        public static DataSet GetOustandingCustomBracesforCheckIn_Detail1Level(int PracticeLocationID)
        {
            

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetOustandingCustomBraceOrdersforCheckIn_Detail1Level");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);

            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public static DataSet GetOustandingCustomBracesforCheckIn_Detail2Level(Int32 PracticeLocationID)
        {
            

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetOustandingCustomBraceOrdersforCheckIn_Detail2Level");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);

            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public static DataSet GetOustandingPOsforCheckIn_ParentLevel(int practiceLocationID)
        {
            

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetOustandingPOsforCheckIn_ParentLevel");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);

            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        #endregion


        #region OutStandingPOsforCheckIn Dataset
        public static DataSet GetOutstandingPOsforCheckIn_Detail1Level(int bregVisionOrderID)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetOutstandingPOsforCheckIn_Detail1Level");

            db.AddInParameter(dbCommand, "BregVisionOrderID", DbType.Int32, bregVisionOrderID);

            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public static DataSet GetOutstandingPOsforCheckIn_Detail2Level(Int32 SupplierOrderID, Int32 PracticeLocationID)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetOutstandingPOsforCheckIn_Detail2Level");

            db.AddInParameter(dbCommand, "SupplierOrderID", DbType.Int32, SupplierOrderID);
            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);

            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        #endregion

        public static DataSet SearchForCodeOrSupplierManualCheckIn(Int32 practiceLocationID, string searchCriteria, string searchText)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_SearchForCodeOrSupplierManualCheckIn");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, searchCriteria);
            db.AddInParameter(dbCommand, "SearchText", DbType.String, searchText);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public static void SaveInvoicePaymentInfo(int supplierOrderID, bool invoicePaid, string invoiceNumber)
        {
            VisionDataContext visionDB = null;
            visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var supplierOrderQuery = from so in visionDB.SupplierOrders
                                    where so.SupplierOrderID == supplierOrderID
                                    select so;

                ClassLibrary.DAL.SupplierOrder supplierOrder = supplierOrderQuery.FirstOrDefault<ClassLibrary.DAL.SupplierOrder>();
                supplierOrder.InvoicePaid = invoicePaid;
                supplierOrder.InvoiceNumber = invoiceNumber;
                visionDB.SubmitChanges();
            }

        }

        public static int POCheckIn_InsertUpdate(Int32 UserID, Int32 PracticeCatalogProductID
                    , Int32 ProductInventoryID, Int32 BregVisionOrderID, Int32 SupplierOrderID,
                    Int32 SupplierOrderLineItemID, decimal ActualWholesaleCost,Int32 Quantity)
        {
            int rowsAffectedPOCheckIn = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_POCheckIn");

            db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
            db.AddInParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, PracticeCatalogProductID);
            db.AddInParameter(dbCommand, "ProductInventoryID", DbType.Int32, ProductInventoryID);
            db.AddInParameter(dbCommand, "BregVisionOrderID", DbType.Int32, BregVisionOrderID);
            db.AddInParameter(dbCommand, "SupplierOrderID", DbType.Int32, SupplierOrderID);
            db.AddInParameter(dbCommand, "SupplierOrderLineItemID", DbType.Int32, SupplierOrderLineItemID);
            db.AddInParameter(dbCommand, "ActualWholesaleCost", DbType.Decimal, ActualWholesaleCost);
            db.AddInParameter(dbCommand, "Quantity", DbType.Int32, Quantity);

            rowsAffectedPOCheckIn = db.ExecuteNonQuery(dbCommand);
            return rowsAffectedPOCheckIn;
        }

        public static int ManualCheckIn_InsertUpdate(Int32 UserID, Int32 PracticeLocationID, Int32 PracticeCatalogProductID
                            ,Int32 ProductInventoryID, decimal ActualWholesaleCost,
                            Int32 Quantity,string Comments)
        {
            int rowsAffectedManualCheckIn = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_ManualCheckIn_InsertUpdate");

            db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);
            db.AddInParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, PracticeCatalogProductID);
            db.AddInParameter(dbCommand, "ProductInventoryID", DbType.Int32, ProductInventoryID);
            db.AddInParameter(dbCommand, "ActualWholesaleCost", DbType.Decimal, ActualWholesaleCost);
            db.AddInParameter(dbCommand, "Quantity", DbType.Int32, Quantity);
            db.AddInParameter(dbCommand, "Comments", DbType.String, Comments);

            rowsAffectedManualCheckIn = db.ExecuteNonQuery(dbCommand);
            return rowsAffectedManualCheckIn;
        }

    }
}
