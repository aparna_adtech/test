using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using BLL;
/*  Make sure the above is at the top of the page. */


namespace DAL.PracticeLocation
{

	/// <summary>
	/// Summary description for PracticeLocation_BillingAddress
	/// </summary>
	public class BillingAddress
	{
		public BillingAddress()
		{
			//
			//
			//
		}

        public DataSet Select(int PracticeLocationID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.USP_GetBillingAddressByPracticeLocation");

            db.AddInParameter(dbCommand, "practiceLocationID", DbType.String, PracticeLocationID);

            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
            
        }


		public void Select(int practiceLocationID, out string attentionOf, out int addressID, out string addressLine1, out string addressLine2
							, out string city, out string state, out string zipCode, out string zipCodePlus4)
		{
			int rowsAffected = 0;
		    
			Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
			DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocationBillingAddress_Select_One_ByPracticeLocationID");

			db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
			db.AddOutParameter(dbCommand, "AttentionOf", DbType.String, 50);
			db.AddOutParameter(dbCommand, "AddressID", DbType.Int32, 4);
			db.AddOutParameter(dbCommand, "AddressLine1", DbType.String, 50);
			db.AddOutParameter(dbCommand, "AddressLine2", DbType.String, 50);
			db.AddOutParameter(dbCommand, "City", DbType.String, 50);
			db.AddOutParameter(dbCommand, "State", DbType.String, 2);
			db.AddOutParameter(dbCommand, "ZipCode", DbType.String, 5);
			db.AddOutParameter(dbCommand, "ZipCodePlus4", DbType.String, 4);
		    
			rowsAffected = db.ExecuteNonQuery(dbCommand);
		    
			attentionOf =  ( ( db.GetParameterValue( dbCommand, "AttentionOf" ) == DBNull.Value ) ? "" : Convert.ToString( db.GetParameterValue( dbCommand, "AttentionOf" ) ) );
			addressID = ( ( db.GetParameterValue( dbCommand, "AddressID" ) == DBNull.Value ) ? -1 : Convert.ToInt32( db.GetParameterValue( dbCommand, "AddressID" ) ) );
			addressLine1 = ( ( db.GetParameterValue( dbCommand, "AddressLine1" ) == DBNull.Value ) ? "" : Convert.ToString( db.GetParameterValue( dbCommand, "AddressLine1" ) ) );
			addressLine2 = ( ( db.GetParameterValue( dbCommand, "AddressLine2" ) == DBNull.Value ) ? "" : Convert.ToString( db.GetParameterValue( dbCommand, "AddressLine2" ) ) );
			city = ( ( db.GetParameterValue( dbCommand, "City" ) == DBNull.Value ) ? "" : Convert.ToString( db.GetParameterValue( dbCommand, "City" ) ) );
			state = ( ( db.GetParameterValue( dbCommand, "State" ) == DBNull.Value ) ? "" : Convert.ToString( db.GetParameterValue( dbCommand, "State" ) ) );
			zipCode = ( ( db.GetParameterValue( dbCommand, "ZipCode" ) == DBNull.Value ) ? "" : Convert.ToString( db.GetParameterValue( dbCommand, "ZipCode" ) ) );
			zipCodePlus4 = ( ( db.GetParameterValue( dbCommand, "ZipCodePlus4" ) == DBNull.Value ) ? "" : Convert.ToString( db.GetParameterValue( dbCommand, "ZipCodePlus4" ) ) );
			
			//int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
		}

		public void UpdateInsert( BLL.PracticeLocation.BillingAddress bllBillingAddress )
		{
			int rowsAffected = 0;

			Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
			DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_PracticeLocationBillingAddress_UpdateInsert_When_Decentralized_Only" );

			db.AddInParameter( dbCommand, "PracticeLocationID", DbType.Int32, bllBillingAddress.PracticeLocationID );
			db.AddOutParameter( dbCommand, "AddressID", DbType.Int32, bllBillingAddress.AddressID );
			db.AddInParameter( dbCommand, "AttentionOf", DbType.String, bllBillingAddress.AttentionOf );
			db.AddInParameter( dbCommand, "AddressLine1", DbType.String, bllBillingAddress.Address.AddressLine1 );
			db.AddInParameter( dbCommand, "AddressLine2", DbType.String, bllBillingAddress.Address.AddressLine2 );
			db.AddInParameter( dbCommand, "City", DbType.String, bllBillingAddress.Address.City );
			db.AddInParameter( dbCommand, "State", DbType.String, bllBillingAddress.Address.State );
			db.AddInParameter( dbCommand, "ZipCode", DbType.String, bllBillingAddress.Address.ZipCode );
			db.AddInParameter( dbCommand, "ZipCodePlus4", DbType.String, bllBillingAddress.Address.ZipCodePlus4 );
			db.AddInParameter( dbCommand, "UserID", DbType.Int32, bllBillingAddress.UserID );

			rowsAffected = db.ExecuteNonQuery( dbCommand );

			//  addressID = Convert.ToInt32( db.GetParameterValue( dbCommand, "AddressID" ) );
			bllBillingAddress.AddressID = ( ( db.GetParameterValue( dbCommand, "AddressID" ) == DBNull.Value ) ? -1 : Convert.ToInt32( db.GetParameterValue( dbCommand, "AddressID" ) ) );

			//int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
		}
	
	
	}
}
