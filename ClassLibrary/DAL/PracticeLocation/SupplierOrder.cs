﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClassLibrary.DAL;

namespace DAL.PracticeLocation
{
    public class SupplierOrder
    {
        public static SupplierOrderInterface GetSupplierOrderInterface(int supplierOrderId)
        {
            
            VisionDataContext dataContext = VisionDataContext.GetVisionDataContext();

            using (dataContext)
            {
                var query = from s in dataContext.SupplierOrders
                            where s.SupplierOrderID == supplierOrderId
                            select new ClassLibrary.DAL.SupplierOrderInterface
                            {
                                SupplierOrderId = s.SupplierOrderID,
                                BregVisionOrderId = s.BregVisionOrderID,
                                PurchaseOrder = s.PurchaseOrder,
                                CreatedDate = s.CreatedDate
                            };

                return query.FirstOrDefault<ClassLibrary.DAL.SupplierOrderInterface>();
            }
        }
    }
}
