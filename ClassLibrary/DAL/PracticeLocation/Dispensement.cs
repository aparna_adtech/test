﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ClassLibrary.DAL;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using com.breg.vision.Integration.VisionBcsModels;
using System.Net;
using Newtonsoft.Json;
using System.Configuration;
using System.Xml.Linq;

namespace DAL.PracticeLocation
{
    public class Dispensement
    {
        private static string versionConfig = ConfigurationManager.AppSettings["DispenseDocumentVersion"];
        public static int DeleteItemfromDispensement(Int32 DispenseID, Int32 DispenseDetailID, int Quantity, string DeleteReason)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_DeleteDispensement");

            db.AddInParameter(dbCommand, "DispenseID", DbType.Int32, DispenseID);
            db.AddInParameter(dbCommand, "DispenseDetailID", DbType.Int32, DispenseDetailID);
            db.AddInParameter(dbCommand, "Quantity", DbType.Int32, Quantity);
            db.AddInParameter(dbCommand, "DeleteReason", DbType.String, DeleteReason);
            db.AddOutParameter(dbCommand, "@DeleteStatus", DbType.Int16, 4);

            int SQLReturn = Convert.ToInt32(db.ExecuteScalar(dbCommand));

            return Convert.ToInt32(db.GetParameterValue(dbCommand, "@DeleteStatus"));

        }


        public static void UpdateDispensementRecords(int DispenseID, int DispenseDetailID, string patientCode, DateTime dateDispensed, int physicianValue, decimal dmeDeposit, string patientEmail, string sideModifier, bool isUpdateInventoryChecked, int toDispenseLocationId, int fromDispenseLocationId, int userId, decimal acb, string mod1, string mod2, string mod3)
        {
            using (var db = VisionDataContext.GetVisionDataContext())
            {
                var dispense = db.Dispenses.First(d => d.DispenseID == DispenseID);
                var dispenseDetail = db.DispenseDetails.First(dd => dd.DispenseDetailID == DispenseDetailID);

                dispense.PatientCode = patientCode;
                dispense.DateDispensed = dateDispensed;
                dispense.PatientEmail = patientEmail;
                dispense.PracticeLocationID = toDispenseLocationId;

                dispenseDetail.PhysicianID = physicianValue;
                dispenseDetail.DMEDeposit = dmeDeposit;
                dispenseDetail.LRModifier = sideModifier;
                dispenseDetail.ActualChargeBilled = acb;
                dispenseDetail.LineTotal = dispenseDetail.Quantity*dispenseDetail.ActualChargeBilled;
                dispenseDetail.Mod1 = mod1;
                dispenseDetail.Mod2 = mod2;
                dispenseDetail.Mod3 = mod3;


                dispense.Total = Enumerable.Sum(db.DispenseDetails.Where(detail => detail.DispenseID == DispenseID).Where(dd => dd.LineTotal != null), detail => Convert.ToDecimal(detail.LineTotal));

                if (isUpdateInventoryChecked && toDispenseLocationId != fromDispenseLocationId)
                {
                    UpdateInventory(dispense, toDispenseLocationId, fromDispenseLocationId, userId, db);
                }

                db.SubmitChanges();
            }
        }

        private static void UpdateInventory(Dispense dispense, int newDispenseLocationId, int currentDispenseLocationId, int userId, VisionDataContext db)
        {

            var dispenseItems = dispense.DispenseDetails;
            foreach (var dispenseDetail in dispenseItems)
            {

                var currentLocationProductInventory = db.ProductInventories.First(pi =>
                    pi.PracticeCatalogProductID == dispenseDetail.PracticeCatalogProductID
                    && pi.PracticeLocationID == currentDispenseLocationId
                    && pi.IsActive);

                var newLocationProductInventory = db.ProductInventories.FirstOrDefault(pi =>
                    pi.PracticeCatalogProductID == dispenseDetail.PracticeCatalogProductID
                    && pi.PracticeLocationID == newDispenseLocationId
                    && pi.IsActive);

                if (newLocationProductInventory == null)
                {
                    newLocationProductInventory = new ProductInventory()
                    {
                        PracticeCatalogProductID = dispenseDetail.PracticeCatalogProductID,
                        PracticeLocationID = newDispenseLocationId,
                        QuantityOnHandPerSystem = 0, //
                        ParLevel = 0,
                        ReorderLevel = 0,
                        CriticalLevel = 0,
                        CreatedUserID = userId,
                        IsSetToActive = true,
                        IsActive = true,
                        CreatedDate = DateTime.Now
                    };
                    db.ProductInventories.InsertOnSubmit(newLocationProductInventory);
                }
                else
                {
                    if (newLocationProductInventory.QuantityOnHandPerSystem > 0)
                    {
                        newLocationProductInventory.QuantityOnHandPerSystem--;
                    }
                }

                currentLocationProductInventory.QuantityOnHandPerSystem++;
            }
        }

        public static DataSet GetCustomBraceOrdersforDispense_ParentLevel(int practiceLocationID, string SearchCriteria, string SearchText)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetCustomBraceOrdersforDispense_ParentLevel");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
            db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);

            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public static DataSet GetCustomBraceOrdersforDispense_Detail1Level(int practiceLocationID, string SearchCriteria, string SearchText)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetCustomBraceOrdersforDispense_Detail1Level");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
            db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);

            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }


        public static DataSet GetCustomBracesForDispense(Int32 practiceLocationID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetCustomBracesToBeDispensed");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);

            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public static void InsertPracticeICD9(int ICD9ID, int PracticeID)
        {

                //  If record already exists, do nothing.
                VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

                using (visionDB)
                {
                    var existsICD9 = from p_icd9 in visionDB.Practice_ICD9s
                                     where p_icd9.ICD9ID == ICD9ID
                                     && p_icd9.PracticeID == PracticeID
                                     select p_icd9;
                    if (existsICD9.Count() == 0)
                    {
                        ClassLibrary.DAL.Practice_ICD9 newICD9 = new ClassLibrary.DAL.Practice_ICD9();
                        newICD9.PracticeID = PracticeID;
                        newICD9.ICD9ID = ICD9ID;
                        newICD9.CreatedUserID = 1;
                        newICD9.CreatedDate = DateTime.Now;
                        newICD9.ModifiedUserID = 1;
                        newICD9.ModifiedDate = DateTime.Now;
                        newICD9.IsActive = true;
                        visionDB.Practice_ICD9s.InsertOnSubmit(newICD9);
                    }
                    else
                    {
                        foreach (ClassLibrary.DAL.Practice_ICD9 existingICD9 in existsICD9)
                        {
                            existingICD9.IsActive = true;
                        }
                    }
                    visionDB.SubmitChanges();
                }
                //VisionDataContext.CloseConnection(visionDB);


        }

        public static int AddPracticeICD9(int PracticeID, string HCPCs, string ICD9Code)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var icd9Exists = from icd9 in visionDB.ICD9s
                                 where icd9.HCPCs == HCPCs
                                 && icd9.ICD9Code == ICD9Code
                                 && (icd9.PracticeID == null || icd9.PracticeID == PracticeID)
                                 select icd9;
                if (icd9Exists.Count() < 1)
                {
                    ClassLibrary.DAL.ICD9 newPracticeICD9 = new ClassLibrary.DAL.ICD9();
                    newPracticeICD9.HCPCs = HCPCs;
                    newPracticeICD9.ICD9Code = ICD9Code;
                    newPracticeICD9.PracticeID = PracticeID;
                    newPracticeICD9.ICD9Description = "";
                    newPracticeICD9.CreatedUserID = 1;
                    newPracticeICD9.CreatedDate = DateTime.Now;
                    newPracticeICD9.ModifiedUserID = 1;
                    newPracticeICD9.ModifiedDate = DateTime.Now;
                    newPracticeICD9.IsActive = true;
                    visionDB.ICD9s.InsertOnSubmit(newPracticeICD9);
                    visionDB.SubmitChanges();

                    //VisionDataContext.CloseConnection(visionDB);
                    return newPracticeICD9.ICD9ID;

                }
                else
                {
                    //VisionDataContext.CloseConnection(visionDB);
                    return icd9Exists.FirstOrDefault().ICD9ID;
                }
            }
            
        }


        public static List<ClassLibrary.DAL.DispenseQueueInterface> GetDispenseQueue(int PracticeLocationID)
        {
            // provide backwards compatible sorted dispense queue (ascending)
            return GetDispenseQueue(PracticeLocationID, false);
        }

        public static List<ClassLibrary.DAL.DispenseQueueInterface> GetDispenseQueueById(int dispenseQueueId)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                // primary query
                var query = (from dq in visionDB.DispenseQueues
                             join pi in visionDB.ProductInventories on dq.ProductInventoryID equals pi.ProductInventoryID
                             join pcp in visionDB.PracticeCatalogProducts on pi.PracticeCatalogProductID equals pcp.PracticeCatalogProductID
                             join pcsb in visionDB.PracticeCatalogSupplierBrands on pcp.PracticeCatalogSupplierBrandID equals pcsb.PracticeCatalogSupplierBrandID
                             join mcp in visionDB.MasterCatalogProducts on pcp.MasterCatalogProductID equals mcp.MasterCatalogProductID into mastercatalog
                             from mc in mastercatalog.DefaultIfEmpty()
                             join tpp in visionDB.ThirdPartyProducts on pcp.ThirdPartyProductID equals tpp.ThirdPartyProductID into thirdparty
                             from tp in thirdparty.DefaultIfEmpty()
                             where
                                 dq.DispenseQueueID == dispenseQueueId
                                 && dq.IsDispensed == false
                                 && dq.IsActive == true
                                 && pi.IsActive == true
                                 && pcp.IsActive == true
                                 && pcsb.IsActive == true
                                 && (pcp.IsThirdPartyProduct == true ? tp.IsActive : mc.IsActive) == true
                             //orderby dq.PhysicianID ascending, dq.PatientCode ascending
                             select new ClassLibrary.DAL.DispenseQueueInterface
                             {
                                 CustomFitEnabled = pcp.Practice.IsCustomFitEnabled,
                                 DispenseQueueID = dq.DispenseQueueID,
                                 DispenseIdentifier = dq.DispenseIdentifier,
                                 QueuedReason = dq.QueuedReason,
                                 PracticeLocationID = dq.PracticeLocationID,
                                 ProductInventoryID = dq.ProductInventoryID,
                                 PhysicianID = dq.PhysicianID,
                                 PracticePayorID = dq.PracticePayerID,
                                 PatientCode = dq.PatientCode,
                                 PatientEmail = dq.PatientEmail,
                                 DispenseDate = dq.DispenseDate,
                                 DispenseSignatureID = dq.DispenseSignatureID.GetValueOrDefault(),
                                 IsMedicare = dq.IsMedicare,
                                 IsRental = dq.IsRental,
                                 ABNForm = dq.ABNForm,
                                 DMEDeposit = dq.DMEDeposit,
                                 QuantityToDispense = dq.QuantityToDispense,
                                 PracticeCatalogProductID = pcp.PracticeCatalogProductID,
                                 SupplierShortName = pcsb.SupplierShortName,
                                 BrandShortName = pcsb.BrandShortName,
                                 ShortName = pcp.IsThirdPartyProduct == true ? tp.ShortName : mc.ShortName,
                                 Code = pcp.IsThirdPartyProduct == true ? tp.Code : mc.Code,
                                 Side = dq.Side, //pcp.IsThirdPartyProduct == true ? tp.LeftRightSide : mc.LeftRightSide,
                                 Size = pcp.IsThirdPartyProduct == true ? tp.Size : mc.Size,
                                 Mod1 = dq.Mod1,
                                 Mod2 = dq.Mod2,
                                 Mod3 = dq.Mod3,
                                 ICD9Code = dq.ICD9_Code,
                                 HCPCsString = dq.HCPCs ?? visionDB.ConcatHCPCS(pcp.PracticeCatalogProductID),
                                 OTSHCPCsString = visionDB.ConcatHCPCSOTS(pcp.PracticeCatalogProductID),
                                 WholesaleCost = pcp.WholesaleCost,
                                 BillingCharge = pcp.BillingCharge,
                                 BillingChargeOffTheShelf = pcp.OffTheShelfBillingCharges.FirstOrDefault() != null ? pcp.OffTheShelfBillingCharges.FirstOrDefault().BillingCharge : -1,
                                 BillingChargeCash = pcp.BillingChargeCash,
                                 hdnBillingCharge = pcp.BillingCharge,
                                 hdnDMEDeposit = pcp.DMEDeposit,
                                 QuantityOnHandPerSystem = pi.QuantityOnHandPerSystem,
                                 CreatedDate = dq.CreatedDate,
                                 ABN_Needed = ((from p in visionDB.ProductHCPCs
                                                join a in visionDB.ABN_HCPCs on p.HCPCS.Substring(0, 5) equals a.HCPCs
                                                where a.ABN_Needed == true
                                                && p.PracticeCatalogProductID == pcp.PracticeCatalogProductID
                                                select (bool?)a.ABN_Needed).Take(1).First()) ?? false,
                                 PatientFirstName = dq.PatientFirstName,
                                 PatientLastName = dq.PatientLastName,
                                 Note = dq.Note,
                                 IsSplitCode = pcp.IsSplitCode,
                                 IsAlwaysOffTheShelf = pcp.IsAlwaysOffTheShelf,
                                 IsCustomFit = dq.IsCustomFit,
                                 FitterID = dq.FitterID,
                                 IsCustomBrace = mc.IsCustomBrace,
                                 IsCustomFabricated = dq.IsCustomFabricated,
                                 IsPreAuthorization = pcp.IsPreAuthorization,
                                 ABNType = dq.ABNType,
                                 EnableSide = CalculateDisableSideModifer(pcp),
                                 PreAuthorized = dq.PreAuthorized,
                                 IsCashCollection = dq.IsCashCollection,
                                 CustomFitProcedures = (
                         from cfp in visionDB.CustomFitProcedures
                         join dqcfp in visionDB.DispenseQueue_CFPs on
                             cfp.CustomFitProcedureID equals dqcfp.CustomFitProcedureID into g
                         from dqcfp2 in (from dqcfp in g
                                         where dqcfp.DispenseQueueID == dq.DispenseQueueID
                                         select dqcfp).DefaultIfEmpty()
                         select new DQCustomFitProcedure
                         {
                             CustomFitProcedureID = cfp.CustomFitProcedureID,
                             Description = dqcfp2.UserInput == null ? (cfp.IsUserEntered ? "" : cfp.Description) : dqcfp2.UserInput,
                             IsActive = (dqcfp2 != null),
                             IsUserEntered = cfp.IsUserEntered
                         }
                                     ),
                                 BregVisionOrderID = dq.BregVisionOrderID

                             });

                // enumerate items as list and return
                return query.ToList<ClassLibrary.DAL.DispenseQueueInterface>();
            }
        }

        public static bool CalculateDisableSideModifer(PracticeCatalogProduct pcp)
        {
            if (pcp.MasterCatalogProduct != null)
            {
                if (pcp.MasterCatalogProduct.LeftRightSide == "LT" || pcp.MasterCatalogProduct.LeftRightSide == "RT")
                    return false;
            }else if (pcp.ThirdPartyProduct != null)
            {
                if (pcp.ThirdPartyProduct.LeftRightSide == "LT" || pcp.ThirdPartyProduct.LeftRightSide == "RT")
                    return false;
            }
            return true;
        }

        public static List<ClassLibrary.DAL.DispenseQueueInterface> GetDispenseQueue(int PracticeLocationID, bool isDescending)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                // primary query
                var query = (from dq in visionDB.DispenseQueues
                             join pl in visionDB.PracticeLocations on dq.PracticeLocationID equals pl.PracticeLocationID
                             join prac in visionDB.Practices on pl.PracticeID equals prac.PracticeID
                            join pi in visionDB.ProductInventories on dq.ProductInventoryID equals pi.ProductInventoryID
                            join pcp in visionDB.PracticeCatalogProducts on pi.PracticeCatalogProductID equals pcp.PracticeCatalogProductID
                            join pcsb in visionDB.PracticeCatalogSupplierBrands on pcp.PracticeCatalogSupplierBrandID equals pcsb.PracticeCatalogSupplierBrandID
                            join mcp in visionDB.MasterCatalogProducts on pcp.MasterCatalogProductID equals mcp.MasterCatalogProductID into mastercatalog
                            from mc in mastercatalog.DefaultIfEmpty()
                            join tpp in visionDB.ThirdPartyProducts on pcp.ThirdPartyProductID equals tpp.ThirdPartyProductID into thirdparty
                            from tp in thirdparty.DefaultIfEmpty()
                            where
                                dq.PracticeLocationID == PracticeLocationID
                                && dq.IsDispensed == false
                                && dq.IsActive == true
                                && pi.IsActive == true
                                && pcp.IsActive == true
                                && pcsb.IsActive == true
                                && (pcp.IsThirdPartyProduct == true ? tp.IsActive : mc.IsActive) == true
                            //orderby dq.PhysicianID ascending, dq.PatientCode ascending
                            select new ClassLibrary.DAL.DispenseQueueInterface
                            {   
                                CustomFitEnabled =  prac.IsCustomFitEnabled,                       
                                DispenseQueueID = dq.DispenseQueueID,
                                DispenseIdentifier = dq.DispenseIdentifier,
                                QueuedReason = dq.QueuedReason,
                                PracticeLocationID = dq.PracticeLocationID,
                                ProductInventoryID =dq.ProductInventoryID,
                                PhysicianID =dq.PhysicianID,
                                PracticePayorID = dq.PracticePayerID,
                                PatientCode = dq.PatientCode,
                                PatientEmail = dq.PatientEmail,
                                DispenseDate = dq.DispenseDate,
                                DispenseSignatureID = dq.DispenseSignatureID.GetValueOrDefault(),
                                IsMedicare = dq.IsMedicare,
                                IsRental = dq.IsRental,
                                ABNForm = dq.ABNForm,
                                DMEDeposit = dq.DMEDeposit,
                                QuantityToDispense = dq.QuantityToDispense,
                                PracticeCatalogProductID = pcp.PracticeCatalogProductID,
                                SupplierShortName = pcsb.SupplierShortName,
                                BrandShortName = pcsb.BrandShortName,
                                ShortName = pcp.IsThirdPartyProduct == true ? tp.ShortName : mc.ShortName,
                                Code = pcp.IsThirdPartyProduct == true ? tp.Code : mc.Code,
                                Side = dq.Side, //pcp.IsThirdPartyProduct == true ? tp.LeftRightSide : mc.LeftRightSide,
                                Size = pcp.IsThirdPartyProduct == true ? tp.Size : mc.Size,
                                Mod1 = dq.Mod1,
                                Mod2 = dq.Mod2,
                                Mod3 = dq.Mod3,
                                ICD9Code = dq.ICD9_Code,
                                HCPCsString = dq.HCPCs ?? visionDB.ConcatHCPCS(pcp.PracticeCatalogProductID),
								OTSHCPCsString = visionDB.ConcatHCPCSOTS(pcp.PracticeCatalogProductID),
                                WholesaleCost = pcp.WholesaleCost,
                                BillingCharge = pcp.BillingCharge,
                                BillingChargeOffTheShelf = pcp.OffTheShelfBillingCharges.FirstOrDefault()!=null?pcp.OffTheShelfBillingCharges.FirstOrDefault().BillingCharge:-1,
                                BillingChargeCash = pcp.BillingChargeCash,
                                hdnBillingCharge = pcp.BillingCharge,
                                hdnDMEDeposit = pcp.DMEDeposit,
                                QuantityOnHandPerSystem = pi.QuantityOnHandPerSystem,
                                CreatedDate = dq.CreatedDate,
                                ABN_Needed = ((from p in visionDB.ProductHCPCs
                                               join a in visionDB.ABN_HCPCs on p.HCPCS equals a.HCPCs
                                               where a.ABN_Needed == true
                                               && p.PracticeCatalogProductID == pcp.PracticeCatalogProductID
                                               select (bool?)a.ABN_Needed).Take(1).First()) ?? false,
                                PatientFirstName = dq.PatientFirstName,
                                PatientLastName = dq.PatientLastName,
                                Note = dq.Note,
                                IsSplitCode = pcp.IsSplitCode,
                                IsAlwaysOffTheShelf = pcp.IsAlwaysOffTheShelf,
                                IsCustomFit = dq.IsCustomFit,
                                FitterID = dq.FitterID,
                                IsCustomBrace = mc.IsCustomBrace,
                                IsCustomFabricated = dq.IsCustomFabricated,
                                IsPreAuthorization = pcp.IsPreAuthorization,
                                ABNType = dq.ABNType,
                                PreAuthorized = dq.PreAuthorized,
                                IsCashCollection = dq.IsCashCollection,
                               
                                CustomFitProcedures = (
                        from cfp in visionDB.CustomFitProcedures
                        join dqcfp in visionDB.DispenseQueue_CFPs on
                            cfp.CustomFitProcedureID equals dqcfp.CustomFitProcedureID into g
                                    from dqcfp2 in (from dqcfp in g where dqcfp.DispenseQueueID == dq.DispenseQueueID
                            select dqcfp).DefaultIfEmpty()
                                    select new DQCustomFitProcedure {
                            CustomFitProcedureID = cfp.CustomFitProcedureID,
                                        Description = dqcfp2.UserInput == null ? (cfp.IsUserEntered ? "" : cfp.Description) : dqcfp2.UserInput,
                            IsActive = (dqcfp2 != null),
                            IsUserEntered = cfp.IsUserEntered
                        }
                                    ),
                                BregVisionOrderID = dq.BregVisionOrderID,
                                DispenseGroupingID = dq.DispenseGroupingId

                            });

                // order query based on isDescending parameter
                var ordered_query =
                    isDescending
                        ? query.OrderByDescending(a => a.CreatedDate)
                        : query.OrderBy(a => a.CreatedDate);

                // enumerate items as list and return
                return ordered_query.ToList<ClassLibrary.DAL.DispenseQueueInterface>();
            }
        }

        public static decimal GetDMEDeposit(int ProductInventoryID)
        {
            VisionDataContext visionDB = null;
            visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var query = (from pi in visionDB.ProductInventories
                             join pcp in visionDB.PracticeCatalogProducts on pi.PracticeCatalogProductID equals pcp.PracticeCatalogProductID
                             where pi.ProductInventoryID == ProductInventoryID
                             select new
                             {
                                 pcp.DMEDeposit
                             }).SingleOrDefault();

                VisionDataContext.CloseConnection(visionDB);

                return query.DMEDeposit;
            }
        }



        public static IEnumerable<ICD9> GetDispenseQueueICD9s(ArrayList ProductIDList, int PracticeLocationID)
        {
            VisionDataContext visionDB = null;
            visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                var practiceQuery = from p in visionDB.PracticeLocations
                                    where p.PracticeLocationID == PracticeLocationID
                                    select p.PracticeID;

                //var productQuery = from dq in visionDB.DispenseQueues
                //                   join pi in visionDB.ProductInventories on dq.ProductInventoryID equals pi.ProductInventoryID
                //                   where dq.PracticeLocationID == PracticeLocationID
                //                   select pi.PracticeCatalogProductID;
                var productList = ProductIDList.Cast<int>().ToList();

                var hcpcsQuery = from ph in visionDB.ProductHCPCs
                                 where productList.Contains<int>((int)ph.PracticeCatalogProductID)
                                 select ph.HCPCS;

                var idc9Query = from idc in visionDB.ICD9s
                                join idc_practice in visionDB.Practice_ICD9s on idc.ICD9ID equals idc_practice.ICD9ID
                                where hcpcsQuery.ToList<string>().Contains<string>((string)idc.HCPCs)
                                && practiceQuery.ToList<int>().Contains<int>((int)idc_practice.PracticeID)
                                && idc.IsActive == true
                                && idc_practice.IsActive == true
                                select idc;

                //VisionDataContext.CloseConnection(visionDB);

                return idc9Query.ToList().AsEnumerable<ICD9>();
            }
        }

        /// <summary>
        /// This will return all the ICD9 codes for all of the HCPCs for each item in the dispense queue.
        /// </summary>
        /// <param name="PracticeLocationID"></param>
        /// <returns></returns>
        public static IEnumerable<ICD9> GetDispenseQueueICD9s(int PracticeLocationID)
        {
            VisionDataContext visionDB = null;
            visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var practiceQuery = from p in visionDB.PracticeLocations
                                    where p.PracticeLocationID == PracticeLocationID
                                    select p.PracticeID;

                var productQuery = from dq in visionDB.DispenseQueues
                                   join pi in visionDB.ProductInventories on dq.ProductInventoryID equals pi.ProductInventoryID
                                   where dq.PracticeLocationID == PracticeLocationID
                                   select pi.PracticeCatalogProductID;

                var hcpcsQuery = from ph in visionDB.ProductHCPCs
                                 where productQuery.Contains<int>((int)ph.PracticeCatalogProductID)
                                 select ph.HCPCS.Substring(0, 5);

                var idc9Query = from idc in visionDB.ICD9s
                                join idc_practice in visionDB.Practice_ICD9s on idc.ICD9ID equals idc_practice.ICD9ID
                                where hcpcsQuery.ToList<string>().Contains<string>((string)idc.HCPCs)
                                && practiceQuery.ToList<int>().Contains<int>((int)idc_practice.PracticeID)
                                && idc.IsActive == true
                                && idc_practice.IsActive == true
                                select idc;

                //VisionDataContext.CloseConnection(visionDB);

                return idc9Query.ToList().AsEnumerable<ICD9>();
            }
        }

        public static void DeleteDispenseQueueItem(Int32 DispenseQueueID)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                DispenseQueue dispenseQueue = visionDB.DispenseQueues.Where(dq => dq.DispenseQueueID == DispenseQueueID).SingleOrDefault();
                if (dispenseQueue != null)
                {
                    //visionDB.DispenseQueues.DeleteOnSubmit(dispenseQueue);
                    dispenseQueue.IsActive = false;
                    visionDB.SubmitChanges();
                }
                //VisionDataContext.CloseConnection(visionDB);
            }
        }
        //HACK(Jackson) We need different behavior based on which version of the application is sending the request. Remove IsCalledFromV1Endpoint when Version 4.9 and below are phased out of use. see comments below for specifics 
        public static int AddUpdateDispensement(int DispenseQueueID, int PracticeLocationID, int InventoryID, string PatientCode, int PhysicianID, string ICD9_Code, bool IsMedicare, bool ABNForm, bool IsCashCollection, string Side, string AbnType, decimal DMEDeposit, int? BregVisionOrderID, int Quantity, bool? isCustomFit, DateTime? DispenseDate, string mod1, string mod2, string mod3, int CreatedUser, DateTime CreatedDate, string QueuedReason, string queuedReasonDescription=null, string abnReason = "", string patientEmail = "", bool isRental = false, string declineReason = "", int medicareOption = 0, int dispenseSignatureID = 0, bool isDeleted = false, string patientFirstName = "", string patientLastName = "", string note = "", int? payerID = null, List<Tuple<int, string>> customFitProcedureResponses = null, int? FitterID = null, bool? isCustomFabricated = null, string deviceIdentifier = null, string dispenseIdentifier = null, bool preAuthorized = false, bool IsCalledFromV1Endpoint = true, string[] documentIds = null)
        {
            using (VisionDataContext visionDB = VisionDataContext.GetVisionDataContext())
            {
                //third party vendors sometimes use LT or RT.  We need L or R
                Side = Side.Replace("RT", "R");
                Side = Side.Replace("Rt", "R");
                Side = Side.Replace("LT", "L");
                Side = Side.Replace("Lt", "L");

                string[] validSides = {"U", "L", "R", "BI"};

                if (!string.IsNullOrEmpty(Side)) // this condition checks whether the item is added for the first time into the queue if so has side as "" empty string.
                {
                    if (!validSides.Contains(Side))
                        Side = "U";
                }

                int? shoppingCartId = null;
                if (BregVisionOrderID != null)
                {
                   var shoppingCart = visionDB.ShoppingCartCustomBraces.FirstOrDefault(x => x.BregVisionOrderID == BregVisionOrderID);
                    if (shoppingCart != null)
                    {
                        shoppingCartId = shoppingCart.ShoppingCartID;
                    }
                }

                DispenseQueue dispenseQueue = null;
                if (DispenseQueueID > 0)
                    dispenseQueue = visionDB.DispenseQueues.Where(dq => dq.DispenseQueueID == DispenseQueueID).Single();
                else
                    dispenseQueue = new DispenseQueue();

                int? physicianID = null;
                if (PhysicianID != 0)
                    physicianID = PhysicianID;

                dispenseQueue.PracticeLocationID = PracticeLocationID;
                dispenseQueue.ProductInventoryID = InventoryID;
                dispenseQueue.PatientCode = PatientCode;
                dispenseQueue.PatientFirstName = patientFirstName;
                dispenseQueue.PatientLastName = patientLastName;
                dispenseQueue.PatientEmail = patientEmail;
                dispenseQueue.PhysicianID = physicianID;

                if (string.IsNullOrEmpty(ICD9_Code))
                    ICD9_Code = "";
                dispenseQueue.ICD9_Code = ICD9_Code.Trim();

                dispenseQueue.IsMedicare = IsMedicare;
                //Adding logic to ensure capability with old and new version of visison app.
                dispenseQueue.ABNForm = AbnType == "Medicare" ? true : ABNForm;
                dispenseQueue.IsCashCollection = IsCashCollection;
                dispenseQueue.Side = Side;
               
                dispenseQueue.IsCustomFit = isCustomFit;
                dispenseQueue.IsCustomFabricated = isCustomFabricated;

                dispenseQueue.Mod1 = mod1;
                dispenseQueue.Mod2 = mod2;
                dispenseQueue.Mod3 = mod3;
                dispenseQueue.QueuedReason = QueuedReason;
                dispenseQueue.PreAuthorized = preAuthorized;

                ApplyCustomFitValuesToDispenseQueue(isCustomFit, dispenseQueue, visionDB, isCustomFabricated, dispenseQueue.ProductInventoryID, PracticeLocationID);

                dispenseQueue.DMEDeposit = DMEDeposit;

                if (!string.IsNullOrEmpty(abnReason))
                    dispenseQueue.ABNReason = abnReason;

                dispenseQueue.IsRental = isRental;
                dispenseQueue.FitterID = FitterID <= 0 ? null : FitterID;
                
                if (!string.IsNullOrEmpty(declineReason))
                    dispenseQueue.DeclineReason = declineReason;

                if (medicareOption > 0)
                    dispenseQueue.MedicareOption = medicareOption;

                if ((BregVisionOrderID != null) && (BregVisionOrderID > 0))
                    dispenseQueue.BregVisionOrderID = BregVisionOrderID;

                dispenseQueue.QuantityToDispense = Quantity;

                if (dispenseSignatureID > 0)
                    dispenseQueue.DispenseSignatureID = dispenseSignatureID;

                if (DispenseDate != null)
                    dispenseQueue.DispenseDate = DispenseDate;

                dispenseQueue.CreatedUser = dispenseQueue.CreatedUser < 1 ? CreatedUser : dispenseQueue.CreatedUser;
                dispenseQueue.CreatedDate = dispenseQueue.CreatedDate < DateTime.Parse("1/1/1000") ? CreatedDate : dispenseQueue.CreatedDate;
                dispenseQueue.ModifiedUser = CreatedUser;
                dispenseQueue.ModifiedDate = CreatedDate;
                dispenseQueue.IsActive = !isDeleted;

                if (DispenseQueueID < 1 || !IsCalledFromV1Endpoint)
                {
                    // HACK: do not update Note and Practice Payer ID on update because the current production app does not persist this information on the dispensement and/or dispensement item, the only correct scenario the app is supporting right now is on initial submit of dispensement to queue or otherwise. fix this if we ever allow editing of payer or note. -hchan 03182016
                    // HACK #2: because the web provides the ability to edit this data, we need to allow it to go through for not just the case above; but, also when this method is called from the website. -hchan 05172016
                    dispenseQueue.Note = note;
                    dispenseQueue.PracticePayerID = payerID <= 0 ? null : payerID;

                    if (DispenseQueueID < 1)
                    {
                        // HACK: only allow the dispense identifier to be specified once. The app is supposed to use this value to identify unique dispensements. Current production app does not pay attention and loses this value on save/update. Theoretically, this value should not change beyond the first write. -hchan 03182016
                        dispenseQueue.DispenseIdentifier = dispenseIdentifier;

                        visionDB.DispenseQueues.InsertOnSubmit(dispenseQueue);
                    }
                }

                dispenseQueue.DeviceIdentifier = deviceIdentifier;
                //Adding logic to ensure capability with old and new version of visison app.
                dispenseQueue.ABNType = ABNForm == true ? "Medicare" : AbnType;

                /* 
                    VBCS-1421
                    Note(Jackson): This check is done every time a dispense record is saved. It is likely that the record that it will find is itself in the following check.

                    In another scenario it might find nothing.

                    This is by design because if the dispense date has changed or any of the other data that could impact how it should be grouped and it should get a new dispensegroupingId                
                */
                {
                    var matchingDispensements =
                        visionDB.DispenseQueues
                            .Where(
                                x =>
                                    x.PracticeLocationID == dispenseQueue.PracticeLocationID
                                    && !x.IsDispensed
                                    && x.IsActive == dispenseQueue.IsActive
                                    &&
                                    (
                                        x.PhysicianID != null
                                        && x.PhysicianID == dispenseQueue.PhysicianID
                                    )
                                    &&
                                    (
                                        x.PatientCode != null
                                        && x.PatientCode != string.Empty
                                        && x.PatientCode == dispenseQueue.PatientCode
                                    )
                                    &&
                                    (
                                        x.BregVisionOrderID == dispenseQueue.BregVisionOrderID
                                        ||
                                        (
                                            x.BregVisionOrderID == null && dispenseQueue.BregVisionOrderID == null
                                        )
                                    )
                                    &&
                                    (
                                        (
                                            x.FitterID != null
                                            && dispenseQueue.FitterID != null
                                            && x.FitterID == dispenseQueue.FitterID
                                        )
                                        ||
                                        (
                                            x.FitterID != null && dispenseQueue.FitterID != null
                                        )
                                    )
                                    &&
                                    (
                                        (
                                            (x.DispenseSignatureID != null && dispenseQueue.DispenseSignatureID != null)
                                            && (x.DispenseSignatureID != 0 && dispenseQueue.DispenseSignatureID != 0)
                                            && x.DispenseSignatureID == dispenseQueue.DispenseSignatureID
                                        )
                                        ||
                                        (
                                            x.DispenseSignatureID == null && dispenseQueue.DispenseSignatureID == null
                                        )
                                    )
                                            /*NOTE(Jackson) If both records have null DispenseSignatureID
                                                *  the signature matching portion of this check passes*/
                            )
                            .ToList()
                            .Where(x => DateMatches(x, dispenseQueue))
                            .ToList();

                    dispenseQueue.DispenseGroupingId =
                        matchingDispensements.Any()
                        ? matchingDispensements.First().DispenseGroupingId
                        : Guid.NewGuid().ToString();
                }

                visionDB.SubmitChanges();

                var dispenseQueueId = dispenseQueue.DispenseQueueID;

                // HACK: do not update Custom Fit Procedure Responses on update because the current production app does not persist this information, the only correct scenario the app is supporting right now is on initial submit of dispensement to queue or otherwise. fix this if we ever allow editing of Custom Fit Procedure Responses. -hchan 03182016
                // HACK #2: because the web provides the ability to edit this data, we need to allow it to go through for not just the case above; but, also when this method is called from the website. -hchan 05172016
                if (DispenseQueueID < 1 || !IsCalledFromV1Endpoint)
                {
                    if (customFitProcedureResponses != null)
                    {
                        IEnumerable<DispenseQueue_CFP> cfpsToDelete =
                            (from dqcfp in visionDB.DispenseQueue_CFPs
                             where dqcfp.DispenseQueueID == dispenseQueueId
                             select dqcfp).ToList();
                        visionDB.DispenseQueue_CFPs.DeleteAllOnSubmit(cfpsToDelete);

                        foreach (var response in customFitProcedureResponses)
                        {
                            var customFitProcedure = new DispenseQueue_CFP()
                            {
                                DispenseQueueID = dispenseQueueId,
                                CustomFitProcedureID = response.Item1,
                                UserInput = response.Item2
                            };
                            visionDB.DispenseQueue_CFPs.InsertOnSubmit(customFitProcedure);
                        }
                        visionDB.SubmitChanges();
                    }
                }

                if(dispenseQueueId > 0 && documentIds != null && documentIds.Length > 0)
                {
                    // ToDo: Update the document Ids.
                    foreach(var docId in documentIds)
                    {

                    }
                }

                return dispenseQueueId;
            }
        }

        private static bool DateMatches(DispenseQueue dbDispenseQueue, DispenseQueue dispensequeue)
        {
            //Hack(Jackson): although we do this in match in the above linq to sql query, because we are doing more filtering we still want to make sure that bregvisionorderid items are grouped together even if the dispensedates change these products have to be dispensed together
            if (dispensequeue.BregVisionOrderID.HasValue 
                && dbDispenseQueue.BregVisionOrderID.HasValue
                && dbDispenseQueue.BregVisionOrderID.Value == dispensequeue.BregVisionOrderID.Value)
            {
                return true;
            }

            if (dbDispenseQueue.DispenseDate == null && dispensequeue.DispenseDate == null)
            {
                return true;
            }
            if (dispensequeue.DispenseDate.HasValue)
            {
                if (!dbDispenseQueue.DispenseDate.HasValue)
                {
                    return false;
                }
                else if (dispensequeue.DispenseDate.Value.Date == dbDispenseQueue.DispenseDate.Value.Date)
                {
                    return true;
                }
            }
            return false;
        }

        private static void ApplyCustomFitValuesToDispenseQueue(bool? isCustomFit, DispenseQueue dispenseQueue, VisionDataContext db, bool? isCustomFabricated, decimal productInventoryId, int practiceLocationId)
        {
            var dispenseQueueCustomFitDetails = GetProductCustomFitDetails(db, productInventoryId);

            // get practice-level custom fit status
            // NOTE: this should be moved at a later date to a global module that handles these types of requests
            var isPracticeCustomFitEnabled =
                    db.PracticeLocations
                        .Where(x => x.PracticeLocationID == practiceLocationId)
                        .Any(x => x.Practice.IsCustomFitEnabled);


            ApplyDispenseQueueCustomFitDetailsToQueue(isCustomFit, dispenseQueue, isCustomFabricated, dispenseQueueCustomFitDetails, practiceLocationId, isPracticeCustomFitEnabled);
        }

        protected internal static void ApplyDispenseQueueCustomFitDetailsToQueue(bool? isCustomFit, DispenseQueue dispenseQueue, bool? isCustomFabricated, ProductCustomFitDetails productCustomFitDetails, int practiceLocationId, bool isPracticeCustomFitEnabled)
        {
            var isCustomFabricatedValue = isCustomFabricated.HasValue && isCustomFabricated.Value;
            if (productCustomFitDetails.IsCustomBrace || isCustomFabricatedValue){
                dispenseQueue.HCPCs = productCustomFitDetails.Hcpcs;
                return;
            }

            if (productCustomFitDetails.IsSplitCode && isPracticeCustomFitEnabled){
                var isPreCustomFitVersion = !isCustomFit.HasValue;
                var isOffTheShelfDispense = isCustomFit.HasValue && productCustomFitDetails.IsSplitCode && !isCustomFit.Value;

                if (isPreCustomFitVersion || isOffTheShelfDispense){
                    dispenseQueue.HCPCs = productCustomFitDetails.HcpcsOts;
                } else {
                    dispenseQueue.HCPCs = productCustomFitDetails.Hcpcs;
                }
            }
            else
            {
                dispenseQueue.HCPCs = productCustomFitDetails.Hcpcs;
            }
        }

        private static ProductCustomFitDetails GetProductCustomFitDetails(VisionDataContext db, decimal productInventoryId)
        {
            var productCustomFitDetails =
                db.ProductInventories.Where(inventory => inventory.ProductInventoryID == productInventoryId)
                    .Select(inventory => new ProductCustomFitDetails
                    {
                        IsCustomBrace =
                            inventory.PracticeCatalogProduct.MasterCatalogProduct != null &&
                            inventory.PracticeCatalogProduct.MasterCatalogProduct.IsCustomBrace,
                        IsSplitCode = inventory.PracticeCatalogProduct.IsSplitCode,
                        IsAlwaysOffTheShelf = inventory.PracticeCatalogProduct.IsAlwaysOffTheShelf,
                        Hcpcs = db.ConcatHCPCS(inventory.PracticeCatalogProductID),
                        HcpcsOts = db.ConcatHCPCSOTS(inventory.PracticeCatalogProductID)
                    }).FirstOrDefault();

			if (productCustomFitDetails == null)
			{
				productCustomFitDetails = new ProductCustomFitDetails
				{
					IsCustomBrace = false,
					IsSplitCode = false,
					IsAlwaysOffTheShelf = false,
					Hcpcs = string.Empty,
					HcpcsOts = string.Empty
				};
			}
            return productCustomFitDetails;
        }

        protected internal class ProductCustomFitDetails
        {
            public bool IsCustomBrace { get; set; }
            public bool IsSplitCode { get; set; }
            public bool IsAlwaysOffTheShelf { get; set; }
            public string Hcpcs { get; set; }
            public string HcpcsOts { get; set; }
        }

        public static bool IsDispensementExist(string deviceIdentifier, string dispenseIdentifier)
        {
            if (deviceIdentifier != null && dispenseIdentifier != null)
            {
                VisionDataContext visionDB = null;
                visionDB = VisionDataContext.GetVisionDataContext();

                using (visionDB)
                {
                    IQueryable<DispenseQueue> dispenseQueue = visionDB.DispenseQueues.Where(
                        dq => dq.DeviceIdentifier == deviceIdentifier && dq.DispenseIdentifier == dispenseIdentifier);
                    if (dispenseQueue != null && dispenseQueue.Count() > 0)
                        return true;
                }
            }
            return false;
        }

        public static int CreateNewDispenseBatch(int PracticeLocationID)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                DispenseBatch dispenseBatch = new DispenseBatch();
                dispenseBatch.DispenseDate = DateTime.Now;
                dispenseBatch.PracticeLocationID = PracticeLocationID;
                dispenseBatch.CreatedUser = 1;
                dispenseBatch.CreatedDate = DateTime.Now;
                dispenseBatch.ModifiedUser = 1;
                dispenseBatch.ModifiedDate = DateTime.Now;
                dispenseBatch.IsActive = true;
                visionDB.DispenseBatches.InsertOnSubmit(dispenseBatch);
                visionDB.SubmitChanges();

                //VisionDataContext.CloseConnection(visionDB);

                return dispenseBatch.DispenseBatchID;
            }
        }

        public static bool AreCustomBracesInDispensementCompletlySelected(int PracticeLocationID, List<int> dispenseQueueIds)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                //get the custom brace order ids with all of their productsids, omit anything that is not a custom brace
                var dispenseQueueBregVisionOrderIdPairs = (from queue in visionDB.DispenseQueues
                    where dispenseQueueIds.Contains(queue.DispenseQueueID)
                          && queue.BregVisionOrderID != null
                          && queue.DispenseQueueID > 0
                    select new KeyValuePair<int,int>(queue.DispenseQueueID,queue.BregVisionOrderID.Value)
                    ).ToList();
                
                if (!dispenseQueueBregVisionOrderIdPairs.Any())// this means that this dispensement has no custom brace orders in it so the rest fo the calls are unneccessary 
                {
                    return true;
                }

                var bregVisionOrderIds = dispenseQueueBregVisionOrderIdPairs.Select(z => z.Value).Distinct().ToList();
                //get all of the dispensequeues with the bregvision order ids that are in this dispensement
                var dispenseQueueIdsRequired = (from dq_bvoid in visionDB.DispenseQueues
                    where
                        dq_bvoid.BregVisionOrderID != null && dq_bvoid.BregVisionOrderID != 0
                        && dq_bvoid.PracticeLocationID == PracticeLocationID
                        && dq_bvoid.IsDispensed == false
                        && dq_bvoid.IsActive == true
                        && bregVisionOrderIds.Contains(dq_bvoid.BregVisionOrderID.Value)
                    select dq_bvoid.DispenseQueueID
                    ).ToList();
                
                
                //make sure the required dispensequeueids are in the list dispensequeueids passed in
                var x = dispenseQueueIdsRequired.Except(dispenseQueueIds);
                return !x.Any();
            }
        }

        public static int DispenseProducts(int PracticeLocationID, List<int> SelectedProductInventoryIDs, List<int> DispensedProducts, List<int> selectedDispenseQueueIDs, bool IsV5Dispensement, out int dispenseId, bool createdByHandheld = false)
        {
            dispenseId = 0;
            if (SelectedProductInventoryIDs.Count > 0)
            {
                int dispenseBatchID = CreateNewDispenseBatch(PracticeLocationID);

                DispenseCustomBraces(PracticeLocationID, dispenseBatchID, SelectedProductInventoryIDs, DispensedProducts, selectedDispenseQueueIDs, IsV5Dispensement, createdByHandheld: createdByHandheld);

                DispenseMultipleItems(PracticeLocationID, dispenseBatchID, SelectedProductInventoryIDs, DispensedProducts, selectedDispenseQueueIDs, IsV5Dispensement, out dispenseId, createdByHandheld: createdByHandheld);

                DispenseSingleItems(PracticeLocationID, dispenseBatchID, SelectedProductInventoryIDs, DispensedProducts, selectedDispenseQueueIDs, IsV5Dispensement, createdByHandheld: createdByHandheld);

                return dispenseBatchID;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// This function groups DispenseQueue items by BregVisionOrderID(only custom brace orders should have one)
        /// and dispenses them in those groups
        /// </summary>
        /// <param name="PracticeLocationID"></param>
        public static void DispenseCustomBraces(int PracticeLocationID, int DispenseBatchID, List<int> SelectedProductInventoryIDs, List<int> DispensedProducts, List<int> selectedDispenseQueueIDs, bool IsV5Dispensement, bool createdByHandheld = false)
        {
            VisionDataContext visionDB = null;
            visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                var customBraces = (from dq_bvoid in visionDB.DispenseQueues
                                    where
                                        dq_bvoid.BregVisionOrderID != null
                                        && dq_bvoid.PracticeLocationID == PracticeLocationID
                                        && dq_bvoid.IsDispensed == false
                                        && dq_bvoid.IsActive == true
                                        && SelectedProductInventoryIDs.Contains(dq_bvoid.ProductInventoryID)
                                        && selectedDispenseQueueIDs.Contains(dq_bvoid.DispenseQueueID)
                                    group dq_bvoid by dq_bvoid.BregVisionOrderID into bvoids
                                    select new
                                    {
                                        BregVisionOrderID = bvoids.Key,
                                        //DispenseQueues1 = (from d in visionDB.DispenseQueues where d.BregVisionOrderID == bvoids.Key select d),
                                        DispenseQueues = (from dq in visionDB.DispenseQueues
														  join pi in visionDB.ProductInventories on dq.ProductInventoryID equals pi.ProductInventoryID
														  join pcp in visionDB.PracticeCatalogProducts on pi.PracticeCatalogProductID equals pcp.PracticeCatalogProductID
														  join p in visionDB.Practices on pcp.PracticeID equals p.PracticeID
														  join pcsb in visionDB.PracticeCatalogSupplierBrands on pcp.PracticeCatalogSupplierBrandID equals pcsb.PracticeCatalogSupplierBrandID
                                                          join mcp in visionDB.MasterCatalogProducts on pcp.MasterCatalogProductID equals mcp.MasterCatalogProductID into mastercatalog
                                                          from mc in mastercatalog.DefaultIfEmpty()
                                                          join tpp in visionDB.ThirdPartyProducts on pcp.ThirdPartyProductID equals tpp.ThirdPartyProductID into thirdparty
                                                          from tp in thirdparty.DefaultIfEmpty()
                                                          where
                                                              dq.PracticeLocationID == PracticeLocationID
                                                              && pi.PracticeLocationID == PracticeLocationID
                                                              && dq.IsDispensed == false
                                                              && dq.IsActive == true
                                                              && dq.BregVisionOrderID == bvoids.Key
                                                              && selectedDispenseQueueIDs.Contains(dq.DispenseQueueID)

                                                          orderby dq.PhysicianID ascending, dq.PatientCode ascending
                                                          select new
                                                          {
                                                              dq.DispenseQueueID,
                                                              dq.PracticeLocationID,
                                                              dq.ProductInventoryID,
                                                              dq.PhysicianID,
                                                              dq.PatientCode,
                                                              dq.PatientEmail,
                                                              dq.PatientFirstName,
                                                              dq.PatientLastName,
                                                              dq.DispenseDate,
                                                              dq.IsMedicare,
                                                              dq.ABNForm,
                                                              dq.ModifiedUser,
                                                              dq.QuantityToDispense,
                                                              dq.IsCashCollection,
                                                              dq.IsDispensed,
                                                              dq.ICD9_Code,
                                                              dq.DMEDeposit,
                                                              dq.PracticePayerID,
                                                              dq.Note,
                                                              dq.IsCustomFit,
                                                              LRModifier = dq.Side,
                                                              pcp.PracticeCatalogProductID,
                                                              pcsb.SupplierShortName,
                                                              pcsb.BrandShortName,
                                                              ShortName = pcp.IsThirdPartyProduct == true ? tp.ShortName : mc.ShortName,
                                                              Code = pcp.IsThirdPartyProduct == true ? tp.Code : mc.Code,
                                                              Side = pcp.IsThirdPartyProduct == true ? tp.LeftRightSide : mc.LeftRightSide,
                                                              Size = pcp.IsThirdPartyProduct == true ? tp.Size : mc.Size,
                                                              HCPCsString = dq.HCPCs ?? visionDB.ConcatHCPCS(pcp.PracticeCatalogProductID),
                                                              pcp.WholesaleCost,
                                                              pcp.BillingCharge,
                                                              pcp.BillingChargeCash,
                                                              //pcp.DMEDeposit,
                                                              pi.QuantityOnHandPerSystem,
                                                              ABN_Needed = ((from p in visionDB.ProductHCPCs
                                                                             join a in visionDB.ABN_HCPCs on p.HCPCS equals a.HCPCs
                                                                             where a.ABN_Needed == true
                                                                             && p.PracticeCatalogProductID == pcp.PracticeCatalogProductID
                                                                             select (bool?)a.ABN_Needed).Take(1).First()) ?? false,
                                                              dq.FitterID,
                                                              dq.ABNType,
                                                              dq.Mod1,
                                                              dq.Mod2,
                                                              dq.Mod3,
															  p.IsBregBilling
                                                          })
                                    });
             
                foreach (var customBrace in customBraces)
                {
                    int dispenseID = 0;
                    var groupingId = Guid.NewGuid().ToString();//New groupingId to be set for the dispensement

                    foreach (var dispenseQueue in customBrace.DispenseQueues)
                    {
                        bool? isCustomFitRemainNullIfSoAndValueOtherWise = dispenseQueue.IsCustomFit.HasValue ? dispenseQueue.IsCustomFit.Value : (bool?)null;

                        //Dispense here. Each item will be part of a custom brace order.
                        dispenseID = DispenseProducts(dispenseQueue.ModifiedUser.GetValueOrDefault(),
                                                                    dispenseQueue.PracticeLocationID,
                                                                    dispenseQueue.ProductInventoryID,
                                                                    dispenseQueue.PatientCode,
                                                                    dispenseQueue.PracticeCatalogProductID,
                                                                    dispenseQueue.PhysicianID.GetValueOrDefault(),
                                                                    dispenseQueue.ICD9_Code,
                                                                    dispenseQueue.LRModifier,
                                                                    dispenseQueue.IsMedicare,
                                                                    dispenseQueue.ABNForm,
                                                                    dispenseQueue.IsCashCollection ? dispenseQueue.BillingChargeCash : dispenseQueue.BillingCharge,
                                                                    dispenseQueue.QuantityToDispense,
                                                                    dispenseQueue.IsCashCollection,
                                                                    dispenseQueue.WholesaleCost,
                                                                    dispenseQueue.DMEDeposit,
                                                                    dispenseID,
                                                                    dispenseQueue.DispenseDate.HasValue ? dispenseQueue.DispenseDate.GetValueOrDefault() : DateTime.Now,
                                                                    createdByHandheld,
                                                                    DispenseBatchID,
                                                                    dispenseQueue.DispenseQueueID,
                                                                    dispenseQueue.PatientEmail,
                                                                    dispenseQueue.PatientFirstName,
                                                                    dispenseQueue.PatientLastName,
                                                                    dispenseQueue.PracticePayerID, 
                                                                    dispenseQueue.Note,
                                                                    isCustomFitRemainNullIfSoAndValueOtherWise, 
                                                                    dispenseQueue.FitterID,
                                                                    dispenseQueue.ABNType,
                                                                    dispenseQueue.Mod1,
                                                                    dispenseQueue.Mod2,
                                                                    dispenseQueue.Mod3, IsV5Dispensement);

						UpdateDispensedDispenseQueueRecord(visionDB, dispenseQueue.DispenseQueueID, groupingId);
                        DispensedProducts.Add(dispenseQueue.QuantityToDispense);
                    }
                    //use the dispense ID here to call the other dispense functions that need it
                    UpdateDispenseTotals(dispenseID);
                    UpdateCustomBrace(dispenseID, customBrace.BregVisionOrderID.GetValueOrDefault());

                }
                visionDB.SubmitChanges();

               // VisionDataContext.CloseConnection(visionDB);
            }
        }

        private static void UpdateDispensedDispenseQueueRecord(VisionDataContext visionDB, int dispenseQueueId, string groupingId)
        {
            //Save the IsDispensed status to the DispenseQueue.
            var dispensedDispenseQueue =
                visionDB.DispenseQueues.Single(d => d.DispenseQueueID == dispenseQueueId);

            dispensedDispenseQueue.IsDispensed = true;
            dispensedDispenseQueue.DispenseGroupingId = groupingId;

        }

        /// <summary>
        /// This function groups DispenseQueue items by PhysicianID and PatientCode(where patientcode has a value)
        /// and dispenses them in those groups
        /// </summary>
        /// <param name="PracticeLocationID"></param>
        public static void DispenseMultipleItems(int PracticeLocationID, int DispenseBatchID, List<int> SelectedProductInventoryIDs, List<int> DispensedProducts, List<int> selectedDispenseQueueIDs, bool IsV5Dispensement, out int DispenseID, bool createdByHandheld = false)
        {
            DispenseID = 0;
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var customBraces = (from dq_MultipleItems in visionDB.DispenseQueues
                                    where
                                        dq_MultipleItems.BregVisionOrderID == null
                                        && dq_MultipleItems.PracticeLocationID == PracticeLocationID

                                        && dq_MultipleItems.PatientCode != null && dq_MultipleItems.PatientCode != String.Empty
                                        && dq_MultipleItems.IsDispensed == false
                                        && dq_MultipleItems.IsActive == true
                                        && dq_MultipleItems.PhysicianID != null
                                        && selectedDispenseQueueIDs.Contains(dq_MultipleItems.DispenseQueueID)
                                    group dq_MultipleItems by 
                                    new
                                    {
                                        dq_MultipleItems.PhysicianID,
                                        dq_MultipleItems.PatientCode,
                                        dq_MultipleItems.FitterID,
                                        DispenseSignatureID = dq_MultipleItems.DispenseSignatureID ?? -1 // grouping by nullable type requires setting null values to a default value http://stackoverflow.com/questions/5782915/groupby-with-id-of-possible-null-object
                                    } into groupMultipleItems
                                    select new
                                    {
                                        groupMultipleItems.Key.PhysicianID,
                                        groupMultipleItems.Key.PatientCode,
                                        groupMultipleItems.Key.DispenseSignatureID,
                                        DispenseQueues = (from dq in visionDB.DispenseQueues
                                                          join pi in visionDB.ProductInventories on dq.ProductInventoryID equals pi.ProductInventoryID
                                                          join pcp in visionDB.PracticeCatalogProducts on pi.PracticeCatalogProductID equals pcp.PracticeCatalogProductID
														  join p in visionDB.Practices on pcp.PracticeID equals p.PracticeID
														  join pcsb in visionDB.PracticeCatalogSupplierBrands on pcp.PracticeCatalogSupplierBrandID equals pcsb.PracticeCatalogSupplierBrandID
														  join mcp in visionDB.MasterCatalogProducts on pcp.MasterCatalogProductID equals mcp.MasterCatalogProductID into mastercatalog
                                                          from mc in mastercatalog.DefaultIfEmpty()
                                                          join tpp in visionDB.ThirdPartyProducts on pcp.ThirdPartyProductID equals tpp.ThirdPartyProductID into thirdparty
                                                          from tp in thirdparty.DefaultIfEmpty()
                                                          where
                                                              dq.PracticeLocationID == PracticeLocationID
                                                              && pi.PracticeLocationID == PracticeLocationID
                                                              && dq.IsDispensed == false
                                                              && dq.IsActive == true
                                                              && dq.PhysicianID == groupMultipleItems.Key.PhysicianID
                                                              && dq.PatientCode == groupMultipleItems.Key.PatientCode
                                                              && (dq.DispenseSignatureID == groupMultipleItems.Key.DispenseSignatureID || (groupMultipleItems.Key.DispenseSignatureID == -1 && dq.DispenseSignatureID == null) ) //see above comment about nullable types and grouping
                                                              && selectedDispenseQueueIDs.Contains(dq.DispenseQueueID)
                                                          orderby dq.PhysicianID ascending, dq.PatientCode ascending
                                                          select new
                                                          {
                                                              dq.DispenseQueueID,
                                                              dq.PracticeLocationID,
                                                              dq.ProductInventoryID,
                                                              dq.PhysicianID,
                                                              dq.PatientCode,
                                                              dq.PatientEmail,
                                                              dq.PatientFirstName,
                                                              dq.PatientLastName,
                                                              dq.DispenseDate,
                                                              dq.IsMedicare,
                                                              dq.ABNForm,
                                                              dq.ModifiedUser,
                                                              dq.QuantityToDispense,
                                                              dq.IsCashCollection,
                                                              dq.IsDispensed,
                                                              dq.ICD9_Code,
                                                              dq.DMEDeposit,
                                                              dq.PracticePayerID,
                                                              dq.Note,
                                                              dq.IsCustomFit,
                                                              LRModifier = dq.Side,
                                                              pcp.PracticeCatalogProductID,
                                                              pcsb.SupplierShortName,
                                                              pcsb.BrandShortName,
                                                              ShortName = pcp.IsThirdPartyProduct == true ? tp.ShortName : mc.ShortName,
                                                              Code = pcp.IsThirdPartyProduct == true ? tp.Code : mc.Code,
                                                              Side = pcp.IsThirdPartyProduct == true ? tp.LeftRightSide : mc.LeftRightSide,
                                                              Size = pcp.IsThirdPartyProduct == true ? tp.Size : mc.Size,
                                                              HCPCsString = dq.HCPCs ?? visionDB.ConcatHCPCS(pcp.PracticeCatalogProductID),
                                                              pcp.WholesaleCost,
                                                              pcp.BillingCharge,
                                                              pcp.BillingChargeCash,
                                                              //pcp.DMEDeposit,
                                                              pi.QuantityOnHandPerSystem,
                                                              ABN_Needed = ((from p in visionDB.ProductHCPCs
                                                                             join a in visionDB.ABN_HCPCs on p.HCPCS equals a.HCPCs
                                                                             where a.ABN_Needed == true
                                                                             && p.PracticeCatalogProductID == pcp.PracticeCatalogProductID
                                                                             select (bool?)a.ABN_Needed).Take(1).First()) ?? false,
                                                              dq.FitterID,
                                                              dq.ABNType,
                                                              dq.Mod1,
                                                              dq.Mod2,
                                                              dq.Mod3,
															  p.IsBregBilling
                                                          })
                                    });

                //This loops through the group.  Each iteration has the same PhysicianID and PatientCode.  They belong in the same dispensement
                foreach (var customBrace in customBraces)
                {
                    int dispenseID = 0;
                    var groupingId = Guid.NewGuid().ToString();//New groupingId to be set for the dispensement
                    //This loop is the actual items.
                    foreach (var dispenseQueue in customBrace.DispenseQueues)
                    {
                         bool? isCustomFitRemainNullIfSoAndValueOtherWise = dispenseQueue.IsCustomFit.HasValue ? dispenseQueue.IsCustomFit.Value : (bool?)null;

						//Dispense here. Each item will be part of a custom brace order.
						dispenseID = DispenseProducts(dispenseQueue.ModifiedUser.GetValueOrDefault(),
																	dispenseQueue.PracticeLocationID,
																	dispenseQueue.ProductInventoryID,
																	dispenseQueue.PatientCode,
																	dispenseQueue.PracticeCatalogProductID,
																	dispenseQueue.PhysicianID.GetValueOrDefault(),
																	dispenseQueue.ICD9_Code,
																	dispenseQueue.LRModifier,
																	dispenseQueue.IsMedicare,
																	dispenseQueue.ABNForm,
																	dispenseQueue.IsCashCollection ? dispenseQueue.BillingChargeCash : dispenseQueue.BillingCharge,
																	dispenseQueue.QuantityToDispense,
																	dispenseQueue.IsCashCollection,
																	dispenseQueue.WholesaleCost,
																	dispenseQueue.DMEDeposit,
																	dispenseID,
																	dispenseQueue.DispenseDate.HasValue ? dispenseQueue.DispenseDate.GetValueOrDefault() : DateTime.Now,
																	createdByHandheld,
																	DispenseBatchID,
																	dispenseQueue.DispenseQueueID,
																	dispenseQueue.PatientEmail,
																	dispenseQueue.PatientFirstName,
																	dispenseQueue.PatientLastName,
																	dispenseQueue.PracticePayerID,
																	dispenseQueue.Note,
																	isCustomFitRemainNullIfSoAndValueOtherWise,
																	dispenseQueue.FitterID,
																	dispenseQueue.ABNType,
																	dispenseQueue.Mod1,
																	dispenseQueue.Mod2,
																	dispenseQueue.Mod3, IsV5Dispensement);

                        UpdateDispensedDispenseQueueRecord(visionDB,dispenseQueue.DispenseQueueID, groupingId);
                        DispensedProducts.Add(dispenseQueue.QuantityToDispense);
                    }
                    //use the dispense ID here to call the other dispense functions that need it
                    UpdateDispenseTotals(dispenseID);
                    DispenseID = dispenseID;
                }
                visionDB.SubmitChanges();

                //VisionDataContext.CloseConnection(visionDB);
            }
        }


        /// <summary>
        /// This function finds all dispense queue items where the patient code is empty or null, and dispenses them individually
        /// </summary>
        /// <param name="PracticeLocationID"></param>
        public static void DispenseSingleItems(int PracticeLocationID, int DispenseBatchID, List<int> SelectedProductInventoryIDs, List<int> DispensedProducts, List<int> selectedDispenseQueueIDs, bool IsV5Dispensement, bool createdByHandheld = false)
        {
            VisionDataContext visionDB = null;
            visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {

                var customBraces = (from dq in visionDB.DispenseQueues
                                    join pi in visionDB.ProductInventories on dq.ProductInventoryID equals pi.ProductInventoryID
                                    join pcp in visionDB.PracticeCatalogProducts on pi.PracticeCatalogProductID equals pcp.PracticeCatalogProductID
									join p in visionDB.Practices on pcp.PracticeID equals p.PracticeID
									join pcsb in visionDB.PracticeCatalogSupplierBrands on pcp.PracticeCatalogSupplierBrandID equals pcsb.PracticeCatalogSupplierBrandID
									join mcp in visionDB.MasterCatalogProducts on pcp.MasterCatalogProductID equals mcp.MasterCatalogProductID into mastercatalog
                                    from mc in mastercatalog.DefaultIfEmpty()
                                    join tpp in visionDB.ThirdPartyProducts on pcp.ThirdPartyProductID equals tpp.ThirdPartyProductID into thirdparty
                                    from tp in thirdparty.DefaultIfEmpty()
                                    where
                                        dq.PracticeLocationID == PracticeLocationID
                                        && pi.PracticeLocationID == PracticeLocationID
                                        && dq.IsDispensed == false
                                        && dq.IsActive == true
                                        &&    (
                                                    (dq.PhysicianID != null && (dq.PatientCode == null || dq.PatientCode == String.Empty))
                                                    ||
                                                    (dq.PhysicianID == null || dq.PhysicianID < 1)
                                                )
                                       && selectedDispenseQueueIDs.Contains(dq.DispenseQueueID)
                                    orderby dq.PhysicianID ascending, dq.PatientCode ascending
                                    select new
                                    {
                                        dq.DispenseQueueID,
                                        dq.PracticeLocationID,
                                        dq.ProductInventoryID,
                                        dq.PhysicianID,
                                        dq.PatientCode,
                                        dq.PatientEmail,
                                        dq.PatientFirstName,
                                        dq.PatientLastName,
                                        dq.DispenseDate,
                                        dq.IsMedicare,
                                        dq.ABNForm,
                                        dq.ModifiedUser,
                                        dq.QuantityToDispense,
                                        dq.IsCashCollection,
                                        dq.IsDispensed,
                                        dq.ICD9_Code,
                                        dq.DMEDeposit,
                                        dq.PracticePayerID,
                                        dq.Note,
                                        dq.IsCustomFit,
                                        LRModifier = dq.Side,
                                        pcp.PracticeCatalogProductID,
                                        pcsb.SupplierShortName,
                                        pcsb.BrandShortName,
                                        ShortName = pcp.IsThirdPartyProduct == true ? tp.ShortName : mc.ShortName,
                                        Code = pcp.IsThirdPartyProduct == true ? tp.Code : mc.Code,
                                        Side = pcp.IsThirdPartyProduct == true ? tp.LeftRightSide : mc.LeftRightSide,
                                        Size = pcp.IsThirdPartyProduct == true ? tp.Size : mc.Size,
                                        HCPCsString = dq.HCPCs ?? visionDB.ConcatHCPCS(pcp.PracticeCatalogProductID),
                                        pcp.WholesaleCost,
                                        pcp.BillingCharge,
                                        pcp.BillingChargeCash,
                                        //pcp.DMEDeposit,
                                        pi.QuantityOnHandPerSystem,
                                        ABN_Needed = ((from p in visionDB.ProductHCPCs
                                                       join a in visionDB.ABN_HCPCs on p.HCPCS equals a.HCPCs
                                                       where a.ABN_Needed == true
                                                       && p.PracticeCatalogProductID == pcp.PracticeCatalogProductID
                                                       select (bool?)a.ABN_Needed).Take(1).First()) ?? false,
                                        dq.FitterID,
                                        dq.ABNType,
                                        dq.Mod1,
                                        dq.Mod2,
                                        dq.Mod3,
										p.IsBregBilling
                                    });

              
                //This loop is the actual items.
                foreach (var dispenseQueue in customBraces)
                {
                    var groupingId = Guid.NewGuid().ToString();//New groupingId to be set for the dispensement
                    int dispenseID = 0;

                    //Dispense here. Each item will be dispensed separately.
                    bool? isCustomFitRemainNullIfSoAndValueOtherWise = dispenseQueue.IsCustomFit.HasValue ? dispenseQueue.IsCustomFit.Value:(bool?) null;

                    dispenseID = DispenseProducts(dispenseQueue.ModifiedUser.GetValueOrDefault(),
                                                                dispenseQueue.PracticeLocationID,
                                                                dispenseQueue.ProductInventoryID,
                                                                dispenseQueue.PatientCode,
                                                                dispenseQueue.PracticeCatalogProductID,
                                                                dispenseQueue.PhysicianID.GetValueOrDefault(),
                                                                dispenseQueue.ICD9_Code,
                                                                dispenseQueue.LRModifier,
                                                                dispenseQueue.IsMedicare,
                                                                dispenseQueue.ABNForm,
                                                                dispenseQueue.IsCashCollection ? dispenseQueue.BillingChargeCash : dispenseQueue.BillingCharge,
                                                                dispenseQueue.QuantityToDispense,
                                                                dispenseQueue.IsCashCollection,
                                                                dispenseQueue.WholesaleCost,
                                                                dispenseQueue.DMEDeposit,
                                                                dispenseID,
                                                                dispenseQueue.DispenseDate.HasValue ? dispenseQueue.DispenseDate.GetValueOrDefault() : DateTime.Now,
                                                                createdByHandheld,
                                                                DispenseBatchID,
                                                                dispenseQueue.DispenseQueueID,
                                                                dispenseQueue.PatientEmail,
                                                                dispenseQueue.PatientFirstName,
                                                                dispenseQueue.PatientLastName,
                                                                dispenseQueue.PracticePayerID, 
                                                                dispenseQueue.Note,
                                                                isCustomFitRemainNullIfSoAndValueOtherWise,
                                                                dispenseQueue.FitterID,
                                                                dispenseQueue.ABNType,
                                                                dispenseQueue.Mod1,
                                                                dispenseQueue.Mod2,
                                                                dispenseQueue.Mod3, IsV5Dispensement);


					UpdateDispensedDispenseQueueRecord(visionDB, dispenseQueue.DispenseQueueID, groupingId);

                    DispensedProducts.Add(dispenseQueue.QuantityToDispense);

                    //use the dispense ID here to call the other dispense functions that need it
                    UpdateDispenseTotals(dispenseID);

                }
                visionDB.SubmitChanges();

                //VisionDataContext.CloseConnection(visionDB);
            }
        }

        public static int DispenseProducts(int UserID, int practiceLocationID, int ProductInventoryID, string PatientCode,
			int PracticeCatalogProductID, int? PhysicianID, string ICD9Code, string LRModifier, bool IsMedicare, bool ABNForm,
			decimal ActualChargeBilled, int Quantity, bool IsCashCollection, decimal WholesaleCost, decimal DMEDeposit,
			int DispenseID, DateTime DispenseDate, bool CreatedWithHandheld, int DispenseBatchID, int DispenseQueueID,
			bool isCustomFit, string ABNType)
        {
            return DispenseProducts(UserID, practiceLocationID, ProductInventoryID, PatientCode, PracticeCatalogProductID,
				PhysicianID, ICD9Code, LRModifier, IsMedicare, ABNForm, ActualChargeBilled, Quantity, IsCashCollection,
				WholesaleCost, DMEDeposit, DispenseID, DispenseDate, CreatedWithHandheld, DispenseBatchID, DispenseQueueID,
				"", "","",null, "",isCustomFit, null, ABNType, "", "", "", false);
        }


        public static int DispenseProducts(int UserID, int practiceLocationID, int ProductInventoryID, string PatientCode,
			int PracticeCatalogProductID, int? PhysicianID, string ICD9Code, string LRModifier, bool IsMedicare, bool IsAbnForm,
			decimal ActualChargeBilled, int Quantity, bool IsCashCollection, decimal WholesaleCost, decimal DMEDeposit, int DispenseID,
			DateTime DispenseDate, bool CreatedWithHandheld, int DispenseBatchID, int DispenseQueueID, string patientEmail,
			string patientFirstName, string patientLastName, int? PracticePayerID, string note, bool? isCustomFit, int? fitterID,
			string ABNType, string Mod1, string Mod2, string Mod3, bool IsV5Dispensement)
        {

            try
            {
                int rowsAffected = 0;
                //Adding logic to ensure capability with old and new version of visison app.
                ABNType = IsAbnForm == true ? "Medicare" : ABNType;
                IsAbnForm = ABNType == "Medicare" ? true : false;
                decimal actualChargeBilled;
                try
                {
                    actualChargeBilled = ApplyOffTheShelfToActualChargeBilled(ActualChargeBilled, IsCashCollection,
                        isCustomFit, fitterID, DispenseQueueID);
                }
                catch (Exception e)
                {
                    //TODO:log
                    actualChargeBilled = ActualChargeBilled;

                }

				ClassLibrary.DAL.Practice practice = DAL.Practice.Practice.GetPracticeByPracticeLocationId(practiceLocationID);

                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");

                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_DispenseProductsNew");

                db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
                db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
                db.AddInParameter(dbCommand, "ProductInventoryID", DbType.Int32, ProductInventoryID);
                db.AddInParameter(dbCommand, "PatientCode", DbType.String, PatientCode);
                db.AddInParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, PracticeCatalogProductID);

                db.AddInParameter(dbCommand, "PhysicianID", DbType.Int32, PhysicianID == null? 0 : PhysicianID);
                db.AddInParameter(dbCommand, "ICD9Code", DbType.String, ICD9Code);
                db.AddInParameter(dbCommand, "LRModifier", DbType.String, LRModifier);
                db.AddInParameter(dbCommand, "IsMedicare", DbType.Boolean, IsMedicare);
                db.AddInParameter(dbCommand, "ABNForm", DbType.Boolean, IsAbnForm);
                db.AddInParameter(dbCommand, "ABNType", DbType.String, ABNType);
                db.AddInParameter(dbCommand, "ActualChargeBilled", DbType.Decimal, actualChargeBilled);
                db.AddInParameter(dbCommand, "Quantity", DbType.Int32, Quantity);
                db.AddInParameter(dbCommand, "IsCashCollection", DbType.Boolean, IsCashCollection);
                db.AddInParameter(dbCommand, "WholesaleCost", DbType.Decimal, WholesaleCost);
                db.AddInParameter(dbCommand, "DMEDeposit", DbType.Decimal, DMEDeposit);
                db.AddInParameter(dbCommand, "DispenseIDIn", DbType.Decimal, DispenseID);

                //Ashwin - This flag is added to verify whether it is a non legacy dispensement and calculate date accordingly
                if (IsV5Dispensement)
                    db.AddInParameter(dbCommand, "DispenseDate", DbType.DateTime, DispenseDate);
                else
                    db.AddInParameter(dbCommand, "DispenseDate", DbType.DateTime, DispenseDate.Date + DateTime.Now.TimeOfDay);

                db.AddInParameter(dbCommand, "CreatedWithHandheld", DbType.Boolean, CreatedWithHandheld);//piggy backing off this we will clear the grouping id if this was not dispensed by the mobile application
                db.AddInParameter(dbCommand, "DispenseBatchID", DbType.Int32, DispenseBatchID);
                db.AddInParameter(dbCommand, "DispenseQueueID", DbType.Int32, DispenseQueueID);
                db.AddInParameter(dbCommand, "PatientEmail", DbType.String, patientEmail);
                db.AddInParameter(dbCommand, "PatientFirstName", DbType.String, patientFirstName);
                db.AddInParameter(dbCommand, "PatientLastName", DbType.String, patientLastName);
                db.AddInParameter(dbCommand, "PracticePayerID", DbType.Int32, PracticePayerID);
                db.AddInParameter(dbCommand, "Note", DbType.String, note);
                db.AddInParameter(dbCommand, "IsCustomfit",DbType.Boolean, isCustomFit);
                db.AddInParameter(dbCommand, "FitterID", DbType.Int32, fitterID);
                db.AddInParameter(dbCommand, "Mod1", DbType.String, Mod1);
                db.AddInParameter(dbCommand, "Mod2", DbType.String, Mod2);
                db.AddInParameter(dbCommand, "Mod3", DbType.String, Mod3);

                db.AddOutParameter(dbCommand, "DispenseID", DbType.Int32, 4);

				if (versionConfig != null && !String.IsNullOrEmpty(versionConfig))
					db.AddInParameter(dbCommand, "@DispenseDocumentVersion", DbType.Int32, Convert.ToInt32(versionConfig)); // Version for DispenseDocumentVersion as in Current Version 
				else
					db.AddInParameter(dbCommand, "@DispenseDocumentVersion", DbType.Int32, 2); // 2 for DispenseDocumentVersion as in Current Version 

				XElement dispenseDocumentFeaturesFlags = new XElement("Dispensement",
					new XElement("IsBregBilling", practice.IsBregBilling)
				);
				db.AddInParameter(dbCommand, "@DispenseDocumentFeaturesFlags", DbType.Xml, dispenseDocumentFeaturesFlags.ToString());

				rowsAffected = db.ExecuteNonQuery(dbCommand);
                Int32 intStatus = Convert.ToInt32(db.GetParameterValue(dbCommand, "DispenseID"));

               
                return intStatus;
            }
            #region Exception Handler
            catch (Exception ex)
            {


                throw ex;

            }
            #endregion
        }

      
        private static decimal ApplyOffTheShelfToActualChargeBilled(decimal actualChargeBilled, bool isCashCollection, bool? isCustomFit, int? fitterId, int dispenseQueueId)
        {
            if (isCashCollection)
            {
                return actualChargeBilled;
            }

            if (fitterId != null)
            {
                return actualChargeBilled;
            }

            using (var db = VisionDataContext.GetVisionDataContext())
            {
                var dispenseQueueCustomFitDetails =
                    db.DispenseQueues.Where(dq => dq.DispenseQueueID == dispenseQueueId).
                        Join(db.ProductInventories, dqj => dqj.ProductInventoryID,
                            inventory => inventory.ProductInventoryID, (dq, inventory) => new {dq, inventory})
                        .Select(dqj => new
                        {
                            isCustomBrace =
                                dqj.inventory.PracticeCatalogProduct.MasterCatalogProduct != null &&
                                dqj.inventory.PracticeCatalogProduct.MasterCatalogProduct.IsCustomBrace,
                            isCustomFabricated = dqj.dq.IsCustomFabricated.HasValue && dqj.dq.IsCustomFabricated.Value,
                            isCustomFit = dqj.dq.IsCustomFit.HasValue && dqj.dq.IsCustomFit.Value,
                            dqj.inventory.PracticeCatalogProduct.IsSplitCode,
                            dqj.inventory.PracticeCatalogProduct.IsAlwaysOffTheShelf,
                            offTheShelfBillingCharge =
                                dqj.inventory.PracticeCatalogProduct.OffTheShelfBillingCharges != null
                                    ? (decimal?)
                                        dqj.inventory.PracticeCatalogProduct.OffTheShelfBillingCharges.First()
                                            .BillingCharge
                                    : null
                        }).First();

                if (dispenseQueueCustomFitDetails.isCustomBrace)
                {
                    return actualChargeBilled;
                }

                if (dispenseQueueCustomFitDetails.isCustomFabricated)
                {
                    return actualChargeBilled;
                }

                if (dispenseQueueCustomFitDetails.IsSplitCode && dispenseQueueCustomFitDetails.isCustomFit)
                {
                    return actualChargeBilled;
                }

                if (dispenseQueueCustomFitDetails.IsSplitCode && !dispenseQueueCustomFitDetails.isCustomFit)
                {
                    return dispenseQueueCustomFitDetails.offTheShelfBillingCharge.Value;
                }

                if (dispenseQueueCustomFitDetails.IsAlwaysOffTheShelf)
                {
                    return dispenseQueueCustomFitDetails.offTheShelfBillingCharge.Value;
                }

                return actualChargeBilled;


            }

        }

        public static void UpdateDispenseTotals(int DispenseID)
        {
            try
            {
                int rowsAffected = 0;

                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_DispenseProducts_UpdateTotals");

                db.AddInParameter(dbCommand, "DispenseID", DbType.Int32, DispenseID);
                rowsAffected = db.ExecuteNonQuery(dbCommand);
            }
            #region Exception Handler
            catch (Exception ex)
            {


                throw ex;

            }
            #endregion
        }

        public static void UpdateCustomBrace(int DispenseID, int BregVisionOrderID)
        {
            try
            {
                int rowsAffected = 0;

                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_DispenseProducts_UpdateCustomBrace");

                db.AddInParameter(dbCommand, "DispenseID", DbType.Int32, DispenseID);
                db.AddInParameter(dbCommand, "BregVisionOrderID", DbType.Int32, BregVisionOrderID);
                rowsAffected = db.ExecuteNonQuery(dbCommand);
            }
            #region Exception Handler
            catch (Exception ex)
            {


                throw ex;

            }
            #endregion
        }

        public static DataSet GetLastItemsDispensed(int practiceID, int practiceLocationID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetLast10ItemsDispensed");

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);

            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
    }
}
