using System;
using System.Collections;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using BLL;
using System.Linq;
using ClassLibrary.DAL;

/*  Make sure the above is at the top of the page. */


namespace DAL.PracticeLocation
{
	/// <summary>
	/// Summary description for PracticeLocation_ShippingAddress
	/// </summary>
	public class ShippingAddress
	{
		public ShippingAddress()
		{
			//
			// Add constructor logic here
			//
		}

		//public void Select(int practiceLocationID, out int addressID, out string attentionOf
		//        , out string addressLine1, out string addressLine2, out string city
		//        , out string state, out string zipCode, out string zipCodePlus4)

		//Done:  Later Change this to pass in a BLL object!!!
		//Done:  Pass in the BLL Shipping Address object.
		public BLL.PracticeLocation.ShippingAddress Select( BLL.PracticeLocation.ShippingAddress bllShippingAddress )
		{
			int rowsAffected = 0;
			int practiceLocationID = bllShippingAddress.PracticeLocationID;

			Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
			DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocationShippingAddress_Select_One_ByParams");
		    
			db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, bllShippingAddress.PracticeLocationID );
			db.AddOutParameter(dbCommand, "AddressID", DbType.Int32, 4);
			db.AddOutParameter(dbCommand, "AttentionOf", DbType.String, 50);
			db.AddOutParameter(dbCommand, "AddressLine1", DbType.String, 50);
			db.AddOutParameter(dbCommand, "AddressLine2", DbType.String, 50);
			db.AddOutParameter(dbCommand, "City", DbType.String, 50);
			db.AddOutParameter(dbCommand, "State", DbType.String, 2);
			db.AddOutParameter(dbCommand, "ZipCode", DbType.String, 5);
			db.AddOutParameter(dbCommand, "ZipCodePlus4", DbType.String, 4);
		    
			rowsAffected = db.ExecuteNonQuery(dbCommand);

			bllShippingAddress.AddressID = ( ( db.GetParameterValue( dbCommand, "AddressID" ) == DBNull.Value ) ? -1 : Convert.ToInt32( db.GetParameterValue( dbCommand, "AddressID" ) ) );
			bllShippingAddress.AttentionOf = ( ( db.GetParameterValue( dbCommand, "AttentionOf" ) == DBNull.Value ) ? "" : Convert.ToString( db.GetParameterValue( dbCommand, "AttentionOf" ) ) );
			bllShippingAddress.Address.AddressLine1 = ( ( db.GetParameterValue( dbCommand, "AddressLine1" ) == DBNull.Value ) ? "" : Convert.ToString( db.GetParameterValue( dbCommand, "AddressLine1" ) ) );
			bllShippingAddress.Address.AddressLine2 = ( ( db.GetParameterValue( dbCommand, "AddressLine2" ) == DBNull.Value ) ? "" : Convert.ToString( db.GetParameterValue( dbCommand, "AddressLine2" ) ) );
			bllShippingAddress.Address.City = ( ( db.GetParameterValue( dbCommand, "City" ) == DBNull.Value ) ? "" : Convert.ToString( db.GetParameterValue( dbCommand, "City" ) ) );
			bllShippingAddress.Address.State = ( ( db.GetParameterValue( dbCommand, "State" ) == DBNull.Value ) ? "" : Convert.ToString( db.GetParameterValue( dbCommand, "State" ) ) );
			bllShippingAddress.Address.ZipCode = ( ( db.GetParameterValue( dbCommand, "ZipCode" ) == DBNull.Value ) ? "" : Convert.ToString( db.GetParameterValue( dbCommand, "ZipCode" ) ) );
			bllShippingAddress.Address.ZipCodePlus4 = ( ( db.GetParameterValue( dbCommand, "ZipCodePlus4" ) == DBNull.Value ) ? "" : Convert.ToString( db.GetParameterValue( dbCommand, "ZipCodePlus4" ) ) );
		
			////int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
			return bllShippingAddress;
		}

        public usp_GetShippingAddressByPracticeLocationResult Select(int practiceLocationId)
        {
            
           VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            using(visionDB){
                //return visionDB.PracticeLocationShippingAddresses.FirstOrDefault(plsa => plsa.PracticeLocationID==practiceLocationId)
                return visionDB.usp_GetShippingAddressByPracticeLocation(practiceLocationId).FirstOrDefault();
            }

        }


		//public void UpdateInsert( int practiceLocationID, out int addressID
		//            , string attentionOf, string addressLine1, string addressLine2
		//            , string city, string state, string zipCode, string zipCodePlus4, int userID )
		public void UpdateInsert( BLL.PracticeLocation.ShippingAddress bllShippingAddress )
		{
			int rowsAffected = 0;

			Database db = DatabaseFactory.CreateDatabase( "BregVisionConnectionString" );
			DbCommand dbCommand = db.GetStoredProcCommand( "dbo.usp_PracticeLocationShippingAddress_UpdateInsert" );

			db.AddInParameter( dbCommand, "PracticeLocationID", DbType.Int32, bllShippingAddress.PracticeLocationID );
			db.AddOutParameter( dbCommand, "AddressID", DbType.Int32, bllShippingAddress.AddressID );
			db.AddInParameter( dbCommand, "AttentionOf", DbType.String, bllShippingAddress.AttentionOf );
			db.AddInParameter( dbCommand, "AddressLine1", DbType.String, bllShippingAddress.Address.AddressLine1 );
			db.AddInParameter( dbCommand, "AddressLine2", DbType.String, bllShippingAddress.Address.AddressLine2 );
			db.AddInParameter( dbCommand, "City", DbType.String, bllShippingAddress.Address.City );
			db.AddInParameter( dbCommand, "State", DbType.String, bllShippingAddress.Address.State );
			db.AddInParameter( dbCommand, "ZipCode", DbType.String, bllShippingAddress.Address.ZipCode );
			db.AddInParameter( dbCommand, "ZipCodePlus4", DbType.String, bllShippingAddress.Address.ZipCodePlus4  );
			db.AddInParameter( dbCommand, "UserID", DbType.Int32, bllShippingAddress.UserID );

			rowsAffected = db.ExecuteNonQuery( dbCommand );

			//  addressID = Convert.ToInt32( db.GetParameterValue( dbCommand, "AddressID" ) );
			bllShippingAddress.AddressID = ( ( db.GetParameterValue( dbCommand, "AddressID" ) == DBNull.Value ) ? -1 : Convert.ToInt32( db.GetParameterValue( dbCommand, "AddressID" ) ) );
						
			//int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
		}
	}
}