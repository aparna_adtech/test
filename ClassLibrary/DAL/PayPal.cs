﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClassLibrary.DAL;
using ClassLibrary.DAL.Helpers;
namespace DAL
{
    public class PayPal
    {

        public static void LogPostInfo(string txType, string itemNumber, string request)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                PayPalPostLog payPalPostLog = new PayPalPostLog();
                payPalPostLog.Transaction_Type = txType;
                payPalPostLog.Item_Number = itemNumber;
                payPalPostLog.Request = request;

                payPalPostLog.CreatedUserID = 1;
                payPalPostLog.CreatedDate = DateTime.Now;
                payPalPostLog.ModifiedUserID = 1;
                payPalPostLog.ModifiedDate = DateTime.Now;
                payPalPostLog.IsActive = true;

                visionDB.PayPalPostLogs.InsertOnSubmit(payPalPostLog);
                visionDB.SubmitChanges();
            }
        }
    }
}
