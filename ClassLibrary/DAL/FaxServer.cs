﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary.DAL
{
    public partial class FaxServer
    {
        

        public static bool GetFaxServerStatus()
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var query = from fs in visionDB.FaxServers
                            select fs;
                FaxServer faxServer = query.First<FaxServer>();

                //VisionDataContext.CloseConnection(visionDB);

                return faxServer.FaxEnabled;
            }
        }

        public static bool EnableFaxServer(string Password)
        {
            
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var query = from fs in visionDB.FaxServers
                            select fs;
                FaxServer faxServer = query.First<FaxServer>();

                if (Password == faxServer.Password)
                {
                    faxServer.FaxEnabled = true;
                    visionDB.SubmitChanges();
                    return true;
                }

                //VisionDataContext.CloseConnection(visionDB);
                return false;
            }
        }

        public static bool DisableFaxServer(string Password)
        {

            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var query = from fs in visionDB.FaxServers
                            select fs;
                FaxServer faxServer = query.First<FaxServer>();


                if (Password == faxServer.Password)
                {
                    faxServer.FaxEnabled = false;
                    visionDB.SubmitChanges();
                    VisionDataContext.CloseConnection(visionDB);
                    return true;
                }

                //VisionDataContext.CloseConnection(visionDB);
                return false;
            }
            
        }

    }
}
