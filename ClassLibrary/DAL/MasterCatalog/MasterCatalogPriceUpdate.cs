﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary.DAL.MasterCatalog
{
    public class  MasterCatalogPriceUpdate
    {   
        public MasterCatalogPriceUpdate(string code, System.Nullable<decimal> wholesaleListCost, string packaging) 
        {
            this.Code = code;
            this.WholesaleListCost = wholesaleListCost;
            this.Packaging = packaging;
        }
        public string Code { get; set; }
        public string Packaging { get; set; }
        public System.Nullable<decimal> WholesaleListCost { get; set; }
    }
}
