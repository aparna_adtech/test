using System.Collections.Generic;

namespace ClassLibrary.DAL.Helpers
{
    public static class ModifiersHelper
    {
        public static IList<string> GetModifiersList()
        {
            List<string> modifiers = new List<string>()
            {
                "CG",
                "EY",
                "GA",
                "GX",
                "GY",
                "GZ",
                "KV",
                "KX",
                "NU",
                "RA",
                "RB",
                "RR"
            };

            return modifiers;
        }
    }
}