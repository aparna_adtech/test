﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
namespace ClassLibrary.DAL.Helpers
{
    class DataTableHelper
    {
        public static void CreateColumn(string columnName, string columnCaption, string dataType, DataTable table)
        {
            DataColumn colPage = new DataColumn(columnName);
            colPage.DataType = System.Type.GetType(dataType);
            colPage.Caption = columnCaption;
            table.Columns.Add(colPage);

        }
    }
}
