﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary.DAL.Helpers
{
    class BillingChargeHelper
    {
        public static decimal? GetAllowableAmount(VisionDataContext db, bool? isOffTheShelf, int? practicePayerId, int practiceCatalogProductId, int practiceLocationId)
        {
            if (practicePayerId == null)
            {
                return null;
            }

            var allowableList = GetAllowableAmounts(db, isOffTheShelf, practicePayerId.Value, practiceCatalogProductId,practiceLocationId);
            return GetAllowableAmount(allowableList);
        }

        public static decimal? GetAllowableAmount(List<decimal?> allowableList)
        {
            decimal? allowableTotal = null;
            if (allowableList.Count > 0)
            {
                foreach (var allowable in allowableList)
                {
                    if (allowable != null && allowable > 0)
                    {
                        if (!allowableTotal.HasValue)
                        {
                            allowableTotal = 0;
                        }
                        allowableTotal += allowable;
                    }
                }
            }
            return allowableTotal;
        }

        public static List<decimal?> GetAllowableAmounts(VisionDataContext db, bool? isOffTheShelf, int practicePayerId, int practiceCatalogProductId, int practiceLocationId)
        {

            var result = from pinv in db.ProductInventories
                         join pcp in db.PracticeCatalogProducts on pinv.PracticeCatalogProductID equals pcp.PracticeCatalogProductID
                         join ph in db.ProductHCPCs on pcp.PracticeCatalogProductID equals ph.PracticeCatalogProductID
                         join mh in db.HCPCs on ph.HCPCS.ToUpper().Substring(0, 5) equals mh.Code.ToUpper()
                         join pp in db.PracticePayers on pcp.PracticeID equals pp.PracticeID
                         join ppa in db.PracticePayerAllowables on mh.HCPCSID equals ppa.HcpcsID into mhppa
                         from mhppai in mhppa.Where(ppa => pp.PracticePayerID == ppa.PracticePayerID)
                         where mhppai.AllowableAmount != null
                               && mhppai.AllowableAmount > 0
                               && ph.IsOffTheShelf == isOffTheShelf
                               && pp.PracticePayerID == practicePayerId
                               && pcp.PracticeCatalogProductID == practiceCatalogProductId
                               && pinv.PracticeLocationID == practiceLocationId
                         select mhppai.AllowableAmount;
            return result.ToList();
        }
    }
}
