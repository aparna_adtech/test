﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace ClassLibrary.DAL.Helpers
{
    public class HcpcsHelper
    {
        public static List<string> GetCustomFabricatedCodes(VisionDataContext db)
        {
            var codes = from hcps in db.HCPCs
                where hcps.IsCustomFabricated
                select hcps.Code;
            return codes.ToList();
        }

        public static bool IsHcpcCustomFabricated(string productHcpc,List<string> customManufacturedCodes)
        {
            if (string.IsNullOrEmpty(productHcpc))
            {
                return false;
            }

            var hcpcCodes = productHcpc.Split(Convert.ToChar(",")).Select(k => k.Trim()).ToArray();
            foreach (var hcpcCode in hcpcCodes)
            {
                if (hcpcCode.Length < 5)
                {
                    continue;
                }

                if (customManufacturedCodes.Contains(hcpcCode.Substring(0, 5)))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsHcpcCustomFabricated(string productHcpc, VisionDataContext db)
        {
            return IsHcpcCustomFabricated(productHcpc, GetCustomFabricatedCodes(db));
        }
    }
}
