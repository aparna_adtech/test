﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Script.Serialization;





namespace ClassLibrary.DAL.Helpers
{
    public class JSONSerializer
    {
        public JSONSerializer()            
        { }

        public static string Serialize(object value)
        {
            JavaScriptSerializer ser = new JavaScriptSerializer();

            List<JavaScriptConverter> converters = new List<JavaScriptConverter>();

            if (value != null)
            {
                Type type = value.GetType();
                if (type == typeof(DataTable) || type == typeof(DataRow) || type == typeof(DataSet))
                {
                    converters.Add(new WebExtensionsDataRowConverter());
                    converters.Add(new WebExtensionsDataTableConverter());
                    converters.Add(new WebExtensionsDataSetConverter());
                }

                if (converters.Count > 0)
                    ser.RegisterConverters(converters);
            }
         
            return ser.Serialize(value);
        }
        public object Deserialize(string jsonText, Type valueType)
        {
            // *** Have to use Reflection with a 'dynamic' non constant type instance
            JavaScriptSerializer ser = new JavaScriptSerializer();


            object result = ser.GetType()
                               .GetMethod("Deserialize")
                               .MakeGenericMethod(valueType)
                              .Invoke(ser, new object[1] { jsonText });
            return result;
        }
    }
}
