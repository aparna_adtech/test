﻿using System;
using System.ServiceModel;
using BregWcfService.DataContracts;
using BregWcfService.HeaderClasses;
using System.Runtime.Serialization;
using System.Web.Security;
using BregWcfService.ErrorHandlerClasses;
using ClassLibrary.DAL;
using CustomFitProcedure = BregWcfService.DataContracts.CustomFitProcedure;

namespace BregWcfService
{

    [DataContract]
    class ExceptionFault
    {
        [DataMember]
        public System.Exception Exception { get; set; }

        public ExceptionFault(System.Exception e)
        {
            Exception = e;
        }
    }
   
    [ServiceContract, CallContext]
    public interface IVisionService
    {
        [FaultContract(typeof (ExceptionFault))]
        [OperationContract]
        bool Heartbeat();

        [FaultContract(typeof(ExceptionFault))]
        [OperationContract]
        EMRIntegrationReference.schedule_item[] GetPatients(int practiceId, int practiceLocationId, DateTime utcStartDate, DateTime? utcEndDate, string userName, string password, string deviceID, string lastLocation, int skip, int take);

        [FaultContract(typeof(ExceptionFault))]
        [OperationContract]
        ScheduleItem[] GetPatientsV2(int practiceId, int practiceLocationId, DateTime utcStartDate, DateTime? utcEndDate, string userName, string password, string deviceID, string lastLocation, int skip, int take);

        [FaultContract(typeof(ExceptionFault))]
        [OperationContract]
        EMRIntegrationReference.schedule_item[] GetScheduledPatientsRange(int practiceId, int practiceLocationId, DateTime utcStartDate, DateTime? utcEndDate, string userName, string password, string deviceID, string lastLocation, int skip, int take);

        [FaultContract(typeof(ExceptionFault))]
        [OperationContract]
        EMRIntegrationReference.schedule_item[] GetScheduledPatientsAll(int practiceId, int practiceLocationId, DateTime utcStartDate, DateTime? utcEndDate, string userName, string password, string deviceID, string lastLocation);

        [FaultContract(typeof(ExceptionFault))]
        [OperationContract]
        UserContext Login(string userName, string password, string deviceID, string lastLocation);

         [FaultContract(typeof(ExceptionFault))]
        [OperationContract]
        bool ValidateLogin(string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         LandingData GetLandingData(int practiceID, int practiceLocationID, int skip, int takke, string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         LandingData GetLandingData1(out int count, int practiceID, int practiceLocationID, int skip, int takke, string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         LandingData GetLandingDataAll(int practiceID, int practiceLocationID, string userName, string password, string deviceID, string lastLocation);

        [OperationContract, FaultContract(typeof (ExceptionFault))]
        ProductUpdateWrapper GetInventoryLandingData(int practiceId, int practiceLocationID, string userName, string password, string deviceID, string lastLocation);

        [OperationContract, FaultContract(typeof(ExceptionFault))]
        string[] GetModifiers(string userName, string password, string deviceID, string lastLocation);

        [OperationContract, FaultContract(typeof(ExceptionFault))]
        InventoryOrderStatusResult[] GetInventoryById(int practiceLocationID, int[] productInventoryID, string userName, string password, string deviceID, string lastLocation);

        [OperationContract, FaultContract(typeof (ExceptionFault))]
        ProductUpdateWrapper GetProductUpdates(int practiceLocationId, int practiceId, int serialNumber, string userName,
            string password, string deviceID, string lastLocation);

        [OperationContract, FaultContract(typeof(ExceptionFault))]
         BregWcfService.Practice[] GetAccessiblePractices(out int count, int skip, int take, string userName, string password, string deviceID, string lastLocation);
        

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         bool AddDevice(string deviceIdentifier, string lastLocation, int? practiceID, int? practiceLocationID, int userID, string userName, string password);


         [OperationContract, FaultContract(typeof(ExceptionFault))]
         int AddToShoppingCart(int practiceLocationID, int practiceCatalogProductID, int UserID, int Quantity, string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         void AddToShoppingCartMultiple(int practiceLocationID, int UserID, ShoppingCartItemCompact[] shoppingCartItems, string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         usp_GetShoppingCartResult[] GetShoppingCart(int practiceLocationID, string userName, string password, string deviceID, string lastLocation, int skip, int take);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         usp_GetShoppingCartResult[] GetShoppingCart1(out int count, int practiceLocationID, string userName, string password, string deviceID, string lastLocation, int skip, int take);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         void UpdateShoppingCart(int practiceLocationID, int shoppingCartItemId, int quantity, string userName, string password, string deviceID, string lastLocation);

        [OperationContract, FaultContract(typeof (ExceptionFault))]
        ServiceResult UpdateIsHcpSignatureRequired(int practiceLocationID, bool isHcpRequired, string userName, string password, string deviceId, string lastLocation);

        [OperationContract, FaultContract(typeof(ExceptionFault))]
         void DeleteAllFromCart(int practiceLocationID, string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         void DeleteFromCart(int practiceLocationID, int shoppingCartItemId, string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         InventoryItem[] SearchForItemsInInventory(int practiceLocationID, string searchCriteria, string searchText, int skip, int take, string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         InventoryItem[] SearchForItemsInInventory1(out int count, int practiceLocationID, string searchCriteria, string searchText, int skip, int take, string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         InventoryCriteria[] GetInventorySearchCodes(string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         ABNReason[] GetAbnReasons(string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         DispenseItem[] SearchForDispenseItems(int practiceID, int practiceLocationID, string searchCriteria, string searchText, int skip, int take, string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         DispenseItem[] SearchForDispenseItems1(out int count, int practiceID, int practiceLocationID, string searchCriteria, string searchText, int skip, int take, string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         DispensementCustomBrace[] GetCustomBraceOrdersforDispense(out int count, int practiceLocationID, string searchCriteria, string searchText, int skip, int take, string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         InventoryCriteria[] GetDispenseSearchCodes(string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         InventoryCriteria[] GetDispenseCustomBraceSearchCodes(string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         InventoryCriteria[] GetDispenseSideCodes(string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         bool IsDispensementExist(string userName, string password, string deviceID, string lastLocation, string dispenseIdentifier);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         Dispensement DispenseOrQueueProducts(Dispensement dispensement, string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         DispensementNoDups DispenseOrQueueProductsNoDups(Dispensement dispensement, string userName, string password, string deviceID, string lastLocation, string dispenseIdentifier);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         void SaveDispenseAbnSignature(int practiceLocationID, int dispenseQueueID, byte[] patientAbnSignature, string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         DispensementCustomBrace DispenseOrQueueCustomBraceProducts(DispensementCustomBrace dispensement, string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         DispensementCustomBraceNoDups DispenseOrQueueCustomBraceProductsNoDups(DispensementCustomBrace dispensement, string userName, string password, string deviceID, string lastLocation, string dispenseIdentifier);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         PhysicianData[] GetPhysicians(int practiceLocationID, string userName, string password, string deviceID, string lastLocation);

        [OperationContract, FaultContract(typeof(ExceptionFault))]
        PhysicianDataForPractice[] GetPhysiciansForPractice(int practiceID, string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         PhysicianData[] GetFitters(int practiceLocationID, string userName, string password, string deviceID, string lastLocation);


        [OperationContract, FaultContract(typeof(ExceptionFault))]
        PhysicianDataForPractice[] GetFittersForPractice(int practiceID, string userName, string password, string deviceID, string lastLocation);

        [OperationContract, FaultContract(typeof(ExceptionFault))]
         DispenseItem[] GetDispenseQueue(int practiceLocationID, bool isDispenseQueueSortDescending, int skip, int take, string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         DispenseItem[] GetDispenseQueue1(out int count, int practiceLocationID, bool isDispenseQueueSortDescending, int skip, int take, string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         Dispensement[] GetDispensedUnsigned(out int count, int practiceLocationID, int skip, int take, string userName, string password, string deviceID, string lastLocation);

        [OperationContract, FaultContract(typeof(ExceptionFault))]
        DispenseSummaryContainer GetDispenseSummary(int practiceLocationID, bool isDispenseQueueSortDescending, string userName, string password, string deviceID, string lastLocation);

        [OperationContract, FaultContract(typeof(ExceptionFault))]
        Dispensement GetDispensementById(int practiceLocationId, int dispenseId, string userName, string password, string deviceID, string lastLocation);

        [OperationContract, FaultContract(typeof(ExceptionFault))]
        Dispensement GetQueuedDispensementByIDs(int practiceLocationId, int[] dispenseQueueIDs, string userName, string password, string deviceID, string lastLocation);

        [OperationContract, FaultContract(typeof(ExceptionFault))]
         ICD9[] GetDispenseQueueICD9s(int practiceLocationID, string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         InventoryCycleType[] GetInventoryCycleTypes(int practiceLocationID, string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         InventoryCycle[] GetInventoryCyclesByType(int practiceLocationID, string userName, string password, string deviceID, string lastLocation, InventoryCycleType inventoryCycleType);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         void AddInventoryCount(int practiceLocationID, string userName, string password, string deviceID, string lastLocation, InventoryCycleType inventoryCycleType, InventoryCycle inventoryCycle, InventoryItemCount[] inventoryItemCounts);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         bool AddInventoryCount1(int practiceLocationID, string userName, string password, string deviceID, string lastLocation, InventoryCycleType inventoryCycleType, InventoryCycle inventoryCycle, InventoryItemCount[] inventoryItemCounts);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         InventoryDiscrepencyItem[] GetInventoryCycleDiscrepencies(int practiceLocationID, string userName, string password, string deviceID, string lastLocation, InventoryCycle inventoryCycle, bool isConsignment);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         void AdjustDiscrepencies(int practiceLocationID, string userName, string password, string deviceID, string lastLocation, InventoryCycle inventoryCycle, bool isConsignment);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         void AdjustDiscrepencies1(int practiceLocationID, string userName, string password, string deviceID, string lastLocation, InventoryCycle inventoryCycle, bool isConsignment, string title);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         ConsignmentItem[] GetConsignmentReplenishmentList(int practiceLocationID, string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         void AddReplesishmentListToShoppingCart(int UserID, ShoppingCartItemCompact[] shoppingCartItems, int practiceLocationID, string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         void AddReplenishmentConsignmentListToShoppingCart(int UserID, ConsignmentItem[] consignmentItems, int practiceLocationID, string userName, string password, string deviceID, string lastLocation);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         void SaveUpcCode(int practiceID, string userName, string password, string deviceID, string lastLocation, int productID, string upcCode, bool isThirdPartyProduct);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         CheckInPO[] GetPendingCheckIns(int practiceLocationID, string userName, string password, string deviceID, string lastLocation, string searchText, int skip, int take);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         CheckInPO[] GetPendingCustomBraceCheckIns(int practiceLocationID, string userName, string password, string deviceID, string lastLocation, int skip, int take);

         [OperationContract, FaultContract(typeof(ExceptionFault))]
         int CheckInPo(CheckInPO checkInPo, string userName, string password, string deviceID, string lastLocation);

        [OperationContract, FaultContract(typeof(ExceptionFault))]
        int CheckInPoAndSaveInvoice(CheckInPO checkInPo, string userName, string password, string deviceID, string lastLocation, bool saveInvoiceInfo);

        [OperationContract, FaultContract(typeof(ExceptionFault))]
         int CheckInManualPo(int practiceLocationID, CheckInProductExtra[] checkInProdExtras, string userName, string password, string deviceID, string lastLocation);
    
        [OperationContract, FaultContract(typeof(ExceptionFault))]
         Payer[] GetPayers(int practiceId, string userName, string password, string deviceId, string lastLocation);

        [OperationContract, FaultContract(typeof(ExceptionFault))]
        Payer[] GetPayersForPractice(int practiceId, string userName, string password, string deviceId, string lastLocation);

        [OperationContract, FaultContract(typeof(ExceptionFault))]
        CustomFitProcedure[] GetCustomFitProcedures(string userName, string password, string deviceId, string lastLocation);

          }

}
