﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using BregWcfService.DataContracts;

namespace BregWcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IProductInventoryService" in both code and config file together.
    [ServiceContract]
    public interface IProductInventoryService
    {   [OperationContract]
        [WebGet (RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "productInventory?practiceId={practiceId}&practiceLocationID={practiceLocationID}&userName={userName}&password={password}&deviceID={deviceId}&lastLocation={lastLocation}")]
        ProductUpdateWrapper GetInventoryLandingData(int practiceId, int practiceLocationID, string userName, string password, string deviceID, string lastLocation);

    }
}
