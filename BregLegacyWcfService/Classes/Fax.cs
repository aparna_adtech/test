﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using J2.eFaxDeveloper.Outbound;
using System.IO;
using ClassLibrary.DAL;

namespace BregWcfService.Classes
{
    public class Fax
    {
        public void SendFax(string faxNumber, String faxBody)
        {
            string transmissionID = "";
            long currentFileTime = System.DateTime.Now.ToFileTimeUtc();
            transmissionID = currentFileTime.ToString().Substring(0, 15);

            //Save off file - Necessary for faxing. 
            string FileSave = "c:/inetpub/wwwroot/fax/dispenseFax";
#if(DEBUG)
            //Mike Sneen's file location
            //FileSave = "C:/DevProjects/BregVisionSystem-DEV- SPRING 2009 branch/BregVisionSolution/BregVision/Fax/dispenseFax";
            // Yuiko file location
            FileSave = "C:/Breg/vision.web/BregVisionLiteEnhancements1/Fax/dispenseFax";
#endif
            FileSave = string.Concat(FileSave, currentFileTime, ".htm");


            // create a write stream
            FileStream writeStream = new FileStream(FileSave, FileMode.Create, FileAccess.Write);

            using (Stream stmText = new MemoryStream(faxBody.Length))
            {
                StreamWriter swText = new StreamWriter(stmText);

                swText.Write(faxBody);
                swText.Flush();

                stmText.Position = 0;

                // Use the stream
                ReadWriteStream(stmText, writeStream);
            }

            SendFax(faxNumber, FileSave, 0);
        }

        private void ReadWriteStream(Stream readStream, Stream writeStream)
        {
            int Length = 256;
            Byte[] buffer = new Byte[Length];
            int bytesRead = readStream.Read(buffer, 0, Length);
            // write the required bytes
            while (bytesRead > 0)
            {
                writeStream.Write(buffer, 0, bytesRead);
                bytesRead = readStream.Read(buffer, 0, Length);
            }
            readStream.Close();
            writeStream.Close();
        }

        public void SendFax(string FaxNumber, String FaxName, Int32 FaxMessageID, J2.eFaxDeveloper.FaxFileType FileType = J2.eFaxDeveloper.FaxFileType.html)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var transmissionID = System.DateTime.Now.ToFileTimeUtc().ToString().Substring(0, 15);
                FaxMessage faxMessage = null;
                if (FaxMessageID == 0)
                {
                    faxMessage = new FaxMessage();
                    visionDB.FaxMessages.InsertOnSubmit(faxMessage);
                }
                else
                {
                    var query = from fm in visionDB.FaxMessages
                                where fm.FaxMessageID == FaxMessageID
                                select fm;
                    faxMessage = query.First<FaxMessage>();
                }

                //done - Yuiko, add one to the SendAttempts Property ()
                faxMessage.FaxNumber = FaxNumber;
                faxMessage.FaxFileName = FaxName;
                faxMessage.SentTime = DateTime.Now;
                faxMessage.CreatedDate = DateTime.Now;

                visionDB.SubmitChanges();
                if (faxMessage.Skip == true)
                {
                    return;
                }

                //check to see if the fax is turned off - This turns the whole system on and off
                FaxServer faxController = visionDB.FaxServers.FirstOrDefault<FaxServer>();

                if (faxController.FaxEnabled == true)//|| (faxMessage.Skip == false ))
                {
                    faxMessage.SendAttempts += 1;
                    visionDB.SubmitChanges();
                }
                else
                {
                    //visionDB.SubmitChanges();
                    return;
                }

                //visionDB.SubmitChanges(); // Yuiko, test this to make sure it saved here, and also saves the status at the end of this function

                //int TransmissionIDStart = FaxName.IndexOf("dispenseFax") + 11;
                //transmissionID = FaxName.Substring(TransmissionIDStart, 14); //done--Yuiko, make this 14 characters and add the first letter of the server name (System.Environment.MachineName)
                //transmissionID = System.Environment.MachineName.Substring(0, 1) + transmissionID;

                if ((faxMessage.SendAttempts == 6) && faxMessage.Skip == false)
                {
                    //done--Yuiko, get the transmission id out of the message.
                    //Yuiko, Send Bobby an email "We have attempted to send the message with transmission id {0} 5 times and it has failed each time.  Please correct it or send it manually."

                    //SendEmail("VisionMaintenance@breg.com", string.Format(@"We have attempted to send the message with transmission id {0} 5 times and it has failed each time.  Please correct it or send it manually.", transmissionID), "Fax Failed");

                    return;
                }

                try
                {
                    //Instantiate the Main object 
                    var faxclient = new OutboundClient();

                    //Set your eFax Developer account identifier (required) 
                    faxclient.AccountId = "7604776221";

                    //Set your eFax Developer user name (required) 
                    faxclient.UserName = "sromeo";

                    //Set your eFax Developer password (required) 
                    faxclient.Password = "victor";

                    //Instantiate outbound request object
                    var request = new OutboundRequest();

                    //Set transmission ID
                    request.TransmissionControl.TransmissionID = transmissionID;

                    //Set "Dynamic Fax Header"
                    request.TransmissionControl.FaxHeader = "";

                    //Set fax disposition details
                    request.DispositionControl.DispositionMethod = DispositionMethod.Post;
                    request.DispositionControl.DispositionLevel = DispositionLevel.Error;
                    request.DispositionControl.DispositionURL = System.Configuration.ConfigurationManager.AppSettings["EFaxNotificationURL"];
                    //#if(DEBUG)
                    //                request.DispositionControl.DispositionURL = "http://72.130.170.214/bregvision/EFaxNotification.aspx";
                    //#endif

#if(!DEBUG)
                //outbound_req.DispositionControl.DispositionEmails.Add(
                //    new DispositionEmail
                //    {
                //        DispositionAddress = "Gregory.armstrong.ross@gmail.com",
                //        DispositionRecipient = "Greg Ross"
                //    });
                //outbound_req.DispositionControl.DispositionEmails.Add(
                //    new DispositionEmail
                //    {
                //        DispositionAddress = "VisionMaintenance@breg.com",
                //        DispositionRecipient = "Robert Haywood"
                //    });
#endif
                    //request.DispositionControl.DispositionEmails.Add(
                    //    new DispositionEmail
                    //    {
                    //        DispositionAddress = "hchan@breg.com",
                    //        DispositionRecipient = "Henry Chan"
                    //    });

                    //Set the recipient fax number for this transmission (required) 
#if(DEBUG)
                    //(619) 342-8639 Mike Sneen's free  online fax number
                    //FaxNumber = "6193428639";
                    //FaxNumber = "6199224340"; //send to my cell so the fax won't go through
                    //FaxNumber = "1112223333"; //Yuiko, Change this.  send to home # so the fax won't go through

                    faxMessage.FaxNumber = FaxNumber;
                    visionDB.SubmitChanges();
#endif
                    request.Recipient.RecipientFax = FaxNumber;

                    //Attach fax document
                    request.Files.Add(
                        new FaxFile
                        {
                            FileContents = File.ReadAllBytes(FaxName),
                            FileType = FileType
                        });

                    //Perform the Post to eFax Developer 
                    try
                    {
                        if (FaxServerEnabled() == true)
                        {
                            var response = faxclient.SendOutboundRequest(request); //This is where it actually sends to EFax

                            if (response.StatusCode == StatusCode.Success)
                            {
                                faxMessage.Sent = true;
                                visionDB.SubmitChanges();
                            }
                            else // fax send failed
                            {
                                string message = string.Format(@"StatusCode:{0}     StatusDescription:{1}     ErrorLevel:{2}     ErrorDescription:{3}", response.StatusCode, response.StatusDescription, response.ErrorLevel, response.ErrorMessage);
                                Email.SendEmail("OrderConfirmation", "VisionMaintenance@breg.com", message, string.Format(@"An response code other than 1 was returned in the SendFax function trying to send {0} to efaxdeveloper to fax number {1}", transmissionID, FaxNumber), BLL.SecureMessaging.MessageClass.BregMaintenance);
                            }
                        }
                        else
                        {
                            //SendEmail("VisionMaintenance@breg.com", string.Format(@"The Fax Server is Disabled and Vision is trying to send {0} to efaxdeveloper to fax number{1}", transmissionID, FaxNumber), string.Format(@"The Fax Server is Disabled and Vision is trying to send {0} to efaxdeveloper to fax number{1}", transmissionID, FaxNumber));
                        }
                    }
                    catch (Exception ex)
                    {
                        // A java.net.MalformedURLException may occur if you selected the POST Protocol HTTPS 
                        Email.SendEmail("OrderConfirmation", "VisionMaintenance@breg.com", ex.Message + " " + ex.StackTrace + " " + ex.Source, string.Format(@"An error occurred trying to send {0} to efaxdeveloper to fax number {1}", transmissionID, FaxNumber), BLL.SecureMessaging.MessageClass.BregMaintenance);
                        //Interaction.MsgBox(ex.Message + ";" + Constants.vbCrLf + Constants.vbCrLf + ex.Source + ";" + Constants.vbCrLf + Constants.vbCrLf + ex.StackTrace); 
                        //return;
                    }

                }
                catch (Exception exx)
                {
                    Email.SendEmail("OrderConfirmation", "VisionMaintenance@breg.com", exx.Message + " " + exx.StackTrace + " " + exx.Source, string.Format(@"An error occurred in the SendFax function trying to send {0} to efaxdeveloper to fax number {1}", transmissionID, FaxNumber), BLL.SecureMessaging.MessageClass.BregMaintenance);
                }
                //if (FaxMessageID == 0)
                //{
                //    visionDB.FaxMessages.InsertOnSubmit(faxMessage);
                //}
                //visionDB.SubmitChanges();
            }
        }

        public static bool SendTestFax(string faxNumber, string message)
        {
            //Instantiate the Main object 
            var faxclient = new OutboundClient();

            //Set your eFax Developer account identifier (required) 
            faxclient.AccountId = "7604776221";

            //Set your eFax Developer user name (required) 
            faxclient.UserName = "sromeo";

            //Set your eFax Developer password (required) 
            faxclient.Password = "victor";

            //Instantiate outbound request object
            var request = new OutboundRequest();

            //Set transmission ID
            request.TransmissionControl.TransmissionID = "tf" + faxNumber;

            //Set "Dynamic Fax Header"
            request.TransmissionControl.FaxHeader = "";

            //Set fax disposition details
            request.DispositionControl.DispositionMethod = DispositionMethod.Post;
            request.DispositionControl.DispositionLevel = DispositionLevel.None;

            //#if(DEBUG)
            //            //(619) 342-8639 Mike Sneen's free  online fax number
            //            faxNumber = "6193428639";
            //#endif

            request.Recipient.RecipientFax = faxNumber;


            request.Files.Add(
            new FaxFile
            {

                FileContents = new System.Text.UTF8Encoding().GetBytes("This is a test fax from Breg Vision.  This message is just to verify that this is a valid fax number, and no action is necessary."),
                FileType = J2.eFaxDeveloper.FaxFileType.txt
            });


            var response = faxclient.SendOutboundRequest(request); //This is where it actually sends to EFax

            if (response.StatusCode == StatusCode.Success)
            {
                return true;
            }
            return false;
        }

        public static string GetStatus(String TransmissionID)
        {
            //Instantiate the Main object 
            var faxclient = new OutboundClient();

            //Set your eFax Developer account identifier (required) 
            faxclient.AccountId = "7604776221";

            //Set your eFax Developer user name (required) 
            faxclient.UserName = "sromeo";

            //Set your eFax Developer password (required) 
            faxclient.Password = "victor";

            try
            {
                // this will fail if we're dealing with multiple machines with different hostnames...
                var response = faxclient.SendOutboundStatusRequest(System.Environment.MachineName.Substring(0, 1) + TransmissionID, "");
                return response.Recipient.Status.Message + "; " + response.Recipient.Status.Classification;
            }
            catch
            {
                return "Error Returning Status";
            }
        }


        private bool FaxServerEnabled()
        {
            try
            {
                VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
                var query = from fs in visionDB.FaxServers
                            select fs;

                FaxServer faxServer = query.First<FaxServer>();

                return faxServer.FaxEnabled;
            }
            catch
            {
                return true;
            }
        }
    }
}