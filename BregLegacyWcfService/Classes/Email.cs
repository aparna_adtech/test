﻿using System;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregWcfService.DocumentServiceReference;
using BregWcfService.EMRIntegrationReference;
using Microsoft.Practices.EnterpriseLibrary.Data;
using ClassLibrary.DAL;
using BregWcfService.EmailTemplates;
using System.Configuration;
using System.Collections.Generic;



namespace BregWcfService.Classes
{
    public class Email
    {
        private class VisionSessionProvider : BLL.IVisionSessionProvider
        {
            private Int32 _practiceID;
            private Int32 _practiceLocationID;

            public VisionSessionProvider(Int32 practiceLocationID)
            {
                try
                {
                    _practiceLocationID = practiceLocationID;
                    using (var db = ClassLibrary.DAL.VisionDataContext.GetVisionDataContext())
                    {
                        _practiceID = db.PracticeLocations.Where(x => x.PracticeLocationID == _practiceLocationID).Select(x => x.PracticeID).First();
                    }
                }
                catch
                {
                    _practiceID = -1;
                    _practiceLocationID = -1;
                }
            }

            public int GetCurrentPracticeID()
            {
                return _practiceID;
            }

            public int GetCurrentPracticeLocationID()
            {
                return _practiceLocationID;
            }
        }

        public static String SendEmail(string MailFromProfile, string MailTo, string MailBody, string MailSubject, string filePath, BLL.SecureMessaging.MessageClass messageClass, BLL.IVisionSessionProvider sessionProvider = null)
        {
            #region secure messaging
            // temp storage for message parts
            var x_sender = "";
            var x_recipient = MailTo;
            var x_body = MailBody;
            var x_subject = MailSubject;
            // apply updates to message parts
            // TODO: need to use 1 instance instead of instantiating per message send...
            new BLL.SecureMessaging(sessionProvider).ApplySecureMessagingAdjustments(messageClass, ref x_sender, ref x_recipient, ref x_body, ref x_subject);
            #endregion

            #region send mail (via database mail)
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");

            DbCommand dbCommand = db.GetStoredProcCommand("msdb.dbo.sp_send_dbmail");
            
            db.AddInParameter(dbCommand, "profile_name", DbType.String, MailFromProfile);
            db.AddInParameter(dbCommand, "recipients", DbType.String, x_recipient);
            db.AddInParameter(dbCommand, "subject", DbType.String, x_subject);
            db.AddInParameter(dbCommand, "body", DbType.String, x_body);
            db.AddInParameter(dbCommand, "body_format", DbType.String, "HTML");
            if (!string.IsNullOrEmpty(filePath) && File.Exists(filePath))
                db.AddInParameter(dbCommand, "file_attachments", DbType.String, filePath);

#if(!DEBUG)
            int SQLReturn = db.ExecuteNonQuery(dbCommand);
#endif
            #endregion

            return MailTo;
        }

        public static String SendEmail(string MailFromProfile, string MailTo, string MailBody, string MailSubject, BLL.SecureMessaging.MessageClass messageClass, BLL.IVisionSessionProvider sessionProvider = null)
        {
            return SendEmail(MailFromProfile, MailTo, MailBody, MailSubject, null, messageClass, sessionProvider);
        }


        public static string SendBillingEmail(int DispenseBatchID, int practiceLocationID)
        {
            if (DispenseBatchID < 1)
            {
                return "";
            }

            using (var visionDB = ClassLibrary.DAL.VisionDataContext.GetVisionDataContext())
            {
                Func<string, string, string, string, string> _formatModifiers = (lrm, m1, m2, m3) =>
                {
                    var x = string.Empty;
                    Action __appendComma = () => { if (!string.IsNullOrEmpty(x)) { x += ", "; } };
                    Action<string> __appendItem = (s) => { if (!string.IsNullOrEmpty(s)) { __appendComma(); x += s; } };
                    __appendItem(lrm);
                    __appendItem(m1);
                    __appendItem(m2);
                    __appendItem(m3);
                    return x;
                };

                var itemsDispensed = from db in visionDB.DispenseBatches
                                     join d in visionDB.Dispenses on db.DispenseBatchID equals d.DispenseBatchID
                                     join dd in visionDB.DispenseDetails on d.DispenseID equals dd.DispenseID
                                     join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                                     where db.DispenseBatchID == DispenseBatchID
                                     && (dq.DeclineReason.ToLower() == "accepted".ToLower() || dq.DeclineReason == null || dq.DeclineReason.Equals(string.Empty))
                                     //&& dd.IsCashCollection == false
                                     select new
                                     {
                                         DispensedDate = d.DateDispensed.ToShortDateString(),
                                         PatientName = (((d.PatientFirstName ?? "") + " " + (d.PatientLastName ?? ""))).Trim(),
                                         d.PatientCode,
                                         Physician = dd.Clinician.Contact.FirstName + " " + dd.Clinician.Contact.LastName,
                                         HCPCs = dq.HCPCs,
                                         Modifiers = _formatModifiers(dd.LRModifier, dq.Mod1, dq.Mod2, dq.Mod3),
                                         ActualChargeBilled = string.Format("{0:C}", dd.ActualChargeBilled),
                                         DMEDeposit = string.Format("{0:C}", dd.DMEDeposit),
                                         CashCarry = dd.IsCashCollection,
                                         ProductName = dd.PracticeCatalogProduct.IsThirdPartyProduct == true ? dd.PracticeCatalogProduct.ThirdPartyProduct.Name : dd.PracticeCatalogProduct.MasterCatalogProduct.Name,
                                         ProductCode = dd.PracticeCatalogProduct.IsThirdPartyProduct == true ? dd.PracticeCatalogProduct.ThirdPartyProduct.Code : dd.PracticeCatalogProduct.MasterCatalogProduct.Code,
                                         Size = dd.PracticeCatalogProduct.IsThirdPartyProduct == true ? dd.PracticeCatalogProduct.ThirdPartyProduct.Size : dd.PracticeCatalogProduct.MasterCatalogProduct.Size,
                                         QuantityToDispense = dd.Quantity,
                                         DxCode = dd.ICD9Code,
                                         PatientRefusalReason = dq.DeclineReason,
                                         dd.IsMedicare,
                                         ABNForm = dd.ABNType
                                         //dd.PracticeCatalogProduct //brand
                                     };

                //make a grid for the dispense queue to render for an email
                var dispenseEmail = new GridView();
                dispenseEmail.DataSource = itemsDispensed;
                dispenseEmail.DataBind();

                var ctlAll = new System.Web.UI.WebControls.Panel();
                ctlAll.Controls.Add(dispenseEmail);
                ctlAll.Controls.Add(new LiteralControl("<br/><br/>"));

                var sb = new StringBuilder();
                var sw = new StringWriter(sb);
                var htmlTW = new HtmlTextWriter(sw);
                ctlAll.RenderControl(htmlTW);

                var html = sb.ToString();

                var MailTo = BLL.PracticeLocation.Email.GetBillingEmail(practiceLocationID);

                var dispenseBatchIDDebugInfo = "";
                //TODO: It seems the empty MailTo check should be done before the expense of building the email.
                if (string.IsNullOrEmpty(MailTo.Trim()) == false && itemsDispensed.Count() > 0)
                {
                    SendEmail("OrderConfirmation", MailTo, html, string.Format("DME Dispensement Information - Billing {0}", dispenseBatchIDDebugInfo), BLL.SecureMessaging.MessageClass.Billing, new VisionSessionProvider(practiceLocationID));
                }

                return html;
            }
        }


        public static bool SendPatientEmail(int DispenseBatchID, int practiceLocationID, MemoryStream pdfMemoryStream, bool fromVision = false)
        {
            #region Exit if no Batch ID

            if (DispenseBatchID < 1)
            {
                pdfMemoryStream = null;
                return false;
            }
            #endregion

            //int[] batchID = { DispenseBatchID }; What I want here is DispenseID's
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                int[] dispenseIDs = (from d in visionDB.Dispenses
                                     where d.DispenseBatchID == DispenseBatchID
                                     select d.DispenseID).ToArray<int>();

                SendPatientEmail(dispenseIDs, practiceLocationID, pdfMemoryStream, fromVision: fromVision, createPDF: true, sendEmail: true);
            }

            return true;
        }

        public static bool RenderDispenseReceiptPDF(int dispenseID, int practiceLocationID, out string pdfFilename)
        {
            var fromVision = false; // always false for PDF generation (left here to keep code consistent)
            var virtualDirName = "BregWcfService/";
            var emailTemplatePath = string.Format("~/{0}EmailTemplates/", virtualDirName);
            
            pdfFilename = null;

            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                #region get item dispensed
                var dispensement = (from d in visionDB.Dispenses
                                     join pl in visionDB.PracticeLocations on d.PracticeLocationID equals pl.PracticeLocationID
                                     join p in visionDB.Practices on pl.PracticeID equals p.PracticeID
                                     where d.DispenseID == dispenseID
                                     select new
                                     {
                                         DispenseID = d.DispenseID,
                                         DateDispensed = d.DateDispensed,
                                         PatientCode = d.PatientCode,
                                         PatientEmail = d.PatientEmail,
                                         PatientFirstName = d.PatientFirstName,
                                         PatientLastName = d.PatientLastName,
                                         PracticeName = p.PracticeName,
                                         PracticeID = p.PracticeID,
                                         PracticeLocationName = pl.Name,
                                         EMREnabled = p.EMRIntegrationPullEnabled,
                                         PatientRefusalReason = (from dq1 in visionDB.DispenseQueues
                                                                 join dd2 in visionDB.DispenseDetails on dq1.DispenseQueueID equals dd2.DispenseQueueID
                                                                 where dd2.DispenseID == d.DispenseID
                                                                 select dq1.DeclineReason).FirstOrDefault(),
                                         PatientSignatureInfo = (
                                                                from dd in visionDB.DispenseDetails
                                                                join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                                                                join ds in visionDB.DispenseSignatures on dq.DispenseSignatureID equals ds.DispenseSignatureID
                                                                where dd.DispenseID == d.DispenseID && ds.PatientSignature.Length > 10
                                                                select ds
                                                            ).FirstOrDefault(),
                                         PatientSignedDate = (
                                                               from dd in visionDB.DispenseDetails
                                                               join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                                                               join ds in visionDB.DispenseSignatures on dq.DispenseSignatureID equals ds.DispenseSignatureID
                                                               where dd.DispenseID == d.DispenseID && ds.PatientSignature.Length > 10
                                                               select ds.PatientSignedDate
                                                              ).FirstOrDefault(),
                                         DispenseItems = (from dd in visionDB.DispenseDetails
                                                          join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                                                          where dd.DispenseID == d.DispenseID /* && dd.IsCashCollection == false */
                                                          select new DispenseItemDto
                                                          {
                                                              DispenseDetailID = dd.DispenseDetailID,
                                                              DispenseQueueID = dd.DispenseQueueID, 
                                                              IsMedicare = dd.IsMedicare, 
                                                              ABNType = dd.ABNType,
                                                              Cash = dd.IsCashCollection,
                                                              Physician = dd.Clinician.Contact.FirstName + " " + dd.Clinician.Contact.LastName,
                                                              Name = dd.PracticeCatalogProduct.IsThirdPartyProduct == true ? dd.PracticeCatalogProduct.ThirdPartyProduct.Name : dd.PracticeCatalogProduct.MasterCatalogProduct.Name,
                                                              Code = dd.PracticeCatalogProduct.IsThirdPartyProduct == true ? dd.PracticeCatalogProduct.ThirdPartyProduct.Code : dd.PracticeCatalogProduct.MasterCatalogProduct.Code, 
                                                              LRModifier = dd.LRModifier,
                                                              Gender = dd.PracticeCatalogProduct.IsThirdPartyProduct ? dd.PracticeCatalogProduct.ThirdPartyProduct.Gender : dd.PracticeCatalogProduct.MasterCatalogProduct.Gender,
                                                              Size = dd.PracticeCatalogProduct.IsThirdPartyProduct == true ? dd.PracticeCatalogProduct.ThirdPartyProduct.Size : dd.PracticeCatalogProduct.MasterCatalogProduct.Size,
                                                              HCPCs = dq.HCPCs, 
                                                              ICD9Code = dd.ICD9Code, 
                                                              MedicareOption = dq.MedicareOption,
                                                              ActualChargeBilled = string.Format("{0:C}", dd.ActualChargeBilled),
                                                              DMEDeposit = string.Format("{0:C}", dd.DMEDeposit),
                                                              QuantityToDispense = dd.Quantity,
                                                              AbnReasonText = dq.ABNReason,
                                                              SupplierName = (
                                                                                 dd.PracticeCatalogProduct.IsThirdPartyProduct == false ?
                                                                                 dd.PracticeCatalogProduct.MasterCatalogProduct.MasterCatalogSubCategory.MasterCatalogCategory.MasterCatalogSupplier.SupplierName :
                                                                                 ((
                                                                                     from pcsb in visionDB.PracticeCatalogSupplierBrands
                                                                                     join tps in visionDB.ThirdPartySuppliers on pcsb.ThirdPartySupplierID equals tps.ThirdPartySupplierID
                                                                                     where pcsb.PracticeCatalogSupplierBrandID == dd.PracticeCatalogProduct.PracticeCatalogSupplierBrandID
                                                                                     select tps

                                                                                 ).FirstOrDefault()).SupplierShortName
                                                                             ),
                                                              WarrantyInfo = dd.PracticeCatalogProduct.PracticeCatalogSupplierBrand.SupplierWarranty.ProductWarranty


                                                          })
                                     }).FirstOrDefault();
                #endregion

                if (dispensement == null)
                    return false;

                var isMedicare = dispensement.DispenseItems.Any(di => di.IsMedicare);

                #region build dispense receipt and email body user controls
                var page = new EmailReadyPage();
                var ctlPatientReceipt = new System.Web.UI.WebControls.Panel();
                var ctlPatientEmailBody = new System.Web.UI.WebControls.Panel();

                #region Dispense Receipt Control

                PatientEmailBody emailBody = page.LoadControl(string.Format("{0}PatientEmailBody.ascx", emailTemplatePath)) as PatientEmailBody;
                //set any properties here after we get text from Kevin
                ctlPatientEmailBody.Controls.Add(emailBody);

                if (!string.IsNullOrWhiteSpace(dispensement.PatientEmail))
                {
                    HippaWaiver hippaWaiver = page.LoadControl(string.Format("{0}HippaWaiver.ascx", emailTemplatePath)) as HippaWaiver;
                    hippaWaiver.PracticeName = dispensement.PracticeName;
                    hippaWaiver.PatientEmail = dispensement.PatientEmail;
                    hippaWaiver.PatientCode = dispensement.PatientCode;
                    hippaWaiver.CreatedDate = dispensement.DateDispensed;
                    hippaWaiver.PatientName = (dispensement.PatientFirstName ?? string.Empty) + " " + (dispensement.PatientLastName ?? string.Empty);

                    if (!string.IsNullOrWhiteSpace(dispensement.PatientRefusalReason))
                    {
                        hippaWaiver.PatientRefusalReason = dispensement.PatientRefusalReason;
                    }

                    if (dispensement.PatientSignatureInfo != null && dispensement.PatientSignatureInfo.DispenseSignatureID != 0)
                    {
                        hippaWaiver.PatientSignatureID = dispensement.PatientSignatureInfo.DispenseSignatureID;
                    }

                    if (dispensement.PatientSignedDate != null && dispensement.PatientSignedDate.HasValue)
                    {
                        hippaWaiver.PatientSignedDate = dispensement.PatientSignedDate.Value;
                    }

                    hippaWaiver.FromVision = fromVision;
                    hippaWaiver.LoadControlInfo();
                    ctlPatientReceipt.Controls.Add(hippaWaiver);
                    //ctlPatientReceipt.Controls.Add(new LiteralControl("<br/><br/>"));
                }

                DispenseReceipt dispenseReceiptControl = page.LoadControl(string.Format("{0}DispenseReceipt.ascx", emailTemplatePath)) as DispenseReceipt;
                dispenseReceiptControl.PracticeID = dispensement.PracticeID;
                dispenseReceiptControl.PracticeLocationID = practiceLocationID;

                if (dispensement.PatientSignatureInfo != null)
                {
                    dispenseReceiptControl.PatientSignatureID = dispensement.PatientSignatureInfo.DispenseSignatureID;
                }

                if (dispensement.PatientSignedDate != null && dispensement.PatientSignedDate.HasValue)
                {
                    dispenseReceiptControl.PatientSignedDate = dispensement.PatientSignedDate.Value;
                }
                dispenseReceiptControl.DispenseID = dispensement.DispenseID;
                dispenseReceiptControl.PracticeName = dispensement.PracticeName;
                dispenseReceiptControl.PracticeLocationName = dispensement.PracticeLocationName;
                dispenseReceiptControl.IsBillingChargeDisplayed =
                    DAL.Practice.Practice.IsDisplayBillChargeOnReceipt(dispensement.PracticeID);
                dispenseReceiptControl.LastItem = true;
                dispenseReceiptControl.PatientRefusalReason = dispensement.PatientRefusalReason;
                dispenseReceiptControl.FromVision = fromVision;
                dispenseReceiptControl.LoadControlInfo(dispensement.DispenseID, emailTemplatePath);

                ctlPatientReceipt.Controls.Add(dispenseReceiptControl);

                AddServiceRefusalSection(page, emailTemplatePath, fromVision, ctlPatientReceipt, dispensement.PracticeName, dispensement.PatientSignatureInfo, dispensement.PatientSignedDate, dispensement.PatientRefusalReason);

                #endregion

                #region Warranty

                var distinctSupplierQuery = (from di in dispensement.DispenseItems
                                             select new
                                             {
                                                 SupplierName = di.SupplierName
                                             }).Select(a => a.SupplierName).Distinct();



                var SupplierQuery = (from distinctSupplier in distinctSupplierQuery
                                     select new WarrantySupplier
                                     {
                                         SupplierName = distinctSupplier,
                                         WarrantyProducts = (
                                                                 from di1 in dispensement.DispenseItems
                                                                 where distinctSupplier == di1.SupplierName
                                                                 select new WarrantyProduct
                                                                 {
                                                                     ProductName = di1.Name,
                                                                     Warranty = di1.WarrantyInfo
                                                                 }
                                                             ).ToList<WarrantyProduct>()
                                     });

                Warranty warranty = page.LoadControl(string.Format("{0}Warranty.ascx", emailTemplatePath)) as Warranty;
                warranty.PracticeName = dispensement.PracticeName;
                //warranty.PatientSignatureUrl = patientSignatureUrl;
                warranty.WarrantySuppliers = SupplierQuery.ToList<WarrantySupplier>();
                warranty.IsMedicare = isMedicare;
                warranty.LoadControlInfo();

                ctlPatientReceipt.Controls.Add(warranty);

                #endregion

                #region Supplier Standards


                if (isMedicare)
                {
                    SupplierStandard supplierStandard = page.LoadControl(string.Format("{0}SupplierStandard.ascx", emailTemplatePath)) as SupplierStandard;

                    supplierStandard.LoadControlInfo();

                    ctlPatientReceipt.Controls.Add(supplierStandard);
                }
                #endregion

                #region DispenseItem
                //for each dispense line item
                foreach (var dispenseItem in dispensement.DispenseItems)
                {
                    //make an abn form for each item that requires it
                    #region ABN Form

                    if (dispenseItem.ABNType == "Medicare")
                    {
                        #region get location address information
                        var addressLine1 = "";
                        var addressLine2 = "";
                        var city = "";
                        var state = "";
                        var zipCode = "";

                        var dalPracticeLocationShippingAddress = new DAL.PracticeLocation.ShippingAddress();
                        var practiceLocationShippingAddress = dalPracticeLocationShippingAddress.Select(practiceLocationID);
                        if (practiceLocationShippingAddress != null)
                        {
                            addressLine1 = practiceLocationShippingAddress.ShipAddress1;
                            addressLine2 = practiceLocationShippingAddress.ShipAddress2;
                            city = practiceLocationShippingAddress.ShipCity;
                            state = practiceLocationShippingAddress.ShipState;
                            zipCode = practiceLocationShippingAddress.ShipZipCode;
                        }

                        var contact = new DAL.PracticeLocation.Contact().Select(practiceLocationID);
                        #endregion

                        ABNFormCtl abnControl = page.LoadControl(string.Format("{0}ABNFormCtl.ascx", emailTemplatePath)) as ABNFormCtl;   //("/App_Resource/BregWcfService.dll/" + "BregWcfService.ABNFormCtl.ascx");
                        abnControl.PracticeName = dispensement.PracticeName;
                        abnControl.PatientName = (dispensement.PatientFirstName ?? string.Empty) + " " + (dispensement.PatientLastName ?? string.Empty);

                        if (dispensement.PatientSignatureInfo != null)
                        {
                            abnControl.PatientSignatureID = dispensement.PatientSignatureInfo.DispenseSignatureID;
                        }
                        abnControl.FromVision = fromVision;
                        if (dispensement.PatientSignedDate != null && dispensement.PatientSignedDate.HasValue)
                        {
                            abnControl.PatientSignedDate = dispensement.PatientSignedDate.Value;
                        }
                        abnControl.Address1 = addressLine1;
                        abnControl.Address2 = addressLine2;
                        abnControl.City = city;
                        abnControl.State = state;
                        abnControl.Zip = zipCode;

                        if (contact != null)
                        {
                            abnControl.BillingPhone = contact.PhoneWork;
                            abnControl.BillingFax = contact.Fax;
                        }

                        if (!string.IsNullOrEmpty(dispensement.PatientCode))
                        {
                            abnControl.PatientCode = dispensement.PatientCode.ToString();
                        }

                        abnControl.ProductName = dispenseItem.Name.ToString();
                        abnControl.ProductCost = ConvertCurrencyToDecimal(dispenseItem.ActualChargeBilled);
                        abnControl.AbnReason = dispenseItem.AbnReasonText;
                        abnControl.MedicareOption = dispenseItem.MedicareOption;
                        abnControl.ABNType = dispenseItem.ABNType;
                        abnControl.LoadControlInfo();
                        ctlPatientReceipt.Controls.Add(abnControl);
                    }else if (dispenseItem.ABNType == "Commercial")
                    {
                        #region get location address information
                        var addressLine1 = "";
                        var addressLine2 = "";
                        var city = "";
                        var state = "";
                        var zipCode = "";

                        var dalPracticeLocationShippingAddress = new DAL.PracticeLocation.ShippingAddress();
                        var practiceLocationShippingAddress = dalPracticeLocationShippingAddress.Select(practiceLocationID);
                        if (practiceLocationShippingAddress != null)
                        {
                            addressLine1 = practiceLocationShippingAddress.ShipAddress1;
                            addressLine2 = practiceLocationShippingAddress.ShipAddress2;
                            city = practiceLocationShippingAddress.ShipCity;
                            state = practiceLocationShippingAddress.ShipState;
                            zipCode = practiceLocationShippingAddress.ShipZipCode;
                        }

                        var contact = new DAL.PracticeLocation.Contact().Select(practiceLocationID);
                        #endregion

                        ABNFormCtlCommercial abnControlCommercial = page.LoadControl(string.Format("{0}ABNFormCtlCommercial.ascx", emailTemplatePath)) as ABNFormCtlCommercial;   //("/App_Resource/

                        abnControlCommercial.PracticeName = dispensement.PracticeName;
                        abnControlCommercial.Address1 = addressLine1;
                        abnControlCommercial.Address2 = addressLine2;
                        abnControlCommercial.City = city;
                        abnControlCommercial.State = state;
                        abnControlCommercial.Zip = zipCode;

                        if (contact != null)
                        {
                            abnControlCommercial.BillingPhone = contact.PhoneWork;
                            abnControlCommercial.BillingFax = contact.Fax;
                        }
                        abnControlCommercial.AbnReason = dispenseItem.AbnReasonText;
                        abnControlCommercial.ProductName = dispenseItem.Name.ToString();
                        abnControlCommercial.MedicareOption = dispenseItem.MedicareOption;
                        if (!string.IsNullOrEmpty(dispensement.PatientCode))
                        {
                            abnControlCommercial.PatientCode = dispensement.PatientCode.ToString();
                        }
                        abnControlCommercial.PatientName = (dispensement.PatientFirstName ?? string.Empty) + " " + (dispensement.PatientLastName ?? string.Empty);
                        abnControlCommercial.ProductCost = ConvertCurrencyToDecimal(dispenseItem.ActualChargeBilled);
                        if (dispensement.PatientSignatureInfo != null)
                        {
                            abnControlCommercial.PatientSignatureID = dispensement.PatientSignatureInfo.DispenseSignatureID;
                        }
                        abnControlCommercial.FromVision = fromVision;
                        if (dispensement.PatientSignedDate != null && dispensement.PatientSignedDate.HasValue)
                        {
                            abnControlCommercial.PatientSignedDate = dispensement.PatientSignedDate.Value;
                        }
                        abnControlCommercial.LoadControlInfo();
                        ctlPatientReceipt.Controls.Add(abnControlCommercial);

                    }

                    #endregion

                    //Make a capped rental notification form for each item that requires it
                    #region Capped Rental Notification
                    if (IsDispenseItemRoutinelyPurchased(dispenseItem))
                    {
                        Notification notification = page.LoadControl(string.Format("{0}Notification.ascx", emailTemplatePath)) as Notification;
                        notification.productName = dispenseItem.Name;
                        notification.FromVision = fromVision;
                        if (dispensement.PatientSignatureInfo != null)
                        {
                            notification.PatientSignatureID = dispensement.PatientSignatureInfo.DispenseSignatureID;
                        }
                        if (dispensement.PatientSignedDate != null && dispensement.PatientSignedDate.HasValue)
                        {
                            notification.PatientSignedDate = dispensement.PatientSignedDate.Value;
                        }

                        notification.LoadControlInfo();
                        ctlPatientReceipt.Controls.Add(notification);
                    }
                    #endregion
                }
                #endregion
                #endregion

                #region render html
                var sb = new StringBuilder();
                var sw = new StringWriter(sb);
                var htmlTW = new HtmlTextWriter(sw);
                try { ctlPatientReceipt.RenderControl(htmlTW); }
                catch { }
                var html = sb.ToString();
                #endregion

                #region render pdf
                string path = System.Web.Hosting.HostingEnvironment.MapPath(string.Format("~/{0}DispenseReceipts", virtualDirName));

                if (!string.IsNullOrEmpty(path))
                {
                    string filename = Path.Combine(path, string.Format("DispenseReceipt{0}.pdf", dispensement.DispenseID)); //string.Format("{0}/test.pdf");

                    PDFParameter pdfParameter = new PDFParameter();
                    pdfParameter.html = html;
                    pdfParameter.filename = filename;
                    pdfParameter.practiceID = dispensement.PracticeID;
                    pdfParameter.practiceLocationID = practiceLocationID;
                    pdfParameter.patientCode = dispensement.PatientCode;
                    pdfParameter.emrEnabled = dispensement.EMREnabled;
                    pdfParameter.dispenseDate = dispensement.DateDispensed;

                    var documents = GetActiveCustomDocuments(dispensement.PracticeID);

                    using (var tempFile = new TempFileManager())
                    {
                        CreatePDF(pdfParameter, tempFile, documents);
                        tempFile.CopyToPath(filename);
                    }

                    pdfFilename = filename;

                    return true;
                }
                else
                    return false;
                #endregion
            }
        }

        private static void AddServiceRefusalSection(EmailReadyPage page, string emailTemplatePath, bool fromVision, Panel ctlPatientReceipt, string practiceName, DispenseSignature patientSignatureInfo, DateTime? patientSignedDate, string patientRefusalReason)
        {
            // NOTE: Do not add refusal section if there is no refusal reason or if the reason is
            // "ACCEPTED"
            if (string.IsNullOrWhiteSpace(patientRefusalReason)
                || patientRefusalReason.ToUpper()=="ACCEPTED")
            {
                return;
            }
            
            var serviceRefusal =
                page.LoadControl(
                    string.Format("{0}ServiceRefusal.ascx", emailTemplatePath)
                ) as ServiceRefusal;

            serviceRefusal.PracticeName = practiceName;
            serviceRefusal.PatientRefusalReason = patientRefusalReason;
            serviceRefusal.FromVision = fromVision;

            if (patientSignatureInfo != null)
                serviceRefusal.PatientSignatureID = patientSignatureInfo.DispenseSignatureID;

            if (patientSignedDate != null)
                serviceRefusal.PatientSignedDate = patientSignedDate.Value;

            serviceRefusal.LoadControlInfo();

            ctlPatientReceipt.Controls.Add(serviceRefusal);
        }

        public class DispenseItemDto
        {
            public int DispenseDetailID { get; set; }
            public int? DispenseQueueID { get; set; }
            public bool IsMedicare { get; set; }
            public bool ABNForm { get; set; }
            public string ABNType { get; set; }
            public bool? Cash { get; set; }
            public string Physician { get; set; }
            public string Name { get; set; }
            public string ShortName { get; set; }
            public string Code { get; set; }
            public string LRModifier { get; set; }
            public string Size { get; set; }
            public string HCPCs { get; set; }
            public string ICD9Code { get; set; }
            public int MedicareOption { get; set; }
            public string ActualChargeBilled { get; set; }
            public string DMEDeposit { get; set; }
            public int? QuantityToDispense { get; set; }
            public string AbnReasonText { get; set; }
            public string SupplierName { get; set; }
            public string WarrantyInfo { get; set; }
            public string Gender { get; set; }
        }

        public static StringBuilder SendPatientEmail(int[] dispenseIDs, int practiceLocationID, MemoryStream pdfMemoryStream, bool fromVision = false, bool createPDF = true, bool sendEmail = true, bool hidePrintButton = false, bool sendFax = true, bool queueOutboundMessages = true)
        {
            var completeString = new StringBuilder();

            var virtualDirName = fromVision ? "BregWcfService/" : string.Empty;
            var emailTemplatePath = string.Format("~/{0}EmailTemplates/", virtualDirName);

            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var practiceLocation =
                    visionDB.PracticeLocations
                        .Where(x => x.PracticeLocationID == practiceLocationID)
                        .Select(x =>
                                    new {
                                        x.PracticeLocationID,
                                        x.IsHcpSignatureRequired
                                    })
                        .Single();

                #region get items dispensed
                var itemsDispensed = from d in visionDB.Dispenses
                                     join pl in visionDB.PracticeLocations on d.PracticeLocationID equals pl.PracticeLocationID
                                     join p in visionDB.Practices on pl.PracticeID equals p.PracticeID
                                     where dispenseIDs.Contains(d.DispenseID)
                                     select new
                                     {
                                         d.CreatedWithHandheld,
                                         IsPhysicianSigned = (
                                            (
                                                from dd in visionDB.DispenseDetails
                                                where dd.DispenseID == d.DispenseID
                                                join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                                                join ds in visionDB.DispenseSignatures on dq.DispenseSignatureID equals ds.DispenseSignatureID
                                                select new { ds.PhysicianSigned }
                                            )
                                            .Any(x => x.PhysicianSigned.HasValue && (x.PhysicianSigned.Value == true))
                                         ),
                                         PhysicianSignedDate =
                                             (
                                             from dd in visionDB.DispenseDetails
                                             where dd.DispenseID == d.DispenseID
                                             join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                                             join ds in visionDB.DispenseSignatures on dq.DispenseSignatureID equals ds.DispenseSignatureID
                                             select ds.PhysicianSignedDate
                                             ).FirstOrDefault(),
                                         DispenseID = d.DispenseID,
                                         DateDispensed = d.DateDispensed,
                                         PatientCode = d.PatientCode,
                                         PatientEmail = d.PatientEmail,
                                         PatientFirstName = d.PatientFirstName,
                                         PatientLastName = d.PatientLastName,
                                         PracticeName = p.PracticeName,
                                         PracticeID = p.PracticeID,
                                         IsSecureMessagingExternalEnabled = p.IsSecureMessagingExternalEnabled,
                                         PracticeLocationName = pl.Name,
                                         p.EMRIntegrationDispenseReceiptPushEnabled,
                                         p.EMRIntegrationHL7PushEnabled,
                                         EmailForPrintDispense = pl.EmailForPrintDispense,
                                         EMREnabled = p.EMRIntegrationPullEnabled,
                                         CloudConnectId = (from dd1 in visionDB.DispenseDetails where dd1.DispenseID == d.DispenseID select dd1.Clinician.CloudConnectId).FirstOrDefault(),
                                         PhysicianName = (from dd1 in visionDB.DispenseDetails where dd1.DispenseID == d.DispenseID select new { FullName = dd1.Clinician.Contact.FirstName + " " + dd1.Clinician.Contact.LastName }).FirstOrDefault(),
                                         PatientRefusalReason = (from dq1 in visionDB.DispenseQueues
                                                                 join dd2 in visionDB.DispenseDetails on dq1.DispenseQueueID equals dd2.DispenseQueueID
                                                                 where dd2.DispenseID == d.DispenseID
                                                                 select dq1.DeclineReason).FirstOrDefault(),
                                         PatientSignatureInfo = (
                                                                from dd in visionDB.DispenseDetails
                                                                join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                                                                join ds in visionDB.DispenseSignatures on dq.DispenseSignatureID equals ds.DispenseSignatureID
                                                                where dd.DispenseID == d.DispenseID && ds.PatientSignature.Length > 10
                                                                select ds
                                                            ).FirstOrDefault(),
                                         PatientSignedDate = (
                                                               from dd in visionDB.DispenseDetails
                                                               join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                                                               join ds in visionDB.DispenseSignatures on dq.DispenseSignatureID equals ds.DispenseSignatureID
                                                               where dd.DispenseID == d.DispenseID && ds.PatientSignature.Length > 10
                                                               select ds.PatientSignedDate
                                                              ).FirstOrDefault(),
                                         DispenseItems = (from dd in visionDB.DispenseDetails
                                                          join dq in visionDB.DispenseQueues on dd.DispenseQueueID equals dq.DispenseQueueID
                                                          where dd.DispenseID == d.DispenseID /* && dd.IsCashCollection == false */
                                                          select new DispenseItemDto {
                                                              DispenseDetailID = dd.DispenseDetailID, 
                                                              DispenseQueueID = dd.DispenseQueueID, 
                                                              IsMedicare = dd.IsMedicare,
                                                              ABNForm = dd.ABNForm,
                                                              ABNType = dd.ABNType, 
                                                              Cash = dd.IsCashCollection,
                                                              Physician = dd.Clinician.Contact.FirstName + " " + dd.Clinician.Contact.LastName, 
                                                              Name = dd.PracticeCatalogProduct.IsThirdPartyProduct == true ? dd.PracticeCatalogProduct.ThirdPartyProduct.Name : dd.PracticeCatalogProduct.MasterCatalogProduct.Name, 
                                                              ShortName = dd.PracticeCatalogProduct.IsThirdPartyProduct == true ? dd.PracticeCatalogProduct.ThirdPartyProduct.ShortName : dd.PracticeCatalogProduct.MasterCatalogProduct.ShortName, 
                                                              Code = dd.PracticeCatalogProduct.IsThirdPartyProduct == true ? dd.PracticeCatalogProduct.ThirdPartyProduct.Code : dd.PracticeCatalogProduct.MasterCatalogProduct.Code, LRModifier = dd.LRModifier,
                                                              Gender = dd.PracticeCatalogProduct.IsThirdPartyProduct ? dd.PracticeCatalogProduct.ThirdPartyProduct.Gender : dd.PracticeCatalogProduct.MasterCatalogProduct.Gender,
                                                              Size = dd.PracticeCatalogProduct.IsThirdPartyProduct == true ? dd.PracticeCatalogProduct.ThirdPartyProduct.Size : dd.PracticeCatalogProduct.MasterCatalogProduct.Size, 
                                                              HCPCs = dq.HCPCs, 
                                                              ICD9Code = dd.ICD9Code, MedicareOption = dq.MedicareOption, ActualChargeBilled = string.Format("{0:C}", dd.ActualChargeBilled), DMEDeposit = string.Format("{0:C}", dd.DMEDeposit), 
                                                              QuantityToDispense = dd.Quantity, 
                                                              AbnReasonText = dq.ABNReason, 
                                                              SupplierName = (
                                                              dd.PracticeCatalogProduct.IsThirdPartyProduct == false ?
                                                                  dd.PracticeCatalogProduct.MasterCatalogProduct.MasterCatalogSubCategory.MasterCatalogCategory.MasterCatalogSupplier.SupplierName :
                                                                  ((
                                                                      from pcsb in visionDB.PracticeCatalogSupplierBrands
                                                                      join tps in visionDB.ThirdPartySuppliers on pcsb.ThirdPartySupplierID equals tps.ThirdPartySupplierID
                                                                      where pcsb.PracticeCatalogSupplierBrandID == dd.PracticeCatalogProduct.PracticeCatalogSupplierBrandID
                                                                      select tps

                                                                      ).FirstOrDefault()).SupplierShortName
                                                              ),
                                                              WarrantyInfo = dd.PracticeCatalogProduct.PracticeCatalogSupplierBrand.SupplierWarranty.ProductWarranty
                                                          })
                                     };
                #endregion

                var dispensements = itemsDispensed.ToList();
                var isMedicareQuery = dispensements.Any(d => d.DispenseItems.Any(di => di.IsMedicare));

                var counter = 1;
                foreach (var dispensement in dispensements)
                {
                    var patient_code = dispensement.PatientCode;

                    // get cloud connect remote_schedule_item_id from the patient code, if possible
                    var vcc_schedule_id = GetCloudConnectRemoteScheduleItemId(ref patient_code);

                    #region build dispense receipt and email body user controls

                    var page = new EmailReadyPage();
                    var ctlPatientReceipt = new System.Web.UI.WebControls.Panel();
                    var ctlPatientEmailBody = new System.Web.UI.WebControls.Panel();

                    #region Email Body

                    PatientEmailBody emailBody = page.LoadControl(string.Format("{0}PatientEmailBody.ascx", emailTemplatePath)) as PatientEmailBody;
                    //set any properties here after we get text from Kevin
                    ctlPatientEmailBody.Controls.Add(emailBody);

                    #endregion

                    #region HIPAA Waiver

                    if (!string.IsNullOrWhiteSpace(dispensement.PatientEmail) && !dispensement.IsSecureMessagingExternalEnabled)
                    {
                        HippaWaiver hippaWaiver = page.LoadControl(string.Format("{0}HippaWaiver.ascx", emailTemplatePath)) as HippaWaiver;
                        hippaWaiver.PracticeName = dispensement.PracticeName;
                        hippaWaiver.PatientEmail = dispensement.PatientEmail;
                        hippaWaiver.PatientCode = patient_code;
                        hippaWaiver.CreatedDate = dispensement.DateDispensed;
                        hippaWaiver.PatientName = (dispensement.PatientFirstName ?? string.Empty) + " " + (dispensement.PatientLastName ?? string.Empty);

                        if (!string.IsNullOrWhiteSpace(dispensement.PatientRefusalReason))
                        {
                            hippaWaiver.PatientRefusalReason = dispensement.PatientRefusalReason;
                        }

                        if (dispensement.PatientSignatureInfo != null && dispensement.PatientSignatureInfo.DispenseSignatureID != 0)
                        {
                            hippaWaiver.PatientSignatureID = dispensement.PatientSignatureInfo.DispenseSignatureID;
                        }

                        if (dispensement.PatientSignedDate != null && dispensement.PatientSignedDate.HasValue)
                        {
                            hippaWaiver.PatientSignedDate = dispensement.PatientSignedDate.Value;
                        }

                        hippaWaiver.FromVision = (fromVision && !createPDF);
                        hippaWaiver.LoadControlInfo();
                        ctlPatientReceipt.Controls.Add(hippaWaiver);
                    }

                    #endregion

                    #region Dispense Receipt

                    DispenseReceipt dispenseReceiptControl = page.LoadControl(string.Format("{0}DispenseReceipt.ascx", emailTemplatePath)) as DispenseReceipt;
                    dispenseReceiptControl.PracticeID = dispensement.PracticeID;
                    dispenseReceiptControl.PracticeLocationID = practiceLocationID;

                    if (dispensement.PatientSignatureInfo != null)
                    {
                        dispenseReceiptControl.PatientSignatureID = dispensement.PatientSignatureInfo.DispenseSignatureID;
                    }

                    if (dispensement.PatientSignedDate != null && dispensement.PatientSignedDate.HasValue)
                    {
                        dispenseReceiptControl.PatientSignedDate = dispensement.PatientSignedDate.Value;
                    }
                    dispenseReceiptControl.DispenseID = dispensement.DispenseID;
                    dispenseReceiptControl.PracticeName = dispensement.PracticeName;
                    dispenseReceiptControl.PracticeLocationName = dispensement.PracticeLocationName;
                    dispenseReceiptControl.IsBillingChargeDisplayed =
                       DAL.Practice.Practice.IsDisplayBillChargeOnReceipt(dispensement.PracticeID);
                    dispenseReceiptControl.LastItem = (counter >= dispensements.Count) ? true : false;
                    dispenseReceiptControl.PatientRefusalReason = dispensement.PatientRefusalReason;
                    dispenseReceiptControl.FromVision = (fromVision && !createPDF);
                    dispenseReceiptControl.LoadControlInfo(dispensement.DispenseID, emailTemplatePath);

                    ctlPatientReceipt.Controls.Add(dispenseReceiptControl);

                    AddServiceRefusalSection(page, emailTemplatePath, fromVision, ctlPatientReceipt, dispensement.PracticeName, dispensement.PatientSignatureInfo, dispensement.PatientSignedDate, dispensement.PatientRefusalReason);

                    #endregion

                    #region Warranty

                    var distinctSupplierQuery = dispensement.DispenseItems.Select(di => di.SupplierName).Distinct();

                    var SupplierQuery = (from distinctSupplier in distinctSupplierQuery
                                         select new WarrantySupplier
                                         {
                                             SupplierName = distinctSupplier,
                                             WarrantyProducts = (
                                                 from di1 in dispensement.DispenseItems
                                                 where distinctSupplier == di1.SupplierName
                                                 select new WarrantyProduct
                                                 {
                                                     ProductName = di1.Name,
                                                     Warranty = di1.WarrantyInfo
                                                 }
                                             ).ToList<WarrantyProduct>()
                                         });

                    Warranty warranty = page.LoadControl(string.Format("{0}Warranty.ascx", emailTemplatePath)) as Warranty;
                    warranty.PracticeName = dispensement.PracticeName;
                    //warranty.PatientSignatureUrl = patientSignatureUrl;
                    warranty.WarrantySuppliers = SupplierQuery.ToList<WarrantySupplier>();
                    warranty.IsMedicare = isMedicareQuery;
                    warranty.LoadControlInfo();

                    ctlPatientReceipt.Controls.Add(warranty);

                    #endregion

                    #region DispenseItem

                    //for each dispense line item
                    foreach (var dispenseItem in dispensement.DispenseItems)
                    {
                        // make an abn form for each item that requires it
                        #region ABN Form

                        dispenseItem.ABNType = dispenseItem.ABNForm == true ? "Medicare" : dispenseItem.ABNType;
                        if (dispenseItem.ABNType == "Medicare")
                        {
                            #region get location address information
                            var addressLine1 = "";
                            var addressLine2 = "";
                            var city = "";
                            var state = "";
                            var zipCode = "";

                            var dalPracticeLocationShippingAddress = new DAL.PracticeLocation.ShippingAddress();
                            var practiceLocationShippingAddress = dalPracticeLocationShippingAddress.Select(practiceLocationID);
                            if (practiceLocationShippingAddress != null)
                            {
                                addressLine1 = practiceLocationShippingAddress.ShipAddress1;
                                addressLine2 = practiceLocationShippingAddress.ShipAddress2;
                                city = practiceLocationShippingAddress.ShipCity;
                                state = practiceLocationShippingAddress.ShipState;
                                zipCode = practiceLocationShippingAddress.ShipZipCode;
                            }

                            var contact = new DAL.PracticeLocation.Contact().Select(practiceLocationID);
                            #endregion

                            ABNFormCtl abnControl = page.LoadControl(string.Format("{0}ABNFormCtl.ascx", emailTemplatePath)) as ABNFormCtl;   //("/App_Resource/BregWcfService.dll/" + "BregWcfService.ABNFormCtl.ascx");
                            abnControl.PracticeName = dispensement.PracticeName;
                            abnControl.PatientName = (dispensement.PatientFirstName ?? string.Empty) + " " + (dispensement.PatientLastName ?? string.Empty);

                            if (dispensement.PatientSignatureInfo != null)
                            {
                                abnControl.PatientSignatureID = dispensement.PatientSignatureInfo.DispenseSignatureID;
                            }
                            abnControl.FromVision = (fromVision && !createPDF);
                            if (dispensement.PatientSignedDate != null && dispensement.PatientSignedDate.HasValue)
                            {
                                abnControl.PatientSignedDate = dispensement.PatientSignedDate.Value;
                            }
                            abnControl.Address1 = addressLine1;
                            abnControl.Address2 = addressLine2;
                            abnControl.City = city;
                            abnControl.State = state;
                            abnControl.Zip = zipCode;

                            if (contact != null)
                            {
                                abnControl.BillingPhone = contact.PhoneWork;
                                abnControl.BillingFax = contact.Fax;
                            }

                            if (!string.IsNullOrEmpty(patient_code))
                            {
                                abnControl.PatientCode = patient_code.ToString();
                            }

                            abnControl.ProductName = dispenseItem.Name.ToString();
                            abnControl.ProductCost = ConvertCurrencyToDecimal(dispenseItem.ActualChargeBilled);
                            abnControl.AbnReason = dispenseItem.AbnReasonText;
                            abnControl.MedicareOption = dispenseItem.MedicareOption;
                            abnControl.ABNType = dispenseItem.ABNType;
                            abnControl.LoadControlInfo();
                            ctlPatientReceipt.Controls.Add(abnControl);
//                            ctlPatientReceipt.Controls.Add(new LiteralControl("<br/><br/>"));
                        }else if (dispenseItem.ABNType == "Commercial")
                        {
                            #region get location address information
                            var addressLine1 = "";
                            var addressLine2 = "";
                            var city = "";
                            var state = "";
                            var zipCode = "";

                            var dalPracticeLocationShippingAddress = new DAL.PracticeLocation.ShippingAddress();
                            var practiceLocationShippingAddress = dalPracticeLocationShippingAddress.Select(practiceLocationID);
                            if (practiceLocationShippingAddress != null)
                            {
                                addressLine1 = practiceLocationShippingAddress.ShipAddress1;
                                addressLine2 = practiceLocationShippingAddress.ShipAddress2;
                                city = practiceLocationShippingAddress.ShipCity;
                                state = practiceLocationShippingAddress.ShipState;
                                zipCode = practiceLocationShippingAddress.ShipZipCode;
                            }

                            var contact = new DAL.PracticeLocation.Contact().Select(practiceLocationID);
                            #endregion

                            ABNFormCtlCommercial abnControlCommercial = page.LoadControl(string.Format("{0}ABNFormCtlCommercial.ascx", emailTemplatePath)) as ABNFormCtlCommercial;   //("/App_Resource/

                            abnControlCommercial.PracticeName = dispensement.PracticeName;
                            abnControlCommercial.Address1 = addressLine1;
                            abnControlCommercial.Address2 = addressLine2;
                            abnControlCommercial.City = city;
                            abnControlCommercial.State = state;
                            abnControlCommercial.Zip = zipCode;

                            if (contact != null)
                            {
                                abnControlCommercial.BillingPhone = contact.PhoneWork;
                                abnControlCommercial.BillingFax = contact.Fax;
                            }
                           
                            abnControlCommercial.AbnReason = dispenseItem.AbnReasonText;
                            abnControlCommercial.ProductName = dispenseItem.Name.ToString();
                            abnControlCommercial.MedicareOption = dispenseItem.MedicareOption;
                            if (!string.IsNullOrEmpty(patient_code))
                            {
                                abnControlCommercial.PatientCode = patient_code.ToString();
                            }
                            abnControlCommercial.PatientName = (dispensement.PatientFirstName ?? string.Empty) + " " + (dispensement.PatientLastName ?? string.Empty);
                            abnControlCommercial.ProductCost = ConvertCurrencyToDecimal(dispenseItem.ActualChargeBilled);
                            if (dispensement.PatientSignatureInfo != null)
                            {
                                abnControlCommercial.PatientSignatureID = dispensement.PatientSignatureInfo.DispenseSignatureID;
                            }
                            abnControlCommercial.FromVision = fromVision;
                            if (dispensement.PatientSignedDate != null && dispensement.PatientSignedDate.HasValue)
                            {
                                abnControlCommercial.PatientSignedDate = dispensement.PatientSignedDate.Value;
                            }
                            abnControlCommercial.LoadControlInfo();
                            ctlPatientReceipt.Controls.Add(abnControlCommercial);
                        }

                        #endregion

                        // Make a capped rental notification form for each item that requires it
                        #region Capped Rental Notification

                        if (IsDispenseItemRoutinelyPurchased(dispenseItem))
                            
                        {
                            Notification notification = page.LoadControl(string.Format("{0}Notification.ascx", emailTemplatePath)) as Notification;
                            notification.productName = dispenseItem.Name;
                            notification.FromVision = (fromVision && !createPDF);
                            if (dispensement.PatientSignatureInfo != null)
                            {
                                notification.PatientSignatureID = dispensement.PatientSignatureInfo.DispenseSignatureID;
                            }
                            if (dispensement.PatientSignedDate != null && dispensement.PatientSignedDate.HasValue)
                            {
                                notification.PatientSignedDate = dispensement.PatientSignedDate.Value;
                            }

                            notification.LoadControlInfo();
                            ctlPatientReceipt.Controls.Add(notification);
                        }

                        #endregion
                    }

                    #endregion

                    #region Supplier Standards

                    if (isMedicareQuery)
                    {
                        SupplierStandard supplierStandard = page.LoadControl(string.Format("{0}SupplierStandard.ascx", emailTemplatePath)) as SupplierStandard;

                        supplierStandard.LoadControlInfo();

                        ctlPatientReceipt.Controls.Add(supplierStandard);
                        //                        ctlPatientReceipt.Controls.Add(new LiteralControl("<br/><br/>"));
                    }

                    #endregion

                    #endregion

                    #region render html

                    var sb = new StringBuilder();
                    var sw = new StringWriter(sb);
                    var htmlTW = new HtmlTextWriter(sw);
                    try { ctlPatientReceipt.RenderControl(htmlTW); } catch { }
                    var html = sb.ToString();

                    if (counter == 1)
                    {
                        html = "<div id=\"divNonPrintable\"><input type=\"button\" value=\"Print\" onclick=\"PrintContent()\" /></div>" + html;
                    }

                    if (createPDF || hidePrintButton)
                    {
                        html = html.Replace("<div id=\"divNonPrintable\">", "<div id=\"divNonPrintable\" style=\"display:none\">"); // Hide "Print" button in the PDF file added by u= 
                    }

                    completeString.Append(html);

                    #endregion

                    #region render pdf

                    string filename = "";
                    try
                    {
                        string path = HostingEnvironment.MapPath(string.Format("~/{0}DispenseReceipts", virtualDirName));

                        bool savePdfToFile = (!string.IsNullOrEmpty(path) && createPDF);
                        bool emrIntegrationDispenseReceiptPushEnabled = dispensement.EMRIntegrationDispenseReceiptPushEnabled;
                        bool emrIntegrationHL7PushEnabled = dispensement.EMRIntegrationHL7PushEnabled;
                        string dispenseReceiptFileName = string.Format("DispenseReceipt{0}.pdf", dispensement.DispenseID);

                        if (savePdfToFile || emrIntegrationDispenseReceiptPushEnabled || (pdfMemoryStream != null))
                        {
                            filename = Path.Combine(path, dispenseReceiptFileName); //string.Format("{0}/test.pdf");

                            PDFParameter pdfParameter = new PDFParameter();
                            pdfParameter.html = html;
                            pdfParameter.filename = filename;
                            pdfParameter.practiceID = dispensement.PracticeID;
                            pdfParameter.practiceLocationID = practiceLocationID;
                            pdfParameter.patientCode = patient_code;
                            pdfParameter.emrEnabled = dispensement.EMREnabled;
                            pdfParameter.dispenseDate = dispensement.DateDispensed;

                            var documents = GetActiveCustomDocuments(dispensement.PracticeID);

                            using (var tempFile = new TempFileManager())
                            {
                                CreatePDF(pdfParameter, tempFile, documents);

                                if (emrIntegrationDispenseReceiptPushEnabled || (pdfMemoryStream != null))
                                {
                                    byte[] tempFileBytes = tempFile.GetBytes();

                                    if (queueOutboundMessages && emrIntegrationDispenseReceiptPushEnabled)
                                    {
                                        string pushFilename = string.IsNullOrEmpty(patient_code) ? dispenseReceiptFileName : (patient_code + ".pdf");
                                        PushDispenseReceiptPdf(tempFile, dispensement.PracticeID, pushFilename, vcc_schedule_id, dispensement.DispenseID);
                                    }

                                    if (pdfMemoryStream != null)
                                        pdfMemoryStream.Write(tempFileBytes, 0, tempFileBytes.Length);
                                }

                                if (savePdfToFile)
                                    tempFile.CopyToPath(pdfParameter.filename);
                            }
                        }

                        if (queueOutboundMessages && emrIntegrationHL7PushEnabled && !string.IsNullOrEmpty(vcc_schedule_id))
                        {
                            var client = new EMRIntegrationReference.EMRIntegrationClient();
                            var dispense_items = new List<OutboundHL7DispenseItem>();
                            var diagnosis_codes = new List<DiagnosisCode>();
                            var patientSignatureDate = dispensement.PatientSignedDate;
                            var physicianSignatureDate = dispensement.PhysicianSignedDate;
                            foreach (var item in dispensement.DispenseItems)
                            {
                                var qty = item.QuantityToDispense.HasValue ? item.QuantityToDispense.Value : 0;
                                decimal unitprice = 0m;
                                unitprice = ConvertCurrencyToDecimal(item.ActualChargeBilled);
                                decimal dmedeposit = 0m;
                                dmedeposit = ConvertCurrencyToDecimal(item.DMEDeposit);
                                dispense_items.Add(
                                    new OutboundHL7DispenseItem
                                    {
                                        unit_price = unitprice,
                                        quantity = qty,
                                        hcpcs_code = item.HCPCs,
                                        item_code = item.Code,
                                        modifier1 = item.LRModifier,
                                        Gender = item.Gender,
                                        Size = item.Size,
                                        dispense_date = dispensement.DateDispensed,
                                        abn_reason = item.AbnReasonText,
                                        is_abn = string.IsNullOrEmpty(item.ABNType) ? false : true,
                                        dme_deposit = dmedeposit,
                                        is_medicare = item.IsMedicare,
                                        item_name = item.ShortName,
                                        supplier_name = item.SupplierName,
                                        patient_signature_date = patientSignatureDate,
                                        physician_signature_date = physicianSignatureDate,
                                    });
                                diagnosis_codes.Add(new DiagnosisCode { Code = item.ICD9Code, Type = DiagnosisCodeType.ICD9 }); // TODO: handle ICD10 in future
                            }
                            client.QueueOutboundHL7Request(dispensement.PracticeID, vcc_schedule_id, dispense_items.ToArray(), diagnosis_codes.ToArray(), dispensement.DispenseID, dispensement.CloudConnectId);
                        }
                    }
                    catch (Exception e)
                    {
                        VisionErrHandler.Current.RaiseError(e);
                    }

                    #endregion

                    #region build e-mail recipient list

                    var patientMailTo = "";
                    var printDispenseMailTo = "";

                    // add patient
                    if (string.IsNullOrEmpty(dispensement.PatientEmail) == false)
                    {
                        patientMailTo = dispensement.PatientEmail;
                    }

                    // add admin print dispense (only if dispensement is physician signed)
                    if ((string.IsNullOrEmpty(dispensement.EmailForPrintDispense) == false) && dispensement.CreatedWithHandheld.HasValue && (dispensement.CreatedWithHandheld == true))
                    {
                        printDispenseMailTo = dispensement.EmailForPrintDispense;
                    }

                    #endregion

                    #region send e-mail

                    if (sendEmail == true)
                    {
                        var locationName =
                            String.IsNullOrEmpty(dispensement.PracticeName.Trim())
                                ? String.Empty
                                : (dispensement.PracticeName.Trim() + " ");
                        var subject = locationName + "DME Dispensement Information";
                        if (!dispensement.IsPhysicianSigned)
                            subject += " without Physician Signature";

                        var sessionProvider = new VisionSessionProvider(practiceLocationID);

                        if (createPDF == false)
                        {
                            if (!string.IsNullOrEmpty(patientMailTo.Trim()))
                                SendEmail("OrderConfirmation", patientMailTo, html, subject, BLL.SecureMessaging.MessageClass.Patient, sessionProvider);
                            if (!string.IsNullOrEmpty(printDispenseMailTo.Trim()))
                                SendEmail("OrderConfirmation", printDispenseMailTo, html, subject, BLL.SecureMessaging.MessageClass.PrintDispensement, sessionProvider);
                        }
                        else
                        {
                            #region render html e-mail body
                            var bodySB = new StringBuilder();
                            var bodySW = new StringWriter(bodySB);
                            var bodyHTMLTW = new HtmlTextWriter(bodySW);
                            try { ctlPatientEmailBody.RenderControl(bodyHTMLTW); }
                            catch { }
                            var bodyHTML = bodySB.ToString();
                            #endregion

#if(DEBUG)
                            if (!string.IsNullOrEmpty(patientMailTo.Trim()))
                                SendEmail("OrderConfirmation", patientMailTo, bodyHTML + "<br/><br/>" + html, string.Format(subject), BLL.SecureMessaging.MessageClass.Patient, sessionProvider);
                            if (!string.IsNullOrEmpty(printDispenseMailTo.Trim()))
                                SendEmail("OrderConfirmation", printDispenseMailTo, bodyHTML + "<br/><br/>" + html, string.Format(subject), BLL.SecureMessaging.MessageClass.PrintDispensement, sessionProvider);
#else
                            if (!string.IsNullOrEmpty(patientMailTo.Trim()))
                                SendEmail("OrderConfirmation", patientMailTo, bodyHTML, subject, filename, BLL.SecureMessaging.MessageClass.Patient, sessionProvider);
                            if (!string.IsNullOrEmpty(printDispenseMailTo.Trim()))
                                SendEmail("OrderConfirmation", printDispenseMailTo, bodyHTML, subject, filename, BLL.SecureMessaging.MessageClass.PrintDispensement, sessionProvider);
#endif
                        }
                    }

                    #endregion

                    #region send fax (only if physician signed (if HCP signature option is enabled) and created by handheld)

                    if ((sendFax == true)
                        && ((practiceLocation.IsHcpSignatureRequired == false) || (dispensement.IsPhysicianSigned == true))
                        && (dispensement.CreatedWithHandheld == true))
                    {
                        var faxnum = (from pl in visionDB.PracticeLocations
                                      where pl.PracticeLocationID == practiceLocationID
                                      select pl.FaxForPrintDispense).FirstOrDefault();
                        var fax_enabled =
                                visionDB.PracticeLocations
                                .Where(pl => pl.PracticeLocationID == practiceLocationID)
                                .Any(pl => pl.Practice.FaxDispenseReceiptEnabled);

                        if (fax_enabled && !string.IsNullOrEmpty(faxnum))
                        {
                            var f = new Fax();
                            if (!createPDF)
                                f.SendFax(faxnum, html);
                            else
                                f.SendFax(faxnum, filename, 0, J2.eFaxDeveloper.FaxFileType.pdf);
                        }
                    }

                    #endregion

                    #region clean up rendered pdf file

                    // NOTE: DO NOT CLEAN UP PDF FILE BECAUSE IT MAY BE QUEUED FOR TRANSMISSION TO EFAX
                    //if (createPDF)
                    //{
                    //    try { File.Delete(filename); }
                    //    catch { }
                    //}

                    #endregion

                    counter++;
                }
            }

            return completeString;
        }

        private static string GetCloudConnectRemoteScheduleItemId(ref string patient_code)
        {
            string vcc_schedule_id = null;
            var prefix = " (SID:";
            var suffix = ")";

            // exit routine early if there is no patient_code
            if (patient_code == null || patient_code.Length < prefix.Length)
                return vcc_schedule_id;

            // get position of sid marker
            var p1 = patient_code.IndexOf(prefix);
            if (p1 >= 0)
            {
                var o = new StringBuilder();
                // copy everything up to the marker
                o.Append(patient_code.Substring(0, p1));
                // read marker all the way through to the right-parenthesis
                var sid = patient_code.Substring(p1);
                var p2 = sid.IndexOf(suffix);
                // cap off marker and "fix" o so it doesn't contain the marker
                if (p2 > -1)
                {
                    // copy everything past end of marker into o
                    o.Append(sid.Substring(p2 + 1));
                    // cap off sid so it's just the sid
                    sid = sid.Substring(0, p2);
                }
                // strip off the prefix of sid
                sid = sid.Replace(prefix, "");

                // replace patient code so it doesn't contain the sid
                patient_code = o.ToString();

                // return the sid to caller
                vcc_schedule_id = sid;
            }
            return vcc_schedule_id;
        }

        public static void PushDispenseReceiptPdf(TempFileManager tempFileManager, int practiceId, string patientCode, string vccScheduleId, int dispenseId)
        {
            var client = new EMRIntegrationClient();
            client.QueueOutboundFile(practiceId, patientCode, tempFileManager.GetBytes(), vccScheduleId, dispenseId);
        }

        public static void CreatePDF(PDFParameter pdfParameter, TempFileManager tempFileManager, byte[][] attachments)
        {
            var client = new CreatePdfClient();
            if (attachments != null && attachments.Length > 0)
            {
                client.CreatePdfWithAttachments(pdfParameter.html, tempFileManager.FullTempfilePath, attachments);
            }
            else
            {
                client.CreatePdf(pdfParameter.html, tempFileManager.FullTempfilePath);
            }
        }

        public static void SendBillingNotificationEmail(int practiceLocationID, bool fromVision = false, InventoryComplete.ConsignmentEmailType emailType = InventoryComplete.ConsignmentEmailType.ConsignmentInventoryComplete)
        {
            InventoryDiscrepencyItem[] discrepencies = ConsignmentItem.GetConsignmentReplenishmentList(practiceLocationID);

            if (discrepencies.Length > 0)
            {

                string html = GetConsignmentEmailHtml(practiceLocationID, fromVision, emailType);


                var MailTo = GetConsignmentEmailAddress();

                if (string.IsNullOrEmpty(MailTo.Trim()) == false)
                {
                    SendEmail("OrderConfirmation", MailTo, html, "DME Dispensement Information - Billing", BLL.SecureMessaging.MessageClass.Billing, new VisionSessionProvider(practiceLocationID));
                }

            }
        }

        private static string GetConsignmentEmailAddress()
        {
            //    //--mws was turned on
            var MailTo = "";

            MailTo = System.Configuration.ConfigurationManager.AppSettings["ConsignmentAdminEmailAddress"];

#if(DEBUG)
            MailTo += ";klord@breg.com";
            if (!MailTo.ToLower().Contains("sneen"))
            {
                MailTo += ";michael_sneen@yahoo.com";
            }
#endif
            return MailTo;
        }

        public static string GetConsignmentEmailHtml(int practiceLocationID, bool fromVision, InventoryComplete.ConsignmentEmailType emailType = InventoryComplete.ConsignmentEmailType.ConsignmentInventoryComplete)
        {
            string path = "";
            string templateName = "~/{0}EmailTemplates/InventoryComplete.ascx";
            string virtualDirName = "";
            string html = "";

            if (fromVision == true)
            {
                virtualDirName = "BregWcfService/";
            }

            path = string.Format(templateName, virtualDirName);

            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var practiceQuery = from p in visionDB.Practices
                                    join pl in visionDB.PracticeLocations on p.PracticeID equals pl.PracticeID
                                    where pl.PracticeLocationID == practiceLocationID
                                    select new
                                    {
                                        PracticeName = p.PracticeName,
                                        PracticeLocationName = pl.Name
                                    };

                var practiceInfo = practiceQuery.FirstOrDefault();

                Page page = new Page();
                var ctlAll = new System.Web.UI.WebControls.Panel();

                //string x = BLL.Website.GetAbsPath(path); ;

                BregWcfService.EmailTemplates.InventoryComplete inventoryComplete = page.LoadControl(path) as BregWcfService.EmailTemplates.InventoryComplete;
                inventoryComplete.PracticeName = practiceInfo.PracticeName;
                inventoryComplete.PracticeLocationName = practiceInfo.PracticeLocationName;
                inventoryComplete.PracticeLocationID = practiceLocationID;
                inventoryComplete.EmailType = emailType;

                inventoryComplete.LoadControlInfo();
                ctlAll.Controls.Add(inventoryComplete);
                ctlAll.Controls.Add(new LiteralControl("<br/><br/>"));

                var sb = new StringBuilder();
                var sw = new StringWriter(sb);
                var htmlTW = new HtmlTextWriter(sw);
                ctlAll.RenderControl(htmlTW);

                html = sb.ToString();

            }
            return html;
        }

        public static bool IsDispenseItemRoutinelyPurchased(DispenseItemDto dispenseItemDto)
        {

            string hcpcsCode = dispenseItemDto.HCPCs;
            if (hcpcsCode == null)
            {
                return false;
            }

            hcpcsCode = hcpcsCode.Trim().ToUpper();

            if (!dispenseItemDto.IsMedicare)
            {
                return false;
            }

            if (!hcpcsCode.StartsWith("E"))
            {
                return false;
            }

            bool isValidHcpcs = IsValidHcpcs(hcpcsCode);

            return isValidHcpcs;
        }

        public static bool IsValidHcpcs(string code)
        {
            if (code == null || code.Length < 5)
            {
                return false;
            }

            var trimmedCode = code.Trim().Substring(0,5);

            using (var visionDb = VisionDataContext.GetVisionDataContext())
            {
                return visionDb.HCPCs.Any(h => h.Code == trimmedCode);
            }
        }

        public static decimal ConvertCurrencyToDecimal(string currency)
        {
            decimal decimalValue;
            if (decimal.TryParse(currency, NumberStyles.Currency, NumberFormatInfo.CurrentInfo, out decimalValue))
            {
                return decimalValue;
            }
            return -1;
        }

        public static byte[][] GetActiveCustomDocuments(int practiceId)
        {
            var ret = new List<byte[]>();
            var client = new BlobService.BlobServiceClient();

            using (var db = VisionDataContext.GetVisionDataContext())
            {
                var activeCustomForms = from cf
                                        in db.GetTable<DispenseReceiptCustomForm>()
                                        where cf.PracticeId == practiceId
                                        where cf.IsActive
                                        orderby cf.BlobOrder
                                        select cf;

                foreach (var form in activeCustomForms)
                {
                    var path = practiceId + "\\" + form.Name;
                    var file = client.GetFile(path);
                    ret.Add(file);
                }
            }

            return ret.ToArray();
        }
    }
}
