﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web.Security;
using ClassLibrary.DAL;

namespace BregWcfService.Classes
{
    public class Device : ClassLibrary.DAL.Device
    {
        /// <summary>
        /// Verifies that deviceIdentifier has been authorized and optionally updates last GPS location
        /// if lastLocation is not null
        /// </summary>
        /// <param name="deviceIdentifier"></param>
        /// <param name="lastLocation"></param>
        /// <returns></returns>
        public static bool SimpleValidateDevice(string deviceIdentifier, string lastLocation)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                // get active records that match the device id
                var query =
                    visionDB.Devices
                    .Where(
                        d =>
                            (d.DeviceIdentifier == deviceIdentifier.Trim())
                            && d.IsActive
                    );

                // check if there's at least 1 match
                var device = query.FirstOrDefault();
                if (device != null)
                {
                    // update location if one was provided
                    if (!string.IsNullOrEmpty(lastLocation))
                    {
                        device.LastLocation = lastLocation;
                        visionDB.SubmitChanges();
                    }
                    return true;
                }
                /* no device match means device not authorized, send back false and allow caller to decide
                 * if authorization is needed */
                else
                {
                    throw new ApplicationException("Device not authorized.");
                }
            }
        }

        public static bool ValidateOrRegisterDeviceForAuthorization(string deviceIdentifier, string lastLocation, string userName, int? practiceID)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                // get active records that match the device id
                var query =
                    visionDB.Devices
                    .Where(
                        d =>
                            (d.DeviceIdentifier == deviceIdentifier.Trim())
                            && d.IsActive
                    );

                // if practice information is provided, then only match records that have a practice set
                if (practiceID.HasValue)
                    query = query.Where(d => d.PracticeID.HasValue && (d.PracticeID.Value == practiceID.Value));
                // if practice is not provided, then only match records that don't have a practice set
                else
                    query = query.Where(d => !d.PracticeID.HasValue);

                // check if there's at least 1 match
                var device = query.FirstOrDefault();
                if (device != null)
                {
                    // update location if one was provided
                    if (!string.IsNullOrEmpty(lastLocation))
                    {
                        device.LastLocation = lastLocation;
                        visionDB.SubmitChanges();
                    }
                    return true;
                }
                /* no device match means device not authorized, send back false and allow caller to decide
                 * if authorization is needed */
                else
                {
                    // always allow apple test account to validate
                    if (userName.ToLower().Trim().Equals("appletest"))
                    {
                        CreateAuthorizedDevice(deviceIdentifier, practiceID);
                        return true;
                    }


                    SendDeviceAuthorizationRequest(deviceIdentifier, userName, practiceID);
                    throw new LoginException("Authorization for this device is pending.");
                }
            }
        }

        private static void CreateAuthorizedDevice(string deviceIdentifier, int? practiceID)
        {
            var requestId = CreateDeviceAuthorization(deviceIdentifier, practiceID);
            ConfirmDeviceAuthorization(requestId.Value);
        }

        public static void SendDeviceAuthorizationRequest(string deviceIdentifier, string userName, int? practiceID)
        {
            // create new authorization request
            var request_id = CreateDeviceAuthorization(deviceIdentifier, practiceID);

            // send authorization request e-mail
            if (request_id.HasValue)
            {
                SendAuthorizationRequestNotificationEmail(
                    request_id.Value,
                    deviceIdentifier,
                    userName,
                    practiceID
                );
            }
        }

        private static void SendAuthorizationRequestNotificationEmail(Guid request_id, string deviceIdentifier, string userName, int? practiceID)
        {
            // look up practice information
            var practicename = (string)null;
            if (practiceID.HasValue)
            {
                using (var visionDB = VisionDataContext.GetVisionDataContext())
                {
                    var p =
                        visionDB.Practices
                        .Where(x => x.PracticeID == practiceID.Value)
                        .FirstOrDefault();
                    if (p != null)
                        practicename = p.PracticeName;
                }
            }
            
            var deviceAuthBaseUrl = System.Configuration.ConfigurationManager.AppSettings["DeviceAuthorizationEndpointBaseURL"];

            // generate and send e-mail
            var approval_url = string.Format(
                @"{0}/BregWcfService/ApproveDeviceRequest.ashx?r={1}",
                deviceAuthBaseUrl,
                request_id.ToString("N")
            );
            var msgbody = string.Format(
                @"<html><body><pre>
Device authorization requested by:

Username: {0}
Practice: {1}
Device ID: {2}

To approve, click on this link:
<a href=""{3}"">{3}</a>
</pre></body></html>
",
                userName,
                practicename ?? "N/A",
                deviceIdentifier,
                approval_url
            );
            var notification_recipients = System.Configuration.ConfigurationManager.AppSettings["DeviceAuthorizationRecipients"];
            var authorization_subject = System.Configuration.ConfigurationManager.AppSettings["DeviceAuthorizationSubject"] ?? "New Vision Device Authorization Request";
            if (!string.IsNullOrEmpty(notification_recipients))
                Email.SendEmail("OrderConfirmation", notification_recipients, msgbody, authorization_subject, BLL.SecureMessaging.MessageClass.BregMaintenance);
        }

        public static Guid? CreateDeviceAuthorization(string deviceIdentifier, int? practiceID)
        {
            try
            {
                using (var visionDB = VisionDataContext.GetVisionDataContext())
                {
                    var existing_req =
                        visionDB.DeviceAuthenticationRequests
                        .Where(
                            x =>
                                (x.DeviceIdentifier == deviceIdentifier.Trim())
                                && (x.PracticeID == practiceID)
                        )
                        .FirstOrDefault();

                    if (existing_req == null)
                    {
                        var req_id = Guid.NewGuid();

                        var request =
                            new DeviceAuthenticationRequest
                            {
                                CreatedDate = DateTime.Now,
                                DeviceIdentifier = deviceIdentifier,
                                PracticeID = practiceID,
                                PracticeLocationID = null,
                                RequestID = req_id
                            };
                        visionDB.DeviceAuthenticationRequests.InsertOnSubmit(request);
                        visionDB.SubmitChanges();

                        return req_id;
                    }
                    else
                    {
                        return existing_req.RequestID;
                    }
                }
            }
            catch
            {
                return null;
            }
        }

        public static bool ConfirmDeviceAuthorization(Guid authorizationRequestId)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var request =
                    visionDB.DeviceAuthenticationRequests
                    .Where(x => x.RequestID == authorizationRequestId)
                    .FirstOrDefault();

                if (request != null)
                {
                    var new_device =
                        new ClassLibrary.DAL.Device
                        {
                            DeviceIdentifier = request.DeviceIdentifier,
                            PracticeID = request.PracticeID,
                            PracticeLocationID = request.PracticeLocationID,
                            IsActive = true,
                            CreatedDate = DateTime.Now,
                            CreatedUserID = 1
                        };

                    visionDB.Devices.InsertOnSubmit(new_device);
                    visionDB.DeviceAuthenticationRequests.DeleteOnSubmit(request);
                    visionDB.SubmitChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool AddDevice(string deviceIdentifier, string lastLocation, int? practiceID, int? practiceLocationID, int userID)
        {
            throw new ApplicationException("Deprecated call");
        }

        //public static bool AddDevice(string deviceIdentifier, string lastLocation, int? practiceID, int? practiceLocationID, int userID)
        //{
        //    using (var visionDB = VisionDataContext.GetVisionDataContext())
        //    {
        //        var query = from d in visionDB.Devices
        //                    where d.DeviceIdentifier == deviceIdentifier.Trim()  //mws enable this when we have devices                            
        //                    select d;

        //        ClassLibrary.DAL.Device device = query.FirstOrDefault();

        //        if (device == null)
        //        {
        //            device = new ClassLibrary.DAL.Device();

        //            device.DeviceIdentifier = deviceIdentifier;
        //            device.PracticeID = practiceID;
        //            device.PracticeLocationID = practiceLocationID;
        //            device.CreatedUserID = userID;
        //            device.CreatedDate = DateTime.Now;

        //            visionDB.Devices.InsertOnSubmit(device);
        //        }

        //        if (!string.IsNullOrEmpty(lastLocation))
        //        {
        //            device.LastLocation = lastLocation;
        //        }

        //        device.ModifiedUserID = userID;
        //        device.ModifiedDate = DateTime.Now;

        //        device.IsActive = true;
        //        visionDB.SubmitChanges();

        //        return true;

        //    }

        //}
    }
}