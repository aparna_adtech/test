﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BregWcfService
{
    public class Utilities
    {
        internal static List<List<int>> BuildIdChunks(List<int> bigListOfIDs)
        {
            var blocksOfIDs = new List<List<int>>();
            for (var i = 0; i <= (int)(bigListOfIDs.Count() / 2000); i++)
            {
                var remaining = bigListOfIDs.Count() - (i * 2000);
                var aBlockOfIDs =
                    bigListOfIDs.GetRange(
                        i * 2000,
                        (remaining > 2000) ? 2000 : remaining
                    );
                blocksOfIDs.Add(aBlockOfIDs);
            }
            return blocksOfIDs;
        }
    }
}