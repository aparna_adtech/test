﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BregWcfService.DataContracts;
using System.ServiceModel.Activation;

namespace BregWcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SchedulePatientsService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SchedulePatientsService.svc or SchedulePatientsService.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class SchedulePatientsService : ISchedulePatientsService
    {
        VisionService _visionService = new VisionService();
        public ScheduleItem[] GetSchedulePatients(int practiceId, int practiceLocationId, DateTime utcStartDate, string userName, string password, string deviceID, string lastLocation, int skip, int take)
        {
            var items = _visionService.GetScheduledPatientsRange(practiceId, practiceLocationId, utcStartDate, null, userName, password, deviceID, lastLocation, skip, take);

            return TranslateScheduleItems(items);
        }

        /// <summary>
        /// Translates between the EMRIntegration service model and the BregVisionWcf model
        /// </summary>
        /// <returns></returns>
        private ScheduleItem[] TranslateScheduleItems(EMRIntegrationReference.schedule_item[] items)
        {
            List<ScheduleItem> newItems = new List<ScheduleItem>();

            if (items == null)
            {
                return newItems.ToArray();
            }

            foreach (var item in items)
            {
                ScheduleItem newItem = new ScheduleItem()
                {
                    CreationDate = item.creation_date,
                    DiagnosisICD9 = item.diagnosis_icd9,
                    IsActive = item.is_active,
                    IsCheckedIn = item.is_checked_in,
                    Message = item.message,
                    PatientDOB = item.patient_dob,
                    PatientEmail = item.patient_email,
                    PatientFirstName = item.patient_first_name,
                    PatientId = item.patient_id,
                    PatientLastName = item.patient_last_name,
                    PatientName = item.patient_name,
                    PhysicianName = item.physician_name,
                    PracticeId = item.practice_id,
                    PracticeLocationId = item.practice_location_id,
                    RemoteScheduleItemId = item.remote_schedule_item_id,
                    ScheduledVisitDateTime = item.scheduled_visit_datetime,
                    ScheduledVisitDatetimeTimezoneId = item.scheduled_visit_datetime_timezoneid,
                    ScheduleItemId = item.schedule_item_id,
                    LastModifiedOn = item.last_modify_date
                };

                newItems.Add(newItem);
            }

            return newItems.ToArray();
        }
    }
}
