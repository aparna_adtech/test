﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BregWcfService
{
    /// <summary>
    /// Summary description for ApproveDeviceRequest
    /// </summary>
    public class ApproveDeviceRequest : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            // set up output content type mime-type
            context.Response.ContentType = "text/plain";

            // clean up incoming request id
            var request_id = context.Request["r"];
            if (string.IsNullOrEmpty(request_id) || (request_id.Length != Guid.Empty.ToString("N").Length))
                request_id = "invalid";
            
            // parse request id into Guid and try to confirm authorization if valid
            // return status of approval or bad request
            try
            {
                Guid request_id_parsed;
                if (Guid.TryParse(request_id, out request_id_parsed))
                    context.Response.Write(Classes.Device.ConfirmDeviceAuthorization(request_id_parsed) ? "Approved" : "Already approved");
                else
                    context.Response.Write("Bad request");
            }
            catch (Exception ex)
            {
                context.Response.Write(ex.ToString());
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}