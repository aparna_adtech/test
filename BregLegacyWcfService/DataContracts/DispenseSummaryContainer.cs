﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BregWcfService
{
    [DataContract]
    public class DispenseSummaryContainer
    {
        [DataMember]
        public DispenseQueueSummaryItem[] DispenseQueueSummary { get; set; }

        [DataMember]
        public DispenseSummaryItem[] PhysicianUnsignedDispensementsSummary { get; set; }

        [DataMember]
        public DispenseSummaryItem[] LastDispensementsSummary { get; set; }
    }
}