﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BregWcfService.DataContracts
{
    [DataContract]
    public class ScheduleItem
    {
        [DataMember]
        public System.DateTime CreationDate { get; set; }

        [DataMember]
        public string DiagnosisICD9 { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public bool IsCheckedIn { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public System.Nullable<System.DateTime> PatientDOB { get; set; }

        [DataMember]
        public string PatientEmail { get; set; }

        [DataMember]
        public string PatientFirstName { get; set; }

        [DataMember]
        public string PatientId { get; set; }

        [DataMember]
        public string PatientLastName { get; set; }

        [DataMember]
        public string PatientName { get; set; }

        [DataMember]
        public string PhysicianName { get; set; }

        [DataMember]
        public int PracticeId { get; set; }

        [DataMember]
        public int PracticeLocationId { get; set; }

        [DataMember]
        public string RemoteScheduleItemId { get; set; }

        [DataMember]
        public System.Guid ScheduleItemId { get; set; }

        [DataMember]
        public System.DateTime ScheduledVisitDateTime { get; set; }

        [DataMember]
        public string ScheduledVisitDatetimeTimezoneId { get; set; }

        [DataMember]
        public DateTime? LastModifiedOn { get; internal set; }
    }
}