﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using ClassLibrary.DAL;

namespace BregWcfService.DataContracts
{
    [DataContract]
    public class PhysicianDataForPractice
    {
        [DataMember]
        public int PracticeId { get; set; }

        [DataMember]
        public int PhysicianId { get; set; }

        [DataMember]
        public int ContactId { get; set; }

        [DataMember]
        public string Salutation { get; set; }

        [DataMember]
        public string PhysicianName { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string MiddleName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string Suffix { get; set; }

        [DataMember]
        public string Pin { get; set; }

        [DataMember]
        public int SortOrder { get; set; }

        [DataMember]
        public int IsActive { get; set; }

        [DataMember]
        public List<int?> PracticeLocationIds { get; set; }

        public static PhysicianDataForPractice[] GetPhysiciansForPractice(int practiceID)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                return visionDB.usp_Physician_SelectALL_Locations_ByPracticeID(practiceID)
                    .GroupBy(p => p.PhysicianID)
                    .Select(g => new PhysicianDataForPractice
                    {
                        PracticeId = g.First().PracticeID,
                        PhysicianId = g.First().PhysicianID,
                        ContactId = g.First().ContactID,
                        Salutation = g.First().Salutation,
                        FirstName = g.First().FirstName,
                        MiddleName = g.First().MiddleName,
                        LastName = g.First().LastName,
                        Suffix = g.First().Suffix,
                        Pin = g.First().Pin,
                        SortOrder = g.First().SortOrder,
                        IsActive = g.First().IsActive,
                        PracticeLocationIds = g.Select(v => v.PracticeLocationID).Distinct().ToList(),
                        PhysicianName = g.First().PhysicianName
                    })
                    .ToArray();
            }
        }
    }
}