﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web.Security;
using ClassLibrary.DAL;
using System.Collections.Generic;

namespace BregWcfService
{
    [DataContract]
    public class PracticeLocation
    {
        [DataMember]
        public int PracticeLocationID { get; set; }

        [DataMember]
        public string PracticeLocationName { get; set; }

        [DataMember]
        public bool IsHcpSignatureRequired { get; set; }

        [DataMember]
        public int PracticeID { get; set; }

        [DataMember]
        public int? AddressID { get; set; }

        [DataMember]
        public int? ContactID { get; set; } 

        [DataMember]
        public int? TimeZoneID { get; set; }

        [DataMember]
        public string Name { get; set; } 

        [DataMember]
        public bool? IsPrimaryLocation { get; set; }

        [DataMember]
        public string EmailForOrderApproval { get; set; }

        [DataMember]
        public string EmailForOrderConfirmation { get; set; }

        [DataMember]
        public string EmailForFaxConfirmation { get; set; } 

        [DataMember]
        public string EmailForLowInventoryAlerts { get; set; } 

        [DataMember]
        public bool? IsEmailForLowInventoryAlertsOn { get; set; }

        [DataMember]
        public int CreatedUserID { get; set; } 

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public int? ModifiedUserID { get; set; }

        [DataMember]
        public DateTime? ModifiedDate { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public string EmailForBillingDispense { get; set; }

        [DataMember]
        public string EmailForPrintDispense { get; set; }

        [DataMember]
        public string FaxForPrintDispense { get; set; }

        [DataMember]
        public string GeneralLedgerNumber { get; set; }


    }
}