﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web.Security;
using ClassLibrary.DAL;
using System.Collections.Generic;

namespace BregWcfService
{
    public class CheckInPO : usp_GetOustandingPOsforCheckIn_ParentLevelResult
    {

        [DataMember]
        public CheckInProductInfo[] CheckInProdInfo { get; set; }

        [DataMember]
        public CheckInCustomBraceSupplerInfo[] CustomBraceSupplierInfo { get; set; }

        [DataMember]
        public string PatientID { get; set; }

        public static CheckInPO[] GetPendingCheckIns(int practiceLocationID, int skip, int take)
        {

            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var query = from ci in visionDB.usp_GetOustandingPOsforCheckIn_ParentLevel(practiceLocationID)
                            select new CheckInPO
                            {
                                BregVisionOrderID = ci.BregVisionOrderID,
                                CustomPurchaseOrderCode = ci.CustomPurchaseOrderCode,
                                PurchaseOrder = ci.PurchaseOrder,
                                ShippingType = ci.ShippingType,
                                Status = ci.Status,
                                Total = ci.Total,
                                CreatedDate = ci.CreatedDate,
                                CreatedUserID = ci.CreatedUserID,
                                CheckInProdInfo = (
                                            from ci1 in visionDB.usp_GetOutstandingPOsforCheckIn_Detail1Level(ci.BregVisionOrderID)
                                            where ci1.BregVisionOrderID == ci.BregVisionOrderID
                                            select new CheckInProductInfo
                                            {
                                                BrandShortName = ci1.BrandShortName,
                                                BregVisionOrderID = ci1.BregVisionOrderID,
                                                InvoiceNumber = ci1.InvoiceNumber,
                                                InvoicePaid = ci1.InvoicePaid,
                                                PurchaseOrder = ci1.PurchaseOrder,
                                                Status = ci1.Status,
                                                SupplierOrderID = ci1.SupplierOrderID,
                                                SupplierShortName = ci1.SupplierShortName,
                                                Total = ci1.Total,
                                                CheckInProdExtra =
                                                (
                                                    from ci2 in visionDB.usp_GetOutstandingPOsforCheckIn_Detail2Level(ci1.SupplierOrderID, practiceLocationID)
                                                    where ci1.SupplierOrderID == ci2.SupplierOrderID
                                                    select new CheckInProductExtra
                                                    {
                                                        ActualWholesaleCost = ci2.ActualWholesaleCost,
                                                        Code = ci2.Code,
                                                        Color = ci2.Color,
                                                        Gender = ci2.Gender,
                                                        IsThirdPartyProduct = ci2.IsThirdPartyProduct,
                                                        LeftRightSide = ci2.LeftRightSide,
                                                        LineTotal = ci2.LineTotal,
                                                        Packaging = ci2.Packaging,
                                                        PracticeCatalogProductID = ci2.PracticeCatalogProductID,
                                                        ProductInventoryID = ci2.ProductInventoryID,
                                                        QuantityCheckedIn = ci2.QuantityCheckedIn,
                                                        QuantityOrdered = ci2.QuantityOrdered,
                                                        QuantityToCheckIn = ci2.QuantityToCheckIn,
                                                        ShortName = ci2.ShortName,
                                                        Size = ci2.Size,
                                                        Status = ci2.Status,
                                                        SupplierOrderID = ci2.SupplierOrderID,
                                                        SupplierOrderLineItemID = ci2.SupplierOrderLineItemID,
                                                        SupplierOrderLineItemStatusID = ci2.SupplierOrderLineItemStatusID,
                                                                
                                                    }
                                                ).ToArray()
                                            }
                                          ).ToArray()
                            };

                return query.Skip(skip).Take(take).ToArray<CheckInPO>();

            }
        }

        public static CheckInPO[] GetPendingCustomBraceCheckIns(int practiceLocationID, int skip, int take)
        {
                //    DataSet ds = DAL.PracticeLocation.CheckIn.GetOustandingCustomBracesforCheckIn_ParentLevel(practiceLocationID);
                //    grdCustomBraceOutstanding.DataSource = ds;

                    
                //    grdCustomBraceOutstanding.SetRecordCount(ds, "Status");
                //    //grdCustomBraceOutstanding.SetGridLabel("Search", Page.IsPostBack, lblBrowseCustomBrace);
                //}
                //grdCustomBraceOutstanding.MasterTableView.DetailTables[0].DataSource = DAL.PracticeLocation.CheckIn.GetOustandingCustomBracesforCheckIn_Detail1Level(practiceLocationID);
                //grdCustomBraceOutstanding.MasterTableView.DetailTables[0].DetailTables[0].DataSource = DAL.PracticeLocation.CheckIn.GetOustandingCustomBracesforCheckIn_Detail2Level(practiceLocationID);
                        
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var query = from ci in visionDB.usp_GetOustandingCustomBraceOrdersforCheckIn_ParentLevel(practiceLocationID)
                            select new CheckInPO
                            {
                                BregVisionOrderID = ci.BregVisionOrderID,
                                CustomPurchaseOrderCode = ci.CustomPurchaseOrderCode,
                                PurchaseOrder = ci.PurchaseOrder,
                                ShippingType = ci.ShippingType,
                                Status = ci.Status,
                                Total = ci.Total,
                                CreatedDate = ci.CreatedDate,
                                CreatedUserID = ci.CreatedUserID,
                                PatientID = ci.PatientName,                                
                                CustomBraceSupplierInfo = (from ci1 in visionDB.usp_GetOustandingCustomBraceOrdersforCheckIn_Detail1Level(practiceLocationID)
                                                           where ci.BregVisionOrderID == ci1.BregVisionOrderID
                                                           select new CheckInCustomBraceSupplerInfo
                                                        {
                                                            BrandShortName = ci1.BrandShortName,
                                                            BregVisionOrderID = ci1.BregVisionOrderID,
                                                            PurchaseOrder = ci1.PurchaseOrder,
                                                            Status = ci1.Status,
                                                            SupplierOrderID = ci1.SupplierOrderID,
                                                            SupplierShortName = ci1.SupplierShortName,
                                                            Total = ci1.Total,
                                                            CustomBraceDetail = (
                                                                                    from ci2 in visionDB.usp_GetOustandingCustomBraceOrdersforCheckIn_Detail2Level(practiceLocationID)
                                                                                    where ci2.SupplierOrderID == ci1.SupplierOrderID
                                                                                    select new CheckInCustomBraceProductDetail
                                                                                    {
                                                                                        ActualWholesaleCost = ci2.ActualWholesaleCost,
                                                                                        Code = ci2.Code,
                                                                                        Color = ci2.Color,
                                                                                        Gender = ci2.Gender,
                                                                                        LeftRightSide = ci2.LeftRightSide,
                                                                                        LineTotal = ci2.LineTotal,
                                                                                        Packaging = ci2.Packaging,
                                                                                        PracticeCatalogProductID = ci2.PracticeCatalogProductID,
                                                                                        ProductInventoryID = ci2.ProductInventoryID,
                                                                                        QuantityCheckedIn = ci2.QuantityCheckedIn,
                                                                                        QuantityOrdered = ci2.QuantityOrdered,
                                                                                        QuantityToCheckIn = ci2.QuantityToCheckIn,
                                                                                        ShortName = ci2.ShortName,
                                                                                        Size = ci2.Size,
                                                                                        Status = ci2.Status,
                                                                                        SupplierOrderID = ci2.SupplierOrderID,
                                                                                        SupplierOrderLineItemID = ci2.SupplierOrderLineItemID,
                                                                                        SupplierOrderLineItemStatusID = ci2.SupplierOrderLineItemStatusID,


                                                                                    }

                                                                                ).ToArray < CheckInCustomBraceProductDetail>()
                                                        }
                                                   ).ToArray<CheckInCustomBraceSupplerInfo>()
                            };

                return query.Skip(skip).Take(take).ToArray<CheckInPO>();
            }
        }

        public static int CheckInPo(CheckInPO checkInPo, bool newApp)
        {

            int totalQuantityCheckedIn = 0;

            foreach (CheckInProductInfo product in checkInPo.CheckInProdInfo)
            {
                if (product.SupplierOrderID > 0 && newApp)
                {
                    DAL.PracticeLocation.CheckIn.SaveInvoicePaymentInfo(product.SupplierOrderID, product.InvoicePaid, product.InvoiceNumber);
                }
                foreach (CheckInProductExtra productDetail in product.CheckInProdExtra)
                {

                    if (productDetail.QuantityOrdered > 0 && productDetail.PracticeCatalogProductID > 0 && productDetail.ProductInventoryID > 0 && productDetail.SupplierOrderID > 0 && productDetail.SupplierOrderLineItemID > 0 && productDetail.QuantityToCheckIn > 0)
                    {
                        
                        DAL.PracticeLocation.CheckIn.POCheckIn_InsertUpdate(1,
                                                                            productDetail.PracticeCatalogProductID,
                                                                            productDetail.ProductInventoryID,
                                                                            product.BregVisionOrderID,
                                                                            productDetail.SupplierOrderID,
                                                                            productDetail.SupplierOrderLineItemID,
                                                                            productDetail.ActualWholesaleCost,
                                                                            productDetail.QuantityToCheckIn
                            );

                        totalQuantityCheckedIn += productDetail.QuantityToCheckIn;

                    }
                }
            }

            return totalQuantityCheckedIn;
            //DAL.PracticeLocation.CheckIn.ManualCheckIn_InsertUpdate(1, PracticeLocationID, PracticeCatalogProductID, ProductInventoryID,
                           //WholesaleCost, Quantity, Comments);
        }

        public static int CheckInManualPo(int practiceLocationID, CheckInProductExtra[] CheckInProdExtras)
        {
            int totalQuantityCheckedIn = 0;

            foreach (CheckInProductExtra productDetail in CheckInProdExtras)
            {

                if ( productDetail.PracticeCatalogProductID > 0 && productDetail.ProductInventoryID > 0  && productDetail.QuantityToCheckIn > 0)
                {
                    DAL.PracticeLocation.CheckIn.ManualCheckIn_InsertUpdate(1, 
                                                                            practiceLocationID, 
                                                                            productDetail.PracticeCatalogProductID, 
                                                                            productDetail.ProductInventoryID,
                                                                            productDetail.ActualWholesaleCost,
                                                                            productDetail.QuantityToCheckIn, 
                                                                            productDetail.Comments
                                                                            );

                    totalQuantityCheckedIn += productDetail.QuantityToCheckIn;
                }
            }

            return totalQuantityCheckedIn;
        }
    }



    public class CheckInProductInfo : usp_GetOutstandingPOsforCheckIn_Detail1LevelResult
    {
        [DataMember]
        public CheckInProductExtra[] CheckInProdExtra { get; set; }

    }

    public class CheckInProductExtra : usp_GetOutstandingPOsforCheckIn_Detail2LevelResult
    {
        [DataMember]
        public string Comments { get; set; }
    }

    public class CheckInCustomBraceSupplerInfo : usp_GetOustandingCustomBraceOrdersforCheckIn_Detail1LevelResult
    {
        [DataMember]
        public CheckInCustomBraceProductDetail[] CustomBraceDetail { get; set; }

        [DataMember]
        public string InvoiceNumber { get; set; }

        [DataMember]
        public bool InvoicePaid { get; set; }
    }

    public class CheckInCustomBraceProductDetail : usp_GetOustandingCustomBraceOrdersforCheckIn_Detail2LevelResult
    {

    }
}