﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web.Security;
using BregWcfService.DataContracts;
using ClassLibrary.DAL;
using System.Collections.Generic;
using System.Data.SqlClient;
using Telerik.Web.Data.Extensions;

namespace BregWcfService
{

    public enum DispenseStatus
    {
        ReadyforSave,
        Saved,
        ReadyForDispense,
        DispenseAttempted,
        Dispensed,
        NotDispensed,
        NotDispensedZeroQuantity,
        DispensedNeedSignature
    }

    [DataContract]
    public class DispenseItem : usp_Get_Dispense_Records_for_Browse_Grid_NewTEstResult
    {
        public DispenseItem()
        {
            ICD9_Code = "";
            PatientCode = "";
        }

        public DispenseItem(DispensementCustomBrace customBraceOrder, DispenseCustomBraceDetail d)
        {
            this.PatientCode = String.IsNullOrWhiteSpace(customBraceOrder.PatientCode)
                ? customBraceOrder.PatientName
                : customBraceOrder.PatientCode;
            this.PatientEmail = customBraceOrder.PatientEmail;
            this.PhysicianID = customBraceOrder.PhysicianIDNum;
            
         

            this.WholesaleCost = d.ActualWholesaleCost;
            this.BregVisionOrderID = d.BregVisionOrderID;
            this.Code = d.Code;
            this.Gender = d.Gender;
            this.IsMedicare = d.IsMedicare;
            this.IsABNForm = d.IsABNForm;
            this.IsSelected = true;
            this.ProductInventoryID = d.ProductInventoryID;
            this.ProductName = d.ProductName;
            this.QuantityOnHand = d.QuantityOnHand;
            this.Side = d.Side;
            this.Size = d.Size;
            this.ABNReason = d.ABNReason;
            this.ABNType = d.ABNType;
            this.DeclineReason = d.DeclineReason;
            this.MedicareOption = d.MedicareOption;
            this.DispenseDate = DateTime.Now;
            this.IsRental = d.IsRental;
            this.QuantityDispensed = d.QuantityToDispense;
            this.IsCashCollection = d.IsCashCollection;
            this.DMEDeposit = d.DMEDeposit;
            this.ICD9_Code = d.ICD9_Code;
            this.DispenseSignatureID = d.DispenseSignatureID;
            this.DispenseDate = d.DispenseDate;
            this.Mod1 = d.Mod1;
            this.Mod2 = d.Mod2;
            this.Mod3 = d.Mod3;
            FitterID = d.FitterID;
            CustomFitProcedureResponses = d.CustomFitProcedureResponses;
            PatientFirstName = d.PatientFirstName;
            PatientLastName = d.PatientLastName;
            PreAuthorized = d.PreAuthorized;

        }

        [DataMember]
        public DispenseStatus DispenseStatus { get; set; }

        //[DataMember]
        //public string Brand { get; set; }

        [DataMember]
        public string Fullname { get; set; }

        [DataMember]
        public bool IsThirdPartySupplier { get; set; }

        [DataMember]
        public string[] ICD9_List { get; set; }

        //add update dispense queue


        [DataMember]
        public Int32 PracticeCatalogProductID { get; set; }

        [DataMember]
        public Int32? DispenseQueueID { get; set; }

        [DataMember]
        public Int32 PracticeLocationID { get; set; }

        [DataMember]
        public string PatientCode { get; set; }

        [DataMember]
        public string PatientEmail { get; set; }

        [DataMember]
        public int PhysicianID { get; set; }

        [DataMember]
        public bool PhysicianSigned { get; set; }

        [DataMember]
        public string PhysicianSignedDate { get; set; }

        [DataMember]
        public string ICD9_Code { get; set; }

        [DataMember]
        public bool IsMedicare { get; set; }

        [DataMember]
        public bool PreAuthorized { get; set; }

        [DataMember]
        public bool IsCashCollection { get; set; }

        [DataMember]
        public bool IsABNForm { get; set; }

        [DataMember]
        public string ABNReason { get; set; }

        [DataMember]
        public string ABNType { get; set; }

        [DataMember]
        public int? BregVisionOrderID { get; set; }

        [DataMember]
        public int QuantityDispensed { get; set; }

        [DataMember]
        public int? FitterID { get; set; }

        [DataMember]
        public string Mod1 { get; set; }

        [DataMember]
        public string Mod2 { get; set; }

        [DataMember]
        public string Mod3 { get; set; }

        internal DateTime _DispenseDate;

        [DataMember]
        public DateTime? DispenseDate 
        { 
            get
            {               
                return _DispenseDate;
            }
             set
            {
                 var d = DispenseItem.CorrectUtcDateSerialization(value.GetValueOrDefault());
                 d = LimitDateValue(d, 90);
                 _DispenseDate = d;
            }
 
        
        }

        public static DateTime CorrectUtcDateSerialization(DateTime? possibleUTCDate)
        {
            if (possibleUTCDate.HasValue)
            {
                DateTime sentDate = possibleUTCDate.GetValueOrDefault();

                //Correct for UTC time on the tablet
                DateTime sentDatePlusHour = sentDate.AddHours(1d);
                if (sentDatePlusHour.CompareTo(DateTime.Now) > 0)
                {
                    DateTime localDate = DateTime.SpecifyKind(sentDate, DateTimeKind.Utc);
                    return  localDate.ToLocalTime();
                }
                else
                {
                    return  sentDate;
                }
            }
            else
            {
                return possibleUTCDate.GetValueOrDefault();
            }
        }

        public static DateTime LimitDateValue(DateTime date, int validDaysFromToday)
        {
            //check for bad date on device
            var dateDiff = date - DateTime.Now;
            return (Math.Abs(dateDiff.Days) > validDaysFromToday) ? DateTime.Now : date;
        }

        [DataMember]
        public int CreatedUser { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public Decimal BillingCharge { get; set; }

        [DataMember]
        public Decimal BillingChargeCash { get; set; }

        [DataMember]
        public Decimal WholesaleCost { get; set; }

        [DataMember]
        public string HcpcString { get; set; }

        [DataMember]
        public bool IsSelected { get; set; } //not sure why we needed this.  Look at removing

        [DataMember]
        public int? PONumber { get; set; }

        [DataMember]
        public string CustomPONumber { get; set; }

        [DataMember]
        public bool IsRental { get; set; }

        [DataMember]
        public string DeclineReason { get; set; }

        [DataMember]
        public int MedicareOption { get; set; }

        [DataMember]
        public int DispenseSignatureID { get; set; }

        [DataMember]
        public bool IsDeleted { get; set; }

        [DataMember]
        public bool? IsCustomFit { get; set; }

        [DataMember]
        public CustomFitProcedureResponse[] CustomFitProcedureResponses { get; set; }

        [DataMember]
        public bool IsCustomFabricated { get; set; }

        [DataMember]
        public string PatientFirstName { get; set; }

        [DataMember]
        public string PatientLastName { get; set; }

        [DataMember]
        public string Note { get; set; }

        [DataMember]
        public int? PracticePayerID { get; set; }

        public static void SaveDispenseAbnSignature(int practiceLocationID, int dispenseQueueID, byte[] patientAbnSignature)
        {

        }


        public static DispenseItem[] SearchForCustomBraceDispenseItems(int practiceLocationID, string searchCriteria, string searchText, int skip, int take)
        {
            List<DispenseItem> singleSupplierSearchItem = new List<DispenseItem>();
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var query = from p in visionDB.usp_GetCustomBraceOrdersforDispense_ParentLevel(practiceLocationID, searchCriteria, searchText)
                            join l1 in visionDB.usp_GetCustomBraceOrdersforDispense_Detail1Level(practiceLocationID, searchCriteria, searchText) on p.BregVisionOrderID equals l1.BregVisionOrderID
                            join pi in visionDB.ProductInventories on l1.ProductInventoryID equals pi.ProductInventoryID
                            join pcp in visionDB.PracticeCatalogProducts on pi.PracticeCatalogProductID equals pcp.PracticeCatalogProductID                            
                            select new DispenseItem
                            {        
                                
                                 BregVisionOrderID = p.BregVisionOrderID,
                                 PatientCode = p.PatientName,
                                 PhysicianID = p.PhysicianID ?? 0,
                                 BrandName = "Breg",
                                 Code =  l1.Code, //
                                 DMEDeposit = pcp.DMEDeposit, 
                                 Gender = l1.Gender, //
                                 ProductInventoryID = l1.ProductInventoryID,
                                 PracticeCatalogProductID = pi.PracticeCatalogProductID,
                                 ProductName = l1.ProductName,
                                 QuantityOnHand = l1.QuantityOnHand, //
                                 Side = l1.Side, //
                                 Size = l1.Size, //
                                 SupplierID = 1,
                                 SupplierName = "Breg",                                 
                                 Fullname = l1.ProductName, //
                                 BillingCharge = pcp.BillingCharge,
                                 BillingChargeCash = pcp.BillingChargeCash,  
                                 PONumber = p.PurchaseOrder,
                                 CustomPONumber = p.CustomPurchaseOrderCode,
                                 ICD9_List = DispenseItem.GetDispenseItemICD9s(practiceLocationID, pcp.ProductHCPCs.ToList()) 
                            };

                singleSupplierSearchItem.AddRange(query.ToList<DispenseItem>());
            }

            return singleSupplierSearchItem.Skip<DispenseItem>(skip).Take<DispenseItem>(take).ToArray<DispenseItem>();
        }

        public static DispenseItem[] SearchForDispenseItems(out int count, int practiceID, int practiceLocationID, string searchCriteria, string searchText, int skip, int take)
        {
            string[] singleSearchText = searchText.Split(',');

            return SearchForDispenseItems(out count, practiceID, practiceLocationID, searchCriteria, singleSearchText, skip, take);
        }

        public static DispenseItem[] SearchForDispenseItems(out int count, int practiceID, int practiceLocationID, string searchCriteria, string[] singleSearchText, int skip, int take)
        {
            List<DispenseItem> singleSupplierSearchItem = new List<DispenseItem>();
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                foreach (string text in singleSearchText)
                {
                    if (!string.IsNullOrEmpty(text))
                    {
                        var query = from i in visionDB.usp_Get_Dispense_Records_for_Browse_Grid_NewTEst(practiceLocationID, practiceID, searchCriteria, text)
                                    join s in visionDB.usp_GetSuppliers_Searchable(practiceLocationID, searchCriteria, text) on i.SupplierID equals s.SupplierID
                                    join pi in visionDB.ProductInventories on i.ProductInventoryID equals pi.ProductInventoryID
                                    join pcp in visionDB.PracticeCatalogProducts on pi.PracticeCatalogProductID equals pcp.PracticeCatalogProductID                                   
                                    select new DispenseItem
                                    {
                                        ABN_Needed = i.ABN_Needed,
                                        BrandName = i.BrandName,
                                        Code = i.Code,
                                        DMEDeposit = i.DMEDeposit,
                                        Gender = i.Gender,
                                        ProductInventoryID = i.ProductInventoryID,
                                        PracticeCatalogProductID = pi.PracticeCatalogProductID,
                                        ProductName = i.ProductName,
                                        QuantityOnHand = i.QuantityOnHand,
                                        Side = i.Side,
                                        Size = i.Size,
                                        SupplierID = i.SupplierID,
                                        SupplierName = i.SupplierName,
                                        Fullname = s.FullName,
                                        BillingCharge = pcp.BillingCharge,
                                        BillingChargeCash = pcp.BillingChargeCash, 
                                        ICD9_List = DispenseItem.GetDispenseItemICD9s(practiceLocationID, pcp.ProductHCPCs.ToList())                                        
                                    };

                        singleSupplierSearchItem.AddRange(query.ToList<DispenseItem>());
                    }
                }
            }

            count = singleSupplierSearchItem.Count;

            return singleSupplierSearchItem.Skip<DispenseItem>(skip).Take<DispenseItem>(take).ToArray<DispenseItem>();
        }



        public static PhysicianData[] GetPhysicians(int practiceLocationID)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var query =  visionDB.usp_PracticeLocation_Physician_Select_All_Names_By_PracticeLocationID(practiceLocationID);

                var renamedQuery = from p in query
                                   select new PhysicianData
                                   {
                                       ContactID = p.ContactID,
                                       PhysicianID = p.PhysicianID,
                                       PhysicianName = p.PhysicianName,
                                       SortOrder = p.SortOrder,
                                       Pin = p.Pin
                                   };

                return renamedQuery.ToArray<PhysicianData>();
            }
        }

        public static PhysicianData[] GetFitters(int practiceLocationId)
        {
            using (VisionDataContext visionDb = VisionDataContext.GetVisionDataContext())
            {
                using (visionDb)
                {
                    var fitters = from plp in visionDb.PracticeLocation_Physicians
                        where (plp.PracticeLocationID == practiceLocationId
                               && plp.Clinician.IsActive
                               && plp.Clinician.IsFitter
                               && plp.Clinician.Contact.IsActive
                               && plp.IsActive)
                        select new PhysicianData
                        {
                            ContactID = plp.Clinician.ContactID.Value,
                            PhysicianID = plp.PhysicianID,
                            PhysicianName =
                                plp.Clinician.Contact.FirstName + ' ' + plp.Clinician.Contact.LastName + ' ' +
                                plp.Clinician.Contact.Suffix,
                            Pin = plp.Clinician.PIN
                        };
                    return fitters.ToArray();
                }
            }
        }

        public static PhysicianDataForPractice[] GetFittersForPractice(int practiceId)
        {
            using (VisionDataContext visionDb = VisionDataContext.GetVisionDataContext())
            {
                using (visionDb)
                {
                    var practiceLocationIds = from locs in visionDb.PracticeLocations
                        where locs.PracticeID == practiceId
                        select locs.PracticeLocationID;

                    var fitters = from plp in visionDb.PracticeLocation_Physicians 
                        group plp by plp.PhysicianID
                        into phys
                        where practiceLocationIds.Contains(phys.First().PracticeLocationID) &&
                                phys.First().Clinician.IsFitter
                        select new PhysicianDataForPractice
                        {
                            ContactId = phys.First().Clinician.ContactID.Value,
                            PhysicianId = phys.First().PhysicianID,
                            PhysicianName = phys.First().Clinician.Contact.FirstName + ' ' + phys.First().Clinician.Contact.LastName + ' ' + phys.First().Clinician.Contact.Suffix,
                            Pin = phys.First().Clinician.PIN,
                            IsActive = phys.First().Clinician.IsActive ? 1 : 0,
                            PracticeLocationIds = phys.Where(x => x.IsActive).Select(y => (int?) y.PracticeLocationID).Distinct().ToList()
                        };

                    var fitterArray = fitters.ToArray();
                    foreach (var fitter in fitterArray)
                    {
                        if (fitter.IsActive == 0)
                        {
                            fitter.PracticeLocationIds = new List<int?>();
                        }
                    }

                    return fitterArray;
                }
            }
        }

        public static string[] GetDispenseItemICD9s(int practiceLocationID, List<ProductHCPC> hcpcs)
        {
            string[] hcpcsArray = hcpcs.Select(a => a.HCPCS).ToArray<string>();
            string hcpcsString = string.Join(",", hcpcsArray);

            return GetDispenseItemICD9s(practiceLocationID, hcpcsString);

        }

        public static string[] GetDispenseItemICD9s(int practiceLocationID, string dispenseItemHcpcs)
        {

            var _ICD9_Codes = GetDispenseItemICD9sList(practiceLocationID, dispenseItemHcpcs);

            var icd9s = BuildIcd9Array(_ICD9_Codes);
            
            return icd9s;
        }

        public static string[] GetDispenseItemICD9s(string dispenseItemHcpcs, IEnumerable<ICD9> dispenseQueueIcd9s)
        {

            var _ICD9_Codes = GetDispenseItemICD9sList(dispenseItemHcpcs, dispenseQueueIcd9s);

            var icd9s = BuildIcd9Array(_ICD9_Codes);
            return icd9s;
        }

        private static string[] BuildIcd9Array(IEnumerable<ICD9> _ICD9_Codes)
        {
            string[] icd9s =
                _ICD9_Codes
                    .Select(icd9 => icd9.ICD9Code.Trim())
                    .Distinct()
                    .ToArray<string>();
            return icd9s;
        }


        public static IEnumerable<ICD9> GetDispenseItemICD9sList(int practiceLocationId, string dispenseItemHcpcs)
        {
            // create truncated hcpcs array
            var hcpcsArray = BuildHcpcsArray(dispenseItemHcpcs);

            // bind icd9 combobox to datasource
            var _ICD9_Codes = DAL.PracticeLocation.Dispensement.GetDispenseQueueICD9s(practiceLocationId).Where<ICD9>(icd9 => hcpcsArray.Contains(icd9.HCPCs));
            return _ICD9_Codes;
        }

        public static IEnumerable<ICD9> GetDispenseItemICD9sList(string dispenseItemHcpcs, IEnumerable<ICD9> dispenseQueueIcd9S)
        {
            // create truncated hcpcs array
            var hcpcsArray = BuildHcpcsArray(dispenseItemHcpcs);

            // bind icd9 combobox to datasource
            var _ICD9_Codes = dispenseQueueIcd9S.Where<ICD9>(icd9 => hcpcsArray.Contains(icd9.HCPCs));
            return _ICD9_Codes;
        }

        private static string[] BuildHcpcsArray(string dispenseItemHcpcs)
        {
            var hcpcsArray = dispenseItemHcpcs.Replace(" ", "").Split(',');
            if (hcpcsArray.Length > 0)
            {
                for (var i = 0; i < hcpcsArray.Length; i++)
                {
                    var truncatedHCPC =
                        (hcpcsArray[i].Length > 4)
                            ? hcpcsArray[i].Substring(0, 5)
                            : hcpcsArray[i];

                    if (truncatedHCPC != null && hcpcsArray[i] != truncatedHCPC)
                        hcpcsArray[i] = truncatedHCPC;
                }
            }
            return hcpcsArray;
        }
    }   
}