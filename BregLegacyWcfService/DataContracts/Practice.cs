﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web.Security;
using ClassLibrary.DAL;
using System.Collections.Generic;

namespace BregWcfService
{
    [DataContract]
    public class Practice
    {
        [DataMember]
        public int PracticeID { get; set; }

        [DataMember]
        public string PracticeName { get; set; }

		[DataMember]
		public bool CameraScanEnabled { get; set; }

		[DataMember]
		public bool IsBregBilling { get; set; }

		[DataMember]
        public int? ContactID { get; set; }

        [DataMember]
        public bool IsBillingCentralized { get; set; }

        [DataMember]
        public bool IsShoppingCentralized { get; set; }

        [DataMember]
        public System.DateTime BillingStartDate { get; set; }

        [DataMember]
        public int CreatedUserID { get; set; }

        [DataMember]
        public System.DateTime CreatedDate { get; set; }

        [DataMember]
        public int? ModifiedUserID { get; set; }

        [DataMember]
        public DateTime? ModifiedDate { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public bool IsOrthoSelect { get; set; }

        [DataMember]
        public bool ABNCostIsCash { get; set; }

        [DataMember]
        private bool IsVisionLite { get; set; }

        [DataMember]
        public int PaymentTypeID { get; set; }

        [DataMember]
        public string PublicDescription { get; set; }

        [DataMember]
        public string LogoPartNumber { get; set; }

        [DataMember]
        public bool EMRIntegrationPullEnabled { get; set; }

        [DataMember]
        public bool FaxDispenseReceiptEnabled { get; set; }

        [DataMember]
        public bool EMRIntegrationDispenseReceiptPushEnabled { get; set; }

        [DataMember]
        public bool EMRIntegrationHL7PushEnabled { get; set; }

        [DataMember]
        public bool IsHandheldPrintEnabled { get; set; }

        [DataMember]
        public bool IsSecureMessagingInternalEnabled { get; set; }

        [DataMember]
        public bool IsSecureMessagingExternalEnabled { get; set; }

        [DataMember]
        public bool IsCustomFitEnabled { get; set; }

        [DataMember]
        public bool IsDisplayBillChargeOnReceipt { get; set; }

        [DataMember]
        public PracticeLocation[] PracticeLocation { get; set; }



        //move this to class library.  Also from ConsignmentPractices page
        public static Guid GetUserIDFromUserName(string userName)
        {
            MembershipUser editMembershipUser = null;
            editMembershipUser = Membership.GetUser(userName);
            return Guid.Parse(editMembershipUser.ProviderUserKey.ToString());
        }

        public static Practice[] GetAllPractices(string userName, UserContext context, out int count, int skip, int take)
        {
            Guid userID = Guid.Empty;

            bool isConsignmentRep = Roles.IsUserInRole(userName, "BregConsignment");

            if (Roles.IsUserInRole(userName, "BregConsignment"))
            {
                userID = GetUserIDFromUserName(userName);
            }
            
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {

                if (Roles.IsUserInRole(userName, "bregadmin") || isConsignmentRep)
                {

                    var query = from p in visionDB.Practices
                                from crp in visionDB.ConsignmentRep_Practices
                                where p.IsActive == true
                                //&& p.PracticeName.Contains("test") == false
                                && (isConsignmentRep == false
                                    ||
                                    (
                                        isConsignmentRep == true
                                        &&
                                        p.PracticeID == crp.PracticeID
                                        && crp.UserID == userID
                                        && crp.IsActive == true
                                    )
                                )

                                select new Practice
                                {
                                    PracticeID = p.PracticeID,
                                    PracticeName = p.PracticeName,
									CameraScanEnabled = p.CameraScanEnabled,
									IsBregBilling = p.IsBregBilling,
									ContactID = p.ContactID,
                                    IsBillingCentralized = p.IsBillingCentralized,
                                    IsShoppingCentralized = p.IsShoppingCentralized,
                                    BillingStartDate = p.BillingStartDate,
                                    CreatedUserID = p.CreatedUserID,
                                    CreatedDate = p.CreatedDate,
                                    ModifiedUserID = p.ModifiedUserID,
                                    ModifiedDate = p.ModifiedDate,
                                    IsActive = p.IsActive,
                                    IsOrthoSelect = p.IsOrthoSelect,
                                    ABNCostIsCash = p.ABNCostIsCash,
                                    IsVisionLite = p.IsVisionLite,
                                    PaymentTypeID = p.PaymentTypeID,
                                    PublicDescription = p.PublicDescription,
                                    LogoPartNumber = p.LogoPartNumber,
                                    EMRIntegrationPullEnabled = p.EMRIntegrationPullEnabled,
                                    FaxDispenseReceiptEnabled = p.FaxDispenseReceiptEnabled,
                                    EMRIntegrationDispenseReceiptPushEnabled = p.EMRIntegrationDispenseReceiptPushEnabled,
                                    EMRIntegrationHL7PushEnabled = p.EMRIntegrationHL7PushEnabled,
                                    IsHandheldPrintEnabled = p.IsHandheldPrintEnabled,
                                    IsSecureMessagingInternalEnabled = p.IsSecureMessagingInternalEnabled,
                                    IsSecureMessagingExternalEnabled = p.IsSecureMessagingExternalEnabled,
                                    IsCustomFitEnabled = p.IsCustomFitEnabled,
                                    IsDisplayBillChargeOnReceipt = p.IsDisplayBillChargeOnReceipt,
                                    PracticeLocation = (
                                                            (
                                                            from pl in visionDB.PracticeLocations
                                                            where pl.PracticeID == p.PracticeID
                                                            select new PracticeLocation
                                                            {
                                                                PracticeLocationID = pl.PracticeLocationID,
                                                                PracticeLocationName = pl.Name,
                                                                IsHcpSignatureRequired = pl.IsHcpSignatureRequired,
                                                                PracticeID = pl.PracticeID,
                                                                AddressID = pl.AddressID,
                                                                ContactID = pl.ContactID,
                                                                TimeZoneID = pl.TimeZoneID,
                                                                Name = pl.Name,
                                                                IsPrimaryLocation = pl.IsPrimaryLocation,
                                                                EmailForOrderApproval = pl.EmailForOrderApproval,
                                                                EmailForOrderConfirmation = pl.EmailForOrderConfirmation,
                                                                EmailForFaxConfirmation = pl.EmailForFaxConfirmation,
                                                                EmailForLowInventoryAlerts = pl.EmailForLowInventoryAlerts,
                                                                IsEmailForLowInventoryAlertsOn = pl.IsEmailForLowInventoryAlertsOn,
                                                                CreatedUserID = pl.CreatedUserID,
                                                                ModifiedUserID = pl.ModifiedUserID,
                                                                ModifiedDate = pl.ModifiedDate,
                                                                IsActive = pl.IsActive,
                                                                EmailForBillingDispense = pl.EmailForBillingDispense,
                                                                EmailForPrintDispense = pl.EmailForPrintDispense,
                                                                FaxForPrintDispense = pl.FaxForPrintDispense,
                                                                GeneralLedgerNumber = pl.GeneralLedgerNumber
                                                            }
                                                            ).ToArray<PracticeLocation>()
                                                       )
                                };

                    List<Practice> practiceList = query.ToList<Practice>();

                    count = practiceList.Count;

                    return practiceList.Skip(skip).Take(take).ToArray<Practice>();

                }
                else
                {
                    List<Practice> practiceList = new List<Practice>();

                    List<ClassLibrary.DAL.Practice> dalPractice = new List<ClassLibrary.DAL.Practice>();
                    dalPractice.Add(context.Practice);

                    var query = (from p in dalPractice
                                 select new Practice
                                 {
                                     PracticeID = p.PracticeID,
                                     PracticeName = p.PracticeName,
									 CameraScanEnabled = p.CameraScanEnabled,
									 IsBregBilling = p.IsBregBilling,
									 ContactID = p.ContactID,
                                     IsBillingCentralized = p.IsBillingCentralized,
                                     IsShoppingCentralized = p.IsShoppingCentralized,
                                     BillingStartDate = p.BillingStartDate,
                                     CreatedUserID = p.CreatedUserID,
                                     CreatedDate = p.CreatedDate,
                                     ModifiedUserID = p.ModifiedUserID,
                                     ModifiedDate = p.ModifiedDate,
                                     IsActive = p.IsActive,
                                     IsOrthoSelect = p.IsOrthoSelect,
                                     ABNCostIsCash = p.ABNCostIsCash,
                                     IsVisionLite = p.IsVisionLite,
                                     PaymentTypeID = p.PaymentTypeID,
                                     PublicDescription = p.PublicDescription,
                                     LogoPartNumber = p.LogoPartNumber,
                                     EMRIntegrationPullEnabled = p.EMRIntegrationPullEnabled,
                                     FaxDispenseReceiptEnabled = p.FaxDispenseReceiptEnabled,
                                     EMRIntegrationDispenseReceiptPushEnabled = p.EMRIntegrationDispenseReceiptPushEnabled,
                                     EMRIntegrationHL7PushEnabled = p.EMRIntegrationHL7PushEnabled,
                                     IsHandheldPrintEnabled = p.IsHandheldPrintEnabled,
                                     IsSecureMessagingInternalEnabled = p.IsSecureMessagingInternalEnabled,
                                     IsSecureMessagingExternalEnabled = p.IsSecureMessagingExternalEnabled,
                                     IsCustomFitEnabled = p.IsCustomFitEnabled,
                                     IsDisplayBillChargeOnReceipt = p.IsDisplayBillChargeOnReceipt,
                                     PracticeLocation = (
                                                             from pr in visionDB.PracticeLocations
                                                             where pr.PracticeID == p.PracticeID
                                                             && pr.IsActive == true
                                                             select new BregWcfService.PracticeLocation
                                                             {
                                                                 PracticeLocationID = pr.PracticeLocationID,
                                                                 PracticeLocationName = pr.Name,
                                                                 IsHcpSignatureRequired = pr.IsHcpSignatureRequired,
                                                                 PracticeID = pr.PracticeID,
                                                                 AddressID = pr.AddressID,
                                                                 ContactID = pr.ContactID,
                                                                 TimeZoneID = pr.TimeZoneID,
                                                                 Name = pr.Name,
                                                                 IsPrimaryLocation = pr.IsPrimaryLocation,
                                                                 EmailForOrderApproval = pr.EmailForOrderApproval,
                                                                 EmailForOrderConfirmation = pr.EmailForOrderConfirmation,
                                                                 EmailForFaxConfirmation = pr.EmailForFaxConfirmation,
                                                                 EmailForLowInventoryAlerts = pr.EmailForLowInventoryAlerts,
                                                                 IsEmailForLowInventoryAlertsOn = pr.IsEmailForLowInventoryAlertsOn,
                                                                 CreatedUserID = pr.CreatedUserID,
                                                                 ModifiedUserID = pr.ModifiedUserID,
                                                                 ModifiedDate = pr.ModifiedDate,
                                                                 IsActive = pr.IsActive,
                                                                 EmailForBillingDispense = pr.EmailForBillingDispense,
                                                                 EmailForPrintDispense = pr.EmailForPrintDispense,
                                                                 FaxForPrintDispense = pr.FaxForPrintDispense,
                                                                 GeneralLedgerNumber = pr.GeneralLedgerNumber
                                                             }
                                                         ).ToArray<BregWcfService.PracticeLocation>(),
                                 }).ToArray<Practice>();

                    count = 1;
                    return query;
                }
            }
            count = 0;
            return null;
        }
    }
}