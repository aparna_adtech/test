﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web.Security;
using ClassLibrary.DAL;
using System.Collections;
using System.Collections.Generic;
using System.Data.Linq;

namespace BregWcfService
{
    [DataContract]
    public class InventoryDiscrepencyItem : InventoryItem
    {
        [DataMember]
        public bool IsConsignment { get; set; }

        [DataMember]
        public bool IsCounted { get; set; }

        [DataMember]
        public bool IsBuyout { get; set; }

        [DataMember]
        public bool IsRMA { get; set; }

        [DataMember]
        public int ConsignmentQuantity { get; set; }

        [DataMember]
        public int InventoryCount { get; set; }

        [DataMember]
        public string ProductName { get; set; }

        [DataMember]
        public int QuantityOnOrder { get; set; }

        [DataMember]
        public int ReOrderQuantity { get; set; }

        [DataMember]
        public int QuantityInCart { get; set; }


        [DataMember]
        public decimal WholesaleTotal
        {
            get
            {
                return CalculatedReOrderQuantity * WholesaleCost;
            }
            set { }
        }

        [DataMember]
        public int CalculatedReOrderQuantity
        {
            get
            {
                if (IsRMA == true || IsBuyout == true)
                {
                    return 0;
                }
                return  (ReOrderQuantity - QuantityInCart) < 0 ? 
                        0 : 
                        (ReOrderQuantity - QuantityInCart);
            }
            set{}
        }

        public static ConsignmentItem[] Cast(InventoryDiscrepencyItem[] inventoryDiscrepencyItems) 
        {
            var query = from i in inventoryDiscrepencyItems
                        select new ConsignmentItem
                        {
                            BillingCharge = i.BillingCharge,
                            BillingChargeCash = i.BillingChargeCash,
                            BrandShortName = i.BrandShortName,
                            BuyoutRMAInventoryCycleID = 0,
                            Category = i.Category,
                            Code = i.Code,
                            ConsignmentQuantity = i.ConsignmentQuantity,
                            CriticalLevel = i.CriticalLevel,
                            Gender = i.Gender,
                            HCPCSString = i.HCPCSString,
                            ICD9_List = i.ICD9_List,
                            InventoryCount = i.InventoryCount,
                            IsActive = i.IsActive,
                            IsBuyout = i.IsBuyout,
                            IsConsignment = i.IsConsignment,
                            IsCounted = i.IsCounted,
                            IsRMA = i.IsRMA,
                            IsThirdPartyProduct = i.IsThirdPartyProduct,
                            Packaging = i.Packaging,
                            ParLevel = i.ParLevel,
                            PracticeCatalogProductID = i.PracticeCatalogProductID,
                            Product = i.Product,
                            ProductInventoryID = i.ProductInventoryID,
                            ProductName = i.ProductName,
                            QOH = i.QOH,
                            QuantityOnOrder = i.QuantityOnOrder,
                            QuantityInCart = i.QuantityInCart,
                            ReorderLevel = i.ReorderLevel,
                            ReOrderQuantity = i.ReOrderQuantity,
                            Side = i.Side,
                            Size = i.Size,
                            SupplierID = i.SupplierID,
                            SupplierShortName = i.SupplierShortName,
                            UPCCode_List = i.UPCCode_List,
                            WholesaleCost = i.WholesaleCost
                        };
            return query.ToArray<ConsignmentItem>();
        }



        public static InventoryDiscrepencyItem[] GetInventoryCycleDiscrepencies(int practiceLocationID, string userName, string password, string deviceID, string lastLocation, InventoryCycle inventoryCycle, bool isConsignment)
        {
            return GetInventoryCycleDiscrepencies(practiceLocationID, inventoryCycle);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="practiceLocationID"></param>
        /// <param name="inventoryCycle"></param>
        /// <param name="IncludeAll">This includes all inventory Items when true.  When false, it excludes items that were not counted and where the count = QOH</param>
        /// <param name="isConsignmentCount"></param>
        /// <returns></returns>
        public static InventoryDiscrepencyItem[] GetInventoryCycleDiscrepencies(int practiceLocationID, InventoryCycle inventoryCycle, bool IncludeAll = false, bool isConsignmentCount = false)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                // look up practice ID
                var practiceID = visionDB.PracticeLocations.Where(x => x.PracticeLocationID == practiceLocationID).Select(x => x.PracticeID).First();
                
                /* NOTE(Henry): Wierd logic from previous implementation. It seems that we are overriding the isConsignmentCount parameter
                and one would have to believe that inventoryCycle is null when isConsignmentCount is false and vice versa. This needs some
                investigation... */
                if (inventoryCycle != null)
                {
                    var inventoryCycleQuery = visionDB.InventoryCycleTypes.Where(x => x.InventoryCycleTypeID == inventoryCycle.InventoryCycleTypeID).Select(x => x.CycleName);
                    var inventoryCycleName = inventoryCycleQuery.FirstOrDefault();
                    if (inventoryCycleName != null)
                        isConsignmentCount = inventoryCycleName.ToLower().Contains("consignment");
                }

                /* NOTE(Henry): This is some more of that wierd logic mentioned above. Slightly refactored. Needs investigation. Unsure
                what the intent is because the currentInventoryCount query depends on a value that is hard-coded to 0 if the
                inventoryCycle is null which makes no sense... */
                var inventoryCycleId = (inventoryCycle == null) ? 0 : inventoryCycle.InventoryCycleID;

                // get the current count based on the requested count cycle
                var currentInventoryCount = (from iic in visionDB.InventoryItemCounts
                                             where (iic.InventoryCycleID == inventoryCycleId)
                                             group iic by iic.ProductInventoryID into c
                                             select new
                                             {
                                                 ProductInventoryID = c.Key,
                                                 InventoryCount = c.Sum(a => a.Count)
                                             }).ToArray();

                // get all current inventory for this location
                var currentInventory = LandingData.GetInventoryOrderStatus(practiceID, practiceLocationID);

                // filter for consignment, if necessary
                if (isConsignmentCount)
                {
                    currentInventory = currentInventory.Where(x => x.IsConsignment && x.ConsignmentQuantity > 0).ToArray();
                }

                // filter to include only counted items, if necessary
                if (!IncludeAll)
                {
                    currentInventory = (from T1 in currentInventory
                                        join T2 in currentInventoryCount on T1.ProductInventoryID equals T2.ProductInventoryID
                                        where T2.InventoryCount != T1.QuantityOnHand
                                        select T1).ToArray();
                }

                // create output
                var discrepancies =
                    (from inventory in currentInventory
                     join count in currentInventoryCount on (inventory.ProductInventoryID ?? -1) equals count.ProductInventoryID into x
                     from count in x.DefaultIfEmpty()
                     select new InventoryDiscrepencyItem
                     {
                         ProductInventoryID = inventory.ProductInventoryID.Value,
                         IsConsignment = inventory.IsConsignment,
                         IsCounted = (count != null ? true : false),
                         ConsignmentQuantity = (isConsignmentCount ? inventory.ConsignmentQuantity : 0),
                         ParLevel = (isConsignmentCount ? 0 : inventory.ParLevel),
                         QOH = inventory.QuantityOnHand ?? 0,
                         QuantityOnOrder = inventory.QuantityOnOrder ?? 0,
                         InventoryCount = (count != null ? count.InventoryCount : 0),
                         PracticeCatalogProductID = inventory.PracticeCatalogProductID ?? -1,
                         SupplierShortName = inventory.SupplierName ?? "",
                         BrandShortName = inventory.SupplierName ?? "",//TODO
                         ProductName = inventory.MCName,
                         Code = inventory.Code,
                         Side = inventory.LeftRightSide,
                         Size = inventory.Size,
                         ReOrderQuantity =
                             inventoryCycle != null
                                 // if the cycle is not null, use the inventory count
                                 ? (((inventory.ConsignmentQuantity - ((count != null ? count.InventoryCount : 0) + (inventory.QuantityOnOrder ?? 0))) < 0)
                                     ? 0
                                     : (inventory.ConsignmentQuantity - ((count != null ? count.InventoryCount : 0) + (inventory.QuantityOnOrder ?? 0))))
                                 //if the cycle is null, use quantity on hand. an adjustment will already have been made
                                 : (((inventory.ConsignmentQuantity - (inventory.QuantityOnHandPlusOnOrder ?? 0)) < 0)
                                     ? 0
                                     : (inventory.ConsignmentQuantity - (inventory.QuantityOnHandPlusOnOrder ?? 0))),
                         IsBuyout = false,//TODO inventory.IsBuyout,
                         IsRMA = false,//TODO inventory.IsRMA,
                         WholesaleCost = inventory.WholesaleCost,
                         QuantityInCart = 0,//TODO
                     })
                    .OrderBy(a => a.IsCounted)
                    .ThenByDescending(a => a.SupplierShortName.ToLower() == "breg")
                    .ThenBy(b => b.SupplierShortName)
                    .ThenBy(c => c.BrandShortName);

                return discrepancies.ToArray();
            }
        }


        public static bool VerifyCycleOpenWithSomeCountedItems(int practiceLocationID, InventoryCycle inventoryCycle, bool isConsignmentCount)
        {
            bool? noItemsCounted;
            bool? allItemsCounted;

            VerifyInventoryCycleStatus(practiceLocationID,
                                        inventoryCycle,
                                        out noItemsCounted,
                                        out allItemsCounted,
                                        isConsignmentCount);

            if (noItemsCounted.HasValue && allItemsCounted.HasValue)
            {
                //if some items were counted then it is probably an open cycle 
                // if all Items were counted then the cycle is closed
                if (noItemsCounted.Value == false && inventoryCycle.EndDate == null)
                {
                    return true;
                }
            }

            return false;
        }


        public static bool VerifyCycleOpenWithNoCountedItems(   int practiceLocationID,
                                                                InventoryCycle inventoryCycle,
                                                                bool isConsignmentCount = true)
        {
            bool? noItemsCounted;
            bool? allItemsCounted;

            VerifyInventoryCycleStatus(practiceLocationID,
                                        inventoryCycle,
                                        out noItemsCounted,
                                        out allItemsCounted,
                                        isConsignmentCount);

            if (noItemsCounted.HasValue && allItemsCounted.HasValue)
            {
                //if no items were counted then it is probably a brand new cycle or something went wrong
                // if all Items were not counted then the cycle is not finished or items were purposely left uncounted
                if (noItemsCounted.Value == true && allItemsCounted.Value == false && inventoryCycle.EndDate == null)
                {
                    return true;
                }
            }

            return false;
        }


        public static bool VerifyCycleClosedWithFullCount(  int practiceLocationID, 
                                                            InventoryCycle inventoryCycle,
                                                            bool isConsignmentCount = true)
        {
            bool? noItemsCounted;
            bool? allItemsCounted;

            VerifyInventoryCycleStatus(practiceLocationID,
                                        inventoryCycle,
                                        out noItemsCounted,
                                        out allItemsCounted,
                                        isConsignmentCount);

            if (noItemsCounted.HasValue && allItemsCounted.HasValue)
            {
                //if no items were counted then it is probably a brand new cycle or something went wrong
                // if all Items were not counted then the cycle is not finished or items were purposely left uncounted
                if (noItemsCounted.Value == false && allItemsCounted.Value == true)
                {
                    return true;
                }
            }

            return false;
        }

        public static void VerifyInventoryCycleStatus(      int practiceLocationID, 
                                                            InventoryCycle inventoryCycle,
                                                            out bool? noItemsCounted,
                                                            out bool? allItemsCounted,
                                                            bool isConsignmentCount = true)
        {

            if (practiceLocationID < 1)
                throw new ApplicationException(string.Format("Not a valid PracticeLocationID", practiceLocationID));
            if(inventoryCycle == null)
                throw new ApplicationException(string.Format("Inventory Cycle cannot be null", practiceLocationID));

            noItemsCounted = null;
            allItemsCounted = null;

            InventoryDiscrepencyItem[] discrepencies = null;
            discrepencies = BregWcfService.InventoryDiscrepencyItem.GetInventoryCycleDiscrepencies( practiceLocationID, 
                                                                                                    inventoryCycle, 
                                                                                                    IncludeAll: true, 
                                                                                                    isConsignmentCount: isConsignmentCount);

            if (discrepencies != null)
            {
                int itemsUNCounted = 0;                
                itemsUNCounted = (from d in discrepencies
                                  where d.IsCounted == false
                                  select d).Count();

                if (itemsUNCounted < 1)
                {
                    // this cycle has no items counted so is probably a brand new cycle.
                    allItemsCounted = true;
                }
                else
                {
                    allItemsCounted = false;
                }
                int itemsCounted = 0;
                itemsCounted = (from d in discrepencies
                                where d.IsCounted == true
                                select d).Count();

                if (itemsCounted < 1)
                {
                    noItemsCounted = true;
                }
                else
                {
                    noItemsCounted = false;
                }
               
            }


        }

        public static void AdjustDiscrepencies(
                                                    int practiceLocationID, 
                                                    string userName, 
                                                    string password, 
                                                    string deviceID, 
                                                    string lastLocation, 
                                                    InventoryCycle inventoryCycle, 
                                                    bool isConsignment,
                                                    string title = "")
        {
            AdjustDiscrepenciesAndCloseCycle(practiceLocationID, userName, inventoryCycle, title);


        }

        public static void AdjustDiscrepenciesAndCloseCycle(
                                                int practiceLocationID, 
                                                string userName, 
                                                InventoryCycle inventoryCycle,
                                                string title = "",
                                                bool fromVision = false)
        {
            UserContext userContext = null;
            int createdUserId = 1;

            try
            {
                userContext = UserContext.Login(userName);
                if (userContext != null && userContext.User != null)
                {
                    createdUserId = userContext.User.UserID;
                }

                //membershipUserID = (Guid)Membership.GetUser(userName).ProviderUserKey;
            }
            catch (Exception ex)
            {

            }

            InventoryDiscrepencyItem[] discrepencies = GetInventoryCycleDiscrepencies(practiceLocationID, inventoryCycle);

            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var inventoryCycleQuery = visionDB.InventoryCycles.Where(ic => ic.InventoryCycleID == inventoryCycle.InventoryCycleID);
                InventoryCycle currentInventoryCycle = inventoryCycleQuery.FirstOrDefault<InventoryCycle>();


                foreach (InventoryDiscrepencyItem discrepency in discrepencies)
                {
                    ProductInventory_QuantityOnHandPhysical physicalProductInventory = new ProductInventory_QuantityOnHandPhysical();
                    physicalProductInventory.InventoryCycleID = currentInventoryCycle.InventoryCycleID;
                    physicalProductInventory.ProductInventoryID = discrepency.ProductInventoryID;
                    physicalProductInventory.Quantity = discrepency.InventoryCount;
                    physicalProductInventory.DateOfPhysicalCount = currentInventoryCycle.StartDate;
                    physicalProductInventory.CreatedUserID = createdUserId;
                    physicalProductInventory.CreatedDate = DateTime.Now;
                    physicalProductInventory.ModifiedUserID = createdUserId;
                    physicalProductInventory.ModifiedDate = DateTime.Now;
                    physicalProductInventory.IsActive = true;

                    visionDB.ProductInventory_QuantityOnHandPhysicals.InsertOnSubmit(physicalProductInventory);

                    var productInventoryQuery = visionDB.ProductInventories.Where<ProductInventory>(pi => pi.ProductInventoryID == discrepency.ProductInventoryID);
                    ProductInventory productInventory = productInventoryQuery.FirstOrDefault<ProductInventory>();
                    //productInventory.QuantityOnHandPerSystem = discrepency.InventoryCount;

                    ProductInventory_ResetQuantityOnHandPerSystem resetProductInventory = new ProductInventory_ResetQuantityOnHandPerSystem();
                    resetProductInventory.ProductInventoryID = discrepency.ProductInventoryID;
                    resetProductInventory.InventoryCycleID = currentInventoryCycle.InventoryCycleID;
                    resetProductInventory.DateReset = DateTime.Now;
                    resetProductInventory.OldQuantityOnHandPerSystem = productInventory.QuantityOnHandPerSystem;
                    resetProductInventory.NewQuantityOnHandPerSystem = discrepency.InventoryCount;
                    resetProductInventory.ActualWholesaleCost = productInventory.PracticeCatalogProduct.WholesaleCost; // need to make sure this gets fetched

                    resetProductInventory.CreatedUserID = createdUserId;
                    resetProductInventory.CreatedDate = DateTime.Now;
                    resetProductInventory.ModifiedUserID = createdUserId;
                    resetProductInventory.ModifiedDate = DateTime.Now;
                    resetProductInventory.IsActive = true;
                    resetProductInventory.QuantityOfShrinkage = resetProductInventory.OldQuantityOnHandPerSystem - resetProductInventory.NewQuantityOnHandPerSystem;

                    visionDB.ProductInventory_ResetQuantityOnHandPerSystems.InsertOnSubmit(resetProductInventory);

                    //update the product Inventory
                    productInventory.QuantityOnHandPerSystem = discrepency.InventoryCount;
                    

                }

                currentInventoryCycle.EndDate = DateTime.Now;
                currentInventoryCycle.Title = title;

                visionDB.SubmitChanges();

                Classes.Email.SendBillingNotificationEmail(practiceLocationID, fromVision, EmailTemplates.InventoryComplete.ConsignmentEmailType.ConsignmentInventoryComplete);               
            }
        }



        public static void UpdateInventoryCount(InventoryDiscrepencyItemUpdate discrepencyUpdate)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var discrepency = (from d in visionDB.ProductInventories
                                    where d.ProductInventoryID == discrepencyUpdate.ProductInventoryID
                                    select d).FirstOrDefault();

                // if the counts are different, subtract them and make a archive item
                if (discrepency != null && discrepencyUpdate.ProductInventoryID > 0)
                {
                    if (discrepencyUpdate.InventoryCountInitial != discrepencyUpdate.InventoryCountRevised || discrepencyUpdate.InventoryCountRevised == 0)
                    {
                        InventoryItemCount manualCount = new InventoryItemCount();
                       
                        manualCount.Count = discrepencyUpdate.InventoryCountRevised - discrepencyUpdate.InventoryCountInitial;
                        
                        manualCount.CountDate = DateTime.Now;
                        manualCount.ProductInventoryID = discrepencyUpdate.ProductInventoryID;

                        List<InventoryItemCount> manualCounts = new List<InventoryItemCount>();
                        manualCounts.Add(manualCount);

                        InventorySetup.AddInventoryCount(discrepencyUpdate.PracticeLocationID,
                                                            discrepencyUpdate.UserName,
                                                            "",
                                                            "ManualUpdate",
                                                            "Vision",
                                                            discrepencyUpdate.CycleType,
                                                            discrepencyUpdate.Cycle,
                                                            manualCounts.ToArray<InventoryItemCount>(),
                                                            false);
                    }
                
                    //Save the isBuyout and isRMA info if it is a consignment item and one of the values is set
                    if (discrepency.IsConsignment == true && !(discrepencyUpdate.IsRMA.HasValue == false && discrepencyUpdate.IsBuyout.HasValue == false))
                    {

                        if (discrepencyUpdate.IsRMA.HasValue) 
                            discrepency.IsRMA = discrepencyUpdate.IsRMA.Value;

                        if (discrepencyUpdate.IsBuyout.HasValue)
                            discrepency.IsBuyout = discrepencyUpdate.IsBuyout.Value;

                        discrepency.BuyoutRMAInventoryCycleID = discrepencyUpdate.Cycle.InventoryCycleID;

                        visionDB.SubmitChanges();    

                    }
                }
            }
        }


   
    }

    [DataContract]
    public class InventoryDiscrepencyItemUpdate
    {
        [DataMember]
        public int ProductInventoryID { get; set; }

        [DataMember]
        public bool IsConsignment { get; set; }

        [DataMember]
        public bool IsCounted { get; set; }

        [DataMember]
        public bool? IsBuyout { get; set; }

        [DataMember]
        public bool? IsRMA { get; set; }

        [DataMember]
        public int InventoryCountInitial { get; set; }

        [DataMember]
        public int InventoryCountRevised { get; set; }

        [DataMember]
        public int PracticeLocationID { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public InventoryCycleType CycleType { get; set; }

        [DataMember]
        public InventoryCycle Cycle { get; set; }
    }
}