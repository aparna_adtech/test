﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
namespace BregWcfService
{
    [DataContract]
    public class DispensementCustomBraceNoDups
    {
        public DispensementCustomBraceNoDups()
        {
        }

        [DataMember]
        public DispensementCustomBrace DispensementCustomBrace { get; set; }

        [DataMember]
        public bool IsDuplicate { get; set; }
    }
}