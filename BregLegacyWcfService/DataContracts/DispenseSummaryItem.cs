﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BregWcfService
{
    [DataContract]
    public class DispenseSummaryItem
    {
        [DataMember]
        public int DispenseId { get; set; }

        [DataMember]
        public string PatientFirstName { get; set; }

        [DataMember]
        public string PatientLastName { get; set; }

        [DataMember]
        public string PatientCode { get; set; }

        [DataMember]
        public DateTime? DispenseDate { get; set; }

        [DataMember]
        public int? PhysicianID { get; set; }

        [DataMember]
        public string DispenseIdentifier { get; set; }

        [DataMember]
        public DateTime DbRowCreateDate { get; set; }

        [DataMember]
        public string DispenseGroupingId { get; set; }
    }
}