﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web.Security;
using BregWcfService.DataContracts;
using ClassLibrary.DAL;
using BregWcfService.HeaderClasses;
using BregWcfService.Classes;
using System.ServiceModel.Activation;
using System.Web;
using ClassLibrary.DAL.Helpers;
using BregWcfService.ErrorHandlerClasses;
using CustomFitProcedure = BregWcfService.DataContracts.CustomFitProcedure;
using System.Collections.Generic;
using System.Configuration;
using System.Xml.Serialization;
using System.Xml;

namespace BregWcfService
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class VisionService : IVisionService
    {
        private List<string> _monitoredPracticeLocations = (ConfigurationManager.AppSettings["Dispense.MonitoredPracticeLocations"] ?? string.Empty).Split(',').ToList();

        private Func<int, int> SetMaximumTake =
            take => { return (take < 1 || take > 500) ? 500 : take; };

        private Func<int, int> SetMinimumSkip =
            skip => { return (skip < 0) ? 0 : skip; };

        //Heartbeat service to determine whether or not the mobile applications are able to communicate with the server
        public bool Heartbeat()
        {
            return true;
        }

        // TODO: delete this before production
        public EMRIntegrationReference.schedule_item[] GetPatients(int practiceId, int practiceLocationId, DateTime utcStartDate, DateTime? utcEndDate, string userName, string password, string deviceID, string lastLocation, int skip, int take)
        {
            return GetScheduledPatientsRange(practiceId, practiceLocationId, utcStartDate, utcEndDate, userName, password, deviceID, lastLocation, skip, take);
        }

        /// <summary>
        /// This method exists to wrap the EMRIntegration service so referencing clients no longer need to reference the EMRIntegration service for object types.
        /// </summary>
        /// <returns></returns>
        public ScheduleItem[] GetPatientsV2(int practiceId, int practiceLocationId, DateTime utcStartDate, DateTime? utcEndDate, string userName, string password, string deviceID, string lastLocation, int skip, int take)
        {
            var items = GetScheduledPatientsRange(practiceId, practiceLocationId, utcStartDate, utcEndDate, userName, password, deviceID, lastLocation, skip, take);

            return TranslateScheduleItems(items);
        }

        /// <summary>
        /// Translates between the EMRIntegration service model and the BregVisionWcf model
        /// </summary>
        /// <returns></returns>
        private ScheduleItem[] TranslateScheduleItems(EMRIntegrationReference.schedule_item[] items)
        {
            List<ScheduleItem> newItems = new List<ScheduleItem>();

            if (items == null)
            {
                return newItems.ToArray();
            }

            foreach (var item in items)
            {
                ScheduleItem newItem = new ScheduleItem()
                {
                    CreationDate = item.creation_date,
                    DiagnosisICD9 = item.diagnosis_icd9,
                    IsActive = item.is_active,
                    IsCheckedIn = item.is_checked_in,
                    Message = item.message,
                    PatientDOB = item.patient_dob,
                    PatientEmail = item.patient_email,
                    PatientFirstName = item.patient_first_name,
                    PatientId = item.patient_id,
                    PatientLastName = item.patient_last_name,
                    PatientName = item.patient_name,
                    PhysicianName = item.physician_name,
                    PracticeId = item.practice_id,
                    PracticeLocationId = item.practice_location_id,
                    RemoteScheduleItemId = item.remote_schedule_item_id,
                    ScheduledVisitDateTime = item.scheduled_visit_datetime,
                    ScheduledVisitDatetimeTimezoneId = item.scheduled_visit_datetime_timezoneid,
                    ScheduleItemId = item.schedule_item_id
                };

                newItems.Add(newItem);
            }

            return newItems.ToArray();
        }

        public EMRIntegrationReference.schedule_item[] GetScheduledPatientsAll(int practiceId, int practiceLocationId, DateTime utcStartDate, DateTime? utcEndDate, string userName, string password, string deviceID, string lastLocation)
        {
            var scheduleItems = GetPatientsFromService(practiceId, practiceLocationId, utcStartDate, utcEndDate, userName, password, deviceID, lastLocation);
            return scheduleItems;
        }

        public EMRIntegrationReference.schedule_item[] GetScheduledPatientsRange(int practiceId, int practiceLocationId, DateTime utcStartDate, DateTime? utcEndDate, string userName, string password, string deviceID, string lastLocation, int skip, int take)
        {
            // NOTE: take is deprecated, leaving the method signature in place to maintain compatibility -hchan 09152016

            skip = SetMinimumSkip(skip);

            var scheduleItems = GetPatientsFromService(practiceId, practiceLocationId, utcStartDate, utcEndDate, userName, password, deviceID, lastLocation);
            return scheduleItems.Skip(skip).ToArray();
        }

        private static EMRIntegrationReference.schedule_item[] GetPatientsFromService(int practiceId, int practiceLocationId, DateTime utcStartDate, DateTime? utcEndDate, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);
            var client = new EMRIntegrationReference.EMRIntegrationClient();
            var scheduleItems = client.GetPatients(practiceId, practiceLocationId, utcStartDate, utcEndDate);
            return scheduleItems;
        }

        public UserContext Login(string userName, string password, string deviceID, string lastLocation)
        {
            return UserContext.Login(userName, password, deviceID, lastLocation);
        }

        public bool ValidateLogin(string userName, string password, string deviceID, string lastLocation)
        {
            return UserContext.ValidateLogin(userName, password, deviceID, lastLocation);
        }

        /// <summary>
        /// Returns Data for the Landing Page.  At the time this was written, ItemsForReorder and Last 10 Items Dispensed.
        /// </summary>
        /// <param name="practiceID">The ID of the Practice</param>
        /// <param name="practiceLocationID">The ID of the Practice Location</param>
        /// <returns></returns>
        public LandingData GetLandingData(int practiceID, int practiceLocationID, int skip, int take, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            skip = SetMinimumSkip(skip);
            take = SetMaximumTake(take);

            var count = 0;
            return LandingData.GetLandingData(out count, practiceID, practiceLocationID, skip, take);
        }

        public LandingData GetLandingData1(out int count, int practiceID, int practiceLocationID, int skip, int take, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            skip = SetMinimumSkip(skip);
            take = SetMaximumTake(take);

            return LandingData.GetLandingData(out count, practiceID, practiceLocationID, skip, take);
        }

        public LandingData GetLandingDataAll(int practiceID, int practiceLocationID, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            return LandingData.GetLandingData(practiceID, practiceLocationID);
        }

        public ProductUpdateWrapper GetInventoryLandingData(int practiceId, int practiceLocationID, string userName, string password, string deviceID, string lastLocation)
        {
            var startTime = DateTime.Now;
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            var ret = new ProductUpdateWrapper()
            {
                ProductInventory = LandingData.GetInventoryLandingData(practiceId, practiceLocationID),
                LatestSerialNumber = ProductUpdateWrapper.GetLatestSerialNumber(practiceLocationID, practiceId),
                TotalServerTime = (DateTime.Now - startTime).TotalSeconds
            };

            return ret;
        }

        public string[] GetModifiers(string userName, string password, string deviceID,
            string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            return ModifiersHelper.GetModifiersList().ToArray();
        }

        public InventoryOrderStatusResult[] GetInventoryById(int practiceLocationID, int[] productInventoryIDs, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            return LandingData.GetInventoryOrderStatusWithPriority(practiceLocationID, productInventoryIDs.ToList()).ToArray();
        }

        public ProductUpdateWrapper GetProductUpdates(int practiceLocationId, int practiceId, int serialNumber, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);
            var wrapper = new ProductUpdateWrapper();
            wrapper.UpdateLatestProductInventory(serialNumber, practiceLocationId, practiceId);
            return wrapper;
        }

        public BregWcfService.Practice[] GetAccessiblePractices(out int count, int skip, int take, string userName, string password, string deviceID, string lastLocation)
        {
            Practice[] practices = new Practice[] { };
            try
            {
                UserContext context = UserContext.Login(userName, password, deviceID, lastLocation);

                count = 0;

                skip = SetMinimumSkip(skip);
                take = SetMaximumTake(take);

                practices = BregWcfService.Practice.GetAllPractices(userName, context, out count, skip, take);
                return practices;
            }
            catch (Exception e)
            {
                LogException(e, userName, deviceID);
                throw;
            }
        }

        public Boolean LogException(Exception exception, string username, string deviceId)
        {
            try
            {
                var path = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "/ExceptionLogs/" +
                           DateTime.UtcNow.ToString("yy-MM-dd") + "-exceptionLog.txt";

                System.IO.FileInfo file = new System.IO.FileInfo(path);
                file.Directory.Create(); // If the directory already exists, this method does nothing.
                using (var append = System.IO.File.AppendText(path))
                {
                    append.WriteLine("exception time : " + DateTime.UtcNow.ToString("F"));
                    append.WriteLine("device id {0} username {1}", deviceId, username);
                    append.WriteLine(exception.Message);
                    append.WriteLine(exception.StackTrace);
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public bool AddDevice(string deviceIdentifier, string lastLocation, int? practiceID, int? practiceLocationID, int userID, string userName, string password)
        {
            if (Membership.ValidateUser(userName, password))
                return BregWcfService.Classes.Device.AddDevice(deviceIdentifier, lastLocation, practiceID, practiceLocationID, userID);
            else
                throw new ApplicationException("Unable to Validate User.");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="practiceLocationID">The practice location of the order</param>
        /// <param name="practiceCatalogProductID">The Product ID to be ordered</param>
        /// <param name="UserID">The User Ordering</param>
        /// <param name="Quantity">The Quantity to be ordered</param>
        /// <returns>Shopping Cart ID</returns>
        public int AddToShoppingCart(int practiceLocationID, int practiceCatalogProductID, int UserID, int Quantity, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

			int? replacementProduct = GetReplacementPracticeCatalogProduct(practiceLocationID, practiceCatalogProductID);
			if (replacementProduct != null)
				practiceCatalogProductID = replacementProduct.Value;

            return LandingData.AddToShoppingCart(practiceLocationID, practiceCatalogProductID, UserID, Quantity);
        }

        public void AddToShoppingCartMultiple(int practiceLocationID, int UserID, ShoppingCartItemCompact[] shoppingCartItems, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

			List<ShoppingCartItemCompact> items = new List<ShoppingCartItemCompact>(shoppingCartItems);
			foreach (var item in items)
			{
				int? replacementProduct = GetReplacementPracticeCatalogProduct(practiceLocationID, item.PracticeCatalogProductID);
				if (replacementProduct != null)
					item.PracticeCatalogProductID = replacementProduct.Value;
			}
			LandingData.AddToShoppingCartMultiple(practiceLocationID, UserID, items.ToArray());
        }

		private int? GetReplacementPracticeCatalogProduct(int practiceLocationID, int practiceCatalogProductID)
		{
			usp_SearchForItemsInInventory_ByPracticeCatalogProductID_With_ThirdPartyProductsResult[] results =
				LandingData.usp_SearchForItemsInInventory_ByPracticeCatalogProductID_With_ThirdPartyProducts(practiceLocationID, practiceCatalogProductID);
			foreach (var result in results)
			{
				if (result.NewPracticeCatalogProductID != 0)
					return result.NewPracticeCatalogProductID;
			}
			return null;
		}

	public void UpdateShoppingCart(int practiceLocationID, int shoppingCartItemId, int quantity, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            DAL.PracticeLocation.ShoppingCart.UpdateCart(shoppingCartItemId, quantity);
        }

        public ServiceResult UpdateIsHcpSignatureRequired(int practiceLocationID, bool isHcpRequired, string userName,
            string password, string deviceId, string lastLocation)
        {
            try
            {
                UserContext.ValidateLogin(userName, password, deviceId, lastLocation);

                DAL.PracticeLocation.PracticeLocation.UpdateIsHCPSignatureRequired(practiceLocationID, isHcpRequired);
            }
            catch (Exception ex)
            {
                return new ServiceResult(false, ex.InnerException.ToString());
            }

            return new ServiceResult(true, "");

        }

        public usp_GetShoppingCartResult[] GetShoppingCart(int practiceLocationID, string userName, string password, string deviceID, string lastLocation, int skip, int take)
        {
            int count = 0;

            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            skip = SetMinimumSkip(skip);
            take = SetMaximumTake(take);

            return ShoppingCartItemCompact.GetShoppingCart(out count, practiceLocationID, skip, take);
        }

        public usp_GetShoppingCartResult[] GetShoppingCart1(out int count, int practiceLocationID, string userName, string password, string deviceID, string lastLocation, int skip, int take)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            skip = SetMinimumSkip(skip);
            take = SetMaximumTake(take);

            return ShoppingCartItemCompact.GetShoppingCart(out count, practiceLocationID, skip, take);
        }

        public void DeleteAllFromCart(int practiceLocationID, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            ShoppingCartItemCompact.DeleteAllFromCart(practiceLocationID);
        }

        public void DeleteFromCart(int practiceLocationID, int shoppingCartItemId, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            DAL.PracticeLocation.Inventory.DeleteItemFromShoppingCart(shoppingCartItemId);
        }

        public InventoryItem[] SearchForItemsInInventory(int practiceLocationID, string searchCriteria, string searchText, int skip, int take, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            skip = SetMinimumSkip(skip);
            take = SetMaximumTake(take);

            int count = 0;
            return InventoryItem.SearchForItemsInInventory(out count, practiceLocationID, searchCriteria, searchText, skip, take);
        }

        public InventoryItem[] SearchForItemsInInventory1(out int count, int practiceLocationID, string searchCriteria, string searchText, int skip, int take, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            skip = SetMinimumSkip(skip);
            take = SetMaximumTake(take);

            return InventoryItem.SearchForItemsInInventory(out count, practiceLocationID, searchCriteria, searchText, skip, take);
        }

        public InventoryCriteria[] GetInventorySearchCodes(string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            return InventoryCriteria.GetInventorySearchCodes();
        }

        public ABNReason[] GetAbnReasons(string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            return AbnReason.GetAbnReasons();
        }

        public DispenseItem[] SearchForDispenseItems(int practiceID, int practiceLocationID, string searchCriteria, string searchText, int skip, int take, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            skip = SetMinimumSkip(skip);
            take = SetMaximumTake(take);

            int count = 0;
            return DispenseItem.SearchForDispenseItems(out count, practiceID, practiceLocationID, searchCriteria, searchText, skip, take);
        }

        public DispenseItem[] SearchForDispenseItems1(out int count, int practiceID, int practiceLocationID, string searchCriteria, string searchText, int skip, int take, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            skip = SetMinimumSkip(skip);
            take = SetMaximumTake(take);

            return DispenseItem.SearchForDispenseItems(out count, practiceID, practiceLocationID, searchCriteria, searchText, skip, take);
        }

        public DispensementCustomBrace[] GetCustomBraceOrdersforDispense(out int count, int practiceLocationID, string searchCriteria, string searchText, int skip, int take, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            skip = SetMinimumSkip(skip);
            take = SetMaximumTake(take);

            return DispensementCustomBrace.GetCustomBraceOrdersforDispense(out count, practiceLocationID, searchCriteria, searchText, skip, take);
        }

        public InventoryCriteria[] GetDispenseSearchCodes(string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            return InventoryCriteria.GetDispenseSearchCodes();
        }

        public InventoryCriteria[] GetDispenseCustomBraceSearchCodes(string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            return InventoryCriteria.GetDispenseCustomBraceSearchCodes();
        }

        public InventoryCriteria[] GetDispenseSideCodes(string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            return InventoryCriteria.GetDispenseSideCodes();
        }

        public bool IsDispensementExist(string userName, string password, string deviceID, string lastLocation, string dispenseIdentifier)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            // If the dispensement indicated by the combination of the deviceID and the dispenseIdentifier already exists, then we've done this before
            return DAL.PracticeLocation.Dispensement.IsDispensementExist(deviceID, dispenseIdentifier);
        }

        public Dispensement DispenseOrQueueProducts(Dispensement dispensement, string userName, string password, string deviceID, string lastLocation)
        {
            DispensementNoDups wrapper = DispenseOrQueueProductsNoDups(dispensement, userName, password, deviceID, lastLocation, null);
            return wrapper.Dispensement;
        }

        public DispensementNoDups DispenseOrQueueProductsNoDups(Dispensement dispensement, string userName, string password, string deviceID, string lastLocation,
            string dispenseIdentifier)
        {
            UserContext userContext = Login(userName, password, deviceID, lastLocation);

            // If the dispensement indicated by the combination of the deviceID and the dispenseIdentifier already exists, then we've done this before
            DispensementNoDups wrapper = new DispensementNoDups()
            {
                Dispensement = dispensement,
                IsDuplicate = DAL.PracticeLocation.Dispensement.IsDispensementExist(deviceID, dispenseIdentifier)
            };

            // fix side
            if (dispensement.DispenseItems != null)
            {
                foreach (var i in dispensement.DispenseItems.Where(x => (x != null) && (x.Side != null) && (x.Side.Length > 1)))
                {
                    if (i.Side.Equals("RT"))
                        i.Side = "R";
                    else if (i.Side.Equals("LT"))
                        i.Side = "L";
                }
            }

            int practiceId = userContext.Practice.PracticeID;

            MemoryStream pdfMemoryStream = null;
            if (isPracticeHandheldPrintEnabled(practiceId))
                pdfMemoryStream = new MemoryStream();

            if (_monitoredPracticeLocations.Contains(dispensement.PracticeLocationID.ToString()))
                LogInformation(dispensement, userName, deviceID, lastLocation, dispenseIdentifier, null, "Monitored Practice Location data capture");

            try
            {
                dispensement.DispenseOrQueueProducts(userName, practiceId, dispensement.PracticeLocationID, pdfMemoryStream, deviceID, dispenseIdentifier);
            }
            catch (Exception ex)
            {
                // if logging is disabled or it failed to log, let the exception go through; otherwise, do not re-throw
                if (!LogInformation(dispensement, userName, deviceID, lastLocation, dispenseIdentifier, ex, "Logging exceptipon data"))
                    throw;
            }

            if (pdfMemoryStream != null)
                dispensement.PatientReceipt = pdfMemoryStream.ToArray();

            return wrapper;
        }

        #region dispensement data logger implementation
        /// <summary>
        /// Serializes the given dispensement to disk, if enabled, to allow support to manually identify what may be causing dispensement failure.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dispensement">dispensement to serialize</param>
        /// <param name="userName">username that submitted dispensement</param>
        /// <param name="deviceID">device identifier of device that submitted dispensement</param>
        /// <param name="lastLocation">last GPS location string for device (unused)</param>
        /// <param name="dispenseIdentifier">unique client-generated dispense identifier submitted with request</param>
        /// <param name="ex">exception that triggered the need to log dispensement data</param>
        /// <returns>true if logging was enabled and item was logged</returns>
        private bool LogInformation<T>(T dispensement, string userName, string deviceID, string lastLocation, string dispenseIdentifier, Exception ex, string note)
        {
            bool runlog = false;
            bool.TryParse(ConfigurationManager.AppSettings["Dispense.RunLog"], out runlog);

            if (runlog)
            {
                char[] delimiter = { ',' };

                var configdelimiter = ConfigurationManager.AppSettings["Dispense.Delimiter"];

                if (!string.IsNullOrEmpty(configdelimiter))
                    delimiter = configdelimiter.ToCharArray();

                var userlist = ConfigurationManager.AppSettings["Dispense.UserList"];
                if (!string.IsNullOrEmpty(userlist) && userlist.Equals("*") || userlist.Split(delimiter).Contains(userName))
                {
                    var currentuserpath = CreateFolderStructure(userName);

                    if (!string.IsNullOrEmpty(currentuserpath))
                        SerializeObjects<T>(dispensement, userName, deviceID, lastLocation, dispenseIdentifier, ex, note,currentuserpath);
                }
                else
                    return false;   // if the user is not in the list, return false as if runlog was turned off.
            }

            return runlog;
        }

        private void SerializeObjects<T>(T dispensement, string userName, string deviceID, string lastLocation, string dispenseIdentifier, Exception ex, string note, string currentuserpath)
        {
            using (var fstream = new StreamWriter(Path.Combine(currentuserpath, DateTime.Now.ToString("HHmmss"))))
            {
                string startdelimiter, enddelimiter;

                #region dump header
                fstream.WriteLine("User:" + userName +
                    "; DeviceId:" + deviceID +
                    "; DispenseIdentifier:" + dispenseIdentifier +
                    "; LastLocation:" + lastLocation +
                    "; Note:" + note);
                #endregion

                #region dump dispensement object
                startdelimiter = "#StartObject#";
                enddelimiter = "#EndObject#";

                fstream.WriteLine(startdelimiter);
                fstream.Flush();

                new DataContractSerializer(typeof(T)).WriteObject(fstream.BaseStream, dispensement);

                fstream.WriteLine();
                fstream.WriteLine(enddelimiter);
                #endregion

                #region dump exception information
                startdelimiter = "#StartException#";
                enddelimiter = "#EndException#";

                fstream.WriteLine(startdelimiter);

                if (ex != null)
                {
                    fstream.WriteLine(ex.Source);
                    fstream.WriteLine(ex.Message);
                    fstream.WriteLine(ex.StackTrace);

                    if (ex.InnerException != null)
                    {
                        fstream.WriteLine(ex.InnerException.Source);
                        fstream.WriteLine(ex.InnerException.Message);
                        fstream.WriteLine(ex.InnerException.StackTrace);
                    }
                }

                fstream.WriteLine();
                fstream.WriteLine(enddelimiter);
                #endregion
            }
        }

        private string CreateFolderStructure(string userName)
        {
            var rootlogfolder = ConfigurationManager.AppSettings["Dispense.RootLogFolder"];
            var today = System.DateTime.Now.ToString("yyyyMMdd");
            var currentuserpath = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(rootlogfolder))
                {
                    if (!Directory.Exists(rootlogfolder))
                        Directory.CreateDirectory(rootlogfolder);

                    var currentdaylogpath = Path.Combine(rootlogfolder, today);
                    if (!Directory.Exists(currentdaylogpath))
                        Directory.CreateDirectory(rootlogfolder);

                    currentuserpath = Path.Combine(currentdaylogpath, userName);
                    if (!Directory.Exists(currentuserpath))
                        Directory.CreateDirectory(currentuserpath);
                }
            }
            catch { /* do nothing if there was a failure to create directory to store logged dispensement */ }

            return currentuserpath;
        }
        #endregion

        private bool isPracticeHandheldPrintEnabled(int practiceId)
        {
            return DAL.Practice.Practice.IsHandheldPrintEnabled(practiceId);
        }

        public DispensementCustomBrace DispenseOrQueueCustomBraceProducts(DispensementCustomBrace dispensement, string userName, string password, string deviceID, string lastLocation)
        {
            DispensementCustomBraceNoDups wrapper = DispenseOrQueueCustomBraceProductsNoDups(dispensement, userName, password, deviceID, lastLocation, null);
            return wrapper.DispensementCustomBrace;
        }

        public DispensementCustomBraceNoDups DispenseOrQueueCustomBraceProductsNoDups(DispensementCustomBrace dispensement, string userName, string password, string deviceID, string lastLocation,
            string dispenseIdentifier)
        {
            UserContext userContext = Login(userName, password, deviceID, lastLocation);

            // If the dispensement indicated by the combination of the deviceId and the dispenseIdentifier already exists, then we've done this before
            DispensementCustomBraceNoDups wrapper = new DispensementCustomBraceNoDups()
            {
                DispensementCustomBrace = dispensement,
                IsDuplicate = DAL.PracticeLocation.Dispensement.IsDispensementExist(deviceID, dispenseIdentifier)
            };

            // fix side
            if (dispensement.CustomBraceDetail != null)
            {
                foreach (var i in dispensement.CustomBraceDetail.Where(x => (x != null) && (x.Side != null) && (x.Side.Length > 1)))
                {
                    if (i.Side.Equals("RT"))
                        i.Side = "R";
                    else if (i.Side.Equals("LT"))
                        i.Side = "L";
                }
            }

            if (_monitoredPracticeLocations.Contains(dispensement.PracticeLocationID.ToString()))
                LogInformation(dispensement, userName, deviceID, lastLocation, dispenseIdentifier, null, "Monitored Practice Location data capture");

            try
            {
                dispensement.AddCustomBraceItemsToDispenseQueue(userName, userContext.Practice.PracticeID, dispensement.PracticeLocationID, deviceID, dispenseIdentifier);
            }
            catch (Exception ex)
            {
                // if logging is disabled or it failed to log, let the exception go through; otherwise, do not re-throw
                if (!LogInformation(dispensement, userName, deviceID, lastLocation, dispenseIdentifier, ex, "Logging exception data"))
                    throw;
            }

            return wrapper;
        }

        public void SaveDispenseAbnSignature(int practiceLocationID, int dispenseQueueID, byte[] patientAbnSignature, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext userContext = Login(userName, password, deviceID, lastLocation);

            DispenseItem.SaveDispenseAbnSignature(practiceLocationID, dispenseQueueID, patientAbnSignature);
        }

        public PhysicianData[] GetPhysicians(int practiceLocationID, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            return DispenseItem.GetPhysicians(practiceLocationID);
        }

        public PhysicianDataForPractice[] GetPhysiciansForPractice(int practiceID, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            return PhysicianDataForPractice.GetPhysiciansForPractice(practiceID);
        }

        public PhysicianData[] GetFitters(int practiceLocationID, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            return DispenseItem.GetFitters(practiceLocationID);
        }

        public PhysicianDataForPractice[] GetFittersForPractice(int practiceID, string userName, string password, string deviceID,
            string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            return DispenseItem.GetFittersForPractice(practiceID);
        }
        public DispenseItem[] GetDispenseQueue(int practiceLocationID, bool isDispenseQueueSortDescending, int skip, int take, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            skip = SetMinimumSkip(skip);
            take = SetMaximumTake(take);

            int count = 0;
            return Dispensement.GetDispenseQueue(out count, practiceLocationID, isDispenseQueueSortDescending, skip, take);
        }

        public DispenseItem[] GetDispenseQueue1(out int count, int practiceLocationID, bool isDispenseQueueSortDescending, int skip, int take, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            skip = SetMinimumSkip(skip);
            take = SetMaximumTake(take);

            return Dispensement.GetDispenseQueue(out count, practiceLocationID, isDispenseQueueSortDescending, skip, take);
        }

        public Dispensement[] GetDispensedUnsigned(out int count, int practiceLocationID, int skip, int take, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            skip = SetMinimumSkip(skip);
            take = SetMaximumTake(take);

            return Dispensement.GetDispensedUnsigned(out count, practiceLocationID, skip, take);
        }

        public DispenseSummaryContainer GetDispenseSummary(int practiceLocationID, bool isDispenseQueueSortDescending, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            return Dispensement.GetDispenseSummary(practiceLocationID, isDispenseQueueSortDescending);
        }

        public Dispensement GetDispensementById(int practiceLocationId, int dispenseId, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            return Dispensement.GetDispensementById(practiceLocationId, dispenseId);
        }

        public Dispensement GetQueuedDispensementByIDs(int practiceLocationId, int[] dispenseQueueIDs, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            return Dispensement.GetQueuedDispensementByDispenseQueueIDs(practiceLocationId, dispenseQueueIDs);
        }

        //This might not be necessary because I return the ICD-9s with each Dispense Item
        public ICD9[] GetDispenseQueueICD9s(int practiceLocationID, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            return Dispensement.GetDispenseQueueICD9s(practiceLocationID);
        }

        public InventoryCycleType[] GetInventoryCycleTypes(int practiceLocationID, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            return InventorySetup.GetInventoryCycleTypes(practiceLocationID, userName);
        }

        public InventoryCycle[] GetInventoryCyclesByType(int practiceLocationID, string userName, string password, string deviceID, string lastLocation, InventoryCycleType inventoryCycleType)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            return InventorySetup.GetInventoryCyclesByType(practiceLocationID, userName, password, deviceID, lastLocation, inventoryCycleType);
        }

        public void AddInventoryCount(int practiceLocationID, string userName, string password, string deviceID, string lastLocation, InventoryCycleType inventoryCycleType, InventoryCycle inventoryCycle, InventoryItemCount[] inventoryItemCounts)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            InventorySetup.AddInventoryCount(practiceLocationID, userName, password, deviceID, lastLocation, inventoryCycleType, inventoryCycle, inventoryItemCounts);
        }

        /// <summary>
        /// Returns false if the cycle is Closed and does not update inventory
        /// </summary>
        /// <param name="practiceLocationID"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="deviceID"></param>
        /// <param name="lastLocation"></param>
        /// <param name="inventoryCycleType"></param>
        /// <param name="inventoryCycle"></param>
        /// <param name="inventoryItemCounts"></param>
        /// <returns></returns>
        public bool AddInventoryCount1(int practiceLocationID, string userName, string password, string deviceID, string lastLocation, InventoryCycleType inventoryCycleType, InventoryCycle inventoryCycle, InventoryItemCount[] inventoryItemCounts)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            return InventorySetup.AddInventoryCount(practiceLocationID, userName, password, deviceID, lastLocation, inventoryCycleType, inventoryCycle, inventoryItemCounts);
        }


        public InventoryDiscrepencyItem[] GetInventoryCycleDiscrepencies(int practiceLocationID, string userName, string password, string deviceID, string lastLocation, InventoryCycle inventoryCycle, bool isConsignment)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            return InventoryDiscrepencyItem.GetInventoryCycleDiscrepencies(practiceLocationID, userName, password, deviceID, lastLocation, inventoryCycle, isConsignment);
        }

        public void AdjustDiscrepencies(int practiceLocationID, string userName, string password, string deviceID, string lastLocation, InventoryCycle inventoryCycle, bool isConsignment)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            InventoryDiscrepencyItem.AdjustDiscrepencies(practiceLocationID, userName, password, deviceID, lastLocation, inventoryCycle, isConsignment);
        }

        public void AdjustDiscrepencies1(int practiceLocationID, string userName, string password, string deviceID, string lastLocation, InventoryCycle inventoryCycle, bool isConsignment, string title)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            InventoryDiscrepencyItem.AdjustDiscrepencies(practiceLocationID, userName, password, deviceID, lastLocation, inventoryCycle, isConsignment);
        }

        public ConsignmentItem[] GetConsignmentReplenishmentList(int practiceLocationID, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            InventoryDiscrepencyItem[] inventoryDiscrepencyItems = ConsignmentItem.GetConsignmentReplenishmentList(practiceLocationID, userName, password, deviceID, lastLocation);

            ConsignmentItem[] consignmentItems = inventoryDiscrepencyItems.Cast<ConsignmentItem>().ToArray<ConsignmentItem>();

            return consignmentItems; // 
        }

        public void AddReplesishmentListToShoppingCart(int UserID, ShoppingCartItemCompact[] shoppingCartItems, int practiceLocationID, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            ConsignmentItem.AddReplenishmentListToShoppingCart(UserID, shoppingCartItems, practiceLocationID);
        }

        public void AddReplenishmentConsignmentListToShoppingCart(int UserID, ConsignmentItem[] consignmentItems, int practiceLocationID, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            ConsignmentItem.AddReplenishmentConsignmentListToShoppingCart(UserID, consignmentItems, practiceLocationID, false);
        }

        public void SaveUpcCode(int practiceID, string userName, string password, string deviceID, string lastLocation, int productID, string upcCode, bool isThirdPartyProduct)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            InventoryItem.SaveUpcCode(practiceID, userName, password, deviceID, lastLocation, productID, upcCode, isThirdPartyProduct);
        }

        public void SaveHCPSignatureRequired(string userName, string password, string deviceID, string lastLocation, bool isHcpSignatureRequired)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

        }

        public CheckInPO[] GetPendingCheckIns(int practiceLocationID, string userName, string password, string deviceID, string lastLocation, string searchText, int skip, int take)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            skip = SetMinimumSkip(skip);
            take = SetMaximumTake(take);

            return CheckInPO.GetPendingCheckIns(practiceLocationID, skip, take);
        }

        public CheckInPO[] GetPendingCustomBraceCheckIns(int practiceLocationID, string userName, string password, string deviceID, string lastLocation, int skip, int take)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            skip = SetMinimumSkip(skip);
            take = SetMaximumTake(take);

            return CheckInPO.GetPendingCustomBraceCheckIns(practiceLocationID, skip, take);
        }

        public int CheckInPo(CheckInPO checkInPo, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            return CheckInPO.CheckInPo(checkInPo, false);
        }

        public int CheckInPoAndSaveInvoice(CheckInPO checkInPo, string userName, string password, string deviceID, string lastLocation, bool saveInvoiceInfo)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            return CheckInPO.CheckInPo(checkInPo, saveInvoiceInfo);
        }

        public int CheckInManualPo(int practiceLocationID, CheckInProductExtra[] checkInProdExtras, string userName, string password, string deviceID, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceID, lastLocation);

            return CheckInPO.CheckInManualPo(practiceLocationID, checkInProdExtras);
        }

        public Payer[] GetPayers(int practiceId, string userName, string password, string deviceId, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceId, lastLocation);
            return DAL.Practice.Payer.SelectAll(practiceId).Select(x => new Payer()
            {
                Name = x.Name,
                PayerId = x.PayerId
            }).ToArray();
        }

        public Payer[] GetPayersForPractice(int practiceId, string userName, string password, string deviceId, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceId, lastLocation);
            return DAL.Practice.Payer.SelectAllForPractice(practiceId).Select(x => new Payer()
            {
                Name = x.Name,
                PayerId = x.PayerId,
                IsActive = x.IsActive
            }).ToArray();
        }

        public CustomFitProcedure[] GetCustomFitProcedures(string userName, string password, string deviceId, string lastLocation)
        {
            UserContext.ValidateLogin(userName, password, deviceId, lastLocation);

            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                return visionDB.CustomFitProcedures.Where(c => c.IsActive).Select(cfp => new CustomFitProcedure
                {
                    CustomFitProcedureId = cfp.CustomFitProcedureID,
                    Description = cfp.Description,
                    SortPosition = cfp.SortPosition,
                    IsUserEntered = cfp.IsUserEntered
                }).ToArray();
            }
        }
    }
}