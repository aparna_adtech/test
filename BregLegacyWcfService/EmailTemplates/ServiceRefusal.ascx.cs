﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BregWcfService.EmailTemplates
{
    public partial class ServiceRefusal : System.Web.UI.UserControl
    {
        public string PracticeName { get; set; }

        public string PatientRefusalReason { get; set; }

        public DateTime? PatientSignedDate { get; set; }

        public bool FromVision { get; set; }

        public int PatientSignatureID { get; set; }

        public void LoadControlInfo()
        {
            lblPracticeRefusal.Text = this.PracticeName;

            Action<Literal, string> _SetPatientSignatureLiteralControl =
                (c, url) =>
                {
                    if (c != null)
                        c.Text = string.Format(@"<img height='87' width='216' src='{0}' alt='' />", url);
                };

            _SetPatientSignatureLiteralControl(PatientDeclineSignatureLiteralControl,
                PatientSignature.GetImageUrl(PatientSignatureID, FromVision, true));

            if (PatientSignatureID > 0 && PatientSignedDate.HasValue)
            {
                lblPatientRefusalDate.Text = PatientSignedDate.Value.ToString(TemplateHelper.DateTimeFormat);
                lblPatientRefusalDate.Visible = true;
            }

            lblPatientRefusalReason.Text = PatientRefusalReason;
            lblPatientRefusalReason.Visible = true;
        }
    }
}