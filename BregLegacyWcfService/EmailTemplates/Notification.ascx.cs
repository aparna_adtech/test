﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BregWcfService
{
    public partial class Notification : System.Web.UI.UserControl
    {
        public string productName { get; set; }
        public int PatientSignatureID { get; set; }
        public DateTime? PatientSignedDate { get; set; }
        public string PhysicianName { get; set; }
        public bool FromVision { get; set; }

        public string ImageUrl { get; private set; }


        protected void Page_Load(object sender, EventArgs e)
        {
            //LoadControlInfo();
        }

        public void LoadControlInfo()
        {
            lblProductName.Text = productName;

            // we want it to render an image either way.  either the signature or a single white dot
            bool renderSignatureUrl = this.PatientSignatureID > 0;

            ImageUrl = BregWcfService.PatientSignature.GetImageUrl(PatientSignatureID, FromVision, renderSignatureUrl);


            if (this.PatientSignatureID > 0 && PatientSignedDate.HasValue)
            {
                lblSignatureDate.Text = PatientSignedDate.Value.ToShortDateString();
                lblSignatureDate.Visible = true;
            }
            
        }
    }
}