﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BregWcfService
{
    public partial class Warranty : System.Web.UI.UserControl
    {
        public string PracticeName { get; set; }
        public string PatientSignatureUrl { get; set; }
        public string PhysicianName { get; set; }
        public bool IsMedicare { get; set; }

        public List<WarrantySupplier> WarrantySuppliers { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LoadControlInfo()
        {
            rptrWarranty.DataSource = WarrantySuppliers;

            lblPractice01.Text = PracticeName;
            lblPractice02.Text = PracticeName;
            lblPractice03.Text = PracticeName;
            lblPractice04.Text = PracticeName;
            this.DataBind();
        }

        protected void rptrWarranty_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            RepeaterItem item = e.Item;
            if ((item.ItemType == ListItemType.Item) ||
                (item.ItemType == ListItemType.AlternatingItem))
            {
                Repeater rptrProduct = (Repeater)item.FindControl("rptrProducts");
                WarrantySupplier supplier = (WarrantySupplier)item.DataItem;
                rptrProduct.DataSource = supplier.WarrantyProducts;
                rptrProduct.DataBind();
            }
            else if ((item.ItemType == ListItemType.Header))
            {
                Label l = item.FindControl("lblPractice05") as Label;
                if (l != null)
                    l.Text = PracticeName;
            }
        }
    }

    public class WarrantySupplier
    {
        public string SupplierName { get; set; }

        public List<WarrantyProduct> WarrantyProducts { get; set; }
    }

    public class WarrantyProduct
    {
        public string ProductName { get; set; }
        public string Warranty { get; set; }
    }
}