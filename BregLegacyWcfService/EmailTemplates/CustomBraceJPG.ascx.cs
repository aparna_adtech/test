﻿using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.PDFUtils;

namespace BregWcfService
{
	public partial class CustomBraceJPG : UserControl
	{
		#region properties

		public string ImageUrlPage1 { get; private set; }
		public string ImageUrlPage2 { get; private set; }

		public int PracticeID
		{
			get
			{
				string bvoID = ParentPracticeID.Value;
				int bvoIDVal = 0;
				if (Int32.TryParse(bvoID, out bvoIDVal))
					return bvoIDVal;
				return 0;
			}
			set
			{
				ParentPracticeID.Value = value.ToString();
			}
		}

		public int PracticeLocationID
		{
			get
			{
				string bvoID = ParentPracticeLocationID.Value;
				int bvoIDVal = 0;
				if (Int32.TryParse(bvoID, out bvoIDVal))
					return bvoIDVal;
				return 0;
			}
			set
			{
				ParentPracticeLocationID.Value = value.ToString();
			}
		}

		public int BregVisionOrderID
		{
			get
			{
				string bvoID = ParentBregVisionOrderID.Value;
				int bvoIDVal = 0;
				if (Int32.TryParse(bvoID, out bvoIDVal))
					return bvoIDVal;
				return 0;
			}
			set
			{
				ParentBregVisionOrderID.Value = value.ToString();
			}
		}

		public int ShoppingCartID
		{
			get
			{
				string shoppingCartID = ParentShoppingCartID.Value;
				int shoppingCartIDVal = 0;
				if (Int32.TryParse(shoppingCartID, out shoppingCartIDVal))
					return shoppingCartIDVal;
				return 0;
			}
			set
			{
				ParentShoppingCartID.Value = value.ToString();
			}
		}

		public int ImageDPI
		{
			get
			{
				string ImageDPI = ParentImageDPI.Value;
				int ImageDPIVal = 0;
				if (Int32.TryParse(ImageDPI, out ImageDPIVal))
					return ImageDPIVal;
				return 0;
			}
			set
			{
				ParentImageDPI.Value = value.ToString();
			}
		}

		public bool Flatten
		{
			get
			{
				string Flatten = ParentFlatten.Value;
				bool FlattenVal = false;
				if (bool.TryParse(Flatten, out FlattenVal))
					return FlattenVal;
				return false;
			}
			set
			{
				ParentFlatten.Value = value.ToString();
			}
		}

		public string DisplayType
		{
			get
			{
				return ParentDisplayType.Value;
			}
			set
			{
				ParentDisplayType.Value = value;
			}
		}

		public int PageNumber
		{
			get
			{
				string PageNumber = ParentPageNumber.Value;
				int PageNumberVal = 0;
				if (Int32.TryParse(PageNumber, out PageNumberVal))
					return PageNumberVal;
				return 0;
			}
			set
			{
				ParentPageNumber.Value = value.ToString();
			}
		}

		public bool IsReadOnly
		{
			get
			{
				return true;
			}
			set
			{
			}
		}

		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
		}

		protected void Page_PreRender(object sender, EventArgs e)
		{
		}

		public void LoadControlInfo()
		{
			byte[] page1Bytes = CustomBracePDFUtils.BuildPDF(PracticeID, PracticeLocationID, BregVisionOrderID, ShoppingCartID, ImageDPI, true, DisplayType, 1);
			string page1Base64 = Convert.ToBase64String(page1Bytes);
			string page1Image = "data:image/" + DisplayType.ToLower() + ";base64," + page1Base64;

			byte[] page2Bytes = CustomBracePDFUtils.BuildPDF(PracticeID, PracticeLocationID, BregVisionOrderID, ShoppingCartID, ImageDPI, true, DisplayType, 2);
			string page2Base64 = Convert.ToBase64String(page2Bytes);
			string page2Image = "data:image/" + DisplayType.ToLower() + ";base64," + page2Base64;

			Action<Literal, string> _SetPageLiteralControl =
			(c, url) =>
			{
				if (c != null)
					c.Text = string.Format(@"<img width='1200' src='{0}' alt='' />", url);
			};

			_SetPageLiteralControl(Page1, page1Image);
			_SetPageLiteralControl(Page2, page2Image);
		}
	}
}