﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ABNFormCtlCommercial.ascx.cs" Inherits="BregWcfService.EmailTemplates.ABNFormCtlCommercial" %>
<%--<!DOCTYPE  html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<%--<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">--%><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><title>Orthopedic Institute of Pennsylvania</title><meta name="author" content="Julia Nevil"/><style type="text/css">
span.cls_003 {
    font-family:Arial,serif;
    font-size:16px;
    color:rgb(0,0,0);
    font-weight:bold;
    font-style: normal;
}
 
* {
margin: 0;
padding: 0;
text-indent: 0;
}

.s1 {
color: black;
font-family: Arial, sans-serif;
font-style: normal;
font-weight: bold;
text-decoration: none;
font-size: 12pt;
}

.s2 {
color: black;
font-family: Arial, sans-serif;
font-style: italic;
font-weight: bold;
text-decoration: none;
font-size: 12pt;
}

h1 {
color: black;
font-family: Arial, sans-serif;
font-style: normal;
font-weight: bold;
text-decoration: none;
font-size: 18pt;
}

.h4 {
color: black;
font-family: Arial, sans-serif;
font-style: normal;
font-weight: bold;
text-decoration: none;
font-size: 13pt;
}

.s3 {
color: black;
font-family: Arial, sans-serif;
font-style: normal;
font-weight: normal;
text-decoration: none;
font-size: 14pt;
}

.s4 {
color: black;
font-family: Arial, sans-serif;
font-style: italic;
font-weight: bold;
text-decoration: none;
font-size: 14pt;
}

.p, p {
color: black;
font-family: Arial, sans-serif;
font-style: normal;
font-weight: normal;
text-decoration: none;
font-size: 12pt;
margin: 0pt;
}

.s6 {
color: black;
font-family: Arial, sans-serif;
font-style: normal;
font-weight: normal;
text-decoration: none;
font-size: 10pt;
}

.h3, h3 {
color: black;
font-family: Arial, sans-serif;
font-style: normal;
font-weight: bold;
text-decoration: none;
font-size: 14pt;
}

.s7 {
color: black;
font-family: Arial, sans-serif;
font-style: normal;
font-weight: bold;
text-decoration: none;
font-size: 11pt;
}

h2 {
color: black;
font-family: Arial, sans-serif;
font-style: normal;
font-weight: bold;
text-decoration: none;
font-size: 16pt;
}

.s9 {
color: black;
font-family: "Arial Unicode MS", sans-serif;
font-style: normal;
font-weight: normal;
text-decoration: none;
font-size: 14pt;
}

.s10 {
color: black;
font-family: Arial, sans-serif;
font-style: normal;
font-weight: normal;
text-decoration: none;
font-size: 16pt;
}

li { display: block; }

#l1 {
padding-left: 0pt;
counter-reset: c1 0;
}

#l1 > li:before {
counter-increment: c1;
content: "(" counter(c1, upper-latin) ") ";
color: black;
font-family: Arial, sans-serif;
font-style: italic;
font-weight: bold;
text-decoration: none;
}

li { display: block; }

#l2 { padding-left: 0pt; }

#l2 > li:before {
content: "• ";
color: black;
font-family: Arial, sans-serif;
font-style: normal;
font-weight: normal;
text-decoration: none;
font-size: 12pt;
}
</style>
</head>

<body>
<div style="page-break-before: always; page-break-inside: avoid; margin: 0;"/>

<table width="100%"> 
    <tr>
        <td>
            <span class="cls_003"><asp:Label ID="lblPracticeName" runat="server"></asp:Label></span>
        </td>

        <td>
            <span class="cls_003"><asp:Label ID="lblAddress1" runat="server"></asp:Label></span>
        </td>

        <td>
            <span class="cls_003"><asp:Label ID="lblAddress2" runat="server"></asp:Label></span>
        </td>

        <td>
            <span class="cls_003"><asp:Label ID="lblCity" runat="server"></asp:Label>,&nbsp;<asp:Label ID="lblState" runat="server"></asp:Label>&nbsp;&nbsp;<asp:Label ID="lblZip" runat="server"></asp:Label></span>
        </td>

        <td colspan="2" width="200px">
            <span class="cls_003"><asp:Label ID="lblPhone" runat="server"></asp:Label></span><br />

            <span class="cls_003"><asp:Label ID="lblFax" runat="server"></asp:Label></span>
        </td>
    </tr>                
</table>
<ol id="l1">
    <li style="padding-top: 2pt; padding-left: 34pt; text-indent: -20pt; text-align: left;">
        <p class="s1" style="display: inline;">Notifier(s): <asp:Label ID="lblNotifier" runat="server"/></p>
    </li>
    <li style="padding-left: 33pt; text-indent: -19pt; text-align: left;">
        <p class="s1" style="display: inline;">Patient Name:<asp:Label ID="lblPatientName" runat="server"/> <i>(C) </i>Identification Number: <asp:Label ID="lblPatientCode" runat="server"/>
        </p>
    </li>
</ol>
<h1 style="padding-left: 14pt; text-indent: 0pt; text-align: left;">A<span class="h4">DVANCE </span>B<span class="h4">ENEFICIARY </span>N<span class="h4">OTICE OF </span>N<span class="h4">ONCOVERAGE </span>(ABN)
</h1>
<p class="s3" style="padding-left: 49pt; text-indent: -36pt; text-align: left;"><span class="s2">NOTE: </span>If your insurance doesn't pay for <i><b>(D)</b></i> <u><b> ITEM </b></u> below, you may have to pay.
</p>
<p style="padding-left: 14pt; text-indent: 0pt; text-align: left;">Your insurance does not pay for everything, even some care that you or your health care provider have good reason to think you need. We expect that your insurance may not pay for the <i><b>(D)</b></i> <b>ITEM</b> below.
</p>
<p style="text-indent: 0pt; text-align: left;">
    <br/>
</p><table style="border-collapse: collapse; margin-left: 5.12pt" cellspacing="0">
    <tr style="height: 28pt">
        <td style="width: 151pt; border-top-style: solid; border-top-width: 1pt; border-left-style: solid; border-left-width: 1pt; border-bottom-style: solid; border-bottom-width: 1pt; border-right-style: solid; border-right-width: 1pt" bgcolor="#DADADA">
            <p style="text-indent: 0pt; text-align: left;">
                <br/>
            </p>
            <p class="s6" style="padding-left: 4pt; text-indent: 0pt; text-align: left;">(D) Item</p>
        </td><td style="width: 215pt; border-top-style: solid; border-top-width: 1pt; border-left-style: solid; border-left-width: 1pt; border-bottom-style: solid; border-bottom-width: 1pt; border-right-style: solid; border-right-width: 1pt" bgcolor="#DADADA">
            <p style="text-indent: 0pt; text-align: left;">
                <br/>
            </p>
            <p class="s6" style="padding-left: 5pt; text-indent: 0pt; text-align: left;">(E) Reason your Insurance May Not Pay</p>
        </td><td style="width: 145pt; border-top-style: solid; border-top-width: 1pt; border-left-style: solid; border-left-width: 1pt; border-bottom-style: solid; border-bottom-width: 1pt; border-right-style: solid; border-right-width: 2pt" bgcolor="#DADADA">
            <p style="text-indent: 0pt; text-align: left;">
                <br/>
            </p>
            <p class="s6" style="padding-left: 5pt; text-indent: 0pt; text-align: left;">(F) Estimated Cost</p>
        </td>
    </tr><tr style="height: 90pt">
        <td style="width: 151pt; border-top-style: solid; border-top-width: 1pt; border-left-style: solid; border-left-width: 1pt; border-bottom-style: solid; border-bottom-width: 2pt; border-right-style: solid; border-right-width: 1pt">
            &nbsp;<br />
            <asp:Label ID="lblProductName" runat="server"/>
            <br /><br />
        </td>
        <td style="width: 215pt; border-top-style: solid; border-top-width: 1pt; border-left-style: solid; border-left-width: 1pt; border-bottom-style: solid; border-bottom-width: 2pt; border-right-style: solid; border-right-width: 1pt">
            &nbsp;<br />
            <asp:Label ID="lblAbnReason" runat="server"/>
            <br /><br />
        </td>
        <td style="width: 145pt; border-top-style: solid; border-top-width: 1pt; border-left-style: solid; border-left-width: 1pt; border-bottom-style: solid; border-bottom-width: 2pt; border-right-style: solid; border-right-width: 2pt">
            &nbsp;<br />
            <asp:Label ID="lblCost" runat="server"/>
            <br /><br />
        </td>
    </tr>
</table>
<p style="text-indent: 0pt; text-align: left;">
    <br/>
</p>
<h3 style="padding-top: 3pt; padding-left: 14pt; text-indent: 0pt; line-height: 16pt; text-align: left;">W<span class="s7">HAT YOU NEED TO DO NOW</span>:
</h3><ul id="l2">
    <li style="padding-left: 21pt; text-indent: -7pt; line-height: 14pt; text-align: left;">
        <p style="display: inline;">Read this notice, so you can make an informed decision about your care.</p>
    </li><li style="padding-left: 21pt; text-indent: -7pt; text-align: left;">
        <p style="display: inline;">Ask us any questions that you may have after you finish reading.</p>
    </li><li style="padding-left: 21pt; text-indent: -7pt; text-align: left;">
        <p style="display: inline;">Choose an option below about whether to receive the <i>
                <b>(D) </b>
            </i><u>
                <b>ITEM </b>
            </u>listed above.
        </p>
    </li>
</ul>
<p class="s1" style="padding-left: 13pt; text-indent: 0pt; text-align: left;">Note: <span class="p">If you choose Option 1 or 2, we may help you to use any other insurance that you might have.</span></p>
<p style="text-indent: 0pt; text-align: left;">
    <br/>
</p>
<h2 style="padding-left: 14pt; text-indent: 0pt; line-height: 17pt; text-align: left;"><span class="s4">(G) </span>O<span class="h4">PTIONS</span>: <span class="s1">Check only one box. We cannot choose a box for you.</span></h2>
<p style="padding-left: 14pt; text-indent: 0pt; line-height: 23pt; text-align: left;"><span class="s9"><asp:CheckBox ID="chkMedicare1" runat="server"/> </span><span class="h3">OPTION 1. </span>I want the <i>
        <b>(D) </b>
    </i><u>
        <b>ITEM </b>
    </u>listed above. You may ask to be paid now, but I
</p>
<p style="padding-top: 2pt; padding-left: 14pt; text-indent: 0pt; text-align: left;">also want my insurance billed for an official decision on payment. I understand that if my insurance doesn't pay, I am responsible for payment, but <b>I can appeal to the insurance company. </b>If my insurance does pay, you will refund any payments I made to you, less co-pays or deductibles.
</p>
<p style="padding-left: 14pt; text-indent: 0pt; line-height: 22pt; text-align: left;"><span class="s9"><asp:CheckBox ID="chkMedicare2" runat="server"/> </span><span class="h3">OPTION 2. </span>I want the <i>
        <b>(D) </b>
    </i><u>
        <b>ITEM </b>
    </u>listed above, but do not bill my insurance. You
</p>
<p style="padding-top: 2pt; padding-left: 14pt; text-indent: 0pt; text-align: left;">may ask to be paid now as I am responsible for payment. <b>I cannot appeal if my insurance is not billed.</b>
</p>
<p style="padding-left: 14pt; text-indent: 0pt; line-height: 22pt; text-align: left;"><span class="s9"><asp:CheckBox ID="chkMedicare3" runat="server"/> </span><span class="h3">OPTION 3. </span>I don't want the <i>
        <b>(D) </b>
    </i><u>
        <b>ITEM </b>
    </u>listed above. I understand with this choice I am <b>not </b>responsible for payment<span class="s10">, </span>and <b>I cannot appeal to see if my insurance would pay.</b>
</p>
<p class="s1" style="padding-top: 12pt; padding-left: 14pt; text-indent: 0pt; text-align: left;">This notice gives our opinion and is not an official decision by your insurance. <span class="p">Signing below means that you have received and understand this notice and that you have received a copy.</span></p>
<p style="text-indent: 0pt; text-align: left;">
    <br/>
</p>
<p class="s2" style="padding-top: 3pt; padding-left: 13pt; text-indent: 0pt; text-align: left;">(I) <span class="s1">Signature: <img height="87" width="216" src="<%= ImageUrl %>" alt="" /></span>(J) <span class="s1">Date: <asp:Label ID="lblSignatureDate" runat="server" /></span><u> </u>
</p>
</body>
