﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PatientEmailBody.ascx.cs" Inherits="BregWcfService.EmailTemplates.PatientEmailBody" %>

<p>
    <strong>
    This email message and attachment are for the sole use of the intended recipient. 
    Any unauthorized use, dissemination or disclosure of this message or its attachments is strictly prohibited.
    </strong>
</p>
<p>
    <strong>
    PDF Attachment:
    </strong>
</p>
<p>
    <strong>
        -   Proof of delivery
    </strong>
</p>
<p>
    <strong>
        -   Product information and standards
    </strong>
</p>


