﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomBraceJPG.ascx.cs" Inherits="BregWcfService.CustomBraceJPG" %>

<style>
	@media print {
		.page-break {
			display: block;
			page-break-before: always;
		}
	}
</style>

<div class="page-break">
	<asp:Literal ID="Page1" runat="server"></asp:Literal>
</div>
<div class="page-break">
	<asp:Literal ID="Page2" runat="server"></asp:Literal>
</div>

<asp:HiddenField ID="ParentPracticeID" runat="server" />
<asp:HiddenField ID="ParentPracticeLocationID" runat="server" />
<asp:HiddenField ID="ParentBregVisionOrderID" runat="server" />
<asp:HiddenField ID="ParentShoppingCartID" runat="server" />
<asp:HiddenField ID="ParentImageDPI" runat="server" />
<asp:HiddenField ID="ParentFlatten" runat="server" />
<asp:HiddenField ID="ParentDisplayType" runat="server" />
<asp:HiddenField ID="ParentPageNumber" runat="server" />
