﻿<%@ Control Language="C#" CodeBehind="CustomBrace.ascx.cs" Inherits="BregWcfService.CustomBrace" %>
<div id="customBraceReadOnlyDiv" style="display: none; z-index: 9999; position: fixed;
    top: 0; left: 0; width: 100%; height: 100%; background-color: White; opacity: .0;
    filter: alpha(opacity=0);">
</div>
<div id="customBracePOBlock" style="padding-left: 5px; padding-right: 20px; page-break-after: always; -webkit-text-fill-color: black">
    <asp:ValidationSummary ShowMessageBox="true" runat="server" ShowSummary="False" ID="ValidationSummary1" />
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td colspan="3">
                Purchase Order - Shipping, Billing, and Shipping Method
            </td>
            <td colspan="2" align="right">
                <asp:Button ID="btnNext" runat="server" Text="Next" />
            </td>
        </tr>
        <%--spacer--%>
        <tr><td colspan="5">&nbsp;</td></tr>
        <%--billing/shipping information--%>
        <tr>
            <td width="2%">
            </td>
            <td valign="top" width="47%">
                <div style="border: 1px solid #CCCCCC;">
                    <table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="white">
                        <tr>
                            <td colspan="2" style="background-color: #EEEEEE;">
                                <b>Billing</b>
                            </td>
                        </tr>
                        <tr>
                            <td width="5%" align="right" style="font-size: small">
                                Customer#
                            </td>
                            <td style="font-size: small">
                                <asp:Label ID="CustomerNumber" runat="server" Style="font-size: small"></asp:Label>
                                &nbsp;&nbsp;&nbsp;Tel:&nbsp;
                                    <asp:Label
                                        ID="txtBillPhone" runat="server">
                                    </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td width="5%" align="right" style="font-size: small">
                                Attention:
                            </td>
                            <td>
                                <asp:Label ID="BillAttention" runat="server" Style="font-size: small"></asp:Label>

                            </td>
                        </tr>
                        <tr>
                            <td width="5%" align="right" style="font-size: small">
                                Address:
                            </td>
                            <td>
                                <asp:Label ID="BillAddress1" runat="server" Style="font-size: small"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td width="5%" align="right" style="font-size: small">
                                &nbsp;
                            </td>
                            <td>
                                <asp:Label ID="BillAddress2" runat="server" Style="font-size: small"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" valign="middle">
                                <div style="font-size: small; text-align: center;">
                                    City:&nbsp;<asp:Label ID="BillCity" runat="server" Width="10em"></asp:Label>
                                    
                                    &nbsp; State:&nbsp;<asp:Label ID="BillState" runat="server" Width="10em"></asp:Label>
                                    &nbsp; Zip&nbsp;Code:&nbsp;<asp:Label ID="BillZipCode" runat="server" Width="5em"></asp:Label>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td width="2%">
                &nbsp;
            </td>
            <td valign="top" width="47%">
                <div style="border: 1px solid #CCCCCC;">
                    <table border="0" cellpadding="2" cellspacing="0" width="100%">
                        <tr>
                            <td colspan="2" style="background-color: #EEEEEE;">
                                <b>Shipping</b>
                            </td>
                        </tr>
                        <tr>
                            <td width="5%" align="right" style="font-size: small">
                                Attention:
                            </td>
                            <td width="40%">
                                <asp:Label ID="ShipAttention" runat="server" Style="font-size: small"></asp:Label>
                                
                            </td>
                        </tr>
                        <tr>
                            <td width="5%" align="right" style="font-size: small">
                                Address:
                            </td>
                            <td width="40%">
                                <asp:Label ID="ShipAddress1" runat="server" Style="font-size: small"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td width="5%" align="right" style="font-size: small">
                                &nbsp;
                            </td>
                            <td width="40%">
                                <asp:Label ID="ShipAddress2" runat="server" Style="font-size: small"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center" valign="middle">
                                <div style="font-size: small;">
                                    City:&nbsp;<asp:Label ID="ShipCity" runat="server" Width="10em"></asp:Label>
                                    &nbsp; State:&nbsp;<asp:Label ID="ShipState" runat="server" Width="10em"></asp:Label>
                                    &nbsp; Zip&nbsp;Code:&nbsp;<asp:Label ID="ShipZipCode" runat="server" Width="5em"></asp:Label>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td width="2%">
                &nbsp;
            </td>
        </tr>
        <%--spacer--%>
        <tr><td colspan="5">&nbsp;</td></tr>
        <%--Patient Information--%>
        <tr>
            <td width="2%">
            </td>
            <td colspan="3">
                <div style="border: 1px solid #CCCCCC;">
                    <table border="0" cellpadding="2" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2" style="background-color: #EEEEEE;">
                            <b>Patient Information</b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div style="font-size: 10pt;">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr id="patientInfoRow">
                                        <td width="50%" valign="top">
                                            <div>
                                                <b>Patient's Name:</b>&nbsp;<asp:Label ID="txtPatientName" runat="server"></asp:Label>
                                                &nbsp;&nbsp;Physician:&nbsp;<asp:DropDownList
                                                        ID="ddlPhysician" runat="server">
                                                    </asp:DropDownList>
                                            </div>
                                            <div style="margin-top: 6px;">
                                                Age:&nbsp;&nbsp;<asp:Label runat="server" ID="txtAge" Width="30px"></asp:Label>
                                                
                                                &nbsp; Weight:&nbsp;&nbsp;<asp:Label ID="txtWeight" runat="server" Width="30px"></asp:Label>
                                                
                                                &nbsp; Height:&nbsp;&nbsp;<asp:Label ID="txtHeight" runat="server" Width="40px"></asp:Label>
                                                
                                                &nbsp; Sex:&nbsp;&nbsp;<asp:RadioButtonList ID="rblSex" runat="server" RepeatDirection="Horizontal"
                                                    RepeatLayout="Flow">
                                                    <asp:ListItem Selected="True" Text="M" Value="Male"></asp:ListItem>
                                                    <asp:ListItem Text="F" Value="Female"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </td>
                                        <td width="50%" valign="top">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td>
                                                        <b>Leg Measurements:</b>
                                                    </td>
                                                    <td>
                                                        
                                                        <asp:RadioButtonList ID="rblLegMeasurements" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Text="Bilat" Value="Bilat"></asp:ListItem>
                                                            <asp:ListItem Text="Cast" Value="Cast"></asp:ListItem>
                                                            <asp:ListItem Text="Reform" Value="Reform"></asp:ListItem>
                                                            <asp:ListItem Text="None" Value="None" Selected="True"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        BRACE FOR:
                                                    </td>
                                                    <td>
                                                        <asp:RadioButtonList ID="rblBraceFor" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Text="Left Leg" Value="Left Leg" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="Right Leg" Value="Right Leg"></asp:ListItem>
                                                            <asp:ListItem Text="Both Legs" Value="Both Legs"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        Measurements taken by:&nbsp;<asp:Label ID="txtMeasurementsTakenBy" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="leftLegMeasurementsRow">
                                        <td colspan="2">
                                            <div style="border: 1px solid #dddddd; margin-top: 2px;">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td colspan="2">
                                                        
                                                        <b>Left Leg Measurements</b> <a id="copyFromRightLegAnchor" href="#" style="padding: 0 8px;
                                                            margin: 2px; background-color: #dddddd; text-decoration: none; color: Black;
                                                            cursor: pointer;" onclick="copyRightLegMeasurements(); return false;">Copy Measurements
                                                            From Right Leg</a> <a id="clearLeftLegMeasurementsAnchor" href="#" style="padding: 0 8px; margin: 2px; background-color: #dddddd;
                                                                text-decoration: none; color: Black; cursor: pointer;" onclick="clearLeftLegMeasurements(); return false;">
                                                                Clear Left Leg Measurements</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="50%" valign="top">
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td>
                                                                    INSTABILITY:
                                                                </td>
                                                                <td>
                                                                    <asp:RadioButtonList ID="rblInstability" runat="server" RepeatDirection="Horizontal">
                                                                        <asp:ListItem Text="None" Value="None" Selected="True"></asp:ListItem>
                                                                        <asp:ListItem Text="ACL" Value="ACL"></asp:ListItem>
                                                                        <asp:ListItem Text="PCL" Value="PCL"></asp:ListItem>
                                                                        <asp:ListItem Text="MCL/LCL" Value="MCL/LCL"></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                            </tr>
                                                            <tr id="trX2KUnloading" runat="server" visible="false">
                                                                <td>
                                                                    CounterForce*:
                                                                </td>
                                                                <td>
                                                                    <asp:RadioButtonList ID="rblCounterForce" runat="server" RepeatDirection="Vertical">
                                                                        <asp:ListItem Text="OA - Medial Unloading" Value="OA - Medial Unloading"></asp:ListItem>
                                                                        <asp:ListItem Text="OA - Lateral Unloading" Value="OA - Lateral Unloading"></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                    <div style="padding: 0 7px;">
                                                                        <asp:Label ID="txtCounterForceDegrees" runat="server" Width="60px" />°
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr id="trFusionUnloading" runat="server" visible="false">
                                                                <td colspan="2">
                                                                    FUSION Solus OA - Medial unloading only.
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="50%" valign="top">
                                                        <table cellpadding="2" cellspacing="0" width="100%" border="0">
                                                            <tr>
                                                                <td width="25%" align="right">
                                                                    1. Thigh Circumference
                                                                </td>
                                                                <td width="25%">
                                                                    <asp:Label ID="txtThigh" runat="server"></asp:Label>
                                                                </td>
                                                                <td width="25%" align="right">
                                                                    3. Knee Offset
                                                                </td>
                                                                <td width="25%">
                                                                    <asp:Label ID="txtKneeOffset" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">
                                                                    2. Calf Circumference
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txtCalf" runat="server"></asp:Label>
                                                                </td>
                                                                <td align="right">
                                                                    4. Knee Width
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txtKneeWidth" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <b>Extension:</b>&nbsp;&nbsp;<asp:RadioButtonList ID="rblExtension" runat="server"
                                                                        RepeatDirection="Horizontal">
                                                                        <asp:ListItem Text="0°" Value="0"></asp:ListItem>
                                                                        <asp:ListItem Text="10°" Value="10" Selected="True"></asp:ListItem>
                                                                        <asp:ListItem Text="20°" Value="20"></asp:ListItem>
                                                                        <asp:ListItem Text="30°" Value="30"></asp:ListItem>
                                                                        <asp:ListItem Text="40°" Value="40"></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                                <td colspan="2">
                                                                    <b>Flexion:</b>&nbsp;&nbsp;<asp:RadioButtonList ID="rblFlexion" runat="server" RepeatDirection="Horizontal">
                                                                        <asp:ListItem Text="None" Value="None" Selected="True"></asp:ListItem>
                                                                        <asp:ListItem Text="45°" Value="45"></asp:ListItem>
                                                                        <asp:ListItem Text="60°" Value="60"></asp:ListItem>
                                                                        <asp:ListItem Text="75°" Value="75"></asp:ListItem>
                                                                        <asp:ListItem Text="90°" Value="90"></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                    <div style="font-size: 9pt;">
                                                                        * New braces ship with 10° extension stops installed.</div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr id="rightLegMeasurementsRow">
                                        <td colspan="2">
                                            <div style="border: 1px solid #dddddd; margin-top: 2px;">
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td colspan="2">
                                                        
                                                        <b>Right Leg Measurements</b> <a id="copyFromLeftLegAnchor" href="#" style="padding: 0 8px;
                                                            margin: 2px; background-color: #dddddd; text-decoration: none; color: Black;
                                                            cursor: pointer;" onclick="copyLeftLegMeasurements(); return false;">Copy Measurements
                                                            From Left Leg</a> <a id="clearRightLegMeasurementsAnchor" href="#" style="padding: 0 8px; margin: 2px; background-color: #dddddd;
                                                                text-decoration: none; color: Black; cursor: pointer;" onclick="clearRightLegMeasurements(); return false;">
                                                                Clear Right Leg Measurements</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="50%" valign="top">
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td>
                                                                    INSTABILITY:
                                                                </td>
                                                                <td>
                                                                    <asp:RadioButtonList ID="rblInstabilityR" runat="server" RepeatDirection="Horizontal">
                                                                        <asp:ListItem Text="None" Value="None" Selected="True"></asp:ListItem>
                                                                        <asp:ListItem Text="ACL" Value="ACL"></asp:ListItem>
                                                                        <asp:ListItem Text="PCL" Value="PCL"></asp:ListItem>
                                                                        <asp:ListItem Text="MCL/LCL" Value="MCL/LCL"></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                            </tr>
                                                            <tr id="trX2KUnloadingR" runat="server" visible="false">
                                                                <td>
                                                                    CounterForce*:
                                                                </td>
                                                                <td>
                                                                    <asp:RadioButtonList ID="rblCounterForceR" runat="server" RepeatDirection="Vertical">
                                                                        <asp:ListItem Text="OA - Medial Unloading" Value="OA - Medial Unloading"></asp:ListItem>
                                                                        <asp:ListItem Text="OA - Lateral Unloading" Value="OA - Lateral Unloading"></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                    <div style="padding: 0 7px;">
                                                                        <asp:Label ID="txtCounterForceDegreesR" runat="server" Width="60px" />°
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr id="trFusionUnloadingR" runat="server" visible="false">
                                                                <td colspan="2">
                                                                    FUSION Solus OA - Medial unloading only.
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="50%" valign="top">
                                                        <table cellpadding="2" cellspacing="0" width="100%" border="0">
                                                            <tr>
                                                                <td width="25%" align="right">
                                                                    1. Thigh Circumference
                                                                </td>
                                                                <td width="25%">
                                                                    <asp:Label ID="txtThighR" runat="server"></asp:Label>
                                                                </td>
                                                                <td width="25%" align="right">
                                                                    3. Knee Offset
                                                                </td>
                                                                <td width="25%">
                                                                    <asp:Label ID="txtKneeOffsetR" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">
                                                                    2. Calf Circumference
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txtCalfR" runat="server"></asp:Label>
                                                                </td>
                                                                <td align="right">
                                                                    4. Knee Width
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="txtKneeWidthR" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <b>Extension:</b>&nbsp;&nbsp;<asp:RadioButtonList ID="rblExtensionR" runat="server"
                                                                        RepeatDirection="Horizontal">
                                                                        <asp:ListItem Text="0°" Value="0"></asp:ListItem>
                                                                        <asp:ListItem Text="10°" Value="10" Selected="True"></asp:ListItem>
                                                                        <asp:ListItem Text="20°" Value="20"></asp:ListItem>
                                                                        <asp:ListItem Text="30°" Value="30"></asp:ListItem>
                                                                        <asp:ListItem Text="40°" Value="40"></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                                <td colspan="2">
                                                                    <b>Flexion:</b>&nbsp;&nbsp;<asp:RadioButtonList ID="rblFlexionR" runat="server" RepeatDirection="Horizontal">
                                                                        <asp:ListItem Text="None" Value="None" Selected="True"></asp:ListItem>
                                                                        <asp:ListItem Text="45°" Value="45"></asp:ListItem>
                                                                        <asp:ListItem Text="60°" Value="60"></asp:ListItem>
                                                                        <asp:ListItem Text="75°" Value="75"></asp:ListItem>
                                                                        <asp:ListItem Text="90°" Value="90"></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                    <div style="font-size: 9pt;">
                                                                        * New braces ship with 10° extension stops installed.</div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
                </div>
            </td>
            <td width="2%">
            </td>
        </tr>
        <%--spacer--%>
        <tr><td colspan="5">&nbsp;</td></tr>
        <%--Brace Information--%>
        <tr>
            <td width="2%">
            </td>
            <td colspan="3">
                <div style="border: 1px solid #CCCCCC;">
                    <table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="white">
                    <%--Fusion Brace Information--%>
                    <tr id="trFusionTitle" runat="server" visible="false">
                        <td colspan="4" style="background-color: #EEEEEE;">
                            <b>Fusion Brace Information</b>
                        </td>
                    </tr>
                    <tr id="trFusionBody" runat="server" visible="false">
                        <td colspan="3">
                            <table cellpadding="0" cellspacing="0" border="1" width="100%">
                                <%--Leave this border--%>
                                <tr>
                                    <%--Standard Hinge Information--%>
                                    <td width="30%" valign="top" rowspan="2">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td colspan="2" style="font-size: small; font-weight: bold;">
                                                    <asp:Label ID="lblStdHingeTitle" runat="server" Text="Standard Hinge"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr style="font-size: small;">
                                                <td>
                                                    <table cellpadding="5" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblFusion" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="top">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:RadioButtonList ID="rblFusionCode" runat="server" RepeatDirection="Vertical"
                                                                    Enabled="false">
                                                                </asp:RadioButtonList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <br />
                                                </td>
                                            </tr>
                                            <%--FUSION Brace Color--%>
                                            <tr>
                                                <td>
                                                    <br />
                                                </td>
                                            </tr>
                                            <%--Color Enhancement--%>
                                            <tr>
                                                <td style="font-size: small;" colspan="2">
                                                    <asp:RadioButtonList ID="rblColorPatternEnhancement" runat="server" RepeatDirection="Vertical"
                                                        Width="299px">
                                                        <asp:ListItem Text="None" Value="None" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="Color Enhancement  (03161)" Value="03161"></asp:ListItem>
                                                        <asp:ListItem Text="Pattern Enhancement  (03162)" Value="03162"></asp:ListItem>
                                                        <asp:ListItem Text="Custom Color / Pattern Enhancement  (03163)" Value="03163"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="height: 30px">
                                                </td>
                                            </tr>
                                            <tr id="tr1" runat="server" height="30px" valign="bottom">
                                                <td colspan="2" style="background-color: #EEEEEE; text-align: left" align="left">
                                                    <b>
                                                        <asp:Label ID="lblFusionShippingMethod" runat="server" Text="Select your shipping method"></asp:Label></b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: small;" colspan="3">
                                                    <%--Fusion Shipping--%>
                                                    <asp:RadioButtonList ID="rblFusionShippingType" runat="server" Style="font-size: small">
                                                        <asp:ListItem Value="5">Saturday</asp:ListItem>
                                                        <asp:ListItem Value="6">Next Day Early AM</asp:ListItem>
                                                        <asp:ListItem Value="7">Next Day AM</asp:ListItem>
                                                        <asp:ListItem Value="1">Next Day</asp:ListItem>
                                                        <asp:ListItem Value="8">Next Day Saver</asp:ListItem>
                                                        <asp:ListItem Value="2">2nd Day</asp:ListItem>
                                                        <asp:ListItem Value="3">3rd Day</asp:ListItem>
                                                        <asp:ListItem Selected="true" Value="4">Ground</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: small;" colspan="3">
                                                    <asp:RadioButtonList ID="rblFusionShippingCarrier" runat="server" Style="font-size: small">
                                                        <asp:ListItem Value="1" Selected="True">UPS</asp:ListItem>
                                                        <asp:ListItem Value="2">Fed Ex</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <%--Note/Special Information--%>
                                    <td valign="top" style="font-size: small;" width="20%">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr valign="top">
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" height="150px" border="0">
                                                        <tr>
                                                            <td valign="top" style="font-size: small; font-weight: bold;">
                                                                <asp:Label ID="lblNotesSpecial" runat="server" Text="NOTES/SPECIAL INSTRUCTIONS"></asp:Label>
                                                                <asp:Label ID="txtNotes" runat="server" Width="270px" Height="130px" TextMode="MultiLine"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: small;">
                                                    <b>
                                                        <asp:Label ID="lblColorTitle" runat="server" Text="Color"></asp:Label></b>
                                                    <asp:RadioButtonList ID="rblColor" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"
                                                        Width="299px">
                                                        <asp:ListItem Text="None" Value="None" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="Forest" Value="Forest"></asp:ListItem>
                                                        <asp:ListItem Text="Orange" Value="Orange"></asp:ListItem>
                                                        <asp:ListItem Text="Navy" Value="Nave"></asp:ListItem>
                                                        <asp:ListItem Text="Pink" Value="Pink"></asp:ListItem>
                                                        <asp:ListItem Text="Royal" Value="Royal"></asp:ListItem>
                                                        <asp:ListItem Text="Yellow" Value="Yellow"></asp:ListItem>
                                                        <asp:ListItem Text="Sage" Value="Sage"></asp:ListItem>
                                                        <asp:ListItem Text="Custom" Value="Custom"></asp:ListItem>
                                                        <asp:ListItem Text="Red" Value="Red"></asp:ListItem>
                                                        <asp:ListItem Text="Charcoal" Value="Charcoal"></asp:ListItem>
                                                        <asp:ListItem Text="Mauve" Value="Maruve"></asp:ListItem>
                                                        <asp:ListItem Text="Pantone*" Value="Pantone"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: small;" align="right">
                                                    <asp:Label ID="txtPantone" runat="server" Width="70px"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr height="35px">
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>
                                                        <asp:Label ID="lblPattern" runat="server" Text="Pattern"></asp:Label></b>
                                                    <asp:RadioButtonList ID="rblPattern" runat="server" RepeatDirection="Horizontal"
                                                        RepeatColumns="2">
                                                        <asp:ListItem Text="None" Value="None" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="Flames" Value="Flames"></asp:ListItem>
                                                        <asp:ListItem Text="Camouflage" Value="Camouflage"></asp:ListItem>
                                                        <asp:ListItem Text="Flag" Value="Flag"></asp:ListItem>
                                                        <asp:ListItem Text="Custom Pattern*" Value="CustomPattern"></asp:ListItem>
                                                        <asp:ListItem Text="Ripples" Value="Ripples"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:Label ID="lblNotes" runat="server" Text="Notes:" Width="70px"></asp:Label>
                                                    <asp:Label ID="txtColorPattern" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <%--Fusion Frame Pads / Accessories--%>
                                    <td align="left" valign="top" width="35%">
                                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                            <tr>
                                                <td style="font-size: small;">
                                                    <b>
                                                        <asp:Label ID="lblFusioFramePadsTitle" runat="server" Text="FUSION FRAME PADS"></asp:Label></b><br />
                                                    <br />
                                                    <asp:Label ID="lblFusionFramePadsNote" runat="server" Text="All FUSION braces available with black frame pads only."></asp:Label><br />
                                                    <br />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="trFusionBodyAccessories" runat="server" visible="false">
                        <td colspan="3">
                            <table cellpadding="0" cellspacing="0" border="1" width="100%">
                                <tr>
                                    <td style="font-size: small;">
                                        <b>
                                            <asp:Label ID="Label8" runat="server" Text="FUSION ACCESSORIES"></asp:Label></b><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-size: small;">
                                        <asp:GridView ID="grdFusionAccessories" runat="server" AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="txtQuantity" runat="server" Text='<%# Bind("Quantity") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="DisplayName" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="MasterCatalogProductID" runat="server" Value='<%# Bind("MasterCatalogProductID") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <asp:CheckBoxList ID="chkFusionAccessories" runat="server" RepeatDirection="Vertical">
                                        </asp:CheckBoxList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    
                    <%--X2K Brace Information --%>
                    <tr id="trX2KAll" runat="server" visible="false">
                        <td colspan="3">
                            <table border="1" cellpadding="2" cellspacing="0" width="100%" bgcolor="white">
                                <%--X2K--%>
                                <tr id="trX2KTitle" runat="server">
                                    <td colspan="6" style="background-color: #EEEEEE;">
                                        <b>
                                            <asp:Label ID="Label7" runat="server" Text=" X2K Brace Information"></asp:Label></b>
                                    </td>
                                </tr>
                                <tr id="trX2KBody" runat="server" style="display: none">
                                    <td colspan="3">
                                        <table cellpadding="0" cellspacing="0" border="1" width="100%">
                                            <%--Leave this border--%>
                                            <tr>
                                                <%--Standard Hinge Information--%>
                                                <td width="30%" valign="top" rowspan="2">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td colspan="2" style="font-size: small; font-weight: bold;">
                                                            </td>
                                                        </tr>
                                                        <tr style="font-size: small;">
                                                            <td>
                                                                <table cellpadding="5" cellspacing="0" border="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblX2K" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td valign="top">
                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:RadioButtonList ID="rblX2KCode" runat="server" RepeatDirection="Vertical" Enabled="false">
                                                                            </asp:RadioButtonList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" style="background-color: #EEEEEE;">
                                                                <b>
                                                                    <asp:Label ID="Label11" runat="server" Text="X2K Frame Pad Colors"></asp:Label></b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" style="font-size: small;">
                                                                <br />
                                                                <br />
                                                                <asp:Label ID="Label12" runat="server" Text="Please choose 2 colors (Unless specified, braces will come with standard colors as noted):"></asp:Label>
                                                                <br />
                                                                <br />
                                                                <div>
                                                                    X2K: Black & Black</div>
                                                                <div>
                                                                    Women's X2K: Black & Silver</div>
                                                                <div>
                                                                    Compact X2K: Black & Black</div>
                                                                <div>
                                                                    X2K Unlimited: Black & Black</div>
                                                                <br />
                                                                <br />
                                                                <asp:CheckBoxList ID="chkX2KColor" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Text="None" Value="None" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem Text="Silver" Value="silver"></asp:ListItem>
                                                                    <asp:ListItem Text="Black" Value="black"></asp:ListItem>
                                                                    <asp:ListItem Text="Navy" Value="navy"></asp:ListItem>
                                                                    <asp:ListItem Text="Forest" Value="forest"></asp:ListItem>
                                                                    <asp:ListItem Text="Burgundy" Value="burgundy"></asp:ListItem>
                                                                </asp:CheckBoxList>
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: small;" colspan="2">
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" style="height: 30px">
                                                            </td>
                                                        </tr>
                                                        <tr id="tr3" runat="server" height="30px" valign="bottom">
                                                            <td colspan="2" style="background-color: #EEEEEE; text-align: left" align="left">
                                                                <b>
                                                                    <asp:Label ID="Label3" runat="server" Text="Select your shipping method"></asp:Label></b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: small;" colspan="3">
                                                                <%--Fusion Shipping--%>
                                                                <asp:RadioButtonList ID="rblX2KShippingType" runat="server" Style="font-size: small">
                                                                    <asp:ListItem Value="5">Saturday</asp:ListItem>
                                                                    <asp:ListItem Value="6">Next Day Early AM</asp:ListItem>
                                                                    <asp:ListItem Value="7">Next Day AM</asp:ListItem>
                                                                    <asp:ListItem Value="1">Next Day</asp:ListItem>
                                                                    <asp:ListItem Value="8">Next Day Saver</asp:ListItem>
                                                                    <asp:ListItem Value="2">2nd Day</asp:ListItem>
                                                                    <asp:ListItem Value="3">3rd Day</asp:ListItem>
                                                                    <asp:ListItem Selected="true" Value="4">Ground</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: small;" colspan="3">
                                                                <asp:RadioButtonList ID="rblX2KShippingCarrier" runat="server" Style="font-size: small">
                                                                    <asp:ListItem Value="1" Selected="True">UPS</asp:ListItem>
                                                                    <asp:ListItem Value="2">Fed Ex</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="top" align="center" width="15%">
                                                </td>
                                                <td valign="top" style="font-size: small;" width="20%">
                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tr valign="top">
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" height="150px" border="0">
                                                                    <tr>
                                                                        <td valign="top" style="font-size: small; font-weight: bold;">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: small;">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: small;" align="right">
                                                            </td>
                                                        </tr>
                                                        <tr height="35px">
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td align="left" valign="top" width="35%">
                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                    <tr>
                                                                        <td style="background-color: #EEEEEE;">
                                                                            <b>
                                                                                <asp:Label ID="Label13" runat="server" Text="X2K Accessories"></asp:Label></b><br />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="font-size: small;">
                                                                            <asp:GridView ID="grdX2KAccessories" runat="server" AutoGenerateColumns="false" BorderStyle="None"
                                                                                BorderWidth="0px">
                                                                                <Columns>
                                                                                    <asp:TemplateField ControlStyle-Width="30px" HeaderStyle-Width="30px">
                                                                                        <HeaderTemplate>
                                                                                            <asp:Label ID="lblQuantity" runat="server" Text="Qty"></asp:Label>
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="txtQuantity" Width="5px" runat="server" Text='<%# Bind("Quantity") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField DataField="DisplayName" HeaderText="Product" />
                                                                                    <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                            <asp:HiddenField ID="MasterCatalogProductID" runat="server" Value='<%# Bind("MasterCatalogProductID") %>' />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                            <asp:CheckBoxList ID="cblX2KAccessories" runat="server" RepeatDirection="Vertical">
                                                                            </asp:CheckBoxList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                </div>
            </td>
            <td width="2%">
            </td>
        </tr>
    </table>
</div>
