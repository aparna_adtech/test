﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BregWcfService.EmailTemplates
{
    public partial class HippaWaiver : System.Web.UI.UserControl
    {
        public string PracticeName { get; set; }
        public string PatientEmail { get; set; }
        public string PatientCode { get; set; }
        public string PatientName { get; set; }
        public DateTime CreatedDate { get; set; }

        //Set these
        public string PatientRefusalReason { get; set; }
        public int PatientSignatureID { get; set; }
        public DateTime? PatientSignedDate { get; set; }
        public bool FromVision { get; set; }
        

        public void LoadControlInfo()
        {
            lblDate.Text = CreatedDate.ToShortDateString();
            lblPatientEmail.Text = PatientEmail;
            lblPatientNameRequest.Text = PatientName;
            lblPatientName.Text = PatientName;
            lblPractice01.Text = PracticeName;
            lblPractice02.Text = PracticeName;
            lblPractice03.Text = PracticeName;
            lblPractice04.Text = PracticeName;
            lblReceivedDate01.Text = CreatedDate.ToShortDateString();
            lblReceivedDate02.Text = CreatedDate.ToShortDateString();

            bool? patientAccepted = null;
            if (!string.IsNullOrEmpty(this.PatientRefusalReason)) // this means it was probably from vision where we don't capture this field yet
            {
                patientAccepted = (this.PatientRefusalReason.ToLower() == "accepted");
            }

            // we want it to render an image either way.  either the signature or a single white dot
            bool renderSignatureUrl = patientAccepted.HasValue && (patientAccepted.Value == true) && (this.PatientSignatureID > 0);
            bool renderDeclineUrl = patientAccepted.HasValue && (patientAccepted.Value == false) && (this.PatientSignatureID > 0);

            Action<Literal, string> _SetPatientSignatureLiteralControl =
            (c, url) =>
            {
                if (c != null)
                    c.Text = string.Format(@"<img height='87' width='216' src='{0}' alt='' />", url);
            };

            _SetPatientSignatureLiteralControl(PatientSignatureLiteralControl, BregWcfService.PatientSignature.GetImageUrl(this.PatientSignatureID, FromVision, renderSignatureUrl));
            _SetPatientSignatureLiteralControl(PatientDeclineSignatureLiteralControl, BregWcfService.PatientSignature.GetImageUrl(this.PatientSignatureID, FromVision, renderDeclineUrl));


            if (this.PatientSignatureID > 0 && PatientSignedDate.HasValue)
            {
                if (patientAccepted == true)
                {  
                    lblPatientSignatureDate.Text = "Patient Signature Date: " + PatientSignedDate.Value.ToString(TemplateHelper.DateTimeFormat) ;

                    lblPatientSignatureDate.Visible = true;
                    lblCreatedDate.Visible = false;
                    lblDate.Visible = false;
                }
                else
                {
                    PatientSignatureLiteralControl.Visible = false;
                    lblDate.Visible  = false;

                    //Delete these lines below later u=
                    //lblPatientRefusalDate.Text = "<br/>" + "Patient Refusal Date: " + PatientSignedDate.Value.ToShortDateString();
                    //lblPatientRefusalDate.Visible = true;
                    //lblPatientRefusalReason.Text = "<br/>" + "Patient Refusal Reason: "  +  this.PatientRefusalReason;
                    //lblPatientRefusalReason.Visible = true;
                }
            }




        }

        
    }
}