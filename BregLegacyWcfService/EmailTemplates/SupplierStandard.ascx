﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SupplierStandard.ascx.cs"
    Inherits="BregWcfService.SupplierStandard" %>
<style type="text/css">.supplierStd {
     margin-bottom: 0;
 }</style>
<div style="page-break-before: always; page-break-inside: avoid; margin: 0; font-size: 10.5pt;">
    <table id="tblSupplierStd" runat="server" cellpadding="0" cellspacing="0" border="0"
        width="855px">
        <tr>
            <td>
                <h3 class="supplierStd">
                    <u>MEDICARE DMEPOS SUPPLIER STANDARDS</u></h3>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="lblSupplierStdIntro" runat="server" CssClass="contentTimesNewRoman"
                    Font-Bold="true" Text="Note: This is an abbreviated version of the supplier standards every Medicare DMEPOS supplier must meet in order to obtain and retain their billing privileges. These standards, in their entirety, are listed in 42 C.F.R. 424.57(c)."></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left">
                <ul class="supplierStd">
                    <li style="list-style-type: decimal">A supplier must be in compliance with all applicable Federal and State licensure and regulatory requirements.</li>
                    <li style="list-style-type: decimal">A supplier must provide complete and accurate information on the DMEPOS supplier application. Any changes to this information must be reported to the National Supplier Clearinghouse within 30 days.</li>
                    <li style="list-style-type: decimal">A supplier must have an authorized individual (whose signature is binding) sign the enrollment application for billing privileges.</li>
                    <li style="list-style-type: decimal">A supplier must fill orders from its own inventory, or contract with other companies for the purchase of items necessary to fill orders. A supplier may not contract with any entity that is currently excluded from the Medicare program, any State health care programs, or any other Federal procurement or non-procurement programs.</li>
                    <li style="list-style-type: decimal">A supplier must advise beneficiaries that they may rent or purchase inexpensive or routinely purchased durable medical equipment, and of the purchase option for capped rental equipment.</li>
                    <li style="list-style-type: decimal">A supplier must notify beneficiaries of warranty coverage and honor all warranties under applicable State law, and repair or replace free of charge Medicare covered items that are under warranty.</li>
                    <li style="list-style-type: decimal">A supplier must maintain a physical facility on an appropriate site and must maintain a visible sign with posted hours of operation. The location must be accessible to the public and staffed during posted hours of business. The location must be at least 200 square feet and contain space for storing records.</li>
                    <li style="list-style-type: decimal">A supplier must permit CMS or its agents to conduct on-site inspections to ascertain the supplier’s compliance with these standards.</li>
                    <li style="list-style-type: decimal">A supplier must maintain a primary business telephone listed under the name of the business in a local directory or a toll free number available through directory assistance. The exclusive use of a beeper, answering machine, answering service or cell phone during posted business hours is prohibited.</li>
                    <li style="list-style-type: decimal">A supplier must have comprehensive liability insurance in the amount of at least $300,000 that covers both the supplier’s place of business and all customers and employees of the supplier. If the supplier manufactures its own items, this insurance must also cover product liability and completed operations.</li>
                    <li style="list-style-type: decimal">A supplier is prohibited from direct solicitation to Medicare beneficiaries. For complete details on this prohibition see 42 CFR § 424.57 (c) (11).</li>
                    <li style="list-style-type: decimal">A supplier is responsible for delivery of and must instruct beneficiaries on the use of Medicare covered items, and maintain proof of delivery and beneficiary instruction.</li>
                    <li style="list-style-type: decimal">A supplier must answer questions and respond to complaints of beneficiaries, and maintain documentation of such contacts.</li>
                    <li style="list-style-type: decimal">A supplier must maintain and replace at no charge or repair cost either directly, or through a service contract with another company, any Medicare-covered items it has rented to beneficiaries.</li>
                    <li style="list-style-type: decimal">A supplier must accept returns of substandard (less than full quality for the particular item) or unsuitable items (inappropriate for the beneficiary at the time it was fitted and rented or sold) from beneficiaries.</li>
                    <li style="list-style-type: decimal">A supplier must disclose these standards to each beneficiary it supplies a Medicare-covered item.</li>
                    <li style="list-style-type: decimal">A supplier must disclose any person having ownership, financial, or control interest in the supplier.</li>
                    <li style="list-style-type: decimal">A supplier must not convey or reassign a supplier number; i.e., the supplier may not sell or allow another entity to use its Medicare billing number.</li>
                    <li style="list-style-type: decimal">A supplier must have a complaint resolution protocol established to address beneficiary complaints that relate to these standards. A record of these complaints must be maintained at the physical facility.</li>
                    <li style="list-style-type: decimal">Complaint records must include: the name, address, telephone number and health insurance claim number of the beneficiary, a summary of the complaint, and any actions taken to resolve it.</li>
                    <li style="list-style-type: decimal">A supplier must agree to furnish CMS any information required by the Medicare statute and regulations.</li>
                    <li style="list-style-type: decimal">All suppliers must be accredited by a CMS-approved accreditation organization in order to receive and retain a supplier billing number. The accreditation must indicate the specific products and services, for which the supplier is accredited in order for the supplier to receive payment for those specific products and services (except for certain exempt pharmaceuticals).</li>
                    <li style="list-style-type: decimal">All suppliers must notify their accreditation organization when a new DMEPOS location is opened.</li>
                    <li style="list-style-type: decimal">All supplier locations, whether owned or subcontracted, must meet the DMEPOS quality standards and be separately accredited in order to bill Medicare.</li>
                    <li style="list-style-type: decimal">All suppliers must disclose upon enrollment all products and services, including the addition of new product lines for which they are seeking accreditation.</li>
                    <li style="list-style-type: decimal">A supplier must meet the surety bond requirements specified in 42 CFR § 424.57 (d).</li>
                    <li style="list-style-type: decimal">A supplier must obtain oxygen from a state-licensed oxygen supplier.</li>
                    <li style="list-style-type: decimal">A supplier must maintain ordering and referring documentation consistent with provisions found in 42 CFR § 424.516(f).</li>
                    <li style="list-style-type: decimal">A supplier is prohibited from sharing a practice location with other Medicare providers and suppliers.</li>
                    <li style="list-style-type: decimal">A supplier must remain open to the public for a minimum of 30 hours per week except physicians (as defined in section 1848(j) (3) of the Act) or physical and occupational therapists or a DMEPOS supplier working with custom made orthotics and prosthetics.</li>                
                </ul>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table id="tblPameto" runat="server" width="100%" style="border-top: 1px solid black">
                    <tr align="center">
                        <td>
                            <asp:Label ID="lblOrgDate" runat="server" Font-Bold="true" Text="1/4/2012"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblPalmetto" runat="server" Font-Bold="true" Text="Palmetto GBA"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblPageNum" runat="server" Font-Bold="true" Text="Page 1 of 1"></asp:Label>
                        </td>
                    </tr>
                    <tr align="center">
                        <td>
                        </td>
                        <td>
                            <asp:Label ID="lblNationalSup" runat="server" Font-Names="Arial" Font-Size="11pt"
                                Text="National Supplier Clearinghouse"></asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr align="center">
                        <td>
                        </td>
                        <td>
                            <asp:Label ID="lblAddressPhone" runat="server" CssClass="smallAddress" Text="P.O. Box 100142 &#183; Columbia, South Carolina &#183; 29202-3142 &#183; (866) 238-9652"></asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr align="center">
                        <td>
                        </td>
                        <td>
                            <asp:Label ID="lblCMS" runat="server" CssClass="contentArial" Font-Italic="true"
                                Text="A CMS Contracted Intermediary and Carrier"></asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
