﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;


namespace BregVision
{
    public class VisionPageBase
    {
        public static LoginContext CurrentLoginContext{get; private set;}

        protected void Page_Init(object sender, EventArgs e)
        {
            CurrentLoginContext = new LoginContext();
            CurrentLoginContext.UserName = HttpContext.Current.User.Identity.Name;
            //CurrentLoginContext.UserID = 
        }

        public class LoginContext
        {
            public int UserID { get; set; }
            public string UserName { get; set; }
            public int PracticeID { get; set; }
            public string PracticeName { get; set; }
            public int PracticeLocationID { get; set; }
            public string PracticeLocationName { get; set; }
        }
    }

    
}
