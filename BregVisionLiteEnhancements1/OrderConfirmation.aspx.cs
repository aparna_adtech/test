using System;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.UI;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace BregVision
{
    public partial class OrderConfirmation : BregVision.Bases.PageBase
    {
		#region properties

		public int BregVisionOrderID
		{
			get
			{
				string bvoID = ParentBregVisionOrderID.Value;
				int bvoIDVal = 0;
				if (Int32.TryParse(bvoID, out bvoIDVal))
					return bvoIDVal;
				return 0;
			}
			set
			{
				ParentBregVisionOrderID.Value = value.ToString();
			}
		}

		public int? ShoppingCartID
		{
			get
			{
				string shoppingCartID = ParentShoppingCartID.Value;
				int shoppingCartIDVal = 0;
				if (Int32.TryParse(shoppingCartID, out shoppingCartIDVal))
					return shoppingCartIDVal;
				return 0;
			}
			set
			{
				ParentShoppingCartID.Value = value.ToString();
			}
		}

		#endregion
		public bool IsCustomBrace
		{
			get; set;
		}


        protected void Page_Load(object sender, EventArgs e)
        {
			// hide location selection dropdown because it's not valid here
			if (!Page.IsPostBack)
            {
                var m = this.Master as BregVision.MasterPages.SiteMaster;
                if (m != null)
                    m.SetLocationDropdownVisibility(false);
            }

            string errorMessage = "";
            int PONumber = 0;
            bool isConsignment = false;

			var referer = Request.ServerVariables["HTTP_REFERER"];
			if ((!IsPostBack) && referer != null && referer.ToLower().Contains("invoicesnapshot"))
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "PreventBackButton", "<script language=JavaScript>try {window.history.forward(1);} catch (e) {}</script>");
                Int32 PLID = 0;
                if (Session["PracticeLocationID"] != null)
                {
                    PLID = Convert.ToInt32("0" + Session["PracticeLocationID"].ToString());
                }
                if (PLID > 0)
                {
                    Int32 SCID = 0;
                    if (Session["PracticeLocationID"] != null)
                    {
                        SCID = GetShoppingCartID(Convert.ToInt32("0" + Session["PracticeLocationID"].ToString()));
                    }
                    if (SCID > 0)
                    {
                        isConsignment = DAL.PracticeLocation.ShoppingCart.GetConsignment(SCID, 0);
                        Int16 STID = 0;
                        STID = Convert.ToInt16(DAL.PracticeLocation.ShoppingCart.GetShippingType(PLID));

                        if (STID > 0)
                        {
							ShoppingCartID = STID;
							var CustomPurchaseOrderCode = GetCustomPurchaseOrderCode(SCID);

                            PONumber = CreatePurchaseOrder(PLID, SCID, 1, STID, CustomPurchaseOrderCode);

                            if (CustomPurchaseOrderCode.Length > 0)
                            {
                                lblCustomPONum.Text = String.Concat("Custom Purchase Order Number:", CustomPurchaseOrderCode.ToString());
                                lblPONumber.Text = "";
                            }
                            else
                            {
                                if (PONumber > 0)
                                {
                                    lblPONumber.Text = String.Concat("Purchase Order Number:", PONumber.ToString());
                                    lblCustomPONum.Text = "";
                                }
                                else
                                {
                                    errorMessage = "Unable to create PO Number";
                                    lblPODetail.Text = errorMessage;
                                    throw new ApplicationException(errorMessage);
                                }

                            }

                            FaxEmailPOs FEPos = new FaxEmailPOs();

							BregVisionOrderID = PONumber - 100000;
                            ViewState["BVOID"] = BregVisionOrderID;


                            //Creates Order Confirmation data
                            string PODetail = FEPos.GetSuppliersAndLineItems(BregVisionOrderID);

                            //Creates and send fax or email to each supplier
                            FEPos.PracticeLocationID = PLID;
                            FEPos.PracticeName = Session["PracticeName"].ToString();
                            FEPos.PracticeLocationName = Session["PracticeLocationName"].ToString();

                            

                            try
                            {

                                //send this string into "PrepareFaxEmailforSupplier" and append it to the string builder
                                DataSet dsCustomBraceInfo = DAL.PracticeLocation.Inventory.GetCustomBraceExtendedInfoFromOrder(SCID);
								IsCustomBrace = false;
                                byte[] customBracePDF = new byte[0];

                                if (dsCustomBraceInfo.Tables[0].Rows.Count == 1)
									IsCustomBrace = true;

                                if (IsCustomBrace == true)
									customBracePDF = FEPos.PrepareFaxEmailforCustomBrace(practiceID, BregVisionOrderID, dsCustomBraceInfo.Tables[0].Rows[0]);
                                
								FEPos.PrepareFaxEmailforSupplier(BregVisionOrderID, customBracePDF, SCID, visionSessionProvider);

                                LiteralControl poDetail = new LiteralControl(PODetail);
                                plPODetail.Controls.Add(poDetail);
                            }
                            catch (Exception prepEx)
                            {
                                errorMessage = "Error Preparing Fax Email for Supplier";
                                lblPODetail.Text = errorMessage;
                                //throw new ApplicationException(errorMessage, prepEx);
                            }

                            //lblPODetail.Text = PODetail;
                        }
                        else
                        {
                            errorMessage = "Unable to retrieve Shipping Type ID";
                            lblPODetail.Text = errorMessage;
                            throw new ApplicationException(errorMessage);
                        }
                    }
                    else
                    {
                        errorMessage = "Unable to retrieve Shopping Cart ID";
                        lblPODetail.Text = errorMessage;
                        throw new ApplicationException(errorMessage);
                    }
                }
                else
                {
                    errorMessage = "Unable to retrieve Practice Location ID";
                    lblPODetail.Text = errorMessage;
                    throw new ApplicationException(errorMessage);
                }
            }
			CustomBrace.DataBind();
        }

        private static string GetCustomPurchaseOrderCode(Int32 SCID)
        {
            try
            {
                var db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                var cmd = db.GetStoredProcCommand("usp_ShoppingCart_Select_CustomPurchaseOrderCodeByShoppingCartID");
                db.AddInParameter(cmd, "ShoppingCartID", DbType.Int32, SCID);
                return db.ExecuteScalar(cmd).ToString();
            }
            catch
            {
                return "***ERR***";
            }
        }

        protected Int32 GetShoppingCartID(Int32 PracticeLocationID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetShoppingCartID");
            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);

            Int32 SQLReturn = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            return SQLReturn;
        }

        protected Int32 CreatePurchaseOrder(Int32 PracticeLocationID,
                Int32 ShoppingCartID, Int32 UserID, Int16 ShippingTypeID, string CustomPurchaseOrderCode)
        {
            Int32 PurchaseOrderID = 0;
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_CreatePurchaseOrderNew");
            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);
            db.AddInParameter(dbCommand, "ShoppingCartID", DbType.Int32, ShoppingCartID);
            db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
            db.AddInParameter(dbCommand, "ShippingTypeID", DbType.Int16, ShippingTypeID);
            db.AddInParameter(dbCommand, "CustomPurchaseOrderCode", DbType.String, CustomPurchaseOrderCode);
            db.AddOutParameter(dbCommand, "PurchaseOrderID", DbType.Int32, PurchaseOrderID);
            
            Int32 SQLReturn = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            Int32 return_value = Convert.ToInt32("0" + db.GetParameterValue(dbCommand, "PurchaseOrderID").ToString());
            return return_value;
        }

        //protected void btnPrintReceipt_Click(object sender, EventArgs e)
        //{
        //    Int32 bvoid = 0;
        //    bvoid = Convert.ToInt32("0" + ViewState["BVOID"].ToString());
        //    string strURL = String.Concat("Authentication/OrderConfirmationReceipt.aspx?BVOID=", (Int32)ViewState["BVOID"]);
        //    ResponseHelper.Redirect(strURL, "_blank", "");
        //}      
    }
}
