using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.TelerikAjax;
using ClassLibrary.DAL;
using DAL.PracticeLocation;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.Web.UI;

namespace BregVision
{
    public partial class Inventory : Bases.PageBase
    {
        #region Private Variables
        int productsAdded = 0;
        Int32 ShoppingCartItemID = 0;

        #endregion

        #region Page Events

        protected void Page_Init(object sender, EventArgs e)
        {
            Master.LocationChanged += new CommandEventHandler(LocationChangedFromMasterPage);

            ctlSearchToolbar.Grid = grdInventory;
            ctlSearchToolbar.DefaultLoadingPanel = RadAjaxLoadingPanel1;
            ctlSearchToolbar.searchEvent += new BregVision.UserControls.General.SearchEventHandler(ctlSearchToolbar_searchEvent);

            ctlSearchToolbar.ClearDropdownButtons();
            ctlSearchToolbar.AddItem("Product Code", "Code");
            ctlSearchToolbar.AddItem("Product Name", "Name");
            ctlSearchToolbar.AddItem("Manufacturer", "Brand");
            ctlSearchToolbar.AddItem("HCPCs", "HCPCs");

            if (Context.User.IsInRole("PracticeAdmin"))
            {
                chkAllLocations.Visible = true;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();

            Master.SelectCurrentTab("Inventory");

            if (grdInventory.HasInitiallyBound == false)
            {
                grdInventory.SetGridLabel("", Page.IsPostBack, lblBrowseDispense, true);//set the default text on the label
                grdInventory.PageSize = recordsDisplayedPerPage;
                grdInventory.InitializeSearch();
            }

        }

        #endregion




        private void LocationChangedFromMasterPage(object sender, CommandEventArgs e)
        {
            Response.Redirect("Inventory.aspx", false);
        }

        private void InitializeAjaxSettings()
        {
            if (RadAjaxManager1 != null)
            {
                RadAjaxManager1.AjaxSettings.AddAjaxSetting(ctlRecordsPerPage.RecordsDisplayedTextBox, grdInventory, RadAjaxLoadingPanel1);
                RadAjaxManager1.AjaxSettings.AddAjaxSetting(btnAddtoCart, Master.SiteCart.RadButtonItemsInCartButton);
                RadAjaxManager1.AjaxSettings.AddAjaxSetting(grdInventory, Master.SiteCart.RadButtonItemsInCartButton);
                RadAjaxManager1.AjaxSettings.AddAjaxSetting(ctlSearchToolbar.Toolbar, ctlSearchToolbar.Toolbar);
            }
        }

        void ctlSearchToolbar_searchEvent(object sender, BregVision.UserControls.General.SearchEventArgs e)
        {
			grdInventory.SearchText = e.SearchText;
            grdInventory.SearchCriteria = e.SearchCriteria;
            grdInventory.SearchType = e.SearchType;
            grdInventory.SetSearchCriteria(e.SearchType);

			lblStatus.Text = string.Empty;
			lblBrowseDispense.Text = string.Empty;
			lblStatusReplaced.Text = string.Empty;
			lblStatusReplacedText.Text = string.Empty;
		}

		#region Grid Commands

		protected void grdInventory_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable)
            {
                DataSet ds = DAL.PracticeLocation.Supplier.GetSuppliers(practiceLocationID);

                grdInventory.DataSource = ds;
            }

        }

        protected void grdInventory_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
        }

		private class CacheItem
		{
			public CacheItem(DateTime dateCached, DataSet data)
			{
				DateCached = dateCached;
				Data = data;
			}
			public static string GetKey(int requestedPracticeID, int practiceLocationID, string searchCriteria, string searchText)
			{
				string key = requestedPracticeID + "|" + practiceLocationID + "|" + searchCriteria + "|" + searchText;
				return key;
			}
			public DateTime DateCached;
			public DataSet Data;
		}

		private static Dictionary<string, CacheItem> _dataCache = new Dictionary<string, CacheItem>();
		private const int _dataCacheAgeSeconds = 30;
        protected void grdInventory_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {
            Int32 requestedPracticeID = 0;
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;

            if (chkAllLocations.Checked == true)
            {
                requestedPracticeID = practiceID;
            }

			DataSet ds = null;
			List<string> deleteKeys = new List<string>();
			foreach (string itemKey in _dataCache.Keys)
			{
				CacheItem item = _dataCache[itemKey];
				if (DateTime.Now > item.DateCached.AddSeconds(_dataCacheAgeSeconds))
				{
					deleteKeys.Add(itemKey);
				}
			}
			foreach (string deleteKey in deleteKeys)
			{
				_dataCache.Remove(deleteKey);
			}

			string key = CacheItem.GetKey(requestedPracticeID, practiceLocationID, grdInventory.SearchCriteria, grdInventory.SearchText);
			if (_dataCache.ContainsKey(key))
				ds = _dataCache[key].Data;
			else
			{
				ds = DAL.PracticeLocation.Inventory.GetAllProductDatabyLocation(requestedPracticeID, practiceLocationID, grdInventory.SearchCriteria, grdInventory.SearchText);
				_dataCache.Add(key, new CacheItem(DateTime.Now, ds));
			}

			e.DetailTableView.DataSource = GetProductInventorySorted(ds, dataItem);
           
            grdInventory.SetGridLabel(grdInventory.SearchCriteria, Page.IsPostBack, lblBrowseDispense);
            grdInventory.PageSize = recordsDisplayedPerPage;
            e.DetailTableView.PageSize = recordsDisplayedPerPage;
            grdInventory.SetRecordCount(ds, "Product");
            //TelerikAjax.UIHelper.FilterOnJoinKey(e, "SupplierID", false);

            if (chkAllLocations.Checked == true)
            {
                e.DetailTableView.Columns.FindByDataField("PracticeLocationName").Visible = true;
            }
        }

        public List<ProductInventoryDataRow> GetProductInventorySorted(DataSet ds, GridDataItem dataItem)
        {
            List<ProductInventoryDataRow> products = new List<ProductInventoryDataRow>();
            foreach (
                var dataRow in
                    ds.Tables[0].Rows.Cast<DataRow>()
                        .Where(
                            x =>
                                !(x["SupplierID"] is DBNull) &&
                                (int.Parse(x["SupplierID"].ToString()) == dataItem.GetColumnInt32("SupplierID"))))
                //.Select(dataRow => new ProductInventoryDataRow()
            {
				var qoh = Convert.ToInt32(dataRow["QuantityOnHand"]);
				var qor = Convert.ToInt32(dataRow["QuantityOnOrder"]);
                var qohPlusOrder = qoh + qor;
                products.Add(new ProductInventoryDataRow()
                {
                    Priority = Convert.ToInt32(dataRow["Priority"]),
                    SupplierID = Convert.ToInt32(dataRow["SupplierID"]),
                    IsShoppingCart = Convert.ToBoolean(dataRow["IsShoppingCart"]),
                    Product = Convert.ToString(dataRow["Product"]),
                    Code = Convert.ToString(dataRow["Code"]),
                    Packaging = Convert.ToString(dataRow["Packaging"]),
                    LeftRightSide = Convert.ToString(dataRow["LeftRightSide"]),
                    Size = Convert.ToString(dataRow["Size"]),
                    Gender = Convert.ToString(dataRow["Gender"]),
                    Color = Convert.ToString(dataRow["Color"]),
                    ParLevel = Convert.ToInt32(dataRow["ParLevel"]),
                    QuantityOnHand = qoh,
                    QuantityOnOrder = qor,
                    SuggestedReorderLevel = Convert.ToInt32(dataRow["SuggestedReorderLevel"]),
                    IsNotFlaggedForReorder = Convert.ToBoolean(dataRow["IsNotFlaggedForReorder"]),
                    CriticalLevel = Convert.ToInt32(dataRow["CriticalLevel"]),
                    PracticeLocationName = Convert.ToString(dataRow["PracticeLocationName"]),
                    practicecatalogproductid = Convert.ToInt32(dataRow["practicecatalogproductid"]),
					NewPracticeCatalogProductID = Convert.ToInt32(dataRow["NewPracticeCatalogProductID"]),
					NewProductCode = Convert.ToString(dataRow["NewProductCode"]),
					NewProductName = Convert.ToString(dataRow["NewProductName"]),
                });

               
            }
            return products.OrderBy(x => x.Priority).ThenBy(x=>x.Product).ToList();
        }

        protected void grdInventory_PreRender(object sender, EventArgs e)
        {
            grdInventory.ExpandGrid(); // UIHelper.ExpandFormatGrid(grdInventory);
        }

        #endregion

        #region Control Events

        protected void btnAddtoCart_Click(object sender, EventArgs e)
        {
            if (chkAllLocations.Checked == false)
            {
                LoopHierarchyRecursive(grdInventory.MasterTableView);
            }
            else
            {
                lblStatus.Text = "Products cannot be added to cart while 'Search All Locations' is selected. No products added to cart.";
            }
        }

        #endregion


        private void LoopHierarchyRecursive(GridTableView gridTableView)
        {
			lblStatusReplaced.Text = string.Empty;
			lblStatusReplacedText.Text = string.Empty;
			lblStatus.Text = string.Empty;
			ShoppingCartItemID = 0;
            foreach (GridNestedViewItem nestedViewItem in gridTableView.GetItems(GridItemType.NestedView))
            {
                if (nestedViewItem.NestedTableViews.Length > 0)
                {
                    foreach (GridDataItem gridDataItem in nestedViewItem.NestedTableViews[0].Items)
                    {

                        if (findSelectColumn(gridDataItem))
                        {
                            Int32 ReOrderQuantity = findReorderQuantity(gridDataItem);
                            //Check to make sure reorder quantity greater than 0, of not, no need to update cart
                            if (ReOrderQuantity > 0)
                            {
                                try                                   //practicecatalogproductid
                                {
                                    ShoppingCartItemID = DAL.PracticeLocation.Inventory.UpdateGrdInventoryData(practiceLocationID,
										GetReplacementProduct(gridDataItem, lblStatusReplaced, lblStatusReplacedText), 1, ReOrderQuantity);
                                    productsAdded += ReOrderQuantity;
                                }
                                catch (System.Data.SqlClient.SqlException ex)
                                {
                                    if (ex.Class == 17 && ex.Number == 50000)
                                    {
                                        string x = ex.Message;
                                        lblStatus.Text = "No products added to cart (Custom Braces must be the only item in the cart)";
                                        lblStatus.CssClass = "ErrorMsg";
                                        lblStatus.Visible = true;
                                        return;
                                    }
									if (ex.Class == 18 && ex.Number == 50000)
                                    {
										string x = ex.Message;
										if (ex.Errors.Count > 0)
											x = ex.Errors[0].Message;
										lblStatus.Text = x;
										lblStatus.CssClass = "ErrorMsg";
                                        lblStatus.Visible = true;
                                        return;
                                    }
								}
								finally
                                {
                                    // Used to clear out select box after adding product to cart
                                    TableCell cell = gridDataItem["ClientSelectColumn"];
                                    CheckBox checkBox = (CheckBox)cell.Controls[0];

                                    checkBox.Checked = false;
                                }
                            }

							// Set Status indicator
							if (productsAdded == 0)
							{
								string StatusText = "No products added to cart";
								lblStatus.CssClass = "ErrorMsg";
								lblStatus.Text = StatusText;
							}
							else
							{
								string StatusText = string.Concat("Number of products added to cart: ", productsAdded.ToString());
								lblStatus.CssClass = "SuccessMsg";
								lblStatus.Text = StatusText;
							}
							lblStatus.Visible = true;
						}

					}  
                }
            }
            if (productsAdded == 0)
            {
                lblStatus.Text = "Products selected have a quantity of 0. No products added to cart.";
                lblStatus.CssClass = "ErrorMsg";
            }
            else
            {
                string StatusText = string.Concat("Number of products added to cart: ", productsAdded.ToString());
                lblStatus.CssClass = "SuccessMsg";
                lblStatus.Text = StatusText;
                //Need to rebind the grid to reflect any changes such as unchecking the clientselectcolumn
                grdInventory.RebindWithExpandState(); //MWS the rebind looses state

            }
        }

		private int GetReplacementProduct(GridDataItem gridDataItem, Label replaceStatusLabel, Label replaceStatusTextLabel)
		{
			string replacementProductCode = findReplacementProductCode(gridDataItem);
			string replacementProductName = findReplacementProductName(gridDataItem);
			if (replacementProductName != null)
			{
				string productCode = findProductCode(gridDataItem);
				string productName = findProductName(gridDataItem);
				int productCount = findReorderQuantity(gridDataItem);
				DisplayReplacementWarning(productCode, productName, productCount, replacementProductCode, replacementProductName, replaceStatusLabel, replaceStatusTextLabel);
				return findReplacementPracticeCatalogProductID(gridDataItem);
			}
			return findPracticeCatalogProductID(gridDataItem);
		}

		private void DisplayReplacementWarning(string productCode, string productName, int productCount, string replacementProductCode, string replacementProductName, Label replaceStatusLabel, Label replaceStatusTextLabel)
		{
			string lineStart = replaceStatusTextLabel.Text != string.Empty ? "<br>" : "NOTE: Where available, the following products will be replaced in the CART:<br><br>";// string.Empty;
			replaceStatusTextLabel.Text += lineStart + "(" + productCount.ToString() + ") [{" + productCode + "} " + productName + "] product(s) replaced with [{" + replacementProductCode + "} " + replacementProductName + "]";
			replaceStatusLabel.Text = "<span></span>";
			replaceStatusTextLabel.Visible = true;
			replaceStatusLabel.Visible = true;
		}

		private string findProductCode(GridDataItem gridDataItem)
		{
			var productName = (string)gridDataItem["code"].Text;
			if (productName != null)
			{
				return productName;
			}
			else
			{
				return "";
			}
		}

		private string findProductName(GridDataItem gridDataItem)
		{
			var productName = (string)gridDataItem["product"].Text;
			if (productName != null)
			{
				return productName;
			}
			else
			{
				return "";
			}
		}
		private Int32 findPracticeCatalogProductID(GridDataItem gridDataItem)
		{
			var PracticeID = (string)gridDataItem["practicecatalogproductid"].Text;
			if (PracticeID != null)
			{
				return Int32.Parse(PracticeID);
			}
			else
			{
				return 0;
			}
		}

		private Int32 findReplacementPracticeCatalogProductID(GridDataItem gridDataItem)
		{
			var PracticeID = (string)gridDataItem["newpracticecatalogproductid"].Text;
			if (PracticeID != null)
			{
				Int32 practiceCatalogProductID = 0;
				if (Int32.TryParse(PracticeID, out practiceCatalogProductID))
					return practiceCatalogProductID;
				return findPracticeCatalogProductID(gridDataItem);
			}
			else
			{
				return 0;
			}
		}

		private string findReplacementProductCode(GridDataItem gridDataItem)
		{
			var newProductCode = gridDataItem["NewProductCode"];
			if (newProductCode != null && newProductCode.Text.Replace("&nbsp;", "") != string.Empty)
				return newProductCode.Text;
			return null;
		}

		private string findReplacementProductName(GridDataItem gridDataItem)
		{
			var newProductName = gridDataItem["NewProductName"];
			if (newProductName != null && newProductName.Text.Replace("&nbsp;", "") != string.Empty)
				return newProductName.Text;
			return null;
		}

        private Int32 findReorderQuantity(GridDataItem gridDataItem)
        {
            RadNumericTextBox nTextBox = (RadNumericTextBox)gridDataItem.FindControl("txtSuggReorderLevel");
            if (nTextBox != null)
            {
                return Int32.Parse(nTextBox.Value.ToString());
            }
            else
            {
                return 0;
                //throw new Exception("Cannot find object with ID 'CheckBox1' in item " + gridDataItem.ItemIndex.ToString());
            }
        }

        private Boolean findSelectColumn(GridDataItem gridDataItem)
        {
            TableCell cell = gridDataItem["ClientSelectColumn"];
            CheckBox checkBox = (CheckBox)cell.Controls[0];
            if (checkBox.Checked)
                return true;
            else
            {
                return false;
            }


        }

        protected void grdInventory_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is Telerik.Web.UI.GridDataItem && e.Item.OwnerTableView.Name == "ChildGrid")
            {
                Telerik.Web.UI.GridDataItem dataItem = (Telerik.Web.UI.GridDataItem)e.Item;               
				int priority = System.Convert.ToInt32(dataItem["Priority"].Text);
                var lblPriority = dataItem.FindControl("lblPriority") as Label;
                //Set Reorder Level
                if (priority == 1)
                {
                    lblPriority.CssClass = "priority-1";
                }
                else if (priority == 2)
                {
                    lblPriority.CssClass = "priority-2";
                }
                else if (priority == 3)
                {
                    lblPriority.CssClass = "priority-3";
                }
                else if (Boolean.Parse(dataItem["IsShoppingCart"].Text))
                {
                    lblPriority.CssClass = "shoppingCart";
                }

            }
        }

        public static int GetPriorityLevelForInventory(int? quantityOnOrder, int? quantityOnHandPlusOnOrder, int? reorderLevel, int? criticalLevel, int? parlevel, bool isNotFlaggedForReorder)
        {
            var priority = 0;
            if (quantityOnOrder > 0)
                priority = 3;
            if ((quantityOnHandPlusOnOrder <= reorderLevel) && parlevel > 0 && !isNotFlaggedForReorder)
                priority = 2;
            if ((quantityOnHandPlusOnOrder <= criticalLevel) && parlevel > 0 && !isNotFlaggedForReorder)
                priority = 1;

            return priority == 0 ? 4 : priority;
        }
    }

}

