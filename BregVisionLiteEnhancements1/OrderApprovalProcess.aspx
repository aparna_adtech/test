<%@ Page Language="C#" MasterPageFile="~/MasterPages/SiteMaster.Master" AutoEventWireup="true" CodeBehind="OrderApprovalProcess.aspx.cs" Inherits="BregVision.OrderApprovalProcess" Title="Untitled Page" %>
 <asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">


<table width="96%" border="0">
    <tr>
        <td colspan="4">
        <b>Order Approved</b>
        </td>
     </tr>
  <tr>
    <td width="21">&nbsp;</td>
    <td>Purchase order number 100003 has been created for the following products.  <br /><br />
    An email has been sent to the user that submitted the order as to the status of the order. <br /><br />
    Please print this for your records. 
  	</td>
    <td colspan="2"></td>
    <td></td>
  </tr> 
  <tr>
  	<td colspan="1">
  	</td>
    <td colspan="2">Order details are below</td>
    <td></td>
 
  </tr>
  <tr>
  	<td colspan="1">
  	</td>
    <td colspan="2" align="center">
       <table width="90%">
        <tr>
            <td colspan="6">
                Breg
            </td>
        </tr>
        <tr bgcolor="blue" style="color:White">
            <td>
                &nbsp;
            </td>
            <td>
                Product
            </td>
            <td>
            Quantity
            </td>
            <td>
            Cost
            </td>
            <td>
            Sub Total
            </td>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                
            </td>
            <td>
                Test 1
            </td>
            <td>
            3
            </td>
            <td>
            $10.00
            </td>
            <td>
            $30
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                
            </td>
            <td>
                Test Product 2
            </td>
            <td>
            5
            </td>
            <td>
            $20.00
            </td>
            <td>
            $100.00
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                
            </td>
            <td>
                
            </td>
            <td>
            
            </td>
            <td>
            
            </td>
            <td>
            Supplier Total: $130
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                DJ Ortho
            </td>
        </tr>
        <tr bgcolor="blue" style="color:White">
            <td>
                &nbsp;
            </td>
            <td>
                Product
            </td>
            <td>
            Quantity
            </td>
            <td>
            Cost
            </td>
            <td>
            Sub Total
            </td>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                
            </td>
            <td>
                Test Product 3
            </td>
            <td>
            10
            </td>
            <td>
            $15.00
            </td>
            <td>
            $150
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                
            </td>
            <td>
                Test Product 4
            </td>
            <td>
            25
            </td>
            <td>
            $40.00
            </td>
            <td>
            $1000.00
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                
            </td>
            <td>
                
            </td>
            <td>
            
            </td>
            <td>
            
            </td>
            <td>
            Supplier Total: $1150
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                
            </td>
            <td>
                
            </td>
            <td>
            
            </td>
            <td>
            
            </td>
            <td>
            
            </td>
            <td>Grand Total: $1280
            </td>
        </tr>
        </table>
    
    </td>
    <td></td>


  </table>
  
  
 </asp:Content>

