using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.WebControls;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common; 

namespace BregVision
{
    public partial class PCtoInventory : System.Web.UI.Page
    {

        protected void InitializeComponent()
        {
         
            //grdInventory.ItemDataBound += new GridItemEventHandler(grdInventory_ItemDataBound);
            grdInventory.ItemDataBound += new GridItemEventHandler(grdInventory_ItemDataBound);
            grdInventory.PageIndexChanged += new GridPageChangedEventHandler(grdInventory_PageIndexChanged);
            grdPracticeCatalog.PageIndexChanged += new GridPageChangedEventHandler(grdPracticeCatalog_PageIndexChanged);
            this.Load += new System.EventHandler(this.Page_Load);

        }

        protected void grdInventory_PageIndexChanged(object source, Telerik.WebControls.GridPageChangedEventArgs e)
        {
            this.grdInventory.CurrentPageIndex = e.NewPageIndex;
            //LoopHierarchyRecursive(grdInventory.MasterTableView);
            //LoadData();
            //this.grdInventory.DataBind();
        }

        protected void grdPracticeCatalog_PageIndexChanged(object source, Telerik.WebControls.GridPageChangedEventArgs e)
        {
            this.grdPracticeCatalog.CurrentPageIndex = e.NewPageIndex;
            //LoopPracticeCatalog(grdPracticeCatalog.MasterTableView);
            //LoopHierarchyRecursive(grdInventory.MasterTableView);
            //LoadData();
            //this.grdInventory.DataBind();
        }

        public DataTable Grid1Data
        {
            get { return (DataTable)Session["_grid1Data"]; }
            set { Session["_grid1Data"] = value; }
        }
        public DataTable Grid2Data
        {
            get { return (DataTable)Session["_grid2Data"]; }
            set { Session["_grid2Data"] = value; }
        } 

        protected void Page_Load(object sender, EventArgs e)
        {
             if (!IsPostBack) 
            {//'init the datasource for both grids

                //DataTable Grid1Data = GetMasterCatalogSuppliers();
                 //DataTable Grid2Data = GetPracticeCatalogData();
                 //Grid2Data = GetDataTable(("SELECT * FROM Mails Where FolderName = 'Junk E-Mail'"))
            }
            
            //if ((Request.Form["__EVENTTARGET"] == "RowMoved"))
            //{
            //    object[] newRow;
            //    string[] args = Request.Form["__EVENTARGUMENT"].Split(',');
            //    Telerik.WebControls.RadGrid srcGrid = (Telerik.WebControls.RadGrid)this.FindControl(args[0]);
            //    int srcRowIndex = srcGrid.CurrentPageIndex * srcGrid.PageSize + int.Parse(args[1]);
            //    //deleting 
            //    if ((args[2] == "DeleteRow"))
            //    {
            //        if ((srcGrid.ID == grdInventory.ID))
            //        {
            //             Grid1Data.Rows.RemoveAt(srcRowIndex);
            //        }
            //        else
            //        {
            //            Grid2Data.Rows.RemoveAt(srcRowIndex);
            //        }
            //    }
            //    //moving from grid1 to grid2 
            //    if ((args[2] == grdPracticeCatalog.ID))
            //    {
            //        newRow = Grid1Data.Rows[srcRowIndex].ItemArray;
            //        Grid1Data.Rows.RemoveAt(srcRowIndex);
            //        Grid2Data.Rows.Add(newRow);
            //    }
            //    //moving from grid2 to grid1 
            //    if ((args[2] == grdInventory.ID))
            //    {
            //        newRow = Grid2Data.Rows[srcRowIndex].ItemArray;
            //        Grid2Data.Rows.RemoveAt(srcRowIndex);
            //        Grid1Data.Rows.Add(newRow);
            //    }
            //    if ((srcGrid.Items.Count == 1))
            //    {
            //        srcGrid.CurrentPageIndex = 0;
            //    }

            //    //rebind grids after modifications 
            //    grdInventory.DataSource = Grid1Data;
            //    grdInventory.DataBind();
            //    grdPracticeCatalog.DataSource = Grid2Data;
            //    grdPracticeCatalog.DataBind();
            //} 

        }
    
     //protected void grdInventory_NeedDataSource(Object Source, GridNeedDataSourceEventArgs e) 
     //{

     //    grdInventory.DataSource = GetMasterCatalogData();
 
     //}

     public DataSet GetMasterCatalogSuppliers()
     {
         //int rowsAffected = 0;

         Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
         DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetMasterCatalogSuppliers");

         //db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
         DataSet ds = db.ExecuteDataSet(dbCommand);
         return ds;


     }
        public DataSet GetPracticeCatalogSuppliers(Int32 practiceID)
        {
            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetPracticeCatalogSupplierBrands");

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;


        }
        protected void grdInventory_DetailTableDataBind(object source, Telerik.WebControls.GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;

            e.DetailTableView.DataSource = GetInventoryBySupplier(Convert.ToInt32(Session["PracticeLocationID"]));
          
        }
        
        protected void grdPracticeCatalog_DetailTableDataBind(object source, Telerik.WebControls.GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;

            e.DetailTableView.DataSource = GetPracticeCatalogDataBySupplier(Convert.ToInt32(Session["practiceid"]));

        }
     public DataSet GetMasterCatalogDataBySupplier()
     {
         //int rowsAffected = 0;

         Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
         DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetMasterCatalogProductInfo");

         //db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);

         DataSet ds = db.ExecuteDataSet(dbCommand);
         return ds;

     }

        public DataSet GetPracticeCatalogDataBySupplier(Int32 practiceID)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetPracticeCatalogProductInfoMCTOPC");

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);

            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;

        }

        protected void grdInventory_NeedDataSource(object source, Telerik.WebControls.GridNeedDataSourceEventArgs e)
         {
             if (!e.IsFromDetailTable)
             {
                 grdInventory.DataSource = GetSuppliers(Convert.ToInt32(Session["PracticeLocationID"]));
             }
         }

        protected void grdInventory_PreRender(object sender, EventArgs e)
         {

             if (!IsPostBack)
             {
                 foreach (GridDataItem item in grdInventory.MasterTableView.Items)
                 {
                     if (item.ItemIndex == 0)
                     {
                         item.Expanded = true;
                         //item.ChildItem.NestedTableViews[0].Items[1].Expanded = true;
                     }

                 }
             }
         }


       protected void grdPracticeCatalog_NeedDataSource(object source, Telerik.WebControls.GridNeedDataSourceEventArgs e)
         {
             if (!e.IsFromDetailTable)
             {
                 grdPracticeCatalog.DataSource = GetPracticeCatalogSuppliers(Convert.ToInt32(Session["practiceid"]));
                 //grdPracticeCatalog.r
             }
         }

       protected void grdPracticeCatalog_PreRender(object sender, EventArgs e)
         {

             if (!IsPostBack)
             {
                 foreach (GridDataItem item in grdPracticeCatalog.MasterTableView.Items)
                 {
                     if (item.ItemIndex == 0)
                     {
                         item.Expanded = true;
                         //item.ChildItem.NestedTableViews[0].Items[1].Expanded = true;
                     }

                 }
             }
         }


        private DataTable GetMasterCatalogData()
        {
            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetMasterCatalogProductInfo");

            DataTable dt = db.ExecuteDataSet(dbCommand).Tables[0];
            return dt;
        }

        //protected void grdPracticeCatalog_NeedDataSource(Object Source, GridNeedDataSourceEventArgs e)
        //{

        //    grdPracticeCatalog.DataSource = GetPracticeCatalogData();
 

        //}
       
        private DataTable GetPracticeCatalogData()
        {
            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetPracticeCatalogProductInfo");
            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, Session["PracticeID"]);
            DataTable dt = db.ExecuteDataSet(dbCommand).Tables[0];
            return dt;
        }
        int intPCItems = 0;
        private void LoopHierarchyRecursive(GridTableView gridTableView)
        {

          foreach (GridNestedViewItem nestedViewItem in gridTableView.GetItems(GridItemType.NestedView))
            //foreach (GridNestedViewItem nestedViewItem in gridTableView.GetItems(GridItemType.SelectedItem))
            {
                if (nestedViewItem.NestedTableViews.Length > 0)
                {
                    //foreach (GridDataItem gridDataItem in nestedViewItem.)
                    foreach (GridDataItem gridDataItem in nestedViewItem.NestedTableViews[0].Items)
                    {

                        //foreach (GridDataItem item in RadGrid1.SelectedItems) 
                        if (findSelectColumn(gridDataItem))
                        {

                            //Add to 


                            Int32 practiceCatalogProductID = UpdateInventory(Convert.ToInt32(Session["PracticeLocationID"]), findPracticeCatalogProductID(gridDataItem), 1);
                            intPCItems += 1;
                            ////lblStatus.Text = "Adding items to the cart.";
                            //Int16 ReOrderQuantity = findReorderQuantity(gridDataItem);
                            ////Check to make sure reorder quantity greater than 0, of not, no need to update cart
                            //if (ReOrderQuantity > 0)
                            //{
                            //    ShoppingCartItemID = UpdateGrdInventoryData(Convert.ToInt16(Session["PracticeLocationID"]), findPracticeCatalogProductID(gridDataItem), 1, ReOrderQuantity);
                            //    productsAdded += ReOrderQuantity;
                            //}

                        }
                    }

                }
                grdInventory.Rebind();
                string strPCStatus = String.Concat("Item(s) added to Inventory: ",intPCItems.ToString());
                lblPCStatus.Text = strPCStatus;
            }
            
         }


        private Int32 UpdateInventory(Int32 PracticeLocationID, Int32 PracticeCatalogProductID, Int32 UserID)
        {
        //int rowsAffected = 0;

        Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
        DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_TransferFromPracticeCatalogtoProductInventory");

        db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);
        db.AddInParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, PracticeCatalogProductID);
        db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
        //db.AddInParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, 4);

        Int32 SQLReturn = db.ExecuteNonQuery(dbCommand);
        //return ds;
        
        return SQLReturn;
        }
        
        private Boolean findSelectColumn(GridDataItem gridDataItem)
        {
            TableCell cell = gridDataItem["ClientSelectColumn"];
            CheckBox checkBox = (CheckBox)cell.Controls[0];
            if (checkBox.Checked)
                return true;
            else
            {
                return false;
            }


        }

        private Int16 findPracticeCatalogProductID(GridDataItem gridDataItem)
        {

            string PracticeCatalogProductID = (string)gridDataItem["PracticeCatalogProductID"].Text;
            if (PracticeCatalogProductID != null)
            {
                return Int16.Parse(PracticeCatalogProductID);

            }
            else
            {
                return 0;
            }

        }

        private Int16 findMasterCatalogProductID(GridDataItem gridDataItem)
        {

            string PracticeID = (string)gridDataItem["Mastercatalogproductid"].Text;
            if (PracticeID != null)
            {
                return Int16.Parse(PracticeID);
               
            }
            else
            {
                return 0;
            }

        }

        protected void btnMoveToPC_Click(object sender, EventArgs e)
        {
            LoopHierarchyRecursive(grdPracticeCatalog.MasterTableView);
        }

        private void LoopInventory(GridTableView gridTableView)
        {

            foreach (GridNestedViewItem nestedViewItem in gridTableView.GetItems(GridItemType.NestedView))
            //foreach (GridNestedViewItem nestedViewItem in gridTableView.GetItems(GridItemType.SelectedItem))
            {
                if (nestedViewItem.NestedTableViews.Length > 0)
                {
                    //foreach (GridDataItem gridDataItem in nestedViewItem.)
                    foreach (GridDataItem gridDataItem in nestedViewItem.NestedTableViews[0].Items)
                    {

                        //foreach (GridDataItem item in RadGrid1.SelectedItems) 
                        //if (findSelectColumn(gridDataItem))
                        //{

                            //Add to 

                        Int32 return_Value = UpdateInventoryLevels(findPracticeCatalogProductID(gridDataItem), Convert.ToInt32(Session["PracticeLocationID"]),1, findInventoryLevel(gridDataItem, "txtQOH"), findInventoryLevel(gridDataItem, "txtParLevel"), findInventoryLevel(gridDataItem, "txtReorderLevel"), findInventoryLevel(gridDataItem, "txtCriticalLevel"));
                            //Int32 practiceCatalogProductID = UpdatePracticeCatalog(Convert.ToInt32(Session["PracticeID"]), findPracticeCatalogProductID(gridDataItem), 1);
                            intPCItems += 1;
                            ////lblStatus.Text = "Adding items to the cart.";
                            //Int16 ReOrderQuantity = findReorderQuantity(gridDataItem);
                            ////Check to make sure reorder quantity greater than 0, of not, no need to update cart
                            
                            //decimal test = findPricing(gridDataItem, "txtWholesaleCost");
                            
                        
                            //if (ReOrderQuantity > 0)
                            //{
                            //    ShoppingCartItemID = UpdateGrdInventoryData(Convert.ToInt16(Session["PracticeLocationID"]), findPracticeCatalogProductID(gridDataItem), 1, ReOrderQuantity);
                            //    productsAdded += ReOrderQuantity;
                            //}

                        //}
                    }

                }
                grdInventory.Rebind();
                string strPCStatus = String.Concat("Number of products updated: ", intPCItems.ToString());
                lblPCStatus.Text = strPCStatus;
            }

        }

        private int findInventoryLevel(GridDataItem gridDataItem,string txtBox)
        {
            RadNumericTextBox nTextBox = (RadNumericTextBox)gridDataItem.FindControl(txtBox);
            //CheckBox checkBox = (CheckBox)gridDataItem.FindControl("chkAddtoCart");

            if (nTextBox != null && nTextBox.Text.Length>0)
            {
                return int.Parse(nTextBox.Value.ToString());
                //Response.Write(gridDataItem.ItemIndex.ToString() + " is: " + nTextBox.Value.ToString() + "<br />");
            }
            else
            {
                return 0;
                //throw new Exception("Cannot find object with ID 'CheckBox1' in item " + gridDataItem.ItemIndex.ToString());
            }
        }

        protected void btnUpdateInventory_Click(object sender, EventArgs e)
        {
            LoopInventory(grdInventory.MasterTableView);
        }

        private Int32 UpdateInventoryLevels(Int32 PracticeCatalogProductID, Int32 PracticeLocationID,Int32 UserID,
            int QOH, int ParLevel, int ReorderLevel, int CriticalLevel)

        {
            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_UpdateInventoryLevels");

            db.AddInParameter(dbCommand, "practiceCatalogProductID", DbType.Int32, PracticeCatalogProductID);
            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);
            db.AddInParameter(dbCommand, "UserID", DbType.Int16, UserID);
            db.AddInParameter(dbCommand, "QOH", DbType.Int16, QOH);
            db.AddInParameter(dbCommand, "ParLevel", DbType.Int16, ParLevel);
            db.AddInParameter(dbCommand, "ReorderLevel", DbType.Int16, ReorderLevel);
            db.AddInParameter(dbCommand, "CriticalLevel", DbType.Int16, CriticalLevel);
            //db.AddOutParameter(dbCommand, "ShoppingCartIDReturn", DbType.Int32, rowsAffected);

            int return_value = db.ExecuteNonQuery(dbCommand);
            //return ds;
            //Int32 return_value = Convert.ToInt32(db.GetParameterValue(dbCommand, "ShoppingCartIDReturn"));
            return return_value;
        }
        
        
        public DataSet GetSuppliers(int practiceLocationID)
        {
           //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetSuppliers");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;


        }

        public DataSet GetInventoryBySupplier(int practiceLocationID)
        {
            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetAllInventoryDataforPCToIventory");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);

            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;

        }

        protected void grdInventory_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            
            {

                try
                {


                    GridDataItem dataItem = (GridDataItem)e.Item;
                    //Set Reorder Level
                    if (dataItem["isactive"].Text != "True")
                    {
                        dataItem.BackColor = System.Drawing.Color.Red;
                        dataItem.ForeColor = System.Drawing.Color.White;

                    }
                }
                catch { }
                




            }
        }
    }



}
