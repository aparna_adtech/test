<%@ Page Language="C#" MasterPageFile="~/MasterPages/SiteMaster.Master" AutoEventWireup="true" CodeBehind="InvoiceSnapshot.aspx.cs" Inherits="BregVision.InvoiceSnapshot" Title="Invoice Snapshot" %>

<%@ Register Assembly="RadGrid.Net2" Namespace="Telerik.WebControls" TagPrefix="radG" %>
<%@ Register Assembly="RadInput.Net2" Namespace="Telerik.WebControls" TagPrefix="radI" %>
<%@ Register Src="UserControls/DispensePage/CustomBraceJPG.ascx" TagName="CustomBrace" TagPrefix="bvu" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">

<style>
	.disabledbutton
	{
		background-color:#ddd;
	}
</style>

<script type="text/javascript">

  function showToolTip(element, context) {
    var tooltipManager = $find("<%= RadToolTipManager1.ClientID %>");

    if (!tooltipManager) return;

    var tooltip = tooltipManager.getToolTipByElement(element);

    if (!tooltip) {
      tooltip = tooltipManager.createToolTip(element);

      tooltip.set_value(context);
    }

    element.onmouseover = null;

    try {
      tooltip.show();
    } catch (ex) {
      alert(ex.description);
    }
  }
</script>

<telerik:RadToolTipManager ID="RadToolTipManager1" Width="300px" Height="100px" RelativeTo="Element" Position="BottomRight" runat="server" AnimationDuration="200">
  <WebServiceSettings Path="HelpSystem/ToolTipWebService.asmx" Method="GetToolTip"/>
</telerik:RadToolTipManager>
<div class="flex-row subWelcome-bar">
  <h1>Invoice Snapshot</h1>
  <bvu:RecordsPerPage ID="ctlRecordsPerPage" runat="server"/>
  <div class="flex-row justify-end align-center">
    <asp:Button ID="btnModifyOrder" runat="server" Text="Modify Order" OnClick="ModifyPO_Click" CssClass="button-accent"/>
	&nbsp;&nbsp;&nbsp;
    <bvu:Help ID="help111" runat="server" HelpID="111"/>
    <asp:Button ID="btnSubmitOrder" runat="server" Text="Submit Order" OnClick="SubmitPO_Click" CssClass="button-primary"/>
  </div>
</div>
<div class="subTabContent-container">
<div class="content">
<div class="section invoice" style="width:100%">
  <div class="flex-row align-start">
  <table style="width:100%; white-space:nowrap" >
  <tr>
    <td style="width:20%;vertical-align:top">
        <div class="section">
        <h2>
            Custom PO Number&nbsp;<span style='<%=!IsCustomBrace ? "" : "display: none" %>'><bvu:Help ID="help130" runat="server" HelpID="130" /></span>
        </h2>
        <span class="FieldLabel" style='<%=!IsCustomBrace ? "" : "display: none" %>'>(Enter if applicable; 50 chars. max)</span>
        <asp:TextBox ID="txtCustomPO" runat="server" ReadOnly='<%#IsCustomBrace %>' TextMode="SingleLine" MaxLength="50" CssClass="RadInput_Breg"></asp:TextBox>
        </div>
        <div class="section">
        <h2>
            Shipping Type&nbsp;<bvu:Help ID="help110" runat="server" HelpID="110"/>
        </h2>
        <asp:Label ID="lblShippingType" runat="server"></asp:Label>
        </div>
	</td>
    <td style='vertical-align:top;width:28%;<%=IsCustomBrace ? "" : "display: none" %>'>
        <h2>
            Patient&nbsp;Information
        </h2>
      <table class="RadGrid_Breg flex-grow">
        <tr>
          <td>
            <strong>Name</strong>
			&nbsp;&nbsp;<asp:Label ForeColor="Red" ID="ErrorName" runat="server"></asp:Label>
          </td>
          <td style="width:100%">
            <asp:Label ID="PatientName" runat="server"></asp:Label>
          </td>
        </tr>
        <tr>
          <td>
            <strong>Age</strong>
			&nbsp;&nbsp;<asp:Label ForeColor="Red" ID="ErrorAge" runat="server"></asp:Label>
          </td>
          <td>
            <asp:Label ID="PatientAge" runat="server"></asp:Label>
          </td>
        </tr>
        <tr>
          <td>
            <strong>Height</strong>
			&nbsp;&nbsp;<asp:Label ForeColor="Red" ID="ErrorHeight" runat="server"></asp:Label>
          </td>
          <td>
            <asp:Label ID="PatientHeight" runat="server"></asp:Label>
          </td>
        </tr>
        <tr>
          <td>
            <strong>Weight</strong>
			&nbsp;&nbsp;<asp:Label ForeColor="Red" ID="ErrorWeight" runat="server"></asp:Label>
          </td>
          <td>
            <asp:Label ID="PatientWeight" runat="server"></asp:Label>
          </td>
        </tr>
      </table>
    </td>
    <td style="width:28%">
      <h2>
        Shipping Address&nbsp;<bvu:Help ID="help120" runat="server" HelpID="120"/>
      </h2>
      <table class="RadGrid_Breg flex-grow">
        <tr>
          <td>
            <strong>Attention</strong>
          </td>
          <td style="width:100%">
            <asp:Label ID="ShipAttention" runat="server"></asp:Label>
          </td>
        </tr>
        <tr>
          <td>
            <strong>Address</strong>
          </td>
          <td>
            <asp:Label ID="ShipAddress1" runat="server"></asp:Label><br/>
            <asp:Label ID="ShipAddress2" runat="server"></asp:Label>
          </td>
        </tr>
        <tr>
          <td>
            <strong>City</strong>
          </td>
          <td>
            <asp:Label ID="ShipCity" runat="server"></asp:Label>
          </td>
        </tr>
        <tr>
          <td>
            <strong>State</strong>
          </td>
          <td>
            <asp:Label ID="ShipState" runat="server"></asp:Label>
          </td>
        </tr>
        <tr>
          <td>
            <strong>Zip Code</strong>
          </td>
          <td>
            <asp:Label ID="ShipZipCode" runat="server"></asp:Label>
          </td>
        </tr>
      </table>
    </td>
    <td style="width:28%">
      <h2>
        Billing Address&nbsp;<bvu:Help ID="help108" runat="server" HelpID="108"/>
      </h2>
      <table class="RadGrid_Breg flex-grow">
        <tr>
          <td>
            <strong>Attention</strong>
          </td>
          <td style="width:100%">
            <asp:Label ID="BillAttention" runat="server"></asp:Label>
          </td>
        </tr>
        <tr>
          <td>
            <strong>Address</strong>
          </td>
          <td>
            <asp:Label ID="BillAddress1" runat="server"></asp:Label><br/>
            <asp:Label ID="BillAddress2" runat="server"></asp:Label>
          </td>
        </tr>
        <tr>
          <td>
            <strong>City</strong>
          </td>
          <td>
            <asp:Label ID="BillCity" runat="server"></asp:Label>
          </td>
        </tr>
        <tr>
          <td>
            <strong>State</strong>
          </td>
          <td>
            <asp:Label ID="BillState" runat="server"></asp:Label>
          </td>
        </tr>
        <tr>
          <td>
            <strong>Zip Code</strong>
          </td>
          <td>
            <asp:Label ID="BillZipCode" runat="server"></asp:Label>
          </td>
        </tr>

      </table>
    </td>
	<td></td>
  </tr>
  </table>
  </div>
</div>

<radG:RadGrid ID="grdInvoiceSnapshot" runat="server" GridLines="None"
              OnNeedDataSource="grdInvoiceSnapshot_NeedDataSource"
              OnItemDataBound="grdInvoiceSnapshot_ItemDataBound"
              EnableAJAX="True" EnableAJAXLoadingTemplate="True"
              LoadingTemplateTransparency="10" AllowPaging="True" AllowSorting="True" ShowGroupPanel="True" Skin="Breg"
              AllowMultiRowSelection="True" EnableViewState="True" EnableEmbeddedSkins="False"
              OnPreRender="grdInvoiceSnapshot_PreRender" CssClass="GridTotals">

  <MasterTableView DataKeyNames="PracticeCatalogSupplierBrandID" AutoGenerateColumns="False" EnableTheming="false" ShowFooter="true">

    <ExpandCollapseColumn CollapseImageUrl="/App_Themes/Breg/images/Common/collapse.png" ExpandImageUrl="/App_Themes/Breg/images/Common/expand.png" Visible="true">
      <HeaderStyle Width="19px"/>
    </ExpandCollapseColumn>

    <DetailTables>
      <radG:GridTableView runat="server" AutoGenerateColumns="False" Width="100%" ShowFooter="true">
        <ParentTableRelation>
          <radG:GridRelationFields DetailKeyField="PracticeCatalogSupplierBrandID" MasterKeyField="PracticeCatalogSupplierBrandID"/>
        </ParentTableRelation>
        <ExpandCollapseColumn Visible="False">
          <HeaderStyle Width="19px"/>
        </ExpandCollapseColumn>
        <RowIndicatorColumn Visible="False">
          <HeaderStyle Width="20px"/>
        </RowIndicatorColumn>
        <Columns>
          <radG:GridBoundColumn DataField="PracticeCatalogProductID" UniqueName="PracticeCatalogProductID" Visible="False">
          </radG:GridBoundColumn>
          <radG:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" UniqueName="PracticeCatalogSupplierBrandID" Visible="False">
          </radG:GridBoundColumn>
          <radG:GridBoundColumn DataField="BrandShortName" HeaderText="Brand" UniqueName="BrandShortName">
          </radG:GridBoundColumn>
          <radG:GridBoundColumn DataField="ShortName" HeaderText="Product" UniqueName="ShortName">
          </radG:GridBoundColumn>
          <radG:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code">
          </radG:GridBoundColumn>
          <radG:GridBoundColumn DataField="Packaging" HeaderText="Pkg." UniqueName="Packaging">
          </radG:GridBoundColumn>
          <radG:GridBoundColumn DataField="LeftRightSide" HeaderText="Side" UniqueName="LeftRightSide">
          </radG:GridBoundColumn>
          <radG:GridBoundColumn DataField="Size" HeaderText="Size" UniqueName="Size">
          </radG:GridBoundColumn>
          <radG:GridBoundColumn DataField="Color" HeaderText="Color" UniqueName="Color">
          </radG:GridBoundColumn>
          <radG:GridBoundColumn DataField="Gender" HeaderText="Gender" UniqueName="Gender">
          </radG:GridBoundColumn>
          <radG:GridBoundColumn DataField="ActualWholesaleCost" DataType="System.Decimal" DataFormatString="{0:$#,#.00}" HeaderText="AW Cost" UniqueName="ActualWholesaleCost">
          </radG:GridBoundColumn>
          <radG:GridBoundColumn DataField="Quantity" HeaderText="Qty." UniqueName="Quantity">
          </radG:GridBoundColumn>
          <radG:GridTemplateColumn HeaderText="Line Total" UniqueName="LineTotal">
            <ItemTemplate>
              <asp:Label ID="lblLineTotal" Text='<%# Eval("TotalCost", "{0:$#,#.00}") %>' runat="server"></asp:Label>
            </ItemTemplate>
          </radG:GridTemplateColumn>
        </Columns>
      </radG:GridTableView>
    </DetailTables>
    <RowIndicatorColumn Visible="False">
      <HeaderStyle Width="20px"/>
    </RowIndicatorColumn>
    <Columns>
      <radG:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" UniqueName="PracticeCatalogSupplierBrandID" Visible="False">
      </radG:GridBoundColumn>
      <radG:GridTemplateColumn HeaderText="Brand" UniqueName="BrandShortNameParent">
        <ItemTemplate>
          <strong>
            <asp:Label ID="lblBrandItem" Text='<%# Eval("BrandShortName") %>' runat="server"></asp:Label>
          </strong>
        </ItemTemplate>
        <FooterTemplate>
          <strong>
            <asp:Label ID="lblGrandTotalLabel" Text="GRAND TOTAL" runat="server"/>
          </strong>
        </FooterTemplate>
      </radG:GridTemplateColumn>
      <radG:GridTemplateColumn HeaderText="Subtotal" UniqueName="TotalCost">
        <ItemTemplate>
          <asp:Label ID="lblTotalCost2" Text='<%# Eval("TotalCost", "{0:$#,#.00}") %>' runat="server"></asp:Label>
        </ItemTemplate>
        <FooterTemplate>
          <asp:Label ID="lblGrandTotal" runat="server"/>
        </FooterTemplate>
      </radG:GridTemplateColumn>
    </Columns>
  </MasterTableView>
  <ClientSettings AllowColumnsReorder="false" ReorderColumnsOnClient="false">
    <Selecting AllowRowSelect="True"/>
  </ClientSettings>
</radG:RadGrid>

	  <div style='overflow-x: hidden; overflow-y: hidden;<%=IsCustomBrace ? "" : "display: none" %>'>
		<bvu:CustomBrace ID="CustomBrace" runat="server" />
	  </div>

</div>
</div>
</asp:Content>
