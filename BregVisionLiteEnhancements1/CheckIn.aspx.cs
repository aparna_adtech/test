using System;
using System.Web.UI.WebControls;
using BregVision.UserControls.CheckInPage;

namespace BregVision
{
  public partial class CheckIn : Bases.PageBase
  {
    #region public properties
    public UserControls.General.RecordsPerPage ctlRecordsPerPageUserControl
    {
      get { return ctlRecordsPerPage; }
    }

    public Telerik.Web.UI.RadGrid grdCustomBraceOutstandingRadGrid
    {
      get { return ctlCheckInPoOutstandingControl.grdPOOutstandingRadGrid; }
    }

    public Telerik.Web.UI.RadAjaxLoadingPanel RadAjaxLoadingPanel1Panel
    {
      get { return RadAjaxLoadingPanel1; }
    }

    public Telerik.Web.UI.RadTabStrip rtsCheckInRadTabStrip
    {
      get { return rtsCheckIn; }
    }

    public CheckInPoOutstanding ctlCheckInPoOutstandingControl
    {
      get { return ctlCheckInPoOutstanding; }
    }

    public SearchPOs ctlSearchPOsSearchPOs
    {
      get { return ctlSearchPOs; }
    }
    #endregion

    #region page events
    protected void Page_Init(object sender, EventArgs e)
    {
      Master.LocationChanged += new CommandEventHandler(LocationChangedFromMasterPage);

      if (BLL.Website.IsVisionExpress())
      {
        rtsCheckIn.Tabs[TabSearchPO.Index].Visible = false;
      }

      if (!IsPostBack)
      {
        int selectedStartupTab = 0;
        rtsCheckIn.SelectedIndex = selectedStartupTab;
        rmpCheckIn.SelectedIndex = selectedStartupTab;
      }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      InitializeAjaxSettings();
      Master.SelectCurrentTab("CheckIn");
    }
    #endregion

    #region helpder methods
    private void InitializeAjaxSettings()
    {
      var radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);

      radAjaxManager.AjaxSettings.AddAjaxSetting(rtsCheckIn, ctlCheckInPoOutstanding.grdPOOutstandingRadGrid);
      radAjaxManager.AjaxSettings.AddAjaxSetting(rtsCheckIn, ctlSearchPOs.grdSearchRadGrid);
      radAjaxManager.AjaxSettings.AddAjaxSetting(rtsCheckIn, ctlPOManualCheckIn.grdManualCheckInRadGrid);
      radAjaxManager.AjaxSettings.AddAjaxSetting(rtsCheckIn, ctlCheckInCustomBrace.grdCustomBraceOutstandingRadGrid);
    }
    #endregion

    #region location changed event handler
    private void LocationChangedFromMasterPage(object sender, CommandEventArgs e)
    {
      Response.Redirect("CheckIn.aspx", false);
    }
    #endregion
  }
}
