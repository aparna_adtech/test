			var currentRow, movedRow = null;
			var isMouseOver = 0;
			var radGridOver = "";
			var isDragDropEnabled = false;

			function drover(dr)
			{
				radGridOver = "DeleteRow";
				dr.style.border = "solid 2px #666";
				dr.style.padding = "1px";
			};
			
			function drout(dr)
			{
				radGridOver = "";
				dr.style.border = "solid 1px #666";
				dr.style.padding = "2px";
			};
			function enabledragdrop()
			{
				//this is to prevent errors when you start dragging items 
				//before the page is completely loaded
				isDragDropEnabled = true;
			};
			function RowCreated(row)
			{
				var mouseDownHandler = function(e)
				{
					if (!isDragDropEnabled)
						return;
					if (!e)
						var e = window.event;
					if (!currentRow)
					{
						currentRow = document.createElement("DIV");
						currentRow.innerHTML = "<table width='100%'><tbody><tr>"+row.Control.innerHTML+"</tr></tbody><table>";
						document.body.appendChild(currentRow);
						currentRow.style.position = "absolute";
						currentRow.style.display = "none";
						//alert('Hi');
						movedRow = row;
					}
					
					ClearDocumentEvents();				
				};
				var mouseUpHandler = function(e)
				{
					if (!e)
						{var e = window.event;
						alert('E');
						}
					if (currentRow)
					{
						if (movedRow && currentRow.style.display != "none")
						{
						    //alert('F');
							movedRow.Owner.SelectRow(movedRow.Control, true);
							var PostBackString = new String();
							PostBackString = "__doPostBack('RowMoved','Args')";
							PostBackString = PostBackString.replace("Args",movedRow.Owner.Owner.ID+","+movedRow.Index+","+radGridOver); 							
							//alert('G');
							alert(PostBackString);
							if ("" != radGridOver && radGridOver != movedRow.Owner.Owner.ID)
								alert(PostBackString);
								eval(PostBackString );
								alert('H');
						}
						document.body.removeChild(currentRow);
						currentRow = null;
						movedRow = null;
					}
					RestoreDocumentEvents();
				};
				var mouseMoveHandler = function(e)
				{
					if (!e)
						var e = window.event;
					if (currentRow)
					{
						currentRow.style.display = "block";
						currentRow.style.backgroundImage = "url(Img/rowBg.gif)";
						currentRow.style.height = "24px";
						currentRow.style.width = "48%";
						currentRow.style.filter = "progid:DXImageTransform.Microsoft.Alpha(style=1,opacity=80,finishOpacity=100,startX=100,finishX=100,startY=100,finishY=100)";
						currentRow.style.MozOpacity = ".80";
						currentRow.style.top =  e.clientY + 
												document.documentElement.scrollTop + 
												document.body.scrollTop + 3 + "px";

						currentRow.style.left = e.clientX + 
												document.documentElement.scrollLeft + 
												document.body.scrollLeft + 3 + "px";
					}
					
				};
				var mouseOverHandler = function(e)
				{
					if (!e)
						var e = window.event;
					var parentEl;
					if(e.srcElement) //IE
					{
						//alert('IE');
						parentEl = e.srcElement;
						//alert(parentEl);
						while (parentEl && parentEl.className != "RadGrid" && parentEl.id != "DeleteRow")
						{						
							parentEl = parentEl.parentElement;						
						}									
					}
					else //gecko browsers
					{
						alert('Gecko');
						parentEl = e.target;
						alert(parentEl);
						while (parentEl && parentEl.className != "RadGrid" && parentEl.id != "DeleteRow")
						{						
							parentEl = parentEl.parentNode;						
						}		
					}								
										
					if (parentEl)
					    {
					    //alert('Setting RADGRIDOVER');
						radGridOver = parentEl.id;
						//alert(radGridOver);
						}
										
					if ("" != radGridOver)
						{
							//document.getElementById(radGridOver+"_head").style.color = "gray";
						}

					isMouseOver++;
				};
				var mouseOutHandher = function(e)
				{
					if (!e)
						var e = window.event;
					isMouseOver--;
					//alert('R');
					if (0 == isMouseOver)
					{
						//alert('S');
						if ("" != radGridOver)
							{
								//document.getElementById(radGridOver+"_head").style.color= "black";
								//alert('T');
							}
						radGridOver = "";
					}
				};
				if (row.ItemType == "Item" || row.ItemType == "AlternatingItem")
				{
					row.Control.style.cursor = "move";
					//alert('U');
					if (row.Control.attachEvent)
					{
						row.Control.attachEvent("onmousedown", mouseDownHandler);
						document.body.attachEvent("onmousemove", mouseMoveHandler);
						//alert('V');
					}
					if (row.Control.addEventListener)
					{
						row.Control.addEventListener("mousedown", mouseDownHandler, false);
						document.body.addEventListener("mousemove", mouseMoveHandler, false);
						alert('W');
					}
				}
				if (row.Control.attachEvent)
				{
				//alert('Y');
					document.body.attachEvent("onmouseup", mouseUpHandler);
					row.Owner.Owner.Control.attachEvent("onmouseover", mouseOverHandler);
					row.Owner.Owner.Control.attachEvent("onmouseout", mouseOutHandler);
				}
				if (row.Control.addEventListener)
				
				{
				alert('Z');
					document.body.addEventListener("mouseup", mouseUpHandler, false);
					row.Owner.Owner.Control.addEventListener("mouseover", mouseOverHandler, false);
					row.Owner.Owner.Control.addEventListener("mouseoout", mouseOutHandler, false);
				}
			};
			function ClearDocumentEvents()
			{
				if (document.onmousedown != this.mouseDownHandler)
				{
					this.documentOnMouseDown = document.onmousedown;
				}

				if (document.onselectstart != this.selectStartHandler)
				{
					this.documentOnSelectStart = document.onselectstart;
				}

				this.mouseDownHandler = function(e){return false;};
				this.selectStartHandler = function(){return false;};

				document.onmousedown = this.mouseDownHandler;
				document.onselectstart = this.selectStartHandler;

			};
			function RestoreDocumentEvents()
			{
				if ((typeof(this.documentOnMouseDown) == "function") &&
					(document.onmousedown != this.mouseDownHandler))
				{
					document.onmousedown = this.documentOnMouseDown;
				}
				else
				{
					document.onmousedown = "";
				}
				
				if ((typeof(this.documentOnSelectStart) == "function") &&
					(document.onselectstart != this.selectStartHandler))
				{
					document.onselectstart = this.documentOnSelectStart;
				}
				else
				{
					document.onselectstart = "";
				}
			};