using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace BregVision
{
    public partial class OrderRequest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // hide location selection dropdown because it's not valid here
            if (!Page.IsPostBack)
            {
                var m = this.Master as BregVision.MasterPages.SiteMaster;
                if (m != null)
                    m.SetLocationDropdownVisibility(false);
            }
        }
    }
}
