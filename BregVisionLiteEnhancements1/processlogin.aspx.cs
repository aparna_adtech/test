using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace BregVision
{


    public partial class processlogin : System.Web.UI.Page
    {

		const string BregAdminHomePage = "~/BregAdmin/BregAdmin.aspx";
		const string PracticeAdminHomePage = "~/Admin/Practice_Admin.aspx";
        const string BregConsignmentHomePage = "~/BregAdmin/ConsignmentPractices.aspx";

        const string defaultPage = "~/default.aspx";

		
		
		protected void Page_Load(object sender, EventArgs e)
        {

            string redirectUrl = "";
            //clear all session variables
            if (Request.QueryString["action"] == "logout" || User.Identity.IsAuthenticated == false)
            {
                LogOut();
                redirectUrl = "~/default.aspx";
            }
            else
            {
                if (Roles.IsUserInRole(User.Identity.Name, "BregConsignment")) //This is a breg admin
                {
                    RemoveSessionVariables();
                    redirectUrl = BregConsignmentHomePage;
                } 
                else if (Roles.IsUserInRole(User.Identity.Name, "BregAdmin")) //This is a breg admin
                {
                    RemoveSessionVariables();
                    redirectUrl = BregAdminHomePage;
                }                                                           //This is a practice admin or practice user
                else if (Roles.IsUserInRole(User.Identity.Name, "PracticeAdmin") || Roles.IsUserInRole(User.Identity.Name, "PracticeLocationUser"))
                {
                    RemoveSessionVariables();

                    DAL.Practice.Practice.SetPracticeSession();

                    //if the site doesn't match the type of user, send them to the other site
                    if (User.IsInRole("VisionLite") != BLL.Website.IsVisionExpress())
                    {
                        //in prod, we know they should either go to www.bregvision.com or www.bregvisionexpress.com.  but in dev we can't be sure where to send them

                        redirectUrl = BLL.Website.GetOppositeSiteLoginUrl();
                    }  //if they're not a vision express user OR Setup is Complete

                    else if ((!BLL.Practice.User.IsVisionLiteUser()) || BLL.Practice.Practice.IsSetupComplete(Session.GetInt32("PracticeID")) == true)  // if the user hasn't completed the wizard, send them there
                    {
                        //if they're an admin logging in to approve an order
                        if (Session["OrderApprovalQueryString"] != null && Roles.IsUserInRole(User.Identity.Name, "PracticeAdmin")) //this is if they are logging in to approve an order
                        {
                            redirectUrl = "~/orderapproval.aspx";
                        }
                        else //if they're not approving an order
                        {
                            redirectUrl = "~/home.aspx";
                        }
                    }
                    else
                    {   //they are a vision lite user and setup is not complete

                        redirectUrl = string.Format("~/Admin/PracticeSetup/PracticeSetup.aspx?n={0}", Session.GetInt32("PracticeID"));
                    }

                }

                // log login activity to database
                try
                {
                    using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["BregVisionConnectionString"].ConnectionString))
                    {
                        conn.Open();
                        using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = "INSERT INTO LoginLog (Username, LoginDateTime, ClientIPAddress) VALUES (@Username, @LoginDateTime, @ClientIPAddress)";
                            cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("Username", User.Identity.Name));
                            cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("LoginDateTime", DateTime.Now));
                            cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("ClientIPAddress", Request.UserHostAddress));

                            cmd.ExecuteNonQuery();
                        }
                        conn.Close();
                    }
                }
                catch
                {
                    // silently fail for now
                }
            }

            //do the redirect here
            Response.Redirect(redirectUrl, false);
        }



        private void RemoveSessionVariables()
        {
            //This is done so that the Master page will reinitialize this variables when loading.
            Session.Remove("PracticeID");
            Session.Remove("PracticeLocationID");
            Session.Remove("PracticeName");
            Session.Remove("PracticeLocationName");
            Session.Remove("UserID");
        }

        private void LogOut()
        {
            Session.Abandon();
            Session.Clear();
            FormsAuthentication.SignOut();
            
        }
    }
}
