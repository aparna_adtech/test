using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web.Security;
using BLL.PracticeLocation;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.WebControls;
using System.Web.UI.WebControls;

namespace BregVision
{
    public partial class cart : Bases.PageBase
    {
        
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!BLL.Website.IsVisionExpress())
            {
                PopulateTransferLocation();
            }
            else
            {
                pnlTransfer.Visible = false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // subscribe to page level location changed
            {
                var m = this.Master as BregVision.MasterPages.SiteMaster;
                if (m != null)
                    m.LocationChanged += new System.Web.UI.WebControls.CommandEventHandler(LocationChangedEventHandler);
            }

            if (!IsPostBack)
            {
                // initial load of cart contents
                RefreshCart();
            }

            // check for consignment items in the cart on each page load vs. only on !IsPostBack (in case cart items are deleted, etc.)
            using (var visionDB = ClassLibrary.DAL.VisionDataContext.GetVisionDataContext())
            {
                var cart_has_consignment =
                    visionDB
                    .usp_GetShoppingCart(practiceLocationID)
                    .Any(c => c.IsConsignment);

                if (cart_has_consignment)
                {
                    btnDeleteAll.Enabled = false;
                    btnTransfer.Enabled = false;
                    lblStatus.Text = @"This is a consignment order.  DeleteAll and Transfer have been disabled for this order.<br/>
Individual consignment items cannot be deleted and quantity on consignment items cannot be reduced below minimum re-order levels.";
                }
            }
        }

        public void chkAllLogoParts_OnCheckChanged(object sender, EventArgs e)
        {
            var chkAllLogoParts = sender as CheckBox;
            if (chkAllLogoParts == null)
                return;

            foreach (var gridDataItem in grdShoppingCart.MasterTableView.Items.Cast<GridDataItem>())
            {
                var chkIsLogoPart = gridDataItem.FindControl("chkIsLogoPart") as CheckBox;
                if (chkIsLogoPart != null)
                    chkIsLogoPart.Checked = (chkIsLogoPart.Enabled && chkAllLogoParts.Checked);
            }
        }

        private void LocationChangedEventHandler(object sender, System.Web.UI.WebControls.CommandEventArgs e)
        {
            // reload the cart when the location changes
            RefreshCart();
        }

        private void RefreshCart()
        {
            grdShoppingCart.DataSource = ShoppingCart.GetShoppingCart(Session.GetInt32("PracticeLocationID"));
            grdShoppingCart.DataBind();

            if (grdShoppingCart.Items.Count == 0)
            {
                Purchase.Enabled = false;
                btnUpdateQuantity.Enabled = false;
            }
            else
            {
                Purchase.Enabled = true;
                btnUpdateQuantity.Enabled = true;
            }
        }

        protected void grdShoppingCart_PageIndexChanged(object source, Telerik.WebControls.GridPageChangedEventArgs e)
        {
            grdShoppingCart.CurrentPageIndex = e.NewPageIndex;

            bool isCustomBrace = false;
            LoopHierarchyRecursive(grdShoppingCart.MasterTableView, out isCustomBrace);
        }

        protected void grdShoppingCart_NeedDataSource(object source, Telerik.WebControls.GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable)
                grdShoppingCart.DataSource = ShoppingCart.GetShoppingCart(Session.GetInt32("PracticeLocationID"));
        }
        
        protected void grdShoppingCart_ColumnCreated(object sender, Telerik.WebControls.GridColumnCreatedEventArgs e)
        {
            var column = e.Column as GridBoundColumn;
            if (column != null)
                column.DataFormatString = "{0:c}";
        }

        private void deleteItemFromShoppingCart(Int32 shoppingCartItemID)
        {
            // do not allow delete if cart item is consignment or the product itself is consignment
            using (var visionDB = ClassLibrary.DAL.VisionDataContext.GetVisionDataContext())
            {
                var cart = (from c in visionDB.usp_GetShoppingCart(practiceLocationID)
                            where c.ShoppingCartItemID == shoppingCartItemID
                            select new { c.PracticeCatalogProductID, c.IsConsignment }).FirstOrDefault();

                var product = (from pi in visionDB.ProductInventories
                               where pi.PracticeLocationID == practiceLocationID
                               && pi.PracticeCatalogProductID == cart.PracticeCatalogProductID
                               && pi.IsConsignment == true
                               select new { pi.IsConsignment }).FirstOrDefault();

                if (cart != null && product != null && cart.IsConsignment == true && product.IsConsignment == true)
                {
                    //can't remove this item from the cart
                    lblStatus.Text = "This is a consignment item and a consignment order.  It cannot be removed from the cart";
                    lblStatus.CssClass = "ErrorMsg";
                    return;
                }
            }

            DAL.PracticeLocation.Inventory.DeleteItemFromShoppingCart(shoppingCartItemID);
        }


        #region Parameters
         //@PracticeCatalogProductID INT,
         //@ShoppingCartItemID int,
         //@PracticeLocationID_TransferFrom int,
         //@PracticeLocationID_TransferTo int,
         //@UserID INT,
         //@Quantity int,
         //@ActualWholesaleCost smallmoney,
         //@Comments varchar(200)
        #endregion
        private void TransferItemFromShoppingCart(  Int32 PracticeID,
                                                    Int32 PracticeCatalogProductID,
                                                    Int32 shoppingCartItemID,
                                                    Int32 PracticeLocationID_TransferFrom,
                                                    Int32 PracticeLocationID_TransferTo,
                                                    Int32 UserID,
                                                    Int32 Quantity,
                                                    Decimal ActualWholesaleCost,
                                                    string Comments)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_Transfer_InsertUpdate");

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, PracticeID);
            db.AddInParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, PracticeCatalogProductID);
            db.AddInParameter(dbCommand, "ShoppingCartItemID", DbType.Int32, shoppingCartItemID);
            db.AddInParameter(dbCommand, "PracticeLocationID_TransferFrom", DbType.Int32, PracticeLocationID_TransferFrom);
            db.AddInParameter(dbCommand, "PracticeLocationID_TransferTo", DbType.Int32, PracticeLocationID_TransferTo);
            db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
            db.AddInParameter(dbCommand, "Quantity", DbType.Int32, Quantity);
            db.AddInParameter(dbCommand, "ActualWholesaleCost", DbType.Decimal, ActualWholesaleCost);
            db.AddInParameter(dbCommand, "Comments", DbType.String, Comments);

            int SQLReturn = db.ExecuteNonQuery(dbCommand);

        }

        protected void grdShoppingCart_DeleteCommand(object sender, Telerik.WebControls.GridCommandEventArgs e)
        {
            //delete item
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = e.Item as GridDataItem;
                Int32 shoppingCartItemID = Int32.Parse(dataItem["ShoppingCartItemID"].Text);
                //Next line calls function to delete item from cart
                deleteItemFromShoppingCart(shoppingCartItemID);
                grdShoppingCart.DataSource = ShoppingCart.GetShoppingCart(Session.GetInt32("PracticeLocationID")); //GetShoppingCart(Convert.ToInt16(Session["PracticeLocationID"]));
                grdShoppingCart.Rebind();
                if (grdShoppingCart.Items.Count == 0)
                {
                    Purchase.Enabled = false;
                }
                else
                {
                    Purchase.Enabled = true;
                }
            }
        }

        protected void grdShoppingCart_ItemDataBound(object sender, Telerik.WebControls.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = e.Item as GridDataItem;
               
                bool isCustomBrace = Convert.ToBoolean(dataItem["IsCustomBrace"].Text);
                if (isCustomBrace)
                {
                    var txtQuantity = dataItem.FindControl("txtQuantity") as Telerik.Web.UI.RadNumericTextBox;
                    txtQuantity.Enabled = false;

                    btnUpdateQuantity.Enabled = false;
                }

                DataRowView drv = e.Item.DataItem as DataRowView;
                if (drv != null)
                {
                    bool isLogoPart = Convert.ToInt16(drv["IsLogoPart"]) == 1 ? true : false;
                    
                    CheckBox chkIsLogoPart = dataItem.FindControl("chkIsLogoPart") as CheckBox;
                    if (chkIsLogoPart != null)
                    {
                        bool mcpIsLogoAblePart = Convert.ToBoolean(drv["McpIsLogoAblePart"]);
                        chkIsLogoPart.Enabled = mcpIsLogoAblePart;
                        chkIsLogoPart.Checked = mcpIsLogoAblePart ? isLogoPart : false;
                    }                   
                }
            }
        }
        //decimal total;
        //int quantity;
        //decimal totalCost;
        //private void grdShoppingCart_ItemDataBound(object sender, Telerik.WebControls.GridItemEventArgs e)
        //{
        //    if (e.Item is GridDataItem)
        //    {
        //        GridDataItem dataItem = e.Item as GridDataItem;
        //        decimal fieldValue = System.Convert.ToDecimal(dataItem["UnitCost"].Text);
        //        int fieldValue2 = int.Parse(dataItem["QuantityHidden"].Text);
        //        decimal totalCostItem = fieldValue * System.Convert.ToDecimal(fieldValue2);
        //        dataItem["TotalCost"].Text = totalCostItem.ToString();
                
        //        //Int16 fieldValue = 1;
        //        total += fieldValue;
        //        quantity += fieldValue2;
        //        totalCost += totalCostItem;
        //        //Response.Write(dataItem["QuantityHidden"].Text);

        //        bool isCustomBrace = bool.Parse(dataItem["IsCustomBrace"].Text);
        //    }
        //    if (e.Item is GridFooterItem)
        //    {
        //        GridFooterItem footerItem = e.Item as GridFooterItem;
        //        //footerItem["UnitCost"].Text = "Total: " + total.ToString();
        //        footerItem["Quantity"].Text = "# Items in Cart: " + quantity.ToString();
        //        footerItem["totalCost"].Text = "Sub Total: " + totalCost.ToString();

        //        //GridBoundColumn column = (GridBoundColumn)grdShoppingCart.MasterTableView.GetColumn("UnitCost");
        //        //column.DataFormatString = "{0:c}"; 
        //    }
        //}

        
        protected void btnTransfer_Click(object sender, EventArgs e)
        {
            TransferAllShoppingCartItems(grdShoppingCart.MasterTableView);
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            DeleteAllShoppingCartItems(grdShoppingCart.MasterTableView);
        }

        protected void Purchase_Click(object sender, EventArgs e)
        {
            bool isCustomBrace = false;
            int grandTotalQuantity = LoopHierarchyRecursive(grdShoppingCart.MasterTableView, out isCustomBrace);

            //if (!(BLL.Website.IsVisionExpress() && isCustomBrace)) //mws. unsure of this requirement.  We want to sell custom braces
            //{

                Session["IsCustomBrace"] = isCustomBrace;
                // Do not redirect if all of the quantities are zero.

                if (grandTotalQuantity > 0)
                {
                    if (isCustomBrace == true)
                    {
                        Response.Redirect("POShipBillCustomBrace.aspx", false);
                    }
                    else
                    {
                        Response.Redirect("POShipBill.aspx", false);
                    }
                    return;
                }
                
            //}
        
            // Display message
            // There are zero products in the shopping cart.  
            grdShoppingCart.DataSource = ShoppingCart.GetShoppingCart(Session.GetInt32("PracticeLocationID")); //GetShoppingCart(Convert.ToInt16(Session["PracticeLocationID"]));
            grdShoppingCart.DataBind();
            Purchase.Enabled = false;
            lblStatus.Text = "VisionLite users cannot purchase custom braces";
            lblStatus.CssClass = "ErrorMsg";


        }

        private void TransferAllShoppingCartItems(GridTableView gridTableView)
        {
            Guid transferID = Guid.NewGuid(); // TEMP
            int practiceID = Convert.ToInt16(Session["PracticeID"]);
            int fromPracticeLocationID = Convert.ToInt16(Session["PracticeLocationID"]);
            int toPracticeLocationID = Convert.ToInt32(cbxTransferToLocation.SelectedValue);
            int userID = 1;
            int counter = 0;
            int grandTotalQuantity = 0;
            int totalQuantity = 0;

            lblStatus.Text = "";

            foreach (GridDataItem gridDataItem in gridTableView.Items)
            {
                totalQuantity = UpdateCart(findShoppingCartItemID(gridDataItem), findQuantity(gridDataItem));

                try
                {
                    TransferItemFromShoppingCart(practiceID,
                                                    findPracticeCatalogProductID(gridDataItem),
                                                    findShoppingCartItemID(gridDataItem),
                                                    fromPracticeLocationID,
                                                    toPracticeLocationID,
                                                    userID,
                                                    findQuantity(gridDataItem),
                                                    findActualWholesaleCost(gridDataItem),
                                                    "");
                    counter += 1;
                    grandTotalQuantity += totalQuantity;

                    // TEMP logging to track issues with shopping cart transfers
                    {
                        try
                        {
                            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["BregVisionConnectionString"].ConnectionString))
                            {
                                conn.Open();
                                using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
                                {
                                    cmd.CommandText = "INSERT INTO TMPcart_transferlog (transferID, practiceID, fromPracticeLocationID, toPracticeLocationID, userID, counter, grandTotalQuantity, totalQuantity, practiceCatalogProductID, shoppingCartItemID, quantity, actualWholesaleCost) VALUES (@transferID, @practiceID, @fromPracticeLocationID, @toPracticeLocationID, @userID, @counter, @grandTotalQuantity, @totalQuantity, @practiceCatalogProductID, @shoppingCartItemID, @quantity, @actualWholesaleCost)";
                                    cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("transferID", transferID));
                                    cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("practiceID", practiceID));
                                    cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("fromPracticeLocationID", fromPracticeLocationID));
                                    cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("toPracticeLocationID", toPracticeLocationID));
                                    cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("userID", userID));
                                    cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("counter", counter));
                                    cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("grandTotalQuantity", grandTotalQuantity));
                                    cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("totalQuantity", totalQuantity));
                                    cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("practiceCatalogProductID", findPracticeCatalogProductID(gridDataItem)));
                                    cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("shoppingCartItemID", findShoppingCartItemID(gridDataItem)));
                                    cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("quantity", findQuantity(gridDataItem)));
                                    cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("actualWholesaleCost", findActualWholesaleCost(gridDataItem)));

                                    cmd.ExecuteNonQuery();
                                }
                                conn.Close();
                            }
                        }
                        catch { /* do nothing for now */ }
                    }
                }
                catch
                {
                    lblStatus.Text = "Error transferring Items.  ";
                    lblStatus.Text = "ErrorMsg";
                }
            }
            grdShoppingCart.DataSource = ShoppingCart.GetShoppingCart(Session.GetInt32("PracticeLocationID")); //GetShoppingCart(Convert.ToInt16(Session["PracticeLocationID"]));
            grdShoppingCart.Rebind();
            if (grdShoppingCart.Items.Count == 0)
            {
                Purchase.Enabled = false;
            }
            else
            {
                Purchase.Enabled = true;
            }
            lblStatus.Text += string.Format("{0} products have been transferred", grandTotalQuantity.ToString());
            lblStatus.CssClass = "SuccessMsg";
        }

        private void DeleteAllShoppingCartItems(GridTableView gridTableView)
        {
            int grandTotalQuantity = 0;
            var totalGridView = ShoppingCart.GetShoppingCart(Session.GetInt32("PracticeLocationID"));
            foreach (DataRow gridDataItem in totalGridView.Tables[0].Rows)
            {
                grandTotalQuantity += findQuantity(gridDataItem);

                deleteItemFromShoppingCart(findShoppingCartItemID(gridDataItem));
                grdShoppingCart.DataSource = ShoppingCart.GetShoppingCart(Session.GetInt32("PracticeLocationID")); //GetShoppingCart(Convert.ToInt16(Session["PracticeLocationID"]));
            }
            grdShoppingCart.Rebind();
            if (grdShoppingCart.Items.Count == 0)
            {
                Purchase.Enabled = false;
            }
            else
            {
                Purchase.Enabled = true;
            }
            lblStatus.Text = string.Format("{0} products have been deleted", grandTotalQuantity.ToString());
            lblStatus.CssClass = "SuccessMsg";

        }



        private int LoopHierarchyRecursive(GridTableView gridTableView, out bool IsCustomBrace)
        {

            int grandTotalQuantity = 0;
            int totalQuantity = 0;
            bool isCustomBrace = false;
            bool isLogoPart = false;
            BregWcfService.InventoryDiscrepencyItem[] inventoryDiscrepencyItems = BregWcfService.ConsignmentItem.GetConsignmentReplenishmentList(practiceLocationID);

            foreach (GridDataItem gridDataItem in gridTableView.Items)
            {
                if (isCustomBrace != true)
                {
                    isCustomBrace = Convert.ToBoolean(gridDataItem["IsCustomBrace"].Text); 
                }

                CheckBox chkIsLogoPart = gridDataItem.FindControl("chkIsLogoPart") as CheckBox;
                if (chkIsLogoPart != null)
                {                    
                    isLogoPart = chkIsLogoPart.Checked;                    
                }

                
                int shoppingCartItemID = findShoppingCartItemID(gridDataItem);
                short quantity = findQuantity(gridDataItem);
                int originalQuantity = gridDataItem.GetColumnInt32("QuantityHidden");

                //if this is a consignment order and a consignment product, do not allow them to set the quantity below the ReorderQuantity
                if (quantity != originalQuantity)
                {

                    using (var visionDB = ClassLibrary.DAL.VisionDataContext.GetVisionDataContext())
                    {
                        var cart = (from c in visionDB.usp_GetShoppingCart(practiceLocationID)
                                    where c.ShoppingCartItemID == shoppingCartItemID
                                    select c).FirstOrDefault();

                        var product = (from pi in visionDB.ProductInventories
                                       where pi.PracticeLocationID == practiceLocationID
                                       && pi.PracticeCatalogProductID == cart.PracticeCatalogProductID
                                       select pi).FirstOrDefault();

                        var productReorder = (from pr in inventoryDiscrepencyItems
                                              where pr.ProductInventoryID == product.ProductInventoryID
                                              && pr.IsConsignment == true
                                              select pr).FirstOrDefault();

                        if ((productReorder != null)
                            && cart.IsConsignment
                            && product.IsConsignment
                            && (productReorder.ReOrderQuantity > 0))
                        {
                            //can't reduce the quantity below the reorder level
                            quantity = Convert.ToInt16(productReorder.ReOrderQuantity);
                            lblStatus.Text += string.Format("Item {0} is a consignment item and a consignment order.  The quantity cannot be reduced below {1}.<br/>", cart.Code, quantity.ToString());
                            lblStatus.CssClass = "ErrorMsg";
                        }
                    }
                }
                totalQuantity = UpdateCart(shoppingCartItemID, quantity, isLogoPart);

                grandTotalQuantity += totalQuantity;
            }
            IsCustomBrace = isCustomBrace;
            return grandTotalQuantity;
        }

        private int UpdateCart(Int32 ShoppingCartItemID, Int16 Quantity, bool isLogoPart = false)
        {
            // Only update if the Quantity is more than zero.
            //if (Quantity > 0)
            //{
                //int rowsAffected = 0;

                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_UpdateShoppingCartQuantities");

                db.AddInParameter(dbCommand, "ShoppingCartItemID", DbType.Int32, ShoppingCartItemID);
                db.AddInParameter(dbCommand, "Quantity", DbType.Int16, Quantity);
                db.AddInParameter(dbCommand, "IsLogoPart", DbType.Boolean, isLogoPart);
                //    db.AddOutParameter(dbCommand, "ShoppingCartIDReturn", DbType.Int32, rowsAffected);

                int SQLReturn = db.ExecuteNonQuery(dbCommand);
                //return ds;
                //Int32 return_value = Convert.ToInt32(db.GetParameterValue(dbCommand, "ShoppingCartIDReturn"));
                return Quantity;
            //}
        }

        //cost
        private Decimal findActualWholesaleCost(GridDataItem gridDataItem)
        {
            string unitCost = (string)gridDataItem["UnitCost"].Text;
            if (unitCost != null)
            {
                string cleanUnitCost = unitCost.Replace("$", "");
                return Decimal.Parse(cleanUnitCost);
            }
            else
            {
                return 0;
            }
        }

        private Int32 findPracticeCatalogProductID(GridDataItem gridDataItem)
        {
            string practiceCatalogProductID = (string)gridDataItem["PracticeCatalogProductID"].Text;
            if (practiceCatalogProductID != null)
            {
                return Int32.Parse(practiceCatalogProductID);
                //Response.Write(gridDataItem.ItemIndex.ToString() + " is: " + PracticeID.ToString() + "<br />");
            }
            else
            {
                return 0;
            }

        }
        private Int32 findShoppingCartItemID(GridDataItem gridDataItem)
        {
            string ShoppingCartItemID = (string)gridDataItem["ShoppingCartItemID"].Text;
            if (ShoppingCartItemID != null)
            {
                return Int32.Parse(ShoppingCartItemID);
                //Response.Write(gridDataItem.ItemIndex.ToString() + " is: " + PracticeID.ToString() + "<br />");
            }
            else
            {
                return 0;
            }

        }

        private Int32 findShoppingCartItemID(DataRow dr)
        {
            string ShoppingCartItemID = dr["ShoppingCartItemID"].ToString();
            if (ShoppingCartItemID != null)
            {
                return Int32.Parse(ShoppingCartItemID);
            }
            else
            {
                return 0;
            }

        }
        private Int32 findQuantity(DataRow dr)
        {
            string Quantity = dr["Quantity"].ToString();
            if (Quantity != null)
            {
                return Int32.Parse(Quantity);
            }
            else
            {
                return 0;
            }

        }
        private Int16 findQuantity(GridDataItem gridDataItem)
        {
            var nTextBox = (Telerik.Web.UI.RadNumericTextBox)gridDataItem.FindControl("txtQuantity");
            //CheckBox checkBox = (CheckBox)gridDataItem.FindControl("chkAddtoCart");

            if (nTextBox != null)
            {
                return Int16.Parse(nTextBox.Value.ToString());
                //Response.Write(gridDataItem.ItemIndex.ToString() + " is: " + nTextBox.Value.ToString() + "<br />");
            }
            else
            {
                return 0;
                //throw new Exception("Cannot find object with ID 'CheckBox1' in item " + gridDataItem.ItemIndex.ToString());
            }
        }

        protected void btnUpdateQuantity_Click(object sender, EventArgs e)
        {

            bool isCustomBrace = false;
            lblStatus.Text = "";

            int grandTotalQuantity = LoopHierarchyRecursive(grdShoppingCart.MasterTableView, out isCustomBrace);

            grdShoppingCart.DataSource = ShoppingCart.GetShoppingCart(Session.GetInt32("PracticeLocationID")); //GetShoppingCart(Convert.ToInt16(Session["PracticeLocationID"]));
            grdShoppingCart.DataBind();
            if (grdShoppingCart.Items.Count == 0)
            {
                Purchase.Enabled = false;
                btnUpdateQuantity.Enabled = false;
                lblStatus.Text = "Shopping Cart is empty.";
                lblStatus.CssClass = "ErrorMsg";
            }
            else
            {
                if (grandTotalQuantity == 0)
                {
                    Purchase.Enabled = false;    
                }
                else
                {
                    Purchase.Enabled = true;
                    lblStatus.Text += "  Quantities updated.";
                    lblStatus.CssClass = "SuccessMsg";
                } 
            }
        }

        private void PopulateTransferLocation()
        {


            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.USP_GetPracticeAndPracticeLocation2");

            db.AddInParameter(dbCommand, "UserName", DbType.String, Context.User.Identity.Name.ToString());

            IDataReader reader = db.ExecuteReader(dbCommand);

            if (reader.Read())
            {

                // Clear location drop down
                cbxTransferToLocation.ClearSelection();

                do
                {
                    //Fill location drop down box

                    if (Int32.Parse(reader["PracticeLocationID"].ToString()) != Int32.Parse(Session["PracticeLocationID"].ToString()))
                    {
                        RadComboBoxItem item = new RadComboBoxItem();

                        item.Text = reader["Name"].ToString();
                        item.Value = reader["PracticeLocationID"].ToString();
                        item.Font.Size = System.Web.UI.WebControls.FontUnit.XXSmall;

                        cbxTransferToLocation.Items.Add(item);
                    }

                } while (reader.Read());

                reader.Close();

                if (cbxTransferToLocation.SelectedItem != null)
                {
                    cbxTransferToLocation.SelectedItem.Font.Size = System.Web.UI.WebControls.FontUnit.XXSmall;
                }
                else
                {
                    btnTransfer.Enabled = false;
                }
            }
        }



    }
}
