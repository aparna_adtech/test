<%@ Page Language="C#" MasterPageFile="~/MasterPages/SiteMaster.Master" AutoEventWireup="True" CodeBehind="cart.aspx.cs" Inherits="BregVision.cart" Title="Breg Vision - Cart" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="RadComboBox.Net2" Namespace="Telerik.WebControls" TagPrefix="radC" %>
<%@ Register Assembly="RadInput.Net2" Namespace="Telerik.WebControls" TagPrefix="radI" %>
<%@ Register Assembly="RadWindow.Net2" Namespace="Telerik.WebControls" TagPrefix="radW" %>
<%@ Register Assembly="RadAjax.Net2" Namespace="Telerik.WebControls" TagPrefix="radA" %>
<script runat="server">
  private void Button_DoubleClickDisable_PreRender(object sender, EventArgs e)
  {
    var b = sender as Button;
    if (b != null)
      b.Attributes.Add("onclick", "this.disabled=true; this.value='working...'; " + Page.ClientScript.GetPostBackEventReference(b, "").ToString());
  }
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

  <radW:RadWindowManager ID="RadWindowManager1" runat="server">
  </radW:RadWindowManager>

  <radA:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Width="100%" EnableViewState="True" EnableAJAX="false">

    <div class="flex-row subWelcome-bar">
      <h1 class="flex-full">
        <asp:Label ID="Label1" runat="server" Text="Label">Shopping Cart</asp:Label>
      </h1>
      <div class="flex">
        <asp:Panel ID="pnlTransfer" runat="server" HorizontalAlign="Center" CssClass="flex-row">
          <div class="flex align-center">
            <asp:Button ID="btnTransfer" runat="server" Text="Transfer" OnClick="btnTransfer_Click"
              Enabled="True" OnPreRender="Button_DoubleClickDisable_PreRender" CssClass="button-accent" />
            <asp:Label ID="lblTo" runat="server" Width="20px"> to </asp:Label>
            <radC:RadComboBox ID="cbxTransferToLocation" runat="server" Skin="Breg" EnableEmbeddedSkins="False"
              Width="150px" Height="27px" ExpandEffect="Fade" AutoPostBack="False" Visible="true" ShowWhileLoading="False" ToolTip="Select a practice location to transfer" CssClass="RadComboBox_Breg">
            </radC:RadComboBox>
          </div>
        </asp:Panel>
        <div class="flex-row justify-center">
          <bvu:Help ID="help133" runat="server" HelpID="133" />
          <asp:Button ID="btnDeleteAll" runat="server" Text="Empty Cart" OnClick="btnDeleteAll_Click" Enabled="True" OnPreRender="Button_DoubleClickDisable_PreRender" CssClass="button-accent" />     
        </div>
        <div class="flex-row justify-center">
          <bvu:Help ID="help132" runat="server" HelpID="132" />
          <asp:Button ID="btnUpdateQuantity" runat="server" Text="Update Quantity" OnClick="btnUpdateQuantity_Click"
            Enabled="True" OnPreRender="Button_DoubleClickDisable_PreRender" CssClass="button-accent" />
        </div>
        <div class="flex-row justify-end">
          <bvu:Help ID="help131" runat="server" HelpID="131" />
          <asp:Button ID="Purchase" runat="server" Text="Purchase" OnClick="Purchase_Click" CssClass="button-primary"
            Enabled="False" OnPreRender="Button_DoubleClickDisable_PreRender" />
        </div>
      </div>
    </div>
    <div class="subTabContent-container">
      <div class="content">
        <asp:Label ID="lblStatus" runat="server" class="WarningMsg"></asp:Label>
        <radG:RadGrid ID="grdShoppingCart" runat="server" GridLines="None" PageSize="20" EnableAJAX="false" EnableAJAXLoadingTemplate="false" LoadingTemplateTransparency="10" AllowPaging="True" AllowSorting="True" Skin="Breg" EnableEmbeddedSkins="False" AllowMultiRowSelection="True" AutoGenerateColumns="false" OnDeleteCommand="grdShoppingCart_DeleteCommand" OnNeedDataSource="grdShoppingCart_NeedDataSource" OnItemDataBound="grdShoppingCart_ItemDataBound" OnPageIndexChanged="grdShoppingCart_PageIndexChanged">
          <PagerStyle Mode="NextPrevAndNumeric" />
          <MasterTableView ShowFooter="True">
            <ExpandCollapseColumn Visible="False">
            </ExpandCollapseColumn>
            <RowIndicatorColumn Visible="False">
            </RowIndicatorColumn>

            <Columns>
              <radG:GridButtonColumn Text="Delete" ButtonType="ImageButton" ImageUrl="~/App_Themes/Breg/images/Common/delete.png" ConfirmText="Delete this product?" CommandName="Delete" UniqueName="Delete"></radG:GridButtonColumn>
              <radG:GridBoundColumn DataField="Supplier" HeaderText="Supplier" UniqueName="Supplier">
              </radG:GridBoundColumn>
              <radG:GridBoundColumn DataField="Category" HeaderText="Category" UniqueName="Category">
              </radG:GridBoundColumn>
              <radG:GridBoundColumn DataField="Product" HeaderText="Product" UniqueName="Product">
              </radG:GridBoundColumn>
              <radG:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
              </radG:GridBoundColumn>
              <radG:GridBoundColumn DataField="Side" HeaderText="Side" UniqueName="LeftRightSide" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
              </radG:GridBoundColumn>
              <radG:GridBoundColumn DataField="Size" HeaderText="Size" UniqueName="Size" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
              </radG:GridBoundColumn>
              <radG:GridBoundColumn DataField="Gender" HeaderText="Gender" UniqueName="Gender" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
              </radG:GridBoundColumn>
              <radG:GridBoundColumn DataField="practicecatalogproductid" UniqueName="practicecatalogproductid" Display="False">
              </radG:GridBoundColumn>
              <radG:GridBoundColumn DataField="ShoppingCartItemID" UniqueName="ShoppingCartItemID" Display="False">
              </radG:GridBoundColumn>
              <radG:GridBoundColumn DataField="ParLevel" HeaderText="Par" UniqueName="ParLevel" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
              </radG:GridBoundColumn>
              <radG:GridBoundColumn DataField="QuantityOnHand" HeaderText="On Hand" UniqueName="QuantityOnHand" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
              </radG:GridBoundColumn>
              <radG:GridBoundColumn DataField="Quantity" UniqueName="QuantityHidden" Display="False">
              </radG:GridBoundColumn>
              <radG:GridBoundColumn DataField="IsCustomBrace" UniqueName="IsCustomBrace" Display="False">
              </radG:GridBoundColumn>
              <radG:GridTemplateColumn UniqueName="Quantity" DataField="Quantity" HeaderText="Quantity" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                <ItemTemplate>
                  <telerik:RadNumericTextBox ID="txtQuantity" Text='<%# Eval("quantity") %>' runat="server"  ShowSpinButtons="False" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" Skin="Breg" EnableEmbeddedSkins="False" NumberFormat-DecimalDigits="0" SpinDownCssClass="Breg" SpinUpCssClass="Breg" >
                  </telerik:RadNumericTextBox>
                </ItemTemplate>
              </radG:GridTemplateColumn>

              <radG:GridTemplateColumn UniqueName="IsLogoPart" DataField="IsLogoPart" HeaderText="Custom Logo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                <HeaderTemplate>
                  Logo<br />
                  <asp:CheckBox ID="chkAllLogoParts" runat="server" OnCheckedChanged="chkAllLogoParts_OnCheckChanged" AutoPostBack="true" />
                </HeaderTemplate>
                <ItemTemplate>
                  <asp:CheckBox ID="chkIsLogoPart" runat="server" />
                </ItemTemplate>
              </radG:GridTemplateColumn>

              <radG:GridBoundColumn DataField="Cost" HeaderText="Unit Cost" UniqueName="UnitCost" DataFormatString="{0:C}" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" >
              </radG:GridBoundColumn>

              <radG:GridTemplateColumn HeaderText="Total Cost" UniqueName="TotalCost" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" >
                <ItemTemplate>
                  <asp:Label ID="lblTotalCost" Text='<%# Eval("LineTotal", "{0:$#,#.00}")%>' runat="server"></asp:Label>
                </ItemTemplate>

              </radG:GridTemplateColumn>
            </Columns>

          </MasterTableView>
          <ClientSettings AllowDragToGroup="True">
          </ClientSettings>
          <GroupPanel Visible="True">
          </GroupPanel>
        </radG:RadGrid>
      </div>
    </div>
  </radA:RadAjaxPanel>
</asp:Content>
