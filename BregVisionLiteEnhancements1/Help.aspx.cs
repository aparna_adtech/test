﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace BregVision
{
    public partial class Help : System.Web.UI.Page
    {
        public string _ToolTipHelper = string.Empty;
        public string _BregAdmin = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void Page_Init(object sender, EventArgs e)   //protected  void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["ParentId"] != null)
            {
                _ToolTipHelper = ",ArticleId=" + Request.QueryString["ParentId"].ToString();
            }


            _BregAdmin = Roles.IsUserInRole(User.Identity.Name, "BregAdmin").ToString();

                
        }

        
    }
}
