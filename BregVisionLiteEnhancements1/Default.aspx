﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" CodeBehind="Default.aspx.cs"
    Inherits="BregVision.Default" EnableSessionState="True" %>

<%@ Register Src="~/UserControls/LandingPage/VisionLanding.ascx" TagName="VisionLanding"
    TagPrefix="bvu" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Inventory Management Software for Orthopedic Practices | Breg Vision</title>
    <link rel="shortcut icon" href="breg_favicon.ico"
        type="image/x-icon" />
    
    <%--<link rel="stylesheet" href="Styles/LandingPage/reset.css" />--%>
    <link rel="stylesheet" href="Styles/LandingPage/DarkTheme.css" />

</head>
<body class="landingBody">
    <div class="landingContainer">
        <form id="form1" runat="server" method="post" action="Default.aspx">
        <bvu:VisionLanding ID="ctlVisionLanding" runat="server" />
        </form>

    </div>
</body>
</html>
