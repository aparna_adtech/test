﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using ClassLibrary.BLL.HelpSystem;
using Telerik.Web.UI;

namespace BregVision
{
    public partial class HelpControl : System.Web.UI.UserControl
    {
        RadTreeNode _TopNode = new RadTreeNode();

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public void toolbar_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            Console.WriteLine(e.Item.Text.ToString());
        }

        public void PopulateTree(int id, int depth)
        {
            // panel swap to treeView

            treeView.Nodes.Clear();

            HelpGroup helpGroup = HelpGroup.GetHelpGroup(id, depth);

            ProcessGroup(helpGroup, _TopNode);

            treeView.Nodes.Add(_TopNode);
        }

        private void ProcessGroup(HelpGroup thisGroup, RadTreeNode thisNode)
        {
            thisNode.Text = thisGroup.Title;
            thisNode.Value = thisGroup.Id.ToString();
            thisNode.ImageUrl = "~/Images/folder16.png";

            foreach (HelpGroup group in thisGroup.HelpGroups)
            {
                RadTreeNode node = new RadTreeNode();
                ProcessGroup(group, node);
                thisNode.Nodes.Add(node);
            }

            foreach (HelpArticle article in thisGroup.HelpArticles)
            {
                RadTreeNode node = new RadTreeNode();
                ProcessArticle(article, node);
                thisNode.Nodes.Add(node);
            }

            foreach (HelpMedia media in thisGroup.HelpMedias)
            {
                RadTreeNode node = new RadTreeNode();
                ProcessMedia(media, node);
                thisNode.Nodes.Add(node);
            }

            if (thisGroup.HelpGroups.Count > 0 || thisGroup.HelpArticles.Count > 0 || thisGroup.HelpMedias.Count > 0)
            {
                thisNode.Expanded = true;
            }
        }

        private void ProcessArticle(HelpArticle thisArticle, RadTreeNode thisNode)
        {
            thisNode.Text = thisArticle.Title;
            thisNode.Value = thisArticle.Id.ToString();
            thisNode.ImageUrl = "~/Images/question16.png";
            
            foreach (HelpArticle article in thisArticle.RelatedArticles)
            {
                RadTreeNode node = new RadTreeNode();
                ProcessArticle(article, node);
                thisNode.Nodes.Add(node);
            }

            foreach (HelpMedia media in thisArticle.RelatedMedia)
            {
                RadTreeNode node = new RadTreeNode();
                ProcessMedia(media, node);
                thisNode.Nodes.Add(node);
            }

            if (thisArticle.RelatedArticles.Count > 0 || thisArticle.RelatedMedia.Count > 0)
            {
                thisNode.Expanded = true;
            }
        }

        private void ProcessMedia(HelpMedia thisMedia, RadTreeNode thisNode)
        {
            thisNode.Text = thisMedia.Title;
            thisNode.Value = thisMedia.Id.ToString();
            thisNode.ImageUrl = "~/Images/vid16.png";

            foreach (HelpMedia media in thisMedia.RelatedMedia)
            {
                RadTreeNode node = new RadTreeNode();
                ProcessMedia(media, node);
                thisNode.Nodes.Add(node);
            }

            foreach (HelpArticle article in thisMedia.RelatedArticles)
            {
                RadTreeNode node = new RadTreeNode();
                ProcessArticle(article, node);
                thisNode.Nodes.Add(node);
            }

            if (thisMedia.RelatedArticles.Count > 0 || thisMedia.RelatedMedia.Count > 0)
            {
                thisNode.Expanded = true;
            }
        }
    }
}