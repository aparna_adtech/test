﻿using System;
using System.IO;
using System.Web;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;

namespace BregVision.Handlers
{
    public class DownloadFileHandlerHelper : IHttpModule
    {
        /// <summary>
        /// You will need to configure this module in the Web.config file of your
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpModule Members

        public void Dispose()
        {
            //clean-up code here.
        }

        public void Init(HttpApplication context)
        {
            // Below is an example of how you can handle LogRequest event and provide 
            // custom logging implementation for it
            context.LogRequest += new EventHandler(OnLogRequest);
        }

        #endregion

        public void OnLogRequest(Object source, EventArgs e)
        {
            //custom logging logic can go here
        }

        public static byte[] MergePdfDocuments(byte[][] files)
        {
            PdfDocument outputDocument = new PdfDocument();
            var i = 0;
            do
            {
                var file = files[i];
                using (var stream = new MemoryStream(file))
                {
                    var inputDocument = PdfReader.Open(stream, PdfDocumentOpenMode.Import);
                    int count = inputDocument.PageCount;
                    for (int idx = 0; idx < count; idx++)
                    {
                        // Get the page from the external document...
                        PdfPage page = inputDocument.Pages[idx];
                        // ...and add it to the output document.
                        outputDocument.AddPage(page);
                    }
                }
            }
            while (++i < files.Length);

            using (var stream = new MemoryStream())
            {
                outputDocument.Save(stream);
                return stream.ToArray();
            }

        }
    }
}
