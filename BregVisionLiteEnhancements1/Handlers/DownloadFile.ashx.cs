﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;


namespace BregVision.Handlers
{
    /// <summary>
    /// Summary description for DownloadFile
    /// </summary>
    public class DownloadFile : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            System.Web.HttpRequest request = System.Web.HttpContext.Current.Request;
            string fileName = request.QueryString["fileName"];
            string fileType = request.QueryString["fileType"];
            string token = request.QueryString["token"];
            string fileExtension = "";

            string dataSessionKey, dataSessionExpireKey;
            Dispense.GetFileDownloadSessionKeys(token, out dataSessionKey, out dataSessionExpireKey);

            var expire = long.MinValue;
            {
                var o = context.Session[dataSessionExpireKey];
                if (o != null)
                    expire = (long)o;
            }
            var isExpired = (expire < DateTime.Now.Ticks);

            var bts = (byte[])context.Session[dataSessionKey];

            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.ClearContent();
            response.Clear();

            // secure document may only be downloaded one time. please try again
            if (bts != null && !isExpired)
            {
                switch (fileType)
                {
                    case "pdf":
                        fileExtension = ".pdf";
                        response.ContentType = "application/pdf";
                        break;
                    default:
                        response.ContentType = "text/plain";
                        fileExtension = ".txt";
                        break;
                }

                response.AddHeader("Content-Disposition", "inline; filename=" + fileName + fileExtension + ";");
                response.OutputStream.Write(bts, 0, bts.Length);
            }
            else
            {
                var s = "The requested secure document(s) may only be downloaded one time per request. Please submit your request again.";
                var b = System.Text.Encoding.Default.GetBytes(s);

                response.ContentType = "text/plain";
                response.OutputStream.Write(b, 0, b.Length);
            }

            response.Flush();
            CleanupSessionData(context);
            response.End();
        }

        private void CleanupSessionData(HttpContext context)
        {
            var prefix = Dispense.FileDownloadSessionKeyPrefix;
            var suffix = Dispense.FileDownloadSessionKeyExpirationSuffix;

            var sessionKeysToDelete = new List<string>();

            try
            {
                foreach (var key in context.Session.Keys)
                {
                    // just in case...
                    if (key == null)
                        continue;

                    var k = key.ToString();

                    // only process expiration keys
                    if (k.StartsWith(prefix) == false)
                        continue;
                    if (k.EndsWith(suffix) == false)
                        continue;

                    // get token from key and verify expiration
                    var token = Dispense.GetTokenFromSessionKey(k);
                    string dataSessionKey, dataSessionExpireKey;
                    Dispense.GetFileDownloadSessionKeys(token, out dataSessionKey, out dataSessionExpireKey);
                    var expire = long.MinValue;
                    {
                        var o = context.Session[dataSessionExpireKey];
                        if (o != null)
                            expire = (long)o;
                    }
                    if (expire < DateTime.Now.Ticks)
                    {
                        sessionKeysToDelete.Add(dataSessionExpireKey);
                        sessionKeysToDelete.Add(dataSessionKey);
                    }
                }
            }
            catch
            { /* if something fails above, let session timeout handle it */ }

            // remove any expired keys
            foreach (var sk in sessionKeysToDelete)
                context.Session.Remove(sk);
        }

        public bool IsReusable
        {
            get { return false; }
        }

    }
}