﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;

namespace BregVision
{
    public partial class contact : System.Web.UI.Page
    {
        public void contactformsubmit_Click(object s, EventArgs e)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
#if(DEBUG)
                db = DatabaseFactory.CreateDatabase("BregVisionBimsDevConnectionString");
#endif
                string MailTo = System.Configuration.ConfigurationManager.AppSettings["ContactFormRecipientAddress"] ?? @"visioncc@breg.com";
#if(DEBUG)
                MailTo = @"hchan@breg.com";
#endif
                string MailFromProfile = @"OrderConfirmation";
                string MailBody = string.Format(@"
        <table>
        <tr>
        <td>Practice Name:</td>
        <td>{0}</td>
        </tr>
        <tr>
        <td>Number of Physicians:</td>
        <td>{1}</td>
        </tr>
        <tr>
        <td>First Name:</td>
        <td>{2}</td>
        </tr>
        <tr>
        <td>Last Name:</td>
        <td>{3}</td>
        </tr>
        <tr>
        <td>Phone Number:</td>
        <td>{4}</td>
        </tr>
        <tr>
        <td>Email:</td>
        <td>{5}</td>
        </tr>
        </table>
",
                    practicename.Text,
                    physiciannum.Text,
                    firstname.Text,
                    lastname.Text,
                    phone.Text,
                    email.Text);

                DbCommand dbCommand = db.GetStoredProcCommand("msdb.dbo.sp_send_dbmail");

                //DBMail profile name:  Order Confirmation.
                db.AddInParameter(dbCommand, "profile_name", DbType.String, MailFromProfile);

                db.AddInParameter(dbCommand, "recipients", DbType.String, MailTo);
                db.AddInParameter(dbCommand, "subject", DbType.String, @"Login Page Contact Request");

                db.AddInParameter(dbCommand, "body", DbType.String, MailBody);
                db.AddInParameter(dbCommand, "body_format", DbType.String, "HTML");

                int SQLReturn = db.ExecuteNonQuery(dbCommand);

                ContactPanel.Visible = false;
                ThankYouPanel.Visible = true;
            }
            catch
            {
                /* do nothing for now */
            }

            practicename.Text = "";
            physiciannum.Text = "";
            firstname.Text = "";
            lastname.Text = "";
            phone.Text = "";
            email.Text = "";
        }
    }
}