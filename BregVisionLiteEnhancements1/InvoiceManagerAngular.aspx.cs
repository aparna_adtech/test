﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BregVision
{
    public partial class InvoiceManager : System.Web.UI.Page
    {
        public int PracticeId { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            PracticeId = (((MasterPages.PracticeAdmin)Page.Master)).Master.practiceID;
        }
    }
}