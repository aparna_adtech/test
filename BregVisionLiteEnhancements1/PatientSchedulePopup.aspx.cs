﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using ClassLibrary.DAL.Helpers;
using System.Text.RegularExpressions;
using BregVision.Extensions;
using BregWcfService.EMRIntegrationReference;

namespace BregVision
{
  public partial class PatientSchedulePopup : Bases.PageBase
  {
    private const string ALL_PHYSICIANS = "-All Physicians-";

    private BregWcfService.EMRIntegrationReference.schedule_item[] _scheduleItems;
    private string _searchText = "";
    private string _physicianNameSearchText = "";

    protected void Page_Load(object sender, EventArgs e)
    {
      PracticeCatalogSearch.searchEvent += new UserControls.General.SearchEventHandler(PracticeCatalogSearch_searchEvent);
      _scheduleItems = GetScheduledPatients();

      if (!Page.IsPostBack)
      {
        GetPhysicianList();
      }

      if (Page.IsPostBack)
      {
        if (cboPhysicians.SelectedItem.Text != ALL_PHYSICIANS)
          _physicianNameSearchText = cboPhysicians.SelectedValue;
        else
          _physicianNameSearchText = "";
      }
    }

    private void GetPhysicianList()
    {
      var allPhysicians = (new[] {new {physician_name = ALL_PHYSICIANS}}).ToList();
      var distinctPhysicianQuery = (from p in _scheduleItems
        select new
        {
          p.physician_name
        }).Distinct().ToList();

      var all = allPhysicians.Union(distinctPhysicianQuery).ToList();

      cboPhysicians.DataSource = all;
      cboPhysicians.DataTextField = "physician_name";
      cboPhysicians.DataValueField = "physician_name";
      cboPhysicians.DataBind();
    }

    void PracticeCatalogSearch_searchEvent(object sender, UserControls.General.SearchEventArgs e)
    {
      //_searchText = e.SearchText;
      ViewState["SearchText"] = e.SearchText;
      grdPatientSchedule.Rebind();
    }

    protected void grdPatientSchedule_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
      if (ViewState["SearchText"] != null)
      {
        _searchText = (string) ViewState["SearchText"];
      }

      grdPatientSchedule.DataSource = _scheduleItems.Search(_searchText, _physicianNameSearchText);
    }

    private BregWcfService.EMRIntegrationReference.schedule_item[] GetScheduledPatients()
    {
      DateTime startDate = DateTime.UtcNow;
      DateTime endDate = DateTime.UtcNow; // Note(Henry): endDate is no longer being used by the GetPatients method

      var client = new EMRIntegrationClient();
      var scheduleItems = client.GetPatients(practiceID, practiceLocationID, startDate, endDate);
      return scheduleItems;
    }

    protected void grdPatientSchedule_DetailTableDataBind(object s, GridDetailTableDataBindEventArgs e)
    {
      List<schedule_item> list = new List<schedule_item>();
      list.Add((schedule_item) e.DetailTableView.ParentItem.DataItem);
      e.DetailTableView.DataSource = list;
    }

    protected void cboPhysicians_SelectedIndexChanged(object s, EventArgs e)
    {
      grdPatientSchedule.Rebind();
    }
  }
}