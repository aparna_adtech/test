﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.WebControls;

namespace BregVision.BregAdmin
{


    public static class GridViewExtensions
    {
        public static Dictionary<string, int> fieldNames;


#region Generic Functions

        //for getting controls
        /// <summary>
        /// Gets a Column Value.  Be sure to set the internal dictionary "fieldNames" before calling, ie 
        ///    private Dictionary&lt;string, int&gt; summaryFields = new Dictionary&lt;string, int&gt;(); 
        ///    summaryFields.Add("ClientSelectColumn", 0); 
        ///    summaryFields.Add("chkIsMedicare", 2); 
        ///    summaryFields.Add("chkABNForm", 3); 
        ///    summaryFields.Add("DispenseQueueID", 5); 
        ///    GridViewExtensions.fieldNames = summaryFields; 
        /// </summary>
        /// <typeparam name="U">The Type that control property will be converted to and returned</typeparam>
        /// <typeparam name="V">The Type of the property of the control</typeparam>
        /// <param name="row">The GridRow</param>
        /// <param name="controlName">The name of the control within the GridRow, ie txtFirstName</param>
        /// <param name="propertyName">The name of the property to get and convert, it "Text"</param>
        /// <returns></returns>
        public static U GetColumn<U, V>(this System.Web.UI.WebControls.TableRow row, string controlName, string propertyName)
        {
            U ret = default(U);
            try
            {
                V text = GetColumnProperty<V>(row, controlName, propertyName);
                ret = ConvertFromString<U>((string)(object)text);
            }
            catch
            {

            }
            return ret;
        }


        /// <summary>
        /// Gets a Control.  Be sure to set the internal dictionary "fieldNames" before calling, ie 
        ///    private Dictionary&lt;string, int&gt; summaryFields = new Dictionary&lt;string, int&gt;(); 
        ///    summaryFields.Add("ClientSelectColumn", 0); 
        ///    summaryFields.Add("chkIsMedicare", 2); 
        ///    summaryFields.Add("chkABNForm", 3); 
        ///    summaryFields.Add("DispenseQueueID", 5); 
        ///    GridViewExtensions.fieldNames = summaryFields; 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="row"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        public static T GetControl<T>(this System.Web.UI.WebControls.TableRow row, string controlName) where T : Control
        {
            //return (T)GetControl(row.Cells[fieldNames[controlName]], controlName);
            T ret = default(T);
            try
            {
                ret = row.FindControl(controlName) as T;
            }
            catch { }
            return ret;
 
        }

        /// <summary>
        /// Gets a Control.  Be sure to set the internal dictionary "fieldNames" before calling, ie 
        ///    private Dictionary&lt;string, int&gt; summaryFields = new Dictionary&lt;string, int&gt;(); 
        ///    summaryFields.Add("ClientSelectColumn", 0); 
        ///    summaryFields.Add("chkIsMedicare", 2); 
        ///    summaryFields.Add("chkABNForm", 3); 
        ///    summaryFields.Add("DispenseQueueID", 5); 
        ///    GridViewExtensions.fieldNames = summaryFields; 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="row"></param>
        /// <param name="controlName"></param>
        /// <param name="cellNumber"></param>
        /// <returns></returns>
        public static T GetControl<T>(this System.Web.UI.WebControls.TableRow row, string controlName, int cellNumber) where T : Control
        {
            T ret = default(T);
            try
            {
                ret = row.FindControl(controlName) as T;
            }
            catch { }
            return ret;
        }

        /// <summary>
        /// Gets a Control.  
        /// </summary>
        /// <typeparam name="T">The Type of the control, ie TextBox</typeparam>
        /// <param name="cell"></param>
        /// <param name="controlName">the name of the control, ie txtFirstName</param>
        /// <returns></returns>
        public static T GetControl<T>(this System.Web.UI.WebControls.TableCell cell, string controlName) where T : Control
        {
            T ret = default(T);
            try
            {
                ret = (T)GetControl(cell, controlName);
            }
            catch { }
            return ret;
        }

        /// <summary>
        /// Gets a control
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        public static Control GetControl(this System.Web.UI.WebControls.TableCell cell, string controlName)
        {
            Control ctrl = null;
            try
            {
                ctrl = cell.FindControl(controlName);
            }
            catch { }
            return ctrl;
        }

        //for getting properties from controls
        /// <summary>
        /// Gets a property(like "Text") from a web control(like a TextBox) embedded in a GridViewRow 
        /// Be sure to set the internal dictionary "fieldNames" before calling, ie 
        ///    private Dictionary&lt;string, int&gt; summaryFields = new Dictionary&lt;string, int&gt;(); 
        ///    summaryFields.Add("ClientSelectColumn", 0); 
        ///    summaryFields.Add("chkIsMedicare", 2); 
        ///    summaryFields.Add("chkABNForm", 3); 
        ///    summaryFields.Add("DispenseQueueID", 5); 
        ///    GridViewExtensions.fieldNames = summaryFields; 
        /// </summary>
        /// <typeparam name="U">The type of the property, ie string</typeparam>
        /// <param name="row">The GridViewRow</param>
        /// <param name="controlName">The name of the control, ie txtFirstName</param>
        /// <param name="propertyName">The name of the property, ie "Text"</param>
        /// <returns></returns>
        public static U GetColumnProperty<U>(this System.Web.UI.WebControls.TableRow row, string controlName, string propertyName)
        {
            U ret = default(U);
            try
            {
                ret =  (U)GetColumnProperty<U>(row.Cells[fieldNames[controlName]], controlName, propertyName);
            }
            catch { }
            return ret;
        }

        /// <summary>
        /// Gets property from a Control.  
        /// </summary>
        /// <typeparam name="U"></typeparam>
        /// <param name="cell"></param>
        /// <param name="controlName"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static U GetColumnProperty<U>(this System.Web.UI.WebControls.TableCell cell, string controlName, string propertyName)
        {
            U propertyToFind = default(U);
            var control = cell.FindControl(controlName);
            if (control != null)
            {
                try
                {
                    propertyToFind = (U)System.ComponentModel.TypeDescriptor.GetProperties(control)[propertyName].GetValue(control);
                }
                catch { }
            }
            return propertyToFind;
        }

        /// <summary>
        /// Converts a string to a generic type
        /// </summary>
        /// <typeparam name="V">The Type to be converted to.  Can be Int32, Decimal, bool, DateTime</typeparam>
        /// <param name="propertyValue">The value to be converted</param>
        /// <returns>propertyValue, converted to V</returns>
        public static V ConvertFromString<V>(string propertyValue) 
        {
            V returnValue = default(V);
            try
            {
                if (returnValue is Int32)
                {
                    int textInt32 = 0;
                    Int32.TryParse(propertyValue, out textInt32);
                    returnValue = (V)(object)textInt32;
                }
                else if (returnValue is Decimal)
                {
                    decimal txtDecimal = decimal.Zero;
                    propertyValue = propertyValue.Replace("$", "");
                    decimal.TryParse(propertyValue, out txtDecimal);
                    returnValue = (V)(object)txtDecimal;
                }
                else if (returnValue is bool)
                {
                    bool txtBool = false;
                    bool.TryParse(propertyValue, out txtBool);
                    returnValue = (V)(object)txtBool;
                }
                else if (returnValue is DateTime)
                {
                    DateTime txtDateTime = DateTime.MinValue;
                    DateTime.TryParse(propertyValue, out txtDateTime);
                    returnValue = (V)(object)txtDateTime;
                }
                else if (returnValue is DateTime?)
                {
                    DateTime txtDateTime;
                    if (!DateTime.TryParse(propertyValue, out txtDateTime))
                        returnValue = (V)(object)(DateTime?)null;
                    else
                        returnValue = (V)(object)txtDateTime;
                }
                else
                {
                    throw new ApplicationException(string.Format("Unable to Convert this type({0})", typeof(V).ToString()));
                }
            }
            catch
            { }
            return returnValue;
        }
#endregion //Generic Functions


        /// <summary>
        /// Be sure to set the internal dictionary "fieldNames" before calling, ie 
        ///    private Dictionary&lt;string, int&gt; summaryFields = new Dictionary&lt;string, int&gt;(); 
        ///    summaryFields.Add("ClientSelectColumn", 0); 
        ///    summaryFields.Add("chkIsMedicare", 2); 
        ///    summaryFields.Add("chkABNForm", 3); 
        ///    summaryFields.Add("DispenseQueueID", 5); 
        ///    GridViewExtensions.fieldNames = summaryFields; 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        public static Int32 GetBoundColumnInt32(this System.Web.UI.WebControls.TableRow row, string controlName)
        {
            try
            {
                string text = row.Cells[fieldNames[controlName]].Text;
                return ConvertFromString<Int32>(text);
            }
            catch
            {
                return 0;
            }
        }

        public static Int32 GetBoundColumnInt32(this Telerik.Web.UI.GridDataItem row, string controlName)
        {
            try
            {
                return ConvertFromString<Int32>(row[controlName].Text);
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// Be sure to set the internal dictionary "fieldNames" before calling, ie 
        ///    private Dictionary&lt;string, int&gt; summaryFields = new Dictionary&lt;string, int&gt;(); 
        ///    summaryFields.Add("ClientSelectColumn", 0); 
        ///    summaryFields.Add("chkIsMedicare", 2); 
        ///    summaryFields.Add("chkABNForm", 3); 
        ///    summaryFields.Add("DispenseQueueID", 5); 
        ///    GridViewExtensions.fieldNames = summaryFields; 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        public static Decimal GetBoundColumnDecimal(this System.Web.UI.WebControls.TableRow row, string controlName)
        {
            Decimal ret = Decimal.Zero;
            try
            {
                string text = row.Cells[fieldNames[controlName]].Text;
                ret = ConvertFromString<Decimal>(text);
            }
            catch { }
            return ret;
        }

        public static Decimal GetBoundColumnDecimal(this Telerik.Web.UI.GridDataItem row, string controlName)
        {
            try
            {
                return ConvertFromString<Decimal>(row[controlName].Text);
            }
            catch
            {
                return default(Decimal);
            }
        }

        /// <summary>
        /// Be sure to set the internal dictionary "fieldNames" before calling, ie 
        ///    private Dictionary&lt;string, int&gt; summaryFields = new Dictionary&lt;string, int&gt;(); 
        ///    summaryFields.Add("ClientSelectColumn", 0); 
        ///    summaryFields.Add("chkIsMedicare", 2); 
        ///    summaryFields.Add("chkABNForm", 3); 
        ///    summaryFields.Add("DispenseQueueID", 5); 
        ///    GridViewExtensions.fieldNames = summaryFields; 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        public static string GetBoundColumnString(this System.Web.UI.WebControls.TableRow row, string controlName)
        {
            string ret = string.Empty;
            try
            {
                ret = row.Cells[fieldNames[controlName]].Text;
            }
            catch { }
            return ret == null ? "" : ret;
        }

        public static string GetBoundColumnString(this Telerik.Web.UI.GridDataItem row, string controlName)
        {
            try
            {
                return row[controlName].Text;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Be sure to set the internal dictionary "fieldNames" before calling, ie 
        ///    private Dictionary&lt;string, int&gt; summaryFields = new Dictionary&lt;string, int&gt;(); 
        ///    summaryFields.Add("ClientSelectColumn", 0); 
        ///    summaryFields.Add("chkIsMedicare", 2); 
        ///    summaryFields.Add("chkABNForm", 3); 
        ///    summaryFields.Add("DispenseQueueID", 5); 
        ///    GridViewExtensions.fieldNames = summaryFields; 
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        public static decimal GetSafeDecimal(this System.Web.UI.WebControls.TableCell cell, string controlName)
        {
            decimal ret = decimal.Zero;
            try
            {
                string text = GetColumnProperty<string>(cell, controlName, "Text");
                ret = ConvertFromString<Decimal>(text);
            }
            catch { }
            return ret;
        }

        /// <summary>
        /// Be sure to set the internal dictionary "fieldNames" before calling, ie 
        ///    private Dictionary&lt;string, int&gt; summaryFields = new Dictionary&lt;string, int&gt;(); 
        ///    summaryFields.Add("ClientSelectColumn", 0); 
        ///    summaryFields.Add("chkIsMedicare", 2); 
        ///    summaryFields.Add("chkABNForm", 3); 
        ///    summaryFields.Add("DispenseQueueID", 5); 
        ///    GridViewExtensions.fieldNames = summaryFields; 
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        public static int GetSafeInt(this System.Web.UI.WebControls.TableCell cell, string controlName)
        {
            int ret = 0;
            try
            {
                string value = GetTextBoxValue(cell, controlName);
                ret = ConvertFromString<Int32>(value);
            }
            catch { }
            return ret;
        }


        /// <summary>
        /// Be sure to set the internal dictionary "fieldNames" before calling, ie 
        ///    private Dictionary&lt;string, int&gt; summaryFields = new Dictionary&lt;string, int&gt;(); 
        ///    summaryFields.Add("ClientSelectColumn", 0); 
        ///    summaryFields.Add("chkIsMedicare", 2); 
        ///    summaryFields.Add("chkABNForm", 3); 
        ///    summaryFields.Add("DispenseQueueID", 5); 
        ///    GridViewExtensions.fieldNames = summaryFields; 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        public static string GetTextBoxValue(this System.Web.UI.WebControls.TableRow row, string controlName)
        {
            string ret = string.Empty;
            try
            {
                ret = GetTextBoxValue(row.Cells[fieldNames[controlName]], controlName);
            }
            catch { }
            return ret;
        }


        /// <summary>
        /// Be sure to set the internal dictionary "fieldNames" before calling, ie 
        ///    private Dictionary&lt;string, int&gt; summaryFields = new Dictionary&lt;string, int&gt;(); 
        ///    summaryFields.Add("ClientSelectColumn", 0); 
        ///    summaryFields.Add("chkIsMedicare", 2); 
        ///    summaryFields.Add("chkABNForm", 3); 
        ///    summaryFields.Add("DispenseQueueID", 5); 
        ///    GridViewExtensions.fieldNames = summaryFields; 
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        public static string GetTextBoxValue(this System.Web.UI.WebControls.TableCell cell, string controlName)
        {
            string ret = string.Empty;
            try
            {
                ret = GetColumnProperty<string>(cell, controlName, "Text");
            }
            catch { }
            return ret;
        }



        public static bool GetCheckBoxValue(this System.Web.UI.WebControls.TableRow row, string controlName)
        {
            bool ret = false;
            try
            {
                ret = GetCheckBoxValue(row, controlName, fieldNames[controlName]);
            }
            catch { }
            return ret;
        }
        public static bool GetCheckBoxValue(this System.Web.UI.WebControls.TableRow row, string controlName, int cellNumber)
        {
            bool ret = false;
            try
            {
                ret = row.Cells[cellNumber].GetCheckBoxValue(controlName);
            }
            catch { }
            return ret;
        }

        public static bool GetCheckBoxBool(this Telerik.Web.UI.GridDataItem row, string controlName)
        {
            try
            {
                return ((CheckBox)row.FindControl(controlName)).Checked;
            }
            catch
            {
                return default(bool);
            }
        }

        /// <summary>
        /// Be sure to set the internal dictionary "fieldNames" before calling, ie 
        ///    private Dictionary&lt;string, int&gt; summaryFields = new Dictionary&lt;string, int&gt;(); 
        ///    summaryFields.Add("ClientSelectColumn", 0); 
        ///    summaryFields.Add("chkIsMedicare", 2); 
        ///    summaryFields.Add("chkABNForm", 3); 
        ///    summaryFields.Add("DispenseQueueID", 5); 
        ///    GridViewExtensions.fieldNames = summaryFields; 
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        public static bool GetCheckBoxValue(this System.Web.UI.WebControls.TableCell cell, string controlName)
        {
            bool ret = false;
            try
            {
                ret = GetColumnProperty<bool>(cell, controlName, "Checked");
            }
            catch { }
            return ret;
        }


        /// <summary>
        /// Be sure to set the internal dictionary "fieldNames" before calling, ie 
        ///    private Dictionary&lt;string, int&gt; summaryFields = new Dictionary&lt;string, int&gt;(); 
        ///    summaryFields.Add("ClientSelectColumn", 0); 
        ///    summaryFields.Add("chkIsMedicare", 2); 
        ///    summaryFields.Add("chkABNForm", 3); 
        ///    summaryFields.Add("DispenseQueueID", 5); 
        ///    GridViewExtensions.fieldNames = summaryFields; 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        public static TextBox GetTextBox(this System.Web.UI.WebControls.TableRow row, string controlName)
        {
            TextBox ret = null;
            try
            {
                ret = GetControl<TextBox>(row, controlName);
            }
            catch { }
            return ret;
        }

        /// <summary>
        /// Be sure to set the internal dictionary "fieldNames" before calling, ie 
        ///    private Dictionary&lt;string, int&gt; summaryFields = new Dictionary&lt;string, int&gt;(); 
        ///    summaryFields.Add("ClientSelectColumn", 0); 
        ///    summaryFields.Add("chkIsMedicare", 2); 
        ///    summaryFields.Add("chkABNForm", 3); 
        ///    summaryFields.Add("DispenseQueueID", 5); 
        ///    GridViewExtensions.fieldNames = summaryFields; 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        public static Decimal GetRadNumericTextBoxDecimal(this System.Web.UI.WebControls.TableRow row, string controlName)
        {
            decimal ret = decimal.Zero;
            try
            {
                double? text;

                // try looking for the newer ASP.NET AJAX control first
                // if not found, then look for the classic ASP.NET control
                var textBox = GetControl<Telerik.Web.UI.RadNumericTextBox>(row, controlName);
                if (textBox != null)
                {
                    text = textBox.Value;
                }
                else
                {
                    var textBoxAlt = GetControl<Telerik.WebControls.RadNumericTextBox>(row, controlName);
                    text = textBoxAlt.Value;
                }

                ret = Convert.ToDecimal(text);
            }
            catch { }
            return ret;
        }

        public static Int32 GetRadNumericTextBoxInt32(this System.Web.UI.WebControls.TableRow row, string controlName)
        {
            try
            {
                var textBox = GetControl<Telerik.Web.UI.RadNumericTextBox>(row, controlName);
                if (textBox != null && textBox.Value.HasValue)
                    return Convert.ToInt32(textBox.Value.Value);
                else
                    return 0;
            }
            catch
            {
                return 0;
            }
        }

        public static Decimal GetTextBoxDecimal(this System.Web.UI.WebControls.TableRow row, string controlName)
        {
            decimal ret = decimal.Zero;
            try
            {
                string text = null;

                // try looking for the newer ASP.NET AJAX control first
                // if not found, then look for the classic ASP.NET control
                var textBox = GetControl<TextBox>(row, controlName);
                if (textBox != null)
                {
                    text = textBox.Text;
                }

                ret = Decimal.Parse(text);
            }
            catch { }
            return ret;
        }

        public static Decimal GetTextBoxDecimal(this Telerik.Web.UI.GridDataItem row, string controlName)
        {
            try
            {
                return ConvertFromString<Decimal>(((TextBox)row.FindControl(controlName)).Text);
            }
            catch
            {
                return default(Decimal);
            }
        }

        public static Int32 GetTextBoxInt32(this Telerik.Web.UI.GridDataItem row, string controlName)
        {
            try
            {
                return Convert.ToInt32(((TextBox)row.FindControl(controlName)).Text);
            }
            catch
            {
                return default(Int32);
            }
        }

        public static string GetTextBoxString(this Telerik.Web.UI.GridDataItem row, string controlName)
        {
            try
            {
                return ((TextBox)row.FindControl(controlName)).Text;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Be sure to set the internal dictionary "fieldNames" before calling, ie 
        ///    private Dictionary&lt;string, int&gt; summaryFields = new Dictionary&lt;string, int&gt;(); 
        ///    summaryFields.Add("ClientSelectColumn", 0); 
        ///    summaryFields.Add("chkIsMedicare", 2); 
        ///    summaryFields.Add("chkABNForm", 3); 
        ///    summaryFields.Add("DispenseQueueID", 5); 
        ///    GridViewExtensions.fieldNames = summaryFields; 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        public static Telerik.Web.UI.RadNumericTextBox GetRadNumericTextBox(this System.Web.UI.WebControls.TableRow row, string controlName)
        {
            Telerik.Web.UI.RadNumericTextBox ret = null;
            try
            {
                ret = GetControl<Telerik.Web.UI.RadNumericTextBox>(row, controlName);
            }
            catch { }
            return ret;

        }

        /// <summary>
        /// Be sure to set the internal dictionary "fieldNames" before calling, ie 
        ///    private Dictionary&lt;string, int&gt; summaryFields = new Dictionary&lt;string, int&gt;(); 
        ///    summaryFields.Add("ClientSelectColumn", 0); 
        ///    summaryFields.Add("chkIsMedicare", 2); 
        ///    summaryFields.Add("chkABNForm", 3); 
        ///    summaryFields.Add("DispenseQueueID", 5); 
        ///    GridViewExtensions.fieldNames = summaryFields; 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        public static string GetRadComboBoxText(this System.Web.UI.WebControls.TableRow row, string controlName)
        {
            string ret = string.Empty;
            try
            {
                ret = GetColumnProperty<string>(row, controlName, "Text");
            }
            catch { }
            return ret;
        }

        public static string GetRadComboBoxText(this Telerik.Web.UI.GridDataItem row, string controlName)
        {
            try
            {
                return ((Telerik.Web.UI.RadComboBox)row.FindControl(controlName)).Text;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Be sure to set the internal dictionary "fieldNames" before calling, ie 
        ///    private Dictionary&lt;string, int&gt; summaryFields = new Dictionary&lt;string, int&gt;(); 
        ///    summaryFields.Add("ClientSelectColumn", 0); 
        ///    summaryFields.Add("chkIsMedicare", 2); 
        ///    summaryFields.Add("chkABNForm", 3); 
        ///    summaryFields.Add("DispenseQueueID", 5); 
        ///    GridViewExtensions.fieldNames = summaryFields; 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        public static Telerik.Web.UI.RadComboBox GetRadComboBox(this System.Web.UI.WebControls.TableRow row, string controlName)
        {

            Telerik.Web.UI.RadComboBox ret = null;
            try
            {
                ret = GetControl<Telerik.Web.UI.RadComboBox>(row, controlName);
            }
            catch { }
            return ret;
        }


        /// <summary>
        /// Be sure to set the internal dictionary "fieldNames" before calling, ie 
        ///    private Dictionary&lt;string, int&gt; summaryFields = new Dictionary&lt;string, int&gt;(); 
        ///    summaryFields.Add("ClientSelectColumn", 0); 
        ///    summaryFields.Add("chkIsMedicare", 2); 
        ///    summaryFields.Add("chkABNForm", 3); 
        ///    summaryFields.Add("DispenseQueueID", 5); 
        ///    GridViewExtensions.fieldNames = summaryFields; 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        public static CheckBox GetCheckBox(this System.Web.UI.WebControls.TableRow row, string controlName)
        {
            CheckBox ret = null;
            try
            {
                ret = GetControl<CheckBox>(row, controlName);
            }
            catch { }
            return ret;
        }


        /// <summary>
        /// Be sure to set the internal dictionary "fieldNames" before calling, ie 
        ///    private Dictionary&lt;string, int&gt; summaryFields = new Dictionary&lt;string, int&gt;(); 
        ///    summaryFields.Add("ClientSelectColumn", 0); 
        ///    summaryFields.Add("chkIsMedicare", 2); 
        ///    summaryFields.Add("chkABNForm", 3); 
        ///    summaryFields.Add("DispenseQueueID", 5); 
        ///    GridViewExtensions.fieldNames = summaryFields; 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        public static DateTime? GetRadDatePickerDateTime(this System.Web.UI.WebControls.TableRow row, string controlName)
        {
            DateTime? ret = null;
            try
            {
                ret = GetColumnProperty<DateTime?>(row, controlName, "SelectedDate");
            }
            catch { }
            return ret;
        }

        public static DateTime? GetRadDatePickerDateTime(this Telerik.Web.UI.GridDataItem row, string controlName)
        {
            try
            {
                return ((Telerik.Web.UI.RadDatePicker)row.FindControl(controlName)).DateInput.SelectedDate;
            }
            catch
            {
                return null;
            }
        }


        /// <summary>
        /// Be sure to set the internal dictionary "fieldNames" before calling, ie 
        ///    private Dictionary&lt;string, int&gt; summaryFields = new Dictionary&lt;string, int&gt;(); 
        ///    summaryFields.Add("ClientSelectColumn", 0); 
        ///    summaryFields.Add("chkIsMedicare", 2); 
        ///    summaryFields.Add("chkABNForm", 3); 
        ///    summaryFields.Add("DispenseQueueID", 5); 
        ///    GridViewExtensions.fieldNames = summaryFields; 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        public static Telerik.Web.UI.RadDatePicker GetRadDatePicker(this System.Web.UI.WebControls.TableRow row, string controlName)
        {
            Telerik.Web.UI.RadDatePicker ret = null;
            try
            {
                ret = GetControl<Telerik.Web.UI.RadDatePicker>(row, controlName);
            }
            catch { }
            return ret;
        }


        public static int GetSafeIntFromLabel(this System.Web.UI.WebControls.TableCell cell, string controlName)
        {
            int ret = 0;
            try
            {
                string txt = GetColumnProperty<string>(cell, controlName, "Text");
                ret = ConvertFromString<Int32>(txt);
            }
            catch { }
            return ret;
        }

        public static string GetSafeStringFromLabel(this System.Web.UI.WebControls.TableCell cell, string controlName)
        {
            string ret = string.Empty;
            try
            {
                ret = GetColumnProperty<string>(cell, controlName, "Text");
            }
            catch { }
            return ret;
        }


        /// <summary>
        /// Be sure to set the internal dictionary "fieldNames" before calling, ie 
        ///    private Dictionary&lt;string, int&gt; summaryFields = new Dictionary&lt;string, int&gt;(); 
        ///    summaryFields.Add("ClientSelectColumn", 0); 
        ///    summaryFields.Add("chkIsMedicare", 2); 
        ///    summaryFields.Add("chkABNForm", 3); 
        ///    summaryFields.Add("DispenseQueueID", 5); 
        ///    GridViewExtensions.fieldNames = summaryFields; 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        public static Int32 GetDropDownListValueInt32(this System.Web.UI.WebControls.TableRow row, string controlName)
        {
            int ret = 0;
            try
            {
                string value = GetDropDownList(row, controlName, fieldNames[controlName]).SelectedValue;
                ret = ConvertFromString<Int32>(value);
            }
            catch { }
            return ret;
        }

        public static Int32 GetDropDownListValueInt32(this Telerik.Web.UI.GridDataItem row, string controlName)
        {
            try
            {
                return ConvertFromString<Int32>(((DropDownList)row.FindControl(controlName)).SelectedValue);
            }
            catch
            {
                return default(Int32);
            }
        }

        /// <summary>
        /// Be sure to set the internal dictionary "fieldNames" before calling, ie 
        ///    private Dictionary&lt;string, int&gt; summaryFields = new Dictionary&lt;string, int&gt;(); 
        ///    summaryFields.Add("ClientSelectColumn", 0); 
        ///    summaryFields.Add("chkIsMedicare", 2); 
        ///    summaryFields.Add("chkABNForm", 3); 
        ///    summaryFields.Add("DispenseQueueID", 5); 
        ///    GridViewExtensions.fieldNames = summaryFields; 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        public static string GetDropDownListValue(this System.Web.UI.WebControls.TableRow row, string controlName)
        {
            string ret = string.Empty;
            try
            {
                ret = GetDropDownList(row, controlName, fieldNames[controlName]).SelectedValue;
            }
            catch { }
            return ret;
        }

        public static string GetDropDownListValueString(this Telerik.Web.UI.GridDataItem row, string controlName)
        {
            try
            {
                return ((DropDownList)row.FindControl(controlName)).SelectedValue;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Be sure to set the internal dictionary "fieldNames" before calling, ie 
        ///    private Dictionary&lt;string, int&gt; summaryFields = new Dictionary&lt;string, int&gt;(); 
        ///    summaryFields.Add("ClientSelectColumn", 0); 
        ///    summaryFields.Add("chkIsMedicare", 2); 
        ///    summaryFields.Add("chkABNForm", 3); 
        ///    summaryFields.Add("DispenseQueueID", 5); 
        ///    GridViewExtensions.fieldNames = summaryFields; 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        public static DropDownList GetDropDownList(this System.Web.UI.WebControls.TableRow row, string controlName)
        {
            DropDownList ret = null;
            try
            {
                ret = GetControl<DropDownList>(row, controlName);
            }
            catch { }
            return ret;
        }

        public static DropDownList GetDropDownList(this System.Web.UI.WebControls.TableRow row, string controlName, int cellNumber)
        {
            DropDownList ret = null;
            try
            {
                ret = GetControl<DropDownList>(row, controlName, cellNumber);
            }
            catch { }
            return ret;
        }
    }
}
