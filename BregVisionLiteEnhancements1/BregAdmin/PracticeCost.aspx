﻿ <%@ Page Title="" Language="C#" MasterPageFile="~/BregAdmin/MasterBregAdmin.Master" AutoEventWireup="true" CodeBehind="PracticeCost.aspx.cs" Inherits="BregVision.BregAdmin.PracticeCost" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="text-align: center">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr style="height: 35px" valign="top">
                <td align="center">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr align="center">
                                <td style="width: 45%">&nbsp;</td>
                                <td>
                                    <a href="BregAdmin.aspx" class="managemenulink">Back</a><br />
                                </td>
                                <td style="width: 45%">&nbsp;</td>
                            </tr>
                        </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table cellpadding="0" cellspacing="0" width="60%">
                        <tr>
                            <td  class="AdminPageTitle">
                                <a name="OrderAdminTopPage">Practice Cost</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td><br />
                    <asp:Label ID="lblPracticeName" runat="server"></asp:Label><br /><br />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table cellpadding="0" cellspacing="3" width="350px" border="0">
                        <tr>
                            <td>
                                <asp:RadioButton ID="rbPracticeCatalogProduct" runat="server" GroupName="gpSelectProduct" CssClass="TextMed" Checked="true" Text="Practice Catalog Product"/><br />
                                <asp:RadioButton ID="rdThirdPartyProduct" runat="server" GroupName="gpSelectProduct" CssClass="TextMed" Text="Third Party Product"/><br />
                                <asp:RadioButton ID="rdCrystal" runat="server" GroupName="gpSelectProduct" CssClass="TextMed" Text ="Crystal Product" /><br /><br /><br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:FileUpload ID="FileUpload1" runat="server" />
                                <asp:Button ID="btnUpload" runat="server" Text="Upload file"  
                                    onclick="btnUpload_Click" /><br /><br />
                                <asp:Label ID="lblUploadStatus" runat="server" CssClass="WarningMsgMed"></asp:Label><br /><br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="gvUpdateInfo" runat="server" CellPadding="4" 
                                    ForeColor="#333333" GridLines="None" CssClass="FieldLabel" 
                                    AutoGenerateColumns="true" Width="575px" 
                                    onrowdatabound="gvUpdateInfo_RowDataBound">
                                    <AlternatingRowStyle BackColor="White" />                               
                                    <EditRowStyle BackColor="#2461BF" />
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" HorizontalAlign ="Center"/>
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#EFF3FB" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
