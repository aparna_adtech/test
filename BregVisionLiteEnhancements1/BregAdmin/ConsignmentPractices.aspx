﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConsignmentPractices.aspx.cs" Inherits="BregVision.BregAdmin.ConsignmentPractices" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link id="Link1" runat="server" rel="Stylesheet" type="text/css" href="~/Admin/Styles/AdminMasterStyle.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="center">
                    <table cellpadding="0" cellspacing="0" style="width: auto; border: 1px solid gray" >
                        <!-- Content Header-->
                        <tr class="AdminPageTitle">
                            <td valign="middle">
                                <asp:Label ID="lblContentHeader" runat="server" Text="Breg Vision Practices"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="grdConsignmentPractices" runat="server" CellPadding="4" 
                                    ForeColor="#333333" GridLines="Both" AutoGenerateColumns="false" DataKeyNames="PracticeID"
                                    BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" 
                                    CssClass="FieldLabel" onrowcommand="grdConsignmentPractices_RowCommand" 
                                    onrowdatabound="grdConsignmentPractices_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Practice Name">
                                            <EditItemTemplate>
                                                    <asp:TextBox ID="txtPracticeName" runat="server" CssClass="FieldLabel" Width="270px" Text='<%# Bind("practiceName") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPracticeNameEditMode" runat="server" Text='<%# Bind("practiceName") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblPracticeNameHeader" runat="server" Text="Practice Name"></asp:Label>
                                                </HeaderTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtNewPracticeName" runat="server" Visible="False"></asp:TextBox>
                                                </FooterTemplate>
                                                <ItemStyle Wrap="False" />
                                            </asp:TemplateField>          
                                                        
                                            <asp:BoundField DataField="PracticeID" Visible="False" ReadOnly="True" />

                                            <asp:TemplateField HeaderText="Practice Location" ItemStyle-VerticalAlign="Top" ItemStyle-BorderWidth="2">
                                                <ItemTemplate>
                                                    <asp:GridView ID="grdPracticeLocation" runat="server" AutoGenerateColumns="false" Width="300">
                                                        <Columns>
                                                           <asp:BoundField DataField="PracticeLocationID" Visible="false" />
                                                           <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate> 
                                                                    <asp:HyperLink ID="hypPracticeLocation" runat="server" NavigateUrl='<%# Bind("Hyperlink") %>' Text='<%# Bind("Name") %>'></asp:HyperLink>                                                                 
                                                                    
                                                                </ItemTemplate>
                                                           </asp:TemplateField>
                                    
                                    
                                                        </Columns>
                                                    </asp:GridView>
                                                </ItemTemplate>
                                            
                                            </asp:TemplateField>
                                            <%--<asp:ButtonField ButtonType="Image" ImageUrl="~/RadControls/Grid/Skins/Outlook/file.gif"
                                                CommandName="SetConsignmentReps" Text="Consignment" HeaderText="Consignment Reps"/>--%>
                                    </Columns>                                    
                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                    <RowStyle ForeColor="#000066" Wrap="False" />
                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                    <HeaderStyle CssClass="AdminSubTitle"/>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
