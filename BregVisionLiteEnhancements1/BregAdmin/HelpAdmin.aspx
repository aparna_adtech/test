﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BregAdmin/MasterBregAdmin.Master" AutoEventWireup="true" CodeBehind="HelpAdmin.aspx.cs" Inherits="BregVision.BregAdmin.HelpAdmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        <Scripts>
            <asp:ScriptReference Path="~/Include/HelpSystem.js" />
            <asp:ScriptReference Path="~/Include/Silverlight.js" />
            <asp:ScriptReference Path="~/FusionCharts/FusionCharts.js" />
            <asp:ScriptReference Path="~/Include/jquery/js/jquery-1.4.4.min.js" />
            <%--<asp:ScriptReference Path="~/Include/jquery/js/jquery-ui-1.8.9.custom.min.js" />--%>
            <%--<asp:ScriptReference Path="http://jquery-ui.googlecode.com/svn/tags/1.6rc2/ui/ui.core.js" />
            <asp:ScriptReference Path="http://jquery-ui.googlecode.com/svn/tags/1.6rc2/ui/ui.spinner.js" />--%>
            <asp:ScriptReference Path="~/Include/jquery-ui/ui/jquery.ui.core.js" />
            <asp:ScriptReference Path="~/Include/jquery-ui/ui/jquery.ui.widget.js" />
            <asp:ScriptReference Path="~/Include/jquery-ui/ui/jquery.ui.button.js" />
            <asp:ScriptReference Path="~/Include/jquery-ui/ui/jquery.ui.spinner.js" />
        </Scripts>
        <Services>
        </Services>
    </telerik:RadScriptManager>
<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        $(function () {
            $telerik.isIE7 = false;
            $telerik.isIE6 = false;
        });
    </script>
</telerik:RadScriptBlock>
    <telerik:RadToolTipManager ID="RadToolTipManager1" Width="300px" Height="100px" RelativeTo="Element"
        Position="BottomRight" runat="server" AnimationDuration="200">
        <WebServiceSettings Path="~/HelpSystem/ToolTipWebService.asmx" Method="GetToolTip" />
    </telerik:RadToolTipManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    </telerik:RadAjaxManager>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" InitialDelayTime="500" EnableAjaxSkinRendering="True" BackgroundPosition="Center" Skin="Breg" EnableEmbeddedSkins="False">
    <img src="App_Themes/Breg/images/Common/loading.gif" />
  </telerik:RadAjaxLoadingPanel>
<telerik:RadInputManager ID="RadInputManager1" runat="server" Skin="Office2007">
    <telerik:NumericTextBoxSetting BehaviorID="numberSetting" Culture="en-US" DecimalDigits="0"
        DecimalSeparator="." GroupSeparator="," GroupSizes="3" MaxValue="999" MinValue="0"
        NegativePattern="-n" PositivePattern="n" SelectionOnFocus="SelectAll">
    </telerik:NumericTextBoxSetting>
</telerik:RadInputManager>

    <div>
        <bvu:VisionRadGridAjax ID="grdHelp" runat="server"  EnableLinqExpressions="false"
                            AllowMultiRowSelection="True"
                            
                            
                            
                            OnNeedDataSource="grdHelp_NeedDataSource" 
                            
                            OnUpdateCommand="grdHelp_UpdateCommand" PageSize="100" 
                            >
            <ClientSettings AllowDragToGroup="false" AllowColumnsReorder="false" ReorderColumnsOnClient="false">
            <Selecting AllowRowSelect="True"></Selecting>
            </ClientSettings>

            <GroupPanel Visible="True" Width="100%"></GroupPanel>

             <MasterTableView CommandItemDisplay="Top" CommandItemSettings-AddNewRecordText="New Help" DataKeyNames="TreeID">
                <CommandItemSettings AddNewRecordText="New Help" ExportToPdfText="Export to Pdf"></CommandItemSettings>

                <RowIndicatorColumn>
                    <HeaderStyle Width="20px"></HeaderStyle>
                </RowIndicatorColumn>

                <ExpandCollapseColumn>
                <HeaderStyle BorderColor="#A2B6CB" BorderWidth="1px" BorderStyle="Solid" Width="20px"></HeaderStyle>
                </ExpandCollapseColumn>

                <Columns>
                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" 
                        HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid" 
                        HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center" 
                        HeaderStyle-Width="50px" HeaderText="Edit" 
                        ImageUrl="~/App_Themes/Breg/images/Common/edit.png" 
                        ItemStyle-HorizontalAlign="Center" Text="Edit" Visible="true">
                        <HeaderStyle HorizontalAlign="Center" BorderColor="#A2B6CB" BorderWidth="1px" BorderStyle="Solid" Width="50px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </telerik:GridButtonColumn>
                    <telerik:GridBoundColumn DataField="GroupID" UniqueName="GroupID" Visible="False" />
                    <telerik:GridBoundColumn DataField="EntityTypeID" UniqueName="EntityTypeID" Visible="False" />
                    <telerik:GridBoundColumn DataField="ArticleID" UniqueName="ArticleID" Visible="False" />
                    <telerik:GridBoundColumn DataField="TreeID" UniqueName="TreeID" />
                    
                    <telerik:GridBoundColumn DataField="GroupTitle" HeaderText="GroupTitle" UniqueName="GroupTitle" ItemStyle-HorizontalAlign="Left" />
                    <telerik:GridBoundColumn DataField="TypeName" HeaderText="TypeName" UniqueName="TypeName" ItemStyle-HorizontalAlign="Left" />
                    <telerik:GridBoundColumn DataField="ArticleTitle" HeaderText="ArticleTitle" UniqueName="ArticleTitle" ItemStyle-HorizontalAlign="Left" />
                    <telerik:GridBoundColumn DataField="ArticleText" HeaderText="ArticleText" UniqueName="ArticleText" ItemStyle-HorizontalAlign="Left" />
                </Columns>
                <EditFormSettings EditFormType="Template">
                    <EditColumn CancelText="Cancel" UniqueName="EditCommandColumn1" 
                        UpdateText="Update">
                    </EditColumn>
                    <FormTemplate>
                        <table border="1" cellpadding="1" cellspacing="2" rules="none" 
                            style="border-collapse: collapse" width="800">
                            <tr class="EditFormHeader">
                                <td colspan="2" align="center">
                                    <b>Edit Help</b><br />
                                    <b>
                                    <asp:Label ID="lblStatus" runat="server" Font-Bold="false" Font-Names="tahoma" 
                                        Font-Size="8pt" Style="color: Red"></asp:Label>
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    Article Title:
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtArticleTitle" runat="server" TabIndex="2" 
                                        Text='<%# Bind( "ArticleTitle") %>' Width="400"> </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    Article Title:
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtArticleText" runat="server" TabIndex="2" 
                                        Text='<%# Bind( "ArticleText") %>' Width="400"> </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" colspan="2">
                                    <asp:Button ID="btnUpdate" runat="server" CommandName="Update" Text="Update" />
                                    &nbsp;
                                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" 
                                        CommandName="Cancel" Text="Cancel" />
                                </td>
                            </tr>
                        </table>
                    </FormTemplate>
                </EditFormSettings>

            </MasterTableView>

        </bvu:VisionRadGridAjax>
    </div>
</asp:Content>
