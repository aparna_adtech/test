﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using SpreadsheetLight;
using ClassLibrary.DAL.MasterCatalog;
using ClassLibrary.DAL;

namespace BregVision.BregAdmin
{
    public partial class WholesalePrice1 : System.Web.UI.Page
    {
        List<String> itemsList = new List<string>();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (this.FileUpload1.HasFile)
            {
                //this.FileUpload1.SaveAs("c:\\" + this.FileUpload1.FileName);
                SaveFile(FileUpload1.PostedFile);
            }
            else
            {
                lblUploadStatus.Text = "You did not specify a file to upload";
            }
        }

        private void SaveFile(HttpPostedFile file)
        {
            string path = Server.MapPath("~/MasterCostList");
            if (!System.IO.Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string sDate = DateTime.Now.ToString("yyyyMMddHHmmss");

            string savePath = path; //"c:\\" + "test\\";
            string fileName = sDate + FileUpload1.FileName;
            string filePath = Path.Combine( savePath, fileName);

            if (System.IO.File.Exists(filePath))
            {
                lblUploadStatus.Text = "A file with the same name already exists.";
                return;
            }

            FileUpload1.SaveAs(filePath);
            lblUploadStatus.Text = "Your file was uploaded successfully.";

            ProcessPriceList(filePath);
        }

        private List<MasterCatalogPriceUpdate> masterCatalogPriceUpdates = new List<MasterCatalogPriceUpdate>(); 
        private void ProcessPriceList(string filePath)
        {
            //col1 = itemnumber, col4 is price
            SLDocument slDocument = new SLDocument(filePath);
            slDocument.ApplyNamedCellStyleToColumn(1, SLNamedCellStyleValues.Title);
            Dictionary<SLCellPoint, SLCell> cells = slDocument.GetCells();

            Dictionary<SLCellPoint, SLCell>.KeyCollection keyCollection = cells.Keys;
            int rowMax = (from k in keyCollection
                         select k.RowIndex).Max();

            int rowMin = (from k in keyCollection
                          select k.RowIndex).Min();

            for (int i = rowMin; i <= rowMax; i++)
            {
                SLCellPoint itemKey = new SLCellPoint(i, 1);
                SLCellPoint priceKey = new SLCellPoint(i, 4);
                SLCellPoint packagingKey = new SLCellPoint(i, 3);

                if (keyCollection.Contains(itemKey) && keyCollection.Contains(priceKey))
                {
                    SLCell itemNumberCell = cells[itemKey];
                    SLCell priceCell = cells[priceKey];
                    SLCell packagingCell = cells[packagingKey];

                    var price = priceCell.NumericValue;
                    // 
                    // Item number or packaging which is string should be trimmed.

                    if(Convert.ToDecimal(price) > 0)
                    {
                        string itemNumber = itemNumberCell.CellText;
                        string packaging = packagingCell.CellText;

                        if (itemNumberCell.DataType == CellValues.SharedString && packagingCell.DataType == CellValues.SharedString)
                        {
                            List<SLRstType> sharedStrings = slDocument.GetSharedStrings();
                            itemNumber = sharedStrings.ElementAt(Convert.ToInt32(itemNumberCell.NumericValue)).GetText();
                            packaging = sharedStrings.ElementAt(Convert.ToInt32(packagingCell.NumericValue)).GetText();
                            masterCatalogPriceUpdates.Add(new MasterCatalogPriceUpdate(itemNumber, Convert.ToDecimal(price), packaging));
                        }
                    }
                }
            }

             UpdateMasterCatalog();

            slDocument.CloseWithoutSaving();
            slDocument.Dispose();
        }

        private void UpdateMasterCatalog()
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                var query = from mcp in visionDB.MasterCatalogProducts
                            select mcp;
                int i = 0;

                foreach (MasterCatalogPriceUpdate priceUpdate in masterCatalogPriceUpdates) 
                {
                    //MasterCatalogProduct masterCatalogProduct = query.Where(c => c.Code == priceUpdate.Code && c.Packaging == priceUpdate.Packaging).FirstOrDefault();
                    List<MasterCatalogProduct> masterCatalogProductList = query.Where(c => c.Code == priceUpdate.Code && c.Packaging == priceUpdate.Packaging).ToList<MasterCatalogProduct>();
                    if (masterCatalogProductList != null)
                    {
                        foreach (MasterCatalogProduct masterCatalogProduct in masterCatalogProductList) 
                        {
                            string oldPrice = Convert.ToDouble(masterCatalogProduct.WholesaleListCost).ToString("F2");
                            string newPrice = Convert.ToDouble(priceUpdate.WholesaleListCost).ToString("F2");
                            
                            masterCatalogProduct.WholesaleListCost = priceUpdate.WholesaleListCost;
                            i = i + 1;
                            itemsList.Add(masterCatalogProduct.Code + "  " + masterCatalogProduct.Name + " -- " + "Old Price: " + oldPrice + "   New Price: " + newPrice);
                        }
                    }
                }

                gvUpdateInfo.DataSource = itemsList;
                gvUpdateInfo.DataBind();

                if (i == 0)
                {
                    lblUploadStatus.Text = "No Items in spreadsheet match practice codes.";
                }
                else
                {
                    lblUploadStatus.Text = i + " items updated successfully.";
                }

                visionDB.SubmitChanges();
            }
        }

        protected void gvUpdateInfo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Text = "Update Result";
            }
        }
    }

   
}