﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using BregVision.BlobService;
using ClassLibrary.DAL;
using PdfSharp.Pdf.IO;

namespace BregVision.BregAdmin
{
    public partial class DispenseReceiptCustomFormPage : System.Web.UI.Page
    {
        private readonly VisionDataContext _db =
            new VisionDataContext(
                System.Configuration.ConfigurationManager.ConnectionStrings["BregVisionConnectionString"].ToString());

        private int PracticeId => Convert.ToInt32(Request.QueryString["pid"]);
        public int NumRows { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblError.Text = null;
            lblSuccess.Text = null;

            if (!IsPostBack)
                BindGridView();
        }

        protected void BindGridView()
        {
            var customFormsTable = _db.GetTable<DispenseReceiptCustomForm>();

            // Populate active custom forms table
            var activeCustomForms = from cf in customFormsTable where cf.PracticeId == PracticeId where cf.IsActive orderby cf.BlobOrder select cf;
            NumRows = activeCustomForms.Count();
            GridView1.DataSource = activeCustomForms;
            GridView1.DataBind();

            // Populate inactive custom forms table
            var inactiveCustomForms = from cf in customFormsTable where cf.PracticeId == PracticeId where !cf.IsActive orderby cf.BlobOrder select cf;
            GridView2.DataSource = inactiveCustomForms;
            GridView2.DataBind();
        }

        // Insert new row in database table
        protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            var txtName = (TextBox)GridView1.FooterRow.FindControl("txt_Name_insert");

            var customForm = new DispenseReceiptCustomForm { Name = txtName.Text };

            _db.DispenseReceiptCustomForms.InsertOnSubmit(customForm);
            _db.SubmitChanges();

            // Refresh Gridview for reflecting new row
            BindGridView();
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            BindGridView();

            if (!CustomFormUpload.HasFile)
            {
                lblError.Text = "No file selected.";
                return;
            }

            var extension = Path.GetExtension(CustomFormUpload.FileName);
            if (string.IsNullOrEmpty(extension) || extension.ToLower() != ".pdf")
            {
                lblError.Text = "Only PDF files are supported.";
                return;
            }

            try
            {
                PdfReader.Open(new MemoryStream(CustomFormUpload.FileBytes));
            }
            catch (PdfReaderException ex)
            {
                lblError.Text = ex.Message;
                return;
            }
            
            try
            {
                var path = PracticeId + "\\" + CustomFormUpload.FileName;
                var client = new BlobServiceClient();
                var success = client.UploadFile(CustomFormUpload.FileBytes, path);

                if (success)
                {
                    var newCustomForm = new DispenseReceiptCustomForm
                    {
                        Name = CustomFormUpload.FileName,
                        PracticeId = PracticeId,
                        BlobOrder = NumRows + 1,
                        IsActive = true
                    };
                    _db.DispenseReceiptCustomForms.InsertOnSubmit(newCustomForm);
                    _db.SubmitChanges();

                    lblSuccess.Text = "File uploaded.";
                }
                else
                {
                    lblError.Text = "File upload failed.";
                }
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }   
            
            BindGridView();
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            // Adjust blob order
            if (e.CommandName.Equals("MoveUp"))
            {
                ReorderRows(e, -1);
            }
            else if (e.CommandName.Equals("MoveDown"))
            {
                ReorderRows(e, 1);
            }
            // Move row from active table to inactive table
            else if (e.CommandName.Equals("Deactivate"))
            {
                var selectedFormId = Convert.ToInt32(e.CommandArgument);
                // Select specific row from database table
                DispenseReceiptCustomForm customForm = (from i in _db.DispenseReceiptCustomForms
                                                        where i.Id == selectedFormId
                                                        select i).First();
                var removedBlobOrder = customForm.BlobOrder;
                customForm.IsActive = false;
                customForm.BlobOrder = 0;

                var customFormsTable = _db.GetTable<DispenseReceiptCustomForm>();
                var activeCustomForms = (from cf in customFormsTable
                                         where cf.PracticeId == PracticeId
                                         where cf.IsActive
                                         where cf.BlobOrder > removedBlobOrder
                                         orderby cf.BlobOrder
                                         select cf).ToArray().ToList();
                // Adjust blob order for any forms lower than the removed form
                if (activeCustomForms.Count > 0)
                {
                    for (var i = removedBlobOrder; i < activeCustomForms.Count; i++)
                    {
                        activeCustomForms[i].BlobOrder = i;
                    }
                }

                _db.SubmitChanges();
                BindGridView();
            }
        }

        protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Activate"))
            {
                // Move form from inactive table to active table
                BindGridView();
                var selectedFormId = Convert.ToInt32(e.CommandArgument);

                DispenseReceiptCustomForm customForm = (from i in _db.DispenseReceiptCustomForms
                                                        where i.Id == selectedFormId
                                                        select i).First();
                customForm.IsActive = true;
                customForm.BlobOrder = NumRows + 1;
                _db.SubmitChanges();

                BindGridView();
            }
            else if (e.CommandName.Equals("Remove"))
            {
                // Select specific row from database table
                var customForm = (from i in _db.DispenseReceiptCustomForms
                                  where i.Id == Convert.ToInt32(e.CommandArgument)
                                  select i).First();

                // Remove file from blob service
                var path = PracticeId + "\\" + customForm.Name;
                var client = new BlobServiceClient();
                var success = client.DeleteFile(path);

                // Delete row from database table
                if (success)
                {
                    _db.DispenseReceiptCustomForms.DeleteOnSubmit(customForm);
                    _db.SubmitChanges();
                }

                GridView2.EditIndex = -1;
                BindGridView();
            }
        }

        private void ReorderRows(GridViewCommandEventArgs e, int indexOffset)
        {
            // Adjust blob order
            var orderedCustomForms = new List<DispenseReceiptCustomForm>();
            var currentFormIndex = -1;

            var customForms =
                (from cf in _db.DispenseReceiptCustomForms
                 where cf.PracticeId == PracticeId
                 orderby cf.BlobOrder
                 select cf).ToList();
            var selectedFormId = Convert.ToInt32(e.CommandArgument);

            int sortOrder = 1;

            foreach (GridViewRow currentRow in GridView1.Rows)
            {
                var customFormId = Convert.ToInt32(GridView1.DataKeys[currentRow.RowIndex].Values[0].ToString());
                var currentCustomForm = customForms.FirstOrDefault(cf => cf.Id == customFormId);
                currentCustomForm.BlobOrder = sortOrder;
                orderedCustomForms.Add(currentCustomForm);

                if (customFormId == selectedFormId)
                    currentFormIndex = orderedCustomForms.Count - 1;

                sortOrder += 1;
            }

            if (currentFormIndex + indexOffset > -1 && currentFormIndex + indexOffset < orderedCustomForms.Count
                && !(currentFormIndex == 0 && indexOffset == -1 || currentFormIndex == customForms.Count && indexOffset == 1))
            {
                orderedCustomForms[currentFormIndex].BlobOrder += indexOffset;
                orderedCustomForms[currentFormIndex + indexOffset].BlobOrder -= indexOffset;
            }

            _db.SubmitChanges();

            BindGridView();
        }
    }
}