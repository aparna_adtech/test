﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConsignmentRepAdmin.aspx.cs" Inherits="BregVision.BregAdmin.ConsignmentRepAdmin" MasterPageFile="~/BregAdmin/MasterBregAdmin.Master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        <Scripts>
            <asp:ScriptReference Path="~/Include/HelpSystem.js" />
            <asp:ScriptReference Path="~/Include/Silverlight.js" />
            <asp:ScriptReference Path="~/FusionCharts/FusionCharts.js" />
            <asp:ScriptReference Path="~/Include/jquery/js/jquery-1.4.4.min.js" />
            <%--<asp:ScriptReference Path="~/Include/jquery/js/jquery-ui-1.8.9.custom.min.js" />--%>
            <%--<asp:ScriptReference Path="http://jquery-ui.googlecode.com/svn/tags/1.6rc2/ui/ui.core.js" />
            <asp:ScriptReference Path="http://jquery-ui.googlecode.com/svn/tags/1.6rc2/ui/ui.spinner.js" />--%>
            <asp:ScriptReference Path="~/Include/jquery-ui/ui/jquery.ui.core.js" />
            <asp:ScriptReference Path="~/Include/jquery-ui/ui/jquery.ui.widget.js" />
            <asp:ScriptReference Path="~/Include/jquery-ui/ui/jquery.ui.button.js" />
            <asp:ScriptReference Path="~/Include/jquery-ui/ui/jquery.ui.spinner.js" />
        </Scripts>
        <Services>
        </Services>
    </telerik:RadScriptManager>
<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        $(function () {
            $telerik.isIE7 = false;
            $telerik.isIE6 = false;
        });
    </script>
</telerik:RadScriptBlock>
    <telerik:RadToolTipManager ID="RadToolTipManager1" Width="300px" Height="100px" RelativeTo="Element"
        Position="BottomRight" runat="server" AnimationDuration="200">
        <WebServiceSettings Path="~/HelpSystem/ToolTipWebService.asmx" Method="GetToolTip" />
    </telerik:RadToolTipManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    </telerik:RadAjaxManager>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" InitialDelayTime="500" EnableAjaxSkinRendering="True" BackgroundPosition="Center" Skin="Breg" EnableEmbeddedSkins="False">
    <img src="App_Themes/Breg/images/Common/loading.gif" />
  </telerik:RadAjaxLoadingPanel>
<telerik:RadInputManager ID="RadInputManager1" runat="server" Skin="Office2007">
    <telerik:NumericTextBoxSetting BehaviorID="numberSetting" Culture="en-US" DecimalDigits="0"
        DecimalSeparator="." GroupSeparator="," GroupSizes="3" MaxValue="999" MinValue="0"
        NegativePattern="-n" PositivePattern="n" SelectionOnFocus="SelectAll">
    </telerik:NumericTextBoxSetting>
</telerik:RadInputManager>
    <div>
        <table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin-bottom: 20px;">
            <tr valign="middle" style="margin-bottom: 5px;">
                <td colspan="2">
                    <table>
                        <tr valign="middle">
                            <td>
                                <asp:Label ID="lblContentHeader" runat="server" Text="Consignment Rep Admin" CssClass="PageTitle"></asp:Label>
                            </td>                    
                        </tr>
                    </table>
                </td>

            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblStatus" runat="server" CssClass="WarningMsg"></asp:Label>
                </td>
            </tr>             
            <tr>
                <td colspan="2">
                    <bvu:VisionRadGridAjax ID="grdPracticeUser" runat="server"  EnableLinqExpressions="false"
                            AllowMultiRowSelection="True"
                            OnItemCommand="grdPracticeUser_OnItemCommand" 
                            OnItemDataBound="grdPracticeUser_OnItemDataBound" 
                            OnNeedDataSource="grdPracticeUser_NeedDataSource" 
                            OnPageIndexChanged="grdPracticeUser_PageIndexChanged" 
                            OnUpdateCommand="grdPracticeUser_UpdateCommand" PageSize="100" 
                            >
                            <ClientSettings AllowDragToGroup="false" AllowColumnsReorder="false" ReorderColumnsOnClient="false">
                            <Selecting AllowRowSelect="True"></Selecting>
                            </ClientSettings>

                            <GroupPanel Visible="True" Width="100%"></GroupPanel>

                            <MasterTableView CommandItemDisplay="Top" 
                                CommandItemSettings-AddNewRecordText="New User" DataKeyNames="UserName" 
                                >
                                <CommandItemSettings AddNewRecordText="New User" ExportToPdfText="Export to Pdf"></CommandItemSettings>

                                <RowIndicatorColumn>
                                <HeaderStyle Width="20px"></HeaderStyle>
                                </RowIndicatorColumn>

                                <ExpandCollapseColumn>
                                <HeaderStyle BorderColor="#A2B6CB" BorderWidth="1px" BorderStyle="Solid" Width="20px"></HeaderStyle>
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridClientSelectColumn HeaderStyle-BorderColor="#A2B6CB" 
                                        HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" 
                                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="35px" 
                                        ItemStyle-HorizontalAlign="Center" UniqueName="ClientSelectColumn">
                                        <HeaderStyle HorizontalAlign="Center" BorderColor="#A2B6CB" BorderWidth="1px" BorderStyle="Solid" Width="35px"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </telerik:GridClientSelectColumn>                                    
                                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Activate" 
                                        ConfirmText="Activate this user?" HeaderStyle-BorderColor="#A2B6CB" 
                                        HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" 
                                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" 
                                        HeaderText="Activate" ImageUrl="~/RadControls\Grid\Skins\Office2007\update.gif" 
                                        ItemStyle-HorizontalAlign="Center" Text="Activate" UniqueName="Activate">
                                        <HeaderStyle HorizontalAlign="Center" BorderColor="#A2B6CB" BorderWidth="1px" BorderStyle="Solid" Width="50px"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </telerik:GridButtonColumn>
                                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" 
                                        HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid" 
                                        HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center" 
                                        HeaderStyle-Width="50px" HeaderText="Edit" 
                                        ImageUrl="~/App_Themes/Breg/images/Common/edit.png" 
                                        ItemStyle-HorizontalAlign="Center" Text="Edit" Visible="true">
                                        <HeaderStyle HorizontalAlign="Center" BorderColor="#A2B6CB" BorderWidth="1px" BorderStyle="Solid" Width="50px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </telerik:GridButtonColumn>

                                    
                                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Unlock" UniqueName="Unlock" 
                                        HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid" 
                                        HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center" 
                                        HeaderStyle-Width="50px" HeaderText="Unlock" 
                                        ImageUrl="~/RadControls/Grid/Skins/Office2007/unlock.png" 
                                        ItemStyle-HorizontalAlign="Center" Text="Unlock" Visible="true">
                                        <HeaderStyle HorizontalAlign="Center" BorderColor="#A2B6CB" BorderWidth="1px" BorderStyle="Solid" Width="50px"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </telerik:GridButtonColumn>
                                    <telerik:GridBoundColumn DataField="UserName" HeaderStyle-BorderColor="#A2B6CB" 
                                        HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" 
                                        HeaderText="UserName" UniqueName="UserName">
                                        <HeaderStyle BorderColor="#A2B6CB" BorderWidth="1px" BorderStyle="Solid"></HeaderStyle>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Email" HeaderStyle-BorderColor="#A2B6CB" 
                                        HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" 
                                        HeaderText="Email" UniqueName="Email">
                                        <HeaderStyle BorderColor="#A2B6CB" BorderWidth="1px" BorderStyle="Solid"></HeaderStyle>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridEditCommandColumn InsertText="New User" 
                                        UniqueName="grdEditCommandColumn5" Visible="false">
                                    </telerik:GridEditCommandColumn>
                                    <telerik:GridBoundColumn DataField="AspNet_UserID" 
                                        HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid" 
                                        HeaderStyle-BorderWidth="1px" HeaderText="AspNet_UserID" 
                                        UniqueName="AspNet_UserID" Visible="false">
                                        <HeaderStyle BorderColor="#A2B6CB" BorderWidth="1px" BorderStyle="Solid"></HeaderStyle>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="IsActive" HeaderStyle-BorderColor="#A2B6CB" 
                                        HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" 
                                        HeaderText="IsActive" UniqueName="IsActive" Visible="false">
                                        <HeaderStyle BorderColor="#A2B6CB" BorderWidth="1px" BorderStyle="Solid"></HeaderStyle>
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <EditFormSettings EditFormType="Template">
                                    <EditColumn CancelText="Cancel" UniqueName="EditCommandColumn1" 
                                        UpdateText="Update">
                                    </EditColumn>
                                    <FormTemplate>
                                        <table border="1" cellpadding="1" cellspacing="2" rules="none" 
                                            style="border-collapse: collapse" width="800">
                                            <tr class="EditFormHeader">
                                                <td colspan="2" align="center">
                                                    <b>Edit User</b><br />
                                                    <b>
                                                    <asp:Label ID="lblStatus" runat="server" Font-Bold="false" Font-Names="tahoma" 
                                                        Font-Size="8pt" Style="color: Red"></asp:Label>
                                                    </b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    User Name:
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtUserName" runat="server" Enabled="false" TabIndex="1" 
                                                        Text='<%# Bind( "UserName") %>' Width="400"> </asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="txtUserNameValidator" runat="server" 
                                                        ControlToValidate="txtUserName" ErrorMessage="Please choose an User Name"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr ID="trOldPassword" runat="server" visible="false">
                                                <td align="left">
                                                    Old Password:
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblPasswordInstructions" runat="server" 
                                                        Text="To change the password, type a new one.  Leave blank to keep current." 
                                                        Visible="false"></asp:Label>
                                                    <br />
                                                    <asp:TextBox ID="txtOldPassword" runat="server" TabIndex="1" 
                                                        TextMode="Password" Width="400"> </asp:TextBox>
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Password:
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtPassword" runat="server" TabIndex="1" TextMode="Password" 
                                                        Width="400"> </asp:TextBox>
                                                    <br />
                                                    <asp:RequiredFieldValidator ID="txtPasswordValidator" runat="server" 
                                                        ControlToValidate="txtPassword" ErrorMessage="Please enter a password"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Confirm Password:
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtPasswordCompare" runat="server" TabIndex="1" 
                                                        TextMode="Password" Width="400"> </asp:TextBox>
                                                    <br />
                                                    <asp:RequiredFieldValidator ID="txtPasswordCompareValidator" runat="server" 
                                                        ControlToValidate="txtPasswordCompare" 
                                                        ErrorMessage="Please confirm your password"></asp:RequiredFieldValidator>
                                                    <br />
                                                    <asp:CompareValidator ID="PasswordValidator" runat="server" 
                                                        ControlToCompare="txtPassword" ControlToValidate="txtPasswordCompare" 
                                                        ErrorMessage="The password and compare password do not match"></asp:CompareValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Email:
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtEmail" runat="server" TabIndex="2" 
                                                        Text='<%# Bind( "Email") %>' Width="400"> </asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="validEmailRequired" runat="server" 
                                                        ControlToValidate="txtEmail" Display="Dynamic" 
                                                        ErrorMessage="Please enter an email address.">
                                                </asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="validEmailRegExp" runat="server" 
                                                        ControlToValidate="txtEmail" Display="Dynamic" 
                                                        ErrorMessage="Please enter a valid email address." 
                                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" colspan="2">
                                                    <asp:Button ID="btnUpdate" runat="server" CommandName="Update" Text="Update" />
                                                    &nbsp;
                                                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" 
                                                        CommandName="Cancel" Text="Cancel" />
                                                </td>
                                            </tr>
                                        </table>
                                    </FormTemplate>
                                </EditFormSettings>
                            </MasterTableView>
                            <ExportSettings>
                                <Pdf PageBottomMargin="" PageFooterMargin="" PageHeaderMargin="" 
                                    PageHeight="11in" PageLeftMargin="" PageRightMargin="" PageTopMargin="" 
                                    PageWidth="8.5in" />
                            </ExportSettings>

                            <PagerStyle Mode="NumericPages" PagerTextFormat="{4} | Displaying page {0} of {1}, items {2} to {3} of {5}"></PagerStyle>

                            <SelectedItemStyle BackColor="#EDCD01"></SelectedItemStyle>

                            <HeaderContextMenu EnableImageSprites="True" CssClass="GridContextMenu GridContextMenu_Web20"></HeaderContextMenu>
                        </bvu:VisionRadGridAjax>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
