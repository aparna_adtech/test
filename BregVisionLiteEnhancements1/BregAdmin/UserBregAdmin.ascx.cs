using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Telerik.WebControls;
using System.Collections.Generic;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

namespace BregVision.BregAdmin
{
    public partial class UserBregAdmin : System.Web.UI.UserControl
    {
        int userID;

        protected void Page_Load(object sender, EventArgs e)
        {


            if (!Page.IsPostBack)
            {
                GetSessionVariables(out userID);

                if (userID == -1)
                {
                    userID = GetUserID();
                    Session["UserID"] = userID;
                }
                //DisplaySessionVariable();
                //DisplayUserLoginInfo();
                //LoadPracticeLocations();
                //cbxChangeLocation.Visible = false;
            }
        }

        //  This may be unneccessary if put in the process login page.
        protected int GetUserID()
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_User_Select_UserID_By_LoginName");   //  usp_User_Select_UserID_Given_UserName

            db.AddInParameter(dbCommand, "UserLoginName", DbType.String, Context.User.Identity.Name.ToString());     // UserName
            db.AddOutParameter(dbCommand, "UserID", DbType.Int32, 4);
            db.ExecuteNonQuery(dbCommand);

            userID = ((db.GetParameterValue(dbCommand, "UserID") == DBNull.Value) ? -1 : Convert.ToInt32(db.GetParameterValue(dbCommand, "UserID")));

            return userID;
        }

        protected void GetSessionVariables( out int userID )
        {
            userID = (Session["UserID"] != null) ? Convert.ToInt32(Session["UserID"].ToString()) : -1;
        }
    }
}