﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Practice;

namespace BregVision.BregAdmin
{
    public partial class PracticeApplicationAgreementTermsAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetTopPracticeApplicationAgreementTerm();
            }
        }

        private void GetTopPracticeApplicationAgreementTerm()
        {
            ClassLibrary.DAL.PracticeApplicationAgreementTerm practiceApplicationAgreementTerm = PracticeApplicationAgreementTerm.GetTopPracticeApplicationAgreementTerm();
            if (practiceApplicationAgreementTerm != null)
            {
                txtContent.Text = practiceApplicationAgreementTerm.Terms;
                litPreview.Text = txtContent.Text;
                labStatus.Text = "Last PracticeApplicationAgreementTerm Loaded... ";
            }
            else
            {
                labStatus.Text = "Last PracticeApplicationAgreementTerm not found in database... ";
            }
            
        }

        protected void btnPreview_Click(Object sender, EventArgs e)
        {
            litPreview.Text = txtContent.Text;
            labStatus.Text = "Preview Loaded... ";
        }

        protected void btnAdd_Click(Object sender, EventArgs e)
        {
            ClassLibrary.DAL.PracticeApplicationAgreementTerm practiceApplicationAgreementTerm = new ClassLibrary.DAL.PracticeApplicationAgreementTerm();

            practiceApplicationAgreementTerm.Terms = txtContent.Text;
            practiceApplicationAgreementTerm.DateTime = DateTime.Now;

            PracticeApplicationAgreementTerm.SavePracticeApplicationAgreementTerm(practiceApplicationAgreementTerm);

            litPreview.Text = txtContent.Text;
            txtContent.Text = "";
            labStatus.Text = "New PracticeApplicationAgreementTerm Added to Database... ";

            btnAdd.Enabled = false;
        }

    }
}
