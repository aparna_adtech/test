﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary.DAL;

namespace BregVision.BregAdmin
{


    public partial class FeeLevelAdmin : System.Web.UI.Page
    {
        protected string _selectedLevelName = "Level1";
        protected IEnumerable<BregVisionMonthlyFeeLevel> feeLevels;
        protected bool _levelControlsBound = false;
        protected VisionDataContext visionDB = null;


        protected void LoadFeeSchedule()
        {
            visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                DataLoadOptions loadOptions = new DataLoadOptions();
                loadOptions.LoadWith<BregVisionMonthlyFeeLevel>(bregVisionMonthlyFeeLevel => bregVisionMonthlyFeeLevel.BregVisionMonthlyFees);
                visionDB.LoadOptions = loadOptions;

                feeLevels = (from fl in visionDB.BregVisionMonthlyFeeLevels
                            select fl).ToList();
                //VisionDataContext.CloseConnection(visionDB);
            }
        }

        protected BregVisionMonthlyFeeLevel GetCurrentLevel()
        {
            //Find the currently selected level
            BregVisionMonthlyFeeLevel selectedBregVisionMonthlyFeeLevel = feeLevels.Single<BregVisionMonthlyFeeLevel>(p => p.LevelName.Equals(_selectedLevelName));

            return selectedBregVisionMonthlyFeeLevel;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadFeeSchedule();
            //Make sure a level is current from the dropdown
            SetSelectedFeeLevel();

            lblMessage.Text = "";
            lblMessage.Visible = false;

            SetFeeLevelEditMode(false);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (MaxFeeCount() == false)
            {
                //Make an instance of a new monthly fee 
                BregVisionMonthlyFee bregVisionMonthlyFee = new BregVisionMonthlyFee();

                visionDB = VisionDataContext.GetVisionDataContext();
                using (visionDB)
                {
                    //Find the currently selected level
                    BregVisionMonthlyFeeLevel selectedBregVisionMonthlyFeeLevel = visionDB.BregVisionMonthlyFeeLevels.Single<BregVisionMonthlyFeeLevel>(p => p.LevelName.Equals(_selectedLevelName));



                    //Set the new fee instance's level id to the current level 
                    bregVisionMonthlyFee.BregVisionMonthlyFeeLevelID = selectedBregVisionMonthlyFeeLevel.BregVisionMonthlyFeeLevelID;

                    //Add the new instance to the fees
                    selectedBregVisionMonthlyFeeLevel.BregVisionMonthlyFees.Insert(0, bregVisionMonthlyFee);
                }
                gvFees.EditIndex = 0;
                BindLevelControls(bregVisionMonthlyFee);
            }
            else
            {
                lblMessage.Text = "Only 10 fee structures can be defined per level.";
                lblMessage.Visible = true;
            }
        }

        protected void gvFees_RowEditing(object sender, GridViewEditEventArgs e)
        {
            if (IsUsedByPractice() == false )
            {
                gvFees.EditIndex = e.NewEditIndex;
            }
            else
            {
                lblMessage.Text = "This Fee Level is already in use and cannot be edited.";
                lblMessage.Visible = true;
            }
        }

        protected bool MaxFeeCount()
        {
            //Find the currently selected level
            BregVisionMonthlyFeeLevel selectedBregVisionMonthlyFeeLevel = GetCurrentLevel();

            if (selectedBregVisionMonthlyFeeLevel.BregVisionMonthlyFees.Count() < 10)
            {
                return false;
            }
            return true;
        }

        protected bool IsUsedByPractice()
        {
            
            // i know the level name and I want to know if the level ID is used by any  practices
            visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                var usedCount = from l in visionDB.BregVisionMonthlyFeeLevels
                                where l.LevelName.Equals(_selectedLevelName)
                                select l.Practice_MonthlyFeeLevels.Count;
                int count = usedCount.First<int>();

                //VisionDataContext.CloseConnection(visionDB);

                return count > 0 ? true : false;
            }
            //return true;
        }

        protected void gvFees_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvFees.EditIndex = -1;
        }

        protected void gvFees_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                GridViewRow row = gvFees.Rows[e.RowIndex];
                if ((row.RowState == (DataControlRowState.Edit | DataControlRowState.Alternate)) || (row.RowState == DataControlRowState.Edit))
                {

                    //Find the currently selected level
                    BregVisionMonthlyFeeLevel selectedBregVisionMonthlyFeeLevel = visionDB.BregVisionMonthlyFeeLevels.Single<BregVisionMonthlyFeeLevel>(p => p.LevelName.Equals(_selectedLevelName));

                    BregVisionMonthlyFee bregVisionMonthlyFee = null;
                    bregVisionMonthlyFee = selectedBregVisionMonthlyFeeLevel.BregVisionMonthlyFees.SingleOrDefault<BregVisionMonthlyFee>(p => p.BregVisionMonthlyFeeID.Equals(row.Cells[1].GetSafeInt("txtBregVisionMonthlyFeeID")));
                    
                    if (bregVisionMonthlyFee == null)
                    {
                        bregVisionMonthlyFee = new BregVisionMonthlyFee();
                        //Add the new instance to the fees
                        selectedBregVisionMonthlyFeeLevel.BregVisionMonthlyFees.Insert(0, bregVisionMonthlyFee);
                    }
                    bregVisionMonthlyFee.BregVisionMonthlyFeeLevelID = row.Cells[0].GetSafeInt("txtBregVisionMonthlyFeeLevelID");
                    bregVisionMonthlyFee.MinTransactions = row.Cells[2].GetSafeInt("txtMinTransactions");
                    bregVisionMonthlyFee.MaxTransactions = row.Cells[3].GetSafeInt("txtMaxTransactions");
                    bregVisionMonthlyFee.MonthlyFee = row.Cells[4].GetSafeDecimal("txtMonthlyFee");
                    bregVisionMonthlyFee.ServiceCode = row.Cells[5].GetTextBoxValue("txtServiceCode");

                    visionDB.SubmitChanges();
                    //VisionDataContext.CloseConnection(visionDB);
                    gvFees.EditIndex = -1;

                }
            }
        }



        protected void ddlFeeLevel_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            SetSelectedFeeLevel();
        }

        protected void SetSelectedFeeLevel()
        {
            if (ddlFeeLevel.SelectedItem != null)
            {
                _selectedLevelName = ddlFeeLevel.SelectedItem.Text;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (_levelControlsBound != true)
            {
                BindLevelControls(null);
            }
        }

        protected void BindLevelControls(BregVisionMonthlyFee bregVisionMonthlyFee)
        {
            ddlFeeLevel.DataSource = feeLevels;
            ddlFeeLevel.DataTextField = "LevelName";
            ddlFeeLevel.DataValueField = "BregVisionMonthlyFeeLevelID";
            ddlFeeLevel.DataBind();
            ddlFeeLevel.SelectedIndex = ddlFeeLevel.Items.IndexOf(ddlFeeLevel.Items.FindByText(_selectedLevelName));

            visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                BregVisionMonthlyFeeLevel selectedBregVisionMonthlyFeeLevel = visionDB.BregVisionMonthlyFeeLevels.Single<BregVisionMonthlyFeeLevel>(p => p.LevelName.Equals(_selectedLevelName));

                if (bregVisionMonthlyFee != null)
                {
                    //Add the new instance to the fees
                    selectedBregVisionMonthlyFeeLevel.BregVisionMonthlyFees.Insert(0, bregVisionMonthlyFee);
                }

                gvFees.DataSource = selectedBregVisionMonthlyFeeLevel.BregVisionMonthlyFees.OrderBy(p => p.MinTransactions);
                gvFees.DataBind();
                _levelControlsBound = true;
            }
        }

        protected void btnAddFeeLevel_Click(object sender, EventArgs e)
        {
            SetFeeLevelEditMode(true);
        }

        protected void SetFeeLevelEditMode(bool EditMode)
        {
            ddlFeeLevel.Visible = !EditMode;
            btnAddFeeLevel.Visible = !EditMode;
            gvFees.Visible = !EditMode;

            lblFeeLevelName.Visible = EditMode;
            txtFeeLevelName.Visible = EditMode;
            btnUpdateFeeLevel.Visible = EditMode;
            btnCancelFeeLevel.Visible = EditMode;
            rfvFeeLevelName.Enabled = EditMode;
            revFeeLevelName.Enabled = EditMode;

        }

        protected void btnUpdateFeeLevel_Click(object sender, EventArgs e)
        {
            visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {

                BregVisionMonthlyFeeLevel bregVisionMonthlyFeeLevel = new BregVisionMonthlyFeeLevel();
                bregVisionMonthlyFeeLevel.LevelName = txtFeeLevelName.Text;
                visionDB.BregVisionMonthlyFeeLevels.InsertOnSubmit(bregVisionMonthlyFeeLevel);
                visionDB.SubmitChanges();
                //VisionDataContext.CloseConnection(visionDB);

                _selectedLevelName = bregVisionMonthlyFeeLevel.LevelName;

                LoadFeeSchedule();

                SetFeeLevelEditMode(false);
            }
        }

        protected void btnCancelFeeLevel_Click(object sender, EventArgs e)
        {
            SetFeeLevelEditMode(false);
        }
    }
}
