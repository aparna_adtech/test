﻿<%@ Page MasterPageFile="MasterBregAdmin.Master" Language="C#" AutoEventWireup="true" CodeBehind="DispenseReceiptCustomForms.aspx.cs" Inherits="BregVision.BregAdmin.DispenseReceiptCustomFormPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="text-align: center">
        <div>
            <a href="BregAdmin.aspx" class="managemenulink">Back</a><br />
        </div>
        <div>
            <asp:FileUpload runat="server" ID="CustomFormUpload" />
            <asp:Button runat="server" ID="btnUpload" Text="Upload" OnClick="btnUpload_Click" />
            <asp:Label runat="server" ID="lblSuccess"></asp:Label>
        </div>
        <div>
            <asp:Label runat="server" ID="lblError" ForeColor="red"></asp:Label>
        </div>
        <table id="OuterTable" width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 300px">
                                    <!-- Content Header-->
                                    <tr class="AdminPageTitle">
                                        <td align="center">
                                            <asp:Label ID="lblContentHeader" runat="server" Text="Active Dispense Receipt Custom Forms"></asp:Label>
                                        </td>
                                    </tr>
                                    <!-- Grid -->
                                    <tr style="width: 100%">
                                        <td style="width: 100%">
                                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" DataKeyNames="Id" CellPadding="4"
                                                ForeColor="#5798CF" Width="700px" OnRowCommand="GridView1_RowCommand"
                                                OnSelectedIndexChanging="GridView1_SelectedIndexChanging" HorizontalAlign="Center">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Height="38px" ItemStyle-Width="30px" HeaderText="Order">
                                                        <ItemTemplate>
                                                            <asp:ImageButton runat="server" ID="btnUp" CommandName="MoveUp" CommandArgument='<%# Eval("Id") %>'
                                                                ToolTip="Click to adjust attachment order" ImageUrl="~/RadControls/Grid/Skins/Office2007/MoveUp.gif" /><br />
                                                            <asp:ImageButton runat="server" ID="btnDown" CommandName="MoveDown" CommandArgument='<%# Eval("Id") %>'
                                                                ToolTip="Click to adjust attachment order" ImageUrl="~/RadControls/Grid/Skins/Office2007/MoveDown.gif" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Name" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-Width="23px" HeaderText="Deactivate">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgInactivate" runat="server" CausesValidation="True" CommandName="Deactivate"
                                                                CommandArgument='<%# Eval("Id") %>' AlternateText="Remove"
                                                                ImageUrl="~/RadControls/Grid/Skins/Outlook/Cancel.gif" ToolTip="Delete Custom Form" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>

                                                <RowStyle ForeColor="#000066" Wrap="False" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                <HeaderStyle CssClass="AdminSubTitle" Width="100%" />
                                            </asp:GridView>
                                            <!--  Blank Row at Bottom of Content  -->
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <table>
        <tr>
            <td>
                <table border="0" cellpadding="0" cellspacing="0" style="width: 300px">
                    <!-- Content Header-->
                    <tr class="AdminPageTitle">
                        <td align="center">
                            <asp:Label ID="Label2" runat="server" Text="Inactive Dispense Receipt Custom Forms"></asp:Label>
                        </td>
                    </tr>
                    <!-- Grid -->
                    <tr style="width: 100%">
                        <td style="width: 100%">
                            <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="false" DataKeyNames="Id" CellPadding="4"
                                ForeColor="#5798CF" Width="700px" OnRowCommand="GridView2_RowCommand" HorizontalAlign="Center">
                                <Columns>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:Label ID="Name" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Width="23px" HeaderText="Activate">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEditUpdate" runat="server" CausesValidation="True" CommandName="Activate" CommandArgument='<%# Eval("Id") %>'
                                                ImageUrl="~\RadControls\Grid\Skins\Outlook\Update.gif" ToolTip="Activate Custom Form" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Width="23px" HeaderText="Remove">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgInactivate" runat="server" CausesValidation="True" CommandName="Remove"
                                                CommandArgument='<%# Eval("Id") %>' AlternateText="Remove"
                                                ImageUrl="~/RadControls/Grid/Skins/Outlook/Cancel.gif" ToolTip="Delete Custom Form" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>

                                <RowStyle ForeColor="#000066" Wrap="False" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <HeaderStyle CssClass="AdminSubTitle" Width="100%" />
                            </asp:GridView>
                            <!--  Blank Row at Bottom of Content  -->
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
