﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using BregVision.Admin.PracticeSetup;
using Telerik.Web.UI;
using ClassLibrary.DAL;

namespace BregVision.BregAdmin
{
    public partial class ConsignmentRepAdmin : Bases.PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (grdPracticeUser.HasInitiallyBound == false)
            {
                grdPracticeUser.Rebind();
                grdPracticeUser.HasInitiallyBound = true;
            }

            lblStatus.Visible = false;
            lblStatus.Text = "";
        }


        private void InitializeAjaxSettings()
        {
            Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);
            if (radAjaxManager != null)
            {
                //radAjaxManager.EnableAJAX = false;
                radAjaxManager.AjaxSettings.AddAjaxSetting(grdPracticeUser, grdPracticeUser, RadAjaxLoadingPanel1);
                radAjaxManager.AjaxSettings.AddAjaxSetting(grdPracticeUser, lblStatus);

            }

        }

        protected void grdPracticeUser_OnItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
         

            if (e.CommandName == "Activate")
            {
                Telerik.Web.UI.GridEditableItem editedItem = e.Item as Telerik.Web.UI.GridEditableItem;

                //Label lblStatus = editedItem.FindControl("lblStatus") as Label;
                //lblStatus.Text = "";

                string userName = editedItem["UserName"].Text;
                //int updateUserID = Convert.ToInt32(editedItem["UserID"].Text);
                //string userName = txtUserName.Text;

                MembershipUser editMembershipUser = null;
                editMembershipUser = Membership.GetUser(userName);
                editMembershipUser.IsApproved = true;

                //BLL.Practice.User editUser = new BLL.Practice.User();


                Membership.UpdateUser(editMembershipUser);
                //editUser.ActivateUser(practiceID, updateUserID);
                grdPracticeUser.Rebind();
            }

            if (e.CommandName == "Unlock")
            {
                Telerik.Web.UI.GridEditableItem editedItem = e.Item as Telerik.Web.UI.GridEditableItem;
                string userName = editedItem["UserName"].Text;
                MembershipUser mu = Membership.GetUser(userName);
                mu.UnlockUser();
                Membership.UpdateUser(mu);
                grdPracticeUser.Rebind();
            }
        }

        protected void grdPracticeUser_OnItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {


           
            //The problem here is I can't find the dropdown / combo box in the grid 
            if (e.Item is Telerik.Web.UI.GridEditableItem && (e.Item as Telerik.Web.UI.GridEditableItem).IsInEditMode)
            {
                Telerik.Web.UI.GridEditableItem editedItem = e.Item as Telerik.Web.UI.GridEditableItem;

                bool isInsert = editedItem.Edit; //this = true for a new item, false for an existing item.



                #region Username and Password
                if (isInsert == true)
                {
                    TextBox txtUserName = editedItem.FindControl("txtUserName") as TextBox;
                    txtUserName.Enabled = true;
                }
                else
                {


                    RequiredFieldValidator txtPasswordValidator = editedItem.FindControl("txtPasswordValidator") as RequiredFieldValidator;
                    txtPasswordValidator.Enabled = false;

                    RequiredFieldValidator txtPasswordCompareValidator = editedItem.FindControl("txtPasswordCompareValidator") as RequiredFieldValidator;
                    txtPasswordCompareValidator.Enabled = false;
                }
                #endregion


            }

            if (e.Item is Telerik.Web.UI.GridEditableItem && ((e.Item as Telerik.Web.UI.GridEditableItem).IsInEditMode == false))
            {
                Telerik.Web.UI.GridEditableItem editedItem = e.Item as Telerik.Web.UI.GridEditableItem;


                bool IsActive = Convert.ToBoolean(editedItem["IsActive"].Text);
                if (IsActive == false)
                {
                    editedItem.BackColor = System.Drawing.Color.Red;
                }

                if (e.Item is GridDataItem)
                {
                    Telerik.Web.UI.GridDataItem dataItem = e.Item as GridDataItem;


                    DataRowView drvUsers = e.Item.DataItem as DataRowView;
                    if (drvUsers != null)
                    {
                        string imageUrl = "~/RadControls/Grid/Skins/Office2007/unlock.png";
                        bool isLockedOut = drvUsers["IsLockedOut"] is System.DBNull ? false : Convert.ToBoolean(drvUsers["IsLockedOut"]);
                        if (isLockedOut == true)
                        {
                            imageUrl = "~/RadControls/Grid/Skins/Office2007/lock.png";
                        }


                        (dataItem["Unlock"].Controls[0] as ImageButton).ImageUrl = imageUrl;
                    }
                }

            }

        }

        protected void grdPracticeUser_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {
            string errString = "";

            Telerik.Web.UI.GridEditableItem editedItem = e.Item as Telerik.Web.UI.GridEditableItem;

            bool isInsert = editedItem.Edit; //this = true for a new item, false for an existing item.

            Label lblStatus = editedItem.FindControl("lblStatus") as Label;
            lblStatus.Text = "";

            TextBox txtUserName = editedItem.FindControl("txtUserName") as TextBox;
            string userName = txtUserName.Text;

            string password = "";
            string oldPassword = "";


            TextBox txtPassword = editedItem.FindControl("txtPassword") as TextBox;
            TextBox txtPasswordCompare = editedItem.FindControl("txtPasswordCompare") as TextBox;
            if (txtPassword.Text == txtPasswordCompare.Text)
            {
                password = txtPassword.Text;
            }
            else
            {
                errString = "The password and compare password do not match";
                e.Canceled = true;
                goto ErrorLabel;
            }


            TextBox txtEmail = editedItem.FindControl("txtEmail") as TextBox;
            string email = txtEmail.Text;

            


            //isInsert
            string oldEmail = "";
         
            

            if (isInsert == false)
            {
                oldEmail = editedItem["Email"].Text;
                

            }

            //Validate
            if (isInsert == true)
            {
                if (userName == "")
                {
                    errString = "Please choose an User Name";
                    e.Canceled = true;
                    goto ErrorLabel;
                }
                else if (Membership.GetUser(userName) != null)
                {
                    errString = "User Name already exists, please choose another User Name";
                    e.Canceled = true;
                    goto ErrorLabel;
                }
            }


            MembershipUser editMembershipUser = null;
            if (isInsert == true)
            {
                try
                {
                    editMembershipUser = Membership.CreateUser(userName, password);
                    Roles.AddUserToRole(userName, "BregConsignment");
                }
                catch (System.ArgumentException exa)
                {
                    switch (exa.Message)
                    {
                        case "The length of parameter 'newPassword' needs to be greater or equal to '7'.":
                            errString = "The new password must contain at least 7 characters with one of the characters alpha-numeric";
                            break;
                        case "Non alpha numeric characters in 'newPassword' needs to be greater than or equal to '1'.":
                            errString = "The new password must contain at least 7 characters with one of the characters alpha-numeric";
                            break;
                        default:
                            errString = exa.Message;
                            break;
                    }
                    e.Canceled = true;
                    goto ErrorLabel;
                }
                catch (Exception ex)
                {
                    string errMessage = "The new password must contain at least 7 characters with one of the characters alpha-numeric";

                    if (ex.Message.Contains("password") && ex.Message.Contains("invalid"))
                    {
                        errString = errMessage;
                    }
                    else
                    {
                        errString = ex.Message;
                    }
                    e.Canceled = true;
                    goto ErrorLabel;
                }
            }
            else
            {
                editMembershipUser = Membership.GetUser(userName);
                if (password != "")
                {
                    try
                    {
                        editMembershipUser.ChangePassword(editMembershipUser.ResetPassword(), password);
                    }
                    catch (System.ArgumentException exa)
                    {
                        switch (exa.Message)
                        {
                            case "The length of parameter 'newPassword' needs to be greater or equal to '7'.":
                                errString = "The new password must contain at least 7 characters with one of the characters non alpha-numeric";
                                break;
                            case "Non alpha numeric characters in 'newPassword' needs to be greater than or equal to '1'.":
                                errString = "The new password must contain at least 7 characters with one of the characters non alpha-numeric";
                                break;
                            default:
                                errString = exa.Message;
                                break;
                        }
                        e.Canceled = true;
                        goto ErrorLabel;
                    }
                    catch (Exception ex)
                    {
                        errString = ex.Message;
                        e.Canceled = true;
                        goto ErrorLabel;
                    }
                }
            }

            if (email != oldEmail)
            {

                editMembershipUser.Email = email;
                //editMembershipUser.GetPassword();
                Membership.UpdateUser(editMembershipUser);
            }

        ErrorLabel:
            if (e.Canceled == true)
            {
                lblStatus.Visible = true;
                lblStatus.Text = errString;
            }
        }

        protected void grdPracticeUser_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
        }

        protected void grdPracticeUser_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable)
            {
                List<usp_GetConsignmentRepsAllResult> reps = GetAllConsignmentReps();

                var RepQuery = from r in reps
                            select new
                            {
                                r.UserName,
                                r.UserId,
                                r.IsLockedOut,
                                IsActive = true,
                                // TODO: fix this when we're actually releasing consignment... if ever.
                                Email = string.Empty,
                                IsApproved = false
                                //r.Email,
                                //r.IsApproved
                            };

                grdPracticeUser.DataSource = RepQuery;
            }
        }

        public List<usp_GetConsignmentRepsAllResult> GetAllConsignmentReps()
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {

                var repsQuery = from r in visionDB.usp_GetConsignmentRepsAll()
                                select r;

                return repsQuery.ToList<usp_GetConsignmentRepsAllResult>();
            }
        }
    }
}