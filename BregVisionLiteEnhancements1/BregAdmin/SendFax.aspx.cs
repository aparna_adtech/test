﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using System.Web.UI;
using ClassLibrary.DAL;
using System.Text;

namespace BregVision.BregAdmin
{
    public partial class SendFax : System.Web.UI.Page
    {
        VisionDataContext visionDB = null;
        IEnumerable<FaxMessage> _FaxMessages = null;

        protected string GetUserErrorInfo()
        {
            string userName = Context.User.Identity.Name;
            int eUserID = (Session["UserID"] != null) ? Convert.ToInt32(Session["UserID"].ToString()) : -1;
            int ePracticeID = (Session["PracticeID"] != null) ? Convert.ToInt32(Session["PracticeID"].ToString()) : -1;
            string ePracticeName = (Session["PracticeName"] != null) ? Session["PracticeName"].ToString() : "Not Set";

            string errString = string.Format(":CurrentUserID={0}:CurrentUserName={1}:CurrentPracticeID={2}:CurrentPracticeName='{3}': \r\n<br>", eUserID.ToString(), userName, ePracticeID.ToString(), ePracticeName);
            return errString;
        }

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                GetUnsentFaxMessages();
                GetFaxServerStatus();
            }
            lblTransactionStatus.Text = "";
        }
        #endregion

        #region OnPreRender
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            Response.AppendHeader("Cache-Control", "no-cache; private; no-store; must-revalidate; max-stale=0; post-check=0; pre-check=0; max-age=0");
            Response.AppendHeader("Pragma", "no-cache");
            Response.AppendHeader("Keep-Alive", "timeout=3, max=993");
            Response.AppendHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT"); //Some random old date

            GetUnsentFaxMessages();
            var query = from fm in _FaxMessages.Take(10)
                        select new
                        {
                            fm.FaxMessageID,
                            TransmissionID = fm.FaxFileName.Substring(fm.FaxFileName.IndexOf("test2") + 5, 15),
                            fm.CreatedDate,
                            fm.FaxNumber,
                            FaxFilename = fm.FaxFileName.Substring(fm.FaxFileName.LastIndexOf("/") + 1),
                            fm.SendAttempts,
                            FaxStatus = FaxEmailPOs.GetStatus(fm.FaxFileName.Substring(fm.FaxFileName.IndexOf("test2") + 5, 15))
                        };

            var failedMessages = query.Where(messageFields => messageFields.FaxStatus.Contains("Success") == false);
            gvUnsentFaxes.DataSource = failedMessages;
            gvUnsentFaxes.DataBind();
        }
        #endregion

        #region Get Functions
        protected void GetAllFaxesSince(DateTime dtDateFrom, DateTime dtDateTo)
        {
            _FaxMessages = FaxMessage.GetAllFaxesSince(dtDateFrom, dtDateTo);
        }

        protected void GetFaxServerStatus()
        {

            bool faxEnabled = FaxServer.GetFaxServerStatus();

            hTitle.InnerText = string.Format("Top 10 Unsent Faxes.  Fax Server is currently {0}.", faxEnabled == true ? "ON" : "OFF");
        }

        protected void GetUnsentFaxMessages()
        {

            _FaxMessages = FaxMessage.GetUnsentFaxMessages();
        }

        #endregion

        protected void ChangeBorderColor(GridViewRowEventArgs e)
        {
            int intSendAttempts = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SendAttempts"));

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (intSendAttempts > 5)
                {
                    e.Row.Font.Bold = true;
                    e.Row.ForeColor = System.Drawing.Color.Red;
                    //e.Row.BorderColor = System.Drawing.Color.Red;
                    //e.Row.BorderStyle = BorderStyle.Dashed;
                }
            }
        }


        #region Button Click Event
        protected void btnResend_Click(object sender, EventArgs e)
        {
            txtTransactionID.Text = "";

            visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                FaxEmailPOs faxServer = new FaxEmailPOs();

                foreach (GridViewRow row in gvUnsentFaxes.Rows)
                {
                    int faxMessageID = Convert.ToInt32(gvUnsentFaxes.DataKeys[row.RowIndex].Value);

                    CheckBox chkResend = row.FindControl("chkResend") as CheckBox;
                    CheckBox chkSkip = row.FindControl("chkSkip") as CheckBox;

                    if (chkResend.Checked == true || chkSkip.Checked == true)
                    {
                        //find the message in this row
                        var message = from fm in _FaxMessages
                                      where fm.FaxMessageID == faxMessageID
                                      select fm;
                        FaxMessage currentMessage = message.First<FaxMessage>();


                        
                        if (chkSkip.Checked == true)
                        {
                            visionDB.FaxMessages.Attach(currentMessage);
                            currentMessage.Skip = true;
                            visionDB.SubmitChanges();
                        }
                        else if ((chkResend.Checked == true) && (currentMessage.SendAttempts == 6))               
                        {
                            //reset the counter
                            visionDB.FaxMessages.Attach(currentMessage);
                            currentMessage.SendAttempts = 0;
                            visionDB.SubmitChanges();
                            //resend any fax messages
                            faxServer.SendFax(currentMessage.FaxNumber, currentMessage.FaxFileName, currentMessage.FaxMessageID);  //FaxEmailPOs.SendFax
                            
                        }
                        else if ((chkResend.Checked == true) && (currentMessage.SendAttempts < 6)) 
                        {
                            //reset the counter
                            visionDB.FaxMessages.Attach(currentMessage);
                            visionDB.SubmitChanges();
                            //resend any fax messages
                            faxServer.SendFax(currentMessage.FaxNumber, currentMessage.FaxFileName, currentMessage.FaxMessageID); 
                        }
                    }
                }                

                //VisionDataContext.CloseConnection(visionDB);
            }
            GetUnsentFaxMessages();
        }

        protected void btnCheckStatus_Click(object sender, EventArgs e)
        {
            string strTransactionID = txtTransactionID.Text;
            //string strTransactionID = "";

            if (strTransactionID.Length == 14)
            {
                strTransactionID = txtTransactionID.Text.Substring(0, 14);
            }
            else if (strTransactionID.Length == 15)
            {
                strTransactionID = txtTransactionID.Text;
            }
            

            lblTransactionStatus.Text = "";

            Int64 intTransmissionID = 0;

            if ((Int64.TryParse(strTransactionID, out intTransmissionID) == true))
            {
                lblTransactionStatus.Text = FaxEmailPOs.GetStatus(strTransactionID);
            }
            else 
            {
                lblTransactionStatus.ForeColor = System.Drawing.Color.Red;
                lblTransactionStatus.Text = "This Transmission ID couldn't be found.";
            }
        }

        protected void btnMarkUnsent_Click(object sender, EventArgs e)
        {
            Int64 intTransmissionID = 0;
            string strTransactionID = txtTransactionID.Text;

            if (strTransactionID.Length == 14)
            {
                strTransactionID = txtTransactionID.Text.Substring(0, 14);
            }
            else if (strTransactionID.Length == 15)
            {
                strTransactionID = txtTransactionID.Text;
            }
            
            //Memo: If Skip==True then, it really doesn't update as an unsent  u= 01/15/2010

            //if (txtTransactionID.Text.Length == 15 && Int64.TryParse(txtTransactionID.Text, out intTransmissionID) == true)
            if (Int64.TryParse(strTransactionID, out intTransmissionID) == true)
            {
                FaxMessage.UpdateUnsentFaxMessagesByTransmissionID(strTransactionID);
            }
        }

        protected void btnCountFaxes_Click(object sender, EventArgs e)
        {
            //visionDB = VisionDataContext.GetVisionDataContext();
            lblCountFaxes.Text = "";
            txtTransactionID.Text = "";

            gvViewAllFaxesSince.Visible = false;

            using (visionDB)
            {
                DateTime dtDateFrom = Convert.ToDateTime(txtDateFrom.Text);
                DateTime dtDateTo = Convert.ToDateTime(txtDateTo.Text);
                GetAllFaxesSince(dtDateFrom, dtDateTo);

                var query = from fm in _FaxMessages
                            where fm.CreatedDate >= dtDateFrom
                            select new 
                            {
                                fm.FaxMessageID,
                                fm.FaxNumber,
                                FaxFileName = fm.FaxFileName.Substring(fm.FaxFileName.LastIndexOf("/") + 1),
                                TransmissionID = fm.FaxFileName.Substring(fm.FaxFileName.IndexOf("test2") + 5, 15),
                                fm.SentTime,
                                fm.CreatedDate,
                                fm.Sent,
                                fm.Skip,
                                fm.SendAttempts,
                                FaxStatus = FaxEmailPOs.GetStatus(fm.FaxFileName.Substring(fm.FaxFileName.IndexOf("test2") + 5, 15))
                            };

                string strFaxCount = _FaxMessages.Count().ToString();
                lblCountFaxes.Text = strFaxCount + " Fax(es) found";

                if(Convert.ToInt32(strFaxCount)== 0) 
                {
                    lblShowPageSize.Visible = false; 
                    txtPageSize.Visible = false;
                    lblShowPageNumber.Visible = false;
                    txtPageNumber.Visible = false;

                    btnShowFaxes.Visible = false;
                }
                else if (Convert.ToInt32(strFaxCount) < 21)
                {
                    lblShowPageSize.Visible = false;
                    txtPageSize.Visible = false;
                    lblShowPageNumber.Visible = false;
                    txtPageNumber.Visible = false;

                    gvViewAllFaxesSince.DataSource = query;
                    gvViewAllFaxesSince.DataBind();
                    gvViewAllFaxesSince.Visible = true;
                    //btnShowFaxes.Visible = true;
                }
                else 
                {
                    lblShowPageSize.Visible = true;
                    txtPageSize.Visible = true;
                    lblShowPageNumber.Visible = true;
                    txtPageNumber.Visible = true;

                    btnShowFaxes.Visible = true;
                }

                //gvViewAllFaxesSince.DataSource = query;//_FaxMessages;
                //gvViewAllFaxesSince.DataBind();

            }
        }

        protected void btnShowFaxes_Click(object sender, EventArgs e)
        {
            DateTime dtDateFrom = Convert.ToDateTime(txtDateFrom.Text);
            DateTime dtDateTo = Convert.ToDateTime(txtDateTo.Text);
            int pageSize = Convert.ToInt32(txtPageSize.Text);
            int pageNumber = Convert.ToInt32(txtPageNumber.Text);
            int recordsToSkip = pageSize * (pageNumber - 1);

            txtTransactionID.Text = "";

            GetAllFaxesSince(dtDateFrom, dtDateTo);

            var query = from fm in _FaxMessages
                        where fm.CreatedDate >= dtDateFrom
                        select new
                        {
                            fm.FaxMessageID,
                            fm.FaxNumber,
                            FaxFileName = fm.FaxFileName.Substring(fm.FaxFileName.LastIndexOf("/") + 1),
                            TransmissionID = fm.FaxFileName.Substring(fm.FaxFileName.IndexOf("test2") + 5, 15),
                            fm.SentTime,
                            fm.CreatedDate,
                            fm.Sent,
                            fm.Skip,
                            fm.SendAttempts,
                            FaxStatus = FaxEmailPOs.GetStatus(fm.FaxFileName.Substring(fm.FaxFileName.IndexOf("test2") + 5, 15))
                        };

            string strFaxCount = _FaxMessages.Count().ToString(); 
            lblCountFaxes.Text = strFaxCount + " Fax(es) found";

            if (Convert.ToInt32(strFaxCount) < 21)
            {
                gvViewAllFaxesSince.DataSource = query;   
            }
            else 
            {
                gvViewAllFaxesSince.DataSource = query.Skip(recordsToSkip).Take(pageSize); 
            }
            gvViewAllFaxesSince.Visible = true;
            gvViewAllFaxesSince.DataBind();
        }

        #endregion


        #region gvViewAllFaxesSince GridView
        protected void gvViewAllFaxesSince_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvViewAllFaxesSince.PageIndex = e.NewPageIndex;
            gvViewAllFaxesSince.DataBind();
        }

        protected void gvViewAllFaxesSince_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            ChangeBorderColor(e);
        }
        #endregion

        #region gvUnsentFaxes GridView
        protected void gvUnsentFaxes_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gvUnsentFaxes_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvUnsentFaxes.PageIndex = e.NewPageIndex;
            gvUnsentFaxes.DataBind();
        }

        protected void gvUnsentFaxes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            ChangeBorderColor(e);
        }
        #endregion

    }
}
