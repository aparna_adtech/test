﻿<%@ Page Language="C#" MasterPageFile="MasterBregAdmin.Master" AutoEventWireup="true" CodeBehind="SalesReportAdmin.aspx.cs" Inherits="BregVision.BregAdmin.SalesReportAdmin" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .hiddencol
        {
            display:none;
        }
    </style>

    <div style="text-align: center">
    <table cellpadding="0" cellspacing="0" width="100%" border="0">
        <tr>
            <td colspan="3">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>&nbsp;</td>
                        <td style="width:100px">
                            <a href="BregAdmin.aspx" class="managemenulink">Back</a><br />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <table cellpadding="0" cellspacing="0" width="80%">
                    <tr>
                        <td  class="AdminPageTitle">
                            Sales Report Admin
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr align="center">
            <td colspan="3">
                <table cellpadding="0" cellspacing="2" border="0" width="100%">
                    <tr style="height: 30px">
                        <td></td>
                        <td class="TextMed" align="left" style="width: 460px"><b>Practice Catalog Supplier Brand</b></td>
                        <td></td>
                        <td class="TextMed" align="left" style="width: 550px" colspan="2"><b>Sales Report Exclusions</b></td>
                        <%--<td></td>--%>
                    </tr>
                    <tr>
                        <td></td>
                        <td align="left">
                            <asp:Label ID="lblPraccticeCatalogSearch" runat="server" CssClass="FieldLabel" Text="Search Supplier/Brand Short Name"></asp:Label>
                            <asp:TextBox id="txtPracticeCatalogSearch" runat="server"></asp:TextBox>&nbsp;
                            <asp:Button ID="btnSearchPracticeCatalog" runat="server" Text="Search" onclick="btnSearchPracticeCatalog_Click" /><br />
                            <asp:Label ID="lblPracticeCatalogSearchCount" runat="server" CssClass="WarningMsg"></asp:Label>
                        </td>
                        <td align="center" style="width: 180px">
                            <asp:Button ID="btnRefreshAll" runat="server" Text ="Refresh All" 
                                onclick="btnRefreshAll_Click" />
                        </td>
                        <td align="left" colspan="2">
                            <asp:Label ID="lblSalesReportExclusions" runat="server" CssClass="FieldLabel" Text="Search Supplier/Brand Short Name"></asp:Label>
                            <asp:TextBox ID="txtSalesReportExclusionsSearch" runat="server"></asp:TextBox>&nbsp;
                            <asp:Button ID="btnSearchSalesReportExclusions" runat="server" Text ="Search" 
                                onclick="btnSearchSalesReportExclusions_Click"/><br />
                            <asp:Label ID="lblMessage" runat="server" CssClass="WarningMsg"></asp:Label>
                        </td>
                        <%--<td></td>--%>
                        <%--<td></td>--%>
                    </tr>
                    <tr id="trAddRow" runat="server" class="FieldLabelBold" visible="false">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td align="left" colspan="2">
                            <asp:Label ID="lblPCSBID" runat="server" Text="PCSB ID"></asp:Label>
                            <asp:TextBox ID="txtPCSBID" runat="server" Width="38px"></asp:TextBox>&nbsp;
                            <asp:RequiredFieldValidator ID="rfvPCSBID" runat="server" ControlToValidate="txtPCSBID" ErrorMessage="*" Display="Dynamic" CssClass="WarningMsg" ValidationGroup ="vgAddNewRow"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revSalesReportSupplierShortName" runat="server" ControlToValidate="txtPCSBID" ErrorMessage="Numeric Number only" CssClass="WarningMsg" Display="Dynamic" ValidationExpression ="^\d+$"></asp:RegularExpressionValidator>
                            <asp:Label ID="lblSupplierShortName" runat="server" Text="Supplier Short Name"></asp:Label>
                            <asp:TextBox ID="txtSupplierShortName" runat="server" Width="130px"></asp:TextBox><br />
                            <asp:Label ID="lblBrandShortName" runat="server" Text="Brand Short Name"></asp:Label>
                            <asp:TextBox ID="txtBrandShortName" runat="server" Width="130px"></asp:TextBox>&nbsp;
                            <asp:Label ID="lblIsExcluded" runat="server" Text="IsExcluded"></asp:Label><asp:CheckBox ID="chkIsExcluded" runat="server" Checked="false" />
                            <asp:Button ID="btnAdd" runat="server" Text="Add" onclick="btnAdd_Click" ValidationGroup ="vgAddNewRow"/>&nbsp;
                            <%--<asp:Button ID="btnCancel" runat="server" Text="Cancel" onclick="btnCancel_Click" />&nbsp;--%>
                            <asp:Button ID="btnClear" runat="server" Text="Clear" 
                                onclick="btnClear_Click" />
                         </td>
                        
                        <%--<td></td>--%>
                    </tr>
                    <tr>
                        <td style="width: 12%"></td>
                        <td valign="top">
                            <asp:GridView ID="gvPracticeCatalogSupplierBrand" runat="server" Width ="470px" 
                                CssClass="FieldLabel"
                                AutoGenerateColumns="False" PageSize="30" AllowPaging="True" 
                                OnPageIndexChanging="gvPracticeCatalogSupplierBrand_PageIndexChanging" 
                                onrowediting="gvPracticeCatalogSupplierBrand_RowEditing" 
                                onselectedindexchanged="gvPracticeCatalogSupplierBrand_SelectedIndexChanged">
                                <SelectedRowStyle BackColor="#cccccc" />
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="AdminSubTitle" >
                                        <HeaderStyle CssClass="AdminSubTitle"></HeaderStyle>
                                    </asp:CommandField>
                                    <asp:TemplateField HeaderText="PCSB ID" HeaderStyle-CssClass="AdminSubTitle" HeaderStyle-Width="250px" ItemStyle-Width="250px" ItemStyle-Height="23px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPCSBID" runat="server" Text='<%# Bind("PracticeCatalogSupplierBrandID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="AdminSubTitle" Width="250px"></HeaderStyle>
                                        <ItemStyle Height="23px" Width="250px"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Supplier Short Name" HeaderStyle-CssClass="AdminSubTitle" HeaderStyle-Width="250px" ItemStyle-Width="250px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSupplierShortName" runat="server" Text ='<%# Bind("SupplierShortName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="AdminSubTitle" Width="250px"></HeaderStyle>
                                        <ItemStyle Width="250px"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Brand Short Name" HeaderStyle-CssClass="AdminSubTitle" HeaderStyle-Width="250px" ItemStyle-Width="250px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBrandShortName" runat="server" Text ='<%# Bind("BrandShortName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="AdminSubTitle" Width="250px"></HeaderStyle>
                                        <ItemStyle Width="250px"></ItemStyle>
                                    </asp:TemplateField>
                                    
                                </Columns>                                
                            </asp:GridView>
                        </td>
                        <td>
                            <asp:Button ID="btnToSalesReportExclutions" runat="server" Text=">>" 
                                onclick="btnToSalesReportExclutions_Click" Enabled="false"/><br /><br />
                            <asp:Button ID="btnExclude" runat="server" Text="Exclude >>" 
                                onclick="btnExclude_Click" Enabled="false" />
                        </td>
                        <td valign="top" colspan="2">
                            <asp:GridView ID="gvSalesReportExclusions" runat="server" 
                                CssClass="FieldLabel"  PageSize="30" AllowPaging="True" 
                            AutoGenerateColumns="False" Width="580px" AllowSorting="true"
                                onrowcancelingedit="gvSalesReportExclusions_RowCancelingEdit" 
                                onrowediting="gvSalesReportExclusions_RowEditing" 
                                onrowupdating="gvSalesReportExclusions_RowUpdating" 
                                onpageindexchanging="gvSalesReportExclusions_PageIndexChanging" 
                                onrowdeleting="gvSalesReportExclusions_RowDeleting">
                                <Columns>
                                    <asp:CommandField ShowCancelButton="true" ShowEditButton="true" 
                                        HeaderStyle-CssClass="AdminSubTitle" HeaderStyle-Width ="150px" 
                                        ItemStyle-Width ="150px">
                                    <HeaderStyle CssClass="AdminSubTitle" Width="150px"></HeaderStyle>
                                    <ItemStyle Width="150px"></ItemStyle>
                                    </asp:CommandField>
                                    <asp:TemplateField HeaderText ="PCSB ID" HeaderStyle-CssClass="AdminSubTitle" HeaderStyle-Width="90px" ItemStyle-Width="90px" ItemStyle-Height="23px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSalesReportPCSBID" runat="server" Text ='<%# Bind("PracticeCatalogSupplierBrandID") %>' Width="90px"></asp:Label>
                                        </ItemTemplate>
                                       <%-- <EditItemTemplate>
                                            <asp:TextBox ID="txtSalesReportPCSBID" runat="server" Text='<%# Bind("PracticeCatalogSupplierBrandID") %>' CssClass="FieldLabel" Width="80px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvSalesReportPCSBID" runat="server" ControlToValidate="txtSalesReportPCSBID" ErrorMessage="*"></asp:RequiredFieldValidator>
                                        </EditItemTemplate>--%>

                                    <HeaderStyle CssClass="AdminSubTitle" Width="90px"></HeaderStyle>
                                    <ItemStyle Height="23px" Width="90px"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText ="Supplier Short Name" HeaderStyle-CssClass="AdminSubTitle" HeaderStyle-Width="160px" ItemStyle-Width="160px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSalesReportSupplierShortName" runat="server" Text ='<%# Bind("SupplierShortName")%>' Width="150px"></asp:Label>
                                        </ItemTemplate>
                                       <EditItemTemplate>
                                            <asp:TextBox ID="txtSalesReportSupplierShortName" runat="server" Text='<%# Bind("SupplierShortName") %>' CssClass="FieldLabel" Width="150px"></asp:TextBox>
                                            <%--<asp:RequiredFieldValidator ID="rfvSalesReportSupplierShortName" runat="server" ControlToValidate="txtSalesReportSupplierShortName" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                                        </EditItemTemplate>

                                    <HeaderStyle CssClass="AdminSubTitle" Width="160px"></HeaderStyle>
                                    <ItemStyle Width="160px"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText ="Brand Short Name" HeaderStyle-CssClass="AdminSubTitle" HeaderStyle-Width="225px" ItemStyle-Width="225px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSalesBrandShortName" runat="server" Text ='<%# Bind("BrandShortName")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtBrandShortName" runat="server" Text='<%# Bind("BrandShortName") %>' CssClass="FieldLabel" Width="150px"></asp:TextBox>
                                            <%--<asp:RequiredFieldValidator ID="rfvBrandShortName" runat="server" ControlToValidate="txtBrandShortName" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                                        </EditItemTemplate>
                                    <HeaderStyle CssClass="AdminSubTitle" Width="225px"></HeaderStyle>
                                    <ItemStyle Width="225px"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="IsExcluded" HeaderStyle-CssClass="AdminSubTitle" HeaderStyle-Width="90px" ItemStyle-Width ="90px">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkvIsExcluded" runat="server" Checked='<%# Bind("IsExcluded") %>' Enabled="false" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="chkIsExcluded" runat="server" Checked='<%# Bind("IsExcluded") %>' />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText ="Delete" HeaderStyle-CssClass="AdminSubTitle" >
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" Text="Delete" OnClientClick ='return confirm("Are you sure you want delete this?");'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:CommandField ShowCancelButton="true" ShowDeleteButton ="true"
                                        HeaderStyle-CssClass="AdminSubTitle" HeaderStyle-Width ="150px" 
                                        ItemStyle-Width ="150px"/>--%>
                                </Columns>
                            </asp:GridView> 
                        </td>
                        <%--<td></td>--%>
                    </tr>
                </table>
            </td>
        </tr>

    </table>
    </div>
    </asp:Content>

