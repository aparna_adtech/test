﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Text;
using ClassLibrary.DAL;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.SqlClient;
using System.Data.Common;


namespace BregVision.BregAdmin
{
    public partial class SalesReportAdmin : System.Web.UI.Page
    {
        protected IEnumerable<zzzSalesReportExclusion> _zzzSalesReportExclusion; 
        protected IEnumerable<PracticeCatalogSupplierBrand> _PracticeCatalogSupplierBrand;
        VisionDataContext visionDB = null;

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadSalesReportExclusionsMasterList("");
            LoadPracticeCatalogSupplierMasterList("");
        }

        protected void LoadSalesReportExclusionsMasterList(string SearchShortName)
        {
            SearchShortName = "%" + SearchShortName + "%";

            visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                //Search for either a supplier short name or brand short name
                _zzzSalesReportExclusion = (from salesReportExclusion in visionDB.zzzSalesReportExclusions
                                            where SqlMethods.Like(salesReportExclusion.BrandShortName, SearchShortName)  || SqlMethods.Like(salesReportExclusion.SupplierShortName, SearchShortName)
                                            select salesReportExclusion).ToList();
            }
        }

        protected void LoadPracticeCatalogSupplierMasterList(string SearchShortName)
        {
            SearchShortName = "%" + SearchShortName + "%";
            
            visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                //Search for either a supplier short name or brand short name
                _PracticeCatalogSupplierBrand = (from practiceCatalogSupplierBrand in visionDB.PracticeCatalogSupplierBrands
                                                 where (practiceCatalogSupplierBrand.IsThirdPartySupplier == true) & (practiceCatalogSupplierBrand.IsActive == true) &
                                                 SqlMethods.Like(practiceCatalogSupplierBrand.BrandShortName, SearchShortName) || SqlMethods.Like(practiceCatalogSupplierBrand.SupplierShortName, SearchShortName)
                                                 orderby practiceCatalogSupplierBrand.PracticeCatalogSupplierBrandID
                                                 select practiceCatalogSupplierBrand).ToList();
            }
        }
        #endregion

        protected bool FindShortName(string SearchShortName)
        {
            visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {

                var salesReportExclusionsFound = from salesReportExclusion in visionDB.zzzSalesReportExclusions
                                       where salesReportExclusion.BrandShortName == SearchShortName
                                       orderby salesReportExclusion.ExclusionID
                                       select salesReportExclusion;

                //VisionDataContext.CloseConnection(visionDB);

                if (salesReportExclusionsFound.Count() > 0)
                {
                    return true;
                }
                return false;
            }

        }


        protected bool FindPCSBID(string PCSBID)
        {
            visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                var salesReportExclusionsFound = from salesReportExclusion in visionDB.zzzSalesReportExclusions
                                                 where Convert.ToString(salesReportExclusion.PracticeCatalogSupplierBrandID)  == PCSBID
                                                 select salesReportExclusion;

                //VisionDataContext.CloseConnection(visionDB);

                if (salesReportExclusionsFound.Count() > 0)
                {
                    return true;
                }
                return false;
            }

        }
        #region button click event
        protected void btnSearchSalesReportExclusions_Click(object sender, EventArgs e)
        {
            LoadSalesReportExclusionsMasterList(txtSalesReportExclusionsSearch.Text);
            lblMessage.Text = _zzzSalesReportExclusion.Count() + " item(s) found.";

            if (_zzzSalesReportExclusion.Count() < 1)
            {
                //Turn the row visible so we can add the new data in the zzzSalesReportExclusion table
                trAddRow.Visible = true;
            }
            else
            {
                trAddRow.Visible = false;
            }
        }

        protected void btnSearchPracticeCatalog_Click(object sender, EventArgs e)
        {
            LoadPracticeCatalogSupplierMasterList(txtPracticeCatalogSearch.Text);

            lblPracticeCatalogSearchCount.Text = _PracticeCatalogSupplierBrand.Count() +" item(s) found.";
    
        }

        protected void btnRefreshAll_Click(object sender, EventArgs e)
        {

            //To Do --
            txtSalesReportExclusionsSearch.Text = "";
            txtPracticeCatalogSearch.Text = "";
            lblMessage.Text = "";
            lblPracticeCatalogSearchCount.Text = "";

            ClearAddNewFields();
            PracticeCatalogSupplierBrandDefaultRowColor();

            trAddRow.Visible = false;          

            LoadSalesReportExclusionsMasterList("");
            LoadPracticeCatalogSupplierMasterList("");
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if(IsPostBack)
            {
                if (FindPCSBID(txtPCSBID.Text) == false)
                {
                    visionDB = VisionDataContext.GetVisionDataContext();

                    using (visionDB)
                    {
                        zzzSalesReportExclusion zzz_SalesReportExclusion = new zzzSalesReportExclusion();

                        zzz_SalesReportExclusion.PracticeCatalogSupplierBrandID = Convert.ToInt32(txtPCSBID.Text);
                        zzz_SalesReportExclusion.SupplierShortName = txtSupplierShortName.Text;
                        zzz_SalesReportExclusion.BrandShortName = txtBrandShortName.Text;
                        zzz_SalesReportExclusion.IsExcluded = chkIsExcluded.Checked;

                        var query = from z in visionDB.zzzSalesReportExclusions
                                    where z.PracticeCatalogSupplierBrandID == zzz_SalesReportExclusion.PracticeCatalogSupplierBrandID
                                    select z;
                        int count = query.Count();

                        visionDB.zzzSalesReportExclusions.InsertOnSubmit(zzz_SalesReportExclusion);
                        visionDB.SubmitChanges();

                        lblMessage.Text = "Updated Successfully!";
                        trAddRow.Visible = false;

                        //Clear all text box
                        ClearAddNewFields();

                        PracticeCatalogSupplierBrandDefaultRowColor();

                        txtSalesReportExclusionsSearch.Text = "";

                        LoadSalesReportExclusionsMasterList("");
                    }
                }
                else 
                {
                    lblMessage.Text = "The PCSBID you entered was already in the Sales Report Exclusions.";
                }
                    //VisionDataContext.CloseConnection(visionDB);

            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            ClearAddNewFields();

            PracticeCatalogSupplierBrandDefaultRowColor();
        }

        protected void btnToSalesReportExclutions_Click(object sender, EventArgs e)
        {
            visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
              
                GridViewRow row = gvPracticeCatalogSupplierBrand.SelectedRow;
                int iSelectedRow = Convert.ToInt32(row.RowIndex);
                int PCSBID = row.Cells[1].GetSafeIntFromLabel("lblPCSBID");
                string supplierShortName = row.Cells[2].GetSafeStringFromLabel("lblSupplierShortName");
                string brandShortName = row.Cells[3].GetSafeStringFromLabel("lblBrandShortName");

                var query = from z in visionDB.zzzSalesReportExclusions
                            where z.PracticeCatalogSupplierBrandID == PCSBID
                            select z;
                int count = query.Count();
                if (count > 0)
                {
                    lblMessage.Text = "The item you selected was already in the Sales Report Exclusions.";
                }
                else
                {
                    trAddRow.Visible = true;

                    lblMessage.Text = "";

                    txtPCSBID.Text = PCSBID.ToString();
                    txtSupplierShortName.Text = supplierShortName;
                    txtBrandShortName.Text = brandShortName;
                }
            }
        }

        protected void btnExclude_Click(object sender, EventArgs e)
        {
               visionDB = VisionDataContext.GetVisionDataContext();
               using (visionDB)
               {

                   GridViewRow row = gvPracticeCatalogSupplierBrand.SelectedRow;
                   int iSelectedRow = Convert.ToInt32(row.RowIndex);
                   int PCSBID = row.Cells[1].GetSafeIntFromLabel("lblPCSBID");
                   string supplierShortName = row.Cells[2].GetSafeStringFromLabel("lblSupplierShortName");
                   string brandShortName = row.Cells[3].GetSafeStringFromLabel("lblBrandShortName");

                   var query = from z in visionDB.zzzSalesReportExclusions
                               where z.PracticeCatalogSupplierBrandID == PCSBID
                               select z;
                   int count = query.Count();
                   if (count > 0)
                   {
                       lblMessage.Text = "The item you selected was already in the Sales Report Exclusions.";
                   }
                   else
                   {
                       trAddRow.Visible = true;

                       lblMessage.Text = "";

                       txtPCSBID.Text = PCSBID.ToString();
                       txtSupplierShortName.Text = supplierShortName;
                       txtBrandShortName.Text = brandShortName;
                       chkIsExcluded.Checked = true;
                   }
               }
        }
        #endregion

        protected void ClearAddNewFields() 
        {
            txtPCSBID.Text = "";
            txtSupplierShortName.Text = "";
            txtBrandShortName.Text = "";
            chkIsExcluded.Checked = false;
        }

        protected void PracticeCatalogSupplierBrandSelectedRowColor() 
        {
            gvPracticeCatalogSupplierBrand.SelectedRowStyle.BackColor = System.Drawing.Color.Silver;
        }

        protected void PracticeCatalogSupplierBrandDefaultRowColor()
        {
            gvPracticeCatalogSupplierBrand.SelectedRowStyle.BackColor = System.Drawing.Color.Transparent;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            BindLevelControlsSalesReportExlusions();
            BindLevelControlPracticeCatalogSupplierBrand();
        }
        #region Bind Level Controls
        protected void BindLevelControlsSalesReportExlusions() 
        {
            LoadSalesReportExclusionsMasterList(txtSalesReportExclusionsSearch.Text);

            gvSalesReportExclusions.DataSource = _zzzSalesReportExclusion;
            gvSalesReportExclusions.DataBind();
        }
        protected void BindLevelControlPracticeCatalogSupplierBrand()
        {
            LoadPracticeCatalogSupplierMasterList(txtPracticeCatalogSearch.Text);//txtSearchShortName.Text);

            gvPracticeCatalogSupplierBrand.DataSource = _PracticeCatalogSupplierBrand;
            gvPracticeCatalogSupplierBrand.DataBind();

        }
        #endregion

        #region gvSalesReportExclusions event
        protected void gvSalesReportExclusions_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvSalesReportExclusions.EditIndex = e.NewEditIndex;
        }

        protected void gvSalesReportExclusions_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvSalesReportExclusions.EditIndex = -1;
        }

        protected void gvSalesReportExclusions_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            visionDB = VisionDataContext.GetVisionDataContext();

            // To Do -- When I tried to update an item from the last page, somehow it thinks the item is not existing, so it tried to add the item instead of updating.
            using (visionDB)
            {
                GridViewRow row = gvSalesReportExclusions.Rows[e.RowIndex];
                if ((row.RowState == (DataControlRowState.Edit | DataControlRowState.Alternate)) || (row.RowState == DataControlRowState.Edit))
                {

                    zzzSalesReportExclusion zzz_SalesReportExclusion = null;
                    //zzz_SalesReportExclusion = _zzzSalesReportExclusion.SingleOrDefault<zzzSalesReportExclusion>(p => p.PracticeCatalogSupplierBrandID.Equals(Convert.ToInt32(row.Cells[1].GetTextBoxValue("txtSalesReportPCSBID"))));
                    zzz_SalesReportExclusion = _zzzSalesReportExclusion.SingleOrDefault<zzzSalesReportExclusion>(p => p.PracticeCatalogSupplierBrandID.Equals(Convert.ToInt32(row.Cells[1].GetSafeIntFromLabel("lblSalesReportPCSBID"))));

                    visionDB.zzzSalesReportExclusions.Attach(zzz_SalesReportExclusion);

                    zzz_SalesReportExclusion.SupplierShortName = row.Cells[1].GetTextBoxValue("txtSalesReportSupplierShortName");
                    zzz_SalesReportExclusion.BrandShortName = row.Cells[1].GetTextBoxValue("txtBrandShortName");
                    zzz_SalesReportExclusion.IsExcluded = Convert.ToBoolean(row.Cells[1].GetCheckBoxValue("chkIsExcluded"));

                    visionDB.SubmitChanges();
                    gvSalesReportExclusions.EditIndex = -1;
                }
            }
        }

        protected void gvSalesReportExclusions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSalesReportExclusions.PageIndex = e.NewPageIndex;
            gvSalesReportExclusions.DataBind();

            lblMessage.Text = "";
        }

        protected void gvSalesReportExclusions_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            visionDB = VisionDataContext.GetVisionDataContext();
            
            using (visionDB)
            {

                GridViewRow row = gvSalesReportExclusions.Rows[e.RowIndex];
                int salesReportExclusionsID = row.Cells[0].GetSafeIntFromLabel("lblSalesReportPCSBID");

                zzzSalesReportExclusion zzz_SalesReportExclusion = null;
                zzz_SalesReportExclusion = _zzzSalesReportExclusion.SingleOrDefault<zzzSalesReportExclusion>(p => p.PracticeCatalogSupplierBrandID.Equals(Convert.ToInt32(row.Cells[0].GetSafeIntFromLabel("lblSalesReportPCSBID"))));
                visionDB.zzzSalesReportExclusions.Attach(zzz_SalesReportExclusion); 

                visionDB.zzzSalesReportExclusions.DeleteOnSubmit(zzz_SalesReportExclusion);

                visionDB.SubmitChanges();
                gvSalesReportExclusions.EditIndex = -1;
            }
        }

        #endregion

        #region gvPracticeCatalogSupplierBrand event
        protected void gvPracticeCatalogSupplierBrand_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPracticeCatalogSupplierBrand.PageIndex = e.NewPageIndex;
            gvPracticeCatalogSupplierBrand.DataBind();
        }
     
        protected void gvPracticeCatalogSupplierBrand_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvPracticeCatalogSupplierBrand.PageIndex = 0;
        }

        protected void gvPracticeCatalogSupplierBrand_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearAddNewFields();
            btnExclude.Enabled = true;
            btnToSalesReportExclutions.Enabled = true;
            PracticeCatalogSupplierBrandSelectedRowColor();
        }
        #endregion
    }
}
