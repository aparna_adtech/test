﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PracticeFees.aspx.cs" Inherits="BregVision.BregAdmin.PracticeFees" %>
<%@ Register Assembly="RadCalendar.Net2" Namespace="Telerik.WebControls" TagPrefix="radCal" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <link runat="server" rel="Stylesheet" type="text/css" href="~/Admin/Styles/AdminMasterStyle.css" />
</head>

<body>
    <form id="form1" runat="server">
    <div>
            <table cellpadding="0" cellspacing="2" border="0">
                <tr>
                    <td colspan="4" class="AdminSubTitle">Set Fees</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td class="TextMed">
                        Fee Level
                    </td>
                    <td class="TextMed">
                        Start Date
                    </td>
                    <td class="TextMed">
                        Discount %
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnAddNew" runat="server" Text="Add New" 
                            onclick="btnAddNew_Click" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlFeeLevel" runat="server"></asp:DropDownList>
                    </td>
                    <td>
                        <radCal:RadDatePicker ID="dtFrom" runat="server" Width="75px" Font-Size="X-Small" DateInput-DisplayDateFormat="MM/dd/yy" ToolTip="Dates can be entered in the following formats: 01/01/2016, January 1,2016, 01012016">
                            <Calendar ID="Calendar1" runat="server" Skin="WebBlue"></Calendar>
                         </radCal:RadDatePicker>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtDiscount" Width="100px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvDiscount" runat="server" ControlToValidate="txtDiscount" ErrorMessage="*"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="cvDiscount" runat="server" ControlToValidate="txtDiscount" ValueToCompare="1" Type="Double" Operator="LessThan" ErrorMessage="%"></asp:CompareValidator> 
                    </td>
                </tr>
                <tr>
                    <td colspan="4"><br />
                        <asp:GridView runat="server" ID="gvPracticeFees" AutoGenerateColumns="false" CssClass="FieldLabel" GridLines="Both" Width="350px">
                            <Columns>
                                <asp:BoundField DataField="LevelName" HeaderText="Level Name" />
                                <asp:BoundField DataField="StartDate" HeaderText="Start Date" DataFormatString="{0:M-dd-yyyy}" />
                                <asp:BoundField DataField="Discount" HeaderText="Discount" DataFormatString="{0:N2}" />
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <br />
    </div>
    </form>
</body>
</html>
