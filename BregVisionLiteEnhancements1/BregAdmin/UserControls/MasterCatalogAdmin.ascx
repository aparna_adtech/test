﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MasterCatalogAdmin.ascx.cs" Inherits="BregVision.BregAdmin.UserControls.MasterCatalogAdmin" %>
<%@ Register Src="~/Admin/UserControls/ProductReplacement.ascx" TagName="ProductReplacement" TagPrefix="bva" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="grdSuppliers">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdSuppliers" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" InitialDelayTime="500" EnableAjaxSkinRendering="True" BackgroundPosition="Center" Skin="Breg" EnableEmbeddedSkins="False">
    <img src="../App_Themes/Breg/images/Common/loading.gif" />
  </telerik:RadAjaxLoadingPanel>
<table id="tblMasterTable" runat="server" border="1" cellpadding="0" cellspacing="0" summary="Summary Description" width="855px" style="margin: 0 auto;">
    <tr>
        <td align="left" style="color: Blue; width: auto">
            <asp:Label ID="Label2" runat="server" Font-Size="XX-Small" Text=""></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <!-- Content Header-->
                <tr align="center" class="AdminPageTitle">
                    <td>
                        <asp:Label ID="lbl" runat="server" Text="Master Catalog Supplier"></asp:Label>
                        <bvu:Help ID="help1" runat="server" HelpID="127" />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <!-- Grid -->
                        <bvu:VisionRadGridAjax AutoGenerateColumns="False" ID="grdSuppliers" 
                        DataSourceID="LinqDataSource1" 
                        OnDetailTableDataBind="grdSuppliers_OnDetailTableDataBind" 
                        OnInsertCommand="grdSuppliers_InsertCommand" 
                        OnUpdateCommand="grdSuppliers_UpdateCommand" 
                        OnItemCommand="grdSuppliers_ItemCommand" 
                        OnItemDataBound="grdSuppliers_ItemDataBound"
                        EnableViewState="true"
                        AllowFilteringByColumn="True" AllowPaging="True"
                        AllowSorting="True" runat="server" ClientSettings-Selecting-AllowRowSelect="true"
                            Skin="Vision" EnableEmbeddedSkins="False">
                        <PagerStyle Mode="NextPrevAndNumeric" />
                        <GroupingSettings CaseSensitive="false" />
                        <MasterTableView AllowFilteringByColumn="false" DataKeyNames="SupplierID" >
                            <Columns>
                                <telerik:GridBoundColumn HeaderText="Brand" DataField="Brand" UniqueName="Brand" 
                                    SortExpression="Brand" ShowFilterIcon="false" />
                                <telerik:GridBoundColumn HeaderText="Name" DataField="Name" UniqueName="Name"  
                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" />
                                <telerik:GridNumericColumn DataField="SupplierID" UniqueName="SupplierID" Display="false" />

                            </Columns>
                            <DetailTables>
                                <telerik:GridTableView runat="server" Name="tblProduct" DataKeyNames="SupplierID,MasterCatalogProductID" DataSourceID="LinqDataSource2"  AllowFilteringByColumn="true" CommandItemDisplay="Top">
                                    <ParentTableRelation>
                                        <telerik:GridRelationFields DetailKeyField="SupplierID" MasterKeyField="SupplierID" />
                                    </ParentTableRelation>
                                    <Columns>
                                        <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" />

                                        <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" ImageUrl="~/App_Themes/Breg/images/Common/delete.png"
                                            UniqueName="btnDelete" Visible="true">
                                        </telerik:GridButtonColumn>
                                        <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Discontinued"
                                            UniqueName="btnIsDiscontinued" Text="Discontinue" ConfirmDialogType="Classic" ConfirmText="Set this product to Discontinued?" Visible="true">
                                        </telerik:GridButtonColumn>
                                        <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" ImageUrl="~/App_Themes/Breg/images/Common/edit.png"
                                            UniqueName="btnEdit">
                                        </telerik:GridButtonColumn>

                                        <telerik:GridBoundColumn HeaderText="ShortName" DataField="ShortName" UniqueName="ShortName" SortExpression="ShortName" 
                                            HeaderStyle-Width="150px" FilterControlWidth="150px"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true" />

                                        <telerik:GridBoundColumn HeaderText="Code" DataField="Code" UniqueName="Code" SortExpression="Code" 
                                            HeaderStyle-Width="90px" FilterControlWidth="90px"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true" />

                                        <telerik:GridBoundColumn HeaderText="UPCCode" DataField="UPCCode" UniqueName="UPCCode" SortExpression="UPCCode" 
                                            HeaderStyle-Width="90px" FilterControlWidth="90px"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true" />


                                        <telerik:GridBoundColumn HeaderText="Packaging" DataField="Packaging" UniqueName="Packaging" SortExpression="Packaging" 
                                            HeaderStyle-Width="40px" FilterControlWidth="40px"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" />

                                        <telerik:GridBoundColumn HeaderText="LeftRightSide" DataField="LeftRightSide" UniqueName="LeftRightSide" SortExpression="LeftRightSide" 
                                            HeaderStyle-Width="40px" FilterControlWidth="40px"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" />

                                        <telerik:GridBoundColumn HeaderText="Mod" DataField="Mod1" UniqueName="Mod1" SortExpression="Mod1" 
                                            HeaderStyle-Width="40px" FilterControlWidth="40px"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" />

                                        <telerik:GridBoundColumn HeaderText="Mod" DataField="Mod2" UniqueName="Mod2" SortExpression="Mod2" 
                                            HeaderStyle-Width="40px" FilterControlWidth="40px"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" />

                                        <telerik:GridBoundColumn HeaderText="Mod" DataField="Mod3" UniqueName="Mod3" SortExpression="Mod3" 
                                            HeaderStyle-Width="40px" FilterControlWidth="40px"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" />

                                        <telerik:GridBoundColumn HeaderText="Size" DataField="Size" UniqueName="Size" SortExpression="Size" 
                                            HeaderStyle-Width="40px" FilterControlWidth="40px"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" />

                                        <telerik:GridBoundColumn HeaderText="Color" DataField="Color" UniqueName="Color" SortExpression="Color" 
                                            HeaderStyle-Width="70px" FilterControlWidth="70px"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" />

                                        <telerik:GridBoundColumn HeaderText="HCPCs" DataField="HCPCSString" UniqueName="HCPCSString"  
                                            HeaderStyle-Width="70px" FilterControlWidth="70px" ShowFilterIcon="false" />

                                        <telerik:GridBoundColumn HeaderText="HCPCsOts" DataField="HCPCSStringOts" UniqueName="HCPCSStringOts"  
                                            HeaderStyle-Width="70px" FilterControlWidth="70px" ShowFilterIcon="false" Display="False" />

                                        <telerik:GridBoundColumn HeaderText="WholesaleCost" DataField="WholesaleCost" UniqueName="WholesaleCost"  
                                            HeaderStyle-Width="70px" FilterControlWidth="70px" ShowFilterIcon="false" DataFormatString="{0:c}" />

                                        <telerik:GridCheckBoxColumn HeaderText="Logo Part" DataField="IsLogoAblePart" UniqueName="IsLogoAblePart"
                                            HeaderStyle-Width="70px" FilterControlWidth="70px" ShowFilterIcon="false" />

										<telerik:GridTemplateColumn UniqueName="btnProductReplacement" HeaderText="Replaced" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"> 
											<ItemTemplate> 
												<asp:ImageButton ID="ProductReplacementActive" CommandName="Edit" ImageUrl="~/App_Themes/Breg/images/Common/replace-active.png"
													Visible='<%# Convert.ToBoolean(Eval("IsReplaced")) %>' ImageAlign="Middle" runat="server"></asp:ImageButton>
												<asp:ImageButton ID="ProductReplacementInactive" CommandName="Edit" ImageUrl="~/App_Themes/Breg/images/Common/replace-inactive.gif"
													Visible='<%# !Convert.ToBoolean(Eval("IsReplaced")) %>' ImageAlign="Middle" runat="server"></asp:ImageButton>
											</ItemTemplate> 
										</telerik:GridTemplateColumn>
										
                                        <telerik:GridNumericColumn DataField="MastercatalogProductID" UniqueName="MastercatalogProductID" Display="false" />

                                        <telerik:GridNumericColumn DataField="SupplierID" UniqueName="SupplierID" Display="false" />

                                        <telerik:GridNumericColumn DataField="MasterCatalogSubCategoryID" UniqueName="MasterCatalogSubCategoryID" Display="false" />
                                    </Columns>
                                    <EditFormSettings EditFormType="Template">
                                        <EditColumn UniqueName="EditCommandColumn1">
                                        </EditColumn>
                                        <FormTemplate>
											<div class="flex-row" style='<%=!ShowProductReplacement ? "" : "display: none" %>'>
												<table id="Table2" border="1" cellpadding="1" cellspacing="2" rules="none" style="border-collapse: collapse">
													<tr id="trPromoteHeader" runat="server">
														<td colspan="1">
																Category:
														</td>
														<td colspan="9" align="left">
															Sub-Category:
														</td>
													</tr>
													<tr id="trPromote" runat="server">
														<td colspan="1">
															<asp:DropDownList ID="ddlCategoriesMaster" runat="server" EnableViewState="true" OnSelectedIndexChanged="ddlCategoriesMaster_OnSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
														</td>
														<td colspan="9" align="left">
															<asp:DropDownList ID="ddlSubCategoriesMaster" runat="server" EnableViewState="true" OnLoad="ddlSubCategoriesMaster_OnLoad"></asp:DropDownList>
														</td>
													</tr>
													<tr>
														<td colspan="10">
															<asp:Label ID="lblUserMessageMaster" runat="server" ForeColor="Red" Text='<%# Eval( "UserMessage") %>'>
															</asp:Label>
														</td>
													</tr>
													<tr>
														<td colspan="10">
															<asp:ValidationSummary ID="validationSummaryMaster" runat="server" HeaderText="Please correct the following:"
																Visible="true" />
														</td>
													</tr>
													<tr>
														<td>
															Product:
														</td>
														<td colspan="3" nowrap="nowrap">
															<asp:TextBox ID="txtProductMaster" runat="server" MaxLength="50" Text='<%# Eval( "ShortName") %>'
																Width="250px">
															</asp:TextBox>
															<asp:RequiredFieldValidator ID="RequiredFieldValidator_txtProductMaster" runat="server"
																ControlToValidate="txtProductMaster" ErrorMessage="Product is a required field." Font-Overline="true"
																SetFocusOnError="true" Text="*">
															</asp:RequiredFieldValidator>
														</td>
														<td>
															Code:
														</td>
														<td colspan="1" nowrap="nowrap">
															<asp:TextBox ID="txtCodeMaster" runat="server" MaxLength="20" Text='<%# Eval( "Code") %>'
																Width="150px">
															</asp:TextBox>
															<asp:RequiredFieldValidator ID="RequiredFieldValidator_txtCodeMaster" runat="server" ControlToValidate="txtCodeMaster"
																ErrorMessage="Code is a required field." SetFocusOnError="true" Text="*">
															</asp:RequiredFieldValidator>
														</td>
														<td>
															Pkg:
														</td>
														<td nowrap="nowrap">
															<asp:TextBox ID="txtPackagingMaster" runat="server" MaxLength="10" Text='<%# Eval( "Packaging") %>'
																Width="50">
															</asp:TextBox>
															<asp:RequiredFieldValidator ID="RequiredFieldValidator_txtPackaging" runat="server"
																ControlToValidate="txtPackagingMaster" ErrorMessage="Packaging is a required field."
																SetFocusOnError="true" Text="*">
															</asp:RequiredFieldValidator>
														</td>
														<td>
															Logo Part:
														</td>
														<td>
															<asp:CheckBox ID="chkIsLogoAblePartMaster" runat="server" Checked='<%# Eval( "IsLogoAblePart") is System.DBNull ? false : Eval("IsLogoAblePart") %>' />
														</td>
													</tr>
													<tr>
														<td>
															Side:
														</td>
														<td nowrap="nowrap">
															<asp:TextBox ID="txtSideMaster" runat="server" MaxLength="20" Text='<%# Eval( "Side") %>'
																Width="70px">
															</asp:TextBox>
														</td>
														<td>
															Mod:
														</td>
														<td colspan="1" nowrap="nowrap">
															<asp:DropDownList ID="ddlMod1" runat="server" CssClass="TextSml" SelectedValue='<%# Eval( "Mod1") %>'>
																<asp:ListItem Text="" Value=""></asp:ListItem>
																<asp:ListItem Text="CG" Value="CG"></asp:ListItem>
																<asp:ListItem Text="EY" Value="EY"></asp:ListItem>
																<asp:ListItem Text="GA" Value="GA"></asp:ListItem>
																<asp:ListItem Text="GX" Value="GX"></asp:ListItem>
																<asp:ListItem Text="GY" Value="GY"></asp:ListItem>
																<asp:ListItem Text="GZ" Value="GZ"></asp:ListItem>
																<asp:ListItem Text="KV" Value="KV"></asp:ListItem>
																<asp:ListItem Text="KX" Value="KX"></asp:ListItem>
																<asp:ListItem Text="NU" Value="NU"></asp:ListItem>
																<asp:ListItem Text="RA" Value="RA"></asp:ListItem>
																<asp:ListItem Text="RB" Value="RB"></asp:ListItem>
																<asp:ListItem Text="RR" Value="RR"></asp:ListItem>
															</asp:DropDownList>
														</td>
														<td>
															Mod:
														</td>
														<td colspan="1" nowrap="nowrap">
															<asp:DropDownList ID="ddlMod2" runat="server" CssClass="TextSml" SelectedValue='<%# Eval( "Mod2") %>'>
																<asp:ListItem Text="" Value=""></asp:ListItem>
																<asp:ListItem Text="CG" Value="CG"></asp:ListItem>
																<asp:ListItem Text="EY" Value="EY"></asp:ListItem>
																<asp:ListItem Text="GA" Value="GA"></asp:ListItem>
																<asp:ListItem Text="GX" Value="GX"></asp:ListItem>
																<asp:ListItem Text="GY" Value="GY"></asp:ListItem>
																<asp:ListItem Text="GZ" Value="GZ"></asp:ListItem>
																<asp:ListItem Text="KV" Value="KV"></asp:ListItem>
																<asp:ListItem Text="KX" Value="KX"></asp:ListItem>
																<asp:ListItem Text="NU" Value="NU"></asp:ListItem>
																<asp:ListItem Text="RA" Value="RA"></asp:ListItem>
																<asp:ListItem Text="RB" Value="RB"></asp:ListItem>
																<asp:ListItem Text="RR" Value="RR"></asp:ListItem>
															</asp:DropDownList>
														</td>
														<td>
															Mod:
														</td>
														<td colspan="1" nowrap="nowrap">
															<asp:DropDownList ID="ddlMod3" runat="server" CssClass="TextSml" SelectedValue='<%# Eval( "Mod3") %>'>
																<asp:ListItem Text="" Value=""></asp:ListItem>
																<asp:ListItem Text="CG" Value="CG"></asp:ListItem>
																<asp:ListItem Text="EY" Value="EY"></asp:ListItem>
																<asp:ListItem Text="GA" Value="GA"></asp:ListItem>
																<asp:ListItem Text="GX" Value="GX"></asp:ListItem>
																<asp:ListItem Text="GY" Value="GY"></asp:ListItem>
																<asp:ListItem Text="GZ" Value="GZ"></asp:ListItem>
																<asp:ListItem Text="KV" Value="KV"></asp:ListItem>
																<asp:ListItem Text="KX" Value="KX"></asp:ListItem>
																<asp:ListItem Text="NU" Value="NU"></asp:ListItem>
																<asp:ListItem Text="RA" Value="RA"></asp:ListItem>
																<asp:ListItem Text="RB" Value="RB"></asp:ListItem>
																<asp:ListItem Text="RR" Value="RR"></asp:ListItem>
															</asp:DropDownList>
														</td>
													</tr>
													<tr>
														 <td>
															Size:
														</td>
														<td nowrap="nowrap">
															<asp:TextBox ID="txtSizeMaster" runat="server" MaxLength="10" Text='<%# Eval( "Size") %>'
																Width="70px">
															</asp:TextBox>
														</td>
														<td>
															Gender:
														</td>
														<td nowrap="nowrap">
															<asp:TextBox ID="txtGenderMaster" runat="server" MaxLength="10" Text='<%# Eval( "Gender") %>'
																Width="70px">
															</asp:TextBox>
														</td>
														<td>
															Color:
														</td>
														<td nowrap="nowrap">
															<asp:TextBox ID="txtColorMaster" runat="server" MaxLength="10" Text='<%# Eval( "Color") %>'
																Width="70px">
															</asp:TextBox>
														</td>
														<td colspan="2"></td>
													</tr>
													<tr>
														<div id="divHCPCs" runat="server">
															<td>
																HCPCs:
															</td>
															<td colspan="3" nowrap="nowrap">
																<asp:TextBox ID="txtHCPCSStringMaster" runat="server" MaxLength="50" Text='<%# Eval( "HCPCSString") %>'
																	Visible="true" Width="250px">
																</asp:TextBox>
																<asp:RegularExpressionValidator ID="RegularExpressionValidator2Master" runat="server" ControlToValidate="txtHCPCSStringMaster"
																	ErrorMessage="Each individual HCPC must be longer than 10 characters.<br/>HCPCs must be seperated by commas.<br/>Please remove any leading or trailing commas."
																	SetFocusOnError="true" Text="*" ValidationExpression="^()*([\s?])*([\w?\$?\.?\s?]{1,4})+([\s?])*([,]([\s?])*([\w?\$?\.?\s?]{1,4})+([\s?])*)*$"
																	Visible="true">
																</asp:RegularExpressionValidator>
															</td>
														</div>
														<td>
															Wholesale Cost:
														</td>
														<td colspan="1" nowrap="nowrap">
															<asp:TextBox ID="txtWholesaleCostMaster" runat="server" MaxLength="9" Text='<%# Eval( "WholesaleCost", "{0:c}") %>'
																Width="70px">
															</asp:TextBox>
															<asp:RegularExpressionValidator ID="RegularExpressionValidator1Master" runat="server" ControlToValidate="txtWholesaleCostMaster"
																ErrorMessage="Cost must be a monetary value." SetFocusOnError="true" Text="*"
																ValidationExpression="^\$?([1-9]{1}[0-9]{0,2}(\,[0-9]{3})*(\.[0-9]{0,2})?|[1-9]{1}[0-9]{0,}(\.[0-9]{0,2})?|0(\.[0-9]{0,2})?|(\.[0-9]{1,2})?)$">
															</asp:RegularExpressionValidator>
														</td>
														<td>
															UPC Code:
														</td>
														<td>
															<asp:TextBox ID="txtUPCCodeMaster" runat="server" MaxLength="25" Text='<%# Eval( "UPCCode") %>'
																Width="70px">
															</asp:TextBox>
														</td>
														<td colspan="2"></td>
													</tr>
													<tr>
													   <div id="divHCPCsOts" runat="server">
															<td>
																HCPCs-Off-The-Shelf:
															</td>
															<td colspan="3" nowrap="nowrap">
																<asp:TextBox ID="txtHcpcsStringOtsMaster" Enabled="False" runat="server" MaxLength="50" Text='<%# Eval( "HCPCSStringOts") %>'
																	Visible="true" Width="250px">
																</asp:TextBox>
																<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtHCPCSStringOtsMaster"
																	ErrorMessage="Each individual HCPC must be longer than 10 characters.<br/>HCPCs must be seperated by commas.<br/>Please remove any leading or trailing commas."
																	SetFocusOnError="true" Text="*" ValidationExpression="^()*([\s?])*([\w?\$?\.?\s?]{1,4})+([\s?])*([,]([\s?])*([\w?\$?\.?\s?]{1,4})+([\s?])*)*$"
																	Visible="true">
																</asp:RegularExpressionValidator>
															</td>
														</div>

														<td align="left" colspan="10">
															<asp:Button ID="btnUpdateMaster" runat="server" CommandName='<%# (Container as GridItem).OwnerTableView.IsItemInserted ? "PerformInsert" : "Update" %>'
																Text='<%# (Container as GridItem).OwnerTableView.IsItemInserted ? "Insert" : "Update" %>' />&nbsp;
															<asp:Button ID="btnCancelMaster" runat="server" CausesValidation="False" CommandName="Cancel"
																Text="Cancel" />
														</td>
													</tr>
												</table>
											</div>
											<div class="flex-row" style='border: thin solid black; <%=ShowProductReplacement ? "" : "display: none" %>'>
												<bva:ProductReplacement IsPracticeSpecific='<%# IsPracticeSpecific %>' MasterCatalogProductID='<%# Eval("MasterCatalogProductID") == DBNull.Value ? 0 : Eval("MasterCatalogProductID") %>' OnWasClickedOn="ProductReplacementWasClickedOn" runat="server"/>
											</div>
                                        </FormTemplate>
                                    </EditFormSettings>
                                </telerik:GridTableView>
                            </DetailTables>
                        </MasterTableView>
                        </bvu:VisionRadGridAjax>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
    
    <asp:LinqDataSource AutoPage="true" ID="LinqDataSource1" runat="server" ContextTypeName="ClassLibrary.VisionDataContext" onselecting="LinqDataSource1_Selecting">
    </asp:LinqDataSource> 
    <asp:LinqDataSource AutoPage="true" ID="LinqDataSource2" runat="server" ContextTypeName="ClassLibrary.VisionDataContext" onselecting="LinqDataSource2_Selecting">
    </asp:LinqDataSource> 
