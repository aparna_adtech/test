﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary.DAL;
using Telerik.Web.UI;

namespace BregVision.BregAdmin.UserControls
{
    public class ProductDisplay
    {
		public int SupplierID{get;set;}
		public int MasterCatalogProductID{get;set;}
		public int MasterCatalogSubCategoryID{get;set;}
		public string Code{get;set;}
		public string ShortName{get;set;}
		public string Packaging{get;set;}
		public string LeftRightSide{get;set;}
		public string Side{get;set;}
		public string Mod1{get;set; }
		public string Mod2 { get; set; }
		public string Mod3 { get; set; }
		public string Size{get;set;}
		public string Color{get;set;}
		public string Gender{get;set;}
		public decimal? WholesaleListCost{get;set;}
		public string UPCCode{get;set;}
		public decimal? WholesaleCost{get;set;}
		public string HCPCSString{get;set;}
		public string HCPCSStringOts { get; set; }
		public string UserMessage{get;set;}
		public bool IsDiscontinued{get;set;}
		public bool IsLogoAblePart { get; set; }
		public bool IsReplaced { get; set; }
	}

	public partial class MasterCatalogAdmin : Bases.UserControlBase
    {
		public event EventHandler WasClickedOn;

		public global::BregVision.UserControls.VisionRadGridAjax grdSuppliers;

		//private void InitializeAjaxSettings(DropDownList ddl)
		//{
		//    Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);

		//    radAjaxManager.AjaxSettings.AddAjaxSetting(grdSuppliers, ddl);

		//}

		protected void ProductReplacementWasClickedOn(object sender, EventArgs e)
		{
			if (WasClickedOn != null)
				WasClickedOn(this, e);
		}

		public bool EnableProductReplacement
		{
			get
			{
				return _EnableProductReplacement;
			}
			set
			{
				_EnableProductReplacement = value;
			}
		}
		private bool _EnableProductReplacement = true;

		public bool IsPracticeSpecific
		{
			get
			{
				return _IsPracticeSpecific;
			}
			set
			{
				_IsPracticeSpecific = value;
			}
		}
		private bool _IsPracticeSpecific = true;
		protected bool ShowProductReplacement
		{
			get
			{
				if (!EnableProductReplacement)
					return false;

				object productReplacement = this.Page.Session[this.ClientID + "_ProductReplacement"];
				if (productReplacement is bool)
					return (bool)productReplacement;
				return false;
			}
			set
			{
				this.Page.Session[this.ClientID + "_ProductReplacement"] = value;
			}
		}

		protected void LinqDataSource1_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            if (Cache["Suppliers"] != null)
            {
                e.Result = Cache["Suppliers"];

            }
            else
            {
                VisionDataContext context = VisionDataContext.GetVisionDataContext();
                using (context)
                {
                    var query = from mcs in context.MasterCatalogSuppliers
                                where mcs.IsActive == true
                                select new
                                {
                                    Brand = mcs.SupplierName,
                                    SupplierID = mcs.MasterCatalogSupplierID,
                                    Name = mcs.SupplierShortName
                                };

                    var orderedQuery = query.OrderBy(a => a.SupplierID).ToList();

                    Cache["Suppliers"] = orderedQuery;

                    e.Result = orderedQuery;
                }
            }
        }

        private static string LinqDataSource2CachePrefix = @"MasterCatalogAdmin_LinqDataSource2_Selecting";
        protected void LinqDataSource2_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            string key = LinqDataSource2.WhereParameters[0].DefaultValue;
            int supplierID = 0;
            int.TryParse(key, out supplierID);

            if (supplierID > 0)
            {
                var cacheKey = string.Format("{0}_{1}", LinqDataSource2CachePrefix, supplierID);
                if (Cache[cacheKey] != null)
                {
                    e.Result = Cache[cacheKey];
                    return;
                }

                VisionDataContext context = VisionDataContext.GetVisionDataContext();
                using (context)
                {
                    var query = from mcp in context.MasterCatalogProducts
                                join mcsc in context.MasterCatalogSubCategories on mcp.MasterCatalogSubCategoryID equals mcsc.MasterCatalogSubCategoryID
                                join mcc in context.MasterCatalogCategories on mcsc.MasterCatalogCategoryID equals mcc.MasterCatalogCategoryID
                                join mcs in context.MasterCatalogSuppliers on mcc.MasterCatalogSupplierID equals mcs.MasterCatalogSupplierID
								where mcp.IsActive == true
									&& mcsc.IsActive == true
									&& mcc.IsActive == true
									&& mcs.IsActive == true
									&& mcs.MasterCatalogSupplierID == supplierID
                                orderby mcp.ShortName
                                orderby mcp.Code
								select new ProductDisplay
                                {
                                    SupplierID = mcs.MasterCatalogSupplierID,
                                    MasterCatalogProductID = mcp.MasterCatalogProductID,
                                    MasterCatalogSubCategoryID = mcp.MasterCatalogSubCategoryID,
                                    Code = mcp.Code,
                                    ShortName = mcp.ShortName,
                                    Packaging = mcp.Packaging,
                                    LeftRightSide = mcp.LeftRightSide,
                                    Side = mcp.LeftRightSide,
                                    Size = mcp.Size,
                                    Mod1 = mcp.Mod1,
                                    Mod2 = mcp.Mod2,
                                    Mod3 = mcp.Mod3,
                                    Color = mcp.Color,
                                    Gender = mcp.Gender,
                                    WholesaleListCost = mcp.WholesaleListCost,
                                    //UPCCode = context.ConcatMasterUPCCodes(mcp.MasterCatalogProductID), //mcp.UPCCode,
                                    WholesaleCost = mcp.WholesaleListCost,
                                    //HCPCSString = context.ConcatBregHCPCS(mcp.MasterCatalogProductID),
                                    //HCPCSStringOts = context.ConcatBregHCPCSOTS(mcp.MasterCatalogProductID),
                                    UserMessage = "",
                                    IsDiscontinued = mcp.IsDiscontinued,
                                    IsLogoAblePart = mcp.IsLogoAblePart,
									IsReplaced =
										context.ProductReplacements
											.Any(x =>
												x.OldMasterCatalogProductID == mcp.MasterCatalogProductID
												&& x.IsActive
												&& ((IsPracticeSpecific && x.PracticeID != null) || (!IsPracticeSpecific && x.PracticeID == null)))
                                };

                    var results = query.ToList();

                    var mcpIds = results.Select(x => x.MasterCatalogProductID).ToList();

                    // populate UPCCode
                    var upcCodeDict =
                        context.UPCCodes.Where(x => x.IsActive)
                        .Join(context.MasterCatalogProducts.Where(x => x.MasterCatalogSupplierID == supplierID), k => k.MasterCatalogProductID, k => k.MasterCatalogProductID, (code, mcp) => new { code.Code, code.MasterCatalogProductID, code.IsActive, mcp.MasterCatalogSupplierID } )
                        .Select(x => new { x.MasterCatalogProductID, x.Code })
                        .AsEnumerable()
                        .Where(x => mcpIds.Contains(x.MasterCatalogProductID.Value))
                        .GroupBy(k => k.MasterCatalogProductID, v => v.Code)
                        .ToDictionary(k => k.Key, v => String.Join(", ", v));

                    // populate HCPCSString
                    var hcpcsAll =
                        context.BregProductHCPCs.Where(x => x.IsActive)
                        .Join(context.MasterCatalogProducts.Where(x => x.MasterCatalogSupplierID == supplierID), k => k.MasterCatalogProductID, k => k.MasterCatalogProductID, (hcpcs, mcp) => new { hcpcs.HCPCS, hcpcs.MasterCatalogProductID, hcpcs.IsOffTheShelf, hcpcs.IsActive, mcp.MasterCatalogSupplierID })
                        .Select(x => new { x.MasterCatalogProductID, x.HCPCS, x.IsOffTheShelf })
                        .AsEnumerable()
                        .Where(x => mcpIds.Contains(x.MasterCatalogProductID));
                    var hcpcsDict =
                        hcpcsAll
                        .Where(x => x.IsOffTheShelf == false)
                        .GroupBy(k => k.MasterCatalogProductID, v => v.HCPCS)
                        .ToDictionary(k => k.Key, v => String.Join(", ", v));
                    var hcpcsOtsDict =
                        hcpcsAll
                        .Where(x => x.IsOffTheShelf == true)
                        .GroupBy(k => k.MasterCatalogProductID, v => v.HCPCS)
                        .ToDictionary(k => k.Key, v => String.Join(", ", v));

                    // update results
                    results.ForEach(x =>
                    {
                        x.UPCCode = upcCodeDict.ContainsKey(x.MasterCatalogProductID) ? upcCodeDict[x.MasterCatalogProductID] : string.Empty;
                        x.HCPCSString = hcpcsDict.ContainsKey(x.MasterCatalogProductID) ? hcpcsDict[x.MasterCatalogProductID] : string.Empty;
                        x.HCPCSStringOts = hcpcsOtsDict.ContainsKey(x.MasterCatalogProductID) ? hcpcsOtsDict[x.MasterCatalogProductID] : string.Empty;
                    });

                    Cache.Add(cacheKey, results, null, DateTime.Now.AddMinutes(5), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Normal, null);
                    e.Result = results;
                }
            }
        }

        protected void grdSuppliers_ItemCommand(object source, GridCommandEventArgs e)
        {
			if (this.WasClickedOn != null)
				this.WasClickedOn(this, EventArgs.Empty);

			ShowProductReplacement = false;
			switch (e.CommandName.ToLower())
            {
				case "edit":
					{
						if (e.CommandSource is System.Web.UI.Control)
						{
							System.Web.UI.Control editControl = (System.Web.UI.Control)(e.CommandSource);
							if (editControl.ID == "ProductReplacementActive" || editControl.ID == "ProductReplacementInactive")
							{
								//if (productReplacementMode)
								//{
								//	e.Canceled = true;
								//	grdSuppliers.MasterTableView.ClearEditItems();
								//}
								//else
								//{
									ShowProductReplacement = true;
								//	}
								//}
							}
						}
					}
					break;
                case "delete":
                    break;
                case "discontinued":
                    SetItemDiscontinued(e);
                    break;
                default:
                    break;
            }
        }

        protected void grdSuppliers_ItemDataBound(object sender, GridItemEventArgs e)
        {
            //<%--ImageUrl='<%# Convert.ToBoolean(Eval("IsDiscontinued")) == true ? "~/RadControls/Grid/Skins/Office2007/Lock.png" : "~/RadControls/Grid/Skins/Office2007/unlock.png" %>'--%>
            if (e.Item.OwnerTableView.Name == "tblProduct")
            {
                if(e.Item is GridDataItem)
                {
                    GridDataItem gridDataItem = e.Item as GridDataItem;
					ImageButton buttonColumn = gridDataItem["btnIsDiscontinued"].Controls[0] as ImageButton;
					if (buttonColumn != null)
					{
						ProductDisplay product = e.Item.DataItem as ProductDisplay;
						buttonColumn.ImageUrl = product.IsDiscontinued == true ? "~/RadControls/Grid/Skins/Office2007/Lock.png" : "~/RadControls/Grid/Skins/Office2007/unlock.png";
					}

					(e.Item.OwnerTableView.GetColumn("btnProductReplacement") as GridTemplateColumn).Visible = EnableProductReplacement;
				}
            }
        }


        private void SetItemDiscontinued(GridCommandEventArgs e)
        {
            Int32 masterCatalogProductID = GetMasterCatalogID(e);
            DAL.Practice.Catalog.DiscontinueMasterCatalogItem(masterCatalogProductID);
        }

        protected void grdSuppliers_OnDetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
        {
			string key = e.DetailTableView.ParentItem["SupplierID"].Text;
            int supplierID = 0;
            int.TryParse(key, out supplierID);
            if (supplierID > 0)
            {
                LinqDataSource2.WhereParameters.Clear();
                LinqDataSource2.WhereParameters.Add("SupplierID", supplierID.ToString());
            }
        }

        public void ddlCategoriesMaster_OnSelectedIndexChanged(object sender, EventArgs e)
        {

            DropDownList ddlCategoriesMaster = sender as DropDownList;
            int selectedCategoryID = Convert.ToInt32(ddlCategoriesMaster.SelectedValue);
            Control parent = ddlCategoriesMaster.Parent;

            LoadSubCategoriesForAddProduct(parent, selectedCategoryID);
        }

        protected void ddlSubCategoriesMaster_OnLoad(object sender, EventArgs e)
        {
            ProductDisplay product = null;
            Int32 supplierID = 0;
            int selectedSubcategory = 0;

            Control parent = (sender as DropDownList).Parent;

            if (parent.NamingContainer is GridEditFormInsertItem)
            {
                GridEditFormInsertItem parentInsertForm = parent.NamingContainer as GridEditFormInsertItem;

                supplierID = Convert.ToInt32(parentInsertForm.OwnerTableView.ParentItem.GetDataKeyValue("SupplierID")); // Convert.ToInt32(parentInsertForm.OwnerTableView.DataKeyValues[0]["SupplierID"]); ;
                product = parentInsertForm.DataItem as ProductDisplay;
            }
            else if (parent.NamingContainer is GridEditFormItem)
            {
                GridEditFormItem parentInsertForm = parent.NamingContainer as GridEditFormItem;

                supplierID = Convert.ToInt32(parentInsertForm.OwnerTableView.ParentItem.GetDataKeyValue("SupplierID")); // Convert.ToInt32(parentInsertForm.OwnerTableView.DataKeyValues[0]["SupplierID"]); ;
                product = parentInsertForm.DataItem as ProductDisplay;

            }

            if (product != null)
            {
                selectedSubcategory = product.MasterCatalogSubCategoryID;
            }

            Int32 selectedCategoryID = DAL.Practice.Catalog.GetMasterCatalogCategoryFromSubCategoryID(selectedSubcategory);

            LoadDropdownsForAddMasterProduct(parent, supplierID, selectedCategoryID, selectedSubcategory);

            

        }

        private void LoadDropdownsForAddMasterProduct(Control form, Int32 supplierID, Int32 selectedCategoryID, Int32 selectedSubcategoryID)
        {
            DropDownList ddlCategories = form.FindControl("ddlCategoriesMaster") as DropDownList;
            DropDownList ddlSubCategories = form.FindControl("ddlSubCategoriesMaster") as DropDownList;

            if (ddlCategories.Items.Count < 1)
            {
                //InitializeAjaxSettings(ddlCategories);
                List<GenericReturn> categories = DAL.Practice.Catalog.GetMasterCatalogSupplierCategories(supplierID);
                BindList(ddlCategories, categories);

                if (selectedCategoryID < 1 && categories.Count > 0)
                {
                    selectedCategoryID = categories[0].ID;
                }

                ListItem categoryItem = ddlCategories.Items.FindByValue(selectedCategoryID.ToString());
                if (categoryItem != null)
                {
                    ddlCategories.ClearSelection();
                    ddlCategories.SelectedValue = selectedCategoryID.ToString();
                    categoryItem.Selected = true;
                }

                List<GenericReturn> subCategories = DAL.Practice.Catalog.GetMasterCatalogSupplierSubCategories(selectedCategoryID);  //(categories[0].ID);
                BindList(ddlSubCategories, subCategories);

                ListItem subCategoryItem = ddlSubCategories.Items.FindByValue(selectedSubcategoryID.ToString());
                if (subCategoryItem != null)
                {
                    ddlSubCategories.ClearSelection();
                    ddlSubCategories.SelectedValue = selectedSubcategoryID.ToString();
                    subCategoryItem.Selected = true;
                }
            }            
        }

        private void LoadSubCategoriesForAddProduct(Control parent, int masterCatalogCategoryID)
        {
            List<GenericReturn> subCategories = DAL.Practice.Catalog.GetMasterCatalogSupplierSubCategories(masterCatalogCategoryID);

            DropDownList ddlSubCategoriesMaster = parent.FindControl("ddlSubCategoriesMaster") as DropDownList;

            BindList(ddlSubCategoriesMaster, subCategories);
        }

        private static void BindList(DropDownList ddl, List<GenericReturn> genericList)
        {
            ddl.DataSource = genericList;
            ddl.DataTextField = "Name";
            ddl.DataValueField = "ID";
            ddl.DataBind();
        }

        protected void grdSuppliers_InsertCommand(object source, GridCommandEventArgs e)
        {
			if (this.WasClickedOn != null)
				this.WasClickedOn(this, EventArgs.Empty);

			if (e.CommandName == "PerformInsert")
            {
                SaveToMasterCatalog(e, 0);
            }
        }

        protected void grdSuppliers_UpdateCommand(object source, GridCommandEventArgs e)
        {
			if (this.WasClickedOn != null)
				this.WasClickedOn(this, EventArgs.Empty);

			if (e.CommandName == "Update")
            {
                Int32 masterCatalogProductID = GetMasterCatalogID(e);

                SaveToMasterCatalog(e, masterCatalogProductID);
            }
        }

        private static Int32 GetMasterCatalogID(GridCommandEventArgs e)
        {
            GridEditableItem editItem = e.Item as GridEditableItem;

            Int32 masterCatalogProductID = 0; //get the mastercatalogproduct id               
            masterCatalogProductID = Convert.ToInt32(editItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["MasterCatalogProductID"]);
            return masterCatalogProductID;
        }

        private void SaveToMasterCatalog(GridCommandEventArgs e, Int32 masterCatalogProductID)
        {
			GridDataItem parentItem = (GridDataItem)(e.Item.OwnerTableView.ParentItem);
            if (parentItem != null)
            {
                // Get the Practice Catalog Supplier Brand ID from the parent Grid Table View.
                string pcsbID = parentItem.OwnerTableView.DataKeyValues[parentItem.ItemIndex]["SupplierID"].ToString();
                int SupplierID = Convert.ToInt32(pcsbID);

                string productName;
                string productCode;
                string packaging;
                string side;
                string size;
                string mod1;
                string mod2;
                string mod3;
                string gender;
                string color;
                string upcCode;
                decimal wholesaleCost;
                string hcpcsString;
                string hcpcsStringOts;
                bool isLogoAblePart = false;

                //Get the GridEditFormInsertItem of the RadGrid  
                GridEditFormItem insertedItem = (GridEditFormItem)e.Item;

                //Access the textbox from the insert form template and store the values in string variables.  
                //call method
                GetValuesFromInsertForm(insertedItem, out productName, out productCode, out packaging, out side, out mod1, out mod2, out mod3, out size, out gender, out color, out wholesaleCost, out hcpcsString, out hcpcsStringOts, out upcCode, out isLogoAblePart, true);


                int categoryID = 0;
                int subCategoryID = 0;

                GetSelectedMasterCategoryIds(out categoryID, out subCategoryID, insertedItem);

                //Save To Master Catalog
                DAL.Practice.Catalog.SaveToMasterCatalog(masterCatalogProductID, productName, productCode, packaging, side, mod1, mod2, mod3, size,
                                     gender, color, wholesaleCost, hcpcsString,hcpcsStringOts, SupplierID, categoryID, subCategoryID, false, upcCode, userID, isLogoAblePart);

                // purge cache
                Cache.Remove(string.Format("{0}_{1}", LinqDataSource2CachePrefix, SupplierID));
            }
        }

        private void GetSelectedIds(out int mcSupplierID, out int categoryID, out int subCategoryID, Control form)
        {
            DropDownList ddlSuppliers = form.FindControl("ddlSuppliers") as DropDownList;
            DropDownList ddlCategories = form.FindControl("ddlCategories") as DropDownList;
            DropDownList ddlSubCategories = form.FindControl("ddlSubCategories") as DropDownList;

            if (ddlSuppliers != null && ddlCategories != null && ddlSubCategories != null && ddlSubCategories.Items.Count > 0)
            {
                mcSupplierID = Convert.ToInt32(ddlSuppliers.SelectedValue);
                categoryID = Convert.ToInt32(ddlCategories.SelectedValue);
                subCategoryID = Convert.ToInt32(ddlSubCategories.SelectedValue);
                return;
            }
            throw new ApplicationException("Master Catalog Info not found");
        }

        private void GetSelectedMasterCategoryIds(out int categoryID, out int subCategoryID, Control form)
        {

            DropDownList ddlCategories = form.FindControl("ddlCategoriesMaster") as DropDownList;
            DropDownList ddlSubCategories = form.FindControl("ddlSubCategoriesMaster") as DropDownList;

            if (ddlCategories != null && ddlSubCategories != null && ddlSubCategories.Items.Count > 0)
            {

                categoryID = Convert.ToInt32(ddlCategories.SelectedValue);
                subCategoryID = Convert.ToInt32(ddlSubCategories.SelectedValue);
                return;
            }
            throw new ApplicationException("Master Catalog Categories Info not found");
        }


        protected void GetValuesFromUpdateForm
        (
            GridEditableItem editedItem, 
            out string productName, 
            out string productCode, 
            out string packaging,
            out string side,
            out string mod1,
            out string mod2,
            out string mod3,
            out string size, 
            out string gender, 
            out string color, 
            out decimal wholesaleCost, 
            out string HCPCSstring, 
            out string upcCode
        )
        {
            productName = (editedItem.FindControl("txtProduct") as TextBox).Text.Trim();
            productCode = (editedItem.FindControl("txtCode") as TextBox).Text.Trim();
            packaging = (editedItem.FindControl("txtPackaging") as TextBox).Text.Trim();
            side = (editedItem.FindControl("txtSide") as TextBox).Text.Trim();
            mod1 = (editedItem.FindControl("ddlMod1") as DropDownList).Text.Trim();
            mod2 = (editedItem.FindControl("ddlMod2") as DropDownList).Text.Trim();
            mod3 = (editedItem.FindControl("ddlMod3") as DropDownList).Text.Trim();
            size = (editedItem.FindControl("txtSize") as TextBox).Text.Trim();
            gender = (editedItem.FindControl("txtGender") as TextBox).Text.Trim();
            color = (editedItem.FindControl("txtColor") as TextBox).Text.Trim();
            upcCode = (editedItem.FindControl("txtUPCCode") as TextBox).Text.Trim();
            wholesaleCost = 0;
            TextBox textboxWholesaleCost = (TextBox)editedItem.FindControl("txtWholesaleCost");
            if (textboxWholesaleCost.Text.Trim().Length > 0)
            {
                wholesaleCost = Convert.ToDecimal(textboxWholesaleCost.Text.Replace("$", "").Replace(",", "").Trim());
            }
            HCPCSstring = (editedItem.FindControl("txtHCPCSstring") as TextBox).Text.Trim().Replace(", ", ",");

        }
        //
        //protected void GetValuesFromInsertForm(GridEditFormItem insertedItem, out string productName, out string productCode, out string packaging
        //    , out string side, out string size, out string gender, out string color, out decimal wholesaleCost, out string HCPCSstring, out string upcCode)
        //{ 
        //    GetValuesFromInsertForm(insertedItem, out productName, out productCode, out packaging
        //                       , out side, out size, out gender, out color, out wholesaleCost, out HCPCSstring, out upcCode, false);
        //}

        protected void GetValuesFromInsertForm(GridEditFormItem insertedItem, out string productName, out string productCode, out string packaging, out string side, out string size, out string mod1, out string mod2, out string mod3, out string gender, out string color, out decimal wholesaleCost, out string hcpcSstring, out string hcpcsStringOts, out string upcCode, out bool isLogoAblePart, bool master = false)
        {
            string masterName = "";
            if (master == true)
            {
                masterName = "Master";
            }
            //string s = insertedItem["Product"].Text.ToString();

            productName = (insertedItem.FindControl("txtProduct" + masterName) as TextBox).Text.Trim();
            productCode = (insertedItem.FindControl("txtCode" + masterName) as TextBox).Text.Trim();
            packaging = (insertedItem.FindControl("txtPackaging" + masterName) as TextBox).Text.Trim();

            side = (insertedItem.FindControl("txtSide" + masterName) as TextBox).Text.Trim();
            size = (insertedItem.FindControl("txtSize" + masterName) as TextBox).Text.Trim();
            gender = (insertedItem.FindControl("txtGender" + masterName) as TextBox).Text.Trim();
            color = (insertedItem.FindControl("txtColor" + masterName) as TextBox).Text.Trim();
            upcCode = (insertedItem.FindControl("txtUPCCode" + masterName) as TextBox).Text.Trim();

            mod1 = (insertedItem.FindControl("ddlMod1") as DropDownList).Text.Trim();
            mod2 = (insertedItem.FindControl("ddlMod2") as DropDownList).Text.Trim();
            mod3 = (insertedItem.FindControl("ddlMod3") as DropDownList).Text.Trim();

            wholesaleCost = 0;
            TextBox textboxWholesaleCost = (TextBox)insertedItem.FindControl("txtWholesaleCost" + masterName);
            if (textboxWholesaleCost.Text.Trim().Length > 0)
            {
                wholesaleCost = Convert.ToDecimal(textboxWholesaleCost.Text.Replace("$", "").Replace(",", "").Trim());
            }

            hcpcSstring = (insertedItem.FindControl("txtHCPCSstring" + masterName) as TextBox).Text.Trim().Replace(", ", ",");
            hcpcsStringOts = (insertedItem.FindControl("txtHCPCSstringOts" + masterName) as TextBox).Text.Trim().Replace(", ", ","); 

            isLogoAblePart = (insertedItem.FindControl("chkIsLogoAblePart" + masterName) as CheckBox).Checked;
        }



    }
}