﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConsignmentOrder.ascx.cs" Inherits="BregVision.BregAdmin.UserControls.ConsignmentOrder" %>
<%@ Register Src="~/UserControls/InventoryCount/InventoryCycleType.ascx" TagPrefix="bvl" TagName="InventoryCycleType" %>
<%@ Register Src="~/UserControls/InventoryCount/InventoryCycle.ascx" TagPrefix="bvl" TagName="InventoryCycle"  %>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        $(function () {
            $telerik.isIE7 = false;
            $telerik.isIE6 = false;
        });
    </script>
    <script type="text/javascript">
        function OnAddToCartClick(btnID, newText) {
            var okToDelete = confirm('All items currently in the cart will be removed. Are you sure you want to continue?');
            if (okToDelete == true) {
                var btn = document.getElementById(btnID);
                btn.disabled = true;
                btn.value = newText;
                return true;
            }
            return false;
        }
    </script>
</telerik:RadScriptBlock>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
<AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnOrderConsignmentItems">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="btnOrderConsignmentItems" />
                <telerik:AjaxUpdatedControl ControlID="btnOrderConsignmentItems1" />
                <telerik:AjaxUpdatedControl ControlID="lblPCStatus" />                
                <telerik:AjaxUpdatedControl ControlID="grdConsignment" />
            </UpdatedControls>
        </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnOrderConsignmentItems1">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="btnOrderConsignmentItems" />
                <telerik:AjaxUpdatedControl ControlID="btnOrderConsignmentItems1" />
                <telerik:AjaxUpdatedControl ControlID="lblPCStatus" />
                <telerik:AjaxUpdatedControl ControlID="grdConsignment" />
            </UpdatedControls>
        </telerik:AjaxSetting>
</AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" InitialDelayTime="500" EnableAjaxSkinRendering="True" BackgroundPosition="Center" Skin="Breg" EnableEmbeddedSkins="False">
    <img src="App_Themes/Breg/images/Common/loading.gif" />
  </telerik:RadAjaxLoadingPanel>
<telerik:RadInputManager ID="RadInputManager1" runat="server" Skin="Office2007">
    <telerik:NumericTextBoxSetting BehaviorID="numberSetting" Culture="en-US" DecimalDigits="0"
        DecimalSeparator="." GroupSeparator="," GroupSizes="3" MaxValue="999" MinValue="0"
        NegativePattern="-n" PositivePattern="n" SelectionOnFocus="SelectAll">
    </telerik:NumericTextBoxSetting>
</telerik:RadInputManager>
<div style="margin: 10px; padding: 10px; text-align: left; width: 97%; border-style:solid; border-width:thin;"> 
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin-bottom: 20px;">
    <tr valign="middle" style="margin-bottom: 5px;">
        <td>
            <table>
                <tr valign="middle">
                    <td>
                        <asp:Label ID="lblContentHeader" runat="server" Text="Inventory Levels" CssClass="PageTitle" OnLoad="lblContentHeader_Load"></asp:Label>
                    </td>
                    <td>
                        
                    </td>
                </tr>
            </table>
        </td>
        <td align="right">
        </td>
    </tr>
</table>
    <table width="100%" cellpadding="0" border="0" cellspacing="0">
     <tr valign="bottom" style="padding-bottom: 5px;">
        <td>
            <table style="margin: 0 auto;">
                <tr valign="middle">
                    <td>
                        <span class="PageTitle">The Following Items Will Be Added To The Cart </span>
                    </td>
                    <td>
                        <!--help-->
                    </td>
                </tr>
            </table>
            <div style="text-align: center;">
                <asp:Label ID="lblPCStatus" runat="server" CssClass="WarningMsg"></asp:Label></div>  
                   
        </td>
        <td>
            
        </td>
    </tr>

    <tr valign="top">
        <td>
            <asp:Button ID="btnOrderConsignmentItems" runat="server" OnClick="btnOrderConsignmentItems_Click" Text="Add To Cart" Width="150px" OnClientClick="OnAddToCartClick(this.id, 'working...');" UseSubmitBehavior="false" /> 

            <bvu:VisionRadGridAjax ID="grdConsignment" runat="server" AllowMultiRowSelection="True"
                EnableViewState="true" 
                OnNeedDataSource="grdConsignment_NeedDataSource"
                OnPageIndexChanged="grdConsignment_PageIndexChanged"
                OnPreRender="grdConsignment_PreRender" 
                OnItemDataBound="grdConsignment_ItemDataBound"
                ShowGroupPanel="True" Width="100%"
                AddNonBreakingSpace="false">
                <MasterTableView AutoGenerateColumns="False" DataKeyNames="ProductInventoryID" EnableTheming="true"
                    EnableViewState="true" Name="mtvConsignment" HierarchyLoadMode="ServerOnDemand">
                    <Columns>
                        <telerik:GridBoundColumn DataField="ProductInventoryID" UniqueName="ProductInventoryID" Visible="False" />
                        <telerik:GridBoundColumn DataField="PracticeCatalogProductID" UniqueName="PracticeCatalogProductID" Visible="False" />
                        <telerik:GridBoundColumn DataField="SupplierShortName" HeaderText="Supplier Name" UniqueName="SupplierShortName" />
                        <telerik:GridBoundColumn DataField="BrandShortName" HeaderText="Brand Name" UniqueName="BrandShortName" />
                        <telerik:GridBoundColumn DataField="ProductName" HeaderText="ProductName" UniqueName="ProductName" />     
                        <telerik:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code" />                   
                        <telerik:GridBoundColumn DataField="QOH" HeaderText="Updated QOH" UniqueName="QOH" />
                        <telerik:GridBoundColumn DataField="QuantityOnOrder" HeaderText="On Order" UniqueName="QuantityOnOrder" />
                        <telerik:GridBoundColumn DataField="QuantityInCart" HeaderText="In Cart" UniqueName="QuantityInCart" />
                        <telerik:GridBoundColumn DataField="CalculatedReOrderQuantity" UniqueName="CalculatedReOrderQuantity" HeaderText="Re-Order Amount"  />
                        <telerik:GridBoundColumn DataField="WholesaleCost" HeaderText="Cost" UniqueName="WholesaleCost" DataFormatString="{0:C}" />
                        <telerik:GridBoundColumn DataField="WholesaleTotal" HeaderText="Total" UniqueName="WholesaleTotal" DataFormatString="{0:C}" />
                    </Columns>
                </MasterTableView>
                <ExportSettings>
                    <Pdf PageBottomMargin="" PageFooterMargin="" PageHeaderMargin="" PageHeight="11in"
                        PageLeftMargin="" PageRightMargin="" PageTopMargin="" PageWidth="8.5in" />
                </ExportSettings>
            </bvu:VisionRadGridAjax>
            <asp:Button ID="btnOrderConsignmentItems1" runat="server" OnClick="btnOrderConsignmentItems_Click"
                            Text="Add To Cart" Width="150px" OnClientClick="OnAddToCartClick(this.id, 'working...');" UseSubmitBehavior="false" />

        </td>
        <td style="padding-left: 5px; padding-top: 10px;">

        </td>
    </tr>
</table>
</div>
