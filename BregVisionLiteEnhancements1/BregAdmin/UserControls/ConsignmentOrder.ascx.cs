﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.Admin.PracticeSetup;
using BregVision.UserControls.General;
using BregWcfService;
using BregVision.UserControls;
using Telerik.Web.UI;

namespace BregVision.BregAdmin.UserControls
{
    public partial class ConsignmentOrder : PracticeSetupBase
    {
        public RecordsPerPage RecordsPerPage { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            RecordsPerPage.recordsPerPageChangedEvent += new BregVision.UserControls.General.RecordsPerPageEventHandler(ctlRecordsPerPage_recordsPerPageChangedEvent);
            grdConsignment.AddColorStylesToPage();
        }

        void ctlRecordsPerPage_recordsPerPageChangedEvent(object sender, BregVision.UserControls.General.RecordsPerPageEventArgs e)
        {
            grdConsignment.PageSize = e.NewRecordsPerPage;
            grdConsignment.Rebind();
        }




        protected void lblContentHeader_Load(object s, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var l = s as Label;
                if (l != null)
                    l.Text = practiceLocationName + " Consignment Reorder";
            }
        }

        protected void btnOrderConsignmentItems_Click(object sender, EventArgs e)
        {
            try
            {
                int userID = 1;

                
               

                lblPCStatus.Text = "";
                lblPCStatus.Visible = false;


                BregWcfService.ConsignmentItem.AddReplenishmentConsignmentListToShoppingCart(userID, practiceLocationID, true);
            }
            catch (BregWcfService.CustomBraceInCartException cex)
            {
                lblPCStatus.Text = "No products added to cart (Custom Braces must be the only item in the cart) <br/>Please remove the custom brace or custom brace accessory and try again";
                lblPCStatus.Visible = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                if (ex.Class == 17 && ex.Number == 50000)
                {
                    string x = ex.Message;

                    lblPCStatus.Text = "No products added to cart (Custom Braces must be the only item in the cart) <br/>Please remove the custom brace or custom brace accessory and try again";
                    lblPCStatus.Visible = true;
                    return;
                }
                if (ex.Class == 18 && ex.Number == 50000) // probably don't need this but I'll leave it here for now
                {
                    string x = ex.Message;
                    lblPCStatus.Text = "No products added to cart (Custom Braces quantity must be 1)";
                    lblPCStatus.Visible = true;
                    return;
                }
            }
            finally
            {
                Button btnAdd = sender as Button;
                if (btnAdd != null)
                {
                    btnAdd.Enabled = true;
                }

                grdConsignment.Rebind();
            }
        }



        protected void grdConsignment_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            InventoryDiscrepencyItem[] discrepencies = BregWcfService.ConsignmentItem.GetConsignmentReplenishmentList(practiceLocationID).Where(a => a.CalculatedReOrderQuantity > 0 || a.IsBuyout == true || a.IsRMA == true ).ToArray < InventoryDiscrepencyItem>();

            grdConsignment.DataSource = discrepencies.OrderBy(a => a.IsRMA).ThenBy(b => b.IsBuyout);  //(a => a.ReOrderQuantity < 1).ThenBy
        }

        protected void grdConsignment_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            grdConsignment.SetExpansionOnPageIndexChanged();
        }

        protected void grdConsignment_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                try
                {

                    GridDataItem dataItem = (GridDataItem)e.Item;
                    InventoryDiscrepencyItem discrepencyItem = dataItem.DataItem as InventoryDiscrepencyItem;

                    if (discrepencyItem != null)
                    {
                        if(discrepencyItem.IsRMA == true)
                            dataItem.SetColor(VisionRadGridAjax.GridColor.Blue);

                        if(discrepencyItem.IsBuyout == true)
                            dataItem.SetColor(VisionRadGridAjax.GridColor.Green);
                    }
                }
                catch{}
            }
        }

        protected void grdConsignment_PreRender(object sender, EventArgs e)
        {
            if (grdConsignment.NeedsRebind == true)
            {
                grdConsignment.Rebind();
            }

            grdConsignment.ExpandGrid();//grdInventory.ExpandGrid(true);
        }

        #region RadInputManager bits

        /// <summary>
        /// Registers a TextBox with a RadInputManager setting. Relies on _GetRadInputManagerNumberSetting()
        /// to retrieve the setting.
        /// 
        /// NOTE: The Load event is fired during postback as well. The _ClearRadInputManagerTagerControls()
        /// method is used to remove registration for TextBoxes that are going to be hidden during a
        /// collapse of a detail table view.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void TextBoxAddToRadInputManager_Load(object source, EventArgs e)
        {
            var t = source as TextBox;
            if (t == null)
                return;

            _GetRadInputManagerNumberSetting()
                .TargetControls
                .Add(new Telerik.Web.UI.TargetInput(t.UniqueID, true));
        }

        /// <summary>
        /// Clears any registered controls that are child items of the provided GridItem.
        /// </summary>
        /// <param name="rowgriditem"></param>
        private void _ClearRadInputManagerTargetControls(Telerik.Web.UI.GridItem rowgriditem)
        {
            var deletelist = new List<Telerik.Web.UI.TargetInput>();
            var targets = _GetRadInputManagerNumberSetting().TargetControls;

            // loop through all nested table views and find targets that
            // have UniqueID values beginning with the table view UniqueID.
            // NOTE: this relies on child nodes always have its parent's ID
            // as a prefix.
            foreach (var views in rowgriditem.OwnerTableView.Items[rowgriditem.ItemIndex].ChildItem.NestedTableViews)
            {
                for (var i = 0; i < targets.Count; i++)
                {
                    var t = targets[i];
                    if (t.ControlID.StartsWith(views.UniqueID))
                        deletelist.Add(t);
                }
            }

            // go through the list to remove and un-register controls
            // from the RadInputManager setting targets list.
            foreach (var t in deletelist)
                targets.Remove(t);
        }

        /// <summary>
        /// Gets the RadInputManager setting with BehaviorID="numberSetting".
        /// </summary>
        /// <returns></returns>
        private Telerik.Web.UI.NumericTextBoxSetting _GetRadInputManagerNumberSetting()
        {
            return
                (Telerik.Web.UI.NumericTextBoxSetting)
                RadInputManager1.GetSettingByBehaviorID("numberSetting");
        }

        /// <summary>
        /// Used to catch a detail table view collapse and trigger removal of
        /// collapsed controls from RadInputManager.
        /// 
        /// NOTE: during PostBack, controls inside a collapsing detail view
        /// are still registered in their Load events. To prevent RadInputManager
        /// from trying to validate against a non-existent control, only visible
        /// controls should be registered.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdInventory_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            // clear controls only when event is triggered by expand/collapse
            // and do it only when collapsing.
            if ((e.CommandName == "ExpandCollapse") && e.Item.Expanded)
                _ClearRadInputManagerTargetControls(e.Item);
        }

        #endregion
    }
}