﻿<%@ Page MasterPageFile="MasterBregAdmin.Master" Language="C#" AutoEventWireup="true" CodeBehind="AccountMaintenance.aspx.cs" Inherits="AccountMaintenance" %>

<%@ Register Assembly="RadAjax.Net2" Namespace="Telerik.WebControls" TagPrefix="radA" %>
<%@ Register Assembly="RadWindow.Net2" Namespace="Telerik.WebControls" TagPrefix="radW" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="text-align: center">
       <radA:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Width="100%" EnableViewState="true" EnableAJAX="false">           
            <radW:RadWindowManager 
                    id="RadWindowManager1"             
                    OffsetElementId = "OffsetElement"
                     runat="server"
                    Skin="WebBlue"
                    SkinsPath="~/RadControls/Window/Skins"
                    Modal="true" >        
                        <windows>
                            <radW:RadWindow ID="RadWindow1"                                 
                             Runat="server"     
                            Height="365px"
                            ></radW:RadWindow>                                                
                        </windows>                  
            </radW:RadWindowManager> 
    
        <table id="OuterTable" width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr style="height: 35px" valign="top">
                <td align="center">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr align="center">
                                <td style="width: 45%">&nbsp;</td>
                                <td>
                                    <a href="BregAdmin.aspx" class="managemenulink">Back</a><br />
                                </td>
                                <td style="width: 45%">&nbsp;</td>
                            </tr>
                        </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <!-- Inner Table to contain Practice Admin Navigation User Control, Grid Header, Grid, and any buttons -->
                    <table border="0" cellpadding="0" cellspacing="0" style="width: auto" summary="Summary Description">
                        <tr>
                            <td style="width: auto">
                                <!-- PracticeAdminHeader UserControl-->
                                <!--  NOT USED HERE -->
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" style="width: auto" >
                                    <!-- Content Header-->
                                    <tr class="AdminPageTitle">
                                        <td valign="middle">
                                            <asp:Label ID="lblContentHeader" runat="server" Text="Breg Vision Express Inactive Practice Applications"></asp:Label>
                                        </td>
                                    </tr>
                                    <!-- Grid -->
                                    <tr>
                                        <td style="width: auto">
                                            <asp:GridView ID="GridPracticeApplications" runat="server" 
                                                ShowFooter="False" DataKeyNames="PracticeApplicationID"
                                                GridLines ="Both" AutoGenerateColumns="False" Width="600px"
                                                BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" 
                                                CssClass="FieldLabel" CellPadding="3"
                                                OnRowEditing="GridPractices_RowEditing" 
                                                OnRowUpdating="GridPractices_RowUpdating" 
                                                OnRowDataBound="GridPractices_OnRowDataBound"
                                                OnRowCancelingEdit="GridPractices_RowCancelingEdit"
                                                >
                                                <Columns>
                                                    <asp:BoundField DataField="PracticeApplicationID" Visible="False" ReadOnly="True" />
                                                    <asp:BoundField DataField="IsPracticeCreated" Visible="False" />
                                                                                                        
                                                    <asp:TemplateField HeaderStyle-Width="60px" ItemStyle-Height="25px">
                                                        <EditItemTemplate>
                                                            <asp:ImageButton ID="ImageButtonEditUpdate" runat="server" CausesValidation="True"
                                                                CommandName="Update" ImageUrl="~/App_Themes/Breg/images/Common/checkCircle.png" ToolTip="Update" />&nbsp;
                                                            <asp:ImageButton ID="ImageButtonEditCancel" runat="server" CausesValidation="False" CommandName="Cancel"
                                                                    ImageUrl="~/App_Themes/Breg/images/Common/cancelCircle.png" ToolTip="Cancel" />
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="ImageButtonEdit" runat="server" CausesValidation="False" CommandName="Edit"
                                                                ImageUrl="~/App_Themes/Breg/images/Common/edit.png" ToolTip="Edit"/>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:ImageButton ID="ImageButtonAddNew" runat="server" CommandName="AddNew" ImageUrl="~\App_Themes\Breg\images\Common\add-record.png"
                                                                ToolTip="Add New Practice" />
                                                            <asp:ImageButton ID="ImageButtonAddNewCancel" runat="server" Visible="false" CommandName="AddNewCancel"
                                                                ImageUrl="~/RadControls/Grid/Skins/Outlook/Cancel.gif" ToolTip="Cancel" />
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                                                                        
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblPracticeNameHeader" runat="server" Text="Practice Name"></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPracticeNameEditMode" runat="server" Text='<%# Bind("practiceName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Wrap="False" />
                                                    </asp:TemplateField>
                                                    
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label runat="server" Text="Notes"></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="labNotes" runat="server" Text='<%# Bind("Notes") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtNotes" runat="server" CssClass="FieldLabel" Width="270px" Text='<%# Bind("Notes") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemStyle Wrap="False" />
                                                    </asp:TemplateField>
                                                    
                                                    <asp:TemplateField >
                                                         <HeaderTemplate>
                                                            <asp:Label ID="lblIsPracticeCreated" runat="server" Text="Is Practice Created"></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkIsPracticeedCreated" runat="server" Enabled="false" Checked='<%# Bind("IsPracticeCreated") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblPracticeFees" runat="server" Text="Practice Fees"></asp:Label>
                                                        </HeaderTemplate>
                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="ddlPracticeFees" runat="server" />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    <asp:TemplateField >
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblIsActivatePractice" runat="server" Text="Activate Practice"></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkActivatePracticeEditMode" runat="server" Enabled="false" />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:CheckBox ID="chkActivatePractice" runat="server" />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                                                                        <asp:TemplateField >
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblPopulateInventory" runat="server" Text="Auto Populate Inventory"></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:CheckBox ID="chkPopulateInventory" runat="server" Checked="true" />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                
                                                <RowStyle ForeColor="#000066" Wrap="False" />
                                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                <HeaderStyle CssClass="AdminSubTitle"/>
                                            </asp:GridView>
                                            <!--  Blank Row at Bottom of Content  -->
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        </radA:RadAjaxPanel>
    </div>
</asp:Content>