<%@ Page MasterPageFile="MasterBregAdmin.Master" Language="C#" AutoEventWireup="true" Inherits="BregAdmin" Codebehind="BregAdmin.aspx.cs" MaintainScrollPositionOnPostback="true" %>

<%--<%@ Register Assembly="RadAjax.Net2" Namespace="Telerik.WebControls" TagPrefix="radA" %>--%>
<%@ Register Assembly="RadWindow.Net2" Namespace="Telerik.WebControls" TagPrefix="radW" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    
    <div style="text-align: center">
            <radW:RadWindowManager 
                    id="RadWindowManager1"             
                    OffsetElementId = "OffsetElement"
                     runat="server"
                    Skin="WebBlue"
                    SkinsPath="~/RadControls/Window/Skins"
                    Modal="true" >        
                        <windows>
                            <radW:RadWindow ID="RadWindow1"                                 
                             Runat="server"     
                            Height="365px"
                            ></radW:RadWindow>                                                
                        </windows>                  
            </radW:RadWindowManager> 

            <radW:RadWindowManager 
                    id="RadWindowManager2"             
                    OffsetElementId = "OffsetElement"
                     runat="server"
                    Skin="WebBlue"
                    SkinsPath="~/RadControls/Window/Skins"
                    Modal="true" >        
                        <windows>
                            <radW:RadWindow ID="RadWindow2"                                 
                             Runat="server"  
                             Width="865px"   
                            Height="665px"
                            ></radW:RadWindow>                                                
                        </windows>                  
            </radW:RadWindowManager> 
           
    
        <table id="OuterTable" width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr style="height: 35px" valign="top">
                <td align="center">
                        <table cellpadding="2" cellspacing="2" width="50%" border="0">
                            <tr align="center">
                                <!--Memo: A stylesheet is point to Admin/Styles/AdminMasterStyle.css -->
                                <td><a href="FeeLevelAdmin.aspx" class="managemenulink">Manage Fees</a></td>
                                <td><a href="AccountMaintenance.aspx" class="managemenulink">Vision Exp. Activation</a></td>
                                <td><a href="ICD9Admin.aspx" class="managemenulink">Manage Dx Codes</a></td>
                                <td><a href="ABNFormAdmin.aspx" class="managemenulink">Manage ABN Required</a></td>
                                <td><a href="OrderAdmin.aspx" class="managemenulink">Manage Orders</a></td>
                                <td><a href="SalesReportAdmin.aspx" class="managemenulink">Manage Sales Report</a></td>
                                <td><a href="SendFax.aspx" class="managemenulink">Resend Faxes</a></td>
                                <td><a href="PracticeApplicationAgreementTermsAdmin.aspx" class="managemenulink">Manage TOS</a></td>
                                <td><a href="MasterCatalogAdministration.aspx" class="managemenulink">Manage Master Catalog Products</a></td>
                                <td><a href="ConsignmentRepAdmin.aspx" class="managemenulink">Manage Consignment Reps</a></td>
                                <td><a href="HelpAdmin.aspx" class="managemenulink">Help Admin</a></td>
                                <td><a href="MasterCost.aspx" class="managemenulink">Master Cost</a></td>
                            </tr>
                        </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <!-- Inner Table to contain Practice Admin Navigation User Control, Grid Header, Grid, and any buttons -->
                    <table border="0" cellpadding="0" cellspacing="0" style="width: auto" summary="Summary Description">
                        <tr>
                            <td style="width: auto">
                                <!-- PracticeAdminHeader UserControl-->
                                <!--  NOT USED HERE -->
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" style="width: auto" >
                                    <!-- Content Header-->
                                    <tr class="AdminPageTitle">
                                        <td valign="middle">
                                            <asp:Label ID="lblContentHeader" runat="server" Text="Breg Vision Practices"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="font-family:Verdana, Arial;font-size:11pt;font-weight:500;">
                                            <asp:CheckBox runat="server" Text="Show Vision practices" ID="chkShowVisionRecords" AutoPostBack="True" Checked="true" OnCheckedChanged="chkApplyFilters_Changed" />
                                            <br/>
                                            <asp:CheckBox runat="server" Text="Show Vision Express practices" ID="chkShowVisionExpressRecords" AutoPostBack="True" Checked="false" OnCheckedChanged="chkApplyFilters_Changed" />
                                            <br/>
                                            <asp:CheckBox runat="server" Text="Include inactive practices" ID="chkShowAllRecords" AutoPostBack="True" OnCheckedChanged="chkApplyFilters_Changed" />
                                            <br/>
                                            <asp:CheckBox runat="server" Text="Include practices with (Test) in the practice name" ID="chkShowTextPractices" AutoPostBack="True" OnCheckedChanged="chkApplyFilters_Changed" />
                                        </td>
                                    </tr>
                                    <!-- Grid -->
                                    <tr>
                                        <td style="width: auto">
                                            <asp:GridView ID="GridPractices" runat="server" ShowFooter="True" DataKeyNames="PracticeID,PracticeName,IsVisionLite, PracticeApplicationID"
                                                OnRowEditing="GridPractices_RowEditing" OnRowUpdating="GridPractices_RowUpdating" GridLines ="Both"
                                                OnRowCancelingEdit="GridPractices_RowCancelingEdit" OnRowDeleting="GridPractices_RowDeleting"
                                                OnRowCommand="GridPractices_RowCommand" AutoGenerateColumns="False" Width="600px" OnRowDataBound="GridPractices_OnRowDataBound"
                                                BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CssClass="FieldLabel"
                                                CellPadding="3">
                                                <Columns>
                                                <asp:ButtonField ButtonType="Image" ImageUrl="~/App_Themes/Breg/images/Common/delete.png"
                                                        CommandName="Delete" />
                                                    <asp:BoundField DataField="PracticeID" Visible="False" ReadOnly="True" />
                                                    <asp:TemplateField HeaderStyle-Width="60px" ItemStyle-Height="25px">
                                                        <EditItemTemplate>
                                                            <asp:ImageButton ID="ImageButtonEditUpdate" runat="server" CausesValidation="True"
                                                                CommandName="Update" ImageUrl="~/App_Themes/Breg/images/Common/checkCircle.png" ToolTip="Update" />&nbsp;
                                                            <asp:ImageButton ID="ImageButtonEditCancel" runat="server" CausesValidation="False" CommandName="Cancel"
                                                                    ImageUrl="~/App_Themes/Breg/images/Common/cancelCircle.png" ToolTip="Cancel" />
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="ImageButtonEdit" runat="server" CausesValidation="False" CommandName="Edit"
                                                                ImageUrl="~/App_Themes/Breg/images/Common/edit.png" ToolTip="Edit"/>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:ImageButton ID="ImageButtonAddNew" runat="server" CommandName="AddNew" ImageUrl="~\App_Themes\Breg\images\Common\add-record.png"
                                                                ToolTip="Add New Practice" />
                                                            <asp:ImageButton ID="ImageButtonAddNewCancel" runat="server" Visible="false" CommandName="AddNewCancel"
                                                                ImageUrl="~/RadControls/Grid/Skins/Outlook/Cancel.gif" ToolTip="Cancel" />
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    <asp:TemplateField HeaderText="Practice Name">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtPracticeName" runat="server" CssClass="FieldLabel" Width="270px" Text='<%# Bind("practiceName") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPracticeNameEditMode" runat="server" Text='<%# Bind("practiceName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblPracticeNameHeader" runat="server" Text="Practice Name"></asp:Label>
                                                        </HeaderTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtNewPracticeName" runat="server" Visible="False"></asp:TextBox>
                                                        </FooterTemplate>
                                                        <ItemStyle Wrap="False" />
                                                    </asp:TemplateField>
                                                    
                                                    <asp:ButtonField ButtonType="Image" ImageUrl="~\RadControls\Grid\Skins\Outlook\SetFees.gif"
                                                        CommandName="SetPracticeFees" Text="Set Practice Fees" />
                                                        
                                                    <asp:BoundField DataField="PracticeID" Visible="False" ReadOnly="True" />

                                                    <asp:ButtonField ButtonType="Image" ImageUrl="~/RadControls/Grid/Skins/Outlook/file.gif"
                                                        CommandName="SetConsignments" Text="Consignment" />
                                                                                                                                                                                                                
                                                    <asp:TemplateField HeaderText="Ortho Select">
                                                        <EditItemTemplate>
                                                            <asp:CheckBox ID="chkIsOrthoSelect" runat="server" Checked='<%# Bind("IsOrthoSelect") %>' />
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkIsOrthoSelectEditMode" runat="server" Checked='<%# Bind("IsOrthoSelect") %>' Enabled="false" />
                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblIsOrthoSelectHeader" runat="server" Text="OrthoSelect"></asp:Label>
                                                        </HeaderTemplate>
                                                        <FooterTemplate>
                                                            <asp:CheckBox ID="chkNewIsOrthoSelect" runat="server" Visible="false" />
                                                         </FooterTemplate>
                                                         <ItemStyle Wrap="false" />
                                                    </asp:TemplateField>
                                                    
                                                    <asp:TemplateField HeaderText="Details">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnDetails"  runat="server" 
                                                            CommandName="GoTo_Practice_Admin" 
                                                            CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                                                            Text="Details" />
                                                        </ItemTemplate> 
                                                    </asp:TemplateField>
                                                    
                                                    <asp:ButtonField HeaderText="Contact" Text="Contact" CommandName="GoToPracticeContact"
                                                        Visible="False" />
                                                    <asp:ButtonField HeaderText="BillToAddress" Text="BillToAddress" CommandName="GoToPracticeBillToAddress"
                                                        Visible="False" />
                                                    <asp:ButtonField HeaderText="Physicians" Text="Physicians" CommandName="GoToPracticePhysicians"
                                                        Visible="False" />
                                                    <asp:ButtonField HeaderText="Locations" Text="Locations" CommandName="GoToPracticeLocation"
                                                        Visible="False" />
                                                    
                                                    <asp:TemplateField >
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblIsActive" runat="server" Text="Is Active"></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkIsActiveEditMode" runat="server" Enabled="false" Checked='<%# Bind("IsActive") %>'/>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:CheckBox ID="chkIsActive" runat="server" Checked='<%# Bind("IsActive") %>' />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    <asp:TemplateField >
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblIsVisionLite" runat="server" Text="Is Vision Express"></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkIsVisionExpressEditMode" runat="server" Enabled="false" Checked='<%# Bind("IsVisionLite") %>'/>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:CheckBox ID="chkIsVisionExpress" runat="server" Checked='<%# Bind("IsVisionLite") %>' />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                                                                        
                                                    <%--<asp:ButtonField HeaderText="PayPal Subscribe" Text="PayPal" CommandName="PayPal" />--%>
                                                    
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblPaymentType" runat="server" Text="Payment Type" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPracticePaymentType" runat="server" Text='<%# Bind("PaymentTypeName") %>' />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="ddlPracticePaymentType" runat="server"></asp:DropDownList>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblBillingStartDateHeader" runat="server" Text="Billing Start Date" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblBillingStartDate" runat="server" Text='<%# Bind("BillingStartDate", "{0:M-dd-yyyy}") %>' />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtBillingStartDate" runat="server" Text='<%# Bind("BillingStartDate", "{0:M-dd-yyyy}") %>' />
                                                            <asp:CompareValidator ID="cvBillingStartDate" runat="server" ControlToValidate="txtBillingStartDate" ErrorMessage="* Enter a valid date" Operator="DataTypeCheck" Type="Date" ValidationGroup="grpDate" />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblPracticeCodeHeader" runat="server" Text="Practice Code" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPracticeCode" runat="server" Text='<%# Bind("PracticeCode") %>' />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtPracticeCode" runat="server" Text='<%# Bind("PracticeCode") %>' />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Camera Scan Enabled">
                                                        <EditItemTemplate>
                                                            <asp:CheckBox ID="chkCameraScanEnabled" runat="server" Checked='<%# Bind("CameraScanEnabled") %>' />
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkCameraScanEnabledEditMode" runat="server" Checked='<%# Bind("CameraScanEnabled") %>' Enabled="false" />
                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblCameraScanEnabledHeader" runat="server" Text="Camera Scan Enabled"></asp:Label>
                                                        </HeaderTemplate>
                                                        <FooterTemplate>
                                                            <asp:CheckBox ID="chkNewCameraScanEnabled" runat="server" Visible="false" />
                                                         </FooterTemplate>
                                                         <ItemStyle Wrap="false" />
                                                    </asp:TemplateField> 

                                                    <asp:TemplateField HeaderText="Fax Dispense Receipt Enabled">
                                                        <EditItemTemplate>
                                                            <asp:CheckBox ID="chkFaxDispenseReceiptEnabled" runat="server" Checked='<%# Bind("FaxDispenseReceiptEnabled") %>' />
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkFaxDispenseReceiptEnabledEditMode" runat="server" Checked='<%# Bind("FaxDispenseReceiptEnabled") %>' Enabled="false" />
                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblFaxDispenseReceiptEnabledEnabledHeader" runat="server" Text="Fax Dispense Receipt Enabled"></asp:Label>
                                                        </HeaderTemplate>
                                                        <FooterTemplate>
                                                            <asp:CheckBox ID="chkNewFaxDispenseReceiptEnabled" runat="server" Visible="false" />
                                                         </FooterTemplate>
                                                         <ItemStyle Wrap="false" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="EMR Pull Enabled">
                                                        <EditItemTemplate>
                                                            <asp:CheckBox ID="chkEmrPullEnabled" runat="server" Checked='<%# Bind("EMRIntegrationPullEnabled") %>' />
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkEmrPullEnabledEditMode" runat="server" Checked='<%# Bind("EMRIntegrationPullEnabled") %>' Enabled="false" />
                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblEmrPullEnabledHeader" runat="server" Text="EMR Pull Enabled"></asp:Label>
                                                        </HeaderTemplate>
                                                        <FooterTemplate>
                                                            <asp:CheckBox ID="chkNewEmrPullEnabled" runat="server" Visible="false" />
                                                         </FooterTemplate>
                                                         <ItemStyle Wrap="false" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Custom Fit Enabled">
                                                        <EditItemTemplate>
                                                            <asp:CheckBox ID="chkCustomFitEnabled" runat="server" Checked='<%# Bind("IsCustomFitEnabled") %>' />
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkCustomFitEnabledEditMode" runat="server" Checked='<%# Bind("IsCustomFitEnabled") %>' Enabled="false" />
                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblCustomFitEnabledHeader" runat="server" Text="Custom Fit Enabled"></asp:Label>
                                                        </HeaderTemplate>
                                                        <FooterTemplate>
                                                            <asp:CheckBox ID="chkNewCustomFitEnabled" runat="server" Visible="false" />
                                                         </FooterTemplate>
                                                         <ItemStyle Wrap="false" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Breg Billing Enabled">
                                                        <EditItemTemplate>
                                                            <asp:CheckBox ID="chkIsBregBilling" runat="server" Checked='<%# Bind("IsBregBilling") %>' />
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkIsBregBillingEditMode" runat="server" Checked='<%# Bind("IsBregBilling") %>' Enabled="false" />
                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblIsBregBillingHeader" runat="server" Text="Breg Billing Enabled"></asp:Label>
                                                        </HeaderTemplate>
                                                        <FooterTemplate>
                                                            <asp:CheckBox ID="chkNewIsBregBilling" runat="server" Visible="false" />
                                                         </FooterTemplate>
                                                         <ItemStyle Wrap="false" />
                                                    </asp:TemplateField> 

                                                    <asp:TemplateField HeaderText="SecMsg (Internal)" FooterText="" ItemStyle-Wrap="false">
                                                        <EditItemTemplate>
                                                            <asp:CheckBox ID="chkSecMsgIntEnabled" runat="server" Checked='<%# Bind("IsSecureMessagingInternalEnabled") %>' />
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSecMsgIntEnabledEditMode" runat="server" Checked='<%# Bind("IsSecureMessagingInternalEnabled") %>' Enabled="false" />
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:CheckBox ID="chkNewSecMsgIntEnabled" runat="server" Visible="false" />
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="SecMsg (External)" FooterText="" ItemStyle-Wrap="false">
                                                        <EditItemTemplate>
                                                            <asp:CheckBox ID="chkSecMsgExtEnabled" runat="server" Checked='<%# Bind("IsSecureMessagingExternalEnabled") %>' />
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSecMsgExtEnabledEditMode" runat="server" Checked='<%# Bind("IsSecureMessagingExternalEnabled") %>' Enabled="false" />
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:CheckBox ID="chkNewSecMsgExtEnabled" runat="server" Visible="false" />
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    
                                                    <asp:TemplateField HeaderText="Action">
                                                        <ItemTemplate>
                                                            <asp:Button ID="btnAction" runat="server" 
                                                            CommandName="ReSubscribe" 
                                                            CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                                                            Text="Action" />
                                                        </ItemTemplate> 
                                                    </asp:TemplateField>
                                                     
                                                    <asp:BoundField DataField="PracticeApplicationID" HeaderText="Practice Application ID" Visible="True" ReadOnly="True" />
                                                    
                                                    <asp:TemplateField HeaderText="Cost Update">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnCostUpdate"  runat="server" 
                                                            CommandName="GoToCostUpdate" 
                                                            CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                                                            Text="Cost Update" />
                                                        </ItemTemplate> 
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Custom Forms">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnCustomForms"  runat="server" 
                                                            CommandName="GoToCustomForms" 
                                                            CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                                                            Text="Custom Forms" />
                                                        </ItemTemplate> 
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                                <RowStyle ForeColor="#000066" Wrap="False" />
                                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                <HeaderStyle CssClass="AdminSubTitle"/>
                                            </asp:GridView>
                                            <!--  Blank Row at Bottom of Content  -->
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
