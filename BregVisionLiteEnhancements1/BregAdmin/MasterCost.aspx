﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BregAdmin/MasterBregAdmin.Master" AutoEventWireup="true" CodeBehind="MasterCost.aspx.cs" Inherits="BregVision.BregAdmin.WholesalePrice1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="text-align: center">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr align="center">
                <td colspan="3">
                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>&nbsp;</td>
                            <td style="width:100px">
                                <a href="BregAdmin.aspx" class="managemenulink">Back</a><br />
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr> 
                <td align="center">
                    <table cellpadding="0" cellspacing="0" width="60%">
                        <tr>
                            <td  class="AdminPageTitle">
                                <a name="OrderAdminTopPage">Master Cost</a>
                            </td>
                        </tr>
                                    <tr>
                <td align="center"><br />
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                    <asp:Button ID="btnUpload" runat="server" Text="Upload file" 
                        onclick="btnUpload_Click" /><br /><br />
                    <asp:Label ID="lblUploadStatus" runat="server" CssClass="WarningMsgMed"></asp:Label><br /><br />
                </td>
            </tr>
            <tr align="center">
                <td>
                    <asp:GridView ID="gvUpdateInfo" runat="server" CellPadding="4" 
                                    ForeColor="#333333" GridLines="None" CssClass="FieldLabel" 
                                    AutoGenerateColumns="true" Width="575px" 
                        onrowdatabound="gvUpdateInfo_RowDataBound">
                                    <AlternatingRowStyle BackColor="White" />                               
                                    <EditRowStyle BackColor="#2461BF" />
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" HorizontalAlign ="Center"/>
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#EFF3FB" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                </td>
            </tr>
                    </table>
                </td>
            </tr>

        </table>
    </div>
</asp:Content>
