﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BLL.Practice;
using System.Web.Security;
using ClassLibrary.DAL;

namespace BregVision.BregAdmin
{
    public partial class ConsignmentPractices : System.Web.UI.Page
    {
        //private int _practiceID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {          
            if (!Page.IsPostBack)
            {
                //BindGridPractices(); //uncomment this out later 09/24/2011 u=
                string userName = Context.User.Identity.Name;
                Guid userID = GetUserIDFromUserName(userName);
                List<ClassLibrary.DAL.Practice> practices = GetPracticesForConsignmentRep(userID);
                
                //--------------------------------------------------
                grdConsignmentPractices.DataSource = practices;
                grdConsignmentPractices.DataBind();
                //--------------------------------------------------
              

                ////---------------------------Practice Location Nested GridView-------------------------
                //// Still Hard coded for the practice ID
                //DataKey key = grdConsignmentPractices.DataKeys[grdConsignmentPractices.SelectedIndex];
                //object value = key.Value;
                //_practiceID= Convert.ToInt32(value);

                ////List<ClassLibrary.DAL.PracticeLocation> practiceLocations = GetPracticeLocationName(157);

                ////grdPracticeLocation.DataSource = practiceLocations.ToList();
                ////grdPracticeLocation.DataBind();
            }
        }

        public Guid GetUserIDFromUserName(string userName)
        {
            MembershipUser editMembershipUser = null;
            editMembershipUser = Membership.GetUser(userName);
            return Guid.Parse(editMembershipUser.ProviderUserKey.ToString());
        }

        public void BindPracticeLocation(GridView grdPracticeLocation, int practiceID)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var rQuery = from pl in visionDB.PracticeLocations
                             where pl.PracticeID == practiceID
                             select new
                             {
                                 PracticeLocationID = pl.PracticeLocationID,
                                 Name = pl.Name,
                                 Hyperlink = string.Format("{0}?id={1}", BLL.Website.GetAbsPath("~/Admin/Consignment_InventoryCount.aspx"), pl.PracticeLocationID)//("~/Admin/PracticeSetup/PracticeSetup.aspx")
                             };

                grdPracticeLocation.DataSource = rQuery.ToList();

                grdPracticeLocation.DataBind();

            }
        }


        public List<ClassLibrary.DAL.Practice> GetPracticesForConsignmentRep(Guid userID) 
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {


                var repsQuery = from p in visionDB.Practices 
                                join r in visionDB.ConsignmentRep_Practices 
                                on p.PracticeID equals r.PracticeID 
                                where r.UserID == userID && r.IsActive == true 
                                select p;

                return repsQuery.ToList<ClassLibrary.DAL.Practice>();

            }
        
        }

        protected void grdConsignmentPractices_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index;

            if ((e.CommandName.Equals("ConsignmentInventoryCount.aspx"))
                    || (e.CommandName.Equals("SetConsignmentReps"))

                )  //GoTo_Practice_Admin
            {
                //Find the selected row index
                index = Convert.ToInt32(e.CommandArgument);

                ////Set the PracticeLocationID Session variable to the datakey value of the selected row
                //Session["PracticeID"] = Convert.ToInt32(grdConsignmentPractices.DataKeys[index].Value);
                //Session["PracticeName"] = grdConsignmentPractices.DataKeys[index].Values[1];
                //bool isVisionLite = (bool)grdConsignmentPractices.DataKeys[index].Values[2];

                ////Set our PracticeLocationID to the first practice location in the practice so things don't fail
                //{
                //    var p_info =
                //        new BLL.PracticeLocation.PracticeLocation()
                //            .SelectAll((Int32)Session["PracticeID"])
                //                .Rows
                //                .Cast<DataRow>()
                //                .Where(r => (r["IsPrimaryLocation"] != System.DBNull.Value) && (bool)r["IsPrimaryLocation"])
                //                .Select(r => new { ID = Convert.ToInt32(r["PracticeLocationID"]), Name = r["Name"].ToString() })
                //                .FirstOrDefault();
                //    if (p_info != null)
                //    {
                //        Session["PracticeLocationID"] = p_info.ID;
                //        Session["PracticeLocationName"] = p_info.Name;
                //    }
                //}

                //Go to page
                switch (e.CommandName)
                {
                    case "SetConsignmentReps":
                        Response.Redirect("~/Admin/Consignment_InventoryCount.aspx");
                        break;
                }
            }
        }

        protected void grdConsignmentPractices_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if(e.Row.RowType == DataControlRowType.DataRow)
            {

                DataKey key = grdConsignmentPractices.DataKeys[e.Row.RowIndex];//[grdConsignmentPractices.SelectedIndex];
                object value = key.Value;
                int practiceID = Convert.ToInt32(value);

                GridView grdPracticeLocation = e.Row.FindControl("grdPracticeLocation") as GridView;

                
                if (grdPracticeLocation != null )
                {
                    BindPracticeLocation(grdPracticeLocation, practiceID);
                }
               

            }
        }
    }
}