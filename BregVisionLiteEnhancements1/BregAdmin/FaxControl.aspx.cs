﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary.DAL;

namespace BregVision.BregAdmin
{
    public partial class FaxControl : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnEnable_Click(object sender, EventArgs e)
        {
            FaxServer.EnableFaxServer(txtPassword.Text);
        }

        protected void btnDisable_Click(object sender, EventArgs e)
        {
            FaxServer.DisableFaxServer(txtPassword.Text);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            bool faxEnabled = FaxServer.GetFaxServerStatus();
            hTitle.InnerText = string.Format("Fax Server is currently {0}.", faxEnabled == true ? "ON" : "OFF");
        }
    }
}
