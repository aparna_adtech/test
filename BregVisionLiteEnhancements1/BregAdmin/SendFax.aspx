﻿<%@ Page Language="C#" MasterPageFile="~/BregAdmin/MasterBregAdmin.Master" AutoEventWireup="true" CodeBehind="SendFax.aspx.cs" Inherits="BregVision.BregAdmin.SendFax" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
    
    <div>
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr align="center">
                    <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="58%">
                        <tr>
                            <td colspan="4">
                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                    <tr align="center">
                                        <td style="width: 45%">
                                            
                                        </td>
                                        <td>
                                            <a href="BregAdmin.aspx" class="managemenulink">Back</a><br />
                                        </td>
                                        <td style="width: 45%">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="AdminPageTitle">Send Fax</td>
                        </tr>
                        <tr><td colspan="4"><h2 id="hTitle" runat="server"></h2><%--transmissionID, createddate--%></td></tr>
                        <tr>
                            <th class="TextMed" style="width: 185px">Transmission ID</th>
                            <th class="TextMed">Status from Efax</th>
                            <th style="width: 142px">&nbsp;</th>
                            <th style="width: 142px">&nbsp;</th>
                        </tr>
                        <tr style="height: 48px">
                            <td valign="top"><asp:TextBox ID="txtTransactionID" runat="server" Width="175px" CssClass="FieldLabel"></asp:TextBox></td>
                            <td valign="top">
                                <asp:Label ID="lblTransactionStatus" runat="server" CssClass="FieldLabel"></asp:Label>&nbsp;<br />
                                <asp:RegularExpressionValidator ID="revTransactionLength" runat="server" CssClass="WarningMsg" ErrorMessage="The ID has to be numbers and mininum length is 14." Display="Dynamic" ControlToValidate="txtTransactionID" ValidationExpression="^[\d+]{14,}$" ValidationGroup="vgCheckStatus"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="rfvTransactionID" runat="server" CssClass="WarningMsg" ErrorMessage="Enter a Transmission ID" ControlToValidate="txtTransactionID" Display="Dynamic" ValidationGroup="vgCheckStatus" ></asp:RequiredFieldValidator>
                            </td>
                            <td valign="top" align="right">
                            <asp:Button ID="btnCheckStatus" runat="server" Text="Check Status" OnClick="btnCheckStatus_Click" Width="118px" ValidationGroup="vgCheckStatus"/></td>
                            <td valign="top" align="right"><asp:Button ID="btnMarkUnsent" runat="server" Text="Mark Unsent" OnClick="btnMarkUnsent_Click" Width="118px"/></td>
                        </tr>
                    </table>
                    
                    <br /><br /><hr /><br /><br />
                    <asp:GridView ID="gvUnsentFaxes" runat="server" AllowSorting="true"
                            AutoGenerateColumns="false" AlternatingRowStyle-BackColor="#CCCCCC" 
                            DataKeyNames="FaxMessageID" CssClass="FieldLabel" Width="885px" 
                            onpageindexchanging="gvUnsentFaxes_PageIndexChanging" 
                            onselectedindexchanged="gvUnsentFaxes_SelectedIndexChanged" 
                            onrowdatabound="gvUnsentFaxes_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Resend" HeaderStyle-CssClass="AdminSubTitle">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkResend" runat="server" Checked="true" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="TransmissionID" HeaderText="TransmissionID" HeaderStyle-CssClass="AdminSubTitle" />
                            <asp:BoundField DataField="FaxNumber" HeaderText="Fax Number" HeaderStyle-CssClass="AdminSubTitle" />
                            <asp:BoundField DataField="CreatedDate" HeaderText="Created Date"  HeaderStyle-CssClass="AdminSubTitle"/>
                            <asp:TemplateField HeaderText="Skip" HeaderStyle-CssClass="AdminSubTitle">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSkip" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FaxStatus" HeaderText="Current Status" HeaderStyle-CssClass="AdminSubTitle"/>
                            <asp:BoundField DataField="SendAttempts" HeaderText="Send Attempts" HeaderStyle-CssClass="AdminSubTitle" />
                            <asp:HyperLinkField HeaderText="View Fax" Text="View..." DataNavigateUrlFields="FaxFilename" DataNavigateUrlFormatString="~/Fax/{0}" Target="_blank"  HeaderStyle-CssClass="AdminSubTitle"/>
                        </Columns>
                    </asp:GridView>
                    <br />
                    <br /><asp:Button ID="btnResend" runat="server" Text="Resend Faxes" 
                        onclick="btnResend_Click" ValidationGroup="vlgDateSince" />&nbsp;
                     <br /><br />
                     <hr />
                     <asp:GridView ID="gvViewAllFaxesSince" runat="server"
                            HeaderStyle-CssClass="AdminSubTitle" ShowFooter="true" 
                            AutoGenerateColumns="false" GridLines="Both" DataKeyNames ="CreatedDate" 
                            AlternatingRowStyle-BackColor="Silver" CssClass="FieldLabel"  Width="885px" 
                            onpageindexchanging="gvViewAllFaxesSince_PageIndexChanging" 
                            onrowdatabound="gvViewAllFaxesSince_RowDataBound">
                         <AlternatingRowStyle BackColor="#CCCCCC" />
                            <Columns>
                                <asp:BoundField HeaderText="TransmissionID" DataField="TransmissionID" HeaderStyle-CssClass="AdminSubTitle" />
                                <asp:BoundField HeaderText ="Fax Message ID" DataField="FaxMessageID" Visible="false"/>
                                <asp:BoundField HeaderText ="Fax Number" DataField ="FaxNumber" HeaderStyle-CssClass ="AdminSubTitle" />
                                <asp:BoundField HeaderText ="Fax File Name" DataField="FaxFileName" HeaderStyle-CssClass ="AdminSubTitle" ItemStyle-HorizontalAlign="Left" />
                                <asp:CheckBoxField HeaderText ="Sent" DataField="Sent" HeaderStyle-CssClass="AdminSubTitle" />
                                <asp:CheckBoxField HeaderText ="Skip" DataField ="Skip" HeaderStyle-CssClass="AdminSubTitle" />
                                <asp:BoundField DataField="SendAttempts" HeaderText="Send Attempts" HeaderStyle-CssClass="AdminSubTitle" />
                                <asp:HyperLinkField HeaderText="View Fax" Text="View..." DataNavigateUrlFields="FaxFilename" DataNavigateUrlFormatString="~/Fax/{0}" Target="_blank"  HeaderStyle-CssClass="AdminSubTitle"/>
                                <asp:BoundField HeaderText ="Created Date" DataField="CreatedDate" HeaderStyle-CssClass ="AdminSubTitle" />
                            </Columns>
                     </asp:GridView>
                     <br />
                     <asp:Label ID="lblCountFaxes" runat="server" CssClass="WarningMsg" Text=""></asp:Label><br /><br />
                     <asp:Label ID="lblFrom" runat="server" CssClass="TextMed" Text="View All Faxes From"></asp:Label>                    
                    
                    <asp:TextBox ID="txtDateFrom" runat="server" Width="80px" ValidationGroup="vgDate"></asp:TextBox>&nbsp;
                    
                    <asp:Label ID="lblTo" runat="server" CssClass="TextMed" Text="To"></asp:Label>
                    <asp:TextBox ID="txtDateTo" runat="server" Width="80px" ValidationGroup="vgDate"></asp:TextBox>
                    <asp:Button ID="btnCountFaxes" runat="server" Text="Submit" 
                            onclick="btnCountFaxes_Click" ValidationGroup ="vgDate" /><br />
                    <asp:RequiredFieldValidator ID="rfvDateFrom" runat="server" CssClass="WarningMsg" ErrorMessage="Select a date(from) " ControlToValidate="txtDateFrom" Display="Dynamic" ValidationGroup="vgDate"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rfvDateTo" runat="server" CssClass="WarningMsg" ErrorMessage="Select a date(to) " ControlToValidate="txtDateTo" Display="Dynamic" ValidationGroup="vgDate"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cmvDate" runat="server" ControlToCompare="txtDateTo" ControlToValidate="txtDateFrom" CssClass="WarningMsg" Operator ="LessThanEqual" Type="Date" ValidationGroup="vgDate" ErrorMessage="Please select valid date"></asp:CompareValidator>
                    <br />
                    <asp:Label ID="lblShowPageSize" runat="server" CssClass="TextMed" Text="Page size" Visible="false"></asp:Label>&nbsp;
                    <asp:TextBox ID="txtPageSize" runat="server" Text="10" Width="30px" Visible="false"></asp:TextBox>&nbsp;
                    <asp:Label ID="lblShowPageNumber" runat="server" CssClass="TextMed" Text="Page Number" Visible="false"></asp:Label>&nbsp;
                    <asp:TextBox ID="txtPageNumber" runat="server" CssClass="TextMed" Width="30px" Text="1" Visible="false"></asp:TextBox>&nbsp;
                    <asp:Button ID="btnShowFaxes" runat="server" Text="Show Faxes" 
                            onclick="btnShowFaxes_Click" Visible="false" ValidationGroup="vgShowFaxes"/><br />
                    <asp:RegularExpressionValidator ID="revPageSize" runat="server" ControlToValidate="txtPageSize" ErrorMessage="Enter Only Number(s) " CssClass="WarningMsg" ValidationExpression="^\d+$" ValidationGroup="vgShowFaxes"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvPageSize" runat="server" ControlToValidate="txtPageSize" ErrorMessage="Enter a page size field " CssClass="WarningMsg" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revPageNumber" runat="server" ControlToValidate="txtPageNumber" ErrorMessage="Enter Only Number(s) " CssClass="WarningMsg" ValidationExpression="^\d+$" ValidationGroup="vgShowFaxes"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvPageNumber" runat="server" ControlToValidate="txtPageNumber" ErrorMessage="Enter a page number field " CssClass="WarningMsg" Display="Dynamic"></asp:RequiredFieldValidator>
                    <br /><br /><br />
                    <%--<asp:TextBox ID="txtDateToday" runat="server" Visible="false"></asp:TextBox>--%>
                    <a href="https://secure.efaxdeveloper.com" target="_blank" class="TextMed">eFax Report Page</a><br />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
