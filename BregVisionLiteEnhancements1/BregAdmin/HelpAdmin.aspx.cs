﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.BregAdmin;
using BregVision;
using ClassLibrary.DAL;
using DAL.PracticeLocation;

namespace BregVision.BregAdmin
{
    public partial class HelpAdmin : Bases.PageBase
    {
       

        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();
        }

        private void InitializeAjaxSettings()
        {
            Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);
            if (radAjaxManager != null)
            {
                //radAjaxManager.EnableAJAX = false;
                radAjaxManager.AjaxSettings.AddAjaxSetting(grdHelp, grdHelp, RadAjaxLoadingPanel1);
                //radAjaxManager.AjaxSettings.AddAjaxSetting(grdHelp, lblStatus);

            }

        }
        
        protected void grdHelp_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
        }

        protected void grdHelp_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var helpQuery = from h in visionDB.HelpArticles
                                join t in visionDB.HelpEntityTrees on h.Id equals t.EntityId
                                //join ht in visionDB.HelpEntityTypes on t.ParentId equals ht.Id
                                join g in visionDB.HelpGroups on t.ParentId equals g.Id
                                where g.Title.ToLower() == "Vision Tooltips".ToLower()                                
                                && h.HelpEntityTypeId == 2
                                select new
                                {
                                    GroupTitle = g.Title,
                                    TypeName = "Article",
                                    ArticleTitle = h.Title,
                                    ArticleText = h.Description,
                                    GroupID = g.Id,
                                    EntityTypeID = h.HelpEntityTypeId,
                                    ArticleID = h.Id,
                                    TreeID = t.Id,
                                };

                grdHelp.DataSource = helpQuery.ToList();
            }
        }

        protected void grdHelp_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {
            Telerik.Web.UI.GridEditableItem editedItem = e.Item as Telerik.Web.UI.GridEditableItem;

            bool isInsert = editedItem.Edit; //this = true for a new item, false for an existing item.

            Label lblStatus = editedItem.FindControl("lblStatus") as Label;
            lblStatus.Text = "";

            TextBox txtArticleTitle = editedItem.FindControl("txtArticleTitle") as TextBox;
            string articleTitle = txtArticleTitle.Text;

            TextBox txtArticleText = editedItem.FindControl("txtArticleText") as TextBox;
            string articleText = txtArticleText.Text;

            int groupID = 4; 
            int entityTypeID = 2; 
            int articleID = 0;
            int treeID = 0;

            string oldArticleText = "";
            string oldArticleTitle = "";

            if (isInsert == false)
            {
                groupID = Convert.ToInt32(editedItem["GroupID"].Text);
                entityTypeID = Convert.ToInt32(editedItem["EntityTypeID"].Text);
                articleID = Convert.ToInt32(editedItem["ArticleID"].Text);
                treeID = Convert.ToInt32(editedItem["TreeID"].Text);

                oldArticleText = editedItem["ArticleText"].Text;
                oldArticleTitle = editedItem["ArticleTitle"].Text;
            }


            if (oldArticleText != articleText || oldArticleTitle != articleTitle)
            {
                using (var visionDB = VisionDataContext.GetVisionDataContext())
                {
                    HelpArticle article = null;

                    if (isInsert == true)
                    {
                        article = new HelpArticle();
                        visionDB.HelpArticles.InsertOnSubmit(article);
                    }
                    else
                    {
                        var articleQuery = from a in visionDB.HelpArticles
                                           where a.Id == articleID
                                           select a;

                        article = articleQuery.FirstOrDefault();
                    }
                    if (article != null)
                    {
                        article.Title = articleTitle;
                        article.Description = articleText;
                        article.HelpEntityTypeId = entityTypeID;

                        visionDB.SubmitChanges();
                    }

                    if (isInsert == true)
                    {
                        HelpEntityTree tree = new HelpEntityTree();
                        tree.EntityId = article.Id;
                        tree.EntityTypeId = entityTypeID;
                        tree.ParentId = groupID;
                        tree.SortOrder = 0;

                        visionDB.HelpEntityTrees.InsertOnSubmit(tree);
                        visionDB.SubmitChanges();
                    }
                }
            }
        }


    }
}