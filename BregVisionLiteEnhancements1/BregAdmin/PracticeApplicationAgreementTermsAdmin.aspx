﻿<%@ Page Language="C#" ValidateRequest="false"  MasterPageFile="MasterBregAdmin.Master" AutoEventWireup="true" CodeBehind="PracticeApplicationAgreementTermsAdmin.aspx.cs" Inherits="BregVision.BregAdmin.PracticeApplicationAgreementTermsAdmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .hiddencol
        {
            display:none;
        }
    </style>

    <div style="text-align: center">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr align="center">
                <td>
                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                        <tr align="center">
                            <td style="width: 425px">&nbsp;</td>
                            <td style="width: 95px">
                                <a href="BregAdmin.aspx" class="managemenulink">Back</a><br />
                            </td>
                            <td style="width:425px">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                                <table cellpadding="0" cellspacing="0" width="925px">
                                    <tr>
                                        <td  class="AdminPageTitle">
                                        Add New Terms and Conditions
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        
                        
                        
                    </table>
                </td>
            </tr>
        </table>
        <table  cellpadding="0" cellspacing="0" style="width:925px" border="0">
            <tr align="center">
                <td>
                    <asp:TextBox ID="txtContent" rows="15" columns="110" TextMode="multiline" runat="server" />
                </td>
            </tr>
            <tr align="right">
                <td>
                    <asp:Label ID="labStatus" ForeColor="Red" runat="server" />
                    <asp:Button ID="btnPreview" runat="server" Text="Preview" Width="80px" OnClick="btnPreview_Click" />&nbsp;
                    <asp:Button ID="btnAdd" runat="server" Text="Add" Width="80px" OnClick="btnAdd_Click" />
                </td>
            </tr>
            <tr align="center">
                <td class="AdminPageTitle">
                    Preview
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr align="center">
                <td>
                    <asp:Literal ID="litPreview" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr align="center">
                <td class="AdminPageTitle">
                    End Preview
                </td>
            </tr>
        </table>
    </div>
</asp:Content>