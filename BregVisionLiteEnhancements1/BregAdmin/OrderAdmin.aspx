﻿<%@ Page Language="C#"  MasterPageFile="MasterBregAdmin.Master" AutoEventWireup="true" CodeBehind="OrderAdmin.aspx.cs" Inherits="BregVision.BregAdmin.OrderAdmin" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .hiddencol
        {
            display:none;
        }
    </style>

    <div style="text-align: center">
    <table cellpadding="0" cellspacing="0" width="100%" border="0">
        <tr align="center">
            <td colspan="3">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>&nbsp;</td>
                        <td style="width:100px">
                            <a href="BregAdmin.aspx" class="managemenulink">Back</a><br />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <table cellpadding="0" cellspacing="0" width="60%">
                    <tr>
                        <td  class="AdminPageTitle">
                            <a name="OrderAdminTopPage">Order Admin</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width:25%"></td>
            <td align="left">
                <table cellpadding="2" cellspacing="3"border="0">
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="lblSearchPO" Text="PO Number" CssClass="TextMed"></asp:Label>
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblSearchCustomPO" Text="Custom PO Number" CssClass="TextMed"></asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtSearchPO" runat="server"></asp:TextBox></td>
                        <td>
                            <asp:TextBox ID="txtSearchCustomPO" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="btnSearch" runat="server" Text="Search" onclick="btnSearch_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Label ID="lblMessage" runat="server" Font-Size="Small" style="color:Red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width:25%"></td>
        </tr>
        <tr align="center">
            <td colspan="3">
                <asp:GridView runat="server" ID="gvPO" 
                    AutoGenerateColumns="false" GridLines="Both"
                    OnRowEditing="gvPO_RowEditing"
                    OnRowUpdating="gvPO_RowUpdating"
                    OnRowCancelingEdit="gvPO_RowCancelingEdit" CssClass="FieldLabel">
                <Columns>
                    <asp:CommandField ShowCancelButton="true" ShowEditButton="true" HeaderStyle-CssClass="AdminSubTitle" HeaderStyle-Width="120px"/>
                    
                    <asp:TemplateField ControlStyle-CssClass="hiddencol" HeaderStyle-CssClass="AdminSubTitle">
                        <ItemTemplate>
                            <asp:Label ID="lblBregVisionOrderID" runat="server" Text='<%# Bind("BregVisionOrderID") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtBregVisionOrderID" runat="server" Text='<%# Bind("BregVisionOrderID") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Practice Location Name" HeaderStyle-CssClass="AdminSubTitle" HeaderStyle-Width="240px">
                        <ItemTemplate>
                            <asp:Label ID="lblPracticeLocationName" runat="server" Text='<%# Bind("PracticeLocationName") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Label ID="lblPracticeLocationNameEdit" runat="server" Text='<%# Bind("PracticeLocationName") %>'></asp:Label>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Purchase Order" HeaderStyle-CssClass="AdminSubTitle">
                        <ItemTemplate>
                            <asp:Label ID="lblPurchaseOrder" runat="server" Text='<%# Bind("PurchaseOrder") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Label ID="lblPurchaseOrderEdit" runat="server" Text='<%# Bind("PurchaseOrder") %>'></asp:Label>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Custom Purchase Order" HeaderStyle-CssClass="AdminSubTitle">
                        <ItemTemplate>
                            <asp:Label ID="lblCustomPurchaseOrder" runat="server" Text='<%# Bind("CustomPurchaseOrder") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Label ID="lblCustomPurchaseOrderEdit" runat="server" Text='<%# Bind("CustomPurchaseOrder") %>'></asp:Label>
                        </EditItemTemplate>
                    </asp:TemplateField>
                      
                    <asp:TemplateField HeaderText="IsActive" HeaderStyle-CssClass="AdminSubTitle">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkvIsActive" runat="server" Checked='<%# Bind("IsActive") %>' Enabled="false" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkIsActive" runat="server" Checked='<%# Bind("IsActive") %>' />
                        </EditItemTemplate>
                    </asp:TemplateField>
                                
                </Columns>
            </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center"><br />
                <a href="#OrderAdminTopPage" class="FieldLabel">Go to top page</a>
            </td>
        </tr>
    </table>
    </div>
    </asp:Content>


