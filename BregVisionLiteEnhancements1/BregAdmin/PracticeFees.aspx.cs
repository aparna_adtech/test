﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary.DAL;

namespace BregVision.BregAdmin
{
    public partial class PracticeFees : System.Web.UI.Page
    {
        protected VisionDataContext visionDB = null;
        int _practiceID = 0;
        protected bool _levelControlsBound = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            _practiceID = Convert.ToInt32(Request.QueryString["pid"].ToString());

            //visionDB = new VisionDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["BregVisionConnectionString"].ToString());
        }

        protected void LoadFeeLevels()
        {
            visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var isVisionExpress = (from p in visionDB.Practices
                                   where p.PracticeID == _practiceID
                                   select p.IsVisionLite).FirstOrDefault<bool>();

                var feeLevels = from fl in visionDB.BregVisionMonthlyFeeLevels
                                where (isVisionExpress == true && fl.LevelName.ToLower().Contains("express")) || (isVisionExpress == false && fl.LevelName.ToLower().Contains("express") == false)
                                select fl;

                //VisionDataContext.CloseConnection(visionDB);

                ddlFeeLevel.DataSource = feeLevels;
                ddlFeeLevel.DataTextField = "LevelName";
                ddlFeeLevel.DataValueField = "BregVisionMonthlyFeeLevelID";
                ddlFeeLevel.DataBind();
            }
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            Int32 practiceID = _practiceID;
            Int32 bregVisionMonthlyFeeLevelID = Convert.ToInt32(ddlFeeLevel.SelectedValue);
            DateTime startDate = Convert.ToDateTime(dtFrom.SelectedDate);
            Decimal discount = Convert.ToDecimal(txtDiscount.Text);

            BLL.Practice.Practice.SetPracticeFee(practiceID, bregVisionMonthlyFeeLevelID, startDate, discount);
        }



        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            BindLevelControls();
            LoadFeeLevels();
            dtFrom.SelectedDate = DateTime.Now;
            txtDiscount.Text = "0";
        }

        protected void BindLevelControls()
        {

            visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var feeLevels = (from fl in visionDB.BregVisionMonthlyFeeLevels
                                join p_fl in visionDB.Practice_MonthlyFeeLevels
                                    on fl.BregVisionMonthlyFeeLevelID equals p_fl.BregVisionMonthlyFeeLevelID
                                where p_fl.PracticeID == _practiceID
                                orderby p_fl.StartDate
                                select new
                                {
                                    fl.LevelName,
                                    p_fl.StartDate,
                                    p_fl.Discount
                                }).ToList();

               // VisionDataContext.CloseConnection(visionDB);

                gvPracticeFees.DataSource = feeLevels;   //.OrderBy(p => p.MinTransactions);
                gvPracticeFees.DataBind();
                _levelControlsBound = true;
            }
        }


    }
}
