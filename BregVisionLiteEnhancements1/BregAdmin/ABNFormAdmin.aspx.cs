﻿using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Web.UI.WebControls;
using ClassLibrary.DAL;


namespace BregVision.BregAdmin
{
    public partial class ABNFormAdmin : System.Web.UI.Page
    {
        protected IEnumerable<ABN_HCPC> _ABN_HCPCs;
        VisionDataContext visionDB = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadABN_HCPCsMasterList( "");
            if(IsPostBack)
            {
                lblMessage.Visible = false;
                lblMessage.Text = "";
            }

        }

        protected void LoadABN_HCPCsMasterList(string SearchHCPCs)
        {
            SearchHCPCs = "%" + SearchHCPCs + "%";

            visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                _ABN_HCPCs = (from hcpcs in visionDB.ABN_HCPCs
                             where SqlMethods.Like(hcpcs.HCPCs, SearchHCPCs)
                             orderby hcpcs.HCPCs
                             select hcpcs).ToList();

                //VisionDataContext.CloseConnection(visionDB);
            }
        }

        protected bool FindHCPCs(string SearchHCPCs)
        {

            visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                var hcPcsFound = from hcpcs in visionDB.ABN_HCPCs
                                 where hcpcs.HCPCs == SearchHCPCs
                                 orderby hcpcs.HCPCs
                                 select hcpcs;

                //VisionDataContext.CloseConnection(visionDB);

                if (hcPcsFound.Count() > 0)
                {
                    return true;
                }
                return false;
            }
            
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadABN_HCPCsMasterList(txtSearchHCPCs.Text);
            if (_ABN_HCPCs.Count() < 1)
            {
                btnAdd.Enabled = true;
            }
            else
            {
                btnAdd.Enabled = false;
            }

            lblMessage.Visible = true;
            lblMessage.Text = _ABN_HCPCs.Count() + " item(s) found.";
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
                if (FindHCPCs(txtSearchHCPCs.Text) == false)
                {
                    visionDB = VisionDataContext.GetVisionDataContext();

                    using (visionDB)
                    {
                        ABN_HCPC abn_HCPC = new ABN_HCPC();
                        abn_HCPC.HCPCs = txtSearchHCPCs.Text;
                        abn_HCPC.ABN_Needed = true;
                        //iCD9.IsActive = true;
                        //iCD9.CreatedUserID = 1;
                        //iCD9.CreatedDate = DateTime.Now;
                        //iCD9.ModifiedUserID = 1;
                        //iCD9.ModifiedDate = DateTime.Now;

                        visionDB.ABN_HCPCs.InsertOnSubmit(abn_HCPC);
                        visionDB.SubmitChanges();

                        btnAdd.Enabled = false;
                        lblMessage.Visible = false;
                    }
                }
                //VisionDataContext.CloseConnection(visionDB);
        }

        protected void gvHCPCs_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvHCPCs.EditIndex = e.NewEditIndex;
        }

        protected void gvHCPCs_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvHCPCs.EditIndex = -1;
        }

        protected void gvHCPCs_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                GridViewRow row = gvHCPCs.Rows[e.RowIndex];
                if ((row.RowState == (DataControlRowState.Edit | DataControlRowState.Alternate)) || (row.RowState == DataControlRowState.Edit))
                {

                    ABN_HCPC abn_HCPC = null;
                    abn_HCPC = _ABN_HCPCs.SingleOrDefault<ABN_HCPC>(p => p.HCPCs.Equals(row.Cells[0].GetTextBoxValue("txtHCPCs")));

                    if (abn_HCPC == null)
                    {
                        abn_HCPC = new ABN_HCPC();
                        visionDB.ABN_HCPCs.InsertOnSubmit(abn_HCPC);
                    }
                    else
                    {
                        visionDB.ABN_HCPCs.Attach(abn_HCPC);
                    }

                    abn_HCPC.HCPCs = row.Cells[0].GetTextBoxValue("txtHCPCs");
                    abn_HCPC.ABN_Needed = row.Cells[1].GetCheckBoxValue("chkIsActive");

                   
                    
                    
                    visionDB.SubmitChanges();
                    gvHCPCs.EditIndex = -1;
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            BindLevelControls();
        }

        protected void BindLevelControls()
        {
            LoadABN_HCPCsMasterList(txtSearchHCPCs.Text);
            gvHCPCs.DataSource = _ABN_HCPCs;
            gvHCPCs.DataBind();

        }

    }
}
