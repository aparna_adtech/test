﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using ClassLibrary.DAL;
using ClassLibrary.DAL.MasterCatalog;
using SpreadsheetLight;
using DocumentFormat.OpenXml.Spreadsheet;

namespace BregVision.BregAdmin
{
    public partial class PracticeCost : System.Web.UI.Page
    {
        int _practiceID = 0;
        string itemNumber;
        int i = 0;
        List<String> itemsList = new List<string>();

        protected void Page_Load(object sender, EventArgs e)
        {
            _practiceID = Convert.ToInt32(Request.QueryString["pid"].ToString());
            lblPracticeName.Text = Session["PracticeName"].ToString();
            if(_practiceID != 211)
            {
                rdCrystal.Enabled = false;
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (this.FileUpload1.HasFile)
            {
                SaveFile(FileUpload1.PostedFile);
            }
            else
            {
                lblUploadStatus.Text = "You did not specify a file to upload";
            }
        }

        private void SaveFile(HttpPostedFile file)
        {
            string path = Server.MapPath("~/MasterCostList");
            if (!System.IO.Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string sDate = DateTime.Now.ToString("yyyyMMddHHmmss");

            string savePath = path; //"c:\\" + "test\\";
            string fileName = sDate + FileUpload1.FileName;
            string filePath = Path.Combine(savePath, fileName);


            if (System.IO.File.Exists(filePath))
            {
                lblUploadStatus.Text = "A file with the same name already exists.";
                return;
            }

            FileUpload1.SaveAs(filePath);
            lblUploadStatus.Text = "Your file was uploaded successfully.";

            ProcessPriceList(filePath);
        }

        private List<MasterCatalogPriceUpdate> masterCatalogPriceUpdates = new List<MasterCatalogPriceUpdate>();
        private void ProcessPriceList(string filePath)
        {
            //col1 = itemnumber, col4 is price
            SLDocument slDocument = new SLDocument(filePath);
            slDocument.ApplyNamedCellStyleToColumn(1, SLNamedCellStyleValues.Title);
            Dictionary<SLCellPoint, SLCell> cells = slDocument.GetCells();

            Dictionary<SLCellPoint, SLCell>.KeyCollection keyCollection = cells.Keys;
            int rowMax = (from k in keyCollection
                          select k.RowIndex).Max();

            int rowMin = (from k in keyCollection
                          select k.RowIndex).Min();

            for (int i = rowMin; i <= rowMax; i++)
            {
                SLCellPoint itemKey = new SLCellPoint(i, 2);  // code
                SLCellPoint priceKey = new SLCellPoint(i, 4); // wholesale price

                if (keyCollection.Contains(itemKey) && keyCollection.Contains(priceKey))
                {
                    SLCell itemNumberCell = cells[itemKey];
                    SLCell priceCell = cells[priceKey];

                    var price = priceCell.NumericValue;
                    // 
                    // Item number or packaging which is string should be trimmed.

                    if (Convert.ToDecimal(price) > 0)
                    {
                        //string itemNumber = itemNumberCell.CellText;
                        itemNumber = itemNumberCell.CellText;

                        if (itemNumberCell.DataType == CellValues.SharedString)
                        {
                            List<SLRstType> sharedStrings = slDocument.GetSharedStrings();
                            itemNumber = sharedStrings.ElementAt(Convert.ToInt32(itemNumberCell.NumericValue)).GetText();
                            masterCatalogPriceUpdates.Add(new MasterCatalogPriceUpdate(itemNumber, Convert.ToDecimal(price), "EA"));
                        }
                    }
                }
            }

            // Check if the file is for Practice Catalog Product or Third Party Product
            if (rbPracticeCatalogProduct.Checked == true)
            {
                UpdatePracticeCatalog();
            }
            else if (rdThirdPartyProduct.Checked == true)
            {
                UpdateThirdPartyProduct();
            }
            else 
            {
                UpdateCrystalProduct();
            }

            slDocument.CloseWithoutSaving();
            slDocument.Dispose();
        }

        private void UpdatePracticeCatalog()
        {
            //need practiceId  
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                var query = (from mcp in visionDB.MasterCatalogProducts
                            //join pcp in visionDB.PracticeCatalogProducts
                                //on mcp.MasterCatalogProductID equals pcp.MasterCatalogProductID
                                where mcp.Packaging == "EA"
                            select new
                            {
                                mcp.Code,
                                mcp.Name,
                                mcp.WholesaleListCost,
                                PracticeCatalogProducts = (from ppcp in mcp.PracticeCatalogProducts
                                                               where ppcp.PracticeID == _practiceID
                                                               select ppcp)
                            }).ToList();

                foreach (var priceUpdate in masterCatalogPriceUpdates) // excel
                {
                    var masterCatalogProductList = query.Where(c => c.Code == priceUpdate.Code).ToList(); // compare between 'Code' in sql and excel
                    if (masterCatalogProductList != null)
                    {
                        foreach (var masterCatalogProduct in masterCatalogProductList)
                        {
                            foreach (var practiceCatalogProduct in masterCatalogProduct.PracticeCatalogProducts)
                            {
                                string oldPrice = Convert.ToDouble(practiceCatalogProduct.WholesaleCost).ToString("F2");
                                string newPrice = Convert.ToDouble(priceUpdate.WholesaleListCost).ToString("F2");

                                practiceCatalogProduct.WholesaleCost = priceUpdate.WholesaleListCost.Value;
                              
                                i = i + 1;

                                itemsList.Add(masterCatalogProduct.Code + "  " + masterCatalogProduct.Name + " -- " + "Old Price: " + oldPrice + "   New Price: " + newPrice);
                            }
                        }
                    }
                }
                gvUpdateInfo.DataSource = itemsList;
                gvUpdateInfo.DataBind();

                ShowMessage();

                visionDB.SubmitChanges();
            }
        }

        private void UpdateThirdPartyProduct()
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                var query = (from tpp in visionDB.ThirdPartyProducts
                            join pcp in visionDB.PracticeCatalogProducts
                            on tpp.ThirdPartyProductID equals pcp.ThirdPartyProductID
                            where pcp.IsThirdPartyProduct == true
                            && pcp.PracticeID == _practiceID
                            select tpp).ToList();

                foreach (var priceUpdate in masterCatalogPriceUpdates) // excel
                {
                    var thirdPartyProductList = query.Where(c => c.Code == priceUpdate.Code).ToList();
                    if (thirdPartyProductList != null)
                    {
                        foreach (var thirdPartyProduct in thirdPartyProductList)
                        {
                            string oldPrice = Convert.ToDouble(thirdPartyProduct.WholesaleListCost).ToString("F2");
                            string newPrice = Convert.ToDouble(priceUpdate.WholesaleListCost).ToString("F2");

                            thirdPartyProduct.WholesaleListCost = priceUpdate.WholesaleListCost.Value;
                            
                            i = i + 1;

                            itemsList.Add(thirdPartyProduct.Code + "    " + thirdPartyProduct.Name + " -- " + "Old Price: " + oldPrice + "   New Price: " + newPrice);
                        }
                    }
                }
                gvUpdateInfo.DataSource = itemsList;
                gvUpdateInfo.DataBind();

                ShowMessage();

                visionDB.SubmitChanges();
            }
        }

        private void UpdateCrystalProduct()
        {            
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                var query = (from tpp in visionDB.ThirdPartyProducts
                             join pcp in visionDB.PracticeCatalogProducts
                             on tpp.ThirdPartyProductID equals pcp.ThirdPartyProductID
                             where pcp.IsThirdPartyProduct == true
                             && pcp.PracticeID == _practiceID
                             && pcp.PracticeCatalogSupplierBrandID == 3140 // BREG
                             select tpp).ToList();
                
                foreach (var priceUpdate in masterCatalogPriceUpdates) // excel 
                {
                    var crystalProductList = query.Where(c => c.Code.EndsWith(priceUpdate.Code));//.Contains(priceUpdate.Code)).ToList(); 
                  
                    if (crystalProductList != null)
                    {
                        foreach (var crystalProduct in crystalProductList)
                        {
                            string oldPrice = Convert.ToDouble(crystalProduct.WholesaleListCost).ToString("F2");
                            string newPrice = Convert.ToDouble(priceUpdate.WholesaleListCost).ToString("F2");

                            crystalProduct.WholesaleListCost = priceUpdate.WholesaleListCost.Value;
                            
                            i = i + 1;

                            itemsList.Add(crystalProduct.Code + "    " + crystalProduct.Name + " -- " + "Old Price: " + oldPrice + "   New Price: " + newPrice);
                        }
                    }
                }
                gvUpdateInfo.DataSource = itemsList;
                gvUpdateInfo.DataBind();

                ShowMessage();

                visionDB.SubmitChanges();
            }
        }

        private void ShowMessage()
        {
            if (i == 0)
            {
                lblUploadStatus.Text = "No Items in spreadsheet match practice codes.";
            }
            else
            {
                lblUploadStatus.Text = i + " items updated successfully.";
            }
        }

        protected void gvUpdateInfo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if(e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Text = "Update Result";
            }
        }
    }
}