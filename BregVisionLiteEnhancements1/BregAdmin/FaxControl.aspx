﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FaxControl.aspx.cs" Inherits="BregVision.BregAdmin.FaxControl" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1 id="hTitle" runat="server"></h1>
        <table>
            <tr>
                <td>
                    <asp:Button ID="btnEnable" runat="server" Text="Enable" 
                        onclick="btnEnable_Click" />
                </td>
                <td>
                    <asp:Button ID="btnDisable" runat="server" Text ="Disable" 
                        onclick="btnDisable_Click" />
                </td>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server" ></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
