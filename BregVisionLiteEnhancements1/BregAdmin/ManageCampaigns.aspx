﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageCampaigns.aspx.cs" Inherits="BregVision.BregAdmin.ManageCampaigns" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
        
    <div>
        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="3" MultiPageID="RadMultiPageMain">
            <Tabs>
                <telerik:RadTab ImageUrl="../Images/photo16.png" Text="Manage Campaign Media" PageViewID="pvMedia">
                </telerik:RadTab>
                <telerik:RadTab ImageUrl="../Images/campaign16.png" Text="Manage Campaigns" PageViewID="pvCampaigns">
                </telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip>
        
        <telerik:RadMultiPage runat="server" ID="RadMultiPageMain" SelectedIndex="0" BorderWidth="1">
            <telerik:RadPageView runat="server" ID="pvMedia">
                 Manage Media Page
             </telerik:RadPageView> 
             
             <telerik:RadPageView runat="server" ID="pvCampaigns">
                 Manage Campaigns
             </telerik:RadPageView> 

        </telerik:RadMultiPage>
    </div>
    </form>
</body>
</html>
