<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="UserBregAdmin.ascx.cs" Inherits="BregVision.BregAdmin.UserBregAdmin" %>
<table cellspacing="3" cellpadding="0" border="0">
    <!--Used to specify the Breg Admin's Login Name and Login Status and to hold the Breg Admin's UserID  -->
    <tr>
        <td style="height: 22px; width: 100%">
            <span>
                <asp:LoginName ID="LoginName1" runat="server" FormatString="Welcome, {0}" Font-Size="x-Small" />
                <asp:LoginStatus ID="lsUSer" runat="server" Font-Size="X-Small" LogoutText="(Log Out)"
                    LogoutPageUrl="~/default.aspx" LogoutAction="RedirectToLoginPage" />
            </span>&nbsp; <span>
                &nbsp;
            </span>&nbsp; <span id="spnChangeLocation" style="visibility: visible">
                &nbsp;
                &nbsp; 
           </span>
        </td>
    </tr>
</table>

