﻿<%@ Page Language="C#" MasterPageFile="MasterBregAdmin.Master" AutoEventWireup="true" CodeBehind="ICD9Admin.aspx.cs" Inherits="BregVision.BregAdmin.ICD9Admin" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .hiddencol
        {
            display:none;
        }
    </style>

    <div style="text-align: center">
    <table cellpadding="0" cellspacing="0" width="100%" border="0">
        <tr align="center">
            <td style="width: 45%">&nbsp;</td>
            <td>
                <a href="BregAdmin.aspx" class="managemenulink">Back</a><br />
            </td>
            <td style="width: 45%">&nbsp;</td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr align="center">
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="875px">
                    <tr>
                        <td colspan="3" class="AdminPageTitle">
                            <div><a name="ICD9TopPage">Dx Code Admin (Viewing top 500 Records)</a></div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table cellpadding="0" cellspacing="0" width="40%" border="0">
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="lblSearchHCPCs" Text="HCPCs" CssClass="TextMed"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lblSearchICD9" Text="Dx Code" CssClass="TextMed"></asp:Label>
                                    </td>
                                    <td>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtSearchHCPCs" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtSearchICD9" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" onclick="btnSearch_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:Label ID="lblMessage" runat="server" Font-Size="Small" style="color:Red" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table><br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:GridView runat="server" ID="gvICD9s" 
                    AutoGenerateColumns="false" GridLines ="Both" OnRowEditing="gvICD9s_RowEditing" 
                    OnRowUpdating="gvICD9s_RowUpdating" CssClass="FieldLabel"
                    OnRowCancelingEdit="gvICD9s_RowCancelingEdit">
                <Columns>
                    <asp:CommandField ShowCancelButton="true" ShowEditButton="true" HeaderStyle-Width ="120px" ControlStyle-Height="25px"  HeaderStyle-CssClass ="AdminSubTitle" HeaderStyle-VerticalAlign ="Middle" ItemStyle-VerticalAlign ="Middle"/>
                    
                    <asp:TemplateField ControlStyle-CssClass="hiddencol" HeaderStyle-CssClass ="AdminSubTitle">
                        <ItemTemplate>
                            <asp:Label ID="lblICD9ID" runat="server" Text='<%# Bind("ICD9ID") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtICD9ID" runat="server" Text='<%# Bind("ICD9ID") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                
                    <asp:TemplateField HeaderText="HCPCs" HeaderStyle-Width ="140px" ItemStyle-Height ="25px" HeaderStyle-CssClass ="AdminSubTitle">
                        <ItemTemplate>
                            <asp:Label ID="lblHCPCs" runat="server" Text='<%# Bind("HCPCs") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtHCPCs" runat="server" Text='<%# Bind("HCPCs") %>' Width="110px" CssClass="FieldLabel"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvHCPCs" runat="server" ControlToValidate="txtHCPCs" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    
                   <asp:TemplateField HeaderText="ICD9Code" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width ="120px" ItemStyle-Height ="25px" HeaderStyle-CssClass ="AdminSubTitle">
                        <ItemTemplate>
                            <asp:Label ID="lblICD9Code" runat="server" Text='<%# Bind("ICD9Code") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtICD9Code" runat="server" Text='<%# Bind("ICD9Code") %>' Width="100px" CssClass="FieldLabel"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvICD9Code" runat="server" ControlToValidate="txtICD9Code" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    
                   <asp:TemplateField HeaderText="ICD9Description" HeaderStyle-Width="350px" ItemStyle-Height ="25px" HeaderStyle-CssClass ="AdminSubTitle" HeaderStyle-HorizontalAlign ="Justify">
                        <ItemTemplate>
                            <asp:Label ID="lblICD9Description" runat="server" Text='<%# Bind("ICD9Description") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtICD9Description" runat="server" Text='<%# Bind("ICD9Description") %>' Width="325px" CssClass="FieldLabel"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvICD9Description" runat="server" ControlToValidate="txtICD9Description" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="IsActive" HeaderStyle-Width="70px" ItemStyle-Height ="25px" HeaderStyle-CssClass ="AdminSubTitle">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkvIsActive" runat="server" Checked='<%# Bind("IsActive") %>' Enabled="false" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkIsActive" runat="server" Checked='<%# Bind("IsActive") %>' />
                        </EditItemTemplate>
                    </asp:TemplateField>
                </Columns>
                </asp:GridView>
            </td>
        </tr>    
        <tr align="center">
            <td>
                   <table width="70%" border="0" cellpadding="0" cellspacing="0">
                        <tr><td colspan="1" align="right" class="FieldLabel"><a href="#ICD9TopPage">Go to top page</a></td></tr>
                        <tr>
                            <td align="left"><hr />
                                <table border="0" cellpadding="0" cellspacing="6" width="42%">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblHCPCS" runat="server" Text="HCPCs" CssClass="TextMed"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblICD9Code" runat="server" Text="DXCode" CssClass="TextMed"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblICD9Description" runat="server" Text="DXCodeDescription" CssClass="TextMed"></asp:Label>
                                        </td>
                                        <td>
                                        
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtHCPCs" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtICD9Code" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtICD9Description" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnAdd" runat="server" Text="Add" onclick="btnAdd_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                   </table>
            </td>
        </tr>
    </table>
    </div>
    </asp:Content>
