﻿<%@ Page Language="C#" MasterPageFile="MasterBregAdmin.Master" AutoEventWireup="true" CodeBehind="ABNFormAdmin.aspx.cs" Inherits="BregVision.BregAdmin.ABNFormAdmin" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .hiddencol
        {
            display:none;
        }
    </style>

    <div style="text-align: center">
    <table cellpadding="0" cellspacing="0" width="100%" border="0">
        <tr>
            <td colspan="3">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>&nbsp;</td>
                        <td style="width:100px">
                            <a href="BregAdmin.aspx" class="managemenulink">Back</a><br />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <table cellpadding="0" cellspacing="0" width="50%">
                    <tr>
                        <td  class="AdminPageTitle">
                            ABNForm Required By HCPC - Admin
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width:25%"></td>
            <td>
                <table cellpadding="2" cellspacing="3" border="0">
                    <tr align="left">
                        <td colspan="3">
                            <asp:Label runat="server" ID="lblSearchHCPCs" Text="HCPCs" CssClass="TextMed"></asp:Label>
                        </td>
                        
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:TextBox ID="txtSearchHCPCs" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="btnSearch" runat="server" Text="Search" onclick="btnSearch_Click" />
                        </td>
                        <td>
                            <asp:Button ID="btnAdd" runat="server" Text="Add" onclick="btnAdd_Click" Enabled="false" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="left">
                            <asp:Label ID="lblMessage" runat="server" CssClass="WarningMsg" Visible="false"></asp:Label><br />
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width:25%"></td>
        </tr>
        <tr align="center">
            <td colspan="3">
                <asp:GridView runat="server" ID="gvHCPCs" 
                    AutoGenerateColumns="false" GridLines="Both"
                    OnRowEditing="gvHCPCs_RowEditing"
                    OnRowUpdating="gvHCPCs_RowUpdating" CssClass="FieldLabel"
                    OnRowCancelingEdit="gvHCPCs_RowCancelingEdit" Width="460px">
                <Columns>
                    <asp:CommandField ShowCancelButton="true" ShowEditButton="true" HeaderStyle-CssClass="AdminSubTitle"/>
                    <asp:TemplateField HeaderText="HCPCs" HeaderStyle-CssClass="AdminSubTitle" HeaderStyle-Width="250px" ItemStyle-Width="250px">
                        <ItemTemplate>
                            <asp:Label ID="lblHCPCs" runat="server" Text='<%# Bind("HCPCs") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtHCPCs" runat="server" Text='<%# Bind("HCPCs") %>' CssClass="FieldLabel" Width="160px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvHCPCs" runat="server" ControlToValidate="txtHCPCs" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </EditItemTemplate>
                    </asp:TemplateField>
                      
                    <asp:TemplateField HeaderText="IsActive" HeaderStyle-CssClass="AdminSubTitle" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkvIsActive" runat="server" Checked='<%# Bind("ABN_Needed") %>' Enabled="false" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkIsActive" runat="server" Checked='<%# Bind("ABN_Needed") %>' />
                        </EditItemTemplate>
                    </asp:TemplateField>  
                                
                </Columns>
            </asp:GridView>
            </td>
        </tr>    
    </table>
    </div>
    </asp:Content>

