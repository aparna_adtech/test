using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BLL;
using BLL.Practice;
using System.Text;
using BregVision.BregAdmin;
using System.Collections.Generic;
using System.Linq;

public partial class BregAdmin : System.Web.UI.Page
{
	//need to add common session variables up here.
    int userID = 1;
    List<ClassLibrary.DAL.PaymentType> _PaymentTypes = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindGridPractices();
            RadWindowManager1.EnableStandardPopups = false;//added by mws 12/17/09
            RadWindowManager2.EnableStandardPopups = false;
        }
        _PaymentTypes = BLL.Practice.PaymentType.GetPaymentTypes();

        SetGridExtensionFieldNames();
    }



    private void SetGridExtensionFieldNames()
    {
            Dictionary<string, int> summaryFields = new Dictionary<string, int>();
            summaryFields.Add("PracticeID", 1);
            summaryFields.Add("ddlPracticePaymentType", 13);
            summaryFields.Add("txtBillingStartDate", 14);
            summaryFields.Add("txtPracticeCode", 15);
            GridViewExtensions.fieldNames = summaryFields; 
    }

    //  Bind the Data to the GridPractices.
    protected void BindGridPractices()
    {
		BLL.Practice.Practice bllPractice = new BLL.Practice.Practice();
        DataSet ds = bllPractice.GetAllPractices();
        GridPractices.DataSource = ds;
        GridPractices.DataBind();
    }

    protected void GridPractices_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            bool isVisible = false;
            bool isVisionLite = (bool)DataBinder.Eval(e.Row.DataItem, "IsVisionLite");
            bool isActive = (bool)DataBinder.Eval(e.Row.DataItem, "IsActive");
            string practiceName = (string)DataBinder.Eval(e.Row.DataItem, "PracticeName");
            int paymentTypeID = (int)DataBinder.Eval(e.Row.DataItem, "PaymentTypeID");

            LinkButton btnDetails = e.Row.FindControl("btnDetails") as LinkButton;
            btnDetails.Visible = Website.IsVisionExpress() == isVisionLite;

            if (chkShowVisionRecords.Checked)
            {
                if (!isVisionLite)
                {
                    isVisible = true;
                }
            }

            
            if (chkShowVisionExpressRecords.Checked)
            {
                if (isVisionLite)
                {
                    isVisible = true;
                }
            }

            if (isVisible)
            {
                if (!chkShowAllRecords.Checked)
                {
                    isVisible = isActive; //only show active records in the datagrid
                }
            }

            if (isVisible)
            {
                if (!chkShowTextPractices.Checked)
                {
                    //eventually we will have a column in the db for this so we don't affect reports with these acts.
                    if (practiceName.ToLower().Contains("test"))
                    {
                        isVisible = false;
                    }
                    
                }
            }
            
            e.Row.Visible = isVisible;

            Button btnAction = (Button)e.Row.FindControl("btnAction");
            if (isVisionLite && !isActive)
            {
                btnAction.Text = "Resubscribe";
            }
            else if (!(bool)DataBinder.Eval(e.Row.DataItem, "IsVisionLite"))
            {
                btnAction.Text = "Downgrade";
            }
            else
            {
                //in order to downgrade or resubscribe the practice must either be a vision user or have IsActive=false
                btnAction.Visible = false;
            }

          
            if ((e.Row.RowState & DataControlRowState.Edit) != 0)
            {
                DropDownList ddlPracticePaymentType = e.Row.GetDropDownList("ddlPracticePaymentType");
                if (ddlPracticePaymentType != null)
                {
                    ddlPracticePaymentType.DataSource = _PaymentTypes;
                    ddlPracticePaymentType.DataTextField = "PaymentTypeName";
                    ddlPracticePaymentType.DataValueField = "PaymentTypeID";
                    ddlPracticePaymentType.DataBind();

                    ddlPracticePaymentType.SelectedValue = paymentTypeID.ToString();
                }
            }

        }

    }
    

    // Update the edited row.
    protected void GridPractices_RowUpdating( object sender, GridViewUpdateEventArgs e )
    {
        int practiceID = Convert.ToInt32( GridPractices.DataKeys[e.RowIndex].Values[0].ToString() );
        TextBox txtPracticeName = ( TextBox ) GridPractices.Rows[e.RowIndex].FindControl( "txtPracticeName" );
        CheckBox chkIsOrthoSelect = (CheckBox)GridPractices.Rows[e.RowIndex].FindControl("chkIsOrthoSelect");
        CheckBox chkCameraScanEnabled = (CheckBox)GridPractices.Rows[e.RowIndex].FindControl("chkCameraScanEnabled");
		CheckBox chkIsBregBilling = (CheckBox)GridPractices.Rows[e.RowIndex].FindControl("chkIsBregBilling");
		CheckBox chkFaxDispenseReceiptEnabled = (CheckBox)GridPractices.Rows[e.RowIndex].FindControl("chkFaxDispenseReceiptEnabled");
        CheckBox chkEMRPullEnabled = (CheckBox)GridPractices.Rows[e.RowIndex].FindControl("chkEMRPullEnabled");
        CheckBox chkCustomFitEnabled = (CheckBox)GridPractices.Rows[e.RowIndex].FindControl("chkCustomFitEnabled");
        CheckBox chkSecMsgIntEnabled = (CheckBox)GridPractices.Rows[e.RowIndex].FindControl("chkSecMsgIntEnabled");
        CheckBox chkSecMsgExtEnabled = (CheckBox)GridPractices.Rows[e.RowIndex].FindControl("chkSecMsgExtEnabled");
        CheckBox chkIsActive = (CheckBox)GridPractices.Rows[e.RowIndex].FindControl("chkIsActive");
        CheckBox chkIsVisionExpress = (CheckBox)GridPractices.Rows[e.RowIndex].FindControl("chkIsVisionExpress");
		Int32 PaymentTypeID = GridPractices.Rows[e.RowIndex].GetDropDownListValueInt32("ddlPracticePaymentType");
        string strBillingStartDate = GridPractices.Rows[e.RowIndex].GetTextBoxValue("txtBillingStartDate");
        DateTime billingStartDate = DateTime.MinValue;
        DateTime.TryParse(strBillingStartDate, out billingStartDate);
        string practiceCode = GridPractices.Rows[e.RowIndex].GetTextBoxValue("txtPracticeCode");

		BLL.Practice.Practice bllP = new BLL.Practice.Practice();
        bllP.PracticeID = practiceID;
        bllP.PracticeName = txtPracticeName.Text.ToString();
        bllP.UserID = userID;
        bllP.IsOrthoSelect = chkIsOrthoSelect.Checked;
        bllP.CameraScanEnabled = chkCameraScanEnabled.Checked;
		bllP.IsBregBilling = chkIsBregBilling.Checked;
		bllP.FaxDispenseReceiptEnabled = chkFaxDispenseReceiptEnabled.Checked;
        bllP.EMRIntegrationPullEnabled = chkEMRPullEnabled.Checked;
        bllP.IsCustomFitEnabled = chkCustomFitEnabled.Checked;
		bllP.SecureMessagingInternalEnabled = chkSecMsgIntEnabled.Checked;
        bllP.SecureMessagingExternalEnabled = chkSecMsgExtEnabled.Checked;
        bllP.PaymentType = (BLL.Practice.PaymentType.PaymentTypeEnum)PaymentTypeID;
        bllP.BillingStartDate = billingStartDate;
        bllP.PracticeCode = practiceCode;
        bllP.Update( bllP );

        
        DAL.Practice.Practice dalPractice = new DAL.Practice.Practice();
        dalPractice.UpdateIsActive(bllP, chkIsActive.Checked); //inactivate or activate as needed
        dalPractice.UpgradeDowngradePractice(bllP, chkIsVisionExpress.Checked);
        
        GridPractices.EditIndex = -1;
        BindGridPractices();
        
    } 

    //  User Clicked Edit, so allow the row to be edited.
    protected void GridPractices_RowEditing( object sender, GridViewEditEventArgs e )
    {
        GridPractices.EditIndex = e.NewEditIndex;
        BindGridPractices();
    }

    //  User Canceled the Row Edit, so remove the editing from the row.
    protected void GridPractices_RowCancelingEdit( object sender, GridViewCancelEditEventArgs e )
    {
        GridPractices.EditIndex = -1;
        BindGridPractices();
    }



    //  User click Delete so Delete the Practice.
    public void GridPractices_RowDeleting( Object sender, GridViewDeleteEventArgs e )
    {
        int practiceID = Convert.ToInt32( GridPractices.DataKeys[e.RowIndex].Values[0].ToString() );
        //GridPractices.EditIndex = ( int ) e.Item.ItemIndex;
        //GridPractices.DataSource = ... 
		BLL.Practice.Practice bllPractice = new BLL.Practice.Practice();
        bllPractice.PracticeID = practiceID;
        bllPractice.UserID = userID;
        bllPractice.Delete( bllPractice );
        BindGridPractices();
    }

    // User Clicked "AddNew" or "Insert" or "Cancel" (ed) the AddNew.
    protected void GridPractices_RowCommand( object sender, GridViewCommandEventArgs e )
    {
        int index;

		if ( ( e.CommandName.Equals( "GoToPracticeLocation" ) ) 
                || ( e.CommandName.Equals( "GoToPracticeContact" ) )
				|| ( e.CommandName.Equals( "GoToPracticeBillToAddress" ) ) 
                || ( e.CommandName.Equals( "GoToPracticePhysicians" ) )
				|| ( e.CommandName.Equals( "GoTo_Practice_Admin" ) )
                || (e.CommandName.Equals("SetPracticeFees"))
                || (e.CommandName.Equals("SetConsignments"))
                || (e.CommandName.Equals("GoToCostUpdate"))
                || (e.CommandName.Equals("GoToCustomForms"))
            )  //GoTo_Practice_Admin
		{
            //Find the selected row index
            index = Convert.ToInt32(e.CommandArgument);

			//Set the PracticeLocationID Session variable to the datakey value of the selected row
			Session["PracticeID"] = Convert.ToInt32( GridPractices.DataKeys[index].Value );
			Session["PracticeName"] = GridPractices.DataKeys[index].Values[1];
            bool isVisionLite = (bool)GridPractices.DataKeys[index].Values[2];

            //Set our PracticeLocationID to the first practice location in the practice so things don't fail
            {
                var p_info =
                    new BLL.PracticeLocation.PracticeLocation()
                        .SelectAll((Int32)Session["PracticeID"])
                            .Rows
                            .Cast<DataRow>()
                            .Where(r => (r["IsPrimaryLocation"] != System.DBNull.Value) && (bool)r["IsPrimaryLocation"])
                            .Select(r => new { ID = Convert.ToInt32(r["PracticeLocationID"]), Name = r["Name"].ToString() })
                            .FirstOrDefault();
                if (p_info != null)
                {
                    Session["PracticeLocationID"] = p_info.ID;
                    Session["PracticeLocationName"] = p_info.Name;
                }
            }
                        
			//Go to page
			switch ( e.CommandName )
			{
				case "GoToPracticeLocation":
					Response.Redirect( "~/Admin/PracticeLocation_All.aspx" );
					break;
				case "GoToPracticeContact":
					Response.Redirect( "~/Admin/Practice_Contact.aspx" );
					break;
				case "GoToPracticeBillToAddress":
					Response.Redirect( "~/Admin/Practice_BillingAddress.aspx" );
					break;
				case "GoToPracticePhysicians":
					Response.Redirect( "~/Admin/Practice_Physicians.aspx" );
					break;
				case "GoTo_Practice_Admin":
                    string adminUrl = BLL.Website.GetAdminPath(isVisionLite);
                    Response.Redirect(adminUrl);
					break;
                case "SetPracticeFees":
                    RadWindowManager1.EnableStandardPopups = true; //added by mws 12/17/09
                    RadWindowManager1.Windows[0].NavigateUrl = "~/BregAdmin/PracticeFees.aspx?pid=" + Session["PracticeID"].ToString();
                    RadWindowManager1.Windows[0].VisibleOnPageLoad = true;
                    RadWindowManager1.Windows[0].Visible = true;
                    RadWindowManager2.Windows[0].VisibleOnPageLoad = false;
                    RadWindowManager2.Windows[0].Visible = false;
                    break;
                case "SetConsignments":
                    RadWindowManager2.EnableStandardPopups = true; //added by ysi 09/21/09
                    RadWindowManager2.Windows[0].NavigateUrl = "~/BregAdmin/ConsignmentRep.aspx?pid=" + Session["PracticeID"].ToString();
                    RadWindowManager2.Windows[0].VisibleOnPageLoad = true;
                    RadWindowManager2.Windows[0].Visible = true;
                    RadWindowManager1.Windows[0].VisibleOnPageLoad = false;
                    RadWindowManager1.Windows[0].Visible = false;
                    break;
                case "GoToCostUpdate":
                    Response.Redirect("~/BregAdmin/PracticeCost.aspx?pid=" + Session["PracticeID"].ToString());
                    break;
                case "GoToCustomForms":
                    Response.Redirect("~/BregAdmin/DispenseReceiptCustomForms.aspx?pid=" + Session["PracticeID"].ToString());
                    break;
            }
		}

        // Check if AddNew Button was clicked.
        if ( e.CommandName.Equals( "AddNew" ) )
        {
            // Get the Controls within the footer.
            TextBox txtNewPracticeName = ( TextBox ) GridPractices.FooterRow.FindControl( "txtNewPracticeName" );
            CheckBox chkNewIsOrthoSelect = (CheckBox) GridPractices.FooterRow.FindControl("chkNewIsOrthoSelect");
            CheckBox chkNewCameraScanEnabled = (CheckBox)GridPractices.FooterRow.FindControl("chkNewCameraScanEnabled");
			CheckBox chkNewIsBregBilling = (CheckBox)GridPractices.FooterRow.FindControl("chkNewIsBregBilling");
			CheckBox chkNewEMRPullEnabled = (CheckBox)GridPractices.FooterRow.FindControl("chkNewEMRPullEnabled");
            CheckBox chkNewFaxDispenseReceiptEnabled = (CheckBox)GridPractices.FooterRow.FindControl("chkNewFaxDispenseReceiptEnabled");
			CheckBox chkNewSecMsgIntEnabled = (CheckBox)GridPractices.FooterRow.FindControl("chkNewSecMsgIntEnabled");
            CheckBox chkNewSecMsgExtEnabled = (CheckBox)GridPractices.FooterRow.FindControl("chkNewSecMsgExtEnabled");
            ImageButton imageButtonAddNew = (ImageButton) GridPractices.FooterRow.FindControl( "ImageButtonAddNew" );
            ImageButton imageButtonAddNewCancel = (ImageButton) GridPractices.FooterRow.FindControl( "ImageButtonAddNewCancel" );
            
            //  Check if the AddNew button was clicked to Add a New record as opposed to Inserting a New Record
            if ( txtNewPracticeName.Visible == false )
            {
                // User clicked to Add a New Record.  Show New Fields for input and show the Insert and Cancel buttons.
                txtNewPracticeName.Visible = true;
                chkNewIsOrthoSelect.Visible = true;
				chkNewCameraScanEnabled.Visible = true;
				chkNewIsBregBilling.Visible = true;
				chkNewFaxDispenseReceiptEnabled.Visible = true;
                chkNewEMRPullEnabled.Visible = true;
                chkNewSecMsgIntEnabled.Visible = true;
                chkNewSecMsgExtEnabled.Visible = true;
                imageButtonAddNew.ImageUrl = @"~\RadControls\Grid\Skins\Outlook\Insert.gif";
                imageButtonAddNew.ToolTip = "Insert New Practice";
                imageButtonAddNewCancel.Visible = true;
            }
            else
            {
                // User clicked to Insert a New Record.  
                // So Insert the New Record.
                // Hide New Fields for input and the Insert and Cancel buttons.
                // Show the Add New Button
                String newPracticeName = txtNewPracticeName.Text.ToString();
                bool newIsOrthoSelect = chkNewIsOrthoSelect.Checked;
				bool newCameraScanEnabled = chkNewCameraScanEnabled.Checked;
				bool newIsBregBilling = chkNewIsBregBilling.Checked;
				bool newFaxDispenseReceiptEnabled = chkNewFaxDispenseReceiptEnabled.Checked;
                bool newEMRPullEnabled = chkNewEMRPullEnabled.Checked;
                bool newSecMsgIntEnabled = chkNewSecMsgIntEnabled.Checked;
                bool newSecMsgExtEnabled = chkNewSecMsgExtEnabled.Checked;

                BLL.Practice.Practice bllP = new BLL.Practice.Practice();
                bllP.PracticeName = newPracticeName;
                bllP.UserID = userID;
                bllP.IsOrthoSelect = newIsOrthoSelect;
				bllP.CameraScanEnabled = newCameraScanEnabled;
				bllP.IsBregBilling = newIsBregBilling;
				bllP.FaxDispenseReceiptEnabled = newFaxDispenseReceiptEnabled;
                bllP.EMRIntegrationPullEnabled = newEMRPullEnabled;
                bllP.SecureMessagingExternalEnabled = newSecMsgExtEnabled;
                bllP.SecureMessagingInternalEnabled = newSecMsgIntEnabled;
                bllP.Insert( bllP );
                // hide txtNewPracticeName and AddNewCancel button.  Switch Tooltip to Add New Record.
                txtNewPracticeName.Visible = false;
                imageButtonAddNew.ImageUrl = @"~\App_Themes\Breg\images\Common\add-record.png";
                imageButtonAddNew.ToolTip = "Add New Practice";
                imageButtonAddNewCancel.Visible = false;

                BindGridPractices();
            }
        }

        //  User Canceled the insertion of a new record.
        //  Revert mode back to AddNew.
        if ( e.CommandName.Equals( "AddNewCancel" ) )
        {
            TextBox txtNewPracticeName = ( TextBox ) GridPractices.FooterRow.FindControl( "txtNewPracticeName" );
            CheckBox chkNewIsOrthoSelect = (CheckBox)GridPractices.FooterRow.FindControl("chkNewIsOrthoSelect");
			CheckBox chkNewCameraScanEnabled = (CheckBox)GridPractices.FooterRow.FindControl("chkNewCameraScanEnabled");
			CheckBox chkNewIsBregBilling = (CheckBox)GridPractices.FooterRow.FindControl("chkNewIsBregBilling");
			CheckBox chkNewFaxDispenseReceiptEnabled = (CheckBox)GridPractices.FooterRow.FindControl("chkNewFaxDispenseReceiptEnabled");
            CheckBox chkNewEMRPullEnabled = (CheckBox)GridPractices.FooterRow.FindControl("chkNewEMRPullEnabled");
            CheckBox chkNewSecMsgIntEnabled = (CheckBox)GridPractices.FooterRow.FindControl("chkNewSecMsgIntEnabled");
            CheckBox chkNewSecMsgExtEnabled = (CheckBox)GridPractices.FooterRow.FindControl("chkNewSecMsgExtEnabled");
            ImageButton imageButtonAddNew = ( ImageButton ) GridPractices.FooterRow.FindControl( "ImageButtonAddNew" );
            ImageButton imageButtonAddNewCancel = ( ImageButton ) GridPractices.FooterRow.FindControl( "ImageButtonAddNewCancel" );
            // hide txtNewPracticeName and AddNewCancel button.  Switch Tooltip to Add New Record.
            txtNewPracticeName.Visible = false;
            chkNewIsOrthoSelect.Visible = false;
			chkNewCameraScanEnabled.Visible = false;
			chkNewIsBregBilling.Visible = false;
			chkNewFaxDispenseReceiptEnabled.Visible = false;
            chkNewEMRPullEnabled.Visible = false;
            chkNewSecMsgIntEnabled.Visible = true;
            chkNewSecMsgExtEnabled.Visible = true;
            imageButtonAddNew.ImageUrl = @"~\App_Themes\Breg\images\Common\add-record.png";
            imageButtonAddNew.ToolTip = "Add New Practice";
            imageButtonAddNewCancel.Visible = false;
        }

        if (e.CommandName.Equals("ReSubscribe"))
        {
            index = Convert.ToInt32(e.CommandArgument);

            int practiceApplicationId;
            int.TryParse(GridPractices.DataKeys[index].Values[3].ToString(), out practiceApplicationId);
            int practiceId = Convert.ToInt32(GridPractices.DataKeys[index].Value);

            if (practiceApplicationId == 0)
            {
                //make sure that they are a vision user and not an express user.
                if (!BLL.Practice.Practice.IsVisionLitePractice(practiceId))
                {
                    //Internal Admin should email this link to the user to complete the process
                    MailtoLink("/purchase/NewPracticeApplication.aspx?PID=" + practiceId.ToString());
                }
            }
            else
            {
                if (BLL.Practice.Practice.IsVisionLitePractice(practiceId))
                {
                    //user is reactivating VisionExpress
                    //set the email to resend and send them to 
                    ClassLibrary.DAL.PracticeApplication pa = BLL.Practice.PracticeApplication.GetPracticeApplication(practiceApplicationId);
                    pa.IsWelcomeEmailSent = false;
                    BLL.Practice.PracticeApplication.UpdatePracticeApplication(pa);

                    //Internal Admin should email this link to the user to complete the process
                    MailtoLink("/Purchase/PurchaseOptions.aspx?PAID=" + practiceApplicationId.ToString());
                }
            }
        }
    }

    private void MailtoLink(string href)
    {
        string baseURL = "http://bregvisionexpress.com";

        StringBuilder sb = new StringBuilder();
        sb.Append("<script language=JavaScript> ");
        sb.Append(string.Format("document.location.href = \"mailto:{0}?subject={1}", "", "Use the following link to complete the transaction"));
        sb.Append(string.Format("&body={0}\"; ", baseURL + href));
        sb.Append(" </script>");
        Page.ClientScript.RegisterStartupScript(typeof(Page),"email", sb.ToString());

    }
	protected void btnReset_Click( object sender, EventArgs e )
	{

	}
	protected void btnSave_Click( object sender, EventArgs e )
	{

	}

    protected void chkApplyFilters_Changed(object sender, EventArgs e)
    {
        GridPractices.EditIndex = -1;
        BindGridPractices();
    }

}
