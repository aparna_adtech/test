﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConsignmentRep.aspx.cs" Inherits="BregVision.BregAdmin.ConsignmentRep" %>

<%@ Register src="../Admin/UserControls/Practice/PracticeConsignmentRep.ascx" tagname="PracticeConsignmentRep" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link id="Link1" runat="server" rel="Stylesheet" type="text/css" href="~/Admin/Styles/AdminMasterStyle.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center">
        <table cellpadding="0" cellspacing="0" width="750px">
            <tr>
                <td>                     
                    <uc1:PracticeConsignmentRep ID="PracticeConsignmentRep1" runat="server" />
                     
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
