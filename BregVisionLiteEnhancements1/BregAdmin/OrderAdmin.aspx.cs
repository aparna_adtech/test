﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Text;
using ClassLibrary.DAL;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.SqlClient;
using System.Data.Common;

namespace BregVision.BregAdmin
{
    public partial class OrderAdmin : System.Web.UI.Page
    {
        protected IEnumerable<BregVisionOrder> _BregVisionOrders;
        VisionDataContext visionDB = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            //LoadOrdersMasterList(txtSearchPO.Text.Trim(), txtSearchCustomPO.Text.Trim());
        }



        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //LoadOrdersMasterList(txtSearchPO.Text.Trim(), txtSearchCustomPO.Text.Trim());

        }

        protected void gvPO_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvPO.EditIndex = e.NewEditIndex;
        }

        protected void gvPO_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvPO.EditIndex = -1;
        }

        protected void gvPO_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                GridViewRow row = gvPO.Rows[e.RowIndex];
                if ((row.RowState == (DataControlRowState.Edit | DataControlRowState.Alternate)) || (row.RowState == DataControlRowState.Edit))
                {

                    BregVisionOrder bregVisionOrder = null;
                    bregVisionOrder = visionDB.BregVisionOrders.SingleOrDefault<BregVisionOrder>(p => p.BregVisionOrderID.Equals(row.Cells[0].GetSafeInt("txtBregVisionOrderID")));


                    bregVisionOrder.IsActive = row.Cells[4].GetCheckBoxValue("chkIsActive");

                    visionDB.SubmitChanges();
                    //VisionDataContext.CloseConnection(visionDB);
                    gvPO.EditIndex = -1;
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            BindLevelControls();
        }

        //protected void LoadOrdersMasterList(string SearchPurchaseOrder, string SearchCustomPurchaseOrder)
        //{

        //    string SearchCustomPurchaseOrderWildCard = "%" + SearchCustomPurchaseOrder + "%";
        //    Int32 poNum = 0;

        //    visionDB = VisionDataContext.GetVisionDataContext();

        //    using (visionDB)
        //    {
        //        _BregVisionOrders = (from orders in visionDB.BregVisionOrders
        //                             where
        //                               ((string.IsNullOrEmpty(SearchPurchaseOrder) == false) && Int32.TryParse(SearchPurchaseOrder, out poNum) == true && (orders.PurchaseOrder == poNum))
        //                               ||
        //                                ((string.IsNullOrEmpty(SearchCustomPurchaseOrder) == false) && (SqlMethods.Like(orders.CustomPurchaseOrderCode, SearchCustomPurchaseOrderWildCard)))
        //                             orderby orders.PurchaseOrder
        //                             select orders).ToList();

        //        //VisionDataContext.CloseConnection(visionDB);
        //    }
        //}

        protected void BindLevelControls()
        {

                //LoadOrdersMasterList(txtSearchPO.Text.Trim(), txtSearchCustomPO.Text.Trim());

                string SearchCustomPurchaseOrderWildCard = "%" + txtSearchCustomPO.Text.Trim() + "%";
                Int32 poNum = 0;

                visionDB = VisionDataContext.GetVisionDataContext();

                using (visionDB)
                {
                    var bregVisionOrders = (from orders in visionDB.BregVisionOrders
                                         where
                                           ((string.IsNullOrEmpty(txtSearchPO.Text.Trim()) == false) && Int32.TryParse(txtSearchPO.Text.Trim(), out poNum) == true && (orders.PurchaseOrder == poNum))
                                           ||
                                            ((string.IsNullOrEmpty(txtSearchCustomPO.Text.Trim()) == false) && (SqlMethods.Like(orders.CustomPurchaseOrderCode, SearchCustomPurchaseOrderWildCard)))
                                         orderby orders.PurchaseOrder
                                            select new
                                            {
                                                orders.BregVisionOrderID,
                                                PracticeLocationName = orders.PracticeLocation.Name,
                                                orders.PurchaseOrder,
                                                CustomPurchaseOrder = orders.CustomPurchaseOrderCode,
                                                orders.IsActive
                                            }).ToList();

                    gvPO.DataSource = bregVisionOrders;
                    gvPO.DataBind();

                    //VisionDataContext.CloseConnection(visionDB);
                }

        }
    }
}
