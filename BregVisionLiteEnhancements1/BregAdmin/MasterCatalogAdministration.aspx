﻿<%@ Page Language="C#" MasterPageFile="MasterBregAdmin.Master" AutoEventWireup="true" CodeBehind="MasterCatalogAdministration.aspx.cs" Inherits="BregVision.BregAdmin.MasterCatalogAdministration" %>
<%@ Register Src="~/BregAdmin/UserControls/MasterCatalogAdmin.ascx" TagName="MasterCatalogAdmin" TagPrefix="bv"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<telerik:RadScriptManager ID="RadScriptManager1" runat="server" AsyncPostBackTimeout="1800" >
		<Scripts>
			<asp:ScriptReference Path="~/Include/HelpSystem.js" />
			<asp:ScriptReference Path="~/Include/Silverlight.js" />
			<asp:ScriptReference Path="~/FusionCharts/FusionCharts.js" />
			<asp:ScriptReference Path="~/Include/jquery/js/jquery-1.4.4.min.js" />
		</Scripts>
		<Services>
		</Services>
	</telerik:RadScriptManager>
    				<script type="text/javascript">

    				    function stopRKey(evt) {
    				        var evt = (evt) ? evt : ((event) ? event : null);
    				        var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
    				        if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
    				    }

    				    document.onkeypress = stopRKey;

    				    function onSilverlightError(sender, args) {
    				    }

    				    function GetHelpPane() {
    				    }
    				    function ExpandHelpPane() {
    				    }


    				    function showToolTip(element, context) {
    				    };
					
					</script>
					
	<telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server" >
	</telerik:RadStyleSheetManager>

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    </telerik:RadAjaxManager>
    <div style="text-align: center">
        <table cellpadding="0" cellspacing="0" width="100%" border="0">
            <tr align="center">
                <td style="width: 45%">&nbsp;</td>
                <td>
                    <a href="BregAdmin.aspx" class="managemenulink">Back</a><br />
                </td>
                <td style="width: 45%">&nbsp;</td>
            </tr>
        </table>
        <table id="OuterTable" width="auto" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <br />
                        <bv:MasterCatalogAdmin ID="ctlMasterCatalogAdmin" IsPracticeSpecific="false" runat="server" />
                    <br />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
