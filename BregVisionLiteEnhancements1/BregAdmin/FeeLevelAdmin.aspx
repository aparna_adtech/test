﻿<%@ Page Language="C#" MasterPageFile="MasterBregAdmin.Master" AutoEventWireup="true" CodeBehind="FeeLevelAdmin.aspx.cs" Inherits="BregVision.BregAdmin.FeeLevelAdmin" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .hiddencol
        {
            display:none;
        }
    </style>

    <div style="text-align: center">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr align="center">
                <td>
                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                        <tr align="center">
                            <td style="width: 425px">&nbsp;</td>
                            <td style="width: 95px">
                                <a href="BregAdmin.aspx" class="managemenulink">Back</a><br />
                            </td>
                            <td style="width:425px">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                                <table cellpadding="0" cellspacing="0" width="925px">
                                    <tr>
                                        <td  class="AdminPageTitle">
                                        Fee Level
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table id="tblFeeLevel" runat="server" cellpadding="2" cellspacing="4" border="0" width="455px">
                        <tr>
                            <td colspan="1" align="right">
                                <asp:DropDownList runat="server" ID="ddlFeeLevel" AutoPostBack="true" OnSelectedIndexChanged="ddlFeeLevel_OnSelectedIndexChanged"></asp:DropDownList>
                            </td>
                            <td colspan="3" align="left">
                                <asp:Button ID="btnAddFeeLevel" runat="server" Text="Add New" 
                                    onclick="btnAddFeeLevel_Click" />
                            </td>
                        </tr>
                        <tr valign="top">
                            <td style="width:185px">
                                <asp:Label ID="lblFeeLevelName" runat="server" Text="Fee Level Name" Visible="false" CssClass="TextMed"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtFeeLevelName" runat="server" Visible="false"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvFeeLevelName" runat="server" Display="Dynamic" ControlToValidate="txtFeeLevelName" ErrorMessage="*"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revFeeLevelName" runat="server" Display="Dynamic" ControlToValidate="txtFeeLevelName" ValidationExpression="[0-9a-zA-Z' ']{1,20}" ErrorMessage="20 alpha-numeric characters"></asp:RegularExpressionValidator>
                            </td>
                            <td>
                                <asp:Button ID="btnUpdateFeeLevel" runat="server" Text="Update" Visible="false" 
                                    onclick="btnUpdateFeeLevel_Click" />
                            </td>
                            <td>
                                    <asp:Button ID="btnCancelFeeLevel" runat="server" Text="Cancel" Visible="false" CausesValidation="false" 
                                        onclick="btnCancelFeeLevel_Click" />
                            </td> 
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" width="100%" border="0">
            <tr align="center">
                <td>
                    <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="Add Fees" /><br /><br />
                    <asp:Label ID="lblMessage" runat="server" Font-Size="Small" style="color:Red" Visible="false"></asp:Label>
                    <asp:GridView runat="server" ID="gvFees" GridLines ="Both"
                        AutoGenerateColumns="false" CssClass="FieldLabel"
                        OnRowEditing="gvFees_RowEditing"
                        OnRowUpdating="gvFees_RowUpdating"
                        OnRowCancelingEdit="gvFees_RowCancelingEdit" Width="925px">
                        <Columns>
                            <asp:CommandField ShowCancelButton="true" ShowEditButton="true" HeaderStyle-CssClass="AdminSubTitle" HeaderStyle-HorizontalAlign ="Center" HeaderStyle-Width="110px" ItemStyle-Width="110px"/>
                            <asp:TemplateField ControlStyle-CssClass="hiddencol" HeaderStyle-CssClass="AdminSubTitle" ItemStyle-Height ="25px">
                                <ItemTemplate>
                                    <asp:Label ID="lblBregVisionMonthlyFeeLevelID" runat="server" Text='<%# Bind("BregVisionMonthlyFeeLevelID") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtBregVisionMonthlyFeeLevelID" runat="server" Text='<%# Bind("BregVisionMonthlyFeeLevelID") %>' ></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField ControlStyle-CssClass="hiddencol" HeaderStyle-CssClass="AdminSubTitle" ItemStyle-Height ="25px">
                                <ItemTemplate>
                                    <asp:Label ID="lblBregVisionMonthlyFeeID" runat="server" Text='<%# Bind("BregVisionMonthlyFeeID") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtBregVisionMonthlyFeeID" runat="server" Text='<%# Bind("BregVisionMonthlyFeeID") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Min Transactions" HeaderStyle-CssClass="AdminSubTitle" ControlStyle-Width="155px" ItemStyle-Height ="25px">
                                <ItemTemplate>
                                    <asp:Label ID="lblMinTransactions" runat="server" Text='<%# Bind("MinTransactions") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtMinTransactions" runat="server" Text='<%# Bind("MinTransactions") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvMinTransactions" runat="server" ControlToValidate="txtMinTransactions" Display="Dynamic" ErrorMessage="*"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revMinTransactions" runat="server" ControlToValidate="txtMinTransactions" ValidationExpression="^\d{1,9}$" Display="Dynamic" ErrorMessage="9 Digits Numeric"></asp:RegularExpressionValidator>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Max Transactions" HeaderStyle-CssClass="AdminSubTitle" ControlStyle-Width="155px" ItemStyle-Height ="25px">
                                <ItemTemplate>
                                    <asp:Label ID="lblMaxTransactions" runat="server" Text='<%# Bind("MaxTransactions") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtMaxTransactions" runat="server" Text='<%# Bind("MaxTransactions") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvMaxTransactions" runat="server" ControlToValidate="txtMaxTransactions" Display="Dynamic" ErrorMessage="*"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revMaxTransactions" runat="server" ControlToValidate="txtMaxTransactions" ValidationExpression="^\d{1,9}$" Display="Dynamic" ErrorMessage="9 Digits Numeric"></asp:RegularExpressionValidator>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Monthly Fee $" HeaderStyle-CssClass="AdminSubTitle" ControlStyle-Width="155px" ItemStyle-Height ="25px">
                                <ItemTemplate>
                                    <asp:Label ID="lblMonthlyFee" runat="server" Text='<%# Bind("MonthlyFee", "{0:N2}") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtMonthlyFee" runat="server" Text='<%# Bind("MonthlyFee", "{0:N2}") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvMonthlyFee" runat="server" ControlToValidate="txtMonthlyFee" Display="Dynamic" ErrorMessage="*"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revMonthlyFee" runat="server" ControlToValidate="txtMonthlyFee" ValidationExpression="(?!^0*$)(?!^0*\.0*$)^\d{1,18}(\.\d{1,2})?$" Display="Dynamic" ErrorMessage="$"></asp:RegularExpressionValidator>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Service Code" HeaderStyle-CssClass="AdminSubTitle" ControlStyle-Width="175px" ItemStyle-Height ="25px">
                                <ItemTemplate>
                                    <asp:Label ID="lblServiceCode" runat="server" Text='<%# Bind("ServiceCode") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtServiceCode" runat="server" Text='<%# Bind("ServiceCode") %>' Width="150px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvServiceCode" runat="server" ControlToValidate="txtServiceCode" Display="Dynamic" ErrorMessage="*"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revServiceCode" runat="server" ControlToValidate="txtServiceCode" ValidationExpression="^\d{1,6}$" Display="Dynamic" ErrorMessage="6 Digits Numeric"></asp:RegularExpressionValidator>
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    </div>
</asp:Content>
