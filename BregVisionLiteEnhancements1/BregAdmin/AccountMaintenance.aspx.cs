﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BLL;
using System.Collections.Generic;
using ClassLibrary.DAL;
using System.Linq;
using BregVision;

public partial class AccountMaintenance : System.Web.UI.Page
{
    List<ClassLibrary.DAL.BregVisionMonthlyFeeLevel> _FeeLevels = null;
  
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindGridPracticeApplications();
            RadWindowManager1.EnableStandardPopups = false;
        }
    }
    
    protected void BindGridPracticeApplications()
    {
        List<ClassLibrary.DAL.PracticeApplication> practiceApplications = BLL.Practice.PracticeApplication.GetPracticeApplications();
        GridPracticeApplications.DataSource = practiceApplications;
        GridPracticeApplications.DataBind();

        
    }


    
    protected void GridPractices_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int practiceApplicationID = Convert.ToInt32(GridPracticeApplications.DataKeys[e.RowIndex].Values[0].ToString());
        TextBox txtNotes = (TextBox)GridPracticeApplications.Rows[e.RowIndex].FindControl("txtNotes");
        CheckBox chkActivatePractice = (CheckBox)GridPracticeApplications.Rows[e.RowIndex].FindControl("chkActivatePractice");

        CheckBox chkPopulateInventory = (CheckBox)GridPracticeApplications.Rows[e.RowIndex].FindControl("chkPopulateInventory");
        bool populateInventory = chkPopulateInventory.Checked;

        ClassLibrary.DAL.PracticeApplication practiceApplication = DAL.Practice.PracticeApplication.GetPracticeApplication(practiceApplicationID);
        practiceApplication.Notes = txtNotes.Text;
        DAL.Practice.PracticeApplication.UpdatePracticeApplication(practiceApplication);

        if (practiceApplication.IsPracticeCreated == false && chkActivatePractice.Checked)
        {
            practiceApplication.IsActive = true;

            if (BLL.Practice.Practice.CreateMinimumPracticeInfo(ref practiceApplication, BLL.Practice.PaymentType.PaymentTypeEnum.BregInvoice, populateInventory))
            {
                SetPracticeFee(e, practiceApplication.PracticeID);
                FaxEmailPOs.SendWelcomeEmail(ref practiceApplication,this);
            }
            else
            {
                throw new ApplicationException(string.Format("Failure to create Practice Information for account {0}", practiceApplication.PracticeApplicationID));
            }
        }

        GridPracticeApplications.EditIndex = -1;
        BindGridPracticeApplications();

    }

    private void SetPracticeFee(GridViewUpdateEventArgs e, Int32? practiceID)
    {
        DropDownList ddlFeeLevel = GridPracticeApplications.Rows[e.RowIndex].FindControl("ddlPracticeFees") as DropDownList;
        if (ddlFeeLevel != null)
        {
            Int32 pid = practiceID ?? 0;
            Int32 bregVisionMonthlyFeeLevelID = Convert.ToInt32(ddlFeeLevel.SelectedValue);
            DateTime startDate = DateTime.Now;
            Decimal discount = Decimal.Zero;

            BLL.Practice.Practice.SetPracticeFee(pid, bregVisionMonthlyFeeLevelID, startDate, discount);
        }
    }

    protected void GridPractices_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            PracticeApplication pa = (PracticeApplication)e.Row.DataItem;

            if (pa.IsPracticeCreated)
            {
                
                CheckBox chkActivatePractice = (CheckBox)e.Row.FindControl("chkActivatePractice");

            }

            ddlPracticeFeesBind(sender, e);
            
        }
    }

    protected void ddlPracticeFeesBind(object sender, GridViewRowEventArgs e)
    {
        PracticeApplication pa = (PracticeApplication)e.Row.DataItem;
        if ( (e.Row.RowState & DataControlRowState.Edit)!= 0 )
        {
            DropDownList ddlPracticeFees = e.Row.FindControl("ddlPracticeFees") as DropDownList;
            if (ddlPracticeFees != null)
            {
                if (!pa.IsPracticeCreated)
                {
                    if (_FeeLevels == null)
                    {
                        _FeeLevels = BLL.Practice.Practice.GetExpressPracticeFees();
                    }
                    ddlPracticeFees.DataSource = _FeeLevels;
                    ddlPracticeFees.DataTextField = "LevelName";
                    ddlPracticeFees.DataValueField = "BregVisionMonthlyFeeLevelID";
                    ddlPracticeFees.DataBind();
                }
                else
                {
                    ddlPracticeFees.Visible = false;
                }
            }
        }
    }

    protected void GridPractices_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridPracticeApplications.EditIndex = e.NewEditIndex;
        BindGridPracticeApplications();
    }

    protected void GridPractices_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridPracticeApplications.EditIndex = -1;
        BindGridPracticeApplications();
    }
   
}
