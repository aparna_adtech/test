﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Text;
using ClassLibrary.DAL;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.SqlClient;
using System.Data.Common;

namespace BregVision.BregAdmin
{
    public partial class ICD9Admin : System.Web.UI.Page
    {
        protected IEnumerable<ICD9> _ICD9s;
        VisionDataContext visionDB = null;


        protected void Page_Load(object sender, EventArgs e)
        {
            LoadICD9MasterList("", "");
        }

        protected void LoadICD9MasterList(string SearchICD9, string SearchHCPCs)
        {
            SearchICD9 = "%" + SearchICD9 + "%";
            SearchHCPCs = "%" + SearchHCPCs + "%";


            visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                _ICD9s = (from icd9 in visionDB.ICD9s
                         where SqlMethods.Like(icd9.ICD9Code, SearchICD9)
                         && SqlMethods.Like(icd9.HCPCs, SearchHCPCs)
                         && icd9.PracticeID == null
                         orderby icd9.HCPCs, icd9.ICD9Code
                         select icd9).ToList();
                //VisionDataContext.CloseConnection(visionDB);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadICD9MasterList(txtSearchICD9.Text, txtSearchHCPCs.Text);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                ICD9 iCD9 = new ICD9();

                iCD9.HCPCs = txtHCPCs.Text;
                iCD9.ICD9Code = txtICD9Code.Text;
                iCD9.ICD9Description = txtICD9Description.Text;
                iCD9.IsActive = true;
                iCD9.CreatedUserID = 1;
                iCD9.CreatedDate = DateTime.Now;
                iCD9.ModifiedUserID = 1;
                iCD9.ModifiedDate = DateTime.Now;

                visionDB.ICD9s.InsertOnSubmit(iCD9);
                visionDB.SubmitChanges();
                //VisionDataContext.CloseConnection(visionDB);

                LoadICD9MasterList(txtICD9Code.Text, txtHCPCs.Text);

                txtHCPCs.Text = "";
                txtICD9Code.Text = "";
                txtICD9Description.Text = "";
            }
        }

        protected void gvICD9s_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvICD9s.EditIndex = e.NewEditIndex;
        }

        protected void gvICD9s_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvICD9s.EditIndex = -1;
        }

        protected void gvICD9s_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                GridViewRow row = gvICD9s.Rows[e.RowIndex];
                if ((row.RowState == (DataControlRowState.Edit | DataControlRowState.Alternate)) || (row.RowState == DataControlRowState.Edit))
                {

                    ICD9 iCD9 = null;
                    iCD9 = _ICD9s.SingleOrDefault<ICD9>(p => p.ICD9ID.Equals(row.Cells[1].GetSafeInt("txtICD9ID")));

                    if (iCD9 == null)
                    {
                        iCD9 = new ICD9();
                    }

                    iCD9.HCPCs = row.Cells[1].GetTextBoxValue("txtHCPCs");
                    iCD9.ICD9Code = row.Cells[2].GetTextBoxValue("txtICD9Code");
                    iCD9.ICD9Description = row.Cells[3].GetTextBoxValue("txtICD9Description");
                    iCD9.IsActive = row.Cells[4].GetCheckBoxValue("chkIsActive");
                    iCD9.CreatedUserID = 1;
                    iCD9.CreatedDate = DateTime.Now;
                    iCD9.ModifiedUserID = 1;
                    iCD9.ModifiedDate = DateTime.Now;

                    visionDB.SubmitChanges();

                    //VisionDataContext.CloseConnection(visionDB);

                    gvICD9s.EditIndex = -1;
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            BindLevelControls();
        }

        protected void BindLevelControls()
        {

            gvICD9s.DataSource = _ICD9s.Take(500);
            gvICD9s.DataBind();
            
        }

    }
}
