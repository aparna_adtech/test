using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary.DAL;
using ClassLibrary.DAL.Helpers;
using DAL.Practice;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.Web.UI;
using BregVision.BregAdmin;
using BregWcfService;
using Dispensement = DAL.PracticeLocation.Dispensement;
using BregVision.Handlers;
using BregWcfService.Classes;
using BregVision.TelerikAjax;
using DocumentFormat.OpenXml.Office2010.Drawing.Charts;
using Practice = ClassLibrary.DAL.Practice;

namespace BregVision
{
  public partial class Dispense : Bases.PageBase
  {
    public int PracticeLocationID { get; set; }
    public string DispenseQueueID { get; set; }

    private string SearchCriteria = "";
    private string SearchText = "";
    protected IEnumerable<ICD9> _ICD9_Codes = null;
    private DataSet _dsSearch = null;
    protected DataSet _dsPhysicians = null;
    protected DataSet _dsFitters = null;
    protected Payer[] _dsPayers = null;
    private UIHelper2 _UIHelper; //mws
    private UIHelper2 _grdCustomBrace_UIHelper;
    private UIHelper2 _grdReceiptHistory_UIHelper;
    private Dictionary<string, int> summaryFields = new Dictionary<string, int>();

    #region properties to expose page controls

    public Telerik.Web.UI.RadAjaxLoadingPanel DefaultLoadingPanel
    {
      get { return LoadingPanel1; }
    }

    #endregion

    #region page-level events

    protected void Page_Init(object sender, EventArgs e)
    {
      #region map field names to column index

      summaryFields.Add("ClientSelectColumn", 0);
      summaryFields.Add("chkIsMedicare", 2);
      //summaryFields.Add("chkABNForm", 3);
      summaryFields.Add("DispenseQueueID", 5);
      summaryFields.Add("txtDateDispensed", 7);
      summaryFields.Add("cboPhysicians", 9);
      summaryFields.Add("txtPatientCode", 10);
      summaryFields.Add("Code", 13);
      summaryFields.Add("cboSide", 14);

      summaryFields.Add("HCPCs", 16);
      summaryFields.Add("WholesaleCost", 17);
      summaryFields.Add("QuantityOnHandPerSystem", 18);
      summaryFields.Add("txtQuantityDispensed", 19);
      summaryFields.Add("chkDMEDeposit", 20);
      summaryFields.Add("txtDMEDeposit", 21);
      summaryFields.Add("BillingCharge", 23);
      summaryFields.Add("BillingChargeCash", 24);
      summaryFields.Add("cboICD9", 25);
      summaryFields.Add("ProductInventoryID", 26);
      summaryFields.Add("PracticeCatalogProductID", 27);

      GridViewExtensions.fieldNames = summaryFields;

      #endregion

      #region master page location changed visibility & event handler

      ((Telerik.Web.UI.RadComboBox) Master.FindControl("cbxChangeLocation")).Visible = true;
      Master.LocationChanged +=
        new CommandEventHandler(
          (_s, _e) => Response.Redirect("Dispense.aspx", false)
          );

      #endregion

      //Master.MainPane.Scrolling = Telerik.Web.UI.SplitterPaneScrolling.None;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      #region instantiate UIHelper objects

      _UIHelper = new UIHelper2(ViewState, grdInventory, true); //mws
      _grdCustomBrace_UIHelper = new UIHelper2(ViewState, grdCustomBrace, true); //mws
      _grdReceiptHistory_UIHelper = new UIHelper2(ViewState, grdReceiptHistory, true); //mws

      #endregion

      #region reset status labels

      _UIHelper.SetGridLabel("", Page.IsPostBack, lblBrowseDispense, true); //set the default text on the label
      _grdCustomBrace_UIHelper.SetGridLabel("", Page.IsPostBack, lblBrowseDispenseCustomBrace, true);
        //set the default text on the label

      lblSummary.Text = "";
      lblSummary2.Text = "";
      lblErrorMsg.Text = ""; //added by u= 02/04/2010
      lblErrorMsg2.Text = ""; //added by u= 02/04/2010

      #endregion

      #region get list of physicians datasource from the database

      _dsPhysicians = GetPhysicians(practiceLocationID);
      var isOneProviderLocation = _dsPhysicians.Tables[0].Rows.Count == 1;
      if (!isOneProviderLocation)
      {
        DataRow emptyPhysicianRow = _dsPhysicians.Tables[0].NewRow();
        emptyPhysicianRow["PhysicianName"] = string.Empty;
        emptyPhysicianRow["PhysicianID"] = -1;
        _dsPhysicians.Tables[0].Rows.InsertAt(emptyPhysicianRow, 0);
      }

      _dsFitters = GetFitters(practiceLocationID);
      DataRow emptyFitterRow = _dsFitters.Tables[0].NewRow();
      emptyFitterRow["ClinicianName"] = string.Empty;
      emptyFitterRow["ClinicianID"] = -1;
      _dsFitters.Tables[0].Rows.InsertAt(emptyFitterRow, 0);

      #endregion

      #region get list of physicians datasource from the database

      _dsPayers = GetPayers(practiceID);

      #endregion

      RadAjaxManager1.ResponseScripts.Add("scrollTop();");

      if (!IsPostBack)
      {
        RadAjaxManager1.ResponseScripts.Add("showDispenseQueue();");

        // make sure search criteria is reset
        ResetInventorySearchCriteria();
        ViewState["SearchCriteria"] = "";

        // make sure first tab is selected in initial load of page
        rmpMain.SelectedIndex = 0;
        pvDispense.SelectedIndex = 0;

        // set initial value for record count
        //txtRecordsDisplayed.Text = recordsDisplayedPerPage.ToString();

        _UIHelper.HasExpanded = true; //mws
        _grdCustomBrace_UIHelper.HasExpanded = true; //mws
        _grdReceiptHistory_UIHelper.HasExpanded = true;

        // hide resend fax button if feature not enabled
        btnResendPrintDispensement.Visible = IsPracticeFaxDispensementEnabled;
      }

      #region reset pagination page size on RadGrids

      // what happened to the other grids on the page???
      //grdInventory.PageSize = Convert.ToInt16(txtRecordsDisplayed.Text);
      grdInventory.PageSize = 10;
      //grdSummary2.PageSize = Convert.ToInt16(txtRecordsDisplayed.Text);
      grdSummary2.PageSize = 10;

      #endregion

      #region set up default enter key search behavior

      // what about the other search boxes on the page???
      var tbFilter = (TextBox) rttbFilter.FindControl("tbFilter");
      UIHelper2.SetEnterPressTarget(tbFilter, "rtbFilter");

      var tbFilterReceiptHistory = (TextBox) rttbFilterReceiptHistory.FindControl("tbFilterReceiptHistory");
      UIHelper2.SetEnterPressTarget(tbFilterReceiptHistory, "rttbSearchReceiptHistory");

      #endregion

      #region add RadAjaxManager settings

      RadAjaxManager1.AjaxSettings.AddAjaxSetting(RadToolbar1, grdSummary2, DefaultLoadingPanel); //tbFilter
      RadAjaxManager1.AjaxSettings.AddAjaxSetting(rttbFilter, grdSummary2, DefaultLoadingPanel); //tbFilter
      RadAjaxManager1.AjaxSettings.AddAjaxSetting(rtbFilter, grdSummary2, DefaultLoadingPanel); //tbFilter
      RadAjaxManager1.AjaxSettings.AddAjaxSetting(tbFilter, grdSummary2, DefaultLoadingPanel);

      #endregion

       grdSummary2.MasterTableView.HierarchyLoadMode = GridChildLoadMode.ServerBind;

      #region focus on search during initial load

            if (!Page.IsPostBack)
        tbFilter.Focus();

      #endregion
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
      GetABNCostType();
    }

    #endregion

    private void GetABNCostType()
    {
      if (Roles.IsUserInRole(Context.User.Identity.Name, "BregAdmin")
          || Roles.IsUserInRole(Context.User.Identity.Name, "ClinicAdmin")
          || Roles.IsUserInRole(Context.User.Identity.Name, "PracticeAdmin"))
      {
        using (var visionDB = VisionDataContext.GetVisionDataContext())
        {
          var abnCost = from p in visionDB.Practices
            where p.PracticeID == practiceID
            select p.ABNCostIsCash;
          chkABNCostIsCash.Checked = abnCost.FirstOrDefault();
          chkABNCostIsCash.Visible = true;
        }
      }
    }

    private bool AutoInsertItemIntoDispenseQueue(string searchCriteria, string searchText)
    {
        if (searchCriteria == "Code")
        {
            var ds = DAL.PracticeLocation.Inventory.GetAllProductDataByLocation(practiceID, practiceLocationID, searchCriteria,
                searchText);

            if (ds.Tables[0].Rows.Count == 1)
            {
                var inventoryRow = ds.Tables[0].Rows[0];

                var productInventoryID = Convert.ToInt32(inventoryRow["ProductInventoryID"]);
                var productName = inventoryRow["ProductName"].ToString();
                var code = inventoryRow["Code"].ToString();
                var isMedicare = false;
                var isABNForm = false;
                var side = inventoryRow["side"].ToString();
                var dmeDeposit = Convert.ToDecimal(inventoryRow["DMEDeposit"]);
                var mod1 = inventoryRow["Mod1"].ToString();
                var mod2 = inventoryRow["Mod2"].ToString();
                var mod3 = inventoryRow["Mod3"].ToString();

                if (productInventoryID > 0)
                {
                    DAL.PracticeLocation.Dispensement.AddUpdateDispensement(
                        DispenseQueueID: 0,
                        PracticeLocationID: practiceLocationID,
                        InventoryID: productInventoryID,
                        PatientCode: "",
                        PhysicianID: 0,
                        ICD9_Code: "",
                        IsMedicare: isMedicare,
                        ABNForm: false,
                        IsCashCollection: false,
                        Side: side,
                        DMEDeposit: dmeDeposit,
                        AbnType: "",
                        BregVisionOrderID: null,
                        Quantity: 1,
                        isCustomFit: null,
                        mod1: mod1,
                        mod2: mod2,
                        mod3: mod3,
                        DispenseDate: null,
                        CreatedUser: 1,
                        CreatedDate: DateTime.Now,
                        QueuedReason: null,
                        dispenseSignatureID: 0,
                        isDeleted: false,
                        IsCalledFromV1Endpoint: false
                        );
                    lblBrowseDispense.Text = string.Format("{0} - {1} added to the Dispensement.<br/>", code,
                        productName);
                    lblBrowseDispense.CssClass = "SuccessMsg";
                    lblBrowseDispense.Visible = true;
                    {
                        var t = (System.Web.UI.WebControls.TextBox) rttbFilter.FindControl("tbFilter");
                        t.Text = "";
                        t.Focus();
                    }
                    grdSummary2.Rebind();
                    grdSummary2.CurrentPageIndex = 0;
                    return true;
                }
            }
        }

        return false;
    }

    /// <summary>
    /// Resets Inventory PageView's search toolbar search criteria to first value or
    /// value set in user prefs.
    /// </summary>
    /// <returns>Value of search criteria ComboBox</returns>
    private string ResetInventorySearchCriteria()
    {
      var cboSearchFilter = (RadComboBox) rttbSearchFilter.FindControl("cboSearchFilter");

      #region set default search criteria based on user pref or select first item if not set

      var defaultDispenseSearchCriteria = Session["DefaultDispenseSearchCriteria"];
      if (defaultDispenseSearchCriteria != null)
      {
        cboSearchFilter.SelectedIndex =
          cboSearchFilter.FindItemIndexByValue(
            defaultDispenseSearchCriteria.ToString());
      }
      else
      {
        cboSearchFilter.SelectedIndex = 0;
      }

      #endregion

      return cboSearchFilter.SelectedValue;
    }

    private void ClearInventorySearch()
    {
      ((TextBox) rttbFilter.FindControl("tbFilter")).Text = "";
      ViewState["SearchText"] = "";

      ResetInventorySearchCriteria();
      ViewState["SearchCriteria"] = "";

      grdInventory.Rebind();
    }

    private void ClearCustomBraceSearch()
    {
      ((TextBox) rttbFilterCustomBrace.FindControl("tbFilterCustomBrace")).Text = "";
      ViewState["grdCustomBrace_SearchText"] = "";

      var SearchCriteria = (RadComboBox) rttbSearchFilterCustomBrace.FindControl("cboSearchFilterCustomBrace");
      SearchCriteria.SelectedIndex = 0;
      ViewState["grdCustomBrace_SearchCriteria"] = SearchCriteria.SelectedValue;

      grdCustomBrace.Rebind();
    }

    private void HideDispenseQueuePanel()
    {
      var dispenseQueuePane = (RadPane) splitterDispense.FindControl("paneDispenseQueue");
      dispenseQueuePane.Visible = false;
    }

    private DataSet GetPhysicians(int practiceLocationID)
    {
      var db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
      var dbCommand =
        db.GetStoredProcCommand("dbo.usp_PracticeLocation_Physician_Select_All_Names_By_PracticeLocationID");

      db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);

      return db.ExecuteDataSet(dbCommand);
    }

    private DataSet GetFitters(int practiceLocationID)
    {
      var db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
      var dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_Fitter_Select_All_Names_By_PracticeLocationID");

      db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);

      return db.ExecuteDataSet(dbCommand);
    }

    private Payer[] GetPayers(int practiceId)
    {
      var payers = Payer.SelectAll(practiceId);

      var payersWithBlankOption = new Payer[payers.Length + 1];
      payersWithBlankOption[0] = new Payer() {Name = "", PayerId = -1};

      for (var i = 0; i < payers.Length; i++)
      {
        var payer = payers[i];
        payersWithBlankOption[i + 1] = payer;
      }

      return payersWithBlankOption;
    }

    #region chkABNCostIsCash CheckBox events

    protected void chkABNCostIsCash_OnCheckChanged(object sender, EventArgs e)
    {
		using (var visionDB = VisionDataContext.GetVisionDataContext())
		{
			Practice practice = visionDB.Practices.Where(p => p.PracticeID == practiceID).SingleOrDefault();
			if (practice != null)
			{
				practice.ABNCostIsCash = chkABNCostIsCash.Checked;
				visionDB.SubmitChanges();
			}
		}
    }

    #endregion

    protected void grdInventory_ItemCommand(object source, GridCommandEventArgs e)
    {
      if (e.CommandName == "ExpandCollapse")
        AddInventoryItemsToDispenseQueue();
    }

    protected void rmpMain_TabClick(object sender, EventArgs e)
    {
      
      switch (rmpMain.SelectedIndex)
      {
        case 0:
          ClearInventorySearch();
          lblSummary.Text = "";
          lblSummary2.Text = "";
          RadAjaxManager1.ResponseScripts.Add("showDispenseQueue();");
          break;
        case 1:
          ClearCustomBraceSearch();
          lblSummary.Text = "";
          lblSummary2.Text = "";
          RadAjaxManager1.ResponseScripts.Add("showDispenseQueue();");
          break;
        case 2:
          grdReceiptHistory.Rebind();
          RadAjaxManager1.ResponseScripts.Add("hideDispenseQueue();");
          break;
        case 3:
          lblSummary.Text = "";
          lblSummary2.Text = "";
          RadAjaxManager1.ResponseScripts.Add("hideDispenseQueue();");
          break;
      }

      //if (rmpMain.SelectedIndex == 2) //Receipt History
      //    grdReceiptHistory.Rebind();
      //else if (rmpMain.SelectedIndex == 3)
      //    ctlPracticeLocationDispensementModifications.Visible = true;
      //else
      //    lblSummary.Text = "";
    }

    #region unused

    //private void RemoveExpandIconWhenNoRecords(GridTableView view)
    //{
    //    if (view.Controls.Count > 0)
    //    {
    //        foreach (GridItem item in view.Controls[0].Controls)
    //        {
    //            if (item is GridNestedViewItem)
    //            {
    //                var nestedViewItem = (GridNestedViewItem)item;
    //                if (nestedViewItem.NestedTableViews[0].Items.Count == 0)
    //                {
    //                    ((ImageButton)
    //                        nestedViewItem
    //                            .NestedTableViews[0]
    //                            .ParentItem["ExpandColumn"]
    //                            .Controls[0]).Visible = false;

    //                    nestedViewItem.Visible = false;
    //                }
    //                else
    //                {
    //                    RemoveExpandIconWhenNoRecords(nestedViewItem.NestedTableViews[0]);
    //                    //item.Expanded = false;
    //                    //item.Enabled = false;           
    //                }
    //            }
    //        }
    //    }
    //}

    //protected void chkScanMode_OnCheckedChanged(object sender, EventArgs e)
    //{
    //    if (chkScanMode.Checked == true)
    //    {
    //        ((RadComboBox)rttbSearchFilter.FindControl("cboSearchFilter"))
    //            .FindItemByValue("UPCCODE")
    //            .Selected = true;
    //    }
    //}

    /// <summary>
    /// This is used by the receipt
    /// </summary>
    /// <param name="PracticeLocationID"></param>
    /// <returns></returns>
    //private IDataReader GetBillingAddressByPracticeLocation(Int32 PracticeLocationID)
    //{
    //    var db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
    //    var dbCommand = db.GetStoredProcCommand("dbo.usp_GetBillingAddressByPracticeLocation");

    //    db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);

    //    return db.ExecuteReader(dbCommand);
    //}
    /// <summary>
    /// This is used by the receipt
    /// </summary>
    /// <param name="DispenseID"></param>
    /// <returns></returns>
    //private IDataReader GetDispenseReceipt(Int32 DispenseID)
    //{
    //    var db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
    //    var dbCommand = db.GetStoredProcCommand("dbo.usp_GetDispenseReceipt");

    //    db.AddInParameter(dbCommand, "DispenseID", DbType.Int32, DispenseID);

    //    return db.ExecuteReader(dbCommand);
    //}
    /// <summary>
    /// This is used by the receipt
    /// </summary>
    /// <param name="DispenseID"></param>
    /// <returns></returns>
    //private DateTime GetCreatedDate(Int32 DispenseID)
    //{
    //    var db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
    //    var dbCommand = db.GetStoredProcCommand("dbo.[usp_GetCreatedDateForReceipt]");

    //    db.AddInParameter(dbCommand, "DispenseID", DbType.Int32, DispenseID);

    //    return (DateTime)db.ExecuteScalar(dbCommand);
    //}

    #endregion
    protected void grdInventory_PreRender(object sender, EventArgs e)
    {
      if (_UIHelper.HasExpanded == false) //mws
      {
        UIHelper2.ExpandFormatGrid(grdInventory);
        _UIHelper.HasExpanded = true; //mws
      }
    }


    protected void grdInventory_PageIndexChanged(object source, GridPageChangedEventArgs e)
    {
      //MWS Merge this event code with the event below it and send in parameters
      AddInventoryItemsToDispenseQueue();

      //Next line makes sure items on new page get expanded when applicable - GR 

      if (ViewState["SearchCriteria"] == null ||
          ViewState["SearchCriteria"].ToString() == "Browse" ||
          ViewState["SearchCriteria"].ToString() == "")
      {
        _UIHelper.HasExpanded = true; //mws
      }
      else
      {
        _UIHelper.HasExpanded = false; //mws
      }
    }

    protected void grdCustomBrace_PageIndexChanged(object source, GridPageChangedEventArgs e)
    {
      AddCustomBraceItemsToDispenseQueue(grdCustomBrace.MasterTableView);
      //Next line makes sure items on new page get expanded when applicable - GR 

      if (ViewState["grdCustomBrace_SearchCriteria"] == null ||
          ViewState["grdCustomBrace_SearchCriteria"].ToString() == "Browse" ||
          ViewState["grdCustomBrace_SearchCriteria"].ToString() == "")
      {
        _grdCustomBrace_UIHelper.HasExpanded = true; //mws
      }

      else
      {
        _grdCustomBrace_UIHelper.HasExpanded = false; //mws
      }
    }

        protected void rtbReceiptHistory_ButtonClick(object sender, RadToolBarEventArgs e)
    {
      if (!(e.Item is RadToolBarButton))
        return;

      var b = (RadToolBarButton) e.Item;

      //MWS consider merging this code with the event code below it and sending in parameters
      if (b.CommandName == "Browse")
      {
        SearchCriteria = "Browse";
        ViewState["grdReceiptHistory_SearchCriteria"] = SearchCriteria;

        SearchText = "";
        ViewState["grdReceiptHistory_SearchText"] = SearchText;
        _grdReceiptHistory_UIHelper.HasExpanded = true; //mws

        grdReceiptHistory.Rebind();
      }

      if (b.CommandName == "Search")
      {
        //var toolbarReceiptHistory = (RadToolBar)sender;

        //var rttbFilterReceiptHistory1 = (RadToolBarButton)toolbarReceiptHistory.FindControl("rttbFilterReceiptHistory");
        _grdReceiptHistory_UIHelper.IsSearch = true;
        //var searchText = (TextBox)rttbFilterReceiptHistory1.FindControl("tbFilterReceiptHistory");
        var searchText = (TextBox) rttbFilterReceiptHistory.FindControl("tbFilterReceiptHistory");
        ViewState["grdReceiptHistory_SearchText"] = searchText.Text;

        //var rttbSearchFilterReceiptHistory1 = (RadToolBarButton)toolbarReceiptHistory.FindControl("rttbSearchFilterReceiptHistory");
        //var SearchCriteria = (RadComboBox)rttbSearchFilterReceiptHistory1.FindControl("cboSearchFilterReceiptHistory");
        var SearchCriteria = (RadComboBox) rttbSearchFilterReceiptHistory.FindControl("cboSearchFilterReceiptHistory");
        ViewState["grdReceiptHistory_SearchCriteria"] = SearchCriteria.SelectedValue;

        grdReceiptHistory.Rebind();
        _grdReceiptHistory_UIHelper.HasExpanded = false; //mws
      }
    }

    protected void rtbCustomBrace_ButtonClick(object sender, RadToolBarEventArgs e)
    {
      if (!(e.Item is RadToolBarButton))
        return;

      var b = (RadToolBarButton) e.Item;

      //MWS consider merging this code with the event code below it and sending in parameters
      if (b.CommandName == "Browse")
      {
        SearchCriteria = "Browse";
        ViewState["grdCustomBrace_SearchCriteria"] = SearchCriteria;

        SearchText = "";
        ViewState["grdCustomBrace_SearchText"] = SearchText;

        grdCustomBrace.Rebind();
        _grdCustomBrace_UIHelper.HasExpanded = true; //mws
      }

      if (b.CommandName == "Search")
      {
        //var toolbarCustomBrace = (RadToolBar)b.Owner;

        //var rttbFilterCustomBrace1 = (RadToolBarButton)toolbarCustomBrace.FindControl("rttbFilterCustomBrace");
        _grdCustomBrace_UIHelper.IsSearch = true;
        //var searchText = (TextBox)rttbFilterCustomBrace1.FindControl("tbFilterCustomBrace");
        var searchText = (TextBox) rttbFilterCustomBrace.FindControl("tbFilterCustomBrace");
        ViewState["grdCustomBrace_SearchText"] = searchText.Text;

        //var rttbSearchFilterCustomBrace1 = (RadToolBarButton)toolbarCustomBrace.FindControl("rttbSearchFilterCustomBrace");
        //var SearchCriteria = (RadComboBox)rttbSearchFilterCustomBrace1.FindControl("cboSearchFilterCustomBrace");
        var SearchCriteria = (RadComboBox) rttbSearchFilterCustomBrace.FindControl("cboSearchFilterCustomBrace");
        ViewState["grdCustomBrace_SearchCriteria"] = SearchCriteria.SelectedValue;

        grdCustomBrace.Rebind();
        _grdCustomBrace_UIHelper.HasExpanded = false; //mws
      }
    }

    protected void RadToolbar1_ButtonClick(object sender, RadToolBarEventArgs e)
    {
      if (!(e.Item is RadToolBarButton))
        return;

      var b = (RadToolBarButton) e.Item;
      if (b.CommandName == "Browse")
      {
        SearchCriteria = "Browse";
        ViewState["SearchCriteria"] = SearchCriteria;

        SearchText = "";
        ViewState["SearchText"] = SearchText;

        grdInventory.Rebind();
        _UIHelper.HasExpanded = true; //mws
      }

      if (b.CommandName == "Search")
      {
        _UIHelper.IsSearch = true;
        var searchText = (TextBox) rttbFilter.FindControl("tbFilter");
        ViewState["SearchText"] = searchText.Text;

        var SearchCriteria = (RadComboBox) rttbSearchFilter.FindControl("cboSearchFilter");
        ViewState["SearchCriteria"] = SearchCriteria.SelectedValue;

        if (chkScanMode.Checked == true)
        {
          if (AutoInsertItemIntoDispenseQueue(SearchCriteria.SelectedValue, searchText.Text) == true)
          {
            if (!((string) ViewState["SearchCriteria"]).ToLower().Equals("browse"))
            {
              // emulate a browse click
              ViewState["SearchCriteria"] = "Browse";
              ViewState["SearchText"] = "";
              grdInventory.Rebind();
              _UIHelper.HasExpanded = true;
            }
            return;
          }
        }

        grdInventory.Rebind();
        _UIHelper.HasExpanded = false; //mws
      }
    }

    public DataSet SearchForItemsInventory(int practiceLocationID, string searchCriteria, string searchText)
    {
      var db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
      var dbCommand = db.GetStoredProcCommand("dbo.usp_SearchforItemsInInventory");

      db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
      db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, searchCriteria);
      db.AddInParameter(dbCommand, "SearchText", DbType.String, searchText);

      return db.ExecuteDataSet(dbCommand);
    }

    protected void grdCustomBrace_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
      if (!e.IsFromDetailTable)
      {
        string searchCriteria = "";
        string searchText = "";

        if (ViewState["grdCustomBrace_SearchCriteria"] != null)
        {
          searchCriteria = (string) ViewState["grdCustomBrace_SearchCriteria"];
        }

        if (ViewState["grdCustomBrace_SearchText"] != null)
        {
          searchText = (string) ViewState["grdCustomBrace_SearchText"];
        }

        grdCustomBrace.DataSource = GetCustomBraceOrdersforDispense_ParentLevel(practiceLocationID, searchCriteria,
          searchText.Trim());
      }
    }

    protected void grdCustomBrace_DetailTableDataBind(object s, GridDetailTableDataBindEventArgs e)
    {
      string searchCriteria = "";
      string searchText = "";

      if (ViewState["grdCustomBrace_SearchCriteria"] != null)
      {
        searchCriteria = (string) ViewState["grdCustomBrace_SearchCriteria"];
      }

      if (ViewState["grdCustomBrace_SearchText"] != null)
      {
        searchText = (string) ViewState["grdCustomBrace_SearchText"];
      }

      var dataItem = (GridDataItem) e.DetailTableView.ParentItem;
      var BregVisionOrderID = dataItem.GetDataKeyValue("BregVisionOrderID").ToString();
      e.DetailTableView.DataSource =
        GetCustomBraceOrdersforDispense_Detail1Level(practiceLocationID, searchCriteria, searchText.Trim())
          .Tables[0]
          .Select("BregVisionOrderID=" + BregVisionOrderID);
    }

    protected void grdInventory_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
      string searchCriteria = "Browse";
      string searchText = "";

      if (ViewState["SearchCriteria"] != null)
      {
        searchCriteria = (string) ViewState["SearchCriteria"];
      }

      if (ViewState["SearchText"] != null)
      {
        searchText = (string) ViewState["SearchText"];
      }

      if (!e.IsFromDetailTable)
      {
        grdInventory.DataSource = GetSuppliers(practiceLocationID, searchCriteria, searchText.Trim());
      }
    }

    protected void grdInventory_DetailTableDataBind(object s, GridDetailTableDataBindEventArgs e)
    {
      string searchCriteria = "Browse";
      string searchText = "";

      if (ViewState["SearchCriteria"] != null)
      {
        searchCriteria = (string) ViewState["SearchCriteria"];
      }

      if (ViewState["SearchText"] != null)
      {
        searchText = (string) ViewState["SearchText"];
      }

      var dataItem = (GridDataItem) e.DetailTableView.ParentItem;
      var SupplierID = dataItem.GetDataKeyValue("SupplierID").ToString();
      e.DetailTableView.DataSource =
        GetAllProductDatabyLocation(practiceID, practiceLocationID, searchCriteria, searchText.Trim())
          .Tables[0]
          .Select("SupplierID=" + SupplierID);
    }

      protected void grdReceiptHistory_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
      {
          if (!e.IsFromDetailTable)
          {
              string searchCriteria = "";
              string searchText = "";
                var searchCriteriaPicker =
                     (RadComboBox)rttbSearchFilterReceiptHistory.FindControl("cboSearchFilterReceiptHistory");
                var isBatch = ViewState["grdReceiptHistory_SearchCriteria"].ToString() == "Batch";
              if (isBatch)
              {
                  searchCriteria = "Batch";
                  searchText = ViewState["grdReceiptHistory_SearchText"].ToString();
              }
              else
              {
                 
                  searchCriteria = searchCriteriaPicker.SelectedValue;
                  ViewState["grdReceiptHistory_SearchCriteria"] = searchCriteriaPicker.SelectedValue;
                  if (ViewState["grdReceiptHistory_SearchText"] != null)
                  {
                      searchText = (string) ViewState["grdReceiptHistory_SearchText"];
                  }
              }

              grdReceiptHistory.DataSource = GetReceiptHistoryParent(practiceLocationID, searchCriteria, searchText);

              if (searchCriteria == "Batch")
              {
                  var dsReceiptHistory = grdReceiptHistory.DataSource as DataSet;
                  var dsHistoryCount = (from hist in dsReceiptHistory.Tables[0].AsEnumerable()
                      select hist).Count();
                  grdReceiptHistory.PageSize = dsHistoryCount + 5;

                  //var selectedDataItems = (from GridDataItem receiptHistoryItem in (grdReceiptHistory.Items as IEnumerable)
                  //                         //where receiptHistoryItem.GetColumnInt32("DispenseBatchID") == Convert.ToInt32(searchText) //DispenseBatchID
                  //                         select receiptHistoryItem).Update(rhi => { rhi.Selected = true; });
              }
              else
              {
                  grdReceiptHistory.PageSize = Convert.ToInt16(Session["RecordsDisplayed"].ToString());
              }
                ViewState["grdReceiptHistory_SearchCriteria"] = searchCriteriaPicker.SelectedValue;
            }
      }

    protected void grdReceiptHistory_DetailTableBind(object s, GridDetailTableDataBindEventArgs e)
    {
      string searchCriteria = "";
      string searchText = "";

      if (ViewState["grdReceiptHistory_SearchCriteria"] != null)
      {
        searchCriteria = (string) ViewState["grdReceiptHistory_SearchCriteria"];
      }

      if (ViewState["grdReceiptHistory_SearchText"] != null)
      {
        searchText = (string) ViewState["grdReceiptHistory_SearchText"];
      }

      var dataItem = (GridDataItem) e.DetailTableView.ParentItem;
      var DispenseID = dataItem.GetDataKeyValue("DispenseID").ToString();
      e.DetailTableView.DataSource =
        GetAllReceiptHistoryDataByLocation(practiceLocationID, searchCriteria, searchText)
          .Tables[0]
          .Select("DispenseID=" + DispenseID);
    }

    public DataSet GetAllReceiptHistoryDataByLocation(int practiceLocationID, string SearchCriteria, string SearchText)
    {
      var db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
      var dbCommand = db.GetStoredProcCommand("dbo.usp_ReceiptHistory_Detail");

      db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
      db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
      db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);

      return db.ExecuteDataSet(dbCommand);
    }

    public DataSet GetAllProductDatabyLocation(int practiceID, int practiceLocationID, string SearchCriteria,
      string SearchText)
    {
      var db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
      var dbCommand = db.GetStoredProcCommand("dbo.usp_Get_Dispense_Records_for_Browse_Grid_20141111");

      db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
      db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
      db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
      db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);

      var ds = (DataSet) null;
      if (_dsSearch == null)
      {
        ds = db.ExecuteDataSet(dbCommand);
        _dsSearch = ds;
        _UIHelper.SetRecordCount(ds, "ProductName");
        _UIHelper.SetGridLabel(SearchCriteria, Page.IsPostBack, lblBrowseDispense);
      }
      else
      {
        ds = _dsSearch;
      }
      return ds;
    }

    private void AddInventoryItemsToDispenseQueue()
    {
      var numProducts = 0;

      var selectedDataItems = from GridDataItem inventoryItem in (grdInventory.Items as IEnumerable)
        where inventoryItem.Selected == true
              && inventoryItem.OwnerTableView.Name == "gvwSupplier"
        select inventoryItem;

      foreach (var gridDataItem in selectedDataItems)
      {
        var productInventoryID = gridDataItem.GetColumnInt32("ProductInventoryID");
        //string txtPatientCode = gridDataItem.GetColumnText("txtPatientCode");
        //Int32 physicianID = gridDataItem.GetColumnSelectedValueInt32("cboPhysicians");
        var isMedicare = gridDataItem.GetColumnChecked("chkIsMedicare");
        // var isABNForm = gridDataItem.GetColumnChecked("chkABNForm");
       // var side = gridDataItem.GetColumnString("Side");
        //var quantityOnHand = gridDataItem.GetColumnInt32("QuantityOnHand");
        //var productName = gridDataItem.GetColumnString("Product");
        var dmeDeposit = gridDataItem.GetColumnDecimal("DMEDeposit");
        var abnType = gridDataItem.GetColumnSelectedValue("ddlAbnType");
        //var quantity = 1
        var mod1 = gridDataItem.GetColumnString("Mod1") == "&nbsp;" ? null : gridDataItem.GetColumnString("Mod1");
        var mod2 = gridDataItem.GetColumnString("Mod2") == "&nbsp;" ? null : gridDataItem.GetColumnString("Mod2");
        var mod3 = gridDataItem.GetColumnString("Mod3") == "&nbsp;" ? null : gridDataItem.GetColumnString("Mod3");


                if (isMedicare == true)
        {
          dmeDeposit = 0M;
        }


        UpdateDispenseQueue(productInventoryID, "", 0, 1, isMedicare, "", dmeDeposit, abnType, null, "", mod1, mod2, mod3,
          patientFirstName: "", patientLastName: "", note: "", payerID: null, isCustomFit: null);
        numProducts += 1;
        gridDataItem.Selected = false;
        try
        {
          //((CheckBox)gridDataItem.FindControl("chkIsMedicare")).Checked = false;
          //((CheckBox)gridDataItem.FindControl("chkABNForm")).Checked = false;
        }
        catch
        {
        }
      }

      var browseDispense = string.Concat(numProducts.ToString(), " product(s) added to dispensement");

      if (numProducts > 0)
      {
        lblBrowseDispense.Text = browseDispense;
        lblBrowseDispense.CssClass = "SuccessMsg";

        //dispenseQueueNeedsRefresh = true;
        grdSummary2.Rebind();
      }
    }

    private void AddCustomBraceItemsToDispenseQueue(GridTableView gridTableView)
    {
      var numProducts = 0;

      var selectedDataItem = from GridDataItem poItem in (gridTableView.Items as IEnumerable)
        where poItem.Selected == true
              && poItem.OwnerTableView.Name == "mtvCustomBraceOutstanding_ParentLevel"
        select poItem;


      foreach (var poItem in selectedDataItem)
      {
        var bregVisionOrderID = poItem.GetColumnInt32("BregVisionOrderID");
        var txtPatientCode = poItem.GetColumnText("txtPatientCode");
        var physicianID = poItem.GetColumnSelectedValueInt32("cboPhysicians");
        var isMedicare = poItem.GetColumnChecked("chkIsMedicare");

        foreach (GridDataItem customBraceItem in poItem.ChildItem.NestedTableViews[0].Items)
        {
          //   var isABNForm = customBraceItem.GetColumnChecked("chkABNForm");
          var abnType = customBraceItem.GetColumnSelectedValue("ddlAbnType");
         // var side = customBraceItem.GetColumnSelectedValue("cboSide");
          var quantityToDispense = customBraceItem.GetColumnInt32("QuantityOnHand");
          var productName = customBraceItem.GetColumnString("Product");
          var productInventoryID = customBraceItem.GetColumnInt32("ProductInventoryID");
          
            var mod1 = "";
            var mod2 = "";
            var mod3 = "";

          UpdateDispenseQueue(productInventoryID, txtPatientCode, physicianID, quantityToDispense, isMedicare, "", 0, abnType,
            bregVisionOrderID, "", mod1, mod2, mod3, patientFirstName: "", patientLastName: "", note: "", payerID: -1, isCustomFit: null);
          numProducts += 1;
        }
        poItem.Selected = false;
      }

      string browseDispense = string.Concat(numProducts.ToString(), " product(s) added to dispensement");
      if (numProducts != 0)
      {
        lblBrowseDispenseCustomBrace.Text = browseDispense;

        //dispenseQueueNeedsRefresh = true;
        grdSummary2.Rebind();
      }
    }

    protected void UpdateDispenseQueue(int InventoryID, string PatientCode, int PhysicianID, int Quantity,
      bool IsMedicare, string Side, decimal DMEDeposit, string abnType, int? BregVisionOrderID, string queueReason,
      string mod1, string mod2, string mod3,
      string patientFirstName, string patientLastName, string note, int? payerID, bool? isCustomFit)
    {
        var ABNForm = abnType == "Medicare" ? true : false;
      DAL.PracticeLocation.Dispensement.AddUpdateDispensement(
        DispenseQueueID: 0,
        PracticeLocationID: practiceLocationID,
        InventoryID: InventoryID,
        PatientCode: PatientCode,
        PhysicianID: PhysicianID,
        payerID: payerID,
        ICD9_Code: "",
        IsMedicare: IsMedicare,
        ABNForm: ABNForm,
        IsCashCollection: false,
        Side: Side,
        DMEDeposit: DMEDeposit,
        AbnType: abnType,
        BregVisionOrderID: BregVisionOrderID,
        Quantity: Quantity,
        isCustomFit: isCustomFit,
        mod1: mod1,
        mod2: mod2,
        mod3: mod3,
        DispenseDate: null,
        CreatedUser: 1,
        CreatedDate: DateTime.Now,
        QueuedReason: queueReason,
        patientFirstName: patientFirstName,
        patientLastName: patientLastName,
        note: note,
        dispenseSignatureID: 0,
        isDeleted: false,
        IsCalledFromV1Endpoint: false
        );
    }

    public DataSet GetSuppliers(int practiceLocationID, string SearchCriteria, string SearchText)
    {
      var db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
      var dbCommand = db.GetStoredProcCommand("dbo.usp_GetSuppliers_Searchable");

      db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
      db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
      db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);

      return db.ExecuteDataSet(dbCommand);
    }

    public DataSet GetCustomBraceOrdersforDispense_ParentLevel(int practiceLocationID, string SearchCriteria,
      string SearchText)
    {
      var db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
      var dbCommand = db.GetStoredProcCommand("dbo.usp_GetCustomBraceOrdersforDispense_ParentLevel");

      db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
      db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
      db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);

      return db.ExecuteDataSet(dbCommand);
    }

    public DataSet GetCustomBraceOrdersforDispense_Detail1Level(int practiceLocationID, string SearchCriteria,
      string SearchText)
    {
      var db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
      var dbCommand = db.GetStoredProcCommand("dbo.usp_GetCustomBraceOrdersforDispense_Detail1Level_20141205");

      db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
      db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
      db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);

      return db.ExecuteDataSet(dbCommand);
    }

    public DataSet GetReceiptHistoryParent(int practiceLocationID, string SearchCriteria, string SearchText)
    {
      var db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
      var dbCommand = db.GetStoredProcCommand("dbo.usp_ReceiptHistory");

      db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
      db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
      db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);

      return db.ExecuteDataSet(dbCommand);
    }

    public DataSet GetInventoryBySupplier(int practiceLocationID)
    {
      var db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
      var dbCommand = db.GetStoredProcCommand("dbo.usp_GetInventoryBySupplier");

      db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);

      return db.ExecuteDataSet(dbCommand);
    }

    #region Button Click Event

    protected void btnAddBrowseToDispensement_Click(object sender, EventArgs e)
    {
      lblSummary.Text = "";
      lblSummary2.Text = "";

      AddInventoryItemsToDispenseQueue();
    }

    protected void btnAddCustomBraceToDispensement_Click(object sender, EventArgs e)
    {
      lblSummary.Text = "";
      lblSummary2.Text = "";

      AddCustomBraceItemsToDispenseQueue(grdCustomBrace.MasterTableView);

      grdCustomBrace.Rebind();
    }

    protected void btnSaveDispenseQueue_Click(object sender, EventArgs e)
    {
      var selectedDispenseQueueIDs = (List<int>) null;

      SaveDispenseQueue(grdSummary2.MasterTableView, out selectedDispenseQueueIDs);
       grdSummary2.Rebind();

      var message = "Dispense Queue saved.";

      lblSummary.Text = message;
      lblSummary2.Text = message;
        lblSummary2.CssClass = "SuccessMsg";
        lblSummary.CssClass = "SuccessMsg";
    }

     private bool CheckQuantityDispensed(GridTableView tableView)
     {
            foreach (GridDataItem row in tableView.Items)
            {
                //There is only ever be 1 item in the nested table view
                if (row.ChildItem.NestedTableViews[0].Items.Count > 0)
                {
                    if (!row.Selected)
                    {
                        continue;
                    }

                    var Quantity = row.ChildItem.NestedTableViews[0].Items[0].GetRadNumericTextBox("txtQuantityDispensed").Value;

                    if (Quantity == null)
                        return true;
                }
            }

            return false;
      }

    protected void btnDispenseProducts_Click(object sender, EventArgs e)
    {
      lblSummary.Text = "";
      lblSummary2.Text = "";

      //RadWindowManager1.EnableStandardPopups = true;
      //RadAjaxManager1.EnableAJAX = false;

      var dispenseBatchID = 0;
      var selectedDispenseQueueIDs = (List<int>) null;
      var practice = DAL.Practice.Practice.Select(practiceID);

      if (practice.IsLateralityRequired != null && practice.IsLateralityRequired.Value)
      {
            if (VerifyIsLateralRequirement(grdSummary2.MasterTableView))
            {
                    var message = "Please select a Side before dispensing";
                    lblSummary.Text = lblSummary2.Text = message;
                    lblSummary.Visible = lblSummary2.Visible = true;
                    lblSummary.CssClass = lblSummary2.CssClass = "ErrorMsg";
                    return;
             }
      }

      if (HasIncompletePhysicianEntries(grdSummary2.MasterTableView))
      {
        var message = "Please select a provider before dispensing.";
        lblSummary.Text = lblSummary2.Text = message;
        lblSummary.Visible = lblSummary2.Visible = true;
        lblSummary.CssClass = lblSummary2.CssClass = "ErrorMsg";
        return;
      }

      if (CheckQuantityDispensed(grdSummary2.MasterTableView))
      {
                var message = "Please Select Quantity Dispensed.";
                lblSummary.Text = lblSummary2.Text = message;
                lblSummary.Visible = lblSummary2.Visible = true;
                lblSummary.CssClass = lblSummary2.CssClass = "ErrorMsg";
                return;
       }
               
      if (HasIncompleteSplitCodeEntries(grdSummary2.MasterTableView))
      {
        var splitcodeMessage =
          "Please complete Split Code, Fitter selections and, if displayed, please select a Custom Fit Procedure before dispensing.";
        lblSummary.Text = lblSummary2.Text = splitcodeMessage;
        lblSummary.Visible = lblSummary2.Visible = true;
        lblSummary.CssClass = lblSummary2.CssClass = "ErrorMsg";
        return;
      }
      var selectedProductInventoryIDs = SaveDispenseQueue(grdSummary2.MasterTableView, out selectedDispenseQueueIDs);
      var dispensedProducts = new List<int>();
      var isDispensed = false;
      int DispenseId;
      

      if (
        Dispensement.AreCustomBracesInDispensementCompletlySelected(practiceLocationID, selectedDispenseQueueIDs) == true)
      {
        dispenseBatchID = Dispensement.DispenseProducts(practiceLocationID, selectedProductInventoryIDs,
        dispensedProducts, selectedDispenseQueueIDs, false, out DispenseId);

        byte[] CompressedPDF;
        //Save here for emr.
        BregWcfService.Classes.Email.SendPatientEmail(dispenseBatchID, practiceLocationID, null,out CompressedPDF, fromVision: true);
        var dispenseCount = dispensedProducts.Sum();
        var message = string.Concat(dispensedProducts.Sum().ToString(), " item(s) dispensed.");

        // trigger dispense queue refresh if at least 1 item dispensed
        if (dispenseCount > 0)
        {
          //dispenseQueueNeedsRefresh = true;
          grdSummary2.Rebind();
        }

        lblSummary.Text = message;
        lblSummary2.Text = message;
          lblSummary2.CssClass = "SuccessMsg";
          lblSummary.CssClass = "SuccessMsg";
        //UIHelper.WriteJavascriptToLabel(grdSummary, lblSummary, message);
        isDispensed = true;

        if (selectedProductInventoryIDs.Count() > 0)
        {
          lblSummary.Visible = true;
          lblSummary2.Visible = true;
          //Send billing email here
          //SendBillingEmail(dispenseBatchID);
          BregWcfService.Classes.Email.SendBillingEmail(dispenseBatchID, practiceLocationID);

          grdCustomBrace.Rebind();
          //grdSummary.Rebind();
          grdInventory.Rebind();

          lblBrowseDispense.Text = "";
          lblBrowseDispenseCustomBrace.Text = "";
          //lblSummary.Text = "";
          //////////Set Search box back to empty
          ((TextBox) rttbFilter.FindControl("tbFilter")).Text = "";
          //////////clear custom brace search box here like the line above
        }
      }
      else
      {
        //Please dispense all items in custom brace order together.
        lblSummary.Text = "Please dispense all items in custom brace order(s) together.";
        lblSummary2.Text = "Please dispense all items in custom brace order(s) together.";
          lblSummary.CssClass = "ErrorMsg";
          lblSummary2.CssClass = "ErrorMsg";
        //UIHelper.WriteJavascriptToLabel(grdSummary, lblSummary, "Please dispense all items in custom brace order(s) together.");
        lblSummary.Visible = true;
        lblSummary2.Visible = true;
        //MWS missingProductIDs contains the ProductID's needed to do the dispensement
        isDispensed = false;
      }

      // if nothing was dispensed, then skip over post-dispense screen updates
      if (!isDispensed)
        return;

      #region post-dispense screen updates

      if (selectedProductInventoryIDs.Count() > 0)
      {
        //btnDispenseProducts.Enabled = false;
        rmpMain.Tabs[2].Enabled = true;
        rmpMain.Tabs[2].Selected = true;
        PageView7.Selected = true;
        ////////Receipt.InnerHtml = BuildReceipt();
        //rmpMain_TabClick(rmpMain, e);
        ClearInventorySearch();
        ClearCustomBraceSearch();

        SelectLastDispenseBatchInReceiptHistory(dispenseBatchID);
        //RadAjaxManager1.ActiveElementID = null;
      }

      #endregion
    }

    private bool HasIncompleteSplitCodeEntries(GridTableView tableView)
    {
        /*
         * Note(Jackson): If custom fit is not enabled for a practice, you should not check the
         * custom fit options because the options were not displayed to the user
         */
        
        if (!practiceIsCustomFitEnabled)
        {
            return false;
        }
        foreach (GridDataItem parentRow in tableView.Items)
        {
            //There is only ever be 1 item in the nested table view
            if (parentRow.ChildItem.NestedTableViews[0].Items.Count > 0)
            {
                var row = parentRow.ChildItem.NestedTableViews[0].Items[0];

                if (!parentRow.Selected)
                {
                    continue;
                }

                var splitCodeOptionList = row.GetDropDownList("cboSplitCode");
                if (!splitCodeOptionList.Visible)
                {
                    continue;//todo this will cause problems if the parent control is closed. 
                }
                if (splitCodeOptionList.SelectedIndex == 0)
                {
                    return true;
                }

                var splitCodeOption = splitCodeOptionList.SelectedValue;
                if (!splitCodeOption.Equals("Custom Fit"))
                {
                    continue;
                }

                var fitterList = row.GetDropDownList("cboFitters");
                if (fitterList.SelectedIndex == 0)
                {
                    return true;
                }

                Panel customFitProcedurePanel = (Panel)row.FindControl("CustomFitProcedurePanel");
                if (customFitProcedurePanel == null)
                {
                    throw new ApplicationException("CustomFitProcedurePanel control not found!");
                }
                else
                {
                    bool isSplitCode = Convert.ToBoolean(customFitProcedurePanel.Attributes["data-splitcode"]);
                    if (isSplitCode)
                    {
                        List<Tuple<int, string>> customFitProcedureList = GetCustomFitProcedureListForRow(row);

                        if (customFitProcedureList.Count == 0)
                            return true;
                    }
                }
            }
        }

        return false;

    }

        private bool VerifyIsLateralRequirement(GridTableView tableView)
        {
            var practiceCatalogProductIds = new List<int>();
            foreach (GridNestedViewItem nestedViewItem in tableView.GetItems(GridItemType.NestedView))
            {
                if (nestedViewItem.NestedTableViews.Length > 0)
                {
                    foreach (GridDataItem row in nestedViewItem.NestedTableViews[0].Items)
                    {
                        if (row.OwnerTableView.ParentItem.Selected)
                        {
                            var side = row.GetDropDownList("cboSide");
                            if(side.SelectedValue == "")
                            {
                                var PracticeCatalogProductID = row.GetBoundColumnInt32("PracticeCatalogProductID");
                                practiceCatalogProductIds.Add(PracticeCatalogProductID);
                            }
                        }
                    }
                }
            }
            return new BLL.Practice.Practice().CheckLateralityExemptions(practiceCatalogProductIds);         
        }

    private bool HasIncompletePhysicianEntries(GridTableView tableView)
    {
       

        foreach (GridDataItem row in tableView.Items)
        {
        //There is only ever be 1 item in the nested table view
        if (row.ChildItem.NestedTableViews[0].Items.Count > 0)
        {
            if (!row.Selected)
            {
                continue;
            }

            var physicianList = row.ChildItem.NestedTableViews[0].Items[0].GetDropDownList("cboPhysicians");
            if (physicianList.SelectedValue == "-1")
            {
                return true;
            }
        }
      }

      return false;
    }

    protected void btnPrintReceiptHistory_Click(object sender, EventArgs e)
    {
      Boolean isItem = PopupSelectedReceipt(grdReceiptHistory.MasterTableView);
      if (!isItem)
      {
        lblReceiptHistory.ForeColor = System.Drawing.Color.Red;
        lblReceiptHistory.Text = "No receipt selected. Please choose a receipt and press the 'Print Preview' button. ";
      }
      else
      {
        lblReceiptHistory.ForeColor = System.Drawing.Color.Black;
        lblReceiptHistory.Text = "Please choose a receipt and press the 'Print Preview' button. ";
      }
    }


    private Boolean FaxSelectedReceipts(GridTableView gridTableView)
    {
      // look up selected dispense IDs
      var selectedDataItems = from GridDataItem receiptHistoryItem in (gridTableView.Items as IEnumerable)
        where receiptHistoryItem.Selected == true
        select receiptHistoryItem.GetColumnInt32("DispenseID");

      // check selection
      if (selectedDataItems.Count() < 1)
      {
        lblReceiptHistory.ForeColor = System.Drawing.Color.Red;
        lblReceiptHistory.Text = "Please select at least one receipt to resend.";
        return false;
      }

      // send faxes
      var totalFaxesSent = SendFaxDispenseReceipt(selectedDataItems);

      // notification
      lblReceiptHistory.ForeColor = System.Drawing.Color.Red;
      lblReceiptHistory.Text = string.Format("Resent {0} fax(es).", totalFaxesSent);

      return true;
    }

    private int SendFaxDispenseReceipt(IEnumerable<int> dispenseIDs)
    {
      // check if fax is enabled
      if (!IsPracticeFaxDispensementEnabled)
        return 0;

      // get configured fax number
      var faxnum = "";
      using (var visionDB = VisionDataContext.GetVisionDataContext())
      {
        faxnum = (from pl in visionDB.PracticeLocations
          where pl.PracticeLocationID == practiceLocationID
          select pl.FaxForPrintDispense).FirstOrDefault();
      }

      // if there is a fax number, and fax is enabled for this practice, send the fax here
      var totalFaxesSent = 0;
      if (!string.IsNullOrWhiteSpace(faxnum))
      {
        var fax = new BregWcfService.Classes.Fax();
        foreach (var id in dispenseIDs)
        {
          try
          {
            //var html = new StringWriter();
            //System.Web.HttpContext.Current.Server.Execute(GetPopupReceiptURL(new[] { id }, true), html);
            //fax.SendFax(faxnum, html.ToString());
            string pdfFilename;
            if (BregWcfService.Classes.Email.RenderDispenseReceiptPDF(id, practiceLocationID, out pdfFilename))
              fax.SendFax(faxnum, pdfFilename, 0, J2.eFaxDeveloper.FaxFileType.pdf);
            totalFaxesSent++;
          }
          catch (Exception ex)
          {
            throw new ApplicationException("Error sending fax.", ex);
          }
        }
      }

      return totalFaxesSent;
    }

    protected void btnResendPrintDispensement_Click(object sender, EventArgs e)
    {
      FaxSelectedReceipts(grdReceiptHistory.MasterTableView);
    }

    #endregion

    private void SelectLastDispenseBatchInReceiptHistory(int DispenseBatchID)
    {
      //set search criteria to Batch and SearchText to BatchID
      ViewState["grdReceiptHistory_SearchCriteria"] = "Batch";
      ViewState["grdReceiptHistory_SearchText"] = DispenseBatchID.ToString();

      grdReceiptHistory.Rebind();

      var selectedDataItems = (from GridDataItem receiptHistoryItem in (grdReceiptHistory.Items as IEnumerable)
        where receiptHistoryItem.GetColumnInt32("DispenseBatchID") == DispenseBatchID
        select receiptHistoryItem).Update(rhi => { rhi.Selected = true; });
    }

    private void SetCellInvisible(GridViewRowEventArgs e, string fieldName)
    {
      if (e.Row.Cells.Count >= summaryFields[fieldName])
      {
        e.Row.Cells[summaryFields[fieldName]].Visible = false;
      }
    }

    private List<Tuple<int, string>> GetCustomFitProcedureListForRow(GridDataItem row)
    {
      // While storing the list of custom fit procedure responses from the user,
      // Dispensement.AddUpdateDispensement() will first remove any existing such items
      // for a given DispenseQueueID *unless the customFitProcedureResponses collection we send it
      // is null*.  We want to ensure that none of these response items get orphaned in the database
      // so we'll always initialize our response collection to an empty list to force that initial
      // delete.
      List<Tuple<int, string>> customFitProcedureResponses = new List<Tuple<int, string>>();
      Repeater rpCustomFitProcedureList = (Repeater) row.FindControl("rpCustomFitProcedureList");
      if (rpCustomFitProcedureList == null)
      {
        throw new ApplicationException("rpCustomFitProcedureList control not found!");
      }
      else
      {
        foreach (RepeaterItem item in rpCustomFitProcedureList.Items)
        {
          Label lblCustomFitProcedureID = (Label) item.FindControl("lblCustomFitProcedureID");
          CheckBox cbIsUserEntered = (CheckBox) item.FindControl("cbIsUserEntered");
          CheckBox cbIsActive = (CheckBox) item.FindControl("cbIsActive");
          TextBox txtDescription = (TextBox) item.FindControl("txtDescription");
          if (lblCustomFitProcedureID == null || cbIsUserEntered == null ||
              txtDescription == null || cbIsActive == null)
          {
            throw new ApplicationException(
              "One or more of lblCustomFitProcedureID, cbIsUserEntered, cbIsActive or txtDescription controls not found!");
          }
          else
          {
            if (cbIsUserEntered.Checked)
              cbIsActive.Checked = (txtDescription.Text != null && txtDescription.Text != string.Empty);

            if (cbIsActive.Checked)
            {
              int customFitProcedureID = -1;
              if (Int32.TryParse(lblCustomFitProcedureID.Text, out customFitProcedureID))
              {
                Tuple<int, string> response = new Tuple<int, string>(
                  customFitProcedureID,
                  txtDescription.Text == string.Empty ? null : txtDescription.Text);
                customFitProcedureResponses.Add(response);
              }
            }
          }
        }
      }
      return customFitProcedureResponses;
    }

    private List<int> SaveDispenseQueue(GridTableView tableView, out List<int> selectedDispenseQueueIDs)
    {
        var selectedProductInventoryIDs = new List<int>();
        selectedDispenseQueueIDs = new List<int>();
        foreach (GridNestedViewItem nestedViewItem in tableView.GetItems(GridItemType.NestedView))
        {
            if (nestedViewItem.NestedTableViews.Length > 0)
            {
                foreach (GridDataItem row in nestedViewItem.NestedTableViews[0].Items)
                {
                    #region get control values

                    var dispenseQueueID = Convert.ToInt32(row.OwnerTableView.ParentItem.GetDataKeyValue("DispenseQueueID").ToString());
                    var ProductInventoryID = row.GetBoundColumnInt32("ProductInventoryID");
                    var quantityOnHand = row.GetBoundColumnInt32("QuantityOnHandPerSystem");
                    var Quantity = row.GetRadNumericTextBoxInt32("txtQuantityDispensed");
                    var isMedicare = row.GetCheckBoxBool("chkIsMedicare");
                    var bregVisionOrderID = (int?)null;
                    var txtPatientCode = row.GetTextBoxString("txtPatientCode");
                    var patientFirstName = row.GetColumnText("txtPatientFirstName");
                    var patientLastName = row.GetColumnText("txtPatientLastName");
                    var note = row.GetColumnText("txtNote");
                    var patientEmail = row.GetTextBoxString("txtPatientEmail");
                    var PracticeCatalogProductID = row.GetBoundColumnInt32("PracticeCatalogProductID");
                    var PhysicianID = row.GetDropDownListValueInt32("cboPhysicians");
                    var payerID = row.GetDropDownListValueInt32("cboPayers");
                    var BillingCharge = row.GetBoundColumnDecimal("BillingCharge");
                    var BillingChargeCash = row.GetBoundColumnDecimal("BillingChargeCash");
                    var side = row.GetDropDownListValueString("cboSide");
                    var mod1 = row.GetDropDownListValueString("ddlMod1");
                    var mod2 = row.GetDropDownListValueString("ddlMod2");
                    var mod3 = row.GetDropDownListValueString("ddlMod3");
                    var IsCashCollection = row.GetCheckBoxBool("chkDMEDeposit");
                    var WholesaleCost = row.GetBoundColumnDecimal("WholesaleCost");
                    var DMEDeposit = row.GetRadNumericTextBoxDecimal("txtDMEDeposit");
                    var DispenseDate = row.GetRadDatePickerDateTime("txtDateDispensed");
                    var icd9Code = row.GetRadComboBoxText("cboICD9");
                    var hcpcsString = row.GetBoundColumnString("HCPCs").Replace("&nbsp;", "");
                    var code = row.GetBoundColumnString("Code");
                    var isCustomFabricated = row.GetCheckBoxBool("chkCustomFabricated");
                    var queuedReason = row.GetColumnSelectedValue("ddlQueuedReason");
                    var abnType = row.GetColumnSelectedValue("ddlAbnType");
                    var preAuth = row.GetCheckBoxBool("chkIsPreAuthorization");
                    int? FitterID = row.GetDropDownListValueInt32("cboFitters");
                    #endregion

                    #region capture product inventory IDs

                    if (row.OwnerTableView.ParentItem.Selected)
                    {
                        selectedProductInventoryIDs.Add(ProductInventoryID);
                    }

                    #endregion

                    #region save any new ICD9 codes

                    SaveNewICD9(icd9Code, hcpcsString);

                    #endregion

                    #region split code logic

                    var splitCodeOption = row.GetDropDownListValueString("cboSplitCode");
                    bool? isCustomFit;

                    if (splitCodeOption.Equals("Custom Fit"))
                        isCustomFit = true;
                    else if (splitCodeOption.Equals("Off the Shelf"))
                        isCustomFit = false;
                    else
                        isCustomFit = false; //This was null. Is false to support 4.2 handheld app compatibility 

                    List<Tuple<int, string>> customFitProcedureResponses = GetCustomFitProcedureListForRow(row);

                    #endregion

                    #region save changes to dispense queue

                    var savedDispenseQueueID = 0;
                    if ((ProductInventoryID > 0) && (PracticeCatalogProductID > 0))
                    {
                        var isABNForm = abnType == "Medicare" ? true : false;
                        savedDispenseQueueID =
                            DAL.PracticeLocation.Dispensement.AddUpdateDispensement
                            (
                                DispenseQueueID: dispenseQueueID,
                                PracticeLocationID: practiceLocationID,
                                InventoryID: ProductInventoryID,
                                PatientCode: txtPatientCode,
                                PhysicianID: PhysicianID,
                                ICD9_Code: icd9Code,
                                IsMedicare: isMedicare,
                                ABNForm: isABNForm,
                                IsCashCollection: IsCashCollection,
                                Side: side,
                                DMEDeposit: DMEDeposit,
                                AbnType: abnType,
                                BregVisionOrderID: bregVisionOrderID,
                                Quantity: Quantity,
                                isCustomFit: isCustomFit,
                                mod1: mod1,
                                mod2: mod2,
                                mod3: mod3,
                                DispenseDate: DispenseDate,
                                CreatedUser: 1,
                                CreatedDate: DateTime.Now,
                                QueuedReason: queuedReason,
                                patientEmail: patientEmail,
                                dispenseSignatureID: 0,
                                isDeleted: false,
                                patientFirstName: patientFirstName,
                                patientLastName: patientLastName,
                                note: note,
                                payerID: payerID,
                                FitterID: FitterID,
                                isCustomFabricated: isCustomFabricated,
                                customFitProcedureResponses: customFitProcedureResponses,
                                preAuthorized: preAuth,
                                IsCalledFromV1Endpoint: false
                            );
                    }

                    #endregion

                    #region capture list of selected dispense queue IDs for possible dispensement

                    if (row.OwnerTableView.ParentItem.Selected && (savedDispenseQueueID > 0))
                    {
                        selectedDispenseQueueIDs.Add(savedDispenseQueueID);
                    }

                    #endregion
                }
            }
        }

        return selectedProductInventoryIDs;
    }

    private void SaveNewICD9(string UserICD9Code, string HCPCsString)
    {
      if (string.IsNullOrEmpty(UserICD9Code.Trim()) || string.IsNullOrEmpty(HCPCsString.Trim()))
      {
        return;
      }
      //parse the HCPCsString from a comma separated string to an array usable by LINQ
      var hcpcsArray = HCPCsString.Replace(" ", "").Split(',');

      //Load the IDC9's 
      _ICD9_Codes = DAL.PracticeLocation.Dispensement.GetDispenseQueueICD9s(practiceLocationID);

      //Find the the ICD9 items that match these HCPCs
      var hcpcsForThisRow = from icd in _ICD9_Codes
        where hcpcsArray.Contains(icd.HCPCs)
        select icd;

      //Se if the user ICD9 Code(can be picked from the list Items in the comboBox or typed in) is in the list
      var icd9Exists = from existing_icd9 in hcpcsForThisRow
        where existing_icd9.ICD9Code == UserICD9Code
        select existing_icd9;
      if (icd9Exists.Count() < 1)
      {
        //user typed a new ICD9 code that wasn't in the dropdown
        foreach (var hcpc in hcpcsArray)
        {
          //Add the ICD9 the user typed in to both the ICD9 and Practice_ICD9 tables
          if (string.IsNullOrEmpty(hcpc.Trim()) == false && string.IsNullOrEmpty(UserICD9Code.Trim()) == false)
          {
            var icd9ID = DAL.PracticeLocation.Dispensement.AddPracticeICD9(practiceID, hcpc.Trim(), UserICD9Code.Trim());
            DAL.PracticeLocation.Dispensement.InsertPracticeICD9(icd9ID, practiceID);
          }
        }
      }
    }

    private string GetPopupReceiptURL(IEnumerable<Int32> selectedDispenseIDs, bool isFaxResend = false)
    {
      // join the dispense ids with a pipe ex. 32|23|2
      var strDispenseIDs =
        String.Join(
          "|",
          selectedDispenseIDs
            .ToArray()
            .Select(x => x.ToString())
            .ToArray());

      // generate URL of the dispense receipt pop-up
      if (selectedDispenseIDs.Count() > 0)
      {
        var s =
          String.Format(
            "Authentication/DispenseReceipt.aspx?DispenseID={0}&PLID={1}&PLName={2}&PracticeName={3}",
            Server.UrlEncode(strDispenseIDs),
            practiceLocationID,
            Server.UrlEncode(practiceLocationName),
            Server.UrlEncode(practiceName));

        if (isFaxResend)
          s += "&faxresend=1";

        return s;
      }
      else
      {
        return null;
      }
    }

    private Boolean PopupSelectedReceipt(GridTableView gridTableView)
    {
      var selectedDataItems = from GridDataItem receiptHistoryItem in (gridTableView.Items as IEnumerable)
        where receiptHistoryItem.Selected == true
        select receiptHistoryItem.GetColumnInt32("DispenseID");

      var url = GetPopupReceiptURL(selectedDataItems);
      if (url != null)
      {
        ResponseHelper.Redirect(url, "_blank", "");
        //RadAjaxManager1.EnableAJAX = true;
        return true;
      }
      else
        return false;
    }

    //protected void txtRecordsDisplayed_TextChanged(object sender, EventArgs e)
    //{
    //    Session["RecordsDisplayed"] = txtRecordsDisplayed.Text;
    //    grdInventory.PageSize = Convert.ToInt16(Session["RecordsDisplayed"].ToString());
    //    ////////grdSummary.PageSize = Convert.ToInt16(Session["RecordsDisplayed"].ToString());
    //    grdReceiptHistory.PageSize = Convert.ToInt16(Session["RecordsDisplayed"].ToString());
    //    //next code refreshes grid for particular page that is displayed
    //    switch (rmpMain.SelectedIndex)
    //    {
    //        case 0:
    //            {
    //                grdInventory.Rebind();
    //                break;
    //            }
    //        case 1:
    //            {
    //                grdCustomBrace.Rebind();
    //                break;
    //            }
    //        case 2:
    //            {
    //                grdReceiptHistory.Rebind();
    //                break;
    //            }
    //    }
    //}

    #region GridView ItemDataBound

    protected void grdInventory_ItemDataBound(object source, GridItemEventArgs e)
    {
      if ((e.Item is GridDataItem) && (e.Item.OwnerTableView.Name == "gvwSupplier"))
      {
        try
        {
          var gridDataItem = (DataRowView) e.Item.DataItem;

          // var ddlAbnType = (DropDownList)e.Item.FindControl("ddlAbnType");
          var IsMedicare1 = (CheckBox) e.Item.FindControl("chkIsMedicare");
          var abnNeeded = Convert.ToBoolean(gridDataItem["ABN_Needed"]);

          }
        catch
        {
        }
      }
    }

    protected void grdCustomBrace_ItemDataBound(object sender, GridItemEventArgs e)
    {
      if (e.Item is GridDataItem)
      {
        var dataItem = (GridDataItem) e.Item;

        if (e.Item.OwnerTableView.Name == "mtvCustomBraceOutstanding_ParentLevel")
        {
          var cboPhysician = e.Item.FindControl("cboPhysicians") as RadComboBox;
            //dataItem.FindControl("ddlPhysician") as DropDownList;
          if (cboPhysician != null)
          {
            cboPhysician.DataSource = _dsPhysicians;
            cboPhysician.DataValueField = "PhysicianId";
            cboPhysician.DataTextField = "PhysicianName";
            cboPhysician.DataBind();

            var gridDataItem = e.Item.DataItem as DataRowView;
            if (gridDataItem != null)
            {
              var physicianID = gridDataItem["PhysicianID"].ToString();
              if (!string.IsNullOrEmpty(physicianID))
                cboPhysician.SelectedValue = physicianID;
              else
                cboPhysician.SelectedIndex = 0;
            }
          }

          var cboPayers = e.Item.FindControl("cboPayers") as RadComboBox;
            //dataItem.FindControl("ddlPhysician") as DropDownList;
          if (cboPayers != null)
          {
            cboPayers.DataSource = _dsPayers;
            cboPayers.DataValueField = "PayerId";
            cboPayers.DataTextField = "Name";
            cboPayers.DataBind();

            var gridDataItem = e.Item.DataItem as DataRowView;
            if (gridDataItem != null)
            {
              var payerID = gridDataItem["PracticePayerID"].ToString();
              if (!string.IsNullOrEmpty(payerID))
              {
                cboPayers.SelectedValue = payerID;
              }
            }
          }
        }

        if (e.Item.OwnerTableView.Name == "mtvCustomBraceOutstanding_Detail1Level")
        {
          var cboSide = e.Item.FindControl("cboSide") as RadComboBox;
            //dataItem.FindControl("ddlPhysician") as DropDownList;
          if (cboSide != null)
          {
            var gridDataItem = e.Item.DataItem as DataRowView;
            if (gridDataItem != null)
            {
              string side = gridDataItem["side"].ToString();
              if (!string.IsNullOrEmpty(side))
              {
                cboSide.SelectedValue = side;
              }
            }
          }
        }
      }
    }

    #endregion

    #region Dispense Queue RadGrid events

    protected void grdSummary2_NeedDataSource(object s, GridNeedDataSourceEventArgs e)
    {
      //Load the IDC9's every time we reload the summary datasource
      _ICD9_Codes = DAL.PracticeLocation.Dispensement.GetDispenseQueueICD9s(practiceLocationID);

      // get dispense queue sorting pref
      var isDispenseQueueSortDescending = false;
      {
        var m = this.Master as BregVision.MasterPages.SiteMaster;
        if (m != null)
          isDispenseQueueSortDescending = m.DispenseQueueSortDescending;
      }

      // get dispense queue data and bind
        grdSummary2.DataSource = DAL.PracticeLocation.Dispensement.GetDispenseQueue(practiceLocationID,
        isDispenseQueueSortDescending);
            // emit javascript to add spinners to the dispense queue grid
            //RadAjaxManager1.ResponseScripts.Add(@"$(function() { $('.QuantitySpinner').spinner({ min: 0, max: 99 }); $('.CurrencySpinner').spinner({ currency: '$', min: 0, max: 9999 }); });");
        }

        private string _GetQueueIconImageUrl(bool doesProductRequirePreAuth, bool isCustomFitEnabledForPractice, bool isProductASplitCodeProduct, bool isProductSetAsAlwaysOffTheShelf, bool isProductACustomBrace, bool isItemFlaggedAsMedicare, bool isItemSelectedAsOffTheShelf)
        {
            /* Note(Henry): This block is mimic'd in the _GetQueueIconImageUrl JavaScript method in Dispense.aspx,
                make sure any changes to this logic are updated there as well... for now... */

            var _isPreAuth = doesProductRequirePreAuth && !isItemFlaggedAsMedicare;
            var _isCustomFit = isCustomFitEnabledForPractice && isProductASplitCodeProduct && !(isProductSetAsAlwaysOffTheShelf || isItemSelectedAsOffTheShelf);
            var _isCustomFab = isProductACustomBrace;

            if (_isCustomFab == true)
            {
                return
                    _isPreAuth
                    ? "~/App_Themes/Breg/images/Common/pre-auth-custom-fab.png"
                    : "~/App_Themes/Breg/images/Common/custom-fab.png";
            }

            if (_isCustomFit == true)
            {
                return
                    _isPreAuth
                    ? "~/App_Themes/Breg/images/Common/pre-auth-custom-fit.png"
                    : "~/App_Themes/Breg/images/Common/custom-fit.png";
            }

            return
                _isPreAuth
                ? "~/App_Themes/Breg/images/Common/Pre-Auth.png"
                : "";
        }

    protected void grdSummary2_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if ((e.Item is GridDataItem) && e.Item.OwnerTableView.Name != "ChildGrid")
        {
            /* Note(Henry): This block is mimic'd in the ChangeChecked JavaScript method in Dispense.aspx,
                make sure any changes to this logic are updated there as well... for now... */

            var parentDq = e.Item.DataItem as ClassLibrary.DAL.DispenseQueueInterface;
            var queueIcon = (Image) e.Item.FindControl("QueueIcon");
            var lblPreAuthorization = (Label) e.Item.FindControl("lblPreAuthorization");

            // handle pre-authorization label default visibility
            lblPreAuthorization.Visible = parentDq.IsPreAuthorization;
            // override pre-auth via CSS if medicare is selected
            if (parentDq.IsMedicare)
                lblPreAuthorization.CssClass += " display-none";

            // obtain default queue icon URL
            var isItemSelectedAsOffTheShelf = parentDq.IsCustomFit.HasValue && (parentDq.IsCustomFit.Value == false);
            queueIcon.ImageUrl = _GetQueueIconImageUrl(parentDq.IsPreAuthorization, parentDq.CustomFitEnabled, parentDq.IsSplitCode, parentDq.IsAlwaysOffTheShelf, parentDq.IsCustomBrace.HasValue && parentDq.IsCustomBrace.Value, parentDq.IsMedicare, isItemSelectedAsOffTheShelf);
                // hide queueIcon if there is no URL
                if (queueIcon.ImageUrl == "")
                    queueIcon.CssClass += " display-none";
        }

          if (!(e.Item is GridDataItem) || e.Item.OwnerTableView.Name != "ChildGrid")
          {
              return;
          }

          var dq = e.Item.DataItem as ClassLibrary.DAL.DispenseQueueInterface;
          if (dq != null)
          {
              var abn_Needed = dq.ABN_Needed;
              var isMedicare = dq.IsMedicare;
              var abnForm = dq.ABNForm;
              var hdnDMEDeposit = dq.hdnDMEDeposit;
              var originalBillingCharge = dq.hdnBillingCharge;


              var queueIcon = (Image) e.Item.OwnerTableView.ParentItem.FindControl("QueueIcon");
              var lblPreAuthorization = (Label) e.Item.OwnerTableView.ParentItem.FindControl("lblPreAuthorization");


              var dmeDeposit = (RadNumericTextBox) e.Item.FindControl("txtDMEDeposit");
              var lblBillingCharge = (Label) e.Item.FindControl("lblBillingCharge");
              var ddlAbnType = (DropDownList) e.Item.FindControl("ddlAbnType");
              var IsMedicare = e.Item.GetCheckBox("chkIsMedicare");
              var cashOverride = e.Item.GetCheckBox("chkDMEDeposit");
              var btnSelectPatient = e.Item.GetControl<Button>("btnSelectPatient");
              var cboICD9 = e.Item.FindControl("cboICD9") as RadComboBox;
              var ddlside = (DropDownList) e.Item.FindControl("cboSide");
              var ddlSideOld = (TextBox) e.Item.FindControl("cboSideOldValue");
              var txtQuantityDispensed = (RadNumericTextBox) e.Item.FindControl("txtQuantityDispensed");
              var cboSplitCode = e.Item.FindControl("cboSplitCode") as DropDownList;

              ddlAbnType.Attributes.Add("onclick",
                  $"ChangeChecked('{IsMedicare.ClientID}','{ddlAbnType.ClientID}','{ddlAbnType.ClientID}','{abn_Needed.ToString().ToLower()}','{dmeDeposit.ClientID}','{hdnDMEDeposit.ToString()}','{cashOverride.ClientID}', '{queueIcon.ClientID}','{dq.IsPreAuthorization}','{lblPreAuthorization.ClientID}', '{lblBillingCharge.ClientID}','{originalBillingCharge.ToString("C")}','{dq.CustomFitEnabled}','{dq.IsSplitCode}','{dq.IsAlwaysOffTheShelf}','{dq.IsCustomBrace}', '{cboSplitCode.ClientID}')");

              IsMedicare.Attributes.Add("onclick",
                  $"ChangeChecked('{IsMedicare.ClientID}','{ddlAbnType.ClientID}','{IsMedicare.ClientID}',{abn_Needed.ToString().ToLower()},'{dmeDeposit.ClientID}',{hdnDMEDeposit.ToString()},'{cashOverride.ClientID}','{queueIcon.ClientID}','{dq.IsPreAuthorization}','{lblPreAuthorization.ClientID}', '{lblBillingCharge.ClientID}','{originalBillingCharge.ToString("C")}','{dq.CustomFitEnabled}','{dq.IsSplitCode}','{dq.IsAlwaysOffTheShelf}','{dq.IsCustomBrace}', '{cboSplitCode.ClientID}')");

              cashOverride.Attributes.Add("onclick",
                  $"ChangeChecked('{IsMedicare.ClientID}','{ddlAbnType.ClientID}','{IsMedicare.ClientID}',{abn_Needed.ToString().ToLower()},'{dmeDeposit.ClientID}',{hdnDMEDeposit.ToString()},'{cashOverride.ClientID}','{queueIcon.ClientID}','{dq.IsPreAuthorization}','{lblPreAuthorization.ClientID}' , '{lblBillingCharge.ClientID}' ,'{originalBillingCharge.ToString("C")}','{dq.CustomFitEnabled}','{dq.IsSplitCode}','{dq.IsAlwaysOffTheShelf}','{dq.IsCustomBrace}', '{cboSplitCode.ClientID}')");

              ddlside.Attributes.Add("onchange",
                  $"CalculateQuantityForSide('{ddlside.ClientID}','{ddlSideOld.ClientID}','{txtQuantityDispensed.ClientID}')");
              txtQuantityDispensed.Attributes.Add("onchange",
                  $"CheckQuantityToDispense('{ddlside.ClientID}','{txtQuantityDispensed.ClientID}')");


              BregWcfService.UserContext userContext = BregWcfService.UserContext.Login(User.Identity.Name);
              if (!Roles.IsUserInRole(User.Identity.Name, "BregAdmin")
                  && !Roles.IsUserInRole(User.Identity.Name, "ClinicAdmin")
                  && !Roles.IsUserInRole(User.Identity.Name, "PracticeAdmin"))
              {
                  var mod1 = (DropDownList) e.Item.FindControl("ddlMod1");
                  var mod2 = (DropDownList) e.Item.FindControl("ddlMod2");
                  var mod3 = (DropDownList) e.Item.FindControl("ddlMod3");
                  mod1.Enabled = false;
                  mod2.Enabled = false;
                  mod3.Enabled = false;
              }

              var practice = DAL.Practice.Practice.Select(practiceID);

              try
              {
                  if (practice.EMRIntegrationPullEnabled)
                  {
                      btnSelectPatient.Attributes.Add(
                          "onclick",
                          "return OpenWindow(" + e.Item.OwnerTableView.ParentItem.ItemIndex + ", '" + cboICD9.ClientID +
                          "')");
                  }
                  else
                      btnSelectPatient.Visible = false;
              }
              catch
              {
                  // fall back to not-enabled state
                  btnSelectPatient.Visible = false;
              }

              #region highlight abn checkbox

              if ((abn_Needed == true) && (ddlAbnType != null))
                  ddlAbnType.CssClass = "abnFlag";

              #endregion

              #region select value for physician

              {
                  var cboPhysician = e.Item.GetDropDownList("cboPhysicians");
                  if (cboPhysician != null)
                  {
                      var physicianID = dq.PhysicianID;
                      if (physicianID.HasValue)
                          cboPhysician.SelectedValue = physicianID.Value.ToString();
                  }
              }

              #endregion

              #region select value for

              {
                  var cboPayers = e.Item.GetDropDownList("cboPayers");
                  if (cboPayers != null)
                  {
                      var payerId = dq.PracticePayorID;
                      if (payerId.HasValue)
                          cboPayers.SelectedValue = payerId.Value.ToString();
                  }
              }

              #endregion

              DisplayCustomFitOptions(e, practice, dq);

              #region select value for side

              {
                  var cboSide = e.Item.GetDropDownList("cboSide");
                  if (cboSide != null)
                  {
                      var side = dq.Side;
                      if (!string.IsNullOrEmpty(side))
                          cboSide.SelectedValue = side;
                  }
              }

              #endregion

              #region set value for dispense date (default to today)

              {
                  var txtDateDispensed = e.Item.FindControl("txtDateDispensed") as RadDatePicker;
                  if (txtDateDispensed != null)
                  {
                      var dispenseDate = dq.DispenseDate;
                      if (dispenseDate.HasValue)
                          txtDateDispensed.SelectedDate = dispenseDate;
                      else
                          txtDateDispensed.SelectedDate = DateTime.Now;
                  }
              }

              #endregion

              #region databind icd9 and select stored value (create new if not in list)

              {
                  if (cboICD9 != null)
                  {
                      // create truncated hcpcs array
                      var hcpcsArray = dq.HCPCsString.Replace(" ", "").Split(',');
                      if (hcpcsArray.Length > 0)
                      {
                          for (var i = 0; i < hcpcsArray.Length; i++)
                          {
                              var truncatedHCPC =
                                  (hcpcsArray[i].Length > 4)
                                      ? hcpcsArray[i].Substring(0, 5)
                                      : hcpcsArray[i];

                              if (hcpcsArray[i] != truncatedHCPC)
                                  hcpcsArray[i] = truncatedHCPC;
                          }
                      }

                      // bind icd9 combobox to datasource
                      cboICD9.DataSource =
                          _ICD9_Codes
                              .Where(icd9 => hcpcsArray.Contains(icd9.HCPCs))
                              .Select(icd9 => new {ICD9Code = icd9.ICD9Code.Trim()})
                              .Distinct();
                      cboICD9.DataBind();

                      // select item or create new item and select it
                      var cboICD9_item = cboICD9.Items.FindItemByValue(dq.ICD9Code);
                      if (cboICD9_item == null)
                      {
                          cboICD9_item = new RadComboBoxItem(dq.ICD9Code, dq.ICD9Code);
                          cboICD9.Items.Insert(0, cboICD9_item);
                      }
                      cboICD9_item.Selected = true;
                  }
              }

              #endregion
          }
      }

    private static void DisplayCustomFitOptions(GridItemEventArgs e, Practice practice, DispenseQueueInterface dq)
    {
      var splitCodeTable = (System.Web.UI.WebControls.Table) e.Item.FindControl("splitCodeTable");
      if (!practice.IsCustomFitEnabled)
      {
        if (splitCodeTable != null)
        {
          splitCodeTable.Enabled = false;
          splitCodeTable.Visible = false;
        }
        return;
      }

      #region select visibility of splitCodeTable

      {
        if (splitCodeTable != null)
        {
          bool isSplitCode = Convert.ToBoolean(splitCodeTable.Attributes["data-splitcode"]);
          bool isAlwaysOTS = Convert.ToBoolean(splitCodeTable.Attributes["data-alwaysofftheshelf"]);

          // Hide the third row if the item is Off The Shelf and can't be toggled (between CF and OTS).
          if ((!isSplitCode || (isSplitCode && isAlwaysOTS)) && !IsHcpcCustomFabricated(dq) &&
              !IsCustomBrace(dq))
          {
            splitCodeTable.Enabled = false;
            splitCodeTable.Visible = false;
          }

          // Remove data-attributes used for initialization
          splitCodeTable.Attributes.Remove("data-splitcode");
          splitCodeTable.Attributes.Remove("data-alwaysofftheshelf");
        }
      }

      #endregion

      #region select value for splitcode options

      {
        // Select initial value for splitcode dropdown
        var cboSplitCode = e.Item.GetDropDownList("cboSplitCode");
        bool isSplitCode = Convert.ToBoolean(cboSplitCode.Attributes["data-splitcode"]);
        bool isAlwaysOTS = Convert.ToBoolean(cboSplitCode.Attributes["data-alwaysofftheshelf"]);
        var isSplitCodeAndAlwaysOffTheShelf = (isSplitCode && isAlwaysOTS);

        var chkCustomFabricated = e.Item.GetCheckBox("chkCustomFabricated");


        var isCustomFit = dq.IsCustomFit.HasValue && dq.IsCustomFit == true;
        var isOffTheShelf = dq.IsCustomFit.HasValue && dq.IsCustomFit == false;

        if (isCustomFit)
        {
          cboSplitCode.SelectedValue = "Custom Fit";
          chkCustomFabricated.Checked = IsQueueCustomFabricated(dq);
          // safe to cast, since IsCustomFit and IsCustomFabricated are assigned at the same time
        }
        else if (isOffTheShelf)
        {
          cboSplitCode.SelectedValue = "Off the Shelf";
          chkCustomFabricated.Checked = IsQueueCustomFabricated(dq);
        }
        else
        // First appearance in dispense queue (has a null for IsCustomFit and IsCustomFabricated), so let's preselect some values
        {
          SetSelectedCustomFitOrOffTheShelfOption(cboSplitCode, practice, dq, chkCustomFabricated, isSplitCode,
            isAlwaysOTS);
        }
        var isCustomFabOrCustomBrace = IsHcpcCustomFabricated(dq) || IsCustomBrace(dq);
        // If product is not a split code product, or is but is always off the shelf, or is a custom fabricated item, disable ability to toggle

        if (!isSplitCode || isSplitCodeAndAlwaysOffTheShelf || isCustomFabOrCustomBrace)
        {
          cboSplitCode.Enabled = false;
        }

        if ((isSplitCodeAndAlwaysOffTheShelf || (isSplitCode && isOffTheShelf)) && !isCustomFabOrCustomBrace)
        {
          var lblBillingCharge = (Label) e.Item.FindControl("lblBillingCharge");
          lblBillingCharge.Text = string.Format("{0:C}", dq.BillingChargeOffTheShelf);
        }


        // Remove data-attributes used for initialization
        cboSplitCode.Attributes.Remove("data-splitcode");
        cboSplitCode.Attributes.Remove("data-alwaysofftheshelf");
      }

      #endregion

      #region select value for fitter

      {
        // Select initial value for fitter dropdown
        var cboFitters = e.Item.GetDropDownList("cboFitters");
        if (cboFitters != null)
        {
          var fitterID = dq.FitterID;
          // Dispense Queue product has value assigned already
          if (fitterID.HasValue)
            cboFitters.SelectedValue = fitterID.Value.ToString();
          else
            cboFitters.SelectedIndex = 0;
        }

        // Select initial display/enabled state of the panel housing the fitter dropdown
        var cboFittersPanel = (System.Web.UI.WebControls.Panel) e.Item.FindControl("cboFittersPanel");
        var customFitProcedurePanel = (System.Web.UI.WebControls.Panel) e.Item.FindControl("CustomFitProcedurePanel");
        if (cboFittersPanel != null)
        {
          bool isSplitCode = Convert.ToBoolean(cboFittersPanel.Attributes["data-splitcode"]);

          if (dq.IsCustomFit == true || IsHcpcCustomFabricated(dq) || IsCustomBrace(dq))
               customFitProcedurePanel.Visible = true;          
          else
          // Split Code item.  Either always off the shelf, or custom fit can be selected (after which we toggle visibility on and enable).
              customFitProcedurePanel.Visible = false;

          // Remove data-attributes used for initialization
          cboFittersPanel.Attributes.Remove("data-splitcode");
        }
      }

      #endregion
    }

    private static bool IsCustomBrace(DispenseQueueInterface dq)
    {
      if (dq.IsCustomBrace.HasValue)
      {
        return dq.IsCustomBrace.Value;
      }

      using (var db = VisionDataContext.GetVisionDataContext())
      {
        return db.ProductInventories.Any(inv =>
          inv.ProductInventoryID == dq.ProductInventoryID &&
          inv.PracticeCatalogProduct.MasterCatalogProductID != null &&
          inv.PracticeCatalogProduct.MasterCatalogProduct.IsCustomBrace);
      }
    }

   
    private static void SetSelectedCustomFitOrOffTheShelfOption(DropDownList cboSplitCode, Practice practice,
      DispenseQueueInterface dq, CheckBox chkCustomFabricated, bool isSplitCode, bool isAlwaysOTS)
    {
      cboSplitCode.SelectedValue = "";

      if (practice.IsCustomFitEnabled)
      {
        var isCustomFabricated = IsHcpcCustomFabricated(dq);
        chkCustomFabricated.Checked = isCustomFabricated;

        if (isCustomFabricated || IsCustomBrace(dq))
        {
          cboSplitCode.SelectedValue = "Custom Fit";
        }
        else
        {
          if (!isSplitCode || (isSplitCode && isAlwaysOTS))
          {
            cboSplitCode.SelectedValue = "Off the Shelf";
          }
        }
      }
    }

    private static bool IsQueueCustomFabricated(DispenseQueueInterface dq)
    {
      return dq.IsCustomFabricated.HasValue && (bool) dq.IsCustomFabricated;
    }

    private static bool IsHcpcCustomFabricated(DispenseQueueInterface dq)
    {
      using (var visionDataContext = VisionDataContext.GetVisionDataContext())
      {
        // First check CF hcpcs...
        if (HcpcsHelper.IsHcpcCustomFabricated(dq.HCPCsString, visionDataContext))
        {
          return true;
        }
        // then check OTS hcpcs
        else
        {
          return HcpcsHelper.IsHcpcCustomFabricated(dq.OTSHCPCsString, visionDataContext);
        }
      }
    }

    protected void grdSummary2_DeleteCommand(object sender, GridCommandEventArgs e)
    {
      if (e.CommandName.ToLower() == "delete")
      {
        DAL.PracticeLocation.Dispensement.DeleteDispenseQueueItem
          (
                Convert.ToInt32(
                ((GridDataItem)e.Item).GetBoundColumnInt32("DispenseQueueID")
              )
          );

        e.Canceled = true; //http://www.telerik.com/forums/heirarchy-delete-error
        grdSummary2.Rebind();

        }
    }


    protected void ctlDispenseFavorites_AddToDispensementClicked(object sender, EventArgs e)
    {
      grdSummary2.Rebind();
    }

    #endregion

    protected void cboSplitCode_OnSelectedIndexChanged(object source, EventArgs e)
    {
      DropDownList input = (System.Web.UI.WebControls.DropDownList) source;
      string value = input.Text;

      Panel fitterDropdownPanel = (System.Web.UI.WebControls.Panel) input.NamingContainer.FindControl("cboFittersPanel");
      Panel customFitProcedurePanel =
        (System.Web.UI.WebControls.Panel) input.NamingContainer.FindControl("CustomFitProcedurePanel");
      DropDownList fitterDropdown = (DropDownList) fitterDropdownPanel.FindControl("cboFitters");

      string billingchargeAttribute;
      if (value.Equals("Custom Fit"))
      {
        customFitProcedurePanel.Visible = true;
        billingchargeAttribute = "data-billingCharge";
      }
      else
      {        
        customFitProcedurePanel.Visible = false;
        billingchargeAttribute = "data-billingChargeOTS";
      }


      try
      {
        ApplyCustomFitBillingCharge(input, billingchargeAttribute);
      }
      catch
      {
        //TODO:log?
      }
    }

    private static void ApplyCustomFitBillingCharge(DropDownList input, string billingchargeAttribute)
    {
      Label billingChargeLabel = null;
      var sanityCheckIndex = 0;
      var parent = input.Parent;
      while (sanityCheckIndex++ < 10 && !(parent is GridTableCell))
      {
        parent = parent.Parent;
        try
        {
          billingChargeLabel = (Label) input.FindControl("lblBillingCharge");
        }
        catch
        {
          //TODO: log?
        }

        if (sanityCheckIndex >= 10)
        {
          //TODO: log?
        }
      }

      try
      {
        if (billingChargeLabel != null)
        {
          var billingCharge = Decimal.Parse(billingChargeLabel.Attributes[billingchargeAttribute]);
          billingChargeLabel.Text = string.Format("{0:C}", billingCharge);
        }
      }
      catch (Exception)
      {
        //TODO:log?
      }
    }

    protected void btnDownloadReceipt_OnClick(object sender, EventArgs e)
    {
      var selectedDataItems =
        from GridDataItem receiptHistoryItem in (grdReceiptHistory.MasterTableView.Items as IEnumerable)
        where receiptHistoryItem.Selected == true
        select receiptHistoryItem.GetColumnInt32("DispenseID");
      var selectedDataItemsList = selectedDataItems.ToList();
      if (selectedDataItemsList.Count == 0)
      {
        lblReceiptHistory.ForeColor = System.Drawing.Color.Red;
        lblReceiptHistory.Text =
          "No receipt selected. Please choose a receipt and press the 'Download Receipt' button. ";
      }
      else
      {
        var customForms = BregWcfService.Classes.Email.GetActiveCustomDocuments(practiceID);
        List<Byte[]> receipts = new List<Byte[]>();
        foreach (var dispenseId in selectedDataItemsList)
        {
           byte[] CompressedPDF;
          System.Text.StringBuilder html = BregWcfService.Classes.Email.SendPatientEmail(
            new int[] {dispenseId},
            practiceLocationID, null,out CompressedPDF,
            fromVision: true,
            createPDF: false,
            sendEmail: false,
            hidePrintButton: true,
            sendFax: false,
            queueOutboundMessages: false);

                    PDFParameter pdfParameter = new PDFParameter() {html = html.ToString()};

          using (var tempFile = new TempFileManager())
          {
            BregWcfService.Classes.Email.CreatePDF(pdfParameter, tempFile, customForms);
            var tempBytes = tempFile.GetBytes();
            receipts.Add(tempBytes.ToArray());
          }
        }
        var merged = DownloadFileHandlerHelper.MergePdfDocuments(receipts.ToArray());

        var token = Guid.NewGuid().ToString("N");

        string dataSessionKey, dataSessionExpireKey;
        GetFileDownloadSessionKeys(token, out dataSessionKey, out dataSessionExpireKey);

        int expirationSeconds = 900;
        var appSettingsExpirationSeconds = System.Configuration.ConfigurationManager.AppSettings["FileDownloadExpirationSeconds"];
        if (appSettingsExpirationSeconds != null)
            int.TryParse(appSettingsExpirationSeconds, out expirationSeconds);

        Session[dataSessionKey] = merged;
        Session[dataSessionExpireKey] = DateTime.Now.AddSeconds(expirationSeconds).Ticks;

        var url = "Handlers/DownloadFile.ashx?fileName=DispenseReceipt&fileType=pdf&token=" + token;
        ResponseHelper.Redirect(url, "_blank", "");
      }
    }

        public static readonly string FileDownloadSessionKeyPrefix = "FileDownload";
        public static readonly string FileDownloadSessionKeyDataSuffix = "data";
        public static readonly string FileDownloadSessionKeyExpirationSuffix = "expire";

        public static void GetFileDownloadSessionKeys(string token, out string dataSessionKey, out string dataSessionExpireKey)
        {
            dataSessionKey = string.Format("{0}.{1}.{2}", FileDownloadSessionKeyPrefix, token, FileDownloadSessionKeyDataSuffix);
            dataSessionExpireKey = string.Format("{0}.{1}.{2}", FileDownloadSessionKeyPrefix, token, FileDownloadSessionKeyExpirationSuffix);
        }

        public static string GetTokenFromSessionKey(string sessionKey)
        {
            var a = sessionKey.Substring(FileDownloadSessionKeyPrefix.Length + 1);
            var i = a.IndexOf('.');
            a = a.Substring(0, i);
            return a;
        }

        protected void grdSummary2_OnDetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
        {
            var dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            var dispenseQueueId = Convert.ToInt32(dataItem.GetDataKeyValue("DispenseQueueID"));
            e.DetailTableView.DataSource = DAL.PracticeLocation.Dispensement.GetDispenseQueueById(dispenseQueueId);

        }

      protected void grdCustomBrace_OnItemCommand(object sender, GridCommandEventArgs e)
      {
          if (e.CommandName == "RowClick")
          {
              e.Item.Expanded = e.Item.Selected;
          }

          else if (e.CommandName == "ExpandCollapse")
          {
              e.Item.Selected = !e.Item.Expanded;
          }
      }

    
  }
}
