using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.WebControls;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common; 

namespace BregVision
{
    public partial class MCtoPC : System.Web.UI.Page
    {

        protected void InitializeComponent()
        {
         
            //grdMasterCatalog.ItemDataBound += new GridItemEventHandler(grdMasterCatalog_ItemDataBound);
            grdMasterCatalog.PageIndexChanged += new GridPageChangedEventHandler(grdMasterCatalog_PageIndexChanged);
            grdPracticeCatalog.PageIndexChanged += new GridPageChangedEventHandler(grdPracticeCatalog_PageIndexChanged);
            this.Load += new System.EventHandler(this.Page_Load);

        }

        protected void grdMasterCatalog_PageIndexChanged(object source, Telerik.WebControls.GridPageChangedEventArgs e)
        {
            this.grdMasterCatalog.CurrentPageIndex = e.NewPageIndex;
            //LoopHierarchyRecursive(grdInventory.MasterTableView);
            //LoadData();
            //this.grdInventory.DataBind();
        }

        protected void grdPracticeCatalog_PageIndexChanged(object source, Telerik.WebControls.GridPageChangedEventArgs e)
        {
            this.grdPracticeCatalog.CurrentPageIndex = e.NewPageIndex;
            //LoopPracticeCatalog(grdPracticeCatalog.MasterTableView);
            //LoopHierarchyRecursive(grdInventory.MasterTableView);
            //LoadData();
            //this.grdInventory.DataBind();
        }

        public DataTable Grid1Data
        {
            get { return (DataTable)Session["_grid1Data"]; }
            set { Session["_grid1Data"] = value; }
        }
        public DataTable Grid2Data
        {
            get { return (DataTable)Session["_grid2Data"]; }
            set { Session["_grid2Data"] = value; }
        } 

        protected void Page_Load(object sender, EventArgs e)
        {
             if (!IsPostBack) 
            {//'init the datasource for both grids

                //DataTable Grid1Data = GetMasterCatalogSuppliers();
                 //DataTable Grid2Data = GetPracticeCatalogData();
                 //Grid2Data = GetDataTable(("SELECT * FROM Mails Where FolderName = 'Junk E-Mail'"))
            }
            
            //if ((Request.Form["__EVENTTARGET"] == "RowMoved"))
            //{
            //    object[] newRow;
            //    string[] args = Request.Form["__EVENTARGUMENT"].Split(',');
            //    Telerik.WebControls.RadGrid srcGrid = (Telerik.WebControls.RadGrid)this.FindControl(args[0]);
            //    int srcRowIndex = srcGrid.CurrentPageIndex * srcGrid.PageSize + int.Parse(args[1]);
            //    //deleting 
            //    if ((args[2] == "DeleteRow"))
            //    {
            //        if ((srcGrid.ID == grdMasterCatalog.ID))
            //        {
            //             Grid1Data.Rows.RemoveAt(srcRowIndex);
            //        }
            //        else
            //        {
            //            Grid2Data.Rows.RemoveAt(srcRowIndex);
            //        }
            //    }
            //    //moving from grid1 to grid2 
            //    if ((args[2] == grdPracticeCatalog.ID))
            //    {
            //        newRow = Grid1Data.Rows[srcRowIndex].ItemArray;
            //        Grid1Data.Rows.RemoveAt(srcRowIndex);
            //        Grid2Data.Rows.Add(newRow);
            //    }
            //    //moving from grid2 to grid1 
            //    if ((args[2] == grdMasterCatalog.ID))
            //    {
            //        newRow = Grid2Data.Rows[srcRowIndex].ItemArray;
            //        Grid2Data.Rows.RemoveAt(srcRowIndex);
            //        Grid1Data.Rows.Add(newRow);
            //    }
            //    if ((srcGrid.Items.Count == 1))
            //    {
            //        srcGrid.CurrentPageIndex = 0;
            //    }

            //    //rebind grids after modifications 
            //    grdMasterCatalog.DataSource = Grid1Data;
            //    grdMasterCatalog.DataBind();
            //    grdPracticeCatalog.DataSource = Grid2Data;
            //    grdPracticeCatalog.DataBind();
            //} 

        }
    
     //protected void grdMasterCatalog_NeedDataSource(Object Source, GridNeedDataSourceEventArgs e) 
     //{

     //    grdMasterCatalog.DataSource = GetMasterCatalogData();
 
     //}

     public DataSet GetMasterCatalogSuppliers()
     {
         //int rowsAffected = 0;

         Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
         DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetMasterCatalogSuppliers");

         //db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
         DataSet ds = db.ExecuteDataSet(dbCommand);
         return ds;


     }
        public DataSet GetPracticeCatalogSuppliers(Int32 practiceID)
        {
            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetPracticeCatalogSupplierBrands");

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;


        }
        protected void grdMasterCatalog_DetailTableDataBind(object source, Telerik.WebControls.GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
           
            e.DetailTableView.DataSource = GetMasterCatalogDataBySupplier();
          
        }
        
        protected void grdPracticeCatalog_DetailTableDataBind(object source, Telerik.WebControls.GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;

            e.DetailTableView.DataSource = GetPracticeCatalogDataBySupplier(Convert.ToInt32(Session["practiceid"]));

        }
     public DataSet GetMasterCatalogDataBySupplier()
     {

         Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
         DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetMasterCatalogProductInfo");

         //db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);

         DataSet ds = db.ExecuteDataSet(dbCommand);
         return ds;

     }

        public DataSet GetPracticeCatalogDataBySupplier(Int32 practiceID)
        {
            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetPracticeCatalogProductInfoMCTOPC");

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);

            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;

        }

        protected void grdMasterCatalog_NeedDataSource(object source, Telerik.WebControls.GridNeedDataSourceEventArgs e)
         {
             if (!e.IsFromDetailTable)
             {
                 grdMasterCatalog.DataSource = GetMasterCatalogSuppliers();
             }
         }

        protected void grdMasterCatalog_PreRender(object sender, EventArgs e)
         {

             if (!IsPostBack)
             {
                 foreach (GridDataItem item in grdMasterCatalog.MasterTableView.Items)
                 {
                     if (item.ItemIndex == 0)
                     {
                         item.Expanded = true;
                         //item.ChildItem.NestedTableViews[0].Items[1].Expanded = true;
                     }

                 }
             }
         }


       protected void grdPracticeCatalog_NeedDataSource(object source, Telerik.WebControls.GridNeedDataSourceEventArgs e)
         {
             if (!e.IsFromDetailTable)
             {
                 grdPracticeCatalog.DataSource = GetPracticeCatalogSuppliers(Convert.ToInt32(Session["practiceid"]));
                 //grdPracticeCatalog.r
             }
         }

       protected void grdPracticeCatalog_PreRender(object sender, EventArgs e)
         {

             if (!IsPostBack)
             {
                 foreach (GridDataItem item in grdPracticeCatalog.MasterTableView.Items)
                 {
                     if (item.ItemIndex == 0)
                     {
                         item.Expanded = true;
                         //item.ChildItem.NestedTableViews[0].Items[1].Expanded = true;
                     }

                 }
             }
         }


        private DataTable GetMasterCatalogData()
        {
            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetMasterCatalogProductInfo");

            DataTable dt = db.ExecuteDataSet(dbCommand).Tables[0];
            return dt;
        }

        //protected void grdPracticeCatalog_NeedDataSource(Object Source, GridNeedDataSourceEventArgs e)
        //{

        //    grdPracticeCatalog.DataSource = GetPracticeCatalogData();
 

        //}
       
        private DataTable GetPracticeCatalogData()
        {
            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetPracticeCatalogProductInfo");
            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, Session["PracticeID"]);
            DataTable dt = db.ExecuteDataSet(dbCommand).Tables[0];
            return dt;
        }
        int intPCItems = 0;
        private void LoopHierarchyRecursive(GridTableView gridTableView)
        {

          foreach (GridNestedViewItem nestedViewItem in gridTableView.GetItems(GridItemType.NestedView))
            //foreach (GridNestedViewItem nestedViewItem in gridTableView.GetItems(GridItemType.SelectedItem))
            {
                if (nestedViewItem.NestedTableViews.Length > 0)
                {
                    //foreach (GridDataItem gridDataItem in nestedViewItem.)
                    foreach (GridDataItem gridDataItem in nestedViewItem.NestedTableViews[0].Items)
                    {

                        //foreach (GridDataItem item in RadGrid1.SelectedItems) 
                        if (findSelectColumn(gridDataItem))
                        {

                            //Add to 


                            Int32 practiceCatalogProductID = UpdatePracticeCatalog(Convert.ToInt32(Session["PracticeID"]), findMasterCatalogProductID(gridDataItem), 1);
                            intPCItems += 1;
                            ////lblStatus.Text = "Adding items to the cart.";
                            //Int16 ReOrderQuantity = findReorderQuantity(gridDataItem);
                            ////Check to make sure reorder quantity greater than 0, of not, no need to update cart
                            //if (ReOrderQuantity > 0)
                            //{
                            //    ShoppingCartItemID = UpdateGrdInventoryData(Convert.ToInt16(Session["PracticeLocationID"]), findPracticeCatalogProductID(gridDataItem), 1, ReOrderQuantity);
                            //    productsAdded += ReOrderQuantity;
                            //}

                        }
                    }

                }
                grdPracticeCatalog.Rebind();
                string strPCStatus = String.Concat("Item(s) added to Practice Catalog: ",intPCItems.ToString());
                lblPCStatus.Text = strPCStatus;
            }
            
         }
        
     
        private Int32 UpdatePracticeCatalog(Int32 PracticeID,Int32 MasterCatalogProductID, Int32 UserID)
        {
       // int rowsAffected = 0;

        Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
        DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeCatalogProduct_Insert_By_MasterCatalogProductID");

        db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, PracticeID);
        db.AddInParameter(dbCommand, "MasterCatalogProductID", DbType.Int32, MasterCatalogProductID);
        db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
        db.AddOutParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, 4);

        int SQLReturn = db.ExecuteNonQuery(dbCommand);
        //return ds;
        Int32 return_value = Convert.ToInt32(db.GetParameterValue(dbCommand, "PracticeCatalogProductID"));
        return return_value;
        }
        
        private Boolean findSelectColumn(GridDataItem gridDataItem)
        {
            TableCell cell = gridDataItem["ClientSelectColumn"];
            CheckBox checkBox = (CheckBox)cell.Controls[0];
            if (checkBox.Checked)
                return true;
            else
            {
                return false;
            }


        }

        private Int16 findPracticeCatalogProductID(GridDataItem gridDataItem)
        {

            string PracticeCatalogProductID = (string)gridDataItem["PracticeCatalogProductID"].Text;
            if (PracticeCatalogProductID != null)
            {
                return Int16.Parse(PracticeCatalogProductID);

            }
            else
            {
                return 0;
            }

        }

        private Int16 findMasterCatalogProductID(GridDataItem gridDataItem)
        {

            string PracticeID = (string)gridDataItem["Mastercatalogproductid"].Text;
            if (PracticeID != null)
            {
                return Int16.Parse(PracticeID);
               
            }
            else
            {
                return 0;
            }

        }

        protected void btnMoveToPC_Click(object sender, EventArgs e)
        {
            LoopHierarchyRecursive(grdMasterCatalog.MasterTableView);
        }

        private void LoopPracticeCatalog(GridTableView gridTableView)
        {

            foreach (GridNestedViewItem nestedViewItem in gridTableView.GetItems(GridItemType.NestedView))
            //foreach (GridNestedViewItem nestedViewItem in gridTableView.GetItems(GridItemType.SelectedItem))
            {
                if (nestedViewItem.NestedTableViews.Length > 0)
                {
                    //foreach (GridDataItem gridDataItem in nestedViewItem.)
                    foreach (GridDataItem gridDataItem in nestedViewItem.NestedTableViews[0].Items)
                    {

                        //foreach (GridDataItem item in RadGrid1.SelectedItems) 
                        //if (findSelectColumn(gridDataItem))
                        //{

                            //Add to 

                            Int32 return_Value = UpdatePricing_PracticeCatalog(findPracticeCatalogProductID(gridDataItem), findPricing(gridDataItem, "txtWholesaleCost"), findPricing(gridDataItem, "txtBillingCharge"), findPricing(gridDataItem, "txtBillingChargeCash"), findPricing(gridDataItem, "txtDMEDeposit"));
                            //Int32 practiceCatalogProductID = UpdatePracticeCatalog(Convert.ToInt32(Session["PracticeID"]), findPracticeCatalogProductID(gridDataItem), 1);
                            intPCItems += 1;
                            ////lblStatus.Text = "Adding items to the cart.";
                            //Int16 ReOrderQuantity = findReorderQuantity(gridDataItem);
                            ////Check to make sure reorder quantity greater than 0, of not, no need to update cart
                            
                            //decimal test = findPricing(gridDataItem, "txtWholesaleCost");
                            
                        
                            //if (ReOrderQuantity > 0)
                            //{
                            //    ShoppingCartItemID = UpdateGrdInventoryData(Convert.ToInt16(Session["PracticeLocationID"]), findPracticeCatalogProductID(gridDataItem), 1, ReOrderQuantity);
                            //    productsAdded += ReOrderQuantity;
                            //}

                        //}
                    }

                }
                grdPracticeCatalog.Rebind();
                string strPCStatus = String.Concat("Number of products updated: ", intPCItems.ToString());
                lblPCStatus.Text = strPCStatus;
            }

        }

        private decimal findPricing(GridDataItem gridDataItem,string txtBox)
        {
            RadNumericTextBox nTextBox = (RadNumericTextBox)gridDataItem.FindControl(txtBox);
            //CheckBox checkBox = (CheckBox)gridDataItem.FindControl("chkAddtoCart");

            if (nTextBox != null && nTextBox.Text.Length>0)
            {
                return decimal.Parse(nTextBox.Value.ToString());
                //Response.Write(gridDataItem.ItemIndex.ToString() + " is: " + nTextBox.Value.ToString() + "<br />");
            }
            else
            {
                return 0;
                //throw new Exception("Cannot find object with ID 'CheckBox1' in item " + gridDataItem.ItemIndex.ToString());
            }
        }

        protected void btnUpdatePC_Click(object sender, EventArgs e)
        {
            LoopPracticeCatalog(grdPracticeCatalog.MasterTableView);
        }

        private Int32 UpdatePricing_PracticeCatalog(Int32 PracticeCatalogProductID, 
            decimal WholesSaleCost, decimal BillingCharge, decimal BillingChargeCash, decimal DMEDeposit)

        {
            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_UpdatePricing_PracticeCatalogProduct");

            db.AddInParameter(dbCommand, "practiceCatalogProductID", DbType.Int32, PracticeCatalogProductID);
            db.AddInParameter(dbCommand, "WholeSaleCost", DbType.Decimal, WholesSaleCost);
            db.AddInParameter(dbCommand, "BillingCharge", DbType.Decimal, BillingCharge);
            db.AddInParameter(dbCommand, "BillingChargeCash", DbType.Decimal, BillingChargeCash);
            db.AddInParameter(dbCommand, "DMEDeposit", DbType.Decimal, DMEDeposit);
            //db.AddOutParameter(dbCommand, "ShoppingCartIDReturn", DbType.Int32, rowsAffected);

            int return_value = db.ExecuteNonQuery(dbCommand);
            //return ds;
            //Int32 return_value = Convert.ToInt32(db.GetParameterValue(dbCommand, "ShoppingCartIDReturn"));
            return return_value;
        }

    }



}
