﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HelpTestPage.aspx.cs" Inherits="BregVision.HelpTestPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Help</title>
    
    <script type="text/javascript" src="~/Include/Silverlight.js" /> 
    
    <script type="text/javascript">
        function onSilverlightError(sender, args) {
            var appSource = "";
            if (sender != null && sender != 0) {
                appSource = sender.getHost().Source;
            }

            var errorType = args.ErrorType;
            var iErrorCode = args.ErrorCode;

            if (errorType == "ImageError" || errorType == "MediaError") {
                return;
            }

            var errMsg = "Unhandled Error in Silverlight Application " + appSource + "\n";

            errMsg += "Code: " + iErrorCode + "    \n";
            errMsg += "Category: " + errorType + "       \n";
            errMsg += "Message: " + args.ErrorMessage + "     \n";

            if (errorType == "ParserError") {
                errMsg += "File: " + args.xamlFile + "     \n";
                errMsg += "Line: " + args.lineNumber + "     \n";
                errMsg += "Position: " + args.charPosition + "     \n";
            }
            else if (errorType == "RuntimeError") {
                if (args.lineNumber != 0) {
                    errMsg += "Line: " + args.lineNumber + "     \n";
                    errMsg += "Position: " + args.charPosition + "     \n";
                }
                errMsg += "MethodName: " + args.methodName + "     \n";
            }

            throw new Error(errMsg);
        }
        
    </script>
    
    <style type="text/css">
    html, body {
	    height: 100%;
	    overflow: hidden;
    }
    body {
	    padding: 0;
	    margin: 0;
	    
    }
    
    </style>
    
    
</head>
<body>
    <form id="form1" runat="server" style="height:100%">
    <script type="text/javascript" src="Include/HelpSystem.js" />
    <asp:ScriptManager ID="ScriptManager" runat="server" />
    
    <div style="height:100%;width:auto">
    <div>
        <input type="button" value="Load Help Context 1" onclick="LoadHelpContext(1);" />
        <input type="button" value="Load Help Context 2" onclick="LoadHelpContext(2);" />
        <input type="button" value="Load Help Context 3" onclick="LoadHelpContext(3);" />
    </div>
    <div>
        <input type="button" value="Select Help Context 66" onclick="SelectHelpContext(66);" />
        <input type="button" value="Select Help Context 10" onclick="SelectHelpContext(10);" />
        <input type="button" value="Select Help Context 1000" onclick="SelectHelpContext(1000);" />
    </div>
    <div>
        <input type="button" value="Search for Text Video" onclick="SearchForHelpArticlesAndHelpMeida('video');" />
    </div>
    
    <div id="silverlightControlHost" style="height:600px;width:800px">
        <object id="HelpControl" data="data:application/x-silverlight-2," type="application/x-silverlight-2" style="height:100%;width:100%">
          <param name="windowless" value="true"/>
	      <param name="source" value="ClientBin/HelpSystem.xap"/>
	      <param name="onError" value="onSilverlightError" />
	      <param name="background" value="white" />
	      <param name="minRuntimeVersion" value="3.0.40624.0" />
	      <param name="autoUpgrade" value="true" />
	      <param name="initParams" value="<%= string.Format("UserRole={0}", Session["UserID"])%>,IsFullMode=true,<%= string.Format("AdminNode={0}", System.Configuration.ConfigurationManager.AppSettings["HelpEntityAdminTopNode"])%>,<%= string.Format("UserNode={0}", System.Configuration.ConfigurationManager.AppSettings["HelpEntityUserTopNode"])%>" />
	      <a href="http://go.microsoft.com/fwlink/?LinkID=149156&v=3.0.40624.0" style="text-decoration:none">
		      <img src="http://go.microsoft.com/fwlink/?LinkId=108181" alt="Get Microsoft Silverlight" style="border-style:none"/>
	      </a>
        </object><iframe id="_sl_historyFrame" style="visibility:hidden;height:0px;width:0px;border:0px"></iframe>
    </div>
    </div>
    </form>
</body>

</html>

