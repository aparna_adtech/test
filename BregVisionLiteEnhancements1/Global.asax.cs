﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using BregVision.ErrHandler;
using System.Text;
using System.Security.Principal;
using System.Configuration;

namespace BregVision
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (!(Request.ServerVariables["HTTP_X_BLUECOAT_VIA"] == null)  && (Request.RequestType.ToUpper() != "POST"))
            {
                HttpContext.Current.RewritePath(Request.Path + "?" + Request.QueryString.ToString().Replace(Server.UrlEncode("amp;"),  "&"), true);
            }

            if (HttpContext.Current.Request.IsSecureConnection.Equals(false) && UseSSL() == true &&
				Request.ServerVariables["HTTP_HOST"] != null && Request.ServerVariables["HTTP_HOST"] != string.Empty)
            {
                Response.Redirect("https://" + Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.RawUrl);
            }
        }

        public static bool UseSSL()
        {
            bool useSsl = false;

            string sslSetting = ConfigurationManager.AppSettings["UseSSL"];
            bool.TryParse(sslSetting, out useSsl);

            return useSsl;
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            try
            {

                HttpCookie authenCookie = Context.Request.Cookies.Get(FormsAuthentication.FormsCookieName);

                if (authenCookie == null) return;

                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authenCookie.Value);

                FormsIdentity id = new FormsIdentity(ticket);

                string[] astrRoles = ticket.UserData.Split(new char[] { ',' });

                GenericPrincipal principal = new GenericPrincipal(id, astrRoles);

                Context.User = principal;

            }

            catch (Exception ex)
            {

                System.IO.StreamWriter wr = new System.IO.StreamWriter(Context.Request.MapPath("log.txt"));

                wr.WriteLine(ex.Message);

                wr.Close();

            }

        }


        protected void Application_Error(object sender, EventArgs e)
        {

            // At this point we have information about the error
            HttpContext ctx = HttpContext.Current;

            try
            {
#if(!DEBUG) 
                Exception ex = ctx.Server.GetLastError();

                //if (!(ctx.Request.Url.ToString().Contains(".axd")))
                //{
                    ErrHandler.VisionErrHandler.RaiseError(ex);
                //}


                // --------------------------------------------------
                // To let the page finish running we clear the error
                // --------------------------------------------------
      
                ctx.Server.ClearError();

                Response.Redirect("~/ErrorPage.aspx", true);
#endif
            }
            catch
            {
                ctx.Server.ClearError();
                Response.Redirect("~/ErrorPage.aspx", true);
            }

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}