if(typeof window.RadControlsNamespace=="undefined"){
window.RadControlsNamespace={};
}
if(typeof (window.RadControlsNamespace.DomEventMixin)=="undefined"||typeof (window.RadControlsNamespace.DomEventMixin.Version)==null||window.RadControlsNamespace.DomEventMixin.Version<3){
RadControlsNamespace.DomEventMixin={Version:3,Initialize:function(_1){
_1.CreateEventHandler=this.CreateEventHandler;
_1.AttachDomEvent=this.AttachDomEvent;
_1.DetachDomEvent=this.DetachDomEvent;
_1.DisposeDomEventHandlers=this.DisposeDomEventHandlers;
_1._domEventHandlingEnabled=true;
_1.EnableDomEventHandling=this.EnableDomEventHandling;
_1.DisableDomEventHandling=this.DisableDomEventHandling;
_1.RemoveHandlerRegister=this.RemoveHandlerRegister;
_1.GetHandlerRegister=this.GetHandlerRegister;
_1.AddHandlerRegister=this.AddHandlerRegister;
_1.handlerRegisters=[];
},EnableDomEventHandling:function(){
this._domEventHandlingEnabled=true;
},DisableDomEventHandling:function(){
this._domEventHandlingEnabled=false;
},CreateEventHandler:function(_2,_3){
var _4=this;
return function(e){
if(!_4._domEventHandlingEnabled&&!_3){
return;
}
return _4[_2](e||window.event);
};
},AttachDomEvent:function(_6,_7,_8,_9){
var _a=this.CreateEventHandler(_8,_9);
var _b=this.GetHandlerRegister(_6,_7,_8);
if(_b!=null){
this.DetachDomEvent(_b.Element,_b.EventName,_8);
}
var _c={"Element":_6,"EventName":_7,"HandlerName":_8,"Handler":_a};
this.AddHandlerRegister(_c);
if(_6.addEventListener){
_6.addEventListener(_7,_a,false);
}else{
if(_6.attachEvent){
_6.attachEvent("on"+_7,_a);
}
}
},DetachDomEvent:function(_d,_e,_f){
var _10=null;
var _11="";
if(typeof _f=="string"){
_11=_f;
_10=this.GetHandlerRegister(_d,_e,_11);
if(_10==null){
return;
}
_f=_10.Handler;
}
if(!_d){
return;
}
if(_d.removeEventListener){
_d.removeEventListener(_e,_f,false);
}else{
if(_d.detachEvent){
_d.detachEvent("on"+_e,_f);
}
}
if(_10!=null&&_11!=""){
this.RemoveHandlerRegister(_10);
_10=null;
}
},DisposeDomEventHandlers:function(){
for(var i=0;i<this.handlerRegisters.length;i++){
var _13=this.handlerRegisters[i];
if(_13!=null){
this.DetachDomEvent(_13.Element,_13.EventName,_13.Handler);
}
}
this.handlerRegisters=[];
},RemoveHandlerRegister:function(_14){
try{
var _15=_14.index;
for(var i in _14){
_14[i]=null;
}
this.handlerRegisters[_15]=null;
}
catch(e){
}
},GetHandlerRegister:function(_17,_18,_19){
for(var i=0;i<this.handlerRegisters.length;i++){
var _1b=this.handlerRegisters[i];
if(_1b!=null&&_1b.Element==_17&&_1b.EventName==_18&&_1b.HandlerName==_19){
return this.handlerRegisters[i];
}
}
return null;
},AddHandlerRegister:function(_1c){
_1c.index=this.handlerRegisters.length;
this.handlerRegisters[this.handlerRegisters.length]=_1c;
}};
RadControlsNamespace.DomEvent={};
RadControlsNamespace.DomEvent.PreventDefault=function(e){
if(!e){
return true;
}
if(e.preventDefault){
e.preventDefault();
}
e.returnValue=false;
return false;
};
RadControlsNamespace.DomEvent.StopPropagation=function(e){
if(!e){
return;
}
if(e.stopPropagation){
e.stopPropagation();
}else{
e.cancelBubble=true;
}
};
RadControlsNamespace.DomEvent.GetTarget=function(e){
if(!e){
return null;
}
return e.target||e.srcElement;
};
RadControlsNamespace.DomEvent.GetRelatedTarget=function(e){
if(!e){
return null;
}
return e.relatedTarget||(e.type=="mouseout"?e.toElement:e.fromElement);
};
RadControlsNamespace.DomEvent.GetKeyCode=function(e){
if(!e){
return 0;
}
return e.which||e.keyCode;
};
};if(typeof (window.RadControlsNamespace)=="undefined"){
window.RadControlsNamespace=new Object();
}
RadControlsNamespace.AppendStyleSheet=function(_1,_2,_3){
if(!_3){
return;
}
if(!_1){
document.write("<"+"link"+" rel='stylesheet' type='text/css' href='"+_3+"' />");
}else{
var _4=document.createElement("LINK");
_4.rel="stylesheet";
_4.type="text/css";
_4.href=_3;
document.getElementById(_2+"StyleSheetHolder").appendChild(_4);
}
};
function RadComboItem(){
this.ComboBox=null;
this.ClientID=null;
this.Index=0;
this.Highlighted=false;
this.Enabled=true;
this.Selected=0;
this.Text="";
this.Value="";
this.Attributes=new Array();
}
RadComboItem.prototype.Initialize=function(_5){
for(var _6 in _5){
this[_6]=_5[_6];
}
};
RadComboItem.prototype.Select=function(){
if(this.ComboBox.FireEvent(this.ComboBox.OnClientSelectedIndexChanging,this)===false){
return;
}
var _7=this.ComboBox.GetText();
var _8=this.ComboBox.GetLastSeparatorIndex(_7);
var _9=_7.substring(0,_8+1)+this.Text;
this.ComboBox.SetText(_9);
this.ComboBox.SetValue(this.Value);
this.ComboBox.SelectedItem=this;
this.ComboBox.SelectedIndex=this.Index;
this.Highlight();
this.ComboBox.FireEvent(this.ComboBox.OnClientSelectedIndexChanged,this);
this.ComboBox.PostBack();
};
RadComboItem.prototype.Highlight=function(){
if(!this.Enabled){
return;
}
if(!this.ComboBox.IsTemplated||this.ComboBox.HighlightTemplatedItems){
if(this.ComboBox.HighlightedItem){
this.ComboBox.HighlightedItem.SetCssClass(this.ComboBox.ItemCssClass);
}
this.SetCssClass(this.ComboBox.ItemCssClassHover);
}
this.ComboBox.HighlightedItem=this;
this.Highlighted=true;
};
RadComboItem.prototype.UnHighlight=function(){
if(!this.ComboBox.IsTemplated||this.ComboBox.HighlightTemplatedItems){
this.SetCssClass(this.ComboBox.ItemCssClass);
}
this.ComboBox.HighlightedItem=null;
this.Highlighted=false;
};
RadComboItem.prototype.ScrollIntoView=function(){
var _a=this.GetDomElement().offsetTop;
var _b=this.GetDomElement().offsetHeight;
var _c=this.ComboBox.DropDownDomElement.scrollTop;
var _d=this.ComboBox.DropDownDomElement.offsetHeight;
if(_a+_b>_c+_d){
this.ComboBox.DropDownDomElement.scrollTop=_a+_b-_d;
}else{
if(_a+_b<=_c){
this.ComboBox.DropDownDomElement.scrollTop=_a;
}
}
};
RadComboItem.prototype.ScrollOnTop=function(){
this.ComboBox.DropDownDomElement.scrollTop=this.GetDomElement().offsetTop;
};
RadComboItem.prototype.NextItem=function(){
return this.ComboBox.Items[this.Index+1];
};
RadComboItem.prototype.GetDomElement=function(){
if(!this.DomElement){
this.DomElement=document.getElementById(this.ClientID);
}
return this.DomElement;
};
RadComboItem.prototype.SetCssClass=function(_e){
this.GetDomElement().className=_e;
};
function RadComboBox(_f,_10,_11){
var _12=window[_10];
if(_12!=null&&typeof (_12.Dispose)!="undefined"){
_12.Dispose();
}
if(window.tlrkComboBoxes==null){
window.tlrkComboBoxes=new Array();
}
tlrkComboBoxes[tlrkComboBoxes.length]=this;
this.Items=new Array();
this.ItemMap=new Object();
this.Created=false;
this.ID=_f;
this.ClientID=_10;
this.TagID=_10;
this.DropDownID=_10+"_DropDown";
this.InputID=_10+"_Input";
this.ImageID=_10+"_Image";
this.DropDownPlaceholderID=_10+"_DropDownPlaceholder";
this.MoreResultsBoxID=_10+"_MoreResultsBox";
this.MoreResultsBoxImageID=_10+"_MoreResultsBoxImage";
this.MoreResultsBoxMessageID=_10+"_MoreResultsBoxMessage";
this.Header=_10+"_Header";
this.Changed=false;
this.Focused=false;
this.InputDomElement=document.getElementById(this.InputID);
this.CachedText=this.OriginalText=this.InputDomElement.value;
this.ImageDomElement=document.getElementById(this.ImageID);
this.DropDownPlaceholder=document.getElementById(this.DropDownPlaceholderID);
this.DropDownDomElement=document.getElementById(this.DropDownID);
this.MoreResultsImageDomElement=document.getElementById(this.MoreResultsBoxImageID);
this.MoreResultsBoxMessageDomElement=document.getElementById(this.MoreResultsBoxMessageID);
this.DomElement=document.getElementById(this.ClientID);
this.ValueHidden=document.getElementById(this.ClientID+"_value");
this.TextHidden=document.getElementById(this.ClientID+"_text");
this.ClientWidthHidden=document.getElementById(this.ClientID+"_clientWidth");
this.ClientHeightHidden=document.getElementById(this.ClientID+"_clientHeight");
this.Enabled=true;
this.DropDownVisible=false;
this.LoadOnDemandUrl=null;
this.HighlightedItem=null;
this.SelectedItem=null;
this.ItemRequestTimeout=300;
this.EnableLoadOnDemand=false;
this.AutoPostBack=false;
this.ShowMoreResultsBox=false;
this.OpenDropDownOnLoad=false;
this.MarkFirstMatch=false;
this.IsCaseSensitive=false;
this.SelectOnTab=true;
this.PostBackReference=null;
this.LoadingMessage="Loading...";
this.ScrollDownImage=null;
this.ScrollDownImageDisabled=null;
this.Overlay=null;
this.RadComboBoxImagePosition="Right";
this.ItemCssClass=null;
this.ItemCssClassHover=null;
this.ItemCssClassDisabled=null;
this.ImageCssClass=null;
this.ImageCssClassHover=null;
this.InputCssClass=null;
this.InputCssClassHover=null;
this.LoadingMessageCssClass="ComboBoxLoadingMessage";
this.AutoCompleteSeparator=null;
this.ExternalCallBackPage=null;
this.OnClientSelectedIndexChanging=null;
this.OnClientDropDownOpening=null;
this.OnClientDropDownClosing=null;
this.OnClientItemsRequesting=null;
this.OnClientSelectedIndexChanged=null;
this.OnClientItemsRequested=null;
this.OnClientKeyPressing=null;
this.OnClientBlur=null;
this.OnClientFocus=null;
this.Skin="Classic";
this.HideTimeoutID=0;
this.RequestTimeoutID=0;
this.IsDetached=false;
this.TextPriorToCallBack=null;
this.AllowCustomText=false;
this.ExpandEffectString=null;
this.HighlightTemplatedItems=false;
this.CausesValidation=false;
this.ClientDataString=null;
this.ShowDropDownOnTextboxClick=true;
this.ShowWhileLoading=_11;
this.MoreResultsImageHovered=false;
this.ErrorMessage=null;
this.AfterClientCallBackError=null;
this.PostBackActive=false;
this.SelectedIndex=-1;
this.IsTemplated=false;
this.CurrentText=null;
this.OffsetX=0;
this.OffsetY=0;
this.Disposed=false;
var me=this;
this.DetermineDirection();
this.InputDomElement.setAttribute("autocomplete","off");
this.DropDownPlaceholder.onselectstart=function(){
return false;
};
RadControlsNamespace.DomEventMixin.Initialize(this);
if(this.ImageDomElement){
this.AttachDomEvent(this.ImageDomElement,"click","OnImageClick");
}
this.AttachDomEvent(document,"click","OnDocumentClick");
this.AttachDomEvent(this.InputDomElement,"click","OnInputClick");
this.AttachDomEvent(this.InputDomElement,"keydown","OnKeyDown");
this.AttachDomEvent(this.InputDomElement,"focus","OnFocus");
this.AttachDomEvent(this.InputDomElement,"input","OnInputChange");
this.AttachDomEvent(this.InputDomElement,"propertychange","OnInputPropertyChange");
this.AttachDomEvent(this.DropDownPlaceholder,"mouseover","OnDropDownOver");
this.AttachDomEvent(this.DropDownPlaceholder,"mouseout","OnDropDownOut");
this.AttachDomEvent(this.DropDownPlaceholder,"click","OnDropDownClick");
if(this.MoreResultsImageDomElement){
this.AttachDomEvent(this.MoreResultsImageDomElement,"mouseover","OnMoreResultsImageOver");
this.AttachDomEvent(this.MoreResultsImageDomElement,"mouseout","OnMoreResultsImageOut");
this.AttachDomEvent(this.MoreResultsImageDomElement,"click","OnMoreResultsImageClick");
}
if(typeof (RadCallbackNamespace)!="undefined"){
window.setTimeout(function(){
me.FixUp(me.InputDomElement,true);
},100);
}else{
if(window.addEventListener){
if(window.opera){
this.AttachDomEvent(window,"load","OnWindowLoad");
}else{
this.OnWindowLoad();
}
}else{
if(document.getElementById(this.ClientID).offsetWidth==0){
this.AttachDomEvent(window,"load","OnWindowLoad");
}else{
this.OnWindowLoad();
}
}
}
this.AttachDomEvent(window,"resize","OnWindowResize");
this.AttachDomEvent(window,"unload","Dispose");
}
RadComboBox.prototype.OnWindowResize=function(){
if(this.DropDownVisible){
this.PositionDropDown();
}
};
RadComboBox.prototype.Initialize=function(_14,_15){
this.LoadConfiguration(_14);
if(!this.Enabled){
this.Disable();
}
this.CreateItems(_15);
this.InitCssNames();
if(this.OpenDropDownOnLoad){
this.AttachDomEvent(window,"load","OpenOnLoad");
}
};
RadComboBox.prototype.OpenOnLoad=function(){
this.FixUp(this.InputDomElement,false);
this.ShowDropDown();
};
RadComboBox.prototype.OnWindowLoad=function(){
this.FixUp(this.InputDomElement,true);
};
RadComboBox.Keys={Shift:16,Escape:27,Up:38,Down:40,Left:37,Right:39,Enter:13,Tab:9,Space:32,PageUp:33,Del:46,F1:112,F12:123};
RadComboBox.prototype.FireEvent=function(_16,_17,_18,_19){
if(typeof (_16)=="function"){
return _16(_17,_18,_19);
}
var _1a=window[_16];
if(!_1a){
return;
}
return _1a(_17,_18,_19);
};
RadComboBox.prototype.PostBack=function(){
if(this.PostBackActive){
return;
}
this.PostBackActive=true;
if(this.AutoPostBack){
if(this.CausesValidation){
if(typeof (WebForm_DoPostBackWithOptions)!="function"&&!(typeof (Page_ClientValidate)!="function"||Page_ClientValidate())){
return;
}
}
eval(this.PostBackReference);
this.PostBackActive=false;
}
};
RadComboBox.prototype.SelectFirstMatch=function(){
var _1b=this.FindItemToSelect();
if(_1b&&_1b.Enabled){
_1b.Highlight();
this.SelectedItem=_1b;
}
};
RadComboBox.prototype.SelectText=function(_1c,_1d){
if(this.InputDomElement.createTextRange){
var _1e=this.InputDomElement.createTextRange();
_1e.moveStart("character",_1c);
_1e.moveEnd("character",_1d);
_1e.select();
}else{
this.InputDomElement.setSelectionRange(_1c,_1c+_1d);
}
};
RadComboBox.prototype.OnInputClick=function(){
this.SelectFirstMatch();
this.SelectText(0,this.GetText().length);
if(this.ShowDropDownOnTextboxClick&&!this.DropDownVisible){
this.ShowDropDown();
}
};
RadComboBox.prototype.OnInputPropertyChange=function(){
if(event.propertyName=="value"){
var _1f=this.GetText();
if(this.CachedText!=_1f){
this.CachedText=_1f;
this.OnInputChange();
}
}
};
RadComboBox.prototype.OnInputChange=function(){
this.SetValue("");
this.TextHidden.value=this.InputDomElement.value;
if(this.EnableLoadOnDemand&&!this.SuppressChange){
var me=this;
if(this.RequestTimeoutID>0){
window.clearTimeout(this.RequestTimeoutID);
this.RequestTimeoutID=0;
}
if(!this.DropDownVisible){
this.ShowDropDown();
}
this.RequestTimeoutID=window.setTimeout(function(){
me.RequestItems(me.GetText(),false);
},this.ItemRequestTimeout);
return;
}
if(!this.SuppressChange&&this.ShouldHighlight()){
this.HighlightMatches();
}
};
RadComboBox.prototype.OnImageClick=function(){
this.SelectFirstMatch();
this.ToggleDropDown();
};
RadComboBox.prototype.OnDocumentClick=function(e){
if(!e){
e=event;
}
var _22=e.target||e.srcElement;
while(_22.nodeType!==9){
if(_22==this.DomElement||_22==this.DropDownPlaceholder){
return;
}
_22=_22.parentNode;
}
if(this.DropDownVisible){
this.HideDropDown();
}
if(this.Focused){
this.RaiseClientBlur();
this.SelectItemOnBlur();
this.Focused=false;
}
};
RadComboBox.prototype.SelectItemOnBlur=function(){
var _23=this.FindItemToSelect();
if(!_23&&!this.AllowCustomText&&this.Items.length>0){
if(this.MarkFirstMatch){
if(this.GetText()==""){
this.SetText(this.OriginalText);
}
this.HighlightMatches();
this.SelectText(0,0);
_23=this.HighlightedItem;
}
}
this.PerformSelect(_23);
};
RadComboBox.prototype.FindItemToSelect=function(){
var _24=this.FindItemByValue(this.GetValue());
if(!_24){
_24=this.FindItemByText(this.GetText());
}
return _24;
};
RadComboBox.prototype.OnMoreResultsImageOver=function(){
this.MoreResultsImageDomElement.style.cursor="pointer";
this.MoreResultsImageDomElement.src=this.ScrollDownImage;
this.MoreResultsImageHovered=true;
};
RadComboBox.prototype.OnMoreResultsImageOut=function(){
this.MoreResultsImageDomElement.style.cursor="default";
this.MoreResultsImageDomElement.src=this.ScrollDownImageDisabled;
this.MoreResultsImageHovered=false;
};
RadComboBox.prototype.OnMoreResultsImageClick=function(){
this.RequestItems(this.GetText(),true);
};
RadComboBox.prototype.OnDropDownOver=function(e){
var _26=this.GetEventTarget(e);
var _27=this.FindNearestItem(_26);
if(_27){
_27.Highlight();
}
};
RadComboBox.prototype.OnDropDownOut=function(e){
if(!e){
e=event;
}
var _29;
try{
_29=e.toElement||e.relatedTarget||e.fromElement;
while(_29.nodeType!==9){
if(_29.parentNode==this.DropDownDomElement){
return;
}
_29=_29.parentNode;
}
}
catch(e){
}
if(this.HighlightedItem){
this.HighlightedItem.UnHighlight();
}
};
RadComboBox.prototype.OnDropDownClick=function(e){
var _2b=this.GetEventTarget(e);
var _2c=this.FindNearestItem(_2b);
if(!_2c||!_2c.Enabled){
return;
}
this.HideDropDown();
this.PerformSelect(_2c);
};
RadComboBox.prototype.GetEventTarget=function(e){
return e.target||e.srcElement;
};
RadComboBox.prototype.FindNearestItem=function(_2e){
while(_2e.nodeType!==9){
if(_2e.parentNode==this.DropDownDomElement){
return this.ItemMap[_2e.id];
}
_2e=_2e.parentNode;
}
return null;
};
RadComboBox.prototype.GetViewPortSize=function(){
var _2f=0;
var _30=0;
var _31=document.body;
if(window.innerWidth){
_2f=window.innerWidth;
_30=window.innerHeight;
}else{
if(document.compatMode&&document.compatMode=="CSS1Compat"){
_31=document.documentElement;
}
_2f=_31.clientWidth;
_30=_31.clientHeight;
}
_2f+=_31.scrollLeft;
_30+=_31.scrollTop;
return {width:_2f-6,height:_30-6};
};
RadComboBox.prototype.GetElementPosition=function(el){
var _33=null;
var pos={x:0,y:0};
var box;
if(el.getBoundingClientRect){
box=el.getBoundingClientRect();
var _36=document.documentElement.scrollTop||document.body.scrollTop;
var _37=document.documentElement.scrollLeft||document.body.scrollLeft;
pos.x=box.left+_37-2;
pos.y=box.top+_36-2;
return pos;
}else{
if(document.getBoxObjectFor){
box=document.getBoxObjectFor(el);
pos.x=box.x-1;
pos.y=box.y-1;
}else{
pos.x=el.offsetLeft;
pos.y=el.offsetTop;
_33=el.offsetParent;
if(_33!=el){
while(_33){
pos.x+=_33.offsetLeft;
pos.y+=_33.offsetTop;
_33=_33.offsetParent;
}
}
}
}
if(window.opera){
_33=el.offsetParent;
while(_33&&_33.tagName!="BODY"&&_33.tagName!="HTML"){
pos.x-=_33.scrollLeft;
pos.y-=_33.scrollTop;
_33=_33.offsetParent;
}
}else{
_33=el.parentNode;
while(_33&&_33.tagName!="BODY"&&_33.tagName!="HTML"){
pos.x-=_33.scrollLeft;
pos.y-=_33.scrollTop;
_33=_33.parentNode;
}
}
return pos;
};
RadComboBox.prototype.Dispose=function(){
if(this.Disposed){
return;
}
this.Disposed=true;
this.HideDropDown();
if(this.Overlay&&this.Overlay.parentNode){
this.Overlay.parentNode.removeChild(this.Overlay);
this.Overlay=null;
}
if(this.DropDownPlaceholder!=null&&this.DropDownPlaceholder.parentNode!=null){
try{
this.DropDownPlaceholder.parentNode.removeChild(this.DropDownPlaceholder);
}
catch(e){
}
}
this.DisposeDomEventHandlers();
this.InputDomElement=null;
this.ImageDomElement=null;
this.DropDownPlaceholder=null;
this.DropDownDomElement=null;
this.MoreResultsImageDomElement=null;
this.MoreResultsBoxMessageDomElement=null;
this.DomElement=null;
this.ValueHidden=null;
this.ClientWidthHidden=null;
this.ClientHeightHidden=null;
tlrkComboBoxes[this.ID]=null;
};
RadComboBox.prototype.LoadConfiguration=function(_38){
for(var _39 in _38){
this[_39]=_38[_39];
}
};
RadComboBox.prototype.InitCssNames=function(){
this.ItemCssClass="ComboBoxItem_"+this.Skin;
this.ItemCssClassHover="ComboBoxItemHover_"+this.Skin;
this.ItemCssClassDisabled="ComboBoxItemDisabled_"+this.Skin;
this.ImageCssClass="ComboBoxImage_"+this.Skin;
this.ImageCssClassHover="ComboBoxImageHover_"+this.Skin;
this.InputCssClass="ComboBoxInput_"+this.Skin;
this.InputCssClassHover="ComboBoxInputHover_"+this.Skin;
this.LoadingMessageCssClass="ComboBoxLoadingMessage_"+this.Skin;
};
RadComboBox.prototype.FindParentForm=function(){
var _3a=document.getElementById(this.TagID);
while(_3a.tagName!="FORM"){
_3a=_3a.parentNode;
}
return _3a;
};
RadComboBox.prototype.DropDownRequiresForm=function(){
var _3b=this.DropDownPlaceholder.getElementsByTagName("input");
return _3b.length>0;
};
RadComboBox.prototype.DetachDropDown=function(){
if((!document.readyState||document.readyState=="complete")&&(!this.IsDetached)){
var _3c=document.body;
if(this.DropDownRequiresForm()){
_3c=this.FindParentForm();
}
this.DropDownPlaceholder.parentNode.removeChild(this.DropDownPlaceholder);
this.DropDownPlaceholder.style.marginLeft="0";
_3c.insertBefore(this.DropDownPlaceholder,_3c.firstChild);
this.IsDetached=true;
}
};
RadComboBox.prototype.CreateItems=function(_3d){
for(var i=0;i<_3d.length;i++){
var _3f=new RadComboItem();
_3f.ComboBox=this;
_3f.Index=this.Items.length;
_3f.Initialize(_3d[i]);
if(_3f.Selected){
this.SelectedItem=_3f;
}
this.ItemMap[_3f.ClientID]=_3f;
this.Items[this.Items.length]=_3f;
}
};
RadComboBox.prototype.ShowOverlay=function(x,y){
if(document.readyState&&document.readyState!="complete"){
return;
}
if(!document.all||window.opera){
return;
}
if(this.Overlay==null){
this.Overlay=document.createElement("iframe");
this.Overlay.src="javascript:''";
this.Overlay.id=this.ClientID+"_Overlay";
this.Overlay.frameBorder=0;
this.Overlay.style.position="absolute";
this.Overlay.style.display="none";
this.DetachDropDown();
this.DropDownPlaceholder.parentNode.insertBefore(this.Overlay,this.DropDownPlaceholder);
this.Overlay.style.zIndex=this.DropDownPlaceholder.style.zIndex-1;
}
this.Overlay.style.left=x;
this.Overlay.style.top=y;
this.Overlay.style.width=this.DropDownPlaceholder.offsetWidth+"px";
this.Overlay.style.height=this.DropDownPlaceholder.offsetHeight+"px";
this.Overlay.style.display="block";
};
RadComboBox.prototype.HideOverlay=function(){
if(!document.all||window.opera){
return;
}
if(this.Overlay!=null){
this.Overlay.style.display="none";
}
};
RadComboBox.prototype.DetermineDirection=function(){
var el=document.getElementById(this.ClientID+"_wrapper");
while(el.tagName.toLowerCase()!="html"){
if(el.dir){
this.RightToLeft=(el.dir.toLowerCase()=="rtl");
return;
}
el=el.parentNode;
}
this.RightToLeft=false;
};
RadComboBox.prototype.PositionDropDown=function(){
this.DetachDropDown();
var _43=this.DomElement.firstChild;
var _44=this.GetElementPosition(_43);
if(this.ExpandEffectString!=null&&document.all){
try{
this.DropDownPlaceholder.style.filter=this.ExpandEffectString;
this.DropDownPlaceholder.filters[0].Apply();
this.DropDownPlaceholder.filters[0].Play();
}
catch(e){
}
}
this.DropDownPlaceholder.style.position="absolute";
this.DropDownPlaceholder.style.top=_44.y+this.OffsetY+this.InputDomElement.offsetHeight+"px";
this.DropDownPlaceholder.style.left=_44.x+this.OffsetX+"px";
this.DropDownPlaceholder.style.display="block";
var _45=this.DomElement.offsetWidth;
this.DropDownPlaceholder.style.width=_45+"px";
var _46=this.DropDownPlaceholder.offsetWidth-_45;
if(_46>0&&_46<_45){
this.DropDownPlaceholder.style.width=_45-_46+"px";
}
if(this.RightToLeft){
this.DropDownPlaceholder.dir="rtl";
}
var _47=this.GetViewPortSize();
if(this.ElementOverflowsBottom(_47,this.DropDownPlaceholder,this.InputDomElement)){
var y=_44.y-this.DropDownPlaceholder.offsetHeight;
if(y>=0){
this.DropDownPlaceholder.style.top=y+"px";
}
}
this.ShowOverlay(this.DropDownPlaceholder.style.left,this.DropDownPlaceholder.style.top);
this.DropDownVisible=true;
};
RadComboBox.prototype.ShowDropDown=function(_49){
if(this.FireEvent(this.OnClientDropDownOpening,this)===false){
return;
}
this.PositionDropDown();
this.InputDomElement.focus();
if(this.EnableLoadOnDemand&&this.Items.length==0&&!_49){
this.RequestItems(this.GetText(),false);
}
};
RadComboBox.prototype.ClearItems=function(){
this.Items=[];
this.ItemMap=new Object();
this.DropDownDomElement.innerHTML="";
};
RadComboBox.prototype.RequestItems=function(_4a,_4b){
if(this.Disposed){
return;
}
if(this.FireEvent(this.OnClientItemsRequesting,this,_4a,_4b)==false){
return;
}
if(!this.LoadingDiv){
this.LoadingDiv=document.createElement("div");
this.LoadingDiv.className=this.LoadingMessageCssClass;
this.LoadingDiv.id=this.ClientID+"_LoadingDiv";
this.LoadingDiv.innerHTML=this.LoadingMessage;
this.DropDownDomElement.insertBefore(this.LoadingDiv,this.DropDownDomElement.firstChild);
}
var _4c=this.GetAjaxUrl(_4a,this.GetText(),this.GetValue(),_4b);
var _4d=this.CreateXmlHttpRequest();
var me=this;
_4d.onreadystatechange=function(){
if(_4d.readyState!=4){
return;
}
me.OnCallBackResponse(_4d,_4b,_4a,_4c);
};
_4d.open("GET",_4c,true);
_4d.setRequestHeader("Content-Type","application/json; charset=utf-8");
_4d.send("");
};
RadComboBox.prototype.OnCallBackResponse=function(_4f,_50,_51,_52){
if(this.LoadingDiv){
this.DropDownDomElement.removeChild(this.LoadingDiv);
this.LoadingDiv=null;
}
if(_4f.status==500){
if(this.FireEvent(this.AfterClientCallBackError,this)===false){
return;
}
alert("RadComboBox: Server error in the ItemsRequested event handler, press ok to view the result.");
var _53;
this.Dispose();
if(this.ErrorMessage){
_53=this.ErrorMessage;
}else{
_53=_4f.responseText;
}
document.body.innerHTML=_53;
return;
}
if(_4f.status==404){
if(this.FireEvent(this.AfterClientCallBackError,this)===false){
return;
}
alert("RadComboBox: Load On Demand Page not found: "+_52);
this.Dispose();
var _53;
if(this.ErrorMessage){
_53=this.ErrorMessage;
}else{
_53="RadComboBox: Load On Demand Page not found: "+_52+"<br/>";
_53+="Please, try using ExternalCallBackPage to map to the exact location of the callbackpage you are using.";
}
document.body.innerHTML=_53;
return;
}
try{
eval("var callBackData = "+_4f.responseText+";");
}
catch(e){
if(this.FireEvent(this.AfterClientCallBackError,this)===false){
return;
}
alert("RadComboBox: load on demand callback error. Press Enter for more information");
this.Dispose();
var _53;
if(this.ErrorMessage){
_53=this.ErrorMessage;
}else{
_53="If RadComboBox is not initially visible on your ASPX page, you may need to use streamers (the ExternallCallBackPage property)";
_53+="<br/>Please, read our online documentation on this problem for details";
_53+="<br/><a href='http://www.telerik.com/help/radcombobox/v2%5FNET2/?combo_externalcallbackpage.html'>http://www.telerik.com/help/radcombobox/v2%5FNET2/combo_externalcallbackpage.html</a>";
}
document.body.innerHTML=_53;
return;
}
if(this.GetText()!=callBackData.Text){
this.RequestItems(this.GetText(),_50);
return;
}
if(!_50){
this.ClearItems();
}
this.SelectedItem=null;
this.HighlightedItem=null;
var _54=this.Items.length;
if(_50){
for(var i=0;i<this.Items.length;i++){
this.Items[i].DomElement=null;
}
}
this.CreateItems(callBackData.Items);
if(_50){
this.DropDownDomElement.innerHTML+=callBackData.DropDownHtml;
if(this.Items[_54+1]!=null){
this.Items[_54+1].ScrollIntoView();
}
}else{
this.DropDownDomElement.innerHTML=callBackData.DropDownHtml;
}
if(this.ShowMoreResultsBox){
this.MoreResultsBoxMessageDomElement.innerHTML=callBackData.Message;
}
this.FireEvent(this.OnClientItemsRequested,this,callBackData.Text,_50);
if(this.ShouldHighlight()){
this.HighlightMatches();
}
};
RadComboBox.prototype.CreateXmlHttpRequest=function(){
if(typeof (XMLHttpRequest)!="undefined"){
return new XMLHttpRequest();
}
if(typeof (ActiveXObject)!="undefined"){
return new ActiveXObject("Microsoft.XMLHTTP");
}
};
RadComboBox.prototype.ClearSelection=function(){
this.SetText("");
this.SetValue("");
this.SelectedItem=null;
this.HighLightedItem=null;
};
RadComboBox.prototype.GetAjaxUrl=function(_56,_57,_58,_59){
_56=_56.replace(/'/g,"&squote");
var url=window.unescape(this.LoadOnDemandUrl)+"&text="+this.EncodeURI(_56);
url=url+"&comboText="+this.EncodeURI(_57);
url=url+"&comboValue="+this.EncodeURI(_58);
url=url+"&skin="+this.EncodeURI(this.Skin);
if(_59){
url=url+"&itemCount="+this.Items.length;
}
if(this.ExternalCallBackPage!=null){
url=url+"&external=true";
}
if(this.ClientDataString!=null){
url+="&clientDataString="+this.EncodeURI(this.ClientDataString);
}
url=url+"&timeStamp="+encodeURIComponent((new Date()).getTime());
return url;
};
RadComboBox.prototype.EncodeURI=function(s){
if(typeof (encodeURIComponent)!="undefined"){
return encodeURIComponent(this.EscapeQuotes(s));
}
if(escape){
return escape(this.EscapeQuotes(s));
}
};
RadComboBox.prototype.EscapeQuotes=function(_5c){
if(typeof (_5c)!="number"){
return _5c.replace(/'/g,"&squote");
}
};
RadComboBox.prototype.ToggleDropDown=function(){
if(this.DropDownVisible){
this.HideDropDown();
}else{
this.ShowDropDown();
if(this.HighlightedItem){
this.HighlightedItem.ScrollIntoView();
}
}
};
RadComboBox.prototype.HideDropDown=function(){
if(this.FireEvent(this.OnClientDropDownClosing,this)===false){
return;
}
this.DropDownPlaceholder.style.display="none";
this.HideOverlay();
this.DropDownVisible=false;
};
RadComboBox.prototype.SetText=function(_5d){
this.SuppressChange=true;
this.InputDomElement.value=_5d;
this.TextHidden.value=_5d;
this.ValueHidden.value="";
if(this.InputDomElement.fireEvent){
var _5e=document.createEventObject();
this.InputDomElement.fireEvent("onchange",_5e);
}else{
if(this.InputDomElement.dispatchEvent){
var _5f=true;
var _5e=document.createEvent("HTMLEvents");
_5e.initEvent("change",_5f,true);
this.InputDomElement.dispatchEvent(_5e);
}
}
this.SuppressChange=false;
};
RadComboBox.prototype.SetValue=function(_60){
this.ValueHidden.value=_60;
};
RadComboBox.prototype.GetValue=function(){
return this.ValueHidden.value;
};
RadComboBox.prototype.PerformSelect=function(_61){
if(_61&&_61!=this.SelectedItem&&!this.EnableLoadOnDemand){
_61.Select();
return;
}
if(_61&&_61==this.SelectedItem&&this.GetText()!=_61.Text&&this.AllowCustomText){
this.SetText(_61.Text);
return;
}
if(_61&&_61==this.SelectedItem){
return;
}
if(_61&&this.OriginalText!=_61.Text){
_61.Select();
return;
}
if(_61&&(!this.SelectedItem||this.SelectedItem.Value!=_61.Value)){
_61.Select();
return;
}
if(this.OriginalText!=this.GetText()){
if(this.HighlightedItem){
this.HighlightedItem.UnHighlight();
}
this.PostBack();
}
};
RadComboBox.prototype.OnKeyDown=function(e){
if(!e){
e=event;
}
this.Changed=true;
this.FireEvent(this.OnClientKeyPressing,this,e);
var _63=e.keyCode||e.which;
this.LastKeyCode=_63;
if(_63==RadComboBox.Keys.Escape&&this.DropDownVisible){
this.HideDropDown();
return;
}
if(_63===RadComboBox.Keys.Enter){
this.HideDropDown();
this.PerformSelect(this.HighlightedItem);
e.returnValue=false;
if(e.preventDefault){
e.preventDefault();
}
return;
}else{
if(_63===RadComboBox.Keys.Down){
e.returnValue=false;
if(e.altKey){
this.ToggleDropDown();
return;
}
this.HighlightNextItem();
return;
}else{
if(_63===RadComboBox.Keys.Up){
e.returnValue=false;
if(e.altKey){
this.ToggleDropDown();
return;
}
this.HighlightPreviousItem();
return;
}else{
if(_63===RadComboBox.Keys.Tab){
this.HideDropDown();
this.RaiseClientBlur();
this.SelectItemOnBlur();
this.Focused=false;
return;
}
}
}
}
if(_63==RadComboBox.Keys.Left||_63==RadComboBox.Keys.Right){
return;
}
};
RadComboBox.prototype.GetLastWord=function(_64){
var _65=-1;
if(this.AutoCompleteSeparator!=null){
_65=this.GetLastSeparatorIndex(_64);
}
var _66=_64.substring(_65+1,_64.length);
return _66;
};
RadComboBox.prototype.GetLastSeparatorIndex=function(_67){
var _68=-1;
if(!this.AutoCompleteSeparator){
return _68;
}
for(var i=0;i<this.AutoCompleteSeparator.length;i++){
var _6a=this.AutoCompleteSeparator.charAt(i);
var _6b=_67.lastIndexOf(_6a);
if(_6b>_68){
_68=_6b;
}
}
return _68;
};
RadComboBox.prototype.GetLastSeparator=function(_6c){
if(!this.AutoCompleteSeparator){
return null;
}
var _6d=this.GetLastSeparatorIndex(_6c);
return _6c.charAt(_6d);
};
RadComboBox.prototype.ShouldHighlight=function(){
if(this.LastKeyCode<RadComboBox.Keys.Space){
return false;
}
if(this.LastKeyCode>=RadComboBox.Keys.PageUp&&this.LastKeyCode<=RadComboBox.Keys.Del){
return false;
}
if(this.LastKeyCode>=RadComboBox.Keys.F1&&this.LastKeyCode<=RadComboBox.Keys.F12){
return false;
}
return true;
};
RadComboBox.prototype.HighlightMatches=function(){
if(!this.MarkFirstMatch){
return;
}
var _6e=this.GetText();
var _6f=this.GetLastWord(_6e);
if(this.GetLastSeparator(_6e)==_6e.charAt(_6e.length-1)){
return;
}
var _70=this.FindFirstMatch(_6f);
if(this.HighlightedItem){
this.HighlightedItem.UnHighlight();
}
if(!_70){
if(!this.AllowCustomText){
if(_6e){
var _71=this.GetLastSeparatorIndex(_6e);
if(_71<_6e.length-1){
this.SetText(_6e.substring(0,_6e.length-1));
this.HighlightMatches();
}
}
}
return;
}
_70.Highlight();
_70.ScrollOnTop();
var _71=this.GetLastSeparatorIndex(_6e);
var _72=_6e.substring(0,_71+1)+_70.Text;
if(_6e!=_72){
this.SetText(_72);
}
this.SetValue(_70.Value);
var _73=_71+_6f.length+1;
var _74=_72.length-_73;
this.SelectText(_73,_74);
};
RadComboBox.prototype.FindFirstMatch=function(_75){
if(!_75){
return null;
}
for(var i=0;i<this.Items.length;i++){
if(this.Items[i].Text.length<_75.length){
continue;
}
if(this.Items[i].Enabled==false){
continue;
}
var _77=this.Items[i].Text.substring(0,_75.length);
if(!this.IsCaseSensitive){
if(_77.toLowerCase()==_75.toLowerCase()){
return this.Items[i];
}
}else{
if(_77==_75){
return this.Items[i];
}
}
}
return null;
};
RadComboBox.prototype.OnFocus=function(e){
if(!e){
e=event;
}
this.Focused=true;
this.FireEvent(this.OnClientFocus,this);
};
RadComboBox.prototype.RaiseClientBlur=function(){
if(this.Focused){
this.FireEvent(this.OnClientBlur,this);
}
};
RadComboBox.prototype.FindNextAvailableIndex=function(_79){
for(var i=_79;i<this.Items.length;i++){
if(this.Items[i].Enabled){
return i;
}
}
return this.Items.length;
};
RadComboBox.prototype.FindPrevAvailableIndex=function(_7b){
for(var i=_7b;i>=0;i--){
if(this.Items[i].Enabled){
return i;
}
}
return -1;
};
RadComboBox.prototype.HighlightNextItem=function(){
var _7d=this.HighlightedItem;
var _7e=0;
if(_7d){
_7e=_7d.Index+1;
}
_7e=this.FindNextAvailableIndex(_7e);
if(_7e<this.Items.length){
this.Items[_7e].Highlight();
this.Items[_7e].ScrollIntoView();
this.Items[_7e].Text;
var _7f=this.GetLastSeparatorIndex(this.GetText());
var _80=this.GetText().substring(0,_7f+1)+this.Items[_7e].Text;
this.SetText(_80);
this.SetValue(this.Items[_7e].Value);
}
};
RadComboBox.prototype.HighlightPreviousItem=function(){
var _81=this.HighlightedItem;
var _82=0;
if(_81){
_82=_81.Index-1;
}
_82=this.FindPrevAvailableIndex(_82);
if(_82>=0){
this.Items[_82].Highlight();
this.Items[_82].ScrollIntoView();
this.Items[_82].Text;
var _83=this.GetLastSeparatorIndex(this.GetText());
var _84=this.GetText().substring(0,_83+1)+this.Items[_82].Text;
this.SetText(_84);
this.SetValue(this.Items[_82].Value);
}
};
RadComboBox.prototype.ElementOverflowsBottom=function(_85,_86,_87){
var _88=this.GetElementPosition(_87).y+_86.offsetHeight;
return _88>_85.height;
};
RadComboBox.prototype.FindItemByText=function(_89){
for(var i=0;i<this.Items.length;i++){
if(this.Items[i].Text==_89){
return this.Items[i];
}
}
return null;
};
RadComboBox.prototype.FindItemByValue=function(_8b){
if(!_8b){
return null;
}
for(var i=0;i<this.Items.length;i++){
if(this.Items[i].Value==_8b){
return this.Items[i];
}
}
return null;
};
RadComboBox.prototype.CancelPropagation=function(_8d){
if(_8d.stopPropagation){
_8d.stopPropagation();
}else{
_8d.cancelBubble=true;
}
};
RadComboBox.prototype.PreventDefault=function(_8e){
if(_8e.preventDefault){
_8e.preventDefault();
}else{
_8e.returnValue=false;
}
};
RadComboBox.prototype.GetText=function(){
return this.InputDomElement.value;
};
RadComboBox.prototype.Enable=function(){
this.EnableDomEventHandling();
this.InputDomElement.disabled=false;
this.Enabled=true;
};
RadComboBox.prototype.Disable=function(){
this.Enabled=false;
this.TextHidden.value=this.GetText();
this.InputDomElement.disabled="disabled";
this.DisableDomEventHandling();
};
RadComboBox.prototype.FixUp=function(_8f,_90){
if((this.ClientWidthHidden.value!="")&&(this.ClientHeightHidden.value!="")){
if(_8f.style.width!=this.ClientWidthHidden.value){
_8f.style.width=this.ClientWidthHidden.value;
}
if(_8f.style.height!=this.ClientHeightHidden.value){
_8f.style.height=this.ClientHeightHidden.value;
}
this.ShowWrapperElement();
return;
}
var _91=_8f.parentNode.getElementsByTagName("img")[0];
if(_90&&_91&&(_91.offsetWidth==0)){
var _92=this;
if(document.attachEvent){
if(document.readyState=="complete"){
window.setTimeout(function(){
_92.FixUp(_8f,false);
},100);
}else{
window.attachEvent("onload",function(){
_92.FixUp(_8f,false);
});
}
}else{
window.addEventListener("load",function(){
_92.FixUp(_8f,false);
},false);
}
return;
}
var _93=null;
if(_8f.currentStyle){
_93=_8f.currentStyle;
}else{
if(document.defaultView&&document.defaultView.getComputedStyle){
_93=document.defaultView.getComputedStyle(_8f,null);
}
}
if(_93==null){
this.ShowWrapperElement();
return;
}
var _94=parseInt(_93.height);
var _95=parseInt(_8f.offsetWidth);
var _96=parseInt(_93.paddingTop);
var _97=parseInt(_93.paddingBottom);
var _98=parseInt(_93.paddingLeft);
var _99=parseInt(_93.paddingRight);
var _9a=parseInt(_93.borderTopWidth);
if(isNaN(_9a)){
_9a=0;
}
var _9b=parseInt(_93.borderBottomWidth);
if(isNaN(_9b)){
_9b=0;
}
var _9c=parseInt(_93.borderLeftWidth);
if(isNaN(_9c)){
_9c=0;
}
var _9d=parseInt(_93.borderRightWidth);
if(isNaN(_9d)){
_9d=0;
}
if(document.compatMode&&document.compatMode=="CSS1Compat"){
if(!isNaN(_94)&&(this.ClientHeightHidden.value=="")){
_8f.style.height=_94-_96-_97-_9a-_9b+"px";
this.ClientHeightHidden.value=_8f.style.height;
}
}
if(!isNaN(_95)&&_95&&(this.ClientWidthHidden.value=="")){
var _9e=0;
if(_91){
_9e=_91.offsetWidth;
}
if(document.compatMode&&document.compatMode=="CSS1Compat"){
var _9f=_95-_9e-_98-_99-_9c-_9d;
if(_9f>=0){
_8f.style.width=_9f+"px";
}
this.ClientWidthHidden.value=_8f.style.width;
}else{
_8f.style.width=_95-_9e;
}
}
this.ShowWrapperElement();
};
RadComboBox.prototype.ShowWrapperElement=function(){
if(!this.ShowWhileLoading){
document.getElementById(this.ClientID+"_wrapper").style.visibility="visible";
}
};;//BEGIN_ATLAS_NOTIFY
if (typeof(Sys) != "undefined"){if (Sys.Application != null && Sys.Application.notifyScriptLoaded != null){Sys.Application.notifyScriptLoaded();}}
//END_ATLAS_NOTIFY
