if(typeof window.RadControlsNamespace=="undefined"){
window.RadControlsNamespace={};
}
if(typeof (window.RadControlsNamespace.DomEventMixin)=="undefined"||typeof (window.RadControlsNamespace.DomEventMixin.Version)==null||window.RadControlsNamespace.DomEventMixin.Version<3){
RadControlsNamespace.DomEventMixin={Version:3,Initialize:function(_1){
_1.CreateEventHandler=this.CreateEventHandler;
_1.AttachDomEvent=this.AttachDomEvent;
_1.DetachDomEvent=this.DetachDomEvent;
_1.DisposeDomEventHandlers=this.DisposeDomEventHandlers;
_1._domEventHandlingEnabled=true;
_1.EnableDomEventHandling=this.EnableDomEventHandling;
_1.DisableDomEventHandling=this.DisableDomEventHandling;
_1.RemoveHandlerRegister=this.RemoveHandlerRegister;
_1.GetHandlerRegister=this.GetHandlerRegister;
_1.AddHandlerRegister=this.AddHandlerRegister;
_1.handlerRegisters=[];
},EnableDomEventHandling:function(){
this._domEventHandlingEnabled=true;
},DisableDomEventHandling:function(){
this._domEventHandlingEnabled=false;
},CreateEventHandler:function(_2,_3){
var _4=this;
return function(e){
if(!_4._domEventHandlingEnabled&&!_3){
return;
}
return _4[_2](e||window.event);
};
},AttachDomEvent:function(_6,_7,_8,_9){
var _a=this.CreateEventHandler(_8,_9);
var _b=this.GetHandlerRegister(_6,_7,_8);
if(_b!=null){
this.DetachDomEvent(_b.Element,_b.EventName,_8);
}
var _c={"Element":_6,"EventName":_7,"HandlerName":_8,"Handler":_a};
this.AddHandlerRegister(_c);
if(_6.addEventListener){
_6.addEventListener(_7,_a,false);
}else{
if(_6.attachEvent){
_6.attachEvent("on"+_7,_a);
}
}
},DetachDomEvent:function(_d,_e,_f){
var _10=null;
var _11="";
if(typeof _f=="string"){
_11=_f;
_10=this.GetHandlerRegister(_d,_e,_11);
if(_10==null){
return;
}
_f=_10.Handler;
}
if(!_d){
return;
}
if(_d.removeEventListener){
_d.removeEventListener(_e,_f,false);
}else{
if(_d.detachEvent){
_d.detachEvent("on"+_e,_f);
}
}
if(_10!=null&&_11!=""){
this.RemoveHandlerRegister(_10);
_10=null;
}
},DisposeDomEventHandlers:function(){
for(var i=0;i<this.handlerRegisters.length;i++){
var _13=this.handlerRegisters[i];
if(_13!=null){
this.DetachDomEvent(_13.Element,_13.EventName,_13.Handler);
}
}
this.handlerRegisters=[];
},RemoveHandlerRegister:function(_14){
try{
var _15=_14.index;
for(var i in _14){
_14[i]=null;
}
this.handlerRegisters[_15]=null;
}
catch(e){
}
},GetHandlerRegister:function(_17,_18,_19){
for(var i=0;i<this.handlerRegisters.length;i++){
var _1b=this.handlerRegisters[i];
if(_1b!=null&&_1b.Element==_17&&_1b.EventName==_18&&_1b.HandlerName==_19){
return this.handlerRegisters[i];
}
}
return null;
},AddHandlerRegister:function(_1c){
_1c.index=this.handlerRegisters.length;
this.handlerRegisters[this.handlerRegisters.length]=_1c;
}};
RadControlsNamespace.DomEvent={};
RadControlsNamespace.DomEvent.PreventDefault=function(e){
if(!e){
return true;
}
if(e.preventDefault){
e.preventDefault();
}
e.returnValue=false;
return false;
};
RadControlsNamespace.DomEvent.StopPropagation=function(e){
if(!e){
return;
}
if(e.stopPropagation){
e.stopPropagation();
}else{
e.cancelBubble=true;
}
};
RadControlsNamespace.DomEvent.GetTarget=function(e){
if(!e){
return null;
}
return e.target||e.srcElement;
};
RadControlsNamespace.DomEvent.GetRelatedTarget=function(e){
if(!e){
return null;
}
return e.relatedTarget||(e.type=="mouseout"?e.toElement:e.fromElement);
};
RadControlsNamespace.DomEvent.GetKeyCode=function(e){
if(!e){
return 0;
}
return e.which||e.keyCode;
};
}
var RadGridNamespace={};
RadGridNamespace.Prefix="grid_";
RadGridNamespace.InitializeClient=function(_22){
var _23=document.getElementById(_22+"AtlasCreation");
if(!_23){
return;
}
var _24=document.createElement("script");
if(navigator.userAgent.indexOf("Safari")!=-1){
_24.innerHTML=_23.innerHTML;
}else{
_24.text=_23.innerHTML;
}
document.body.appendChild(_24);
document.body.removeChild(_24);
_23.parentNode.removeChild(_23);
};
RadGridNamespace.AsyncRequest=function(_25,_26,_27,e){
var _29=window[_27];
if(_29!=null&&typeof (_29.AsyncRequest)=="function"){
_29.AsyncRequest(_25,_26,e);
}
};
RadGridNamespace.AsyncRequestWithOptions=function(_2a,_2b,e){
var _2d=window[_2b];
if(_2d!=null&&typeof (_2d.AsyncRequestWithOptions)=="function"){
_2d.AsyncRequestWithOptions(_2a,e);
}
};
RadGridNamespace.HideShowCells=function(_2e,_2f,_30){
for(var i=0,l=_2e.rows.length;i<l;i++){
var _33=_2e.rows[i].cells[_2f];
if(!_30){
if(_33!=null&&_33.colSpan==1&&_33.style.display!="none"){
_33.style.display="none";
}
}else{
if(_33!=null&&_33.colSpan==1&&_33.style.display=="none"){
_33.style.display=(window.netscape)?"table-cell":"";
}
}
}
};
RadGridNamespace.GetWidth=function(_34){
var _35;
if(window.getComputedStyle){
_35=window.getComputedStyle(_34,"").getPropertyValue("width");
}else{
if(_34.currentStyle){
_35=_34.currentStyle.width;
}else{
_35=_34.offsetWidth;
}
}
if(_35.toString().indexOf("%")!=-1){
_35=_34.offsetWidth;
}
if(_35.toString().indexOf("px")!=-1){
_35=parseInt(_35);
}
return _35;
};
RadGridNamespace.GetScrollBarWidth=function(){
try{
if(typeof (RadGridNamespace.scrollbarWidth)=="undefined"){
var _36,_37=0;
var _38=document.createElement("div");
_38.style.position="absolute";
_38.style.top="-1000px";
_38.style.left="-1000px";
_38.style.width="100px";
_38.style.overflow="auto";
var _39=document.createElement("div");
_39.style.width="1000px";
_38.appendChild(_39);
document.body.appendChild(_38);
_36=_38.offsetWidth;
_37=_38.clientWidth;
document.body.removeChild(document.body.lastChild);
RadGridNamespace.scrollbarWidth=_36-_37;
if(RadGridNamespace.scrollbarWidth<=0||_37==0){
RadGridNamespace.scrollbarWidth=16;
}
}
return RadGridNamespace.scrollbarWidth;
}
catch(error){
return false;
}
};
RadGridNamespace.GetTableColGroup=function(_3a){
try{
return _3a.getElementsByTagName("colgroup")[0];
}
catch(error){
return false;
}
};
RadGridNamespace.GetTableColGroupCols=function(_3b){
try{
var _3c=new Array();
var _3d=_3b.childNodes[0];
for(var i=0;i<_3b.childNodes.length;i++){
if((_3b.childNodes[i].tagName)&&(_3b.childNodes[i].tagName.toLowerCase()=="col")){
_3c[_3c.length]=_3b.childNodes[i];
}
}
return _3c;
}
catch(error){
return false;
}
};
RadGridNamespace.Confirm=function(_3f,e){
if(!confirm(_3f)){
e.cancelBubble=true;
e.returnValue=false;
return false;
}
};
RadGridNamespace.SynchronizeWithWindow=function(){
};
RadGridNamespace.IsRightToLeft=function(_41){
try{
while(_41){
if(_41.currentStyle&&_41.currentStyle.direction.toLowerCase()=="rtl"){
return true;
}else{
if(getComputedStyle&&getComputedStyle(_41,"").getPropertyValue("direction").toLowerCase()=="rtl"){
return true;
}else{
if(_41.dir.toLowerCase()=="rtl"){
return true;
}
}
}
_41=_41.parentNode;
}
return false;
}
catch(error){
new RadGridNamespace.Error(error,this,this.OnError,this.OnError);
}
};
RadGridNamespace.FireEvent=function(_42,_43,_44){
try{
var _45=true;
if(typeof (_42[_43])=="string"){
eval(_42[_43]);
}else{
if(typeof (_42[_43])=="function"){
if(_44){
switch(_44.length){
case 1:
_45=_42[_43](_44[0]);
break;
case 2:
_45=_42[_43](_44[0],_44[1]);
break;
}
}else{
_45=_42[_43]();
}
}
}
if(typeof (_45)!="boolean"){
return true;
}else{
return _45;
}
}
catch(error){
throw error;
}
};
RadGridNamespace.CheckParentNodesFor=function(_46,_47){
while(_46){
if(_46==_47){
return true;
}
_46=_46.parentNode;
}
return false;
};
RadGridNamespace.GetCurrentElement=function(e){
if(!e){
var e=window.event;
}
var _49;
if(e.srcElement){
_49=e.srcElement;
}else{
_49=e.target;
}
return _49;
};
RadGridNamespace.GetEventPosX=function(e){
var x=e.clientX;
var _4c=RadGridNamespace.GetCurrentElement(e);
while(_4c.parentNode){
if(typeof (_4c.parentNode.scrollLeft)=="number"){
x+=_4c.parentNode.scrollLeft;
}
_4c=_4c.parentNode;
}
if(document.body.currentStyle&&document.body.currentStyle.margin.indexOf("px")!=-1&&!window.opera){
x=parseInt(x)-parseInt(document.body.currentStyle.marginLeft);
}
if(RadGridNamespace.IsRightToLeft(document.body)){
x=x-RadGridNamespace.GetScrollBarWidth();
}
return x;
};
RadGridNamespace.GetEventPosY=function(e){
var y=e.clientY;
var _4f=RadGridNamespace.GetCurrentElement(e);
while(_4f.parentNode){
if(typeof (_4f.parentNode.scrollTop)=="number"){
y+=_4f.parentNode.scrollTop;
}
_4f=_4f.parentNode;
}
if(document.body.currentStyle&&document.body.currentStyle.margin.indexOf("px")!=-1&&!window.opera){
y=parseInt(y)-parseInt(document.body.currentStyle.marginTop);
}
return y;
};
RadGridNamespace.IsChildOf=function(_50,_51){
while(_50.parentNode){
if(_50.parentNode==_51){
return true;
}
_50=_50.parentNode;
}
return false;
};
RadGridNamespace.GetFirstParentByTagName=function(_52,_53){
while(_52.parentNode){
if(_52.tagName.toLowerCase()==_53.toLowerCase()){
return _52;
}
_52=_52.parentNode;
}
return null;
};
RadGridNamespace.FindScrollPosX=function(_54){
var x=0;
while(_54.parentNode){
if(typeof (_54.parentNode.scrollLeft)=="number"){
x+=_54.parentNode.scrollLeft;
}
_54=_54.parentNode;
}
if(document.body.currentStyle&&document.body.currentStyle.margin.indexOf("px")!=-1&&!window.opera){
x=parseInt(x)-parseInt(document.body.currentStyle.marginLeft);
}
return x;
};
RadGridNamespace.FindScrollPosY=function(_56){
var y=0;
while(_56.parentNode){
if(typeof (_56.parentNode.scrollTop)=="number"){
y+=_56.parentNode.scrollTop;
}
_56=_56.parentNode;
}
if(document.body.currentStyle&&document.body.currentStyle.margin.indexOf("px")!=-1&&!window.opera){
y=parseInt(y)-parseInt(document.body.currentStyle.marginTop);
}
return y;
};
RadGridNamespace.FindPosX=function(_58){
try{
var x=0;
var _5a=0;
if(_58.offsetParent){
while(_58.offsetParent){
x+=_58.offsetLeft;
if(_58.currentStyle&&_58.currentStyle.borderLeftWidth.indexOf("px")!=-1&&!window.opera){
_5a+=parseInt(_58.currentStyle.borderLeftWidth);
}
_58=_58.offsetParent;
}
}else{
if(_58.x){
x+=_58.x;
}
}
if(document.compatMode=="BackCompat"||navigator.userAgent.indexOf("Safari")!=-1){
if(document.body.currentStyle&&document.body.currentStyle.margin.indexOf("px")!=-1&&!window.opera){
x=parseInt(x)-parseInt(document.body.currentStyle.marginLeft);
}
if(document.defaultView&&document.defaultView.getComputedStyle&&document.defaultView.getComputedStyle(document.body).marginLeft.indexOf("px")!=-1&&!window.opera){
x=parseInt(x)+parseInt(document.defaultView.getComputedStyle(document.body).marginLeft);
}
}
return x+_5a;
}
catch(error){
return x;
}
};
RadGridNamespace.FindPosY=function(_5b){
var y=0;
var _5d=0;
if(_5b.offsetParent){
while(_5b.offsetParent){
y+=_5b.offsetTop;
if(_5b.currentStyle&&_5b.currentStyle.borderTopWidth.indexOf("px")!=-1&&!window.opera){
_5d+=parseInt(_5b.currentStyle.borderTopWidth);
}
_5b=_5b.offsetParent;
}
}else{
if(_5b.y){
y+=_5b.y;
}
}
if(document.compatMode=="BackCompat"||navigator.userAgent.indexOf("Safari")!=-1){
if(document.body.currentStyle&&document.body.currentStyle.margin.indexOf("px")!=-1&&!window.opera){
y=parseInt(y)-parseInt(document.body.currentStyle.marginTop);
}
if(document.defaultView&&document.defaultView.getComputedStyle&&document.defaultView.getComputedStyle(document.body).marginTop.indexOf("px")!=-1&&!window.opera){
y=parseInt(y)+parseInt(document.defaultView.getComputedStyle(document.body).marginTop);
}
}
return y+_5d;
};
RadGridNamespace.GetNodeNextSiblingByTagName=function(_5e,_5f){
while((_5e!=null)&&(_5e.tagName!=_5f)){
_5e=_5e.nextSibling;
}
return _5e;
};
RadGridNamespace.GetNodeNextSibling=function(_60){
while(_60!=null){
if(_60.nextSibling){
_60=_60.nextSibling;
}else{
_60=null;
}
if(_60){
if(_60.nodeType==1){
break;
}
}
}
return _60;
};
RadGridNamespace.DeleteSubString=function(_61,_62,_63){
return _61=_61.substring(0,_62)+_61.substring(_63+1,_61.length);
};
RadGridNamespace.ClearDocumentEvents=function(){
if(document.onmousedown!=this.mouseDownHandler){
this.documentOnMouseDown=document.onmousedown;
}
if(document.onselectstart!=this.selectStartHandler){
this.documentOnSelectStart=document.onselectstart;
}
if(document.ondragstart!=this.dragStartHandler){
this.documentOnDragStart=document.ondragstart;
}
this.mouseDownHandler=function(e){
return false;
};
this.selectStartHandler=function(){
return false;
};
this.dragStartHandler=function(){
return false;
};
document.onmousedown=this.mouseDownHandler;
document.onselectstart=this.selectStartHandler;
document.ondragstart=this.dragStartHandler;
};
RadGridNamespace.RestoreDocumentEvents=function(){
if((typeof (this.documentOnMouseDown)=="function")&&(document.onmousedown!=this.mouseDownHandler)){
document.onmousedown=this.documentOnMouseDown;
}else{
document.onmousedown="";
}
if((typeof (this.documentOnSelectStart)=="function")&&(document.onselectstart!=this.selectStartHandler)){
document.onselectstart=this.documentOnSelectStart;
}else{
document.onselectstart="";
}
if((typeof (this.documentOnDragStart)=="function")&&(document.ondragstart!=this.dragStartHandler)){
document.ondragstart=this.documentOnDragStart;
}else{
document.ondragstart="";
}
};
RadGridNamespace.AddStyleSheet=function(_65){
if(RadGridNamespace.StyleSheets==null){
RadGridNamespace.StyleSheets={};
}
var _66=RadGridNamespace.StyleSheets[_65];
if(_66!=null){
return null;
}
var css=null;
var _68=null;
var _69=document.getElementsByTagName("head")[0];
if(window.netscape||navigator.userAgent.indexOf("Safari")!=-1){
css=document.createElement("style");
css.media="all";
css.type="text/css";
_69.appendChild(css);
}else{
try{
css=document.createStyleSheet();
}
catch(e){
return false;
}
}
var _6a=document.styleSheets[document.styleSheets.length-1];
RadGridNamespace.StyleSheets[_65]=_6a;
return _6a;
};
RadGridNamespace.AddRule=function(ss,_6c,_6d){
try{
if(!ss){
return false;
}
if(ss.insertRule&&navigator.userAgent.indexOf("Safari")==-1){
var _6e=ss.insertRule(_6c+" {"+_6d+"}",ss.cssRules.length);
return ss.cssRules[ss.cssRules.length-1];
}
if(navigator.userAgent.indexOf("Safari")!=-1){
ss.addRule(_6c,_6d);
return ss.cssRules[ss.cssRules.length-1];
}
if(ss.addRule){
ss.addRule(_6c,_6d);
return true;
}
return false;
}
catch(e){
return false;
}
};
RadGridNamespace.addClassName=function(_6f,_70){
var s=_6f.className;
var p=s.split(" ");
if(p.length==1&&p[0]==""){
p=[];
}
var l=p.length;
for(var i=0;i<l;i++){
if(p[i]==_70){
return;
}
}
p[p.length]=_70;
_6f.className=p.join(" ");
};
RadGridNamespace.removeClassName=function(_75,_76){
if(_75.className.replace(/^\s*|\s*$/g,"")==_76){
_75.className="";
return;
}
var _77=_75.className.split(" ");
var _78=[];
for(var i=0,l=_77.length;i<l;i++){
if(_77[i]==""){
continue;
}
if(_76.indexOf(_77[i])==-1){
_78[_78.length]=_77[i];
}
}
_75.className=_78.join(" ");
return;
_75.className=(_75.className.toString()==_76)?"":_75.className.replace(_76,"").replace(/\s*$/g,"");
return;
var p=s.split(" ");
var np=[];
var l=p.length;
var j=0;
for(var i=0;i<l;i++){
if(p[i]!=_76){
np[j++]=p[i];
}
}
_75.className=np.join(" ");
};
RadGridNamespace.CheckIsParentDisplay=function(_7e){
try{
while(_7e){
if(_7e.style){
if(_7e.currentStyle){
if(_7e.currentStyle.display=="none"){
return false;
}
}else{
if(_7e.style.display=="none"){
return false;
}
}
}
_7e=_7e.parentNode;
}
if(window.top){
if(window.top.location!=window.location){
return false;
}
}
return true;
}
catch(e){
return false;
}
};
RadGridNamespace.EncodeURI=function(_7f){
if(encodeURI){
return encodeURI(_7f);
}else{
return escape(_7f);
}
};
if(typeof (window.RadControlsNamespace)=="undefined"){
window.RadControlsNamespace=new Object();
}
RadControlsNamespace.AppendStyleSheet=function(_80,_81,_82){
if(!_82){
return;
}
if(!_80){
document.write("<"+"link"+" rel='stylesheet' type='text/css' href='"+_82+"' />");
}else{
var _83=document.createElement("link");
_83.rel="stylesheet";
_83.type="text/css";
_83.href=_82;
var _84=document.getElementById(_81+"StyleSheetHolder");
if(_84!=null){
document.getElementById(_81+"StyleSheetHolder").appendChild(_83);
}
}
};
RadGridNamespace.RadGrid=function(_85){
var _86=window[_85.ClientID];
if(_86!=null&&typeof (_86.Dispose)=="function"){
window.setTimeout(function(){
_86.Dispose();
},100);
}
RadControlsNamespace.DomEventMixin.Initialize(this);
this.AttachDomEvent(window,"unload","OnWindowUnload");
window[_85.ClientID]=this;
window["grid_"+_85.ClientID]=this;
if(RadGridNamespace.DocumentCanBeModified()){
this._constructor(_85);
}else{
this.objectData=_85;
this.AttachDomEvent(window,"load","OnWindowLoad");
}
};
RadGridNamespace.DocumentCanBeModified=function(){
return (RadGridNamespace.DocumentHasBeenFullyLoaded==true)||(document.readyState=="complete")||window.opera;
};
RadGridNamespace.RadGrid.prototype.OnWindowUnload=function(e){
this.Dispose();
};
RadGridNamespace.RadGrid.prototype.OnWindowLoad=function(e){
RadGridNamespace.DocumentHasBeenFullyLoaded=true;
this._constructor(this.objectData);
this.objectData=null;
};
RadGridNamespace.RadGrid.prototype._constructor=function(_89){
this.Type="RadGrid";
if(_89.ClientSettings){
this.InitializeEvents(_89.ClientSettings.ClientEvents);
}
RadGridNamespace.FireEvent(this,"OnGridCreating");
for(var _8a in _89){
this[_8a]=_89[_8a];
}
this.Initialize();
RadGridNamespace.FireEvent(this,"OnMasterTableViewCreating");
this.GridStyleSheet=RadGridNamespace.AddStyleSheet(this.ClientID);
if(this.ClientSettings&&this.ClientSettings.Scrolling.AllowScroll&&this.ClientSettings.Scrolling.UseStaticHeaders){
var ID=_89.MasterTableView.ClientID;
_89.MasterTableView.ClientID=ID+"_Header";
this.MasterTableViewHeader=new RadGridNamespace.RadGridTable(_89.MasterTableView);
this.MasterTableViewHeader._constructor(this);
if(document.getElementById(ID+"_Footer")){
_89.MasterTableView.ClientID=ID+"_Footer";
this.MasterTableViewFooter=new RadGridNamespace.RadGridTable(_89.MasterTableView);
this.MasterTableViewFooter._constructor(this);
}
_89.MasterTableView.ClientID=ID;
}
this.MasterTableView._constructor(this);
RadGridNamespace.FireEvent(this,"OnMasterTableViewCreated");
this.DetailTablesCollection=new Array();
this.LoadDetailTablesCollection(this.MasterTableView,1);
this.AttachDomEvents();
RadGridNamespace.FireEvent(this,"OnGridCreated");
this.InitializeFeatures(_89);
if(typeof (window.event)=="undefined"){
window.event=null;
}
};
RadGridNamespace.RadGrid.prototype.Dispose=function(){
if(this.Disposed){
return;
}
this.Disposed=true;
try{
RadGridNamespace.FireEvent(this,"OnGridDestroying");
this.DisposeDomEventHandlers();
this.DisposeEvents();
this.GridStyleSheet=null;
this.DisposeFeatures();
this.DisposeDetailTablesCollection(this.MasterTableView,1);
if(this.MasterTableViewHeader!=null){
this.MasterTableViewHeader.Dispose();
}
if(this.MasterTableViewFooter!=null){
this.MasterTableViewFooter.Dispose();
}
if(this.MasterTableView!=null){
this.MasterTableView.Dispose();
}
this.DisposeProperties();
}
catch(error){
}
};
RadGridNamespace.RadGrid.ClientEventNames={OnGridCreating:true,OnGridCreated:true,OnGridDestroying:true,OnMasterTableViewCreating:true,OnMasterTableViewCreated:true,OnTableCreating:true,OnTableCreated:true,OnTableDestroying:true,OnScroll:true,OnKeyPress:true,OnRequestStart:true,OnRequestEnd:true,OnRequestError:true,OnError:true,OnRowDeleting:true,OnRowDeleted:true};
RadGridNamespace.RadGrid.prototype.IsClientEventName=function(_8c){
return RadGridNamespace.RadGrid.ClientEventNames[_8c]==true;
};
RadGridNamespace.RadGrid.prototype.InitializeEvents=function(_8d){
for(var _8e in _8d){
if(typeof (_8d[_8e])!="string"){
continue;
}
if(this.IsClientEventName(_8e)){
if(_8d[_8e]!=""){
var _8f=_8d[_8e];
if(_8f.indexOf("(")!=-1){
this[_8e]=_8f;
}else{
this[_8e]=eval(_8f);
}
}else{
this[_8e]=null;
}
}
}
};
RadGridNamespace.RadGrid.prototype.DisposeEvents=function(){
for(var _90 in RadGridNamespace.RadGrid.ClientEventNames){
this[_90]=null;
}
};
RadGridNamespace.RadGrid.prototype.GetDetailTable=function(_91,_92){
if(_91.HierarchyIndex==_92){
return _91;
}
if(_91.DetailTables){
for(var i=0;i<_91.DetailTables.length;i++){
var res=this.GetDetailTable(_91.DetailTables[i],_92);
if(res){
return res;
}
}
}
};
RadGridNamespace.RadGrid.prototype.LoadDetailTablesCollection=function(_95,_96){
try{
if(_95.Controls[0]!=null&&_95.Controls[0].Rows!=null){
for(var i=0;i<_95.Controls[0].Rows.length;i++){
var _98=_95.Controls[0].Rows[i].ItemType;
if(_98=="NestedView"){
var _99=_95.Controls[0].Rows[i].NestedTableViews;
for(var j=0;j<_99.length;j++){
var _9b=_99[j];
if(_9b.Visible){
var _9c=this.GetDetailTable(this.MasterTableView,_9b.HierarchyIndex);
RadGridNamespace.FireEvent(this,"OnTableCreating",[_9c]);
_9b._constructor(this);
this.DetailTablesCollection[this.DetailTablesCollection.length]=_9b;
if(_9b.AllowFilteringByColumn){
this.InitializeFilterMenu(_9b);
}
RadGridNamespace.FireEvent(this,"OnTableCreated",[_9b]);
}
this.LoadDetailTablesCollection(_9b,_96+1);
}
}
}
}
}
catch(error){
new RadGridNamespace.Error(error,this,this.OnError);
}
};
RadGridNamespace.RadGrid.prototype.DisposeDetailTablesCollection=function(_9d,_9e){
if(_9d.Controls[0]!=null&&_9d.Controls[0].Rows!=null){
for(var i=0;i<_9d.Controls[0].Rows.length;i++){
var _a0=_9d.Controls[0].Rows[i].ItemType;
if(_a0=="NestedView"){
var _a1=_9d.Controls[0].Rows[i].NestedTableViews;
for(var j=0;j<_a1.length;j++){
var _a3=_a1[j];
_a3.Dispose();
}
}
}
}
};
RadGridNamespace.RadGrid.prototype.AddRtlClass=function(){
if(RadGridNamespace.IsRightToLeft(this.Control)){
RadGridNamespace.addClassName(this.Control,"RadGridRTL_"+this.Skin);
}
};
RadGridNamespace.RadGrid.prototype.Initialize=function(){
this.Control=document.getElementById(this.ClientID);
if(this.Control==null){
return;
}
this.Control.tabIndex=0;
this.AddRtlClass();
this.GridDataDiv=document.getElementById(this.ClientID+"_GridData");
if(this.GroupPanel){
this.GroupPanelControl=document.getElementById(this.GroupPanel.ClientID+"_GroupPanel");
}
this.GridHeaderDiv=document.getElementById(this.ClientID+"_GridHeader");
this.GridFooterDiv=document.getElementById(this.ClientID+"_GridFooter");
this.PostDataValue=document.getElementById(this.ClientID+"PostDataValue");
this.LoadingTemplate=document.getElementById(this.ClientID+"_LoadingTemplate");
this.PagerControl=document.getElementById(this.MasterTableView.ClientID+"_Pager");
this.TopPagerControl=document.getElementById(this.MasterTableView.ClientID+"_TopPager");
if(this.LoadingTemplate){
this.LoadingTemplate.style.display="none";
if(this.GridDataDiv){
this.GridDataDiv.appendChild(this.LoadingTemplate);
}
}
};
RadGridNamespace.RadGrid.prototype.DisposeProperties=function(){
this.Control=null;
this.GridDataDiv=null;
this.GroupPanelControl=null;
this.GridHeaderDiv=null;
this.GridFooterDiv=null;
this.PostDataValue=null;
this.LoadingTemplate=null;
this.PagerControl=null;
};
RadGridNamespace.RadGrid.prototype.InitializeFeatures=function(_a4){
if(!this.MasterTableView.Control){
return;
}
if(this.GroupPanelControl!=null){
this.GroupPanelObject=new RadGridNamespace.RadGridGroupPanel(this.GroupPanelControl,this);
}
if(this.ClientSettings&&this.ClientSettings.Scrolling.AllowScroll){
this.InitializeDimensions();
this.InitializeScroll();
}
if(this.AllowFilteringByColumn||this.MasterTableView.AllowFilteringByColumn){
var _a5=(this.MasterTableViewHeader)?this.MasterTableViewHeader:this.MasterTableView;
this.InitializeFilterMenu(_a5);
}
if(this.ClientSettings&&this.ClientSettings.AllowKeyboardNavigation&&this.MasterTableView.Rows){
if(!this.MasterTableView.RenderActiveItemStyleClass||this.MasterTableView.RenderActiveItemStyleClass==""){
if(this.MasterTableView.RenderActiveItemStyle&&this.MasterTableView.RenderActiveItemStyle!=""){
RadGridNamespace.AddRule(this.GridStyleSheet,".ActiveItemStyle"+this.MasterTableView.ClientID+"1 td",this.MasterTableView.RenderActiveItemStyle);
}else{
RadGridNamespace.AddRule(this.GridStyleSheet,".ActiveItemStyle"+this.MasterTableView.ClientID+"2 td","background-color:#FFA07A;");
}
}
if(this.ActiveRow==null){
this.ActiveRow=this.MasterTableView.Rows[0];
}
this.SetActiveRow(this.ActiveRow);
}
if(window[this.ClientID+"_Slider"]){
this.Slider=new RadGridNamespace.Slider(window[this.ClientID+"_Slider"]);
}
};
RadGridNamespace.RadGrid.prototype.DisposeFeatures=function(){
if(this.Slider!=null){
this.Slider.Dispose();
this.Slider=null;
}
if(this.GroupPanelControl!=null){
this.GroupPanelObject.Dispose();
this.GroupPanelControl=null;
}
if(this.AllowFilteringByColumn||this.MasterTableView.AllowFilteringByColumn){
var _a6=(this.MasterTableViewHeader)?this.MasterTableViewHeader:this.MasterTableView;
this.DisposeFilterMenu(_a6);
}
this.Control=null;
};
RadGridNamespace.RadGrid.prototype.AsyncRequest=function(_a7,_a8,e){
var _aa;
if(this.StatusBarSettings!=null&&this.StatusBarSettings.StatusLabelID!=null&&this.StatusBarSettings.StatusLabelID!=""){
var _ab=document.getElementById(this.StatusBarSettings.StatusLabelID);
if(_ab!=null){
_aa=_ab.innerHTML;
_ab.innerHTML=this.StatusBarSettings.LoadingText;
}
}
var _ac=this.ClientID;
this.OnRequestEndInternal=function(){
RadGridNamespace.FireEvent(window[_ac],"OnRequestEnd");
if(_ab){
_ab.innerHTML=_aa;
}
};
RadAjaxNamespace.AsyncRequest(_a7,_a8,_ac,e);
};
RadGridNamespace.RadGrid.prototype.AjaxRequest=function(_ad,_ae){
this.AsyncRequest(_ad,_ae);
};
RadGridNamespace.RadGrid.prototype.ClearSelectedRows=function(){
for(var i=0;i<this.DetailTablesCollection.length;i++){
var _b0=this.DetailTablesCollection[i];
_b0.ClearSelectedRows();
}
this.MasterTableView.ClearSelectedRows();
};
RadGridNamespace.RadGrid.prototype.AsyncRequestWithOptions=function(_b1,e){
RadAjaxNamespace.AsyncRequestWithOptions(_b1,this.ClientID,e);
};
RadGridNamespace.RadGrid.prototype.DeleteRow=function(_b3,_b4,e){
var _b6=(e.srcElement)?e.srcElement:e.target;
if(!_b6){
return;
}
var row=_b6.parentNode.parentNode;
var _b8=row.parentNode.parentNode;
var _b9=row.rowIndex;
var _ba=row.cells.length;
var _bb=this.GetTableObjectByID(_b3);
var _bc=this.GetRowObjectByRealRow(_bb,row);
var _bd={Row:_bc};
if(!RadGridNamespace.FireEvent(this,"OnRowDeleting",[_bb,_bd])){
return;
}
_b8.deleteRow(row.rowIndex);
for(var i=_b9;i<_b8.rows.length;i++){
if(_b8.rows[i].cells.length!=_ba&&_b8.rows[i].style.display!="none"){
_b8.deleteRow(i);
i--;
}else{
break;
}
}
if(_b8.tBodies[0].rows.length==1&&_b8.tBodies[0].rows[0].style.display=="none"){
_b8.tBodies[0].rows[0].style.display="";
}
this.PostDataValue.value+="DeletedRows,"+_b3+","+_b4+";";
RadGridNamespace.FireEvent(this,"OnRowDeleted",[_bb,_bd]);
};
RadGridNamespace.RadGrid.prototype.SelectRow=function(_bf,_c0,e){
var _c2=(e.srcElement)?e.srcElement:e.target;
if(!_c2){
return;
}
var row=RadGridNamespace.GetFirstParentByTagName(_c2,"tr");
var _c4=RadGridNamespace.GetFirstParentByTagName(row,"table");
var _c5=row.rowIndex;
var _c6;
if(_bf==this.MasterTableView.UID){
_c6=this.MasterTableView;
}else{
for(var i=0;i<this.DetailTablesCollection.length;i++){
if(this.DetailTablesCollection[i].ClientID==_c4.id){
_c6=this.DetailTablesCollection[i];
break;
}
}
}
if(_c6!=null){
if(this.AllowMultiRowSelection){
_c6.SelectRow(row,false);
}else{
_c6.SelectRow(row,true);
}
}
};
RadGridNamespace.RadGrid.prototype.SelectAllRows=function(_c8,_c9,e){
var _cb=(e.srcElement)?e.srcElement:e.target;
if(!_cb){
return;
}
var row=_cb.parentNode.parentNode;
var _cd=row.parentNode.parentNode;
var _ce=row.rowIndex;
var _cf;
if(_c8==this.MasterTableView.UID){
_cf=this.MasterTableView;
}else{
for(var i=0;i<this.DetailTablesCollection.length;i++){
if(this.DetailTablesCollection[i].UID==_c8){
_cf=this.DetailTablesCollection[i];
break;
}
}
}
if(_cf!=null){
if(this.AllowMultiRowSelection){
if(_cf==this.MasterTableViewHeader){
_cf=this.MasterTableView;
}
_cf.ClearSelectedRows();
if(_cb.checked){
for(var i=0;i<_cf.Control.tBodies[0].rows.length;i++){
var row=_cf.Control.tBodies[0].rows[i];
_cf.SelectRow(row,false);
}
}else{
for(var i=0;i<_cf.Control.tBodies[0].rows.length;i++){
var row=_cf.Control.tBodies[0].rows[i];
_cf.DeselectRow(row);
}
this.UpdateClientRowSelection();
}
}
}
};
RadGridNamespace.RadGrid.prototype.UpdateClientRowSelection=function(){
var _d1=this.MasterTableView.GetSelectedRowsIndexes();
this.SavePostData("SelectedRows",this.MasterTableView.ClientID,_d1);
for(var i=0;i<this.DetailTablesCollection.length;i++){
_d1=this.DetailTablesCollection[i].GetSelectedRowsIndexes();
this.SavePostData("SelectedRows",this.DetailTablesCollection[i].ClientID,_d1);
}
};
RadGridNamespace.RadGrid.prototype.HandleActiveRow=function(e){
if((this.AllowRowResize)||(this.AllowRowSelect)){
var _d4=this.GetCellFromPoint(e);
if((_d4!=null)&&(_d4.parentNode.id!="")&&(_d4.parentNode.id!=-1)&&(_d4.cellIndex==0)){
var _d5=_d4.parentNode.parentNode.parentNode;
this.SetActiveRow(_d5,_d4.parentNode.rowIndex);
}
}
};
RadGridNamespace.RadGrid.prototype.SetActiveRow=function(_d6){
if(_d6==null){
return;
}
if(_d6.Owner.RenderActiveItemStyle){
RadGridNamespace.removeClassName(this.ActiveRow.Control,"ActiveItemStyle"+_d6.Owner.ClientID+"1");
}else{
RadGridNamespace.removeClassName(this.ActiveRow.Control,"ActiveItemStyle"+_d6.Owner.ClientID+"2");
}
RadGridNamespace.removeClassName(this.ActiveRow.Control,_d6.Owner.RenderActiveItemStyleClass);
if(this.ActiveRow.Control.style.cssText==_d6.Owner.RenderActiveItemStyle){
this.ActiveRow.Control.style.cssText="";
}
this.ActiveRow=_d6;
if(!this.ActiveRow.Owner.RenderActiveItemStyleClass||this.ActiveRow.Owner.RenderActiveItemStyleClass==""){
if(this.ActiveRow.Owner.RenderActiveItemStyle&&this.ActiveRow.Owner.RenderActiveItemStyle!=""){
RadGridNamespace.addClassName(this.ActiveRow.Control,"ActiveItemStyle"+this.ActiveRow.Owner.ClientID+"1");
}else{
RadGridNamespace.addClassName(this.ActiveRow.Control,"ActiveItemStyle"+this.ActiveRow.Owner.ClientID+"2");
}
}else{
RadGridNamespace.addClassName(this.ActiveRow.Control,this.ActiveRow.Owner.RenderActiveItemStyleClass);
}
this.SavePostData("ActiveRow",this.ActiveRow.Owner.ClientID,this.ActiveRow.RealIndex);
};
RadGridNamespace.RadGrid.prototype.GetNextRow=function(_d7,_d8){
if(_d7!=null){
if(_d7.tBodies[0].rows[_d8]!=null){
while(_d7.tBodies[0].rows[_d8]!=null){
_d8++;
if(_d8<=(_d7.tBodies[0].rows.length-1)){
return _d7.tBodies[0].rows[_d8];
}else{
return null;
}
}
}
}
};
RadGridNamespace.RadGrid.prototype.GetPreviousRow=function(_d9,_da){
if(_d9!=null){
if(_d9.tBodies[0].rows[_da]!=null){
while(_d9.tBodies[0].rows[_da]!=null){
_da--;
if(_da>=0){
return _d9.tBodies[0].rows[_da];
}else{
return null;
}
}
}
}
};
RadGridNamespace.RadGrid.prototype.GetNextHierarchicalRow=function(_db,_dc){
if(_db!=null){
if(_db.tBodies[0].rows[_dc]!=null){
_dc++;
var row=_db.tBodies[0].rows[_dc];
if(_db.tBodies[0].rows[_dc]!=null){
if((row.cells[1]!=null)&&(row.cells[2]!=null)){
if((row.cells[1].getElementsByTagName("table").length>0)||(row.cells[2].getElementsByTagName("table").length>0)){
var _de=this.GetNextRow(row.cells[2].firstChild,0);
return _de;
}else{
return null;
}
}
}
}
}
};
RadGridNamespace.RadGrid.prototype.GetPreviousHierarchicalRow=function(_df,_e0){
if(_df!=null){
if(_df.parentNode!=null){
if(_df.parentNode.tagName.toLowerCase()=="td"){
var _e1=_df.parentNode.parentNode.parentNode.parentNode;
var _e2=_df.parentNode.parentNode.rowIndex;
return this.GetPreviousRow(_e1,_e2);
}else{
return null;
}
}else{
return this.GetPreviousRow(_df,_e0);
}
}
};
RadGridNamespace.RadGrid.prototype.HandleCellEdit=function(e){
var _e4=RadGridNamespace.GetCurrentElement(e);
var _e5=RadGridNamespace.GetFirstParentByTagName(_e4,"td");
if(_e5!=null){
_e4=_e5;
var _e6=_e4.parentNode.parentNode.parentNode;
var _e7=this.GetTableObjectByID(_e6.id);
if((_e7!=null)&&(_e7.Columns.length>0)&&(_e7.Columns[_e4.cellIndex]!=null)){
if(_e7.Columns[_e4.cellIndex].ColumnType!="GridBoundColumn"){
return;
}
this.EditedCell=_e7.Control.rows[_e4.parentNode.rowIndex].cells[_e4.cellIndex];
this.CellEditor=new RadGridNamespace.RadGridCellEditor(this.EditedCell,_e7.Columns[_e4.cellIndex],this);
}
}
};
RadGridNamespace.RadGridCellEditor=function(_e8,_e9,_ea){
if(_ea.CellEditor){
return;
}
this.Control=document.createElement("input");
this.Control.style.border="1px groove";
this.Control.style.width="100%";
this.Control.value=_e8.innerHTML;
this.OldValue=this.Control.value;
_e8.innerHTML="";
var _eb=this;
this.Control.onblur=function(e){
if(!e){
var e=window.event;
}
_e8.removeChild(this);
_e8.innerHTML=this.value;
if(this.value!=_eb.OldValue){
alert(1);
}
_ea.CellEditor=null;
};
_e8.appendChild(this.Control);
if(this.Control.focus){
this.Control.focus();
}
};
if(!("console" in window)||!("firebug" in console)){
var names=["log","debug","info","warn","error","assert","dir","dirxml","group","groupEnd","time","timeEnd","count","trace","profile","profileEnd"];
window.console={};
for(var i=0;i<names.length;++i){
window.console[names[i]]=function(){
};
}
}
RadGridNamespace.Error=function(_ed,_ee,_ef){
if((!_ed)||(!_ee)||(!_ef)){
return false;
}
this.Message=_ed.message;
if(_ef!=null){
if("string"==typeof (_ef)){
try{
eval(_ef);
}
catch(e){
var _f0="";
_f0="";
_f0+="Telerik RadGrid Error:\r\n";
_f0+="-----------------\r\n";
_f0+="Message: \""+e.message+"\"\r\n";
_f0+="Raised by: "+_ee.Type+"\r\n";
alert(_f0);
}
}else{
if("function"==typeof (_ef)){
try{
_ef(this);
}
catch(e){
var _f0="";
_f0="";
_f0+="Telerik RadGrid Error:\r\n";
_f0+="-----------------\r\n";
_f0+="Message: \""+e.message+"\"\r\n";
_f0+="Raised by: "+_ee.Type+"\r\n";
alert(_f0);
}
}
}
}else{
this.Owner=_ee;
for(var _f1 in _ed){
this[_f1]=_ed[_f1];
}
this.Message="";
this.Message+="Telerik RadGrid Error:\r\n";
this.Message+="-----------------\r\n";
this.Message+="Message: \""+_ed.message+"\"\r\n";
this.Message+="Raised by: "+_ee.Type+"\r\n";
alert(this.Message);
}
};
RadGridNamespace.RadGrid.prototype.GetTableObjectByID=function(id){
if(this.MasterTableView.ClientID==id||this.MasterTableView.UID==id){
return this.MasterTableView;
}else{
for(var i=0;i<this.DetailTablesCollection.length;i++){
if(this.DetailTablesCollection[i].ClientID==id||this.DetailTablesCollection[i].UID==id){
return this.DetailTablesCollection[i];
}
}
}
if(this.MasterTableViewHeader!=null){
if(this.MasterTableViewHeader.ClientID==id||this.MasterTableViewHeader.UID==id){
return table=this.MasterTableViewHeader;
}
}
};
RadGridNamespace.RadGrid.prototype.GetRowObjectByRealRow=function(_f4,row){
if(_f4.Rows!=null){
for(var i=0;i<_f4.Rows.length;i++){
if(_f4.Rows[i].Control==row){
return _f4.Rows[i];
}
}
}
};
RadGridNamespace.RadGrid.prototype.SavePostData=function(){
try{
var _f7=new String();
for(var i=0;i<arguments.length;i++){
_f7+=arguments[i]+",";
}
_f7=_f7.substring(0,_f7.length-1);
if(this.PostDataValue!=null){
switch(arguments[0]){
case "ReorderedColumns":
this.PostDataValue.value+=_f7+";";
break;
case "HidedColumns":
var _f9=arguments[0]+","+arguments[1]+","+arguments[2];
this.UpdatePostData(_f7,_f9);
_f9="ShowedColumns"+","+arguments[1]+","+arguments[2];
this.UpdatePostData(_f7,_f9);
break;
case "ShowedColumns":
var _f9=arguments[0]+","+arguments[1]+","+arguments[2];
this.UpdatePostData(_f7,_f9);
_f9="HidedColumns"+","+arguments[1]+","+arguments[2];
this.UpdatePostData(_f7,_f9);
break;
case "HidedRows":
var _f9=arguments[0]+","+arguments[1]+","+arguments[2];
this.UpdatePostData(_f7,_f9);
_f9="ShowedRows"+","+arguments[1]+","+arguments[2];
this.UpdatePostData(_f7,_f9);
break;
case "ShowedRows":
var _f9=arguments[0]+","+arguments[1]+","+arguments[2];
this.UpdatePostData(_f7,_f9);
_f9="HidedRows"+","+arguments[1]+","+arguments[2];
this.UpdatePostData(_f7,_f9);
break;
case "ResizedColumns":
var _f9=arguments[0]+","+arguments[1]+","+arguments[2];
this.UpdatePostData(_f7,_f9);
break;
case "ResizedRows":
var _f9=arguments[0]+","+arguments[1]+","+arguments[2];
this.UpdatePostData(_f7,_f9);
break;
case "ResizedControl":
var _f9=arguments[0]+","+arguments[1];
this.UpdatePostData(_f7,_f9);
break;
case "ClientCreated":
var _f9=arguments[0]+","+arguments[1];
this.UpdatePostData(_f7,_f9);
break;
case "ScrolledControl":
var _f9=arguments[0]+","+arguments[1];
this.UpdatePostData(_f7,_f9);
break;
case "AJAXScrolledControl":
var _f9=arguments[0]+","+arguments[1];
this.UpdatePostData(_f7,_f9);
break;
case "SelectedRows":
var _f9=arguments[0]+","+arguments[1]+",";
this.UpdatePostData(_f7,_f9);
break;
case "EditRow":
var _f9=arguments[0]+","+arguments[1];
this.UpdatePostData(_f7,_f9);
break;
case "ActiveRow":
var _f9=arguments[0]+","+arguments[1];
this.UpdatePostData(_f7,_f9);
break;
case "CollapsedRows":
var _f9=arguments[0]+","+arguments[1]+","+arguments[2];
this.UpdatePostData(_f7,_f9);
_f9="ExpandedRows"+","+arguments[1]+","+arguments[2];
this.UpdatePostData(_f7,_f9);
break;
case "ExpandedRows":
var _f9=arguments[0]+","+arguments[1]+","+arguments[2];
this.UpdatePostData(_f7,_f9);
_f9="CollapsedRows"+","+arguments[1]+","+arguments[2];
this.UpdatePostData(_f7,_f9);
break;
case "CollapsedGroupRows":
var _f9=arguments[0]+","+arguments[1]+","+arguments[2];
this.UpdatePostData(_f7,_f9);
_f9="ExpandedGroupRows"+","+arguments[1]+","+arguments[2];
this.UpdatePostData(_f7,_f9);
break;
case "ExpandedGroupRows":
var _f9=arguments[0]+","+arguments[1]+","+arguments[2];
this.UpdatePostData(_f7,_f9);
_f9="CollapsedGroupRows"+","+arguments[1]+","+arguments[2];
this.UpdatePostData(_f7,_f9);
break;
default:
this.UpdatePostData(_f7,_f7);
break;
}
}
}
catch(error){
new RadGridNamespace.Error(error,this,this.OnError);
}
};
RadGridNamespace.RadGrid.prototype.UpdatePostData=function(_fa,_fb){
var _fc,_fd=new Array();
_fc=this.PostDataValue.value.split(";");
for(var i=0;i<_fc.length;i++){
if(_fc[i].indexOf(_fb)==-1){
_fd[_fd.length]=_fc[i];
}
}
this.PostDataValue.value=_fd.join(";");
this.PostDataValue.value+=_fa+";";
};
RadGridNamespace.RadGrid.prototype.DeletePostData=function(_ff,_100){
var _101,_102=new Array();
_101=this.PostDataValue.value.split(";");
for(var i=0;i<_101.length;i++){
if(_101[i].indexOf(_100)==-1){
_102[_102.length]=_101[i];
}
}
this.PostDataValue.value=_102.join(";");
};
RadGridNamespace.RadGrid.prototype.HandleDragAndDrop=function(e,_105){
try{
var _106=this;
if(_107!=null&&_107.Columns.length>0&&_107.Columns[_108]!=null&&!_107.Columns[_108].Reorderable){
this.Control.style.cursor="no-drop";
this.DisableDrop();
}else{
this.Control.style.cursor="";
}
if(this.MoveHeaderDiv!=null&&_105!=null&&_105.tagName.toLowerCase()!="th"&&!RadGridNamespace.IsChildOf(_105,this.MoveHeaderDivRefCell.parentNode)&&!(this.GroupPanelControl!=null&&RadGridNamespace.IsChildOf(_105,this.GroupPanelControl))){
this.Control.style.cursor="no-drop";
this.DisableDrop();
}else{
this.Control.style.cursor="";
}
if((_105!=null)&&(_105.tagName.toLowerCase()=="th")){
var _109=_105.parentNode.parentNode.parentNode;
var _107=this.GetTableObjectByID(_109.id);
var _108=RadGridNamespace.GetRealCellIndex(_107,_105);
if((_107!=null)&&(_107.Columns.length>0)&&(_107.Columns[_108]!=null)&&((_107.Columns[_108].Reorderable)||(_107.Owner.ClientSettings.AllowDragToGroup&&_107.Columns[_108].Groupable))){
var _10a=RadGridNamespace.GetEventPosX(e);
var _10b=RadGridNamespace.FindPosX(_105);
var endX=_10b+_105.offsetWidth;
this.ResizeTolerance=5;
var _10d=_105.title;
var _10e=_105.style.cursor;
if(!((_10a>=endX-this.ResizeTolerance)&&(_10a<=endX+this.ResizeTolerance))){
if(this.MoveHeaderDiv){
if(this.MoveHeaderDiv.innerHTML!=_105.innerHTML){
_105.title=this.ClientSettings.ClientMessages.DropHereToReorder;
_105.style.cursor="default";
if(_105.parentNode.parentNode.parentNode==this.MoveHeaderDivRefCell.parentNode.parentNode.parentNode){
this.MoveReorderIndicators(e,_105);
}else{
this.DisableDrop();
}
}
}else{
_105.title=this.ClientSettings.ClientMessages.DragToGroupOrReorder;
_105.style.cursor="move";
}
this.AttachDomEvent(_105,"mousedown","OnDragDropMouseDown");
}else{
_105.style.cursor=_10e;
_105.title="";
}
}
}
if(_107!=null&&_107.Columns.length>0&&_107.Columns[_108]!=null&&!_107.Columns[_108].Reorderable){
this.Control.style.cursor="no-drop";
this.DisableDrop();
}
if(this.MoveHeaderDiv!=null){
this.MoveHeaderDiv.style.visibility="";
this.MoveHeaderDiv.style.display="";
RadGridNamespace.RadGrid.PositionDragElement(this.MoveHeaderDiv,e);
}
}
catch(error){
new RadGridNamespace.Error(error,this,this.OnError);
}
};
RadGridNamespace.RadGrid.prototype.DisableDrop=function(e){
if(this.ReorderIndicator1!=null){
this.ReorderIndicator1.style.visibility="hidden";
this.ReorderIndicator1.style.display="none";
this.ReorderIndicator1.style.position="absolute";
}
if(this.ReorderIndicator2!=null){
this.ReorderIndicator2.style.visibility=this.ReorderIndicator1.style.visibility;
this.ReorderIndicator2.style.display=this.ReorderIndicator1.style.display;
this.ReorderIndicator2.style.position=this.ReorderIndicator1.style.position;
}
};
RadGridNamespace.RadGrid.PositionDragElement=function(_110,_111){
_110.style.top=_111.clientY+document.documentElement.scrollTop+document.body.scrollTop+1+"px";
_110.style.left=_111.clientX+document.documentElement.scrollLeft+document.body.scrollLeft+1+"px";
};
RadGridNamespace.RadGrid.prototype.OnDragDropMouseDown=function(e){
var _113=RadGridNamespace.GetCurrentElement(e);
var _114=false;
var form=document.getElementById(this.FormID);
if(form!=null&&form["__EVENTTARGET"]!=null&&form["__EVENTTARGET"].value!=""){
_114=true;
}
if((_113.tagName.toLowerCase()=="input"&&_113.type.toLowerCase()=="text")||(_113.tagName.toLowerCase()=="textarea")){
return;
}
_113=RadGridNamespace.GetFirstParentByTagName(_113,"th");
if(_113.tagName.toLowerCase()=="th"&&!this.IsResize){
if(((window.netscape||window.opera||navigator.userAgent.indexOf("Safari")!=-1)&&(e.button==0))||(e.button==1)){
this.CreateDragAndDrop(e,_113);
}
RadGridNamespace.ClearDocumentEvents();
this.DetachDomEvent(_113,"mousedown","OnDragDropMouseDown");
this.AttachDomEvent(document,"mouseup","OnDragDropMouseUp");
if(this.GroupPanelControl!=null){
this.AttachDomEvent(this.GroupPanelControl,"mouseup","OnDragDropMouseUp");
}
}
};
RadGridNamespace.RadGrid.prototype.OnDragDropMouseUp=function(e){
this.DetachDomEvent(document,"mouseup","OnDragDropMouseUp");
if(this.GroupPanelControl!=null){
this.DetachDomEvent(this.GroupPanelControl,"mouseup","OnDragDropMouseUp");
}
this.FireDropAction(e);
this.DestroyDragAndDrop(e);
RadGridNamespace.RestoreDocumentEvents();
};
RadGridNamespace.CopyAttributes=function(_117,_118){
for(var i=0;i<_118.attributes.length;i++){
try{
if(_118.attributes[i].name.toLowerCase()=="id"){
continue;
}
if(_118.attributes[i].value!=null&&_118.attributes[i].value!="null"&&_118.attributes[i].value!=""){
_117.setAttribute(_118.attributes[i].name,_118.attributes[i].value);
}
}
catch(e){
continue;
}
}
};
RadGridNamespace.RadGrid.prototype.CreateDragAndDrop=function(e,_11b){
this.MoveHeaderDivRefCell=_11b;
this.MoveHeaderDiv=document.createElement("div");
var _11c=document.createElement("table");
if(this.MoveHeaderDiv.mergeAttributes){
this.MoveHeaderDiv.mergeAttributes(this.Control);
}else{
RadGridNamespace.CopyAttributes(this.MoveHeaderDiv,this.Control);
}
if(_11c.mergeAttributes){
_11c.mergeAttributes(this.MasterTableView.Control);
}else{
RadGridNamespace.CopyAttributes(_11c,this.MasterTableView.Control);
}
_11c.style.margin="0px";
_11c.style.height=_11b.offsetHeight+"px";
_11c.style.width=_11b.offsetWidth+"px";
var _11d=document.createElement("thead");
var tr=document.createElement("tr");
_11c.appendChild(_11d);
_11d.appendChild(tr);
tr.appendChild(_11b.cloneNode(true));
this.MoveHeaderDiv.appendChild(_11c);
document.body.appendChild(this.MoveHeaderDiv);
this.MoveHeaderDiv.style.height=_11b.offsetHeight+"px";
this.MoveHeaderDiv.style.width=_11b.offsetWidth+"px";
this.MoveHeaderDiv.style.position="absolute";
RadGridNamespace.RadGrid.PositionDragElement(this.MoveHeaderDiv,e);
if(window.netscape){
this.MoveHeaderDiv.style.MozOpacity=3/4;
}else{
this.MoveHeaderDiv.style.filter="alpha(opacity=75);";
}
this.MoveHeaderDiv.style.cursor="move";
this.MoveHeaderDiv.style.visibility="hidden";
this.MoveHeaderDiv.style.display="none";
this.MoveHeaderDiv.style.fontWeight="bold";
this.MoveHeaderDiv.onmousedown=null;
RadGridNamespace.ClearDocumentEvents();
if(this.ClientSettings.AllowColumnsReorder){
this.CreateReorderIndicators(_11b);
}
};
RadGridNamespace.RadGrid.prototype.DestroyDragAndDrop=function(){
if(this.MoveHeaderDiv!=null){
var _11f=this.MoveHeaderDiv.parentNode;
_11f.removeChild(this.MoveHeaderDiv);
this.MoveHeaderDiv.onmouseup=null;
this.MoveHeaderDiv.onmousemove=null;
this.MoveHeaderDiv=null;
this.MoveHeaderDivRefCell=null;
this.DragCellIndex=null;
RadGridNamespace.RestoreDocumentEvents();
this.DestroyReorderIndicators();
}
};
RadGridNamespace.RadGrid.prototype.FireDropAction=function(e){
if((this.MoveHeaderDiv!=null)&&(this.MoveHeaderDiv.style.display!="none")){
var _121=RadGridNamespace.GetCurrentElement(e);
if((_121!=null)&&(this.MoveHeaderDiv!=null)){
if(_121!=this.MoveHeaderDivRefCell){
var _122=this.GetTableObjectByID(this.MoveHeaderDivRefCell.parentNode.parentNode.parentNode.id);
var _123=_122.HeaderRow;
if(RadGridNamespace.IsChildOf(_121,_123)){
if(_121.tagName.toLowerCase()!="th"){
_121=RadGridNamespace.GetFirstParentByTagName(_121,"th");
}
var _124=_121.parentNode.parentNode.parentNode;
var _125=this.MoveHeaderDivRefCell.parentNode.parentNode.parentNode;
if(_124.id==_125.id){
var _126=this.GetTableObjectByID(_124.id);
var _127=_121.cellIndex;
if((window.attachEvent&&!window.opera&&!window.netscape)||navigator.userAgent.indexOf("Safari")!=-1){
_127=RadGridNamespace.GetRealCellIndex(_126,_121);
}
var _128=this.MoveHeaderDivRefCell.cellIndex;
if((window.attachEvent&&!window.opera&&!window.netscape)||navigator.userAgent.indexOf("Safari")!=-1){
_128=RadGridNamespace.GetRealCellIndex(_126,this.MoveHeaderDivRefCell);
}
if(!_126||!_126.Columns[_127]){
return;
}
if(!_126.Columns[_127].Reorderable){
return;
}
_126.SwapColumns(_127,_128,(this.ClientSettings.ColumnsReorderMethod!="Reorder"));
if(this.ClientSettings.ColumnsReorderMethod=="Reorder"){
if((!this.ClientSettings.ReorderColumnsOnClient)&&(this.ClientSettings.PostBackReferences.PostBackColumnsReorder!="")){
eval(this.ClientSettings.PostBackReferences.PostBackColumnsReorder);
}
}
}
}else{
if(RadGridNamespace.CheckParentNodesFor(_121,this.GroupPanelControl)){
if((this.ClientSettings.PostBackReferences.PostBackGroupByColumn!="")&&(this.ClientSettings.AllowDragToGroup)){
var _126=this.GetTableObjectByID(this.MoveHeaderDivRefCell.parentNode.parentNode.parentNode.id);
var _129=this.MoveHeaderDivRefCell.cellIndex;
if((window.attachEvent&&!window.opera&&!window.netscape)||navigator.userAgent.indexOf("Safari")!=-1){
_129=RadGridNamespace.GetRealCellIndex(_126,this.MoveHeaderDivRefCell);
}
var _12a=_126.Columns[_129].RealIndex;
if(_126.Columns[_129].Groupable){
if(_126==this.MasterTableViewHeader){
this.SavePostData("GroupByColumn",this.MasterTableView.ClientID,_12a);
}else{
this.SavePostData("GroupByColumn",_126.ClientID,_12a);
}
eval(this.ClientSettings.PostBackReferences.PostBackGroupByColumn);
}
}
}
}
}
}
}
};
RadGridNamespace.GetRealCellIndex=function(_12b,cell){
for(var i=0;i<_12b.Columns.length;i++){
if(_12b.Columns[i].Control==cell){
return i;
}
}
};
RadGridNamespace.RadGrid.prototype.CreateReorderIndicators=function(_12e){
if((this.ReorderIndicator1==null)&&(this.ReorderIndicator2==null)){
var _12f=this.MoveHeaderDivRefCell.parentNode.parentNode.parentNode;
var _130=this.GetTableObjectByID(_12f.id);
var _131=_130.HeaderRow;
if(!RadGridNamespace.IsChildOf(_12e,_131)){
return;
}
this.ReorderIndicator1=document.createElement("span");
this.ReorderIndicator2=document.createElement("span");
if(this.Skin==""||this.Skin=="None"){
this.ReorderIndicator1.innerHTML="&darr;";
this.ReorderIndicator2.innerHTML="&uarr;";
}else{
this.ReorderIndicator1.className="TopReorderIndicator_"+this.Skin;
this.ReorderIndicator2.className="BottomReorderIndicator_"+this.Skin;
this.ReorderIndicator1.style.width=this.ReorderIndicator1.style.height=this.ReorderIndicator2.style.width=this.ReorderIndicator2.style.height="10px";
}
this.ReorderIndicator1.style.backgroundColor="transparent";
this.ReorderIndicator1.style.color="darkblue";
this.ReorderIndicator1.style.font="bold 18px Arial";
this.ReorderIndicator2.style.backgroundColor=this.ReorderIndicator1.style.backgroundColor;
this.ReorderIndicator2.style.color=this.ReorderIndicator1.style.color;
this.ReorderIndicator2.style.font=this.ReorderIndicator1.style.font;
this.ReorderIndicator1.style.top=RadGridNamespace.FindPosY(_12e)-this.ReorderIndicator1.offsetHeight+"px";
this.ReorderIndicator1.style.left=RadGridNamespace.FindPosX(_12e)+"px";
this.ReorderIndicator2.style.top=RadGridNamespace.FindPosY(_12e)+_12e.offsetHeight+"px";
this.ReorderIndicator2.style.left=this.ReorderIndicator1.style.left;
this.ReorderIndicator1.style.visibility="hidden";
this.ReorderIndicator1.style.display="none";
this.ReorderIndicator1.style.position="absolute";
this.ReorderIndicator2.style.visibility=this.ReorderIndicator1.style.visibility;
this.ReorderIndicator2.style.display=this.ReorderIndicator1.style.display;
this.ReorderIndicator2.style.position=this.ReorderIndicator1.style.position;
document.body.appendChild(this.ReorderIndicator1);
document.body.appendChild(this.ReorderIndicator2);
}
};
RadGridNamespace.RadGrid.prototype.DestroyReorderIndicators=function(){
if((this.ReorderIndicator1!=null)&&(this.ReorderIndicator2!=null)){
document.body.removeChild(this.ReorderIndicator1);
document.body.removeChild(this.ReorderIndicator2);
this.ReorderIndicator1=null;
this.ReorderIndicator2=null;
}
};
RadGridNamespace.RadGrid.prototype.MoveReorderIndicators=function(e,_133){
if((this.ReorderIndicator1!=null)&&(this.ReorderIndicator2!=null)){
this.ReorderIndicator1.style.visibility="visible";
this.ReorderIndicator1.style.display="";
this.ReorderIndicator2.style.visibility="visible";
this.ReorderIndicator2.style.display="";
var _134=0;
if(document.body.currentStyle&&document.body.currentStyle.margin.indexOf("px")!=-1){
_134=parseInt(document.body.currentStyle.marginTop);
}
_134-=(this.Skin==""||this.Skin=="None"&&navigator.userAgent.indexOf("Safari")==-1)?10:5;
if(navigator.userAgent.indexOf("Safari")!=-1){
_134-=10;
}
this.ReorderIndicator1.style.top=RadGridNamespace.FindPosY(_133)-this.ReorderIndicator1.offsetHeight/2+_134+"px";
this.ReorderIndicator1.style.left=RadGridNamespace.FindPosX(_133)-RadGridNamespace.FindScrollPosX(_133)+document.documentElement.scrollLeft+document.body.scrollLeft+"px";
if(parseInt(this.ReorderIndicator1.style.left)<RadGridNamespace.FindPosX(this.Control)){
this.ReorderIndicator1.style.left=RadGridNamespace.FindPosX(this.Control)+5;
}
this.ReorderIndicator2.style.top=parseInt(this.ReorderIndicator1.style.top)+_133.offsetHeight+this.ReorderIndicator2.offsetHeight+"px";
this.ReorderIndicator2.style.left=this.ReorderIndicator1.style.left;
this.ReorderIndicator2.style.zIndex=this.ReorderIndicator1.style.zIndex=99999;
}
};
RadGridNamespace.RadGrid.prototype.AttachDomEvents=function(){
try{
this.AttachDomEvent(this.Control,"mousemove","OnMouseMove");
this.AttachDomEvent(this.Control,"keydown","OnKeyDown");
this.AttachDomEvent(this.Control,"keyup","OnKeyUp");
this.AttachDomEvent(this.Control,"click","OnClick");
}
catch(error){
new RadGridNamespace.Error(error,this,this.OnError,this.OnError);
}
};
RadGridNamespace.RadGrid.prototype.OnMouseMove=function(e){
try{
var _136=RadGridNamespace.GetCurrentElement(e);
if(this.ClientSettings.Resizing.AllowRowResize){
this.DetectResizeCursorsOnRows(e,_136);
this.MoveRowResizer(e);
}
if((this.ClientSettings.AllowDragToGroup)||(this.ClientSettings.AllowColumnsReorder)){
this.HandleDragAndDrop(e,_136);
}
}
catch(error){
return false;
}
};
RadGridNamespace.RadGrid.prototype.OnKeyDown=function(e){
var _138={KeyCode:e.keyCode,IsShiftPressed:e.shiftKey,IsCtrlPressed:e.ctrlKey,IsAltPressed:e.altKey,Event:e};
if(!RadGridNamespace.FireEvent(this,"OnKeyPress",[_138])){
return;
}
if(e.keyCode==16){
this.IsShiftPressed=true;
}
if(e.keyCode==17){
this.IsCtrlPressed=true;
}
if(this.ClientSettings&&this.ClientSettings.AllowKeyboardNavigation){
this.ActiveRow.HandleActiveRow(e);
}
if(e.keyCode==27&&this.MoveHeaderDiv){
this.DestroyDragAndDrop();
}
};
RadGridNamespace.RadGrid.prototype.OnClick=function(e){
};
RadGridNamespace.RadGrid.prototype.OnKeyUp=function(e){
if(e.keyCode==16){
this.IsShiftPressed=false;
}
if(e.keyCode==17){
this.IsCtrlPressed=false;
}
};
RadGridNamespace.RadGrid.prototype.DetectResizeCursorsOnRows=function(e,_13c){
try{
var _13d=this;
if((_13c!=null)&&(_13c.tagName.toLowerCase()=="td")&&!this.MoveHeaderDiv){
var _13e=_13c.parentNode.parentNode.parentNode;
var _13f=this.GetTableObjectByID(_13e.id);
if(_13f!=null){
if(_13f.Columns!=null){
if(_13f.Columns[_13c.cellIndex].ColumnType!="GridRowIndicatorColumn"){
return;
}
}
if(!_13f.Control.tBodies[0]){
return;
}
var _140=this.GetRowObjectByRealRow(_13f,_13c.parentNode);
if(_140!=null){
var _141=RadGridNamespace.GetEventPosY(e);
var _142=RadGridNamespace.FindPosY(_13c);
var endY=_142+_13c.offsetHeight;
this.ResizeTolerance=5;
var _144=_13c.title;
if((_141>endY-this.ResizeTolerance)&&(_141<endY+this.ResizeTolerance)){
_13c.style.cursor="n-resize";
_13c.title=this.ClientSettings.ClientMessages.DragToResize;
this.AttachDomEvent(_13c,"mousedown","OnResizeMouseDown");
}else{
_13c.style.cursor="default";
_13c.title="";
this.DetachDomEvent(_13c,"mousedown","OnResizeMouseDown");
}
}
}
}
}
catch(error){
new RadGridNamespace.Error(error,this,this.OnError);
}
};
RadGridNamespace.RadGrid.prototype.OnResizeMouseDown=function(e){
this.CreateRowResizer(e);
RadGridNamespace.ClearDocumentEvents();
this.AttachDomEvent(document,"mouseup","OnResizeMouseUp");
};
RadGridNamespace.RadGrid.prototype.OnResizeMouseUp=function(e){
this.DetachDomEvent(document,"mouseup","OnResizeMouseUp");
this.DestroyRowResizerAndResizeRow(e,true);
RadGridNamespace.RestoreDocumentEvents();
};
RadGridNamespace.RadGrid.prototype.CreateRowResizer=function(e){
try{
this.DestroyRowResizer();
var _148=RadGridNamespace.GetCurrentElement(e);
if((_148!=null)&&(_148.tagName.toLowerCase()=="td")){
if(_148.cellIndex>0){
var _149=_148.parentNode.rowIndex;
_148=_148.parentNode.parentNode.parentNode.rows[_149].cells[0];
}
this.RowResizer=null;
this.CellToResize=_148;
var _14a=_148.parentNode.parentNode.parentNode;
var _14b=this.GetTableObjectByID(_14a.id);
this.RowResizer=document.createElement("div");
this.RowResizer.style.backgroundColor="navy";
this.RowResizer.style.height="1px";
this.RowResizer.style.fontSize="1";
this.RowResizer.style.position="absolute";
this.RowResizer.style.cursor="n-resize";
if(_14b!=null){
this.RowResizerRefTable=_14b;
if(this.GridDataDiv){
this.RowResizer.style.left=RadGridNamespace.FindPosX(this.GridDataDiv)+"px";
var _14c=(RadGridNamespace.FindPosX(this.GridDataDiv)+this.GridDataDiv.offsetWidth)-parseInt(this.RowResizer.style.left);
if(_14c>_14b.Control.offsetWidth){
this.RowResizer.style.width=_14b.Control.offsetWidth+"px";
}else{
this.RowResizer.style.width=_14c+"px";
}
if(parseInt(this.RowResizer.style.width)>this.GridDataDiv.offsetWidth){
this.RowResizer.style.width=this.GridDataDiv.offsetWidth+"px";
}
}else{
this.RowResizer.style.width=_14b.Control.offsetWidth+"px";
this.RowResizer.style.left=RadGridNamespace.FindPosX(_148)+"px";
}
}
this.RowResizer.style.top=RadGridNamespace.GetEventPosY(e)-(RadGridNamespace.GetEventPosY(e)-e.clientY)+document.body.scrollTop+document.documentElement.scrollTop+"px";
var _14d=document.body;
_14d.appendChild(this.RowResizer);
}
}
catch(error){
new RadGridNamespace.Error(error,this,this.OnError);
}
};
RadGridNamespace.RadGrid.prototype.DestroyRowResizerAndResizeRow=function(e,_14f){
try{
if((this.CellToResize!="undefined")&&(this.CellToResize!=null)&&(this.CellToResize.tagName.toLowerCase()=="td")&&(this.RowResizer!="undefined")&&(this.RowResizer!=null)){
var _150;
if(this.GridDataDiv){
_150=parseInt(this.RowResizer.style.top)+this.GridDataDiv.scrollTop-(RadGridNamespace.FindPosY(this.CellToResize));
}else{
_150=parseInt(this.RowResizer.style.top)-(RadGridNamespace.FindPosY(this.CellToResize));
}
if(_150>0){
var _151=this.CellToResize.parentNode.parentNode.parentNode;
var _152=this.GetTableObjectByID(_151.id);
if(_152!=null){
_152.ResizeRow(this.CellToResize.parentNode.rowIndex,_150);
}
}
}
if(_14f){
this.DestroyRowResizer();
}
}
catch(error){
new RadGridNamespace.Error(error,this,this.OnError);
}
};
RadGridNamespace.RadGrid.prototype.DestroyRowResizer=function(){
try{
if((this.RowResizer!="undefined")&&(this.RowResizer!=null)&&(this.RowResizer.parentNode!=null)){
var _153=this.RowResizer.parentNode;
_153.removeChild(this.RowResizer);
this.RowResizer=null;
this.RowResizerRefTable=null;
}
}
catch(error){
new RadGridNamespace.Error(error,this,this.OnError);
}
};
RadGridNamespace.RadGrid.prototype.MoveRowResizer=function(e){
try{
if((this.RowResizer!="undefined")&&(this.RowResizer!=null)&&(this.RowResizer.parentNode!=null)){
this.RowResizer.style.top=RadGridNamespace.GetEventPosY(e)-(RadGridNamespace.GetEventPosY(e)-e.clientY)+document.body.scrollTop+document.documentElement.scrollTop+"px";
if(this.ClientSettings.Resizing.EnableRealTimeResize){
this.DestroyRowResizerAndResizeRow(e,false);
this.UpdateRowResizerWidth(e);
}
}
}
catch(error){
new RadGridNamespace.Error(error,this,this.OnError);
}
};
RadGridNamespace.RadGrid.prototype.UpdateRowResizerWidth=function(e){
var _156=RadGridNamespace.GetCurrentElement(e);
if((_156!=null)&&(_156.tagName.toLowerCase()=="td")){
var _157=this.RowResizerRefTable;
if(_157!=null){
if(this.GridDataDiv){
var _158=(RadGridNamespace.FindPosX(this.GridDataDiv)+this.GridDataDiv.offsetWidth)-parseInt(this.RowResizer.style.left);
if(_158>_157.Control.offsetWidth){
this.RowResizer.style.width=_157.Control.offsetWidth+"px";
}else{
this.RowResizer.style.width=_158+"px";
}
if(parseInt(this.RowResizer.style.width)>this.GridDataDiv.offsetWidth){
this.RowResizer.style.width=this.GridDataDiv.offsetWidth+"px";
}
}else{
this.RowResizer.style.width=_157.Control.offsetWidth+"px";
}
}
}
};
RadGridNamespace.RadGrid.prototype.SetHeaderAndFooterDivsWidth=function(){
if((document.compatMode=="BackCompat"&&navigator.userAgent.toLowerCase().indexOf("msie")!=-1)||(navigator.userAgent.toLowerCase().indexOf("msie")!=-1&&navigator.userAgent.toLowerCase().indexOf("6.0")!=-1)){
if(this.ClientSettings.Scrolling.UseStaticHeaders){
if(this.GridHeaderDiv!=null&&this.GridDataDiv!=null&&this.GridHeaderDiv!=null){
this.GridHeaderDiv.style.width="100%";
if(this.GridHeaderDiv&&this.GridDataDiv){
if(this.GridDataDiv.offsetWidth>0){
this.GridHeaderDiv.style.width=this.GridDataDiv.offsetWidth-RadGridNamespace.GetScrollBarWidth()+"px";
}
}
if(this.GridHeaderDiv&&this.GridFooterDiv){
this.GridFooterDiv.style.width=this.GridHeaderDiv.style.width;
}
}
}
}
if(this.ClientSettings.Scrolling.AllowScroll&&this.ClientSettings.Scrolling.UseStaticHeaders){
var _159=RadGridNamespace.IsRightToLeft(this.GridHeaderDiv);
if((!_159&&this.GridHeaderDiv&&parseInt(this.GridHeaderDiv.style.marginRight)!=RadGridNamespace.GetScrollBarWidth())||(_159&&this.GridHeaderDiv&&parseInt(this.GridHeaderDiv.style.marginLeft)!=RadGridNamespace.GetScrollBarWidth())){
if(!_159){
this.GridHeaderDiv.style.marginRight=RadGridNamespace.GetScrollBarWidth()+"px";
this.GridHeaderDiv.style.marginLeft="";
}else{
this.GridHeaderDiv.style.marginLeft=RadGridNamespace.GetScrollBarWidth()+"px";
this.GridHeaderDiv.style.marginRight="";
}
}
if(this.GridHeaderDiv&&this.GridDataDiv){
if((this.GridDataDiv.clientWidth==this.GridDataDiv.offsetWidth)){
this.GridHeaderDiv.style.width="100%";
if(!_159){
this.GridHeaderDiv.style.marginRight="";
}else{
this.GridHeaderDiv.style.marginLeft="";
}
}
}
if(this.GroupPanelObject&&this.GroupPanelObject.Items.length>0&&navigator.userAgent.toLowerCase().indexOf("msie")!=-1){
if(this.MasterTableView&&this.MasterTableViewHeader){
this.MasterTableView.Control.style.width=this.MasterTableViewHeader.Control.offsetWidth+"px";
}
}
if(this.GridFooterDiv){
this.GridFooterDiv.style.marginRight=this.GridHeaderDiv.style.marginRight;
this.GridFooterDiv.style.marginLeft=this.GridHeaderDiv.style.marginLeft;
this.GridFooterDiv.style.width=this.GridHeaderDiv.style.width;
}
}
};
RadGridNamespace.RadGrid.prototype.SetDataDivHeight=function(){
if(this.GridDataDiv&&this.Control.style.height!=""){
this.GridDataDiv.style.height="10px";
var _15a=0;
if(this.GroupPanelObject){
_15a+=this.GroupPanelObject.Control.offsetHeight;
}
if(this.GridHeaderDiv){
_15a+=this.GridHeaderDiv.offsetHeight;
}
if(this.GridFooterDiv){
_15a+=this.GridFooterDiv.offsetHeight;
}
if(this.PagerControl){
_15a+=this.PagerControl.offsetHeight;
}
if(this.TopPagerControl){
_15a+=this.TopPagerControl.offsetHeight;
}
var _15b=this.Control.clientHeight-_15a;
if(_15b>0){
var _15c=this.Control.style.position;
if(window.netscape){
this.Control.style.position="absolute";
}
this.GridDataDiv.style.height=_15b+"px";
if(window.netscape){
this.Control.style.position=_15c;
}
}
}
};
RadGridNamespace.RadGrid.prototype.InitializeDimensions=function(){
try{
var _15d=this;
this.InitializeAutoLayout();
this.ApplyFrozenScroll();
if(!this.EnableAJAX){
this.OnWindowResize();
}else{
var _15e=function(){
_15d.OnWindowResize();
};
if(window.netscape&&!window.opera){
_15e();
}else{
setTimeout(_15e,0);
}
}
this.Control.RadResize=function(){
_15d.OnWindowResize();
};
if(navigator.userAgent.toLowerCase().indexOf("msie")!=-1){
setTimeout(function(){
_15d.AttachDomEvent(window,"resize","OnWindowResize");
},0);
}else{
this.AttachDomEvent(window,"resize","OnWindowResize");
}
this.Control.RadShow=function(){
_15d.OnWindowResize();
};
this.ClientSettings.Scrolling.FrozenColumnsCount+=this.MasterTableViewHeader.ExpandCollapseColumns.length+this.MasterTableViewHeader.GroupSplitterColumns.length;
}
catch(error){
new RadGridNamespace.Error(error,this,this.OnError);
}
};
RadGridNamespace.RadGrid.prototype.OnWindowResize=function(e){
this.SetHeaderAndFooterDivsWidth();
this.SetDataDivHeight();
var _160=this;
_160.ApplyFrozenScroll();
};
RadGridNamespace.RadGrid.prototype.InitializeAutoLayout=function(){
if(this.ClientSettings.Scrolling.AllowScroll&&this.ClientSettings.Scrolling.UseStaticHeaders){
if(this.MasterTableView&&this.MasterTableViewHeader){
if(this.MasterTableView.TableLayout!="Auto"||window.netscape||window.opera){
return;
}
this.MasterTableView.Control.style.tableLayout=this.MasterTableViewHeader.Control.style.tableLayout="";
var _161=this.MasterTableView.Control.tBodies[0].rows[this.ClientSettings.FirstDataRowClientRowIndex];
var _162=this.MasterTableViewHeader.HeaderRow;
var _163=Math.min(_162.cells.length,_161.cells.length);
for(var i=0;i<_163;i++){
var col=this.MasterTableViewHeader.ColGroup.Cols[i];
if(!col){
continue;
}
if(col.width!=""){
continue;
}
var _166=_162.cells[i].offsetWidth;
var _167=_161.cells[i].offsetWidth;
var _168=(_166>_167)?_166:_167;
if(this.MasterTableViewFooter&&this.MasterTableViewFooter.Control){
if(this.MasterTableViewFooter.Control.tBodies[0].rows[0]&&this.MasterTableViewFooter.Control.tBodies[0].rows[0].cells[i]){
if(this.MasterTableViewFooter.Control.tBodies[0].rows[0].cells[i].offsetWidth>_168){
_168=this.MasterTableViewFooter.Control.tBodies[0].rows[0].cells[i].offsetWidth;
}
}
}
if(_168<=0){
continue;
}
_162.cells[i].style.width=_161.cells[i].style.width=this.MasterTableView.ColGroup.Cols[i].width=col.width=_168;
if(this.MasterTableViewFooter&&this.MasterTableViewFooter.Control){
if(this.MasterTableViewFooter.Control.tBodies[0].rows[0]&&this.MasterTableViewFooter.Control.tBodies[0].rows[0].cells[i]){
this.MasterTableViewFooter.Control.tBodies[0].rows[0].cells[i].style.width=_168;
}
}
}
this.MasterTableView.Control.style.tableLayout=this.MasterTableViewHeader.Control.style.tableLayout="fixed";
if(this.MasterTableViewFooter&&this.MasterTableViewFooter.Control){
this.MasterTableViewFooter.Control.style.tableLayout="fixed";
}
if(window.netscape){
this.OnWindowResize();
}
}
}
};
RadGridNamespace.RadGrid.prototype.InitializeSaveScrollPosition=function(){
if(!this.ClientSettings.Scrolling.SaveScrollPosition){
return;
}
if(this.ClientSettings.Scrolling.ScrollTop!=""&&!this.ClientSettings.Scrolling.EnableAJAXScrollPaging){
this.GridDataDiv.scrollTop=this.ClientSettings.Scrolling.ScrollTop;
}
if(this.ClientSettings.Scrolling.ScrollLeft!=""){
var _169=document.getElementById(this.ClientID+"_Frozen");
if(this.GridHeaderDiv&&!_169){
this.GridHeaderDiv.scrollLeft=this.ClientSettings.Scrolling.ScrollLeft;
}
if(this.GridFooterDiv&&!_169){
this.GridFooterDiv.scrollLeft=this.ClientSettings.Scrolling.ScrollLeft;
}
if(_169){
_169.scrollLeft=this.ClientSettings.Scrolling.ScrollLeft;
}else{
this.GridDataDiv.scrollLeft=this.ClientSettings.Scrolling.ScrollLeft;
}
}
};
RadGridNamespace.RadGrid.prototype.InitializeAjaxScrollPaging=function(){
if(!this.ClientSettings.Scrolling.EnableAJAXScrollPaging){
return;
}
this.ScrollCounter=0;
this.CurrentAJAXScrollTop=0;
if(this.ClientSettings.Scrolling.AJAXScrollTop!=""){
this.CurrentAJAXScrollTop=this.ClientSettings.Scrolling.AJAXScrollTop;
}
var _16a=this.CurrentPageIndex*this.MasterTableView.PageSize*20;
var _16b=this.MasterTableView.PageCount*this.MasterTableView.PageSize*20;
var _16c=this.MasterTableView.Control;
var _16d=_16c.offsetHeight;
if(!window.opera){
_16c.style.marginTop=_16a+"px";
_16c.style.marginBottom=_16b-_16a-_16d+"px";
}else{
_16c.style.position="relative";
_16c.style.top=_16a+"px";
_16c.style.marginBottom=_16b-_16d+"px";
}
this.CurrentAJAXScrollTop=_16a;
this.GridDataDiv.scrollTop=_16a;
this.CreateScrollerToolTip();
this.AttachDomEvent(this.GridDataDiv,"scroll","OnAJAXScroll");
};
RadGridNamespace.RadGrid.prototype.CreateScrollerToolTip=function(){
var _16e=document.getElementById(this.ClientID+"ScrollerToolTip");
if(!_16e){
this.ScrollerToolTip=document.createElement("span");
this.ScrollerToolTip.id=this.ClientID+"ScrollerToolTip";
this.ScrollerToolTip.style.backgroundColor="#F5F5DC";
this.ScrollerToolTip.style.border="1px solid";
this.ScrollerToolTip.style.position="absolute";
this.ScrollerToolTip.style.display="none";
this.ScrollerToolTip.style.font="icon";
this.ScrollerToolTip.style.padding="2";
document.body.appendChild(this.ScrollerToolTip);
}
};
RadGridNamespace.RadGrid.prototype.HideScrollerToolTip=function(){
var _16f=this;
setTimeout(function(){
var _170=document.getElementById(_16f.ClientID+"ScrollerToolTip");
if(_170&&_170.parentNode){
_170.style.display="none";
}
},200);
};
RadGridNamespace.RadGrid.prototype.ShowScrollerTooltip=function(_171,_172){
var _173=document.getElementById(this.ClientID+"ScrollerToolTip");
if(_173){
_173.style.display="";
_173.style.top=parseInt(RadGridNamespace.FindPosY(this.GridDataDiv))+Math.round(this.GridDataDiv.offsetHeight*_171)+"px";
_173.style.left=parseInt(RadGridNamespace.FindPosX(this.GridDataDiv))+this.GridDataDiv.offsetWidth-(this.GridDataDiv.offsetWidth-this.GridDataDiv.clientWidth)-_173.offsetWidth+"px";
this.ApplyPagerTooltipText(_173,_172,this.MasterTableView.PageCount);
}
};
RadGridNamespace.RadGrid.prototype.ApplyPagerTooltipText=function(_174,_175,_176){
var _177=this.ClientSettings.ClientMessages.PagerTooltipFormatString;
var _178=/\{0[^\}]*\}/g;
var _179=/\{1[^\}]*\}/g;
var _17a=((_175==0)?1:_175+1);
var _17b=_176;
_177=_177.replace(_178,_17a).replace(_179,_17b);
_174.innerHTML=_177;
};
RadGridNamespace.RadGrid.prototype.InitializeScroll=function(){
var _17c=this;
var grid=this;
var _17e=function(){
grid.InitializeSaveScrollPosition();
};
if(window.netscape&&!window.opera){
window.setTimeout(_17e,0);
}else{
_17e();
}
this.InitializeAjaxScrollPaging();
this.AttachDomEvent(this.GridDataDiv,"scroll","OnGridScroll");
if(this.GridHeaderDiv){
this.AttachDomEvent(this.GridHeaderDiv,"scroll","OnGridScroll");
}
};
RadGridNamespace.RadGrid.prototype.ApplyFrozenScroll=function(){
this.isFrozenScroll=false;
if(this.MasterTableView.FrozenColumnsCount==0){
return;
}
var _17f=document.getElementById(this.ClientID+"_Frozen");
if(_17f){
var _180=document.getElementById(this.ClientID+"_FrozenScroll");
this.AttachDomEvent(_17f,"scroll","OnGridFrozenScroll");
if(this.MasterTableView.Control.offsetWidth>this.GridDataDiv.clientWidth){
if(!window.netscape&&navigator.userAgent.indexOf("Safari")==-1){
_17f.style.height="100%";
}else{
_17f.style.height="16px";
}
_180.style.width=this.GridDataDiv.scrollWidth+"px";
_180.style.height="16px";
if(this.ClientSettings.Scrolling.SaveScrollPosition&&this.ClientSettings.Scrolling.ScrollLeft!=""){
_17f.scrollLeft=this.ClientSettings.Scrolling.ScrollLeft;
}
if(this.GridDataDiv.style.overflowX!=null){
this.GridDataDiv.style.overflowX="hidden";
}else{
_17f.style.marginTop="-16px";
_17f.style.zIndex=99999;
_17f.style.position="relative";
}
if(window.netscape&&!window.opera){
var _181=RadGridNamespace.GetScrollBarWidth();
_17f.style.width=this.GridDataDiv.offsetWidth-_181+"px";
}
this.isFrozenScroll=true;
}else{
_17f.style.height="";
_180.style.width="";
this.GridDataDiv.style.overflow="auto";
this.isFrozenScroll=false;
}
}
};
RadGridNamespace.RadGrid.prototype.OnGridFrozenScroll=function(e){
var _183=(e.srcElement)?e.srcElement:e.target;
if(this.ClientSettings.Scrolling.FrozenColumnsCount>this.MasterTableViewHeader.Columns.length){
this.isFrozenScroll=false;
}
if(this.isFrozenScroll){
var _184=this.MasterTableViewHeader.Columns[this.ClientSettings.Scrolling.FrozenColumnsCount-1].Control;
var _185=RadGridNamespace.FindPosX(_184)-RadGridNamespace.FindScrollPosX(_184)+document.documentElement.scrollLeft+document.body.scrollLeft+_184.offsetWidth;
var _186=_183.scrollWidth-_185;
this.notFrozenColumns=[];
for(var i=this.ClientSettings.Scrolling.FrozenColumnsCount;i<this.MasterTableViewHeader.Columns.length;i++){
var _188=this.MasterTableViewHeader.Columns[i];
var _189=false;
if(window.netscape&&_188.Control.style.display=="none"){
_188.Control.style.display="table-cell";
_189=true;
}
this.notFrozenColumns[this.notFrozenColumns.length]={Index:i,Width:_188.Control.offsetWidth};
if(window.netscape&&_189){
_188.Control.style.display="none";
_189=false;
}
}
var _18a=RadGridNamespace.GetScrollBarWidth();
if(window.netscape&&!window.opera){
_18a=0;
}
var _18b=Math.floor(_183.scrollLeft/(_183.scrollWidth-_183.offsetWidth+_18a)*100);
var _18c=0;
var i=0;
while(i<this.notFrozenColumns.length-1){
var _188=this.notFrozenColumns[i];
var _18d=Math.floor(_188.Width/_186*100);
if(_18d+_18c<_18b){
this.MasterTableViewHeader.HideNotFrozenColumn(_188.Index);
_18c+=_18d;
}else{
this.MasterTableViewHeader.ShowNotFrozenColumn(_188.Index);
}
i++;
}
this.MasterTableView.Control.style.width=this.MasterTableViewHeader.Control.offsetWidth+"px";
if(this.MasterTableViewFooter){
this.MasterTableViewFooter.Control.style.width=this.MasterTableViewHeader.Control.offsetWidth+"px";
}
this.SavePostData("ScrolledControl",this.ClientID,this.GridDataDiv.scrollTop,_183.scrollLeft);
}else{
}
};
RadGridNamespace.RadGrid.prototype.OnGridScroll=function(e){
var _18f=(e.srcElement)?e.srcElement:e.target;
if(window.opera&&this.isFrozenScroll){
this.GridDataDiv.scrollLeft=this.GridHeaderDiv.scrollLeft=0;
return;
}
if(this.ClientSettings.Scrolling.UseStaticHeaders){
if(!this.isFrozenScroll){
if(this.GridHeaderDiv){
if(_18f==this.GridHeaderDiv){
this.GridDataDiv.scrollLeft=this.GridHeaderDiv.scrollLeft;
}
if(_18f==this.GridDataDiv){
this.GridHeaderDiv.scrollLeft=this.GridDataDiv.scrollLeft;
}
}
if(this.GridFooterDiv){
this.GridFooterDiv.scrollLeft=this.GridDataDiv.scrollLeft;
}
}else{
if(this.GridHeaderDiv){
this.GridHeaderDiv.scrollLeft=this.GridDataDiv.scrollLeft;
}
if(this.GridFooterDiv){
this.GridFooterDiv.scrollLeft=this.GridDataDiv.scrollLeft;
}
}
}
this.SavePostData("ScrolledControl",this.ClientID,this.GridDataDiv.scrollTop,this.GridDataDiv.scrollLeft);
var evt={};
evt.ScrollTop=this.GridDataDiv.scrollTop;
evt.ScrollLeft=this.GridDataDiv.scrollLeft;
evt.ScrollControl=this.GridDataDiv;
evt.IsOnTop=(this.GridDataDiv.scrollTop==0)?true:false;
evt.IsOnBottom=((this.GridDataDiv.scrollHeight-this.GridDataDiv.offsetHeight+16)==this.GridDataDiv.scrollTop)?true:false;
RadGridNamespace.FireEvent(this,"OnScroll",[evt]);
};
RadGridNamespace.RadGrid.prototype.OnAJAXScroll=function(e){
if(this.GridDataDiv){
this.CurrentScrollTop=this.GridDataDiv.scrollTop;
}
this.ScrollCounter++;
var _192=this;
RadGridNamespace.AJAXScrollHanlder=function(_193){
if(_192.ScrollCounter!=_193){
return;
}
if(_192.CurrentAJAXScrollTop!=_192.GridDataDiv.scrollTop){
if(_192.CurrentPageIndex==_194){
return;
}
var _195=_192.ClientID;
var _196=_192.MasterTableView.ClientID;
_192.SavePostData("AJAXScrolledControl",_192.GridDataDiv.scrollLeft,_192.LastScrollTop,_192.GridDataDiv.scrollTop,_194);
var _197=_192.ClientSettings.PostBackFunction;
_197=_197.replace("{0}",_192.UniqueID);
eval(_197);
}
_192.ScrollCounter=0;
_192.HideScrollerToolTip();
};
var evt={};
evt.ScrollTop=this.GridDataDiv.scrollTop;
evt.ScrollLeft=this.GridDataDiv.scrollLeft;
evt.ScrollControl=this.GridDataDiv;
evt.IsOnTop=(this.GridDataDiv.scrollTop==0)?true:false;
evt.IsOnBottom=((this.GridDataDiv.scrollHeight-this.GridDataDiv.offsetHeight+16)==this.GridDataDiv.scrollTop)?true:false;
RadGridNamespace.FireEvent(this,"OnScroll",[evt]);
var _199=this.GridDataDiv.scrollTop/(this.GridDataDiv.scrollHeight-this.GridDataDiv.offsetHeight+16);
var _194=Math.round((this.MasterTableView.PageCount-1)*_199);
setTimeout("RadGridNamespace.AJAXScrollHanlder("+this.ScrollCounter+")",500);
this.ShowScrollerTooltip(_199,_194);
};
RadGridNamespace.RadGridTable=function(_19a){
if((!_19a)||typeof (_19a)!="object"){
return;
}
for(var _19b in _19a){
this[_19b]=_19a[_19b];
}
this.Type="RadGridTable";
this.ServerID=this.ID;
this.SelectedRows=new Array();
this.SelectedCells=new Array();
this.SelectedColumns=new Array();
this.ExpandCollapseColumns=new Array();
this.GroupSplitterColumns=new Array();
this.HeaderRow=null;
};
RadGridNamespace.RadGridTable.prototype._constructor=function(_19c){
if((!_19c)||typeof (_19c)!="object"){
return;
}
this.Control=document.getElementById(this.ClientID);
if(!this.Control){
return;
}
this.ColGroup=RadGridNamespace.GetTableColGroup(this.Control);
if(!this.ColGroup){
return;
}
this.ColGroup.Cols=RadGridNamespace.GetTableColGroupCols(this.ColGroup);
this.Owner=_19c;
if(this.Owner.ClientSettings){
this.InitializeEvents(this.Owner.ClientSettings.ClientEvents);
this.Control.style.overflow=((this.Owner.ClientSettings.Resizing.ClipCellContentOnResize&&((this.Owner.ClientSettings.Resizing.AllowColumnResize)||(this.Owner.ClientSettings.Resizing.AllowRowResize)))||(this.Owner.ClientSettings.Scrolling.AllowScroll&&this.Owner.ClientSettings.Scrolling.UseStaticHeaders))?"hidden":"";
}
if(navigator.userAgent.toLowerCase().indexOf("msie")!=-1&&this.Control.style.tableLayout=="fixed"&&this.Control.style.width.indexOf("%")!=-1){
this.Control.style.width="";
}
this.CreateStyles();
if(this.Owner.ClientSettings&&this.Owner.ClientSettings.Scrolling.AllowScroll&&this.Owner.ClientSettings.Scrolling.UseStaticHeaders){
if(this.ClientID.indexOf("_Header")!=-1||this.ClientID.indexOf("_Detail")!=-1){
this.Columns=this.GetTableColumns(this.Control,this.RenderColumns);
}else{
this.Columns=this.Owner.MasterTableViewHeader.Columns;
this.ExpandCollapseColumns=this.Owner.MasterTableViewHeader.ExpandCollapseColumns;
this.GroupSplitterColumns=this.Owner.MasterTableViewHeader.GroupSplitterColumns;
}
}else{
this.Columns=this.GetTableColumns(this.Control,this.RenderColumns);
}
if(this.Owner.ClientSettings&&this.Owner.ClientSettings.ShouldCreateRows){
this.InitializeRows(this.Controls[0].Rows);
}
};
RadGridNamespace.RadGridTable.prototype.Dispose=function(){
if(this.ColGroup&&this.ColGroup.Cols){
this.ColGroup.Cols=null;
this.ColGroup=null;
}
this.Owner=null;
this.DisposeEvents();
this.ExpandCollapseColumns=null;
this.GroupSplitterColumns=null;
this.DisposeRows();
this.DisposeColumns();
this.RenderColumns=null;
this.SelectedRows=null;
this.ExpandCollapseColumns=null;
this.DetailTables=null;
this.DetailTablesCollection=null;
this.Control=null;
this.HeaderRow=null;
};
RadGridNamespace.RadGridTable.prototype.CreateStyles=function(){
if(!this.SelectedItemStyleClass||this.SelectedItemStyleClass==""){
if(this.SelectedItemStyle&&this.SelectedItemStyle!=""){
RadGridNamespace.AddRule(this.Owner.GridStyleSheet,".SelectedItemStyle"+this.ClientID+"1 td",this.SelectedItemStyle);
}else{
RadGridNamespace.AddRule(this.Owner.GridStyleSheet,".SelectedItemStyle"+this.ClientID+"2 td","background-color:Navy;color:White;");
}
}
RadGridNamespace.addClassName(this.Control,"grid"+this.ClientID);
if(window.netscape&&!window.opera){
RadGridNamespace.AddRule(this.Owner.GridStyleSheet,".grid"+this.ClientID+" td","overflow: hidden;-moz-user-select:-moz-none;");
RadGridNamespace.AddRule(this.Owner.GridStyleSheet,".grid"+this.ClientID+" th","overflow: hidden;-moz-user-select:-moz-none;");
}else{
if(window.opera||navigator.userAgent.indexOf("Safari")!=-1){
var _19d=this;
setTimeout(function(){
RadGridNamespace.AddRule(_19d.Owner.GridStyleSheet,".grid"+_19d.ClientID+" td","overflow: hidden;");
RadGridNamespace.AddRule(_19d.Owner.GridStyleSheet,".grid"+_19d.ClientID+" th","overflow: hidden;");
},100);
}else{
RadGridNamespace.AddRule(this.Owner.GridStyleSheet,".grid"+this.ClientID+" td","overflow: hidden; text-overflow: ellipsis;");
RadGridNamespace.AddRule(this.Owner.GridStyleSheet,".grid"+this.ClientID+" th","overflow: hidden; text-overflow: ellipsis;");
}
}
};
RadGridNamespace.RadGridTable.prototype.InitializeEvents=function(_19e){
for(clientEvent in _19e){
if(typeof (_19e[clientEvent])!="string"){
continue;
}
if(!this.Owner.IsClientEventName(clientEvent)){
if(_19e[clientEvent]!=""){
var _19f=_19e[clientEvent];
if(_19f.indexOf("(")!=-1){
this[clientEvent]=_19f;
}else{
this[clientEvent]=eval(_19f);
}
}else{
this[clientEvent]=null;
}
}
}
};
RadGridNamespace.RadGridTable.prototype.DisposeEvents=function(){
for(var _1a0 in RadGridNamespace.RadGridTable.ClientEventNames){
this[_1a0]=null;
}
};
RadGridNamespace.RadGridTable.prototype.InitializeRows=function(rows){
if(this.ClientID.indexOf("_Header")!=-1||this.ClientID.indexOf("_Footer")!=-1){
return;
}
try{
var _1a2=[];
for(var i=0;i<rows.length;i++){
if(!rows[i].Visible||rows[i].ClientRowIndex<0){
continue;
}
if(rows[i].ItemType=="THead"||rows[i].ItemType=="TFoot"){
continue;
}
RadGridNamespace.FireEvent(this,"OnRowCreating");
rows[i]._constructor(this);
_1a2[_1a2.length]=rows[i];
RadGridNamespace.FireEvent(this,"OnRowCreated",[rows[i]]);
}
this.Rows=_1a2;
}
catch(error){
new RadGridNamespace.Error(error,this,this.Owner.OnError);
}
};
RadGridNamespace.RadGridTable.prototype.DisposeRows=function(){
if(this.Rows!=null){
for(var i=0;i<this.Rows.length;i++){
var row=this.Rows[i];
row.Dispose();
}
this.Rows=null;
}
};
RadGridNamespace.RadGridTable.prototype.DisposeColumns=function(){
if(this.Columns!=null){
for(var i=0;i<this.Columns.length;i++){
var _1a7=this.Columns[i];
_1a7.Dispose();
}
this.Columns=null;
}
};
RadGridNamespace.RadGridTable.prototype.GetTableRows=function(_1a8,_1a9){
if(this.ClientID.indexOf("_Header")!=-1||this.ClientID.indexOf("_Footer")!=-1){
return;
}
try{
var _1aa=this;
var _1ab=new Array();
var j=0;
for(var i=0;i<_1a9.length;i++){
if((_1a9[i].ItemType=="THead")||(_1a9[i].ItemType=="TFoot")){
continue;
}
if((_1a9[i])&&(_1a9[i].Visible)){
RadGridNamespace.FireEvent(this,"OnRowCreating");
setTimeout(function(){
_1ab[_1ab.length]=_1a9[i]._constructor(_1aa);
},0);
RadGridNamespace.FireEvent(this,"OnRowCreated",[_1ab[j]]);
j++;
}
}
return _1ab;
}
catch(error){
new RadGridNamespace.Error(error,this,this.Owner.OnError);
}
};
RadGridNamespace.RadGridTable.prototype.GetTableHeaderRow=function(){
try{
if(this.Control.tHead){
for(var i=0;i<this.Control.tHead.rows.length;i++){
if(this.Control.tHead.rows[i]!=null){
if(this.Control.tHead.rows[i].cells[0]!=null){
if(this.Control.tHead.rows[i].cells[0].tagName!=null){
if(this.Control.tHead.rows[i].cells[0].tagName.toLowerCase()=="th"){
this.HeaderRow=this.Control.tHead.rows[i];
break;
}
}
}
}
}
}
}
catch(error){
new RadGridNamespace.Error(error,this,this.Owner.OnError);
}
};
RadGridNamespace.RadGridTable.prototype.GetTableColumns=function(_1af,_1b0){
try{
this.GetTableHeaderRow();
var _1b1=new Array();
if(!this.HeaderRow){
return;
}
if(!this.HeaderRow.cells[0]){
return;
}
var j=0;
for(var i=0;i<_1b0.length;i++){
if(_1b0[i].Visible){
RadGridNamespace.FireEvent(this,"OnColumnCreating");
_1b1[_1b1.length]=new RadGridNamespace.RadGridTableColumn(_1b0[i]);
_1b1[j]._constructor(this.HeaderRow.cells[j],this);
_1b1[j].RealIndex=i;
if(_1b0[i].ColumnType=="GridExpandColumn"){
this.ExpandCollapseColumns[this.ExpandCollapseColumns.length]=_1b1[j];
}
if(_1b0[i].ColumnType=="GridGroupSplitterColumn"){
this.GroupSplitterColumns[this.GroupSplitterColumns.length]=_1b1[j];
}
if(_1b0[i].ColumnType=="GridRowIndicatorColumn"){
if(this.ClientID.indexOf("_Header")!=-1){
this.Owner.ClientSettings.Scrolling.FrozenColumnsCount++;
}
}
RadGridNamespace.FireEvent(this,"OnColumnCreated",[_1b1[j]]);
j++;
}
}
return _1b1;
}
catch(error){
new RadGridNamespace.Error(error,this,this.Owner.OnError);
}
};
RadGridNamespace.RadGridTable.prototype.RemoveTableLayOut=function(){
this.masterTableLayOut=this.Owner.MasterTableView.Control.style.tableLayout;
this.detailTablesTableLayOut=new Array();
for(var i=0;i<this.Owner.DetailTablesCollection.length;i++){
this.detailTablesTableLayOut[this.detailTablesTableLayOut.length]=this.Owner.DetailTablesCollection[i].Control.style.tableLayout;
this.Owner.DetailTablesCollection[i].Control.style.tableLayout="";
}
};
RadGridNamespace.RadGridTable.prototype.RestoreTableLayOut=function(){
this.Owner.MasterTableView.Control.style.tableLayout=this.masterTableLayOut;
for(var i=0;i<this.Owner.DetailTablesCollection.length;i++){
this.Owner.DetailTablesCollection[i].Control.style.tableLayout=this.detailTablesTableLayOut[i];
}
};
RadGridNamespace.RadGridTable.prototype.SelectRow=function(row,_1b7){
try{
if(!this.Owner.ClientSettings.Selecting.AllowRowSelect){
return;
}
var _1b8=this.Owner.GetRowObjectByRealRow(this,row);
if(_1b8!=null){
if(_1b8.ItemType=="Item"||_1b8.ItemType=="AlternatingItem"){
_1b8.SetSelected(_1b7);
}
}
}
catch(error){
new RadGridNamespace.Error(error,this,this.Owner.OnError);
}
};
RadGridNamespace.RadGridTable.prototype.DeselectRow=function(row){
try{
if(!this.Owner.ClientSettings.Selecting.AllowRowSelect){
return;
}
var _1ba=this.Owner.GetRowObjectByRealRow(this,row);
if(_1ba!=null){
if(_1ba.ItemType=="Item"||_1ba.ItemType=="AlternatingItem"){
this.RemoveFromSelectedRows(_1ba);
_1ba.RemoveSelectedRowStyle();
_1ba.Selected=false;
_1ba.CheckClientSelectColumns();
}
}
}
catch(error){
new RadGridNamespace.Error(error,this,this.Owner.OnError);
}
};
RadGridNamespace.RadGridTable.prototype.ResizeRow=function(_1bb,_1bc,_1bd){
try{
if(!this.Owner.ClientSettings.Resizing.AllowRowResize){
return;
}
if(!RadGridNamespace.FireEvent(this,"OnRowResizing",[_1bb,_1bc])){
return;
}
this.RemoveTableLayOut();
var _1be=this.Control.style.tableLayout;
this.Control.style.tableLayout="";
var _1bf=this.Control.parentNode.parentNode.parentNode.parentNode;
var _1c0=this.Owner.GetTableObjectByID(_1bf.id);
var _1c1;
if(_1c0!=null){
_1c1=_1c0.Control.style.tableLayout;
_1c0.Control.style.tableLayout="";
}
if(!_1bd){
if(this.Control){
if(this.Control.rows[_1bb]){
if(this.Control.rows[_1bb].cells[0]){
this.Control.rows[_1bb].cells[0].style.height=_1bc+"px";
this.Control.rows[_1bb].style.height=_1bc+"px";
}
}
}
}else{
if(this.Control){
if(this.Control.tBodies[0]){
if(this.Control.tBodies[0].rows[_1bb]){
if(this.Control.tBodies[0].rows[_1bb].cells[0]){
this.Control.tBodies[0].rows[_1bb].cells[0].style.height=_1bc+"px";
this.Control.tBodies[0].rows[_1bb].style.height=_1bc+"px";
}
}
}
}
}
this.Control.style.tableLayout=_1be;
if(_1c0!=null){
_1c0.Control.style.tableLayout=_1c1;
}
this.RestoreTableLayOut();
var _1c2=this.Owner.GetRowObjectByRealRow(this,this.Control.rows[_1bb]);
this.Owner.SavePostData("ResizedRows",this.Control.id,_1c2.RealIndex,_1bc+"px");
RadGridNamespace.FireEvent(this,"OnRowResized",[_1bb,_1bc]);
}
catch(error){
new RadGridNamespace.Error(error,this,this.Owner.OnError);
}
};
RadGridNamespace.RadGridTable.prototype.ResizeColumn=function(_1c3,_1c4){
if(isNaN(parseInt(_1c3))){
var _1c5="Column index must be of type \"Number\"!";
alert(_1c5);
return;
}
if(isNaN(parseInt(_1c4))){
var _1c5="Column width must be of type \"Number\"!";
alert(_1c5);
return;
}
if(_1c3<0){
var _1c5="Column index must be non-negative!";
alert(_1c5);
return;
}
if(_1c4<0){
var _1c5="Column width must be non-negative!";
alert(_1c5);
return;
}
if(_1c3>(this.Columns.length-1)){
var _1c5="Column index must be less than columns count!";
alert(_1c5);
return;
}
if(!this.Owner.ClientSettings.Resizing.AllowColumnResize){
return;
}
if(!this.Columns){
return;
}
if(!this.Columns[_1c3].Resizable){
return;
}
if(!RadGridNamespace.FireEvent(this,"OnColumnResizing",[_1c3,_1c4])){
return;
}
try{
if(this==this.Owner.MasterTableView&&this.Owner.MasterTableViewHeader){
this.Owner.MasterTableViewHeader.ResizeColumn(_1c3,_1c4);
}
var _1c6=this.Control.clientWidth;
var _1c7=this.Owner.Control.clientWidth;
if(this.HeaderRow){
var _1c8=this.HeaderRow.cells[_1c3].scrollWidth-_1c4;
}
if(window.netscape||window.opera){
if(this.HeaderRow){
if(this.HeaderRow.cells[_1c3]){
this.HeaderRow.cells[_1c3].style.width=_1c4+"px";
}
}
if(this==this.Owner.MasterTableViewHeader){
var _1c9=this.Owner.MasterTableView.Control.tBodies[0].rows[this.Owner.ClientSettings.FirstDataRowClientRowIndex];
if(_1c9){
if(_1c9.cells[_1c3]){
_1c9.cells[_1c3].style.width=_1c4+"px";
}
}
if(this.Owner.MasterTableViewFooter&&this.Owner.MasterTableViewFooter.Control){
if(this.Owner.MasterTableViewFooter.Control.tBodies[0].rows[0]&&this.Owner.MasterTableViewFooter.Control.tBodies[0].rows[0].cells[_1c3]){
if(_1c4>0){
this.Owner.MasterTableViewFooter.Control.tBodies[0].rows[0].cells[_1c3].style.width=_1c4+"px";
}
}
}
}
}
if(this.ColGroup){
if(this.ColGroup.Cols[_1c3]){
if(_1c4>0){
this.ColGroup.Cols[_1c3].width=_1c4+"px";
}
}
}
if(this==this.Owner.MasterTableViewHeader){
if(this.Owner.MasterTableView.ColGroup){
if(this.Owner.MasterTableView.ColGroup.Cols[_1c3]){
if(_1c4>0){
this.Owner.MasterTableView.ColGroup.Cols[_1c3].width=_1c4+"px";
}
}
}
if(this.Owner.MasterTableViewFooter&&this.Owner.MasterTableViewFooter.ColGroup){
if(this.Owner.MasterTableViewFooter.ColGroup.Cols[_1c3]){
if(_1c4>0){
this.Owner.MasterTableViewFooter.ColGroup.Cols[_1c3].width=_1c4+"px";
}
}
}
}
if(_1c4.toString().indexOf("px")!=-1){
_1c4=_1c4.replace("px","");
}
if(this==this.Owner.MasterTableView||this==this.Owner.MasterTableViewHeader){
if(_1c4.toString().indexOf("%")!=-1){
this.Owner.SavePostData("ResizedColumns",this.Owner.MasterTableView.ClientID,this.Columns[_1c3].RealIndex,_1c4);
}else{
this.Owner.SavePostData("ResizedColumns",this.Owner.MasterTableView.ClientID,this.Columns[_1c3].RealIndex,_1c4+"px");
}
}else{
if(_1c4.toString().indexOf("%")!=-1){
this.Owner.SavePostData("ResizedColumns",this.ClientID,this.Columns[_1c3].RealIndex,_1c4);
}else{
this.Owner.SavePostData("ResizedColumns",this.ClientID,this.Columns[_1c3].RealIndex,_1c4+"px");
}
}
if(this.Owner.MasterTableViewHeader){
this.Owner.ClientSettings.Resizing.ResizeGridOnColumnResize=true;
}
if(this.Owner.ClientSettings.Resizing.ResizeGridOnColumnResize){
if(this==this.Owner.MasterTableViewHeader){
for(var i=0;i<this.ColGroup.Cols.length;i++){
if(i!=_1c3&&this.ColGroup.Cols[i].width==""){
this.ColGroup.Cols[i].width=this.HeaderRow.cells[i].scrollWidth+"px";
this.Owner.MasterTableView.ColGroup.Cols[i].width=this.ColGroup.Cols[i].width;
if(this.Owner.MasterTableViewFooter&&this.Owner.MasterTableViewFooter.ColGroup){
this.Owner.MasterTableViewFooter.ColGroup.Cols[i].width=this.ColGroup.Cols[i].width;
}
}
}
this.Control.style.width=(this.Control.offsetWidth-_1c8)+"px";
this.Owner.MasterTableView.Control.style.width=this.Control.style.width;
if(this.Owner.MasterTableViewFooter&&this.Owner.MasterTableViewFooter.Control){
this.Owner.MasterTableViewFooter.Control.style.width=this.Control.style.width;
}
var _1cb=(this.Control.scrollWidth>this.Control.offsetWidth)?this.Control.scrollWidth:this.Control.offsetWidth;
var _1cc=this.Owner.GridDataDiv.offsetWidth;
this.Owner.SavePostData("ResizedControl",this.ClientID,_1cb+"px",_1cc+"px",this.Owner.Control.offsetHeight+"px");
}else{
if(window.netscape||window.opera){
this.Control.style.width=(this.Control.offsetWidth-_1c8)+"px";
this.Owner.Control.style.width=this.Control.style.width;
}
var _1cb=(this.Control.scrollWidth>this.Control.offsetWidth)?this.Control.scrollWidth:this.Control.offsetWidth;
this.Owner.SavePostData("ResizedControl",this.ClientID,_1cb+"px",this.Owner.Control.offsetWidth+"px",this.Owner.Control.offsetHeight+"px");
}
}else{
var _1cd=(this.Control.offsetWidth-_1c7)/this.ColGroup.Cols.length;
var _1ce="";
for(var i=_1c3+1;i<this.ColGroup.Cols.length;i++){
var _1cf=0;
if(this.ColGroup.Cols[i].width!=""){
_1cf=parseInt(this.ColGroup.Cols[i].width)-_1cd;
}
if(this.HeaderRow){
_1cf=this.HeaderRow.cells[i].scrollWidth-_1cd;
}
this.ColGroup.Cols[i].width="";
if(this==this.Owner.MasterTableViewHeader){
this.Owner.MasterTableView.ColGroup.Cols[i].width="";
}
if(this.Owner.MasterTableViewFooter){
this.Owner.MasterTableViewFooter.ColGroup.Cols[i].width="";
}
}
if(_1c7>0){
this.Owner.Control.style.width=_1c7+"px";
}
this.Control.style.width=_1c6+"px";
if(this==this.Owner.MasterTableViewHeader){
this.Owner.MasterTableView.Control.style.width=this.Control.style.width;
}
if(this.Owner.MasterTableViewFooter){
this.Owner.MasterTableViewFooter.Control.style.width=this.Control.style.width;
}
}
if(this.Owner.GroupPanelObject&&this.Owner.GroupPanelObject.Items.length>0&&navigator.userAgent.toLowerCase().indexOf("msie")!=-1){
if(this.Owner.MasterTableView&&this.Owner.MasterTableViewHeader){
this.Owner.MasterTableView.Control.style.width=this.Owner.MasterTableViewHeader.Control.offsetWidth+"px";
}
}
RadGridNamespace.FireEvent(this,"OnColumnResized",[_1c3,_1c4]);
if(window.netscape){
this.Control.style.cssText=this.Control.style.cssText;
}
this.Owner.ApplyFrozenScroll();
}
catch(error){
new RadGridNamespace.Error(error,this,this.Owner.OnError);
}
};
RadGridNamespace.RadGridTable.prototype.ReorderColumns=function(_1d0,_1d1){
if(isNaN(parseInt(_1d0))){
var _1d2="First column index must be of type \"Number\"!";
alert(_1d2);
return;
}
if(isNaN(parseInt(_1d1))){
var _1d2="Second column index must be of type \"Number\"!";
alert(_1d2);
return;
}
if(_1d0<0){
var _1d2="First column index must be non-negative!";
alert(_1d2);
return;
}
if(_1d1<0){
var _1d2="Second column index must be non-negative!";
alert(_1d2);
return;
}
if(_1d0>(this.Columns.length-1)){
var _1d2="First column index must be less than columns count!";
alert(_1d2);
return;
}
if(_1d1>(this.Columns.length-1)){
var _1d2="Second column index must be less than columns count!";
alert(_1d2);
return;
}
if(!this.Owner.ClientSettings.AllowColumnsReorder){
return;
}
if(!this.Columns){
return;
}
if(!this.Columns[_1d0].Reorderable){
return;
}
if(!this.Columns[_1d1].Reorderable){
return;
}
this.SwapColumns(_1d0,_1d1);
if((!this.Owner.ClientSettings.ReorderColumnsOnClient)&&(this.Owner.ClientSettings.PostBackReferences.PostBackColumnsReorder!="")){
if(this==this.Owner.MasterTableView){
eval(this.Owner.ClientSettings.PostBackReferences.PostBackColumnsReorder);
}
}
};
RadGridNamespace.RadGridTable.prototype.SwapColumns=function(_1d3,_1d4,_1d5){
if(isNaN(parseInt(_1d3))){
var _1d6="First column index must be of type \"Number\"!";
alert(_1d6);
return;
}
if(isNaN(parseInt(_1d4))){
var _1d6="Second column index must be of type \"Number\"!";
alert(_1d6);
return;
}
if(_1d3<0){
var _1d6="First column index must be non-negative!";
alert(_1d6);
return;
}
if(_1d4<0){
var _1d6="Second column index must be non-negative!";
alert(_1d6);
return;
}
if(_1d3>(this.Columns.length-1)){
var _1d6="First column index must be less than columns count!";
alert(_1d6);
return;
}
if(_1d4>(this.Columns.length-1)){
var _1d6="Second column index must be less than columns count!";
alert(_1d6);
return;
}
if(!this.Owner.ClientSettings.AllowColumnsReorder){
return;
}
if(!this.Columns){
return;
}
if(!this.Columns[_1d3].Reorderable){
return;
}
if(!this.Columns[_1d4].Reorderable){
return;
}
try{
if(this==this.Owner.MasterTableView&&this.Owner.MasterTableViewHeader){
this.Owner.MasterTableViewHeader.SwapColumns(_1d3,_1d4,!this.Owner.ClientSettings.ReorderColumnsOnClient);
return;
}
if(typeof (_1d5)=="undefined"){
_1d5=true;
}
if(this.Owner.ClientSettings.ColumnsReorderMethod=="Reorder"){
if(_1d4>_1d3){
while(_1d3+1<_1d4){
this.SwapColumns(_1d4-1,_1d4,false);
_1d4--;
}
}else{
while(_1d4<_1d3-1){
this.SwapColumns(_1d4+1,_1d4,false);
_1d4++;
}
}
}
if(!RadGridNamespace.FireEvent(this,"OnColumnSwapping",[_1d3,_1d4])){
return;
}
var _1d7=this.Control;
var _1d8=this.Columns[_1d3];
var _1d9=this.Columns[_1d4];
this.Columns[_1d3]=_1d9;
this.Columns[_1d4]=_1d8;
var _1da=this.ColGroup.Cols[_1d3].width;
if(_1da==""&&this.HeaderRow){
_1da=this.HeaderRow.cells[_1d3].offsetWidth;
}
var _1db=this.ColGroup.Cols[_1d4].width;
if(_1db==""&&this.HeaderRow){
_1db=this.HeaderRow.cells[_1d4].offsetWidth;
}
var _1dc=this.Owner.ClientSettings.Resizing.AllowColumnResize;
var _1dd=(typeof (this.Columns[_1d3].Resizable)=="boolean")?this.Columns[_1d3].Resizable:false;
var _1de=(typeof (this.Columns[_1d4].Resizable)=="boolean")?this.Columns[_1d4].Resizable:false;
this.Owner.ClientSettings.Resizing.AllowColumnResize=true;
this.Columns[_1d3].Resizable=true;
this.Columns[_1d4].Resizable=true;
this.ResizeColumn(_1d3,_1db);
this.ResizeColumn(_1d4,_1da);
this.Owner.ClientSettings.Resizing.AllowColumnResize=_1dc;
this.Columns[_1d3].Resizable=_1dd;
this.Columns[_1d4].Resizable=_1de;
if(navigator.userAgent.indexOf("Safari")!=-1){
var _1df=this.Columns[_1d3].Control;
var _1e0=this.Columns[_1d4].Control;
this.Columns[_1d3].Control=_1e0;
this.Columns[_1d4].Control=_1df;
}
var _1e1=(this==this.Owner.MasterTableViewHeader)?this.Owner.MasterTableView.ClientID:this.ClientID;
this.Owner.SavePostData("ReorderedColumns",_1e1,this.Columns[_1d3].UniqueName,this.Columns[_1d4].UniqueName);
for(var i=0;i<_1d7.rows.length;i++){
if(_1d7.rows[i]!=null){
if((_1d7.rows[i].cells[_1d3]!=null)&&(_1d7.rows[i].cells[_1d4]!=null)){
if(_1d7.rows[i].cells[_1d3].innerHTML!=null){
var _1e3=_1d7.rows[i].cells[_1d3].innerHTML;
var _1e4=_1d7.rows[i].cells[_1d4].innerHTML;
_1d7.rows[i].cells[_1d3].innerHTML=_1e4;
_1d7.rows[i].cells[_1d4].innerHTML=_1e3;
}
}
}
}
if(this.Owner.MasterTableViewHeader==this){
var _1d7=this.Owner.MasterTableView.Control;
for(var i=0;i<_1d7.rows.length;i++){
if(_1d7.rows[i]!=null){
if((_1d7.rows[i].cells[_1d3]!=null)&&(_1d7.rows[i].cells[_1d4]!=null)){
if(_1d7.rows[i].cells[_1d3].innerHTML!=null){
var _1e3=_1d7.rows[i].cells[_1d3].innerHTML;
var _1e4=_1d7.rows[i].cells[_1d4].innerHTML;
_1d7.rows[i].cells[_1d3].innerHTML=_1e4;
_1d7.rows[i].cells[_1d4].innerHTML=_1e3;
}
}
}
}
}
if(_1d5&&(!this.Owner.ClientSettings.ReorderColumnsOnClient)&&(this.Owner.ClientSettings.PostBackReferences.PostBackColumnsReorder!="")){
eval(this.Owner.ClientSettings.PostBackReferences.PostBackColumnsReorder);
}
RadGridNamespace.FireEvent(this,"OnColumnSwapped",[_1d3,_1d4]);
this.Owner.InitializeFilterMenu(this);
}
catch(error){
new RadGridNamespace.Error(error,this,this.Owner.OnError);
}
};
RadGridNamespace.RadGridTable.prototype.MoveColumnToLeft=function(_1e5){
if(isNaN(parseInt(_1e5))){
var _1e6="Column index must be of type \"Number\"!";
alert(_1e6);
return;
}
if(_1e5<0){
var _1e6="Column index must be non-negative!";
alert(_1e6);
return;
}
if(_1e5>(this.Columns.length-1)){
var _1e6="Column index must be less than columns count!";
alert(_1e6);
return;
}
if(!this.Owner.ClientSettings.AllowColumnsReorder){
return;
}
try{
if(!RadGridNamespace.FireEvent(this,"OnColumnMovingToLeft",[_1e5])){
return;
}
var _1e7=_1e5--;
this.SwapColumns(_1e5,_1e7);
RadGridNamespace.FireEvent(this,"OnColumnMovedToLeft",[_1e5]);
}
catch(error){
new RadGridNamespace.Error(error,this,this.Owner.OnError);
}
};
RadGridNamespace.RadGridTable.prototype.MoveColumnToRight=function(_1e8){
if(isNaN(parseInt(_1e8))){
var _1e9="Column index must be of type \"Number\"!";
alert(_1e9);
return;
}
if(_1e8<0){
var _1e9="Column index must be non-negative!";
alert(_1e9);
return;
}
if(_1e8>(this.Columns.length-1)){
var _1e9="Column index must be less than columns count!";
alert(_1e9);
return;
}
if(!this.Owner.ClientSettings.AllowColumnsReorder){
return;
}
try{
if(!RadGridNamespace.FireEvent(this,"OnColumnMovingToRight",[_1e8])){
return;
}
var _1ea=_1e8++;
this.SwapColumns(_1e8,_1ea);
RadGridNamespace.FireEvent(this,"OnColumnMovedToRight",[_1e8]);
}
catch(error){
new RadGridNamespace.Error(error,this,this.Owner.OnError);
}
};
RadGridNamespace.RadGridTable.prototype.CanShowHideColumn=function(_1eb){
if(!this.Owner.ClientSettings.AllowColumnHide){
return false;
}
if(isNaN(parseInt(_1eb))){
var _1ec="Column index must be of type \"Number\"!";
alert(_1ec);
return false;
}
if(_1eb<0){
var _1ec="Column index must be non-negative!";
alert(_1ec);
return false;
}
if(_1eb>(this.Columns.length-1)){
var _1ec="Column index must be less than columns count!";
alert(_1ec);
return false;
}
return true;
};
RadGridNamespace.RadGridTable.prototype.HideNotFrozenColumn=function(_1ed){
this.HideShowNotFrozenColumn(_1ed,false);
};
RadGridNamespace.RadGridTable.prototype.ShowNotFrozenColumn=function(_1ee){
this.HideShowNotFrozenColumn(_1ee,true);
};
RadGridNamespace.RadGridTable.prototype.HideShowNotFrozenColumn=function(_1ef,_1f0){
if(this.Owner.MasterTableViewHeader){
this.Owner.MasterTableViewHeader.Columns[_1ef].FrozenDisplay=_1f0;
if(!window.netscape){
this.HideShowCol(this.Owner.MasterTableViewHeader,_1ef,_1f0);
}else{
RadGridNamespace.HideShowCells(this.Owner.MasterTableViewHeader.Control,_1ef,_1f0);
}
}
if(this.Owner.MasterTableView){
this.Owner.MasterTableView.Columns[_1ef].FrozenDisplay=_1f0;
if(!window.netscape){
this.HideShowCol(this.Owner.MasterTableView,_1ef,_1f0);
}else{
RadGridNamespace.HideShowCells(this.Owner.MasterTableView.Control,_1ef,_1f0);
}
}
if(this.Owner.MasterTableViewFooter){
this.Owner.MasterTableViewFooter.Columns[_1ef].FrozenDisplay=_1f0;
if(!window.netscape){
this.HideShowCol(this.Owner.MasterTableViewFooter,_1ef,_1f0);
}else{
RadGridNamespace.HideShowCells(this.Owner.MasterTableViewFooter.Control,_1ef,_1f0);
}
}
};
RadGridNamespace.RadGridTable.prototype.HideColumn=function(_1f1){
if(!this.CanShowHideColumn(_1f1)){
return false;
}
try{
if(!RadGridNamespace.FireEvent(this,"OnColumnHiding",[_1f1])){
return;
}
this.HideShowColumn(_1f1,false);
if(this!=this.Owner.MasterTableViewHeader){
this.Owner.SavePostData("HidedColumns",this.ClientID,this.Columns[_1f1].RealIndex);
}
RadGridNamespace.FireEvent(this,"OnColumnHidden",[_1f1]);
}
catch(error){
new RadGridNamespace.Error(error,this,this.Owner.OnError);
}
};
RadGridNamespace.RadGridTable.prototype.ShowColumn=function(_1f2){
if(!this.CanShowHideColumn(_1f2)){
return false;
}
try{
if(!RadGridNamespace.FireEvent(this,"OnColumnShowing",[_1f2])){
return;
}
this.HideShowColumn(_1f2,true);
if(this!=this.Owner.MasterTableViewHeader){
this.Owner.SavePostData("ShowedColumns",this.ClientID,this.Columns[_1f2].RealIndex);
}
RadGridNamespace.FireEvent(this,"OnColumnShowed",[_1f2]);
}
catch(error){
new RadGridNamespace.Error(error,this,this.Owner.OnError);
}
};
RadGridNamespace.RadGridTable.prototype.HideShowCol=function(_1f3,_1f4,_1f5){
if(_1f3&&_1f3.ColGroup&&_1f3.ColGroup.Cols&&_1f3.ColGroup.Cols[_1f4]){
_1f3.ColGroup.Cols[_1f4].style.display=(_1f5)?"":"none";
}
};
RadGridNamespace.RadGridTable.prototype.HideShowColumn=function(_1f6,_1f7){
var _1f7=this.Columns[_1f6].Display=_1f7;
if(this.Owner.MasterTableViewHeader){
this.HideShowCol(this.Owner.MasterTableViewHeader,_1f6,_1f7);
if(window.netscape){
RadGridNamespace.HideShowCells(this.Owner.MasterTableViewHeader.Control,_1f6,_1f7);
}
}
if(this.Owner.MasterTableView){
this.HideShowCol(this.Owner.MasterTableView,_1f6,_1f7);
if(window.netscape){
RadGridNamespace.HideShowCells(this.Owner.MasterTableView.Control,_1f6,_1f7);
}
}
if(this.Owner.MasterTableViewFooter){
this.HideShowCol(this.Owner.MasterTableViewFooter,_1f6,_1f7);
if(window.netscape){
RadGridNamespace.HideShowCells(this.Owner.MasterTableViewFooter.Control,_1f6,_1f7);
}
}
};
RadGridNamespace.RadGridTable.prototype.CanShowHideRow=function(_1f8){
if(!this.Owner.ClientSettings.AllowRowHide){
return false;
}
if(isNaN(parseInt(_1f8))){
var _1f9="Row index must be of type \"Number\"!";
alert(_1f9);
return false;
}
if(_1f8<0){
var _1f9="Row index must be non-negative!";
alert(_1f9);
return false;
}
if(_1f8>(this.Rows.length-1)){
var _1f9="Row index must be less than rows count!";
alert(_1f9);
return false;
}
return true;
};
RadGridNamespace.RadGridTable.prototype.HideRow=function(_1fa){
if(!this.CanShowHideRow(_1fa)){
return false;
}
try{
if(!RadGridNamespace.FireEvent(this,"OnRowHiding",[_1fa])){
return;
}
if(this.Rows){
if(this.Rows[_1fa]){
if(this.Rows[_1fa].Control){
this.Rows[_1fa].Control.style.display="none";
this.Rows[_1fa].Display=false;
}
}
}
if(this!=this.Owner.MasterTableViewHeader){
this.Owner.SavePostData("HidedRows",this.ClientID,this.Rows[_1fa].RealIndex);
}
RadGridNamespace.FireEvent(this,"OnRowHidden",[_1fa]);
}
catch(error){
new RadGridNamespace.Error(error,this,this.Owner.OnError);
}
};
RadGridNamespace.RadGridTable.prototype.ShowRow=function(_1fb){
if(!this.CanShowHideRow(_1fb)){
return false;
}
try{
if(!RadGridNamespace.FireEvent(this,"OnRowShowing",[_1fb])){
return;
}
if(this.Rows){
if(this.Rows[_1fb]){
if(this.Rows[_1fb].Control){
if(this.Rows[_1fb].ItemType!="NestedView"){
if(window.netscape){
this.Rows[_1fb].Control.style.display="table-row";
}else{
this.Rows[_1fb].Control.style.display="";
}
this.Rows[_1fb].Display=true;
}
}
}
}
if(this!=this.Owner.MasterTableViewHeader){
this.Owner.SavePostData("ShowedRows",this.ClientID,this.Rows[_1fb].RealIndex);
}
RadGridNamespace.FireEvent(this,"OnRowShowed",[_1fb]);
}
catch(error){
new RadGridNamespace.Error(error,this,this.Owner.OnError);
}
};
RadGridNamespace.RadGridTable.prototype.ExportToExcel=function(_1fc){
try{
this.Owner.SavePostData("ExportToExcel",this.ClientID,_1fc);
__doPostBack(this.Owner.UniqueID,"ExportToExcel");
}
catch(e){
throw e;
}
};
RadGridNamespace.RadGridTable.prototype.ExportToWord=function(_1fd){
try{
this.Owner.SavePostData("ExportToWord",this.ClientID,_1fd);
__doPostBack(this.Owner.UniqueID,"ExportToWord");
}
catch(e){
throw e;
}
};
RadGridNamespace.RadGridTable.prototype.ExportToCSV=function(_1fe){
try{
this.Owner.SavePostData("ExportToCSV",this.ClientID,_1fe);
__doPostBack(this.Owner.UniqueID,"ExportToCSV");
}
catch(e){
throw e;
}
};
RadGridNamespace.RadGridTable.prototype.ExportToPdf=function(_1ff){
try{
this.Owner.SavePostData("ExportToPdf",this.ClientID,_1ff);
__doPostBack(this.Owner.UniqueID,"ExportToPdf");
}
catch(e){
throw e;
}
};
RadGridNamespace.RadGridTable.prototype.AddToSelectedRows=function(_200){
try{
this.SelectedRows[this.SelectedRows.length]=_200;
}
catch(e){
throw e;
}
};
RadGridNamespace.RadGridTable.prototype.IsInSelectedRows=function(_201){
try{
for(var i=0;i<this.SelectedRows.length;i++){
if(this.SelectedRows[i]!=_201){
return true;
}
}
return false;
}
catch(e){
throw e;
}
};
RadGridNamespace.RadGridTable.prototype.ClearSelectedRows=function(){
var _203=this.SelectedRows;
for(var i=0;i<this.SelectedRows.length;i++){
if(!RadGridNamespace.FireEvent(this,"OnRowDeselecting",[this.SelectedRows[i]])){
continue;
}
this.SelectedRows[i].Selected=false;
this.SelectedRows[i].CheckClientSelectColumns();
this.SelectedRows[i].RemoveSelectedRowStyle();
var last=this.SelectedRows[i];
try{
this.SelectedRows.splice(i,1);
i--;
}
catch(ex){
}
RadGridNamespace.FireEvent(this,"OnRowDeselected",[last]);
}
this.SelectedRows=new Array();
};
RadGridNamespace.RadGridTable.prototype.RemoveFromSelectedRows=function(_206){
try{
var _207=new Array();
for(var i=0;i<this.SelectedRows.length;i++){
var last=this.SelectedRows[i];
if(this.SelectedRows[i]!=_206){
_207[_207.length]=this.SelectedRows[i];
}else{
if(!this.Owner.AllowMultiRowSelection){
if(!RadGridNamespace.FireEvent(this,"OnRowDeselecting",[this.SelectedRows[i]])){
continue;
}
}
try{
this.SelectedRows.splice(i,1);
i--;
}
catch(ex){
}
_206.CheckClientSelectColumns();
setTimeout(function(){
RadGridNamespace.FireEvent(_206.Owner,"OnRowDeselected",[_206]);
},100);
}
}
this.SelectedRows=_207;
}
catch(e){
throw e;
}
};
RadGridNamespace.RadGridTable.prototype.GetSelectedRowsIndexes=function(){
try{
var _20a=new Array();
for(var i=0;i<this.SelectedRows.length;i++){
_20a[_20a.length]=this.SelectedRows[i].RealIndex;
}
return _20a.join(",");
}
catch(e){
throw e;
}
};
RadGridNamespace.RadGridTable.prototype.GetCellByColumnUniqueName=function(_20c,_20d){
if(this.ClientID.indexOf("_Header")!=-1){
return;
}
if((!_20c)||(!_20d)){
return;
}
if(!this.Columns){
return;
}
for(var i=0;i<this.Columns.length;i++){
if(this.Columns[i].UniqueName.toUpperCase()==_20d.toUpperCase()){
return _20c.Control.cells[i];
}
}
return null;
};
RadGridNamespace.RadGridTableColumn=function(_20f){
if((!_20f)||typeof (_20f)!="object"){
return;
}
RadControlsNamespace.DomEventMixin.Initialize(this);
for(var _210 in _20f){
this[_210]=_20f[_210];
}
this.Type="RadGridTableColumn";
this.ResizeTolerance=5;
this.CanResize=false;
};
RadGridNamespace.RadGridTableColumn.prototype._constructor=function(_211,_212){
this.Control=_211;
this.Owner=_212;
this.Index=_211.cellIndex;
if((window.opera&&typeof (_211.cellIndex)=="undefined")||(navigator.userAgent.indexOf("Safari")!=-1)){
var _213=this;
setTimeout(function(){
_213.Index=RadGridNamespace.GetRealCellIndex(_213.Owner,_213.Control);
},200);
}
this.AttachDomEvent(this.Control,"click","OnClick");
this.AttachDomEvent(this.Control,"dblclick","OnDblClick");
this.AttachDomEvent(this.Control,"mousemove","OnMouseMove");
this.AttachDomEvent(this.Control,"mousedown","OnMouseDown");
this.AttachDomEvent(this.Control,"mouseup","OnMouseUp");
this.AttachDomEvent(this.Control,"mouseover","OnMouseOver");
this.AttachDomEvent(this.Control,"mouseout","OnMouseOut");
this.AttachDomEvent(this.Control,"contextmenu","OnContextMenu");
};
RadGridNamespace.RadGridTableColumn.prototype.Dispose=function(){
this.DisposeDomEventHandlers();
if(this.ColumnResizer){
this.ColumnResizer.Dispose();
}
this.Control=null;
this.Owner=null;
this.Index=null;
};
RadGridNamespace.RadGridTableColumn.prototype.OnContextMenu=function(e){
try{
if(!RadGridNamespace.FireEvent(this.Owner,"OnColumnContextMenu",[this.Index,e])){
return;
}
}
catch(error){
new RadGridNamespace.Error(error,this,this.Owner.Owner.OnError);
}
};
RadGridNamespace.RadGridTableColumn.prototype.OnClick=function(e){
try{
if(!RadGridNamespace.FireEvent(this.Owner,"OnColumnClick",[this.Index])){
return;
}
}
catch(error){
new RadGridNamespace.Error(error,this,this.Owner.Owner.OnError);
}
};
RadGridNamespace.RadGridTableColumn.prototype.OnDblClick=function(e){
try{
if(!RadGridNamespace.FireEvent(this.Owner,"OnColumnDblClick",[this.Index])){
return;
}
}
catch(error){
new RadGridNamespace.Error(error,this,this.Owner.Owner.OnError);
}
};
RadGridNamespace.RadGridTableColumn.prototype.OnMouseMove=function(e){
if(this.Owner.Owner.ClientSettings&&this.Owner.Owner.ClientSettings.Resizing.AllowColumnResize&&this.Resizable&&this.Control.tagName.toLowerCase()=="th"){
var _218=RadGridNamespace.GetEventPosX(e);
var _219=RadGridNamespace.FindPosX(this.Control);
var endX=_219+this.Control.offsetWidth;
var _21b=RadGridNamespace.GetCurrentElement(e);
if(this.Owner.Owner.GridDataDiv&&!this.Owner.Owner.GridHeaderDiv&&!window.netscape){
var _21c=0;
if(document.body.currentStyle&&document.body.currentStyle.margin.indexOf("px")!=-1&&!window.opera){
_21c=parseInt(document.body.currentStyle.marginLeft);
}
this.ResizeTolerance=10;
}
if((_218>=endX-this.ResizeTolerance)&&(_218<=endX+this.ResizeTolerance)&&!this.Owner.Owner.MoveHeaderDiv){
this.Control.style.cursor="e-resize";
this.Control.title=this.Owner.Owner.ClientSettings.ClientMessages.DragToResize;
this.CanResize=true;
_21b.style.cursor="e-resize";
this.Owner.Owner.IsResize=true;
}else{
this.Control.style.cursor="";
this.Control.title="";
this.CanResize=false;
_21b.style.cursor="";
this.Owner.Owner.IsResize=false;
}
}
};
RadGridNamespace.RadGridTableColumn.prototype.OnMouseDown=function(e){
if(this.CanResize){
if(((window.netscape||window.opera||navigator.userAgent.indexOf("Safari")!=-1)&&(e.button==0))||(e.button==1)){
var _21e=RadGridNamespace.GetEventPosX(e);
var _21f=RadGridNamespace.FindPosX(this.Control);
var endX=_21f+this.Control.offsetWidth;
if((_21e>=endX-this.ResizeTolerance)&&(_21e<=endX+this.ResizeTolerance)){
this.ColumnResizer=new RadGridNamespace.RadGridColumnResizer(this,this.Owner.Owner.ClientSettings.Resizing.EnableRealTimeResize);
this.ColumnResizer.Position(e);
}
}
RadGridNamespace.ClearDocumentEvents();
}
};
RadGridNamespace.RadGridTableColumn.prototype.OnMouseUp=function(e){
RadGridNamespace.RestoreDocumentEvents();
};
RadGridNamespace.RadGridTableColumn.prototype.OnMouseOver=function(e){
if(!RadGridNamespace.FireEvent(this.Owner,"OnColumnMouseOver",[this.Index])){
return;
}
if(this.Owner.Owner.Skin!=""&&this.Owner.Owner.Skin!="None"){
RadGridNamespace.addClassName(this.Control,"GridHeaderOver_"+this.Owner.Owner.Skin);
}
};
RadGridNamespace.RadGridTableColumn.prototype.OnMouseOut=function(e){
if(!RadGridNamespace.FireEvent(this.Owner,"OnColumnMouseOut",[this.Index])){
return;
}
if(this.Owner.Owner.Skin!=""&&this.Owner.Owner.Skin!="None"){
RadGridNamespace.removeClassName(this.Control,"GridHeaderOver_"+this.Owner.Owner.Skin);
}
};
RadGridNamespace.RadGridColumnResizer=function(_224,_225){
if(!_224){
return;
}
RadControlsNamespace.DomEventMixin.Initialize(this);
this.Column=_224;
this.IsRealTimeResize=_225;
this.CurrentWidth=null;
this.LeftResizer=document.createElement("span");
this.LeftResizer.style.backgroundColor="navy";
this.LeftResizer.style.width="1"+"px";
this.LeftResizer.style.position="absolute";
this.LeftResizer.style.cursor="e-resize";
this.RightResizer=document.createElement("span");
this.RightResizer.style.backgroundColor="navy";
this.RightResizer.style.width="1"+"px";
this.RightResizer.style.position="absolute";
this.RightResizer.style.cursor="e-resize";
this.ResizerToolTip=document.createElement("span");
this.ResizerToolTip.style.backgroundColor="#F5F5DC";
this.ResizerToolTip.style.border="1px solid";
this.ResizerToolTip.style.position="absolute";
this.ResizerToolTip.style.font="icon";
this.ResizerToolTip.style.padding="2";
this.ResizerToolTip.innerHTML="Width: <b>"+this.Column.Control.offsetWidth+"</b> <em>pixels</em>";
document.body.appendChild(this.LeftResizer);
document.body.appendChild(this.RightResizer);
document.body.appendChild(this.ResizerToolTip);
this.CanDestroy=true;
this.AttachDomEvent(document,"mouseup","OnMouseUp");
this.AttachDomEvent(this.Column.Owner.Owner.Control,"mousemove","OnMouseMove");
};
RadGridNamespace.RadGridColumnResizer.prototype.OnMouseUp=function(e){
this.Destroy(e);
};
RadGridNamespace.RadGridColumnResizer.prototype.OnMouseMove=function(e){
this.Move(e);
};
RadGridNamespace.RadGridColumnResizer.prototype.Position=function(e){
this.LeftResizer.style.top=RadGridNamespace.FindPosY(this.Column.Control)-RadGridNamespace.FindScrollPosY(this.Column.Control)+document.documentElement.scrollTop+document.body.scrollTop+"px";
this.LeftResizer.style.left=RadGridNamespace.FindPosX(this.Column.Control)-RadGridNamespace.FindScrollPosX(this.Column.Control)+document.documentElement.scrollLeft+document.body.scrollLeft+"px";
this.RightResizer.style.top=this.LeftResizer.style.top;
this.RightResizer.style.left=parseInt(this.LeftResizer.style.left)+this.Column.Control.offsetWidth+"px";
this.ResizerToolTip.style.top=parseInt(this.RightResizer.style.top)-20+"px";
this.ResizerToolTip.style.left=parseInt(this.RightResizer.style.left)-5+"px";
if(parseInt(this.LeftResizer.style.left)<RadGridNamespace.FindPosX(this.Column.Owner.Control)){
this.LeftResizer.style.display="none";
}
if(!this.Column.Owner.Owner.ClientSettings.Scrolling.AllowScroll){
this.LeftResizer.style.height=this.Column.Owner.Control.tBodies[0].offsetHeight+this.Column.Owner.Control.tHead.offsetHeight+"px";
}else{
if(this.Column.Owner.Owner.ClientSettings.Scrolling.UseStaticHeaders){
this.LeftResizer.style.height=this.Column.Owner.Owner.GridDataDiv.clientHeight+this.Column.Owner.Control.tHead.offsetHeight+"px";
}else{
this.LeftResizer.style.height=this.Column.Owner.Owner.GridDataDiv.clientHeight+"px";
}
}
this.RightResizer.style.height=this.LeftResizer.style.height;
};
RadGridNamespace.RadGridColumnResizer.prototype.Destroy=function(e){
if(this.CanDestroy){
this.DetachDomEvent(document,"mouseup","OnMouseUp");
this.DetachDomEvent(this.Column.Owner.Owner.Control,"mousemove","OnMouseMove");
if(this.CurrentWidth!=null){
if(this.CurrentWidth>0){
this.Column.Owner.ResizeColumn(this.Column.Control.cellIndex,this.CurrentWidth);
this.CurrentWidth=null;
}
}
document.body.removeChild(this.LeftResizer);
document.body.removeChild(this.RightResizer);
document.body.removeChild(this.ResizerToolTip);
this.CanDestroy=false;
}
};
RadGridNamespace.RadGridColumnResizer.prototype.Dispose=function(){
try{
this.Destroy();
}
catch(error){
}
this.DisposeDomEventHandlers();
this.MouseUpHandler=null;
this.MouseMoveHandler=null;
this.LeftResizer=null;
this.RightResizer=null;
this.ResizerToolTip=null;
};
RadGridNamespace.RadGridColumnResizer.prototype.Move=function(e){
this.LeftResizer.style.left=RadGridNamespace.FindPosX(this.Column.Control)-RadGridNamespace.FindScrollPosX(this.Column.Control)+document.documentElement.scrollLeft+document.body.scrollLeft+"px";
this.RightResizer.style.left=parseInt(this.LeftResizer.style.left)+(RadGridNamespace.GetEventPosX(e)-RadGridNamespace.FindPosX(this.Column.Control))+"px";
this.ResizerToolTip.style.left=parseInt(this.RightResizer.style.left)-5+"px";
var _22b=parseInt(this.RightResizer.style.left)-parseInt(this.LeftResizer.style.left);
var _22c=this.Column.Control.scrollWidth-_22b;
this.ResizerToolTip.innerHTML="Width: <b>"+_22b+"</b> <em>pixels</em>";
if(!RadGridNamespace.FireEvent(this.Column.Owner,"OnColumnResizing",[this.Column.Index,_22b])){
return;
}
if(_22b<=0){
this.RightResizer.style.left=this.RightResizer.style.left;
this.Destroy(e);
return;
}
this.CurrentWidth=_22b;
if(this.IsRealTimeResize){
var _22d=(navigator.userAgent.indexOf("Safari")!=-1)?RadGridNamespace.GetRealCellIndex(this.Column.Owner,this.Column.Control):this.Column.Control.cellIndex;
this.Column.Owner.ResizeColumn(_22d,_22b);
}else{
this.CurrentWidth=_22b;
return;
}
if(RadGridNamespace.FindPosX(this.LeftResizer)!=RadGridNamespace.FindPosX(this.Column.Control)){
this.LeftResizer.style.left=RadGridNamespace.FindPosX(this.Column.Control)+"px";
}
if(RadGridNamespace.FindPosX(this.RightResizer)!=(RadGridNamespace.FindPosX(this.Column.Control)+this.Column.Control.offsetWidth)){
this.RightResizer.style.left=RadGridNamespace.FindPosX(this.Column.Control)+this.Column.Control.offsetWidth+"px";
}
if(RadGridNamespace.FindPosY(this.LeftResizer)!=RadGridNamespace.FindPosY(this.Column.Control)){
this.LeftResizer.style.top=RadGridNamespace.FindPosY(this.Column.Control)+"px";
this.RightResizer.style.top=RadGridNamespace.FindPosY(this.Column.Control)+"px";
}
if(this.Column.Owner.Owner.GridDataDiv){
this.LeftResizer.style.left=parseInt(this.LeftResizer.style.left.replace("px",""))-this.Column.Owner.Owner.GridDataDiv.scrollLeft+"px";
this.RightResizer.style.left=parseInt(this.LeftResizer.style.left.replace("px",""))+this.Column.Control.offsetWidth+"px";
this.ResizerToolTip.style.left=parseInt(this.RightResizer.style.left)-5+"px";
}
if(!this.Column.Owner.Owner.ClientSettings.Scrolling.AllowScroll){
this.LeftResizer.style.height=this.Column.Owner.Control.tBodies[0].offsetHeight+this.Column.Owner.Control.tHead.offsetHeight+"px";
}else{
if(this.Column.Owner.Owner.ClientSettings.Scrolling.UseStaticHeaders){
this.LeftResizer.style.height=this.Column.Owner.Owner.GridDataDiv.clientHeight+this.Column.Owner.Control.tHead.offsetHeight+"px";
}else{
this.LeftResizer.style.height=this.Column.Owner.Owner.GridDataDiv.clientHeight+"px";
}
}
this.RightResizer.style.height=this.LeftResizer.style.height;
};
RadGridNamespace.RadGridTableRow=function(_22e){
if((!_22e)||typeof (_22e)!="object"){
return;
}
RadControlsNamespace.DomEventMixin.Initialize(this);
for(var _22f in _22e){
this[_22f]=_22e[_22f];
}
this.Type="RadGridTableRow";
var _230=document.getElementById(this.OwnerID);
this.Control=_230.tBodies[0].rows[this.ClientRowIndex];
if(!this.Control){
return;
}
this.Index=this.Control.sectionRowIndex;
this.RealIndex=this.RowIndex;
};
RadGridNamespace.RadGridTableRow.prototype._constructor=function(_231){
this.Owner=_231;
this.CreateStyles();
if(this.Selected){
this.LoadSelected();
}
if(this.Owner.HierarchyLoadMode=="Client"){
if(this.Owner.Owner.ClientSettings.AllowExpandCollapse){
for(var i=0;i<this.Owner.ExpandCollapseColumns.length;i++){
var _233=this.Owner.ExpandCollapseColumns[i].Control.cellIndex;
var _234=this.Control.cells[_233];
var html=this.Control.innerHTML;
if(!_234){
continue;
}
var _236;
for(var j=0;j<_234.childNodes.length;j++){
if(!_234.childNodes[j].tagName){
continue;
}
var _238;
if(this.Owner.ExpandCollapseColumns[i].ButtonType=="ImageButton"){
_238="img";
}else{
if(this.Owner.ExpandCollapseColumns[i].ButtonType=="LinkButton"){
_238="a";
}else{
if(this.Owner.ExpandCollapseColumns[i].ButtonType=="PushButton"){
_238="button";
}
}
}
if(_234.childNodes[j].tagName.toLowerCase()==_238){
_236=_234.childNodes[j];
break;
}
}
if(_236){
var _239=this;
var _23a=function(){
_239.OnHierarchyExpandButtonClick(this);
};
_236.onclick=_23a;
_236.ondblclick=null;
_23a=null;
}
_236=null;
}
}
}
if(this.Owner.GroupLoadMode=="Client"){
if(this.Owner.Owner.ClientSettings.AllowGroupExpandCollapse){
for(var i=0;i<this.Owner.GroupSplitterColumns.length;i++){
var _233=this.Owner.GroupSplitterColumns[i].Control.cellIndex;
var html=this.Control.innerHTML;
var _234=this.Control.cells[_233];
if(!_234){
continue;
}
var _236;
for(var j=0;j<_234.childNodes.length;j++){
if(!_234.childNodes[j].tagName){
continue;
}
if(_234.childNodes[j].tagName.toLowerCase()=="img"){
_236=_234.childNodes[j];
break;
}
}
if(_236){
var _239=this;
var _23a=function(){
_239.OnGroupExpandButtonClick(this);
};
_236.onclick=_23a;
_236.ondblclick=null;
_23a=null;
}
_236=null;
}
}
}
this.AttachDomEvent(this.Control,"click","OnClick");
this.AttachDomEvent(this.Control,"dblclick","OnDblClick");
this.AttachDomEvent(document,"mousedown","OnMouseDown");
this.AttachDomEvent(document,"mouseup","OnMouseUp");
this.AttachDomEvent(document,"mousemove","OnMouseMove");
this.AttachDomEvent(this.Control,"mouseover","OnMouseOver");
this.AttachDomEvent(this.Control,"mouseout","OnMouseOut");
this.AttachDomEvent(this.Control,"contextmenu","OnContextMenu");
if(this.Owner.Owner.ClientSettings.ActiveRowData&&this.Owner.Owner.ClientSettings.ActiveRowData!=""){
var data=this.Owner.Owner.ClientSettings.ActiveRowData.split(";")[0].split(",");
if(data[0]==this.Owner.ClientID&&data[1]==this.RealIndex){
this.Owner.Owner.ActiveRow=this;
}
}
};
RadGridNamespace.GroupRowExpander=function(_23c){
this.startRow=_23c;
};
RadGridNamespace.GroupRowExpander.prototype.NotFinished=function(_23d){
var _23e=(this.currentGridRow!=null);
if(!_23e){
return false;
}
var _23f=(this.currentGridRow.GroupIndex=="");
var _240=(this.currentGridRow.GroupIndex==_23d.GroupIndex);
var _241=(this.currentGridRow.GroupIndex.indexOf(_23d.GroupIndex+"_")==0);
return (_23f||_240||_241);
};
RadGridNamespace.GroupRowExpander.prototype.ToggleExpandCollapse=function(_242){
var _243=this.startRow;
var _244=_243.Owner;
var _245=_242.parentNode.parentNode.sectionRowIndex;
var _246=_244.Rows[_245];
if(_246.Expanded){
if(!RadGridNamespace.FireEvent(_246.Owner,"OnGroupCollapsing",[_246])){
return;
}
}else{
if(!RadGridNamespace.FireEvent(_246.Owner,"OnGroupExpanding",[_246])){
return;
}
}
var _247=_244.Control.rows[_245+1];
if(!_247){
return;
}
this.currentRowIndex=_247.rowIndex;
this.lastGroupIndex=null;
while(true){
this.currentGridRow=_244.Rows[this.currentRowIndex];
var _248=this.NotFinished(_246);
if(!_248){
break;
}
var _249=(this.lastGroupIndex!=null)&&(this.currentGridRow.GroupIndex.indexOf(this.lastGroupIndex)!=-1);
var _24a=(this.currentGridRow.ItemType!="GroupHeader")&&(!this.currentGridRow.IsVisible());
var _24b=_249&&_24a;
if(this.currentGridRow.ItemType=="GroupHeader"&&!this.currentGridRow.Expanded){
if(this.currentGridRow.IsVisible()){
this.currentGridRow.Hide();
_242.src=_244.GroupSplitterColumns[0].ExpandImageUrl;
_242.title=_244.Owner.GroupingSettings.ExpandTooltip;
if(_244.Rows[this.currentRowIndex+1]==null||_244.Rows[this.currentRowIndex+1].ItemType=="GroupHeader"){
this.currentGridRow.Expanded=false;
}
}else{
_242.src=_244.GroupSplitterColumns[0].CollapseImageUrl;
_242.title=_244.Owner.GroupingSettings.CollapseTooltip;
this.currentGridRow.Show();
if(_244.Rows[this.currentRowIndex+1]==null||_244.Rows[this.currentRowIndex+1].ItemType=="GroupHeader"){
this.currentGridRow.Expanded=true;
}
}
this.lastGroupIndex=this.currentGridRow.GroupIndex;
}else{
if(!_24b){
if(this.currentGridRow.ItemType=="NestedView"){
if(this.currentGridRow.Expanded){
if(this.currentGridRow.IsVisible()){
this.currentGridRow.Hide();
}else{
this.currentGridRow.Show();
}
}
}else{
if(this.currentGridRow.IsVisible()){
this.currentGridRow.Hide();
_242.src=_244.GroupSplitterColumns[0].ExpandImageUrl;
_242.title=_244.Owner.GroupingSettings.ExpandTooltip;
_246.Expanded=false;
}else{
_242.src=_244.GroupSplitterColumns[0].CollapseImageUrl;
_242.title=_244.Owner.GroupingSettings.CollapseTooltip;
this.currentGridRow.Show();
_246.Expanded=true;
}
}
}
}
this.currentRowIndex++;
}
if(_246.Expanded!=null){
if(_246.Expanded){
_244.Owner.SavePostData("ExpandedGroupRows",_244.ClientID,_246.RealIndex);
_243.title=_244.Owner.GroupingSettings.CollapseTooltip;
}else{
_244.Owner.SavePostData("CollapsedGroupRows",_244.ClientID,_246.RealIndex);
_243.title=_244.Owner.GroupingSettings.ExpandTooltip;
}
}
if(_246.Expanded){
if(!RadGridNamespace.FireEvent(_246.Owner,"OnGroupExpanded",[_246])){
return;
}
}else{
if(!RadGridNamespace.FireEvent(_246.Owner,"OnGroupCollapsed",[_246])){
return;
}
}
};
RadGridNamespace.RadGridTableRow.prototype.OnGroupExpandButtonClick=function(_24c){
var _24d=new RadGridNamespace.GroupRowExpander(this);
_24d.ToggleExpandCollapse(_24c);
};
RadGridNamespace.RadGridTableRow.prototype.OnHierarchyExpandButtonClick=function(_24e){
var _24f=this.Owner.Control.rows[_24e.parentNode.parentNode.rowIndex+1];
var _250=this.Owner.Rows[_24e.parentNode.parentNode.sectionRowIndex];
if(!_24f){
return;
}
if(this.TableRowIsVisible(_24f)){
if(!RadGridNamespace.FireEvent(this.Owner,"OnHierarchyCollapsing",[this])){
return;
}
this.HideTableRow(_24f);
_250.Expanded=false;
if(this.Owner.ExpandCollapseColumns[0].ButtonType=="ImageButton"){
_24e.src=this.Owner.ExpandCollapseColumns[0].ExpandImageUrl;
}else{
_24e.innerHTML="+";
}
_24e.title=this.Owner.Owner.HierarchySettings.ExpandTooltip;
this.Owner.Owner.SavePostData("CollapsedRows",this.Owner.ClientID,this.RealIndex);
if(!RadGridNamespace.FireEvent(this.Owner,"OnHierarchyCollapsed",[this])){
return;
}
}else{
if(!RadGridNamespace.FireEvent(this.Owner,"OnHierarchyExpanding",[this])){
return;
}
if(this.Owner.ExpandCollapseColumns[0].ButtonType=="ImageButton"){
_24e.src=this.Owner.ExpandCollapseColumns[0].CollapseImageUrl;
}else{
_24e.innerHTML="-";
}
_24e.title=this.Owner.Owner.HierarchySettings.CollapseTooltip;
this.ShowTableRow(_24f);
_250.Expanded=true;
this.Owner.Owner.SavePostData("ExpandedRows",this.Owner.ClientID,this.RealIndex);
if(!RadGridNamespace.FireEvent(this.Owner,"OnHierarchyExpanded",[this])){
return;
}
}
};
RadGridNamespace.RadGridTableRow.prototype.TableRowIsVisible=function(_251){
return _251.style.display!="none";
};
RadGridNamespace.RadGridTableRow.prototype.IsVisible=function(){
return this.TableRowIsVisible(this.Control);
};
RadGridNamespace.RadGridTableRow.prototype.HideTableRow=function(_252){
if(this.TableRowIsVisible(_252)){
_252.style.display="none";
if(navigator.userAgent.toLowerCase().indexOf("msie")!=-1&&navigator.userAgent.toLowerCase().indexOf("6.0")!=-1){
var _253=_252.getElementsByTagName("select");
for(var i=0;i<_253.length;i++){
_253[i].style.display="none";
}
}
}
};
RadGridNamespace.RadGridTableRow.prototype.Hide=function(){
this.HideTableRow(this.Control);
};
RadGridNamespace.RadGridTableRow.prototype.ShowTableRow=function(_255){
if(window.netscape||window.opera){
_255.style.display="table-row";
}else{
_255.style.display="block";
if(navigator.userAgent.toLowerCase().indexOf("msie")!=-1&&navigator.userAgent.toLowerCase().indexOf("6.0")!=-1){
var _256=_255.getElementsByTagName("select");
for(var i=0;i<_256.length;i++){
_256[i].style.display="";
}
}
}
};
RadGridNamespace.RadGridTableRow.prototype.Show=function(){
this.ShowTableRow(this.Control);
};
RadGridNamespace.RadGridTableRow.prototype.Dispose=function(){
this.DisposeDomEventHandlers();
this.Control=null;
this.Owner=null;
};
RadGridNamespace.RadGridTableRow.prototype.CreateStyles=function(){
if(!this.Owner.Owner.ClientSettings.ApplyStylesOnClient){
return;
}
switch(this.ItemType){
case "GroupHeader":
break;
case "EditFormItem":
this.Control.className+=" "+this.Owner.RenderEditItemStyleClass;
this.Control.style.cssText+=" "+this.Owner.RenderEditItemStyle;
break;
default:
var _258=eval("this.Owner.Render"+this.ItemType+"StyleClass");
if(typeof (_258)!="undefined"){
this.Control.className+=" "+_258;
}
var _259=eval("this.Owner.Render"+this.ItemType+"Style");
if(typeof (_259)!="undefined"){
this.Control.style.cssText+=" "+_259;
}
break;
}
if(!this.Display){
if(this.Control.style.cssText!=""){
if(this.Control.style.cssText.lastIndexOf(";")==this.Control.style.cssText.length-1){
this.Control.style.cssText+="display:none;";
}else{
this.Control.style.cssText+=";display:none;";
}
}else{
this.Control.style.cssText+="display:none;";
}
}
};
RadGridNamespace.RadGridTableRow.prototype.OnContextMenu=function(e){
try{
if(!RadGridNamespace.FireEvent(this.Owner,"OnRowContextMenu",[this.Index,e])){
return;
}
if(this.Owner.Owner.ClientSettings.ClientEvents.OnRowContextMenu!=""){
if(e.preventDefault){
e.preventDefault();
}else{
e.returnValue=false;
return false;
}
}
}
catch(error){
new RadGridNamespace.Error(error,this,this.Owner.Owner.OnError);
}
};
RadGridNamespace.RadGridTableRow.prototype.OnClick=function(e){
try{
if(this.Owner.Owner.RowResizer){
return;
}
if(!RadGridNamespace.FireEvent(this.Owner,"OnRowClick",[this.Control.sectionRowIndex,e])){
return;
}
if(e.shiftKey&&this.Owner.SelectedRows[0]){
if(this.Owner.SelectedRows[0].Control.rowIndex>this.Control.rowIndex){
for(var i=this.Control.rowIndex;i<this.Owner.SelectedRows[0].Control.rowIndex+1;i++){
var _25d=this.Owner.Owner.GetRowObjectByRealRow(this.Owner,this.Owner.Control.rows[i]);
if(_25d){
if(!_25d.Selected){
this.Owner.SelectRow(this.Owner.Control.rows[i],false);
}
}
}
}
if(this.Owner.SelectedRows[0].Control.rowIndex<this.Control.rowIndex){
for(var i=this.Owner.SelectedRows[0].Control.rowIndex;i<this.Control.rowIndex+1;i++){
var _25d=this.Owner.Owner.GetRowObjectByRealRow(this.Owner,this.Owner.Control.rows[i]);
if(_25d){
if(!_25d.Selected){
this.Owner.SelectRow(this.Owner.Control.rows[i],false);
}
}
}
}
}
if(!e.shiftKey){
this.HandleRowSelection(e);
}
var _25e=RadGridNamespace.GetCurrentElement(e);
if(!_25e){
return;
}
if(!_25e.tagName){
return;
}
if(_25e.tagName.toLowerCase()=="input"||_25e.tagName.toLowerCase()=="select"||_25e.tagName.toLowerCase()=="option"||_25e.tagName.toLowerCase()=="button"||_25e.tagName.toLowerCase()=="a"||_25e.tagName.toLowerCase()=="textarea"){
return;
}
if(this.ItemType=="Item"||this.ItemType=="AlternatingItem"){
if(this.Owner.Owner.ClientSettings.EnablePostBackOnRowClick){
var _25f=this.Owner.Owner.ClientSettings.PostBackFunction;
_25f=_25f.replace("{0}",this.Owner.Owner.UniqueID).replace("{1}","RowClick;"+this.ItemIndexHierarchical);
var form=document.getElementById(this.Owner.Owner.FormID);
if(form!=null&&form["__EVENTTARGET"]!=null&&form["__EVENTTARGET"].value==this.Owner.Owner.UniqueID){
form["__EVENTTARGET"].value="";
}
if(form!=null&&form["__EVENTTARGET"]!=null&&form["__EVENTTARGET"].value==""){
eval(_25f);
}
}
}
}
catch(error){
new RadGridNamespace.Error(error,this,this.Owner.Owner.OnError);
}
};
RadGridNamespace.RadGridTableRow.prototype.HandleActiveRow=function(e){
var _262=RadGridNamespace.GetCurrentElement(e);
if(_262!=null&&_262.tagName&&(_262.tagName.toLowerCase()=="input"||_262.tagName.toLowerCase()=="textarea")){
return;
}
if(this.Owner.Owner.ActiveRow!=null){
if(!RadGridNamespace.FireEvent(this.Owner,"OnActiveRowChanging",[this.Owner.Owner.ActiveRow])){
return;
}
if(e.keyCode==13){
this.Owner.Owner.SavePostData("EditRow",this.Owner.ClientID,this.Owner.Owner.ActiveRow.RealIndex);
eval(this.Owner.Owner.ClientSettings.PostBackReferences.PostBackEditRow);
}
if(e.keyCode==40){
var _263=this.Owner.Rows[this.Owner.Owner.ActiveRow.Control.sectionRowIndex+1];
if(_263!=null){
this.Owner.Owner.SetActiveRow(_263);
this.ScrollIntoView(_263);
}
}
if(e.keyCode==39){
return;
var _263=this.Owner.Owner.GetNextHierarchicalRow(_264,this.Owner.Owner.ActiveRow.Control.sectionRowIndex);
if(_263!=null){
_264=_263.parentNode.parentNode;
this.Owner.Owner.SetActiveRow(_264,_263.sectionRowIndex);
this.ScrollIntoView(_263);
}
}
if(e.keyCode==38){
var _265=this.Owner.Rows[this.Owner.Owner.ActiveRow.Control.sectionRowIndex-1];
if(_265!=null){
this.Owner.Owner.SetActiveRow(_265);
this.ScrollIntoView(_265);
}
}
if(e.keyCode==37){
return;
var _265=this.Owner.Owner.GetPreviousHierarchicalRow(_264,this.Owner.Owner.ActiveRow.Control.sectionRowIndex);
if(_265!=null){
var _264=_265.parentNode.parentNode;
this.Owner.Owner.SetActiveRow(_264,_265.sectionRowIndex);
this.ScrollIntoView(_265);
}
}
if(e.keyCode==32){
if(this.Owner.Owner.ClientSettings.Selecting.AllowRowSelect){
this.Owner.Owner.ActiveRow.Owner.SelectRow(this.Owner.Owner.ActiveRow.Control,!this.Owner.Owner.AllowMultiRowSelection);
}
}
}
if(window.netscape){
e.preventDefault();
return false;
}else{
e.returnValue=false;
}
RadGridNamespace.FireEvent(this.Owner,"OnActiveRowChanged",[this.Owner.Owner.ActiveRow]);
};
RadGridNamespace.RadGridTableRow.prototype.ScrollIntoView=function(row){
if(row.Control&&row.Control.focus){
row.Control.scrollIntoView(false);
try{
row.Control.focus();
}
catch(e){
}
}
};
RadGridNamespace.RadGridTableRow.prototype.HandleExpandCollapse=function(){
};
RadGridNamespace.RadGridTableRow.prototype.HandleGroupExpandCollapse=function(){
};
RadGridNamespace.RadGridTableRow.prototype.HandleRowSelection=function(e){
var _268=RadGridNamespace.GetCurrentElement(e);
if(_268.onclick){
return;
}
if(_268.tagName.toLowerCase()=="input"||_268.tagName.toLowerCase()=="select"||_268.tagName.toLowerCase()=="option"||_268.tagName.toLowerCase()=="button"||_268.tagName.toLowerCase()=="a"||_268.tagName.toLowerCase()=="textarea"||_268.tagName.toLowerCase()=="img"){
return;
}
this.SetSelected(!e.ctrlKey,e);
};
RadGridNamespace.RadGridTableRow.prototype.CheckClientSelectColumns=function(){
if(!this.Owner.Columns){
return;
}
for(var i=0;i<this.Owner.Columns.length;i++){
if(this.Owner.Columns[i].ColumnType=="GridClientSelectColumn"){
var cell=this.Owner.GetCellByColumnUniqueName(this,this.Owner.Columns[i].UniqueName);
if(cell!=null){
var _26b=cell.getElementsByTagName("input")[0];
if(_26b!=null){
_26b.checked=this.Selected;
}
}
}
}
};
RadGridNamespace.RadGridTableRow.prototype.SetSelected=function(_26c,e){
if(!this.Selected){
if(!RadGridNamespace.FireEvent(this.Owner,"OnRowSelecting",[this,e])){
return;
}
}
if((this.ItemType=="Item")||(this.ItemType=="AlternatingItem")){
if(_26c){
this.SingleSelect();
}else{
this.MultiSelect();
}
}
this.CheckClientSelectColumns();
if(this.Selected){
if(!RadGridNamespace.FireEvent(this.Owner,"OnRowSelected",[this,e])){
return;
}
}
};
RadGridNamespace.RadGridTableRow.prototype.SingleSelect=function(){
if(!this.Owner.Owner.ClientSettings.Selecting.AllowRowSelect){
return;
}
this.Owner.ClearSelectedRows();
this.Owner.Owner.ClearSelectedRows();
this.Selected=true;
this.ApplySelectedRowStyle();
this.Owner.AddToSelectedRows(this);
this.Owner.Owner.UpdateClientRowSelection();
};
RadGridNamespace.RadGridTableRow.prototype.SingleDeselect=function(){
if(!this.Owner.Owner.ClientSettings.Selecting.AllowRowSelect){
return;
}
this.Owner.ClearSelectedRows();
this.Owner.Owner.ClearSelectedRows();
this.Selected=false;
this.RemoveSelectedRowStyle();
this.Owner.RemoveFromSelectedRows(this);
this.Owner.Owner.UpdateClientRowSelection();
};
RadGridNamespace.RadGridTableRow.prototype.MultiSelect=function(){
if((!this.Owner.Owner.ClientSettings.Selecting.AllowRowSelect)||(!this.Owner.Owner.AllowMultiRowSelection)){
return;
}
if(this.Selected){
if(!RadGridNamespace.FireEvent(this.Owner,"OnRowDeselecting",[this])){
return;
}
this.Selected=false;
this.RemoveSelectedRowStyle();
this.Owner.RemoveFromSelectedRows(this);
this.Owner.Owner.UpdateClientRowSelection();
}else{
this.Selected=true;
this.ApplySelectedRowStyle();
this.Owner.AddToSelectedRows(this);
this.Owner.Owner.UpdateClientRowSelection();
}
};
RadGridNamespace.RadGridTableRow.prototype.LoadSelected=function(){
this.ApplySelectedRowStyle();
this.Owner.AddToSelectedRows(this);
};
RadGridNamespace.RadGridTableRow.prototype.ApplySelectedRowStyle=function(){
if(!this.Owner.SelectedItemStyleClass||this.Owner.SelectedItemStyleClass==""){
if(this.Owner.SelectedItemStyle&&this.Owner.SelectedItemStyle!=""){
RadGridNamespace.addClassName(this.Control,"SelectedItemStyle"+this.Owner.ClientID+"1");
}else{
RadGridNamespace.addClassName(this.Control,"SelectedItemStyle"+this.Owner.ClientID+"2");
}
}else{
RadGridNamespace.addClassName(this.Control,this.Owner.SelectedItemStyleClass);
}
};
RadGridNamespace.RadGridTableRow.prototype.RemoveSelectedRowStyle=function(){
if(this.Owner.SelectedItemStyle){
RadGridNamespace.removeClassName(this.Control,"SelectedItemStyle"+this.Owner.ClientID+"1");
}else{
RadGridNamespace.removeClassName(this.Control,"SelectedItemStyle"+this.Owner.ClientID+"2");
}
RadGridNamespace.removeClassName(this.Control,this.Owner.SelectedItemStyleClass);
if(this.Control.style.cssText==this.Owner.SelectedItemStyle){
this.Control.style.cssText="";
}
};
RadGridNamespace.RadGridTableRow.prototype.OnDblClick=function(e){
try{
if(!RadGridNamespace.FireEvent(this.Owner,"OnRowDblClick",[this.Control.sectionRowIndex,e])){
return;
}
}
catch(error){
new RadGridNamespace.Error(error,this,this.Owner.Owner.OnError);
}
};
RadGridNamespace.RadGridTableRow.prototype.CreateRowSelectorArea=function(e){
if((this.Owner.Owner.RowResizer)||(e.ctrlKey)){
return;
}
var _270=null;
if(e.srcElement){
_270=e.srcElement;
}else{
if(e.target){
_270=e.target;
}
}
if(!_270.tagName){
return;
}
if(_270.tagName.toLowerCase()=="input"||_270.tagName.toLowerCase()=="textarea"){
return;
}
if((!this.Owner.Owner.ClientSettings.Selecting.AllowRowSelect)||(!this.Owner.Owner.AllowMultiRowSelection)){
return;
}
var _271=RadGridNamespace.GetCurrentElement(e);
if((!_271)||(!RadGridNamespace.IsChildOf(_271,this.Control))){
return;
}
if(!this.RowSelectorArea){
this.RowSelectorArea=document.createElement("span");
this.RowSelectorArea.style.backgroundColor="navy";
this.RowSelectorArea.style.border="indigo 1px solid";
this.RowSelectorArea.style.position="absolute";
this.RowSelectorArea.style.font="icon";
if(window.netscape&&!window.opera){
this.RowSelectorArea.style.MozOpacity=1/10;
}else{
if(window.opera||navigator.userAgent.indexOf("Safari")>-1){
this.RowSelectorArea.style.opacity=0.1;
}else{
this.RowSelectorArea.style.filter="alpha(opacity=10);";
}
}
if(this.Owner.Owner.GridDataDiv){
this.RowSelectorArea.style.top=RadGridNamespace.FindPosY(this.Control)-this.Owner.Owner.GridDataDiv.scrollTop+"px";
this.RowSelectorArea.style.left=RadGridNamespace.FindPosX(this.Control)-this.Owner.Owner.GridDataDiv.scrollLeft+"px";
if(parseInt(this.RowSelectorArea.style.left)<RadGridNamespace.FindPosX(this.Owner.Owner.Control)){
this.RowSelectorArea.style.left=RadGridNamespace.FindPosX(this.Owner.Owner.Control)+"px";
}
}else{
this.RowSelectorArea.style.top=RadGridNamespace.FindPosY(this.Control)+"px";
this.RowSelectorArea.style.left=RadGridNamespace.FindPosX(this.Control)+"px";
}
document.body.appendChild(this.RowSelectorArea);
this.FirstRow=this.Control;
RadGridNamespace.ClearDocumentEvents();
}
};
RadGridNamespace.RadGridTableRow.prototype.DestroyRowSelectorArea=function(e){
if(this.RowSelectorArea){
var _273=this.RowSelectorArea.style.height;
document.body.removeChild(this.RowSelectorArea);
this.RowSelectorArea=null;
RadGridNamespace.RestoreDocumentEvents();
var _274=RadGridNamespace.GetCurrentElement(e);
var _275;
if((!_274)||(!RadGridNamespace.IsChildOf(_274,this.Owner.Control))){
return;
}
var _276=RadGridNamespace.GetFirstParentByTagName(_274,"td");
if((_274.tagName.toLowerCase()=="td")||(_274.tagName.toLowerCase()=="tr")||_276.tagName.toLowerCase()=="td"){
if(_274.tagName.toLowerCase()=="td"){
_275=_274.parentNode;
}else{
if(_276.tagName.toLowerCase()=="td"){
_275=_276.parentNode;
}else{
if(_274.tagName.toLowerCase()=="tr"){
_275=_274;
}
}
}
for(var i=this.FirstRow.rowIndex;i<_275.rowIndex+1;i++){
var _278=this.Owner.Owner.GetRowObjectByRealRow(this.Owner,this.Owner.Control.rows[i]);
if(_278){
if(_273!=""){
if(!_278.Selected){
this.Owner.SelectRow(this.Owner.Control.rows[i],false);
}
}
}
}
}
}
};
RadGridNamespace.RadGridTableRow.prototype.ResizeRowSelectorArea=function(e){
if((this.RowSelectorArea)&&(this.RowSelectorArea.parentNode)){
var _27a=RadGridNamespace.GetCurrentElement(e);
if((!_27a)||(!RadGridNamespace.IsChildOf(_27a,this.Owner.Control))){
return;
}
var _27b=parseInt(this.RowSelectorArea.style.left);
if(this.Owner.Owner.GridDataDiv){
var _27c=RadGridNamespace.GetEventPosX(e)-this.Owner.Owner.GridDataDiv.scrollLeft;
}else{
var _27c=RadGridNamespace.GetEventPosX(e);
}
var _27d=parseInt(this.RowSelectorArea.style.top);
if(this.Owner.Owner.GridDataDiv){
var _27e=RadGridNamespace.GetEventPosY(e)-this.Owner.Owner.GridDataDiv.scrollTop;
}else{
var _27e=RadGridNamespace.GetEventPosY(e);
}
if((_27c-_27b-5)>0){
this.RowSelectorArea.style.width=_27c-_27b-5+"px";
}
if((_27e-_27d-5)>0){
this.RowSelectorArea.style.height=_27e-_27d-5+"px";
}
if(this.RowSelectorArea.offsetWidth>this.Owner.Control.offsetWidth){
this.RowSelectorArea.style.width=this.Owner.Control.offsetWidth+"px";
}
var _27f=(RadGridNamespace.FindPosX(this.Owner.Control)+this.Owner.Control.offsetHeight)-parseInt(this.RowSelectorArea.style.top);
if(this.RowSelectorArea.offsetHeight>_27f){
if(_27f>0){
this.RowSelectorArea.style.height=_27f+"px";
}
}
}
};
RadGridNamespace.RadGridTableRow.prototype.OnMouseDown=function(e){
if(this.Owner.Owner.ClientSettings.Selecting.EnableDragToSelectRows&&this.Owner.Owner.AllowMultiRowSelection){
if(!this.Owner.Owner.RowResizer){
this.CreateRowSelectorArea(e);
}
}
};
RadGridNamespace.RadGridTableRow.prototype.OnMouseUp=function(e){
this.DestroyRowSelectorArea(e);
};
RadGridNamespace.RadGridTableRow.prototype.OnMouseMove=function(e){
this.ResizeRowSelectorArea(e);
};
RadGridNamespace.RadGridTableRow.prototype.OnMouseOver=function(e){
if(!RadGridNamespace.FireEvent(this.Owner,"OnRowMouseOver",[this.Control.sectionRowIndex,e])){
return;
}
if(this.Owner.Owner.Skin!=""&&this.Owner.Owner.Skin!="None"){
RadGridNamespace.addClassName(this.Control,"GridRowOver_"+this.Owner.Owner.Skin);
}
};
RadGridNamespace.RadGridTableRow.prototype.OnMouseOut=function(e){
if(!RadGridNamespace.FireEvent(this.Owner,"OnRowMouseOut",[this.Control.sectionRowIndex,e])){
return;
}
if(this.Owner.Owner.Skin!=""&&this.Owner.Owner.Skin!="None"){
RadGridNamespace.removeClassName(this.Control,"GridRowOver_"+this.Owner.Owner.Skin);
}
};
RadGridNamespace.RadGridGroupPanel=function(_285,_286){
this.Control=_285;
this.Owner=_286;
this.Items=new Array();
this.groupPanelItemCounter=0;
this.getGroupPanelItems(this.Control,0);
var _287=this;
};
RadGridNamespace.RadGridGroupPanel.prototype.Dispose=function(){
this.UnLoadHandler=null;
this.Control=null;
this.Owner=null;
this.DisposeItems();
for(var _288 in this){
this[_288]=null;
}
};
RadGridNamespace.RadGridGroupPanel.prototype.DisposeItems=function(){
if(this.Items!=null){
for(var i=0;i<this.Items.length;i++){
var item=this.Items[i];
item.Dispose();
}
}
};
RadGridNamespace.RadGridGroupPanel.prototype.groupPanelItemCounter=0;
RadGridNamespace.RadGridGroupPanel.prototype.getGroupPanelItems=function(_28b){
for(var i=0;i<_28b.rows.length;i++){
var _28d=false;
var row=_28b.rows[i];
for(var j=0;j<row.cells.length;j++){
var cell=row.cells[j];
if(cell.tagName.toLowerCase()=="th"){
var _291;
if(this.Owner.GroupPanel.GroupPanelItems[this.groupPanelItemCounter]){
_291=this.Owner.GroupPanel.GroupPanelItems[this.groupPanelItemCounter].HierarchicalIndex;
}
if(_291){
this.Items[this.Items.length]=new RadGridNamespace.RadGridGroupPanelItem(cell,this,_291);
_28d=true;
this.groupPanelItemCounter++;
}
}
if((cell.firstChild)&&(cell.firstChild.tagName)){
if(cell.firstChild.tagName.toLowerCase()=="table"){
this.getGroupPanelItems(cell.firstChild);
}
}
}
}
};
RadGridNamespace.RadGridGroupPanel.prototype.IsItem=function(_292){
for(var i=0;i<this.Items.length;i++){
if(this.Items[i].Control==_292){
return this.Items[i];
}
}
return null;
};
RadGridNamespace.RadGridGroupPanelItem=function(_294,_295,_296){
RadControlsNamespace.DomEventMixin.Initialize(this);
this.Control=_294;
this.Owner=_295;
this.HierarchicalIndex=_296;
this.Control.style.cursor="move";
this.AttachDomEvent(this.Control,"mousedown","OnMouseDown");
};
RadGridNamespace.RadGridGroupPanelItem.prototype.Dispose=function(){
this.DisposeDomEventHandlers();
for(var _297 in this){
this[_297]=null;
}
this.Control=null;
this.Owner=null;
};
RadGridNamespace.RadGridGroupPanelItem.prototype.OnMouseDown=function(e){
if(((window.netscape||window.opera||navigator.userAgent.indexOf("Safari")!=-1)&&(e.button==0))||(e.button==1)){
this.CreateDragDrop(e);
this.CreateReorderIndicators(this.Control);
this.AttachDomEvent(document,"mouseup","OnMouseUp");
this.AttachDomEvent(document,"mousemove","OnMouseMove");
}
};
RadGridNamespace.RadGridGroupPanelItem.prototype.OnMouseUp=function(e){
this.FireDropAction(e);
this.DestroyDragDrop(e);
this.DestroyReorderIndicators();
this.DetachDomEvent(document,"mouseup","OnMouseUp");
this.DetachDomEvent(document,"mousemove","OnMouseMove");
};
RadGridNamespace.RadGridGroupPanelItem.prototype.OnMouseMove=function(e){
this.MoveDragDrop(e);
};
RadGridNamespace.RadGridGroupPanelItem.prototype.FireDropAction=function(e){
var _29c=RadGridNamespace.GetCurrentElement(e);
if(_29c!=null){
if(!RadGridNamespace.IsChildOf(_29c,this.Owner.Control)){
this.Owner.Owner.SavePostData("UnGroupByExpression",this.HierarchicalIndex);
eval(this.Owner.Owner.ClientSettings.PostBackReferences.PostBackUnGroupByExpression);
}else{
var item=this.Owner.IsItem(_29c);
if((_29c!=this.Control)&&(item!=null)&&(_29c.parentNode==this.Control.parentNode)){
this.Owner.Owner.SavePostData("ReorderGroupByExpression",this.HierarchicalIndex,item.HierarchicalIndex);
eval(this.Owner.Owner.ClientSettings.PostBackReferences.PostBackReorderGroupByExpression);
}
if(window.netscape){
this.Control.style.MozOpacity=4/4;
}else{
this.Control.style.filter="alpha(opacity=100);";
}
}
}
};
RadGridNamespace.RadGridGroupPanelItem.prototype.CreateDragDrop=function(e){
this.MoveHeaderDiv=document.createElement("div");
var _29f=document.createElement("table");
if(this.MoveHeaderDiv.mergeAttributes){
this.MoveHeaderDiv.mergeAttributes(this.Owner.Owner.Control);
}else{
RadGridNamespace.CopyAttributes(this.MoveHeaderDiv,this.Control);
}
if(_29f.mergeAttributes){
_29f.mergeAttributes(this.Owner.Control);
}else{
RadGridNamespace.CopyAttributes(_29f,this.Owner.Control);
}
_29f.style.margin="0px";
_29f.style.height=this.Control.offsetHeight+"px";
_29f.style.width=this.Control.offsetWidth+"px";
_29f.style.border="0px";
_29f.style.borderCollapse="collapse";
_29f.style.padding="0px";
var _2a0=document.createElement("thead");
var tr=document.createElement("tr");
_29f.appendChild(_2a0);
_2a0.appendChild(tr);
tr.appendChild(this.Control.cloneNode(true));
this.MoveHeaderDiv.appendChild(_29f);
document.body.appendChild(this.MoveHeaderDiv);
this.MoveHeaderDiv.style.height=_29f.style.height;
this.MoveHeaderDiv.style.width=_29f.style.width;
this.MoveHeaderDiv.style.position="absolute";
RadGridNamespace.RadGrid.PositionDragElement(this.MoveHeaderDiv,e);
if(window.netscape){
this.MoveHeaderDiv.style.MozOpacity=3/4;
}else{
this.MoveHeaderDiv.style.filter="alpha(opacity=75);";
}
this.MoveHeaderDiv.style.cursor="move";
this.MoveHeaderDiv.style.display="none";
this.MoveHeaderDiv.onmousedown=null;
RadGridNamespace.ClearDocumentEvents();
};
RadGridNamespace.RadGridGroupPanelItem.prototype.DestroyDragDrop=function(e){
if(this.MoveHeaderDiv!=null){
var _2a3=this.MoveHeaderDiv.parentNode;
_2a3.removeChild(this.MoveHeaderDiv);
this.MoveHeaderDiv.onmouseup=null;
this.MoveHeaderDiv.onmousemove=null;
this.MoveHeaderDiv=null;
RadGridNamespace.RestoreDocumentEvents();
}
};
RadGridNamespace.RadGridGroupPanelItem.prototype.MoveDragDrop=function(e){
if(this.MoveHeaderDiv!=null){
if(window.netscape){
this.Control.style.MozOpacity=1/4;
}else{
this.Control.style.filter="alpha(opacity=25);";
}
this.MoveHeaderDiv.style.visibility="";
this.MoveHeaderDiv.style.display="";
RadGridNamespace.RadGrid.PositionDragElement(this.MoveHeaderDiv,e);
var _2a5=RadGridNamespace.GetCurrentElement(e);
if(_2a5!=null){
if(RadGridNamespace.IsChildOf(_2a5,this.Owner.Control)){
var item=this.Owner.IsItem(_2a5);
if((_2a5!=this.Control)&&(item!=null)&&(_2a5.parentNode==this.Control.parentNode)){
this.MoveReorderIndicators(e,_2a5);
}else{
this.ReorderIndicator1.style.visibility="hidden";
this.ReorderIndicator1.style.display="none";
this.ReorderIndicator1.style.position="absolute";
this.ReorderIndicator2.style.visibility=this.ReorderIndicator1.style.visibility;
this.ReorderIndicator2.style.display=this.ReorderIndicator1.style.display;
this.ReorderIndicator2.style.position=this.ReorderIndicator1.style.position;
}
}
}
}
};
RadGridNamespace.RadGridGroupPanelItem.prototype.CreateReorderIndicators=function(_2a7){
if((this.ReorderIndicator1==null)&&(this.ReorderIndicator2==null)){
this.ReorderIndicator1=document.createElement("span");
this.ReorderIndicator2=document.createElement("span");
if(this.Owner.Owner.Skin==""||this.Owner.Owner.Skin=="None"){
this.ReorderIndicator1.innerHTML="&darr;";
this.ReorderIndicator2.innerHTML="&uarr;";
}else{
this.ReorderIndicator1.className="TopReorderIndicator_"+this.Owner.Owner.Skin;
this.ReorderIndicator2.className="BottomReorderIndicator_"+this.Owner.Owner.Skin;
this.ReorderIndicator1.style.width=this.ReorderIndicator1.style.height=this.ReorderIndicator2.style.width=this.ReorderIndicator2.style.height="10px";
}
this.ReorderIndicator1.style.backgroundColor="transparent";
this.ReorderIndicator1.style.color="darkblue";
this.ReorderIndicator1.style.font="bold 18px Arial";
this.ReorderIndicator2.style.backgroundColor=this.ReorderIndicator1.style.backgroundColor;
this.ReorderIndicator2.style.color=this.ReorderIndicator1.style.color;
this.ReorderIndicator2.style.font=this.ReorderIndicator1.style.font;
this.ReorderIndicator1.style.top=RadGridNamespace.FindPosY(_2a7)-this.ReorderIndicator1.offsetHeight+"px";
this.ReorderIndicator1.style.left=RadGridNamespace.FindPosX(_2a7)+"px";
this.ReorderIndicator2.style.top=RadGridNamespace.FindPosY(_2a7)+_2a7.offsetHeight+"px";
this.ReorderIndicator2.style.left=this.ReorderIndicator1.style.left;
this.ReorderIndicator1.style.visibility="hidden";
this.ReorderIndicator1.style.display="none";
this.ReorderIndicator1.style.position="absolute";
this.ReorderIndicator2.style.visibility=this.ReorderIndicator1.style.visibility;
this.ReorderIndicator2.style.display=this.ReorderIndicator1.style.display;
this.ReorderIndicator2.style.position=this.ReorderIndicator1.style.position;
document.body.appendChild(this.ReorderIndicator1);
document.body.appendChild(this.ReorderIndicator2);
}
};
RadGridNamespace.RadGridGroupPanelItem.prototype.DestroyReorderIndicators=function(){
if((this.ReorderIndicator1!=null)&&(this.ReorderIndicator2!=null)){
document.body.removeChild(this.ReorderIndicator1);
document.body.removeChild(this.ReorderIndicator2);
this.ReorderIndicator1=null;
this.ReorderIndicator2=null;
}
};
RadGridNamespace.RadGridGroupPanelItem.prototype.MoveReorderIndicators=function(e,_2a9){
if((this.ReorderIndicator1!=null)&&(this.ReorderIndicator2!=null)){
this.ReorderIndicator1.style.visibility="visible";
this.ReorderIndicator1.style.display="";
this.ReorderIndicator2.style.visibility="visible";
this.ReorderIndicator2.style.display="";
this.ReorderIndicator1.style.top=RadGridNamespace.FindPosY(_2a9)-this.ReorderIndicator1.offsetHeight+"px";
this.ReorderIndicator1.style.left=RadGridNamespace.FindPosX(_2a9)+"px";
this.ReorderIndicator2.style.top=RadGridNamespace.FindPosY(_2a9)+_2a9.offsetHeight+"px";
this.ReorderIndicator2.style.left=this.ReorderIndicator1.style.left;
}
};
RadGridNamespace.RadGridMenu=function(_2aa,_2ab,_2ac){
if(!_2aa||!_2ab){
return;
}
RadControlsNamespace.DomEventMixin.Initialize(this);
for(var _2ad in _2aa){
this[_2ad]=_2aa[_2ad];
}
this.Owner=_2ab;
this.ItemData=_2aa.Items;
this.Items=[];
};
RadGridNamespace.RadGridMenu.prototype.Initialize=function(){
if(this.Control!=null){
return;
}
this.Control=document.createElement("table");
this.Control.style.backgroundColor=this.SelectColumnBackColor;
this.Control.style.border="outset 1px";
this.Control.style.fontSize="small";
this.Control.style.textAlign="left";
this.Control.cellPadding="0";
this.Control.style.borderCollapse="collapse";
this.Control.style.zIndex=998;
this.Skin=(this.Owner&&this.Owner.Owner&&this.Owner.Owner.Skin)||"None";
var _2ae=RadGridNamespace.IsRightToLeft(this.Owner.Control);
if(_2ae){
this.Control.style.direction="rtl";
RadGridNamespace.addClassName(this.Control,"RadGridRTL_"+this.Skin);
}
RadGridNamespace.addClassName(this.Control,"GridFilterMenu_"+this.Skin);
RadGridNamespace.addClassName(this.Control,this.CssClass);
this.Items=this.CreateItems(this.ItemData);
this.Control.style.position="absolute";
this.Control.style.display="none";
document.body.appendChild(this.Control);
var _2af=document.createElement("img");
_2af.src=this.SelectedImageUrl;
_2af.src=this.NotSelectedImageUrl;
this.Control.style.zIndex=100000;
};
RadGridNamespace.RadGridMenu.prototype.Dispose=function(){
this.DisposeDomEventHandlers();
this.DisposeItems();
this.ItemData=null;
this.Owner=null;
this.Control=null;
};
RadGridNamespace.RadGridMenu.prototype.CreateItems=function(_2b0){
var _2b1=[];
for(var i=0;i<_2b0.length;i++){
_2b1[_2b1.length]=new RadGridNamespace.RadGridMenuItem(_2b0[i],this);
}
return _2b1;
};
RadGridNamespace.RadGridMenu.prototype.DisposeItems=function(){
for(var i=0;i<this.Items.length;i++){
var item=this.Items[i];
item.Dispose();
}
this.Items=null;
};
RadGridNamespace.RadGridMenu.prototype.HideItem=function(_2b5){
for(var i=0;i<this.Items.length;i++){
if(this.Items[i].Value==_2b5){
this.Items[i].Control.style.display="none";
}
}
};
RadGridNamespace.RadGridMenu.prototype.ShowItem=function(_2b7){
for(var i=0;i<this.Items.length;i++){
if(this.Items[i].Value==_2b7){
this.Items[i].Control.style.display="";
}
}
};
RadGridNamespace.RadGridMenu.prototype.SelectItem=function(_2b9){
for(var i=0;i<this.Items.length;i++){
if(this.Items[i].Value==_2b9){
this.Items[i].Selected=true;
this.Items[i].SelectImage.src=this.SelectedImageUrl;
}else{
this.Items[i].Selected=false;
this.Items[i].SelectImage.src=this.NotSelectedImageUrl;
}
}
};
RadGridNamespace.RadGridMenu.prototype.Show=function(_2bb,_2bc,e){
this.Initialize();
this.Control.style.display="";
this.Control.style.top=e.clientY+document.documentElement.scrollTop+document.body.scrollTop+5+"px";
this.Control.style.left=e.clientX+document.documentElement.scrollLeft+document.body.scrollLeft+5+"px";
this.AttachHideEvents();
};
RadGridNamespace.RadGridMenu.prototype.OnKeyPress=function(e){
if(e.keyCode==27){
this.DetachHideEvents();
this.Hide();
}
};
RadGridNamespace.RadGridMenu.prototype.OnClick=function(e){
if(!e.cancelBubble){
this.DetachHideEvents();
this.Hide();
}
};
RadGridNamespace.RadGridMenu.prototype.AttachHideEvents=function(){
this.AttachDomEvent(document,"keypress","OnKeyPress");
this.AttachDomEvent(document,"click","OnClick");
};
RadGridNamespace.RadGridMenu.prototype.DetachHideEvents=function(){
this.DetachDomEvent(document,"keypress","OnKeyPress");
this.DetachDomEvent(document,"click","OnClick");
};
RadGridNamespace.RadGridMenu.prototype.Hide=function(){
if(this.Control.style.display==""){
this.Control.style.display="none";
}
};
RadGridNamespace.RadGridMenuItem=function(_2c0,_2c1){
for(var _2c2 in _2c0){
this[_2c2]=_2c0[_2c2];
}
this.Owner=_2c1;
this.Skin=this.Owner.Skin;
this.Control=this.Owner.Control.insertRow(-1);
this.Control.insertCell(-1);
var _2c3=document.createElement("table");
_2c3.style.width="100%";
_2c3.cellPadding="0";
_2c3.cellSpacing="0";
_2c3.insertRow(-1);
var td1=_2c3.rows[0].insertCell(-1);
var td2=_2c3.rows[0].insertCell(-1);
if(this.Skin=="None"){
td1.style.borderTop="solid 1px "+this.Owner.SelectColumnBackColor;
td1.style.borderLeft="solid 1px "+this.Owner.SelectColumnBackColor;
td1.style.borderRight="none 0px";
td1.style.borderBottom="solid 1px "+this.Owner.SelectColumnBackColor;
td1.style.padding="2px";
td1.style.textAlign="center";
}else{
RadGridNamespace.addClassName(td1,"GridFilterMenuSelectColumn_"+this.Skin);
}
td1.style.width="16px";
td1.appendChild(document.createElement("img"));
td1.childNodes[0].src=this.Owner.NotSelectedImageUrl;
this.SelectImage=td1.childNodes[0];
if(this.Skin=="None"){
td2.style.borderTop="solid 1px "+this.Owner.TextColumnBackColor;
td2.style.borderLeft="none 0px";
td2.style.borderRight="solid 1px "+this.Owner.TextColumnBackColor;
td2.style.borderBottom="solid 1px "+this.Owner.TextColumnBackColor;
td2.style.padding="2px";
td2.style.backgroundColor=this.Owner.TextColumnBackColor;
td2.style.cursor="pointer";
}else{
RadGridNamespace.addClassName(td2,"GridFilterMenuTextColumn_"+this.Skin);
}
td2.innerHTML=this.Text;
this.Control.cells[0].appendChild(_2c3);
var _2c6=this;
this.Control.onclick=function(){
if(_2c6.Owner.Owner.Owner.EnableAJAX){
if(_2c6.Owner.Owner==_2c6.Owner.Owner.Owner.MasterTableViewHeader){
RadGridNamespace.AsyncRequest(_2c6.UID,_2c6.Owner.Owner.Owner.MasterTableView.UID+"!"+_2c6.Owner.Column.UniqueName,_2c6.Owner.Owner.Owner.ClientID);
}else{
RadGridNamespace.AsyncRequest(_2c6.UID,_2c6.Owner.Owner.UID+"!"+_2c6.Owner.Column.UniqueName,_2c6.Owner.Owner.Owner.ClientID);
}
}else{
var _2c7=_2c6.Owner.Owner.Owner.ClientSettings.PostBackFunction;
if(_2c6.Owner.Owner==_2c6.Owner.Owner.Owner.MasterTableViewHeader){
_2c7=_2c7.replace("{0}",_2c6.UID).replace("{1}",_2c6.Owner.Owner.Owner.MasterTableView.UID+"!"+_2c6.Owner.Column.UniqueName);
}else{
_2c7=_2c7.replace("{0}",_2c6.UID).replace("{1}",_2c6.Owner.Owner.UID+"!"+_2c6.Owner.Column.UniqueName);
}
eval(_2c7);
}
};
var _2c6=this;
this.Control.onmouseover=function(e){
if(_2c6.Skin=="None"){
this.cells[0].childNodes[0].rows[0].cells[0].style.backgroundColor=_2c6.Owner.HoverBackColor;
this.cells[0].childNodes[0].rows[0].cells[0].style.borderTop="solid 1px "+_2c6.Owner.HoverBorderColor;
this.cells[0].childNodes[0].rows[0].cells[0].style.borderLeft="solid 1px "+_2c6.Owner.HoverBorderColor;
this.cells[0].childNodes[0].rows[0].cells[0].style.borderBottom="solid 1px "+_2c6.Owner.HoverBorderColor;
this.cells[0].childNodes[0].rows[0].cells[1].style.backgroundColor=_2c6.Owner.HoverBackColor;
this.cells[0].childNodes[0].rows[0].cells[1].style.borderTop="solid 1px "+_2c6.Owner.HoverBorderColor;
this.cells[0].childNodes[0].rows[0].cells[1].style.borderRight="solid 1px "+_2c6.Owner.HoverBorderColor;
this.cells[0].childNodes[0].rows[0].cells[1].style.borderBottom="solid 1px "+_2c6.Owner.HoverBorderColor;
}else{
RadGridNamespace.addClassName(this.cells[0].childNodes[0].rows[0].cells[0],"GridFilterMenuHover_"+_2c6.Skin);
RadGridNamespace.addClassName(this.cells[0].childNodes[0].rows[0].cells[1],"GridFilterMenuHover_"+_2c6.Skin);
}
};
this.Control.onmouseout=function(e){
if(_2c6.Skin=="None"){
this.cells[0].childNodes[0].rows[0].cells[0].style.borderTop="solid 1px "+_2c6.Owner.SelectColumnBackColor;
this.cells[0].childNodes[0].rows[0].cells[0].style.borderLeft="solid 1px "+_2c6.Owner.SelectColumnBackColor;
this.cells[0].childNodes[0].rows[0].cells[0].style.borderBottom="solid 1px "+_2c6.Owner.SelectColumnBackColor;
this.cells[0].childNodes[0].rows[0].cells[0].style.backgroundColor="";
this.cells[0].childNodes[0].rows[0].cells[1].style.borderTop="solid 1px "+_2c6.Owner.TextColumnBackColor;
this.cells[0].childNodes[0].rows[0].cells[1].style.borderRight="solid 1px "+_2c6.Owner.TextColumnBackColor;
this.cells[0].childNodes[0].rows[0].cells[1].style.borderBottom="solid 1px "+_2c6.Owner.TextColumnBackColor;
this.cells[0].childNodes[0].rows[0].cells[1].style.backgroundColor=_2c6.Owner.TextColumnBackColor;
}else{
RadGridNamespace.removeClassName(this.cells[0].childNodes[0].rows[0].cells[0],"GridFilterMenuHover_"+_2c6.Skin);
RadGridNamespace.removeClassName(this.cells[0].childNodes[0].rows[0].cells[1],"GridFilterMenuHover_"+_2c6.Skin);
}
};
};
RadGridNamespace.RadGridMenuItem.prototype.Dispose=function(){
this.Control.onclick=null;
this.Control.onmouseover=null;
this.Control.onmouseout=null;
var _2ca=this.Control.getElementsByTagName("table");
while(_2ca.length>0){
var _2cb=_2ca[0];
if(_2cb.parentNode!=null){
_2cb.parentNode.removeChild(_2cb);
}
}
this.Control=null;
this.Owner=null;
};
RadGridNamespace.RadGridFilterMenu=function(_2cc,_2cd){
RadGridNamespace.RadGridMenu.call(this,_2cc,_2cd);
};
RadGridNamespace.RadGridFilterMenu.prototype=new RadGridNamespace.RadGridMenu;
RadGridNamespace.RadGridFilterMenu.prototype.Show=function(_2ce,e){
this.Initialize();
if(!_2ce){
return;
}
this.Owner=_2ce.Owner;
this.Column=_2ce;
for(var i=0;i<this.Items.length;i++){
if(_2ce.DataTypeName=="System.Boolean"){
if((this.Items[i].Value=="GreaterThan")||(this.Items[i].Value=="LessThan")||(this.Items[i].Value=="GreaterThanOrEqualTo")||(this.Items[i].Value=="LessThanOrEqualTo")||(this.Items[i].Value=="Between")||(this.Items[i].Value=="NotBetween")){
this.Items[i].Control.style.display="none";
continue;
}
}
if(_2ce.DataTypeName!="System.String"){
if((this.Items[i].Value=="StartsWith")||(this.Items[i].Value=="EndsWith")||(this.Items[i].Value=="Contains")||(this.Items[i].Value=="DoesNotContain")||(this.Items[i].Value=="IsEmpty")||(this.Items[i].Value=="NotIsEmpty")){
this.Items[i].Control.style.display="none";
continue;
}
}
if(_2ce.FilterListOptions=="VaryByDataType"){
if(this.Items[i].Value=="Custom"){
this.Items[i].Control.style.display="none";
continue;
}
}
this.Items[i].Control.style.display="";
}
this.SelectItem(_2ce.CurrentFilterFunction);
var args={Menu:this,TableView:this.Owner,Column:this.Column,Event:e};
if(!RadGridNamespace.FireEvent(this.Owner,"OnFilterMenuShowing",[this.Owner,args])){
return;
}
this.Control.style.display="";
this.Control.style.top=e.clientY+document.documentElement.scrollTop+document.body.scrollTop+5+"px";
this.Control.style.left=e.clientX+document.documentElement.scrollLeft+document.body.scrollLeft+5+"px";
this.AttachHideEvents();
};
RadGridNamespace.RadGrid.prototype.InitializeFilterMenu=function(_2d2){
if(this.AllowFilteringByColumn||_2d2.AllowFilteringByColumn){
if(!_2d2||!_2d2.Control){
return;
}
if(!_2d2.Control.tHead){
return;
}
if(!_2d2.IsItemInserted){
var _2d3=_2d2.Control.tHead.rows[_2d2.Control.tHead.rows.length-1];
}else{
var _2d3=_2d2.Control.tHead.rows[_2d2.Control.tHead.rows.length-2];
}
if(!_2d3){
return;
}
var _2d4=_2d3.getElementsByTagName("img");
var _2d5=this;
if(!_2d2.Columns){
return;
}
if(!_2d2.Columns[0]){
return;
}
var _2d6=_2d2.Columns[0].FilterImageUrl;
for(var i=0;i<_2d4.length;i++){
var _2d8=RadGridNamespace.EncodeURI(_2d6);
if(_2d4[i].getAttribute("src").indexOf(_2d8)==-1){
continue;
}
_2d4[i].onclick=function(e){
if(!e){
var e=window.event;
}
e.cancelBubble=true;
var _2da=this.parentNode.cellIndex;
if(window.attachEvent&&!window.opera&&!window.netscape){
_2da=RadGridNamespace.GetRealCellIndexFormCells(this.parentNode.parentNode.cells,this.parentNode);
}
_2d5.FilteringMenu.Show(_2d2.Columns[_2da],e);
if(e.preventDefault){
e.preventDefault();
}else{
e.returnValue=false;
return false;
}
};
}
this.FilteringMenu=new RadGridNamespace.RadGridFilterMenu(this.FilterMenu,_2d2);
}
};
RadGridNamespace.RadGrid.prototype.DisposeFilterMenu=function(_2db){
if(this.FilteringMenu!=null){
this.FilteringMenu.Dispose();
this.FilteringMenu=null;
}
};
RadGridNamespace.GetRealCellIndexFormCells=function(_2dc,cell){
for(var i=0;i<_2dc.length;i++){
if(_2dc[i]==cell){
return i;
}
}
};
if(typeof (window.RadGridNamespace)=="undefined"){
window.RadGridNamespace=new Object();
}
RadGridNamespace.Slider=function(_2df){
RadControlsNamespace.DomEventMixin.Initialize(this);
if(!document.readyState||document.readyState=="complete"||window.opera){
this._constructor(_2df);
}else{
this.objectData=_2df;
this.AttachDomEvent(window,"load","OnWindowLoad");
}
};
RadGridNamespace.Slider.prototype.OnWindowLoad=function(e){
this.DetachDomEvent(window,"load","OnWindowLoad");
this._constructor(this.objectData);
this.objectData=null;
};
RadGridNamespace.Slider.prototype._constructor=function(_2e1){
var _2e2=this;
for(var _2e3 in _2e1){
this[_2e3]=_2e1[_2e3];
}
this.Owner=window[this.OwnerID];
this.OwnerGrid=window[this.OwnerGridID];
this.Control=document.getElementById(this.ClientID);
if(this.Control==null){
return;
}
this.Control.unselectable="on";
this.Control.parentNode.style.padding="10px";
this.ToolTip=document.createElement("div");
this.ToolTip.unselectable="on";
this.ToolTip.style.backgroundColor="#F5F5DC";
this.ToolTip.style.border="1px outset";
this.ToolTip.style.font="icon";
this.ToolTip.style.padding="2px";
this.ToolTip.style.marginTop="5px";
this.ToolTip.style.marginBottom="15px";
this.Control.appendChild(this.ToolTip);
this.Line=document.createElement("hr");
this.Line.unselectable="on";
this.Line.style.width="100%";
this.Line.style.height="2px";
this.Line.style.backgroundColor="buttonface";
this.Line.style.border="1px outset threedshadow";
this.Control.appendChild(this.Line);
this.Thumb=document.createElement("div");
this.Thumb.unselectable="on";
this.Thumb.style.position="relative";
this.Thumb.style.width="8px";
this.Thumb.style.marginTop="-15px";
this.Thumb.style.height="16px";
this.Thumb.style.backgroundColor="buttonface";
this.Thumb.style.border="1px outset threedshadow";
this.Control.appendChild(this.Thumb);
this.Link=document.createElement("a");
this.Link.unselectable="on";
this.Link.style.width="100%";
this.Link.style.height="100%";
this.Link.style.display="block";
this.Link.href="javascript:void(0);";
this.Thumb.appendChild(this.Link);
this.LineX=RadGridNamespace.FindPosX(this.Line);
this.AttachDomEvent(this.Control,"mousedown","OnMouseDown");
this.AttachDomEvent(this.Link,"keydown","OnKeyDown");
var _2e4=this.OwnerGrid.CurrentPageIndex/this.OwnerGrid.MasterTableView.PageCount;
this.SetPosition(_2e4*this.Line.offsetWidth);
var _2e5=parseInt(this.Thumb.style.left)/this.Line.offsetWidth;
var _2e6=Math.round((this.OwnerGrid.MasterTableView.PageCount-1)*_2e5);
this.OwnerGrid.ApplyPagerTooltipText(this.ToolTip,this.OwnerGrid.CurrentPageIndex,this.OwnerGrid.MasterTableView.PageCount);
};
RadGridNamespace.Slider.prototype.Dispose=function(){
this.DisposeDomEventHandlers();
for(var _2e7 in this){
this[_2e7]=null;
}
this.Control=null;
this.Line=null;
this.Thumb=null;
this.ToolTip=null;
};
RadGridNamespace.Slider.prototype.OnKeyDown=function(e){
this.AttachDomEvent(this.Link,"keyup","OnKeyUp");
if(e.keyCode==39){
this.SetPosition(parseInt(this.Thumb.style.left)+this.Thumb.offsetWidth);
}
if(e.keyCode==37){
this.SetPosition(parseInt(this.Thumb.style.left)-this.Thumb.offsetWidth);
}
if(e.keyCode==39||e.keyCode==37){
var _2e9=parseInt(this.Thumb.style.left)/this.Line.offsetWidth;
var _2ea=Math.round((this.OwnerGrid.MasterTableView.PageCount-1)*_2e9);
this.OwnerGrid.ApplyPagerTooltipText(this.ToolTip,_2ea,this.OwnerGrid.MasterTableView.PageCount);
}
};
RadGridNamespace.Slider.prototype.OnKeyUp=function(e){
this.DetachDomEvent(this.Link,"keyup","OnKeyUp");
if(e.keyCode==39||e.keyCode==37){
var _2ec=this;
setTimeout(function(){
_2ec.ChangePage();
},100);
}
};
RadGridNamespace.Slider.prototype.OnMouseDown=function(e){
this.DetachDomEvent(this.Control,"mousedown","OnMouseDown");
if(((window.netscape||window.opera||navigator.userAgent.indexOf("Safari")!=-1))&&(e.button==0)||(e.button==1)){
this.SetPosition(RadGridNamespace.GetEventPosX(e)-this.LineX);
this.AttachDomEvent(document,"mousemove","OnMouseMove");
this.AttachDomEvent(document,"mouseup","OnMouseUp");
}
};
RadGridNamespace.Slider.prototype.OnMouseUp=function(e){
this.DetachDomEvent(document,"mousemove","OnMouseMove");
this.DetachDomEvent(document,"mouseup","OnMouseUp");
var _2ef=parseInt(this.Thumb.style.left)/this.Line.offsetWidth;
var _2f0=Math.round((this.OwnerGrid.MasterTableView.PageCount-1)*_2ef);
this.OwnerGrid.ApplyPagerTooltipText(this.ToolTip,_2f0,this.OwnerGrid.MasterTableView.PageCount);
var _2f1=this;
setTimeout(function(){
_2f1.ChangePage();
},100);
};
RadGridNamespace.Slider.prototype.OnMouseMove=function(e){
this.SetPosition(RadGridNamespace.GetEventPosX(e)-this.LineX);
var _2f3=parseInt(this.Thumb.style.left)/this.Line.offsetWidth;
var _2f4=Math.round((this.OwnerGrid.MasterTableView.PageCount-1)*_2f3);
this.OwnerGrid.ApplyPagerTooltipText(this.ToolTip,_2f4,this.OwnerGrid.MasterTableView.PageCount);
};
RadGridNamespace.Slider.prototype.GetPosition=function(e){
this.SetPosition(RadGridNamespace.GetEventPosX(e)-this.LineX);
};
RadGridNamespace.Slider.prototype.SetPosition=function(_2f6){
if(_2f6>=0&&_2f6<=this.Line.offsetWidth){
this.Thumb.style.left=_2f6+"px";
}
};
RadGridNamespace.Slider.prototype.ChangePage=function(){
var _2f7=parseInt(this.Thumb.style.left)/this.Line.offsetWidth;
var _2f8=Math.round((this.OwnerGrid.MasterTableView.PageCount-1)*_2f7);
if(this.OwnerGrid.CurrentPageIndex==_2f8){
this.AttachDomEvent(this.Control,"mousedown","OnMouseDown");
return;
}
this.OwnerGrid.SavePostData("AJAXScrolledControl",(this.OwnerGrid.GridDataDiv)?this.OwnerGrid.GridDataDiv.scrollLeft:"",(this.OwnerGrid.GridDataDiv)?this.OwnerGrid.LastScrollTop:"",(this.OwnerGrid.GridDataDiv)?this.OwnerGrid.GridDataDiv.scrollTop:"",_2f8);
var _2f9=this.OwnerGrid.ClientSettings.PostBackFunction;
_2f9=_2f9.replace("{0}",this.OwnerGrid.UniqueID);
eval(_2f9);
};

//BEGIN_ATLAS_NOTIFY
if (typeof(Sys) != "undefined"){if (Sys.Application != null && Sys.Application.notifyScriptLoaded != null){Sys.Application.notifyScriptLoaded();}}
//END_ATLAS_NOTIFY
