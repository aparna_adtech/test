<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PTOBraceContent.aspx.cs"
    Inherits="BregVision.PTOBraceContent" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<span id="Div1" runat="server" visible="false"></span>

<script runat="server" id="scrCodeBehind" type="text/C#">
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToInt16(Request.QueryString["ID"]) == 1)

                Div1.InnerHtml = GetDiv1();
                Div1.Visible = true;

            if (Convert.ToInt16(Request.QueryString["ID"]) == 2)

                Div1.InnerHtml = GetDiv2();
                Div1.Visible = true;

                //Div2.Visible = true;

             if (Convert.ToInt16(Request.QueryString["ID"]) == 3)

                Div1.InnerHtml = GetDiv3();
                Div1.Visible = true;

            if (Convert.ToInt16(Request.QueryString["ID"]) == 4)

                Div1.InnerHtml = GetDiv2();
                Div1.Visible = true;

                //Div3.Visible = true;

        }

        private string GetDiv1()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();


            sb.Append("<table style='height:205px' width=85% border=0 valign=top>");
            sb.Append("<tr>");
            sb.Append("<td valign=middle style='width:220px' rowspan=1>");
            sb.Append("<img src='Images/ShellAirAnkleWalker/ShellAirAnkleWalker.gif' border=0>");
            sb.Append("</td>");

            //sb.Append("<td colspan=3 style='font-weight:bold;font:arial'>Patellofermoral Braces</td>");        
            //sb.Append("</td></tr>");        
            //sb.Append("</td></tr>");        

            sb.Append("<td width=5px></td><td><table border=0 width='100%'><tr><td>");
            sb.Append("<label style='color: #617099; font-size: 12pt; font-weight: bold; font-family: arial'>BREG's Shell Air Ankle Walker");
            sb.Append("</label></td><td align=right rowspan=2><img src='Images/ShellAirAnkleWalker/NowAvailable.gif' border=0></td></tr>");
            //sb.Append("</label><br><br><label style='color: #00036B; font-size: 10pt; font-weight:normal; font-family: arial'>BREG patellofemoral braces are specifically designed to provide unmatched comfort and maximum pain relief as a result of maltracking or patellar instability.");
            //sb.Append("</label><br><br><label style='color: #00036B; font-size: 10pt; font-weight: bold; font-family: arial'>Patient Comfort & Compliance</Label>");
            sb.Append("<tr><td>BREG's Shell Air Ankle Walker combines the protection of a full shell with the comfort and support of a pneumatic walker.");
            sb.Append("</td></tr><tr></table><table border=0 width='100%'><tr><td><label style='color:#0002CF; font-family: arial; font-size:10pt; font-weight:bold'>Features and Benefits</label><label style='color:#0002CF; font-family: arial; font-size:9pt; font-weight: bold'><ul><li>Three piece design includes posterior and anterior shells for maximum immobilization</li><li>Dual air chambers positioned medial/lateral at the ankle can be inflated for ideal compression and a custom fit</li><li>Lightweight low-profile design</li><li>Rocker sole facilitates a more natural gait</li><li>Deluxe liner includes a detachable toe cover</li><li>Wider foot base design for greater patient comfort and support</li><li>Includes complimentary walker sock</li><li>Latex free</li></ul></td>");
            //sb.Append("<td><label style='color:#0002CF; font-family: arial; font-size:10pt; font-weight:bold'>Indications</label><label style='color:#0002CF; font-family: arial; font-size:10pt; font-weight: bold'><ul><li>Lateral Patella Subluxation</li><li>Chronic Patella maltracking</li><li>Mild Chondrmalacia</li></ul>  </td></tr></table></tr>");
            //sb.Append("</label><br><br><label style='color:#0002CF; font-family: arial; font-size:10pt; font-weight:bold'>Additional Features and Benefits</label>");
            //sb.Append("</label><label style='color:#0002CF; font-family: arial; font-size:10pt; font-weight: bold'><ul><li>Lightweight</li><li>Low profile footbed height</li><li>Malleable aluminum uprights for a customized fit</li><li>Wide footbed accomodates swelling and post-op bandages</li><li> XS-XL sizes</li></ul>");

            //sb.Append("To see the 'Vision' of the future in inventory management, email<a href='mailto:orthopracticesolutions@breg.com'> Breg</a>  for more information.");

            sb.Append("</table>");

            //sb.Append("</label></td></tr></table>");


 
            return sb.ToString();
        }

        private string GetDiv2()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();


            sb.Append("<table style='height:205px' width=85% border=0 valign=top>");
            sb.Append("<tr>");
            sb.Append("<td valign=middle style='width:220px' rowspan=1>");
            sb.Append("<img src='Images/AnkleLaceUp/AnkleLaceUp.gif' border=0>");
            sb.Append("</td>");

            //sb.Append("<td colspan=3 style='font-weight:bold;font:arial'>Patellofermoral Braces</td>");        
            //sb.Append("</td></tr>");        
            //sb.Append("</td></tr>");        

            sb.Append("<td width=5px></td><td><table border=0 width='100%'><tr><td>");
            sb.Append("<label style='color: #617099; font-size: 12pt; font-weight: bold; font-family: arial'>BREG's Ankle Lace-Up with Tibia Strap");
            sb.Append("</label></td><td align=right rowspan=2><img src='Images/AnkleLaceUp/NowAvailable.gif' border=0></td></tr>");
            //sb.Append("</label><br><br><label style='color: #00036B; font-size: 10pt; font-weight:normal; font-family: arial'>BREG patellofemoral braces are specifically designed to provide unmatched comfort and maximum pain relief as a result of maltracking or patellar instability.");
            //sb.Append("</label><br><br><label style='color: #00036B; font-size: 10pt; font-weight: bold; font-family: arial'>Patient Comfort & Compliance</Label>");
            sb.Append("<tr><td>BREG�s Ankle Lace-Up with Tibia Strap provides comfortable protection and strong support, eliminating the need for taping.");
            sb.Append("</td></tr><tr></table><table border=0 width='100%'><tr><td><label style='color:#0002CF; font-family: arial; font-size:10pt; font-weight:bold'>Features and Benefits</label><label style='color:#0002CF; font-family: arial; font-size:9pt; font-weight: bold'><ul><li>Designed to support and protect the ankle</li><li>Figure 8 configuration eliminates taping</li><li>Thin and strong ballistic type nylon is less bulky for a comfortable fit</li><li>Straps are made up of non-stretch lightweight nylon</li><li>Elastic panel at the achilles provides superior comfort and fit</li><li>Knit tongue is cool and provides for easy application</li><li>Latex free</li><li>Available in 7 versatile sizes</li></ul></td>");
            //sb.Append("<td><label style='color:#0002CF; font-family: arial; font-size:10pt; font-weight:bold'>Indications</label><label style='color:#0002CF; font-family: arial; font-size:10pt; font-weight: bold'><ul><li>Lateral Patella Subluxation</li><li>Chronic Patella maltracking</li><li>Mild Chondrmalacia</li></ul>  </td></tr></table></tr>");
            //sb.Append("</label><br><br><label style='color:#0002CF; font-family: arial; font-size:10pt; font-weight:bold'>Additional Features and Benefits</label>");
            //sb.Append("</label><label style='color:#0002CF; font-family: arial; font-size:10pt; font-weight: bold'><ul><li>Lightweight</li><li>Low profile footbed height</li><li>Malleable aluminum uprights for a customized fit</li><li>Wide footbed accomodates swelling and post-op bandages</li><li> XS-XL sizes</li></ul>");

            //sb.Append("To see the 'Vision' of the future in inventory management, email<a href='mailto:orthopracticesolutions@breg.com'> Breg</a>  for more information.");

            sb.Append("</table>");

            //sb.Append("</label></td></tr></table>");
            
     
            return sb.ToString();
        }

        private string GetDiv3()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();


            sb.Append("<table style='height:205px' width=85% border=0 valign=top>");
            sb.Append("<tr>");
            sb.Append("<td valign=middle style='width:220px' rowspan=1>");
            sb.Append("<img src='Images/BasicLumbarSupport/BasicLumbarSupport.gif' border=0>");
            sb.Append("</td>");

            //sb.Append("<td colspan=3 style='font-weight:bold;font:arial'>Patellofermoral Braces</td>");        
            //sb.Append("</td></tr>");        
            //sb.Append("</td></tr>");        

            sb.Append("<td width=5px></td><td><table border=0 width='100%'><tr><td>");
            sb.Append("<label style='color: #617099; font-size: 12pt; font-weight: bold; font-family: arial'>BREG's Basic Lumbar Support");
            sb.Append("</label></td><td align=right rowspan=2><img src='Images/BasicLumbarSupport/New.jpg' border=0></td></tr>");
            //sb.Append("</label><br><br><label style='color: #00036B; font-size: 10pt; font-weight:normal; font-family: arial'>BREG patellofemoral braces are specifically designed to provide unmatched comfort and maximum pain relief as a result of maltracking or patellar instability.");
            //sb.Append("</label><br><br><label style='color: #00036B; font-size: 10pt; font-weight: bold; font-family: arial'>Patient Comfort & Compliance</Label>");
            sb.Append("<tr><td>The Basic Lumbar Support provides mild compression for lumbar stabilization.  The durable, construction offers double side pulls for added support. A neoprene pocket includes a rigid posterior panel for support and lumbar stabilization.");
            sb.Append("</td></tr><tr></table><table border=0 width='100%'><tr><td><label style='color:#0002CF; font-family: arial; font-size:10pt; font-weight:bold'>Features and Benefits</label><label style='color:#0002CF; font-family: arial; font-size:9pt; font-weight: bold'><ul><li>Rigid posterior panel for support</li><li>Durable Construction</li><li>Double side pulls for added stabilization</li><li>Latex free</li></ul></td>");
            sb.Append("<td><label style='color:#0002CF; font-family: arial; font-size:10pt; font-weight:bold'>Recommended HCPCs: L0626</label></td></tr></table></tr>");
            //sb.Append("</label><br><br><label style='color:#0002CF; font-family: arial; font-size:10pt; font-weight:bold'>Additional Features and Benefits</label>");
            //sb.Append("</label><label style='color:#0002CF; font-family: arial; font-size:10pt; font-weight: bold'><ul><li>Lightweight</li><li>Low profile footbed height</li><li>Malleable aluminum uprights for a customized fit</li><li>Wide footbed accomodates swelling and post-op bandages</li><li> XS-XL sizes</li></ul>");

            //sb.Append("To see the 'Vision' of the future in inventory management, email<a href='mailto:orthopracticesolutions@breg.com'> Breg</a>  for more information.");

            sb.Append("</table>");

            //sb.Append("</label></td></tr></table>");


            return sb.ToString();
        }


        private string GetDiv4()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.Append("<table border=0 style='height:285px' width=85%>");
            sb.Append("<tr>");
            sb.Append("<td valign=middle style='width:220px'>");
            sb.Append("<img src='Images/SilverPro/SilverPro2.jpg' border=0>");
            sb.Append("</td>");

            //sb.Append("<td colspan=3 style='font-weight:bold;font:arial'>Patellofermoral Braces</td>");        
            //sb.Append("</td></tr>");        
            //sb.Append("</td></tr>");        

            sb.Append("<td width=15px></td><td>");
            sb.Append("<label style='color: #00023E; font-size: 11pt; font-weight: bold; font-family: arial'>TAKE  A PROACTIVE APPROACH TO WOUND CARE MANAGEMENT");
            sb.Append("</label>");

            sb.Append("</td></tr></table>");
            
            
 

            return sb.ToString();
        }
</script>
    



