using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.WebControls;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

namespace BregVision
{
    public partial class OrderApproval : BregVision.Bases.PageBase
    {
        string strGrandTotal;
        Int32 BVOID;

        protected void InitializeComponent()
        {
            grdInvoiceSnapshot.ItemDataBound += new GridItemEventHandler(grdInvoiceSnapshot_ItemDataBound);
            //grdOrderApproval.ItemDataBound += new GridItemEventHandler(grdOrderApproval_ItemDataBound);

         }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Response.Write("PLID: ");
            //Response.Write(Session["PracticeLocationID"]);
            //Response.Write("Shopping Cart ID: ");
            //Response.Write(GetShoppingCartID(Convert.ToInt16(Session["PracticeLocationID"])));

            //Response.Write("Shipping Type: ");
            //Response.Write(Session["ShippingTypeID"]);

            if (Session["OrderApprovalQueryString"]== null) //User has reached this page by mistake
                Response.Redirect("default.aspx");


            if (!Page.IsPostBack)
            {
                string strGrandTotal = "notsetyet";
                if (Session["PracticeLocationID"] != null)
                {
                    decimal decGrandTotal = GetInvoiceSnapshot_GrandTotal(Convert.ToInt32(Session["PracticeLocationID"]));
                    strGrandTotal = String.Format("{0:$#,0.00}", decGrandTotal);
                    Session["InvSnap_GrandTotal"] = strGrandTotal;
                }
            }

            if (Session["InvSnap_GrandTotal"] != null)
            {
                strGrandTotal = Session["InvSnap_GrandTotal"].ToString();
            }
            
            

        }

        //New Code

        public DataSet GetInvoiceSnapshot_Parent(int PracticeLocationID)
        {


            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");

            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetInvoiceSnapshotShoppingCartItems_ParentLevel_Given_PracticeLocationID");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);

            DataSet ds = db.ExecuteDataSet(dbCommand);

            return ds;

        }

        //[usp_GetInvoiceSnapshotShoppingCartItems_ParentLevel_GrandTotal_Given_PracticeLocationID]

        //  20071019 JB  Get Parent with Total given the PracticeLocationID
        public decimal GetInvoiceSnapshot_GrandTotal(int PracticeLocationID)
        {

            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");

            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetInvoiceSnapshotShoppingCartItems_ParentLevel_GrandTotal_Given_PracticeLocationID");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);

            DataTable dt = db.ExecuteDataSet(dbCommand).Tables[0];
            DataRow dr = dt.Rows[0];
            string sGrandTotal = dr.ItemArray[0].ToString();
            //  ecimal grandTotal = Convert.ToDecimal( strGrandTotal );

            decimal decGTotal = Convert.ToDecimal(sGrandTotal);

            return decGTotal;
        }


        public DataSet GetInvoiceSnapshot_Detail(int PracticeLocationID)
        {
            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetInvoiceSnapshotShoppingCartItems_Detail");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;


        }

        protected void grdInvoiceSnapshot_NeedDataSource(object source, Telerik.WebControls.GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable)
            {
                //  20071019 use PracticeLocationID
                grdInvoiceSnapshot.DataSource = GetInvoiceSnapshot_Parent(Convert.ToInt16(Session["PracticeLocationID"]));
                //grdInvoiceSnapshot.DataSource = GetInvoiceSnapshot_Parent(Convert.ToInt16(Session["PracticeID"]));
            }
        }

        protected void grdInvoiceSnapshot_DetailTableDataBind(object source, Telerik.WebControls.GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            //switch (e.DetailTableView.DataMember)
            //{
            //case "usp_GetAllProductDatabyLocation":
            //    {
            //string CustomerID = dataItem.GetDataKeyValue("CustomerID").ToString();
            //e.DetailTableView.DataSource = GetDataTable("SELECT * FROM Orders WHERE CustomerID = '" + CustomerID + "'");
            e.DetailTableView.DataSource = GetInvoiceSnapshot_Detail(Convert.ToInt16(Session["PracticeLocationID"]));
            //        break;
            //    }

            //case "OrderDetails":
            //    {
            //        string OrderID = dataItem.GetDataKeyValue("OrderID").ToString();
            //        e.DetailTableView.DataSource = GetDataTable("SELECT * FROM [Order Details] WHERE OrderID = " + OrderID);
            //        break;
            //    }
            //}
        }

        //decimal total;
        int quantity;
        decimal subtotal;
        protected void grdInvoiceSnapshot_ItemDataBound(object sender, Telerik.WebControls.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                try
                {
                    //  20071022 JB
                    GridDataItem dataItem = e.Item as GridDataItem;
                    //decimal fieldValue = System.Convert.ToDecimal(dataItem["ActualWholesaleCost"].Text);
                    //decimal totalCostItem = Convert.ToDecimal(dataItem["TotalCost"].Text.Replace("$",""));           //fieldValue * System.Convert.ToDecimal(fieldValue2);
                    //dataItem["TotalCost"].Text = totalCostItem.ToString();
                    //Int16 fieldValue = 1;
                    //total += fieldValue;

                    int fieldValue2 = (dataItem["Quantity"] != null) ? Convert.ToInt32(dataItem["Quantity"].Text) : 0;

                    Label lblLineTotalLabel = (Label)dataItem["LineTotal"].FindControl("lblLineTotal");
                    string lineTotalValue = lblLineTotalLabel.Text.Replace("$", "").Replace(",", "");
                    decimal lineTotal = Convert.ToDecimal(lineTotalValue);

                    quantity += fieldValue2;
                    subtotal += lineTotal;
                }
                catch
                {

                }



            }
            if (e.Item is GridFooterItem)
            {

                GridFooterItem footerItem = e.Item as GridFooterItem;
                //footerItem["UnitCost"].Text = "Total: " + total.ToString();
                //footerItem["Quantity"].Text = "# Items in Cart: " + quantity.ToString();
                //footerItem["totalCost"].Text = "Sub Total: " + totalCost.ToString();
                ///footerItem["totalCost"].Text =  String.Format("Sub Total: {0:$#,#.00}", totalCost);

                try
                {
                    if (footerItem["TotalCost"] != null)
                    {
                        Label lblGrandTotalLabel = (Label)footerItem["TotalCost"].FindControl("lblGrandTotal");
                        lblGrandTotalLabel.Text = String.Format("{0:$#,0.00}", strGrandTotal);
                        lblGrandTotalLabel.Font.Underline = true;
                        //footerItem["GrandTotal"].Text = "Grand Total: $44,444.44";
                    }
                }
                catch
                {
                }
                try
                {
                    if (footerItem["Quantity"] != null)
                    {
                        //footerItem["Quantity"].Text = String.Format("# Items in Cart: {0:#,#}", quantity);
                        //footerItem["LineTotal"].Text = String.Format("Sub Total: {0:$#,0.00}", subtotal);

                        footerItem["Quantity"].Text = String.Format("{0:#,#}", quantity);
                        footerItem["LineTotal"].Text = String.Format("{0:$#,0.00}", subtotal);
                        footerItem["BrandShortName"].Text = "Sub Total:";

                        footerItem["Quantity"].Font.Overline = true;
                        footerItem["LineTotal"].Font.Overline = true;
                        footerItem["BrandShortName"].Font.Overline = true;
                    }
                }
                catch
                {
                }
                //GridBoundColumn column = (GridBoundColumn)grdShoppingCart.MasterTableView.GetColumn("UnitCost");
                //column.DataFormatString = "{0:c}"; 
            }
        }


        //



        //public DataSet GetOrderApproval_Parent(int PracticeID)
        //{
        //    int rowsAffected = 0;

        //    Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
        //    DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetInvoiceSnapshotShoppingCartItems_ParentLevel");

        //    db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, PracticeID);
        //    DataSet ds = db.ExecuteDataSet(dbCommand);
        //    return ds;


        //}

        //public DataSet GetOrderApproval_Detail(int PracticeLocationID)
        //{
        //    int rowsAffected = 0;

        //    Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
        //    DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetInvoiceSnapshotShoppingCartItems_Detail");

        //    db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);
        //    DataSet ds = db.ExecuteDataSet(dbCommand);
        //    return ds;


        //}

        //protected void grdOrderApproval_NeedDataSource(object source, Telerik.WebControls.GridNeedDataSourceEventArgs e)
        //{
        //    if (!e.IsFromDetailTable)
        //    {
        //        grdOrderApproval.DataSource = GetOrderApproval_Parent(Convert.ToInt16(Session["PracticeID"]));
        //    }
        //}

        //protected void grdOrderApproval_DetailTableDataBind(object source, Telerik.WebControls.GridDetailTableDataBindEventArgs e)
        //{
        //    GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
        //    //switch (e.DetailTableView.DataMember)
        //    //{
        //    //case "usp_GetAllProductDatabyLocation":
        //    //    {
        //    //string CustomerID = dataItem.GetDataKeyValue("CustomerID").ToString();
        //    //e.DetailTableView.DataSource = GetDataTable("SELECT * FROM Orders WHERE CustomerID = '" + CustomerID + "'");
        //    e.DetailTableView.DataSource = GetOrderApproval_Detail(Convert.ToInt16(Session["OrderApprovalQueryString"]));
        //    //        break;
        //    //    }

        //    //case "OrderDetails":
        //    //    {
        //    //        string OrderID = dataItem.GetDataKeyValue("OrderID").ToString();
        //    //        e.DetailTableView.DataSource = GetDataTable("SELECT * FROM [Order Details] WHERE OrderID = " + OrderID);
        //    //        break;
        //    //    }
        //    //}
        //}

        //decimal total;
        //int quantity;
        //decimal totalCost;
        //protected void grdOrderApproval_ItemDataBound(object sender, Telerik.WebControls.GridItemEventArgs e)
        //{
        //    if (e.Item is GridDataItem)
        //    {
        //        try
        //        {

        //            GridDataItem dataItem = e.Item as GridDataItem;
        //            decimal fieldValue = System.Convert.ToDecimal(dataItem["ActualWholesaleCost"].Text);
        //            int fieldValue2 = int.Parse(dataItem["Quantity"].Text);
        //            decimal totalCostItem = fieldValue * System.Convert.ToDecimal(fieldValue2);
        //            dataItem["TotalCost"].Text = totalCostItem.ToString();

        //            //Int16 fieldValue = 1;
        //            total += fieldValue;
        //            quantity += fieldValue2;
        //            totalCost += totalCostItem;
        //            //Response.Write(dataItem["QuantityHidden"].Text);      
        //        }
        //        catch
        //        {

        //        }



        //    }
        //    if (e.Item is GridFooterItem)
        //    {
        //        try
        //        {
        //            GridFooterItem footerItem = e.Item as GridFooterItem;
        //            //footerItem["UnitCost"].Text = "Total: " + total.ToString();
        //            footerItem["Quantity"].Text = "# Items in Cart: " + quantity.ToString();
        //            footerItem["totalCost"].Text = "Sub Total: " + totalCost.ToString();
        //        }
        //        catch
        //        {
        //        }
        //        //GridBoundColumn column = (GridBoundColumn)grdShoppingCart.MasterTableView.GetColumn("UnitCost");
        //        //column.DataFormatString = "{0:c}"; 
        //    }
        //}

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt16(radOrderStatus.SelectedIndex) == -1)
            {
                lblStatus.Text = "Approved or Denied must be selected.";
                radOrderStatus.Focus();
                return;
            }

            if (Convert.ToInt16(radOrderStatus.SelectedValue) == 1) //Denied
            {
                if (txtComments.Value.Length == 0)
                {
                    lblStatus.Text = "Comments are needed for denied purchase orders";
                    txtComments.Focus();
                    return;
                }
            }  

        //All is good, create the purchase order
            //Response.Write("PLID: ");
            //Response.Write(Session["PracticeLocationID"]);
            //Response.Write("Shopping Cart ID: ");
            //Response.Write(GetShoppingCartID(Convert.ToInt16(Session["PracticeLocationID"])));
            
            //Response.Write("Shipping Type: ");
            //Response.Write(Session["ShippingTypeID"]);
            //return;

            Int32 PLID = Convert.ToInt32(Session["PracticeLocationID"]);
            
            Int32 SCID = GetShoppingCartID(Convert.ToInt32(Session["PracticeLocationID"]));
            
            Int16 STID = Convert.ToInt16(Session["ShippingTypeID"]);
            
            int PONumber = CreatePurchaseOrder(PLID, SCID, 1, STID);

            DataSet dsCustomBraceInfo = DAL.PracticeLocation.Inventory.GetCustomBraceExtendedInfoFromOrder(SCID);
            bool isCustomBrace = false;
            if (dsCustomBraceInfo.Tables[0].Rows.Count == 1)
            {
                isCustomBrace = true;
            }
            
            //int PONumber = CreatePurchaseOrder(Convert.ToInt16(Session["PracticeLocationID"]), GetShoppingCartID(Convert.ToInt16(Session["PracticeLocationID"])), 1, Convert.ToInt16(Session["ShippingTypeID"]));

            lblStatus.Text = String.Concat("Purchase Order Number:", PONumber.ToString(), " has been created for the following order. Please use this number when referencing this PO.");
            //lblStatus.Text = "A purchase order has been created for the following order. Please use this number when referencing this PO.";
            btnSubmit.Enabled = false;

            //New Code
            FaxEmailPOs FEPos = new FaxEmailPOs();
            //FEPos.BregVisionOrderID = 26;
            BVOID = Convert.ToInt32(PONumber.ToString()) - 100000;
            ViewState["BVOID"] = BVOID;
            //if (ViewState["DispenseID"] != null)
            //{
            //    DispenseID = (Int32)ViewState["DispenseID"];
            //}

            //Creates Order Confirmation data
            string PODetail = FEPos.GetSuppliersAndLineItems(BVOID);
            lblPODetail.Text = PODetail;
            
            //Fax order
            FEPos.PracticeLocationID = Convert.ToInt32(Session["PracticeLocationID"]);
            FEPos.PracticeName = Session["PracticeName"].ToString();
            FEPos.PracticeLocationName = Session["PracticeLocationName"].ToString();

            byte[] customBrace = new byte[0];

            if (isCustomBrace == true)
            {
                customBrace = FEPos.PrepareFaxEmailforCustomBrace(practiceID, BVOID, dsCustomBraceInfo.Tables[0].Rows[0]);
            }

            FEPos.PrepareFaxEmailforSupplier(BVOID, customBrace, SCID, visionSessionProvider);
            
            grdInvoiceSnapshot.Visible = false;
            divOrderConf.Visible = true;
            divOrderDetails.Visible = false;


        }

        protected Int32 GetShoppingCartID(Int32 PracticeLocationID)
        {
            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetShoppingCartID");
            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);

            Int32 SQLReturn = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            return SQLReturn;
        }

        protected Int32 CreatePurchaseOrder(Int32 PracticeLocationID,
                Int32 ShoppingCartID, Int32 UserID, Int32 ShippingTypeID)
        {

            //return 0;






            //Int32 PurchaseOrderID = 0;
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_CreatePurchaseOrder");
            
            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);
            db.AddInParameter(dbCommand, "ShoppingCartID", DbType.Int32, ShoppingCartID);
            db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
            db.AddInParameter(dbCommand, "ShippingTypeID", DbType.Int32, ShippingTypeID);
            db.AddOutParameter(dbCommand, "PurchaseOrderID", DbType.Int32, 4);
            
            Int32 SQLReturn = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            
            Int32 return_value = Convert.ToInt32(db.GetParameterValue(dbCommand, "PurchaseOrderID"));
            
            return return_value;
        }

        protected void btnPrintReceipt_Click(object sender, EventArgs e)
        {
            string strURL = String.Concat("Authentication/OrderConfirmationReceipt.aspx?BVOID=", (Int32)ViewState["BVOID"]);
            ResponseHelper.Redirect(strURL, "_blank", "");
        }

        public static class ResponseHelper
        {
            public static void Redirect(string url, string target, string windowFeatures)
            {
                HttpContext context = HttpContext.Current;
                if ((String.IsNullOrEmpty(target) || target.Equals("_self", StringComparison.OrdinalIgnoreCase)) && String.IsNullOrEmpty(windowFeatures))
                {
                    context.Response.Redirect(url);
                }
                else
                {
                    Page page = (Page)context.Handler; if (page == null)
                    {
                        throw new InvalidOperationException("Cannot redirect to new window outside Page context.");
                    }
                    url = page.ResolveClientUrl(url); string script;
                    if (!String.IsNullOrEmpty(windowFeatures))
                    {
                        script = @"window.open(""{0}"", ""{1}"", ""{2}"");";
                    }
                    else
                    {
                        script = @"window.open(""{0}"", ""{1}"");";
                    }
                    script = String.Format(script, url, target, windowFeatures);
                    ScriptManager.RegisterStartupScript(page, typeof(Page), "Redirect", script, true);
                }
            }
        }
        
        protected void Page_unLoaded(object sender, EventArgs e)
        {
            //Clear out Order Approval Session Variable
            Session.Remove("OrderApprovalQueryString");

        } 


    }
}
