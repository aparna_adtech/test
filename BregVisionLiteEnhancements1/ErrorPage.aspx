﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="BregVision.ErrorPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Breg Vision Error</title>
	<link runat="server" href="https://fonts.googleapis.com/css?family=Roboto:400,500,900" rel="stylesheet" />
	<style type="text/css">
		body {
			background: #ffffff;
			color: #000000;
			font-family: 'Roboto', 'Helvetica-Neue', 'Helvetica', sans-serif;
			margin: 0; /* it's good practice to zero the margin and padding of the body element to account for differing browser defaults */
			padding: 0;
			text-align: center; /* this centers the container in IE 5* browsers. The text is then set to the left aligned default in the #container selector */
		}

		#header {
			padding: 5px;
			text-align: left;
		}
</style>

</head>
<body class="twoColHybRtHdr">
	<form id="form1" runat="server">
		<div id="header">
			<img src="Images/VisionLogo.gif" />
		</div>
<br />
		<div>
			<h1>An Error has occurred.</h1>
			<p>
				An email has been sent to the Breg IT department.
			</p>
			<br />
			<br />
			<p>
				Please feel free to contact customer service
			<br />
				at <strong>888-290-8481</strong> or <a href="mailto:visioncc@breg.com"><strong>visioncc@breg.com</strong></a>
				<br />
				if you need further assistance.
			</p>
		</div>
	</form>
</body>
</html>
