﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/SiteMaster.Master" AutoEventWireup="true" 
    CodeBehind="InvoiceManagerBlend.aspx.cs" Inherits="BregVision.InvoiceManager" %>

<%@ MasterType VirtualPath="~/MasterPages/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<%--    <script src="http://code.angularjs.org/1.3.11/angular.js"></script>--%>
<style type="text/css">

</style>
    <link href="Styles/InvoiceManager.css" rel="stylesheet" />
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
    <div class="invDiv" ng-app="invoiceApp">
		<div class="wrapper" ng-controller="InvoiceCtrl as invCtrl"> 
		<!-- header -->
			<div class="header-top"> 
				<p  ng-show="false">Invoice number was saved successfully!</p><i title="click to close" class="fa fa-times"></i>
			</div>
			<div class="content-bar content-bar_top"> 
			<!-- left sidebar -->
				<div class="left-sidebar">
					<div class="top-sidebar sidebar-bg"> 
					
					<!-- Invoice Search  -->
					<h3 class="heading-three">Search Invoice</h3>
						<div class="top-input"> 
							<div class="input-checked"> 
								<input id="chek" type="checkbox" />
								<label for="chek">show inactive supplier</label>
							</div>
							<div class="input-name"> 
								<label for="text">supplier name</label>
								<select name="InvoiceSearchSupplierSelect" id="InvoiceSearchSupplierSelect" 
                                    ng-options= "supplier.Name for supplier in invCtrl.InvoiceSearchSuppliers track by supplier.Id" 
                                    ng-model="invCtrl.SearchInvoice.Supplier ">
								</select>
							</div>
							<div class="from-date"> 
								<label for="rangeStart">from date</label>
								<input name="rangeStart" type="date" ng-model="invCtrl.dateFrom" ng-change="ctrl.startTimeChange()" placeholder="yyyy-MM-dd" required=""/>
								<div class="input-icon"> 
									<i class="fa fa-calendar"></i>
								</div>
							</div>
							<div class="to-date"> 
   								<label for="rangeEnd">To date</label>
								<input name="rangeEnd" type="date" ng-model="invCtrl.dateTo" ng-change="ctrl.endTimeChange()" placeholder="yyyy-MM-dd" required=""/>
								<div class="input-icon"> 
									<i class="fa fa-calendar"></i>
								</div>
							</div>
							<div class="invoice-number"> 
								<label for="searchInvoiceNumber">invoice number</label>
								<input name="searchInvoiceNumber" ng-model="invCtrl.SearchInvoice.Number" required="" />
							</div>
							<div class="warning" ng-show="false"> 
								<p>oops!sorry nothing found try again</p>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Invoice Search Result -->
				<div class="right-content">
					<div class="top-content"> 
						<div class="head-title"> 
							<h3 class="heading-three">invoice search result</h3><i class="fa fa-angle-right"></i>
						</div>
						<div class="top-item"> 
							<table class="first-table">
								<div class="item-head"> 
									<tr> 
										<th>Inv. Number</th>
										<th>Supplier</th>
										<th>total</th>
										<th>invoice date</th>
										<th></th>
									</tr>
								</div>
							    <div class="item-one"> 
							        <tr class="first-item first-item-color"
							            ng-click="invCtrl.SelectInvoice(inv)"
							            ng-repeat="inv in invCtrl.Invoices
                                        | filter: {Number: invCtrl.SearchInvoice.Number}
                                        | supplier: invCtrl.SearchInvoice.Supplier
                                        | dateRange:invCtrl.dateFrom.getTime():invCtrl.dateTo.getTime()"> 
							            <td>{{inv.Number}}</td><td>{{inv.Supplier.Name}}</td><td>{{inv.Total | currency}}</td><td>{{inv.Date | date: 'MM/dd/yyyy'}}</td>
							        </tr>
							    </div>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="content-bar"> 
			
			<!-- left sidebar -->
				<div class="left-sidebar left-side">
					<div class="top-sidebar sidebar-bg"> 
					
					<!-- Search  PC -->
						<h3 class="heading-three">Create Invoice</h3>
						<div class="top-input">
							<div class="invoice-number2"> 
								<label for="number">invoice number</label>
								<input class="input_bg_color" id="number" type="text" ng-model="invCtrl.NewInvoice.Number" required=""/>
							</div>
							<div class="input-name2"> 
								<label for="text">supplier name</label>
								<select class="input_bg_color" name="newInvoiceSupplierSelect" id="newInvoiceSupplierSelect" 
                                    ng-options= "supplier.Name for supplier in invCtrl.InvoiceCreateSuppliers track by supplier.Id" 
                                    ng-model="invCtrl.NewInvoice.Supplier ">
								</select>
							</div>
<%--							<div class="shipping-charges sm-input"> --%>
<%--								<label for="sh">shipping charges</label>--%>
<%--								<input id="sh" type="text" ng-model="invCtrl.NewInvoice.ShippingCharges" />--%>
<%--							</div>--%>
<%--							<div class="sales-tax sm-input"> --%>
<%--								<label for="stax">sales tax</label>--%>
<%--								<input id="stax" type="text" value="$7.98" ng-model="invCtrl.NewInvoice.Tax"/>--%>
<%--							</div>--%>
<%--							<div class="total-ammount sm-input"> --%>
<%--								<label for="tn">total ammount</label>--%>
<%--								<input id="tn" type="text" ng-model="invCtrl.NewInvoice.Total"/>--%>
<%--							</div>--%>
						</div>
						<div class="for-button"> 
							<button class="clear" type="submit">Clear</button>
							<input class="create" type="button" ng-click="invCtrl.createNewInvoiceClick()" value ="Create" />
						</div>
					</div>
				</div>
				<div class="right-content">
					<div class="top-content"> 
					
					<!-- Current Active Invoice  -->
						<h3 class="heading-three">current active invoice</h3>
						<div class="top-item">
							<div class="second-form"> 
								<div class="second-input second-one"> 
									<label for="s1">invoice no</label>
									<input type="text" id="s1" ng-model="invCtrl.CurrentInvoice.Number"/>
								</div> 
								<div class="second-input second-two"> 
									<label for="s2">supplier name</label>
									<input type="text" id="s2" ng-model="invCtrl.CurrentInvoice.Supplier.Name" disabled=""/>
								</div> 
								<div class="second-input second-three"> 
									<label for="s3">invoice date</label>
<%--									<input type="date" id="s3"/>--%>
									<input type="text" ng-model="invCtrl.CurrentInvoice.Date| date: 'MM/dd/yyyy'" />
								</div>
							</div>
							<table class="second-table">
								<div class="item-head"> 
									<tr> 
										<th>Product</th>
										<th>P.O. No</th>
										<th>Code</th>
										<th>Quantity</th>
										<th>Unit Cost</th>
										<th>Total cost</th>
										<th>Delete</th>
										<th>Catalog Price</th>
									</tr>
								</div>
								<div class="item-one"> 
									<tr class="first-item first-active" ng-repeat="invoiceLineItem in invCtrl.CurrentInvoice.InvoiceLineItems"> 
<%--										<td>rons testing vision R&D</td>--%>
<%--										<td>AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</td>--%>
<%--										<td><input type="number" /></td>--%>
<%--										<td><input class="money" type="text" placeholder="$564" /><i title="Set Master Catalog Amount" class="fa fa-book"></i></td>--%>
<%--										<td>10/10/10</td>--%>
<%--										<td class="no-bg"><i title="Remove This Line Item"  class="fa fa-trash-o"></i></td>--%>
                                    <td>{{invoiceLineItem.ProductName}}</td>
                                    <td>{{invoiceLineItem.PurchaseOrderNumber}}</td>
                                    <td>{{invoiceLineItem.ProductCode}}</td>
                                    <td><input type="number" ng-model="invoiceLineItem.QuantityInvoiced" ng-change="invCtrl.updateLineItemTotal(invoiceLineItem)"/>
                                    <td><input type="number" step="0.01" ng-model="invoiceLineItem.WholesaleCost"/></td>
                                    <td>{{invoiceLineItem.QuantityInvoiced * invoiceLineItem.WholesaleCost}}</td>
                                    <td><input type="button" ng-click="invCtrl.removeItemFromCurrentInvoice(invoiceLineItem)"/>Delete</td>
                                    <td><input type="button" ng-click="invCtrl.updateWholesaleCost(invoiceLineItem)"/>Update Catalog</td>
									</tr>
								</div>
							</table>
						</div>
						
							<div class="second-paid"> 
								<div class="input-checked"> 
									<label for="chk">paid</label>
									<input id="chk" type="checkbox" ng-model="invCtrl.CurrentInvoice.IsPaid" />
								</div>
								<div class="paid-section"> 
									<div class="left-paid"> 
										<div class="shipping-charges sm-input paid-input"> 
											<label for="shh">shipping charges</label>
											<input id="shh" type="text" ng-model="invCtrl.CurrentInvoice.ShippingCharges"/>
										</div>
										<div class="sales-tax sm-input paid-input"> 
											<label for="staax">sales tax</label>
											<input id="staax" type="text"   ng-model="invCtrl.CurrentInvoice.Tax"/>
										</div>
										<div class="total number sm-input paid-input"> 
											<label for="tan">total ammount</label>
											<input id="tan" type="text" ng-model="invCtrl.CurrentInvoice.Total"/>
										</div>
									</div>
								    <div class="right-paid"> 
								        <%--										<button class="delete" type="submit">Delete Invoice</button>--%>
								        <%--										<button class="sinvoice" type="submit">Save Invoice</button>--%>
								        <input class="sinvoice" type="button" ng-click="invCtrl.updateInvoice()" value = "Save Invoice" >
								        <input class="delete" type="button" ng-click="invCtrl.deleteInvoice()" value = "Delete Invoice" >
								    </div>
								</div>
							</div>
					</div>
				</div>
			</div>
			
			<div class="content-bar"> 
			<!-- left sidebar -->
				<div class="left-sidebar left-sides">
					<div class="top-sidebar sidebar-bg"> 
					
					<!-- Create Invoice  -->
						<h3 class="heading-three">Search P.O.</h3>
						<div class="top-input"> 
							<div class="input-checked"> 
								<input id="chekk" type="checkbox" />
								<label for="chekk">show inactive supplier</label>
							</div>
<%--							<div class="input-name2"> --%>
<%--								<label for="text">supplier name</label>--%>
<%--								<select name="purchaseOrderSupplierSelect" id="purchaseOrderSupplierSelect" --%>
<%--                                    ng-options= "supplier.Name for supplier in invCtrl.PurchaseOrderSuppliers track by supplier.Id" --%>
<%--                                    ng-model="invCtrl.SearchPurchaseOrder.Supplier ">--%>
<%--								</select>--%>
<%--								<div class="popup-icon"> --%>
<%--									<i title="you must fill this"  class="fa fa-exclamation"></i>--%>
<%--								</div>--%>
<%--							</div>--%>
							<div class="po-number"> 
								<label for="skh">Po number</label>
								<input id="skh" type="text" ng-model="invCtrl.SearchPurchaseOrder.PONumber "/>
							</div>
							<div class="from-date"> 
								<label for="fgdate">po order date</label>
								<input id="fgdate" type="date" value="Po order date" />
								<div class="input-icon"> 
									<i class="fa fa-calendar"></i>
								</div>
							</div>
							<div class="product-name"> 
								<label for="proname">product name</label>
								<input id="proname" type="text" value="Product name" />
							</div>
							<div class="product-code"> 
								<label for="pcode">product code</label>
								<input id="pcode" type="text" value="Product code" />
							</div>
						</div>
						<div class="for-button"> 
							<button class="clearall" type="submit">Clear</button>
						</div>
					</div>
				</div>
				<div class="right-content">
					<div class="top-content"> 
					
					<!-- Purchase Order result  -->
						<h3 class="heading-three">purchase order result</h3>
					    <div class="top-item">
					        <table class="third-table">
					            <div class="item-head"> 
					                <tr> 
					                    <th>P.O. No</th>
					                    <th>suppliers</th>
					                    <th>order date</th>
					                    <th></th>
					                </tr>
					            </div>
					            <div class="item-one"> 
					                <tr class="first-item first-active"
					                    ng-repeat="po in invCtrl.PurchaseOrders 
                                        | filter: {PONumber: invCtrl.SearchPurchaseOrder.PONumber}
                                        | purchaseOrderSupplierFilter: invCtrl.CurrentInvoice.Supplier" 
					                    ng-click="invCtrl.SelectPurchaseOrder(po)"> 
					                    <td>{{po.PONumber}}</td><td>{{po.Supplier.Name}}</td><td>{{po.OrderDate  | date: 'MM/dd/yyyy'}}</td>
					                </tr>
					            </div>
					        </table>
					    </div>
						
					<!-- Purchase Order Details  -->
						<h3 class="heading-three">purchase order details</h3>
						<div class="bottom_bg">
						<div class="oreange-button"> 
							<div class="or-text">
								<p><span>P.O No</span>{{invCtrl.CurrentPurchaseOrder.PONumber}}</p>
							</div>
							<div class="or-button">
<%--								<button class="orang-btn" type="submit">Add to current active invoice</button>--%>
                                <input type="button" class="orang-btn" ng-click="invCtrl.addSelectedPoItemsToCurrentInvoice()" value = "Add to Invoice" >

							</div>
						</div>
						<table class="fourth-table">
								<div class="item-head"> 
									<tr> 
										<th class="small-td">select all<input type="checkbox" ng-model ="invCtrl.SelectAll" ng-click="invCtrl.ToggleSelectAll()"/></th>
										<th>name</th>
										<th>code</th>
										<th>ordered</th>
										<th>checked in</th>
										<th>Invc'd</th>
										<th>unit cost</th>
										<th>invoiced details</th>
									</tr>
								</div>
								<div class="item-one"> 
									<tr class="sec-item" ng-repeat="lineItem in invCtrl.CurrentPurchaseOrder.LineItems" item="{{lineItem}}"> 
                                        <td ><input type="checkbox" ng-model="lineItem.Selected" /></td>
                                        <td >{{lineItem.Name}}</td>
                                        <td >{{lineItem.ProductCode}}</td>
                                        <td style="text-align: center ">{{lineItem.QuantityCheckedIn}}</td>
                                        <td style="text-align: center ">{{lineItem.QuantityOrdered}}</td>
                                        <td style="text-align: center ">{{invCtrl.totalSupplierOrderLineItemInvoicedCount(lineItem)}}</td>
                                        <td>{{lineItem.Cost}}</td>
                                        <td>
                                            <div ng-repeat="invoice in lineItem.Invoices">Number:{{invoice.Number}} Date:{{invoice.Date | date: 'MM/dd/yyyy'}} Qty:{{invoice.Quantity}} </div>
                                        </td>
									</tr>
								</div>
							</table>
						</div>
					</div>
				</div>
			</div>
			
		</div>
</div>
            <script type="text/javascript">
            var app = angular.module("invoiceApp", []);

            app.controller("InvoiceCtrl",
                function ($scope, $http) {
                    var self = this;
                    self.dateFrom = new Date();
                    self.dateFrom.setMonth(self.dateFrom.getMonth() - 1);
                    self.dateTo = new Date();
                    self.dateTo.setMonth(self.dateTo.getMonth() + 1);
                    self.NewInvoice = {
                        Number: '',
                        Supplier: null,
                        Tax: null,
                        ShippingCharges: null,
                        Total: null
                    };
                    self.SearchInvoice = {
                        Number: '',
                        Supplier: null,
                        Tax: null,
                        ShippingCharges: null,
                        Total: null
                    };
                    self.CurrentInvoice = null;
                    //                {
                    //                    Number: '',
                    //                    Supplier: null,
                    //                    Tax: null,
                    //                    ShippingCharges: null,
                    //                    Total: null
                    //                };

                    this.CurrentPurchaseOrder = null;

                    this.SearchPurchaseOrder = {
                        PONumber: '',
                        Supplier: null,
                        OrderDate: null,
                        LineItems: null
                    };

                    self.Invoices = [];

                    self.InvoiceSearchSuppliers = [
                        { Id: -1, Name: 'Select Supplier' }
                    ];

                    self.InvoiceCreateSuppliers = [
                        { Id: -1, Name: 'Select Supplier' }
                    ];

                    self.PurchaseOrderSuppliers = [
                        { Id: -1, Name: 'Select Supplier' }
                    ];

                    self.NewInvoice.Supplier = self.InvoiceCreateSuppliers[0];

                    self.totalSupplierOrderLineItemInvoicedCount = function(lineItem) {
                        var sum = 0;
                        angular.forEach(lineItem.Invoices, function(invoice, key) {
                            sum += invoice.Quantity;
                        });
                        return sum;
                    }


                    self.createNewInvoiceClick = function () {
                        var newInvoice = {
                            Number: self.NewInvoice.Number,
                            Supplier: self.NewInvoice.Supplier,
                            Tax: self.NewInvoice.Tax,
                            Total: self.NewInvoice.Total,
                            ShippingCharges: self.NewInvoice.ShippingCharges,
                            Date: self.dateFrom,
                            InvoiceLineItems: []
                        };
                        self.createInvoice(newInvoice);
                        self.Invoices.push(newInvoice);
                        self.CurrentInvoice = newInvoice;
                    };


                    self.createInvoice = function (newInvoice) {
                        $scope.working = true;
                        $scope.answered = true;

                        $http.post('http://localhost/BregRestfulService/api/invoice/', newInvoice).success(function (data, status, headers, config) {
                            newInvoice.Id = data;
                            $scope.working = false;
                        }).error(function (data, status, headers, config) {
                            $scope.title = "Oops... something went wrong";
                            $scope.working = false;
                        });
                    };

                    self.updateInvoice = function () {
                        $scope.working = true;
                        $scope.answered = true;

                        $http.put('http://localhost/BregRestfulService/api/invoice/', self.CurrentInvoice).success(function (data, status, headers, config) {
                            $scope.correctAnswer = (data === true);
                            $scope.working = false;
                        }).error(function (data, status, headers, config) {
                            $scope.title = "Oops... something went wrong";
                            $scope.working = false;
                        });
                    };

                    self.updateWholesaleCost = function (invoiceLineItem) {
                        $scope.working = true;
                        $scope.answered = true;

                        $http.put('http://localhost/BregRestfulService/api/invoice/product/', invoiceLineItem).success(function (data, status, headers, config) {
                            $scope.correctAnswer = (data === true);
                            $scope.working = false;
                        }).error(function (data, status, headers, config) {
                            $scope.title = "Oops... something went wrong";
                            $scope.working = false;
                        });
                    };

                    self.ToggleSelectAll = function () {
                        angular.forEach(self.CurrentPurchaseOrder.LineItems, function (lineItem, key) {
                            lineItem.Selected = self.SelectAll;
                        });

                    }

                    self.addSelectedPoItemsToCurrentInvoice = function () {
                        if (!self.CurrentInvoice || self.CurrentInvoice == null) {
                            return;
                        }
                        angular.forEach(self.CurrentPurchaseOrder.LineItems, function (lineItem, key) {
                            if (lineItem.Selected && lineItem.Selected === true) {
                                self.addPoItemToCurrentInvoice(lineItem);
                            }
                        });
                    };

                    self.removeItemFromCurrentInvoice = function (lineItem) {
                        $scope.working = true;
                        $scope.answered = true;
                        if (lineItem.Id !== null) {
                            $http.delete('http://localhost/BregRestfulService/api/Invoice/LineItem/' + lineItem.InvoiceLineItemId).success(function (data, status, headers, config) {
                                $scope.correctAnswer = (data === true);
                                $scope.working = false;
                            }).error(function (data, status, headers, config) {
                                $scope.title = "Oops... something went wrong";
                                $scope.working = false;
                            });
                        }

                        var index = self.CurrentInvoice.InvoiceLineItems.indexOf(lineItem);
                        self.CurrentInvoice.InvoiceLineItems.splice(index, 1);
                    };

                    self.deleteInvoice = function () {
                        $scope.working = true;
                        $scope.answered = true;
                        if (self.CurrentInvoice == null || self.CurrentInvoice.Id == null) {
                            return;
                        }

                        $http.delete('http://localhost/BregRestfulService/api/Invoice/' + self.CurrentInvoice.Id).success(function (data, status, headers, config) {
                            $scope.correctAnswer = (data === true);
                            $scope.working = false;
                        }).error(function (data, status, headers, config) {
                            $scope.title = "Oops... something went wrong";
                            $scope.working = false;
                        });

                        var index = self.Invoices.indexOf(self.CurrentInvoice);
                        self.Invoices.splice(index, 1);
                        self.CurrentInvoice = null;
                    };

                    self.addPoItemToCurrentInvoice = function (lineItem) {
                        self.CurrentInvoice.InvoiceLineItems.push({
                            InvoiceLineItemId: null,
                            PurchaseOrderNumber: self.CurrentPurchaseOrder.PONumber,
                            SupplierOrderLineItemId: lineItem.Id,
                            ProductName: lineItem.Name,
                            ProductCode: lineItem.ProductCode,
                            QuantityCheckedIn: lineItem.QuantityCheckedIn,
                            QuantityOrdered: lineItem.QuantityOrdered,
                            QuantityInvoiced: null,
                            WholesaleCost: lineItem.Cost

                        });
                    };

                    self.SelectInvoice = function (selectedInvoice) {
                        if (self.CurrentPurchaseOrder != null && selectedInvoice.Supplier.Id != self.CurrentPurchaseOrder.Supplier.Id) {
                            self.CurrentPurchaseOrder = null;
                        }
                        self.CurrentInvoice = selectedInvoice;
                    };

                    self.SelectPurchaseOrder = function (selectedPurchaseOrder) {
                        self.CurrentPurchaseOrder = selectedPurchaseOrder;
                    };

                    $scope.handleDrop = function (data) {
                        self.addPoItemToCurrentInvoice(data);
                    };

                    self.getPurchaseOrders = function () {
                        $scope.working = true;
                        $scope.answered = false;
                        $scope.title = "loading purchase orders...";
                        $scope.options = [];

                        $http.get("http://localhost/BregRestfulService/api/supplierorder/302").success(function (data, status, headers, config) {
                            self.PurchaseOrders = data;
                            self.PopulatePurchaseOrderLineItems();
                            $scope.title = data.title;
                            $scope.answered = false;
                            $scope.working = false;
                        }).error(function (data, status, headers, config) {
                            $scope.title = "Oops... something went wrong";
                            $scope.working = false;
                        });

                    };

                    self.PopulatePurchaseOrderLineItems = function () {
                        angular.forEach(self.PurchaseOrders, function (purchaseOrder, key) {
                            self.getPurchaseOrdersLineItems(purchaseOrder);
                        });
                    }

                    self.getPurchaseOrdersLineItems = function (selectedPurchaseOrder) {
                        $scope.working = true;
                        $scope.answered = false;
                        $scope.title = "loading purchase order line items...";
                        $scope.options = [];

                        $http.get("http://localhost/BregRestfulService/api/supplierorder/lineitems/" + selectedPurchaseOrder.SupplierOrderId).success(function (data, status, headers, config) {
                            selectedPurchaseOrder.LineItems = data;
                            $scope.title = data.title;
                            $scope.answered = false;
                            $scope.working = false;
                        }).error(function (data, status, headers, config) {
                            $scope.title = "Oops... something went wrong";
                            $scope.working = false;
                        });
                    };

                    self.getPurchaseOrders();

                    self.getInvoices = function () {
                        $scope.working = true;
                        $scope.answered = false;
                        $scope.title = "loading invoices...";
                        $scope.options = [];

                        $http.get("http://localhost/BregRestfulService/api/invoice/302").success(function (data, status, headers, config) {
                            self.Invoices = data;
                            self.populateInvoiceItems();
                            $scope.title = data.title;
                            $scope.answered = false;
                            $scope.working = false;
                        }).error(function (data, status, headers, config) {
                            $scope.title = "Oops... something went wrong";
                            $scope.working = false;
                        });
                    };

                    self.populateInvoiceItems = function () {
                        angular.forEach(self.Invoices, function (invoice, key) {
                            $http.get("http://localhost/BregRestfulService/api/invoice/lineitems/" + invoice.Id).success(function (data, status, headers, config) {
                                invoice.InvoiceLineItems = data;
                                $scope.title = data.title;
                                $scope.answered = false;
                                $scope.working = false;
                            }).error(function (data, status, headers, config) {
                                $scope.title = "Oops... something went wrong";
                                $scope.working = false;
                            });

                        })
                    }

                    self.getInvoices();

                    self.getSuppliers = function () {
                        $scope.working = true;
                        $scope.answered = false;
                        $scope.title = "loading suppliers...";
                        $scope.options = [];

                        $http.get("http://localhost/BregRestfulService/api/supplier/302").success(function (data, status, headers, config) {
                            Array.prototype.push.apply(self.InvoiceSearchSuppliers, data);
                            Array.prototype.push.apply(self.InvoiceCreateSuppliers, data);
                            Array.prototype.push.apply(self.PurchaseOrderSuppliers, data);
                            $scope.title = data.title;
                            $scope.answered = false;
                            $scope.working = false;
                        }).error(function (data, status, headers, config) {
                            $scope.title = "Oops... something went wrong";
                            $scope.working = false;
                        });
                    };

                    self.getSuppliers();

                }
            );
            app.directive('draggablepoitem', function () {
                return function (scope, element) {
                    var el = element[0];

                    el.draggable = true;

                    el.addEventListener(
                        'dragstart',
                        function (e) {
                            e.dataTransfer.effectAllowed = 'move';
                            var item = this.getAttribute("item");
                            e.dataTransfer.setData('text', item);
                            this.classList.add('drag');
                            return false;
                        },
                        false
                    );

                    el.addEventListener();
                }
            });
            app.directive('droppableinvoice', function () {
                return {
                    scope: {
                        drop: "&", //parent
                        bin: '='
                    },
                    link: function (scope, element) {
                        var el = element[0];

                        el.addEventListener(
                            'dragover',
                            function (e) {
                                e.dataTransfer.dropEffect = 'move';
                                //allows us to drop
                                if (e.preventDefault) e.preventDefault();
                                this.classList.add('over');
                                return false;
                            },
                            false
                        );

                        el.addEventListener(
                            'dragenter',
                            function (e) {
                                this.classList.add('over');
                                return false;
                            },
                            false
                        );

                        el.addEventListener(
                            'dragleave',
                            function (e) {
                                this.classList.remove('over');
                                return false;
                            },
                            false
                        );

                        el.addEventListener(
                            'drop',
                            function (e) {
                                //Stops some browsers from redirecting
                                if (e.stopPropagation) e.stopPropagation();

                                this.classList.remove('over');

                                var binId = this.id;
                                var data = JSON.parse(e.dataTransfer.getData('text'));
                                var item = document.getElementById(data.Id);
                                // this.appendChild(item);

                                // call the drop passed drop function
                                scope.$apply(function (scope) {
                                    var fn = scope.drop();
                                    if ('undefined' !== typeof fn) {
                                        fn(data);
                                    }
                                });

                                return false;
                            },
                            false
            );
                    }
                }
            });
            app.directive('draggable', function () {
                return function (scope, element) {
                    var el = element[0];

                    el.draggable = true;

                    el.addEventListener(
                        'dragstart',
                        function (e) {
                            e.dataTransfer.effectAllowed = 'move';
                            e.dataTransfer.setData('Text', this.id);
                            this.classList.add('drag');
                            return false;
                        },
                        false
                    );
                }
            });
            app.directive('droppable', function () {
                return {
                    scope: {
                        drop: "&", //parent
                        bin: '='
                    },
                    link: function (scope, element) {
                        var el = element[0];

                        el.addEventListener(
                            'dragover',
                            function (e) {
                                e.dataTransfer.dropEffect = 'move';
                                //allows us to drop
                                if (e.preventDefault) e.preventDefault();
                                this.classList.add('over');
                                return false;
                            },
                            false
                        );

                        el.addEventListener(
                            'dragenter',
                            function (e) {
                                this.classList.add('over');
                                return false;
                            },
                            false
                        );

                        el.addEventListener(
                            'dragleave',
                            function (e) {
                                this.classList.remove('over');
                                return false;
                            },
                            false
                        );

                        el.addEventListener(
                            'drop',
                            function (e) {
                                //Stops some browsers from redirecting
                                if (e.stopPropagation) e.stopPropagation();

                                this.classList.remove('over');

                                var binId = this.id;
                                var data = e.dataTransfer.getData('Text');
                                var item = document.getElementById(data);
                                this.appendChild(item);

                                // call the drop passed drop function
                                scope.$apply(function (scope) {
                                    var fn = scope.drop(data);
                                    if ('undefined' !== typeof fn) {
                                        fn(item.id, binId);
                                    }
                                });

                                return false;
                            },
                            false
                        );
                    }
                }
            })
                .filter("dateRange", [
                    function () {
                        return function (invoices, dateFrom, dateTo) {
                            var filtered = [];
                            angular.forEach(invoices, function (invoice) {
                                var invDate = new Date(invoice.Date).setHours(0, 0, 0, 0);
                                var dateFromFloor = new Date(dateFrom).setHours(0, 0, 0, 0);
                                var dateToFloor = new Date(dateTo).setHours(0, 0, 0, 0);
                                if (invDate >= dateFromFloor && invDate <= dateToFloor) {
                                    filtered.push(invoice);
                                }
                            });
                            return filtered;
                        };
                    }
                ])
                .filter("supplier", [
                    function () {
                        return function (invoices, supplier) {
                            var filtered = [];
                            angular.forEach(invoices, function (invoice) {
                                if (supplier === null || supplier.Id === -1 || supplier.Id === invoice.Supplier.Id) {
                                    filtered.push(invoice);
                                }
                            });
                            return filtered;
                        };
                    }
                ]).filter("purchaseOrderSupplierFilter", [
                    function () {
                        return function (purchaseOrders, supplier) {
                            var filtered = [];
                            angular.forEach(purchaseOrders, function (purchaseOrder) {
                                if (supplier === undefined || supplier.Id === -1 || supplier.Id === purchaseOrder.Supplier.Id) {
                                    filtered.push(purchaseOrder);
                                }
                            });
                            return filtered;
                        };
                    }
                ]);


    </script>

</asp:Content>
