using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Telerik.WebControls;
using System.Collections.Generic;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
namespace BregVision.Admin
{
    public partial class PracticeAdminUserPractice : System.Web.UI.UserControl
    {

        int userID;
        int practiceID;
        int practiceLocationID;
        string practiceName;
        string practiceLocationName;



        protected void Page_Init(object sender, EventArgs e)
        {
            //PopulateClinicAndLocation();
            //GetUserName, userID, PracticeID, and PracticeName.
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            
            
            if ( !Page.IsPostBack ) 
            {
                    GetSessionVariables( out userID, out practiceID, out practiceName, out practiceLocationID, out practiceLocationName );

                if (userID == -1)
                {
                    userID = GetUserID();
                    Session["UserID"] = userID;
                }
                DisplaySessionVariable();
                DisplayUserLoginInfo();
                //LoadPracticeLocations();
                cbxChangeLocation.Visible = false;
            }
            // THE PRacticelocationid is valid then display the control.
            // else add a blank to the display.
            //if (practiceLocationID > 0)//= (Session["PracticeLocationID"] != null) ? Convert.ToInt32(Session["PracticeLocationID"].ToString()) : -1;
            //{
            //    //cbxChangeLocation.SelectedValue = practiceLocationID.ToString();
            //    cbxChangeLocation.Visible = true;
            //}
            //else
            //{
            //    cbxChangeLocation.Visible = false;
            //}

            //GetSessionVariables if neccessary...
            //Get All Location for the Practice and load the combo box.
        }

        //  GetSessionVariables
        //  Get All LocationID and LocationName for the PracticeID since this is an admin.
        //  Set the Location Combo box.  Add a blank for no practice selected.
        //  On click of practice.  Set the Session variable for the LocationID and the LocationName.
        //  Display the PracticeLocation.aspx page.

        //


        //protected void GetPracticeLocations()
        //{
        //    Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
        //    DbCommand dbCommand = db.GetStoredProcCommand("dbo.[usp_PracticeLocation_Select_All_ListNames_By_PracticeID]");

        //    db.AddInParameter(dbCommand, "UserName", DbType.String, Context.User.Identity.Name.ToString());
        //    db.AddOutParameter(dbCommand, "UserID", DbType.Int32, 4);
        //    db.ExecuteNonQuery(dbCommand);
        //}

        protected void LoadPracticeLocations()
        {
            if (practiceLocationID > 0)
            {
                cbxChangeLocation.ClearSelection();

                DataTable dt = SelectAllPracticeLocations(practiceID);

                cbxChangeLocation.DataSource = dt;
                cbxChangeLocation.DataValueField = "PracticeLocationID";
                cbxChangeLocation.DataTextField = "PracticeLocationName";

                cbxChangeLocation.DataBind();

                //RadComboBoxItem item = new RadComboBoxItem();
                //item.Text = reader["Name"].ToString();
                //item.Value = reader["PracticeLocationID"].ToString();
                //cbxChangeLocation.Items.Add(item);                    
            }
        }


        public DataTable SelectAllPracticeLocations(int practiceID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            using (DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_Select_All_ListNames_By_PracticeID"))
            {
                db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);

                DataTable dtPracticeLocation = new DataTable();
                dtPracticeLocation = db.ExecuteDataSet(dbCommand).Tables[0];
                dbCommand.Dispose();

                //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));

                return dtPracticeLocation;
            }
        }

        protected void DisplayUserLoginInfo()
        {

            lblClinic.Text = "Practice: " + "<b>" + practiceName + "</b>";
            
            // Clear location drop down
            lblLocation.Text = "Location: " + "<b>" + practiceLocationName + "</b>";

        }

        protected int GetUserID()
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_User_Select_UserID_By_LoginName");

            db.AddInParameter(dbCommand, "UserLoginName", DbType.String, Context.User.Identity.Name.ToString());
            db.AddOutParameter(dbCommand, "UserID", DbType.Int32, 4);
            db.ExecuteNonQuery(dbCommand);

            userID = ((db.GetParameterValue(dbCommand, "UserID") == DBNull.Value) ? -1 : Convert.ToInt32(db.GetParameterValue(dbCommand, "UserID")));

            return userID;

        }

        protected void GetSessionVariables(out int userID, out int practiceID, out string practiceName,
                    out int practiceLocationID, out string practiceLocationName)
        {
            userID = (Session["UserID"] != null) ? Convert.ToInt32(Session["UserID"].ToString()) : -1;

            practiceID = (Session["PracticeID"] != null) ? Convert.ToInt32(Session["PracticeID"].ToString()) : -1;
            practiceLocationID = (Session["PracticeLocationID"] != null) ? Convert.ToInt32(Session["PracticeLocationID"].ToString()) : -1;
            practiceName = (Session["PracticeName"] != null) ? Session["PracticeName"].ToString() : "";
            practiceLocationName = (Session["PracticeLocationName"] != null) ? Session["PracticeLocationName"].ToString() : "";
        }

        
        protected void DisplaySessionVariable()
        {
            //lblUserID.Text = "UserID: " + userID;
            //lblPracticeID.Text = "PracticeID: " + practiceID;
            //lblPracticeName.Text = "PracticeName: " + practiceName;
            //lblPracticeLocationID.Text = "PracticeLocationID: " + practiceLocationID;
            //lblPracticeLocationName.Text = "PracticeLocationName: " + practiceLocationName;
        }

        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {


        }


        public void GetPracticeIDandPracticeLocationID(string userName)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.USP_GetPracticeAndPracticeLocation2");

            db.AddInParameter(dbCommand, "UserName", DbType.String, userName);

            DataSet ds = db.ExecuteDataSet(dbCommand);
            //grd.DataSource = ds.Tables[0];
            //grd.DataBind();

            //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
        }

        private void PopulateClinicAndLocation()
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.USP_GetPracticeAndPracticeLocation2");

            db.AddInParameter(dbCommand, "UserName", DbType.String, Context.User.Identity.Name.ToString());

            IDataReader reader = db.ExecuteReader(dbCommand);
            //grd.DataSource = ds.Tables[0];
            //grd.DataBind();

            //using (SqlConnection con = new SqlConnection(connect))
            //{
            //  SqlCommand cmd = new SqlCommand(SQL, con);
            // con.Open();
            //SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            if (reader.Read())
            {
                lblClinic.Text = "Practice: " + reader["PracticeName"].ToString();
                Session["PracticeName"] = reader["PracticeName"].ToString();
                Session["PracticeID"] = reader["PracticeID"].ToString();
                // Clear location drop down
                cbxChangeLocation.ClearSelection();
                do
                {
                    //Set Primary Clinic Location
                    if (reader["isPrimaryLocation"].ToString() == "True")
                    {
                        lblLocation.Text = "Location: " + reader["Name"].ToString();
                        Session["PracticeLocationName"] = reader["Name"].ToString();
                        Session["PracticeLocationID"] = reader["PracticeLocationID"].ToString();
                    }

                    //Fill location drop down box

                    RadComboBoxItem item = new RadComboBoxItem();
                    item.Text = reader["Name"].ToString();
                    item.Value = reader["PracticeLocationID"].ToString();

                    cbxChangeLocation.Items.Add(item);

                } while (reader.Read());
                reader.Close();
                //}

            }
        }

        protected void cbxChangeLocation_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
        {
           // lblLocation.Text = "Location:" + cbxChangeLocation.Text.ToString();
            //Session.Remove("PracticeLocationName");
            Session["PracticeLocationName"] = cbxChangeLocation.SelectedItem.Text.ToString();
            Session["PracticeLocationID"] = cbxChangeLocation.SelectedItem.Value.ToString();    //.Value.ToString();
        }
    }
}