<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PracticeAdmin.Master" Inherits="Practice_Physicians" CodeBehind="Practice_Physicians.aspx.cs" %>

<%@ Register Src="~/Admin/UserControls/Practice/PracticePhysicians.ascx" TagName="PracticePhysicians"
    TagPrefix="bvl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true"
    Visible="true">

  <bvl:PracticePhysicians ID="ctlPracticePhysicians" runat="server" />

</asp:Content>
