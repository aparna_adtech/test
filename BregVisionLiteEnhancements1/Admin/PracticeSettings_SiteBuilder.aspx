﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PracticeSettingsAdmin.Master" Inherits="PracticeSettings_SiteBuilder" CodeBehind="PracticeSettings_SiteBuilder.aspx.cs" %>

<%@ Register Src="~/Admin/UserControls/PracticeSettings/PracticeSettingsSiteBuilder.ascx" TagName="PracticeSettingsSiteBuilder" TagPrefix="bvl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true" Visible="true">
  <bvl:PracticeSettingsSiteBuilder ID="ctlPracticeSettingsSiteBuilder" runat="server" />
</asp:Content>
