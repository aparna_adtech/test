<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="PracticeAdminPLHIM.ascx.cs" Inherits="BregVision.Admin.PracticeAdminPLHIM"  %>
<%@ OutputCache Duration="3600" VaryByParam="none" %>
    <style type="text/css"> 

            body  
            {
                font: 100% Verdana, Arial, Helvetica, sans-serif;
                background: #ffffff;
                margin: 0; /* it's good practice to zero the margin and padding of the body element to account for differing browser defaults */
                padding: 0;
                text-align: center; /* this centers the container in IE 5* browsers. The text is then set to the left aligned default in the #container selector */
                color: #000000;
            }
            a.menulink:link
            {
                color: #40465d;
                background-image: url(../images/05main.gif);
                background-repeat: repeat;               
                padding: 14 0 0 0;
                text-decoration: none;
                float:left; 
                list-style:none; 
                text-align:center; 
                width:82px; 
                height:41px; 
            }
            a.menulink:visited
            {
                color: #40465d;
                background-image: url(./images/05main.gif);
                background-repeat: repeat;
                display: block;
                padding: 14 0 0 0;
                text-decoration: none;
                float:left; 
                list-style:none; 
                text-align:center; 
                width:82px; 
                height:41px; 
            }
            a.menulink:hover
            {
                color: White;
                background-image: url(../images/05mainover.gif);
                background-repeat: repeat;
                height: 41px;
                width: 82px;
                padding: 14px 0px 0px 0px;
                text-decoration: none;
            }

    </style>


<div id="PLHIMKKGcZdDDiv" style="position:relative;width:788px;z-index:50;white-space:nowrap;direction:ltr;">
  <div id="PLHIMKKGcZdDMain" style="width:788px; height:41px">
      <ul style="margin:0px; padding:0px; font:normal normal bold 11px Arial,Helvetica,sans-serif;">
          <%--<li style="cursor:default; float:left; list-style:none; text-align:center; width:13px; height:41px; font-size:1px; background-image:url(../images/05edge_left.gif);"></li>--%>
          <li style="float:left; list-style:none; text-align:center; width:175px; height:41px; font-size:1px; background-image:url(../images/05main.gif);">
            <a href="#" style="display:block;height:41px;"></a>
          </li>
          <li id="liHome" runat="server" style="float:left; list-style:none; text-align:center; width:82px; height:41px; background-image:url(../images/05main.gif);">
            <a href="../home.aspx" class="menulink" style="display:block; height:41px; padding:14px 0px 0px 0px; text-decoration:none;">Home</a>
          </li>
          <li id="liDispense" runat="server" style="float:left; list-style:none; text-align:center; width:82px; height:41px; background-image:url(../images/05main.gif);">
            <a href="../Dispense.aspx" class="menulink" title="Dispense&nbsp;Products" style="display:block; height:41px; padding:14px 0px 0px 0px; text-decoration:none;">Dispense</a>
          </li>
          <li id="liInventory" runat="server" style="float:left; list-style:none; text-align:center; width:82px; height:41px; background-image:url(../images/05main.gif);">
            <a href="../Inventory.aspx" class="menulink" title="Add&nbsp;items&nbsp;to&nbsp;shopping&nbsp;cart" style="display:block; height:41px; padding:14px 0px 0px 0px; text-decoration:none;">Inventory</a>
          </li>
          <li id="liCheckIn" runat="server" style="float:left; list-style:none; text-align:center; width:82px; height:41px; background-image:url(../images/05main.gif);">
            <a href="../Checkin.aspx" class="menulink" title="Check&nbsp;In&nbsp;Products" style="display:block; height:41px; padding:14px 0px 0px 0px; text-decoration:none;">Check-In</a>
          </li>
          <%--<li onmouseover="mouseOver(this);return true;" onmouseout="mouseOut(this);return true;"  id="liAdmin" runat="server"  style="float:left;list-style:none;text-align:center;width:217px;height:41px;font-size:1px;background-image:url(../images/05main.gif);"><a href="#" style="display:block;height:41px;"></a></li>--%>
          <%--<li style="cursor:default;float:left;list-style:none;text-align:center;width:13px;height:41px;font-size:1px;background-image:url(../images/05edge_right.gif);"></li>--%>
      </ul>
</div>

    <script type="text/javascript">
        function mouseOver(ListItem) {
            ListItem.style.backgroundImage = "url(../images/05mainover.gif)";
        }
        function mouseOut(ListItem) {
            ListItem.style.backgroundImage = "url(../images/05main.gif)";
        }
    </script>

    <script type="text/javascript">          var PLHIM_ID = "PLHIMKKGcZdD"</script>
    <%--<script type="text/javascript" src="../Pluginlab/Scripts/PLHIM2.js">      /* PLHIMMenu script ID:PLHIMKKGcZdD */</script>--%>
</div>