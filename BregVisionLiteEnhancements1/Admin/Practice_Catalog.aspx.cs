using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.WebControls;



namespace BregVision.Admin
{
    public partial class Practice_Catalog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((MasterPages.PracticeAdmin)Page.Master).SelectCurrentTab("TabCatalog");
        }
    }
}
