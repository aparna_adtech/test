<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" MasterPageFile="~/MasterPages/PracticeLocationAdmin.Master" CodeBehind="PracticeLocation_InventoryLevels.aspx.cs" Inherits="BregVision.Admin.PracticeLocation_InventoryLevels" %>

<%@ Register Src="~/Admin/UserControls/PracticeLocation/PracticeLocationInventoryLevels.ascx"
  TagName="PracticeLocationInventoryLevels" TagPrefix="bvl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" 
  Visible="true">
   <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <%--<script type="text/javascript">
        $(function () {
            $telerik.isIE7 = false;
            $telerik.isIE6 = false;
        });
        function MsgOnParLevelIncrease(parLevelTextboxID, oldParLevel) {
            var parLevelTextBox = document.getElementById(parLevelTextboxID);
            var newParLevel = parLevelTextBox.value;

            if (newParLevel > oldParLevel) {
                alert('Increasing Par Level does not incease your Consignment Level or notify Breg.  Please notify your Rep.');
            }
        };
    </script>--%>
</telerik:RadScriptBlock>
  <telerik:RadToolTipManager ID="RadToolTipManager1" Width="300px" Height="100px" RelativeTo="Element"
    Position="BottomRight" runat="server" AnimationDuration="200">
    <WebServiceSettings Path="~/HelpSystem/ToolTipWebService.asmx" Method="GetToolTip" />
  </telerik:RadToolTipManager>
  <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server"></telerik:RadAjaxManager>
    <span class="admin-page flex-grow">
    <bvl:PracticeLocationInventoryLevels ID="ctlPracticeLocationInventoryLevels" runat="server" />
        </span>
</asp:Content>
