﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace BregVision.Admin {
    
    
    public partial class PracticeLocation_InventoryCount {
        
        /// <summary>
        /// RadToolTipManager1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadToolTipManager RadToolTipManager1;
        
        /// <summary>
        /// RadAjaxManager1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadAjaxManager RadAjaxManager1;
        
        /// <summary>
        /// ctlRecordsPerPage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::BregVision.UserControls.General.RecordsPerPage ctlRecordsPerPage;
        
        /// <summary>
        /// ctlPracticeLocationInventoryCount control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::BregVision.Admin.UserControls.PracticeLocation.PracticeLocationInventoryCount ctlPracticeLocationInventoryCount;
        
        /// <summary>
        /// ctlConsignmentOrder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::BregVision.BregAdmin.UserControls.ConsignmentOrder ctlConsignmentOrder;
    }
}
