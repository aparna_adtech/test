﻿using System;
using System.Web.UI;

public partial class PracticeSettings_SiteBuilder : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		((BregVision.MasterPages.PracticeSettingsAdmin)Page.Master).SelectCurrentTab("TabSettingsSiteBuilder");
	}
}
