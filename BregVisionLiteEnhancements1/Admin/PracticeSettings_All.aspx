<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PracticeSettingsAdmin.Master" ValidateRequest="false" Inherits="PracticeSettings_All" Codebehind="PracticeSettings_All.aspx.cs" %>

<%@ Register Src="~/Admin/UserControls/PracticeSettings/PracticeSettingsGeneral.ascx" TagName="PracticeSettingsGeneral" TagPrefix="bvl" %>
  
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true" Visible="true">
  <bvl:PracticeSettingsGeneral ID="ctlPracticeSettingsGeneral" runat="server" />
</asp:Content>