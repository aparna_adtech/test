<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PracticeLocationAdmin.Master"
  Inherits="PracticeLocation_Physician" CodeBehind="PracticeLocation_Physician.aspx.cs" %>

<%@ Register Src="~/Admin/UserControls/PracticeLocation/PracticeLocationPhysicians.ascx" TagName="PracticeLocationPhysicians" TagPrefix="bvl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true" Visible="true">
  <bvl:PracticeLocationPhysicians ID="ctlPracticeLocationPhysicians" runat="server" />
</asp:Content>
