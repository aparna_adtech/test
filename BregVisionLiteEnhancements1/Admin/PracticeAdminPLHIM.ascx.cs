using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace BregVision.Admin
{
    public partial class PracticeAdminPLHIM : System.Web.UI.UserControl
    {
        //public HtmlGenericControl AdminMenu
        //{
        //    get { return liAdmin;  }
        //    set { liAdmin = value; }
        //}
        public HtmlGenericControl CheckInMenu
        {
            get { return liCheckIn; }
            set { liCheckIn = value; }
        }
        public HtmlGenericControl DispenseMenu
        {
            get { return liDispense; }
            set { liDispense = value; }
        }
        public HtmlGenericControl HomeMenu
        {
            get { return liHome; }
            set { liHome = value; }
        }
        public HtmlGenericControl InventoryMenu
        {
            get { return liInventory; }
            set { liInventory = value; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}