<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddUser.aspx.cs" Inherits="BregVision.AddUser" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
<asp:createuserwizard ID="Createuserwizard1" runat="server"> <WizardSteps> <asp:CreateUserWizardStep ID="CreateUserWizardStep1" runat="server"/> 
 <asp:WizardStep runat="server" AllowReturn="False"
                OnActivate="AssignUserToRoles_Activate"              
                OnDeactivate="AssignUserToRoles_Deactivate">

    <table>

        <tr>

           <td>

              Select one or more roles for the user:

           </td>

        </tr>

        <tr>

            <td>

               <asp:ListBox ID="AvailableRoles" runat="server" 

                            SelectionMode="Multiple" >

               </asp:ListBox>

            </td>

        </tr>

    </table>

</asp:WizardStep>

<asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server"/>  

</WizardSteps> </asp:createuserwizard>   
 
    </div>
    </form>
</body>
</html>

