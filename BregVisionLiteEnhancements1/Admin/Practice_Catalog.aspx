<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/MasterPages/PracticeAdmin.Master"
  Inherits="BregVision.Admin.Practice_Catalog" CodeBehind="Practice_Catalog.aspx.cs" %>

<%@ Register Src="~/Admin/UserControls/Practice/PracticeCatalog.ascx" TagName="PracticeCatalog"
  TagPrefix="bvl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true"
  Visible="true">
  <telerik:RadScriptBlock ID="RadScriptBlock3" runat="server">
    <script type="text/javascript">
      $(function () {
        $telerik.isIE7 = false;
        $telerik.isIE6 = false;
      });
        </script>
  </telerik:RadScriptBlock>
  <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
  </telerik:RadAjaxManager>
  <bvl:PracticeCatalog ID="ctlPracticeCatalog" runat="server" />
</asp:Content>
