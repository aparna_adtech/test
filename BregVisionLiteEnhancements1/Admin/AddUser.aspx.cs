using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace BregVision
{
    public partial class AddUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void AssignUserToRoles_Activate(object sender, EventArgs e)
        {



            // Databind list of roles in the role manager system to listbox

            AvailableRoles.DataSource = Roles.GetAllRoles(); ;

            AvailableRoles.DataBind();

        }



        // Deactivate event fires when user hits "next" in the CreateUserWizard 

        public void AssignUserToRoles_Deactivate(object sender, EventArgs e)
        {



            // Add user to all selected roles from the roles listbox

            for (int i = 0; i < AvailableRoles.Items.Count; i++)
            {

                if (AvailableRoles.Items[i].Selected == true)

                    Roles.AddUserToRole(Createuserwizard1.UserName, AvailableRoles.Items[i].Value);

            }

        }


    
    }
}
