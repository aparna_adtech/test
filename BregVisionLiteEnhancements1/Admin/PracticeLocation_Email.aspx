<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PracticeLocationAdmin.Master" Inherits="PracticeLocation_Email" CodeBehind="PracticeLocation_Email.aspx.cs" %>

<%@ Register Src="~/Admin/UserControls/PracticeLocation/PracticeLocationEmails.ascx" TagName="PracticeLocationEmails" TagPrefix="bvl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true"
  Visible="true">
    <span class="admin-page">

        <bvl:PracticeLocationEmails ID="ctlPracticeLocationEmails" runat="server" />
    </span>
</asp:Content>
