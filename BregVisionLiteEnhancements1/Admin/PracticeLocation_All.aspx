<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PracticeLocationAdmin.Master" ValidateRequest="false" Inherits="PracticeLocation_All" Codebehind="PracticeLocation_All.aspx.cs" %>

<%@ Register Src="~/Admin/UserControls/Practice/PracticeLocations.ascx" TagName="PracticeLocations" TagPrefix="bvl" %>
  
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true" Visible="true">
  <bvl:PracticeLocations ID="ctlPracticeLocations" runat="server"></bvl:PracticeLocations>
</asp:Content>