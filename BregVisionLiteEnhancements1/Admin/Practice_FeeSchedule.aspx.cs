﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using System.Web.UI;
using BregVision.Bases;
using ClassLibrary.DAL;
using DocumentFormat.OpenXml.Drawing;
using Telerik.Web.UI;
using Newtonsoft.Json;

namespace BregVision.Admin
{
    public partial class Practice_FeeSchedule : System.Web.UI.Page
    {
        public string FeeScheduleAsJson;
        public int PracticeId;

        protected void Page_Load(object sender, EventArgs e)
        {
            ((MasterPages.PracticeAdmin)Page.Master).SelectCurrentTab("TabFeeSchedule");

            PracticeId = (((MasterPages.PracticeAdmin) Page.Master)).Master.practiceID;
            
            using (var db = VisionDataContext.GetVisionDataContext())
            {

                var pracPayers = from practicePayer in db.PracticePayers
                    where practicePayer.PracticeID == PracticeId
                    orderby practicePayer.PracticePayerID
                    select practicePayer;

                var payerNames = from pracPayer in pracPayers
                    orderby pracPayer.PracticePayerID
								 select new { PracticePayerID = pracPayer.PracticePayerID, PayerName = pracPayer.Name, IsActive = pracPayer.IsActive };
                var payerNamesList = payerNames.ToList();

                var hcpcsList = new List<Hcpcs>();

                var distinctPracticeCatalogHcpcs = (from ph in db.ProductHCPCs
                    join pcp in db.PracticeCatalogProducts on ph.PracticeCatalogProductID equals
                        pcp.PracticeCatalogProductID
                    where pcp.PracticeID == PracticeId
                    select ph.HCPCS.Substring(0, 5)).Distinct();

//                var allowableAmounts = from pp in db.PracticePayers 
//                    join ppa in db.PracticePayerAllowables on pp.PracticePayerID equals ppa.PracticePayerID into ljppa
//                    from ppa in ljppa.DefaultIfEmpty()
//                    join hcpcs in db.HCPCs on ppa.HcpcsID equals hcpcs.HCPCSID
//                    join distinctPraCatHcps in distinctPracticeCatalogHcpcs on hcpcs.Code equals distinctPraCatHcps
//                    where pp.PracticeID == PracticeId
//                          orderby hcpcs.Code,pp.PracticePayerID
//                    select new AllowableDto(hcpcs,pp, ppa);
//                var allowableAmounts = from pp in db.PracticePayers
//                                       join hcpcs in db.HCPCs on 1 equals 1
//                                       join ppa in db.PracticePayerAllowables on pp.PracticePayerID equals ppa.PracticePayerID into ljppa
//                                       from ppa in ljppa.DefaultIfEmpty()
//                                       where distinctPracticeCatalogHcpcs.Contains(hcpcs.Code)
//                                       orderby hcpcs.Code, pp.PracticePayerID
//                                       select new AllowableDto(hcpcs, pp, ppa);

                var payerHcpcCombinations = from pp in db.PracticePayers.Where(x => x.PracticeID == PracticeId)
                                            from hcpcs in db.HCPCs.Where(y => distinctPracticeCatalogHcpcs.Contains(y.Code))
                                            select new {pp, hcpcs};

                var allowableAmounts = from payerHcpcCombo in payerHcpcCombinations
                                       join ppa in db.PracticePayerAllowables on payerHcpcCombo.pp.PracticePayerID equals ppa.PracticePayerID into ljppa
                                       from ppa in ljppa.Where(x => x.HcpcsID ==payerHcpcCombo.hcpcs.HCPCSID) .DefaultIfEmpty()
                                       orderby payerHcpcCombo.hcpcs.Code, payerHcpcCombo.pp.PracticePayerID
                                       select new AllowableDto(payerHcpcCombo.hcpcs, payerHcpcCombo.pp, ppa);


//                var distinctHcps = practiceHcpcs.SelectMany(hcpc => hcpc.Code).Distinct();
                
                    //join ppa in db.PracticePayerAllowables on hcpc1.HCPCSID equals ppa.HcpcsID
                    
                    //remove and change to join on practice hcpcs
                    //group hcpc1 by hcpc1.HCPCSID;

//                var hcpcsDb = from hcpc in db.HCPCs
//                    join hcpcId in practiceHcpcs on hcpc.Code equals hcpcId
//                    select hcpc;

                //TODO Have to account for new HCPCS being added to master list.  
                //TODO (cont) If no PracticePayerAllowable is defined for a payor-hcpcs combination it will need to be added.

                int allowableAmountsAdded = 0;
                
                var allowAmounts = new List<AllowableAmount>();
                var currentHcpc = "";
                foreach (var allowable in allowableAmounts)
                {
                    //Add amount to allowable list being built for code
                    if (currentHcpc == "" || allowable.Hcpc.Code == currentHcpc)
                    {
                        currentHcpc = allowable.Hcpc.Code;
                        AddAllowableToList(allowAmounts, allowable, db);
                    }
                    else//Add the completed Hcpcs/Allowable list that was being processed and start new one
                    {
                        hcpcsList.Add(new Hcpcs(currentHcpc, allowAmounts));
                        currentHcpc = allowable.Hcpc.Code;
                        allowAmounts = new List<AllowableAmount>();
                        AddAllowableToList(allowAmounts, allowable, db);
                    }
//                    var allowAmountsDb = from ams in hcpc.Allowable
//                        orderby ams.PracticePayerID
//                        select ams;
//                    string code = null;
//
//                    foreach (var practicePayerAllowable in hcpc.AllowabeAmounts)
//                    {
//                        code = practicePayerAllowable.HCPC.Code;
//                        allowAmounts.Add(new AllowableAmount(
//                            practicePayerAllowable.PracticePayerAllowableID, practicePayerAllowable.AllowableAmount));
//                        allowableAmountsAdded++;
//                    }
//                    hcpcsList.Add(new Hcpcs(code, allowAmounts));
                }

                var feeSchedule = new
                {
                    payerNamesList,
                    hcpcsList

                };

                FeeScheduleAsJson = JsonConvert.SerializeObject(feeSchedule);
            }
        }

        private static void AddAllowableToList(List<AllowableAmount> allowAmounts, AllowableDto allowableDto, VisionDataContext db)
        {
            var allowable = allowableDto.PracticePayerAllowable;
            if (allowable == null)
            {
                allowable = new PracticePayerAllowable
                {
                    PracticePayerID = allowableDto.PracticePayer.PracticePayerID,
                    HcpcsID = allowableDto.Hcpc.HCPCSID
                };
                db.PracticePayerAllowables.InsertOnSubmit(allowable);
                db.SubmitChanges();
            }

            allowAmounts.Add(new AllowableAmount(
                allowable.PracticePayerAllowableID,
                allowable.AllowableAmount));
        }

        public class AllowableDto
        {
            public HCPC Hcpc { get; set; }
            public PracticePayer PracticePayer { get; set; }
            public PracticePayerAllowable PracticePayerAllowable { get; set; }

            public AllowableDto(HCPC hcpc, PracticePayer practicePayer, PracticePayerAllowable practicePayerAllowable)
            {
                Hcpc = hcpc;
                PracticePayer = practicePayer;
                PracticePayerAllowable = practicePayerAllowable;
            }
        }


        [WebMethod]
        public static bool UpdateFeeSchedule(int feeScheduleId, string amount)
        {
            try
            {
                using (var db = VisionDataContext.GetVisionDataContext())
                {
                    var pyrFeeSched =
                        db.PracticePayerAllowables.Single(pyrAllow => pyrAllow.PracticePayerAllowableID == feeScheduleId);
                    pyrFeeSched.AllowableAmount = Convert.ToDecimal(amount);
                    db.SubmitChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        [WebMethod]
        public static bool AddPayer(string practiceId, string payerName)
        {
            try
            {
                using (var db = VisionDataContext.GetVisionDataContext())
                {
                    var pyr = new PracticePayer
                    {
                        PracticeID = Convert.ToInt32(practiceId),
                        Name = payerName,
						IsActive = true
                    };

                    int practiceIdInt = int.Parse(practiceId);

                    var distinctPracticeCatalogHcpcs = (from ph in db.ProductHCPCs
                                                        join pcp in db.PracticeCatalogProducts on ph.PracticeCatalogProductID equals
                                                            pcp.PracticeCatalogProductID
                                                        where pcp.PracticeID == practiceIdInt
                                                        select ph.HCPCS.Substring(0, 5)).Distinct();


                    var hcpcs = db.HCPCs.Where(x => distinctPracticeCatalogHcpcs.Contains(x.Code));

                    foreach (var hcpc in hcpcs)
                    {
                        pyr.PracticePayerAllowables.Add(new PracticePayerAllowable
                        {
                            HCPC = hcpc,
                            AllowableAmount = null
                        });
                    }
                    db.PracticePayers.InsertOnSubmit(pyr);
                    db.SubmitChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }
        [WebMethod]

		public static bool UpdatePayer(int payerId, string name, string enabled)
		{
			try
			{
				using (var db = VisionDataContext.GetVisionDataContext())
				{
					var pyr = db.PracticePayers.Single(pp => pp.PracticePayerID == payerId);
					if (name != "")
						pyr.Name = name;
					if (enabled != "")
						pyr.IsActive = (enabled == "true" ? true : false);
					db.SubmitChanges();
					return true;
				}
			}
			catch (Exception e)
			{
				return false;
			}
		}

        private class Hcpcs
        {
            public string Code { get; set; }
            public List<AllowableAmount> AllowableAmounts { get; set; }

            public Hcpcs(string code, List<AllowableAmount> allowableAmounts)
            {
                Code = code;
                AllowableAmounts = allowableAmounts;
            }
        }

        private class AllowableAmount
        {
            public int Id { get; set; }
            public decimal? Amount { get; set; }

            public AllowableAmount(int id, decimal? amount)
            {
                Id = id;
                Amount = amount;
            }
        }

    }

}
