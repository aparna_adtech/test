<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PracticeLocationAdmin.Master" CodeBehind="PracticeLocation_Superbill.aspx.cs" Inherits="BregVision.Admin.PracticeLocation_Superbill" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="RadAjax.Net2" Namespace="Telerik.WebControls" TagPrefix="radA" %>
<%@ Register Assembly="RadGrid.Net2" Namespace="Telerik.WebControls" TagPrefix="radG" %>
<%@ Register Assembly="RadInput.Net2" Namespace="Telerik.WebControls" TagPrefix="radI" %>
<%@ Register Assembly="RadComboBox.Net2" Namespace="Telerik.WebControls" TagPrefix="radC" %>
<%@ Register Assembly="RadToolbar.Net2" Namespace="Telerik.WebControls" TagPrefix="radTlb" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<%--@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" --%>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent"  runat="server" EnableViewState="true" Visible="true">

<script type="text/javascript">
 var grid;

 function GridCreated()
 {
   grid = this;

}
//function OpenWindow() {
//    var oWnd = window.open("ReportViewer.aspx", "mywindow", "status=1");
//}  

</script> 
    <div>
        <table id="tblUpload" runat="server" width="100%" cellpadding="0" cellspacing="0" >
	        <tbody>
		        <tr>
		            <td style="width:40%"></td>
		            <td style="width:20%"></td>
		            <td style="width:40%">
		                <table>
                            <tr>
                                <td>
                                    <asp:FileUpload ID="LogoUploader" runat="server" />
                                </td>
                                <td>
                                    <asp:Button runat="server" ID="btnUploadLogo" Text="Upload" OnClick="btnbtnUploadLogo_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Image ID="imgPracticeLogo" runat="server" Height="100px" Visible="false" />
                                </td>
                            </tr>
                        </table>
		            </td>
		        </tr>
	        </tbody>
        </table>
                
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
		    <tbody>
				<tr class="AdminPageTitle">
                    <td colspan="3" align="center">
                        <asp:Label ID="lblContentHeader" runat="server" Text="Super Bill" />
                    </td>
                </tr>
		        <tr>
			        <th id="Grid1_head" align="center" class="AdminSubTitle"> Practice Catalog</th>
			        <th>&nbsp;</th>
			        <th id="Grid2_head" align="center" class="AdminSubTitle">Create Super Bill/Flow Sheet</th>
		        </tr>
                <tr>
		            <td style="width: 44%">
		                <asp:Label ID="lblMCStatus" runat="server" CssClass="WarningMsg"></asp:Label>
		            </td>
		            <td style="width: 12%">
                         <asp:Button ID="btnViewSuperbill" runat="server" OnClick="btnViewSuperbill_Click" Text="View/Print Super Bill" />
		            </td>
		            <td style="width: 44%">
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>
                                    <asp:Label ID="lblLogoFilename" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Image ID="imgUpload" runat="server" Visible="false" />
                                </td>
                            </tr>
                        </table>
		                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
	                            <td>
                                    <table cellpadding="0" cellspacing="0" border="0">
		                                  <tr>
		                                    <td colspan="3" >
		                                        <asp:Label ID="lblPCStatus" runat="server" CssClass="WarningMsg"></asp:Label>					                
		                                    </td>
		                                  </tr>
                                          <tr>
		                                    <td colspan="3" rowspan="4">
                                                <asp:Button ID="btnViewFlowSheet" runat="server" OnClick="btnViewFlowSheet_Click" Text="View/Print Flowsheet" />
                                            </td>
                                          </tr>
		                            </table>        					                   
				        
		                        </td>
		                    </tr>
		                </table>        					        
		            </td>
		        </tr>
			</tbody>
        </table>
				<radA:RadAjaxPanel ID="RadAjaxPanel1" runat="server"  Width="100%">
					<table width="100%" cellpadding="0" cellspacing="0">
				        <tbody>							
					        <tr>
					            <td>
                                    <radTlb:RadToolbar ID="RadToolbar1" runat="server"  OnOnClick="RadToolbar1_OnClick"
                                     AutoPostBack="True" Skin="Inox_32x32" UseFadeEffect="True" EnableViewState="false">
                                  <Items>
					                    <radTlb:RadToolbarTemplateButton ID="rttbFilter" runat="server" Width="145px">
                                              <ButtonTemplate>
                                                  <telerik:RadTextBox runat="server" TabIndex="2" ID="RadTextBox1" Label="Search"/>
                                                   <%--<asp:TextBox ID="tbFilter" TabIndex="2" runat="server" Width="88"></asp:TextBox>--%>
                                              </ButtonTemplate>
                                        </radTlb:RadToolbarTemplateButton>
                                        <radTlb:RadToolbarTemplateButton ID="rttbSearchFilter" runat="server" Width="110px">
                                            <ButtonTemplate>
                                                <radC:RadComboBox id="cboSearchFilter" TabIndex="1" runat="server" Skin="WebBlue" ExpandEffect="fade" Width="100" >
                                                    <Items>
                                                        <radC:RadComboBoxItem ID="RadComboBoxItem3" runat="server" Text="Product Code" Value="Code" />
                                                    </Items>                                          
                                                     <Items>
                                                        <radC:RadComboBoxItem ID="RadComboBoxItem5" runat="server" Text="Product Name" Value="Name" />
                                                    </Items>
                                                    <Items>
                                                        <radC:RadComboBoxItem ID="RadComboBoxItem2" runat="server" Text="Manufacturer" Value="Brand" />
                                                    </Items>                    
                                                    <Items>
                                                        <radC:RadComboBoxItem ID="RadComboBoxItem4" runat="server" Text="Category" Value="Category" />
                                                    </Items>
                                                    <Items>
                                                        <radC:RadComboBoxItem ID="RadComboBoxItem1" runat="server" Text="HCPCs" Value="HCPCs" />
                                                    </Items> 
                                                </radC:RadComboBox>
                                            </ButtonTemplate>                          
                                        </radTlb:RadToolbarTemplateButton>

                                        <radTlb:RadToolbarToggleButton ID="rtbFilter" runat="server" 
                                        ButtonText="Search" ToolTip="Search" CausesValidation="true" AccessKey="s"
                                        TabIndex="3" CommandName="Search" Hidden="False" ButtonImage="Find.gif" Width="200px" />
                                        <radTlb:RadToolbarToggleButton ID="rtbBrowse" runat="server" 
                                        ButtonText="Browse" ToolTip="Browse all products" CausesValidation="true" AccessKey="p"
                                        TabIndex="4" CommandName="Browse" Hidden="False" ButtonImage="NewProject.gif" Width="200px"  />
                                    </Items>                                                                        
                                </radTlb:RadToolbar>
					            </td>
					            <td>
					            </td>
					            <td>					            
					            </td>
					        </tr>
					        <tr>
						        <td width="47%" align="left" valign="top">
                     <radG:RadGrid ID="grdPracticeCatalog" runat="server" GridLines="None" OnPreRender="grdPracticeCatalog_PreRender" OnNeedDataSource="grdPracticeCatalog_NeedDataSource" OnDetailTableDataBind="grdPracticeCatalog_DetailTableDataBind" OnPageIndexChanged="grdPracticeCatalog_PageIndexChanged" EnableAJAX="True" EnableAJAXLoadingTemplate="True" LoadingTemplateTransparency="10" AllowPaging="True" AllowSorting="True" ShowGroupPanel="True" Skin="Office2007" Width="98%" AllowMultiRowSelection="True" PageSize="100" EnableViewState="true">
                        <PagerStyle Mode="NextPrevAndNumeric" />
                        <MasterTableView DataKeyNames="PracticeCatalogSupplierBrandID" AutoGenerateColumns="False"  EnableTheming="true">
                            <ExpandCollapseColumn Resizable="False">
                                <HeaderStyle Width="20px" />
                            </ExpandCollapseColumn>
                            <DetailTables>
                                <radG:GridTableView Name="grdtvPracticeCatalog" DataKeyNames="PracticeCatalogSupplierBrandID" runat="server"  AutoGenerateColumns="False">
                                    <ParentTableRelation>
                                    <radG:GridRelationFields DetailKeyField="PracticeCatalogSupplierBrandID" MasterKeyField="PracticeCatalogSupplierBrandID" />
                                </ParentTableRelation>
                                    <ExpandCollapseColumn Visible="False" Resizable="False">
                                        <HeaderStyle Width="20px" />
                                    </ExpandCollapseColumn>
                                    <RowIndicatorColumn Visible="False">
                                        <HeaderStyle Width="20px" />
                                    </RowIndicatorColumn>
								        <Columns>
									        <radG:GridClientSelectColumn UniqueName="ClientSelectColumn"/>
									        <radg:GridBoundColumn  DataField="PracticeCatalogSupplierBrandID" Visible="false" UniqueName="PracticeCatalogSupplierBrandID" ></radg:GridBoundColumn>
									        <radg:GridBoundColumn  DataField="PracticeCatalogProductID" display="false" UniqueName="PracticeCatalogProductID"></radg:GridBoundColumn>
									        <radg:GridBoundColumn DataField="MasterCatalogProductID" display="false" UniqueName="MasterCatalogProductID"></radg:GridBoundColumn>
									        <radg:GridBoundColumn DataField="ShortName" HeaderText="Name:" UniqueName="ShortName"></radg:GridBoundColumn>
									        <radg:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code"></radg:GridBoundColumn>
									        <radg:GridBoundColumn  DataField="LeftRightSide" HeaderText="Side:" UniqueName="LeftRightSide"></radg:GridBoundColumn>
									        <radg:GridBoundColumn  DataField="Size" HeaderText="Size:" UniqueName="Size"></radg:GridBoundColumn>
									        <radg:GridBoundColumn DataField="WholesaleCost" HeaderText="Wholesale Cost" UniqueName="WholesaleCost" DataFormatString="{0:C}"></radg:GridBoundColumn>
									        <radg:GridBoundColumn DataField="BillingCharge" HeaderText="Billing Charge" UniqueName="BillingCharge" DataFormatString="{0:C}"></radg:GridBoundColumn>
									        <radg:GridBoundColumn DataField="BillingChargeCash" HeaderText="Billing Charge Cash" UniqueName="BillingChargeCash" DataFormatString="{0:C}"></radg:GridBoundColumn>
									        <radg:GridBoundColumn DataField="DMEDeposit" HeaderText="DME Deposit" UniqueName="DMEDeposit" DataFormatString="{0:C}"></radg:GridBoundColumn >
							        </Columns>
				                      </radG:GridTableView>        				              
                            </DetailTables>
                                        <RowIndicatorColumn Visible="False">
                                            <HeaderStyle Width="20px" />
                                        </RowIndicatorColumn>
                                        <Columns>
							                <radG:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" UniqueName="PracticeCatalogSupplierBrandID"  Visible="false" >
                                            </radG:GridBoundColumn>
                                            <radG:GridBoundColumn DataField="SupplierName" HeaderText="Supplier" UniqueName="SupplierName" />
                                            <radG:GridBoundColumn DataField="BrandName" HeaderText="Brand" UniqueName="BrandName">
                                            </radG:GridBoundColumn>
                                            
                                        </Columns>
                                    </MasterTableView>
                                    <GroupPanel Visible="True">
                                    </GroupPanel>
                                    <ClientSettings AllowColumnsReorder="false" AllowDragToGroup="false" ReorderColumnsOnClient="false">
                                        <Selecting AllowRowSelect="True"/>
                                         <ClientEvents OnGridCreated="GridCreated" ></ClientEvents>
                                    </ClientSettings>
                                    <ExportSettings>
                                        <Pdf PageBottomMargin="" PageFooterMargin="" PageHeaderMargin="" PageHeight="11in"
                                            PageLeftMargin="" PageRightMargin="" PageTopMargin="" PageWidth="8.5in" />
                                    </ExportSettings>
                                </radG:RadGrid>

                                   </td>
						        <td width="6%" align="center">
						        <asp:Button runat="server" ID="btnMoveToPC" Text=">>" ToolTip="Move selected item to selected category in Billing Sheets" OnClick="btnMoveToPC_Click" />
						        <br />
						        </td>
						        <td width="47%" align="left" valign="top">
        						
						            <asp:SqlDataSource ID="sdsHCPCLookup" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:BregVisionConnectionString %>" 
                                        SelectCommand="usp_GetSuperbillHCPCs" SelectCommandType="StoredProcedure">
                                        <SelectParameters>
                                            <asp:SessionParameter DefaultValue="3" Name="PracticeLocationID" 
                                                SessionField="PracticeLocationID" Type="Int32" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                   <radG:RadGrid 
                                        ID="grdSuperbill" runat="server" GridLines="None" 
                                        OnNeedDataSource="grdSuperbill_NeedDataSource" 
                                        OnDetailTableDataBind="grdSuperbill_DetailTableDataBind"
                                        EnableAJAX="False" 
                                        EnableAJAXLoadingTemplate="False" 
                                        LoadingTemplateTransparency="10" AllowPaging="True" 
                                        AllowSorting="True" ShowGroupPanel="True" Skin="Office2007" 
                                        Width="98%" AllowMultiRowSelection="True" PageSize="100" EnableViewState="true"
                                        OnItemCommand="grdSuperbill_ItemCommand" 
                                        OnInsertCommand="grdSuperbill_InsertCommand"
                                        OnDeleteCommand="grdSuperbill_DeleteCommand"
                                        OnUpdateCommand="grdSuperbill_UpdateCommand"
                                        OnCancelCommand="grdSuperbill_CancelCommand" 
                                        OnItemDataBound="grdSuperbill_OnItemDataBound" 
                                        OnEditCommand="grdSuperbill_OnEditCommand">
                                    
                                        <PagerStyle Mode="NextPrevAndNumeric" />
                                        
                                        <MasterTableView 
                                            Name="mtvSuperbill" 
                                            DataKeyNames="ColumnID" 
                                            AutoGenerateColumns="False" 
                                            EnableTheming="true" 
                                            EnableViewState="true">
                                            
                                            <ExpandCollapseColumn Resizable="False">
                                                    <HeaderStyle Width="20px" />
                                            </ExpandCollapseColumn>
                                            
                                            <Columns>
 				                                <radG:GridBoundColumn DataField="ColumnID" UniqueName="ColumnID" Visible="False" />
           
                                                <radG:GridBoundColumn DataField="Name" HeaderText="Column On Super Bill" UniqueName="Name" ItemStyle-Font-Bold="true" />
         
                                                
                                            </Columns>
                                            
                                            
                                            <DetailTables>
                                                    
                                                    <radG:GridTableView Name="grdtvCategory" DataKeyNames="CategoryID" runat="server" ShowHeader="false" CommandItemSettings-AddNewRecordText="New Category"  AutoGenerateColumns="False" CommandItemDisplay="Top">
                                                    <ParentTableRelation>
                                                        <radG:GridRelationFields DetailKeyField="ColumnNumber" MasterKeyField="ColumnID" />
                                                    </ParentTableRelation>
                                                    <ExpandCollapseColumn Visible="False" Resizable="False">
                                                        <HeaderStyle Width="20px" />
                                                    </ExpandCollapseColumn>
                                                    <RowIndicatorColumn Visible="False">
                                                        <HeaderStyle Width="20px" />
                                                    </RowIndicatorColumn>
                        								
                        								
						                            <Columns>
							                            <radG:GridClientSelectColumn UniqueName="ClientSelectColumn"/>
							                            <radg:GridBoundColumn  DataField="CategoryID" Visible="false" ReadOnly="true"  UniqueName="CategoryID" ></radg:GridBoundColumn>
							                            <radg:GridBoundColumn  DataField="ColumnNumber" Visible="false" display="false" ReadOnly="true" UniqueName="ColumnNumber"></radg:GridBoundColumn>
							                            <radg:GridBoundColumn DataField="Sequence" Visible="false" display="false" ReadOnly="true" UniqueName="Sequence"></radg:GridBoundColumn>
							                            <radg:GridBoundColumn DataField="CategoryName" HeaderText="Category" HeaderStyle-Font-Bold="true" ItemStyle-Font-Bold="true" UniqueName="CategoryName"></radg:GridBoundColumn>
                                                        <radG:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" Visible="true" ImageUrl="~/App_Themes/Breg/images/Common/delete.png"></radG:GridButtonColumn> 
                                                        <radg:GridEditCommandColumn ButtonType="ImageButton" InsertText="Add Category" InsertImageUrl="~/RadControls/Grid/Skins/Office2007/Insert.gif" CancelImageUrl="~/RadControls/Grid/Skins/Office2007/Cancel.gif" UniqueName="EditCommandColumn"></radg:GridEditCommandColumn>
						                            </Columns>
                							        <DetailTables>   

                                                        <radG:GridTableView Name="grdtvSuperbill" DataKeyNames="SuperBillProductID" runat="server"  AutoGenerateColumns="False">
                                                            <ParentTableRelation>
                                                                <radG:GridRelationFields DetailKeyField="CategoryID" MasterKeyField="CategoryID" />
                                                            </ParentTableRelation>
                                                            <ExpandCollapseColumn Visible="False" Resizable="False">
                                                                <HeaderStyle Width="20px" />
                                                            </ExpandCollapseColumn>
                                                            <RowIndicatorColumn Visible="False">
                                                                <HeaderStyle Width="20px" />
                                                            </RowIndicatorColumn>
                                								
                                								
						                                    <Columns>
						                                        <radG:GridBoundColumn DataField="SuperBillProductID" UniqueName="SuperBillProductID" Visible="False" ReadOnly="true" />
						                                        <radG:GridBoundColumn DataField="CategoryID" UniqueName="CategoryID" Visible="False" ReadOnly="true" />
						                             	        <radG:GridBoundColumn DataField="PracticeCatalogProductID" UniqueName="PracticeCatalogProductID" Visible="False" ReadOnly="true" />
                                                                <radG:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" Visible="true" ImageUrl="~/App_Themes/Breg/images/Common/delete.png"></radG:GridButtonColumn> 
                                                                <radG:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Visible="true" ImageUrl="~/App_Themes/Breg/images/Common/edit.png"></radG:GridButtonColumn>                                   

							                                    <radG:GridClientSelectColumn UniqueName="ClientSelectColumn"/>
							                                    <radG:GridDropDownColumn DataSourceID="sdsHCPCLookup" AllowFiltering="false" DataField="HCPCs" HeaderText="HCPCs" UniqueName="HCPCs" ListTextField="HCPCsCode" ListValueField="HCPCsCode"/>
                                                                <radG:GridBoundColumn DataField="ProductName" HeaderText="ProductName" UniqueName="ProductName" />
                                                                <radG:GridCheckBoxColumn DataField="FlowsheetOnly" ColumnEditorID="chkEditor"  HeaderText="Flowsheet Only" UniqueName="FlowsheetOnly" />
                                                                <radG:GridEditCommandColumn UniqueName="grdEditCommandColumn5" Visible="false"></radG:GridEditCommandColumn>
						                                    </Columns>
                        						            <EditFormSettings EditFormType="Template">
                        						                <EditColumn UniqueName="EditCommandColumn1" UpdateText="Update" CancelText="Cancel">  
                                                                </EditColumn>
                        						                <FormTemplate>
                        						                    <table id="Table2" cellspacing="2" cellpadding="1" width="450" border="1" rules="none" style="border-collapse: collapse">
                                                                        <tr class="EditFormHeader">
                                                                            <td colspan="2">
                        						                                <b>Billing</b>
                        						                            </td>
                        						                        </tr>
                        						                        <tr>
                                                                            <td>
                                                                                HCPCs:</td>
                                                                            <td>
                                                                                <radc:radcombobox                             
                                                                                    id="ddlHCPCs" 
                                                                                    Runat="server"
                                                                                    DataTextField="HCPCsCode" 
                                                                                    DataValueField="HCPCsCode" 
                                                                                    AllowCustomText="true" 
                                                                                    TabIndex="1" 
                                                                                    >
                                                                                </radc:radcombobox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Product Name:</td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtProductName" runat="server" Text='<%# Bind( "ProductName") %>' TabIndex="2" Width="400"> </asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Flowsheet Only</td>
                                                                            <td><asp:CheckBox ID="chkFlowsheetOnly" runat="server" Checked='<%# Bind( "FlowsheetOnly") %>' TabIndex="3" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right" colspan="2">
                                                                                <asp:Button ID="btnUpdate" Text="Update" runat="server" CommandName="Update"></asp:Button>&nbsp;
                                                                                <asp:Button ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False" CommandName="Cancel"></asp:Button></td>
                                                                        </tr>
                        						                    </table>
                                                                </FormTemplate>
                        						            </EditFormSettings>
                                						    
                        						              <DetailTables>  
                        						                    <radG:GridTableView Name="grdtvAddOn" DataKeyNames="SuperBillProductAddOnID" runat="server"  AutoGenerateColumns="False" CommandItemSettings-AddNewRecordText="New AddOn" CommandItemDisplay="Top">
                                                                        <ParentTableRelation>
                                                                            <radG:GridRelationFields DetailKeyField="SuperBillProductID_FK" MasterKeyField="SuperBillProductID" />
                                                                        </ParentTableRelation>
                                                                        <Columns> 
                                                                            <radG:GridBoundColumn DataField="SuperBillProductAddOnID" UniqueName="SuperBillProductAddOnID" Visible="False" ReadOnly="true" />
						                             	                    <radG:GridBoundColumn DataField="PracticeCatalogProductID" UniqueName="PracticeCatalogProductID" Visible="False" ReadOnly="true" />
                                                                            <radG:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" Visible="true" ImageUrl="~/App_Themes/Breg/images/Common/delete.png"></radG:GridButtonColumn> 
                                                                            <radG:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Visible="true" ImageUrl="~/App_Themes/Breg/images/Common/edit.png"></radG:GridButtonColumn>                                   

							                                                <radG:GridClientSelectColumn UniqueName="ClientSelectColumn"/>
							                                                <radG:GridDropDownColumn DataSourceID="sdsHCPCLookup" AllowFiltering="false" DataField="HCPCs" HeaderText="HCPCs" UniqueName="HCPCs" ListTextField="HCPCsCode" ListValueField="HCPCsCode"/>
                                                                            <radG:GridBoundColumn DataField="ProductName" HeaderText="AddOn Name" UniqueName="ProductName" />
                                                                            <radG:GridBoundColumn DataField="Deposit" HeaderText="Deposit" UniqueName="Deposit" DataFormatString="{0:C2}" />
                                                                            <radG:GridBoundColumn DataField="Cost" HeaderText="Cost" UniqueName="Cost" DataFormatString="{0:C2}" />
                                                                            <radG:GridBoundColumn DataField="Charge" HeaderText="Charge" UniqueName="Charge" DataFormatString="{0:C2}" />
                                                                            <radG:GridCheckBoxColumn DataField="FlowsheetOnly" ColumnEditorID="chkEditor"  HeaderText="Flowsheet Only" UniqueName="FlowsheetOnly" />
                                                                            <radg:GridEditCommandColumn ButtonType="ImageButton" Visible="false" InsertText="New AddOn" InsertImageUrl="~/RadControls/Grid/Skins/Office2007/Insert.gif" CancelImageUrl="~/RadControls/Grid/Skins/Office2007/Cancel.gif" UniqueName="EditCommandColumn6"></radg:GridEditCommandColumn>
                                                                            <radG:GridBoundColumn DataField="SuperBillProductID_FK" UniqueName="SuperBillProductID_FK" Visible="False" ReadOnly="true" />
                                                                        </Columns>  
                                                                        <EditFormSettings EditFormType="Template">
                        						                            <EditColumn UniqueName="EditCommandColumn2" UpdateText="Update" CancelText="Cancel">  
                                                                            </EditColumn>
                        						                            <FormTemplate>
                        						                                <table id="Table3" cellspacing="2" cellpadding="1" width="450" border="1" rules="none" style="border-collapse: collapse">
                                                                                    <tr class="EditFormHeader">
                                                                                        <td colspan="2">
                        						                                            <b>Billing</b>
                        						                                        </td>
                        						                                    </tr>
                        						                                    <tr>
                                                                                        <td>
                                                                                            HCPCs:</td>
                                                                                        <td>
                                                                                            <radc:radcombobox                             
                                                                                                id="ddlAddOnHCPCs" 
                                                                                                Runat="server"
                                                                                                DataTextField="HCPCsCode" 
                                                                                                DataValueField="HCPCsCode" 
                                                                                                AllowCustomText="true" 
                                                                                                TabIndex="1" 
                                                                                                >
                                                                                            </radc:radcombobox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>AddOn Name:</td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="txtAddOnProductName" runat="server" Text='<%# Bind( "ProductName") %>' TabIndex="2" Width="400"> </asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Deposit:</td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="txtDeposit" runat="server" Text='<%# Bind( "Deposit", "{0:C}") %>' TabIndex="2" Width="50"> </asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    
                                                                                    <tr>
                                                                                        <td>Cost:</td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="txtCost" runat="server" Text='<%# Bind( "Cost", "{0:C}") %>' TabIndex="2" Width="50"> </asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    
                                                                                    <tr>
                                                                                        <td>Charge:</td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="txtCharge" runat="server" Text='<%# Bind( "Charge", "{0:C}") %>' TabIndex="2" Width="50"> </asp:TextBox>
                                                                                        </td>
                                                                                    </tr>                                                                            
                                                                                    <tr>
                                                                                        <td>Flowsheet Only</td>
                                                                                        <td><asp:CheckBox ID="chkAddOnFlowsheetOnly" runat="server" Checked='<%# Bind( "FlowsheetOnly") %>' TabIndex="3" /></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="right" colspan="2">
                                                                                            <asp:Button ID="btnUpdateAddOn" Text='<%# (Container is Telerik.WebControls.GridEditFormInsertItem) ? "Insert" : "Update" %>'
                                                                                            runat="server" CommandName='<%# (Container is Telerik.WebControls.GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'></asp:Button>&nbsp;

                                                                                            
                                                                                            <asp:Button ID="btnCancelAddOn" Text="Cancel" runat="server" CausesValidation="False" CommandName="Cancel"></asp:Button></td>
                                                                                    </tr>
                        						                                </table>
                                                                            </FormTemplate>
                        						                        </EditFormSettings>   						            
                        						                    </radG:GridTableView>
                        						              </DetailTables>
			                                                  </radG:GridTableView>
                                				              
                                                    </DetailTables>
                        								 
			                                        </radG:GridTableView>
                        				              
                                            </DetailTables>
                                            
                                            
                                            
                                            <RowIndicatorColumn Visible="False">
                                                <HeaderStyle Width="20px" />
                                            </RowIndicatorColumn>
                                  
                                        </MasterTableView>
                                        
                                        <GroupPanel Visible="True" />

                                        <ClientSettings AllowColumnsReorder="false" AllowDragToGroup="false" ReorderColumnsOnClient="false">
                                            <Selecting AllowRowSelect="True"/>
                                             <ClientEvents OnGridCreated="GridCreated" ></ClientEvents>
                                        </ClientSettings>
                                        <ExportSettings>
                                            <Pdf PageBottomMargin="" PageFooterMargin="" PageHeaderMargin="" PageHeight="11in"
                                                PageLeftMargin="" PageRightMargin="" PageTopMargin="" PageWidth="8.5in" />
                                        </ExportSettings>
                                  
                                        
                                        
                                    </radG:RadGrid>
                                    <radG:GridCheckBoxColumnEditor ID="chkEditor" runat="server" ></radG:GridCheckBoxColumnEditor> 
                                     
                                    
                                </td>
					        </tr>	
					        <tr>
					            <td colspan="3">
					            
					            </td>
					        </tr>							
        					
				        </tbody>
			        </table>
			        </radA:RadAjaxPanel>
	            <br />
        <asp:Button runat="server" ID="btnShowPage" Text="Return To Billing Forms Setup" OnClick="btnShowPage_Click" Visible="false" />   
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Enabled="False" Visible="False" Font-Names="Verdana" Font-Size="8pt" Height="800px" ProcessingMode="Remote" Width="1200px">
            <ServerReport ReportServerUrl="http://bimsdev/reportserver" />
        </rsweb:ReportViewer>
        
    </div>

</asp:Content>