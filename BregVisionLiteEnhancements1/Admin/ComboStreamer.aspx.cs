﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik;
using Telerik.WebControls;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

namespace BregVision.Admin
{
    public partial class ComboStreamer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void cboPhysicianName_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_Physician_Select_All_By_PracticeLocationID");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, Convert.ToInt32(Session["PracticeLocationID"].ToString()));
            DataSet ds = db.ExecuteDataSet(dbCommand);
            //return ds;


            //Data.vaccineprofessionalDataContext db = new Data.vaccineprofessionalDataContext();
            //var results = db.vpBusinessUnitSeach("", "UnArchive");
            RadComboBox comboBox = (RadComboBox)sender;
            comboBox.DataSource = ds;
            
            //comboBox.DataBind();
        }


        public DataSet dsPopulatePhysicians()
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_Physician_Select_All_By_PracticeLocationID");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, Convert.ToInt32(Session["PracticeLocationID"].ToString()));
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;


        }

    
    }


}
