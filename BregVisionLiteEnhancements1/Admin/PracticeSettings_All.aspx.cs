using System;
using System.Reflection.Emit;
using System.Web.UI;
using Telerik.OpenAccess;
using Label = System.Web.UI.Control;


public partial class PracticeSettings_All : System.Web.UI.Page
{
  //this might be set in the master page on the init event
  private bool _ShowSettings = true;
  public bool ShowSettings
	{
    get
    {
      return _ShowSettings;
    }
    set
    {
			_ShowSettings = value;
    }
  }

  protected void Page_Load(object sender, EventArgs e)
  {
    //this might be set in the master page on the init event
    //this.ctlPracticeSettings.Visible = this.ShowSettings;

    Label pageTitleLabel = Master.FindControl("practiceSettingsViewName");
    if (pageTitleLabel != null)
    {
     // pageTitleLabel.Text = "All Locations;";
    }
    //((BregVision.MasterPages.PracticeAdmin)Page.Master).SelectCurrentTab("PracticeSettingsGeneralTab");

  }

}

