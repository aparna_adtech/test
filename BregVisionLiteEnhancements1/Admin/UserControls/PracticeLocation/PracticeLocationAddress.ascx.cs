﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.Admin.PracticeSetup;

namespace BregVision.Admin.UserControls.PracticeLocation
{

    public partial class PracticeLocationAddress : PracticeSetupBase, IVisionWizardControl
    {
        // Set initial values.	
        bool isPracticeCentralizedBilling;		// use post demo

        int addressID = -1;                 //use session or get from practice.
        string attentionOf = "";
        string addressLine1 = "";
        string addressLine2 = "";
        string city = "";
        string state = "";                  //maybe should be state // review best method for this later.
        string zipCode = "";
        string zipCodePlus4 = "";
        bool isBillingCentralized;

        /// <summary>
        /// IVisionWizardControl Interface.
        /// </summary>
        public bool NeedsInitialBind { get; set; }

        new protected void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);
            ucResetSaveButtonTable.Reset += new EventHandler(btnReset_Click);		//new EventHandler( ucResetSaveButtonTable_Reset );
            ucResetSaveButtonTable.Save += new EventHandler(btnSave_Click);		//new EventHandler(ucResetSaveButtonTable_Save);
        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            lblPracticeLocationShippingHeader.Text = practiceLocationName + " Shipping Address";
            lblPracticeLocationBillToHeader.Text = practiceLocationName + " Bill To Address";

            //  Get the Shipping Address and display
            GetPracticeLocationShippingAddress(practiceLocationID);
            //SetShippingAddressTextBoxValues();  the is included in the above method.

            // Get practice Billing type;  either Centralized or Decentrallized.
            isPracticeCentralizedBilling = GetPracticeBillingType(practiceID);

            //  Get the Billing Address and display
            GetPracticeLocationBillingAddress(practiceLocationID);
            SetBillingAddressTextBoxValues();

            if (isPracticeCentralizedBilling)
            {
                // is Centralized, so BillingAddress controls are disable.
                pnlBillingAddress.Enabled = false;
                ucPracticeLocationBillingAddress.IsValidationEnabled = false;
            }
            else
            {
                // is Decentralized, so BillingAddress controls are enable.
                //  Note:Insert/Update BillingAddress is not implemented yet.
                pnlBillingAddress.Enabled = true;
                ucPracticeLocationBillingAddress.IsValidationEnabled = true;
            }
            
        }



        // Call the BLL Class.
        //
        protected void GetPracticeLocationShippingAddress(int practiceLocationID)
        {
            BLL.PracticeLocation.ShippingAddress bllPLSA = new BLL.PracticeLocation.ShippingAddress();
            bllPLSA.PracticeLocationID = practiceLocationID;
            bllPLSA.Select(bllPLSA);

            //bllPLSA = bllPLSA.Select( practiceLocationID );

            //practiceLocationID = bllPLSA.PracticeLocationID;
            addressID = bllPLSA.AddressID;
            attentionOf = bllPLSA.AttentionOf;
            addressLine1 = bllPLSA.Address.AddressLine1;
            addressLine2 = bllPLSA.Address.AddressLine2;
            city = bllPLSA.Address.City;
            state = bllPLSA.Address.State;
            zipCode = bllPLSA.Address.ZipCode;
            zipCodePlus4 = bllPLSA.Address.ZipCodePlus4;


            //DAL.PracticeLocation.ShippingAddress  dalPLSA = new DAL.PracticeLocation.ShippingAddress();
            //dalPLSA.Select( practiceLocationID, out addressID, out attentionOf, out addressLine1
            //	, out addressLine2, out city, out state, out zipCode, out zipCodePlus4 );
            // Set the Shipping Address Text Box Values.
            SetShippingAddressTextBoxValues();
        }

        protected void GetPracticeLocationBillingAddress(int practiceLocationID)
        {
            //jb this should be DAL not BLL ClassLibrary.BLL.PracticeLocation.BillingAddress dalPLBA = new ClassLibrary.BLL.PracticeLocation.BillingAddress();

            DAL.PracticeLocation.BillingAddress dalPLBA = new DAL.PracticeLocation.BillingAddress();
            dalPLBA.Select(practiceLocationID, out attentionOf, out addressID, out addressLine1, out addressLine2, out city, out state, out zipCode, out zipCodePlus4);
            //dalPBA.SelectOneByPractice( practiceID, out attentionOf, out addressID, out addressLine1, out addressLine2, out city, out state, out zipCode, out zipCodePlus4 );

            // Check practice to see if the Practice has centralized billing 
            // Is so then the location's billing address should be populated from there.
            // Display in billing address in page and disable.

        }

        protected void SetShippingAddressTextBoxValues()
        {
            // SetRadioButtonListValues( isBillingCentralized );

            //UserControl ucPracticeBillingAddress9 = ( UserControl ) FindControl( "ucPracticeBillingAddress" );

            TextBox txtAttentionOf = (TextBox)ucPracticeLocationShippingAddress.FindControl("txtAttentionOf");
            TextBox txtAddressLine1 = (TextBox)ucPracticeLocationShippingAddress.FindControl("txtAddressLine1");
            TextBox txtAddressLine2 = (TextBox)ucPracticeLocationShippingAddress.FindControl("txtAddressLine2");
            TextBox txtCity = (TextBox)ucPracticeLocationShippingAddress.FindControl("txtCity");

            DropDownList cmbState = (DropDownList)ucPracticeLocationShippingAddress.FindControl("cmbState");

            TextBox txtZipCode = (TextBox)ucPracticeLocationShippingAddress.FindControl("txtZipCode");
            TextBox txtZipCodePlus4 = (TextBox)ucPracticeLocationShippingAddress.FindControl("txtZipCodePlus4");


            //if ( isBillingCentralized == true )
            //{
            txtAttentionOf.Text = attentionOf;
            txtAddressLine1.Text = addressLine1;
            txtAddressLine2.Text = addressLine2;
            txtCity.Text = city;
            state = (state.Length < 2) ? "  " : state;
            cmbState.SelectedValue = state.ToString();
            // Fix this above with below!!!
            ucPracticeLocationShippingAddress.State = state.ToString();

            txtZipCode.Text = zipCode;
            txtZipCodePlus4.Text = zipCodePlus4;
            //}
        }

        protected void GetShippingAddressTextBoxValues()
        {
            // SetRadioButtonListValues( isBillingCentralized );

            //UserControl ucPracticeBillingAddress9 = ( UserControl ) FindControl( "ucPracticeBillingAddress" );

            TextBox txtAttentionOf = (TextBox)ucPracticeLocationShippingAddress.FindControl("txtAttentionOf");
            TextBox txtAddressLine1 = (TextBox)ucPracticeLocationShippingAddress.FindControl("txtAddressLine1");
            TextBox txtAddressLine2 = (TextBox)ucPracticeLocationShippingAddress.FindControl("txtAddressLine2");
            TextBox txtCity = (TextBox)ucPracticeLocationShippingAddress.FindControl("txtCity");

            DropDownList cmbState = (DropDownList)ucPracticeLocationShippingAddress.FindControl("cmbState");

            TextBox txtZipCode = (TextBox)ucPracticeLocationShippingAddress.FindControl("txtZipCode");
            TextBox txtZipCodePlus4 = (TextBox)ucPracticeLocationShippingAddress.FindControl("txtZipCodePlus4");


            attentionOf = txtAttentionOf.Text;
            addressLine1 = txtAddressLine1.Text;
            addressLine2 = txtAddressLine2.Text;
            city = txtCity.Text;
            //state = ( state.Length < 2 ) ? "  " : state;
            state = cmbState.SelectedValue.ToString();
            // ??/ TODO: Fix this above with below!!!
            //state = ucPracticeLocationShippingAddress.State.ToString();

            zipCode = txtZipCode.Text;
            zipCodePlus4 = txtZipCodePlus4.Text;
        }


        protected void GetBillingAddressTextBoxValues()
        {
            // SetRadioButtonListValues( isBillingCentralized );

            //UserControl ucPracticeBillingAddress9 = ( UserControl ) FindControl( "ucPracticeBillingAddress" );

            TextBox txtAttentionOf = (TextBox)ucPracticeLocationBillingAddress.FindControl("txtAttentionOf");
            TextBox txtAddressLine1 = (TextBox)ucPracticeLocationBillingAddress.FindControl("txtAddressLine1");
            TextBox txtAddressLine2 = (TextBox)ucPracticeLocationBillingAddress.FindControl("txtAddressLine2");
            TextBox txtCity = (TextBox)ucPracticeLocationBillingAddress.FindControl("txtCity");

            DropDownList cmbState = (DropDownList)ucPracticeLocationBillingAddress.FindControl("cmbState");

            TextBox txtZipCode = (TextBox)ucPracticeLocationBillingAddress.FindControl("txtZipCode");
            TextBox txtZipCodePlus4 = (TextBox)ucPracticeLocationBillingAddress.FindControl("txtZipCodePlus4");


            attentionOf = txtAttentionOf.Text;
            addressLine1 = txtAddressLine1.Text;
            addressLine2 = txtAddressLine2.Text;
            city = txtCity.Text;
            //state = ( state.Length < 2 ) ? "  " : state;
            state = cmbState.SelectedValue.ToString();
            // ??/ TODO: Fix this above with below!!!
            //state = ucPracticeLocationBillingAddress.State.ToString();

            zipCode = txtZipCode.Text;
            zipCodePlus4 = txtZipCodePlus4.Text;
        }

        protected void SetBillingAddressTextBoxValues()
        {
            // SetRadioButtonListValues( isBillingCentralized );

            //UserControl ucPracticeBillingAddress9 = ( UserControl ) FindControl( "ucPracticeBillingAddress" );

            TextBox txtAttentionOf = (TextBox)ucPracticeLocationBillingAddress.FindControl("txtAttentionOf");
            TextBox txtAddressLine1 = (TextBox)ucPracticeLocationBillingAddress.FindControl("txtAddressLine1");
            TextBox txtAddressLine2 = (TextBox)ucPracticeLocationBillingAddress.FindControl("txtAddressLine2");
            TextBox txtCity = (TextBox)ucPracticeLocationBillingAddress.FindControl("txtCity");

            DropDownList cmbState = (DropDownList)ucPracticeLocationBillingAddress.FindControl("cmbState");

            TextBox txtZipCode = (TextBox)ucPracticeLocationBillingAddress.FindControl("txtZipCode");
            TextBox txtZipCodePlus4 = (TextBox)ucPracticeLocationBillingAddress.FindControl("txtZipCodePlus4");


            //if ( isBillingCentralized == true )
            //{
            txtAttentionOf.Text = attentionOf;
            txtAddressLine1.Text = addressLine1;
            txtAddressLine2.Text = addressLine2;
            txtCity.Text = city;
            if (!string.IsNullOrEmpty(state))
            {
                cmbState.SelectedValue = state.ToString();
                // Fix this above with below!!!
                ucPracticeLocationBillingAddress.State = state.ToString();
            }
            txtZipCode.Text = zipCode;
            txtZipCodePlus4.Text = zipCodePlus4;
            //}
        }

        protected void SaveShippingAddress()
        {
            BLL.PracticeLocation.ShippingAddress bllShippingAddress = new BLL.PracticeLocation.ShippingAddress();
            int practiceLocationID = Convert.ToInt32(Session["PracticeLocationID"].ToString());
            bllShippingAddress.PracticeLocationID = practiceLocationID;
            bllShippingAddress.AddressID = addressID;
            bllShippingAddress.AttentionOf = attentionOf;
            bllShippingAddress.Address.AddressLine1 = addressLine1;
            bllShippingAddress.Address.AddressLine2 = addressLine2;
            bllShippingAddress.Address.City = city;
            bllShippingAddress.Address.State = state;
            bllShippingAddress.Address.ZipCode = zipCode;
            bllShippingAddress.Address.ZipCodePlus4 = zipCodePlus4;

            bllShippingAddress.UpdateInsert(bllShippingAddress);
        }

        protected void SaveBillingAddress()
        {
            BLL.PracticeLocation.BillingAddress bllBillingAddress = new BLL.PracticeLocation.BillingAddress();
            int practiceLocationID = Convert.ToInt32(Session["PracticeLocationID"].ToString());
            bllBillingAddress.PracticeLocationID = practiceLocationID;
            bllBillingAddress.AddressID = addressID;
            bllBillingAddress.AttentionOf = attentionOf;
            bllBillingAddress.Address.AddressLine1 = addressLine1;
            bllBillingAddress.Address.AddressLine2 = addressLine2;
            bllBillingAddress.Address.City = city;
            bllBillingAddress.Address.State = state;
            bllBillingAddress.Address.ZipCode = zipCode;
            bllBillingAddress.Address.ZipCodePlus4 = zipCodePlus4;

            bllBillingAddress.UpdateInsert(bllBillingAddress);

            GetPracticeLocationBillingAddress(practiceLocationID);
            SetBillingAddressTextBoxValues();
        }

        protected bool GetPracticeBillingType(int practiceID)
        {
            int junkaddressID;
            DAL.Practice.BillingAddress dalPBA = new DAL.Practice.BillingAddress();
            //DAL.Practice.BillingAddress dalPBA = new DAL.Practice.BillingAddress();
            dalPBA.GetPracticeBillingAddressID(practiceID, out junkaddressID, out isBillingCentralized);
            return isBillingCentralized;
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            //Load the info from the database if it exists
            // Call the same method that loads the data on the pageload is not post back.
            //  Get the Shipping Address and display

            lblPracticeLocationShippingHeader.Text = practiceLocationName + " Shipping Address";
            lblPracticeLocationBillToHeader.Text = practiceLocationName + " Bill To Address";

            //  Get the Shipping Address and display
            GetPracticeLocationShippingAddress(practiceLocationID);
            //SetShippingAddressTextBoxValues();  the is included in the above method.

            // Get practice Billing type;  either Centralized or Decentrallized.
            isPracticeCentralizedBilling = GetPracticeBillingType(practiceID);

            //  Get the Billing Address and display
            GetPracticeLocationBillingAddress(practiceLocationID);
            SetBillingAddressTextBoxValues();

            if (isPracticeCentralizedBilling)
            {
                // is Centralized, so BillingAddress controls are disable.
                pnlBillingAddress.Enabled = false;
            }
            else
            {
                // is Decentralized, so BillingAddress controls are enable.
                //  Note:Insert/Update BillingAddress is not implemented yet.
                pnlBillingAddress.Enabled = true;
            }
        }




        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveData();
        }

        private void SaveData()
        {
            //Save Shipping
            GetShippingAddressTextBoxValues();
            SaveShippingAddress();

            //Save Billing if enabled.
            if (pnlBillingAddress.Enabled == true)
            {
                GetBillingAddressTextBoxValues();
                SaveBillingAddress();
            }
        }

        // only if Practice Billing is Decentralized and therefore when the pnlBillingAddress is enabled.
        protected void SaveBillingAddress(int practiceLocationID)
        {
            // if the a billing address exists for the practiceLocationID then populate it.

            // if the PL billing address does not yet exist then populate it.
        }

        protected eSetupStatus ValidateSetupData()
        {
            Page.Validate();
            if (Page.IsValid)
            {
                return eSetupStatus.Complete;
            }
            return eSetupStatus.Incomplete;
        }

        /// <summary>
        /// IVisionWizardControl interface save.  Called by wizard control on tab change
        /// </summary>
        public void Save()
        {
            SaveData();

            SaveStatus(practiceID, practiceLocationID, this.ToString(), ValidateSetupData());
        }
    }
}