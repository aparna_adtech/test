﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeLocationAddress.ascx.cs"
  Inherits="BregVision.Admin.UserControls.PracticeLocation.PracticeLocationAddress" %>
<%@ Register Src="~/UserControls/ResetSaveButtonTable.ascx" TagName="ResetSaveButtonTable"
  TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/Address.ascx" TagName="Address" TagPrefix="UC_A" %>

<div class="flex-row actions-bar">
  <h2>Addresses</h2>
  <uc1:ResetSaveButtonTable ID="ucResetSaveButtonTable" runat="server" />
</div>
<div class="flex-row align-start justify-spaceAround">
  <div class="flex-grow">
    <h3><asp:Label ID="lblPracticeLocationShippingHeader" runat="server" Text="Content Header"></asp:Label><bvu:Help ID="help42" runat="server" HelpID="42" /></h3>   
    <asp:Panel ID="pnlShippingAddress" runat="server" Enabled="true">
      <UC_A:Address ID="ucPracticeLocationShippingAddress" runat="server" />
    </asp:Panel>
  </div>
  <div class="flex-grow">
    <h3><asp:Label ID="lblPracticeLocationBillToHeader" runat="server" Text="Content Header"></asp:Label></h3>
    <asp:Panel ID="pnlBillingAddress" runat="server" Enabled="true">
      <UC_A:Address ID="ucPracticeLocationBillingAddress" runat="server" />
    </asp:Panel>
  </div>
</div>

