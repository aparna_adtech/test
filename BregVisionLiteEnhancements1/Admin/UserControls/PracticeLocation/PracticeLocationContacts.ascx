﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeLocationContacts.ascx.cs"
  Inherits="BregVision.Admin.UserControls.PracticeLocation.PracticeLocationContacts" %>
<%@ Register Src="~/UserControls/ResetSaveButtonTable.ascx" TagName="ResetSaveButtonTable"
  TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/Contact.ascx" TagName="Contact" TagPrefix="UC_Contact" %>

<div class="flex-row actions-bar">
   <h2>
    <asp:Label ID="lblContentHeader" runat="server" Text="Location Contact"></asp:Label>
    <bvu:Help ID="help43" runat="server" HelpID="43" />
  </h2> 
  <uc1:ResetSaveButtonTable ID="ucResetSaveButtonTable" runat="server"></uc1:ResetSaveButtonTable>
</div>
<UC_Contact:Contact ID="ucPracticeLocationContact" runat="server" />
