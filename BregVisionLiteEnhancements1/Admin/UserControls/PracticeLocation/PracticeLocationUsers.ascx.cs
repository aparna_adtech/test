﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.Admin.PracticeSetup;

namespace BregVision.Admin.UserControls.PracticeLocation
{
    public partial class PracticeLocationUsers : Bases.UserControlBase, IVisionWizardControl
    {

        /// <summary>
        /// IVisionWizardControl Interface.
        /// </summary>
        public bool NeedsInitialBind { get; set; }

        new protected void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);
            ucResetSaveButtonTable.Reset += new EventHandler(btnReset_Click);
            ucResetSaveButtonTable.Save += new EventHandler(btnSave_Click);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            GetUsersAndLoad(practiceLocationID);

            lblContentHeader.Text = practiceLocationName + " Users";
        }

        protected void GetUsersAndLoad(int practiceLocationID)
        {
            DataTable dtPracticeNonLocationUserFullNames = GetPracticeNonLocationUserFullNames(practiceID);
            PopulateListBoxPracticeNonLocationUsers(dtPracticeNonLocationUserFullNames);

            DataTable dtPracticeLocationUserFullNames = GetPracticeLocationUserFullNames(practiceLocationID);
            PopulateListBoxPracticeLocationUsers(dtPracticeLocationUserFullNames);
        }

        protected DataTable GetPracticeNonLocationUserFullNames(int practiceID)
        {

            BLL.PracticeLocation.User bllPLU = new BLL.PracticeLocation.User();
            DataTable dtPracticeNonLocationUserFullNames = bllPLU.SelectAllNonLocationNames(practiceLocationID); // .SelectAllUserFullNames();
            return dtPracticeNonLocationUserFullNames;

        }

        private DataTable GetPracticeLocationUserFullNames(int practiceLocationID)
        {
            BLL.PracticeLocation.User bllPLU = new BLL.PracticeLocation.User();
            DataTable dtPracticeLocationUser = bllPLU.SelectAllNames(practiceLocationID); // .SelectAllUserFullNames();
            return dtPracticeLocationUser;
        }


        protected void PopulateListBoxPracticeLocationUsers(DataTable dtPracticeLocationUsers)
        {
            listBoxPracticeLocationUsers.DataSource = dtPracticeLocationUsers;
            listBoxPracticeLocationUsers.DataTextField = "UserFullName";
            listBoxPracticeLocationUsers.DataValueField = "UserID";
            listBoxPracticeLocationUsers.DataBind();
        }

        private void PopulateListBoxPracticeNonLocationUsers(DataTable dtPracticeNonLocationUserNames)
        {
            listBoxPracticeNonLocationUsers.DataSource = dtPracticeNonLocationUserNames;
            listBoxPracticeNonLocationUsers.DataTextField = "UserFullName";
            listBoxPracticeNonLocationUsers.DataValueField = "UserID";
            listBoxPracticeNonLocationUsers.DataBind();

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            int x;

            x = listBoxPracticeNonLocationUsers.Items.Count - 1;

            for (int i = x; i >= 0; i--)
            {
                ListItem listItem = new ListItem();
                listItem = listBoxPracticeNonLocationUsers.Items[i];

                if (listItem.Selected)
                {
                    listBoxPracticeLocationUsers.Items.Add(listItem);

                    listBoxPracticeNonLocationUsers.Items[i].Attributes.Add("style", "color:blue");
                    listBoxPracticeNonLocationUsers.Items.Remove(listItem);
                }
            }
        }


        protected void btnRemove_Click(object sender, EventArgs e)
        {
            int x;

            x = listBoxPracticeLocationUsers.Items.Count - 1;

            for (int i = x; i >= 0; i--)
            {
                ListItem listItem = new ListItem();
                listItem = listBoxPracticeLocationUsers.Items[i];

                if (listItem.Selected)
                {
                    listBoxPracticeNonLocationUsers.Items.Add(listItem);
                    listBoxPracticeLocationUsers.Items.Remove(listItem);
                }
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PracticeLocation_All.aspx");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveData();
        }

        private void SaveData()
        {
            AddPracticeLocationUsersToDatabase();

            RemovePracticeLocationUsersFromDatabase();
        }

        protected void AddPracticeLocationUsersToDatabase()
        {
            int countPLUsers = listBoxPracticeLocationUsers.Items.Count;
            int practiceLocationUserID;
            int index = countPLUsers - 1;

            for (int i = index; i >= 0; i--)
            {
                ListItem listItem = new ListItem();
                listItem = listBoxPracticeLocationUsers.Items[i];

                if (listItem.Selected)
                {
                    practiceLocationUserID = Convert.ToInt32(listItem.Value);
                    InsertPracticeLocationUser(practiceLocationUserID);
                }
            }
        }

        protected void RemovePracticeLocationUsersFromDatabase()
        {
            int countNonPLUsers = listBoxPracticeNonLocationUsers.Items.Count;
            int nonPracticeLocationUserID;
            int index = countNonPLUsers - 1;

            for (int i = index; i >= 0; i--)
            {
                ListItem listItem = new ListItem();
                listItem = listBoxPracticeNonLocationUsers.Items[i];

                if (listItem.Selected)
                {
                    nonPracticeLocationUserID = Convert.ToInt32(listItem.Value);
                    DeletePracticeLocationUser(nonPracticeLocationUserID);
                }
            }
        }

        protected void InsertPracticeLocationUser(int userUserID)
        {
            //  If record already exists, do nothing.
            //  If record already exists as inactive the set as active.
            BLL.PracticeLocation.User bllPLU = new BLL.PracticeLocation.User();
            bllPLU.PracticeLocationID = practiceLocationID;
            bllPLU.UserUserID = userUserID;
            bllPLU.UserID = userID;
            bllPLU.Insert(bllPLU);
        }

        protected void DeletePracticeLocationUser(int userUserID)
        {
            // set as inactive.
            BLL.PracticeLocation.User bllPLU = new BLL.PracticeLocation.User();
            bllPLU.PracticeLocationID = practiceLocationID;
            bllPLU.UserUserID = userUserID;
            bllPLU.UserID = userID;
            bllPLU.Delete(bllPLU);
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            GetUsersAndLoad(practiceLocationID);
        }

        /// <summary>
        /// IVisionWizardControl interface save.  Called by wizard control on tab change
        /// </summary>
        public void Save()
        {
            SaveData();
        }
    }
}