﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.Admin.PracticeSetup;

namespace BregVision.Admin.UserControls.PracticeLocation
{
    public partial class PracticeLocationPhysicians : PracticeSetupBase, IVisionWizardControl
    {
        /// <summary>
        /// IVisionWizardControl Interface.
        /// </summary>
        public bool NeedsInitialBind { get; set; }


        new protected void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);
            ucResetSaveButtonTable.Reset += new EventHandler(btnReset_Click);		//new EventHandler( ucResetSaveButtonTable_Reset );
            ucResetSaveButtonTable.Save += new EventHandler(btnSave_Click);		//new EventHandler(ucResetSaveButtonTable_Save);
        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!this.IsPostBack || NeedsInitialBind)
            {
                GetCliniciansAndLoad(practiceLocationID);

                lblContentHeader.Text = practiceLocationName + " Clinicians";
            }
        }


        protected void GetCliniciansAndLoad(int practiceLocationID)
        {
            DataTable dtNonLocationPracticeClinicianNames = GetNonLocationPracticeClinicianNames(practiceLocationID);
            PopulateListBoxNonLocationPracticeClinicians(dtNonLocationPracticeClinicianNames);

            DataTable dtPracticeLocationClinicianNames = GetPracticeLocationClinicianNames(practiceLocationID);
            PopulateListBoxPracticeLocationClinicians(dtPracticeLocationClinicianNames);
        }

        protected DataTable GetNonLocationPracticeClinicianNames(int practiceLocationID)
        {
            BLL.PracticeLocation.Physician bllPLP = new BLL.PracticeLocation.Physician();
            DataTable dtNonPracticeLocationClinician = bllPLP.SelectAllNonLocationNames(practiceLocationID);
            return dtNonPracticeLocationClinician;
        }

        protected DataTable GetPracticeLocationClinicianNames(int practiceLocationID)
        {
            BLL.PracticeLocation.Physician bllPLP = new BLL.PracticeLocation.Physician();
            DataTable dtPracticeLocationClinician = bllPLP.SelectAllNames(practiceLocationID);
            return dtPracticeLocationClinician;
        }


        protected void PopulateListBoxNonLocationPracticeClinicians(DataTable dtNonPracticeLocationClinician)
        {
            listBoxPracticeClinicians.DataSource = dtNonPracticeLocationClinician;
            listBoxPracticeClinicians.DataTextField = "ClinicianName";
            listBoxPracticeClinicians.DataValueField = "ClinicianID";
            listBoxPracticeClinicians.DataBind();
        }

        protected void PopulateListBoxPracticeLocationClinicians(DataTable dtPracticeLocationClinician)
        {
            listBoxPracticeLocationClinicians.DataSource = dtPracticeLocationClinician;
            listBoxPracticeLocationClinicians.DataTextField = "ClinicianName";
            listBoxPracticeLocationClinicians.DataValueField = "ClinicianID";
            listBoxPracticeLocationClinicians.DataBind();
        }


        protected void btnAdd_Click(object sender, EventArgs e)
        {
            int index = listBoxPracticeClinicians.Items.Count - 1;

            for (int i = index; i >= 0; i--)
            {
                ListItem listItem = new ListItem();
                listItem = listBoxPracticeClinicians.Items[i];

                if (listItem.Selected)
                {
                    listBoxPracticeLocationClinicians.Items.Add(listItem);
                    //listBoxPracticePhysicians.Items[i].Attributes.Add( "style", "color:blue" );   //works but is annoying
                    listBoxPracticeClinicians.Items.Remove(listItem);
                }
            }

            if (NeedsInitialBind)
            {
                SaveData();
            }
        }


        protected void btnRemove_Click(object sender, EventArgs e)
        {
            int index = listBoxPracticeLocationClinicians.Items.Count - 1;

            for (int i = index; i >= 0; i--)
            {
                ListItem listItem = new ListItem();
                listItem = listBoxPracticeLocationClinicians.Items[i];

                if (listItem.Selected)
                {
                    listBoxPracticeClinicians.Items.Add(listItem);
                    listBoxPracticeLocationClinicians.Items.Remove(listItem);
                }
            }

            if (NeedsInitialBind)
            {
                SaveData();
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PracticeLocation_All.aspx");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveData();
        }

        private void SaveData()
        {
            AddPracticeLocationPhysiciansToDatabase();

            RemovePracticeLocationPhysiciansFromDatabase();
        }

        protected void AddPracticeLocationPhysiciansToDatabase()
        {
            int countPLPhysicians = listBoxPracticeLocationClinicians.Items.Count;
            int practiceLocationPhysicianID;
            int index = countPLPhysicians - 1;

            for (int i = index; i >= 0; i--)
            {
                ListItem listItem = new ListItem();
                listItem = listBoxPracticeLocationClinicians.Items[i];

                if (listItem.Selected)
                {
                    practiceLocationPhysicianID = Convert.ToInt32(listItem.Value);
                    InsertPracticeLocationPhysician(practiceLocationPhysicianID);
                }
            }
        }

        protected void RemovePracticeLocationPhysiciansFromDatabase()
        {
            int countNonPLPhysicians = listBoxPracticeClinicians.Items.Count;
            int nonPracticeLocationPhysicianID;
            int index = countNonPLPhysicians - 1;

            for (int i = index; i >= 0; i--)
            {
                ListItem listItem = new ListItem();
                listItem = listBoxPracticeClinicians.Items[i];

                if (listItem.Selected)
                {
                    nonPracticeLocationPhysicianID = Convert.ToInt32(listItem.Value);
                    DeletePracticeLocationPhysician(nonPracticeLocationPhysicianID);
                }
            }
        }

        protected void InsertPracticeLocationPhysician(int physicianID)
        {
            //  If record already exists, do nothing.
            //  If record already exists as inactive the set as active.
            BLL.PracticeLocation.Physician bllPLP = new BLL.PracticeLocation.Physician();
            bllPLP.PracticeLocationID = practiceLocationID;
            bllPLP.PhysicianID = physicianID;
            bllPLP.UserID = userID;
            bllPLP.Insert(bllPLP);
        }

        protected void DeletePracticeLocationPhysician(int physicianID)
        {
            // set as inactive.
            BLL.PracticeLocation.Physician bllPLP = new BLL.PracticeLocation.Physician();
            bllPLP.PracticeLocationID = practiceLocationID;
            bllPLP.PhysicianID = physicianID;
            bllPLP.UserID = userID;
            bllPLP.Delete(bllPLP);
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            GetCliniciansAndLoad(practiceLocationID);
        }

        protected eSetupStatus ValidateSetupData()
        {
            Page.Validate();
            if (Page.IsValid)
            {
                if (listBoxPracticeLocationClinicians.Items.Count > 0)
                {
                    return eSetupStatus.Complete;
                }
            }
            return eSetupStatus.Incomplete;
        }

        /// <summary>
        /// IVisionWizardControl interface save.  Called by wizard control on tab change
        /// </summary>
        public void Save()
        {
            SaveData();

            SaveStatus(practiceID, practiceLocationID, this.ToString(), ValidateSetupData());
        }
    }
}