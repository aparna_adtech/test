﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.Admin.PracticeSetup;
using BregVision.UserControls.General;
using Telerik.Web.UI;
using BregWcfService;
using System.Linq;
using BregVision.TelerikAjax;
using BregVision.UserControls;
using BregVision.UserControls.InventoryCount;

namespace BregVision.Admin.UserControls.PracticeLocation
{
    public partial class PracticeLocationInventoryCount : PracticeSetupBase
    {
        
        public RecordsPerPage RecordsPerPage { get; set; }

        public BregVision.UserControls.VisionRadGridAjax InventoryGrid
        {
            get { return grdInventory; }          
        }

        public InventoryCycle InventoryCycleControl
        {
            get
            {
                return ctlInventoryCycle;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            RecordsPerPage.recordsPerPageChangedEvent += new BregVision.UserControls.General.RecordsPerPageEventHandler(ctlRecordsPerPage_recordsPerPageChangedEvent);
            CreateRedRowStyle();

            grdInventory.PageSize = RecordsPerPage.recordsDisplayedPerPage;

            ctlInventoryCycle.InventoryCycleTypeControl = ctlInventoryCycleType;
            ctlInventoryCycle.InventoryCycleEvent += new BregVision.UserControls.InventoryCount.InventoryCycleEventHandler(ctlInventoryCycle_InventoryCycleEvent);


            
        }

        private void CreateRedRowStyle()
        {           
            this.Page.Header.StyleSheet.CreateStyleRule(new VisionRadGridAjax.RedStyle(), this.Page, ".RedRow_Vision");
        }


        void ctlInventoryCycle_InventoryCycleEvent(object sender, BregVision.UserControls.InventoryCount.InventoryCycleEventArgs e)
        {
            //if it is a consignment cycle, hide the parLevel field
            if (ctlInventoryCycle.IsConsignmentCycle() == true)
            {
                grdInventory.MasterTableView.Columns.FindByDataFieldSafe("ParLevel").Visible = false;

                grdInventory.MasterTableView.Columns.FindByDataFieldSafe("ReOrderQuantity").Visible = true;
                grdInventory.MasterTableView.Columns.FindByDataFieldSafe("IsRMA").Visible = true;
                grdInventory.MasterTableView.Columns.FindByDataFieldSafe("IsBuyout").Visible = true;

            }
            else //not consignment
            {
                grdInventory.MasterTableView.Columns.FindByDataFieldSafe("ParLevel").Visible = true;

                grdInventory.MasterTableView.Columns.FindByDataFieldSafe("ReOrderQuantity").Visible = false;
                grdInventory.MasterTableView.Columns.FindByDataFieldSafe("IsRMA").Visible = false;
                grdInventory.MasterTableView.Columns.FindByDataFieldSafe("IsBuyout").Visible = false;
            }
            grdInventory.Rebind();
        }

        void ctlRecordsPerPage_recordsPerPageChangedEvent(object sender, BregVision.UserControls.General.RecordsPerPageEventArgs e)
        {
            grdInventory.PageSize = e.NewRecordsPerPage;
            grdInventory.Rebind();
        }

        protected void lblContentHeader_Load(object s, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var l = s as Label;
                if (l != null)
                    l.Text = practiceLocationName + " Inventory Count";
            }
        }







        protected void btnCloseInventory_Click(object sender, EventArgs e)
        {            

            if (ctlInventoryCycle.CurrentInventoryCycle != null)
            {
                string title = txtTitle.Text;
                string userName = Context.User.Identity.Name;

                UpdateInventoryCounts(grdInventory.MasterTableView);

                



                if (ctlInventoryCycle.IsConsignmentCycle()) //can't close a consignment cycle unless all items are counted.
                {
                    bool cycleReadyForClose = BregWcfService.InventoryDiscrepencyItem.VerifyCycleClosedWithFullCount(practiceLocationID, ctlInventoryCycle.CurrentInventoryCycle, isConsignmentCount: true);

                    if (cycleReadyForClose == false)
                    {
                        lblPCStatus.Text = "You cannot close this cycle.  There are still uncounted items";
                        return;
                    }
                }


                BregWcfService.InventoryDiscrepencyItem.AdjustDiscrepenciesAndCloseCycle(
                                                                            practiceLocationID, 
                                                                            userName, 
                                                                            ctlInventoryCycle.CurrentInventoryCycle, 
                                                                            title, 
                                                                            fromVision: true);


                
                if (userName.ToLower().Contains("testadmin") || userName.ToLower().Contains("klord"))
                {
                    ShowConsignmentBillingEmail();
                }


                lblPCStatus.Text = "Inventory Levels updated.";
                txtTitle.Text = "";

                //reload open inventory Cycles here
                ctlInventoryCycleType.CauseInventoryCycleReload();
                grdInventory.CurrentPageIndex = 0;
                grdInventory.DataSourceID = null;
                grdInventory.DataSource = null;
                grdInventory.Rebind();

            }
        }

        private void ShowConsignmentBillingEmail()
        {
            
#if(DEBUG)
            if (ctlInventoryCycle.IsConsignmentCycle())
            {
                
                string html = BregWcfService.Classes.Email.GetConsignmentEmailHtml(practiceLocationID, true, BregWcfService.EmailTemplates.InventoryComplete.ConsignmentEmailType.ConsignmentInventoryComplete);
                litConsignmentEmail.Text = html;
            }
#endif
        }

        protected void btnUpdateInventory_Click(object sender, EventArgs e)
        {
            UpdateInventoryCounts(grdInventory.MasterTableView);
        }

        private void UpdateInventoryCounts(Telerik.Web.UI.GridTableView gridTableView)
        {
            foreach(Telerik.Web.UI.GridDataItem gridDataItem in gridTableView.Items)
            {
                if (!gridDataItem.Selected)
                    continue;

                InventoryDiscrepencyItemUpdate descrepencyUpdate = new InventoryDiscrepencyItemUpdate();

                descrepencyUpdate.PracticeLocationID = practiceLocationID;
                descrepencyUpdate.UserName = Context.User.Identity.Name;
                descrepencyUpdate.CycleType = ctlInventoryCycle.CurrentInventoryCycle.InventoryCycleType;
                descrepencyUpdate.Cycle = ctlInventoryCycle.CurrentInventoryCycle;

                descrepencyUpdate.ProductInventoryID = gridDataItem.GetColumnInt32("ProductInventoryID");
                descrepencyUpdate.IsCounted = gridDataItem.GetColumnBoolean("IsCounted");

                descrepencyUpdate.InventoryCountInitial = gridDataItem.GetColumnInt32("InventoryCountHdn");
                descrepencyUpdate.InventoryCountRevised = gridDataItem.GetColumnTextInt32("txtInventoryCount");


                if (gridTableView.Columns.FindByUniqueName("IsRMA").Visible == true)
                {
                    descrepencyUpdate.IsBuyout = gridDataItem.GetColumnChecked("chkIsBuyout");
                    descrepencyUpdate.IsRMA = gridDataItem.GetColumnChecked("chkIsRMA");
                }
                InventoryDiscrepencyItem.UpdateInventoryCount(descrepencyUpdate);
            }

            grdInventory.Rebind();
        }




        protected void grdInventory_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                try
                {

                    GridDataItem dataItem = (GridDataItem)e.Item;
                    InventoryDiscrepencyItem discrepencyItem = dataItem.DataItem as InventoryDiscrepencyItem;
                    if (discrepencyItem != null)
                    {
                        if(discrepencyItem.IsCounted == false)
                            dataItem.SetColor(VisionRadGridAjax.GridColor.Red);
                    }
                }
                catch{}
            }
        }

        protected void grdInventory_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            InventoryDiscrepencyItem[] discrepencies = null;

            discrepencies = GetInventoryDiscrepencies();



            if (discrepencies != null )
            {
                //If there are No items counted, then don't show the grid.  It means the cycle was probably just opened.             
                bool currentCycleOpenWithSomeCountedItems = BregWcfService.InventoryDiscrepencyItem.VerifyCycleOpenWithSomeCountedItems(
                                                                                                                                    practiceLocationID,
                                                                                                                                    ctlInventoryCycle.CurrentInventoryCycle,
                                                                                                                                    isConsignmentCount: true);
                
                if (currentCycleOpenWithSomeCountedItems == true)
                {
                    grdInventory.DataSource = discrepencies;
                    btnCloseInventory.Enabled = true;
                    btnCloseInventory1.Enabled = true;
                    btnUpdateInventory.Enabled = true;
                    txtTitle.Enabled = true;
                    return;
                }
            }

            if (discrepencies != null)
            {
                grdInventory.DataSource = discrepencies.Where(a => a.ProductInventoryID == 0).ToArray<InventoryDiscrepencyItem>();
            }
            btnCloseInventory.Enabled = false;
            btnCloseInventory1.Enabled = false;
            btnUpdateInventory.Enabled = false;
            txtTitle.Enabled = false;
        }

        public InventoryDiscrepencyItem[] GetInventoryDiscrepencies(int productInventoryID = 0)
        {
            InventoryDiscrepencyItem[] discrepencies = null;
            ctlInventoryCycle.GetInventoryCycle();

            if (ctlInventoryCycle.CurrentInventoryCycle != null)
            {
                discrepencies = BregWcfService.InventoryDiscrepencyItem.GetInventoryCycleDiscrepencies(practiceLocationID, ctlInventoryCycle.CurrentInventoryCycle, true);
            }
            return discrepencies;
        }

        protected void grdInventory_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            grdInventory.SetExpansionOnPageIndexChanged();
        }

        protected void grdInventory_PreRender(object sender, EventArgs e)
        {
            if (grdInventory.NeedsRebind == true)
            {
                grdInventory.Rebind();
            }

            grdInventory.ExpandGrid();//grdInventory.ExpandGrid(true);
        }

        #region RadInputManager bits

        /// <summary>
        /// Registers a TextBox with a RadInputManager setting. Relies on _GetRadInputManagerNumberSetting()
        /// to retrieve the setting.
        /// 
        /// NOTE: The Load event is fired during postback as well. The _ClearRadInputManagerTagerControls()
        /// method is used to remove registration for TextBoxes that are going to be hidden during a
        /// collapse of a detail table view.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void TextBoxAddToRadInputManager_Load(object source, EventArgs e)
        {
            var t = source as TextBox;
            if (t == null)
                return;

            _GetRadInputManagerNumberSetting()
                .TargetControls
                .Add(new Telerik.Web.UI.TargetInput(t.UniqueID, true));
        }

        /// <summary>
        /// Clears any registered controls that are child items of the provided GridItem.
        /// </summary>
        /// <param name="rowgriditem"></param>
        private void _ClearRadInputManagerTargetControls(Telerik.Web.UI.GridItem rowgriditem)
        {
            var deletelist = new List<Telerik.Web.UI.TargetInput>();
            var targets = _GetRadInputManagerNumberSetting().TargetControls;

            // loop through all nested table views and find targets that
            // have UniqueID values beginning with the table view UniqueID.
            // NOTE: this relies on child nodes always have its parent's ID
            // as a prefix.
            foreach (var views in rowgriditem.OwnerTableView.Items[rowgriditem.ItemIndex].ChildItem.NestedTableViews)
            {
                for (var i = 0; i < targets.Count; i++)
                {
                    var t = targets[i];
                    if (t.ControlID.StartsWith(views.UniqueID))
                        deletelist.Add(t);
                }
            }

            // go through the list to remove and un-register controls
            // from the RadInputManager setting targets list.
            foreach (var t in deletelist)
                targets.Remove(t);
        }

        /// <summary>
        /// Gets the RadInputManager setting with BehaviorID="numberSetting".
        /// </summary>
        /// <returns></returns>
        private Telerik.Web.UI.NumericTextBoxSetting _GetRadInputManagerNumberSetting()
        {
            return
                (Telerik.Web.UI.NumericTextBoxSetting)
                RadInputManager1.GetSettingByBehaviorID("numberSetting");
        }

        /// <summary>
        /// Used to catch a detail table view collapse and trigger removal of
        /// collapsed controls from RadInputManager.
        /// 
        /// NOTE: during PostBack, controls inside a collapsing detail view
        /// are still registered in their Load events. To prevent RadInputManager
        /// from trying to validate against a non-existent control, only visible
        /// controls should be registered.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdInventory_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            // clear controls only when event is triggered by expand/collapse
            // and do it only when collapsing.
            if ((e.CommandName == "ExpandCollapse") && e.Item.Expanded)
                _ClearRadInputManagerTargetControls(e.Item);
        }

        #endregion
    }


}