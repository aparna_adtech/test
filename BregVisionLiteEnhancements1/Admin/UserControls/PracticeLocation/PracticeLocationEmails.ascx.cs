﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.Admin.PracticeSetup;

namespace BregVision.Admin.UserControls.PracticeLocation
{
    public partial class PracticeLocationEmails : PracticeSetupBase, IVisionWizardControl
    {

        //variable for the controls
        string emailForOrderApproval;
        string emailForOrderConfirmation;
        string emailForFaxConfirmation;
        string emailForBillingDispense;
        string emailForLowInventoryAlerts;
        string emailForPrintDispense;
        string faxForPrintDispense;
        bool isEmailForLowInventoryAlertsOn;

        /// <summary>
        /// IVisionWizardControl Interface.
        /// </summary>
        public bool NeedsInitialBind { get; set; }

        new protected void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);
            ucResetSaveButtonTable.Reset += new EventHandler(btnReset_Click);
            ucResetSaveButtonTable.Save += new EventHandler(btnSave_Click);
        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            lblContentHeader.Text = practiceLocationName + " Contact";

            //-------------------Added by u= 09/03/2013 --------------------
            bool bFaxDispenseEnabled = BLL.Practice.Practice.SelectFaxDispenseEnabled(practiceID);
            System.Web.UI.HtmlControls.HtmlTableRow trPrintDispenseFax = (System.Web.UI.HtmlControls.HtmlTableRow)ucPracticeLocationEmail.FindControl("trPrintDispenseFax");

            if (bFaxDispenseEnabled == true) 
            {
                trPrintDispenseFax.Visible = true;
            }
            //--------------------------------------------------------------
            if (!Page.IsPostBack)
            {
                GetEmail(practiceLocationID);
                SetTextBoxValues();
            }
        }

        protected void SetTextBoxValues()
        {
            // Get Controls from inside the User Control
            TextBox txtOrderApprovalEmail = (TextBox)ucPracticeLocationEmail.FindControl("txtOrderApprovalEmail");
            TextBox txtOrderConfirmationEmail = (TextBox)ucPracticeLocationEmail.FindControl("txtOrderConfirmationEmail");
            TextBox txtEmailForBillingDispense = (TextBox)ucPracticeLocationEmail.FindControl("txtEmailForBillingDispense");
            TextBox txtEmailForPrintDispense = (TextBox)ucPracticeLocationEmail.FindControl("txtEmailForPrintDispense");
            TextBox txtFaxForPrintDispense = (TextBox)ucPracticeLocationEmail.FindControl("txtFaxForPrintDispense");

            //Set the values of the controls.
            txtOrderApprovalEmail.Text = emailForOrderApproval;
            txtOrderConfirmationEmail.Text = emailForOrderConfirmation;
            txtEmailForBillingDispense.Text = emailForBillingDispense;
            txtEmailForPrintDispense.Text = emailForPrintDispense;
            txtFaxForPrintDispense.Text = faxForPrintDispense;
        }

        protected void GetTextBoxValues()
        {
            // Get Controls from inside the User Control
            TextBox txtOrderApprovalEmail = (TextBox)ucPracticeLocationEmail.FindControl("txtOrderApprovalEmail");
            TextBox txtOrderConfirmationEmail = (TextBox)ucPracticeLocationEmail.FindControl("txtOrderConfirmationEmail");
            TextBox txtEmailForBillingDispense = (TextBox)ucPracticeLocationEmail.FindControl("txtEmailForBillingDispense");
            TextBox txtEmailForPrintDispense = (TextBox)ucPracticeLocationEmail.FindControl("txtEmailForPrintDispense");
            TextBox txtFaxForPrintDispense = (TextBox)ucPracticeLocationEmail.FindControl("txtFaxForPrintDispense");

            //Get the values of the controls.
            emailForOrderApproval = txtOrderApprovalEmail.Text.ToString();
            emailForOrderConfirmation = txtOrderConfirmationEmail.Text.ToString();
            emailForFaxConfirmation = "";
            emailForBillingDispense = txtEmailForBillingDispense.Text.ToString();
            emailForPrintDispense = txtEmailForPrintDispense.Text.ToString();
            faxForPrintDispense = txtFaxForPrintDispense.Text.ToString();
            emailForLowInventoryAlerts = "";
        }

        protected void GetEmail(int practiceLocationID)
        {
            BLL.PracticeLocation.Email bllPLEmail = new BLL.PracticeLocation.Email();
            bllPLEmail.PracticeLocationID = practiceLocationID;
            bllPLEmail = bllPLEmail.SelectOne(bllPLEmail);

            emailForOrderApproval = bllPLEmail.EmailForOrderApproval;
            emailForOrderConfirmation = bllPLEmail.EmailForOrderConfirmation;
            emailForFaxConfirmation = bllPLEmail.EmailForFaxConfirmation;
            emailForBillingDispense = bllPLEmail.EmailForBillingDispense;
            emailForPrintDispense = bllPLEmail.EmailForPrintDispense;
            faxForPrintDispense = bllPLEmail.FaxForPrintDispense; // added by u= 08/29/2013
            emailForLowInventoryAlerts = bllPLEmail.EmailForLowInventoryAlerts;
            isEmailForLowInventoryAlertsOn = bllPLEmail.IsEmailForLowInventoryAlertsOn;

        }




        protected void btnSave_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
                SaveData();
        }

        private void SaveData()
        {
            GetTextBoxValues();
            UpdateEmail();
        }

        public void UpdateEmail()
        {
            BLL.PracticeLocation.Email bllPLE = new BLL.PracticeLocation.Email();

            bllPLE.PracticeLocationID = practiceLocationID;
            bllPLE.EmailForOrderApproval = emailForOrderApproval;
            bllPLE.EmailForOrderConfirmation = emailForOrderConfirmation;
            bllPLE.EmailForFaxConfirmation = emailForFaxConfirmation;
            bllPLE.EmailForBillingDispense = emailForBillingDispense;
            bllPLE.EmailForPrintDispense = emailForPrintDispense;
            bllPLE.FaxForPrintDispense = faxForPrintDispense;
            bllPLE.EmailForLowInventoryAlerts = emailForLowInventoryAlerts;
            bllPLE.IsEmailForLowInventoryAlertsOn = isEmailForLowInventoryAlertsOn;
            bllPLE.UserID = userID;

            bllPLE.Update(bllPLE);
        }


        protected void btnReset_Click(object sender, EventArgs e)
        {
            GetEmail(practiceLocationID);
            SetTextBoxValues();
        }

        protected eSetupStatus ValidateSetupData()
        {
            Page.Validate();
            if (Page.IsValid)
            {
                return eSetupStatus.Complete;
            }
            return eSetupStatus.Incomplete;
        }

        /// <summary>
        /// IVisionWizardControl interface save.  Called by wizard control on tab change
        /// </summary>
        public void Save()
        {
            SaveData();

            SaveStatus(practiceID, practiceLocationID, this.ToString(), ValidateSetupData());
        }
    }
}