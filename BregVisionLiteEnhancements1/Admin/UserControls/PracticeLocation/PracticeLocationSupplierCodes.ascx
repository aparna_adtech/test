﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeLocationSupplierCodes.ascx.cs" Inherits="BregVision.Admin.UserControls.PracticeLocation.PracticeLocationSupplierCodes" %>
<%@ Register Src="~/UserControls/ResetSaveButtonTable.ascx" TagName="ResetSaveButtonTable" TagPrefix="uc1" %>

  <h2>
    <asp:Label ID="lblContentHeader" runat="server" Text="Content Header"></asp:Label>
    <bvu:Help ID="help52" runat="server" HelpID="52"/>
  </h2>
<br/>
<asp:GridView ID="GridAccountCode" runat="server" AutoGenerateColumns="False" DataKeyNames="practiceLocationID, PracticeCatalogSupplierBrandID, IsInDB, AccountCode"
              OnRowCancelingEdit="GridAccountCode_RowCancelingEdit"
              OnRowCommand="GridAccountCode_RowCommand"
              OnRowEditing="GridAccountCode_RowEditing"
              OnRowUpdating="GridAccountCode_RowUpdating"
              OnRowDataBound="GridAccountCode_RowDataBound"
              ShowFooter="True" CssClass="RadGrid_Breg">
  <Columns>
    <asp:TemplateField ShowHeader="False">
      <EditItemTemplate>
        <asp:ImageButton ID="ImageButtonEditUpdate" runat="server" CausesValidation="True" CommandName="Update" ImageUrl="~/App_Themes/Breg/images/Common/checkCircle.png" ToolTip="Update"/>&nbsp;
        <asp:ImageButton ID="ImageButtonEditCancel" runat="server" CausesValidation="False" CommandName="Cancel" ImageUrl="~/App_Themes/Breg/images/Common/cancelCircle.png" ToolTip="Cancel"/>
      </EditItemTemplate>
      <ItemTemplate>
        <asp:ImageButton ID="ImageButtonEdit" runat="server" CausesValidation="False" CommandName="Edit" ImageUrl="~/App_Themes/Breg/images/Common/edit.png" ToolTip="Edit"/>
      </ItemTemplate>
      <HeaderTemplate>
        <bvu:Help ID="help12" runat="server" HelpID="12"/>
      </HeaderTemplate>
    </asp:TemplateField>
    <asp:BoundField DataField="PracticeLocationID, PracticeCatalogSupplierBrandID" Visible="False"/>
    <asp:TemplateField ConvertEmptyStringToNull="False" InsertVisible="False" Visible="False">
      <EditItemTemplate>
        <asp:Label ID="txtPracticeLocationIDEdit" runat="server" Text='<%# Bind("PracticeLocationID") %>' Visible="false">
        </asp:Label>
      </EditItemTemplate>
      <ItemTemplate>
        <asp:Label ID="lblPracticeLocationID" runat="server" Text='<%# Bind("PracticeLocationID") %>' Visible="false">
        </asp:Label>
      </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField ConvertEmptyStringToNull="False" InsertVisible="False" Visible="False">
      <EditItemTemplate>
        <asp:Label ID="txtPracticeCatalogSupplierBrandIDEdit" runat="server" Text='<%# Bind("PracticeCatalogSupplierBrandID") %>' Visible="false">
        </asp:Label>
      </EditItemTemplate>
      <ItemTemplate>
        <asp:Label ID="lblPracticeCatalogSupplierBrandID" runat="server" Text='<%# Bind("PracticeCatalogSupplierBrandID") %>' Visible="false">
        </asp:Label>
      </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField HeaderText="Supplier">
      <EditItemTemplate>
        <asp:Label ID="lblSupplierShortNameEdit" runat="server" Text='<%# Bind("SupplierShortName") %>'>
        </asp:Label>
      </EditItemTemplate>
      <ItemTemplate>
        <asp:Label ID="lblSupplierShortName" runat="server" Text='<%# Bind("SupplierShortName") %>'>
        </asp:Label>
      </ItemTemplate>
      <HeaderTemplate>
        <asp:Label ID="lblContentHeader" runat="server" Text="Supplier"></asp:Label>
      </HeaderTemplate>
    </asp:TemplateField>
    <asp:TemplateField HeaderText="Account Number">
      <EditItemTemplate>
        <asp:TextBox ID="txtAccountCode" runat="server" MaxLength="50" Text='<%# Bind("AccountCode") %>'>
        </asp:TextBox>
      </EditItemTemplate>
      <ItemTemplate>
        <asp:Label ID="lblAccountCode" runat="server" Text='<%# Bind("AccountCode") %>'>
        </asp:Label>
      </ItemTemplate>
      <HeaderTemplate>
        <asp:Label ID="lblContentHeader2" runat="server" Text="Account Number"></asp:Label>
        <bvu:Help ID="help53" runat="server" HelpID="53"/>
      </HeaderTemplate>
    </asp:TemplateField>
    <asp:TemplateField ConvertEmptyStringToNull="False" InsertVisible="False" Visible="False">
      <EditItemTemplate>
        <asp:Label ID="lblIsInDBEdit" runat="server" Text='<%# Bind("IsInDB") %>' Visible="false">
        </asp:Label>
      </EditItemTemplate>
      <ItemTemplate>
        <asp:Label ID="lblIsInDB" runat="server" Text='<%# Bind("IsInDB") %>' Visible="false">
        </asp:Label>
      </ItemTemplate>
    </asp:TemplateField>
  </Columns>
  <HeaderStyle CssClass="AdminSubTitle"/>
</asp:GridView>