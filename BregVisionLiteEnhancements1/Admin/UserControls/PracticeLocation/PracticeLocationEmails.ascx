﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeLocationEmails.ascx.cs"
  Inherits="BregVision.Admin.UserControls.PracticeLocation.PracticeLocationEmails" %>
<%@ Register Src="~/UserControls/ResetSaveButtonTable.ascx" TagName="ResetSaveButtonTable"
  TagPrefix="uc2" %>
<%@ Register Src="~/Admin/UserControls/PracticeLocationEmail.ascx" TagName="PracticeLocationEmail"
  TagPrefix="uc1" %>
<div class="flex-row actions-bar">
<h2>
  <asp:Label ID="lblContentHeader" runat="server" Text="Content Header"></asp:Label>
  <bvu:Help ID="help47" runat="server" HelpID="47" />
</h2>
  <uc2:ResetSaveButtonTable ID="ucResetSaveButtonTable" runat="server" />
</div>
<br />
<uc1:PracticeLocationEmail ID="ucPracticeLocationEmail" runat="server" />


