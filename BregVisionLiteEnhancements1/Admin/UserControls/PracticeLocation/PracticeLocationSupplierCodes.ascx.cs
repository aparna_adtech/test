﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.Admin.PracticeSetup;

namespace BregVision.Admin.UserControls.PracticeLocation
{
    public partial class PracticeLocationSupplierCodes : PracticeSetupBase, IVisionWizardControl
    {
        string _AccountCode = "";

        /// <summary>
        /// IVisionWizardControl Interface.
        /// </summary>
        public bool NeedsInitialBind { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!this.IsPostBack || NeedsInitialBind)
            {
                lblContentHeader.Text = practiceLocationName + " Supplier Account Numbers";

                //DataTable dt = GetAccountCodes( practiceLocationID );
                BindGridAccountCode();
               
            }
        }

        private void BindGridAccountCode()
        {
            DataTable dtSupplierCodes = GetAccountCodes(practiceLocationID);

            if (dtSupplierCodes.Rows.Count > 0)
            {
                GridAccountCode.DataSource = dtSupplierCodes;
                GridAccountCode.DataBind();

                if (dtSupplierCodes.Rows.Count == 1)
                {
                    DataRow[] rows = dtSupplierCodes.Select("SupplierShortName='breg'");
                    if (rows.GetUpperBound(0) == 0)
                    {
                        string accountCode = rows[0].Field<string>("AccountCode");
                        if (string.IsNullOrEmpty(accountCode))
                        {
                            _AccountCode = BLL.Practice.PracticeApplication.GetBregAccountNumber(practiceID);
                            GridAccountCode.EditIndex = 0;
                        }
                    }

                }
            }
            else  //No records found - insert a display row so the GridView will display.
            {
                // Sorting Works but is commented out for now.
                //GridAccountCode.AllowSorting  = false;
                DisplayNoRecordsFoundRow(dtSupplierCodes);
            }
        }

        private DataTable GetAccountCodes(int practiceLocationID)
        {
            DAL.PracticeLocation.SupplierCode dalPLSupplierCode = new DAL.PracticeLocation.SupplierCode();
            DataTable dt = dalPLSupplierCode.SelectAll(practiceLocationID);
            return dt;
        }

        //  Display "No Records Found" when the grid's data source has now records.
        protected void DisplayNoRecordsFoundRow(DataTable dtSupplierCodes)
        {
            dtSupplierCodes.Rows.Add(dtSupplierCodes.NewRow());
            GridAccountCode.DataSource = dtSupplierCodes;
            GridAccountCode.DataBind();

            int totalColumns = GridAccountCode.Rows[0].Cells.Count;
            GridAccountCode.Rows[0].Cells.Clear();
            GridAccountCode.Rows[0].Cells.Add(new TableCell());
            GridAccountCode.Rows[0].Cells[0].ColumnSpan = totalColumns;
            GridAccountCode.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
            GridAccountCode.Rows[0].Cells[0].Text = "No Records Found";
        }


        // Update the edited row.
        protected void GridAccountCode_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            //////int practiceID = Convert.ToInt32( GridAccountCode.DataKeys[e.RowIndex].Values[0].ToString() );

            int practiceLocationID = Convert.ToInt32(GridAccountCode.DataKeys[e.RowIndex].Values[0].ToString());
            int practiceCatalogSupplierBrandID = Convert.ToInt32(GridAccountCode.DataKeys[e.RowIndex].Values[1].ToString());
            int isInDBEdit = Convert.ToInt32(GridAccountCode.DataKeys[e.RowIndex].Values[2].ToString());  //lblIsInDBEdit
            Label lblisInDBEdit = (Label)GridAccountCode.Rows[e.RowIndex].FindControl("lblisInDBEdit");
            TextBox txtAccountCode = (TextBox)GridAccountCode.Rows[e.RowIndex].FindControl("txtAccountCode");
            //  Label lblAccountCode = (TextBox)GridAccountCode.Rows[e.RowIndex].FindControl("txtSupplierShortName");
            string accountCode = txtAccountCode.Text.ToString().Trim();

            DAL.PracticeLocation.SupplierCode dalPLSupplierCode = new DAL.PracticeLocation.SupplierCode();

            if (isInDBEdit == 0)
            {
                dalPLSupplierCode.Insert(practiceLocationID, practiceCatalogSupplierBrandID, accountCode, userID);
                // Insert into the database.
            }

            if (isInDBEdit == 1)
            {
                dalPLSupplierCode.Update(practiceLocationID, practiceCatalogSupplierBrandID, accountCode, userID);
            }

            // Set Edit Mode back to Read Only.
            GridAccountCode.EditIndex = -1;
            BindGridAccountCode();

        }

        //  User Clicked Edit, so allow the row to be edited.
        protected void GridAccountCode_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridAccountCode.EditIndex = e.NewEditIndex;
            BindGridAccountCode();
        }

        //  User Canceled the Row Edit, so remove the editing from the row.
        protected void GridAccountCode_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridAccountCode.EditIndex = -1;
            BindGridAccountCode();
        }



        //  User click Delete so Delete the Practice.
        public void GridAccountCode_RowDeleting(Object sender, GridViewDeleteEventArgs e)
        {
            int practiceLocationID = Convert.ToInt32(GridAccountCode.DataKeys[e.RowIndex].Values[0].ToString());
            int practiceCatalogSupplierBrandID = Convert.ToInt32(GridAccountCode.DataKeys[e.RowIndex].Values[1].ToString());
            DAL.PracticeLocation.SupplierCode dalPLSupplierCode = new DAL.PracticeLocation.SupplierCode();
            dalPLSupplierCode.Delete(practiceLocationID, practiceCatalogSupplierBrandID, userID);
        }

        // User Clicked "AddNew" or "Insert" or "Cancel" (ed) the AddNew.
        protected void GridAccountCode_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            
        }

        protected void GridAccountCode_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Label lblSupplierShortName = e.Row.FindControl("lblSupplierShortNameEdit") as Label;
            if (lblSupplierShortName != null )
            {
                if (lblSupplierShortName.Text.Trim().ToLower() == "breg")
                {
                    TextBox txtAccountCode = e.Row.FindControl("txtAccountCode") as TextBox;
                    if (string.IsNullOrEmpty(txtAccountCode.Text.Trim()))
                    {                        
                        if (txtAccountCode != null)
                        {
                            txtAccountCode.Text = _AccountCode;
                        }
                         
                    }
                }
            }
        }

        protected eSetupStatus ValidateSetupData()
        {
            return eSetupStatus.Complete;
        }

        /// <summary>
        /// IVisionWizardControl interface save.  Called by wizard control on tab change
        /// </summary>
        public void Save()
        {
            //Do nothing.  Each table row is saved by the grid

            SaveStatus(practiceID, practiceLocationID, this.ToString(), ValidateSetupData());
        }
    }
}