﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeLocationUsers.ascx.cs" Inherits="BregVision.Admin.UserControls.PracticeLocation.PracticeLocationUsers" %>
<%@ Register Src="~/UserControls/ResetSaveButtonTable.ascx" TagName="ResetSaveButtonTable" TagPrefix="uc1" %>

<table border="1" cellpadding="0" cellspacing="0" style="background-color: white"
    summary="Summary Description" width="600px">
    <tr>
        <td>
            <table border="0" cellpadding="0" cellspacing="0" style="width: auto; text-align: center;">
                <!-- Content Header-->
                <tr>
                    <td class="AdminPageTitle">
                        <asp:Label ID="lblContentHeader" runat="server" Text="Location Users"></asp:Label>
                    </td>
                </tr>
                <!-- Grid -->
                <tr style="font-size: 11pt">
                    <td style="width: auto">
                        <!-- Content Header   table, tr, and td are not neccessary here  -->
                        <table border="0" cellpadding="0" cellspacing="0">
                            <%--<tr>
                                                    <td colspan="3">
                                                        <asp:Label ID="lblPracticeLocationUsers" runat="server" Font-Bold="True" Font-Size="Larger"></asp:Label>
                                                    </td>
                                                </tr>--%>
                            <tr>
                                <td align="center" class="AdminSubTitle">
                                    Practice Non Location Users
                                </td>
                                <td>
                                </td>
                                <td align="center" class="AdminSubTitle">
                                    Practice Location Users
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="width: 225px;">
                                    <asp:ListBox ID="listBoxPracticeNonLocationUsers" runat="server" Height="280px" SelectionMode="Multiple"
                                        Width="220px"></asp:ListBox>
                                </td>
                                <td align="center" style="width: 150px;">
                                    <table>
                                        <tr align="center">
                                            <td>
                                                <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="Add >>" Width="100px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr align="center">
                                            <td>
                                                <asp:Button ID="btnRemove" runat="server" OnClick="btnRemove_Click" Text="<< Remove"
                                                    Width="100px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="width: 225px">
                                    <asp:ListBox ID="listBoxPracticeLocationUsers" runat="server" Height="280px" SelectionMode="Multiple"
                                        Width="220px"></asp:ListBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="height: 35px" valign="bottom">
                    <td style="width: auto">
                        <!-- Buttons -->
                        <uc1:ResetSaveButtonTable ID="ucResetSaveButtonTable" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
