﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.Admin.PracticeSetup;
using Microsoft.Practices.EnterpriseLibrary.Data;
using BregVision.TelerikAjax;
using BregVision.UserControls.General;
using ClassLibrary.DAL;
using Telerik.Web.Data.Extensions;
using Telerik.Web.UI;

namespace BregVision.Admin.UserControls.PracticeLocation
{
    public partial class PracticeLocationDispensementModifications : PracticeSetupBase, IVisionWizardControl
    {

        /// <summary>
        /// IVisionWizardControl Interface.
        /// </summary>
        public bool NeedsInitialBind { get; set; }

        new protected void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);

            ctlSearchToolbar.searchEvent += new SearchEventHandler(ctlSearchToolbar_searchEvent);
            if (this.Page is Dispense)
            {
                //ctlRecordsPerPage.Visible = false;
                //ctlSearchToolbar.DefaultLoadingPanel = ((Dispense)this.Page).DefaultLoadingPanel;
            }
            else
            {
                //ctlRecordsPerPage.recordsPerPageChangedEvent += new RecordsPerPageEventHandler(ctlRecordsPerPage_recordsPerPageChangedEvent);
            }

            ctlSearchToolbar.Grid = grdPracticeCatalog;
            //

            ctlSearchToolbar.ClearDropdownButtons();
            ctlSearchToolbar.AddItem("Product Code", "ProductCode");
            ctlSearchToolbar.AddItem("Patient Code", "PatientCode");
            ctlSearchToolbar.AddItem("Patient Name", "PatientName");

            ctlSearchToolbar.AddItem("Provider Name", "PhysicianName");
            ctlSearchToolbar.AddItem("Date Dispensed", "DispenseDate");
            ctlSearchToolbar.AddItem("Claim Number", "BCSClaimID");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();
            grdPracticeCatalog.SetGridLabel("", false, lblPCStatus, true);//set the default text on the label
        }

        void ctlRecordsPerPage_recordsPerPageChangedEvent(object sender, RecordsPerPageEventArgs e)
        {
            grdPracticeCatalog.Rebind();
        }

        private void InitializeAjaxSettings()
        {
            // get ajax manager object, exit if not found
            var radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);
            if (radAjaxManager == null)
                return;

            // get loading panel for page
            var loadingPanel = (RadAjaxLoadingPanel)null;
            if (this.Page is Dispense)
                loadingPanel = ((Dispense)this.Page).DefaultLoadingPanel;
            else if (this.Page is Admin.PracticeLocation_DispensementModifications)
                loadingPanel = ((Admin.PracticeLocation_DispensementModifications)this.Page).DefaultLoadingPanel;

            // add ajax manager settings
            radAjaxManager.AjaxSettings.AddAjaxSetting(grdPracticeCatalog, grdPracticeCatalog, loadingPanel);
            radAjaxManager.AjaxSettings.AddAjaxSetting(ctlSearchToolbar.Toolbar, grdPracticeCatalog, loadingPanel);
            radAjaxManager.AjaxSettings.AddAjaxSetting(grdPracticeCatalog, lblPCStatus);
            radAjaxManager.AjaxSettings.AddAjaxSetting(ctlSearchToolbar.Toolbar, lblPCStatus);
        }

        void ctlSearchToolbar_searchEvent(object sender, SearchEventArgs e)
        {
            grdPracticeCatalog.SearchText = e.SearchText;
            grdPracticeCatalog.SearchCriteria = e.SearchCriteria;
            grdPracticeCatalog.SearchType = e.SearchType;
            grdPracticeCatalog.SetSearchCriteria(e.SearchType);

            lblPCStatus.Text = "";

            grdPracticeCatalog.Visible = true;
        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var p = this.Page as PracticeLocation_DispensementModifications;
                if (p != null)
                    p.HeaderLabel.Text = string.Format("{0} Dispensement Modification", practiceLocationName);
            }
        }

        protected void grdPracticeCatalog_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {

            string searchCriteria = grdPracticeCatalog.SearchCriteria;
            string searchText = grdPracticeCatalog.SearchText;

            if (!e.IsFromDetailTable)
            {
                grdPracticeCatalog.DataSource = DAL.PracticeLocation.Supplier.GetPracticeCatalogSuppliers(practiceLocationID, searchCriteria.ToString(), searchText.ToString());
            }

            grdPracticeCatalog.PageSize = recordsDisplayedPerPage;
            grdPracticeCatalog.Visible = true;
        }


        protected void grdPracticeCatalog_ItemCommand(object source, GridCommandEventArgs e)
        {
            //  If the update (edit) command is being called, clear all insert items.
            if (e.CommandName == RadGrid.EditCommandName)
            {
                e.Item.OwnerTableView.IsItemInserted = false;
                e.Item.OwnerTableView.Caption = "";
            }
        }

        protected void grdPracticeCatalog_ItemDataBound(object source, GridItemEventArgs e)
        {

            if ((e.Item is GridEditFormItem) && (e.Item.IsInEditMode))
            {

                GridEditableItem editItem = e.Item as GridEditableItem;
                RadComboBox ddlDevice = (RadComboBox)(editItem.FindControl("cboPhysicianName"));
                ddlDevice.Text = DataBinder.Eval(e.Item.DataItem, "PhysicianName").ToString();
                ddlDevice.SelectedValue = DataBinder.Eval(e.Item.DataItem, "PhysicianID").ToString();

                RadComboBox locations = (RadComboBox)(editItem.FindControl("cboItemLocation"));
                locations.Text = practiceLocationName;
                locations.SelectedValue = practiceLocationID.ToString();
                //var btnDeleteDispenseQueueItem = e.Item.GetControl<ImageButton>("btnDeleteDispenseQueueItem");


            }
        }


        protected void grdPracticeCatalog_UpdateCommand(object source, GridCommandEventArgs e)
        {

            // Header for message display to user.
            string messageHeader;

            if (e.CommandName == RadGrid.UpdateCommandName)  //"Update"
            {
                //Get the GridEditableItem of the RadGrid  
                GridEditableItem editedItem = e.Item as GridEditableItem;
               

                string patientCode;
                DateTime dateDispensed;
                Int32 physicianValue;
                decimal dmeDeposit;
                Int32 DispenseID;
                Int32 DispenseDetailID;
                string patientEmail;
                string sideModifier;
                int dispenseLocationId;
                bool isUpdateInventoryChecked;
                string mod1;
                string mod2;
                string mod3;
                decimal acb;

                GetValuesFromUpdateForm(editedItem, out patientCode, out dateDispensed,
                    out physicianValue, out dmeDeposit, out DispenseID, out DispenseDetailID, out patientEmail, out sideModifier, out dispenseLocationId, 
                    out isUpdateInventoryChecked, out acb, out mod1, out mod2, out mod3);

                try
                {
                    //Update Record
                    DAL.PracticeLocation.Dispensement.UpdateDispensementRecords(DispenseID, DispenseDetailID, patientCode, dateDispensed, physicianValue, dmeDeposit,patientEmail, sideModifier, isUpdateInventoryChecked, dispenseLocationId, practiceLocationID, userID, acb, mod1, mod2, mod3);

                    grdPracticeCatalog.Rebind();

                    lblPCStatus.Enabled = true;
                    lblPCStatus.Text = "Record updated.";
                    lblPCStatus.CssClass = "SuccessMsg";
                    lblPCStatus.Visible = true;

                }
                catch (Exception ex)
                {
                    grdPracticeCatalog.Controls.Add(new LiteralControl("Unable to update product. Reason: " + ex.Message));
                    e.Canceled = true;

                    // Display message to user.
                    messageHeader = "Unable to update product. Reason: " + ex.Message;
                    ////e.Item.OwnerTableView.Caption = SetProductCaptionMessage(messageHeader, productName, productCode, packaging, side, size, gender, color, wholesaleCost);
                    //DisplayMessage(messageHeader);
                }

            }
        }
        protected void GetValuesFromUpdateForm(GridEditableItem editedItem, out string patientCode, out DateTime dateDispensed, out int physicianValue, out decimal dmeDeposit, out int DispenseID, out int DispenseDetailID, out string patientEmail, out string sideModifier, out int itemLocationID, out bool isUpdateInventoryChecked, out decimal acb, out string mod1, out string mod2, out string mod3)
        {
            patientCode = (editedItem.FindControl("txtPatientCode") as TextBox).Text.Trim();
            dateDispensed = Convert.ToDateTime((editedItem.FindControl("txtDateDispensed") as RadDatePicker).SelectedDate.ToString());
            physicianValue = Convert.ToInt32((editedItem.FindControl("cboPhysicianName") as RadComboBox).SelectedValue.ToString());
            //dmeDeposit = (editedItem.FindControl("txtDMEDeposit") as TextBox).Text.Trim();

            patientEmail = (editedItem.FindControl("txtPatientEmail") as TextBox).Text.Trim();
            sideModifier = (editedItem.FindControl("cboSide") as DropDownList).SelectedValue;
            mod1 = (editedItem.FindControl("ddlmod1") as DropDownList).SelectedValue;
            mod2 = (editedItem.FindControl("ddlmod2") as DropDownList).SelectedValue;
            mod3 = (editedItem.FindControl("ddlmod3") as DropDownList).SelectedValue;
            itemLocationID = Convert.ToInt32((editedItem.FindControl("cboItemLocation") as RadComboBox).SelectedValue);
            isUpdateInventoryChecked = (editedItem.FindControl("chkUpdateInventory") as CheckBox).Checked;



            dmeDeposit = 0;
            RadNumericTextBox textboxdmeDeposit = (RadNumericTextBox)editedItem.FindControl("txtDMEDeposit");
            if (textboxdmeDeposit.Text.Trim().Length > 0)
            {
                dmeDeposit = Convert.ToDecimal(textboxdmeDeposit.Text.Replace("$", "").Replace(",", "").Trim());
            }
            acb = 0;
            RadNumericTextBox textboxAcb= (RadNumericTextBox)editedItem.FindControl("txtACB");
            if (textboxAcb.Text.Trim().Length > 0)
            {
                acb = Convert.ToDecimal(textboxAcb.Text.Replace("$", "").Replace(",", "").Trim());
            }

            DispenseID = Convert.ToInt32((editedItem.FindControl("txtDispenseID") as TextBox).Text.Trim());
            DispenseDetailID = Convert.ToInt32((editedItem.FindControl("txtDispenseDetailID") as TextBox).Text.Trim());


        }

        //protected void grdPracticeCatalog_DeleteCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        //{
        //    //delete item
        //    if (e.Item is GridDataItem)
        //    {
        //        GridDataItem dataItem = e.Item as GridDataItem;
        //        Int32 DispenseID = Int32.Parse(dataItem["DispenseID"].Text);
        //        Int32 DispenseDetailID = Int32.Parse(dataItem["DispenseDetailID"].Text);
        //        Int32 Quantity = Int32.Parse(dataItem["Quantity"].Text);

        //        //Next line calls function to delete item from cart
        //        int deleteStatus = DAL.PracticeLocation.Dispensement.DeleteItemfromDispensement(DispenseID, DispenseDetailID, Quantity);

        //        grdPracticeCatalog.Rebind();

        //        if (deleteStatus == 0)  //Succuss
        //            lblPCStatus.Text = "Dispensement deleted.<br>Item added back into inventory.";
        //        else if (deleteStatus > 0)
        //            lblPCStatus.Text = "Dispensement partially deleted. Other items still in dispensement record.<br>Total(s) adjusted to reflect deleted dispensement.";
        //        else
        //            lblPCStatus.Text = "Unable to modify dispensement.";
        //    }
        //}

        //this is called from a binder in the ascx
        public DataSet dsPopulatePhysicians()
        {
            return DAL.PracticeLocation.Physician.PopulatePhysicians(practiceLocationID);
        }


        protected eSetupStatus ValidateSetupData()
        {
            return eSetupStatus.Complete;
        }

        /// <summary>
        /// IVisionWizardControl interface save.  Called by wizard control on tab change
        /// </summary>
        public void Save()
        {
            //do nothing.  Grid rows are saved individually

            SaveStatus(practiceID, practiceLocationID, this.ToString(), ValidateSetupData());
        }

        protected DataSet dsPopulateLocations()
        {
            using (var db = VisionDataContext.GetVisionDataContext())
            {
                var locations = db.PracticeLocations.Where(loc => loc.PracticeID == practiceID && loc.IsActive);
                var command = (SqlCommand) db.GetCommand(locations);
                var adapter = new SqlDataAdapter(command);
                var dataset = new DataSet();
                adapter.Fill(dataset);
                return dataset;

            }
        }

        protected void OpenDeleteDispensementWindow(object sender, EventArgs e)
        {
            //Set Command Arguments to private parameters for dispense delete
            ImageButton imgBtn = sender as ImageButton;
            string[] commandArgs = imgBtn.CommandArgument.ToString().Split(new char[] { ',' });
            Session["DispenseItemDispenseIDToDelete"]  = commandArgs[0];
            Session["DispenseItemDispenseDetailIDToDelete"] = commandArgs[1];
            Session["DispenseItemQuantityToDelete"] = commandArgs[2];

            //Open Rad window to show delete reason options
            string script = "function f(){$find(\"" + deleteQueueReason.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
        }

        protected void DeleteDispensement_Click(object sender, EventArgs e)
        {
            Int32 DispenseID = Int32.Parse(Session["DispenseItemDispenseIDToDelete"].ToString());
            Int32 DispenseDetailID = Int32.Parse(Session["DispenseItemDispenseDetailIDToDelete"].ToString());
            Int32 Quantity = Int32.Parse(Session["DispenseItemQuantityToDelete"].ToString());
                
            int deleteStatus = DAL.PracticeLocation.Dispensement.DeleteItemfromDispensement(DispenseID, DispenseDetailID, Quantity, GetSelectedDeleteReasonValue());

            grdPracticeCatalog.Rebind();
            if (deleteStatus == 0)
            {
                //Success
                lblPCStatus.Text = "Dispensement deleted.<br>Item added back into inventory.";
                lblPCStatus.CssClass = "SuccessMsg";
            }
            else if (deleteStatus > 0)
                lblPCStatus.Text =
                    "Dispensement partially deleted. Other items still in dispensement record.<br>Total(s) adjusted to reflect deleted dispensement.";
            else
            {
                lblPCStatus.Text = "Unable to modify dispensement.";
                lblPCStatus.CssClass = "ErrorMsg";
            }


        }

        private string GetSelectedDeleteReasonValue()
        {
            if (rbPatientReturnProduct.Checked)
            {
                rbPatientReturnProduct.Checked = false;
                return rbPatientReturnProduct.Text;
            }
            else if (rbDefectiveProduct.Checked)
            {
                rbDefectiveProduct.Checked = false;
                return rbDefectiveProduct.Text;
            }
            else if (rbDoubleDispensement.Checked)
            {
                rbDoubleDispensement.Checked = false;
                return rbDoubleDispensement.Text;
            }
            else
            {
                rbOther.Checked = false;
                tbDeleteReasonOther.Text = "";
                return tbDeleteReasonOther.Text;
            }
        }
    }
}