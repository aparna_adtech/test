﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeLocationDispensementModifications.ascx.cs" Inherits="BregVision.Admin.UserControls.PracticeLocation.PracticeLocationDispensementModifications" %>
<telerik:RadWindowManager runat="server" RestrictionZoneID="offsetElement" ID="RadwindowManagerDispenseModification">
  <Windows>
    <telerik:RadWindow ID="deleteQueueReason" runat="server" Modal="True" VisibleOnPageLoad="false" ReloadOnShow="true"
         Behaviors="Close" VisibleStatusbar="false" Height="280" Width="500" Top="60" Left="560" Skin="Breg" EnableEmbeddedSkins="False">
      <ContentTemplate>
        <h3 class="no-wrap">Please select the reason for deleting this item</h3>
        <div>
          <telerik:RadButton runat="server" ID="rbPatientReturnProduct" GroupName="DeleteReasonValue" ButtonType="ToggleButton" ToggleType="Radio" AutoPostBack="false" Text="Patient Return Product" />
          <telerik:RadButton runat="server" ID="rbDefectiveProduct" GroupName="DeleteReasonValue" ButtonType="ToggleButton" ToggleType="Radio" AutoPostBack="false" Text="Defective Product" />
          <telerik:RadButton runat="server" ID="rbDoubleDispensement" GroupName="DeleteReasonValue" ButtonType="ToggleButton" ToggleType="Radio" AutoPostBack="false" Text="Double Dispensement" />
          <div>
            <div class="half">
              <telerik:RadButton runat="server" ID="rbOther" GroupName="DeleteReasonValue" ButtonType="ToggleButton" ToggleType="Radio" AutoPostBack="false" Text="Other" />
            </div>
            <div class="half">
              <asp:TextBox Visible="true" runat="server" ID="tbDeleteReasonOther" />
            </div>
          </div>
        </div>
        <br />
        <div class="buttons-container">
          <asp:Button runat="server" OnClientClick="void(0);" CssClass="button-accent" Text="Cancel" />
          <asp:Button runat="server" ID="btnConfirmDeleteQueueItem" CssClass="button-primary" Text="Delete" OnClick="DeleteDispensement_Click" />
        </div>
      </ContentTemplate>
    </telerik:RadWindow>
  </Windows>
</telerik:RadWindowManager>
<div class="flex-row actions-bar no-max-width">
  <bvu:SearchToolbar ID="ctlSearchToolbar" runat="server" />
  <div class="flex">&nbsp;</div>
</div>
<div>
  <asp:Label ID="lblPCStatus" CssClass="WarningMsg" runat="server" />
  <asp:Label ID="lblDeleteMessage" CssClass="WarningMsg" runat="server" />
</div>
<div class="no-max-width">
  <bvu:VisionRadGridAjax ID="grdPracticeCatalog" runat="server" AllowMultiRowSelection="True" GridLines="None" Visible="True" OnNeedDataSource="grdPracticeCatalog_NeedDataSource" OnItemCommand="grdPracticeCatalog_ItemCommand" OnItemDataBound="grdPracticeCatalog_ItemDataBound" OnUpdateCommand="grdPracticeCatalog_UpdateCommand" Width="100%" Skin="Breg" EnableEmbeddedSkins="False" AddNonBreakingSpace="False" CssClass="no-max-width">
    <PagerStyle Mode="NumericPages" AlwaysVisible="true" />
    <MasterTableView>
      <Columns>
        <telerik:GridTemplateColumn>
          <ItemTemplate>
            <asp:ImageButton ID="deleteQueueReason" CommandArgument='<%# Eval("DispenseID") + ", " + Eval("DispenseDetailID") + ", " + Eval("Quantity") %> ' runat="server" ImageUrl="~/App_Themes/Breg/images/Common/delete.png" OnClick="OpenDeleteDispensementWindow" />
          </ItemTemplate>
        </telerik:GridTemplateColumn>
        <telerik:GridButtonColumn ButtonType="ImageButton" UniqueName="btnEdit" CommandName="Edit" ImageUrl="~/App_Themes/Breg/images/Common/edit.png" ItemStyle-HorizontalAlign="Center" />
        <telerik:GridBoundColumn DataField="DispenseID" UniqueName="DispenseID" Display="False" />
        <telerik:GridBoundColumn DataField="DispenseDetailID" UniqueName="DispenseDetailID" Display="False" />
        <telerik:GridBoundColumn DataField="Quantity" UniqueName="Quantity" Display="False" />
        <telerik:GridTemplateColumn HeaderText="Dispensement Item" HeaderStyle-HorizontalAlign="Center">
          <ItemTemplate>
            <div style="font-weight: bold; white-space: normal;">
              <%# Eval("BrandShortName") %> - <%# Eval("Product") %> - <%# Eval("Code") %><!-- add hcpc here -->
            </div>
            <table class="DispenseModificationTable">
              <tr>
                <td>
                  <div class="text-gray">
                    <small>Size</small>
                  </div>
                  <%# Eval("Size") %>
                        
                </td>
                <td>
                  <div class="text-gray">
                    <small>Claim Number</small>
                  </div>
                  <%# Eval("BCSClaimID") %>
                        
                </td>
                <td>
                  <div class="text-gray">
                    <small>Side</small>
                  </div>
                  <%# Eval("Side") %>
                </td>
                <td>
                  <div class="text-gray">
                    <small>Mod</small>
                  </div>
                  <%# Eval("Mod1") %>
                </td>
                <td>
                  <div class="text-gray">
                    <small>Mod</small>
                  </div>
                  <%# Eval("Mod2") %>
                </td>
                <td>
                  <div class="text-gray">
                    <small>Mod</small>
                  </div>
                  <%# Eval("Mod3") %>
                </td>
                <td>
                  <div class="text-gray">
                    <small>Gender</small>
                  </div>
                  <%# Eval("Gender") %>
                </td>
                <td>
                  <div class="text-gray">
                    <small>Color</small>
                  </div>
                  <!-- add color field here -->
                </td>
                <td>
                  <div class="text-gray">
                    <small>Pkg</small>
                  </div>
                  <%# Eval("Packaging") %>
                </td>
                <td>
                  <div class="text-gray">
                    <small>Date Dispensed</small>
                  </div>
                  <%# Eval("DateDispensed", "{0:MM/d/yyyy}") %>
                </td>
                <td>
                  <div class="text-gray">
                    <small>Provider Name</small>
                  </div>
                  <%# Eval("PhysicianName") %>
                </td>
                <td>
                  <div class="text-gray">
                    <small>Patient Code</small>
                  </div>
                  <%# Eval("PatientCode") %>
                </td>
                <td>
                  <div class="text-gray">
                    <small>Patient Name</small>
                  </div>
                  <%# Eval("PatientFirstName").ToString() + ' ' + Eval("PatientLastName").ToString() %>
                </td>
                <td>
                  <div class="text-gray">
                    <small>ACB</small>
                  </div>
                  <%# Eval("ActualChargeBilled", "{0:$###,##0.00}") %>
                </td>
                <td>
                  <div class="text-gray">
                    <small>DME Deposit</small>
                  </div>
                  <%# Eval("DMEDeposit", "{0:$###,##0.00}") %>
                </td>
                <td>
                  <div class="text-gray">
                    <small>Total</small>
                  </div>
                  <%# Eval("Total", "{0:$###,##0.00}") %>
                </td>
                <td>
                  <div class="text-gray">
                    <small>Line Total</small>
                  </div>
                  <%# Eval("LineTotal", "{0:$###,##0.00}") %>
                </td>
                <td>
                  <div class="text-gray">
                    <small>Qty</small>
                  </div>
                  <%# Eval("Quantity") %>
                </td>
              </tr>
            </table>
          </ItemTemplate>
        </telerik:GridTemplateColumn>
      </Columns>
      <EditFormSettings EditFormType="Template">
        <EditColumn UniqueName="EditCommandColumn1"></EditColumn>
        <FormTemplate>
          <table>
            <!-- old buttons -->
            <tr>
              <asp:TextBox ID="txtDispenseID" Text='<%# Eval("DispenseID") %>' runat="server" Visible="false" />
              <asp:TextBox ID="txtDispenseDetailID" Text='<%# Eval("DispenseDetailID") %>' runat="server" Visible="false" />
              <asp:TextBox ID="txtQuantity" Text='<%# Eval("Quantity") %>' runat="server" Visible="false" />
              <td>Patient Code</td>
              <td>
                <asp:TextBox ID="txtPatientCode" Text='<%# Eval("PatientCode") %>' runat="server" MaxLength="50" /><%--                                                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator_txtProduct" runat="server"                                                                     Text="*" Font-Overline="true"    ControlToValidate="txtPatientCode" SetFocusOnError="true" ErrorMessage="Product is a required field." />--%>
              </td>
              <td>Patient Email</td>
              <td>
                <asp:TextBox ID="txtPatientEmail" Text='<%# Eval("PatientEmail") %>' runat="server" MaxLength="50" />
              </td>
              <td>Side</td>
              <td colspan="3">
                <div class="Dropdown-Container">
                  <asp:DropDownList ID="cboSide" runat="server" CssClass="TextXXSml" SelectedValue='<%# Eval("Side") %>'>
                    <asp:ListItem Text="U" Value="U"></asp:ListItem>
                    <asp:ListItem Text="LT" Value="L"></asp:ListItem>
                    <asp:ListItem Text="RT" Value="R"></asp:ListItem>
                    <asp:ListItem Text="BI" Value="BI"></asp:ListItem>
                  </asp:DropDownList>
                </div>
              </td>
              <td>ACB
              </td>
              <td>
                <telerik:RadNumericTextBox runat="server" ID="txtACB" Text='<%# Eval("ActualChargeBilled") %>'
                   ShowSpinButtons="False" Culture="English (United States)" LabelCssClass="radLabelCss_Default"
                  MaxValue="9999" MinValue="0" Skin="Web20" Width="100px" Type="Currency"
                  NumberFormat-DecimalDigits="2">
                  <NumberFormat DecimalDigits="2" />
                </telerik:RadNumericTextBox>
              </td>
              <td>Date Dispensed
              </td>
              <td>
                <telerik:RadDatePicker ID="txtDateDispensed" runat="server" SelectedDate='<%# Eval("DateDispensed") %>' DateInput-DisplayDateFormat="MM/dd/yy"
                  ToolTip="Dates can be entered in the following formats(01012007,01/01/2007,January 1,2007)">
                  <DateInput runat="server" DisplayDateFormat="MM/dd/yy" />
                  <Calendar runat="server" ShowRowHeaders="false" ImagesPath="~/App_Themes/Breg/images/Common/" />
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator_txtCode" runat="server" Text="*"
                  ControlToValidate="txtDateDispensed" SetFocusOnError="true" ErrorMessage="Date is a required field." />
              </td>
            </tr>
            <tr>
              <td>Provider Name</td>
              <td>
                <telerik:RadComboBox ID="cboPhysicianName" runat="server" Skin="Breg" EnableEmbeddedSkins="False" DataSource='<%# dsPopulatePhysicians() %>' DataTextField='ClinicianName' DataValueField='ClinicianID'></telerik:RadComboBox>
              </td>
              <td>Dispense Location</td>
              <td nowrap>
                <telerik:RadComboBox ID="cboItemLocation" runat="server" Skin="Breg" EnableEmbeddedSkins="False" DataSource='<%# dsPopulateLocations() %>' DataTextField="Name" DataValueField="PracticeLocationId" ExpandEffect="Fade" ShowWhileLoading="False" ToolTip="Select a practice location to change from where the item was dispensed." />
              </td>
              <td>Modifiers
              </td>
              <td colspan="3">
                <div class="third">
                  <div class="Dropdown-Container">
                    <asp:DropDownList ID="ddlMod2" runat="server" CssClass="TextXXSml" SelectedValue='<%# Eval("Mod2") %>'>
                      <asp:ListItem Text="" Value=""></asp:ListItem>
                      <asp:ListItem Text="CG" Value="CG"></asp:ListItem>
                      <asp:ListItem Text="EY" Value="EY"></asp:ListItem>
                      <asp:ListItem Text="GA" Value="GA"></asp:ListItem>
                      <asp:ListItem Text="GX" Value="GX"></asp:ListItem>
                      <asp:ListItem Text="GY" Value="GY"></asp:ListItem>
                      <asp:ListItem Text="GZ" Value="GZ"></asp:ListItem>
                      <asp:ListItem Text="KV" Value="KV"></asp:ListItem>
                      <asp:ListItem Text="KX" Value="KX"></asp:ListItem>
                      <asp:ListItem Text="NU" Value="NU"></asp:ListItem>
                      <asp:ListItem Text="RA" Value="RA"></asp:ListItem>
                      <asp:ListItem Text="RB" Value="RB"></asp:ListItem>
                      <asp:ListItem Text="RR" Value="RR"></asp:ListItem>
                    </asp:DropDownList>
                  </div>
                </div>
                <div class="third">
                  <div class="Dropdown-Container">
                    <asp:DropDownList ID="ddlMod1" runat="server" CssClass="TextXXSml" SelectedValue='<%# Eval("Mod1") %>'>
                      <asp:ListItem Text="" Value=""></asp:ListItem>
                      <asp:ListItem Text="CG" Value="CG"></asp:ListItem>
                      <asp:ListItem Text="EY" Value="EY"></asp:ListItem>
                      <asp:ListItem Text="GA" Value="GA"></asp:ListItem>
                      <asp:ListItem Text="GX" Value="GX"></asp:ListItem>
                      <asp:ListItem Text="GY" Value="GY"></asp:ListItem>
                      <asp:ListItem Text="GZ" Value="GZ"></asp:ListItem>
                      <asp:ListItem Text="KV" Value="KV"></asp:ListItem>
                      <asp:ListItem Text="KX" Value="KX"></asp:ListItem>
                      <asp:ListItem Text="NU" Value="NU"></asp:ListItem>
                      <asp:ListItem Text="RA" Value="RA"></asp:ListItem>
                      <asp:ListItem Text="RB" Value="RB"></asp:ListItem>
                      <asp:ListItem Text="RR" Value="RR"></asp:ListItem>
                    </asp:DropDownList>
                  </div>
                </div>
                <div class="third">
                  <div class="Dropdown-Container">
                    <asp:DropDownList ID="ddlMod3" runat="server" CssClass="TextXXSml" SelectedValue='<%# Eval("Mod3") %>'>
                      <asp:ListItem Text="" Value=""></asp:ListItem>
                      <asp:ListItem Text="CG" Value="CG"></asp:ListItem>
                      <asp:ListItem Text="EY" Value="EY"></asp:ListItem>
                      <asp:ListItem Text="GA" Value="GA"></asp:ListItem>
                      <asp:ListItem Text="GX" Value="GX"></asp:ListItem>
                      <asp:ListItem Text="GY" Value="GY"></asp:ListItem>
                      <asp:ListItem Text="GZ" Value="GZ"></asp:ListItem>
                      <asp:ListItem Text="KV" Value="KV"></asp:ListItem>
                      <asp:ListItem Text="KX" Value="KX"></asp:ListItem>
                      <asp:ListItem Text="NU" Value="NU"></asp:ListItem>
                      <asp:ListItem Text="RA" Value="RA"></asp:ListItem>
                      <asp:ListItem Text="RB" Value="RB"></asp:ListItem>
                      <asp:ListItem Text="RR" Value="RR"></asp:ListItem>
                    </asp:DropDownList>
                  </div>
                </div>
              </td>
              <td>DME Deposit</td>
              <td>
                <telerik:RadNumericTextBox runat="server" ID="txtDMEDeposit" Text='<%# Eval("DMEDeposit") %>'  ShowSpinButtons="False" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="9999" MinValue="0" Skin="Breg" EnableEmbeddedSkins="False" Type="Currency" NumberFormat-DecimalDigits="2">
                  <NumberFormat DecimalDigits="2" />
                </telerik:RadNumericTextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Text="*" ControlToValidate="txtDMEDeposit" SetFocusOnError="true" ErrorMessage="DME Deposit is a required field." />
              </td>
              <td>
                <asp:CheckBox ID="chkUpdateInventory" runat="server" ToolTip="By checking this box, the product  quantity will be removed from the new location selected for this dispensement and replaced in the originally dispensed location" /></td>
              <td>&nbsp;Update Inventory
              </td>
            </tr>
            <tr>
              <td colspan="12">
                <div class="flex-row justify-end">
                  <div>
                    <asp:Button ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False" CommandName="Cancel" CssClass="button-accent" />
                    <asp:Button ID="btnUpdate" Text='Update' runat="server" CommandName='Update' ToolTip="Contact Billing Department if changes made will affect billing information." CssClass="button-primary"></asp:Button>
                  </div>
                </div>
              </td>

            </tr>
            <tr>
              <td colspan="12">
                <asp:Label ID="lblUserMessage" Text='' ForeColor="Red" runat="server" />
                <asp:ValidationSummary ID="validationSummary" HeaderText="Please correct the following:" Visible="true" runat="server" />
              </td>
            </tr>

          </table>
        </FormTemplate>
      </EditFormSettings>
    </MasterTableView>
  </bvu:VisionRadGridAjax>
</div>
