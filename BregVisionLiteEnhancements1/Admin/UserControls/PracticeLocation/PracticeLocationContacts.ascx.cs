﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.Admin.PracticeSetup;
using Telerik.Web.UI;

namespace BregVision.Admin.UserControls.PracticeLocation
{
    public partial class PracticeLocationContacts : PracticeSetupBase, IVisionWizardControl
    {

        int contactID;
        string title = "";
        string salutation = "";
        string firstName = "";
        string middleName = "";
        string lastName = "";
        string suffix = "";
        string emailAddress = "";
        string phoneWork = "";
        string phoneWorkExtension = "";
        string phoneCell = "";
        string phoneHome = "";
        string fax = "";
        private bool isSpecificContact;

        /// <summary>
        /// IVisionWizardControl Interface.
        /// </summary>
        public bool NeedsInitialBind { get; set; }


        //The code in this page is a little deceiving.  It appears to be a standard crud page but is in fact saving a new record on each change,
        //and maintaining a history.  On the save, the new contact ID is saved in the practiceLocation table so it knows how to find the last 
        //saved contact when it loads.


        new protected void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);
            ucResetSaveButtonTable.Reset += new EventHandler(btnReset_Click);		//new EventHandler( ucResetSaveButtonTable_Reset );
            ucResetSaveButtonTable.Save += new EventHandler(btnSave_Click);		//new EventHandler(ucResetSaveButtonTable_Save);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();
        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            lblContentHeader.Text = practiceLocationName + " Contact";

            GetContact(practiceLocationID);
            SetTextBoxesValues();
        }

        private void InitializeAjaxSettings()
        {
            Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);
            if (radAjaxManager != null)
            {
                radAjaxManager.AjaxSettings.AddAjaxSetting(ucResetSaveButtonTable.btnSaveButton, this);
                radAjaxManager.AjaxSettings.AddAjaxSetting(ucResetSaveButtonTable.btnResetButton, this);
            }
        }


        protected void GetContact(int practiceLocationID)
        {
            BLL.PracticeLocation.Contact bllPLContact = new BLL.PracticeLocation.Contact();
            bllPLContact = bllPLContact.Select(practiceLocationID);

            if (bllPLContact.ContactID > 0)   //Always true;
            {
                contactID = bllPLContact.ContactID;
                title = bllPLContact.Title;
                salutation = bllPLContact.Salutation;
                firstName = bllPLContact.FirstName;
                middleName = bllPLContact.MiddleName;
                lastName = bllPLContact.LastName;
                suffix = bllPLContact.Suffix;
                emailAddress = bllPLContact.EmailAddress;
                phoneWork = bllPLContact.PhoneWork;
                phoneWorkExtension = bllPLContact.PhoneWorkExtension;
                phoneCell = bllPLContact.PhoneCell;
                phoneHome = bllPLContact.PhoneHome;
                fax = bllPLContact.Fax;
                isSpecificContact = bllPLContact.IsSpecificContact;

            }
        }


        protected int GetContactId(int practiceLocationID)
        {
            BLL.PracticeLocation.Contact bllPLContact = new BLL.PracticeLocation.Contact();
            bllPLContact = bllPLContact.Select(practiceLocationID);
            return bllPLContact.ContactID;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveData();
        }

        private void SaveData()
        {
            GetTextBoxValues();
            contactID = GetContactId(practiceLocationID);

            if (contactID > 0)
            {
                UpdateContact(contactID, title, salutation, firstName, middleName, lastName, suffix, emailAddress, phoneWork, phoneWorkExtension, phoneCell, phoneHome, fax, userID, isSpecificContact);
            }
            else
            {
                AddContact(out contactID, title, salutation, firstName, middleName, lastName, suffix, phoneWork, phoneWorkExtension, phoneCell, phoneHome, fax, userID, isSpecificContact);
            }
        }

        void GetTextBoxValues()
        {
            // Get Controls from inside the User Control
            TextBox txtTitle = (TextBox)ucPracticeLocationContact.FindControl("txtTitle");

            DropDownList ddlSalutation = (DropDownList)ucPracticeLocationContact.FindControl("ddlSalutation");

            TextBox txtFirstName = (TextBox)ucPracticeLocationContact.FindControl("txtFirstName");
            TextBox txtMiddleName = (TextBox)ucPracticeLocationContact.FindControl("txtMiddleName");
            TextBox txtLastName = (TextBox)ucPracticeLocationContact.FindControl("txtLastName");

            DropDownList ddlSuffix = (DropDownList)ucPracticeLocationContact.FindControl("ddlSuffix");

            TextBox txtEmailAddress = (TextBox)ucPracticeLocationContact.FindControl("txtEmailAddress");

            title = txtTitle.Text.ToString();
            salutation = ddlSalutation.SelectedItem.Text.ToString();
            firstName = txtFirstName.Text.ToString();
            middleName = txtMiddleName.Text.ToString();
            lastName = txtLastName.Text.ToString();
            suffix = ddlSuffix.SelectedItem.Text.ToString();
            emailAddress = txtEmailAddress.Text.ToString();

            RadMaskedTextBox txtPhoneWork = (RadMaskedTextBox)ucPracticeLocationContact.FindControl("txtPhoneWork");
            RadMaskedTextBox txtPhoneWorkExtension = (RadMaskedTextBox)ucPracticeLocationContact.FindControl("txtPhoneWorkExtension");
            RadMaskedTextBox txtPhoneCell = (RadMaskedTextBox)ucPracticeLocationContact.FindControl("txtPhoneCell");
            RadMaskedTextBox txtPhoneHome = (RadMaskedTextBox)ucPracticeLocationContact.FindControl("txtPhoneHome");
            RadMaskedTextBox txtFax = (RadMaskedTextBox)ucPracticeLocationContact.FindControl("txtFax");


            phoneWork = txtPhoneWork.Text.ToString();
            phoneWorkExtension = txtPhoneWorkExtension.Text.ToString();
            phoneCell = txtPhoneCell.Text.ToString();
            phoneHome = txtPhoneHome.Text.ToString();
            fax = txtFax.Text.ToString();

            RadButton chkIsSpecificContact = (RadButton)ucPracticeLocationContact.FindControl("chkIsSpecificContact");
            isSpecificContact = chkIsSpecificContact.Checked;
        }

        void SetTextBoxesValues()
        {
            // Get Controls from inside the User Control
            TextBox txtTitle = (TextBox)ucPracticeLocationContact.FindControl("txtTitle");

            DropDownList ddlSalutation = (DropDownList)ucPracticeLocationContact.FindControl("ddlSalutation");

            TextBox txtFirstName = (TextBox)ucPracticeLocationContact.FindControl("txtFirstName");
            TextBox txtMiddleName = (TextBox)ucPracticeLocationContact.FindControl("txtMiddleName");
            TextBox txtLastName = (TextBox)ucPracticeLocationContact.FindControl("txtLastName");

            DropDownList ddlSuffix = (DropDownList)ucPracticeLocationContact.FindControl("ddlSuffix");

            TextBox txtEmailAddress = (TextBox)ucPracticeLocationContact.FindControl("txtEmailAddress");


            txtTitle.Text = title;

            ddlSalutation.ClearSelection();
            ddlSalutation.Items.FindByText(salutation).Selected = true;

            txtFirstName.Text = firstName;
            txtMiddleName.Text = middleName;
            txtLastName.Text = lastName;

            ddlSuffix.ClearSelection();
            ddlSuffix.Items.FindByText(suffix).Selected = true;

            txtEmailAddress.Text = emailAddress;

            RadMaskedTextBox txtPhoneWork = (RadMaskedTextBox)ucPracticeLocationContact.FindControl("txtPhoneWork");
            RadMaskedTextBox txtPhoneWorkExtension = (RadMaskedTextBox)ucPracticeLocationContact.FindControl("txtPhoneWorkExtension");
            RadMaskedTextBox txtPhoneCell = (RadMaskedTextBox)ucPracticeLocationContact.FindControl("txtPhoneCell");
            RadMaskedTextBox txtPhoneHome = (RadMaskedTextBox)ucPracticeLocationContact.FindControl("txtPhoneHome");
            RadMaskedTextBox txtFax = (RadMaskedTextBox)ucPracticeLocationContact.FindControl("txtFax");


            txtPhoneWork.Text = phoneWork.ToString();
            txtPhoneWorkExtension.Text = phoneWorkExtension.ToString();
            txtPhoneCell.Text = phoneCell.ToString();
            txtPhoneHome.Text = phoneHome.ToString();
            txtFax.Text = fax.ToString();

            RadButton chkIsSpecificContact = (RadButton)ucPracticeLocationContact.FindControl("chkIsSpecificContact");
            chkIsSpecificContact.Checked= isSpecificContact;
        }

        public void AddContact(out int newContactID, string title, string salutation, string firstName, string middleName, string lastName, string suffix, string phoneWork, string phoneWorkExtension, string phoneCell, string phoneHome, string fax, int createdUserID, bool isSpecificContact)
        {
            BLL.PracticeLocation.Contact bllPLContact = new BLL.PracticeLocation.Contact();

            bllPLContact.PracticeLocationID = practiceLocationID;
            bllPLContact.ContactID = contactID;
            bllPLContact.Title = title;
            bllPLContact.Salutation = salutation;
            bllPLContact.FirstName = firstName;
            bllPLContact.MiddleName = middleName;
            bllPLContact.LastName = lastName;
            bllPLContact.Suffix = suffix;
            bllPLContact.EmailAddress = emailAddress;
            bllPLContact.PhoneWork = phoneWork;
            bllPLContact.PhoneWorkExtension = phoneWorkExtension;
            bllPLContact.PhoneCell = phoneCell;
            bllPLContact.PhoneHome = phoneHome;
            bllPLContact.Fax = fax;
            bllPLContact.UserID = userID;
            bllPLContact.IsSpecificContact = isSpecificContact;

            newContactID = bllPLContact.Insert(bllPLContact);   //.Create( title, salutation, firstName, middleName, lastName, suffix, emailAddress, phoneWork, phoneCell, phoneHome, fax, userID );
        }


        public void UpdateContact(int contactID, string title, string salutation, string firstName, string middleName, string lastName, string suffix, string emailAddress, string phoneWork, string phoneWorkExtension, string phoneCell, string phoneHome, string fax, int userID, bool isSpecificContact)
        {
            BLL.PracticeLocation.Contact bllPLContact = new BLL.PracticeLocation.Contact();

            bllPLContact.ContactID = contactID;
            bllPLContact.Title = title;
            bllPLContact.Salutation = salutation;
            bllPLContact.FirstName = firstName;
            bllPLContact.MiddleName = middleName;
            bllPLContact.LastName = lastName;
            bllPLContact.Suffix = suffix;
            bllPLContact.EmailAddress = emailAddress;
            bllPLContact.PhoneWork = phoneWork;
            bllPLContact.PhoneWorkExtension = phoneWorkExtension;
            bllPLContact.PhoneCell = phoneCell;
            bllPLContact.PhoneHome = phoneHome;
            bllPLContact.Fax = fax;
            bllPLContact.IsSpecificContact = isSpecificContact;
            bllPLContact.UserID = userID;

            bllPLContact.Update(bllPLContact);
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PracticeLocation_All.aspx");
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            GetContact(practiceLocationID);
            SetTextBoxesValues();
        }

        protected eSetupStatus ValidateSetupData()
        {
            Page.Validate();
            if (Page.IsValid)
            {
                return eSetupStatus.Complete;
            }
            return eSetupStatus.Incomplete;
        }

        /// <summary>
        /// IVisionWizardControl interface save.  Called by wizard control on tab change
        /// </summary>
        public void Save()
        {
            SaveData();

            SaveStatus(practiceID, practiceLocationID, this.ToString(), ValidateSetupData());
        }
    }
}