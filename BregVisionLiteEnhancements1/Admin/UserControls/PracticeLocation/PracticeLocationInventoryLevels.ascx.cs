﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.Admin.PracticeSetup;
using BregVision.Domain.DispenseHistory.Models;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.WebControls;
using BregVision.TelerikAjax;

namespace BregVision.Admin.UserControls.PracticeLocation
{
    public partial class PracticeLocationInventoryLevels : PracticeSetupBase, IVisionWizardControl
    {
        int intPCItems = 0;
        bool isGrouping = false;
        int productCount = 50;

        /// <summary>
        /// IVisionWizardControl Interface.
        /// </summary>
        public bool NeedsInitialBind { get; set; }


        #region Page Events

        new protected void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);

            ProductSearch.Grid = grdPracticeCatalog;
            ProductSearch.DefaultLoadingPanel = RadAjaxLoadingPanel1;
            ProductSearch.searchEvent += new global::BregVision.UserControls.General.SearchEventHandler(ProductSearch_searchEvent);
            ProductSearch.ClearDropdownButtons();
            ProductSearch.AddItem("Product Code", "Code");
            ProductSearch.AddItem("Product Name", "Name");
            ProductSearch.AddItem("Manufacturer", "Brand");
            ProductSearch.AddItem("HCPCs", "HCPCs");

            //Show the consignment columns for bregAdmins

            grdInventory.MasterTableView.DetailTables[0].Columns.FindByUniqueNameSafe("IsConsignment").Visible = Context.User.IsInRole("BregAdmin");
            grdInventory.MasterTableView.DetailTables[0].Columns.FindByUniqueNameSafe("ConsignmentQuantity").Visible = Context.User.IsInRole("BregAdmin");
            grdInventory.MasterTableView.DetailTables[0].Columns.FindByUniqueNameSafe("ConsignmentLockParLevel").Visible = Context.User.IsInRole("BregAdmin");

            InventorySearch.Grid = grdInventory;
            InventorySearch.DefaultLoadingPanel = RadAjaxLoadingPanel1;
            InventorySearch.searchEvent += new global::BregVision.UserControls.General.SearchEventHandler(InventorySearch_searchEvent);
            InventorySearch.ClearDropdownButtons();
            InventorySearch.AddItem("Product Code", "Code");
            InventorySearch.AddItem("Product Name", "Name");
            InventorySearch.AddItem("Manufacturer", "Brand");
            InventorySearch.AddItem("HCPCs", "HCPCs");

            //Telerik.Web.UI.RadScriptManager.GetCurrent(this.Page).RegisterPostBackControl(btnUpdateInventory);
        }

        protected void chkConsignmentLockParLevelAll_OnInit(object sender, EventArgs e)
        {

            CheckBox chkConsignmentLockParLevelAll = sender as CheckBox;
            if (chkConsignmentLockParLevelAll != null)
            {
                chkConsignmentLockParLevelAll.Visible = Context.User.IsInRole("BregAdmin");
            }
        }

        protected void lblContentHeader_Load(object s, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var l = s as Label;
                if (l != null)
                    l.Text = practiceLocationName + " Inventory Level";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();
        }

        private void InitializeAjaxSettings()
        {
            var radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);
            if (radAjaxManager != null)
            {
                radAjaxManager.AjaxSettings.AddAjaxSetting(ProductSearch.Toolbar, grdPracticeCatalog, RadAjaxLoadingPanel1);
                radAjaxManager.AjaxSettings.AddAjaxSetting(ProductSearch.Toolbar, lblMCStatus);

                radAjaxManager.AjaxSettings.AddAjaxSetting(InventorySearch.Toolbar, grdInventory, RadAjaxLoadingPanel1);
                radAjaxManager.AjaxSettings.AddAjaxSetting(InventorySearch.Toolbar, RadInputManager1);
                radAjaxManager.AjaxSettings.AddAjaxSetting(InventorySearch.Toolbar, lblPCStatus);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (PracticeLocationIDChanged || grdPracticeCatalog.HasInitiallyBound == false)
            {
                grdPracticeCatalog.SetGridLabel("", false, lblMCStatus, true);//set the default text on the label
                grdInventory.SetGridLabel("", false, lblPCStatus, true);//set the default text on the label

                txtRecordsDisplayed.Text = recordsDisplayedPerPage.ToString();
                grdInventory.PageSize = recordsDisplayedPerPage;
                grdPracticeCatalog.PageSize = recordsDisplayedPerPage;

                grdInventory.InitializeSearch();
                grdPracticeCatalog.InitializeSearch();
            }
        }




        void ProductSearch_searchEvent(object sender, global::BregVision.UserControls.General.SearchEventArgs e)
        {
            //if (e.SearchType == "Search")
            //{
            //    grdPracticeCatalog.MasterTableView.HierarchyLoadMode = Telerik.Web.UI.GridChildLoadMode.ServerBind;
            //}
            //else
            //{
            //    grdPracticeCatalog.MasterTableView.HierarchyLoadMode = Telerik.Web.UI.GridChildLoadMode.ServerOnDemand;
            //}

            grdPracticeCatalog.SearchText = e.SearchText;
            grdPracticeCatalog.SearchCriteria = e.SearchCriteria;
            grdPracticeCatalog.SearchType = e.SearchType;
            grdPracticeCatalog.SetSearchCriteria(e.SearchType);


        }


        void InventorySearch_searchEvent(object sender, global::BregVision.UserControls.General.SearchEventArgs e)
        {
            grdInventory.SearchText = e.SearchText;
            grdInventory.SearchCriteria = e.SearchCriteria;
            grdInventory.SearchType = e.SearchType;
            grdInventory.SetSearchCriteria(e.SearchType);
        }
        #endregion

        #region Practice Catalog Grid Events


        protected void grdPracticeCatalog_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            if (e.Item.OwnerTableView.Name == "grdtvPracticeCatalog")
            {
                grdPracticeCatalog.MasterTableView.DetailTables[0].CurrentPageIndex = e.NewPageIndex;
            }

            grdPracticeCatalog.SetExpansionOnPageIndexChanged();
        }

        protected void grdPracticeCatalog_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            //string searchCriteria = "";
            //string searchText = "";

            if (practiceID > 0)
            {
                if (!e.IsFromDetailTable || TelerikAjax.UIHelper.IsGridRebindReasonSet(e.RebindReason, Telerik.Web.UI.GridRebindReason.InitialLoad))
                    grdPracticeCatalog.DataSource = DAL.Practice.Catalog.GetPracticeCatalogSuppliers(grdPracticeCatalog.SearchCriteria, grdPracticeCatalog.SearchText, practiceID);

                //searchCriteria = grdPracticeCatalog.SearchCriteria;
                //searchText = grdPracticeCatalog.SearchText;

                //if (searchText != null && !string.IsNullOrEmpty(searchCriteria))
                //{
                //    grdPracticeCatalog.PageSize = recordsDisplayedPerPage;
                //    if (!e.IsFromDetailTable)
                //    {
                //        DataTable dt = GetPracticeCatalogSuppliers(practiceID, searchCriteria, searchText, recordsDisplayedPerPage, 1);
                //        grdPracticeCatalog.DataSource = dt;
                //        productCount = TelerikAjax.UIHelper.SetMaxCount(dt, "ProductCount");
                //    }
                //    grdPracticeCatalog.MasterTableView.DetailTables[0].DataSource = GetPracticeCatalogDataBySupplier(practiceID,grdPracticeCatalog.SearchCriteria, grdPracticeCatalog.SearchText);


                //if (productCount > 0)
                //{
                //    grdPracticeCatalog.MasterTableView.DetailTables[0].VirtualItemCount = productCount;
                //    grdPracticeCatalog.VirtualItemCount = productCount;
                //}


                //}
            }
            else
            {
                lblDeleteMessage.Text = "There seems to be a problem with your session.  Please log out and log back in again to continue.";
            }
        }



        private int grdPracticeCatalog_RecordCount = 0;

        protected void grdPracticeCatalog_OnDetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {

            //Telerik.Web.UI.GridDataItem gridParentDataItem = e.DetailTableView.ParentItem as Telerik.Web.UI.GridDataItem;
            //int practiceCatalogSupplierBrandID = gridParentDataItem.GetColumnInt32("PracticeCatalogSupplierBrandID");

            //int startPage = GetStartPageNumber(grdPracticeCatalog) ;

            //if (practiceCatalogSupplierBrandID > 0)
            //{
            //    grdPracticeCatalog.MasterTableView.DetailTables[0].DataSource = GetPracticeCatalogDataBySupplierPaged(practiceID, practiceCatalogSupplierBrandID, grdPracticeCatalog.SearchCriteria, grdPracticeCatalog.SearchText, recordsDisplayedPerPage, startPage);
            //}
            //TelerikAjax.UIHelper.FilterOnJoinKey(e, "PracticeCatalogSupplierBrandID", false);

            var data = DAL.Practice.Catalog.GetPracticeCatalogDataBySupplier2(grdPracticeCatalog.SearchCriteria, grdPracticeCatalog.SearchText, practiceID, e.DetailTableView.ParentItem.GetColumnInt32("PracticeCatalogSupplierBrandID"));
            e.DetailTableView.DataSource = data;

            grdPracticeCatalog_RecordCount += data.Count(x => !string.IsNullOrEmpty(x.ShortName));

            // HACK: sets the record count on the page. The last run through on a search would leave the grdPracticeCatalog_RecordCount value updated with the correct total...
            grdPracticeCatalog.SetRecordCount(grdPracticeCatalog_RecordCount);
            grdPracticeCatalog.SetGridLabel(grdPracticeCatalog.SearchCriteria, Page.IsPostBack, lblMCStatus);
        }

        protected void grdPracticeCatalog_PreRender(object sender, EventArgs e)
        {
            grdPracticeCatalog.ExpandGrid();
        }

        #endregion


        #region Inventory Grid Events

        #region RadInputManager bits

        /// <summary>
        /// Registers a TextBox with a RadInputManager setting. Relies on _GetRadInputManagerNumberSetting()
        /// to retrieve the setting.
        /// 
        /// NOTE: The Load event is fired during postback as well. The _ClearRadInputManagerTagerControls()
        /// method is used to remove registration for TextBoxes that are going to be hidden during a
        /// collapse of a detail table view.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void TextBoxAddToRadInputManager_Load(object source, EventArgs e)
        {
            var t = source as TextBox;
            if (t == null)
                return;

            _GetRadInputManagerNumberSetting("numberSetting")
                .TargetControls
                .Add(new Telerik.Web.UI.TargetInput(t.UniqueID, true));
        }


        /// <summary>
        /// Clears any registered controls that are child items of the provided GridItem.
        /// </summary>
        /// <param name="rowgriditem"></param>
        private void _ClearRadInputManagerTargetControls(Telerik.Web.UI.GridItem rowgriditem)
        {
            var deletelist = new List<Telerik.Web.UI.TargetInput>();
            var targets = _GetRadInputManagerNumberSetting("numberSetting").TargetControls;

            // loop through all nested table views and find targets that
            // have UniqueID values beginning with the table view UniqueID.
            // NOTE: this relies on child nodes always have its parent's ID
            // as a prefix.
            foreach (var views in rowgriditem.OwnerTableView.Items[rowgriditem.ItemIndex].ChildItem.NestedTableViews)
            {
                for (var i = 0; i < targets.Count; i++)
                {
                    var t = targets[i];
                    if (t.ControlID.StartsWith(views.UniqueID))
                        deletelist.Add(t);
                }
            }

            // go through the list to remove and un-register controls
            // from the RadInputManager setting targets list.
            foreach (var t in deletelist)
                targets.Remove(t);
        }

        /// <summary>
        /// Gets the RadInputManager setting with BehaviorID="numberSetting".
        /// </summary>
        /// <returns></returns>
        private Telerik.Web.UI.NumericTextBoxSetting _GetRadInputManagerNumberSetting(string id)
        {
            return
                (Telerik.Web.UI.NumericTextBoxSetting)
                RadInputManager1.GetSettingByBehaviorID(id);
        }

        /// <summary>
        /// Used to catch a detail table view collapse and trigger removal of
        /// collapsed controls from RadInputManager.
        /// 
        /// NOTE: during PostBack, controls inside a collapsing detail view
        /// are still registered in their Load events. To prevent RadInputManager
        /// from trying to validate against a non-existent control, only visible
        /// controls should be registered.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdInventory_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            // clear controls only when event is triggered by expand/collapse
            // and do it only when collapsing.
            if ((e.CommandName == "ExpandCollapse") && e.Item.Expanded)
                _ClearRadInputManagerTargetControls(e.Item);
        }

        #endregion

        protected void grdInventory_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            grdInventory.SetExpansionOnPageIndexChanged();
        }

        protected void grdInventory_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {

            string searchCriteria = "";
            string searchText = "";
            bool isActive = true;

            if (practiceLocationID > 0)
            {

                searchCriteria = grdInventory.SearchCriteria;
                searchText = grdInventory.SearchText;

                if (searchText != null && !string.IsNullOrEmpty(searchCriteria))
                {
                    if (!e.IsFromDetailTable)
                    {
                        grdInventory.DataSource = GetSuppliers(practiceLocationID, searchCriteria, searchText, isActive);
                    }

                    //grdInventory.MasterTableView.DetailTables[0].DataSource = GetInventoryBySupplier(practiceLocationID, searchCriteria, searchText, isActive);
                    //grdInventory.SetRecordCount((DataSet)grdInventory.MasterTableView.DetailTables[0].DataSource, "Product");
                    //grdInventory.SetGridLabel(searchCriteria, Page.IsPostBack, lblPCStatus); //lblPCStatus is actually inventory

                }
            }
            else
            {
                lblDeleteMessage.Text = "There seems to be a problem with your session.  Please log out and log back in again to continue.";
                //WriteJavascriptToLabel(((Telerik.Web.UI.RadGrid)source), lblDeleteMessage, "There seems to be a problem with your session.  Please log out and log back in again to continue.");
            }
        }


        private int grdInventory_RecordCount = 0;

        protected void grdInventory_OnDetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {
            //TelerikAjax.UIHelper.FilterOnJoinKey(e, "SupplierID", false);
            var data = GetInventoryBySupplier2(practiceLocationID, e.DetailTableView.ParentItem.GetColumnInt32("SupplierID"), grdInventory.SearchCriteria, grdInventory.SearchText, true);
            e.DetailTableView.DataSource = data;

            grdInventory_RecordCount += data.Tables[0].AsEnumerable().Count(x => x.Field<string>("Product") != null);

            // HACK: sets the record count on the page. The last run through on a search would leave the grdInventory_RecordCount value updated with the correct total...
            grdInventory.SetRecordCount(grdInventory_RecordCount);
            grdInventory.SetGridLabel(grdInventory.SearchCriteria, Page.IsPostBack, lblPCStatus); //lblPCStatus is actually inventory
        }


        protected void grdInventory_PreRender(object sender, EventArgs e)
        {
            if (grdInventory.NeedsRebind == true)
            {
                grdInventory.Rebind();
            }

            grdInventory.ExpandGrid();//grdInventory.ExpandGrid(true);
        }

        #endregion


        public int GetStartPageNumber(Telerik.Web.UI.RadGrid radGrid)
        {
            return ((ShouldApplySortFilterOrGroup(radGrid)) ? 0 : radGrid.MasterTableView.DetailTables[0].CurrentPageIndex) + 1;
        }

        public bool ShouldApplySortFilterOrGroup(Telerik.Web.UI.RadGrid radGrid)
        {
            return radGrid.MasterTableView.FilterExpression != "" ||
                (radGrid.MasterTableView.GroupByExpressions.Count > 0 || isGrouping) ||
                radGrid.MasterTableView.SortExpressions.Count > 0;
        }



        public DataTable GetPracticeCatalogSuppliers(Int32 practiceID, string SearchCriteria, string SearchText, int PageSize, int PageNumber)
        {
            // uses "PracticeCatalogSupplierBrandID" as the foreign key in the grid
            DataTable SuppliersWithInventory = new DataTable();

            int totalPages = 0; //this is currently unused.  I'm leaving it here for future use in custom paging

            DataSet dsAllSuppliers = GetPracticeCatalogSuppliersPaged(practiceID, SearchCriteria, SearchText, PageSize, PageNumber, out totalPages);

            //DataSet dsPracticeCatalogData = GetPracticeCatalogDataBySupplier(practiceID, SearchCriteria, SearchText);

            var query = (from supplier in dsAllSuppliers.Tables[0].AsEnumerable()
                         //join practiceCatalogData in dsPracticeCatalogData.Tables[0].AsEnumerable()
                         //on supplier.Field<int>("PracticeCatalogSupplierBrandID") equals practiceCatalogData.Field<int>("PracticeCatalogSupplierBrandID")
                         where supplier.Field<int>("ProductCount") > 0
                         select supplier);

            if (query != null && query.Count() > 0)
            {
                SuppliersWithInventory = query.CopyToDataTable();
            }

            return SuppliersWithInventory;
        }


        public static DataSet GetPracticeCatalogSuppliersPaged(Int32 practiceID, string SearchCriteria, string SearchText, Int32 pageSize, Int32 pageNumber, out Int32 totalPages)
        {


            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetPracticeCatalogSupplierBrandsPaged");

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
            db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);
            db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
            db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
            db.AddOutParameter(dbCommand, "TotalRecords", DbType.Int32, 4);
            DataSet ds = db.ExecuteDataSet(dbCommand);

            totalPages = 0;  // Convert.ToInt32(db.GetParameterValue(dbCommand, "TotalRecords"));
            return ds;

        }



        public DataSet GetPracticeCatalogDataBySupplier(string SearchCriteria, string SearchText)
        {

            if (grdPracticeCatalog.VisionDataSet == null)
            {
                grdPracticeCatalog.VisionDataSet = GetPracticeCatalogDataBySupplier(practiceID, SearchCriteria, SearchText);
            }

            return grdPracticeCatalog.VisionDataSet;
        }

        public DataSet GetPracticeCatalogDataBySupplier(Int32 practiceID, string SearchCriteria, string SearchText)
        {
            DataSet ds = null;
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetPracticeCatalogProductInfoMCTOPC");
            db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
            db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);

            ds = db.ExecuteDataSet(dbCommand);

            return ds;
        }

        public static DataSet GetPracticeCatalogDataBySupplierPaged(Int32 practiceID, Int32 practiceCatalogSupplierBrandID, string SearchCriteria, string SearchText, Int32 pageSize, Int32 pageNumber)
        {
            DataSet ds = null;
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetPracticeCatalogProductInfoMCToPC_Paged");
            db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
            db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            db.AddInParameter(dbCommand, "PracticeCatalogSupplierBrandID", DbType.Int32, practiceCatalogSupplierBrandID);
            db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
            db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);

            ds = db.ExecuteDataSet(dbCommand);

            return ds;
        }


        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            var selectedDataItems = from Telerik.Web.UI.GridDataItem inventoryItem in (grdInventory.Items as IEnumerable)
                                    where inventoryItem.Selected == true
                                    && inventoryItem.GetColumnInt32("IsDeletable") == 1
                                    select inventoryItem;

            string deleteMessage = "";


            foreach (Telerik.Web.UI.GridDataItem item in selectedDataItems)
            {
                Telerik.Web.UI.GridDataItem dataItem = item as Telerik.Web.UI.GridDataItem;

                Telerik.Web.UI.GridDataItem parent = dataItem.OwnerTableView.ParentItem;

                deleteMessage += DeleteAll(grdInventory, dataItem, parent);
            }
            grdInventory.Rebind();
            UpdateLabel(lblDeleteMessage, deleteMessage);
        }


        protected void btnMoveToPC_Click(object sender, EventArgs e)
        {

            TransferAllToProductInventory(grdPracticeCatalog.MasterTableView);
        }


        private void TransferAllToProductInventory(Telerik.Web.UI.GridTableView gridTableView)
        {

            if (practiceLocationID > 0)
            {
                foreach (Telerik.Web.UI.GridNestedViewItem nestedViewItem in gridTableView.GetItems(Telerik.Web.UI.GridItemType.NestedView))
                {
                    if (nestedViewItem.NestedTableViews.Length > 0)
                    {
                        foreach (Telerik.Web.UI.GridDataItem gridDataItem in nestedViewItem.NestedTableViews[0].Items)
                        {
                            if (gridDataItem.Selected == true)
                            {
                                //Add to 
                                Int32 practiceCatalogProductID = 0;
                                practiceCatalogProductID = findPracticeCatalogProductID(gridDataItem);
                                if (practiceCatalogProductID > 0)
                                {
                                    Int32 ret = TransferToProductInventory(practiceLocationID, practiceCatalogProductID, 1);
                                    intPCItems += 1;
                                    gridDataItem.Selected = false;
                                }
                                else
                                {
                                    
                                    lblPCStatus.Text = "There was an error updating the grid.  All of your items may not have been updated.";
                                    //BregVision.ErrHandler.VisionErrHandler KEH = new BregVision.ErrHandler.VisionErrHandler();
                                    //BregVision.ErrHandler.VisionErrHandler.RaiseError("There was an error in function TransferAllToProductInventory on PracticeLocation_InventoryLevels.aspx.  The PracticeCatalogProductID was 0 or null");
                                    break;
                                }

                            }
                        }

                    }

                }
                if (intPCItems > 0)
                    grdInventory.Rebind();
                AddToInventoryCompletedLabel.Visible = true;
                lblPCStatus.Text = String.Concat("Item(s) added to Inventory: ", intPCItems.ToString());
            }
            else
            {
                lblPCStatus.Text = "There seems to be a problem with your session.  Please log out and log back in again to continue.";
            }

        }


        private Int32 TransferToProductInventory(Int32 PracticeLocationID, Int32 PracticeCatalogProductID, Int32 UserID)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_TransferFromPracticeCatalogtoProductInventory");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);
            db.AddInParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, PracticeCatalogProductID);
            db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);

            Int32 SQLReturn = db.ExecuteNonQuery(dbCommand);

            return SQLReturn;

        }


        private Int32 findPracticeCatalogProductID(Telerik.Web.UI.GridDataItem gridDataItem)
        {
            return findIntegerDataField(gridDataItem, "PracticeCatalogProductID");
        }

        private Int32 findMasterCatalogProductID(Telerik.Web.UI.GridDataItem gridDataItem)
        {
            return findIntegerDataField(gridDataItem, "Mastercatalogproductid");
        }

        private Int32 findIntegerDataField(Telerik.Web.UI.GridDataItem gridDataItem, string FieldName)
        {

            string strFieldValue = (string)gridDataItem[FieldName].Text;
            if (strFieldValue != null)
            {
                try
                {
                    int retValue = 0;
                    bool isNumeric = false;
                    isNumeric = Int32.TryParse(strFieldValue, out retValue);
                    if (isNumeric == true)
                    {
                        return retValue;
                    }
                }
                catch { }
            }

            return 0;

        }

        protected void btnUpdateInventory_Click(object sender, EventArgs e)
        {

            UpdateAllInventoryLevels(grdInventory.MasterTableView);

        }

        private void UpdateAllInventoryLevels(Telerik.Web.UI.GridTableView gridTableView)
        {
            string consignmentMessage = "";

            foreach (Telerik.Web.UI.GridNestedViewItem nestedViewItem in gridTableView.GetItems(Telerik.Web.UI.GridItemType.NestedView))
            {
                if (nestedViewItem.NestedTableViews.Length > 0)
                {
                    foreach (Telerik.Web.UI.GridDataItem gridDataItem in nestedViewItem.NestedTableViews[0].Items)
                    {
                        if (!gridDataItem.Selected)
                            continue;
                        
                        CheckBox chkIsNotFlaggedForReorder = (CheckBox)gridDataItem.FindControl("chkIsNotFlaggedForReorder");
                        bool isNotFlaggedForReorder = (chkIsNotFlaggedForReorder != null) ? chkIsNotFlaggedForReorder.Checked : false;

                        bool isConsignment = false;
                        bool consignmentLockParLevel = false;
                        int parLevel = 0;
                        int consignmentQuantity = 0;

                        int practiceCatalogProductID = 0;
                        practiceCatalogProductID = findPracticeCatalogProductID(gridDataItem);
                        if (practiceCatalogProductID > 0)
                        {
                            parLevel = findTextboxIntegerValue(gridDataItem, "txtParLevel");

                            if (Context.User.IsInRole("bregadmin"))
                            {
                                CheckBox chkIsConsignment = (CheckBox)gridDataItem.FindControl("chkIsConsignment");
                                CheckBox chkConsignmentLockParLevel = (CheckBox)gridDataItem.FindControl("chkConsignmentLockParLevel");

                                isConsignment = (chkIsConsignment != null) ? chkIsConsignment.Checked : false;
                                consignmentQuantity = findTextboxIntegerValue(gridDataItem, "txtConsignmentQuantity");
                                consignmentLockParLevel = (chkConsignmentLockParLevel != null) ? chkConsignmentLockParLevel.Checked : false;
                            }
                            else
                            {
                                var isConsignmentVar = gridDataItem["IsConsignmentHdn"]; 
                                bool.TryParse(isConsignmentVar.Text, out isConsignment);

                                var consignmentQuantityVar = gridDataItem["ConsignmentQuantityHdn"];
                                int.TryParse(consignmentQuantityVar.Text, out consignmentQuantity);

                                var consignmentLockParLevelVar = gridDataItem["ConsignmentLockParLevelHdn"];
                                bool.TryParse(consignmentLockParLevelVar.Text, out consignmentLockParLevel);
                            }

                            if (isConsignment == true)
                            {
                                //consignment items can not be custom braces or CustomBraceAccessories.
                                if (DAL.Practice.Catalog.IsCustomBraceOrAccessory(practiceCatalogProductID) == true)
                                {
                                    consignmentMessage = string.Format("This Item is a custom Brace or Accessory.  Custom Items can not be Consigned.");
                                    gridDataItem.BackColor = System.Drawing.Color.Red;
                                    break;
                                }

                                if (consignmentLockParLevel == true)
                                {
                                    if (parLevel != consignmentQuantity)
                                    {
                                        consignmentMessage = string.Format("Par Level {0} must equal consignment quantity", consignmentQuantity);
                                        gridDataItem.BackColor = System.Drawing.Color.Red;
                                        break;
                                    }
                                }
                                else if (parLevel < consignmentQuantity)
                                {
                                    consignmentMessage = string.Format("Par Level cannot be lower than {0} due to consignment", consignmentQuantity);
                                    gridDataItem.BackColor = System.Drawing.Color.Red;
                                    break;
                                }
                            }

                            gridDataItem.Selected = false;
                            Int32 return_Value = UpdateInventoryLevels(     practiceCatalogProductID, 
                                                                            practiceLocationID, 
                                                                            userID, 
                                                                            findTextboxIntegerValue(gridDataItem, "txtQOH"), 
                                                                            parLevel, 
                                                                            findTextboxIntegerValue(gridDataItem, "txtReorderLevel"), 
                                                                            findTextboxIntegerValue(gridDataItem, "txtCriticalLevel"), 
                                                                            isNotFlaggedForReorder, 
                                                                            isConsignment, 
                                                                            consignmentQuantity,
                                                                            consignmentLockParLevel,
                                                                            gridDataItem.GetColumnChecked("chkIsLogoAblePart"));

                            intPCItems += 1;
                        }
                        gridDataItem.Selected = false;
                    }

                }
            }
         
            string strPCStatus =
                String.Format(
                    "Number of products updated: {0}{1}",
                    intPCItems.ToString(),
                    string.IsNullOrEmpty(consignmentMessage)
                        ? string.Empty
                        : ("<br />" + consignmentMessage));
            lblPCStatus.Text = strPCStatus;
        }




        private Int32 UpdateInventoryLevels(Int32 PracticeCatalogProductID, 
                                            Int32 PracticeLocationID, 
                                            Int32 UserID,
                                            int QOH, 
                                            int ParLevel, 
                                            int ReorderLevel, 
                                            int CriticalLevel, 
                                            bool isNotFlaggedForReorder, 
                                            bool isConsignment, 
                                            int consignmentQuantity, 
            
                                            bool consignmentLockParLevel = false,
                                            bool isLogoAblePart = false)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_UpdateInventoryLevels");

            db.AddInParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, PracticeCatalogProductID);
            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);
            db.AddInParameter(dbCommand, "UserID", DbType.Int16, UserID);
            db.AddInParameter(dbCommand, "QOH", DbType.Int16, QOH);
            db.AddInParameter(dbCommand, "ParLevel", DbType.Int16, ParLevel);
            db.AddInParameter(dbCommand, "ReorderLevel", DbType.Int16, ReorderLevel);
            db.AddInParameter(dbCommand, "CriticalLevel", DbType.Int16, CriticalLevel);
            db.AddInParameter(dbCommand, "IsNotFlaggedForReorder", DbType.Boolean, isNotFlaggedForReorder);
            db.AddInParameter(dbCommand, "IsConsignment", DbType.Boolean, isConsignment);
            db.AddInParameter(dbCommand, "ConsignmentQuantity", DbType.Int16, consignmentQuantity);
            db.AddInParameter(dbCommand, "ConsignmentLockParLevel", DbType.Boolean, consignmentLockParLevel);
            db.AddInParameter(dbCommand, "IsLogoAblePart", DbType.Boolean, isLogoAblePart);

            int return_value = db.ExecuteNonQuery(dbCommand);

            return return_value;
        }



        private int findTextboxIntegerValue(Telerik.Web.UI.GridDataItem gridDataItem, string txtBox)
        {

            TextBox nTextBox = (TextBox)gridDataItem.FindControl(txtBox);

            if (nTextBox != null && nTextBox.Text.Length > 0)
            {
                try
                {
                    int retValue = 0;
                    bool isNumeric = false;
                    isNumeric = Int32.TryParse(nTextBox.Text, out retValue);
                    if (isNumeric == true)
                    {
                        return retValue;
                    }

                }
                catch { }
            }

            return 0;
        }

        public DataSet GetSuppliers(int practiceLocationID)
        {

            //Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            //DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetSuppliers");

            //db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            //DataSet ds = db.ExecuteDataSet(dbCommand);
            //return ds;

            using (var c = new SqlConnection(ConfigurationManager.ConnectionStrings["BregVisionConnectionString"].ConnectionString))
            {
                c.Open();
                var cmd = c.CreateCommand();
                cmd.CommandText = "usp_GetSuppliers";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter { ParameterName = "PracticeLocationID", DbType = DbType.Int32, Value = practiceLocationID });
                var r = cmd.ExecuteReader();
                var dt = new DataTable();
                dt.Load(r);
                var ds = new DataSet();
                ds.Tables.Add(dt);
                return ds;
            }

        }
        public DataTable GetSuppliers(int practiceLocationID, string SearchCriteria, string SearchText, Boolean isActive)
        {
            DataTable SuppliersWithInventory = new DataTable();


            DataSet dsAllSuppliers = GetSuppliers(practiceLocationID);

            DataSet dsInventory = GetInventoryBySupplier(practiceLocationID, SearchCriteria, SearchText, isActive);

            var query = (from supplier in dsAllSuppliers.Tables[0].AsEnumerable()
                         join inventory in dsInventory.Tables[0].AsEnumerable()
                         on supplier.Field<int>("SupplierID") equals inventory.Field<int>("SupplierID")
                         select supplier).Distinct();

            if (query != null && query.Count() > 0)
            {
                SuppliersWithInventory = query.CopyToDataTable();
            }


            return SuppliersWithInventory;

        }


        public DataSet GetInventoryBySupplier(string SearchCriteria, string SearchText, Boolean isActive)
        {
            if (grdInventory.VisionDataSet == null)
            {
                grdInventory.VisionDataSet = GetInventoryBySupplier(practiceLocationID, SearchCriteria, SearchText, isActive);
            }

            return grdInventory.VisionDataSet;
        }


        public DataSet GetInventoryBySupplier(int practiceLocationID, string SearchCriteria, string SearchText, Boolean isActive)
        {

            using (var c = new SqlConnection(ConfigurationManager.ConnectionStrings["BregVisionConnectionString"].ConnectionString))
            {
                c.Open();
                var cmd = c.CreateCommand();
                cmd.CommandText = "usp_GetAllInventoryDataforPCToIventory_New";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter { ParameterName = "isActive", DbType = DbType.Boolean, Value = isActive });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "SearchCriteria", DbType = DbType.String, Value = SearchCriteria });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "SearchText", DbType = DbType.String, Value = SearchText });
                cmd.Parameters.Add(new SqlParameter { ParameterName = "PracticeLocationID", DbType = DbType.Int32, Value = practiceLocationID });
                var r = cmd.ExecuteReader();
                var dt = new DataTable();
                dt.Load(r);
                var ds = new DataSet();
                ds.Tables.Add(dt);
                return ds;
            }

        }

        public DataSet GetInventoryBySupplier2(int practiceLocationID, int supplierID, string SearchCriteria, string SearchText, Boolean isActive)
        {
            var ds = GetInventoryBySupplier(practiceLocationID, SearchCriteria, SearchText, isActive);
            var dt = ds.Tables[0];
            var dt_remove = new List<DataRow>();
            dt_remove.AddRange(dt.Rows.Cast<DataRow>().Where(row => int.Parse(row["SupplierID"].ToString()) != supplierID).ToArray());
            foreach (var item in dt_remove)
                dt.Rows.Remove(item);
            return ds;
        }


        protected void grdInventory_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            // only check for the grdInventory
            if (e.Item.OwnerTableView.OwnerGrid.ID == "grdInventory")
            {

                if (e.Item is Telerik.Web.UI.GridDataItem)
                {

                    try
                    {
                        Telerik.Web.UI.GridDataItem dataItem = (Telerik.Web.UI.GridDataItem)e.Item;
                        //Set Reorder Level

                        try
                        {
                            Telerik.Web.UI.GridColumn isActive = e.Item.OwnerTableView.GetColumnSafe("isactive");
                            if (isActive != null)
                            {
                                if (dataItem["isactive"].Text.ToLower() != "true")
                                {
                                    //dataItem.BackColor = System.Drawing.Color.Red;
                                    dataItem.CssClass = "RedRow_Office2007";
                                    dataItem.ForeColor = System.Drawing.Color.White;
                                }
                            }
                        }
                        catch { }

                        //par level
                        try
                        {
                            Telerik.Web.UI.GridColumn parLevelColumn = e.Item.OwnerTableView.GetColumnSafe("ParLevel");
                            if (parLevelColumn != null)
                            {
                                //string parLevel = dataItem["ParLevel"].Text;

                                DataRowView drv = dataItem.DataItem as DataRowView;

                                if (drv != null)
                                {
                                    try
                                    {
                                        CheckBox chkIsLogoPartItem = dataItem["IsLogoAblePartPi"].FindControl("chkIsLogoAblePart") as CheckBox;
                                        if (drv != null && chkIsLogoPartItem != null)
                                        {
                                            //if mcp is logoable, then they can check or uncheck pcp is logoable
                                            bool mcpIsLogoAblePart = Convert.ToBoolean(drv["McpIsLogoAblePart"]);
                                            chkIsLogoPartItem.Enabled = mcpIsLogoAblePart;
                                            if (mcpIsLogoAblePart == false)
                                            {
                                                chkIsLogoPartItem.Checked = false;
                                            }
                                        }
                                    }
                                    catch { }


                                    var isConsignmentVar = drv.Row["IsConsignment"];
                                    bool isConsignment = false;
                                    bool.TryParse(isConsignmentVar.ToString(), out isConsignment);

                                    if (isConsignment == true)
                                    {
                                        var consignmentLockParLevelVar = drv.Row["ConsignmentLockParLevel"];
                                        var isConsignmentRowLocked = false;
                                        bool.TryParse(consignmentLockParLevelVar.ToString(), out isConsignmentRowLocked);

                                            TextBox txtParLevel = dataItem["ParLevel"].FindControl("txtParLevel") as TextBox;
                                            if (txtParLevel != null && Context.User.IsInRole("BregAdmin") == false)
                                            {
                                                if (isConsignmentRowLocked == true)
                                                {
                                                    txtParLevel.Enabled = false;

                                                }
                                                else
                                                {
                                                    //message that upping par level doesn't notify breg
                                                    var oldParLevelVar = drv["ParLevel"];
                                                    int oldParLevel = -1;
                                                    int.TryParse(oldParLevelVar.ToString(), out oldParLevel);
                                                    if (oldParLevel > -1)
                                                    {
                                                        txtParLevel.Attributes.Add("onChange", "MsgOnParLevelIncrease('" + txtParLevel.ClientID + "'," + oldParLevel.ToString() + ")");
                                                    }
                                                }
                                            }
                                           
                                        //}
                                    }
                                }
                                
                            }
                        }
                        catch{ }
                    }
                    catch { }
                }

                // check IsDeleted to show the garbage can (delete button).
                if (e.Item is Telerik.Web.UI.GridDataItem)
                {
                    try
                    {
                        //  20071022 JB
                        Telerik.Web.UI.GridDataItem dataItem = e.Item as Telerik.Web.UI.GridDataItem;

                        Telerik.Web.UI.GridColumn isDeletableColumn = e.Item.OwnerTableView.GetColumnSafe("IsDeletable");
                        if (isDeletableColumn != null)
                        {
                            int isDeletable = (dataItem["IsDeletable"] != null) ? Convert.ToInt32(dataItem["IsDeletable"].Text.ToString()) : 0;

                            if (isDeletable == 1)
                            {
                                string productCode = (dataItem["Code"] != null) ? dataItem["Code"].Text.ToString() : "";
                                string productName = (dataItem["Product"] != null) ? dataItem["Product"].Text.ToString() : "";

                                productCode = productCode.Replace("\\", "/");
                                productCode = productCode.Replace("\'", "\\\'");
                                productCode = productCode.Replace("\"", "\\\"");

                                productName = productName.Replace("\\", "/");
                                productName = productName.Replace("\'", "\\\'");
                                productName = productName.Replace("\"", "\\\"");

                                string stringOnClickMessage = @"Delete from Inventory?\n\n Product:  " + productName + @"\n Code:     " + productCode;
                                string stringOnClientClickMessage = @"if(!confirm('" + stringOnClickMessage + "'))return false;";


                                ImageButton btnDelete = (ImageButton)dataItem["DeleteColumn"].FindControl("btnDelete");
                                btnDelete.ImageUrl = "~/App_Themes/Breg/images/Common/delete.png";
                                btnDelete.ToolTip = "Delete Product from Inventory.";  //\r\n Product: " + productName + "\r\n Code: " + productCode;
                                btnDelete.Enabled = true;

                                btnDelete.OnClientClick = @"if(!confirm('" + stringOnClickMessage + "'))return false;";

                            }
                            else
                            {
                                ImageButton btnDelete = (ImageButton)dataItem["DeleteColumn"].FindControl("btnDelete");

                                if (Context.User.IsInRole("BregAdmin"))
                                {
                                    btnDelete.ImageUrl = "~/App_Themes/Breg/images/Common/delete-unsafe.png";
                                    btnDelete.ToolTip = "Delete Product From Inventory, as well as Cart and Orders";
                                    btnDelete.Enabled = true;
                                }
                                else
                                {
                                    btnDelete.ImageUrl = "~/App_Themes/Breg/images/Common/clear.png";
                                    btnDelete.ToolTip = "";
                                    btnDelete.Enabled = false;
                                }

                                

                            }
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }


        protected void chkConsignmentLockParLevelAll_OnCheckedChanged(object sender, EventArgs e)
        {
            
            CheckBox chkLockParLevels = sender as CheckBox;
            if(chkLockParLevels != null)
            {
                bool  lockParLevels = chkLockParLevels.Checked;
                DAL.PracticeLocation.Inventory.UpdateConsignmentLevelLocks(practiceLocationID, lockParLevels);
                grdInventory.RebindWithExpandState();
            }
        }


        protected void txtRecordsDisplayed_TextChanged(object sender, EventArgs e)
        {

            try
            {
                //this is to ensure that the user didn't leave the textbox empty.  The empty catch will just cause the page to stay as it was
                // i left the outer error handler because later we might want to catch and log certain errors
                recordsDisplayedPerPage = Convert.ToInt32(txtRecordsDisplayed.Text);

                grdInventory.PageSize = recordsDisplayedPerPage;
                grdPracticeCatalog.PageSize = recordsDisplayedPerPage;

                grdInventory.Rebind();
                grdPracticeCatalog.Rebind();
            }
            catch { }
        }

        protected void grdInventory_DeleteCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.Item is Telerik.Web.UI.GridDataItem)
                {
                    Telerik.Web.UI.GridDataItem dataItem = e.Item as Telerik.Web.UI.GridDataItem;

                    Telerik.Web.UI.GridDataItem parent = dataItem.OwnerTableView.ParentItem;

                    Telerik.Web.UI.RadGrid grid = (Telerik.Web.UI.RadGrid)sender;

                    string deleteMessage = DeleteAll(grid, dataItem, parent);

                    UpdateLabel(lblDeleteMessage, deleteMessage);
                }
            }
            catch
            {
            }
        }

        protected string DeleteAll(Telerik.Web.UI.RadGrid sender, Telerik.Web.UI.GridDataItem dataItem, Telerik.Web.UI.GridDataItem parent)
        {

            //delete item
            //if (e.Item is GridDataItem)
            //{
            int removalType = -1;
            string dependencyMessage = "";

            //GridDataItem dataItem = e.Item as GridDataItem;

            //GridDataItem parent = dataItem.OwnerTableView.ParentItem;

            Int32 productInventoryID = Int32.Parse(dataItem["ProductInventoryID"].Text);

            string productCode = (dataItem["Code"] != null) ? dataItem["Code"].Text.ToString() : "";
            string productName = (dataItem["Product"] != null) ? dataItem["Product"].Text.ToString() : "";

            productCode = productCode.Replace("\\", "/");
            productName = productName.Replace("\\", "/");

            string productNameCode = "<br />  Product: " + "<b>" + productName + "</b>" + "<br />  Code: " + "<b>" + productCode + "</b><br/><br/>";

            ////Next line calls function to delete item from cart
            bool isAdmin = Context.User.IsInRole("BregAdmin");
            ProductInventory_Delete(productInventoryID, userID, out removalType, out dependencyMessage, isAdmin);


            // move this out of the ajaxpanel.
            string deleteMessage = "";
            switch (removalType)
            {
                case -1:
                    {
                        //  do nothing
                        deleteMessage = ConfigurationSettings.AppSettings["PIDeleteError"].ToString();
                        deleteMessage += productNameCode;
                        break;
                    }
                case 0:
                    {
                        //Show dependencies.
                        deleteMessage = ConfigurationSettings.AppSettings["PIDeleteNotRemoved"].ToString();
                        //deleteMessage += productNameCode;

                        string deleteDependencies = "";
                        if ((Convert.ToInt32(dependencyMessage.Contains("level")) > 0) == true)
                            deleteDependencies += "<br /> " + ConfigurationSettings.AppSettings["PIDeleteDependencyLevels"].ToString();
                        if ((Convert.ToInt32(dependencyMessage.Contains("Cart")) > 0) == true)
                            deleteDependencies += "<br /> " + ConfigurationSettings.AppSettings["PIDeleteDependencyInCart"].ToString();
                        if ((Convert.ToInt32(dependencyMessage.Contains("Order")) > 0) == true)
                            deleteDependencies += "<br /> " + ConfigurationSettings.AppSettings["PIDeleteDependencyOnOrder"].ToString();

                        deleteMessage += deleteDependencies.ToString() + productNameCode;
                        break;
                    }
                case 1:
                    {
                        //  removed.  hard delete.
                        grdInventory.CauseRebindOnDelete(parent);
                        deleteMessage = ConfigurationSettings.AppSettings["PIDeleted"].ToString();
                        deleteMessage += productNameCode;
                        break;
                    }
                case 2:
                    {
                        //  removed.  soft delete.
                        grdInventory.CauseRebindOnDelete(parent);
                        deleteMessage = ConfigurationSettings.AppSettings["PIRemoved"].ToString();
                        deleteMessage += productNameCode;
                        break;
                    }
                default:
                    {
                        //  do nothing
                        break;
                    }
            }
            return deleteMessage;
        }

        protected void UpdateLabel(Label lblToUpdate, string UpdateText)
        {

            lblToUpdate.Text = UpdateText;
        }


        protected void ProductInventory_Delete(int productInventoryID, int userID, out int removalType, out string dependencyMessage, bool isAdmin)
        {
            removalType = -1;
            dependencyMessage = "";

            // Delete the productInventoryID from Inventory.
            DAL.PracticeLocation.Inventory dalInventory = new DAL.PracticeLocation.Inventory();
            dalInventory.Delete(productInventoryID, userID, out removalType, out dependencyMessage, isAdmin);

            int junk = removalType;
            string junkstring = dependencyMessage;
        }

        protected eSetupStatus ValidateSetupData()
        {
            return eSetupStatus.Complete;
        }

        /// <summary>
        /// IVisionWizardControl interface save.  Called by wizard control on tab change
        /// </summary>
        public void Save()
        {
            //do nothing.  Grid rows are saved individually

            SaveStatus(practiceID, practiceLocationID, this.ToString(), ValidateSetupData());
        }

        protected void btnRunParLevels_OnClick(object sender, EventArgs e)
        {
            if (ParLevelEndDate.SelectedDate.HasValue && ParLevelStartDate.SelectedDate.HasValue)
            {
                foreach (
                    Telerik.Web.UI.GridNestedViewItem nestedViewItem in
                        grdInventory.MasterTableView.GetItems(Telerik.Web.UI.GridItemType.NestedView))
                {
                    if (nestedViewItem.NestedTableViews.Length > 0)
                    {
                        foreach (Telerik.Web.UI.GridDataItem gridDataItem in nestedViewItem.NestedTableViews[0].Items)
                        {
                            if (!gridDataItem.Selected)
                                continue;

                            if (ParLevelEndDate.SelectedDate.HasValue && ParLevelStartDate.SelectedDate.HasValue)
                            {
                                var startDate = (DateTime) ParLevelStartDate.SelectedDate;
                                var endDate = (DateTime) ParLevelEndDate.SelectedDate;
                                HistoricalProductReceiptSummary summary =
                                    Domain.DispenseHistory.Utility.ReceiptHistoryAggregator
                                        .GenerateReceiptHistorySummary(
                                            findPracticeCatalogProductID(gridDataItem), practiceLocationID,
                                            startDate, endDate);

                                Label lbl = gridDataItem.FindControl("SuggestedParLevel") as Label;
                                lbl.Text = summary.AverageMonth != null
                                    ? Math.Ceiling(summary.AverageMonth.TotalQuantity).ToString()
                                    : "No History";

                            }
                        }
                    }
                }
            }
            else
            {
                
            }
        }
    }
}