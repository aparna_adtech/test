﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeLocationPhysicians.ascx.cs"
Inherits="BregVision.Admin.UserControls.PracticeLocation.PracticeLocationPhysicians" %>
<%@ Register Src="~/UserControls/ResetSaveButtonTable.ascx" TagName="ResetSaveButtonTable"
TagPrefix="uc1" %>
<div class="flex-row actions-bar">
  <h2>
    <asp:Label ID="lblContentHeader" runat="server" Text="Location Physicians"></asp:Label>
    <bvu:Help ID="help44" runat="server" HelpID="44"/>
  </h2>
  <uc1:ResetSaveButtonTable ID="ucResetSaveButtonTable" runat="server"></uc1:ResetSaveButtonTable>
</div>
<div>
  <asp:Label ID="lblPracticeLocationPhysicians" runat="server"></asp:Label>
</div>
<div class="flex-row justify-spaceAround">
  <div>
    <h3>Practice Clinicians</h3>
    <asp:ListBox ID="listBoxPracticeClinicians" runat="server" Height="280px" SelectionMode="Multiple" Width="220px">
    </asp:ListBox>
  </div>
  <div class="flex-column align-center">
    <bvu:Help ID="help45" runat="server" HelpID="45"/>
    <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="Add &raquo;" ToolTip="Add selected clinician(s) to location" Width="100px"/>
    <br />
    <asp:Button ID="btnRemove" runat="server" OnClick="btnRemove_Click" Text=" &laquo; Remove"
                ToolTip="Remove selected clinician(s) from location" Width="100px"/>
    <bvu:Help ID="help46" runat="server" HelpID="46"/>
  </div>
  <div>
    <h3>Practice Location Clinicians</h3>
    <asp:ListBox ID="listBoxPracticeLocationClinicians" runat="server" Height="280px"
                 SelectionMode="Multiple" Width="220px">
    </asp:ListBox>
  </div>
</div>
