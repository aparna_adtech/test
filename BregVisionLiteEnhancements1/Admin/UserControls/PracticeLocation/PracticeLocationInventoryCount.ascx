﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeLocationInventoryCount.ascx.cs" Inherits="BregVision.Admin.UserControls.PracticeLocation.PracticeLocationInventoryCount" %>

<%@ Register Src="~/UserControls/InventoryCount/InventoryCycleType.ascx" TagPrefix="bvl" TagName="InventoryCycleType" %>
<%@ Register Src="~/UserControls/InventoryCount/InventoryCycle.ascx" TagPrefix="bvl" TagName="InventoryCycle"  %>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        $(function () {
            $telerik.isIE7 = false;
            $telerik.isIE6 = false;
        });
    </script>
</telerik:RadScriptBlock>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnCloseInventory">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="lblPCStatus" />
                <telerik:AjaxUpdatedControl ControlID="txtTitle" />
                <telerik:AjaxUpdatedControl ControlID="grdInventory" />
                <telerik:AjaxUpdatedControl ControlID="ctlInventoryCycleType" />
                <telerik:AjaxUpdatedControl ControlID="ctlInventoryCycle" />
                <telerik:AjaxUpdatedControl ControlID="litConsignmentEmail" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnCloseInventory1">     
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="lblPCStatus" />
                <telerik:AjaxUpdatedControl ControlID="txtTitle" />
                <telerik:AjaxUpdatedControl ControlID="grdInventory"  />
                <telerik:AjaxUpdatedControl ControlID="ctlInventoryCycleType" />
                <telerik:AjaxUpdatedControl ControlID="ctlInventoryCycle" />
                <telerik:AjaxUpdatedControl ControlID="litConsignmentEmail" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnUpdateInventory">     
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="lblPCStatus" />
                <telerik:AjaxUpdatedControl ControlID="txtTitle" />
                <telerik:AjaxUpdatedControl ControlID="grdInventory"  />
                <telerik:AjaxUpdatedControl ControlID="ctlInventoryCycleType" />
                <telerik:AjaxUpdatedControl ControlID="ctlInventoryCycle" />
                <telerik:AjaxUpdatedControl ControlID="litConsignmentEmail" />
            </UpdatedControls>
        </telerik:AjaxSetting>        
        <telerik:AjaxSetting AjaxControlID="ctlInventoryCycleType">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="ctlInventoryCycleType" />
                <telerik:AjaxUpdatedControl ControlID="ctlInventoryCycle" />
                <telerik:AjaxUpdatedControl ControlID="txtTitle" />
                <telerik:AjaxUpdatedControl ControlID="grdInventory"  />
                <telerik:AjaxUpdatedControl ControlID="btnCloseInventory" />
                <telerik:AjaxUpdatedControl ControlID="btnCloseInventory1" />
                <telerik:AjaxUpdatedControl ControlID="btnUpdateInventory" />
            </UpdatedControls>
        </telerik:AjaxSetting>

                <telerik:AjaxSetting AjaxControlID="ctlInventoryCycle">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="ctlInventoryCycle" />
                <telerik:AjaxUpdatedControl ControlID="txtTitle" />
                <telerik:AjaxUpdatedControl ControlID="grdInventory"  />
                <telerik:AjaxUpdatedControl ControlID="btnCloseInventory" />
                <telerik:AjaxUpdatedControl ControlID="btnCloseInventory1" />
                <telerik:AjaxUpdatedControl ControlID="btnUpdateInventory" />
            </UpdatedControls>
        </telerik:AjaxSetting>

        <telerik:AjaxSetting AjaxControlID="grdInventory">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="grdInventory" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" InitialDelayTime="500" EnableAjaxSkinRendering="True" BackgroundPosition="Center" Skin="Breg" EnableEmbeddedSkins="False">
    <img src="App_Themes/Breg/images/Common/loading.gif" />
  </telerik:RadAjaxLoadingPanel>
<telerik:RadInputManager ID="RadInputManager1" runat="server" Skin="Office2007">
    <telerik:NumericTextBoxSetting BehaviorID="numberSetting" Culture="en-US" DecimalDigits="0"
        DecimalSeparator="." GroupSeparator="," GroupSizes="3" MaxValue="999" MinValue="0"
        NegativePattern="-n" PositivePattern="n" SelectionOnFocus="SelectAll">
    </telerik:NumericTextBoxSetting>
</telerik:RadInputManager>
<div style="margin: 10px; padding: 10px; text-align: left; width: 97%; border-style:solid; border-width:thin;"> 
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin-bottom: 20px;">
    <tr valign="middle" style="margin-bottom: 5px;">
        <td>
            <table>
                <tr valign="middle">
                    <td>
                        <asp:Label ID="lblContentHeader" runat="server" Text="Inventory Levels" CssClass="PageTitle" OnLoad="lblContentHeader_Load"></asp:Label>
                    </td>
                    <td>
                        
                    </td>
                </tr>
            </table>
        </td>
        <td align="right">

        </td>
    </tr>
</table>  
    <table width="100%" cellpadding="0" border="0" cellspacing="0">
     <tr valign="bottom" style="padding-bottom: 5px;">
        <td>
            <table style="margin: 0 auto;">
                <tr valign="middle">
                    <td>
                        <span class="PageTitle">Open Inventory Cycle Count</span>
                    </td>
                    <td>
                        <!--help-->
                    </td>
                </tr>
            </table>
            <div style="text-align: center;">
                <asp:Label ID="lblPCStatus" runat="server" CssClass="WarningMsg"></asp:Label></div>  
                   
        </td>
        <td>
            
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table>
                <tr>
                    <td>
                        <bvl:InventoryCycleType ID="ctlInventoryCycleType" runat="server" />
                    </td>
                    <td>
                        <bvl:InventoryCycle ID="ctlInventoryCycle" runat="server" />
                    </td>
                </tr>
            </table>
            
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table>
                <tr>
                    <td>
                        <asp:Button ID="btnCloseInventory" runat="server" OnClick="btnCloseInventory_Click" Text="Close Inventory" Width="150px" OnClientClick="if(!confirm('Are you sure you want to close this cycle?')) return false;"  />
                    </td>
                    <td>
                        <asp:Label ID="lblTitle" runat="server" Text="Inventory Cycle Title"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
                    </td>
                    <td align="center">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/NotReconciledLegend.gif" />
                    </td>
                </tr>
            </table>
            
            
        </td>
    </tr>
    <tr valign="top">
        <td>
             

            <bvu:VisionRadGridAjax ID="grdInventory" runat="server" AllowMultiRowSelection="True"
                EnableViewState="true"
                OnNeedDataSource="grdInventory_NeedDataSource" 
                OnItemDataBound="grdInventory_ItemDataBound"
                OnPageIndexChanged="grdInventory_PageIndexChanged"
                OnPreRender="grdInventory_PreRender" AllowPaging="true"
                ShowGroupPanel="True" Width="100%"
                AddNonBreakingSpace="false">
                <MasterTableView AutoGenerateColumns="False" DataKeyNames="ProductInventoryID" EnableTheming="true"
                    EnableViewState="true" Name="mtvInventory" HierarchyLoadMode="ServerOnDemand">
                    <Columns>
                        <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" />
                        <telerik:GridBoundColumn DataField="ProductInventoryID" UniqueName="ProductInventoryID" Visible="False" />
                        <telerik:GridBoundColumn DataField="SupplierShortName" HeaderText="SupplierShortName" UniqueName="SupplierShortName" />
                        <telerik:GridBoundColumn DataField="BrandShortName" HeaderText="BrandShortName" UniqueName="BrandShortName" />
                        <telerik:GridBoundColumn DataField="ProductName" HeaderText="ProductName" UniqueName="ProductName" />     
                        <telerik:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code" />   
                        <telerik:GridBoundColumn DataField="ParLevel" HeaderText="Par" UniqueName="ParLevel" />                  
                        <telerik:GridBoundColumn DataField="QOH" HeaderText="Initial QOH" UniqueName="QOH" />
                        <telerik:GridBoundColumn DataField="QuantityOnOrder" HeaderText="QuantityOnOrder" UniqueName="QuantityOnOrder" />
                        <telerik:GridTemplateColumn DataField="InventoryCount" HeaderStyle-Width="50px" HeaderStyle-Wrap="true"
                            UniqueName="InventoryCount">
                            <HeaderTemplate>
                                <span>Updated QOH&nbsp;</span>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtInventoryCount" runat="server" Font-Size="8pt" Text='<%# bind("InventoryCount") %>' 
                                    Width="60px" OnLoad="TextBoxAddToRadInputManager_Load"></asp:TextBox>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="ReOrderQuantity" HeaderText="Reorder" UniqueName="ReOrderQuantity" />
                        <telerik:GridTemplateColumn DataField="IsRMA" HeaderStyle-Width="50px" HeaderStyle-Wrap="true"
                            UniqueName="IsRMA">
                            <HeaderTemplate>
                                <span>Is RMA&nbsp;</span>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkIsRMA" runat="server" Checked='<%# bind("IsRMA") %>' Visible="true"  />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn DataField="IsBuyout" HeaderStyle-Width="50px" HeaderStyle-Wrap="true"
                            UniqueName="IsBuyout">
                            <HeaderTemplate>
                                <span>Is Buyout&nbsp;</span>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkIsBuyout" runat="server" Checked='<%# bind("IsBuyout") %>' Visible="true" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="IsCounted" UniqueName="IsCounted" Visible="False" />
                        <telerik:GridBoundColumn DataField="InventoryCount" UniqueName="InventoryCountHdn" Visible="false" />
                    </Columns>
                </MasterTableView>
                <ExportSettings>
                    <Pdf PageBottomMargin="" PageFooterMargin="" PageHeaderMargin="" PageHeight="11in"
                        PageLeftMargin="" PageRightMargin="" PageTopMargin="" PageWidth="8.5in" />
                </ExportSettings>
            </bvu:VisionRadGridAjax>

            <asp:Button ID="btnCloseInventory1" runat="server" OnClick="btnCloseInventory_Click"
                            Text="Close Inventory" Width="150px" OnClientClick="if(!confirm('Are you sure you want to close this cycle?')) return false;"  />

        </td>
        <td style="padding-left: 5px; padding-top: 10px;">
            <asp:Button ID="btnUpdateInventory" runat="server" OnClick="btnUpdateInventory_Click"  Text="Update Inventory Counts" Width="200px" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Literal ID="litConsignmentEmail" runat="server"></asp:Literal>
        </td>
    </tr>
</table>
</div>