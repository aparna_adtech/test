﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeLocationInventoryLevels.ascx.cs"
  Inherits="BregVision.Admin.UserControls.PracticeLocation.PracticeLocationInventoryLevels" %>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
  <AjaxSettings>
    <telerik:AjaxSetting AjaxControlID="txtRecordsDisplayed">
      <UpdatedControls>
        <telerik:AjaxUpdatedControl ControlID="grdPracticeCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
        <telerik:AjaxUpdatedControl ControlID="grdInventory" LoadingPanelID="RadAjaxLoadingPanel1" />
        <telerik:AjaxUpdatedControl ControlID="RadInputManager1" />
      </UpdatedControls>
    </telerik:AjaxSetting>
    <telerik:AjaxSetting AjaxControlID="btnUpdateInventory">
      <UpdatedControls>
        <telerik:AjaxUpdatedControl ControlID="lblPCStatus" />
        <telerik:AjaxUpdatedControl ControlID="grdInventory" LoadingPanelID="RadAjaxLoadingPanel1" />
        <telerik:AjaxUpdatedControl ControlID="RadInputManager1" />
      </UpdatedControls>
    </telerik:AjaxSetting>
    <telerik:AjaxSetting AjaxControlID="chkConsignmentLockParLevelAll">
      <UpdatedControls>
        <telerik:AjaxUpdatedControl ControlID="lblPCStatus" />
        <telerik:AjaxUpdatedControl ControlID="grdInventory" LoadingPanelID="RadAjaxLoadingPanel1" />
        <telerik:AjaxUpdatedControl ControlID="RadInputManager1" />
      </UpdatedControls>
    </telerik:AjaxSetting>
    <telerik:AjaxSetting AjaxControlID="btnDeleteAll">
      <UpdatedControls>
        <telerik:AjaxUpdatedControl ControlID="lblDeleteMessage" />
        <telerik:AjaxUpdatedControl ControlID="grdInventory" LoadingPanelID="RadAjaxLoadingPanel1" />
        <telerik:AjaxUpdatedControl ControlID="RadInputManager1" />
      </UpdatedControls>
    </telerik:AjaxSetting>
    <telerik:AjaxSetting AjaxControlID="grdPracticeCatalog">
      <UpdatedControls>
        <telerik:AjaxUpdatedControl ControlID="grdPracticeCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
      </UpdatedControls>
    </telerik:AjaxSetting>
    <telerik:AjaxSetting AjaxControlID="btnMoveToPC">
      <UpdatedControls>
        <telerik:AjaxUpdatedControl ControlID="lblPCStatus" />
        <telerik:AjaxUpdatedControl ControlID="grdPracticeCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
        <telerik:AjaxUpdatedControl ControlID="grdInventory" LoadingPanelID="RadAjaxLoadingPanel1" />
        <telerik:AjaxUpdatedControl ControlID="RadInputManager1" />
      </UpdatedControls>
    </telerik:AjaxSetting>
    <telerik:AjaxSetting AjaxControlID="grdInventory">
      <UpdatedControls>
        <telerik:AjaxUpdatedControl ControlID="lblDeleteMessage" />
        <telerik:AjaxUpdatedControl ControlID="grdInventory" LoadingPanelID="RadAjaxLoadingPanel1" />
        <telerik:AjaxUpdatedControl ControlID="RadInputManager1" />
      </UpdatedControls>
    </telerik:AjaxSetting>
  </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" InitialDelayTime="500" EnableAjaxSkinRendering="True" BackgroundPosition="Center" Skin="Breg" EnableEmbeddedSkins="False">
  <img src="../App_Themes/Breg/images/Common/loading.gif" />
</telerik:RadAjaxLoadingPanel>

<div class="flex-column flex-grow no-max-width">
  <telerik:RadInputManager ID="RadInputManager1" runat="server" Skin="Breg">
    <telerik:NumericTextBoxSetting BehaviorID="numberSetting" Culture="en-US" DecimalDigits="0"
      DecimalSeparator="." GroupSeparator="," GroupSizes="3" MaxValue="999" MinValue="0"
      NegativePattern="-n" PositivePattern="n" SelectionOnFocus="SelectAll">
    </telerik:NumericTextBoxSetting>
  </telerik:RadInputManager>
  <div class="flex-row lastChildLeft">
    <h2 class="no-wrap">
      <asp:Label ID="lblContentHeader" runat="server" Text="Inventory Levels" CssClass="PageTitle" OnLoad="lblContentHeader_Load"></asp:Label>
      <bvu:Help ID="help54" runat="server" HelpID="54" />
    </h2>
    <telerik:RadNumericTextBox ID="txtRecordsDisplayed" runat="server" AutoPostBack="true" Label="Records per Page"
      Culture="English (United States)" MaxValue="100" MinValue="0"
      NumberFormat-DecimalDigits="0" OnTextChanged="txtRecordsDisplayed_TextChanged"
      ShowSpinButtons="false" Skin="Breg" EnableEmbeddedSkins="False">
    </telerik:RadNumericTextBox>
  </div>
  <div class="flex-row">
    <h3 class="no-wrap">Practice Catalog&nbsp;<bvu:Help ID="help1" runat="server" HelpID="18" />
    </h3>
  </div>
  <div class="flex-row actions-bar-last-child-left">
    <div class="flex-row search-container">
      <bvu:SearchToolbar ID="ProductSearch" runat="server"></bvu:SearchToolbar>
      <bvu:Help ID="help21" runat="server" HelpID="21" />
    </div>
    <div class="flex-row flex-no-space-between-children" style="margin-left:390px ">
      <asp:UpdatePanel runat="server" UpdateMode="Always" RenderMode="Inline">
        <ContentTemplate>
          <asp:Label ID="AddToInventoryCompletedLabel" runat="server" Text="Product(s) added" Visible="false" EnableViewState="false" />
        </ContentTemplate>
      </asp:UpdatePanel>
      <bvu:Help ID="help63" runat="server" HelpID="63" />
      <asp:Button ID="btnMoveToPC" runat="server" OnClick="btnMoveToPC_Click" Text="Add to Inventory"
        ToolTip="Move all selected items to Practice Catalog" CssClass="RadButton_Breg" />
    </div>
  </div>
  <div>
    <asp:Label ID="lblMCStatus" runat="server" CssClass="WarningMsg"></asp:Label>
  </div>
  <div class="flex-grow">
    <bvu:VisionRadGridAjax ID="grdPracticeCatalog" runat="server" AllowMultiRowSelection="True"
      PagerStyle-AlwaysVisible="true" OnNeedDataSource="grdPracticeCatalog_NeedDataSource"
      OnDetailTableDataBind="grdPracticeCatalog_OnDetailTableDataBind" OnPageIndexChanged="grdPracticeCatalog_PageIndexChanged"
      OnPreRender="grdPracticeCatalog_PreRender" AddNonBreakingSpace="false" Skin="Breg" EnableEmbeddedSkins="False" CssClass="flex-grow">
      <MasterTableView DataKeyNames="PracticeCatalogSupplierBrandID" HierarchyLoadMode="ServerOnDemand">
        <DetailTables>
          <telerik:GridTableView runat="server" DataKeyNames="PracticeCatalogSupplierBrandID"
            Name="grdtvPracticeCatalog">
            <ParentTableRelation>
              <telerik:GridRelationFields DetailKeyField="PracticeCatalogSupplierBrandID" MasterKeyField="PracticeCatalogSupplierBrandID" />
            </ParentTableRelation>
            <Columns>
              <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" />
              <telerik:GridBoundColumn DataField="ShortName" HeaderText="Name"
                UniqueName="ShortName" />
              <telerik:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code" HeaderStyle-HorizontalAlign="Center"
                ItemStyle-HorizontalAlign="Center" />
              <telerik:GridBoundColumn DataField="LeftRightSide" HeaderText="Side" UniqueName="LeftRightSide"
                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
              <telerik:GridBoundColumn DataField="Size" HeaderText="Size" UniqueName="Size" HeaderStyle-HorizontalAlign="Center"
                ItemStyle-HorizontalAlign="Center" />
              <telerik:GridBoundColumn DataField="Gender" HeaderText="Gender" UniqueName="Gender"
                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
              <telerik:GridBoundColumn DataField="WholesaleCost" DataFormatString="{0:C}" HeaderText="Wholesale Cost"
                UniqueName="WholesaleCost" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
              <telerik:GridBoundColumn DataField="BillingCharge" DataFormatString="{0:C}" HeaderText="Billing Charge"
                UniqueName="BillingCharge" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
              <telerik:GridBoundColumn DataField="BillingChargeCash" DataFormatString="{0:C}" HeaderText="Billing Charge Cash"
                UniqueName="BillingChargeCash" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
              <telerik:GridBoundColumn DataField="DMEDeposit" DataFormatString="{0:C}" HeaderText="DME Deposit"
                UniqueName="DMEDeposit" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
              <telerik:GridCheckBoxColumn DataField="IsLogoAblePart" UniqueName="IsLogoAblePart" HeaderText="Logo Part"
                HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
              <telerik:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" UniqueName="PracticeCatalogSupplierBrandID"
                Visible="false" />
              <telerik:GridBoundColumn DataField="PracticeCatalogProductID" UniqueName="PracticeCatalogProductID"
                Visible="false" />
              <telerik:GridBoundColumn DataField="MasterCatalogProductID" UniqueName="MasterCatalogProductID"
                Visible="false" />
            </Columns>
          </telerik:GridTableView>
        </DetailTables>
        <Columns>
          <telerik:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" UniqueName="PracticeCatalogSupplierBrandID"
            Visible="false" />
          <telerik:GridBoundColumn DataField="SupplierName" HeaderText="Supplier" UniqueName="SupplierName"
            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
          <telerik:GridBoundColumn DataField="BrandName" HeaderText="Brand" UniqueName="BrandName"
            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
          <telerik:GridBoundColumn DataField="ProductCount" Visible="false" UniqueName="ProductCount" />
        </Columns>
      </MasterTableView>
    </bvu:VisionRadGridAjax>
  </div>
  <br />
  <div class="flex-row">
    <h3>Inventory
        <bvu:Help ID="help55" runat="server" HelpID="55" />
    </h3>
    <div class="flex-row flex-row-align-start flex-no-space-between-children calculate-par-levels-row" style="margin-left: 420px;">
      <telerik:RadDatePicker ID="ParLevelStartDate" runat="server" Skin="Breg" EnableEmbeddedSkins="False">
        <DateInput runat="server" DisplayDateFormat="MM/dd/yy" Placeholder="Start Date" />
        <Calendar runat="server" ShowRowHeaders="false" ImagesPath="~/App_Themes/Breg/images/Common/" />
      </telerik:RadDatePicker>
      <telerik:RadDatePicker ID="ParLevelEndDate" runat="server" Skin="Breg" EnableEmbeddedSkins="False">
        <DateInput runat="server" DisplayDateFormat="MM/dd/yy" Placeholder="End Date" />
        <Calendar runat="server" ShowRowHeaders="false" ImagesPath="~/App_Themes/Breg/images/Common/" />
      </telerik:RadDatePicker>
      <asp:Button ID="btnRunParLevels" runat="server" OnClick="btnRunParLevels_OnClick" Text="Calculate Par Levels" CssClass="button-primary" />
    </div>
  </div>
  <div class="flex-row actions-bar-last-child-left flex-no-space-between-children">
    <div class="flex-row search-container">
      <bvu:SearchToolbar ID="InventorySearch" runat="server"></bvu:SearchToolbar>
      <bvu:Help ID="help23" runat="server" HelpID="23" />
    </div>
    <div class="flex-row" style="margin-left: 200px; ">
      <bvu:Help ID="help57" runat="server" HelpID="57" />
      <asp:Button ID="btnDeleteAll" runat="server" OnClick="btnDeleteAll_Click" Text="Delete All" CssClass="button-accent" />
      <bvu:Help ID="help56" runat="server" HelpID="56" />
      <asp:Button ID="btnUpdateInventory" runat="server" OnClick="btnUpdateInventory_Click" Text="Update Inventory Levels" CssClass="button-primary" />
    </div>
  </div>
  <div>
    <asp:Label ID="lblPCStatus" runat="server" CssClass="WarningMsg"></asp:Label>
    <asp:Label ID="lblDeleteMessage" runat="server" CssClass="WarningMsg"></asp:Label>
  </div>
  <bvu:VisionRadGridAjax ID="grdInventory" runat="server" AllowMultiRowSelection="True"
    EnableViewState="true" OnDeleteCommand="grdInventory_DeleteCommand" OnItemDataBound="grdInventory_ItemDataBound"
    OnNeedDataSource="grdInventory_NeedDataSource" OnDetailTableDataBind="grdInventory_OnDetailTableDataBind"
    OnPageIndexChanged="grdInventory_PageIndexChanged" OnPreRender="grdInventory_PreRender"
    ShowGroupPanel="True" OnItemCommand="grdInventory_ItemCommand" AddNonBreakingSpace="false" Skin="Breg" EnableEmbeddedSkins="False">
    <MasterTableView AutoGenerateColumns="False" DataKeyNames="SupplierID"
      EnableViewState="true" Name="mtvInventory" HierarchyLoadMode="ServerOnDemand">
      <Columns>
        <telerik:GridBoundColumn DataField="SupplierID" UniqueName="SupplierID" Visible="False" />
        <telerik:GridBoundColumn DataField="Brand" HeaderText="Brand" UniqueName="Brand" />
        <telerik:GridBoundColumn DataField="Name" HeaderText="Name" UniqueName="Name" />
        <telerik:GridBoundColumn DataField="LastName" UniqueName="LastName" Visible="false" />
        <telerik:GridBoundColumn DataField="PhoneWork" HeaderText="Phone" UniqueName="PhoneWork" />
        <telerik:GridBoundColumn DataField="Address" HeaderText="Address" UniqueName="Address" />
        <telerik:GridBoundColumn DataField="City" HeaderText="City" UniqueName="City" />
        <telerik:GridBoundColumn DataField="ZipCode" HeaderText="Zip Code" UniqueName="ZipCode" />
      </Columns>
      <DetailTables>
        <telerik:GridTableView runat="server" AutoGenerateColumns="False" DataKeyNames="SupplierID"
          Name="gtvInventoryDetails">
          <ParentTableRelation>
            <telerik:GridRelationFields DetailKeyField="SupplierID" MasterKeyField="SupplierID" />
          </ParentTableRelation>
          <Columns>
            <telerik:GridTemplateColumn ItemStyle-HorizontalAlign="Center"
              ItemStyle-VerticalAlign="Middle" UniqueName="DeleteColumn">
              <ItemTemplate>
                <asp:ImageButton ID="btnDelete" runat="server" CommandName="Delete" ImageAlign="Middle"
                  ImageUrl="~/App_Themes/Breg/images/Common/delete.png" />
              </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" />
            <%-- Hidden columns --%>
            <telerik:GridBoundColumn DataField="ProductInventoryID" Display="false" UniqueName="ProductInventoryID"
              Visible="false" />
            <telerik:GridBoundColumn DataField="IsDeletable" Display="false" UniqueName="IsDeletable"
              Visible="false" />
            <telerik:GridBoundColumn DataField="PracticeCatalogProductID" Display="false" UniqueName="PracticeCatalogProductID"
              Visible="false" />
            <telerik:GridBoundColumn DataField="SupplierID" UniqueName="SupplierID" Visible="false" />
            <telerik:GridBoundColumn DataField="Category" Display="false" UniqueName="Category" />
            <%-- Visible columns --%>
            <telerik:GridBoundColumn DataField="Product" HeaderText="Product" UniqueName="Product" HeaderStyle-Width="400px" ItemStyle-Width="400px" />
            <telerik:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code" HeaderStyle-HorizontalAlign="Center"
              ItemStyle-HorizontalAlign="Center" />
            <telerik:GridBoundColumn DataField="LeftRightSide" HeaderText="Side" UniqueName="LeftRightSide"
              HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
            <telerik:GridBoundColumn DataField="Size" HeaderText="Size" UniqueName="Size" HeaderStyle-HorizontalAlign="Center"
              ItemStyle-HorizontalAlign="Center" />
            <telerik:GridBoundColumn DataField="Gender" HeaderText="Gender" UniqueName="Gender"
              HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
            <telerik:GridBoundColumn DataField="WholesaleCost" DataFormatString="{0:C}" HeaderText="WLC"
              UniqueName="WholesaleCost" HeaderStyle-HorizontalAlign="Right"
              ItemStyle-HorizontalAlign="Right" />
            <telerik:GridTemplateColumn DataField="QOH" HeaderStyle-Wrap="true"
              UniqueName="TemplateColumn5">
              <HeaderTemplate>
                <span>On Hand</span>
                <bvu:Help ID="help59" runat="server" HelpID="59" />
              </HeaderTemplate>
              <ItemTemplate>
                <asp:TextBox ID="txtQOH" runat="server" Text='<%# Eval("QOH") %>' OnLoad="TextBoxAddToRadInputManager_Load">
                </asp:TextBox>
              </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn DataField="ParLevel" HeaderStyle-Wrap="true"
              UniqueName="ParLevel">
              <HeaderTemplate>
                <span>Par Level</span>
                <bvu:Help ID="help60" runat="server" HelpID="60" />
              </HeaderTemplate>
              <ItemTemplate>
                <asp:TextBox ID="txtParLevel" runat="server" Text='<%# Eval("ParLevel") %>' OnLoad="TextBoxAddToRadInputManager_Load">
                </asp:TextBox>
              </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn DataField="SuggestedParLevel" UniqueName="SuggestedParLevel" HeaderStyle-Wrap="true">
              <HeaderTemplate>
                <span>Sugg. Par Level</span>
              </HeaderTemplate>
              <ItemTemplate>
                <asp:Label runat="server" ID="suggestedParLevel"></asp:Label>
              </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn DataField="ReorderLevel" HeaderStyle-Wrap="true"
              UniqueName="TemplateColumn7">
              <HeaderTemplate>
                <span>Reorder Level&nbsp;</span>
                <bvu:Help ID="help61" runat="server" HelpID="61" />
              </HeaderTemplate>
              <ItemTemplate>
                <asp:TextBox ID="txtReorderLevel" runat="server" Text='<%# Eval("ReorderLevel") %>'
                  OnLoad="TextBoxAddToRadInputManager_Load">
                </asp:TextBox>
              </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn DataField="CriticalLevel" HeaderStyle-Wrap="true"
              UniqueName="TemplateColumn8">
              <HeaderTemplate>
                <span>Critical Level&nbsp;</span>
                <bvu:Help ID="help62" runat="server" HelpID="62" />
              </HeaderTemplate>
              <ItemTemplate>
                <asp:TextBox ID="txtCriticalLevel" runat="server" Text='<%# Eval("CriticalLevel") %>'
                  OnLoad="TextBoxAddToRadInputManager_Load">
                </asp:TextBox>
              </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn DataField="IsNotFlaggedForReorder"
              HeaderStyle-Wrap="true" UniqueName="IsNotFlaggedForReorder"
              HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
              <HeaderTemplate>
                <span>Is Not Flagged For Reorder&nbsp;</span>
                <bvu:Help ID="help107" runat="server" HelpID="107" />
              </HeaderTemplate>
              <ItemTemplate>
                <asp:CheckBox ID="chkIsNotFlaggedForReorder" runat="server" Checked='<%# Eval("IsNotFlaggedForReorder") %>'
                  Visible="true" />
              </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn DataField="IsLogoAblePart" HeaderStyle-Wrap="true"
              UniqueName="IsLogoAblePartPi" ItemStyle-HorizontalAlign="Center"
              ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center">
              <HeaderTemplate>
                <span>Logo Part</span>
              </HeaderTemplate>
              <ItemTemplate>
                <asp:CheckBox ID="chkIsLogoAblePart" runat="server" Checked='<%# Eval("IsLogoAblePart") is System.DBNull ? false : Eval("IsLogoAblePart") %>' />
              </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn DataField="IsConsignment"
              HeaderStyle-Wrap="true" UniqueName="IsConsignment" HeaderStyle-HorizontalAlign="Center"
              ItemStyle-HorizontalAlign="Center">
              <HeaderTemplate>
                <span>Is Consignment&nbsp;</span>
              </HeaderTemplate>
              <ItemTemplate>
                <asp:CheckBox ID="chkIsConsignment" runat="server" Checked='<%# Eval("IsConsignment") %>'
                  Visible="true" />
              </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn DataField="ConsignmentQuantity" HeaderStyle-Wrap="true"
              UniqueName="ConsignmentQuantity">
              <HeaderTemplate>
                <span>Consignment Quantity</span>
              </HeaderTemplate>
              <ItemTemplate>
                <asp:TextBox ID="txtConsignmentQuantity" runat="server" Text='<%# Eval("ConsignmentQuantity") %>'>
                </asp:TextBox>
              </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn DataField="ConsignmentLockParLevel"
              HeaderStyle-Wrap="true" UniqueName="ConsignmentLockParLevel"
              HeaderStyle-HorizontalAlign="Center"
              ItemStyle-HorizontalAlign="Center">
              <HeaderTemplate>
                <span>Consignment Lock Par&nbsp;</span>
                <asp:CheckBox ID="chkConsignmentLockParLevelAll" Text="Check All" runat="server" Visible="false"
                  AutoPostBack="true" OnCheckedChanged="chkConsignmentLockParLevelAll_OnCheckedChanged" OnInit="chkConsignmentLockParLevelAll_OnInit" />
              </HeaderTemplate>
              <ItemTemplate>
                <asp:CheckBox ID="chkConsignmentLockParLevel" runat="server" Checked='<%# Eval("ConsignmentLockParLevel") %>'
                  Visible="true" />
              </ItemTemplate>
            </telerik:GridTemplateColumn>
            <%-- Hidden columns --%>
            <telerik:GridBoundColumn DataField="IsConsignment" UniqueName="IsConsignmentHdn" Visible="false" />
            <telerik:GridBoundColumn DataField="ConsignmentQuantity" UniqueName="ConsignmentQuantityHdn" Visible="false" />
            <telerik:GridBoundColumn DataField="ConsignmentLockParLevel" UniqueName="ConsignmentLockParLevelHdn" Visible="false" />
            <telerik:GridBoundColumn DataField="isactive" UniqueName="isactive" Visible="false" />
          </Columns>
        </telerik:GridTableView>
      </DetailTables>
    </MasterTableView>
    <ExportSettings>
      <Pdf PageBottomMargin="" PageFooterMargin="" PageHeaderMargin="" PageHeight="11in"
        PageLeftMargin="" PageRightMargin="" PageTopMargin="" PageWidth="8.5in" />
    </ExportSettings>
  </bvu:VisionRadGridAjax>
</div>
