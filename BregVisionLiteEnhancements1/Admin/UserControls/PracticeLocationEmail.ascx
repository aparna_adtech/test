<%@ Control Language="C#" AutoEventWireup="true" Inherits="PracticeLocationEmail" CodeBehind="PracticeLocationEmail.ascx.cs" %>
<!-- Content table, tr, and td mght not neccessary here -->
<div id="tableContact" class="admin-form flex-column flex-grow admin-page">
  <div class="flex-grow">
    <asp:Label ID="lblOrderApprovalEmail" runat="server" CssClass="FieldLabel">Order Approval Email<bvu:Help ID="help48" runat="server" HelpID="48" />
    </asp:Label>
    <div>
      <asp:TextBox ID="txtOrderApprovalEmail" runat="server" MaxLength="200" CssClass="FieldLabel"></asp:TextBox>
    </div>
      <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtOrderApprovalEmail" Text="* Invalid Email" Display="Dynamic" ValidationExpression="((\s*;\s*)*\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)+" ValidationGroup="Address"/>
  </div>
  <div class="flex-grow">
    <asp:Label ID="lblOrderConfirmationEmail" runat="server" CssClass="FieldLabel">Order Confirmation Email<bvu:Help ID="help49" runat="server" HelpID="49" />
    </asp:Label>
    <div>
      <asp:TextBox ID="txtOrderConfirmationEmail" runat="server" MaxLength="200" CssClass="FieldLabel"></asp:TextBox>
    </div>
    <asp:RequiredFieldValidator ID="rfvOrderConfirmationEmail" runat="server" ControlToValidate="txtOrderConfirmationEmail" Text="* Email Id required" Display="Dynamic" ValidationGroup="Address"/>
    <asp:RegularExpressionValidator ID="revOrderConfirmationEmail" runat="server" ControlToValidate="txtOrderConfirmationEmail" Text="* Invalid Email" Display="Dynamic" ValidationExpression="((\s*;\s*)*\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)+" ValidationGroup="Address" />
  </div>
  <div class="flex-grow">
    <asp:Label ID="Label1" runat="server" CssClass="FieldLabel">Billing Dispense Email
      <bvu:Help ID="help51" runat="server" HelpID="51" />
    </asp:Label>
    <div>
      <asp:TextBox ID="txtEmailForBillingDispense" runat="server" MaxLength="200" CssClass="FieldLabel"></asp:TextBox>
    </div>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEmailForBillingDispense" Text="* Invalid Email" Display="Dynamic" ValidationExpression="((\s*;\s*)*\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)+" ValidationGroup="Address"/>
  </div>
  <div class="flex-grow">
    <asp:Label ID="Label2" runat="server" Text="Print Dispense Email" CssClass="FieldLabel"></asp:Label>
    <asp:TextBox ID="txtEmailForPrintDispense" runat="server" MaxLength="200" CssClass="FieldLabel"></asp:TextBox>
  </div>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtEmailForPrintDispense" Text="* Invalid Email" Display="Dynamic" ValidationExpression="((\s*;\s*)*\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)+" ValidationGroup="Address" />
  <table style="width: 100%">
    <tr id="trPrintDispenseFax" runat="server" visible="false">
      <td>
        <asp:Label ID="Label3" runat="server" Text="Print Dispense Fax" CssClass="FieldLabel"></asp:Label>
        <asp:TextBox ID="txtFaxForPrintDispense" runat="server" MaxLength="100" CssClass="FieldLabel"></asp:TextBox>
      </td>
    </tr>
  </table>
</div>
<!-- End of Content table, tr, and td are not neccessary here -->
