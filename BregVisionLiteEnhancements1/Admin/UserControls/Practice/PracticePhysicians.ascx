﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticePhysicians.ascx.cs"
Inherits="BregVision.Admin.UserControls.PracticePhysicians" %>

<div class="content">
<h2 class="flex-row justify-start">
  <asp:Label ID="lblContentHeader" runat="server" Text="Physicians"></asp:Label>
  <bvu:Help ID="Help15" runat="server" HelpID="15"/>
</h2>
<asp:Button ID="btnSortClinicians" CssClass="btnSortClinicians" runat="server" Text="Sort A-Z" OnClick="SortClinicians"/>
<asp:GridView ID="GridPracticePhysician" runat="server" AutoGenerateColumns="False"
              DataKeyNames="clinicianID, salutation, suffix" OnRowCancelingEdit="GridPracticePhysician_RowCancelingEdit"
              OnRowCommand="GridPracticePhysician_RowCommand" OnRowDataBound="GridPracticePhysician_RowDataBound"
              OnRowDeleting="GridPracticePhysician_RowDeleting" OnRowEditing="GridPracticePhysician_RowEditing"
              OnRowUpdating="GridPracticePhysician_RowUpdating" OnSorting="GridPracticePhysician_Sorting"
              ShowFooter="True" Skin="Breg" EnableEmbeddedStylesheets="False" CssClass="RadGrid_Breg">
<Columns>
<asp:TemplateField>
  <ItemTemplate>
    <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="True" CommandName="Delete"
                     AlternateText="Delete" ImageUrl="~/App_Themes/Breg/images/Common/delete.png"
                     ToolTip="Delete Physician"/>
  </ItemTemplate>
</asp:TemplateField>
<asp:TemplateField>
  <ItemTemplate>
    <asp:ImageButton runat="server" ID="btnUp" CommandName="MoveUp" CommandArgument='<%# Eval("clinicianID") %>' ToolTip="Click to move this row up" ImageUrl="~/RadControls/Grid/Skins/Office2007/MoveUp.gif"/>
    <asp:ImageButton runat="server" ID="btnDown" CommandName="MoveDown" CommandArgument='<%# Eval("clinicianID") %>' ToolTip="Click to move this row down" ImageUrl="~/RadControls/Grid/Skins/Office2007/MoveDown.gif"/>
  </ItemTemplate>
</asp:TemplateField>
<asp:TemplateField ShowHeader="False">
  <EditItemTemplate>
    <asp:ImageButton ID="btnEditUpdate" runat="server" CausesValidation="True" CommandName="Update"
                     ImageUrl="~/App_Themes/Breg/images/Common/checkCircle.png" ToolTip="Update"/>&nbsp;
    <asp:ImageButton ID="btnEditCancel" runat="server" CausesValidation="True" CommandName="Cancel" ImageUrl="~/App_Themes/Breg/images/Common/cancelCircle.png" ToolTip="Cancel"/>
  </EditItemTemplate>
  <ItemTemplate>
    <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="False" CommandName="Edit"
                     ImageUrl="~/App_Themes/Breg/images/Common/edit.png" ToolTip="Edit"/>
  </ItemTemplate>
  <FooterTemplate>
    <asp:ImageButton ID="ImageButtonAddNew" runat="server" CommandName="AddNew" ValidationGroup="PracticeGroup" ImageUrl="~\App_Themes\Breg\images\Common\add-record.png"
                     ToolTip="Add New Practice Physician"/>
    <asp:ImageButton ID="ImageButtonAddNewCancel" runat="server" CommandName="AddNewCancel"
                     ImageUrl="~/App_Themes/Breg/images/Common/cancelCircle.png" ToolTip="Cancel" Visible="false"
                     CausesValidation="false"/>
  </FooterTemplate>
</asp:TemplateField>
<asp:BoundField DataField="practiceID" HeaderText="PracticeID" SortExpression="practiceID"
                Visible="False"/>
<asp:TemplateField HeaderText="clinicianID" SortExpression="clinicianID" Visible="False">
  <EditItemTemplate>
    <asp:Label ID="lblclinicianID" runat="server" Text='<%# Eval("clinicianID") %>'
               Visible="false">
    </asp:Label>
  </EditItemTemplate>
  <ItemTemplate>
    <asp:Label ID="lblclinicianIDEdit" runat="server" Text='<%# Bind("clinicianID") %>'
               Visible="false">
    </asp:Label>
  </ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="ContactID" InsertVisible="False" SortExpression="contactID"
                   Visible="False">
  <EditItemTemplate>
    <asp:Label ID="lblContactIDEdit" runat="server" Text='<%# Eval("contactID") %>'
               Visible="false">
    </asp:Label>
  </EditItemTemplate>
  <ItemTemplate>
    <asp:Label ID="lblContactID" runat="server" Text='<%# Bind("contactID") %>'
               Visible="false">
    </asp:Label>
  </ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Prefix"
                   SortExpression="salutation">
  <EditItemTemplate>
    <div class="Dropdown-Container">
      <asp:DropDownList ID="ddlSalutation" runat="server" DataTextField="salutation" DataValueField="salutation">
      </asp:DropDownList>
    </div>
  </EditItemTemplate>
  <ItemTemplate>
    <asp:Label ID="lblSalutation" runat="server" Text='<%# Bind("salutation") %>'></asp:Label>
  </ItemTemplate>
  <FooterTemplate>
      <div class="Dropdown-Container">
    <asp:DropDownList ID="ddlNewSalutation" runat="server" DataTextField="salutation"
                          DataValueField="salutation" Visible="false">
        </asp:DropDownList>
      </div>
  </FooterTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderStyle-Wrap="true" HeaderText="First Name"
                   SortExpression="firstName">
  <EditItemTemplate>
    <asp:TextBox ID="txtFirstName" runat="server" MaxLength="25"
                 Text='<%# Bind("firstName") %>' Width="100px">
    </asp:TextBox>
    <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName"
                                ErrorMessage="Required" Text="*" ToolTip="First Name is a required field" Display="Dynamic">
    </asp:RequiredFieldValidator>
  </EditItemTemplate>
  <ItemTemplate>
    <asp:Label ID="lblFirstName" runat="server" Text='<%# Bind("firstName") %>'></asp:Label>
  </ItemTemplate>
  <FooterTemplate>
    <asp:TextBox ID="txtNewFirstName" runat="server" Visible="False" ValidationGroup="PracticeGroup"
                 Width="100px">
    </asp:TextBox>
    <asp:RequiredFieldValidator ID="rfvNewFirstName" runat="server" ControlToValidate="txtNewFirstName"
                                ErrorMessage="Required" Text="*" ToolTip="First Name is a required field" Visible="false"
                                Display="Dynamic" ValidationGroup="PracticeGroup">
    </asp:RequiredFieldValidator>
  </FooterTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderStyle-Wrap="true" HeaderText="Middle Name"
                   SortExpression="middleName">
  <EditItemTemplate>
    <asp:TextBox ID="txtMiddleName" runat="server" MaxLength="2"
                 Text='<%# Bind("middleName") %>' Width="20px">
    </asp:TextBox>
  </EditItemTemplate>
  <ItemTemplate>
    <asp:Label ID="lblMiddleName" runat="server" Text='<%# Bind("middleName") %>'></asp:Label>
  </ItemTemplate>
  <FooterTemplate>
    <asp:TextBox ID="txtNewMiddleName" runat="server" Visible="False"
                 Width="20px">
    </asp:TextBox>
  </FooterTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderStyle-Wrap="true" HeaderText="Last Name" SortExpression="lastName">
  <EditItemTemplate>
    <asp:TextBox ID="txtLastName" runat="server" MaxLength="25"
                 Text='<%# Bind("lastName") %>' Width="100px">
    </asp:TextBox><asp:RequiredFieldValidator
                    ID="rfvLastName" runat="server" ControlToValidate="txtLastName" ErrorMessage="Required"
                    Text="*" ToolTip="Last Name is a required field" Visible="false" Display="Dynamic">
    </asp:RequiredFieldValidator>
  </EditItemTemplate>
  <ItemTemplate>
    <asp:Label ID="lblLastName" runat="server" Text='<%# Bind("lastName") %>'></asp:Label>
  </ItemTemplate>
  <FooterTemplate>
    
    <asp:TextBox ID="txtNewLastName" runat="server" Visible="False" ValidationGroup="PracticeGroup"
                 Width="100px">
    </asp:TextBox>
      
      <asp:RequiredFieldValidator ID="rfvNewLastName" runat="server"
                                              ControlToValidate="txtNewLastName" ErrorMessage="Required" Text="*" ToolTip="Last Name is a required field"
                                              Visible="false" Display="Dynamic" ValidationGroup="PracticeGroup">
    </asp:RequiredFieldValidator>
  </FooterTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Suffix"
                   SortExpression="suffix">
  <EditItemTemplate>
    <div class="Dropdown-Container">
      <asp:DropDownList ID="ddlSuffix" runat="server" DataTextField="suffix"
                        DataValueField="suffix">
      </asp:DropDownList>
    </div>
  </EditItemTemplate>
  <ItemTemplate>
    <asp:Label ID="lblSuffix" runat="server" Text='<%# Bind("suffix") %>'></asp:Label>
  </ItemTemplate>
  <FooterTemplate>
         <div class="Dropdown-Container">
    <asp:DropDownList ID="ddlNewSuffix" runat="server" DataTextField="suffix"
                      DataValueField="suffix" Visible="false"></asp:DropDownList>
         </div>
  </FooterTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Provider"
                   SortExpression="isProvider">
  <EditItemTemplate>
    <asp:CheckBox ID="chkProvider" runat="server" Checked='<%# Bind("isProvider") %>'/>
  </EditItemTemplate>
  <ItemTemplate>
    <asp:CheckBox ID="viewChkProvider" runat="server" Checked='<%# Bind("isProvider") %>' Enabled="False"/>
  </ItemTemplate>
  <FooterTemplate>
    <asp:CheckBox ID="chkNewIsProvider" runat="server" Visible="False"/>
  </FooterTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Fitter"
                   SortExpression="isFitter">
  <EditItemTemplate>
    <asp:CheckBox ID="chkFitter" runat="server" Checked='<%# Bind("isFitter") %>'/>
  </EditItemTemplate>
  <ItemTemplate>
    <asp:CheckBox ID="viewChkFitter" runat="server" Checked='<%# Bind("isFitter") %>' Enabled="False"/>
  </ItemTemplate>
  <FooterTemplate>
    <asp:CheckBox ID="chkNewIsFitter" runat="server" Visible="False"/>
  </FooterTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="PIN" ItemStyle-HorizontalAlign="Left"
                   ItemStyle-Wrap="false">
  <EditItemTemplate>
    <asp:TextBox ID="txtPin" runat="server" MaxLength="4" Text='<%# Bind("Pin") %>'
                 TextMode="Password" Placeholder="Enter New PIN"/>
    &nbsp;--&ensp;or&ensp;--&nbsp;
    <asp:CheckBox ID="chkClearPin" runat="server" Text="Remove PIN"/>
  </EditItemTemplate>
  <ItemTemplate>
    <asp:Label ID="lblPin" runat="server" Text="****"></asp:Label>
  </ItemTemplate>
  <FooterTemplate>
    <asp:TextBox ID="txtNewPin" runat="server" Visible="False"
                 Width="30px">
    </asp:TextBox>
  </FooterTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderStyle-Width="80px" HeaderStyle-Wrap="true" HeaderText="EMR/PM Id" ItemStyle-Width="125px" SortExpression="cloudConnectId">
  <EditItemTemplate>
    <asp:TextBox ID="txtCloudConnectId" runat="server" CssClass="FieldLabel" MaxLength="50" Text='<%# Bind("cloudConnectId") %>' Width="100px"></asp:TextBox>
  </EditItemTemplate>
  <ItemTemplate>
    <asp:Label ID="lblCloudConnectId" runat="server" CssClass="FieldLabel" Text='<%# Bind("CloudConnectId") %>'></asp:Label>
  </ItemTemplate>
  <FooterTemplate>
    <asp:TextBox ID="txtNewCloudConnectId" runat="server" CssClass="FieldLabel" Visible="False" Width="100px"></asp:TextBox>
  </FooterTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderStyle-Width="60px" HeaderText="Sort Order" ItemStyle-Width="60px"
                   SortExpression="SortOrder" Visible="false">
  <EditItemTemplate>
    <asp:TextBox ID="txtSortOrder" runat="server" Text='<%# Bind("SortOrder") %>'
                 Visible="false">
    </asp:TextBox>
  </EditItemTemplate>
  <ItemTemplate>
    <asp:Label ID="lblSortOrder" runat="server" Text='<%# Bind("SortOrder") %>'
               Visible="false">
    </asp:Label>
  </ItemTemplate>
  <FooterTemplate>
    <asp:TextBox ID="txtNewSortOrder" runat="server" Visible="false"></asp:TextBox>
  </FooterTemplate>
</asp:TemplateField>
</Columns>
</asp:GridView>
</div>
