﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeProducts.ascx.cs" Inherits="BregVision.Admin.UserControls.PracticeProducts" %>
<%@ Register Src="~/BregAdmin/UserControls/MasterCatalogAdmin.ascx" TagName="MasterCatalogAdmin" TagPrefix="bv" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
  <AjaxSettings>
    <telerik:AjaxSetting AjaxControlID="RadGrid1">
      <UpdatedControls>
        <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1"/>
      </UpdatedControls>
    </telerik:AjaxSetting>
  </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" InitialDelayTime="500" EnableAjaxSkinRendering="True" BackgroundPosition="Center" Skin="Breg" EnableEmbeddedSkins="False">
  <asp:Image runat="server" ImageUrl="../../../App_Themes/Breg/images/Common/loading.gif"/>
</telerik:RadAjaxLoadingPanel>
<bv:MasterCatalogAdmin ID="ctlMasterCatalogAdmin" EnableProductReplacement="false" runat="server" Visible="true"/>
<div class="content">
<div class="flex-row">
  <h2>
    <asp:Label ID="lblContentHeader" runat="server" Text="Third Party Supplier Products"></asp:Label>
    <bvu:Help ID="help127" runat="server" HelpID="127"/>
  </h2>
  <asp:Label ID="lblCommandName" runat="server" Text=""></asp:Label>
</div>
<bvu:VisionRadGridAjax ID="RadGrid1" runat="server" EnableLinqExpressions="false" OnItemCreated="RadGrid1_ItemCreated" OnCancelCommand="RadGrid1_CancelCommand" OnDeleteCommand="RadGrid1_DeleteCommand" OnDetailTableDataBind="RadGrid1_DetailTableDataBind" OnInsertCommand="RadGrid1_InsertCommand" OnItemCommand="RadGrid1_ItemCommand" OnItemDataBound="RadGrid1_ItemDataBound" OnNeedDataSource="RadGrid1_NeedDataSource" OnPreRender="RadGrid1_PreRender" PageSize="100" OnUpdateCommand="RadGrid1_UpdateCommand" Skin="Breg" EnableEmbeddedSkins="False" AddNonBreakingSpace="False">
<MasterTableView DataKeyNames="PracticeID, ThirdPartySupplierID, LinkThirdPartySupplierID, PracticeCatalogSupplierBrandID" EnableNoRecordsTemplate="true" HierarchyLoadMode="ServerOnDemand" Name="mtvVendor">
<Columns>
  <telerik:GridBoundColumn DataField="Vendor" HeaderText="Supplier" ItemStyle-HorizontalAlign="left"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="PracticeID" HeaderText="" Visible="false"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="ThirdPartySupplierID" HeaderText="" Visible="false"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="LinkThirdPartySupplierID" HeaderText="LinkThirdPartySupplierID" Visible="false"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" HeaderText="PracticeCatalogSupplierBrandID" Visible="false"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="IsVendor" DataType="System.Byte" HeaderText="IsVendor" Visible="false"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="Sequence" HeaderText="LinkThirdPartySupplierID" Visible="false"></telerik:GridBoundColumn>
</Columns>
<DetailTables>
<telerik:GridTableView EnableColumnsViewState="false" Caption="" CommandItemDisplay="Top" DataKeyNames="PracticeCatalogSupplierBrandID, ThirdPartyProductID, PracticeCatalogProductID" HierarchyLoadMode="ServerOnDemand" Name="gtvSupplierProduct" PagerStyle-HorizontalAlign="Center" PageSize="15">
  <ParentTableRelation>
    <telerik:GridRelationFields DetailKeyField="PracticeCatalogSupplierBrandID" MasterKeyField="PracticeCatalogSupplierBrandID"/>
  </ParentTableRelation>
  <Columns>
    <telerik:GridTemplateColumn UniqueName="Promote" Visible="false">
      <ItemTemplate>
        <asp:ImageButton ID="btnPromote" runat="server" ImageUrl="~/RadControls/Grid/Skins/Office2007/MoveUp.gif" CommandName="Promote" Visible='<%# Page.User.IsInRole("BregAdmin") %>'/>
      </ItemTemplate>
    </telerik:GridTemplateColumn>
    <telerik:GridButtonColumn ButtonType="ImageButton" CommandArgument="Copy" CommandName="InitInsert" ImageUrl="~/App_Themes/Breg/images/Common/copy.png" UniqueName="btnCopy" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridButtonColumn>
    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" ImageUrl="~/App_Themes/Breg/images/Common/delete.png" UniqueName="btnDelete" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridButtonColumn>
    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" ImageUrl="~/App_Themes/Breg/images/Common/edit.png" UniqueName="btnEdit" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridButtonColumn>
    <telerik:GridBoundColumn DataField="RowNumber" DataType="System.Int32" HeaderText="#" UniqueName="RowNumber" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="ShortName" DataType="System.String" HeaderText="Product" UniqueName="ShortName" Visible="true"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="Code" DataType="System.String" HeaderText="Code" UniqueName="Code" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="Packaging" DataType="System.String" HeaderText="Pkg." Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="Side" DataType="System.String" HeaderText="Side" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="Size" DataType="System.String" HeaderText="Size" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
    <telerik:GridTemplateColumn HeaderText="Modifiers" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
      <ItemTemplate>
        <span class="comma-list">
          <span><%#  Eval("Mod1") %></span>
          <span><%#  Eval("Mod2") %></span>
          <span><%#  Eval("Mod3") %></span>
        </span>
      </ItemTemplate>
    </telerik:GridTemplateColumn>
    <telerik:GridBoundColumn DataField="Mod1" DataType="System.String" HeaderText="Mod" Visible="false"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="Mod2" DataType="System.String" HeaderText="Mod" Visible="false"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="Mod3" DataType="System.String" HeaderText="Mod" Visible="false"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="Gender" DataType="System.String" HeaderText="Gender" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="Color" DataType="System.String" HeaderText="Color" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="HCPCSString" DataType="System.String" HeaderText="HCPCs" Visible="true"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="WholesaleCost" DataFormatString="{0:C}" DataType="System.Decimal" HeaderText="Wholesale Cost" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"/>
    <telerik:GridBoundColumn DataField="UPCCode" DataType="System.String" HeaderText="UPC Code" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="IsDiscontinued" DataType="System.Boolean" HeaderText="IsDiscontinued" Visible="false"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="IsDeletable" DataType="System.Boolean" HeaderText="IsDeletable" Visible="false"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="ThirdPartyProductID" DataType="System.Int32" HeaderText="ThirdPartyProductID" Visible="false"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" DataType="System.Int32" HeaderText="PracticeCatalogSupplierBrandID" Visible="false"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="PracticeCatalogProductID" DataType="System.Int32" HeaderText="PracticeCatalogProductID" Visible="false"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="UserMessage" DataType="System.String" HeaderText="UserMessage" Visible="false"></telerik:GridBoundColumn>
  </Columns>
  <EditFormSettings EditFormType="Template">
    <EditColumn UniqueName="EditCommandColumn1"></EditColumn>
    <FormTemplate>
      <table id="Table2">
        <!-- old buttons -->
        <tr id="trPromoteHeader" runat="server" visible="false">
          <td colspan="1">Master Catalog Supplier</td>
          <td colspan="1">Category</td>
          <td colspan="8" align="left">Sub-Category</td>
        </tr>
        <tr id="trPromote" runat="server" visible="false">
          <td colspan="1">
            <asp:DropDownList ID="ddlSuppliers" runat="server" EnableViewState="true" OnSelectedIndexChanged="ddlSuppliers_OnSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
          </td>
          <td colspan="1">
            <asp:DropDownList ID="ddlCategories" runat="server" EnableViewState="true" OnSelectedIndexChanged="ddlCategories_OnSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
          </td>
          <td colspan="8" align="left">
            <asp:DropDownList ID="ddlSubCategories" runat="server" EnableViewState="true" OnLoad="ddlSubCategories_OnLoad"></asp:DropDownList>
          </td>
        </tr>
        <tr style='<%# string.IsNullOrWhiteSpace(Eval("UserMessage").ToString()) ? " display: none" : "" %>'>
          <td colspan="11">
            <asp:Label ID="lblUserMessage" runat="server" Text='<%# Eval("UserMessage") %>'> </asp:Label>
            <asp:ValidationSummary ID="validationSummary" runat="server" HeaderText="Please correct the following:" Visible="true"/>
          </td>
        </tr>
        <tr>
          <td>Product</td>
          <td nowrap="nowrap">
            <asp:TextBox ID="txtProduct" runat="server" MaxLength="50" Text='<%# Eval("ShortName") %>'> </asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator_txtProduct" runat="server" ControlToValidate="txtProduct" ErrorMessage="Product is a required field." Font-Overline="true" SetFocusOnError="true" Text="*"> </asp:RequiredFieldValidator>
          </td>
          <td>Code</td>
          <td nowrap="nowrap">
            <asp:TextBox ID="txtCode" runat="server" MaxLength="20" Text='<%# Eval("Code") %>'> </asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator_txtCode" runat="server" ControlToValidate="txtCode" ErrorMessage="Code is a required field." SetFocusOnError="true" Text="*"> </asp:RequiredFieldValidator>
          </td>
          <td>Pkg</td>
          <td nowrap="nowrap">
            <asp:TextBox ID="txtPackaging" runat="server" MaxLength="10" Text='<%# Eval("Packaging") %>'> </asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator_txtPackaging" runat="server" ControlToValidate="txtPackaging" ErrorMessage="Packaging is a required field." SetFocusOnError="true" Text="*"> </asp:RequiredFieldValidator>
          </td>
          <td>Side</td>
          <td nowrap="nowrap">
            <asp:TextBox ID="txtSide" runat="server" MaxLength="20" Text='<%# Eval("Side") %>'> </asp:TextBox>
          </td>
          <td>Size</td>
          <td nowrap="nowrap">
            <asp:TextBox ID="txtSize" runat="server" MaxLength="10" Text='<%# Eval("Size") %>'> </asp:TextBox>
          </td>
          <td>
            Modifiers
          </td>
          <td nowrap="nowrap" colspan="3">
            <div class="third">
              <div class="Dropdown-Container">
                <asp:DropDownList ID="ddlMod1" runat="server" SelectedValue='<%# Eval("Mod1") %>'>
                  <asp:ListItem Text="" Value=""></asp:ListItem>
                  <asp:ListItem Text="CG" Value="CG"></asp:ListItem>
                  <asp:ListItem Text="EY" Value="EY"></asp:ListItem>
                  <asp:ListItem Text="GA" Value="GA"></asp:ListItem>
                  <asp:ListItem Text="GX" Value="GX"></asp:ListItem>
                  <asp:ListItem Text="GY" Value="GY"></asp:ListItem>
                  <asp:ListItem Text="GZ" Value="GZ"></asp:ListItem>
                  <asp:ListItem Text="KV" Value="KV"></asp:ListItem>
                  <asp:ListItem Text="KX" Value="KX"></asp:ListItem>
                  <asp:ListItem Text="NU" Value="NU"></asp:ListItem>
                  <asp:ListItem Text="RA" Value="RA"></asp:ListItem>
                  <asp:ListItem Text="RB" Value="RB"></asp:ListItem>
                  <asp:ListItem Text="RR" Value="RR"></asp:ListItem>
                </asp:DropDownList>
              </div>
            </div>
            <div class="third">
              <div class="Dropdown-Container">
                <asp:DropDownList ID="ddlMod2" runat="server" SelectedValue='<%# Eval("Mod2") %>'>
                  <asp:ListItem Text="" Value=""></asp:ListItem>
                  <asp:ListItem Text="CG" Value="CG"></asp:ListItem>
                  <asp:ListItem Text="EY" Value="EY"></asp:ListItem>
                  <asp:ListItem Text="GA" Value="GA"></asp:ListItem>
                  <asp:ListItem Text="GX" Value="GX"></asp:ListItem>
                  <asp:ListItem Text="GY" Value="GY"></asp:ListItem>
                  <asp:ListItem Text="GZ" Value="GZ"></asp:ListItem>
                  <asp:ListItem Text="KV" Value="KV"></asp:ListItem>
                  <asp:ListItem Text="KX" Value="KX"></asp:ListItem>
                  <asp:ListItem Text="NU" Value="NU"></asp:ListItem>
                  <asp:ListItem Text="RA" Value="RA"></asp:ListItem>
                  <asp:ListItem Text="RB" Value="RB"></asp:ListItem>
                  <asp:ListItem Text="RR" Value="RR"></asp:ListItem>
                </asp:DropDownList>
              </div>
            </div>
            <div class="third">
              <div class="Dropdown-Container">
                <asp:DropDownList ID="ddlMod3" runat="server" SelectedValue='<%# Eval("Mod3") %>'>
                  <asp:ListItem Text="" Value=""></asp:ListItem>
                  <asp:ListItem Text="CG" Value="CG"></asp:ListItem>
                  <asp:ListItem Text="EY" Value="EY"></asp:ListItem>
                  <asp:ListItem Text="GA" Value="GA"></asp:ListItem>
                  <asp:ListItem Text="GX" Value="GX"></asp:ListItem>
                  <asp:ListItem Text="GY" Value="GY"></asp:ListItem>
                  <asp:ListItem Text="GZ" Value="GZ"></asp:ListItem>
                  <asp:ListItem Text="KV" Value="KV"></asp:ListItem>
                  <asp:ListItem Text="KX" Value="KX"></asp:ListItem>
                  <asp:ListItem Text="NU" Value="NU"></asp:ListItem>
                  <asp:ListItem Text="RA" Value="RA"></asp:ListItem>
                  <asp:ListItem Text="RB" Value="RB"></asp:ListItem>
                  <asp:ListItem Text="RR" Value="RR"></asp:ListItem>
                </asp:DropDownList>
              </div>
            </div>
          </td>
        </tr>
        <tr>
          <td>Gender</td>
          <td nowrap="nowrap">
            <asp:TextBox ID="txtGender" runat="server" MaxLength="10" Text='<%# Eval("Gender") %>'> </asp:TextBox>
          </td>
          <td>Color</td>
          <td nowrap="nowrap">
            <asp:TextBox ID="txtColor" runat="server" MaxLength="10" Text='<%# Eval("Color") %>'> </asp:TextBox>
          </td>
          <div id="divHCPCs" runat="server">
            <td>HCPCs</td>
            <td nowrap="nowrap">
              <asp:TextBox ID="txtHCPCSString" runat="server" MaxLength="50" Text='<%# Eval("HCPCSString") %>' Visible="true"> </asp:TextBox>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtHCPCSString" ErrorMessage="Each individual HCPC must be longer than 10 characters.<br/>HCPCs must be seperated by commas.<br/>Please remove any leading or trailing commas." SetFocusOnError="true" Text="*" ValidationExpression="^()*([\s?])*([\w?\$?\.?\s?]{1,4})+([\s?])*([,]([\s?])*([\w?\$?\.?\s?]{1,4})+([\s?])*)*$" Visible="true"> </asp:RegularExpressionValidator>
            </td>
          </div>
          <td>Wholesale Cost</td>
          <td nowrap="nowrap">
            <asp:TextBox ID="txtWholesaleCost" runat="server" MaxLength="9" Text='<%# Eval("WholesaleCost") %>'> </asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtWholesaleCost" ErrorMessage="Cost must be a monetary value." SetFocusOnError="true" Text="*" ValidationExpression="^\$?([1-9]{1}[0-9]{0,2}(\,[0-9]{3})*(\.[0-9]{0,2})?|[1-9]{1}[0-9]{0,}(\.[0-9]{0,2})?|0(\.[0-9]{0,2})?|(\.[0-9]{1,2})?)$"> </asp:RegularExpressionValidator>
          </td>
          <td>UPC&nbsp;Code</td>
          <td>
            <asp:TextBox ID="txtUPCCode" runat="server" MaxLength="25" Text='<%# Eval("UPCCode") %>'> </asp:TextBox>
          </td>
          <td>&nbsp;</td>
          <td colspan="3" nowrap>
            <div class="half">
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" CssClass="button-accent"/>
              </div>
            <div class="half">
            <asp:Button ID="btnUpdate" runat="server" CommandName='<%# (Container as GridItem).OwnerTableView.IsItemInserted ? "PerformInsert" : "Update" %>' Text='<%# (Container as GridItem).OwnerTableView.IsItemInserted ? "Insert" : "Update" %>' CssClass="button-primary"/>
              </div>
          </td>
        </tr>
      </table>
    </FormTemplate>
  </EditFormSettings>
</telerik:GridTableView>
<telerik:GridTableView AutoGenerateColumns="false" DataKeyNames="PracticeID, LinkThirdPartySupplierID, ThirdPartySupplierID, PracticeCatalogSupplierBrandID" HierarchyLoadMode="ServerOnDemand" Name="gtvSupplier" GridLines="Both">
  <ParentTableRelation>
    <telerik:GridRelationFields DetailKeyField="LinkThirdPartySupplierID" MasterKeyField="LinkThirdPartySupplierID"/>
  </ParentTableRelation>
  <Columns>
    <telerik:GridBoundColumn DataField="Brand" HeaderText="Brand" Visible="true"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="IsVendorBrand" DataType="System.Byte" HeaderText="IsVendorBrand" Visible="false"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" HeaderText="" Visible="false"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="PracticeID" HeaderText="" Visible="false"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="ThirdPartySupplierID" HeaderText="" Visible="false"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="LinkThirdPartySupplierID" HeaderText="LinkThirdPartySupplierID" Visible="false"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="Sequence" HeaderText="Sequence" Visible="false"></telerik:GridBoundColumn>
  </Columns>
  <DetailTables>
    <telerik:GridTableView AllowFilteringByColumn="false" AllowPaging="true" EnableColumnsViewState="false" AutoGenerateColumns="false" Caption="" CommandItemDisplay="Top" DataKeyNames="PracticeCatalogSupplierBrandID, ThirdPartyProductID, PracticeCatalogProductID" HierarchyLoadMode="ServerOnDemand" Name="gtvProduct" PageSize="15">
      <ParentTableRelation>
        <telerik:GridRelationFields DetailKeyField="PracticeCatalogSupplierBrandID" MasterKeyField="PracticeCatalogSupplierBrandID"/>
      </ParentTableRelation>
      <Columns>
        <telerik:GridTemplateColumn UniqueName="Promote" Visible="false">
          <ItemTemplate>
            <asp:ImageButton ID="btnPromote" runat="server" ImageUrl="~/RadControls/Grid/Skins/Office2007/MoveUp.gif" CommandName="Promote" Visible='<%# Page.User.IsInRole("BregAdmin") %>'/>
          </ItemTemplate>
        </telerik:GridTemplateColumn>
        <telerik:GridButtonColumn ButtonType="ImageButton" CommandArgument="Copy" CommandName="InitInsert" ImageUrl="~/App_Themes/Breg/images/Common/copy.png" UniqueName="btnCopy" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridButtonColumn>
    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" ImageUrl="~/App_Themes/Breg/images/Common/delete.png" UniqueName="btnDelete" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridButtonColumn>
    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" ImageUrl="~/App_Themes/Breg/images/Common/edit.png" UniqueName="btnEdit" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridButtonColumn>
    <telerik:GridBoundColumn DataField="RowNumber" DataType="System.Int32" HeaderText="#" UniqueName="RowNumber" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="ShortName" DataType="System.String" HeaderText="Product" UniqueName="ShortName" Visible="true"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="Code" DataType="System.String" HeaderText="Code" UniqueName="Code" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="Packaging" DataType="System.String" HeaderText="Pkg." Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="Side" DataType="System.String" HeaderText="Side" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="Size" DataType="System.String" HeaderText="Size" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
    <telerik:GridTemplateColumn HeaderText="Modifiers" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
      <ItemTemplate>
        <span class="comma-list">
          <span><%#  Eval("Mod1") %></span>
          <span><%#  Eval("Mod2") %></span>
          <span><%#  Eval("Mod3") %></span>
        </span>
      </ItemTemplate>
    </telerik:GridTemplateColumn>
    <telerik:GridBoundColumn DataField="Mod1" DataType="System.String" HeaderText="Mod" Visible="false"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="Mod2" DataType="System.String" HeaderText="Mod" Visible="false"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="Mod3" DataType="System.String" HeaderText="Mod" Visible="false"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="Gender" DataType="System.String" HeaderText="Gender" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="Color" DataType="System.String" HeaderText="Color" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="HCPCSString" DataType="System.String" HeaderText="HCPCs" Visible="true"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="WholesaleCost" DataFormatString="{0:C}" DataType="System.Decimal" HeaderText="Wholesale Cost" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"/>
    <telerik:GridBoundColumn DataField="UPCCode" DataType="System.String" HeaderText="UPC Code" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="IsDiscontinued" DataType="System.Boolean" HeaderText="IsDiscontinued" Visible="false"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="IsDeletable" DataType="System.Boolean" HeaderText="IsDeletable" Visible="false"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="ThirdPartyProductID" DataType="System.Int32" HeaderText="ThirdPartyProductID" Visible="false"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" DataType="System.Int32" HeaderText="PracticeCatalogSupplierBrandID" Visible="false"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="PracticeCatalogProductID" DataType="System.Int32" HeaderText="PracticeCatalogProductID" Visible="false"></telerik:GridBoundColumn>
    <telerik:GridBoundColumn DataField="UserMessage" DataType="System.String" HeaderText="UserMessage" Visible="false"></telerik:GridBoundColumn>
      </Columns>
      <EditFormSettings EditFormType="Template">
        <EditColumn UniqueName="EditCommandColumn1" FooterStyle-CssClass="display-none"></EditColumn>
        <FormTemplate>
          <table id="Table2" class="rgEditTable">
            <!-- old buttons -->
            <tr id="trPromoteHeader" runat="server" visible="false">
              <td colspan="3">Master Catalog Supplier</td>
              <td colspan="3">Category</td>
              <td colspan="8" align="left">Sub-Category</td>
            </tr>
            <tr id="trPromote" runat="server" visible="false">
              <td colspan="3">
                <asp:DropDownList ID="ddlSuppliers" runat="server" EnableViewState="true" OnSelectedIndexChanged="ddlSuppliers_OnSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
              </td>
              <td colspan="3">
                <asp:DropDownList ID="ddlCategories" runat="server" EnableViewState="true" OnSelectedIndexChanged="ddlCategories_OnSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
              </td>
              <td colspan="8" align="left">
                <asp:DropDownList ID="ddlSubCategories" runat="server" EnableViewState="true" OnLoad="ddlSubCategories_OnLoad"></asp:DropDownList>
              </td>
            </tr>
            <tr>
              <td colspan="14">
                <asp:Label ID="lblUserMessage" runat="server" Text='<%# Eval("UserMessage") %>'> </asp:Label>
                <asp:ValidationSummary ID="validationSummary" runat="server" HeaderText="Please correct the following:" Visible="true" CssClass="alignLeft"/>
              </td>
            </tr>
            <tr>
              <td>Product</td>
              <td nowrap="nowrap">
                <asp:TextBox ID="txtProduct" runat="server" MaxLength="50" Text='<%# Eval("ShortName") %>'> </asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator_txtProduct" runat="server" ControlToValidate="txtProduct" ErrorMessage="Product is a required field." Font-Overline="true" SetFocusOnError="true" Text="*"> </asp:RequiredFieldValidator>
              </td>
              <td>Code</td>
              <td nowrap="nowrap">
                <asp:TextBox ID="txtCode" runat="server" MaxLength="20" Text='<%# Eval("Code") %>'> </asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator_txtCode" runat="server" ControlToValidate="txtCode" ErrorMessage="Code is a required field." SetFocusOnError="true" Text="*"> </asp:RequiredFieldValidator>
              </td>
              <td>Pkg</td>
              <td nowrap="nowrap">
                <asp:TextBox ID="txtPackaging" runat="server" MaxLength="10" Text='<%# Eval("Packaging") %>'> </asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator_txtPackaging" runat="server" ControlToValidate="txtPackaging" ErrorMessage="Packaging is a required field." SetFocusOnError="true" Text="*"> </asp:RequiredFieldValidator>
              </td>
              <td>Side</td>
              <td nowrap="nowrap">
                <asp:TextBox ID="txtSide" runat="server" MaxLength="20" Text='<%# Eval("Side") %>'> </asp:TextBox>
              </td>
              <td>Size</td>
              <td nowrap="nowrap">
                <asp:TextBox ID="txtSize" runat="server" MaxLength="10" Text='<%# Eval("Size") %>'> </asp:TextBox>
              </td>
              <td>
                Modifiers
              </td>
              <td nowrap="nowrap" colspan="3">
                <div class="third">
                  <div class="Dropdown-Container">
                    <asp:DropDownList ID="ddlMod1" runat="server" SelectedValue='<%# Eval("Mod1") %>'>
                      <asp:ListItem Text="" Value=""></asp:ListItem>
                      <asp:ListItem Text="CG" Value="CG"></asp:ListItem>
                      <asp:ListItem Text="EY" Value="EY"></asp:ListItem>
                      <asp:ListItem Text="GA" Value="GA"></asp:ListItem>
                      <asp:ListItem Text="GX" Value="GX"></asp:ListItem>
                      <asp:ListItem Text="GY" Value="GY"></asp:ListItem>
                      <asp:ListItem Text="GZ" Value="GZ"></asp:ListItem>
                      <asp:ListItem Text="KV" Value="KV"></asp:ListItem>
                      <asp:ListItem Text="KX" Value="KX"></asp:ListItem>
                      <asp:ListItem Text="NU" Value="NU"></asp:ListItem>
                      <asp:ListItem Text="RA" Value="RA"></asp:ListItem>
                      <asp:ListItem Text="RB" Value="RB"></asp:ListItem>
                      <asp:ListItem Text="RR" Value="RR"></asp:ListItem>
                    </asp:DropDownList>
                  </div>
                </div>
                <div class="third">
                  <div class="Dropdown-Container">
                    <asp:DropDownList ID="ddlMod2" runat="server" SelectedValue='<%# Eval("Mod2") %>'>
                      <asp:ListItem Text="" Value=""></asp:ListItem>
                      <asp:ListItem Text="CG" Value="CG"></asp:ListItem>
                      <asp:ListItem Text="EY" Value="EY"></asp:ListItem>
                      <asp:ListItem Text="GA" Value="GA"></asp:ListItem>
                      <asp:ListItem Text="GX" Value="GX"></asp:ListItem>
                      <asp:ListItem Text="GY" Value="GY"></asp:ListItem>
                      <asp:ListItem Text="GZ" Value="GZ"></asp:ListItem>
                      <asp:ListItem Text="KV" Value="KV"></asp:ListItem>
                      <asp:ListItem Text="KX" Value="KX"></asp:ListItem>
                      <asp:ListItem Text="NU" Value="NU"></asp:ListItem>
                      <asp:ListItem Text="RA" Value="RA"></asp:ListItem>
                      <asp:ListItem Text="RB" Value="RB"></asp:ListItem>
                      <asp:ListItem Text="RR" Value="RR"></asp:ListItem>
                    </asp:DropDownList>
                  </div>
                </div>
                <div class="third">
                  <div class="Dropdown-Container">
                    <asp:DropDownList ID="ddlMod3" runat="server" SelectedValue='<%# Eval("Mod3") %>'>
                      <asp:ListItem Text="" Value=""></asp:ListItem>
                      <asp:ListItem Text="CG" Value="CG"></asp:ListItem>
                      <asp:ListItem Text="EY" Value="EY"></asp:ListItem>
                      <asp:ListItem Text="GA" Value="GA"></asp:ListItem>
                      <asp:ListItem Text="GX" Value="GX"></asp:ListItem>
                      <asp:ListItem Text="GY" Value="GY"></asp:ListItem>
                      <asp:ListItem Text="GZ" Value="GZ"></asp:ListItem>
                      <asp:ListItem Text="KV" Value="KV"></asp:ListItem>
                      <asp:ListItem Text="KX" Value="KX"></asp:ListItem>
                      <asp:ListItem Text="NU" Value="NU"></asp:ListItem>
                      <asp:ListItem Text="RA" Value="RA"></asp:ListItem>
                      <asp:ListItem Text="RB" Value="RB"></asp:ListItem>
                      <asp:ListItem Text="RR" Value="RR"></asp:ListItem>
                    </asp:DropDownList>
                  </div>
                </div>
              </td>
            </tr>
            <tr>
              <td>Gender</td>
              <td nowrap="nowrap">
                <asp:TextBox ID="txtGender" runat="server" MaxLength="10" Text='<%# Eval("Gender") %>'> </asp:TextBox>
              </td>
              <td>Color</td>
              <td nowrap="nowrap">
                <asp:TextBox ID="txtColor" runat="server" MaxLength="10" Text='<%# Eval("Color") %>'> </asp:TextBox>
              </td>
              <td id="divHCPCsLabel">HCPCs</td>
              <td id="divHCPCs" nowrap="nowrap">
                <asp:TextBox ID="txtHCPCSString" runat="server" MaxLength="50" Text='<%# Eval("HCPCSString") %>' Visible="true"> </asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtHCPCSString" ErrorMessage="Each individual HCPC must be longer than 10 characters.<br/>HCPCs must be seperated by commas.<br/>Please remove any leading or trailing commas." SetFocusOnError="true" Text="*" ValidationExpression="^()*([\s?])*([\w?\$?\.?\s?]{1,4})+([\s?])*([,]([\s?])*([\w?\$?\.?\s?]{1,4})+([\s?])*)*$" Visible="true"> </asp:RegularExpressionValidator>
              </td>
              <td>Wholesale Cost</td>
              <td colspan="1" nowrap="nowrap">
                <asp:TextBox ID="txtWholesaleCost" runat="server" MaxLength="9" Text='<%# Eval("WholesaleCost") %>'> </asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtWholesaleCost" ErrorMessage="Cost must be a monetary value." SetFocusOnError="true" Text="*" ValidationExpression="^\$?([1-9]{1}[0-9]{0,2}(\,[0-9]{3})*(\.[0-9]{0,2})?|[1-9]{1}[0-9]{0,}(\.[0-9]{0,2})?|0(\.[0-9]{0,2})?|(\.[0-9]{1,2})?)$"> </asp:RegularExpressionValidator>
              </td>
              <td>UPC Code</td>
              <td nowrap="nowrap">
                <asp:TextBox ID="txtUPCCode" runat="server" MaxLength="25" Text='<%# Eval("UPCCode") %>'> </asp:TextBox>
              </td>
              <td>&nbsp;</td>
              <td colspan="3">
                <div>
                  <div class="half">
                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CommandName="Cancel" CssClass="button-accent" Text="Cancel"/>
                  </div>
                  <div class="half">
                    <asp:Button ID="btnUpdate" runat="server" CssClass="button-primary" CommandName='<%# (Container as GridItem).OwnerTableView.IsItemInserted ? "PerformInsert" : "Update" %>' Text='<%# (Container as GridItem).OwnerTableView.IsItemInserted ? "Insert" : "Update" %>'/>
                  </div>
                </div>
              </td>
            </tr>
          </table>
        </FormTemplate>
      </EditFormSettings>
    </telerik:GridTableView>
  </DetailTables>
</telerik:GridTableView>
</DetailTables>
</MasterTableView>
</bvu:VisionRadGridAjax>
</div>
