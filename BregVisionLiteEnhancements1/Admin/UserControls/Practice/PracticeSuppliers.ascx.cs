﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.Admin.PracticeSetup;
using ClassLibrary.DAL;
using System.Linq;
using System.Collections.Generic;

namespace BregVision.Admin.UserControls
{
    public partial class PracticeSuppliers : PracticeSetupBase, IVisionWizardControl
    {

        /// <summary>
        /// IVisionWizardControl Interface.
        /// </summary>
        public bool NeedsInitialBind { get; set; }
        private bool isInEdit = false;

        protected void page_Init(object sender, EventArgs e)
        {
            lblGSStatus.Text = "";            
        }


        protected void RemoveVisionOption(DropDownList ddl)
        {
            if (ddl != null)
            {
                foreach (ListItem item in ddl.Items)
                {
                    if (item.Text.ToLower() == "vendor")
                    {
                        ddl.Items.Remove(item);
                    }
                }
            }
        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            lblContentHeader.Text = practiceName + " Third Party Suppliers";
            if (isInEdit == false)
            {
                BindGridSupplier(practiceID);  //GetThirdPartySuppliers( practiceID );
                BindGridMCSupplier(practiceID);  // Get all mC Suppliers.
            }
            
            // NOTE: disabling this feature for everybody, including BregAdmin until we need it.
            // Accidental promotion of Third Party Supplier into Master Catalog Supplier has
            // caused problems and there should be a warning displayed before it commits the change.
            // -hchan 05242016
            //if (!Context.User.IsInRole("BregAdmin"))
            //{
                GridSupplier.Columns[1].Visible = false;
            //}
        }

        protected void btnVerifyFax_Click(object sender, EventArgs e)
        {
            bool faxSent = false;

            LinkButton verifyButton = sender as LinkButton;
            if (verifyButton != null)
            {
                string[] tppParams = verifyButton.CommandArgument.Split('|');


                Int32 thirdPartyProductID = Convert.ToInt32(tppParams[0]);
                string faxNumber = tppParams[1];

                try
                {
                    faxSent = FaxEmailPOs.SendTestFax(faxNumber, "");
                }
                catch (Exception ex)
                {

                    faxSent = false;
                }

                VisionDataContext visionDB = null;
                visionDB = VisionDataContext.GetVisionDataContext();

                using (visionDB)
                {
                    var ttpQuery = (from tps in visionDB.ThirdPartySuppliers
                                    where tps.ThirdPartySupplierID == thirdPartyProductID
                                    select tps).Update(rhi => rhi.IsFaxVerified = faxSent);

                    visionDB.SubmitChanges();
                }

            }
        }

   


        protected void BindGridSupplier(int practiceID)
        {
            DataTable dtThirdPartySuppliers = GetThirdPartySuppliers(practiceID);

            if (dtThirdPartySuppliers.Rows.Count > 0)
            {
                GridSupplier.DataSource = dtThirdPartySuppliers;
                GridSupplier.DataBind();

                SetVisionLiteMaxSuppliers(dtThirdPartySuppliers);
            }
            else  //No records found - insert a display row so the GridView will display.
            {
                // Sorting Works but is commented out for now.
                //GridSupplier.AllowSorting  = false;
                DisplayNoRecordsFoundRow(dtThirdPartySuppliers);
            }
        }

        private void SetVisionLiteMaxSuppliers(DataTable dtThirdPartySuppliers)
        {
            if (BLL.Website.IsVisionExpress() && dtThirdPartySuppliers.Rows.Count > 5)
            {
                ImageButton addNewButton = GridSupplier.FooterRow.FindControl("ImageButtonAddNew") as ImageButton;
                if (addNewButton != null)
                {
                    addNewButton.Enabled = false;
                    addNewButton.ToolTip = "Vision Express only allowed 6 Suppliers";
                }
            }
        }


        protected void BindGridMCSupplier(int practiceID)
        {
            DataTable dtMCSuppliers = GetMCSuppliers(practiceID);

            if (dtMCSuppliers.Rows.Count > 0)
            {
                GridMCSupplier.DataSource = dtMCSuppliers;
                GridMCSupplier.DataBind();
            }
            else  //No records found - insert a display row so the GridView will display.
            {
                // Sorting Works but is commented out for now.
                //GridSupplier.AllowSorting  = false;
                DisplayNoRecordsFoundRowMCSuppliers(dtMCSuppliers);
            }
        }

        protected DataTable GetThirdPartySuppliers(int practiceID)
        {
            //BLL.Practice.ThirdPartySupplier bllSupplier = new BLL.Practice.ThirdPartySupplier();
            DAL.Practice.ThirdPartySupplier dalPThirdPartySupplier = new DAL.Practice.ThirdPartySupplier();
            DataTable dtThirdPartySuppliers = dalPThirdPartySupplier.SelectAll(practiceID);
            return dtThirdPartySuppliers;

            //GridSupplier.DataSource = dtThirdPartySuppliers;
            //GridSupplier.DataBind();
        }


        protected DataTable GetMCSuppliers(int practiceID)
        {
            //BLL.Practice.ThirdPartySupplier bllSupplier = new BLL.Practice.ThirdPartySupplier();
            DAL.Practice.ThirdPartySupplier dalPThirdPartySupplier = new DAL.Practice.ThirdPartySupplier();
            DataTable dtMCSuppliers = dalPThirdPartySupplier.SelectAllMCSuppliers(practiceID);
            return dtMCSuppliers;
        }


        //  Display "No Records Found" when the grid's data source has now records.
        protected void DisplayNoRecordsFoundRow(DataTable dtThirdPartySuppliers)
        {
            dtThirdPartySuppliers.Rows.Add(dtThirdPartySuppliers.NewRow());
            GridSupplier.DataSource = dtThirdPartySuppliers;
            GridSupplier.DataBind();

            int totalColumns = GridSupplier.Rows[0].Cells.Count;
            GridSupplier.Rows[0].Cells.Clear();
            GridSupplier.Rows[0].Cells.Add(new TableCell());
            GridSupplier.Rows[0].Cells[0].ColumnSpan = totalColumns;
            GridSupplier.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
            GridSupplier.Rows[0].Cells[0].Text = "No Records Found";
        }

        //  Display "No Records Found" when the grid's data source has now records.
        protected void DisplayNoRecordsFoundRowMCSuppliers(DataTable dtMCSuppliers)
        {
            dtMCSuppliers.Rows.Add(dtMCSuppliers.NewRow());
            GridMCSupplier.DataSource = dtMCSuppliers;
            GridMCSupplier.DataBind();

            int totalColumns = GridMCSupplier.Rows[0].Cells.Count;
            GridMCSupplier.Rows[0].Cells.Clear();
            GridMCSupplier.Rows[0].Cells.Add(new TableCell());
            GridMCSupplier.Rows[0].Cells[0].ColumnSpan = totalColumns;
            GridMCSupplier.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
            GridMCSupplier.Rows[0].Cells[0].Text = "No Records Found";
        }

        ////  User Clicked Edit, so allow the row to be edited.
        //protected void GridSupplier_RowEditing(object sender, GridViewEditEventArgs e)
        //{
        //    GridSupplier.EditIndex = e.NewEditIndex;
        //    BindGridSupplier( practiceID );
        //}

        protected void GridSupplier_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int thirdPartySupplierID = Convert.ToInt32(GridSupplier.DataKeys[e.RowIndex].Values[0].ToString());
            //int practiceID = Convert.ToInt32(GridSupplier.DataKeys[e.RowIndex].Values[1].ToString());
            TextBox txtSupplierName = (TextBox)GridSupplier.Rows[e.RowIndex].FindControl("txtSupplierName");
            TextBox txtSupplierShortName = (TextBox)GridSupplier.Rows[e.RowIndex].FindControl("txtSupplierShortName");
            DropDownList ddlSupplierType = (DropDownList)GridSupplier.Rows[e.RowIndex].FindControl("ddlSupplierType");
            DropDownList ddlOrderPlacementType = (DropDownList)GridSupplier.Rows[e.RowIndex].FindControl("ddlOrderPlacementType");
            Telerik.Web.UI.RadMaskedTextBox txtFaxOrderPlacement = (Telerik.Web.UI.RadMaskedTextBox)GridSupplier.Rows[e.RowIndex].FindControl("txtFaxOrderPlacement");
            TextBox txtEmailOrderPlacement = (TextBox)GridSupplier.Rows[e.RowIndex].FindControl("txtEmailOrderPlacement");

            string supplierName = txtSupplierName.Text.ToString().Trim();
            string supplierShortName = txtSupplierShortName.Text.ToString().Trim();

            string supplierType = ddlSupplierType.SelectedValue.ToString();
            string orderPlacementType = ddlOrderPlacementType.SelectedValue.ToString();

            string faxOrderPlacement = txtFaxOrderPlacement.Text.ToString().Trim();
            string emailOrderPlacement = txtEmailOrderPlacement.Text.ToString().Trim();

            bool isVendor = (supplierType == "Vendor") ? true : false;
            bool isEmailOrderPlacement = (orderPlacementType == "Email") ? true : false;

            DAL.Practice.ThirdPartySupplier dalPTPS = new DAL.Practice.ThirdPartySupplier();

            dalPTPS.Update(thirdPartySupplierID, practiceID, supplierName, supplierShortName, faxOrderPlacement, emailOrderPlacement, isEmailOrderPlacement, isVendor, userID);

            // Set Edit Mode back to Read Only.
            GridSupplier.EditIndex = -1;
            BindGridSupplier(practiceID);
        }


        // Set the Drop Down List
        protected void GridSupplier_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            // use if neccessary
            // ddlSupplierType   ddlOrderPlacementType
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlSupplierType = (DropDownList)e.Row.FindControl("ddlSupplierType");
                DropDownList ddlOrderPlacementType = (DropDownList)e.Row.FindControl("ddlOrderPlacementType");

                if (ddlSupplierType != null)
                {

                    // Retain the value of the Salutation when in EditMode.
                    // DataKey 0 is practiceID, DataKey 1 is salutation, DataKey 2 is suffix.

                    ddlSupplierType.SelectedValue = (Convert.ToBoolean(GridSupplier.DataKeys[e.Row.RowIndex].Values[2]) ? "Manufacturer" : "Manufacturer");

                }

                if (ddlOrderPlacementType != null) //Edit Mode
                {
                    //ddlOrderPlacementType.DataSource = GetSuffixList();
                    //ddlOrderPlacementType.DataBind();
                    // Retain the value of the Suffix when in EditMode.
                    // DataKey 0 is practiceID, DataKey 1 is salutation, DataKey 2 is suffix.
                    ddlOrderPlacementType.SelectedValue = (Convert.ToBoolean(GridSupplier.DataKeys[e.Row.RowIndex].Values[3]) ? "Email" : "Fax");
                    
                    SetFaxEmailViewMode(ddlOrderPlacementType);

                    bool isEmailDisplayed = (Convert.ToBoolean(GridSupplier.DataKeys[e.Row.RowIndex].Values[3]));

                }
                else //View Mode
                {
                    SetFaxEmailViewMode(e.Row);
                }

                ImageButton imgDelete = e.Row.FindControl("imgDelete") as ImageButton;
                if (imgDelete != null)
                {
                    imgDelete.Attributes.Add("onclick", "return confirm('Are you sure you want delete this Supplier?');");
                }
            }

        }


        //  User Clicked Edit, so allow the row to be edited.
        protected void GridSupplier_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridSupplier.EditIndex = e.NewEditIndex;
            BindGridSupplier(practiceID);
        }



        //  User Canceled the Row Edit, so remove the editing from the row.
        protected void GridSupplier_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            CancelSupplierGridEdit();
        }

        private void CancelSupplierGridEdit()
        {
            GridSupplier.EditIndex = -1;
            BindGridSupplier(practiceID);
        }



        //  User click Delete so Delete the Practice.
        public void GridSupplier_RowDeleting(Object sender, GridViewDeleteEventArgs e)
        {
            int thirdPartySupplierID = Convert.ToInt32(GridSupplier.DataKeys[e.RowIndex].Values[0].ToString());

			bool unableToDelete = false;
			using (var db = VisionDataContext.GetVisionDataContext())
            {
				var openOrders = from so in db.SupplierOrders
								join pcsb in db.PracticeCatalogSupplierBrands on so.PracticeCatalogSupplierBrandID equals pcsb.PracticeCatalogSupplierBrandID
								 join tps in db.ThirdPartySuppliers on pcsb.ThirdPartySupplierID equals tps.ThirdPartySupplierID
								 where so.SupplierOrderStatusID != 2 && pcsb.ThirdPartySupplierID == thirdPartySupplierID
								select new { so, tps };

                string errorText = string.Empty;
				bool addedPrefix = false;
				foreach (var openOrder in openOrders)
                {
					unableToDelete = true;
					if (!addedPrefix)
					{
						addedPrefix = true;
						errorText += "Unable to delete supplier " + openOrder.tps.SupplierName + ": PO(s) ";
					}
					else
					{
						errorText += ", ";
					}
					errorText += openOrder.so.PurchaseOrder + " ";
					if (openOrder.so.CustomPurchaseOrderCode != string.Empty)
						errorText += "(" + openOrder.so.CustomPurchaseOrderCode + ") ";
				}
				if (unableToDelete)
				{
					errorText += " are still outstanding!";
					lblGSStatus.Text = errorText;
				}
			}

			if (!unableToDelete)
			{
				DAL.Practice.ThirdPartySupplier dalPTPS = new DAL.Practice.ThirdPartySupplier();
				try
				{
					Int32 numRecordsDeleted = dalPTPS.Delete(thirdPartySupplierID, userID);
					if (numRecordsDeleted < 1)
					{
						ShowDeleteFailedMessage();
					}
				}
				catch (Exception ex)
				{
					ShowDeleteFailedMessage();
				}
				GridSupplier.EditIndex = -1;
				BindGridSupplier(practiceID);
			}
        }

        private void ShowDeleteFailedMessage()
        {
            lblGSStatus.Text = "Unable to Delete Record";
        }


        // User Clicked "AddNew" or "Insert" or "Cancel" (ed) the AddNew.
        protected void GridSupplier_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Promote"))
            {
                PromoteToMasterCatalog(sender, e);
            }

            if (e.CommandName.Equals("GoToPracticeVendorBrands"))
            {
                //Find the selected row index
                int index = Convert.ToInt32(e.CommandArgument);
                //
                Session["ThirdPartySupplierID"] = Convert.ToInt32(e.CommandArgument); //GridPracticeLocation.DataKeys[index].Values[1];
                //Session["PracticeLocationName"] = e.CommandSource.Text.ToString();  //GridPracticeLocation.DataKeys[index].Values[2];
                LinkButton lb = (LinkButton)e.CommandSource;

                //Get these from the next page.
                //Session["SupplierName"] = GridSupplier.DataKeys[index].Values[4].ToString();   //lb.Text.ToString();
                //Session["SupplierShortName"] = GridSupplier.DataKeys[index].Values[5].ToString();   //lb.Text.ToString();

                Response.Redirect("Practice_Vendor_Brands.aspx");
            }


            // Check if AddNew Button was clicked.  Note: this could be AddNew or Insert button that was clicked.
            if (e.CommandName.Equals("AddNew"))
            {
                isInEdit = true;
                // Get the Controls within the footer.
                //int thirdPartySupplierID = Convert.ToInt32(GridSupplier.DataKeys[e.RowIndex].Values[0].ToString());
                //int practiceID = Convert.ToInt32(GridSupplier.DataKeys[e.RowIndex].Values[1].ToString());
                TextBox txtNewSupplierName = (TextBox)GridSupplier.FooterRow.FindControl("txtNewSupplierName");
                TextBox txtNewSupplierShortName = (TextBox)GridSupplier.FooterRow.FindControl("txtNewSupplierShortName");
                DropDownList ddlNewSupplierType = (DropDownList)GridSupplier.FooterRow.FindControl("ddlNewSupplierType");
                DropDownList ddlNewOrderPlacementType = (DropDownList)GridSupplier.FooterRow.FindControl("ddlNewOrderPlacementType");
                Telerik.Web.UI.RadMaskedTextBox txtNewFaxOrderPlacement = (Telerik.Web.UI.RadMaskedTextBox)GridSupplier.FooterRow.FindControl("txtNewFaxOrderPlacement");
                TextBox txtNewEmailOrderPlacement = (TextBox)GridSupplier.FooterRow.FindControl("txtNewEmailOrderPlacement");
                //footer validators
                RequiredFieldValidator rfvNewSupplierName = (RequiredFieldValidator)GridSupplier.FooterRow.FindControl("rfvNewSupplierName");
                RequiredFieldValidator rfvSupplierShortName = (RequiredFieldValidator)GridSupplier.FooterRow.FindControl("rfvNewSupplierShortName");

                RequiredFieldValidator rfvNewFaxOrderPlacement = (RequiredFieldValidator)GridSupplier.FooterRow.FindControl("rfvNewFaxOrderPlacement");
                RegularExpressionValidator revNewFaxOrderPlacement = (RegularExpressionValidator)GridSupplier.FooterRow.FindControl("revNewFaxOrderPlacement");
                RequiredFieldValidator rfvNewEmailOrderPlacement = (RequiredFieldValidator)GridSupplier.FooterRow.FindControl("rfvNewEmailOrderPlacement");
                RegularExpressionValidator revEmailAddress = (RegularExpressionValidator)GridSupplier.FooterRow.FindControl("revEmailAddress");

                ImageButton imageButtonAddNew = (ImageButton)GridSupplier.FooterRow.FindControl("ImageButtonAddNew");
                ImageButton imageButtonAddNewCancel = (ImageButton)GridSupplier.FooterRow.FindControl("ImageButtonAddNewCancel");

                //  Check if the AddNew button was clicked to Add a New record as opposed to Inserting a New Record
                if (imageButtonAddNewCancel.Visible == false)
                {
                    // User clicked to Add a New Record.  
                    //Display the New Fields for input and their validation controls.
                    //Display the Insert and Cancel buttons.

                    SetFooterVisibility(true, txtNewSupplierName, txtNewSupplierShortName, ddlNewSupplierType, ddlNewOrderPlacementType, txtNewFaxOrderPlacement, txtNewEmailOrderPlacement, rfvNewFaxOrderPlacement, revNewFaxOrderPlacement, rfvNewEmailOrderPlacement, revEmailAddress, rfvNewSupplierName, rfvSupplierShortName);

                    SetFaxEmailFooterVisibility(ddlNewOrderPlacementType);
                    //SetFaxEmailVisibility(ddlNewOrderPlacementType.SelectedValue, txtNewFaxOrderPlacement, txtNewEmailOrderPlacement, lblFax, lblEmail, rfvNewFaxOrderPlacement, rfvNewEmailOrderPlacement);

                    imageButtonAddNew.ImageUrl = @"~\App_Themes\Breg\images\Common\checkCircle.png";
                    imageButtonAddNew.ToolTip = "Insert New Third Party Supplier";
                    imageButtonAddNewCancel.Visible = true;

                    //rfvNewFirstName.Visible = true;
                    //rfvNewLastName.Visible = true;



                    // If the "No Records Found" was displayed before the user clicked AddNew than clear that row.
                    if (GridSupplier.Rows[0].Cells[0].Text == "No Records Found")
                    {
                        GridSupplier.Rows[0].Cells.Clear();
                    }


                }
                else    //  User clicked the Insert button.
                {
                    // User clicked to Insert a New Record, so Insert the New Record.
                    // Hide the Add New Fields for input and their validators.
                    // Hide the Insert and Cancel buttons.
                    // Show the Add New Button


                    int thirdPartySupplier;
                    string supplierName = txtNewSupplierName.Text.ToString().Trim();
                    string supplierShortName = txtNewSupplierShortName.Text.ToString().Trim();

                    string supplierType = ddlNewSupplierType.SelectedValue.ToString();
                    string orderPlacementType = ddlNewOrderPlacementType.SelectedValue.ToString();

                    string faxOrderPlacement = txtNewFaxOrderPlacement.Text.ToString().Trim();
                    string emailOrderPlacement = txtNewEmailOrderPlacement.Text.ToString().Trim();

                    bool isVendor = (supplierType == "Vendor") ? true : false;
                    bool isEmailOrderPlacement = (orderPlacementType == "Email") ? true : false;

                    DAL.Practice.ThirdPartySupplier dalPTPS = new DAL.Practice.ThirdPartySupplier();
                    dalPTPS.Insert(out thirdPartySupplier, practiceID, supplierName, supplierShortName, faxOrderPlacement, emailOrderPlacement, isEmailOrderPlacement, isVendor, userID);

                    SetFooterVisibility(true, txtNewSupplierName, txtNewSupplierShortName, ddlNewSupplierType, ddlNewOrderPlacementType, txtNewFaxOrderPlacement, txtNewEmailOrderPlacement, rfvNewFaxOrderPlacement, revNewFaxOrderPlacement, rfvNewEmailOrderPlacement, revEmailAddress, rfvNewSupplierName, rfvSupplierShortName);

                    //txtNewSupplierName.Visible = false;
                    //txtNewSupplierName.Visible = false;
                    //txtNewSupplierShortName.Visible = false;
                    //ddlNewSupplierType.Visible = false;
                    //ddlNewOrderPlacementType.Visible = false;
                    //txtNewFaxOrderPlacement.Visible = false;
                    //txtNewEmailOrderPlacement.Visible = false;

                    //rfvNewFaxOrderPlacement.Enabled = false;
                    //revNewFaxOrderPlacement.Enabled = false;
                    //rfvNewEmailOrderPlacement.Enabled = false;
                    //revEmailAddress.Enabled = false;

                    imageButtonAddNew.ImageUrl = @"~\App_Themes\Breg\images\Common\add-record.png";
                    imageButtonAddNew.ToolTip = "Add New Third Party Supplier";
                    imageButtonAddNewCancel.Visible = false;

                    //rfvNewFirstName.Visible = false;
                    //rfvNewLastName.Visible = false;

                    BindGridSupplier(practiceID);
                }
            }

            //  User Canceled the insertion of a new record.  Therefore, revert mode back to AddNew.
            //  Hide the Add New Fields for input and their validators.
            //  Hide the Insert and Cancel buttons.
            //  Show the Add New Button
            if (e.CommandName.Equals("AddNewCancel"))
            {

                //int thirdPartySupplierID = Convert.ToInt32(GridSupplier.DataKeys[e.RowIndex].Values[0].ToString());
                //int practiceID = Convert.ToInt32(GridSupplier.DataKeys[e.RowIndex].Values[1].ToString());
                TextBox txtNewSupplierName = (TextBox)GridSupplier.FooterRow.FindControl("txtNewSupplierName");
                TextBox txtNewSupplierShortName = (TextBox)GridSupplier.FooterRow.FindControl("txtNewSupplierShortName");
                DropDownList ddlNewSupplierType = (DropDownList)GridSupplier.FooterRow.FindControl("ddlNewSupplierType");
                DropDownList ddlNewOrderPlacementType = (DropDownList)GridSupplier.FooterRow.FindControl("ddlNewOrderPlacementType");
                Telerik.Web.UI.RadMaskedTextBox txtNewFaxOrderPlacement = (Telerik.Web.UI.RadMaskedTextBox)GridSupplier.FooterRow.FindControl("txtNewFaxOrderPlacement");
                TextBox txtNewEmailOrderPlacement = (TextBox)GridSupplier.FooterRow.FindControl("txtNewEmailOrderPlacement");

                RequiredFieldValidator rfvNewSupplierName = (RequiredFieldValidator)GridSupplier.FooterRow.FindControl("rfvNewSupplierName");
                RequiredFieldValidator rfvSupplierShortName = (RequiredFieldValidator)GridSupplier.FooterRow.FindControl("rfvSupplierShortName");

                RequiredFieldValidator rfvNewFaxOrderPlacement = (RequiredFieldValidator)GridSupplier.FooterRow.FindControl("rfvNewFaxOrderPlacement");
                RegularExpressionValidator revNewFaxOrderPlacement = (RegularExpressionValidator)GridSupplier.FooterRow.FindControl("revNewFaxOrderPlacement");
                RequiredFieldValidator rfvNewEmailOrderPlacement = (RequiredFieldValidator)GridSupplier.FooterRow.FindControl("rfvNewEmailOrderPlacement");
                RegularExpressionValidator revEmailAddress = (RegularExpressionValidator)GridSupplier.FooterRow.FindControl("revEmailAddress");

                //RequiredFieldValidator rfvNewFirstName = (RequiredFieldValidator)GridSupplier.FooterRow.FindControl("rfvNewFirstName");
                //RequiredFieldValidator rfvNewLastName = (RequiredFieldValidator)GridSupplier.FooterRow.FindControl("rfvNewLastName");

                ImageButton imageButtonAddNew = (ImageButton)GridSupplier.FooterRow.FindControl("ImageButtonAddNew");
                ImageButton imageButtonAddNewCancel = (ImageButton)GridSupplier.FooterRow.FindControl("ImageButtonAddNewCancel");

                SetFooterVisibility(true, txtNewSupplierName, txtNewSupplierShortName, ddlNewSupplierType, ddlNewOrderPlacementType, txtNewFaxOrderPlacement, txtNewEmailOrderPlacement, rfvNewFaxOrderPlacement, revNewFaxOrderPlacement, rfvNewEmailOrderPlacement, revEmailAddress, rfvNewSupplierName, rfvSupplierShortName);

                //txtNewSupplierName.Visible = false;
                //txtNewSupplierName.Visible = false;
                //txtNewSupplierShortName.Visible = false;
                //ddlNewSupplierType.Visible = false;
                //ddlNewOrderPlacementType.Visible = false;
                //txtNewFaxOrderPlacement.Visible = false;
                //txtNewEmailOrderPlacement.Visible = false;

                imageButtonAddNew.ImageUrl = @"~\RadControls\Grid\Skins\Outlook\AddRecord.gif";
                imageButtonAddNew.ToolTip = "Add New Third Party Supplier";

                imageButtonAddNewCancel.Visible = false;
                //rfvNewFirstName.Visible = false;
                //rfvNewLastName.Visible = false;

                // Check if user cancel the add new record when adding the first record.
                // Is so call the Bind method so that the "No Records Found" message is displayed.
                if (GridSupplier.Rows.Count < 2)
                {
                    // Sorting Works but is commented out for now.
                    //GridSupplier.AllowSorting = false;                
                }

                BindGridSupplier(practiceID);
            }
        }

        private void PromoteToMasterCatalog(object sender, GridViewCommandEventArgs e)
        {
            int thirdPartySupplierID = Convert.ToInt32(e.CommandArgument);

            DAL.Practice.Catalog.PromoteToMasterCatalog(thirdPartySupplierID, Context.User.Identity.Name);
        }


        private static void SetFooterVisibility(bool visible, TextBox txtNewSupplierName, TextBox txtNewSupplierShortName, DropDownList ddlNewSupplierType, DropDownList ddlNewOrderPlacementType, Telerik.Web.UI.RadMaskedTextBox txtNewFaxOrderPlacement, TextBox txtNewEmailOrderPlacement, RequiredFieldValidator rfvNewFaxOrderPlacement, RegularExpressionValidator revNewFaxOrderPlacement, RequiredFieldValidator rfvNewEmailOrderPlacement, RegularExpressionValidator revEmailAddress, RequiredFieldValidator rfvNewSupplierName, RequiredFieldValidator rfvNewSupplierShortName)
        {


            txtNewSupplierName.Visible = visible;
            txtNewSupplierName.Visible = visible;
            txtNewSupplierShortName.Visible = visible;
            ddlNewSupplierType.Visible = visible;
            ddlNewOrderPlacementType.Visible = visible;
            txtNewFaxOrderPlacement.Visible = visible;
            txtNewEmailOrderPlacement.Visible = visible;

            rfvNewFaxOrderPlacement.Enabled = visible;
            revNewFaxOrderPlacement.Enabled = visible;
            rfvNewEmailOrderPlacement.Enabled = visible;
            revEmailAddress.Enabled = visible;
            if (rfvNewSupplierShortName != null)
            {
                rfvNewSupplierShortName.Visible = visible;
                rfvNewSupplierShortName.Enabled = visible;
                rfvNewSupplierName.Visible = visible;
                rfvNewSupplierName.Enabled = visible;
            }
        }

        protected void SetFaxEmailViewMode()
        {
            foreach (GridViewRow row in GridSupplier.Rows)
            {
                SetFaxEmailViewMode(row);
            }
        }

        protected void SetFaxEmailViewMode(GridViewRow row)
        {
            Label lblOrderPlacementType = row.FindControl("lblOrderPlacementType") as Label;

            Label lblFaxOrderPlacement = row.FindControl("lblFaxOrderPlacement") as Label;
            Label lblEmailOrderPlacement = row.FindControl("lblEmailOrderPlacement") as Label;

            System.Web.UI.HtmlControls.HtmlControl lblFax = row.FindControl("lblItemFaxOrderPlacement") as System.Web.UI.HtmlControls.HtmlControl;
            System.Web.UI.HtmlControls.HtmlControl lblEmail = row.FindControl("lblItemEmailOrderPlacement") as System.Web.UI.HtmlControls.HtmlControl;

            RequiredFieldValidator rfvFax = null;
            RequiredFieldValidator rfvEmail = null;

            SetFaxEmailVisibility(lblOrderPlacementType.Text, lblFaxOrderPlacement, lblEmailOrderPlacement, lblFax, lblEmail, rfvFax, rfvEmail);

        }

        protected void ddlNewOrderPlacementType_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            isInEdit = true;
            //Footer row visible for add
            DropDownList ddl = sender as DropDownList;


            SetFaxEmailFooterVisibility(ddl);
        }

        private void SetFaxEmailFooterVisibility(DropDownList ddl)
        {
            Telerik.Web.UI.RadMaskedTextBox txtFax = GridSupplier.FooterRow.FindControl("txtNewFaxOrderPlacement") as Telerik.Web.UI.RadMaskedTextBox;
            TextBox txtEmail = GridSupplier.FooterRow.FindControl("txtNewEmailOrderPlacement") as TextBox;

            System.Web.UI.HtmlControls.HtmlControl lblFax = GridSupplier.FooterRow.FindControl("lblFooterFaxOrderPlacement") as System.Web.UI.HtmlControls.HtmlControl;
            System.Web.UI.HtmlControls.HtmlControl lblEmail = GridSupplier.FooterRow.FindControl("lblFooterEmailOrderPlacement") as System.Web.UI.HtmlControls.HtmlControl;

            RequiredFieldValidator rfvFax = GridSupplier.FooterRow.FindControl("rfvNewFaxOrderPlacement") as RequiredFieldValidator;
            RequiredFieldValidator rfvEmail = GridSupplier.FooterRow.FindControl("rfvNewEmailOrderPlacement") as RequiredFieldValidator;

            SetFaxEmailVisibility(ddl.SelectedValue, txtFax, txtEmail, lblFax, lblEmail, rfvFax, rfvEmail);
        }

        protected void ddlOrderPlacementType_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            isInEdit = true;
            // row in edit mode for updates
            DropDownList ddl = sender as DropDownList;

            SetFaxEmailViewMode(ddl);
        }

        private static void SetFaxEmailViewMode(DropDownList ddl)
        {
            Telerik.Web.UI.RadMaskedTextBox txtFax = ddl.Parent.FindControl("txtFaxOrderPlacement") as Telerik.Web.UI.RadMaskedTextBox;
            TextBox txtEmail = ddl.Parent.FindControl("txtEmailOrderPlacement") as TextBox;

            System.Web.UI.HtmlControls.HtmlControl lblFax = ddl.Parent.FindControl("lblEditFaxOrderPlacement") as System.Web.UI.HtmlControls.HtmlControl;
            System.Web.UI.HtmlControls.HtmlControl lblEmail = ddl.Parent.FindControl("lblEditEmailOrderPlacement") as System.Web.UI.HtmlControls.HtmlControl;

            RequiredFieldValidator rfvFax = ddl.Parent.FindControl("rfvItemFaxOrderPlacement") as RequiredFieldValidator;
            RequiredFieldValidator rfvEmail = ddl.Parent.FindControl("rfvItemEmailOrderPlacement") as RequiredFieldValidator;

            SetFaxEmailVisibility(ddl.SelectedValue, txtFax, txtEmail, lblFax, lblEmail, rfvFax, rfvEmail);
            
        }

        private static void SetFaxEmailVisibility(string placementType, Control txtFax, Control txtEmail, System.Web.UI.HtmlControls.HtmlControl lblFax, System.Web.UI.HtmlControls.HtmlControl lblEmail, RequiredFieldValidator rfvFax, RequiredFieldValidator rfvEmail)
        {

            if (placementType == "Fax")
            {
                bool visible = false;
                SetPlacementVisibility(txtFax, txtEmail, lblFax, lblEmail, rfvFax, rfvEmail, visible);
                ((ITextControl)txtEmail).Text = "";
            }
            else
            {
                bool visible = true;
                SetPlacementVisibility(txtFax, txtEmail, lblFax, lblEmail, rfvFax, rfvEmail, visible);
                ((ITextControl)txtFax).Text = "";
            }
        }

        private static void SetPlacementVisibility(Control txtFax, Control txtEmail, System.Web.UI.HtmlControls.HtmlControl lblFax, System.Web.UI.HtmlControls.HtmlControl lblEmail, RequiredFieldValidator rfvFax, RequiredFieldValidator rfvEmail, bool visible)
        {
            txtEmail.Visible = visible;
            lblEmail.Visible = visible;
            if (rfvEmail != null)
            {
                rfvEmail.Visible = visible;
                rfvEmail.Enabled = visible;
            }

            txtFax.Visible = !visible;
            lblFax.Visible = !visible;
            if (rfvFax != null)
            {
                rfvFax.Visible = !visible;
                rfvFax.Enabled = !visible;
            }
        }



        //protected void btnBack_Click(object sender, EventArgs e)
        //{

        //}

        protected eSetupStatus ValidateSetupData()
        {
            Page.Validate();
            if (Page.IsValid && FaxNumbersVerified())
            {
                return eSetupStatus.Complete;
            }
            return eSetupStatus.Incomplete;
        }

        private bool FaxNumbersVerified()
        {
            DataTable dt = GetThirdPartySuppliers(practiceID);
            foreach (DataRow row in dt.Rows)
            {
                bool isFax = !Convert.ToBoolean(row["IsEmailOrderPlacement"]);
                bool isFaxVerified = Convert.ToBoolean(row["IsFaxVerified"]);
                if (isFax == true && isFaxVerified == false)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// IVisionWizardControl interface save.  Called by wizard control on tab change
        /// </summary>
        public void Save()
        {
            //do nothing.  Grid rows are saved individually

            SaveStatus(practiceID, -1, this.ToString(), ValidateSetupData());
        }
    }
}