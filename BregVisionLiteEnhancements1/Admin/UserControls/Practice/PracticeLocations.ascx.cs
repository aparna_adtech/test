﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.Admin.PracticeSetup;
using ClassLibrary.DAL;

namespace BregVision.Admin.UserControls
{
    public partial class PracticeLocations : PracticeSetupBase, IVisionWizardControl
    {

        public bool DisableLink { get; set; }

        const string IMAGE_PATH = @"~\RadControls\Grid\Skins\Outlook\";
        const string IMAGE_PATH_INSERT = IMAGE_PATH + "Insert.gif";
        const string IMAGE_PATH_ADDRECORD = IMAGE_PATH + "AddRecord.gif";

        private bool IsInEditMode = false;
        /// <summary>\
        /// IVisionWizardControl Interface.
        /// </summary>
        public bool NeedsInitialBind { get; set; }
        
        protected void Page_Load(object sender, EventArgs e)
        {

            
        }


        // Error in Page when Practice Location has 0 location and addNew images or Edit image is clicked.
        protected void Page_PreRender(object sender, EventArgs e)
        {
            //  Get User ID and Role
            //  If user is not authorized than send to login page.
            //  If user is BregAdmin
            //  If user is PracticeAdmin

            //InitializeFooterAjaxSettings(GridPracticeLocation.FooterRow);

            lblPracticeLocations.Text = practiceName + " " + " Locations";
            if (!IsInEditMode)
            {
                BindGridPracticeLocation();
            }

            lblPracticeLocations.Text = practiceName + " " + " Locations";
            lblStatus.Text = "";
            
        }

        //private void InitializeAjaxSettings()
        //{
 
        //    Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);
        //    if (radAjaxManager != null)
        //    {
        //        radAjaxManager.AjaxSettings.AddAjaxSetting(GridPracticeLocation, GridPracticeLocation);
        //    }

        //}

        //private void InitializeFooterAjaxSettings(GridViewRow footerRow)
        //{
        //    // Get the Controls within the footer.
        //    TextBox txtNewPracticeLocationName = null;
        //    ImageButton imageButtonAddNew = null;
        //    ImageButton imageButtonAddNewCancel = null;
        //    DropDownList ddlNewIsPrimaryLocationYesNo = null;

        //    if (footerRow != null)
        //    {
        //        txtNewPracticeLocationName = footerRow.FindControl("txtNewPracticeLocationName") as TextBox;
        //        imageButtonAddNew = footerRow.FindControl("ImageButtonAddNew") as ImageButton;
        //        imageButtonAddNewCancel = footerRow.FindControl("ImageButtonAddNewCancel") as ImageButton;
        //        ddlNewIsPrimaryLocationYesNo = footerRow.FindControl("ddlNewIsPrimaryLocationYesNo") as DropDownList;
        //    }
        //    Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);
        //    if (radAjaxManager != null)
        //    {

        //        if (imageButtonAddNew != null)
        //        {
        //            radAjaxManager.AjaxSettings.AddAjaxSetting(GridPracticeLocation, imageButtonAddNew);
        //            radAjaxManager.AjaxSettings.AddAjaxSetting(imageButtonAddNew, imageButtonAddNew);
        //            radAjaxManager.AjaxSettings.AddAjaxSetting(imageButtonAddNew, GridPracticeLocation);

        //            if (txtNewPracticeLocationName != null)
        //            {
        //                radAjaxManager.AjaxSettings.AddAjaxSetting(imageButtonAddNew, txtNewPracticeLocationName);
        //            }
        //            if (ddlNewIsPrimaryLocationYesNo != null)
        //            {
        //                radAjaxManager.AjaxSettings.AddAjaxSetting(imageButtonAddNew, ddlNewIsPrimaryLocationYesNo);
        //            }
        //            if (imageButtonAddNewCancel != null)
        //            {
        //                radAjaxManager.AjaxSettings.AddAjaxSetting(imageButtonAddNew, imageButtonAddNewCancel);
        //            }
        //        }

        //    }

        //}

        //private void InitializeFooterAddUpdateAjaxSettings(GridViewRow footerRow)
        //{
        //    // Get the Controls within the footer.
        //    ImageButton imageButtonAddNew = null;

        //    if (footerRow != null)
        //    {
        //        imageButtonAddNew = footerRow.FindControl("ImageButtonAddNew") as ImageButton;
            
        //        Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);
        //        if (radAjaxManager != null)
        //        {

        //            if (imageButtonAddNew != null)
        //            {
        //                radAjaxManager.AjaxSettings.AddAjaxSetting(imageButtonAddNew, imageButtonAddNew);
        //            }
     
        //        }
        //    }

        //}


        //  Get all Practice Locations and bind the Data to the GridPracticeLocation.
        protected void BindGridPracticeLocation()
        {
            BLL.PracticeLocation.PracticeLocation bllPracticeLocation = new BLL.PracticeLocation.PracticeLocation();
            DataTable dtPracticeLocation = null;

            if (Page.User.IsInRole("PracticeLocationUser") == true)
            {
                dtPracticeLocation = bllPracticeLocation.SelectAccessibleToUser(practiceID, Context.User.Identity.Name.ToString());
            }
            else
            {
                dtPracticeLocation = bllPracticeLocation.SelectAll(practiceID);
            }

            if (dtPracticeLocation.Rows.Count > 0)
            {
                GridPracticeLocation.DataSource = dtPracticeLocation;  //dvPracticeLocation;
                GridPracticeLocation.DataBind();

                SetVisionLiteMaxLocations(dtPracticeLocation);
                SetVisionLiteChangeDefaultLocationNameLabel(dtPracticeLocation);
                // if sessionPracticeLocationID is set then select the row in the GridView.
                if ( practiceLocationID > 0 )
                {

                    foreach (GridViewRow row in GridPracticeLocation.Rows)
                    {
                        int gridPracticeLocationID = Convert.ToInt32(GridPracticeLocation.DataKeys[row.RowIndex].Values[1]);

                        if (gridPracticeLocationID == practiceLocationID)
                        {
                            GridPracticeLocation.SelectedIndex = row.RowIndex;
                            break;
                        }
                    }
                }
                else
                {
                    GridPracticeLocation.SelectedIndex = -1;
                }
            }
            else  //No records found - insert a display row so the GridView will display.
            {
                //GridPracticeLocation.AllowSorting = false;
                DisplayNoRecordsFoundRow(dtPracticeLocation);
            }
        }

        private void SetVisionLiteChangeDefaultLocationNameLabel(DataTable dtPracticeLocation)
        {
            foreach (DataRow row in dtPracticeLocation.Rows)
            {
                if (row["Name"].ToString().ToLower() == "Default Location".ToLower())
                {

                    lblStatus.Text = "Please Rename the default location to complete this step.";
                    break;
                }
            }
        }

        private void SetVisionLiteMaxLocations(DataTable dtPracticeLocation)
        {
            if (BLL.Website.IsVisionExpress() && dtPracticeLocation.Rows.Count > 1)
            {
                ImageButton addNewButton = GridPracticeLocation.FooterRow.FindControl("ImageButtonAddNew") as ImageButton;
                if (addNewButton != null)
                {
                    addNewButton.Enabled = false;
                    addNewButton.ToolTip = "Vision Express only allowed 2 locations";
                }
            }
        }

        //  Display "No Records Found" when the grid's data source has now records.
        protected void DisplayNoRecordsFoundRow(DataTable dtPracticeLocation)
        {
            dtPracticeLocation.Rows.Add(dtPracticeLocation.NewRow());
            
            GridPracticeLocation.DataSource = dtPracticeLocation;
            GridPracticeLocation.DataBind();

            int totalColumns = GridPracticeLocation.Rows[0].Cells.Count;
            GridPracticeLocation.Rows[0].Cells.Clear();
            GridPracticeLocation.Rows[0].Cells.Add(new TableCell());
            GridPracticeLocation.Rows[0].Cells[0].ColumnSpan = totalColumns;
            GridPracticeLocation.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
            GridPracticeLocation.Rows[0].Cells[0].Text = "No Records Found";
        }

        //protected void GridPracticeLocation_OnRowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.Footer)
        //    {
        //        e.Row.PreRender += new EventHandler(Row_PreRender);
        //    }
        //}

        //void Row_PreRender(object sender, EventArgs e)
        //{
        //    GridViewRow row = sender as GridViewRow;
        //    //InitializeFooterAddUpdateAjaxSettings(row);
        //}

        // Set the Drop Down List
        protected void GridPracticeLocation_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            // use if neccessary
            // ddlSupplierType   ddlOrderPlacementType
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton locationLink = e.Row.FindControl("linkPracticeLocationName") as LinkButton;
                if (locationLink != null)
                {
                    locationLink.Enabled = !DisableLink;
                }

                ImageButton imgDelete = e.Row.FindControl("imgDelete") as ImageButton;
                if (imgDelete != null)
                {
                    imgDelete.Attributes.Add("onclick", "return confirm('Are you sure you want delete this Location?');");
                }
                

            }
            //else if (e.Row.RowType == DataControlRowType.Footer)
            //{
            //    InitializeFooterAjaxSettings(e.Row);
            //}
        }

        // Update the edited row.
        protected void GridPracticeLocation_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int updatePracticeID = Convert.ToInt32(GridPracticeLocation.DataKeys[e.RowIndex].Values[0].ToString());
            int updatePracticeLocationID = Convert.ToInt32(GridPracticeLocation.DataKeys[e.RowIndex].Values[1].ToString());
            TextBox txtPracticeLocationName = (TextBox)GridPracticeLocation.Rows[e.RowIndex].FindControl("txtPracticeLocationName");
	    TextBox txtGLNumber = (TextBox)GridPracticeLocation.Rows[e.RowIndex].FindControl("txtGLNumber");
            DropDownList ddlIsPrimaryLocationYesNo = (DropDownList)GridPracticeLocation.Rows[e.RowIndex].FindControl("ddlIsPrimaryLocationYesNo");
            CheckBox chkIsHCPSignatureRequired = (CheckBox)GridPracticeLocation.Rows[e.RowIndex].FindControl("chkIsHCPSignatureRequired");

            BLL.PracticeLocation.PracticeLocation bllPracticeLocation = new BLL.PracticeLocation.PracticeLocation();
            bllPracticeLocation.PracticeID = updatePracticeID;
            bllPracticeLocation.PracticeLocationID = updatePracticeLocationID;
            bllPracticeLocation.Name = txtPracticeLocationName.Text.ToString();
            bllPracticeLocation.IsPrimaryLocation = (ddlIsPrimaryLocationYesNo.SelectedValue.ToString() == "Yes") ? true : false;
            bllPracticeLocation.IsHCPSignatureRequired = (chkIsHCPSignatureRequired.Checked);
            bllPracticeLocation.UserID = userID;

            using (var db = VisionDataContext.GetVisionDataContext()) 
            { 
                var practiceLocation = 
                    db.PracticeLocations.First(pl => pl.PracticeLocationID == updatePracticeLocationID); 
                practiceLocation.GeneralLedgerNumber = txtGLNumber.Text; 
                db.SubmitChanges(); 
            } 

            bllPracticeLocation.Update(bllPracticeLocation);
            //set centralized or decentralized billing address for this new location(Actually, redoes the billing addresses for all practice locations)
            DAL.Practice.BillingAddress.UpdatePracticeLocationBillingAddresses(updatePracticeID, userID);

            // Set Edit Mode back to Read Only.
            GridPracticeLocation.EditIndex = -1;
            BindGridPracticeLocation();

        }

        //  User Clicked Edit, so allow the row to be edited.
        protected void GridPracticeLocation_RowEditing(object sender, GridViewEditEventArgs e)
        {
            IsInEditMode = true;
            string yesNo = GridPracticeLocation.DataKeys[e.NewEditIndex].Values[4].ToString();
            GridPracticeLocation.EditIndex = e.NewEditIndex;
            BindGridPracticeLocation();

            DropDownList ddlIsPrimaryLocationYesNo1 = (DropDownList)GridPracticeLocation.Rows[GridPracticeLocation.EditIndex].FindControl("ddlIsPrimaryLocationYesNo");
            ddlIsPrimaryLocationYesNo1.SelectedValue = yesNo;

        }

        //  User Canceled the Row Edit, so remove the editing from the row.
        protected void GridPracticeLocation_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridPracticeLocation.EditIndex = -1;
            BindGridPracticeLocation();
        }

        //  User click Delete so Delete the Practice.
        public void GridPracticeLocation_RowDeleting(Object sender, GridViewDeleteEventArgs e)
        {
            if (GridPracticeLocation.Rows.Count > 1)
            {
                int deletePracticeLocationID = Convert.ToInt32(GridPracticeLocation.DataKeys[e.RowIndex].Values[1].ToString());

                BLL.PracticeLocation.PracticeLocation bllPracticeLocation = new BLL.PracticeLocation.PracticeLocation();
                bllPracticeLocation.PracticeLocationID = deletePracticeLocationID;
                bllPracticeLocation.UserID = userID;
                int rowsDeleted = bllPracticeLocation.Delete(bllPracticeLocation);
                BindGridPracticeLocation();
                if( rowsDeleted <  1)
                {
                    lblStatus.Text = "Unable to delete location";
                }
            }
            else
            {
                lblStatus.Text = "Must have at least one location";
                e.Cancel = true;
            }
        }

        // User Clicked "AddNew" or "Insert" or "Cancel" (ed) the AddNew.
        protected void GridPracticeLocation_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("GoToPracticeLocation"))
            {
                //Find the selected row index
                //int index = Convert.ToInt32( e.CommandArgument );

                practiceLocationID = Convert.ToInt32(e.CommandArgument); //GridPracticeLocation.DataKeys[index].Values[1];
                //Session["PracticeLocationName"] = e.CommandSource.Text.ToString();  //GridPracticeLocation.DataKeys[index].Values[2];
                LinkButton lb = (LinkButton)e.CommandSource;
                practiceLocationName = lb.Text.ToString();
                Response.Redirect("PracticeLocation_ShippingAddress.aspx", false);
                return;
            }


            // Check if AddNew Button was clicked.
            if (e.CommandName.Equals("AddNew"))
            {
                // Get the Controls within the footer.
                TextBox txtNewPracticeLocationName = (TextBox)GridPracticeLocation.FooterRow.FindControl("txtNewPracticeLocationName");
                ImageButton imageButtonAddNew = (ImageButton)((GridView)sender).FooterRow.FindControl("ImageButtonAddNew");
                ImageButton imageButtonAddNewCancel = (ImageButton)GridPracticeLocation.FooterRow.FindControl("ImageButtonAddNewCancel");
                DropDownList ddlNewIsPrimaryLocationYesNo = (DropDownList)GridPracticeLocation.FooterRow.FindControl("ddlNewIsPrimaryLocationYesNo");
		TextBox txtNewGLId = (TextBox)GridPracticeLocation.FooterRow.FindControl("txtNewGLNumber");
                CheckBox chkNewIsHCPSignatureRequired = (CheckBox)GridPracticeLocation.FooterRow.FindControl("chkNewIsHCPSignatureRequired");

                //  Check if the AddNew button was clicked to Add a New record as opposed to Inserting a New Record
                if (imageButtonAddNewCancel.Visible == false)
                {

                    // User clicked to Add a New Record.  Show New Fields for input and show the Insert and Cancel buttons.
                    txtNewPracticeLocationName.Visible = true;
                    ddlNewIsPrimaryLocationYesNo.Visible = true;
		    txtNewGLId.Visible = true;
                    chkNewIsHCPSignatureRequired.Visible = true;

                    imageButtonAddNew.ImageUrl = IMAGE_PATH_INSERT;  //@"~\RadControls\Grid\Skins\Outlook\Insert.gif";
                    imageButtonAddNew.ToolTip = "Insert New Practice Location";
                    
                    imageButtonAddNewCancel.Visible = true;

                    // If the "No Records Found" was displayed before the user clicked AddNew than clear that row.
                    if (GridPracticeLocation.Rows[0].Cells[0].Text == "No Records Found")
                    {
                        GridPracticeLocation.Rows[0].Cells.Clear();
                        // BindGridPracticeLocation();
                    }
                }
                else
                {
                    // User clicked to Insert a New Record.  
                    // So Insert the Naw Record.
                    // Hide New Fields for input and the Insert and Cancel buttons.
                    // Show the Add New Button

                    string newPracticeName = txtNewPracticeLocationName.Text.ToString();
		    string newGeneralLedgerNumber = txtNewGLId.Text;

                    //int practiceID = Convert.ToInt32( GridPracticeLocation.FooterRow.FindControl["PracticeID"].  .DataKeys[footer  [e.RowIndex].Values[0].ToString() );

                    BLL.PracticeLocation.PracticeLocation bllPracticeLocation = new BLL.PracticeLocation.PracticeLocation();
                    bllPracticeLocation.PracticeID = practiceID;
                    bllPracticeLocation.Name = newPracticeName;
                    bllPracticeLocation.IsPrimaryLocation = (ddlNewIsPrimaryLocationYesNo.SelectedValue.ToString() == "Yes") ? true : false;
                    bllPracticeLocation.UserID = userID;
		    bllPracticeLocation.GeneralLedgerNumber = newGeneralLedgerNumber;
                    bllPracticeLocation.IsHCPSignatureRequired = chkNewIsHCPSignatureRequired.Checked;

                    bllPracticeLocation.Insert(bllPracticeLocation);

                    // hide txtNewPracticeLocationName and AddNewCancel button.  Switch Tooltip to Add New Record.
                    txtNewPracticeLocationName.Visible = false;
		    txtNewGLId.Visible = false;

                    imageButtonAddNew.ImageUrl = IMAGE_PATH_ADDRECORD;  //@"~\App_Themes\Breg\images\Common\add-record.png";
                    imageButtonAddNew.ToolTip = "Add New Practice Location";
                    imageButtonAddNewCancel.Visible = false;

                    BindGridPracticeLocation();
                }
                IsInEditMode = true;
            }

            //  User Canceled the insertion of a new record.
            //  Revert mode back to AddNew.
            if (e.CommandName.Equals("AddNewCancel"))
            {
                TextBox txtNewPracticeLocationName = (TextBox)GridPracticeLocation.FooterRow.FindControl("txtNewPracticeLocationName");
                ImageButton imageButtonAddNew = (ImageButton)GridPracticeLocation.FooterRow.FindControl("ImageButtonAddNew");
                ImageButton imageButtonAddNewCancel = (ImageButton)GridPracticeLocation.FooterRow.FindControl("ImageButtonAddNewCancel");
                DropDownList ddlNewIsPrimaryLocationYesNo = (DropDownList)GridPracticeLocation.FooterRow.FindControl("ddlNewIsPrimaryLocationYesNo");
		TextBox txtNewGLNumber = (TextBox)GridPracticeLocation.FooterRow.FindControl("txtNewGLNumber");
                CheckBox chkNewIsHCPSignatureRequired = (CheckBox)GridPracticeLocation.FooterRow.FindControl("chkNewIsHCPSignatureRequired");

                // hide txtNewPracticeLocationName and AddNewCancel button.  Switch Tooltip to Add New Record.
                txtNewPracticeLocationName.Visible = false;
                ddlNewIsPrimaryLocationYesNo.Visible = false;
		txtNewGLNumber.Visible = false;
                chkNewIsHCPSignatureRequired.Visible = false;

                imageButtonAddNew.ImageUrl = IMAGE_PATH_ADDRECORD;  // @"~\App_Themes\Breg\images\Common\add-record.png";
                imageButtonAddNew.ToolTip = "Add New Practice Location";

                imageButtonAddNewCancel.Visible = false;

                BindGridPracticeLocation();
            }
        }
        protected bool ValidateDefaultNameHasChanged()
        {
            foreach (GridViewRow row in GridPracticeLocation.Rows)
            {
                LinkButton linkPracticeLocationName = row.FindControl("linkPracticeLocationName") as LinkButton;
                if (linkPracticeLocationName != null)
                {
                    if (linkPracticeLocationName.Text.ToLower() == "default location".ToLower())
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        protected eSetupStatus ValidateSetupData()
        {
            Page.Validate();
            if (Page.IsValid)
            {
                if (ValidateDefaultNameHasChanged())
                {
                    return eSetupStatus.Complete;
                }
            }
            return eSetupStatus.Incomplete;
        }

        /// <summary>
        /// IVisionWizardControl interface save.  Called by wizard control on tab change
        /// </summary>
        public void Save()
        {
            // don't do anything.  Each Row saves itself

            SaveStatus(practiceID, -1, this.ToString(), ValidateSetupData());
        }
    }
}
