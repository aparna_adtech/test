﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeLocations.ascx.cs" Inherits="BregVision.Admin.UserControls.PracticeLocations" %>
<div class="flex-row">
  <h2>
                                    <asp:Label ID="lblPracticeLocations" runat="server" Text="Practice Locations"></asp:Label>
    <bvu:Help ID="help40" runat="server" HelpID="40"/>
  </h2>
                                    <asp:Label ID="lblStatus" runat="server" CssClass="WarningMsg"></asp:Label>
</div>
<br/>
<div class="flex-row child-float-left">
                                    <asp:GridView ID="GridPracticeLocation" runat="server" AutoGenerateColumns="False"
    DataKeyNames="practiceID,practiceLocationID,Name,isPrimaryLocation,IsPrimaryLocationYesNo"
                                        OnRowDataBound="GridPracticeLocation_RowDataBound"
                                        OnRowCancelingEdit="GridPracticeLocation_RowCancelingEdit" 
                                        OnRowCommand="GridPracticeLocation_RowCommand"
                                        OnRowDeleting="GridPracticeLocation_RowDeleting" 
                                        OnRowEditing="GridPracticeLocation_RowEditing"
                                        OnRowUpdating="GridPracticeLocation_RowUpdating" 
    ShowFooter="True" Skin="Breg" CssClass="RadGrid_Breg" EnableTheming="False">
                                        <Columns>
      <asp:TemplateField HeaderStyle-Width="30px">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="True"
                           CommandName="Delete" ImageUrl="~/App_Themes/Breg/images/Common/delete.png" ToolTip="Delete Location"/>
                                                </ItemTemplate>
                                            
        <HeaderStyle Width="30px"></HeaderStyle>
      </asp:TemplateField>
      <asp:TemplateField HeaderStyle-Width="30px" ShowHeader="False">
                                                <EditItemTemplate>
                                                    <asp:ImageButton ID="ImageButtonEditUpdate" runat="server" CausesValidation="True"
                           CommandName="Update" ImageUrl="~/App_Themes/Breg/images/Common/checkCircle.png" ToolTip="Update"/>&nbsp;
                                                    <asp:ImageButton ID="ImageButtonEditCancel" runat="server" CausesValidation="False"
                           CommandName="Cancel" ImageUrl="~/App_Themes/Breg/images/Common/cancelCircle.png" ToolTip="Cancel"/>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImageButtonEdit" runat="server" CausesValidation="False" CommandName="Edit"
                           ImageUrl="~/App_Themes/Breg/images/Common/edit.png" ToolTip="Edit"/>
                                                </ItemTemplate>
                                                <FooterTemplate>
          <asp:ImageButton ID="ImageButtonAddNew" runat="server" CommandName="AddNew" ImageUrl="~\App_Themes\Breg\images\Common\add-record.png"
                           ToolTip="Add New Practice Location"/>
                                                    <asp:ImageButton ID="ImageButtonAddNewCancel" runat="server" CommandName="AddNewCancel"
            ImageUrl="~/App_Themes/Breg/images/Common/cancelCircle.png"
                           ToolTip="Cancel" Visible="false"/>
                                                </FooterTemplate>

        <HeaderStyle Width="30px"></HeaderStyle>
                                            </asp:TemplateField>
      <asp:BoundField DataField="PracticeID" Visible="False"/>
                                            <asp:TemplateField ConvertEmptyStringToNull="False" InsertVisible="False" Visible="False">
                                                <EditItemTemplate>
          <asp:TextBox ID="txtPracticeLocationIDEdit" runat="server" Text='<%# Eval("PracticeLocationID") %>'
                                                        Visible="false">
                                                    </asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
          <asp:Label ID="lblPracticeLocationID" runat="server" Text='<%# Eval("PracticeLocationID") %>'
            Visible="false">
          </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Location Name">
                                                <EditItemTemplate>
          <asp:TextBox ID="txtPracticeLocationName" runat="server" CssClass="no-wrap" Text='<%# Eval("Name") %>'>
          </asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
          <asp:LinkButton ID="linkPracticeLocationName" runat="server" CommandArgument='<%# Eval("PracticeLocationID") %>'
            CommandName="GoToPracticeLocation" Text='<%# Eval("Name") %>' CssClass="no-wrap">
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                                <HeaderTemplate>
          <asp:Label ID="lblPracticeLocationNameHeader" runat="server" Text="Location"></asp:Label>
                                                </HeaderTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtNewPracticeLocationName" runat="server" Visible="false"></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Wrap="false">
                                                <HeaderTemplate>
                                                    <asp:Label runat="server">Is Primary Location</asp:Label>
          <bvu:Help ID="help41" runat="server" HelpID="41"/>
                                                </HeaderTemplate>
                                                <ItemTemplate>
          <asp:Label ID="lblIsPrimaryLocationYesNo" runat="server" Text='<%# Eval("IsPrimaryLocationYesNo") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
           <div class="Dropdown-Container">
            <asp:DropDownList ID="ddlIsPrimaryLocationYesNo" runat="server" CssClass="RadComboBox_Breg"
                                                        DataTextField="IsPrimaryLocationYesNo" DataValueField="IsPrimaryLocationYesNo">
                                                        <asp:ListItem>No</asp:ListItem>
                                                        <asp:ListItem>Yes</asp:ListItem>
                                                    </asp:DropDownList>
           </div>
                                                </EditItemTemplate>
                                                <FooterTemplate>
           <div class="Dropdown-Container">          
          <asp:DropDownList ID="ddlNewIsPrimaryLocationYesNo" runat="server" CssClass="RadComboBox_Breg"
                                                        DataTextField="IsPrimaryLocationYesNo" DataValueField="IsPrimaryLocationYesNo"
                                                        Visible="false">
                                                        <asp:ListItem>No</asp:ListItem>
                                                        <asp:ListItem>Yes</asp:ListItem>
                                                    </asp:DropDownList>
        </div>

                                                </FooterTemplate>
        <HeaderStyle Wrap="False"></HeaderStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Wrap="false">
                                                <HeaderTemplate>
                                                    <asp:Label runat="server">HCP Signature</asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                    <asp:CheckBox ID="chkIsHCPSignatureRequired" runat="server" CssClass="FieldLabel"  Enabled="False" DataTextField="IsHCPSignatureRequired" DataValueField="IsHCPSignatureRequired" Checked='<%#  Eval("IsHCPSignatureRequired") == DBNull.Value ? false : Eval("IsHCPSignatureRequired") %>'/>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                    <asp:CheckBox ID="chkIsHCPSignatureRequired" runat="server" CssClass="FieldLabel" Checked='<%#  Eval("IsHCPSignatureRequired") == DBNull.Value ? false : Eval("IsHCPSignatureRequired") %>'/>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:CheckBox ID="chkNewIsHCPSignatureRequired" runat="server" CssClass="FieldLabel"
                                                        DataTextField="IsHCPSignatureRequired" DataValueField="IsHCPSignatureRequired"
                                                        Visible="false">
                                                    </asp:CheckBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Location General Ledger #" SortExpression="GLNumber"> 
                                                <EditItemTemplate> 
                                                    <asp:TextBox ID="txtGLNumber" runat="server" Text='<%# Bind("GLNumber") %>' MaxLength="50"></asp:TextBox> 
                                                </EditItemTemplate> 
                                                <FooterTemplate> 
                                                    <asp:TextBox ID="txtNewGLNumber" runat="server" Visible="False" MaxLength="50"></asp:TextBox> 
                                                </FooterTemplate> 
                                                <ItemTemplate> 
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("GLNumber") %>'></asp:Label> 
                                                </ItemTemplate> 
                                            </asp:TemplateField>
                                        </Columns>
    <RowStyle Font-Bold="False" Font-Italic="False"/>
    <HeaderStyle CssClass="AdminSubTitle"/>
                                    </asp:GridView>
</div>
