﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary.DAL;

namespace BregVision.Admin.UserControls.Practice
{
    public partial class PracticeConsignmentRep : System.Web.UI.UserControl
    {
        private int _practiceID = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            ResetSaveButtonTable1.Save += new EventHandler(ResetSaveButtonTable1_Save);
            ResetSaveButtonTable1.Reset += new EventHandler(btnReset_Click);
            _practiceID = Convert.ToInt32(Request.QueryString["pid"].ToString());

            if (!Page.IsPostBack)
            {                               
                ////practiceID = 
                //List<usp_GetConsignmentRepsAllResult> reps = GetAllConsignmentReps();
                //List<usp_GetConsignmentRepsByPracticeResult> repsPractice = GetConsignmentRepsByPractice(_practiceID);
                ////reps[0].

                PopulatelistBoxConsignmentRepsAll();
                //listBoxGetConsignmentRepsAll.DataSource = reps;
                //listBoxGetConsignmentRepsAll.DataTextField = "UserName";
                //listBoxGetConsignmentRepsAll.DataValueField = "UserId";
                //listBoxGetConsignmentRepsAll.DataBind();

                PopulatelistBoxConsignmentPractice();
                //listBoxGetConsignmentPractice.DataSource = repsPractice;               
                //listBoxGetConsignmentPractice.DataTextField = "UserName";
                //listBoxGetConsignmentPractice.DataValueField = "UserId";
                //listBoxGetConsignmentPractice.DataBind();
            }
        }

        private void PopulatelistBoxConsignmentPractice()
        {
            List<usp_GetConsignmentRepsByPracticeResult> repsPractice = GetConsignmentRepsByPractice(_practiceID);

            listBoxGetConsignmentPractice.DataSource = repsPractice;
            listBoxGetConsignmentPractice.DataTextField = "UserName";
            listBoxGetConsignmentPractice.DataValueField = "UserId";
            listBoxGetConsignmentPractice.DataBind();
        }

        private void PopulatelistBoxConsignmentRepsAll() 
        {

            List<usp_GetConsignmentRepsAllResult> allReps = GetAllConsignmentReps();
            List<usp_GetConsignmentRepsByPracticeResult> repsPractice = GetConsignmentRepsByPractice(_practiceID);

            var reps = (from r in allReps
                        where !(from pr in repsPractice select pr.UserId).Contains(r.UserId)
                        select r).ToList();


            listBoxGetConsignmentRepsAll.DataSource = reps;
            listBoxGetConsignmentRepsAll.DataTextField = "UserName";
            listBoxGetConsignmentRepsAll.DataValueField = "UserId";
            listBoxGetConsignmentRepsAll.DataBind();
        }


        void ResetSaveButtonTable1_Reset(object sender, EventArgs e)
        {
            //throw new NotImplementedException(btnReset_Click);
        }


        void ResetSaveButtonTable1_Save(object sender, EventArgs e)
        {
            int countPracticeID = listBoxGetConsignmentPractice.Items.Count;
            int index = countPracticeID - 1;

            for (int i = index; i >= 0; i--)
            {
                ListItem listItem = new ListItem();
                listItem = listBoxGetConsignmentPractice.Items[i];

                if (listItem.Selected)
                {
                    Guid userID = Guid.Parse (listItem.Value);
                    SavePracticeConsignmentReps(userID, _practiceID);
                }
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {            
            GetConsignmentRepsAndLoad(_practiceID);
        }

        protected void GetConsignmentRepsAndLoad(int practiceID)
        {
            object dtAllAvailabieConsignmentReps = GetAllConsignmentReps(); 
            PopulatelistBoxConsignmentRepsAll();

            object dtConsignmentReps = GetConsignmentRepsByPractice(practiceID);
            PopulatelistBoxConsignmentPractice();

        }

        private void SavePracticeConsignmentReps(Guid userID, int _practiceID)
        {
            //this saves consignmentReps one at a time
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var repsQuery = from r in visionDB.ConsignmentRep_Practices 
                                where r.UserID == userID 
                                && r.PracticeID == _practiceID 
                                select r;
                if (repsQuery != null)
                {
                    List<ConsignmentRep_Practice> reps = repsQuery.ToList<ConsignmentRep_Practice>();

                    if (reps.Count < 1)
                    {
                        ConsignmentRep_Practice crp = new ConsignmentRep_Practice();
                        crp.UserID = userID;
                        crp.PracticeID = _practiceID;
                        crp.IsActive = true;
                        visionDB.ConsignmentRep_Practices.InsertOnSubmit(crp);
                        visionDB.SubmitChanges();
                    }
                    else 
                    {
                        ConsignmentRep_Practice crp = reps.FirstOrDefault<ConsignmentRep_Practice>();
                        crp.IsActive = true;
                        visionDB.SubmitChanges();
                    }
                }             
                
            }
        }

        public List<usp_GetConsignmentRepsAllResult> GetAllConsignmentReps()
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {

                var repsQuery = from r in visionDB.usp_GetConsignmentRepsAll()
                                select r;

                return repsQuery.ToList<usp_GetConsignmentRepsAllResult>();
            }
        }

        public List<usp_GetConsignmentRepsByPracticeResult> GetConsignmentRepsByPractice(int practiceID) 
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var repsByPracticeQuery = from rb in visionDB.usp_GetConsignmentRepsByPractice(practiceID)
                                          select rb;

                return repsByPracticeQuery.ToList<usp_GetConsignmentRepsByPracticeResult>();
            
            }
        
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            int x;

            x = listBoxGetConsignmentRepsAll.Items.Count - 1;

            for (int i = x; i >= 0; i--)
            {
                ListItem listItem = new ListItem();
                listItem = listBoxGetConsignmentRepsAll.Items[i];

                if (listItem.Selected)
                {
                    listBoxGetConsignmentPractice.Items.Add(listItem);

                    listBoxGetConsignmentRepsAll.Items[i].Attributes.Add("style", "color:blue");
                    listBoxGetConsignmentRepsAll.Items.Remove(listItem);
                }
            }
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            int x;

            x = listBoxGetConsignmentPractice.Items.Count - 1;

            for (int i = x; i >= 0; i--)
            {
                ListItem listItem = new ListItem();
                listItem = listBoxGetConsignmentPractice.Items[i];

                if (listItem.Selected)
                {
                    Guid userID = Guid.Empty;
                    
                    Guid.TryParse(listItem.Value, out userID);

                    if (userID != Guid.Empty)
                    {
                        DeletePracticeICD(userID);
                        listBoxGetConsignmentPractice.Items.Remove(listItem);

                        listBoxGetConsignmentRepsAll.Items.Add(listItem);
                    }
                }
            }
        }

        protected void DeletePracticeICD(Guid userID)
        {
            // set as inactive.
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            var existsPracticeRep = from rep in visionDB.ConsignmentRep_Practices
                                    where rep.UserID == userID  
                                    && rep.PracticeID == _practiceID
                                    select rep;

            foreach(ConsignmentRep_Practice rep in existsPracticeRep)
            {
                rep.IsActive = false;
            }

            visionDB.SubmitChanges();

            VisionDataContext.CloseConnection(visionDB);

        }
    }
}