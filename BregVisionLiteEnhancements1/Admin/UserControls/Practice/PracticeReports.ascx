﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeReports.ascx.cs"
    Inherits="BregVision.Admin.UserControls.PracticeReports" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" />

<asp:UpdatePanel ID="ReportActionsUpdatePanel" runat="server" UpdateMode="Conditional" RenderMode="Inline" ChildrenAsTriggers="false">
    <ContentTemplate>
        <asp:HiddenField ID="DatePickers" runat="server" />
        <div class="flex-row subWelcome-bar">
            <h1>Reports
            </h1>
            <div class="Dropdown-Container Dropdown-Report">
                <asp:DropDownList ID="ddlReportVersion" AutoPostBack="True" runat="server" OnSelectedIndexChanged="ddlReportVersion_OnSelectedIndexChanged">
                    <asp:ListItem Value="all">Select a Report Version</asp:ListItem>
                    <asp:ListItem Value="view">View</asp:ListItem>
                    <asp:ListItem Value="export">Export</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="Dropdown-Container">
                <asp:DropDownList ID="ddlReport" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReport_SelectedIndexChanged" DataTextField="Text"
                    DataValueField="Value" Enabled="true" />
            </div>
            <div class="flex-row">
                <asp:Button ID="ScheduleReportButton" runat="server" Text="Schedule E-mail Report"
                    OnClick="ScheduleReportButton_Click" CssClass="button-accent" OnClientClick="ShowScheduleDiv(); return false;"/>

                <asp:Button ID="ManageScheduledReportsButton" runat="server" Text="Manage Scheduled Reports"
                    OnClick="ManageScheduledReportsButton_Click" CssClass="button-primary"  />
            </div>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="ddlReport" EventName="SelectedIndexChanged" />
        <asp:AsyncPostBackTrigger ControlID="ddlReportVersion" EventName="SelectedIndexChanged" />

        <asp:AsyncPostBackTrigger ControlID="ScheduleReportButton" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="ManageScheduledReportsButton" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>
<div class="subTabContent-container flex-column">
    <asp:UpdatePanel ID="ScheduleTab" runat="server" RenderMode="Block" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="ScheduleEmailReportPanel" runat="server" Style="display: none">
                <div class="content">
                    <h2>Schedule E-mail Report</h2>

                    <h3>Enter schedule parameters for the&thinsp;
                <em>
                    <asp:Label ID="ScheduleReportNameLabel" runat="server" Text=""></asp:Label>
                </em>
                        &thinsp;report.
                    </h3>
                    <asp:HiddenField runat="server" ID="SubscriptionId" />
                    <asp:ValidationSummary ID="ScheduleReportValidationSummary" runat="server" ShowMessageBox="True"
                        ShowSummary="False" ValidationGroup="ScheduleReportValidationGroup" />
                    <table>
                        <tr>
                            <td class="FieldLabel">Recipient E-mail Address</td>
                            <td>
                                <asp:TextBox ID="ScheduleReportRecipientTextBox" runat="server" Text="" ValidationGroup="ScheduleReportValidationGroup"
                                    Columns="45" MaxLength="200">
                                </asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="ScheduleReportRecipientTextBoxRequiredFieldValidator"
                                    runat="server" ControlToValidate="ScheduleReportRecipientTextBox" Display="Dynamic"
                                    ErrorMessage="Recipient E-mail Address is required." ValidationGroup="ScheduleReportValidationGroup">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="ScheduleReportRecipientTextBoxRegularExpressionValidator"
                                    runat="server" ControlToValidate="ScheduleReportRecipientTextBox" Display="Dynamic"
                                    ErrorMessage="Recipient E-mail Address is invalid." SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    ValidationGroup="ScheduleReportValidationGroup">
                                </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Start Date</td>
                            <td>
                                <telerik:RadDatePicker ID="ScheduleStartDateRadDatePicker" runat="server" ShowPopupOnFocus="True">
                                    <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                                        ViewSelectorText="x">
                                    </Calendar>
                                    <DateInput DateFormat="M/d/yyyy" DisplayDateFormat="M/d/yyyy">
                                    </DateInput>
                                    <DatePopupButton HoverImageUrl="~/App_Themes/Breg/images/Common/datePickerPopupHover.png" ImageUrl="~/App_Themes/Breg/images/Common/datePickerPopup.png" />
                                </telerik:RadDatePicker>
                                <asp:RequiredFieldValidator ID="ScheduleStartDateRadDatePickerRequiredFieldValidator"
                                    runat="server" ControlToValidate="ScheduleStartDateRadDatePicker" Display="Dynamic"
                                    ErrorMessage="Schedule start is required." ValidationGroup="ScheduleReportValidationGroup">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Frequency</td>
                            <td>
								<asp:UpdatePanel ID="UpdatePanel1" runat="server">
								<ContentTemplate>
                                <asp:ObjectDataSource ID="ScheduleFrequencyDataSource" runat="server" SelectMethod="GetScheduleTypes"
                                    TypeName="BregVision.Admin.UserControls.PracticeReports"></asp:ObjectDataSource>
                                <div class="Dropdown-Container">
                                    <asp:DropDownList ID="ScheduleTypeDropDownList" runat="server" AutoPostBack="True"
                                        DataSourceID="ScheduleFrequencyDataSource" DataTextField="Name" DataValueField="Value"
                                        ValidationGroup="ScheduleReportValidationGroup" OnSelectedIndexChanged="ScheduleTypeDropDownList_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <div>
                                        <asp:Panel ID="ScheduleTypeWeeklyParametersPanel" runat="server" Visible="false"
                                            GroupingText="Select enabled days of the week">
                                            <asp:ObjectDataSource ID="ScheduleTypeWeeklyEnabledDaysOfWeekDataSource" runat="server"
                                                SelectMethod="GetScheduleTypeWeeklyEnabledDaysOfWeekDataSource" TypeName="BregVision.Admin.UserControls.PracticeReports"></asp:ObjectDataSource>
                                            <asp:CheckBoxList ID="ScheduleTypeWeeklyEnabledDaysOfWeekCheckBoxList" runat="server"
                                                DataSourceID="ScheduleTypeWeeklyEnabledDaysOfWeekDataSource" DataTextField="Name"
                                                DataValueField="Value" ValidationGroup="ScheduleReportValidationGroup" RepeatColumns="2">
                                            </asp:CheckBoxList>
                                        </asp:Panel>
                                        <asp:Panel ID="ScheduleTypeMonthlyParametersPanel" runat="server" Visible="false">
                                            <asp:Panel ID="ScheduleTypeMonthlyEnabledMonthsPanel" runat="server" GroupingText="Select enabled months">
                                                <asp:ObjectDataSource ID="ScheduleTypeMonthlyEnabledMonthsDataSource" runat="server"
                                                    SelectMethod="GetScheduleTypeMonthlyEnabledMonthsDataSource" TypeName="BregVision.Admin.UserControls.PracticeReports"></asp:ObjectDataSource>
                                                <asp:CheckBoxList ID="ScheduleTypeMonthlyEnabledMonthsCheckBoxList" runat="server"
                                                    RepeatColumns="2" DataTextField="Name" DataValueField="Value" DataSourceID="ScheduleTypeMonthlyEnabledMonthsDataSource"
                                                    ValidationGroup="ScheduleReportValidationGroup">
                                                </asp:CheckBoxList>
                                            </asp:Panel>

                                            <asp:ObjectDataSource ID="ScheduleTypeMonthlyDayOfMonthToSendDataSource" runat="server"
                                                SelectMethod="GetScheduleTypeMonthlyDayOfMonthToSendDataSource" TypeName="BregVision.Admin.UserControls.PracticeReports"></asp:ObjectDataSource>
                                            <span>Day of the month to send report: </span>
                                            <telerik:RadComboBox ID="ScheduleTypeMonthlyDayOfMonthToSendRadComboBox" runat="server"
                                                DataSourceID="ScheduleTypeMonthlyDayOfMonthToSendDataSource">
                                            </telerik:RadComboBox>
                                        </asp:Panel>
                                    </div>
                                </div>
								</ContentTemplate>
								</asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                    <div>
                        <asp:Button ID="ScheduleReportCloseCancelButton" runat="server" CausesValidation="False"
                            OnClick="ScheduleReportCloseCancelButton_Click" Text="Close/Cancel" CssClass="button-accent" OnClientClick="ShowScheduleDiv(); return false;" />
                        <asp:Button ID="ScheduleReportScheduleButton" runat="server" ValidationGroup="ScheduleReportValidationGroup"
                            Text="Schedule Report" OnClick="ScheduleReportScheduleButton_Click" CssClass="button-primary" />
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ScheduleReportCloseCancelButton" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="ManageTab" runat="server" RenderMode="Block">
        <ContentTemplate>
            <asp:Panel ID="ManageScheduledReportsPanel" runat="server" Visible="false">
                <div class="content">
                    <h2>Manage Scheduled Reports</h2>
                    <div>
                        <h3>The following reports are scheduled in the system.</h3>
                        <asp:ObjectDataSource ID="ScheduledReportsDataSource" runat="server" SelectMethod="GetScheduledReports"
                            TypeName="BregVision.Admin.UserControls.PracticeReports"></asp:ObjectDataSource>
                        <asp:GridView ID="ScheduledReportsGridView" runat="server" AutoGenerateColumns="False"
                            DataSourceID="ScheduledReportsDataSource" AllowSorting="True"
                            EmptyDataText="No reports have been scheduled." Skin="Breg" EnableEmbeddedStylesheets="False">
                            <RowStyle />
                            <AlternatingRowStyle />
                            <EmptyDataRowStyle />
                            <HeaderStyle />
                            <Columns>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="Button1" runat="server" CausesValidation="false" ImageUrl="~/App_Themes/Breg/images/Common/delete.png"
                                            ImageAlign="Middle" CommandArgument='<%# Eval("SubscriptionId") %>' Text="Delete"
                                            OnCommand="ScheduledReportsDeleteButton_Command" ToolTip="Delete this scheduled report."></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="Button2" runat="server" CausesValidation="false" ImageUrl="~/RadControls/Grid/Skins/Outlook/Edit.gif"
                                            ImageAlign="Middle" CommandArgument='<%# Eval("SubscriptionId") %>' Text="Edit"
                                            OnCommand="ScheduledReportsEditButton_Command" ToolTip="Edit this scheduled report."></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ReportName" HeaderText="Report" SortExpression="ReportName" />
                                <asp:BoundField DataField="EmailRecipient" HeaderText="Recipient" SortExpression="EmailRecipient" />
                                <asp:BoundField DataField="ScheduleRecurrenceType" HeaderText="Schedule" SortExpression="ScheduleRecurrenceType">
                                    <ItemStyle />
                                </asp:BoundField>
                                <asp:BoundField DataField="DateScheduled" HeaderText="Date Scheduled" SortExpression="DateScheduled"
                                    DataFormatString="{0:d}">
                                    <ItemStyle />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <br />
                        <asp:Button ID="ManageScheduledReportsCloseButton" runat="server" Text="Close"
                            OnClick="ManageScheduledReportsCloseButton_Click" CssClass="accent-button" OnClientClick="ShowManageDiv(); return false;" />
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="ReportsOnly" runat="server" UpdateMode="Conditional" RenderMode="Inline" ChildrenAsTriggers="false">
        <ContentTemplate>
            <div class="subTabContent-container flex-column">
                <asp:UpdatePanel ID="ReportViewerUpdatePanel" runat="server" RenderMode="Block"
                    UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="content flex-full-height">
                            <asp:Panel ID="ReportActionsPanel" runat="server" Visible="false" DefaultButton="SaveParametersButton">
                                <asp:Button ID="SaveParametersButton" runat="server" Text="Save Report Parameters"
                                    OnClick="SaveParametersButton_Click" CssClass="button-accent button-primary" />
                                <asp:Button ID="ClearSavedParametersButton" runat="server" Text="Clear Saved Parameters"
                                    OnClick="ClearSavedParametersButton_Click" CssClass="button-accent button-primary" />
                            </asp:Panel>
                            <bvu:BregVisionSSRSReportViewer ID="ReportViewer1" runat="server" Visible="false" Enabled="true"
                                KeepSessionAlive="true" ProcessingMode="Remote" SizeToReportContent="false" Height="100%" Width="100%" ShowReportBody="False" OnReportRefresh="ReportViewer1_ReportRefresh">
                                <ServerReport ReportServerUrl="http://www.bregvision.com/ReportServer" ReportPath="/PracticeAdminReports/Products Dispensed Report" />
                            </bvu:BregVisionSSRSReportViewer>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlReport" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<script type="text/javascript">

    function ShowScheduleDiv(sender, args) {
        $('#<%=ScheduleEmailReportPanel.ClientID %>').toggle()
    }

    function ShowManageDiv(sender, args) {
        $('#<%=ManageScheduledReportsPanel.ClientID %>').toggle()
    }

</script>
