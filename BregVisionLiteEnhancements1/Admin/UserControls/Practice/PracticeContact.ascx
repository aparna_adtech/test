﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeContact.ascx.cs"
  Inherits="BregVision.Admin.UserControls.PracticeContact" %>
<%@ Register Src="~/UserControls/ResetSaveButtonTable.ascx" TagName="ResetSaveButtonTable"
  TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/Contact.ascx" TagName="Contact" TagPrefix="UC_Contact" %>

<div class="content">
  <div class="flex-row actions-bar">
    <h2>
      <asp:Label ID="lblContentHeader" runat="server" Text="Contact"></asp:Label>&nbsp;<bvu:Help
        ID="help7" runat="server" HelpID="7" />
    </h2>
    <uc1:ResetSaveButtonTable ID="ucButtons" runat="server" />
  </div>

  <UC_Contact:Contact ID="ucPracticeContact" runat="server" />

</div>
