﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;
using BregVision.Domain.PracticeReportScheduling.Enums;
using BregVision.Domain.PracticeReportScheduling.Models;
using BregVision.Models.PracticeReportScheduling;
using BregVision.Domain.PracticeReportScheduling.Utility;
using System.Collections;
using BregVision.UserControls;

namespace BregVision.Admin.UserControls
{
    public partial class PracticeReports : Bases.UserControlBase
    {
        #region INIT
        new protected void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);

            SetReportList();

            RemoveReportsForVisionExpress();

            RemoveSparkReports();


        }

        private ListItemCollection GetReportListAll()
        {
            ListItemCollection fullReportCollection = new ListItemCollection();
            fullReportCollection.Add(new ListItem("Select a Report", "0"));
            fullReportCollection.Add(new ListItem("Products Dispensed", "1"));
            fullReportCollection.Add(new ListItem("Products Dispensed (Export)", "20"));
            fullReportCollection.Add(new ListItem("Product Dispensed by Physician", "5"));
            fullReportCollection.Add(new ListItem("Product Dispensed by Physician (Export)", "24"));
            fullReportCollection.Add(new ListItem("Product Dispensed by Practice", "7"));
            fullReportCollection.Add(new ListItem("Product Dispensed by Practice (Export)", "26"));
            fullReportCollection.Add(new ListItem("Product Dispensed by HCPCs", "10"));
            fullReportCollection.Add(new ListItem("Product Dispensed by HCPCs (Export)", "29"));
            fullReportCollection.Add(new ListItem("Product Dispensed by Physician/HCPCs/Location", "13"));
            fullReportCollection.Add(new ListItem("Product Dispensed by Physician/HCPCs/Location (Export)", "32"));
            fullReportCollection.Add(new ListItem("Product Dispensed by Payer", "15"));
            fullReportCollection.Add(new ListItem("Product Dispensed by Payer (Export)", "34"));
            fullReportCollection.Add(new ListItem("Products Dispensed - Custom Fit", "18"));
            fullReportCollection.Add(new ListItem("Products Dispensed - Custom Fit (Export)", "37"));
            fullReportCollection.Add(new ListItem("Products Dispensed with Payor Allowable", "19"));
            fullReportCollection.Add(new ListItem("Products Dispensed with Payor Allowable (Export)", "38"));
            fullReportCollection.Add(new ListItem("Products Purchased", "2"));
            fullReportCollection.Add(new ListItem("Products Purchased (Export)", "21"));
            fullReportCollection.Add(new ListItem("Products Purchased by Practice", "8"));
            fullReportCollection.Add(new ListItem("Products Purchased by Practice (Export)", "27"));
            fullReportCollection.Add(new ListItem("Products On Hand", "3"));
            fullReportCollection.Add(new ListItem("Products On Hand (Export)", "22"));
            fullReportCollection.Add(new ListItem("Products On Hand With Supplier Wide", "16"));
            fullReportCollection.Add(new ListItem("Products On Hand With Supplier Wide (Export)", "35"));
            fullReportCollection.Add(new ListItem("Products On Hand by Practice Report", "11"));
            fullReportCollection.Add(new ListItem("Products On Hand by Practice Report (Export)", "30"));
            fullReportCollection.Add(new ListItem("PO Reconciliation", "4"));
            fullReportCollection.Add(new ListItem("PO Reconciliation (Export)", "23"));
            fullReportCollection.Add(new ListItem("Product Transfer by Practice", "9"));
            fullReportCollection.Add(new ListItem("Product Transfer by Practice (Export)", "28"));
            fullReportCollection.Add(new ListItem("Dead Stock", "6"));
            fullReportCollection.Add(new ListItem("Dead Stock (Export)", "25"));
            fullReportCollection.Add(new ListItem("Manual Check-In ", "12"));
            fullReportCollection.Add(new ListItem("Manual Check-In (Export)", "31"));
            fullReportCollection.Add(new ListItem("Product Shrinkage", "14"));
            fullReportCollection.Add(new ListItem("Product Shrinkage (Export)", "33"));
            fullReportCollection.Add(new ListItem("Product Catalog By Practice Report", "17"));
            fullReportCollection.Add(new ListItem("Product Catalog By Practice Report (Export)", "36"));
            return fullReportCollection;
        }

        public void SetReportList()
        {
            ddlReport.DataSource = GetReportListAll();
            ddlReport.DataBind();
        }

        private void RemoveSparkReports()
        {
            bool isSpark = DAL.Practice.Practice.IsSpark(practiceID);

            if (isSpark == false)
            {
                RemoveReport("Product Shrinkage");
            }

            //TODO: Unfinished consignment concept
//            bool isConsignment = DAL.Practice.Practice.IsConsignment(practiceLocationID);
//            if (isConsignment == false)
//            {
//                RemoveReport("Consignment Dead Stock");
//            }
        }

        private void RemoveReport(string reportName)
        {
            ListItem report = ddlReport.Items.FindByText(reportName);
            if (report != null)
            {
                ddlReport.Items.Remove(report);
            }
        }

        private void RemoveReportsForVisionExpress()
        {
            if (BLL.Website.IsVisionExpress())
            {
                for (int i = 4; i < 12; i++) //loop through the report values
                {
                    ListItem report = ddlReport.Items.FindByValue(i.ToString());
                    if (report != null)
                    {
                        ddlReport.Items.Remove(report);
                    }
                }
            }
        }
        #endregion INIT

        #region SESSION PROFILE STORAGE

        private SavedReportParametersCollection GetSessionReportParametersCollection(ServerReport sr)
        {
            var s_key = this.UniqueID + "_SavedReportParametersCollection_" + sr.ReportPath;
            var o = Session[s_key] as SavedReportParametersCollection;
            if (o == null)
            {
                o = new SavedReportParametersCollection();
                Session[s_key] = o;
            }
            return (SavedReportParametersCollection)o;
        }

        private bool UpdateSessionStoredReportParameters(ServerReport sr, SavedReportParametersCollection c)
        {
            // session stored collection does not need manual update
            // the stored pointer to collection created in GetSessionReportParametersCollection()
            // is updated on-the-fly
            return true;
        }

        #endregion SESSION PROFILE STORAGE

        //REPORT CRUD
        #region SAVED REPORT PARAMETERS

        private SavedReportParametersCollection GetReportParametersCollection(ServerReport sr)
        {
            if (sr == null)
                return null;

            return UserProfileUtility.GetProfileReportParametersCollection(sr);
            //return GetSessionReportParametersCollection(sr);
        }

        private bool UpdateStoredReportParameters(SavedReportParametersCollection c)
        {
            return UserProfileUtility.UpdateProfileStoredReportParameters(ReportViewer1.ServerReport, c);
            //return UpdateSessionStoredReportParameters(ReportViewer1.ServerReport, c);
        }

        private void ClearStoredReportParameters()
        {
            UserProfileUtility.ClearProfileStoredReportParameters(ReportViewer1.ServerReport);
        }

        private void RestoreServerReportParameters(ServerReport sr, IList<ReportParameter> additional_params)
        {
            // retrieve stored parameters
            var stored_params = GetReportParametersCollection(sr);

            // create storage for parameters list
            var parameters_to_set = new LocalReportParametersCollection();

            // populate parameters list storage with data from stored_params
            foreach (var p in stored_params)
                parameters_to_set.Add(new ReportParameter(p.Key, p.Values.ToArray(), true));

            // override stored parameters with overlapping values in additional_params
            foreach (var p in additional_params)
                parameters_to_set.Add(p);

            // set server report parameters
            sr.SetParameters(parameters_to_set);
        }

        private bool SaveServerReportParameters(ServerReport sr)
        {
            var storage = GetReportParametersCollection(sr);
            storage.Clear();

            var p = sr.GetParameters().Where(x => x.DataType != ParameterDataType.DateTime);
            if (p == null)
                return false;

            foreach (var item in p)
                storage.Add(new SavedReportParameter(item.Name, item.Values));

            UpdateStoredReportParameters(storage);

            return true;
        }

        private void AddJavaScriptAlert(Control requestor, string msg)
        {
            if (ScriptManager.GetCurrent(Page) != null)
            {
                var type = this.GetType();
                var scriptkey = "SaveParametersResponse";
                ScriptManager.RegisterStartupScript(requestor, type, scriptkey, string.Format(@"alert('{0}');", msg), true);
            }
        }

        #endregion SAVED REPORT PARAMETERS

        #region MANAGE SCHEDULED REPORTS

        public System.Data.DataTable GetScheduledReports()
        {
            return
                new ReportScheduler(userID, practiceID)
                    .ListScheduledReports()
                    .AsDataTable();
        }

        public void CreateScheduledReport()
        {
            var recurrence_type = (RecurrenceType)Enum.Parse(typeof(RecurrenceType), ScheduleTypeDropDownList.SelectedValue);
            var schedule = GetScheduleFromReucurrenceType(recurrence_type);

            // schedule requested report with server
            new ReportScheduler(userID, practiceID)
                .ScheduleReport(
                    ReportViewer1.ServerReport,
                    ScheduleReportRecipientTextBox.Text,
                    schedule);

            // hide schedule report panel
            ScheduleEmailReportPanel.Visible = false;
        }

        public void UpdateScheduledReport(string subscription_id)
        {
            var recurrence_type = (RecurrenceType)Enum.Parse(typeof(RecurrenceType), ScheduleTypeDropDownList.SelectedValue);
            var schedule = GetScheduleFromReucurrenceType(recurrence_type);

            // schedule requested report with server
            new ReportScheduler(userID, practiceID)
                .UpdateScheduledReportParameters(
                    subscription_id,
                    ReportViewer1.ServerReport,
                    ScheduleReportRecipientTextBox.Text,
                    schedule);

            // hide schedule report panel
            ScheduleEmailReportPanel.Visible = false;
        }

        private ReportSchedule GetScheduleFromReucurrenceType(RecurrenceType type)
        {
            switch (type)
            {
                case RecurrenceType.Daily:
                    return new DailyReportSchedule();
                    break;
                case RecurrenceType.Weekly:
                    var enabled_weekdays =
                        ScheduleTypeWeeklyEnabledDaysOfWeekCheckBoxList.Items
                            .Cast<ListItem>()
                            .Where(x => x.Selected)
                            .Select(x => (RecurrenceWeekdays)Enum.Parse(typeof(RecurrenceWeekdays), x.Value))
                            .ToArray();
                    return new WeeklyReportSchedule(enabled_weekdays);
                    break;
                case RecurrenceType.Monthly:
                    var enabled_months =
                        ScheduleTypeMonthlyEnabledMonthsCheckBoxList.Items
                            .Cast<ListItem>()
                            .Where(x => x.Selected)
                            .Select(x => (RecurrenceMonths)Enum.Parse(typeof(RecurrenceMonths), x.Value))
                            .ToArray();
                    return new MonthlyReportSchedule(int.Parse(ScheduleTypeMonthlyDayOfMonthToSendRadComboBox.Text), enabled_months);
                    break;
            }
            return null;
        }

        private ReportsListItem[] ReportsList = ReportsListItemGenerator.Generate(
            System.Configuration.ConfigurationManager.AppSettings["ReportServerReportPath"]
            ?? "/PracticeAdminReports/4.1"
        );

        public void DeleteScheduledReport(string subscription_id)
        {
            new ReportScheduler(userID, practiceID).RemoveScheduledReport(subscription_id);
        }

        public ReportScheduler.IScheduledReportSummary GetScheduledReportById(string subscription_id)
        {
            return new ReportScheduler(userID, practiceID).GetScheduledReportBySubscriptionId(subscription_id);
        }

        protected string GetSelectedReportPath(string value)
        {
            return ReportsList.First(x => x.ID == value).Path;
        }
        #endregion MANAGE SCHEDULED REPORTS

        #region SCHEDULE ENUMS

        public IEnumerable<object> GetScheduleTypes()
        {
            return Enum.GetValues(typeof(RecurrenceType)).Cast<RecurrenceType>() //list containing enum values
                .Select(x => new { Name = x.ToString(), Value = (int)x }).ToList(); //return Name/Value pairs
        }

        public IEnumerable<object> GetScheduleTypeWeeklyEnabledDaysOfWeekDataSource()
        {
            return Enum.GetValues(typeof(RecurrenceWeekdays)).Cast<RecurrenceWeekdays>() //list containing enum values
                .Select(x => new { Name = x.ToString(), Value = (int)x }).ToList(); //return Name/Value pairs
        }

        public IEnumerable<object> GetScheduleTypeMonthlyEnabledMonthsDataSource()
        {
            return Enum.GetValues(typeof(RecurrenceMonths)).Cast<RecurrenceMonths>() //list containing enum values
                .Select(x => new { Name = x.ToString(), Value = (int)x }).ToList(); //return Name/Value pairs
        }

        public IEnumerable<int> GetScheduleTypeMonthlyDayOfMonthToSendDataSource()
        {
            for (var i = 1; i <= 31; i++)
                yield return i;
        }
        #endregion SCHEDULE ENUMS
        //END: REPORT CRUD

        //PAGE EVENTS
        #region PAGE_EVENTS_SAVE_PARAMETERS
        protected void SaveParametersButton_Click(object sender, EventArgs e)
        {
            if (SaveServerReportParameters(ReportViewer1.ServerReport))
            {
                AddJavaScriptAlert(
                    (Button)sender,
                    string.Format(
                        @"Report parameters saved for ""{0}"" report.",
                        ddlReport.SelectedItem.Text));
            }

            // should probably display an error message if we didn't save properly...
        }

        protected void ClearSavedParametersButton_Click(object sender, EventArgs e)
        {
            ClearStoredReportParameters();
            AddJavaScriptAlert(
                (Button)sender,
                string.Format(
                    @"Report parameters cleared for ""{0}"" report.",
                    ddlReport.SelectedItem.Text));
        }
        #endregion PAGE_EVENTS_SAVE_PARAMETERS

        #region PAGE_EVENTS_SELECT_REPORT
        protected void ddlReport_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            {
                // configure report viewer
                ReportViewer1.ConfigureReportViewer(GetSelectedReportPath(ddlReport.SelectedValue));

                // restore saved report parameters, making sure practice ID is always set
                RestoreServerReportParameters(
                    ReportViewer1.ServerReport,
                    new[] { new ReportParameter("PracticeID", practiceID.ToString(), false) });

                // process and render the report
                ToggleActionPanelDisplay();
                if (ReportViewer1.ShowReportBody)
                    ReportViewer1.ServerReport.Refresh();
                DatePickers.Value = string.Join(",", (new List<string>(GetDateParameters()).ToArray()));
            }

            if (((DropDownList)sender).SelectedValue != "0")
            {
                ReportActionsPanel.Visible = true;
                ScheduleReportNameLabel.Text = ((DropDownList)sender).SelectedItem.Text;
            }
            else
            {
                ReportActionsPanel.Visible = false;
                ScheduleReportNameLabel.Text = "";
            }
        }

        protected void ScheduledReportsDeleteButton_Command(object sender, CommandEventArgs e)
        {
            DeleteScheduledReport(e.CommandArgument.ToString());
            // rebind grid to display post-delete list
            ScheduledReportsGridView.DataBind();
        }

        protected void ScheduledReportsEditButton_Command(object sender, CommandEventArgs e)
        {
            SubscriptionId.Value = e.CommandArgument.ToString();
            var reports = new ReportScheduler(userID, practiceID).ListScheduledReports();
            var reportSchedule = reports.FirstOrDefault(x => x.SubscriptionId == e.CommandArgument.ToString());
            ScheduleEmailReportPanel.Visible = true;
            if (reportSchedule != null)
            {

                ScheduleStartDateRadDatePicker.SelectedDate = reportSchedule.DateScheduled;
                ScheduleReportRecipientTextBox.Text = reportSchedule.EmailRecipient;

                if (ScheduleTypeDropDownList.Items.Count < 1)
                    ScheduleTypeDropDownList.DataBind();

                var selectedScheduleType = ScheduleTypeDropDownList.Items.FindByText(reportSchedule.ScheduleRecurrenceType);
                if (selectedScheduleType != null)
                {
                    ScheduleTypeDropDownList.SelectedIndex = ScheduleTypeDropDownList.Items.IndexOf(selectedScheduleType);
                }
                ScheduleTypeDropDownList_SelectedIndexChanged(ScheduleTypeDropDownList, EventArgs.Empty);
            }
        }

        #endregion PAGE_EVENTS_SELECT_REPORT

        #region PAGE_EVENTS_MANAGED_SCHEDULED_REPORTS
        protected void ManageScheduledReportsButton_Click(object sender, EventArgs e)
        {
            // make dialog visible
            ManageScheduledReportsPanel.Visible = true;
            // refresh scheduled reports list
            ScheduledReportsGridView.DataBind();
        }

        protected void ManageScheduledReportsCloseButton_Click(object sender, EventArgs e)
        {
            // hide dialog
            ManageScheduledReportsPanel.Visible = false;
        }
        #endregion PAGE_EVENTS_MANAGED_SCHEDULED_REPORTS

        #region PAGE_EVENTS_SCHEDULE_REPORT
        protected void ScheduleReportScheduleButton_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                return;

            //TODO: Find current subscript_id
            var subscription_id = SubscriptionId.Value;

            var isNew = String.IsNullOrEmpty(subscription_id);

            if (isNew)
            {
                //Schedule Report
                CreateScheduledReport();
            }
            else
            {
                UpdateScheduledReport(subscription_id);
            }


            // notify user
            AddJavaScriptAlert(this, "Report successfully scheduled.");
        }

        protected void ScheduleReportButton_Click(object sender, EventArgs e)
        {
            SubscriptionId.Value = null;
            // show dialog
            ScheduleEmailReportPanel.Visible = true;

            // reset schedule start date
            ScheduleStartDateRadDatePicker.SelectedDate = null;

            // reset schedule type
            if (ScheduleTypeDropDownList.Items.Count < 1)
                ScheduleTypeDropDownList.DataBind();
            ScheduleTypeDropDownList.SelectedIndex = 0;
            ScheduleTypeDropDownList_SelectedIndexChanged(ScheduleTypeDropDownList, EventArgs.Empty);
        }

        protected void ScheduleTypeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            var d = sender as DropDownList;
            if (d == null)
                return;

            var selected_schedule_type =
                (RecurrenceType)
                Enum.Parse(typeof(RecurrenceType), d.SelectedValue);

            ScheduleTypeWeeklyParametersPanel.Visible = (selected_schedule_type == RecurrenceType.Weekly);
            ScheduleTypeMonthlyParametersPanel.Visible = (selected_schedule_type == RecurrenceType.Monthly);

            if (ScheduleTypeWeeklyParametersPanel.Visible)
            {
                // reset enabled days of week
                if (ScheduleTypeWeeklyEnabledDaysOfWeekCheckBoxList.Items.Count < 1)
                    ScheduleTypeWeeklyEnabledDaysOfWeekCheckBoxList.DataBind();
                foreach (var item in ScheduleTypeWeeklyEnabledDaysOfWeekCheckBoxList.Items.Cast<ListItem>())
                    item.Selected = true;
            }

            if (ScheduleTypeMonthlyParametersPanel.Visible)
            {
                // reset enabled months
                if (ScheduleTypeMonthlyEnabledMonthsCheckBoxList.Items.Count < 1)
                    ScheduleTypeMonthlyEnabledMonthsCheckBoxList.DataBind();
                foreach (var item in ScheduleTypeMonthlyEnabledMonthsCheckBoxList.Items.Cast<ListItem>())
                    item.Selected = true;

                // reset day of month to send
                if (ScheduleTypeMonthlyDayOfMonthToSendRadComboBox.Items.Count < 1)
                    ScheduleTypeMonthlyDayOfMonthToSendRadComboBox.DataBind();
                ScheduleTypeMonthlyDayOfMonthToSendRadComboBox.SelectedIndex = 0;
            }
        }

        protected void ScheduleReportCloseCancelButton_Click(object sender, EventArgs e)
        {
            // hide dialog
            ScheduleEmailReportPanel.Visible = false;
        }

        #endregion PAGE_EVENTS_SCHEDULE_REPORT
        //END: PAGE EVENTS

        protected void ddlReportVersion_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            var collection = GetReportListAll();
            var newCollection = new ListItemCollection();

            // filter report list based on export/view/all selection
            if (ddlReportVersion.SelectedValue == "export")
            {
                foreach (ListItem li in collection)
                {
                    if (li.Text.ToLower().Contains("export") || li.Value == "0")
                        newCollection.Add(new ListItem(li.Text, li.Value));
                }
            }
            else if (ddlReportVersion.SelectedValue == "view")
            {
                foreach (ListItem li in collection)
                {
                    if (!li.Text.ToLower().Contains("export") || li.Value == "0")
                        newCollection.Add(new ListItem(li.Text, li.Value));
                }
            }
            else
            {
                newCollection = collection;
            }

            // store last selected item so that we restore it after a rebind
            var lastSelectedReportValue = ddlReport.SelectedValue;

            // rebind with new collection
            ddlReport.DataSource = newCollection;
            ddlReport.DataBind();

            // try to find and select last report or select the first
            var itemToSelect = ddlReport.Items.Cast<ListItem>().Where(x => x.Value == lastSelectedReportValue).FirstOrDefault();
            if (itemToSelect != null)
                ddlReport.SelectedIndex = ddlReport.Items.IndexOf(itemToSelect);
            else
                ddlReport.SelectedIndex = 0;

            ToggleActionPanelDisplay();
        }

        protected void ReportViewer1_ReportRefresh(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // always show body if refresh button pressed
            ReportViewer1.ShowReportBody = true;
        }


        private void ToggleActionPanelDisplay()
        {
            // show the report viewer as long as a report has been selected (view or export version)
            ReportViewer1.Visible = (ddlReport.SelectedItem != null && ddlReport.SelectedItem.Text != "Select a Report");

            // show the report body automatically if the report is not an export report
            ReportViewer1.ShowReportBody = (ddlReport.SelectedItem.Text.ToLower().Contains("(export)") == false);

            // show the actions panel to allow report parameter management if the report viewer itself is visible
            ReportActionsPanel.Visible = ReportViewer1.Visible;
        }

        private IEnumerable<string> GetDateParameters()
        {
            //VBCS 2083 SSRS calendar icon in chrome not loading changes
            //concatenating the date parameters to use it in the java script to show the date icon at runtime
            foreach (ReportParameterInfo info in ReportViewer1.ServerReport.GetParameters())
            {
                if (info.DataType == ParameterDataType.DateTime)
                {
                    yield return string.Format("[{0}]", info.Prompt);
                }
            }
        }
    }
}
