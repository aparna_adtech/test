﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.Admin.PracticeSetup;

namespace BregVision.Admin.UserControls
{
    public partial class PracticeBillingAddress : PracticeSetupBase, IVisionWizardControl
    {
        // Set initial values.

        int addressID = -1;                 //use session or get from practice.
        string attentionOf = "";
        string addressLine1 = "";
        string addressLine2 = "";
        string city = "";
        string state = "";                  //maybe should be state // review best method for this later.
        string zipCode = "";
        string zipCodePlus4 = "";
        bool isBillingCentralized = false;
        bool isInEdit = false;

        // Constants used in the Radio Button List
        protected const string CENTRALIZED = "Centralized";
        protected const string DECENTRALIZED = "Decentralized";

        /// <summary>
        /// IVisionWizardControl Interface.
        /// </summary>
        public bool NeedsInitialBind { get; set; }

        new protected void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);
            ucResetSaveButtonTable.Reset += new EventHandler(btnReset_Click);
            ucResetSaveButtonTable.Save += new EventHandler(btnSave_Click);
            lblContentHeader.Text = practiceName + " Bill To Address";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            
            ucPracticeBillingAddress.State = state;

            if (isInEdit == false)
            {
                GetPracticeBillToInfo(practiceID);
            }

            bool isBillingCentralized = ((rbl1.SelectedValue == rbl1.Items.FindByValue(CENTRALIZED).ToString()) ? true : false);
            SetBillingAddressPanelControl(isBillingCentralized);

        }

        private void InitializeAjaxSettings()
        {
            Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);
            if (radAjaxManager != null)
            {
                radAjaxManager.AjaxSettings.AddAjaxSetting(rbl1, rbl1);
                radAjaxManager.AjaxSettings.AddAjaxSetting(rbl1, pnlBillingAddress);
                radAjaxManager.AjaxSettings.AddAjaxSetting(rbl1, ucResetSaveButtonTable);
            }
        }


        protected void GetPracticeBillToInfo(int practiceID)
        {
            DAL.Practice.BillingAddress dalPBA = new DAL.Practice.BillingAddress();
            dalPBA.GetPracticeBillingAddressID(practiceID, out addressID, out isBillingCentralized);

            if (isBillingCentralized)
            {
                SetRadioButtonListValues(isBillingCentralized);
            }
            else
            {
                SetRadioButtonListValues(isBillingCentralized);
            }

            if (addressID > -1)
            {
                dalPBA.SelectOneByPractice(practiceID, out attentionOf, out addressID, out addressLine1, out addressLine2, out city, out state, out zipCode, out zipCodePlus4);
                ViewState.Add("Practice_BillToAddressID", addressID);
                
                SetTextBoxValues();
            }
        }

        protected void ResetAddressProperties()
        {
            attentionOf = "";
            addressLine1 = "";
            addressLine2 = "";
            city = "";
            state = "";  //new
            state = "";
            zipCode = "";
            zipCodePlus4 = "";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveData();
        }

        private void SaveData()
        {
            DAL.Practice.BillingAddress dalPBA = new DAL.Practice.BillingAddress(); //DAL.Practice.BillingAddress();

            bool isContinue = true;

            GetRadioButtonListValues();

            dalPBA.UpdatePracticeIsBillingCentralized(practiceID, isBillingCentralized, userID, addressID);

            //Set the User Interface.
            if (isBillingCentralized == true)
            {
                Page.Validate();

                if (Page.IsValid == true)
                {
                    GetTextBoxValues();
                    isContinue = true;
                }
                else
                {
                    isContinue = false;
                }
            }
            else
            {

                ResetAddressProperties();
                SetTextBoxValuesToEmpty();
                //  Set all Practice Location Billing Addresses to IsActice for the Practice!!!
            }

            //set the Database.
            // IMPORTANT  If Practice Is Decentralized one proc should be called to update the Practice, PrBillingAddress, and any PLBillingAddresses;
            // IMPORTANT  If Practice Is Centralized and exists one proc should be called to update the Practice, PrBillingAddress, and any PLBillingAddresses;
            // IMPORTANT  If Practice Is Centralized and is new one proc should be called to update the Practice, PrBillingAddress, and any PLBillingAddresses;

            if (isContinue)
            {
                addressID = (ViewState["Practice_BillToAddressID"] == null) ? -1 : Convert.ToInt32(ViewState["Practice_BillToAddressID"].ToString());

                if (addressID > 0)
                {
                    if (isBillingCentralized == true)
                    {
                        dalPBA.UpdatePracticeBillingAddress(practiceID, addressID, attentionOf, addressLine1, addressLine2, city, state, zipCode, zipCodePlus4, userID);
                        //  The above should be change to include both and set all practice locations.
                        //dalPBA.UpdatePracticeLocationBillingAddresses( practiceID, isBillingCentralized, addressID, attentionOf, addressLine1, addressLine2, city, state, zipCode, zipCodePlus4, userID );
                        SetTextBoxValues();
                    }
                    else
                    {
                        // jb moved above
                        // reset address properties to defaults - empty strings.
                        ResetAddressProperties();  //Should move above

                        // if address exists update it to default values
                        //dalPBA.UpdatePracticeBillingAddress( practiceID, addressID, attentionOf, addressLine1, addressLine2, city, state, zipCode, zipCodePlus4, userID );
                        //  The above should be change to include both and set all practice locations.
                        SetRadioButtonListValues(isBillingCentralized);
                        SetTextBoxValues();
                    }
                }
                else
                {
                    // This should have a transaction around this.
                    if (isBillingCentralized == true)
                    {
                        if (!AddressExistsForPracticeLocation())
                            dalPBA.Insert(practiceID, isBillingCentralized, out addressID, attentionOf, addressLine1, addressLine2, city, state, zipCode, zipCodePlus4, userID);
                        else
                            dalPBA.UpdatePracticeBillingAddress(practiceID, addressID, attentionOf, addressLine1, addressLine2, city, state, zipCode, zipCodePlus4, userID);

                        //  Set all Practice Location Billing Addresses to IsActice for the Practice!!!  This should be in the same proc nested.  Use TOP to reuse the PracticeLocation_BillingAddressID.
                        ViewState["Practice_BillToAddress"] = addressID.ToString();
                    }
                    else  //an address has not yet been set.
                    {
                        //  Set all Practice Location Billing Addresses to IsActice for the Practice!!!  This should be in the same proc nested.

                        ResetAddressProperties();
                    }
                }
            }
        }

        private  bool  AddressExistsForPracticeLocation()
        {
            DAL.Practice.BillingAddress dalPBA = new DAL.Practice.BillingAddress();
           return dalPBA.AddressExistsForPracticeLocation(practiceLocationID);
        }
       
        protected void SetRadioButtonListValues(bool isBillingCentralized)
        {
            if (isBillingCentralized)
            {
                pnlBillingAddress.Enabled = true;
                rbl1.SelectedValue = rbl1.Items.FindByValue(CENTRALIZED).ToString();
            }
            else
            {
                rbl1.SelectedValue = rbl1.Items.FindByValue(DECENTRALIZED).ToString();
                SetBillingAddressPanelControl(isBillingCentralized);
                pnlBillingAddress.Enabled = false;
            }
            //SetBillingAddressPanelControl( isBillingCentralized );
        }

        protected void GetRadioButtonListValues()
        {
            if (rbl1.SelectedItem.ToString() == CENTRALIZED)
            {
                isBillingCentralized = true;
            }
            else
            {
                isBillingCentralized = false;
            }
        }

        protected void GetTextBoxValues()
        {
            GetRadioButtonListValues();

            //UserControl ucPracticeBillingAddress9 = ( UserControl )FindControl("ucPracticeBillingAddress");

            TextBox txtAttentionOf = (TextBox)ucPracticeBillingAddress.FindControl("txtAttentionOf");
            TextBox txtAddressLine1 = (TextBox)ucPracticeBillingAddress.FindControl("txtAddressLine1");
            TextBox txtAddressLine2 = (TextBox)ucPracticeBillingAddress.FindControl("txtAddressLine2");
            TextBox txtCity = (TextBox)ucPracticeBillingAddress.FindControl("txtCity");

            DropDownList cmbState = (DropDownList)ucPracticeBillingAddress.FindControl("cmbState");

            TextBox txtZipCode = (TextBox)ucPracticeBillingAddress.FindControl("txtZipCode");
            TextBox txtZipCodePlus4 = (TextBox)ucPracticeBillingAddress.FindControl("txtZipCodePlus4");

            attentionOf = Convert.ToString(txtAttentionOf.Text);
            attentionOf = Convert.ToString(txtAttentionOf.Text);
            addressLine1 = Convert.ToString(txtAddressLine1.Text);
            addressLine2 = Convert.ToString(txtAddressLine2.Text);
            city = Convert.ToString(txtCity.Text);
            state = Convert.ToString(cmbState.SelectedValue);  //maybe should be state // review best method for this later.
            zipCode = Convert.ToString(txtZipCode.Text);
            zipCodePlus4 = Convert.ToString(txtZipCodePlus4.Text);
        }

        protected void SetTextBoxValues()
        {
            SetRadioButtonListValues(isBillingCentralized);

            //UserControl ucPracticeBillingAddress9 = ( UserControl ) FindControl( "ucPracticeBillingAddress" );

            TextBox txtAttentionOf = (TextBox)ucPracticeBillingAddress.FindControl("txtAttentionOf");
            TextBox txtAddressLine1 = (TextBox)ucPracticeBillingAddress.FindControl("txtAddressLine1");
            TextBox txtAddressLine2 = (TextBox)ucPracticeBillingAddress.FindControl("txtAddressLine2");
            TextBox txtCity = (TextBox)ucPracticeBillingAddress.FindControl("txtCity");

            DropDownList cmbState = (DropDownList)ucPracticeBillingAddress.FindControl("cmbState");

            TextBox txtZipCode = (TextBox)ucPracticeBillingAddress.FindControl("txtZipCode");
            TextBox txtZipCodePlus4 = (TextBox)ucPracticeBillingAddress.FindControl("txtZipCodePlus4");


            if (isBillingCentralized == true)
            {
                txtAttentionOf.Text = attentionOf;
                txtAddressLine1.Text = addressLine1;
                txtAddressLine2.Text = addressLine2;
                txtCity.Text = city;
                cmbState.SelectedValue = state.ToString();
                // Fix this above with below!!!
                ucPracticeBillingAddress.State = state.ToString();

                txtZipCode.Text = zipCode;
                txtZipCodePlus4.Text = zipCodePlus4;
            }
        }

        protected void SetTextBoxValuesToEmpty()
        {
            SetRadioButtonListValues(isBillingCentralized);

            if (isBillingCentralized == false)
            {
                //UserControl ucPracticeBillingAddress = ( UserControl ) FindControl( "ucPracticeBillingAddress" );

                TextBox txtAttentionOf = (TextBox)ucPracticeBillingAddress.FindControl("txtAttentionOf");
                TextBox txtAddressLine1 = (TextBox)ucPracticeBillingAddress.FindControl("txtAddressLine1");
                TextBox txtAddressLine2 = (TextBox)ucPracticeBillingAddress.FindControl("txtAddressLine2");
                TextBox txtCity = (TextBox)ucPracticeBillingAddress.FindControl("txtCity");

                DropDownList cmbState = (DropDownList)ucPracticeBillingAddress.FindControl("cmbState");

                TextBox txtZipCode = (TextBox)ucPracticeBillingAddress.FindControl("txtZipCode");
                TextBox txtZipCodePlus4 = (TextBox)ucPracticeBillingAddress.FindControl("txtZipCodePlus4");

                txtAttentionOf.Text = attentionOf;
                txtAddressLine1.Text = addressLine1;
                txtAddressLine2.Text = addressLine2;
                txtCity.Text = city;
                //cmbState.SelectedValue = state.ToString();
                cmbState.SelectedIndex = 0;
                txtZipCode.Text = zipCode;
                txtZipCodePlus4.Text = zipCodePlus4;
            }
        }

        protected void rbl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            isInEdit = true;
        }

        protected void SetBillingAddressPanelControl(bool isCentralizedBilling)
        {
            pnlBillingAddress.Enabled = isCentralizedBilling;
            ucResetSaveButtonTable.btnSaveButton.CausesValidation = isCentralizedBilling;
           // Page.ClientTarget = isCentralizedBilling == true ? "UpLevel" : "DownLevel";
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Practice_All.aspx");
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            GetPracticeBillToInfo(practiceID);
            if (isBillingCentralized == false)
            {
                ResetAddressProperties();
                SetTextBoxValuesToEmpty();
            }
        }

        protected eSetupStatus ValidateSetupData()
        {
            bool isBillingCentralized = ((rbl1.SelectedValue == rbl1.Items.FindByValue(CENTRALIZED).ToString()) ? true : false);
            Page.Validate();
            if (Page.IsValid==true || isBillingCentralized==false)
            {
                return eSetupStatus.Complete;
            }
            return eSetupStatus.Incomplete;
        }

        /// <summary>
        /// IVisionWizardControl interface save.  Called by wizard control on tab change
        /// </summary>
        public void Save()
        {
            SaveData();
            
            SaveStatus(practiceID, -1, this.ToString(), ValidateSetupData());
        }
    }
}