﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.Admin.PracticeSetup;

namespace BregVision.Admin.UserControls
{
    public partial class PracticeCreditApp : PracticeSetupBase, IVisionWizardControl
    {

        /// <summary>
        /// IVisionWizardControl Interface.
        /// </summary>
        public bool NeedsInitialBind { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected eSetupStatus ValidateSetupData()
        {
            return eSetupStatus.Complete;
        }

        /// <summary>
        /// IVisionWizardControl interface save.  Called by wizard control on tab change
        /// </summary>
        public void Save()
        {
            //do nothing.  Grid rows are saved individually

            SaveStatus(practiceID, -1, this.ToString(), ValidateSetupData());
        }
    }
}