﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeCreditApp.ascx.cs" Inherits="BregVision.Admin.UserControls.PracticeCreditApp" %>

    <style type="text/css"> 


.style1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	text-align: left;
}
.style2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight:bold;
	text-align: left;
}
.style3 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight:bold;
	text-align: left;
}
</style>
<table border="0" width="100%" style=" vertical-align:top; height: 500px">
    <tr align="left" valign="top">
        <td valign="top" align="left">
            <p class="style2"><font color="black">If you wish to purchase BREG products, you will need to have an account number.  If you do not have an account number, please click the 
            link below to download the credit application.  Once complete, please fax the credit application into the fax # on the form.  
            BREG will process your application and issue you and account number to begin ordering BREG products.</font></p>
            <p class="style2"><font color="black">You do not need to fax the credit application in to use Vision Express, however if you wish to purchase BREG products through the site you will need an account #.</font></p>
            <br />
            <p class="style2"><a href="../UserControls/Practice/BREGCreditApp.pdf">Right Click and choose "Save Target As..." to download the Credit App</a></p>
            
        </td>
    </tr>
</table>