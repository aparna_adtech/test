﻿using System;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.Admin.PracticeSetup;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.Web.UI;

namespace BregVision.Admin.UserControls
{
    public partial class PracticeContact : PracticeSetupBase, IVisionWizardControl
    {

        int contactID;
        string title = "";
        string salutation = "";
        string firstName = "";
        string middleName = "";
        string lastName = "";
        string suffix = "";
        string emailAddress = "";
        string phoneWork = "";
        string phoneWorkExtension = "";
        string phoneCell = "";
        string phoneHome = "";
        string fax = "";
        bool isSpecificContact;


        /// <summary>
        /// IVisionWizardControl Interface.
        /// </summary>
        public bool NeedsInitialBind { get; set; }

        new protected void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);
            ucButtons.Reset += new EventHandler(btnReset_Click);
            ucButtons.Save += new EventHandler(btnSave_Click);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {

            Session["Practice_ContactID"] = "-1";

            lblContentHeader.Text = practiceName + " Contact";

            //Load the contact info if it exists
            GetContactID(practiceID, out contactID);  // This is not neccessary on a post back where the value is contactID is set.

            if (contactID > -1)
            {
                Session["Practice_ContactID"] = contactID.ToString();
                GetContact(contactID, out title, out salutation, out firstName, out middleName, out lastName,
                            out suffix, out emailAddress, out phoneWork, out phoneWorkExtension, out phoneCell, out phoneHome, out fax, out isSpecificContact);
                SetTextBoxesValues();
            }
            
        }


        void GetTextBoxValues()
        {
            // Get Controls from inside the User Control
            TextBox txtTitle = (TextBox)ucPracticeContact.FindControl("txtTitle");
            DropDownList ddlSalutation = (DropDownList)ucPracticeContact.FindControl("ddlSalutation");
            TextBox txtFirstName = (TextBox)ucPracticeContact.FindControl("txtFirstName");
            TextBox txtMiddleName = (TextBox)ucPracticeContact.FindControl("txtMiddleName");
            TextBox txtLastName = (TextBox)ucPracticeContact.FindControl("txtLastName");
            DropDownList ddlSuffix = (DropDownList)ucPracticeContact.FindControl("ddlSuffix");
            TextBox txtEmailAddress = (TextBox)ucPracticeContact.FindControl("txtEmailAddress");
            RadMaskedTextBox txtPhoneWork = (RadMaskedTextBox)ucPracticeContact.FindControl("txtPhoneWork");
            RadMaskedTextBox txtPhoneWorkExtension = (RadMaskedTextBox)ucPracticeContact.FindControl("txtPhoneWorkExtension");
            RadMaskedTextBox txtPhoneCell = (RadMaskedTextBox)ucPracticeContact.FindControl("txtPhoneCell");
            RadMaskedTextBox txtPhoneHome = (RadMaskedTextBox)ucPracticeContact.FindControl("txtPhoneHome");
            RadMaskedTextBox txtFax = (RadMaskedTextBox)ucPracticeContact.FindControl("txtFax");
            RadButton chkIsSpecificContact = (RadButton) ucPracticeContact.FindControl("chkIsSpecificContact");

            title = txtTitle.Text;
            salutation = ddlSalutation.SelectedItem.Text;
            firstName = txtFirstName.Text;
            middleName = txtMiddleName.Text;
            lastName = txtLastName.Text;
            //suffix = txtSuffix.Text;
            suffix = ddlSuffix.SelectedItem.Text;
            emailAddress = txtEmailAddress.Text;

            phoneWork = txtPhoneWork.Text;
            phoneWorkExtension = txtPhoneWorkExtension.Text;
            phoneCell = txtPhoneCell.Text;
            phoneHome = txtPhoneHome.Text;
            fax = txtFax.Text;
            isSpecificContact = chkIsSpecificContact.Checked;

        }

        void SetTextBoxesValues()
        {
            TextBox txtTitle = (TextBox)ucPracticeContact.FindControl("txtTitle");
            DropDownList ddlSalutation = (DropDownList)ucPracticeContact.FindControl("ddlSalutation");
            TextBox txtFirstName = (TextBox)ucPracticeContact.FindControl("txtFirstName");
            TextBox txtMiddleName = (TextBox)ucPracticeContact.FindControl("txtMiddleName");
            TextBox txtLastName = (TextBox)ucPracticeContact.FindControl("txtLastName");
            DropDownList ddlSuffix = (DropDownList)ucPracticeContact.FindControl("ddlSuffix");
            TextBox txtEmailAddress = (TextBox)ucPracticeContact.FindControl("txtEmailAddress");

            RadMaskedTextBox txtPhoneWork = (RadMaskedTextBox)ucPracticeContact.FindControl("txtPhoneWork");
            RadMaskedTextBox txtPhoneWorkExtension = (RadMaskedTextBox)ucPracticeContact.FindControl("txtPhoneWorkExtension");
            RadMaskedTextBox txtPhoneCell = (RadMaskedTextBox)ucPracticeContact.FindControl("txtPhoneCell");
            RadMaskedTextBox txtPhoneHome = (RadMaskedTextBox)ucPracticeContact.FindControl("txtPhoneHome");
            RadMaskedTextBox txtFax = (RadMaskedTextBox)ucPracticeContact.FindControl("txtFax");
            RadButton chkIsSpecificContact = (RadButton)ucPracticeContact.FindControl("chkIsSpecificContact");

            //ucPracticeContact.FindControl( "txtTitle" ).Text = title;
            txtTitle.Text = title;
            ddlSalutation.ClearSelection();
            ddlSalutation.Items.FindByText(salutation).Selected = true;
            txtFirstName.Text = firstName;
            txtMiddleName.Text = middleName;
            txtLastName.Text = lastName;
            ddlSuffix.ClearSelection();
            ddlSuffix.Items.FindByText(suffix).Selected = true;
            txtEmailAddress.Text = emailAddress;

            // Remove if statement after data validation controls are placed.
            txtPhoneWork.Text = phoneWork;
            txtPhoneWorkExtension.Text = phoneWorkExtension;
            txtPhoneCell.Text = phoneCell;
            txtPhoneHome.Text = phoneHome;
            txtFax.Text = fax;
            chkIsSpecificContact.Checked = isSpecificContact;

        }

        public void GetContact(int contactID, out string title, out string salutation, out string firstName, out string middleName, out string lastName, out string suffix, out string emailAddress, out string phoneWork, out string phoneWorkExtension, out string phoneCell, out string phoneHome, out string fax, out bool isSpecificContact)
        {
            BLL.Practice.Contact c = new BLL.Practice.Contact();
            c.LoadFromID(contactID);

            title = c.Title;
            salutation = c.Salutation;
            firstName = c.FirstName;
            middleName = c.MiddleName;
            lastName = c.LastName;
            suffix = c.Suffix;
            emailAddress = c.EmailAddress;
            phoneWork = c.PhoneWork;
            phoneWorkExtension = c.PhoneWorkExtension;
            phoneCell = c.PhoneCell;
            phoneHome = c.PhoneHome;
            fax = c.Fax;
            isSpecificContact = c.IsSpecificContact;
        }


        public void AddContact(out int newContactID, string title, string salutation, string firstName, string middleName, string lastName, string suffix, string phoneWork, string phoneWorkExtension, string phoneCell, string phoneHome, string fax, int createdUserID, bool specificContact)
        {
            BLL.Practice.Contact c = new BLL.Practice.Contact();
            newContactID = c.Create(title, salutation, firstName, middleName, lastName, suffix, emailAddress, phoneWork, phoneWorkExtension, phoneCell, phoneHome, fax, userID, isSpecificContact);
        }


        public void UpdateContact(int contactID, string title, string salutation, string firstName, string middleName, string lastName, string suffix, string emailAddress, string phoneWork, string phoneWorkExtension, string phoneCell, string phoneHome, string fax, int userID, bool isSpecificContact)
        {
            BLL.Practice.Contact c = new BLL.Practice.Contact(contactID);

            c.ContactID = contactID;
            c.Title = title;
            c.Salutation = salutation;
            c.FirstName = firstName;
            c.MiddleName = middleName;
            c.LastName = lastName;
            c.Suffix = suffix;
            c.EmailAddress = emailAddress;
            c.PhoneWork = phoneWork;
            c.PhoneWorkExtension = phoneWorkExtension;
            c.PhoneCell = phoneCell;
            c.PhoneHome = phoneHome;
            c.Fax = fax;
            c.IsSpecificContact = isSpecificContact;
            c.UserID = userID;

            bool isUpdated = c.Update();
        }


        // Put this into the BAL and DLL.
        public void UpdatePracticeMainContactID(int practiceID, int contactID, int modifiedUserID)
        {
            int rowAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_Practice_Update_ContactID");

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            db.AddInParameter(dbCommand, "ContactID", DbType.Int32, contactID);
            db.AddInParameter(dbCommand, "ModifiedUserID", DbType.Int32, modifiedUserID);

            rowAffected = db.ExecuteNonQuery(dbCommand);
        }


        // add to BLL and DAL
        public void GetContactID(int practiceID, out int contactID)
        {
            int rowAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");  //use key in web config for the connection string        
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_Practice_Select_ContactID_ByParams");

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            db.AddOutParameter(dbCommand, "ContactID", DbType.Int32, 4);

            rowAffected = db.ExecuteNonQuery(dbCommand);

            if ((db.GetParameterValue(dbCommand, "ContactID") is DBNull))
            {
                contactID = -1;
            }
            else
            {
                contactID = Convert.ToInt32(db.GetParameterValue(dbCommand, "ContactID"));
            }
        }

        //Event Handler for the ucButtons Save button click event.
        void btnSave_Click(object sender, EventArgs e)
        {
            SaveData();
        }

        private void SaveData()
        {
            GetTextBoxValues();
            contactID = Convert.ToInt32(Session["Practice_ContactID"]);
            if (contactID > 0)
            {
                UpdateContact(contactID, title, salutation, firstName, middleName, lastName, suffix, emailAddress, phoneWork, phoneWorkExtension, phoneCell, phoneHome, fax, userID, isSpecificContact);
            }
            else
            {
                //GetSessionVariables(out userID, out practiceID, out practiceName);
                // This should have a transaction around this.
                AddContact(out contactID, title, salutation, firstName, middleName, lastName, suffix, phoneWork, phoneWorkExtension, phoneCell, phoneHome, fax, userID, isSpecificContact);
                UpdatePracticeMainContactID(practiceID, contactID, userID);
                Session["Practice_ContactID"] = contactID.ToString();
            }
        }


        //Event Handler for the ucButtons Reset button click event.
        void btnReset_Click(object sender, EventArgs e)
        {
            //Load the contact info if it exists
            GetContactID(practiceID, out contactID);  // This is not neccessary on a post back where the value of the contactID is set.

            if (contactID > -1)
            {
                Session["Practice_ContactID"] = contactID.ToString();
                GetContact(contactID, out title, out salutation, out firstName, out middleName, out lastName,
                            out suffix, out emailAddress, out phoneWork,out phoneWorkExtension, out phoneCell, out phoneHome, out fax, out isSpecificContact);
                SetTextBoxesValues();
            }
        }

        protected eSetupStatus ValidateSetupData()
        {
            Page.Validate();
            if (Page.IsValid)
            {
                return eSetupStatus.Complete;
            }
            return eSetupStatus.Incomplete;
        }

        /// <summary>
        /// IVisionWizardControl interface save.  Called by wizard control on tab change
        /// </summary>
        public void Save()
        {
            SaveData();

            SaveStatus(practiceID, -1, this.ToString(), ValidateSetupData());
        }
    }
}