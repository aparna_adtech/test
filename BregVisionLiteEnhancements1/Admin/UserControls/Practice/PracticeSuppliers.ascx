﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeSuppliers.ascx.cs" Inherits="BregVision.Admin.UserControls.PracticeSuppliers" %>
<div class="content">
  <h2>Suppliers</h2>
  <h3>
    <asp:Label ID="Label1" runat="server" Text="Master Catalog Suppliers"></asp:Label>&nbsp;<bvu:Help ID="help126" runat="server" HelpID="126" />
  </h3>
  <!-- Grid Master Catalog Suppliers -->
  <asp:GridView ID="GridMCSupplier" runat="server" AutoGenerateColumns="False" DataKeyNames="SupplierName, SupplierShortName" ShowFooter="True" EmptyDataText="No records found" CssClass="RadGrid_Breg">
    <Columns>
      <asp:TemplateField SortExpression="SupplierName">
        <HeaderTemplate>
          <asp:Label runat="server">Supplier Full Name</asp:Label>
        </HeaderTemplate>
        <ItemTemplate>
          <asp:Label ID="lblSupplierName" runat="server"  Text='<%# Bind("SupplierName") %>'>
          </asp:Label>
        </ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField SortExpression="Supplier Short Name">
        <HeaderTemplate>
          <asp:Label runat="server">Supplier Display Name</asp:Label>&nbsp;<bvu:Help ID="Help9" runat="server" HelpID="9" />
        </HeaderTemplate>
        <ItemTemplate>
          <asp:Label ID="lblSupplierShortName" runat="server"  Text='<%# Bind("SupplierShortName") %>'></asp:Label>
        </ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField SortExpression="OrderPlacementType">
        <HeaderTemplate>
          <asp:Label runat="server">Order Placement Type</asp:Label>&nbsp;<bvu:Help ID="help10" runat="server" HelpID="10" />
        </HeaderTemplate>
        <ItemTemplate>
          <asp:Label ID="lblOrderPlacementType" runat="server"  Text='<%# Bind("OrderPlacementType") %>'></asp:Label>
        </ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField SortExpression="OrderPlacementFaxEmail" Visible="True">
        <HeaderTemplate>
          <asp:Label runat="server">Fax / Email Order Placement</asp:Label>&nbsp;<bvu:Help ID="help11" runat="server" HelpID="11" />
        </HeaderTemplate>
        <ItemTemplate>
          <asp:Label ID="lblEmailOrderPlacement" runat="server"  Text='<%# Bind("OrderPlacementFaxEmail") %>'></asp:Label>
        </ItemTemplate>
      </asp:TemplateField>
    </Columns>
    <HeaderStyle CssClass="AdminSubTitle" />
  </asp:GridView>
  <div class="flex-row">
    <h3>
      <asp:Label ID="lblContentHeader" runat="server" Text="Third Party Suppliers"></asp:Label>
      <bvu:Help ID="help8" runat="server" HelpID="8" />
    </h3>
  </div>
  <div>
    <asp:Label ID="lblGSStatus" runat="server" CssClass="WarningMsg"></asp:Label>
  </div>
  <!-- Grid Third Party Suppliers -->
  <asp:GridView ID="GridSupplier" runat="server" AutoGenerateColumns="False" DataKeyNames="ThirdPartySupplierID, PracticeID, IsVendor, IsEmailOrderPlacement, SupplierName, SupplierShortName" OnRowCancelingEdit="GridSupplier_RowCancelingEdit" OnRowCommand="GridSupplier_RowCommand" OnRowDataBound="GridSupplier_RowDataBound" OnRowDeleting="GridSupplier_RowDeleting" OnRowEditing="GridSupplier_RowEditing" OnRowUpdating="GridSupplier_RowUpdating" ShowFooter="True" EmptyDataText="No records found" CssClass="RadGrid_Breg">
    <Columns>
      <asp:TemplateField>
        <ItemTemplate>
          <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="True" CommandName="Delete" ImageUrl="~/App_Themes/Breg/images/Common/delete.png" ToolTip="Delete Supplier" />
        </ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField>
        <ItemTemplate>
          <asp:ImageButton ID="imgPromote" runat="server" CausesValidation="false" CommandName="Promote" CommandArgument='<%# Bind("ThirdPartySupplierID") %>' ImageUrl="~\RadControls\Grid\Skins\Outlook\MoveUp.gif" ToolTip="Promote Supplier to Master Catalog" />
        </ItemTemplate>
      </asp:TemplateField>
      <asp:BoundField DataField="thirdPartySupplierID" HeaderText="ThirdPartySupplierID" SortExpression="thirdPartySupplierID" Visible="False" />
      <asp:TemplateField ShowHeader="False">
        <HeaderTemplate>
          <bvu:Help ID="Help116" runat="server" HelpID="116" />
        </HeaderTemplate>
        <EditItemTemplate>
          <asp:ImageButton ID="btnEditUpdate" runat="server" CausesValidation="True" CommandName="Update" ImageUrl="~/App_Themes/Breg/images/Common/checkCircle.png" ToolTip="Update" />&nbsp;
          <asp:ImageButton ID="btnEditCancel" runat="server" CausesValidation="false" CommandName="Cancel" ImageUrl="~/App_Themes/Breg/images/Common/cancelCircle.png" ToolTip="Cancel" />
        </EditItemTemplate>
        <ItemTemplate>
          <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="False" CommandName="Edit" ImageUrl="~/App_Themes/Breg/images/Common/edit.png" ToolTip="Edit" />
        </ItemTemplate>
        <FooterTemplate>
          <asp:ImageButton ID="ImageButtonAddNew" runat="server" CommandName="AddNew" ImageUrl="~\App_Themes\Breg\images\Common\add-record.png" ToolTip="Add New Third Party Supplier" />
          <asp:ImageButton ID="ImageButtonAddNewCancel" runat="server" CommandName="AddNewCancel" ImageUrl="~/App_Themes/Breg/images/Common/cancelCircle.png" ToolTip="Cancel" Visible="false" CausesValidation="false" />
        </FooterTemplate>
      </asp:TemplateField>
      <asp:TemplateField HeaderText="ThirdPartySupplierID" SortExpression="ThirdPartySupplierID" Visible="False">
        <ItemTemplate>
          <asp:Label ID="lblThirdPartySupplierIDEdit" runat="server" Text='<%# Bind("ThirdPartySupplierID") %>' Visible="false"></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
          <asp:Label ID="lblThirdPartySupplierID" runat="server" Text='<%# Eval("ThirdPartySupplierID") %>' Visible="false"></asp:Label>
        </EditItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField HeaderText="PracticeID" SortExpression="practiceID" Visible="False">
        <ItemTemplate>
          <asp:Label ID="lblPracticeIDEdit" runat="server"  Text='<%# Bind("practiceID") %>' Visible="false"></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
          <asp:Label ID="lblPracticeID" runat="server"  Text='<%# Eval("practiceID") %>' Visible="false"></asp:Label>
        </EditItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField HeaderText="Supplier Full Name" SortExpression="SupplierName">
        <EditItemTemplate>
          <asp:TextBox ID="txtSupplierName" runat="server"  MaxLength="100" Text='<%# Bind("SupplierName") %>' Placeholder="Supplier Full Name">
          </asp:TextBox>
          <asp:RequiredFieldValidator ID="rfvSupplierName" runat="server" ControlToValidate="txtSupplierName" ErrorMessage="Required" Text="*">
          </asp:RequiredFieldValidator>
        </EditItemTemplate>
        <ItemTemplate>
          <asp:Label ID="lblSupplierName" runat="server" Text='<%# Bind("SupplierName") %>'>
          </asp:Label>
        </ItemTemplate>
        <FooterTemplate>
          <asp:TextBox ID="txtNewSupplierName" runat="server" Visible="false">
          </asp:TextBox>
          <asp:RequiredFieldValidator ID="rfvNewSupplierName" runat="server" ControlToValidate="txtNewSupplierName" ErrorMessage="Required" Text="*" Visible="false">
          </asp:RequiredFieldValidator>
        </FooterTemplate>
      </asp:TemplateField>
      <asp:TemplateField HeaderText="Supplier Display Name" SortExpression="Supplier Short Name">
        <EditItemTemplate>
          <asp:TextBox ID="txtSupplierShortName" runat="server"  MaxLength="25" Text='<%# Bind("SupplierShortName") %>' Placeholder="Supplier Display Name" ></asp:TextBox>
          <asp:RequiredFieldValidator ID="rfvSupplierShortName" runat="server" ControlToValidate="txtSupplierShortName" ErrorMessage="Required" Text="*">
          </asp:RequiredFieldValidator>
        </EditItemTemplate>
        <ItemTemplate>
          <asp:Label ID="lblSupplierShortName" runat="server" Text='<%# Bind("SupplierShortName") %>'></asp:Label>
        </ItemTemplate>
        <FooterTemplate>
          <asp:TextBox ID="txtNewSupplierShortName" runat="server" Visible="False"></asp:TextBox>
          <asp:RequiredFieldValidator ID="rfvNewSupplierShortName" runat="server" ControlToValidate="txtNewSupplierShortName" ErrorMessage="Required" Text="*" Visible="false">
          </asp:RequiredFieldValidator>
        </FooterTemplate>
      </asp:TemplateField>
      <asp:TemplateField HeaderText="Supplier Type" SortExpression="Supplier Type">
        <ItemTemplate>
          <asp:Label ID="lblSupplierType" runat="server"  Text='<%# Bind("SupplierType") %>'></asp:Label>
          <asp:LinkButton ID="linkPracticeVendorBrands" runat="server" CommandArgument='<%# Bind("ThirdPartySupplierID") %>' CommandName="GoToPracticeVendorBrands" Text='<%# Eval("VendorLink", "({0})") %>' style='<%# string.IsNullOrWhiteSpace(Eval("VendorLink").ToString()) ? " display: none": " font-size: .9em; margin-left: 2px" %>'></asp:LinkButton>
        </ItemTemplate>
        <EditItemTemplate>
          <div class="Dropdown-Container">
              <asp:DropDownList ID="ddlSupplierType" runat="server" DataTextField="SupplierType" DataValueField="SupplierType">
                <asp:ListItem>Manufacturer</asp:ListItem>
                <asp:ListItem>Vendor</asp:ListItem>
              </asp:DropDownList>
          </div>
        </EditItemTemplate>
        <FooterTemplate>
          <div class="Dropdown-Container">
            <asp:DropDownList ID="ddlNewSupplierType" runat="server" DataTextField="SupplierType" DataValueField="SupplierType" Visible="false">
              <asp:ListItem>Manufacturer</asp:ListItem>
              <asp:ListItem>Vendor</asp:ListItem>
            </asp:DropDownList>
           </div>
        </FooterTemplate>
      </asp:TemplateField>
      <asp:TemplateField HeaderText="Order Placement Type" SortExpression="OrderPlacementType">
        <ItemTemplate>
          <asp:Label ID="lblOrderPlacementType" runat="server" Text='<%# Bind("OrderPlacementType") %>'></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
          <div class="Dropdown-Container">
            <asp:DropDownList ID="ddlOrderPlacementType" runat="server" AutoPostBack="true" CausesValidation="false"  DataTextField="OrderPlacementType" DataValueField="OrderPlacementType" OnSelectedIndexChanged="ddlOrderPlacementType_OnSelectedIndexChanged">
              <asp:ListItem>Fax</asp:ListItem>
              <asp:ListItem>Email</asp:ListItem>
            </asp:DropDownList>
          </div>
        </EditItemTemplate>
        <FooterTemplate>
          <div class="Dropdown-Container">
            <asp:DropDownList ID="ddlNewOrderPlacementType" runat="server"  AutoPostBack="true" DataTextField="OrderPlacementType" DataValueField="OrderPlacementType" Visible="false" OnSelectedIndexChanged="ddlNewOrderPlacementType_OnSelectedIndexChanged">
              <asp:ListItem>Fax</asp:ListItem>
              <asp:ListItem>Email</asp:ListItem>
            </asp:DropDownList>
          </div>
        </FooterTemplate>
      </asp:TemplateField>
      <asp:TemplateField HeaderText="Fax / Email Order Placement" SortExpression="FaxOrderPlacement" Visible="True">
        <ItemTemplate>
          <label id="lblItemFaxOrderPlacement" runat="server" class="FieldLabel">
          </label>
          <asp:Label ID="lblFaxOrderPlacement" runat="server"  Text='<%#Bind("FaxOrderPlacement", "{0:(###) ###-####}") %>'></asp:Label>
          <label id="lblItemEmailOrderPlacement" runat="server" class="FieldLabel">
          </label>
          <asp:Label ID="lblEmailOrderPlacement" runat="server"  Text='<%# Bind("EmailOrderPlacement") %>'></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
          <label id="lblEditFaxOrderPlacement" runat="server" class="FieldLabel">
            Fax:
          </label>
          <telerik:RadMaskedTextBox ID="txtFaxOrderPlacement" runat="server" Height="24px"  Mask="(###) ###-####" Rows="1" Columns="50" BorderStyle="None" BorderWidth="0" DisplayMask="(###) ###-####" SelectionOnFocus="CaretToBeginning" Skin="" TextWithLiterals="() -" Text='<%# Bind("FaxOrderPlacement") %>'></telerik:RadMaskedTextBox>
          <asp:RequiredFieldValidator ID="rfvItemFaxOrderPlacement" runat="server" ControlToValidate="txtFaxOrderPlacement" Text="*" />
          <asp:RegularExpressionValidator ID="revItemFaxOrderPlacement" runat="server" ControlToValidate="txtFaxOrderPlacement" Display="dynamic" ErrorMessage="regularexpressionvalidator" Font-Size="smaller" ToolTip="Phone Work must be 10 digits or blank" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">10 digits</asp:RegularExpressionValidator>
          <label id="lblEditEmailOrderPlacement" runat="server" class="FieldLabel">
            Email:
          </label>
          <asp:TextBox ID="txtEmailOrderPlacement" runat="server"  MaxLength="100" Text='<%# Bind("EmailOrderPlacement") %>'></asp:TextBox>
          <asp:RequiredFieldValidator ID="rfvItemEmailOrderPlacement" runat="server" ControlToValidate="txtEmailOrderPlacement" Text="*" Enabled="false" />
          <asp:RegularExpressionValidator ID="revEmailAddress" runat="server" ControlToValidate="txtEmailOrderPlacement" ValidationExpression=".*@.*\..*" ErrorMessage="* Your entry is not a valid e-mail address." Display="dynamic">
            *
          </asp:RegularExpressionValidator>
        </EditItemTemplate>
        <FooterTemplate>
          <label id="lblFooterFaxOrderPlacement" runat="server" class="FieldLabel" visible="false">
            Fax:
          </label>
          <telerik:RadMaskedTextBox ID="txtNewFaxOrderPlacement" runat="server" Height="24px"  Visible="false" Mask="(###) ###-####" Rows="1" Columns="50" BorderStyle="None" BorderWidth="0" DisplayMask="(###) ###-####" SelectionOnFocus="CaretToBeginning" Skin="" TextWithLiterals="() -"></telerik:RadMaskedTextBox>
          <asp:RequiredFieldValidator ID="rfvNewFaxOrderPlacement" runat="server" ControlToValidate="txtNewFaxOrderPlacement" Text="*" Enabled="false" />
          <asp:RegularExpressionValidator ID="revNewFaxOrderPlacement" runat="server" ControlToValidate="txtNewFaxOrderPlacement" Display="dynamic" ErrorMessage="regularexpressionvalidator" Font-Size="smaller" ToolTip="Phone Work must be 10 digits or blank" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">10 digits</asp:RegularExpressionValidator>
          <label id="lblFooterEmailOrderPlacement" runat="server" class="FieldLabel" visible="false">
            Email:
          </label>
          <asp:TextBox ID="txtNewEmailOrderPlacement" runat="server"  Visible="False"></asp:TextBox>
          <asp:RequiredFieldValidator ID="rfvNewEmailOrderPlacement" runat="server" ControlToValidate="txtNewEmailOrderPlacement" Text="*" Enabled="false" />
          <asp:RegularExpressionValidator ID="revEmailAddress" runat="server" ControlToValidate="txtNewEmailOrderPlacement" ValidationExpression=".*@.*\..*" ErrorMessage="* Your entry is not a valid e-mail address." Display="dynamic" Enabled="false">
            *
          </asp:RegularExpressionValidator>
        </FooterTemplate>
      </asp:TemplateField>
      <asp:TemplateField HeaderText="Fax Verification">
        <ItemTemplate>
          <asp:Image ID="imgVerified" runat="server" ImageUrl='<%# (Eval("IsFaxVerified") is System.DBNull || (Convert.ToBoolean(Eval("IsFaxVerified")) == false)) ? "" : "~/App_Themes/Breg/images/Common/check.png" %>' CssClass='<%# (Eval("IsFaxVerified") is System.DBNull || (Convert.ToBoolean(Eval("IsFaxVerified")) == false)) ? "hidden" : "" %>' />
          <asp:LinkButton ID="btnVerifyFax" runat="server" OnClick="btnVerifyFax_Click" CommandArgument='<%# Eval("ThirdPartySupplierID").ToString() + "|" + Eval("FaxOrderPlacement").ToString() %>' Text='<%# (Eval("IsFaxVerified") is System.DBNull || (Convert.ToBoolean(Eval("IsFaxVerified")) == false)) ? "Click to Verify" : "Verified" %>' Visible='<%# Eval("OrderPlacementType").ToString().ToLower().Contains("fax") ? true : false %>' Enabled='<%# (Eval("IsFaxVerified") is System.DBNull || (Convert.ToBoolean(Eval("IsFaxVerified")) == false)) ? true : false %>'>
          </asp:LinkButton>
        </ItemTemplate>
      </asp:TemplateField>
    </Columns>
  </asp:GridView>
</div>
