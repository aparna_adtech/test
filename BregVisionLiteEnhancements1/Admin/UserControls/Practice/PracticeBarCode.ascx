﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeBarCode.ascx.cs"
Inherits="BregVision.Admin.UserControls.PracticeBarCode" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<div class="content bar-code">
  <div class="flex-row">
    <h2>
      <asp:Label ID="lblContentHeader" runat="server" Text="Bar Codes" CssClass="PageTitle"></asp:Label>
    </h2>
    <bvu:RecordsPerPage ID="ctlRecordsPerPage" runat="server"/>
  </div>
  <asp:Panel ID="ItemSelectorPanel" runat="server">
    <div class="flex-row align-start">
      <div class="flex-column flex-grow">
        <div class="actions-bar flex-row">
          <h3>Practice Catalog</h3>
          <bvu:SearchToolbar ID="PracticeCatalogSearch" runat="server"></bvu:SearchToolbar>
        </div>
        <div>
          <asp:Label ID="lblMCStatus" runat="server" CssClass="WarningMsg"></asp:Label>
        </div>
        <bvu:VisionRadGridAjax ID="grdPracticeCatalog" runat="server" OnNeedDataSource="grdPracticeCatalog_NeedDataSource"
                               OnDetailTableDataBind="grdPracticeCatalog_OnDetailTableDataBind" OnPageIndexChanged="grdPracticeCatalog_PageIndexChanged"
                               OnPreRender="grdPracticeCatalog_PreRender" AddNonBreakingSpace="false" CssClass="RadGrid_Breg" ImagesPath="../../../App_Themes/Breg/images/Common/" Skin="Breg" EnableEmbeddedSkins="False">
          <MasterTableView AutoGenerateColumns="False" DataKeyNames="PracticeCatalogSupplierBrandID"
                           HierarchyLoadMode="ServerOnDemand">
            <DetailTables>
              <telerik:GridTableView runat="server" AutoGenerateColumns="False" DataKeyNames="PracticeCatalogSupplierBrandID"
                                     Name="grdtvPracticeCatalog" Skin="Breg" EnableEmbeddedSkins="False">
                <ParentTableRelation>
                  <telerik:GridRelationFields DetailKeyField="PracticeCatalogSupplierBrandID" MasterKeyField="PracticeCatalogSupplierBrandID"/>
                </ParentTableRelation>
                <Columns>
                  <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" ItemStyle-HorizontalAlign="Center"
                                                  HeaderStyle-HorizontalAlign="Center"/>
                  <telerik:GridBoundColumn DataField="ShortName" HeaderText="Name" UniqueName="ShortName"/>
                  <telerik:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code" ItemStyle-HorizontalAlign="Center"
                                           HeaderStyle-HorizontalAlign="Center"/>
                  <telerik:GridTemplateColumn HeaderText="Side / Size / Gender" ItemStyle-HorizontalAlign="Center"
                                              HeaderStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                      <small>
                        <table class="sideSizeGender">
                          <tr>
                            <td>Side</td>
                            <td><%# Eval("LeftRightSide") %></td>
                          </tr>
                          <tr>
                            <td>Size</td>
                            <td><%# Eval("Size") %></td>
                          </tr>
                          <tr>
                            <td>Gender</td>
                            <td><%# Eval("Gender") %></td>
                          </tr>
                        </table>
                      </small>
                    </ItemTemplate>
                  </telerik:GridTemplateColumn>
                  <telerik:GridBoundColumn DataField="LeftRightSide" HeaderText="Side" UniqueName="Side"
                                           Visible="false"/>
                  <telerik:GridBoundColumn DataField="Size" HeaderText="Size" UniqueName="Size" Visible="false"/>
                  <telerik:GridBoundColumn DataField="Gender" HeaderText="Gender" UniqueName="Gender"
                                           Visible="false"/>
                  <telerik:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" UniqueName="PracticeCatalogSupplierBrandID"
                                           Visible="false"/>
                  <telerik:GridBoundColumn DataField="PracticeCatalogProductID" Visible="false" UniqueName="PracticeCatalogProductID"/>
                  <telerik:GridBoundColumn DataField="ProductInventoryID" Visible="false" UniqueName="ProductInventoryID"/>
                  <telerik:GridBoundColumn DataField="MasterCatalogProductID" Visible="false" UniqueName="MasterCatalogProductID"/>
                </Columns>
              </telerik:GridTableView>
            </DetailTables>
            <Columns>
              <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" ItemStyle-HorizontalAlign="Center"
                                              HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px"/>
              <telerik:GridTemplateColumn HeaderText="Supplier / Brand" ItemStyle-HorizontalAlign="Center"
                                          HeaderStyle-HorizontalAlign="Center">
                <ItemTemplate>
                  <%# Eval("SupplierName") %>
                  /

                  <%# Eval("BrandName") %>
                </ItemTemplate>
              </telerik:GridTemplateColumn>
              <telerik:GridBoundColumn DataField="SupplierName" HeaderText="Supplier" UniqueName="SupplierName"
                                       Visible="false"/>
              <telerik:GridBoundColumn DataField="BrandName" HeaderText="Brand" UniqueName="BrandName"
                                       Visible="false"/>
              <telerik:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" UniqueName="PracticeCatalogSupplierBrandID"
                                       Visible="false"/>
            </Columns>
          </MasterTableView>
        </bvu:VisionRadGridAjax>
      </div>
      <div class="flex-column align-self-center layout-padding">
        <asp:Button ID="btnMoveToLabel" runat="server" OnClick="btnMoveToLabel_Click" Text="ADD &raquo;"
                    ToolTip="Move all selected items to Practice Catalog"/>
        <br/>
        <asp:Button ID="btnMoveToMC" runat="server" OnClick="btnMoveToMC_Click" Text="&laquo; REMOVE"
                    ToolTip="Remove all selected items from Practice Catalog"/>
      </div>
      <div class="flex-column flex-grow">
        <div class="flex-row actions-bar">
          <h3>Products to Print</h3>
          <asp:Button ID="btnPrintLabels" runat="server" OnClick="btnPrintLabels_Click" Text="Print Labels" CssClass="button-primary"/>
        </div>
        <div>
          <asp:Label ID="lblPCStatus" runat="server" CssClass="WarningMsg"></asp:Label>
          <asp:Label ID="lblDeleteMessage" runat="server" CssClass="WarningMsg"></asp:Label>
        </div>
        <bvu:VisionRadGridAjax ID="grdSummary" runat="server" AllowMultiRowSelection="True"
                               ClientSettings-Selecting-AllowRowSelect="true" OnDeleteCommand="grdSummary_DeleteCommand"
                               OnLoad="grdSummary_OnLoad" PagerStyle-AlwaysVisible="true" AddNonBreakingSpace="false" CssClass="RadGrid_Breg" Skin="Breg" EnableEmbeddedSkins="False">
          <MasterTableView>
            <Columns>
              <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" ItemStyle-HorizontalAlign="Center"
                                              HeaderStyle-HorizontalAlign="Center"/>
              <telerik:GridTemplateColumn HeaderText="Brand / Product / Code" ItemStyle-HorizontalAlign="Center"
                                          HeaderStyle-HorizontalAlign="Center">
                <ItemTemplate>
                  <%# Eval("BrandShortName") %>
                  /

                  <%# Eval("ShortName") %>
                  /

                  <%# Eval("Code") %>
                </ItemTemplate>
              </telerik:GridTemplateColumn>
              <telerik:GridTemplateColumn HeaderText="Side / Size / Gender" ItemStyle-HorizontalAlign="Center"
                                          HeaderStyle-HorizontalAlign="Center">
                <ItemTemplate>
                  <%# Eval("Side") %>
                  /

                  <%# Eval("Size") %>
                  /

                  <%# Eval("Gender") %>
                </ItemTemplate>
              </telerik:GridTemplateColumn>
              <telerik:GridBoundColumn DataField="BrandShortName" HeaderText="Brand" UniqueName="BrandShortName"
                                       Visible="false"/>
              <telerik:GridBoundColumn DataField="ShortName" HeaderText="Name" UniqueName="ShortName"
                                       Visible="false"/>
              <telerik:GridBoundColumn DataField="Code" HeaderText="Code" ItemStyle-HorizontalAlign="Center"
                                       HeaderStyle-HorizontalAlign="Center" UniqueName="ProductName" Visible="false"/>
              <telerik:GridBoundColumn DataField="Side" HeaderText="Side" ItemStyle-HorizontalAlign="Center"
                                       HeaderStyle-HorizontalAlign="Center" UniqueName="Side" Visible="false"/>
              <telerik:GridBoundColumn DataField="Size" HeaderText="Size" ItemStyle-HorizontalAlign="Center"
                                       HeaderStyle-HorizontalAlign="Center" UniqueName="Size" Visible="false"/>
              <telerik:GridBoundColumn DataField="Gender" HeaderText="Gender" ItemStyle-HorizontalAlign="Center"
                                       HeaderStyle-HorizontalAlign="Center" UniqueName="Gender" Visible="false"/>
              <telerik:GridBoundColumn DataField="PracticeLocationID" Visible="False" UniqueName="PracticeLocationID"/>
              <telerik:GridBoundColumn DataField="ProductInventoryID" Visible="False" UniqueName="ProductInventoryID"/>
              <telerik:GridBoundColumn DataField="PracticeCatalogProductID" Visible="False" UniqueName="PracticeCatalogProductID"/>
            </Columns>
          </MasterTableView>
        </bvu:VisionRadGridAjax>
      </div>
    </div>
  </asp:Panel>
  <asp:Panel ID="ReturnToSearchPanel" runat="server" Visible="false">
    <span>Label printing is designed for Avery 5260 labels.<br />
    Please export to the appropriate format below (export icon), insert Avery 5260 labels into your printer's manual paper feed area (if applicable) and press the printer icon. 
    </span>
    <div style="margin-top: 20px; margin-bottom: 20px;">
      <asp:Button ID="btnReturnToSearch" runat="server" OnClick="btnReturnToSearch_Click"
                  Text="&laquo; Return to Search" CssClass="button-primary"/>
    </div>
  </asp:Panel>
  <bvu:BregVisionSSRSReportViewer ID="ReportViewer1" runat="server" KeepSessionAlive="true" AsyncRendering="false"
                                  SizeToReportContent="true" ProcessingMode="Remote" Visible="false" Width="100%">
    <ServerReport ReportServerUrl="http://Vision/reportserver"/>
  </bvu:BregVisionSSRSReportViewer>
</div>
