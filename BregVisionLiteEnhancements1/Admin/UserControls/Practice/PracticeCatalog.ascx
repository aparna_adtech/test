﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="PracticeCatalog.ascx.cs" Inherits="BregVision.Admin.UserControls.PracticeCatalog" %>
<%@ Register Src="~/Admin/UserControls/ProductReplacement.ascx" TagName="ProductReplacement" TagPrefix="bva" %>

<script runat="server" type="text/C#">

  public bool CheckBoxChecked(object x)
  {
    return (x == null || x.ToString() == "True");
  }

</script>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
  <script type="text/javascript" language="javascript">
    function ShowInsertForm() {
      //alert('got here');
      //window.radopen("~/Admin/HCPCs.aspx", "HCPCsDialog");
      return false;
    }
  </script>
</telerik:RadScriptBlock>
<telerik:RadScriptBlock ID="RadScriptBlock2" runat="server">
  <script language="javascript" type="text/javascript">
    function refreshGrid() {
      <% if (WriteAjaxScript())
         { %>
      $.find('<%= Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page).ClientID %>').ajaxRequest("Rebind");
      <% } %>
    }

    function StopPropagation(e) {
      //cancel bubbling
      e.cancelBubble = true;
      if (e.stopPropagation) {
        e.stopPropagation();
      }
    }

    function onCheckBoxClick(chkAllItems) {
      var combo = $find("<%= cboPracticeLocation.ClientID %>");

      var items = combo.get_items();
      //enumerate all items
      for (var i = 0; i < items.get_count(); i++) {
        var item = items.getItem(i);
        //get the checkbox element of the current item
        var chk1 = $get(combo.get_id() + "_i" + i + "_chkLocation");
        chk1.checked = chkAllItems.checked;
      }
    }

    function updateLocationWarning() {
      var combo = $find("<%= cboPracticeLocation.ClientID %>");
      var items = combo.get_items();
      var has_checked = false;
      for (var i = 0; i < items.get_count(); i++) {
        var item = items.getItem(i);
        var c = $get(combo.get_id() + "_i" + i + "_chkLocation");
        if (c.checked == true) {
          $('#AddToLocationsWarning')[0].style.display = 'none';
          return;
        }
        $('#AddToLocationsWarning')[0].style.display = 'block';
      }
    }
    
      function SetTextLabel() {
           var combo = $find("<%= cboPracticeLocation.ClientID %>");
           var textfield = document.getElementById(combo.get_id() + "_Input");
           var allCheck = document.getElementById(combo.get_id() + "_Header_chkLocationHeader").checked;
           if (allCheck)
               textfield.value = "All Locations";
           else {
               textfield.value = "";
               var count= 0;
               var items = combo.get_items();
               for (var i = 0; i < items.get_count() ; i++) {
                   var c = $get(combo.get_id() + "_i" + i + "_chkLocation");
                   if (c.checked == true) {
                       if(count == 0)
                           textfield.value += $("label[for='" + c.id + "']").text();
                       else
                           textfield.value += ', ' + $("label[for='" + c.id + "']").text();
                       count++;
                   }
               }
           }
      }
  </script>
</telerik:RadScriptBlock>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
  <AjaxSettings>
    <telerik:AjaxSetting AjaxControlID="txtRecordsDisplayed">
      <UpdatedControls>
        <telerik:AjaxUpdatedControl ControlID="grdMasterCatalog" LoadingPanelID="RadAjaxLoadingPanel1"/>
        <telerik:AjaxUpdatedControl ControlID="grdPracticeCatalog" LoadingPanelID="RadAjaxLoadingPanel1"/>
        <telerik:AjaxUpdatedControl ControlID="RadInputManager1"/>
      </UpdatedControls>
    </telerik:AjaxSetting>
    <telerik:AjaxSetting AjaxControlID="btnUpdatePC">
      <UpdatedControls>
        <telerik:AjaxUpdatedControl ControlID="RadInputManager1"/>
        <telerik:AjaxUpdatedControl ControlID="grdPracticeCatalog" LoadingPanelID="RadAjaxLoadingPanel1"/>
        <telerik:AjaxUpdatedControl ControlID="lblDeleteMessage"/>
        <telerik:AjaxUpdatedControl ControlID="lblPCStatus"/>
      </UpdatedControls>
    </telerik:AjaxSetting>
    <telerik:AjaxSetting AjaxControlID="grdMasterCatalog">
      <UpdatedControls>
        <telerik:AjaxUpdatedControl ControlID="grdMasterCatalog" LoadingPanelID="RadAjaxLoadingPanel1"/>
      </UpdatedControls>
    </telerik:AjaxSetting>
    <telerik:AjaxSetting AjaxControlID="btnMoveToPC">
      <UpdatedControls>
        <telerik:AjaxUpdatedControl ControlID="RadInputManager1"/>
        <telerik:AjaxUpdatedControl ControlID="grdPracticeCatalog" LoadingPanelID="RadAjaxLoadingPanel1"/>
        <telerik:AjaxUpdatedControl ControlID="lblPCStatus"/>
        <telerik:AjaxUpdatedControl ControlID="btnMoveToPC"/>
      </UpdatedControls>
    </telerik:AjaxSetting>
    <telerik:AjaxSetting AjaxControlID="grdPracticeCatalog">
      <UpdatedControls>
        <telerik:AjaxUpdatedControl ControlID="RadInputManager1"/>
        <telerik:AjaxUpdatedControl ControlID="grdPracticeCatalog" LoadingPanelID="RadAjaxLoadingPanel1"/>
      </UpdatedControls>
    </telerik:AjaxSetting>
  </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" InitialDelayTime="500" EnableAjaxSkinRendering="True" BackgroundPosition="Center" Skin="Breg" EnableEmbeddedSkins="False">
  <img src="../App_Themes/Breg/images/Common/loading.gif"/>
</telerik:RadAjaxLoadingPanel>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
  <telerik:NumericTextBoxSetting BehaviorID="stockSetting" Culture="en-US" DecimalDigits="0"
                                 DecimalSeparator="." GroupSeparator="," GroupSizes="3" MaxValue="999" MinValue="0"
                                 NegativePattern="-n" PositivePattern="n" SelectionOnFocus="SelectAll">
  </telerik:NumericTextBoxSetting>
  <telerik:NumericTextBoxSetting BehaviorID="currencySetting" Culture="en-US" DecimalDigits="2"
                                 DecimalSeparator="." GroupSeparator="," GroupSizes="3" MaxValue="9999" MinValue="0"
                                 NegativePattern="($n)" PositivePattern="$n" SelectionOnFocus="SelectAll" Type="Currency">
  </telerik:NumericTextBoxSetting>
</telerik:RadInputManager>

<div class="content">
<div class="flex-row">
  <h2>
    <asp:Label ID="lblContentHeader" runat="server" Text="Practice Catalog" CssClass="PageTitle"></asp:Label>
    <bvu:Help ID="Help16" runat="server" HelpID="16"/>
  </h2>
  <div class="flex-row justify-end">
    <telerik:RadNumericTextBox ID="txtRecordsDisplayed" runat="server" AutoPostBack="true"
                               Culture="English (United States)" MaxValue="100" MinValue="0"
                               NumberFormat-DecimalDigits="0" OnTextChanged="txtRecordsDisplayed_TextChanged"
                               ShowSpinButtons="false" Skin="Breg" EnableEmbeddedSkins="False" Label="Records per Page">
    </telerik:RadNumericTextBox>
    <div style="margin-left: 10px; width: 20px;">
      <bvu:Help ID="Help7" runat="server" HelpID="19"/>
    </div>
  </div>
</div>
<div class="flex-row">
  <h3>
    <span class="PageTitle">Master Catalog</span>
    <bvu:Help ID="Help17" runat="server" HelpID="17"/>
  </h3>
  <asp:UpdatePanel runat="server" UpdateMode="Always" RenderMode="Inline">
    <ContentTemplate>
      <asp:Label ID="AddToInventoryCompletedLabel" runat="server" Text="Product(s) added" Visible="false" EnableViewState="false"/>
    </ContentTemplate>
  </asp:UpdatePanel>
  <div class="flex-row justify-end">
    <div id="AddToLocationsWarning" class="no-wrap">
      A location must be selected prior to addition!
    </div>
    <asp:Label ID="lblMCStatus" runat="server" CssClass="WarningMsg"></asp:Label>
    <bvu:Help ID="Help216" runat="server" HelpID="16"/>
  </div>
</div>
<div class="flex-row actions-bar">
  <div class="search-container flex-row">
    <bvu:SearchToolbar ID="MasterCatalogSearch" runat="server"></bvu:SearchToolbar>
    <bvu:Help ID="Help1" runat="server" HelpID="23"/>
    <asp:CheckBox ID="chkMoveHcpcs" runat="server" Checked="true" Text="Transfer HCPCs"/>&nbsp;&nbsp;
    <asp:CheckBox ID="chkMoveModifiers" runat="server" Checked="True" Text="Transfer Modifiers"/>
  </div>
  <div class="no-wrap">
    <span class="FieldLabel FieldLabelLeft">Add to Locations</span>
    <telerik:RadComboBox ID="cboPracticeLocation" runat="server" HighlightTemplatedItems="true" AllowCustomText="false" EmptyMessage="Select Location(s)">
      <HeaderTemplate>
        <div onclick="StopPropagation(event)">
          <asp:CheckBox ID="chkLocationHeader" runat="server" Text="All Locations" onclick="onCheckBoxClick(this); updateLocationWarning(); SetTextLabel();"/>
        </div>
      </HeaderTemplate>
      <ItemTemplate>
        <div onclick="StopPropagation(event)">
          <asp:CheckBox ID="chkLocation" runat="server" Text='<%# Bind("Name") %>' onclick="updateLocationWarning(); SetTextLabel();"/>
        </div>
      </ItemTemplate>
    </telerik:RadComboBox>
    &nbsp;
    <asp:Button ID="btnMoveToPC" runat="server" OnClick="btnMoveToPC_Click" Text="Add to Inventory" ToolTip="Move all selected items to Practice Catalog" CssClass="button-primary"/>
  </div>
</div>

<bvu:VisionRadGridAjax ID="grdMasterCatalog" runat="server" EnableLinqExpressions="false" OnItemCommand="grdMasterCatalog_ItemCommand"
                       AllowMultiRowSelection="True" OnNeedDataSource="grdMasterCatalog_NeedDataSource"
                       OnDetailTableDataBind="grdMasterCatalog_OnDetailTableDataBind" OnPageIndexChanged="grdMasterCatalog_PageIndexChanged"
                       OnPreRender="grdMasterCatalog_PreRender" AddNonBreakingSpace="false" Skin="Breg" EnableEmbeddedSkins="False"
                       PagerStyle-AlwaysVisible="true">
  <MasterTableView DataKeyNames="SupplierID" HierarchyLoadMode="ServerOnDemand">
    <DetailTables>
      <telerik:GridTableView runat="server" DataKeyNames="SupplierID" Width="100%">
        <ParentTableRelation>
          <telerik:GridRelationFields DetailKeyField="SupplierID" MasterKeyField="SupplierID"/>
        </ParentTableRelation>
        <Columns>
          <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" HeaderStyle-HorizontalAlign="Center"
                                          ItemStyle-HorizontalAlign="Center"/>
          <telerik:GridBoundColumn DataField="ShortName" HeaderText="Name" UniqueName="ShortName"/>
          <telerik:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code"/>
          <telerik:GridBoundColumn DataField="Packaging" HeaderText="Pkg." UniqueName="Packaging"/>
          <telerik:GridBoundColumn DataField="LeftRightSide" HeaderText="Side" UniqueName="LeftRightSide" ItemStyle-HorizontalAlign="Center"
                                   ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"/>
          <telerik:GridBoundColumn DataField="Modifiers" HeaderText="Modifiers" UniqueName="Modifiers"/>
          <telerik:GridBoundColumn DataField="Size" HeaderText="Size" UniqueName="Size"/>
          <telerik:GridBoundColumn DataField="Color" HeaderText="Color" UniqueName="Color"/>
          <telerik:GridBoundColumn DataField="WholesaleListCost" DataFormatString="{0:C}" HeaderText="WLC" UniqueName="WholesaleListCost" ItemStyle-HorizontalAlign="Right"
                                   ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Right"/>
          <telerik:GridCheckBoxColumn DataField="IsLogoAblePart" HeaderText="Logo" UniqueName="IsLogoAblePart" ItemStyle-HorizontalAlign="Center"
                                      ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"/>
          <telerik:GridBoundColumn DataField="MastercatalogProductID" Display="false" UniqueName="MastercatalogProductID"/>
          <telerik:GridBoundColumn DataField="SupplierID" UniqueName="SupplierID" Visible="false"/>
          <telerik:GridBoundColumn DataField="MasterCatalogSubCategoryID" Display="false" UniqueName="MasterCatalogSubCategoryID"/>
        </Columns>
      </telerik:GridTableView>
    </DetailTables>
    <Columns>
      <telerik:GridBoundColumn DataField="SupplierID" UniqueName="SupplierID" Visible="False"/>
      <telerik:GridBoundColumn DataField="Brand" HeaderText="Brand" UniqueName="Brand"/>
      <telerik:GridBoundColumn DataField="Name" HeaderText="First Name" UniqueName="Name"/>
      <telerik:GridBoundColumn DataField="LastName" HeaderText="Last Name" UniqueName="LastName"/>
      <telerik:GridBoundColumn DataField="PhoneWork" HeaderText="Phone" UniqueName="PhoneWork"/>
      <telerik:GridBoundColumn DataField="Address" HeaderText="Address" UniqueName="Address"/>
      <telerik:GridBoundColumn DataField="City" HeaderText="City" UniqueName="City"/>
      <telerik:GridBoundColumn DataField="ZipCode" HeaderText="Zip Code" UniqueName="ZipCode"/>
    </Columns>
  </MasterTableView>
</bvu:VisionRadGridAjax>
<div class="flex-row">
  <h3>
    <span class="PageTitle">Practice Catalog</span>
    <bvu:Help ID="Help18" runat="server" HelpID="18"/>
  </h3>
  <div>
    <asp:Label ID="lblPCStatus" runat="server" CssClass="WarningMsg"></asp:Label>
    <asp:Label ID="lblDeleteMessage" runat="server" CssClass="WarningMsg"></asp:Label>
  </div>
</div>
<div class="flex-row actions-bar">
  <div class="search-container flex-row">
    <bvu:SearchToolbar ID="PracticeCatalogSearch" runat="server"></bvu:SearchToolbar>
    <bvu:Help ID="Help5" runat="server" HelpID="23"/>
  </div>
  <div>
    <bvu:Help ID="Help8" runat="server" HelpID="20"/>
    <asp:Button ID="btnUpdatePC" runat="server" OnClick="btnUpdatePC_Click" Text="Update Pricing" CssClass="button-primary"/>
  </div>
</div>


<div id="practiceCatalogWrapper">
<bvu:VisionRadGridAjax ID="grdPracticeCatalog" runat="server" AllowMultiRowSelection="True"
                       OnDeleteCommand="grdPracticeCatalog_DeleteCommand" OnItemCommand="grdPracticeCatalog_ItemCommand"
                       OnItemDataBound="grdPracticeCatalog_ItemDataBound" OnNeedDataSource="grdPracticeCatalog_NeedDataSource"
                       OnDetailTableDataBind="grdPracticeCatalog_OnDetailTableDataBind" OnPageIndexChanged="grdPracticeCatalog_PageIndexChanged"
                       OnPreRender="grdPracticeCatalog_PreRender" AddNonBreakingSpace="false" Skin="Breg" EnableEmbeddedSkins="False"
                       PagerStyle-AlwaysVisible="true">
<MasterTableView DataKeyNames="PracticeCatalogSupplierBrandID" HierarchyLoadMode="ServerOnDemand" Name="MasterTableView_PracticeCatalog">
<Columns>
  <telerik:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" UniqueName="PracticeCatalogSupplierBrandID"
                           Visible="false"/>
  <telerik:GridBoundColumn DataField="SupplierName" HeaderText="Supplier" UniqueName="SupplierName"/>
  <telerik:GridBoundColumn DataField="BrandName" HeaderText="Brand" UniqueName="BrandName"/>
</Columns>
<DetailTables>
<telerik:GridTableView runat="server" AutoGenerateColumns="False" DataKeyNames="PracticeCatalogSupplierBrandID"
                       HierarchyLoadMode="ServerBind" Name="GridTableView_PracticeCatalog">
<ParentTableRelation>
  <telerik:GridRelationFields DetailKeyField="PracticeCatalogSupplierBrandID" MasterKeyField="PracticeCatalogSupplierBrandID"/>
</ParentTableRelation>
<Columns>
  <telerik:GridTemplateColumn HeaderStyle-Width="40px" HeaderStyle-Wrap="true" HeaderText=""
                              ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="middle" UniqueName="DeleteColumn">
    <HeaderTemplate>
      <bvu:Help ID="Help116" runat="server" HelpID="13"/>
    </HeaderTemplate>
    <ItemTemplate>
      <asp:ImageButton ID="btnDelete" runat="server" CommandName="Delete" ImageAlign="Middle"/>
    </ItemTemplate>
  </telerik:GridTemplateColumn>
  <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn2" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="40px"/>
  <telerik:GridBoundColumn DataField="ShortName" HeaderText="Name" UniqueName="ShortName"/>
    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" ImageUrl="~/App_Themes/Breg/images/Common/edit.png" UniqueName="btnEdit" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridButtonColumn>
  <telerik:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code" HeaderStyle-Width="40px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"/>
  <telerik:GridBoundColumn DataField="LeftRightSide" HeaderText="Side" UniqueName="LeftRightSide" HeaderStyle-Width="40px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"/>
  <telerik:GridTemplateColumn DataField="IsSideExempt" HeaderStyle-Width="40px" HeaderStyle-Wrap="true" UniqueName="IsSideExempt" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center">
    <HeaderTemplate>Is Side Exempt</HeaderTemplate>
    <ItemTemplate>
      <asp:CheckBox ID="chkIsSideExempt" runat="server" Checked='<%# Eval("IsLateralityExempt") == DBNull.Value ? false : Convert.ToBoolean(Eval("IsLateralityExempt")) %>'/>
    </ItemTemplate>
  </telerik:GridTemplateColumn>
  <telerik:GridTemplateColumn DataField="Mod1" HeaderText="Modifiers" UniqueName="Mod1">
    <ItemTemplate>
    <div class="Dropdown-Container">
      <asp:DropDownList ID="ddlMod1" runat="server" SelectedValue='<%# Bind("Mod1") %>'>
        <asp:ListItem Text="" Value=""></asp:ListItem>
        <asp:ListItem Text="CG" Value="CG"></asp:ListItem>
        <asp:ListItem Text="EY" Value="EY"></asp:ListItem>
        <asp:ListItem Text="GA" Value="GA"></asp:ListItem>
        <asp:ListItem Text="GX" Value="GX"></asp:ListItem>
        <asp:ListItem Text="GY" Value="GY"></asp:ListItem>
        <asp:ListItem Text="GZ" Value="GZ"></asp:ListItem>
        <asp:ListItem Text="KV" Value="KV"></asp:ListItem>
        <asp:ListItem Text="KX" Value="KX"></asp:ListItem>
        <asp:ListItem Text="NU" Value="NU"></asp:ListItem>
        <asp:ListItem Text="RA" Value="RA"></asp:ListItem>
        <asp:ListItem Text="RB" Value="RB"></asp:ListItem>
        <asp:ListItem Text="RR" Value="RR"></asp:ListItem>
      </asp:DropDownList>
    </div>
    <div class="Dropdown-Container">
      <asp:DropDownList ID="ddlMod2" runat="server" SelectedValue='<%# Bind("Mod2") %>'>
        <asp:ListItem Text="" Value=""></asp:ListItem>
        <asp:ListItem Text="CG" Value="CG"></asp:ListItem>
        <asp:ListItem Text="EY" Value="EY"></asp:ListItem>
        <asp:ListItem Text="GA" Value="GA"></asp:ListItem>
        <asp:ListItem Text="GX" Value="GX"></asp:ListItem>
        <asp:ListItem Text="GY" Value="GY"></asp:ListItem>
        <asp:ListItem Text="GZ" Value="GZ"></asp:ListItem>
        <asp:ListItem Text="KV" Value="KV"></asp:ListItem>
        <asp:ListItem Text="KX" Value="KX"></asp:ListItem>
        <asp:ListItem Text="NU" Value="NU"></asp:ListItem>
        <asp:ListItem Text="RA" Value="RA"></asp:ListItem>
        <asp:ListItem Text="RB" Value="RB"></asp:ListItem>
        <asp:ListItem Text="RR" Value="RR"></asp:ListItem>
      </asp:DropDownList>
    </div>
    <div class="Dropdown-Container">
      <asp:DropDownList ID="ddlMod3" runat="server" SelectedValue='<%# Bind("Mod3") %>'>
        <asp:ListItem Text="" Value=""></asp:ListItem>
        <asp:ListItem Text="CG" Value="CG"></asp:ListItem>
        <asp:ListItem Text="EY" Value="EY"></asp:ListItem>
        <asp:ListItem Text="GA" Value="GA"></asp:ListItem>
        <asp:ListItem Text="GX" Value="GX"></asp:ListItem>
        <asp:ListItem Text="GY" Value="GY"></asp:ListItem>
        <asp:ListItem Text="GZ" Value="GZ"></asp:ListItem>
        <asp:ListItem Text="KV" Value="KV"></asp:ListItem>
        <asp:ListItem Text="KX" Value="KX"></asp:ListItem>
        <asp:ListItem Text="NU" Value="NU"></asp:ListItem>
        <asp:ListItem Text="RA" Value="RA"></asp:ListItem>
        <asp:ListItem Text="RB" Value="RB"></asp:ListItem>
        <asp:ListItem Text="RR" Value="RR"></asp:ListItem>
      </asp:DropDownList>
    </div>
    </div>
    </ItemTemplate>
  </telerik:GridTemplateColumn>
  <telerik:GridBoundColumn DataField="Size" HeaderText="Size" UniqueName="Size" HeaderStyle-Width="40px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"/>
  <telerik:GridTemplateColumn DataField="HCPCSString" UniqueName="HCPCs" HeaderText="Edit HCPCs">
    <ItemTemplate>
      <div>
        <div>
          <asp:Label runat="server" Text="Custom Fit" data-toggle="splitToggleable" data-init="false" data-pcpid='<%# Eval("PracticecatalogProductID") %>' Style="display: none;"></asp:Label>
        </div>
        <asp:TextBox ID="txtHcpcs" runat="server" Text='<%# Bind("HCPCSString") %>'></asp:TextBox><br/>
        <div style="float: left; margin-right: 10px">
          <div>
            <asp:Label runat="server" Text="Off the Shelf" data-toggle="splitToggleable" data-init="false" data-pcpid='<%# Eval("PracticecatalogProductID") %>' Style="display: none;"></asp:Label>
          </div>
          <asp:TextBox ID="txtHcpcsOTS" runat="server" Text='<%# Bind("HCPCSStringOTS") %>' data-toggle="splitToggleable" data-init="false"
                       data-pcpid='<%# Eval("PracticecatalogProductID") %>' Style="display: none;">
          </asp:TextBox>
        </div>
        <div style="float: left;">
          <div>
            <asp:Label runat="server" Text="Always OTS" data-toggle="splitToggleable" data-init="false" data-pcpid='<%# Eval("PracticecatalogProductID") %>' Style="display: none;"></asp:Label>
          </div>
          <asp:CheckBox ID="chkIsAlwaysOffTheShelf" runat="server" Checked='<%# Bind("IsAlwaysOffTheShelf") %>' data-toggle="splitToggleable" data-init="false"
                        data-pcpid='<%# Eval("PracticecatalogProductID") %>' Style="display: none;"/>
        </div>
      </div>
    </ItemTemplate>
  </telerik:GridTemplateColumn>
  <telerik:GridTemplateColumn DataField="SplitCode" UniqueName="HCPCsSplitCode" HeaderStyle-Width="40px" HeaderText="Split Code" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
    <ItemTemplate>
      <asp:CheckBox ID="chkIsSplitCode" CssClass="splitToggle" runat="server" Enabled="<%# practiceIsCustomFitEnabled %>" Checked='<%# Eval("IsSplitCode") %>' OnPreRender="SplitCodeDependent_OnPreRender"
                    OnClick="splitCodeToggle(this);" data-pcpid='<%# Eval("PracticecatalogProductID") %>'/>
    </ItemTemplate>
  </telerik:GridTemplateColumn>
  <telerik:GridTemplateColumn DataField="StockingUnits" HeaderStyle-Width="40px" HeaderStyle-Wrap="true"
                              UniqueName="TemplateColumn3" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"
                              HeaderStyle-HorizontalAlign="Center">
    <HeaderTemplate>
      <span>Stocking Units</span>
      <bvu:Help ID="Help27" runat="server" HelpID="27"/>
    </HeaderTemplate>
    <ItemTemplate>
      <telerik:RadNumericTextBox ID="txtStockingUnits" runat="server" Culture="English (United States)"
                                 MaxValue="999" HeaderStyle-Width="40px" MinValue="1" NumberFormat-DecimalDigits="0"
                                  ShowSpinButtons="False" Text='<%# Bind("StockingUnits") %>' Type="Number"
                                 Width="50px">
      </telerik:RadNumericTextBox>
    </ItemTemplate>
  </telerik:GridTemplateColumn>
  <telerik:GridTemplateColumn DataField="WholesaleCost" HeaderStyle-Width="50px" HeaderStyle-Wrap="true"
                              UniqueName="TemplateColumn2" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"
                              HeaderStyle-HorizontalAlign="Center">
    <HeaderTemplate>
      <span>Wholesale Cost</span>
      <bvu:Help ID="Help28" runat="server" HelpID="28"/>
    </HeaderTemplate>
    <ItemTemplate>
      <asp:TextBox ID="txtWholesaleCost" runat="server" Text='<%# Bind("WholesaleCost") %>'
                   Width="50px" OnLoad="RadNumericTextBox_Load2">
      </asp:TextBox>
    </ItemTemplate>
  </telerik:GridTemplateColumn>
  <telerik:GridTemplateColumn DataField="BillingCharge" HeaderStyle-Width="50px" HeaderStyle-Wrap="true"
                              UniqueName="TemplateColumn3" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"
                              HeaderStyle-HorizontalAlign="Center">
    <HeaderTemplate>
      <span>Billing<br />
                  Charge</span>
      <bvu:Help ID="Help29" runat="server" HelpID="29"/>
    </HeaderTemplate>
    <ItemTemplate>
      <div>
        <asp:Label runat="server" Text="CF" data-toggle="splitToggleable" data-init="false" data-pcpid='<%# Eval("PracticecatalogProductID") %>' Style="display: none;"></asp:Label>
      </div>
      <asp:TextBox ID="txtBillingCharge" runat="server" Text='<%# Bind("BillingCharge") %>'
                   Width="50px" OnLoad="RadNumericTextBox_Load2">
      </asp:TextBox><br/>
      <div>
        <asp:Label runat="server" Text="OTS" data-toggle="splitToggleable" data-init="false" data-pcpid='<%# Eval("PracticecatalogProductID") %>' Style="display: none;"></asp:Label>
      </div>
      <asp:TextBox ID="txtBillingChargeOTS" runat="server" Text='<%# Bind("BillingChargeOTS") %>'
                   Width="50px" OnPreRender="RadNumericTextBox_Load2" data-toggle="splitToggleable" data-init="false" data-pcpid='<%# Eval("PracticecatalogProductID") %>' Style="display: none;">
      </asp:TextBox>
    </ItemTemplate>
  </telerik:GridTemplateColumn>
  <telerik:GridTemplateColumn DataField="BillingChargeCash" HeaderStyle-Width="50px"
                              HeaderStyle-Wrap="true" UniqueName="TemplateColumn4" ItemStyle-HorizontalAlign="Center"
                              ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center">
    <HeaderTemplate>
      <span>Cash &amp;<br />
                  Carry</span>
      <bvu:Help ID="Help30" runat="server" HelpID="30"/>
    </HeaderTemplate>
    <ItemTemplate>
      <asp:TextBox ID="txtBillingChargeCash" runat="server" Text='<%# Bind("BillingChargeCash") %>'
                   Width="50px" OnLoad="RadNumericTextBox_Load2">
      </asp:TextBox>
    </ItemTemplate>
  </telerik:GridTemplateColumn>
  <telerik:GridTemplateColumn DataField="DMEDeposit" HeaderStyle-Width="50px" HeaderStyle-Wrap="true"
                              UniqueName="TemplateColumn5" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"
                              HeaderStyle-HorizontalAlign="Center">
    <HeaderTemplate>
      <span>DME Deposit</span>
      <bvu:Help ID="Help31" runat="server" HelpID="31"/>
    </HeaderTemplate>
    <ItemTemplate>
      <asp:TextBox ID="txtDMEDeposit" runat="server" Text='<%# Bind("DMEDeposit") %>'
                   Width="50px" OnLoad="RadNumericTextBox_Load2">
      </asp:TextBox>
    </ItemTemplate>
  </telerik:GridTemplateColumn>

  <telerik:GridTemplateColumn DataField="IsLogoAblePart" HeaderStyle-Width="40px" HeaderStyle-Wrap="true"
                              UniqueName="IsLogoAblePartPcp" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center">
    <HeaderTemplate>
      <span>Logo Part</span>
    </HeaderTemplate>
    <ItemTemplate>
      <asp:CheckBox ID="chkIsLogoAblePart" runat="server" Checked='<%# Eval("IsLogoAblePart") %>'/>
    </ItemTemplate>
  </telerik:GridTemplateColumn>
  <telerik:GridTemplateColumn DataField="PreAuthorization" HeaderStyle-Width="40px" HeaderStyle-Wrap="true" UniqueName="PreAuthorization"
                              ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center">
    <HeaderTemplate>Pre- Author ization</HeaderTemplate>
    <ItemTemplate>
      <asp:CheckBox ID="chkIsPreAuthorization" runat="server" Checked='<%# Eval("IsPreAuthorization") == DBNull.Value ? false : Convert.ToBoolean(Eval("IsPreAuthorization")) %>'/>
    </ItemTemplate>

  </telerik:GridTemplateColumn>
	<telerik:GridTemplateColumn UniqueName="btnEdit" HeaderText="Replaced" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"> 
		<ItemTemplate> 
			<asp:ImageButton ID="ProductReplacementActive" CommandName="Edit" ImageUrl="~/App_Themes/Breg/images/Common/replace-active.png"
				Visible='<%# Convert.ToBoolean(Eval("IsReplaced")) %>' ImageAlign="Middle" runat="server"></asp:ImageButton>
			<asp:ImageButton ID="ProductReplacementInactive" CommandName="Edit" ImageUrl="~/App_Themes/Breg/images/Common/replace-inactive.gif"
				Visible='<%# !Convert.ToBoolean(Eval("IsReplaced")) %>' ImageAlign="Middle" runat="server"></asp:ImageButton>
		</ItemTemplate> 
	</telerik:GridTemplateColumn>

  <telerik:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" UniqueName="PracticeCatalogSupplierBrandID"
                           Visible="false"/>
  <telerik:GridBoundColumn DataField="PracticeCatalogProductID" Display="false" UniqueName="PracticeCatalogProductID"/>
  <telerik:GridBoundColumn DataField="IsDeletable" Visible="false" UniqueName="IsDeletable"/>
  <telerik:GridBoundColumn DataField="MasterCatalogProductID" Visible="false" UniqueName="MasterCatalogProductID"/>
</Columns>
  <EditFormSettings EditFormType="Template">
    <EditColumn UniqueName="EditCommandColumn1"></EditColumn>
    <FormTemplate>
		<div class="flex-row" style='border: thin solid black; <%# ShowProductReplacement ? "" : "display: none" %>'>
			<bva:ProductReplacement IsPracticeSpecific="true" IsShowCombinedCatalogReplacement="true"
				MasterCatalogProductID='<%# (bool)Eval("IsThirdPartyProduct") ? 0 : (int)Eval("MasterCatalogProductID") %>'
				ThirdPartyProductID='<%# (bool)Eval("IsThirdPartyProduct") ? (int)Eval("MasterCatalogProductID") : 0 %>'
				OnWasClickedOn="ProductReplacementWasClickedOn" runat="server"/>
		</div>
    </FormTemplate>
  </EditFormSettings>
</telerik:GridTableView>
</DetailTables>
</MasterTableView>
</bvu:VisionRadGridAjax>
</div>

</div>
<script type="text/javascript">
  function splitCodeToggle(x) {
      var $splitCodeCell = $(x).closest('td');

      var $HCPCCell = $splitCodeCell.prev();
      $HCPCCell.find('[data-toggle="splitToggleable"]').each(function() {
        $(this).toggle();
      });

      var $billingCell = $splitCodeCell.next().next().next();
      $billingCell.find('[data-toggle="splitToggleable"]').each(function() {
        $(this).toggle();
      });

  }

  function bindInitialSplitState() {
      $("#practiceCatalogWrapper").find('[data-toggle="splitToggleable"][data-init="false"]').each(function() {
        $(this).data('init', 'true');

        var pcpid = $(this).data('pcpid');
        var $splitToggle = $("#practiceCatalogWrapper").find('.splitToggle[data-pcpid="' + pcpid + '"]');

        if (!$splitToggle.data('hidden')) {
          $(this).show();
        }
      });
  }
</script>
