﻿using System;
using System.Data;
using System.Drawing;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using BregVision.Admin.PracticeSetup;

namespace BregVision.Admin.UserControls
{
    public partial class PracticeUsers : PracticeSetupBase, IVisionWizardControl
    {

        public string UP1 = "";

        /// <summary>
        /// IVisionWizardControl Interface.
        /// </summary>
        public bool NeedsInitialBind { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (grdPracticeUser.HasInitiallyBound == false)
            {
                grdPracticeUser.Rebind();
                grdPracticeUser.HasInitiallyBound = true;
            }

            lblStatus.Visible = false;
            lblStatus.Text = "";
        }

        private void InitializeAjaxSettings()
        {
            Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);
            if (radAjaxManager != null)
            {
                //radAjaxManager.EnableAJAX = false;
                radAjaxManager.AjaxSettings.AddAjaxSetting(grdPracticeUser, grdPracticeUser, RadAjaxLoadingPanel1);
                radAjaxManager.AjaxSettings.AddAjaxSetting(grdPracticeUser, lblStatus);

            }

        }


        protected void grdPracticeUser_OnItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (BLL.Website.IsVisionExpress() && grdPracticeUser.Items.Count > 2)
            {
                if (e.CommandName == "InitInsert")
                {
                    e.Canceled = true;
                    lblStatus.Visible = true;
                    lblStatus.Text = "Only 3 users allowed in Vision Express";
                }
            }

            if (e.CommandName == "Activate" || e.CommandName == "Unlock")
            {
                var editableItem = e.Item as Telerik.Web.UI.GridEditableItem;

                var userName = editableItem["UserName"].Text;
                var visionUserId = Convert.ToInt32(editableItem["UserID"].Text);

                MembershipUser membershipProviderUser = null;
                membershipProviderUser = Membership.GetUser(userName);
                membershipProviderUser.UnlockUser();
                membershipProviderUser.IsApproved = true;
                Membership.UpdateUser(membershipProviderUser);

                (new BLL.Practice.User()).ActivateUser(practiceID, visionUserId);

                grdPracticeUser.Rebind();
            }
        }

        protected void grdPracticeUser_OnItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            RadComboBox rcbRole = null;
            string userRole = "";

            RadComboBox rcbDefaultPracticeLocation = null;
            int defaultPracticeLocation = 0;

            CheckBoxList cbPracticeLocationPermissions = null;
            //The problem here is I can't find the dropdown / combo box in the grid 
            if (e.Item is Telerik.Web.UI.GridEditableItem && (e.Item as Telerik.Web.UI.GridEditableItem).IsInEditMode)
            {
                Telerik.Web.UI.GridEditableItem editedItem = e.Item as Telerik.Web.UI.GridEditableItem;

                bool isInsert = editedItem.Edit; //this = true for a new item, false for an existing item.

                #region Default Records per Page
                Telerik.Web.UI.RadNumericTextBox txtDefaultRecordsPerPage = editedItem.FindControl("txtDefaultRecordsPerPage") as Telerik.Web.UI.RadNumericTextBox;
                if ((txtDefaultRecordsPerPage != null) && string.IsNullOrEmpty(txtDefaultRecordsPerPage.Text))
                {
                    txtDefaultRecordsPerPage.Text = "10";
                }
                #endregion

                #region Username and Password
                if (isInsert == true)
                {
                    TextBox txtUserName = editedItem.FindControl("txtUserName") as TextBox;
                    txtUserName.Enabled = true;
                }
                else
                {
                    //HtmlTableRow trOldPassword = editedItem.FindControl("trOldPassword") as HtmlTableRow;
                    //trOldPassword.Visible = true;

                    //Label lblPasswordInstructions = editedItem.FindControl("lblPasswordInstructions") as Label;
                    //lblPasswordInstructions.Visible = true;

                    RequiredFieldValidator txtPasswordValidator = editedItem.FindControl("txtPasswordValidator") as RequiredFieldValidator;
                    txtPasswordValidator.Enabled = false;

                    RequiredFieldValidator txtPasswordCompareValidator = editedItem.FindControl("txtPasswordCompareValidator") as RequiredFieldValidator;
                    txtPasswordCompareValidator.Enabled = false;
                }
                #endregion

                #region User Roles
                rcbRole = editedItem.FindControl("ddlRole") as RadComboBox;


                string[] AllRoles = Roles.GetAllRoles();
                string[] roles = new string[AllRoles.GetUpperBound(0) - 1]; //we exclude bregAdmin and VisionLite 

                int roleCounter = 0;
                for (int i = 0; i < AllRoles.GetUpperBound(0); i++) //we exclude bregAdmin and VisionLite 
                {
                    if (AllRoles[i] != "BregAdmin" && AllRoles[i] != "VisionLite")
                    {
                        roles[roleCounter] = AllRoles[i];
                        roleCounter++;
                    }
                }
                rcbRole.DataSource = roles;
                rcbRole.DataBind();



                if (isInsert == false)
                {
                    userRole = editedItem["UserRole"].Text;

                }
                else
                {//PracticeLocationUser
                    userRole = "PracticeLocationUser";
                }
                int index = rcbRole.FindItemIndexByText(userRole);
                rcbRole.SelectedIndex = index;
                #endregion

                #region Locations
                rcbDefaultPracticeLocation = editedItem.FindControl("ddlDefaultLocation") as RadComboBox;

                DataRowView drCurrentRow = editedItem.DataItem as DataRowView;

                BLL.PracticeLocation.PracticeLocation bllPracticeLocation = new BLL.PracticeLocation.PracticeLocation();
                DataTable dtPracticeLocation = bllPracticeLocation.SelectAll(practiceID);

                rcbDefaultPracticeLocation.DataSource = dtPracticeLocation;
                rcbDefaultPracticeLocation.DataTextField = "Name";
                rcbDefaultPracticeLocation.DataValueField = "PracticeLocationID";
                rcbDefaultPracticeLocation.DataBind();

                if (isInsert == false)
                {
                    defaultPracticeLocation = Convert.ToInt32(drCurrentRow["DefaultLocationID"]);
                    if (defaultPracticeLocation > 0)
                    {
                        int LocationIndex = rcbDefaultPracticeLocation.FindItemIndexByValue(defaultPracticeLocation.ToString());
                        rcbDefaultPracticeLocation.SelectedIndex = LocationIndex;
                    }
                }
                #endregion

                #region Default Dispense Search Criteria
                RadComboBox rcbDefaultDispenseSearchCriteria = editedItem.FindControl("ddlDefaultDispenseSearchCriteria") as RadComboBox;

                if (isInsert == false)
                {
                    string defaultDispenseSearchCriteria = drCurrentRow["DefaultDispenseSearchCriteria"].ToString();

                    if (defaultDispenseSearchCriteria != null && defaultDispenseSearchCriteria != string.Empty)
                    {
                        int dDSCIndex = rcbDefaultDispenseSearchCriteria.FindItemIndexByValue(defaultDispenseSearchCriteria);
                        rcbDefaultDispenseSearchCriteria.SelectedIndex = dDSCIndex;
                    }
                }
                #endregion

                #region Location Permissions

                cbPracticeLocationPermissions = editedItem.FindControl("cblPracticeLocationPermissions") as CheckBoxList;

                int editUserID = 0; // editUserID will stay 0 when it is a new record
                if (drCurrentRow != null)
                {
                    editUserID = Convert.ToInt32(drCurrentRow["UserID"]);
                }

                //if EditUserID is 0, this will return true for all locations by default
                DataTable dtPracticeLocationPermission = bllPracticeLocation.SelectUserPermissions(practiceID, editUserID);

                foreach (DataRow row in dtPracticeLocationPermission.Rows)
                {
                    ListItem listItem = new ListItem();
                    listItem.Text = row["Name"].ToString();
                    listItem.Value = row["PracticeLocationID"].ToString();
                    listItem.Selected = Convert.ToBoolean(row["AllowAccess"]);
                    cbPracticeLocationPermissions.Items.Add(listItem);
                }

                #endregion

                #region Show Hide sections for Admin vs Practice User
                //These are the table rows for the dispense, inventory, etc. permissions.
                HtmlTableRow UserOnly1 = editedItem.FindControl("UserOnly1") as HtmlTableRow;
                HtmlTableRow UserOnly2 = editedItem.FindControl("UserOnly2") as HtmlTableRow;
                HtmlTableRow UserOnly3 = editedItem.FindControl("UserOnly3") as HtmlTableRow;
                HtmlTableRow UserOnly4 = editedItem.FindControl("UserOnly4") as HtmlTableRow;
                HtmlTableRow UserOnly5 = editedItem.FindControl("UserOnly5") as HtmlTableRow;
                HtmlTableRow UserOnly6 = editedItem.FindControl("UserOnly6") as HtmlTableRow;
                HtmlTableRow UserOnly7 = editedItem.FindControl("UserOnly7") as HtmlTableRow;
                HtmlTableRow UserPermissions = editedItem.FindControl("UserPermissions") as HtmlTableRow;

                //If 'PracticeAdmin' is selected during this postback, then hide the permissions because admins have permissions to everything
                int adminIndex = rcbRole.FindItemIndexByText("PracticeAdmin");
                if (rcbRole.SelectedIndex == adminIndex)
                {
                    UserOnly1.Style.Add("visibility", "hidden");
                    UserOnly2.Style.Add("visibility", "hidden");
                    UserOnly3.Style.Add("visibility", "hidden");
                    UserOnly4.Style.Add("visibility", "hidden");
                    UserOnly5.Style.Add("visibility", "hidden");
                    UserOnly6.Style.Add("visibility", "hidden");
                    UserOnly7.Style.Add("visibility", "hidden");
                    UserPermissions.Style.Add("visibility", "hidden");
                }

                //Get ehe clientIDs of the permissions rows so we can hide/show them with javascript in the page as the user changes roles
                string javascript = "<script type='text/javascript'>";
                javascript += string.Format("userOnly1ClientID = '{0}';", UserOnly1.ClientID);
                javascript += string.Format("userOnly2ClientID = '{0}';", UserOnly2.ClientID);
                javascript += string.Format("userOnly3ClientID = '{0}';", UserOnly3.ClientID);
                javascript += string.Format("userOnly4ClientID = '{0}';", UserOnly4.ClientID);
                javascript += string.Format("userOnly5ClientID = '{0}';", UserOnly5.ClientID);
                javascript += string.Format("userOnly6ClientID = '{0}';", UserOnly6.ClientID);
                javascript += string.Format("userOnly7ClientID = '{0}';", UserOnly7.ClientID);
                javascript += string.Format("userPermissionsClientID = '{0}';", UserPermissions.ClientID);
                javascript += "</script>";
                //grdPracticeUser.Controls.Add(new LiteralControl(javascript));
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "EditScript", javascript);
                if (this.Page is PracticeSetup.PracticeSetup)
                {
                    Telerik.Web.UI.RadScriptManager.RegisterClientScriptBlock(this.Page, this.practiceID.GetType(), "EditScript", javascript, false);
                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "EditScript", javascript);
                }
          
                #endregion

            }

            if (e.Item is GridDataItem)
            {
                var item = (GridDataItem) e.Item;
                var unverifiedUser = false;
                bool IsActive = Convert.ToBoolean(item["IsActive"].Text);
                if (IsActive == false)
                {
                    item.BackColor = System.Drawing.Color.LightGray;
                    unverifiedUser = true;
                }
                Telerik.Web.UI.GridDataItem dataItem = e.Item as GridDataItem;

                if (unverifiedUser)
                    (dataItem["Activate"].Controls[0] as ImageButton).ImageUrl = "~/App_Themes/Breg/images/Common/unverified-user.png";

                DataRowView drvUsers = e.Item.DataItem as DataRowView;
                if (drvUsers != null)
                {
                    string imageUrl = "~/App_Themes/Breg/images/Common/lock-open.png";
                    bool isLockedOut = drvUsers["IsLockedOut"] is System.DBNull ? false : Convert.ToBoolean(drvUsers["IsLockedOut"]);
                    if (isLockedOut == true)
                    {
                        imageUrl = "~/App_Themes/Breg/images/Common/lock.png";
                    }


                    (dataItem["Unlock"].Controls[0] as ImageButton).ImageUrl = imageUrl;
                }
            }

        }

        protected void grdPracticeUser_DeleteCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {

            Telerik.Web.UI.GridEditableItem editedItem = e.Item as Telerik.Web.UI.GridEditableItem;

            //Label lblStatus = editedItem.FindControl("lblStatus") as Label;
            //lblStatus.Text = "";

            string userName = editedItem["UserName"].Text;
            int deleteUserID = Convert.ToInt32(editedItem["UserID"].Text);
            //string userName = txtUserName.Text;

            MembershipUser editMembershipUser = null;
            editMembershipUser = Membership.GetUser(userName);
            editMembershipUser.IsApproved = false;

            BLL.Practice.User editUser = new BLL.Practice.User();


            Membership.UpdateUser(editMembershipUser);
            editUser.DeleteUser(practiceID, deleteUserID);


        }


        protected void grdPracticeUser_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {
            string errString = "";

            Telerik.Web.UI.GridEditableItem editedItem = e.Item as Telerik.Web.UI.GridEditableItem;

            bool isInsert = editedItem.Edit; //this = true for a new item, false for an existing item.

            Label lblStatus = editedItem.FindControl("lblStatus") as Label;
            lblStatus.Text = "";

            TextBox txtUserName = editedItem.FindControl("txtUserName") as TextBox;
            string userName = txtUserName.Text;

            string password = "";
            string oldPassword = "";
            //if (isInsert == false)
            //{
            //    TextBox txtOldPassword = editedItem.FindControl("txtOldPassword") as TextBox;
            //    oldPassword = txtOldPassword.Text;
            //    if (oldPassword != "")
            //    {
            //        bool validUsernameAndPassword = Membership.ValidateUser(userName, oldPassword);
            //        if (validUsernameAndPassword == false)
            //        {
            //            errString = "The username and old password combination are not valid";
            //            e.Canceled = true;
            //            goto ErrorLabel;
            //        }
            //    }
            //}

            TextBox txtPassword = editedItem.FindControl("txtPassword") as TextBox;
            TextBox txtPasswordCompare = editedItem.FindControl("txtPasswordCompare") as TextBox;
            if (txtPassword.Text == txtPasswordCompare.Text)
            {
                password = txtPassword.Text;
            }
            else
            {
                errString = "The password and compare password do not match";
                e.Canceled = true;
                goto ErrorLabel;
            }


            TextBox txtEmail = editedItem.FindControl("txtEmail") as TextBox;
            string email = txtEmail.Text;


            RadComboBox rcbRole = editedItem.FindControl("ddlRole") as RadComboBox;
            string role = rcbRole.Text;

            bool showProductInfo = true;
            bool allowDispense = true;
            bool allowInventory = true;
            bool allowCheckIn = true;
            bool allowReports = true;
            bool allowCart = true;
            bool allowDispenseModification = true;
            bool allowPracticeUserOrderWithoutApproval = false;
            bool scrollEntirePage = false;
            bool dispenseQueueSortDescending = false;
            bool allowInventoryClose = false;

            CheckBox chkShowProductInfo = editedItem.FindControl("chkShowProductInfo") as CheckBox;
            showProductInfo = chkShowProductInfo.Checked;

            CheckBox chkScrollEntirePage = editedItem.FindControl("chkScrollEntirePage") as CheckBox;
            scrollEntirePage = chkScrollEntirePage.Checked;

            CheckBox chkDispenseQueueSortDescending = editedItem.FindControl("chkDispenseQueueSortDescending") as CheckBox;
            dispenseQueueSortDescending = chkDispenseQueueSortDescending.Checked;

            CheckBox chkAllowInventoryClose = editedItem.FindControl("chkAllowInventoryClose") as CheckBox;
            allowInventoryClose = chkAllowInventoryClose.Checked;

            if (role == "PracticeLocationUser")
            {
                CheckBox chkAllowDispense = editedItem.FindControl("chkAllowDispense") as CheckBox;
                allowDispense = chkAllowDispense.Checked;

                CheckBox chkAllowInventory = editedItem.FindControl("chkAllowInventory") as CheckBox;
                allowInventory = chkAllowInventory.Checked;

                CheckBox chkAllowCheckIn = editedItem.FindControl("chkAllowCheckIn") as CheckBox;
                allowCheckIn = chkAllowCheckIn.Checked;

                CheckBox chkAllowReports = editedItem.FindControl("chkAllowReports") as CheckBox;
                allowReports = chkAllowReports.Checked;

                CheckBox chkAllowCart = editedItem.FindControl("chkAllowCart") as CheckBox;
                allowCart = chkAllowCart.Checked;

                CheckBox chkAllowDispenseModification = editedItem.FindControl("chkAllowDispenseModification") as CheckBox;
                allowDispenseModification = chkAllowDispenseModification.Checked;

                CheckBox chkAllowPracticeUserOrderWithoutApproval = editedItem.FindControl("chkAllowPracticeUserOrderWithoutApproval") as CheckBox;
                allowPracticeUserOrderWithoutApproval = chkAllowPracticeUserOrderWithoutApproval.Checked;
            }


            RadComboBox rcbDefaultLocation = editedItem.FindControl("ddlDefaultLocation") as RadComboBox;
            string defaultLocation = rcbDefaultLocation.Text;
            int defaultLocationID = Convert.ToInt32(rcbDefaultLocation.SelectedValue);

            Telerik.Web.UI.RadNumericTextBox txtDefaultRecordsPerPage = editedItem.FindControl("txtDefaultRecordsPerPage") as Telerik.Web.UI.RadNumericTextBox;
            string defaultRecordsPerPage = txtDefaultRecordsPerPage.Text;
            int defaultRecordsPerPageInt = Convert.ToInt32(txtDefaultRecordsPerPage.Text);

            RadComboBox rcbDefaultDispenseSearchCriteria = editedItem.FindControl("ddlDefaultDispenseSearchCriteria") as RadComboBox;
            string defaultDispenseSearchCriteria = rcbDefaultDispenseSearchCriteria.Text;
            string defaultDispenseSearchCriteriaValue = rcbDefaultDispenseSearchCriteria.SelectedValue;

            //isInsert
            string oldEmail = "";
            int editUserID = 0;
            string oldRole = "";
            bool oldShowProductInfo = false;
            bool oldAllowDispense = false;
            bool oldAllowInventory = false;
            bool oldAllowCheckIn = false;
            bool oldAllowReports = false;
            bool oldAllowCart = false;
            bool oldAllowDispenseModification = false;
            bool oldAllowPracticeUserOrderWithoutApproval = false;
            bool oldScrollEntirePage = false;
            bool oldDispenseQueueSortDescending = false;
            bool oldAllowInventoryClose = false;
            int oldDefaultLocationID = 0;
            int oldDefaultRecordsPerPage = 0;
            string oldDefaultDispenseSearchCriteria = "";
            if (isInsert == false)
            {
                oldEmail = editedItem["Email"].Text;
                editUserID = Convert.ToInt32(editedItem["UserID"].Text);
                oldRole = editedItem["UserRole"].Text;

                oldShowProductInfo = Convert.ToBoolean(editedItem["ShowProductInfo"].Text);

                oldAllowDispense = Convert.ToBoolean(editedItem["AllowDispense"].Text);
                oldAllowInventory = Convert.ToBoolean(editedItem["AllowInventory"].Text);
                oldAllowCheckIn = Convert.ToBoolean(editedItem["AllowCheckIn"].Text);
                oldAllowReports = Convert.ToBoolean(editedItem["AllowReports"].Text);
                oldAllowCart = Convert.ToBoolean(editedItem["AllowCart"].Text);
                oldAllowDispenseModification = Convert.ToBoolean(editedItem["AllowDispenseModification"].Text);
                oldAllowPracticeUserOrderWithoutApproval = Convert.ToBoolean(editedItem["AllowPracticeUserOrderWithoutApproval"].Text);
                oldScrollEntirePage = Convert.ToBoolean(editedItem["ScrollEntirePage"].Text);
                oldDispenseQueueSortDescending = Convert.ToBoolean(editedItem["DispenseQueueSortDescending"].Text);
                oldAllowInventoryClose = Convert.ToBoolean(editedItem["AllowInventoryClose"].Text);
                oldDefaultLocationID = Convert.ToInt32(editedItem["DefaultLocationID"].Text);
                oldDefaultRecordsPerPage = Convert.ToInt32(editedItem["DefaultRecordsPerPage"].Text);
                oldDefaultDispenseSearchCriteria = editedItem["DefaultDispenseSearchCriteria"].Text;
            }

            //Validate
            if (isInsert == true)
            {
                if (userName == "")
                {
                    errString = "Please choose an User Name";
                    e.Canceled = true;
                    goto ErrorLabel;
                }
                else if (Membership.GetUser(userName) != null)
                {
                    errString = "User Name already exists, please choose another User Name";
                    e.Canceled = true;
                    goto ErrorLabel;
                }
            }
            else
            {

            }
            CheckBoxList cblPracticeLocationPermissions = editedItem.FindControl("cblPracticeLocationPermissions") as CheckBoxList;
            foreach (ListItem practiceLocationItem in cblPracticeLocationPermissions.Items)
            {
                if (Convert.ToInt32(practiceLocationItem.Value) == defaultLocationID)
                {
                    if (practiceLocationItem.Selected == false)
                    {
                        errString = "The User is not allowed to access the Default Location.  Please change the default location or give the user access to that location";
                        e.Canceled = true;
                        goto ErrorLabel;
                    }
                }
            }


            //Save
            BLL.Practice.User editUser = new BLL.Practice.User();
            MembershipUser editMembershipUser = null;
            if (isInsert == true)
            {
                try
                {
                    editMembershipUser = Membership.CreateUser(userName, password);
                    //We add to the bregvision user's table here
                    editUserID = editUser.InsertUser(practiceID, userName, userID);
                }
                catch (System.ArgumentException exa)
                {
                    switch (exa.Message)
                    {
                        case "The length of parameter 'newPassword' needs to be greater or equal to '7'.":
                            errString = "The new password must contain at least 7 characters with one of the characters alpha-numeric";
                            break;
                        case "Non alpha numeric characters in 'newPassword' needs to be greater than or equal to '1'.":
                            errString = "The new password must contain at least 7 characters with one of the characters alpha-numeric";
                            break;
                        default:
                            errString = exa.Message;
                            break;
                    }
                    e.Canceled = true;
                    goto ErrorLabel;
                }
                catch (Exception ex)
                {
                    string errMessage = "The new password must contain at least 7 characters with one of the characters alpha-numeric";

                    if (ex.Message.Contains("password") && ex.Message.Contains("invalid"))
                    {
                        errString = errMessage;
                    }
                    else
                    {
                        errString = ex.Message;
                    }
                    e.Canceled = true;
                    goto ErrorLabel;
                }
            }
            else
            {
                editMembershipUser = Membership.GetUser(userName);
                if (password != "")
                {
                    try
                    {
                        editMembershipUser.ChangePassword(editMembershipUser.ResetPassword(), password);
                    }
                    catch (System.ArgumentException exa)
                    {
                        switch (exa.Message)
                        {
                            case "The length of parameter 'newPassword' needs to be greater or equal to '7'.":
                                errString = "The new password must contain at least 7 characters with one of the characters non alpha-numeric";
                                break;
                            case "Non alpha numeric characters in 'newPassword' needs to be greater than or equal to '1'.":
                                errString = "The new password must contain at least 7 characters with one of the characters non alpha-numeric";
                                break;
                            default:
                                errString = exa.Message;
                                break;
                        }
                        e.Canceled = true;
                        goto ErrorLabel;
                    }
                    catch (Exception ex)
                    {
                        errString = ex.Message;
                        e.Canceled = true;
                        goto ErrorLabel;
                    }
                }
            }

            if (email != oldEmail)
            {

                editMembershipUser.Email = email;
                //editMembershipUser.GetPassword();
                Membership.UpdateUser(editMembershipUser);
            }

            if (role != oldRole && oldRole != "BregAdmin")
            {
                // in breg vision we only allow one role so remove them from the old roles and add them to the new role
                string[] AllRoles = Roles.GetAllRoles();
                foreach (string roleName in AllRoles)
                {
                    if (Roles.IsUserInRole(userName, roleName))
                    {
                        Roles.RemoveUserFromRole(userName, roleName);
                    }
                }
                Roles.AddUserToRole(userName, role);
                if (BLL.Website.IsVisionExpress())
                {
                    Roles.AddUserToRole(userName, "VisionLite");
                }
            }

            if (editUserID != 0)
            {
                //compare the old values to the new values and only update if any of them are different
                if (showProductInfo != oldShowProductInfo
                    || allowDispense != oldAllowDispense
                    || allowInventory != oldAllowInventory
                    || allowCheckIn != oldAllowCheckIn
                    || allowReports != oldAllowReports
                    || allowCart != oldAllowCart
                    || defaultLocationID != oldDefaultLocationID
                    || allowDispenseModification != oldAllowDispenseModification
                    || allowPracticeUserOrderWithoutApproval != oldAllowPracticeUserOrderWithoutApproval
                    || scrollEntirePage != oldScrollEntirePage
                    || dispenseQueueSortDescending != oldDispenseQueueSortDescending
                    || allowInventoryClose != oldAllowInventoryClose
                    || defaultRecordsPerPageInt != oldDefaultRecordsPerPage
                    || defaultDispenseSearchCriteriaValue != oldDefaultDispenseSearchCriteria)
                {
                    //save to the breg Vision User Table
                    editUser.UpdateDefaultPreferences(
                                                        practiceID, 
                                                        editUserID, 
                                                        showProductInfo, 
                                                        allowDispense,
                                                        allowInventory,
                                                        allowCheckIn, 
                                                        allowReports, 
                                                        allowCart, 
                                                        allowDispenseModification, 
                                                        defaultLocationID,
                                                        defaultRecordsPerPageInt, 
                                                        defaultDispenseSearchCriteriaValue, 
                                                        scrollEntirePage, 
                                                        dispenseQueueSortDescending, 
                                                        allowPracticeUserOrderWithoutApproval,
                                                        allowInventoryClose: allowInventoryClose);
                }

                //Save to the User_PracticeLocation_Permissions table
                foreach (ListItem practiceLocationItem in cblPracticeLocationPermissions.Items)
                {
                    string locationName = practiceLocationItem.Text;
                    int locationID = Convert.ToInt32(practiceLocationItem.Value);
                    bool locationAllowed = true;
                    if (role == "PracticeLocationUser")
                    {
                        locationAllowed = practiceLocationItem.Selected;
                    }
                    BLL.PracticeLocation.PracticeLocation updatePracticeLocation = new BLL.PracticeLocation.PracticeLocation();
                    updatePracticeLocation.SetUserPermissions(locationID, editUserID, locationAllowed);
                }
            }
            else
            {
                throw new ApplicationException("The editUserID is 0.  There should be a updateUserID when calling 'UpdateDefaultPreferences'");
            }




        ErrorLabel:
            if (e.Canceled == true)
            {
                lblStatus.Visible = true;
                lblStatus.Text = errString;
                lblStatus.ForeColor = Color.Red;
                lblStatus.CssClass = "WarningMessage";
            }
        }

        protected void grdPracticeUser_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
        }

        protected void grdPracticeUser_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable)
            {
                DataTable dtUsers = GetPracticeLocationUserFullNames(practiceID);

                // patch the data to limit the DefaultRecordsPerPage to a maximum of 100 -hchan 11/18/2010
                foreach (DataRow r in dtUsers.Rows)
                {
                    if ((r["DefaultRecordsPerPage"] is Int32) && ((Int32)r["DefaultRecordsPerPage"] > 100))
                        r["DefaultRecordsPerPage"] = 100;
                }

                grdPracticeUser.DataSource = dtUsers;
            }
        }



        private DataTable GetPracticeLocationUserFullNames(int practiceID)
        {
            //throw new Exception("The method or operation is not implemented.");
            BLL.Practice.User bllPU = new BLL.Practice.User();
            DataTable dtPracticeUser = bllPU.SelectAllUserLogins(practiceID); // .SelectAllUserFullNames();
            return dtPracticeUser;
        }

        protected eSetupStatus ValidateSetupData()
        {
            return eSetupStatus.Complete;
        }

        /// <summary>
        /// IVisionWizardControl interface save.  Called by wizard control on tab change
        /// </summary>
        public void Save()
        {
            //do nothing.  Grid rows are saved individually

            SaveStatus(practiceID, -1, this.ToString(), ValidateSetupData());
        }
    }
}