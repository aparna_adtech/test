﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using BregVision.Admin.PracticeSetup;
using ClassLibrary.DAL;
using Telerik.Web.UI;

namespace BregVision.Admin.UserControls
{

    public partial class PracticeProducts : PracticeSetupBase, IVisionWizardControl
    {
        //^()*([\s?])*([\w?\$?\.?\s?]{1,4})+([\s?])*([,]([\s?])*([\w?\$?\.?\s?]{1,4})+([\s?])*)*$    //this should work no leading commas or trailing commas alphanumberic with periods please.
        //important//DELETE FROM ProductHCPCs//WHERE productHCPCSID IN (1,7)

        private bool _isPromote = false;
        bool clearProductCaption = false;   //Clears Message to User.
        bool isOptionC;             //Alternative
        bool blnCopy;               //copy
        bool blnHCPCS;      // Set in config.
        bool blnAppendToLabel;
        bool blnRowNumber;
        int intPageSize;
        int intPageIndex;
        int intItemIndex;
        int intRealIndex;

        bool isInEdit = false;

        /// <summary>
        /// IVisionWizardControl Interface.
        /// </summary>
        public bool NeedsInitialBind { get; set; }


        public int? PracticeCatalogSupplierBrandID
        {
            get
            {
                if (ViewState["SelectedPracticeCatalogSupplierBrandID"] != null)
                { return Convert.ToInt32(ViewState["PracticeCatalogSupplierBrandID"]); }
                else { return null; }
            }
            set { ViewState["PracticeCatalogSupplierBrandID"] = value; }
        }

        public int? SelectedProductID
        {
            get
            {
                if (ViewState["SelectedProductID"] != null)
                { return Convert.ToInt32(ViewState["SelectedProductID"]); }
                else { return null; }
            }
            set { ViewState["SelectedProductID"] = value; }
        }

        public int? SelectedPage
        {
            get
            {
                if (ViewState["SelectedPage"] != null)
                { return Convert.ToInt32(ViewState["SelectedPage"]); }
                else { return null; }
            }
            set { ViewState["SelectedPage"] = value; }
        }

        public int? NeedToSelectPage
        {
            get
            {
                if (ViewState["NeedToSelectPage"] != null)
                { return Convert.ToInt32(ViewState["NeedToSelectPage"]); }
                else { return null; }
            }
            set { ViewState["NeedToSelectPage"] = value; }
        }

        public string TableViewName
        {
            get
            {
                if (ViewState["TableViewName"] != null && ViewState["TableViewName"].ToString() != "")
                { return ViewState["TableViewName"].ToString(); }
                else { return ""; }
            }
            set { ViewState["TableViewName"] = value; }
        }



        protected void SetMode(int? selectedProductID, int? selectedPage, int? needToSelectPage)
        {
            SelectedProductID = selectedProductID;
            SelectedPage = selectedPage;
            NeedToSelectPage = needToSelectPage;
        }

        //  Greg: After testing please add this to the .aspx page to avoid backspacing and creating dirty data.
        //<body onload="history.forward()">


        protected void Page_Load(object sender, EventArgs e)
        {

            SetOptions();

            ctlMasterCatalogAdmin.Visible =  Page.User.IsInRole("BregAdmin");

            if (Page.User.IsInRole("BregAdmin"))
            {
                if (RadGrid1.MasterTableView.HasDetailTables)
                {
                    GridColumn promoteColumn = RadGrid1.MasterTableView.DetailTables[0].GetColumnSafe("Promote");
                    if (promoteColumn != null)
                    {
                        promoteColumn.Visible = true;
                    }

                    if (RadGrid1.MasterTableView.DetailTables.Count > 1 && RadGrid1.MasterTableView.DetailTables[1].HasDetailTables)
                    {
                        GridColumn promoteColumn1 = RadGrid1.MasterTableView.DetailTables[1].DetailTables[0].GetColumnSafe("Promote");
                        if (promoteColumn1 != null)
                        {
                            promoteColumn1.Visible = true;
                        }
                    }
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (isInEdit == false)
            {
                RadGrid1.Rebind();
            }
        }


        protected void AppendToLabel(string message)
        {
            if (blnAppendToLabel)
            {
                lblCommandName.Text = lblCommandName.Text + "<br/>" + message;
            }
        }

        protected void SetOptions()
        {
            isOptionC = ((ConfigurationManager.AppSettings["IsVendorGridOptionC"] != null)
                ? Convert.ToBoolean(ConfigurationManager.AppSettings["IsVendorGridOptionC"].ToString()) : false);
            blnCopy = ((ConfigurationManager.AppSettings["IsVendorGridCopy"] != null)
                ? Convert.ToBoolean(ConfigurationManager.AppSettings["IsVendorGridCopy"].ToString()) : false);
            blnHCPCS = ((ConfigurationManager.AppSettings["IsVendorGridHCPC"] != null)
                ? Convert.ToBoolean(ConfigurationManager.AppSettings["IsVendorGridHCPC"].ToString()) : false);
            blnAppendToLabel = ((ConfigurationManager.AppSettings["IsVendorGridDebugLabel"] != null)
                ? Convert.ToBoolean(ConfigurationManager.AppSettings["IsVendorGridDebugLabel"].ToString()) : false);
            blnRowNumber = ((ConfigurationManager.AppSettings["IsVendorGridRowNumber"] != null)
                ? Convert.ToBoolean(ConfigurationManager.AppSettings["IsVendorGridRowNumber"].ToString()) : false);

        }

        protected void RadGrid1_PreRender(object sender, EventArgs e)
        {
            int firstNested = -1;
            if (TableViewName != null && TableViewName != "")
            {
                if (TableViewName == "gtvSupplierProduct")
                { firstNested = 0; }

                if (TableViewName == "gtvProduct")
                { firstNested = 1; }

                //firstNested = (TableViewName == "gtvSupplierProduct")? 0 : 1;
            }
            int vendorIndex = -1;
            int brandIndex = -1;

            foreach (GridDataItem vendorItem in RadGrid1.MasterTableView.Items)
            {
                if (vendorItem.Expanded)
                {
                    vendorIndex = vendorItem.ItemIndex;
                    AppendToLabel("vendorItem.ItemIndex is :" + vendorItem.ItemIndex.ToString());
                }
            }
            //VARY this based on the firstNested.
            if (firstNested == 1)
            {
                if (vendorIndex != -1)
                {
                    // get next expanded item.
                    foreach (GridDataItem brandItem in (RadGrid1.MasterTableView.Items[vendorIndex] as GridDataItem).ChildItem.NestedTableViews[firstNested].Items)  //hard code 1 for this.
                    {
                        if (brandItem.Expanded)
                        {
                            brandIndex = brandItem.ItemIndex;
                            AppendToLabel("brandItem.ItemIndex is :" + brandItem.ItemIndex.ToString());
                        }
                    }
                }
                if (brandIndex != -1)
                {
                    //int brandRealIndex = (2 * brandIndex) + 1;
                    GridDataItem vendorGridDataItem = (RadGrid1.MasterTableView.Items[vendorIndex] as GridDataItem);
                    //Greg: this blow up on the integration only.
                    //int firstNestedCount = vendorGridDataItem.ChildItem.NestedTableViews.Count();
                    GridTableView gtvBrand = vendorGridDataItem.ChildItem.NestedTableViews[firstNested];  //hard code 1 for this.
                    //if (gtvSupplier.HasDetailTables && gtvSupplier.Items.Count > 0 && gtvSupplier.Items[0].HasChildItems)
                    //{

                    SetGTVProduct(gtvBrand, brandIndex);
                    //}
                }
            }
            if (firstNested == 0)
            {
                if (vendorIndex != -1)
                {
                    GridTableView gtvVendor = RadGrid1.MasterTableView;
                    SetGTVProduct(gtvVendor, vendorIndex);
                }
            }
        }

        protected void SetGTVProduct(GridTableView gtvBrand, int brandIndex)
        {
            try
            {
                GridTableView gtvProduct = (gtvBrand.Items[brandIndex] as GridDataItem).ChildItem.NestedTableViews[0];  //hard code 0 ALWAYS for this.
                //AppendToLabel("firstNestedCount is : " + firstNestedCount.ToString()); 
                AppendToLabel("gtvProduct.Name is :" + gtvProduct.Name.ToString());

                if (SelectedProductID == null)
                {
                    if (SelectedPage != null && NeedToSelectPage != null)
                    {
                        try
                        {
                            gtvProduct.CurrentPageIndex = Convert.ToInt32(SelectedPage);
                            gtvProduct.Rebind();
                        }
                        catch //(Exception ex)
                        {
                            //string errr = ex.Message; // throw;
                        }

                    }
                }
                else
                {
                    if (SelectedPage != null)
                    {
                        try
                        {
                            gtvProduct.CurrentPageIndex = Convert.ToInt32(SelectedPage);
                        }
                        catch// (Exception ex)
                        {
                            //string errr = ex.Message; // throw;
                        }
                    }
                    else
                    {
                        if (NeedToSelectPage == 1)
                        {
                            try
                            {
                                SetProperPage(gtvProduct);
                            }
                            catch //(Exception ex)
                            {
                                //string errr = ex.Message; // throw;
                            }
                        }
                    }
                }

            }
            catch //(Exception ex)
            {
                // int uu = 0;
            }
        }

        //Display the page containing the new record.
        protected void SetProperPage(GridTableView gtvProducts)
        {
            int isNewRecordFound = -1;

            if (gtvProducts.Items.Count > 0)
            {
                int dc = gtvProducts.Items.Count;

                //Loop through the pages, look for the page with the selected record.
                for (int i = 0; i < gtvProducts.PageCount; i++)
                {
                    // SetPageCount to i
                    gtvProducts.CurrentPageIndex = i;

                    //may use variable here?
                    //ViewState["SettingPageToSelect"] = true;
                    gtvProducts.Rebind();

                    //Check for newly inserted record
                    foreach (GridDataItem dataItem in gtvProducts.Items)
                    {
                        // search the new record, if reset modes attributes.
                        if (dataItem["ThirdPartyProductID"].Text.ToString() == SelectedProductID.ToString())
                        {
                            //isNewRecordFound = 1;
                            SelectedProductID = null;
                            NeedToSelectPage = null;
                            return;
                        }
                    }
                }
                if (isNewRecordFound == -1)
                {
                    //record is not found so kill viewState
                    SelectedProductID = null;
                    NeedToSelectPage = null;
                }
            }
        }
    
        protected void RadGrid1_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "Promote")
            {
                RadGrid grid = source as RadGrid;
                e.Item.Edit = true;
                _isPromote = true;
                e.Item.OwnerTableView.Rebind();  //grid.Rebind();

                //We need to find the correct editform template here and then call the following method so we can show the dropdowns
                //This happens in item created once we set _isPromote, e.item.edit, and call a rebind
            }


            //lblCommandName.Text = lblCommandName.Text + "<br/>" + e.CommandName.ToString();
            AppendToLabel("RadGrid1_ItemCommand  RadGrid1.CommandName = " + e.CommandName.ToString());
            isInEdit = true;
            if (e.CommandName == RadGrid.ExpandCollapseCommandName)
            {
                
                foreach (GridItem item in e.Item.OwnerTableView.Items)
                {
                    if (item.Expanded && item != e.Item)
                    {
                        item.Expanded = false;
                        AppendToLabel("collasped:  " + e.Item.OwnerTableView.Name.ToString());
                    }
                    if (item.Expanded && item == e.Item)
                    {
                        if (e.Item.OwnerTableView.Name == "gtvProduct"
                            || e.Item.OwnerTableView.Name == "gtvSupplierProduct")
                        {
                            e.Item.OwnerTableView.Caption = "";
                            clearProductCaption = true;
                            SelectedProductID = null;
                            NeedToSelectPage = 1;
                            SelectedPage = null;
                            //Session["OwnerTableView"] = e.Item.OwnerTableView;
                            //        //AppendToLabel("set to:  " + e.Item.OwnerTableView.Name.ToString());
                        }

                    }

                }
            }


            ////  If the gtvSupplier is being expanded or collapsed than clear the user message.
            //if (e.CommandName == "ExpandCollapse" && (e.Item.OwnerTableView.Name == "gtvSupplier" || e.Item.OwnerTableView.Name == "gtvVendor"))
            //{
            //    clearProductCaption = true;
            //    e.Item.OwnerTableView.DetailTables[0].Caption = "";
            //}


            if (e.Item.OwnerTableView.Name == "gtvProduct" || e.Item.OwnerTableView.Name == "gtvSupplierProduct")
            {
                //s//tring pcsbID = parentItem.OwnerTableView.DataKeyValues[parentItem.ItemIndex]["PracticeCatalogSupplierBrandID"].ToString();

                GridDataItem parentItem = (GridDataItem)(e.Item.OwnerTableView.ParentItem);
                if (parentItem != null)
                {
                    // Get the Practice Catalog Supplier Brand ID from the parent Grid Table View.
                    string pcsbID = parentItem.OwnerTableView.DataKeyValues[parentItem.ItemIndex]["PracticeCatalogSupplierBrandID"].ToString();
                    PracticeCatalogSupplierBrandID = Convert.ToInt32(pcsbID);

                }



                //string SElectedPCSBID = e.Item.OwnerTableView.ParentItem["PracticeCatalogSupplierBrandID"].ToString();
                TableViewName = e.Item.OwnerTableView.Name.ToString();
                // prevent add and edit from displaying at the same time
                RadGrid grid = (source as RadGrid);
                //  If the insert command is being called, clear all edit items.

                // if command is to refresh the grid than set than SelectedPage == null and NeedSelectedPage = null
                if (e.CommandName == RadGrid.RebindGridCommandName)
                {
                    SelectedPage = e.Item.OwnerTableView.CurrentPageIndex;  //null

                    //JB NeedToSelectPage = 1;                                   // null;
                    //SelectedPage = null;
                    //NeedSelectedPage = 1;
                    NeedToSelectPage = 1;
                    SelectedProductID = null;

                    e.Item.OwnerTableView.IsItemInserted = false;
                    e.Item.OwnerTableView.ClearChildEditItems();
                    clearProductCaption = true;
                    e.Item.OwnerTableView.Caption = "";
                    return;
                }

                // if command is to move the page than SelectedPage == null and NeedSelectedPage = null
                if (e.CommandName == RadGrid.PageCommandName)
                {
                    SelectedPage = null;
                    NeedToSelectPage = null;
                    e.Item.OwnerTableView.IsItemInserted = false;
                    return;
                }



                if (e.CommandName == RadGrid.InitInsertCommandName)  //Insert command.
                {
                    e.Item.OwnerTableView.ClearEditItems();
                    e.Item.OwnerTableView.Caption = "";

                }
                //  If the update (edit) command is being called, clear all insert items.
                if (e.CommandName == RadGrid.EditCommandName)
                {
                    e.Item.OwnerTableView.IsItemInserted = false;
                    e.Item.OwnerTableView.Caption = "";
                }


                // If "CopyProductFromThisProduct" button was clicked.
                if (e.CommandName == "InitInsert" && e.CommandArgument.ToString() == "Copy")   //Insert new product based on existing product.
                { //*******************Need to check these code u= 11/30/2010
                    e.Item.OwnerTableView.ClearEditItems();
                    e.Item.OwnerTableView.Caption = "";
                    //Reset Product ID
                    SelectedProductID = null;   //ViewState.Remove("SelectedItem");  
                    // cancel the default operation
                    e.Canceled = true;
                    //Prepare an IDictionary with the predefined values
                    System.Collections.Specialized.ListDictionary newValues = new System.Collections.Specialized.ListDictionary();

                    // Retain the same page.
                    //  Get the current page, and display the same page where insert was clicked.
                    SelectedPage = e.Item.OwnerTableView.CurrentPageIndex;
                    NeedToSelectPage = 1;
                    //ViewState["curPageIndex"] = e.Item.OwnerTableView.CurrentPageIndex;


                    GridDataItem dataItem = e.Item as GridDataItem;
                    //Get archetype record values and save to viewstate in order to copy to new record.
                    SetViewStateForCopyToNewRow(dataItem, ref newValues);
                    // Display copied item in new Insert template field.

                    e.Item.OwnerTableView.InsertItem(newValues);
                    //Retrieve the save page from above.
                    //e.Item.OwnerTableView.CurrentPageIndex = int.Parse(ViewState["curPageIndex"].ToString());


                    //Set correct page and Rebind the gtvProduct grid table view? To set the page?
                    // Is this neccessary??
                    e.Item.OwnerTableView.CurrentPageIndex = Convert.ToInt32(SelectedPage);
                    e.Item.OwnerTableView.Rebind();
                }

                // If Insert button was clicked, keep the current page.
                if (e.CommandName == RadGrid.InitInsertCommandName && e.CommandArgument.ToString() != "Copy")  //Add New Record
                {
                    e.Item.OwnerTableView.ClearEditItems();
                    e.Item.OwnerTableView.Caption = "";
                    e.Canceled = true;
                    //Reset Product ID
                    SelectedProductID = null;   //ViewState.Remove("SelectedItem");  
                    NeedToSelectPage = 1;


                    SelectedPage = e.Item.OwnerTableView.CurrentPageIndex;
                    NeedToSelectPage = 1;
                    //ViewState["curPageIndex"] = e.Item.OwnerTableView.CurrentPageIndex;
                    e.Item.OwnerTableView.InsertItem();


                    e.Item.OwnerTableView.CurrentPageIndex = Convert.ToInt32(SelectedPage);  //e.Item.OwnerTableView.CurrentPageIndex = int.Parse(ViewState["curPageIndex"].ToString());


                    e.Item.OwnerTableView.Rebind();

                }

                // If Cancle button was clicked, keep the current page.
                if (e.CommandName == RadGrid.CancelCommandName && e.Item.OwnerTableView.IsItemInserted)
                {
                    try
                    {
                        SelectedPage = e.Item.OwnerTableView.CurrentPageIndex;
                        NeedToSelectPage = 1;
                        //ViewState["curPageIndex"] = e.Item.OwnerTableView.CurrentPageIndex;
                        //ViewState.Remove("SelectedItem");
                        e.Item.OwnerTableView.CurrentPageIndex = Convert.ToInt32(SelectedPage); //e.Item.OwnerTableView.CurrentPageIndex = int.Parse(ViewState["curPageIndex"].ToString());
                        //e.Item.OwnerTableView.IsItemInserted = false;
                        e.Item.OwnerTableView.Rebind();
                    }
                    catch (Exception ex)
                    {
                        string strEMessage = ex.Message.ToString();
                    }
                }
            }
        }

        protected void RadGrid1_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
            {
                if (_isPromote == true)
                {
                    GridEditFormItem form = e.Item as GridEditFormItem;
                    if (form != null)
                    {
                        ShowPromoteItems(form as Control);
                    }
                }
            }
 
        }


        private static void ShowPromoteItems(Control parent)
        {
            //GridEditFormItem form = ((GridDataItem)grid.EditItems[itemIndex]).EditFormItem;
            (parent.FindControl("trPromote") as HtmlTableRow).Visible = true;
            (parent.FindControl("trPromoteHeader") as HtmlTableRow).Visible = true;
            (parent.FindControl("btnUpdate") as Button).CommandArgument = "Promote";
        }

        protected void ddlSubCategories_OnLoad(object sender, EventArgs e)
        {
            Control parent = (sender as DropDownList).Parent;
            LoadDropdownsForPromoteProduct(parent);
        }

        private static void LoadDropdownsForPromoteProduct(Control form)
        {
            DropDownList ddlSuppliers = form.FindControl("ddlSuppliers") as DropDownList;
            DropDownList ddlCategories = form.FindControl("ddlCategories") as DropDownList;
            DropDownList ddlSubCategories = form.FindControl("ddlSubCategories") as DropDownList;

            if (ddlSuppliers != null && ddlCategories != null && ddlSubCategories != null && ddlSubCategories.Items.Count < 1)
            {
                //Load suppliers
                List<GenericReturn> suppliers = DAL.Practice.Catalog.GetActiveMasterCatalogSuppliers();
                BindList(ddlSuppliers, suppliers); ;

                List<GenericReturn> categories = DAL.Practice.Catalog.GetMasterCatalogSupplierCategories(suppliers[0].ID);
                BindList(ddlCategories, categories);

                List<GenericReturn> subCategories = DAL.Practice.Catalog.GetMasterCatalogSupplierSubCategories(categories[0].ID);
                BindList(ddlSubCategories, subCategories);
            }
        }

        public void ddlSuppliers_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            isInEdit = true;
            DropDownList ddlSuppliers = sender as DropDownList;
            int selectedMCSupplierID = Convert.ToInt32(ddlSuppliers.SelectedValue);
            Control parent = ddlSuppliers.Parent;

            ShowPromoteItems(parent);

            LoadCategoriesForPromoteProduct(selectedMCSupplierID, parent);
        }


        public void ddlCategories_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            isInEdit = true;
            DropDownList ddlCategories = sender as DropDownList;
            int selectedCategoryID = Convert.ToInt32(ddlCategories.SelectedValue);
            Control parent = ddlCategories.Parent;
            if (parent is HtmlTableCell)
            {
                parent = ddlCategories.Parent.Parent;

            }

            ShowPromoteItems(parent);

            LoadSubCategoriesForPromoteProduct(parent, selectedCategoryID);
        }

        private static void LoadCategoriesForPromoteProduct(int selectedMCSupplierID, Control parent)
        {
            List<GenericReturn> categories = DAL.Practice.Catalog.GetMasterCatalogSupplierCategories(selectedMCSupplierID);

            DropDownList ddlCategories = parent.FindControl("ddlCategories") as DropDownList;

            BindList(ddlCategories, categories);

            int masterCatalogCategoryID = categories[0].ID;

            LoadSubCategoriesForPromoteProduct(parent, masterCatalogCategoryID);
        }

        private static void LoadSubCategoriesForPromoteProduct(Control parent, int masterCatalogCategoryID)
        {
            List<GenericReturn> subCategories = DAL.Practice.Catalog.GetMasterCatalogSupplierSubCategories(masterCatalogCategoryID);

            //DropDownList ddlSubCategories = parent.FindControl("ddlSubCategoriesMaster") as DropDownList;
            DropDownList ddlSubCategories = UIHelper.FindControlRecursive(parent, "ddlSubCategories") as DropDownList;

            if (ddlSubCategories != null)
            {
                BindList(ddlSubCategories, subCategories);
            }
        }


        public string SetQueue(string itemIndex)
        {
            return ((RadGrid1.PageSize * RadGrid1.CurrentPageIndex) + Convert.ToInt32(itemIndex) + 1).ToString();
        }


        private static void BindList(DropDownList ddl, List<GenericReturn> genericList)
        {
            ddl.DataSource = genericList;
            ddl.DataTextField = "Name";
            ddl.DataValueField = "ID";
            ddl.DataBind();
        }



        protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
        {

            //AppendToLabel("RadGrid1_ItemDataBound" + e.Item.GetType().ToString() + ", " + e.EventInfo.EventName.ToString()); 

            //// check IsDeleted to show the garbage can (delete button).
            if (e.Item.OwnerTableView.Name == "gtvProduct" || e.Item.OwnerTableView.Name == "gtvSupplierProduct")
            {
                if (e.Item is GridEditableItem && e.Item.IsInEditMode)
                {
                    GridEditableItem form = (GridEditableItem)e.Item;


                    if (e.Item is GridEditFormInsertItem) // insert (Add New Record or Insert product from this product)
                    {

                        if (ViewState["newValues"] != null)  // if being copy from an existing record.
                        {
                            PopulateNewRecordOnCopyFromExistingRecord(form);   //copy to OldRecordCopied viewstate until the insert button then compare 
                            //  or cancel button is pressed. then clear.
                        }
                    }


                    //Set focus to Product TextBox.
                    TextBox textProduct = (form.Controls[1].FindControl("txtProduct") as TextBox);
                    textProduct.Focus();  //Or get select record and explicitly set the background color. //May need to move this to highlight the selected row on update.
                    if (!blnHCPCS)
                    {

                        HtmlGenericControl divHCPCs = (form.Controls[1].FindControl("divHCPCs") as HtmlGenericControl);
                        divHCPCs.Visible = false;
                        HtmlGenericControl divHCPCsLabel = (form.Controls[1].FindControl("divHCPCsLabel") as HtmlGenericControl);
                        divHCPCsLabel.Visible = false;

                    }
                }



                if (e.Item is GridHeaderItem)
                {
                    if (!blnHCPCS || !blnRowNumber)
                    {
                        GridHeaderItem headerItem = e.Item as GridHeaderItem;
                        if (!blnHCPCS)
                        {
                            headerItem["HCPCSString"].Visible = false;
                        }
                        if (!blnRowNumber)
                        {
                            headerItem["RowNumber"].Visible = false;
                        }
                    }
                }



                // Display or hide the delete button.
                if (e.Item is GridDataItem)
                {
                    try
                    {
                        GridDataItem dataItem = e.Item as GridDataItem;

                        if (blnRowNumber)
                        {
                            intPageSize = e.Item.OwnerTableView.CurrentPageIndex;
                            intPageIndex = e.Item.OwnerTableView.PageSize;
                            intItemIndex = e.Item.ItemIndex;

                            //get value here and display
                            intRealIndex = (intPageSize * intPageIndex) + intItemIndex + 1;
                            dataItem["RowNumber"].Text = intRealIndex.ToString(); // ((RadGrid1.PageSize * RadGrid1.CurrentPageIndex) + int.parse(itemIndex) + 1).ToString();
                            dataItem["RowNumber"].Visible = true;
                        }
                        else
                        {
                            dataItem["RowNumber"].Width = 0;
                            dataItem["RowNumber"].Visible = false;
                            dataItem["RowNumber"].Enabled = false;
                        }

                        if (!blnHCPCS)
                        {
                            dataItem["HCPCSString"].Width = 0;
                            dataItem["HCPCSString"].Visible = false;
                            dataItem["HCPCSString"].Enabled = false;
                        }



                        int isDeletable = (dataItem["IsDeletable"] != null) ? Convert.ToInt32(dataItem["IsDeletable"].Text.ToString()) : 0;

                        string productCode = (dataItem["Code"] != null) ? dataItem["Code"].Text.ToString() : "";
                        string productName = (dataItem["ShortName"] != null) ? dataItem["ShortName"].Text.ToString() : "";

                        string stringOnClientClickMessage = FormatConfirmationMessage(productCode, productName);
                        (dataItem["btnDelete"].Controls[0] as ImageButton).Attributes.Add("onclick", stringOnClientClickMessage);  //"return confirm('Delete " + name + "?');");  
                        (dataItem["btnDelete"].Controls[0] as Image).ToolTip = "Delete Product";
                        if (SelectedProductID != null)
                        {
                            if (dataItem["ThirdPartyProductID"] != null)
                            {
                                if (dataItem["ThirdPartyProductID"].Text.ToString() == SelectedProductID.ToString())
                                {
                                    e.Item.Selected = true;

                                }
                            }
                        }
                        if (isDeletable != 1)                       
                        {
                            if (Page.User.IsInRole("BregAdmin"))
                            {
                                (dataItem["btnDelete"].Controls[0] as ImageButton).ImageUrl = "~/App_Themes/Breg/images/Common/delete-unsafe.png";
                            }
                            else
                            {
                                (dataItem["btnDelete"].Controls[0] as ImageButton).Visible = false;
                            }

                        }

                        // do in structure.  Set ToolTip of the Edit button,
                        (dataItem["btnEdit"].Controls[0] as Image).ToolTip = "Edit Product";

                        if (blnCopy)
                        {
                            (dataItem["btnCopy"].Controls[0] as ImageButton).Visible = true;
                            (dataItem["btnCopy"].Controls[0] as Image).ToolTip = "Create New Record based on this Product";
                        }
                        else
                        {
                            (dataItem["btnCopy"].Controls[0] as ImageButton).Visible = false;
                        }

                        // FIX THIS!!!
                        //if (Session["selectedItem"] != null)
                        //{
                        //    selectedItem = Convert.ToString((Convert.ToInt32(Session["selectedItem"])));
                        //    if (dataItem["ThirdPartyProductID"].Text.ToString() == Session["selectedItem"].ToString())
                        //    {
                        //        e.Item.Selected = true;
                        //    }
                        //}

                        // Greg:  Note the following code has not been tested, look here if problems with selected row.
                        //if (SelectedProductID != null)
                        //{
                        //    if (dataItem["ThirdPartyProductID"] != null)
                        //    {
                        //        if (dataItem["ThirdPartyProductID"].Text.ToString() == SelectedProductID.ToString())
                        //        {
                        //            e.Item.Selected = true;
                        //        }
                        //    }
                        //}



                    }
                    catch (Exception ex)
                    {
                        // implement a generic block of code for this.
                        // Catch for dummy programming.
                        string strErrMessage = ex.Message;
                    }
                }
            }
        }

        protected void RadGrid1_CancelCommand(object source, GridCommandEventArgs e)
        {
            AppendToLabel("RadGrid1_CancelCommand:   " + e.CommandName.ToString());

            if (e.CommandName == RadGrid.CancelCommandName && (e.Item.OwnerTableView.Name == "gtvProduct" || e.Item.OwnerTableView.Name == "gtvSupplierProduct"))  //"Update"
            {
                if (ViewState["newValues"] != null)
                {
                    ViewState.Remove("newValues");
                }
                //AppendToLabel("DATATABLE GetProducts: PracticeCatalogSupplierBrandID:" + practiceCatalogSupplierBrandID.ToString());
                e.Item.OwnerTableView.Caption = "";
                //e.Item.OwnerTableView.Rebind();
            }
        }


        protected void RadGrid1_InsertCommand(object source, GridCommandEventArgs e)
        {
            ViewState["SelectedPage"] = null;
            ViewState["NeedToSelectPage"] = 1;

            AppendToLabel("RadGrid1_InsertCommand" + e.CommandName.ToString());
            AppendToLabel("NeedToSelectPage");

            int rowsAffected = 0;

            if (e.CommandName == "PerformInsert"
                    && (e.Item.OwnerTableView.Name == "gtvProduct" || e.Item.OwnerTableView.Name == "gtvSupplierProduct"))
            {
                isInEdit = true;
                GridDataItem parentItem = (GridDataItem)(e.Item.OwnerTableView.ParentItem);
                if (parentItem != null)
                {
                    // Get the Practice Catalog Supplier Brand ID from the parent Grid Table View.
                    string pcsbID = parentItem.OwnerTableView.DataKeyValues[parentItem.ItemIndex]["PracticeCatalogSupplierBrandID"].ToString();
                    int practiceCatalogSupplierBrandID = Convert.ToInt32(pcsbID);
                    int newThirdPartyProductID = -1;
                    string productName;
                    string productCode;
                    string packaging;
                    string side;
                    string size;
                    string gender;
                    string color;
                    string mod1;
                    string mod2;
                    string mod3;
                    decimal wholesaleCost;
                    string HCPCSString;
                    string upcCode;
                    bool isHCPCSInserted = blnHCPCS;
                    //Get the GridEditFormInsertItem of the RadGrid  
                    GridEditFormInsertItem insertedItem = (GridEditFormInsertItem)e.Item;

                    //Access the textbox from the insert form template and store the values in string variables.  
                    //call method
                    GetValuesFromInsertForm(insertedItem, out productName, out productCode, out packaging
                                , out side, out size, out gender, out mod1, out mod2, out mod3, out color, out wholesaleCost, out HCPCSString, out upcCode);

                    bool isDuplicateRecord = false;
                    if (ViewState["newValues"] != null)
                    {
                        isDuplicateRecord = CompareNewRecordWithCopiedRecord(productName, productCode, packaging, side, size, gender, color, wholesaleCost, mod1, mod2, mod3);
                    }

                    //if not a duplicate than continue
                    if (isDuplicateRecord == false)
                    {
                        //Compare the new values to the old record in the database, if they are an exact match then send message to user.
                        try
                        {
                            string userMessage = "";
                            DAL.Practice.ThirdPartyProduct dalTPP = new DAL.Practice.ThirdPartyProduct();
                            rowsAffected = dalTPP.Insert(out newThirdPartyProductID, practiceCatalogSupplierBrandID, productCode, productName, packaging
                                                        , side, size, color, gender, mod1, mod2, mod3
                                                        , wholesaleCost, HCPCSString, isHCPCSInserted, userID, upcCode, out userMessage);

                            if (userMessage == "Duplicate")
                            {
                                GridEditFormInsertItem insertedItemBogus = (GridEditFormInsertItem)e.Item;
                                Label labelUserMessage = (insertedItemBogus.FindControl("lblUserMessage") as Label);
                                TextBox textboxShortName = (insertedItemBogus.FindControl("txtProduct") as TextBox);
                                labelUserMessage.Text = "Unable to insert product. Reason: This would create a duplicate product. <br/>Please adjust or cancel.";
                                labelUserMessage.Visible = true;
                                e.Canceled = true;
                                if (textboxShortName != null)
                                {
                                    textboxShortName.Focus();
                                }
                                return;
                            }
                            else
                            {
                                // Display message to user.
                                string messageHeader = "Inserted product:";
                                e.Item.OwnerTableView.Caption = SetProductCaptionMessage(messageHeader, productName, productCode, packaging, side, size, gender, color, wholesaleCost, mod1, mod2, mod3);
                                if (ViewState["newValues"] != null) { ViewState.Remove("newValues"); }

                                // SetMode.
                                SelectedProductID = newThirdPartyProductID;
                                NeedToSelectPage = 1;

                                e.Item.OwnerTableView.Rebind();
                            }
                        }
                        catch (Exception ex)
                        {
                            //RadGrid1.Controls.Add(new LiteralControl("Unable to insert Customers. Reason: " + ex.Message + ex.Source + ex.InnerException));
                            e.Canceled = true;
                            // Display message to user.
                            string messageHeader = "Unable to insert product. Reason: " + ex.Message;
                            e.Item.OwnerTableView.Caption = SetProductCaptionMessage(messageHeader, productName, productCode, packaging, side, size, gender, color, wholesaleCost, mod1, mod2, mod3);
                            if (ViewState["newValues"] != null) { ViewState.Remove("newValues"); }
                        }
                    }
                    else
                    {
                        GridEditFormInsertItem insertedItemBogus = (GridEditFormInsertItem)e.Item;
                        TextBox textboxShortName = (insertedItemBogus.FindControl("txtProduct") as TextBox);
                        Label labelUserMessage = (insertedItemBogus.FindControl("lblUserMessage") as Label);
                        labelUserMessage.Text = "Unable to insert product. Reason: This would create a duplicate product. <br/>Please adjust or cancel.";
                        labelUserMessage.Visible = true;
                        e.Canceled = true;
                        textboxShortName.Focus();
                        return;
                    }
                }
            }
        }


        protected void RadGrid1_UpdateCommand(object source, GridCommandEventArgs e)
        {
            //ViewState["NeedToSelectedItem"] = true;
            AppendToLabel("RadGrid1_UpdateCommand" + e.CommandName.ToString());
            //AppendToLabel("NeedToSelectedItem");
            int rowsAffected = 0;
            // Header for message display to user.
            string messageHeader;

            if (e.CommandName == RadGrid.UpdateCommandName && (e.Item.OwnerTableView.Name == "gtvProduct" || e.Item.OwnerTableView.Name == "gtvSupplierProduct"))  //"Update"
            {
                //Get the GridEditableItem of the RadGrid  
                GridEditableItem editedItem = e.Item as GridEditableItem;
                //Get the primary key value using the DataKeyValue.  
                int thirdPartyProductID = Convert.ToInt32(editedItem.OwnerTableView.DataKeyValues[editedItem.ItemIndex]["ThirdPartyProductID"].ToString());
                int practiceCatalogSupplierBrandID = Convert.ToInt32(editedItem.OwnerTableView.DataKeyValues[editedItem.ItemIndex]["PracticeCatalogSupplierBrandID"].ToString());


                //GridEditFormItem form = ((GridDataItem)RadGrid1.EditItems[0]).EditFormItem;
                GridEditFormItem form = e.Item as GridEditFormItem;

                Button btnPromote = (form.FindControl("btnUpdate") as Button);

                string productName;
                string productCode;
                string packaging;
                string side;
                string size;
                string gender;
                string color;
                decimal wholesaleCost;
                string HCPCSString;
                string upcCode;
                Boolean isHCPCSUpdated = blnHCPCS;
                string userMessage = "";
                string mod1;
                string mod2;
                string mod3;

                GetValuesFromUpdateForm(editedItem, out productName, out productCode, out packaging, out side, out size, out gender, out mod1, out mod2, out mod3, out color, out wholesaleCost, out HCPCSString, out upcCode);

                if (btnPromote.CommandArgument == "Promote")
                {
                    int mcSupplierID = 0;
                    int categoryID = 0;
                    int subCategoryID = 0;

                    GetSelectedIds(out mcSupplierID, out categoryID, out subCategoryID, form);
//                  The Promote feature is currently disabled.  If enabled saving hcpcs/ots will have to be addressed.
//                    DAL.Practice.Catalog.SaveToMasterCatalog(0, productName, productCode, packaging, side, size,
//                                         gender, color, wholesaleCost, hcpcsString, hcpcsStringOts, mcSupplierID, categoryID, subCategoryID, false, upcCode, userID);

                }
                else
                {
                    messageHeader = SaveProduct(e, ref rowsAffected, editedItem, thirdPartyProductID, practiceCatalogSupplierBrandID, productName, productCode, packaging, side, size,
                                                    gender, color, wholesaleCost, upcCode, HCPCSString, isHCPCSUpdated, ref userMessage, mod1, mod2, mod3);
                }
            }
        }



        private void GetSelectedIds(out int mcSupplierID, out int categoryID, out int subCategoryID, Control form)
        {
            DropDownList ddlSuppliers = form.FindControl("ddlSuppliers") as DropDownList;
            DropDownList ddlCategories = form.FindControl("ddlCategories") as DropDownList;
            DropDownList ddlSubCategories = form.FindControl("ddlSubCategories") as DropDownList;

            if (ddlSuppliers != null && ddlCategories != null && ddlSubCategories != null && ddlSubCategories.Items.Count > 0)
            {
                mcSupplierID = Convert.ToInt32(ddlSuppliers.SelectedValue);
                categoryID = Convert.ToInt32(ddlCategories.SelectedValue);
                subCategoryID = Convert.ToInt32(ddlSubCategories.SelectedValue);
                return;
            }
            throw new ApplicationException("Master Catalog Info not found");
        }

        private void GetSelectedMasterCategoryIds(out int categoryID, out int subCategoryID, Control form)
        {
            
            DropDownList ddlCategories = form.FindControl("ddlCategoriesMaster") as DropDownList;
            DropDownList ddlSubCategories = form.FindControl("ddlSubCategoriesMaster") as DropDownList;

            if (ddlCategories != null && ddlSubCategories != null && ddlSubCategories.Items.Count > 0)
            {
                
                categoryID = Convert.ToInt32(ddlCategories.SelectedValue);
                subCategoryID = Convert.ToInt32(ddlSubCategories.SelectedValue);
                return;
            }
            throw new ApplicationException("Master Catalog Categories Info not found");
        }

        private string SaveProduct(GridCommandEventArgs e, ref int rowsAffected, GridEditableItem editedItem, int thirdPartyProductID, int practiceCatalogSupplierBrandID, string productName, string productCode, string packaging, string side, string size, string gender, string color, decimal wholesaleCost, string upcCode, string HCPCSString, Boolean isHCPCSUpdated, ref string userMessage, string mod1, string mod2, string mod3)
        {
            string messageHeader = string.Empty;
                try
                {
                    DAL.Practice.ThirdPartyProduct dalTPP = new DAL.Practice.ThirdPartyProduct();
                    rowsAffected = dalTPP.Update(thirdPartyProductID, practiceCatalogSupplierBrandID, productCode, productName
                                            , packaging, side, size, color, gender, mod1, mod2, mod3
                                            , wholesaleCost, HCPCSString, isHCPCSUpdated, userID, upcCode, out userMessage);

                    if (userMessage == "Duplicate")
                    {
                        //GridEditFormInsertItem insertedItemBogus = (GridEditFormInsertItem)e.Item;
                        Label labelUserMessage = (editedItem.FindControl("lblUserMessage") as Label);
                        TextBox textboxProduct = (editedItem.FindControl("txtProduct") as TextBox);
                        labelUserMessage.Text = "Unable to update product. Reason: This would create a duplicate product. <br/>Please adjust or cancel.";
                        labelUserMessage.Visible = true;
                        e.Canceled = true;
                        if (textboxProduct != null)
                        {
                            textboxProduct.Focus();
                        }

                    }
                else if (userMessage == "Deleted")
                    {
                        //GridEditFormInsertItem insertedItemBogus = (GridEditFormInsertItem)e.Item;
                        //Label labelUserMessage = (editedItem.FindControl("lblUserMessage") as Label);
                        //TextBox textboxProduct = (editedItem.FindControl("txtProduct") as TextBox);

                        messageHeader = "Unable to update product. <br/>Reason: This product has already been deleted by any source.";
                        e.Item.OwnerTableView.Caption = SetProductCaptionMessage(messageHeader, productName, productCode, packaging, side, size, gender, color, wholesaleCost, mod1, mod2, mod3);

                        e.Canceled = true;

                        //CLOSE THE EDIT BOX.
                        //e.Item.OwnerTableView.ClearEditItems();
                    //return;
                    }


                    else
                    {
                        // Display message to user.
                        messageHeader = "Updated product:";
                        e.Item.Selected = true;
                        e.Item.OwnerTableView.Caption = SetProductCaptionMessage(messageHeader, productName, productCode, packaging, side, size, gender, color, wholesaleCost, mod1, mod2, mod3);

                        //DisplayMessage(messageHeader + "0line522_");
                        SelectedProductID = thirdPartyProductID;
                        if (userMessage == "UpdatedProductName")
                        {
                            SelectedPage = null;        // Because page is not yet known.
                            NeedToSelectPage = 1;       // Because Product Name was changed.
                            //Session["selectedItem"] = thirdPartyProductID.ToString();                            
                        }
                        else
                        {
                            //uncomment below to increse accuracy and decrease performance.
                            SelectedPage = null;
                            NeedToSelectPage = 1;       // SelectedPage is already known it is the same.
                            // maybe problem if pulls new data set.
                            //SelectedPage is ThemeableAttribute same
                        }
                    }
                }
                catch (Exception ex)
                {
                    RadGrid1.Controls.Add(new LiteralControl("Unable to update product. Reason: " + ex.Message));
                    e.Canceled = true;

                    // Display message to user.
                    messageHeader = "Unable to update product. Reason: " + ex.Message;
                    e.Item.OwnerTableView.Caption = SetProductCaptionMessage(messageHeader, productName, productCode, packaging, side, size, gender, color, wholesaleCost, mod1, mod2, mod3);
                    //DisplayMessage(messageHeader);
                }
            return messageHeader;
        }


        protected void RadGrid1_DeleteCommand(object source, GridCommandEventArgs e)
        {
            SelectedProductID = null;  //ViewState.Remove("SelectedItem");


            AppendToLabel("RadGrid1_DeleteCommand" + e.CommandName.ToString());
            AppendToLabel("Removed selectedItem is NULL!");
            int rowsAffected = 0;
            if (e.CommandName == RadGrid.DeleteCommandName && (e.Item.OwnerTableView.Name == "gtvProduct" || e.Item.OwnerTableView.Name == "gtvSupplierProduct"))
            {
                if (e.Item is GridDataItem)
                {
                    //Get the GridDataItem of the RadGrid     
                    GridDataItem dataItem = (GridDataItem)e.Item;
                    //Get the primary key value using the DataKeyValue.     
                    string strPracticeCatalogProductID = dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PracticeCatalogProductID"].ToString();

                    int practiceCatalogProductID = Convert.ToInt32(strPracticeCatalogProductID);
                    string productCode = (dataItem["Code"] != null) ? dataItem["Code"].Text.ToString() : "";
                    string productName = (dataItem["ShortName"] != null) ? dataItem["ShortName"].Text.ToString() : "";

                    int removalType = -1;
                    string dependencyMessage = "";

                    try
                    {
                        bool isAdmin = Page.User.IsInRole("BregAdmin");
                        DAL.Practice.Catalog dalC = new DAL.Practice.Catalog();
                        rowsAffected = dalC.Delete(practiceCatalogProductID, userID, out removalType, out dependencyMessage, isAdmin); //practiceCatalogSupplierBrandID, productCode, productName, packaging, side, size, color, gender, wholesaleCost, userID);

                        // Display message to user.
                        string messageHeader = "Deleted product:";
                        string userMessage = CreateUserMessage(messageHeader, dependencyMessage, productName, productCode, removalType);
                        e.Item.OwnerTableView.Caption = userMessage;    //"product delete attempt:" + dependencyMessage;
                    }
                    catch (Exception ex)
                    {
                        RadGrid1.Controls.Add(new LiteralControl("Unable to delete product. Reason: " + ex.Message));
                        e.Canceled = true;
                        // Display message to user.
                        string messageHeader = "Unable to delete product.";
                        string userMessage = CreateUserMessage(messageHeader, dependencyMessage, productName, productCode, removalType);
                        e.Item.OwnerTableView.Caption = userMessage;
                        //e.Item.OwnerTableView
                    }
                }
            }
        }


        protected string CreateUserMessage(string messageHeader, string dependencyMessage, string productName, string productCode, int removalType)
        {
            string deleteMessage = "";
            productName = FormatStringForJavaScript(productName);
            productCode = FormatStringForJavaScript(productCode);

            string productNameCode = "<br />  Product: " + productName + "<br />  Code: " + productCode;

            switch (removalType)
            {
                case -1:
                    {
                        //  do nothing
                        deleteMessage = ConfigurationManager.AppSettings["PCPDeleteError"].ToString();
                        deleteMessage += productNameCode;
                        break;
                    }
                case 0:
                    {
                        //Show dependencies.
                        deleteMessage = ConfigurationManager.AppSettings["PCPDeleteNotRemoved"].ToString();

                        string deleteDependencies = "";
                        //if ( ( Convert.ToInt32(dependencyMessage.Contains("level") ) > 0) == true)
                        //    deleteDependencies += "<br /> &nbsp;&nbsp;" + ConfigurationManager.AppSettings["PCPDeleteDependencyLevels"].ToString();
                        //if ( ( Convert.ToInt32(dependencyMessage.Contains("Cart") ) > 0) == true)
                        //    deleteDependencies += "<br /> &nbsp;&nbsp;" + ConfigurationManager.AppSettings["PCPDeleteDependencyInCart"].ToString();
                        //if ( ( Convert.ToInt32(dependencyMessage.Contains("Order") ) > 0) == true)
                        //    deleteDependencies += "<br /> &nbsp;&nbsp;" + ConfigurationManager.AppSettings["PCPDeleteDependencyOnOrder"].ToString();

                        deleteMessage += deleteDependencies.ToString() + productNameCode;
                        break;
                    }
                case 1:
                    {
                        //  removed.  hard delete.
                        deleteMessage = ConfigurationManager.AppSettings["PCPDeleted"].ToString();
                        deleteMessage += productNameCode;
                        break;
                    }
                case 2:
                    {
                        //  removed.  soft delete.
                        deleteMessage = ConfigurationManager.AppSettings["PCPRemoved"].ToString();
                        deleteMessage += productNameCode;
                        break;
                    }
                default:
                    {
                        //  do nothing
                        break;
                    }
            }
            return deleteMessage;
        }

        protected string SetProductCaptionMessage(string messageHeader, string productName, string productCode, string packaging
                                        , string side, string size, string gender, string color, decimal wholesaleCost, string mod1, string mod2, string mod3)
        {
            //string productMessage;
            StringBuilder sb = new StringBuilder();
            //string delimiter = "; ";
            sb.Append(messageHeader);
            sb.Append("<br/>");
            sb.Append("Product: ");
            sb.Append("\t" + productName);
            sb.Append("<br/>");
            sb.Append("Code: ");
            sb.Append(productCode);
            return sb.ToString();
        }

        // Create string for Deltet Confirmation Message.
        protected string FormatConfirmationMessage(string productCode, string productName)
        {
            productCode = FormatStringForJavaScript(productCode);
            productName = FormatStringForJavaScript(productName);

            string stringOnClickMessage = @" Delete Product?\n\n Product:  "  //= @" Delete from Practice Catalog?\n\n Product:  "
                + productName + @"\n Code:     "
                + productCode;

            string stringOnClientClickMessage = @"if(!confirm('" + stringOnClickMessage + "'))return false;";
            return stringOnClientClickMessage;
        }

        // Replace string with escape character for single quotes, double quote, and backslashes
        protected string FormatStringForJavaScript(string inputString)
        {
            string outputString = inputString.Replace("\\", "/").Replace("\'", "\\\'").Replace("\"", "\\\"");
            return outputString.Trim();

        }

        protected void GetValuesFromUpdateForm(GridEditableItem editedItem, out string productName, out string productCode, out string packaging
            , out string side, out string size, out string gender, out string mod1, out string mod2, out string mod3, out string color, out decimal wholesaleCost, out string HCPCSstring, out string upcCode)
        {
            productName = (editedItem.FindControl("txtProduct") as TextBox).Text.Trim();
            productCode = (editedItem.FindControl("txtCode") as TextBox).Text.Trim();
            packaging = (editedItem.FindControl("txtPackaging") as TextBox).Text.Trim();
            side = (editedItem.FindControl("txtSide") as TextBox).Text.Trim();
            size = (editedItem.FindControl("txtSize") as TextBox).Text.Trim();
            gender = (editedItem.FindControl("txtGender") as TextBox).Text.Trim();
            mod1 = (editedItem.FindControl("ddlMod1") as DropDownList).Text.Trim();
            mod2 = (editedItem.FindControl("ddlMod2") as DropDownList).Text.Trim();
            mod3 = (editedItem.FindControl("ddlMod3") as DropDownList).Text.Trim();
            color = (editedItem.FindControl("txtColor") as TextBox).Text.Trim();
            upcCode = (editedItem.FindControl("txtUPCCode") as TextBox).Text.Trim();
            wholesaleCost = 0;
            TextBox textboxWholesaleCost = (TextBox)editedItem.FindControl("txtWholesaleCost");
            if (textboxWholesaleCost.Text.Trim().Length > 0)
            {
                wholesaleCost = Convert.ToDecimal(textboxWholesaleCost.Text.Replace("$", "").Replace(",", "").Trim());
            }
            HCPCSstring = (editedItem.FindControl("txtHCPCSstring") as TextBox).Text.Trim().Replace(", ", ",");

        }
        //
        protected void GetValuesFromInsertForm(GridEditFormItem insertedItem, out string productName, out string productCode, out string packaging
            , out string side, out string size, out string gender, out string mod1, out string mod2, out string mod3, out string color, out decimal wholesaleCost, out string HCPCSstring, out string upcCode)
        {
            GetValuesFromInsertForm(insertedItem, out productName, out productCode, out packaging
                               , out side, out size, out gender, out mod1, out mod2, out mod3, out color, out wholesaleCost, out HCPCSstring, out upcCode, false);
        }

        protected void GetValuesFromInsertForm(GridEditFormItem insertedItem, out string productName, out string productCode, out string packaging
            , out string side, out string size, out string gender, out string mod1, out string mod2, out string mod3, out string color, out decimal wholesaleCost, out string HCPCSstring, out string upcCode, bool master)
        {
            string masterName = "";
            if (master == true)
            {
                masterName = "Master";
            }
            //string s = insertedItem["Product"].Text.ToString();

            productName = (insertedItem.FindControl("txtProduct" + masterName) as TextBox).Text.Trim();
            productCode = (insertedItem.FindControl("txtCode" + masterName) as TextBox).Text.Trim();
            packaging = (insertedItem.FindControl("txtPackaging" + masterName) as TextBox).Text.Trim();

            side = (insertedItem.FindControl("txtSide" + masterName) as TextBox).Text.Trim();
            size = (insertedItem.FindControl("txtSize" + masterName) as TextBox).Text.Trim();
            gender = (insertedItem.FindControl("txtGender" + masterName) as TextBox).Text.Trim();
            mod1 = (insertedItem.FindControl("ddlMod1" + masterName) as DropDownList).SelectedItem.Value;
            mod2 = (insertedItem.FindControl("ddlMod2" + masterName) as DropDownList).SelectedItem.Value;
            mod3 = (insertedItem.FindControl("ddlMod3" + masterName) as DropDownList).SelectedItem.Value;
            color = (insertedItem.FindControl("txtColor" + masterName) as TextBox).Text.Trim();
            upcCode = (insertedItem.FindControl("txtUpcCode" + masterName) as TextBox).Text.Trim();

            wholesaleCost = 0;
            TextBox textboxWholesaleCost = (TextBox)insertedItem.FindControl("txtWholesaleCost" + masterName);
            if (textboxWholesaleCost.Text.Trim().Length > 0)
            {
                wholesaleCost = Convert.ToDecimal(textboxWholesaleCost.Text.Replace("$", "").Replace(",", "").Trim());
            }

            HCPCSstring = (insertedItem.FindControl("txtHCPCSstring" + masterName) as TextBox).Text.Trim().Replace(", ", ",");
        }

        protected void SetViewStateForCopyToNewRow(GridDataItem dataItem, ref ListDictionary newValues)
        {
            //ListDictionary newValues
            newValues["UserMessage"] = "Note: This product is based on an existing product: <br/>Please modify to prevent duplicate records.";
            newValues["ShortName"] = dataItem["ShortName"].Text.ToString().Trim();
            newValues["Code"] = (dataItem["Code"] != null) ? dataItem["Code"].Text.Trim() : ""; // dataItem["Code"].Text.ToString().Trim();
            newValues["Packaging"] = dataItem["Packaging"].Text.Trim();
            newValues["WholesaleCost"] = dataItem["WholesaleCost"].Text.Trim().Replace("$", "");
            newValues["Side"] = (dataItem["Side"].Text.Trim().ToString() != "&nbsp;") ? dataItem["Side"].Text.Trim().ToString() : "";
            newValues["Size"] = (dataItem["Size"].Text.Trim().ToString() != "&nbsp;") ? dataItem["Size"].Text.Trim().ToString() : "";
            newValues["Gender"] = (dataItem["Gender"].Text.Trim().ToString() != "&nbsp;") ? dataItem["Gender"].Text.Trim().ToString() : "";
            newValues["Mod1"] = (dataItem["Mod1"].Text.Trim().ToString() != "&nbsp;") ? dataItem["Mod1"].Text.Trim().ToString() : "";
            newValues["Mod2"] = (dataItem["Mod2"].Text.Trim().ToString() != "&nbsp;") ? dataItem["Mod2"].Text.Trim().ToString() : "";
            newValues["Mod3"] = (dataItem["Mod3"].Text.Trim().ToString() != "&nbsp;") ? dataItem["Mod3"].Text.Trim().ToString() : "";
            newValues["Color"] = (dataItem["Color"].Text.Trim().ToString() != "&nbsp;") ? dataItem["Color"].Text.Trim().ToString() : "";
            newValues["HCPCSString"] = (dataItem["HCPCSString"].Text.Trim().ToString() != "&nbsp;") ? dataItem["HCPCSString"].Text.Trim().ToString() : "";
            newValues["UPCCode"] = (dataItem["UPCCode"].Text.Trim().ToString() != "&nbsp;") ? dataItem["UPCCode"].Text.Trim().ToString() : "";

            ViewState["newValues"] = newValues;
            //ViewState["Copy"] = "Copy";
        }


        protected bool CompareNewRecordWithCopiedRecord(string productName, string productCode, string packaging, string side, string size, string gender, string color, decimal wholesaleCost, string mod1, string mod2, string mod3)
        {
            ListDictionary newValues = (ListDictionary)ViewState["newValues"];

            if (productName == newValues["ShortName"].ToString()
                    && productCode == newValues["Code"].ToString()
                    && packaging == newValues["Packaging"].ToString()
                    && side == newValues["Side"].ToString()
                    && size == newValues["Size"].ToString()
                    && gender == newValues["Gender"].ToString()
                    && color == newValues["Color"].ToString()
                    && mod1 == newValues["Mod1"].ToString()
                    && mod2 == newValues["Mod2"].ToString()
                    && mod3 == newValues["Mod3"].ToString()
                )
            {
                return true;  // New record will cause duplicate.
            }
            else
            {
                return false;
            }
        }

        protected void PopulateNewRecordOnCopyFromExistingRecord(GridEditableItem form)
        {
            TextBox textProduct = (form.Controls[1].FindControl("txtProduct") as TextBox);
            TextBox textCode = (form.Controls[1].FindControl("txtCode") as TextBox);
            TextBox textPackaging = (form.Controls[1].FindControl("txtPackaging") as TextBox);
            TextBox textWholesaleCost = (form.Controls[1].FindControl("txtWholesaleCost") as TextBox);

            TextBox textSide = (form.Controls[1].FindControl("txtSide") as TextBox);
            TextBox textSize = (form.Controls[1].FindControl("txtSize") as TextBox);
            TextBox textGender = (form.Controls[1].FindControl("txtGender") as TextBox);
            DropDownList textMod1 = (form.Controls[1].FindControl("ddlMod1") as DropDownList);
            DropDownList textMod2 = (form.Controls[1].FindControl("ddlMod2") as DropDownList);
            DropDownList textMod3 = (form.Controls[1].FindControl("ddlMod3") as DropDownList);
            TextBox textColor = (form.Controls[1].FindControl("txtColor") as TextBox);
            TextBox textUpcCode = (form.Controls[1].FindControl("txtUpcCode") as TextBox);

            TextBox textHCPCSString = (form.Controls[1].FindControl("txtHCPCSString") as TextBox);

            ListDictionary newValues = (ListDictionary)ViewState["newValues"];
            textProduct.Text = newValues["ShortName"].ToString(); // = dataItem["ShortName"].Text.ToString();
            textCode.Text = newValues["Code"].ToString(); // = dataItem["Code"].Text.ToString();
            textPackaging.Text = newValues["Packaging"].ToString();  // = dataItem["Packaging"].Text.ToString();
            textWholesaleCost.Text = newValues["WholesaleCost"].ToString();  // = dataItem["WholesaleCost"].Text.ToString();
            textUpcCode.Text = newValues["UPCCode"].ToString();  // = dataItem["WholesaleCost"].Text.ToString();

            textSide.Text = newValues["Side"].ToString();  // = dataItem["Side"].Text.ToString();
            textSize.Text = newValues["Size"].ToString();  // = dataItem["Size"].Text.ToString();
            textGender.Text = newValues["Gender"].ToString();  // = dataItem["Gender"].Text.ToString();
            textMod1.Text = newValues["Mod1"].ToString();  // = dataItem["Mod1"].Text.ToString();
            textMod2.Text = newValues["Mod2"].ToString();  // = dataItem["Mod2"].Text.ToString();
            textMod3.Text = newValues["Mod3"].ToString();  // = dataItem["Mod3"].Text.ToString();
            textColor.Text = newValues["Color"].ToString();  // = dataItem["Color"].Text.ToString();

            textHCPCSString.Text = newValues["HCPCSString"].ToString();
        }


        public DataTable GetVendors(int practiceID)
        {
            DAL.Practice.ThirdPartyProduct dalTPP = new DAL.Practice.ThirdPartyProduct();
            DataTable dt = dalTPP.GetVendors(practiceID);
            return dt;
        }

        public DataTable GetBrands(int thirdPartySupplierID)
        {
            DAL.Practice.ThirdPartyProduct dalTPP = new DAL.Practice.ThirdPartyProduct();
            DataTable dt = dalTPP.GetBrands(thirdPartySupplierID);                  // .GetVendors(practiceID);
            return dt;
        }

        public DataTable GetProducts(int practiceCatalogSupplierBrandID)
        {
            AppendToLabel("GetProducts: ID is : " + practiceCatalogSupplierBrandID.ToString());

            DAL.Practice.ThirdPartyProduct dalTPP = new DAL.Practice.ThirdPartyProduct();
            DataTable dt = dalTPP.GetProducts(practiceCatalogSupplierBrandID);      // .GetVendors(practiceID);
            return dt;
        }

        protected void RadGrid1_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            //only call if this is the MasterGridView on the Initial Load.
            if (((e.RebindReason == GridRebindReason.InitialLoad)  || (e.RebindReason == GridRebindReason.ExplicitRebind))
                    && (e.IsFromDetailTable == false))
            {
                RadGrid1.DataSource = GetVendors(practiceID);
            }
        }

        // Occurs during the data binding of the detail tables.
        // Check which detail table view is being bound.
        protected void RadGrid1_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {
            //AppendToLabel("RadGrid1_DetailTableDataBind: GTV: " + e.DetailTableView.Name.ToString() + ".");

            //  If the DetailTableView is in edit mode then return.
            GridDataItem parentItem = (GridDataItem)e.DetailTableView.ParentItem;
            if (parentItem.Edit)
            {
                return;
            }

            // Get the DetailTableView's name.
            string detailTableViewName = e.DetailTableView.Name.ToString();
            if (detailTableViewName == "gtvSupplier")
            {
                //  Check gtvSupplier's primary key, first make sure it is safe (exists)
                if (e.DetailTableView.GetColumnSafe("PracticeCatalogSupplierBrandID") != null)
                {
                    int practiceCatalogSupplierBrandID = Convert.ToInt32(parentItem["PracticeCatalogSupplierBrandID"].Text.ToString());

                    //If pcsbID = 0 and OptionC is false continue.
                    if (practiceCatalogSupplierBrandID == 0 || !isOptionC)  // If pcsbID = 0 and OptionC is false continue.check for option.)  
                    {
                        // Get the Brands of the Supplier via the gtvSupplier's Supplier to Brand linking key.
                        if (e.DetailTableView.GetColumnSafe("ThirdPartySupplierID") != null)
                        {
                            int thirdPartySupplierID = Convert.ToInt32(parentItem["ThirdPartySupplierID"].Text.ToString());
                            DataTable dtSupplier = GetBrands(thirdPartySupplierID);
                            e.DetailTableView.DataSource = dtSupplier;
                        }
                    }
                }
            }

            //  Check if the Detail Table View is gtvProduct.
            if (detailTableViewName == "gtvProduct")
            {
                if (e.DetailTableView.GetColumnSafe("PracticeCatalogSupplierBrandID") != null)
                {
                    //JB
                    //////Check if Paging is occuring.
                    ////if ( (ViewState["SettingPageToSelect"] != null) && (Boolean)(ViewState["SettingPageToSelect"]) )
                    ////{
                    ////    // Check if the dtProduct is in viewState
                    ////    if (ViewState["dtProduct"] != null)
                    ////    {
                    ////        //if in viewState do not call the database, get from the viewState.
                    ////        e.DetailTableView.DataSource = (DataTable)ViewState["dtProduct"];
                    ////    }
                    ////    //if not in viewState then call the database and save to viewstate.
                    ////    else
                    ////    {
                    ////        // Get the linking key between the parent (gtvSupplier) and the nested (gtvProduct)
                    ////        int practiceCatalogSupplierBrandID = Convert.ToInt32(parentItem["PracticeCatalogSupplierBrandID"].Text.ToString());
                    ////        DataTable dtProduct = GetProducts(practiceCatalogSupplierBrandID);
                    ////        ViewState["dtProduct"] = dtProduct;
                    ////        e.DetailTableView.DataSource = dtProduct;
                    ////    }
                    ////}
                    ////else  //Paging is not occuring, so call the database.
                    ////{

                    int practiceCatalogSupplierBrandID = Convert.ToInt32(parentItem["PracticeCatalogSupplierBrandID"].Text.ToString());
                    DataTable dtProduct = GetProducts(practiceCatalogSupplierBrandID);
                    e.DetailTableView.DataSource = dtProduct;

                    ////}

                    //  Clear the user message if neccessary.
                    if (clearProductCaption)
                    {
                        e.DetailTableView.Caption = "";
                        clearProductCaption = false;
                    }
                    int count = e.DetailTableView.Items.Count;
                }
            }


            // Check if the Detail Table View is gtvSupplierProduct.
            if (detailTableViewName == "gtvSupplierProduct")
            {
                if (e.DetailTableView.GetColumnSafe("PracticeCatalogSupplierBrandID") != null)
                {
                    int practiceCatalogSupplierBrandID = Convert.ToInt32(parentItem["PracticeCatalogSupplierBrandID"].Text.ToString());
                    // If the supplier is the brand then connect the supplier directly to the products.
                    if (practiceCatalogSupplierBrandID > 0 && isOptionC)  // check for option.
                    {
                        DataTable dtProduct = GetProducts(practiceCatalogSupplierBrandID);
                        e.DetailTableView.DataSource = dtProduct;
                        if (clearProductCaption)
                        {
                            e.DetailTableView.Caption = "";
                            clearProductCaption = false;
                        }
                    }
                }
            }
        }

        protected eSetupStatus ValidateSetupData()
        {
            return eSetupStatus.Complete;
        }

        /// <summary>
        /// IVisionWizardControl interface save.  Called by wizard control on tab change
        /// </summary>
        public void Save()
        {
            //do nothing.  Grid rows are saved individually

            SaveStatus(practiceID, -1, this.ToString(), ValidateSetupData());
        }
    }
}
