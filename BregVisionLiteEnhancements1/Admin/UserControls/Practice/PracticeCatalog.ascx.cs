﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.Admin.PracticeSetup;
using BregVision.TelerikAjax;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.WebControls;
using ClassLibrary.DAL;

namespace BregVision.Admin.UserControls
{
    public partial class PracticeCatalog : PracticeSetupBase, IVisionWizardControl
    {


        int intPCItems = 0;
        private string deleteMessage = "";
		bool productReplacementWasClickedOn = false;

        /// <summary>
        /// IVisionWizardControl Interface.
        /// </summary>
        public bool NeedsInitialBind { get; set; }

		public bool ShowProductReplacement
		{
			get
			{
				object productReplacement = this.Page.Session[this.ClientID + "_ProductReplacement"];
				if (productReplacement is bool)
					return (bool)productReplacement;
				return false;
			}
			set
			{
				this.Page.Session[this.ClientID + "_ProductReplacement"] = value;
			}
		}

		protected void ProductReplacementWasClickedOn(object sender, EventArgs e)
		{
			productReplacementWasClickedOn = true;
		}

        protected void page_Init(object sender, EventArgs e)
        {
			MasterCatalogSearch.Grid = grdMasterCatalog;
			MasterCatalogSearch.DefaultLoadingPanel = RadAjaxLoadingPanel1;
			MasterCatalogSearch.searchEvent += new global::BregVision.UserControls.General.SearchEventHandler(MasterCatalogSearch_searchEvent);

			PracticeCatalogSearch.Grid = grdPracticeCatalog;
            PracticeCatalogSearch.DefaultLoadingPanel = RadAjaxLoadingPanel1;
            PracticeCatalogSearch.searchEvent += new global::BregVision.UserControls.General.SearchEventHandler(PracticeCatalogSearch_searchEvent);
            PracticeCatalogSearch.ClearDropdownButtons();
            PracticeCatalogSearch.AddItem("Product Code", "Code");
            PracticeCatalogSearch.AddItem("Product Name", "Name");
            PracticeCatalogSearch.AddItem("Manufacturer", "Brand");
            PracticeCatalogSearch.AddItem("HCPCs", "HCPCs");

            RadInputManagerImplementation_Setup();
        }


        #region RadInputManager implementation

        private bool _IsDetailTableBinding = false;
        
        private class RadInputManagerSettingItem
        {
            public string RadInputManagerBehaviorID { get; set; }
            public string TargetControlClientID { get; set; }
        }

        private List<RadInputManagerSettingItem> RadInputManagerSettings = new List<RadInputManagerSettingItem>();

        private static class RadInputBehaviorIDs
        {
            public static readonly string CurrencyValue = "currencySetting";
            public static readonly string StockValue = "stockSetting";
        }

        protected void RadNumericTextBox_Load1(object sender, EventArgs e)
        {
            if (!_IsDetailTableBinding)
                return;

            var t = sender as TextBox;
            if (t == null)
                return;

            var s =
                new RadInputManagerSettingItem
                {
                    RadInputManagerBehaviorID = RadInputBehaviorIDs.StockValue,
                    TargetControlClientID = t.ClientID
                };
            RadInputManagerSettings.Add(s);
        }

        protected void RadNumericTextBox_Load2(object sender, EventArgs e)
        {
            if (!_IsDetailTableBinding)
                return;

            var t = sender as TextBox;
            if (t == null)
                return;

            var s =
                new RadInputManagerSettingItem
                {
                    RadInputManagerBehaviorID = RadInputBehaviorIDs.CurrencyValue,
                    TargetControlClientID = t.ClientID
                };
            RadInputManagerSettings.Add(s);
        }

        private void RadInputManager_EmitClientSideJavaScript(object sender, EventArgs e)
        {
            // output script storage
            var script = new System.Text.StringBuilder();

            // fill script if there are settings to attach to RadInputManager, leave as empty script if not
            // NOTE: helps prevent unintentional script execution on each page load (from other async postbacks)
            if (RadInputManagerSettings.Count > 0)
            {
                // convert list into a lookup table to group settings by behavior id
                var behavior_lookup_table = RadInputManagerSettings.ToLookup(x => x.RadInputManagerBehaviorID);

                // loop through each behavior and render script
                foreach (var behavior_group in behavior_lookup_table)
                {
                    // get behavior id
                    var behavior_id = behavior_group.Key;

                    // set up a local var to contain the InputSetting object
                    script.Append(string.Format(@"var {0}_s=$find('{1}').get_inputSettings('{0}');", behavior_id, RadInputManager1.ClientID));

                    // output lines to add target control to behavior
                    foreach (var target in behavior_group)
                        script.Append(string.Format(@"{0}_s.addTargetInput('{1}');", behavior_id, target.TargetControlClientID));
                }
            }

            // register the startup script
            var ajaxmanager = Telerik.Web.UI.RadAjaxManager.GetCurrent(Page);
            if (ajaxmanager != null)
            {
                ScriptManager.RegisterStartupScript(
                    Page,
                    typeof(Page),
                    "BregRadInputManagerLoad",
                    string.Format(
                        @"var BregPracticeCatalogRadInputManagerLoadHandler=function(){{Sys.Application.remove_load(BregPracticeCatalogRadInputManagerLoadHandler);{0}}};Sys.Application.add_load(BregPracticeCatalogRadInputManagerLoadHandler);",
                        script.ToString()),
                    true);
            }
        }

        private void RadInputManagerImplementation_Setup()
        {
            Page.PreRenderComplete += new EventHandler(RadInputManager_EmitClientSideJavaScript);
            Page.PreRenderComplete += new EventHandler(SplitCodeToggle_EmitClientSideJavaScript);
        }

        #endregion

        private void SplitCodeToggle_EmitClientSideJavaScript(object sender, EventArgs e)
        {
            string scriptstring = "bindInitialSplitState();";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "bindInitialSplitState", scriptstring, true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Thoese 2 lines get datatable whole of locations
            if (!Page.IsPostBack)
            { 
                BLL.PracticeLocation.PracticeLocation practiceLocation = new BLL.PracticeLocation.PracticeLocation();
                DataTable allLocations =  practiceLocation.SelectAll(practiceID);

                practiceIsCustomFitEnabled = DAL.Practice.Practice.IsCustomFitEnabled(practiceID);
                
                cboPracticeLocation.DataSource = allLocations;
                cboPracticeLocation.DataTextField = "Name";
                cboPracticeLocation.DataValueField = "PracticeLocationID";
                cboPracticeLocation.DataBind();

            }
            lblContentHeader.Text = practiceName + " Practice Catalog";

            if (grdMasterCatalog.HasInitiallyBound == false)
            {


                grdMasterCatalog.SetGridLabel("", false, lblMCStatus, true);//set the default text on the label
                grdPracticeCatalog.SetGridLabel("", false, lblPCStatus, true);//set the default text on the label


                txtRecordsDisplayed.Text = recordsDisplayedPerPage.ToString();
                grdMasterCatalog.PageSize = recordsDisplayedPerPage;
                grdPracticeCatalog.PageSize = recordsDisplayedPerPage;

                grdMasterCatalog.InitializeSearch();
                grdPracticeCatalog.InitializeSearch();
            }

        }

        private void InitializeAjaxSettings()
        {
            var radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);
            if (radAjaxManager != null)
            {
                radAjaxManager.AjaxRequest += new Telerik.Web.UI.RadAjaxControl.AjaxRequestDelegate(RadAjaxManager_AjaxRequest);
                radAjaxManager.AjaxSettings.AddAjaxSetting(radAjaxManager, grdPracticeCatalog);
                radAjaxManager.AjaxSettings.AddAjaxSetting(radAjaxManager, RadInputManager1);

                // add update trigger for search
                radAjaxManager.AjaxSettings.AddAjaxSetting(PracticeCatalogSearch.Toolbar, RadInputManager1);

                // find banner ads from SiteMaster parent
                //var m = this.Page.Master;
                //while ((m != null) && !(m is MasterPages.SiteMaster))
                //    m = m.Master;
                //if (m != null)
                //{
                //    //var h_banner = ((MasterPages.SiteMaster)m).HorizontalBannerAd;
                //    //var v_banner = ((MasterPages.SiteMaster)m).VerticalBannerAd;

                //    //if (h_banner != null)
                //    //    h_banner.UpdateTimer.Enabled = false;
                //    //if (v_banner != null)
                //    //    v_banner.UpdateTimer.Enabled = false;

                //    //if (h_banner != null)
                //    //    radAjaxManager.AjaxSettings.AddAjaxSetting(h_banner.UpdateTimer, RadInputManager1);
                //    //if (v_banner != null)
                //    //    radAjaxManager.AjaxSettings.AddAjaxSetting(v_banner.UpdateTimer, RadInputManager1);
                //}
            }
        }

        public bool WriteAjaxScript()
        {
            if (!Context.User.IsInRole("BregAdmin"))
            {
                if (Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page) != null)
                    return true;
            }
            return false;
        }

        void RadAjaxManager_AjaxRequest(object sender, Telerik.Web.UI.AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                //int index = -1;
                List<int> indexes = new List<int>();
                foreach (Telerik.Web.UI.GridDataItem row in grdPracticeCatalog.MasterTableView.Items)
                {
                    if (row.Expanded == true)
                    {
                        indexes.Add(row.ItemIndex);
                    }
                }
                grdPracticeCatalog.Rebind();

                foreach (int index in indexes)
                {
                    grdPracticeCatalog.MasterTableView.Items[index].Expanded = true;
                }
            }
        }

		void PracticeCatalogSearch_searchEvent(object sender, global::BregVision.UserControls.General.SearchEventArgs e)
		{
			grdPracticeCatalog.SearchText = e.SearchText;
			grdPracticeCatalog.SearchCriteria = e.SearchCriteria;
			grdPracticeCatalog.SearchType = e.SearchType;
			grdPracticeCatalog.SetSearchCriteria(e.SearchType);
		}

		void MasterCatalogSearch_searchEvent(object sender, global::BregVision.UserControls.General.SearchEventArgs e)
		{
			grdMasterCatalog.SearchText = e.SearchText;
			grdMasterCatalog.SearchCriteria = e.SearchCriteria;
			grdMasterCatalog.SearchType = e.SearchType;
			grdMasterCatalog.SetSearchCriteria(e.SearchType);
		}

		protected void grdMasterCatalog_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
		{
			grdMasterCatalog.SetExpansionOnPageIndexChanged();

		}

		protected void grdPracticeCatalog_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            grdPracticeCatalog.SetExpansionOnPageIndexChanged();
        }


		protected void grdMasterCatalog_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
		{
			if (!e.IsFromDetailTable || TelerikAjax.UIHelper.IsGridRebindReasonSet(e.RebindReason, Telerik.Web.UI.GridRebindReason.InitialLoad))
				grdMasterCatalog.DataSource = DAL.Practice.Catalog.GetMasterCatalogSuppliers();
		}

        private DAL.Practice.Catalog _catalogInstance = new DAL.Practice.Catalog();
		protected void grdMasterCatalog_OnDetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {
            _IsDetailTableBinding = true;

            var data = _catalogInstance.GetMasterCatalogDataBySupplier2(grdMasterCatalog.SearchCriteria, grdMasterCatalog.SearchText, e.DetailTableView.ParentItem.GetColumnInt32("SupplierID"));
            e.DetailTableView.DataSource = data;
            grdMasterCatalog.SetRecordCount(data.Count(x => !string.IsNullOrEmpty(x.ShortName)));
            grdMasterCatalog.SetGridLabel(grdMasterCatalog.SearchCriteria, Page.IsPostBack, lblMCStatus);
        }

        protected void grdPracticeCatalog_OnDetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {
            _IsDetailTableBinding = true;

            var data = DAL.Practice.Catalog.GetPracticeCatalogDataBySupplier2(grdPracticeCatalog.SearchCriteria, grdPracticeCatalog.SearchText, practiceID, e.DetailTableView.ParentItem.GetColumnInt32("PracticeCatalogSupplierBrandID"));
            e.DetailTableView.DataSource = data;
            e.DetailTableView.GetColumn("IsSideExempt").Visible = DAL.Practice.Practice.IsLateralityRequired(practiceID); // show  IsSideExempt if islaterailtyRequired
            grdPracticeCatalog.SetRecordCount(data.Count(x => !string.IsNullOrEmpty(x.ShortName)));
            grdPracticeCatalog.SetGridLabel(grdPracticeCatalog.SearchCriteria, Page.IsPostBack, lblPCStatus);
        }


		protected void grdMasterCatalog_PreRender(object sender, EventArgs e)
		{
			grdMasterCatalog.ExpandGrid();
		}

		protected void grdPracticeCatalog_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
		{
			ShowProductReplacement = false;

            //  If the update (edit) command is being called, clear all insert items.
            if (e.CommandName == RadGrid.EditCommandName)
            {
				if (e.CommandSource is System.Web.UI.Control)
				{
					System.Web.UI.Control editControl = (System.Web.UI.Control)(e.CommandSource);
					if (editControl.ID == "ProductReplacementActive" || editControl.ID == "ProductReplacementInactive")
					{
						ShowProductReplacement = true;
					}
				}
			}

			//if (e.CommandName == "btnHCPCs")
			//{
			//    try
			//    {

			//        Telerik.Web.UI.GridDataItem item = e.Item as Telerik.Web.UI.GridDataItem;

			//        string PCID = item["PracticeCatalogProductID"].Text;
			//        string HCPCsURL = string.Concat("~/Admin/HCPCs.aspx?PCID=", PCID);

			//        rwmHCPCs.Windows[0].NavigateUrl = HCPCsURL;
			//        rwmHCPCs.Windows[0].VisibleOnPageLoad = true;
			//        //Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page).EnableAJAX = false; //MWS this would work but makes page really slow without ajax

			//        //Get the values from the cells using the columnUniqueName   
			//    }
			//    catch
			//    {

			//    }

			//}
			//else
			//{
			//    rwmHCPCs.Windows[0].VisibleOnPageLoad = false;
			//}
		}

		protected void grdMasterCatalog_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
		{
			if (e.CommandName == "Edit")
			{
				try
				{
					Telerik.Web.UI.GridDataItem item = e.Item as Telerik.Web.UI.GridDataItem;
				}
				catch
				{
				}
			}
		}

		protected void grdPracticeCatalog_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable || TelerikAjax.UIHelper.IsGridRebindReasonSet(e.RebindReason, Telerik.Web.UI.GridRebindReason.InitialLoad))
                grdPracticeCatalog.DataSource = DAL.Practice.Catalog.GetPracticeCatalogSuppliers(grdPracticeCatalog.SearchCriteria, grdPracticeCatalog.SearchText, practiceID);
        }

        protected void grdPracticeCatalog_PreRender(object sender, EventArgs e)
        {
            grdPracticeCatalog.ExpandGrid();

            lblDeleteMessage.Text = deleteMessage;

			//grdPracticeCatalog.MasterTableView.DetailTables[0].GetColumn("btnReplacement").Visible = Context.User.IsInRole("BregAdmin");
		}

        //protected void grdPracticeCatalog_Refresh()
        //{
            
        //}

        private void MoveToPracticeCatalog(Telerik.Web.UI.GridTableView gridTableView)
        {
            intPCItems = 0;
            foreach (Telerik.Web.UI.GridNestedViewItem nestedViewItem in gridTableView.GetItems(Telerik.Web.UI.GridItemType.NestedView))
            //foreach (GridNestedViewItem nestedViewItem in gridTableView.GetItems(GridItemType.SelectedItem))
            {
                if (nestedViewItem.NestedTableViews.Length > 0)
                {
                    //foreach (GridDataItem gridDataItem in nestedViewItem.)
                    foreach (Telerik.Web.UI.GridDataItem gridDataItem in nestedViewItem.NestedTableViews[0].Items)
                    {

                        if (gridDataItem.Selected == true)
                        {
                            Int32 masterCatalogProductID = gridDataItem.GetColumnInt32("Mastercatalogproductid");
                            var practiceCatalogProductID = DAL.Practice.Catalog.UpdatePracticeCatalog(practiceID, masterCatalogProductID, userID, chkMoveModifiers.Checked);
                            CopyMcHcpcsToPc(masterCatalogProductID, practiceCatalogProductID);
                            intPCItems += 1;
                            //Also updates inventory level for checked locations

                            foreach (Telerik.Web.UI.RadComboBoxItem item in cboPracticeLocation.Items)
                            {
                                Int32 checkedPracticeLocationID = 0;
                                CheckBox checkBoxItem = item.FindControl("chkLocation") as CheckBox;

                                if (checkBoxItem != null && checkBoxItem.Checked == true)
                                {
                                    Int32.TryParse(item.Value, out checkedPracticeLocationID);
                                    if (checkedPracticeLocationID > 0)
                                    {
                                        Int32 ret = DAL.PracticeLocation.Inventory.TransferToProductInventory(checkedPracticeLocationID, practiceCatalogProductID, 1);
                                    }
                                }
                            }
                           
                        }
                    }
                }
            }
            if (intPCItems > 0)
                grdPracticeCatalog.Rebind();

            AddToInventoryCompletedLabel.Visible = true;
            SetPCStatusLabel("Item(s) added to Practice Catalog: {0}", intPCItems.ToString());
        }

        private void CopyMcHcpcsToPc(int masterCatalogProductID, int practiceCatalogProductID)
        {
            if (chkMoveHcpcs.Checked == true)
            {
                DAL.Practice.Catalog.CopyMcHcpcsToPc(masterCatalogProductID, practiceCatalogProductID, userID);
            }
        }

       

        protected void btnMoveToPC_Click(object sender, EventArgs e)
        {
            MoveToPracticeCatalog(grdMasterCatalog.MasterTableView);
        }



        private void UpdatePracticeCatalog(Telerik.Web.UI.GridTableView gridTableView)
        {
            int totalPCItemsUpdate = 0;
            int userID = GetUserID();


            foreach (Telerik.Web.UI.GridNestedViewItem nestedViewItem in gridTableView.GetItems(Telerik.Web.UI.GridItemType.NestedView))
            //foreach (GridNestedViewItem nestedViewItem in gridTableView.GetItems(GridItemType.SelectedItem))
            {
                if (nestedViewItem.NestedTableViews.Length > 0)
                //if (nestedViewItem.Expanded == true)  //jb 2008.02.28
                {
                    //Control gcsc = (Control)nestedViewItem.FindControl("ClientSelectColumn2");
                    //foreach (GridDataItem gridDataItem in nestedViewItem.)
                    foreach (Telerik.Web.UI.GridDataItem gridDataItem in nestedViewItem.NestedTableViews[0].Items)
                    {
                        var practiceCatalogProductID = gridDataItem.GetColumnInt32("PracticeCatalogProductID");
                        if (gridDataItem.Selected && (practiceCatalogProductID > 0))
                        {
                            //Add to 
                            Int32 return_Value = DAL.Practice.Catalog.UpdatePricing_PracticeCatalog(practiceCatalogProductID,
                                                                                                    gridDataItem.GetColumnTextDecimal("txtWholesaleCost"),
                                                                                                    gridDataItem.GetColumnTextDecimal("txtBillingCharge"),
                                                                                                    gridDataItem.GetColumnTextDecimal("txtBillingChargeCash"),
                                                                                                    gridDataItem.GetColumnTextDecimal("txtDMEDeposit"),
                                                                                                    gridDataItem.GetColumnSelectedValue("ddlMod1"),
                                                                                                    gridDataItem.GetColumnSelectedValue("ddlMod2"),
                                                                                                    gridDataItem.GetColumnSelectedValue("ddlMod3"));
                            intPCItems += 1;

                            //Update quantities
                            return_Value = DAL.Practice.Catalog.UpdateInventoryLevels_PracticeCatalog(practiceCatalogProductID,
                                                                                                        gridDataItem.GetColumnTextInt32("txtStockingUnits"),
                                                                                                        practiceID, userID);


                           
                            DAL.Practice.Catalog.UpdateCatalogItem(practiceCatalogProductID,
                                                                    gridDataItem.GetColumnChecked("chkIsLogoAblePart"),
                                                                    gridDataItem.GetColumnChecked("chkIsSplitCode"),
                                                                    gridDataItem.GetColumnChecked("chkIsAlwaysOffTheShelf"),
                                                                    gridDataItem.GetColumnChecked("chkIsPreAuthorization"),
                                                                    gridDataItem.GetColumnChecked("chkIsSideExempt")
                                                                    );
                            //Update HCPCs from comma separated list
                            AddUpdateHcpcs(gridDataItem, practiceCatalogProductID);

                            //Update BillingChargeOTS
                            AddUpdateOffTheShelfBillingCharge(gridDataItem, practiceCatalogProductID);
                        }
                        gridDataItem.Selected = false;
                    }
                    totalPCItemsUpdate += intPCItems;
                    intPCItems = 0;
                }

            }

            SetPCStatusLabel("Number of products updated: {0}", totalPCItemsUpdate.ToString());
        }

        private int GetUserID()
        {
            int userID = 0;

            if (Context.User.IsInRole("BregAdmin"))
            {
                userID = -99;               
            }
            else
            {
                BLL.Practice.User user = new BLL.Practice.User();
                DataTable userData = user.SelectUser(Context.User.Identity.Name.ToString());
                if (userData != null)
                {
                    if (userData.Rows.Count == 1)
                    {
                        DataRow currentUser = userData.Rows[0];
                        userID = Convert.ToInt32(currentUser["UserID"]);                        
                    }
                }
            }
            return userID;
        }

        private void AddUpdateHcpcs(Telerik.Web.UI.GridDataItem gridDataItem, Int32 practiceCatalogProductId)
        {
            var hcpcs = gridDataItem.GetColumnText("txtHcpcs");

            DAL.Practice.Catalog.SavePracticeCatalogHcpcs(practiceCatalogProductId, hcpcs, userID, isOffTheShelf: false);

            var isSplitCodeProduct = gridDataItem.GetColumnChecked("chkIsSplitCode");
            if (isSplitCodeProduct)
            {
                hcpcs = gridDataItem.GetColumnText("txtHcpcsOTS");
            }
            else
            {
                hcpcs = string.Empty;
            }
            DAL.Practice.Catalog.SavePracticeCatalogHcpcs(practiceCatalogProductId, hcpcs, userID, isOffTheShelf: true);
        }

        private void AddUpdateOffTheShelfBillingCharge(Telerik.Web.UI.GridDataItem gridDataItem, Int32 practiceCatalogProductId)
        {
            var isSplitCodeProduct = gridDataItem.GetColumnChecked("chkIsSplitCode");

            decimal billingChargeOTS;
            if (isSplitCodeProduct)
            {
                billingChargeOTS = gridDataItem.GetColumnTextDecimal("txtBillingChargeOTS");
                DAL.Practice.Catalog.SavePracticeCatalogBillingOTS(practiceCatalogProductId, billingChargeOTS);
            }
            else
            {
                DAL.Practice.Catalog.DeletePracticeCatalogBillingOTS(practiceCatalogProductId);
            }
            
        }

        private void SetPCStatusLabel(string message, string totalPCItemsUpdate)
        {
            string strPCStatus = String.Format(message, totalPCItemsUpdate);
            lblPCStatus.Text = strPCStatus;
        }


        protected void grdPracticeCatalog_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            // check IsDeleted to show the garbage can (delete button).
            if (e.Item is Telerik.Web.UI.GridDataItem)
            {
                if (e.Item.OwnerTableView.Name == "MasterTableView_PracticeCatalog")
                {
                    if (e.Item.HasChildItems == true)
                    {
                        e.Item.Visible = true;
                    }
                    else
                    {
                        e.Item.Visible = false;
                    }

                }
                if (e.Item.OwnerTableView.Name == "GridTableView_PracticeCatalog")
                {

                    Telerik.Web.UI.GridDataItem dataItem = e.Item as Telerik.Web.UI.GridDataItem;
                    try
                    {
                        //  20071022 JB

                        int isDeletable = dataItem.GetColumnInt32("IsDeletable");

                        string productCode = dataItem.GetColumnString("Code");
                        string productName = dataItem.GetColumnString("ShortName");

                        #region Clean Product Code
                        productCode = productCode.Replace("\\", "/");
                        productCode = productCode.Replace("\'", "\\\'");
                        productCode = productCode.Replace("\"", "\\\"");
                        #endregion
                        #region Clean Product Name
                        productName = productName.Replace("\\", "/");
                        productName = productName.Replace("\'", "\\\'");
                        productName = productName.Replace("\"", "\\\"");
                        #endregion
                        DAL.Practice.Catalog.PracticeCatalogProductInfo catalogItem = dataItem.DataItem as DAL.Practice.Catalog.PracticeCatalogProductInfo;
                        CheckBox chkIsLogoPartItem = dataItem["IsLogoAblePartPcp"].FindControl("chkIsLogoAblePart") as CheckBox;

                        if(catalogItem != null && chkIsLogoPartItem != null)
                        {
                            //if mcp is logoable, then they can check or uncheck pcp is logoable
                            chkIsLogoPartItem.Enabled = catalogItem.McpIsLogoAblePart;
                            if (catalogItem.McpIsLogoAblePart == false)
                            {
                                chkIsLogoPartItem.Checked = false;
                            }
                        }

                        

                        string stringOnClickMessage = @" Delete from Practice Catalog?\n\n Product:  "
                            + productName + @"\n Code:     "
                            + productCode;

                        ImageButton btnDelete = (ImageButton)dataItem["DeleteColumn"].FindControl("btnDelete");

                        btnDelete.ToolTip = "Delete Product from Practice Catalog.";
                        btnDelete.Enabled = true;
                        btnDelete.OnClientClick = @"if(!confirm('" + stringOnClickMessage + "'))return false;";


                        if (isDeletable == 1)
                        {
                            btnDelete.ImageUrl = "~/App_Themes/Breg/images/Common/delete.png";
                        }
                        else
                        {
                            if (Context.User.IsInRole("BregAdmin"))
                            {
                                stringOnClickMessage = @" Delete from Practice Catalog?\n\n This product has existing inventory on-hand quantities.\n" +
                                                                @" Please confirm that you wish to zero on-hand quantities automatically;\n" +
                                                                @" otherwise, please cancel and resolve on-hand quantities manually\n before trying again. \n\n Product:  "
                                                                + productName + @"\n Code:     "
                                                                + productCode;
                                btnDelete.OnClientClick = @"if(!confirm('" + stringOnClickMessage + "'))return false;";
                                btnDelete.ImageUrl = "~/App_Themes/Breg/images/Common/delete-unsafe.png";
                            }
                            else
                            {
                                btnDelete.Visible = false;
                            }
                        }
                    }
                    catch { }


                    string PCID = dataItem["PracticeCatalogProductID"].Text;
                    string HCPCsURL = string.Concat("~/Admin/HCPCs.aspx?PCID=", PCID);

                    //HyperLink btnHCPCs = dataItem.FindControl("btnHCPCs") as HyperLink;
                    //btnHCPCs.Attributes["href"] = "#";
                    //btnHCPCs.Attributes["onclick"] = String.Format("return ShowEditForm('{0}','{1}');", PCID, e.Item.ItemIndex.ToString());

                    //btnHCPCs.ResolveUrl();

                }
            }
        }

        protected void grdPracticeCatalog_DeleteCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            //delete item
            if (e.Item is Telerik.Web.UI.GridDataItem)
            {
                int removalType = -1;
                string dependencyMessage = "";

                Telerik.Web.UI.GridDataItem dataItem = e.Item as Telerik.Web.UI.GridDataItem;
                Int32 practiceCatalogProductID = dataItem.GetColumnInt32("PracticeCatalogProductID");
                string productCode = dataItem.GetColumnString("Code");
                string productName = dataItem.GetColumnString("ShortName");
                #region Clean Product Info
                productCode = productCode.Replace("\\", "/");
                productName = productName.Replace("\\", "/");
                #endregion

                ////Next line calls function to delete item from cart
                PracticeCatalogProduct_Delete(practiceCatalogProductID, out removalType, out dependencyMessage);

                string productNameCode = "<br />  Product: " + productName + "<br />  Code: " + productCode;

                // move this out of the ajaxpanel.

                switch (removalType)
                {
                    case -1:
                        { 
                            //  do nothing
                            deleteMessage = ConfigurationManager.AppSettings["PCPDeleteError"].ToString();
                            deleteMessage += productNameCode;
                            break;
                        }
                    case 0:
                        {
                            //Show dependencies.
                            deleteMessage = ConfigurationManager.AppSettings["PCPDeleteNotRemoved"].ToString();

                            string deleteDependencies = "";

                            deleteMessage += deleteDependencies.ToString() + productNameCode;
                            break;
                        }
                    case 1:
                        {
                            //  removed.  hard delete.
                            deleteMessage = ConfigurationManager.AppSettings["PCPDeleted"].ToString();
                            deleteMessage += productNameCode;
                            break;
                        }
                    case 2:
                        {
                            //  removed.  soft delete.
                            deleteMessage = ConfigurationManager.AppSettings["PCPRemoved"].ToString();
                            deleteMessage += productNameCode;
                            break;
                        }
                    default:
                        {
                            //  do nothing
                            break;
                        }
                }
                lblDeleteMessage.Text = deleteMessage.ToString();

            }
        }





        // Events
        protected void btnUpdatePC_Click(object sender, EventArgs e)
        {
            UpdatePracticeCatalog(grdPracticeCatalog.MasterTableView);
        }

        protected void PracticeCatalogProduct_Delete(int practiceCatalogProductID, out int removalType, out string dependencyMessage)
        {
            // Delete the productInventoryID from Inventory.
            bool isAdmin = Context.User.IsInRole("BregAdmin");
            DAL.Practice.Catalog dalCatalog = new DAL.Practice.Catalog();
            dalCatalog.Delete(practiceCatalogProductID, userID, out removalType, out dependencyMessage, isAdmin);

            int junk = removalType;
            string junkstring = dependencyMessage;

        }


        protected void txtRecordsDisplayed_TextChanged(object sender, EventArgs e)
        {

            recordsDisplayedPerPage = Convert.ToInt32(txtRecordsDisplayed.Text);

            grdMasterCatalog.PageSize = recordsDisplayedPerPage;
            grdPracticeCatalog.PageSize = recordsDisplayedPerPage;

            grdMasterCatalog.Rebind();
            grdPracticeCatalog.Rebind();

        }





        public DataSet GetMasterCatalogDataBySupplier(string SearchCriteria, string SearchText)
        {
            if (grdMasterCatalog.VisionDataSet == null)
            {
                grdMasterCatalog.VisionDataSet = DAL.Practice.Catalog.GetMasterCatalogDataBySupplier(SearchCriteria, SearchText);
            }

            return grdMasterCatalog.VisionDataSet;
        }




        public DataSet GetPracticeCatalogDataBySupplier(string SearchCriteria, string SearchText)
        {
            if (grdPracticeCatalog.VisionDataSet == null)
            {
                grdPracticeCatalog.VisionDataSet = DAL.Practice.Catalog.GetPracticeCatalogDataBySupplier1(SearchCriteria, SearchText, practiceID);
            }

            return grdPracticeCatalog.VisionDataSet;
        }



        protected eSetupStatus ValidateSetupData()
        {
            return eSetupStatus.Complete;
        }

        /// <summary>
        /// IVisionWizardControl interface save.  Called by wizard control on tab change
        /// </summary>
        public void Save()
        {
            //do nothing.  Grid rows are saved individually

            SaveStatus(practiceID, -1, this.ToString(), ValidateSetupData());
        }

        protected void SplitCodeDependent_OnPreRender(object sender, EventArgs e)
        {
            var input = (System.Web.UI.WebControls.WebControl)sender;

            Int32 pcpID = Convert.ToInt32(input.Attributes["data-pcpid"]);

            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                IEnumerable<PracticeCatalogProduct> piQuery = (from pcp in visionDB.PracticeCatalogProducts
                                                               where pcp.PracticeCatalogProductID == pcpID
                                                               select pcp);

                bool isSplit = piQuery.First().IsSplitCode;

                if (isSplit)
                {
                    input.Attributes.Add("data-hidden", "false");
                }
                else
                {
                    input.Attributes.Add("data-hidden", "true");
                }
            }
        }
    }
}
