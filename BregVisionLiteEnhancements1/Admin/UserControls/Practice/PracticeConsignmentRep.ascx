﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeConsignmentRep.ascx.cs" Inherits="BregVision.Admin.UserControls.Practice.PracticeConsignmentRep" %>
<%@ Register src="../../../UserControls/ResetSaveButtonTable.ascx" tagname="ResetSaveButtonTable" tagprefix="uc1" %>
<table border="0" width="100%" cellpadding="0" cellspacing="0" summary="Summary Description">
    <tr align="center">
        <td>
            <table border="0" cellpadding="0" cellspacing="0">
                <!-- Content Header-->
                <tr class="AdminPageTitle">
                    <td align="center">
                        <asp:Label ID="lblContentHeader" runat="server" Text="Consignment Reps"></asp:Label>
                    </td>
                </tr>
                <!-- Grid -->
                <tr style="font-size: 11pt">
                    
                    
                  <td style="width: auto">
                        <!-- Content Header   table, tr, and td are not neccessary here  -->
                        <table style="width: auto" cellpadding="0" cellspacing="0">
                            <tr>
                                <td colspan="3" style="width: 496px">
                                    <asp:Label ID="lblConsignmentRep" runat="server" Font-Bold="True" Font-Size="Larger"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="AdminSubTitle" align="center">
                                    Master List
                                </td>
                                <td>
                                </td>
                                <td class="AdminSubTitle" align="center">
                                    Practice List
                                </td>
                            </tr>
                            
                            <tr>
                                <td style="width: 400px;" align="center">
                                    <asp:ListBox ID="listBoxGetConsignmentRepsAll" runat="server" Height="350px" Width="350px" SelectionMode="Multiple"
                                        Font-Names="Consolas"></asp:ListBox>
                                </td>
                                <td style="width: 100px;" align="center">
                                    <table>
                                        <tr align="center">
                                            <td>
                                                <asp:Button ID="btnAdd" runat="server" Text="Add >>" Width="100px" 
                                                    onclick="btnAdd_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr align="center">
                                            <td>
                                                <asp:Button ID="btnRemove" runat="server" Text="<< Remove" Width="100px" 
                                                    onclick="btnRemove_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width: 400px" align="center">
                                    <asp:ListBox ID="listBoxGetConsignmentPractice" runat="server" Height="350px" Width="350px"
                                        SelectionMode="Multiple" Font-Names="Consolas"></asp:ListBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <!--button-->                
                        <uc1:ResetSaveButtonTable ID="ResetSaveButtonTable1" runat="server" />            
                   </td>
                </tr>
            </table>
        </td>
    </tr>
</table>