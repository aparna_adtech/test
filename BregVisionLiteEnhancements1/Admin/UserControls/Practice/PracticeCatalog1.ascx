﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeCatalog1.ascx.cs" Inherits="BregVision.Admin.UserControls.Practice.PracticeCatalog1" %>
<%@ Register Src="~/BregAdmin/UserControls/MasterCatalogAdmin.ascx" TagName="MasterCatalogAdmin" TagPrefix="bvu"  %>
<%@ Register Src="~/Admin/UserControls/General/PracticeCatalogProduct.ascx" TagName="PracticeCatalogProduct" TagPrefix="bvu"  %>

<div>
    <table width="100%">
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td valign="top">
                <bvu:MasterCatalogAdmin ID="ctlMasterCatalogAdmin" IsPracticeSpecific="true" runat="server" />
            </td>
            <td>
            
            </td>
            <td valign="top">
                <bvu:PracticeCatalogProduct ID="ctlPracticeCatalogProduct" runat="server" />
            </td>
        </tr>
    </table>
</div>
