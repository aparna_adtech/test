﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace BregVision.Admin.UserControls.Practice
{
    public partial class PracticeCatalog1 : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HideColumn(2, "edit");
            HideColumn(1, "delete");
        }

        private void HideColumn(int columnIndex, string commandName)
        {
            if (ctlMasterCatalogAdmin.grdSuppliers.MasterTableView.DetailTables[0].Columns[columnIndex] is GridButtonColumn)
            {
                GridButtonColumn editColumn = ctlMasterCatalogAdmin.grdSuppliers.MasterTableView.DetailTables[0].Columns[columnIndex] as GridButtonColumn;
                if (editColumn.CommandName.ToLower() == commandName)
                {
                    editColumn.Visible = false;
                }
            }
        }
    }
}