﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeUsers.ascx.cs" Inherits="BregVision.Admin.UserControls.PracticeUsers" %>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server"></telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" InitialDelayTime="500" EnableAjaxSkinRendering="True" BackgroundPosition="Center" Skin="Breg" EnableEmbeddedSkins="False">
  <img src="../App_Themes/Breg/images/Common/loading.gif"/>
</telerik:RadAjaxLoadingPanel>
<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
  <script type="text/javascript">
    var userOnly1ClientID;
    var userOnly2ClientID;
    var userOnly3ClientID;
    var userOnly4ClientID;
    var userOnly5ClientID;
    var userOnly6ClientID;
    var userOnly7ClientID;
    var userPermissionsClientID;

    function OnClientSelectedIndexChanged(sender, eventArgs) {
      var item = eventArgs.get_item();
      if (item.get_text() == "PracticeAdmin") {
        AdminItemsHideShow("hidden");
      } else if (item.get_text() == "PracticeLocationUser") {
        AdminItemsHideShow("visible");
      }
    }

    function AdminItemsHideShow(Visibility) {
      try {
        document.getElementById(userOnly1ClientID).style.visibility = Visibility;
        document.getElementById(userOnly2ClientID).style.visibility = Visibility;
        document.getElementById(userOnly3ClientID).style.visibility = Visibility;
        document.getElementById(userOnly4ClientID).style.visibility = Visibility;
        document.getElementById(userOnly5ClientID).style.visibility = Visibility;
        document.getElementById(userOnly6ClientID).style.visibility = Visibility;
        document.getElementById(userOnly7ClientID).style.visibility = Visibility;
        document.getElementById(userPermissionsClientID).style.visibility = Visibility;
      } catch (ex) {
      }
    }
  </script>
</telerik:RadScriptBlock>
<div class="content">
<div class="flex-row">
  <h2>
    <asp:Label ID="lblContentHeader" runat="server" Text="Users"></asp:Label>
    <bvu:Help ID="help33" runat="server" HelpID="33"/>
  </h2>
  <div style="display: none">
    <telerik:RadNumericTextBox ID="txtIamAHackToAllowTheEditFomTextboxToGetAStyle" runat="server" Skin="Breg" EnableEmbeddedSkins="False"></telerik:RadNumericTextBox>
    <telerik:RadComboBox ID="ddlIamAHackToAllowTheEditFomTextboxToGetAStyle" runat="server" Skin="Breg" EnableEmbeddedSkins="False"></telerik:RadComboBox>
  </div>
</div>
<div>
  <asp:Label ID="lblStatus" runat="server"></asp:Label>
</div>
<bvu:VisionRadGridAjax ID="grdPracticeUser" runat="server" EnableLinqExpressions="false" AllowMultiRowSelection="True" OnDeleteCommand="grdPracticeUser_DeleteCommand" OnItemCommand="grdPracticeUser_OnItemCommand" OnItemDataBound="grdPracticeUser_OnItemDataBound" OnNeedDataSource="grdPracticeUser_NeedDataSource" OnPageIndexChanged="grdPracticeUser_PageIndexChanged" OnUpdateCommand="grdPracticeUser_UpdateCommand" PageSize="100" Skin="Breg" EnableEmbeddedSkins="False" AddNonBreakingSpace="False">
<ClientSettings AllowDragToGroup="false" AllowColumnsReorder="false" ReorderColumnsOnClient="false">
  <Selecting AllowRowSelect="True"></Selecting>
</ClientSettings>
<GroupPanel Visible="True" Width="100%"></GroupPanel>
<MasterTableView CommandItemDisplay="Top" CommandItemSettings-AddNewRecordText="New User" DataKeyNames="UserName">
<CommandItemSettings AddNewRecordText="New User" ExportToPdfText="Export to Pdf"></CommandItemSettings>
<RowIndicatorColumn>
  <HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>
<ExpandCollapseColumn>
  <HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>
<Columns>
  <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn">
    <HeaderStyle Width="35px"></HeaderStyle>
    <ItemStyle HorizontalAlign="Center"></ItemStyle>
  </telerik:GridClientSelectColumn>
  <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" ConfirmText="Delete this user?" HeaderText="Delete" ImageUrl="~/App_Themes/Breg/images/Common/delete.png" Text="Delete" UniqueName="Delete">
    <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
    <ItemStyle HorizontalAlign="Center"></ItemStyle>
  </telerik:GridButtonColumn>
  <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Activate" ConfirmText="Activate this user?" HeaderText="Activate" ImageUrl="~/App_Themes/Breg/images/Common/verified-user.png" Text="Activate" UniqueName="Activate">
    <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
    <ItemStyle HorizontalAlign="Center"></ItemStyle>
  </telerik:GridButtonColumn>
  <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" HeaderText="Edit" ImageUrl="~/App_Themes/Breg/images/Common/edit.png" Text="Edit" Visible="true" UniqueName="Edit">
    <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
    <ItemStyle HorizontalAlign="Center"></ItemStyle>
  </telerik:GridButtonColumn>
  <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Unlock" HeaderText="Unlock" ImageUrl="~/App_Themes/Breg/images/Common/lock-open.png" Visible="true" UniqueName="Unlock">
    <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
    <ItemStyle HorizontalAlign="Center"></ItemStyle>
  </telerik:GridButtonColumn>
  <telerik:GridBoundColumn DataField="UserName" HeaderText="UserName" UniqueName="UserName"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="Email" HeaderText="Email" UniqueName="Email"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="UserRole" HeaderText="UserRole" UniqueName="UserRole"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="ShowProductInfo" HeaderText="ShowProductInfo" UniqueName="ShowProductInfo" Visible="false"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="AllowDispense" HeaderText="AllowDispense" UniqueName="AllowDispense" Visible="false"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="AllowInventory" HeaderText="AllowInventory" UniqueName="AllowInventory" Visible="false"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="AllowCheckIn" HeaderText="AllowCheckIn" UniqueName="AllowCheckIn" Visible="false"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="AllowReports" HeaderText="AllowReports" UniqueName="AllowReports" Visible="false"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="AllowCart" HeaderText="AllowCart" UniqueName="AllowCart" Visible="false"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="AllowDispenseModification" HeaderText="AllowDispenseModification" UniqueName="AllowDispenseModification" Visible="false"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="AllowPracticeUserOrderWithoutApproval" HeaderText="AllowPracticeUserOrderWithoutApproval" UniqueName="AllowPracticeUserOrderWithoutApproval" Visible="false"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="ScrollEntirePage" HeaderText="Scroll Entire Page" UniqueName="ScrollEntirePage" Visible="false"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="DispenseQueueSortDescending" HeaderText="Dispense Queue Sorts in Descending Order" UniqueName="DispenseQueueSortDescending" Visible="false"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="AllowInventoryClose" HeaderText="Allowed to Close an Inventory Cycle" UniqueName="AllowInventoryClose" Visible="false"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="DefaultLocationID" HeaderText="DefaultLocationID" UniqueName="DefaultLocationID" Visible="false"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="DefaultRecordsPerPage" HeaderText="DefaultRecordsPerPage" UniqueName="DefaultRecordsPerPage" Visible="false"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="DefaultDispenseSearchCriteria" HeaderText="DefaultDispenseSearchCriteria" UniqueName="DefaultDispenseSearchCriteria" Visible="false"></telerik:GridBoundColumn>
  <telerik:GridEditCommandColumn InsertText="New User" UniqueName="grdEditCommandColumn5" Visible="false"></telerik:GridEditCommandColumn>
  <telerik:GridBoundColumn DataField="AspNet_UserID" HeaderText="AspNet_UserID" UniqueName="AspNet_UserID" Visible="false"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="UserID" HeaderText="UserID" UniqueName="UserID" Visible="false"></telerik:GridBoundColumn>
  <telerik:GridBoundColumn DataField="IsActive" HeaderText="IsActive" UniqueName="IsActive" Visible="false"></telerik:GridBoundColumn>
</Columns>
<EditFormSettings EditFormType="Template">
  <EditColumn CancelText="Cancel" UniqueName="EditCommandColumn1" UpdateText="Update"></EditColumn>
  <FormTemplate>
    <asp:Label ID="lblStatus" runat="server"></asp:Label>
    <div class="buttons-container" style="width: 95%; margin: 0 auto 10px;">
      <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" CssClass="button-accent"/>
      <asp:Button ID="btnUpdate" runat="server" CommandName="Update" Text="Update" ValidationGroup="PracticeGroup" CssClass="button-primary"/>
    </div>
    <div class="flex-row align-start justify-start admin-editUser">
      <div class="fifth">
        <table>
          <tr>
            <td>
              <span class="FieldLabel">User Name</span>
              <asp:TextBox ID="txtUserName" runat="server" Enabled="false" TabIndex="1" Text='<%# Bind("UserName") %>'> </asp:TextBox>
              <asp:RequiredFieldValidator ID="txtUserNameValidator" runat="server" ValidationGroup="PracticeGroup" CssClass="validation-error-message" Display="Dynamic" ControlToValidate="txtUserName" ErrorMessage="Please choose an User Name"></asp:RequiredFieldValidator>
            </td>
          </tr>
          <tr>
            <td>
              <span class="FieldLabel">Email</span>
              <span>
                  <asp:TextBox ID="txtEmail" runat="server" TabIndex="2" Text='<%# Bind("Email") %>'> </asp:TextBox>
                  <asp:RequiredFieldValidator ID="validEmailRequired" runat="server" ValidationGroup="PracticeGroup" CssClass="validation-error-message" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Please enter an email address."> </asp:RequiredFieldValidator>
                  <asp:RegularExpressionValidator ID="validEmailRegExp" runat="server" ValidationGroup="PracticeGroup" CssClass="validation-error-message" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Please enter a valid email address." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
              </span>
            </td>
          </tr>
          <tr id="trOldPassword" runat="server" visible="False">
            <td>
              <asp:Label ID="lblPasswordInstructions" runat="server" Text="To change the password, type a new one.  Leave blank to keep current." Visible="false"></asp:Label>
              <asp:TextBox ID="txtOldPassword" runat="server" TabIndex="1" TextMode="Password"> </asp:TextBox>
            </td>
          </tr>
          <tr>
            <td>
              <span class="FieldLabel">New Password</span>
              <asp:TextBox ID="txtPassword" runat="server" TabIndex="1" TextMode="Password"> </asp:TextBox>
              <asp:RequiredFieldValidator ID="txtPasswordValidator" runat="server" ValidationGroup="PracticeGroup" CssClass="validation-error-message" Display="Dynamic" ControlToValidate="txtPassword" ErrorMessage="Please enter a password"></asp:RequiredFieldValidator>
            </td>
          </tr>
          <tr>
            <td>
              <span class="FieldLabel">Confirm New Password</span>
              <asp:TextBox ID="txtPasswordCompare" runat="server" TabIndex="1" TextMode="Password"> </asp:TextBox>
              <asp:RequiredFieldValidator ID="txtPasswordCompareValidator" runat="server" ValidationGroup="PracticeGroup" CssClass="validation-error-message" Display="Dynamic" ControlToValidate="txtPasswordCompare" ErrorMessage="Please confirm your password"></asp:RequiredFieldValidator>
              <asp:CompareValidator ID="PasswordValidator" runat="server" ValidationGroup="PracticeGroup" ControlToCompare="txtPassword" Display="Dynamic" ControlToValidate="txtPasswordCompare" ErrorMessage="The password and compare password do not match"></asp:CompareValidator>
            </td>
          </tr>
          <tr>
            <td>
              <span class="FieldLabel">Role</span>
              <div class="flex-row">
                <telerik:RadComboBox ID="ddlRole" runat="server" AllowCustomText="false" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged" TabIndex="3" ></telerik:RadComboBox>
                <bvu:Help ID="help34" runat="server" HelpID="34"/>
              </div>
            </td>
          </tr>
        </table>
      </div>
      <div class="fifth">
        <table>
          <tr>
            <td>
              <span class="FieldLabel">Default Location</span>
              <telerik:RadComboBox ID="ddlDefaultLocation" runat="server" AllowCustomText="false" TabIndex="10"></telerik:RadComboBox>
              <bvu:Help ID="help36" runat="server" HelpID="36"/>
            </td>
          </tr>
          <tr>
            <td >
                <div >
              <span class="FieldLabel">Default Records per Page</span>
                
              <telerik:RadNumericTextBox ID="txtDefaultRecordsPerPage" runat="server" Culture="English (United States)" IncrementSettings-Step="5" MaxValue="100" MinValue="1" NumberFormat-DecimalDigits="0"  ShowSpinButtons="False" TabIndex="11" Text='<%# Bind("DefaultRecordsPerPage") %>'>
              </telerik:RadNumericTextBox>
                <bvu:Help ID="help37" runat="server" HelpID="37"/>
                <div class="block">
                    <asp:RequiredFieldValidator ID="txtDefaultRecordsPerPageRequiredValidator" runat="server" CssClass="validation-error-message" ControlToValidate="txtDefaultRecordsPerPage" ErrorMessage="Please enter the default records to display per page for this user"/>   
                </div>            
             </div>
            </td>
          </tr>
          <tr>
            <td>
              <span class="FieldLabel">Default Dispense Search Criteria</span>
              <telerik:RadComboBox ID="ddlDefaultDispenseSearchCriteria" runat="server" AllowCustomText="false" TabIndex="12" Text="ProductCode">
                <Items>
                  <telerik:RadComboBoxItem ID="RadComboBoxItem3" runat="server" Text="Product Code" Value="Code"/>
                  <telerik:RadComboBoxItem ID="RadComboBoxItem5" runat="server" Text="Product Name" Value="Name"/>
                  <telerik:RadComboBoxItem ID="RadComboBoxItem2" runat="server" Text="Manufacturer" Value="Brand"/>
                  <telerik:RadComboBoxItem ID="RadComboBoxItem1" runat="server" Text="HCPCs" Value="HCPCs"/>
                </Items>
              </telerik:RadComboBox>
              <bvu:Help ID="help38" runat="server" HelpID="38"/>
            </td>
          </tr>
        </table>
      </div>
      <div class="fifth">
        <table>
          <tr>
            <td>
              <asp:CheckBox ID="chkShowProductInfo" runat="server" Checked='<%# Eval("ShowProductInfo") is System.DBNull ? false : Eval("ShowProductInfo") %>' TabIndex="4"/>
              <span>Show Product Info on Home Page</span>
            </td>
          </tr>
          <tr>
            <td>
              <asp:CheckBox ID="chkScrollEntirePage" runat="server" Checked='<%# Eval("ScrollEntirePage") is System.DBNull ? false : Eval("ScrollEntirePage") %>' TabIndex="10"/>
              <span>Scroll Entire Page</span>
            </td>
          </tr>
          <tr>
            <td>
              <asp:CheckBox ID="chkDispenseQueueSortDescending" runat="server" Checked='<%# Eval("DispenseQueueSortDescending") is System.DBNull ? false : Eval("DispenseQueueSortDescending") %>'/>
              <span>Dispense Queue Sorts in Descending Order</span>
            </td>
          </tr>
          <tr>
            <td>
              <asp:CheckBox ID="chkAllowInventoryClose" runat="server" Checked='<%# Eval("AllowInventoryClose") is System.DBNull ? false : Eval("AllowInventoryClose") %>'/>
              <span>Allowed to Close an Inventory Cycle</span>
            </td>
          </tr>
          <tr id="UserOnly7" runat="server">
            <td>
              <asp:CheckBox ID="chkAllowPracticeUserOrderWithoutApproval" runat="server" Checked='<%# Eval("AllowPracticeUserOrderWithoutApproval") is System.DBNull ? false : Eval("AllowPracticeUserOrderWithoutApproval") %>'/>
              <span>Allow user to order without Approval</span>
            </td>
          </tr>
        </table>
      </div>
      <div class="fifth">
        <table>
          <tr id="UserOnly1" runat="server">
            <td>
              <asp:CheckBox ID="chkAllowDispense" runat="server" Checked='<%# Eval("AllowDispense") is System.DBNull ? false : Eval("AllowDispense") %>' TabIndex="4"/>
              <span>Allow Dispense</span>
            </td>
          </tr>
          <tr id="UserOnly2" runat="server">
            <td>
              <asp:CheckBox ID="chkAllowInventory" runat="server" Checked='<%# Eval("AllowInventory") is System.DBNull ? false : Eval("AllowInventory") %>' TabIndex="5"/>
              <span>Allow Inventory</span>
            </td>
          </tr>
          <tr id="UserOnly3" runat="server">
            <td>
              <asp:CheckBox ID="chkAllowCheckIn" runat="server" Checked='<%# Eval("AllowCheckIn") is System.DBNull ? false : Eval("AllowCheckIn") %>' TabIndex="6"/>
              <span>Allow Check In</span>
            </td>
          </tr>
          <tr id="UserOnly4" runat="server">
            <td>
              <asp:CheckBox ID="chkAllowReports" runat="server" Checked='<%# Eval("AllowReports") is System.DBNull ? false : Eval("AllowReports") %>' TabIndex="7"/>
              <span>Allow Reports</span>
            </td>
          </tr>
          <tr id="UserOnly5" runat="server">
            <td>
              <asp:CheckBox ID="chkAllowCart" runat="server" Checked='<%# Eval("AllowCart") is System.DBNull ? false : Eval("AllowCart") %>' TabIndex="8"/>
              <span>Allow Shopping Cart</span>
            </td>
          </tr>
          <tr id="UserOnly6" runat="server">
            <td>
              <asp:CheckBox ID="chkAllowDispenseModification" runat="server" Checked='<%# Eval("AllowDispenseModification") is System.DBNull ? false : Eval("AllowDispenseModification") %>' TabIndex="9"/>
              <span>Allow Dispense Modification</span>
            </td>
          </tr>
        </table>
      </div>
      <div class="fifth">
        <table class="flex-grow">
          <tr id="UserPermissions" runat="server">
            <td>
              <div class="flex-row">
                <strong>Permissions | Practice Location</strong>
                <bvu:Help ID="help39" runat="server" HelpID="39"/>
              </div>
              <asp:CheckBoxList ID="cblPracticeLocationPermissions" runat="server" RepeatLayout="Table" TextAlign="Right"></asp:CheckBoxList>
            </td>
          </tr>
        </table>
      </div>
    </div>
    <br/>
  </FormTemplate>
</EditFormSettings>
</MasterTableView>
<ExportSettings>
  <Pdf PageBottomMargin="" PageFooterMargin="" PageHeaderMargin="" PageHeight="11in" PageLeftMargin="" PageRightMargin="" PageTopMargin="" PageWidth="8.5in"/>
</ExportSettings>
<PagerStyle Mode="NumericPages" PagerTextFormat="{4} | Displaying page {0} of {1}, items {2} to {3} of {5}"></PagerStyle>
<SelectedItemStyle BackColor="#EDCD01"></SelectedItemStyle>
<HeaderContextMenu EnableImageSprites="True" CssClass="GridContextMenu GridContextMenu_Web20"></HeaderContextMenu>
</bvu:VisionRadGridAjax>
</div>
