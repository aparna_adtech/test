﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeBillingAddress.ascx.cs"
  Inherits="BregVision.Admin.UserControls.PracticeBillingAddress" %>
<%@ Register Src="~/UserControls/ResetSaveButtonTable.ascx" TagName="ResetSaveButtonTable"
  TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/Address.ascx" TagName="Address" TagPrefix="UC_A" %>

<div class="content">
  <div class="flex-row">
    <h2 class="flex-row">
      <asp:Label ID="lblContentHeader" runat="server" Text="Centralized Bill To Address"></asp:Label>&nbsp;<bvu:Help ID="Help6" runat="server" HelpID="6" />
    </h2>
    <uc1:ResetSaveButtonTable ID="ucResetSaveButtonTable" runat="server"></uc1:ResetSaveButtonTable>
  </div>
  <div class="admin-form-element">
    <span id="lblBillToAddress" runat="server" class="FieldLabel">Bill to Address</span>
    <asp:RadioButtonList ID="rbl1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbl1_SelectedIndexChanged" ToolTip="Centralized: All locations share the same bill to address. Decentralized: each location has it's own bill to address." RepeatLayout="Flow" RepeatColumns="2" CellPadding="0" CellSpacing="0" RepeatDirection="Horizontal">
      <asp:ListItem>Centralized</asp:ListItem>
      <asp:ListItem>Decentralized</asp:ListItem>
    </asp:RadioButtonList>

    <bvu:Help ID="Help5" runat="server" HelpID="5" />
   </div>

  <asp:Panel ID="pnlBillingAddress" runat="server" Enabled="true">
    <UC_A:Address ID="ucPracticeBillingAddress" runat="server"></UC_A:Address>
  </asp:Panel>

 

</div>
