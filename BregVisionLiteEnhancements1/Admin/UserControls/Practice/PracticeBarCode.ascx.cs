﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Reporting.WebForms;
using Telerik.Web.UI;
using BregVision.Admin.PracticeSetup;

namespace BregVision.Admin.UserControls
{
    public partial class PracticeBarCode : PracticeSetupBase, IVisionWizardControl
    {

        ArrayList list = new ArrayList();

        Telerik.Web.UI.RadAjaxLoadingPanel defaultLoadingPanel = null;
        

        /// <summary>
        /// IVisionWizardControl Interface.
        /// </summary>
        public bool NeedsInitialBind { get; set; }

        protected void page_Init(object sender, EventArgs e)
        {
            
            //hack.  The control shouldn't know about the page it's on
            if (this.Page is PracticeSetup.PracticeSetup)
            {
                defaultLoadingPanel = ((PracticeSetup.PracticeSetup)this.Page).DefaultLoadingPanel;
            }
            else if (this.Page is BarCode)
            {
                BarCode BarCodePage = this.Page as BarCode;
                defaultLoadingPanel = BarCodePage.RadAjaxLoadingPanel1Panel;
            }

            PracticeCatalogSearch.Grid = grdPracticeCatalog;
            PracticeCatalogSearch.DefaultLoadingPanel = defaultLoadingPanel;
            PracticeCatalogSearch.searchEvent += new BregVision.UserControls.General.SearchEventHandler(PracticeCatalogSearch_searchEvent);

        }

        void PracticeCatalogSearch_searchEvent(object sender, BregVision.UserControls.General.SearchEventArgs e)
        {
            grdPracticeCatalog.SearchText = e.SearchText;
            grdPracticeCatalog.SearchCriteria = e.SearchCriteria;
            grdPracticeCatalog.SearchType = e.SearchType;
            grdPracticeCatalog.SetSearchCriteria(e.SearchType);

            ReportViewer1.Visible = false;
            ReportViewer1.Enabled = false;
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();

            grdPracticeCatalog.SetGridLabel("", false, lblMCStatus, true);//set the default text on the label

            grdPracticeCatalog.PageSize = recordsDisplayedPerPage;
            grdSummary.PageSize = recordsDisplayedPerPage;

        }

        private void InitializeAjaxSettings()
        {
            Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);
            if (radAjaxManager != null)
            {


                radAjaxManager.AjaxSettings.AddAjaxSetting(grdPracticeCatalog, grdPracticeCatalog, defaultLoadingPanel);
                radAjaxManager.AjaxSettings.AddAjaxSetting(grdSummary, grdSummary, defaultLoadingPanel);

                radAjaxManager.AjaxSettings.AddAjaxSetting(btnMoveToLabel, grdPracticeCatalog, defaultLoadingPanel);
                radAjaxManager.AjaxSettings.AddAjaxSetting(btnMoveToLabel, grdSummary, defaultLoadingPanel);
                radAjaxManager.AjaxSettings.AddAjaxSetting(btnMoveToLabel, lblMCStatus);
                radAjaxManager.AjaxSettings.AddAjaxSetting(btnMoveToLabel, lblPCStatus);

                radAjaxManager.AjaxSettings.AddAjaxSetting(btnMoveToMC, grdPracticeCatalog, defaultLoadingPanel);
                radAjaxManager.AjaxSettings.AddAjaxSetting(btnMoveToMC, grdSummary, defaultLoadingPanel);
                radAjaxManager.AjaxSettings.AddAjaxSetting(btnMoveToMC, lblMCStatus);
                radAjaxManager.AjaxSettings.AddAjaxSetting(btnMoveToMC, lblPCStatus);
            }
        }

        protected void grdSummary_OnLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                grdSummary.DataSource = new Object[] { };
                grdSummary.DataBind(); //Used to show a 'No Records Found' when the page initially loads
            }
        }


        protected void grdPracticeCatalog_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            grdPracticeCatalog.SetExpansionOnPageIndexChanged();
        }


        protected void grdPracticeCatalog_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            string searchCriteria = "";
            string searchText = "";

            searchCriteria = grdPracticeCatalog.SearchCriteria;
            searchText = grdPracticeCatalog.SearchText;

            if (!e.IsFromDetailTable)
            {
                grdPracticeCatalog.DataSource = GetPracticeCatalogSuppliers();

            }

            grdPracticeCatalog.MasterTableView.DetailTables[0].DataSource = GetPracticeCatalogDataBySupplier(searchCriteria, searchText);
            grdPracticeCatalog.SetRecordCount((DataSet)grdPracticeCatalog.MasterTableView.DetailTables[0].DataSource, "ShortName");
            grdPracticeCatalog.SetGridLabel(searchCriteria, Page.IsPostBack, lblMCStatus);
            
        }

        protected void grdPracticeCatalog_OnDetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {
            TelerikAjax.UIHelper.FilterOnJoinKey(e, "PracticeCatalogSupplierBrandID", false);
        }

        public DataSet GetPracticeCatalogDataBySupplier(string SearchCriteria, string SearchText)
        {

            if (grdPracticeCatalog.VisionDataSet == null)
            {
                grdPracticeCatalog.VisionDataSet = BLL.Practice.Barcode.GetPracticeCatalogDataBySupplier1(practiceID, SearchText, SearchCriteria);
            }

            return grdPracticeCatalog.VisionDataSet;

        }

        protected void grdPracticeCatalog_PreRender(object sender, EventArgs e)
        {
            grdPracticeCatalog.ExpandGrid();
        }

        private void LoopParentsPracticeCatalog(GridTableView gridTableView)
        {
            foreach (GridDataItem gridDataItem in gridTableView.Items)
                //foreach (GridDataItem item in RadGrid1.SelectedItems) 
                if (findSelectColumn(gridDataItem))
                {

                    IDataReader reader = GetProductsBySupplierBrand(practiceID, findPracticeCatalogSupplierBrandID(gridDataItem));
                    if (reader.Read())
                    {
                        do
                        {

                            UpdateSummaryArray(Convert.ToInt32(reader["PracticeCatalogProductID"].ToString()));
                        } while (reader.Read());
                    }
                    reader.Close();

                }

        }

        private void LoopHierarchyRecursive(GridTableView gridTableView)
        {

            foreach (GridNestedViewItem nestedViewItem in gridTableView.GetItems(GridItemType.NestedView))
            {

                if (nestedViewItem.NestedTableViews.Length > 0)
                {

                    foreach (GridDataItem gridDataItem in nestedViewItem.NestedTableViews[0].Items)
                    {


                        if (findSelectColumn(gridDataItem))
                        {

                            UpdateSummaryArray(findPracticeCatalogProductID(gridDataItem));



                        }
                    }

                }

            }

        }

        private Boolean findSelectColumn(GridDataItem gridDataItem)
        {
            TableCell cell = gridDataItem["ClientSelectColumn"];
            CheckBox checkBox = (CheckBox)cell.Controls[0];
            if (checkBox.Checked)
                return true;
            else
            {
                return false;
            }


        }

        protected void btnMoveToLabel_Click(object sender, EventArgs e)
        {
            ReportViewer1.Visible = false;
            ReportViewer1.Enabled = false;

            LoopHierarchyRecursive(grdPracticeCatalog.MasterTableView);
            LoopParentsPracticeCatalog(grdPracticeCatalog.MasterTableView);

            grdSummary.DataSource = GetSummaryData(BuildArrayString(list));
            grdSummary.DataBind(); //Binds to data whether or not we have a

        }

        protected void btnMoveToMC_Click(object sender, EventArgs e)
        {
            ReportViewer1.Visible = false;
            ReportViewer1.Enabled = false;
            list = (ArrayList)ViewState["ArrayList"];

            foreach (GridDataItem gridDataItem in grdSummary.MasterTableView.Items)

                if (findSelectColumn(gridDataItem))
                {

                    Int32 PracticecatalogproductID = Int32.Parse(gridDataItem["PracticecatalogproductID"].Text);
                    if (ViewState["ArrayList"] != null)
                    {
                        list.Remove(PracticecatalogproductID);
                    }


                }



            grdSummary.DataSource = GetSummaryData(BuildArrayString(list));
            grdSummary.DataBind(); //Binds to data whether or not we have a


        }

        protected void btnReturnToSearch_Click(object sender, EventArgs e)
        {
            ctlRecordsPerPage.Visible = true;
            ItemSelectorPanel.Visible = true;
            ReturnToSearchPanel.Visible = false;
            ReportViewer1.Visible = false;
        }

        protected void btnPrintLabels_Click(object sender, EventArgs e)
        {
            ctlRecordsPerPage.Visible = false;
            ItemSelectorPanel.Visible = false;
            ReturnToSearchPanel.Visible = true;
            ReportViewer1.Visible = true;
            
            string reportName = "";

            List<ReportParameter> paramList = new List<ReportParameter>();

            reportName = @"/PracticeAdminReports/labels30perpageorig";

            if (reportName.Length > 0)
            {
                SetReportName(reportName);
                if (ViewState["ArrayList"] != null)
                {
                    list = (ArrayList)ViewState["ArrayList"];
                }
                paramList.Add(new ReportParameter("ProductInventoryIDArray", BuildArrayString(list), false));
                SetReportParameters(paramList);
            }

        }

        protected void SetReportName(string reportName)
        {
            ReportViewer1.ConfigureReportViewer(reportName);
        }

        protected void SetReportParameters(List<ReportParameter> paramList)
        {
            ReportViewer1.ServerReport.SetParameters(paramList);
            // Process and render the report
            ReportViewer1.ServerReport.Refresh();
        }


        protected void ProductInventory_Delete(int productInventoryID, int userID, out int removalType, out string dependencyMessage)
        {
            // Delete the productInventoryID from Inventory.
            DAL.PracticeLocation.Inventory dalInventory = new DAL.PracticeLocation.Inventory();
            dalInventory.Delete(productInventoryID, userID, out removalType, out dependencyMessage);

            int junk = removalType;
            string junkstring = dependencyMessage;

            //Response.Write("Deleting productInventoryID: " + productInventoryID);
        }

        private string BuildArrayString(ArrayList list)
        {
            string arrayValues = "";
            foreach (Int32 item in list)
            {

                arrayValues += String.Concat(item.ToString(), ",");

            }

            if (arrayValues.Length > 0)
            {
                arrayValues = arrayValues.Substring(0, arrayValues.Length - 1);

            }
            return arrayValues;
        }

        protected void UpdateSummaryArray(Int32 PracticeCatalogProductID)
        {


            if (ViewState["ArrayList"] != null)
            {
                list = (ArrayList)ViewState["ArrayList"];

                //Checks for dups and exits the loop if one exists



                Int32 listItemtoRemove = -1;
                foreach (Int32 item in list)
                {
                    if (Convert.ToInt32(item) == Convert.ToInt32(PracticeCatalogProductID))
                        listItemtoRemove = item;

                }

                if (listItemtoRemove >= 0)
                {
                    list.Remove(listItemtoRemove);
                }

                list.Add(PracticeCatalogProductID);
            }
            else
            {
                //Checks for dups and exits the loop if one exists
                Int32 listItemtoRemove = -1;
                foreach (Int32 item in list)
                {
                    if (Convert.ToInt32(item) == Convert.ToInt32(PracticeCatalogProductID))
                        listItemtoRemove = item;

                }

                if (listItemtoRemove >= 0)
                {
                    list.Remove(listItemtoRemove);
                }
                list.Add(PracticeCatalogProductID);
                ViewState["ArrayList"] = list;
            }

        }

        private Int32 findPracticeCatalogProductID(GridDataItem gridDataItem)
        {

            string PracticeCatalogProductID = (string)gridDataItem["PracticeCatalogProductID"].Text;
            if (PracticeCatalogProductID != null)
            {
                return Int32.Parse(PracticeCatalogProductID);
            }
            else
            {
                return 0;
            }

        }

        private Int32 findPracticeCatalogSupplierBrandID(GridDataItem gridDataItem)
        {

            string PracticeCatalogSupplierBrandID = (string)gridDataItem["PracticeCatalogSupplierBrandID"].Text;
            if (PracticeCatalogSupplierBrandID != null)
            {
                return Int32.Parse(PracticeCatalogSupplierBrandID);
            }
            else
            {
                return 0;
            }

        }

        protected void grdSummary_DeleteCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            //delete item
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = e.Item as GridDataItem;
                Int32 PracticecatalogproductID = Int32.Parse(dataItem["PracticecatalogproductID"].Text);
                if (ViewState["ArrayList"] != null)
                {
                    list = (ArrayList)ViewState["ArrayList"];
                    list.Remove(PracticecatalogproductID);

                    grdSummary.DataSource = grdSummary.DataSource = GetSummaryData(BuildArrayString(list));
                    grdSummary.Rebind();

                }
            }
        }

        #region Data functions

        public DataSet GetSummaryData(string arrayValues)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetPreBarCodeLabelSummary");

            db.AddInParameter(dbCommand, "ProductInventoryIDArray", DbType.String, arrayValues);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;

        }

        public IDataReader GetProductsBySupplierBrand(Int32 PracticeID, Int32 PracticeCatalogSupplierBrandID)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetProductsBySupplierBrand");
            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, PracticeID);
            db.AddInParameter(dbCommand, "PracticeCatalogSupplierBrandID", DbType.Int32, PracticeCatalogSupplierBrandID);

            IDataReader reader = db.ExecuteReader(dbCommand);
            return reader;

        }

        public DataSet GetPracticeCatalogSuppliers()
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetPracticeCatalogSupplierBrands");

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }


        #endregion

        protected eSetupStatus ValidateSetupData()
        {
            return eSetupStatus.Complete;
        }

        /// <summary>
        /// IVisionWizardControl interface save.  Called by wizard control on tab change
        /// </summary>
        public void Save()
        {
            //do nothing.  Grid rows are saved individually

            SaveStatus(practiceID, -1, this.ToString(), ValidateSetupData());
        }



























            


            

            

            




















    }
}