﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.Admin.PracticeSetup;
using ClassLibrary.DAL;
using System.Linq;
using System.Collections.Generic;


namespace BregVision.Admin.UserControls
{
    public partial class PracticePhysicians : PracticeSetupBase, IVisionWizardControl
    {

        /// <summary>
        /// IVisionWizardControl Interface.
        /// </summary>
        public bool NeedsInitialBind { get; set; }

        private bool isInEdit = false;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            lblContentHeader.Text = practiceName + " Clinicians";

            if (isInEdit == false)
            {
                BindGridPracticePhysician();
            }
        }



        //  Get all Physicians and bind to the GridPracticePhysicians.
        protected void BindGridPracticePhysician()
        {

            DataTable dtPracticePhysician = GetAllPhysicians(practiceID);

            if (dtPracticePhysician.Rows.Count > 0)
            {
                DataView dvPracticePhysician = dtPracticePhysician.DefaultView;

                dvPracticePhysician.Sort = "SortOrder";

                GridPracticePhysician.DataSource = dvPracticePhysician;
                GridPracticePhysician.DataBind();

            }
            else  //No records found - insert a display row so the GridView will display.
            {
                DisplayNoRecordsFoundRow(dtPracticePhysician);
            }

        }

        protected DataTable GetAllPhysicians(int practiceID)
        {
            BLL.Practice.Physician bllPP = new BLL.Practice.Physician();
            DataTable dtPracticePhysician = bllPP.GetAllPhysicians(practiceID);
            return dtPracticePhysician;
        }

        //  Display "No Records Found" when the grid's data source has now records.
        protected void DisplayNoRecordsFoundRow(DataTable dtPracticePhysician)
        {
            var dataRow = dtPracticePhysician.NewRow();
            dataRow["isProvider"] = false;
            dataRow["isFitter"] = false;
            dtPracticePhysician.Rows.Add(dataRow);
            GridPracticePhysician.DataSource = dtPracticePhysician;
            GridPracticePhysician.DataBind();

            int totalColumns = GridPracticePhysician.Rows[0].Cells.Count;
            GridPracticePhysician.Rows[0].Cells.Clear();
            GridPracticePhysician.Rows[0].Cells.Add(new TableCell());
            GridPracticePhysician.Rows[0].Cells[0].ColumnSpan = totalColumns;
            GridPracticePhysician.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
            GridPracticePhysician.Rows[0].Cells[0].Text = "No Records Found";
        }

        // Set the Drop Down List for Salutation and for Suffix and cache both datatables.
        protected void GridPracticePhysician_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlSalutation = (DropDownList)e.Row.FindControl("ddlSalutation");
                DropDownList ddlSuffix = (DropDownList)e.Row.FindControl("ddlSuffix");

                if (ddlSalutation != null)
                {

                    ddlSalutation.DataSource = GetSalutationList();
                    ddlSalutation.DataBind();
                    // Retain the value of the Salutation when in EditMode.
                    // DataKey 0 is practiceID, DataKey 1 is salutation, DataKey 2 is suffix.
                    ddlSalutation.SelectedValue = GridPracticePhysician.DataKeys[e.Row.RowIndex].Values[1].ToString();
                }

                if (ddlSuffix != null)
                {
                    ddlSuffix.DataSource = GetSuffixList();
                    ddlSuffix.DataBind();
                    // Retain the value of the Suffix when in EditMode.
                    // DataKey 0 is practiceID, DataKey 1 is salutation, DataKey 2 is suffix.
                    ddlSuffix.SelectedValue = GridPracticePhysician.DataKeys[e.Row.RowIndex].Values[2].ToString();
                }
            }

            if (e.Row.RowType == DataControlRowType.Footer)   // Add New controls are in the footer.
            {
                DropDownList ddlNewSalutation = (DropDownList)e.Row.FindControl("ddlNewSalutation");
                DropDownList ddlNewSuffix = (DropDownList)e.Row.FindControl("ddlNewSuffix");

                ddlNewSalutation.DataSource = GetSalutationList();
                ddlNewSalutation.DataBind();

                ddlNewSuffix.DataSource = GetSuffixList();
                ddlNewSuffix.DataBind();
            }

            ImageButton imgDelete = e.Row.FindControl("imgDelete") as ImageButton;
            if (imgDelete != null)
            {
                imgDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this Clinician?');");
            }
        }


        //  User Clicked Edit, so allow the row to be edited.
        protected void GridPracticePhysician_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridPracticePhysician.EditIndex = e.NewEditIndex;
            // Sorting Works but is commented out for now.
            //GridPracticePhysician.AllowSorting = false;
            BindGridPracticePhysician();
            isInEdit = true;
        }

        // Update the edited row.
        protected void GridPracticePhysician_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int physicianID = Convert.ToInt32(GridPracticePhysician.DataKeys[e.RowIndex].Values[0].ToString());

            Label lblContactIDEdit = (Label)GridPracticePhysician.Rows[e.RowIndex].FindControl("lblContactIDEdit");
            DropDownList ddlSalutation = (DropDownList)GridPracticePhysician.Rows[e.RowIndex].FindControl("ddlSalutation");
            TextBox txtFirstName = (TextBox)GridPracticePhysician.Rows[e.RowIndex].FindControl("txtFirstName");
            TextBox txtMiddleName = (TextBox)GridPracticePhysician.Rows[e.RowIndex].FindControl("txtMiddleName");
            TextBox txtLastName = (TextBox)GridPracticePhysician.Rows[e.RowIndex].FindControl("txtLastName");
            DropDownList ddlSuffix = (DropDownList)GridPracticePhysician.Rows[e.RowIndex].FindControl("ddlSuffix");
            CheckBox chkProvider = (CheckBox)GridPracticePhysician.Rows[e.RowIndex].FindControl("chkProvider");
            CheckBox chkFitter = (CheckBox)GridPracticePhysician.Rows[e.RowIndex].FindControl("chkFitter");
	    TextBox txtCloudConnectId = (TextBox)GridPracticePhysician.Rows[e.RowIndex].FindControl("txtCloudConnectId");
            TextBox txtPin = (TextBox)GridPracticePhysician.Rows[e.RowIndex].FindControl("txtPin");
            CheckBox chkClearPin = (CheckBox)GridPracticePhysician.Rows[e.RowIndex].FindControl("chkClearPin");
            TextBox txtSortOrder = (TextBox)GridPracticePhysician.Rows[e.RowIndex].FindControl("txtSortOrder");
            int sortOrder = 0;
            int.TryParse(txtSortOrder.Text, out sortOrder);

            BLL.Practice.Physician bllPP = new BLL.Practice.Physician();

            //bllPP.PhysicianID = physicianID;                  //  This is unneccessary for the update!
            bllPP.ContactID = Convert.ToInt32(lblContactIDEdit.Text.ToString());
            bllPP.Salutation = ddlSalutation.SelectedValue;
            bllPP.FirstName = txtFirstName.Text.ToString();
            bllPP.MiddleName = txtMiddleName.Text.ToString();
            bllPP.LastName = txtLastName.Text.ToString();
            bllPP.Suffix = ddlSuffix.SelectedValue;
            bllPP.IsProvider = chkProvider.Checked;
            bllPP.IsFitter = chkFitter.Checked;
	    bllPP.CloudConnectId = txtCloudConnectId.Text.ToString();
            bllPP.Pin = txtPin.Text.ToString();
            bllPP.ClearPin = chkClearPin.Checked;
            bllPP.UserID = userID;
            bllPP.SortOrder = sortOrder;

            bllPP.Update(bllPP);

            // Set Edit Mode back to Read Only.
            GridPracticePhysician.EditIndex = -1;
            // Sorting Works but is commented out for now.
            //GridPracticePhysician.AllowSorting = true;
            BindGridPracticePhysician();

        }

        //  User Canceled the Row Edit, so remove the editing from the row.
        protected void GridPracticePhysician_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridPracticePhysician.EditIndex = -1;
            // Sorting Works but is commented out for now.
            //GridPracticePhysician.AllowSorting = true;
            BindGridPracticePhysician();
        }


        //  User clicked Delete so Delete the Practice.
        public void GridPracticePhysician_RowDeleting(Object sender, GridViewDeleteEventArgs e)
        {
            int physicianID = Convert.ToInt32(GridPracticePhysician.DataKeys[e.RowIndex].Values[0].ToString());

            BLL.Practice.Physician bllPP = new BLL.Practice.Physician();

            bllPP.PhysicianID = physicianID;
            bllPP.UserID = userID;

            bllPP.Delete(bllPP);

            BindGridPracticePhysician();
        }

        protected void SortClinicians(object sender, EventArgs e)
        {
            BLL.Practice.Physician bllPP = new BLL.Practice.Physician();
            bllPP.SortClinicians(practiceID);
            isInEdit = false;
        }

        // User Clicked "AddNew" or "Insert" or "Cancel" (ed) the AddNew.
        protected void GridPracticePhysician_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            // Get the Controls within the footer.
            //TextBox txtNewPracticeName = ( TextBox ) GridPracticePhysician.FooterRow.FindControl( "txtNewPracticeName" );
            //Label lblContactIDEdit = ( Label ) GridPracticePhysician.FooterRow.FindControl( "lblContactIDEdit" );
            DropDownList ddlNewSalutation = (DropDownList)GridPracticePhysician.FooterRow.FindControl("ddlNewSalutation");
            TextBox txtNewFirstName = (TextBox)GridPracticePhysician.FooterRow.FindControl("txtNewFirstName");
            TextBox txtNewMiddleName = (TextBox)GridPracticePhysician.FooterRow.FindControl("txtNewMiddleName");
            TextBox txtNewLastName = (TextBox)GridPracticePhysician.FooterRow.FindControl("txtNewLastName");
            DropDownList ddlNewSuffix = (DropDownList)GridPracticePhysician.FooterRow.FindControl("ddlNewSuffix");
            TextBox txtNewPin = (TextBox)GridPracticePhysician.FooterRow.FindControl("txtNewPin");
            TextBox txtNewSortOrder = (TextBox)GridPracticePhysician.FooterRow.FindControl("txtNewSortOrder");
            var chkNewIsProvider = (CheckBox)GridPracticePhysician.FooterRow.FindControl("chkNewIsProvider");
            var chkNewIsFitter = (CheckBox)GridPracticePhysician.FooterRow.FindControl("chkNewIsFitter");
	    TextBox txtCloudConnectId = (TextBox)GridPracticePhysician.FooterRow.FindControl("txtNewCloudConnectId");
 
            RequiredFieldValidator rfvNewFirstName = (RequiredFieldValidator)GridPracticePhysician.FooterRow.FindControl("rfvNewFirstName");
            RequiredFieldValidator rfvNewLastName = (RequiredFieldValidator)GridPracticePhysician.FooterRow.FindControl("rfvNewLastName");

            ImageButton imageButtonAddNew = (ImageButton)GridPracticePhysician.FooterRow.FindControl("ImageButtonAddNew");
            ImageButton imageButtonAddNewCancel = (ImageButton)GridPracticePhysician.FooterRow.FindControl("ImageButtonAddNewCancel");


            // Check if AddNew Button was clicked.  Note: this could be AddNew or Insert button that was clicked.
            if (e.CommandName.Equals("AddNew"))
            {

                int sortOrder = 0;
                int.TryParse(txtNewSortOrder.Text, out sortOrder);
                if (sortOrder == 0)
                {
                    txtNewSortOrder.Text = Convert.ToString(GridPracticePhysician.Rows.Count + 1);
                }


                //  Check if the AddNew button was clicked to Add a New record as opposed to Inserting a New Record
                if (imageButtonAddNewCancel.Visible == false)
                {
                    // User clicked to Add a New Record.  
                    //Display the New Fields for input and their validation controls.
                    //Display the Insert and Cancel buttons.

                    ddlNewSalutation.Visible = true;
                    txtNewFirstName.Visible = true;
                    txtNewMiddleName.Visible = true;
                    txtNewLastName.Visible = true;
                    ddlNewSuffix.Visible = true;
                    txtNewPin.Visible = true;
                    txtNewSortOrder.Visible = false;
                    chkNewIsProvider.Visible = true;
                    chkNewIsFitter.Visible = true;
		    txtCloudConnectId.Visible = true;
                    

                    imageButtonAddNew.ImageUrl = @"~\RadControls\Grid\Skins\Outlook\Insert.gif";
                    imageButtonAddNew.ToolTip = "Insert New Practice Physician";
                    imageButtonAddNewCancel.Visible = true;

                    rfvNewFirstName.Visible = true;
                    rfvNewLastName.Visible = true;

                    // Sorting Works but is commented out for now.
                    //GridPracticePhysician.AllowSorting = false;

                    // If the "No Records Found" was displayed before the user clicked AddNew than clear that row.
                    if (GridPracticePhysician.Rows[0].Cells[0].Text == "No Records Found")
                    {
                        GridPracticePhysician.Rows[0].Cells.Clear();
                        //BindGridPracticePhysician();   // Test commenting this out
                    }


                }
                else    //  User clicked the Insert button.
                {
                    // User clicked to Insert a New Record, so Insert the New Record.
                    // Hide the Add New Fields for input and their validators.
                    // Hide the Insert and Cancel buttons.
                    // Show the Add New Button

                    string newSalutation = ddlNewSalutation.SelectedValue.ToString();
                    string newFirstName = txtNewFirstName.Text.ToString();
                    string newMiddleName = txtNewMiddleName.Text.ToString();
                    string newLastName = txtNewLastName.Text.ToString();
                    string newSuffix = ddlNewSuffix.SelectedValue.ToString();
                    string newPin = txtNewPin.Text.ToString();
                    bool newIsProvider = chkNewIsProvider.Checked;
                    bool newIsFitter = chkNewIsFitter.Checked;
		    string newCloudConnectId = txtCloudConnectId.Text.ToString();

                    BLL.Practice.Physician bllPP = new BLL.Practice.Physician();
                    bllPP.PracticeID = practiceID;
                    bllPP.Salutation = newSalutation;
                    bllPP.FirstName = newFirstName;
                    bllPP.MiddleName = newMiddleName;
                    bllPP.LastName = newLastName;
                    bllPP.Suffix = newSuffix;
                    bllPP.Pin = newPin;
                    bllPP.UserID = userID;
                    bllPP.SortOrder = sortOrder;
                    bllPP.IsProvider = newIsProvider;
                    bllPP.IsFitter = newIsFitter;
		    bllPP.CloudConnectId = newCloudConnectId;

                    bllPP.Insert(bllPP);

                    ddlNewSalutation.Visible = false;
                    txtNewFirstName.Visible = false;
                    txtNewMiddleName.Visible = false;
                    txtNewLastName.Visible = false;
                    ddlNewSuffix.Visible = false;
                    txtNewPin.Visible = false;
                    txtNewSortOrder.Visible = false;
                    chkNewIsProvider.Visible = false;
                    chkNewIsFitter.Visible = false;
		    txtCloudConnectId.Visible = false;

                    imageButtonAddNew.ImageUrl = @"~\RadControls\Grid\Skins\Outlook\AddRecord.gif";
                    imageButtonAddNew.ToolTip = "Add New Practice Physician";
                    imageButtonAddNewCancel.Visible = false;

                    rfvNewFirstName.Visible = false;
                    rfvNewLastName.Visible = false;

                    // Sorting Works but is commented out for now.
                    // GridPracticePhysician.AllowSorting = true;

                    BindGridPracticePhysician();
                }
                isInEdit = true;
            }

            //  User Canceled the insertion of a new record.  Therefore, revert mode back to AddNew.
            //  Hide the Add New Fields for input and their validators.
            //  Hide the Insert and Cancel buttons.
            //  Show the Add New Button
            if (e.CommandName.Equals("AddNewCancel"))
            {

                imageButtonAddNewCancel.CausesValidation = false;

                ddlNewSalutation.Visible = false;
                txtNewFirstName.Visible = false;
                txtNewMiddleName.Visible = false;
                txtNewLastName.Visible = false;
                ddlNewSuffix.Visible = false;
                txtNewPin.Visible = false;
                txtNewSortOrder.Visible = false;
                chkNewIsProvider.Visible = false;
                chkNewIsFitter.Visible = false;
		txtCloudConnectId.Visible = false;

                imageButtonAddNew.ImageUrl = @"~\RadControls\Grid\Skins\Outlook\AddRecord.gif";
                imageButtonAddNew.ToolTip = "Add New Practice Physician";

                imageButtonAddNewCancel.Visible = false;
                rfvNewFirstName.Visible = false;
                rfvNewLastName.Visible = false;

                // Sorting Works but is commented out for now.
                // GridPracticePhysician.AllowSorting = true;

                // Check if user cancel the add new record when adding the first record.
                // Is so call the Bind method so that the "No Records Found" message is displayed.
                if (GridPracticePhysician.Rows.Count < 2)
                {
                    // Sorting Works but is commented out for now.
                    //GridPracticePhysician.AllowSorting = false;                
                }

                BindGridPracticePhysician();
            }

            if (e.CommandName.Equals("MoveUp"))
            {
                ReorderRows(e, -1);
            }
            else if (e.CommandName.Equals("MoveDown"))
            {
                ReorderRows(e, 1);
            }
        }

        private void ReorderRows(GridViewCommandEventArgs e, int indexOffset)
        {
            var bllPP = new BLL.Practice.Physician();
            var physicians = new List<int>();
            foreach (GridViewRow currentRow in GridPracticePhysician.Rows)
            {
                physicians.Add(Convert.ToInt32(GridPracticePhysician.DataKeys[currentRow.RowIndex].Values[0].ToString()));
            }

            if (physicians != null && physicians.Count > 0)
                bllPP.ReorderRows(Convert.ToInt32(e.CommandArgument), practiceID, indexOffset, physicians);
            isInEdit = false;
        }

        // Get Salutation DataTable to populate the Drop Down List for Salutation.
        // Retrieve datatable from cache or populate the datatable and put into cache.
        public DataTable GetSalutationList()
        {
            // Get datatable from cache, otherwise get database and put it into cache.
            DataTable dtSalutation = (DataTable)Cache["dtSalutation"];
            if (dtSalutation == null)
            {

                DAL.Lookup dalLookup = new DAL.Lookup();
                dtSalutation = dalLookup.Salutation_SelectAll();
                Cache.Insert("dtSalutation", dtSalutation);
            }
            return dtSalutation;
        }

        // Get Suffix DataTable to populate the Drop Down List for Suffix.
        // Retrieve datatable from cache or populate the datatable and put into cache.
        public DataTable GetSuffixList()
        {
            // Get datatable from cache, otherwise get database and put it into cache.
            DataTable dtSuffix = (DataTable)Cache["dtSuffix"];
            if (dtSuffix == null)
            {
                DAL.Lookup dalLookup = new DAL.Lookup();
                dtSuffix = dalLookup.Suffix_SelectAll();
                Cache.Insert("dtSuffix", dtSuffix);
            }
            return dtSuffix;
        }



        protected void GridPracticePhysician_Sorting(object sender, GridViewSortEventArgs e)
        {


            ViewState["sortexpression"] = e.SortExpression;

            if (ViewState["sortdirection"] == null)
            {
                ViewState["sortdirection"] = "asc";
            }
            else
            {
                if (ViewState["sortdirection"].ToString() == "asc")
                {
                    ViewState["sortdirection"] = "desc";
                }
                else
                {
                    ViewState["sortdirection"] = "asc";
                }
            }
            BindGridPracticePhysician();

        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Practice_All.aspx");
        }

        protected eSetupStatus ValidateSetupData()
        {
            Page.Validate();
            if (Page.IsValid)
            {
                if (GridPracticePhysician.Rows.Count > 0)
                {
                    if (GridPracticePhysician.Rows[0].Cells[0].Text != "No Records Found")
                    {
                        return eSetupStatus.Complete;
                    }

                }
            }
            return eSetupStatus.Incomplete;
        }

        /// <summary>
        /// IVisionWizardControl interface save.  Called by wizard control on tab change
        /// </summary>
        public void Save()
        {
            //do nothing.  Grid rows are saved individually

            SaveStatus(practiceID, -1, this.ToString(), ValidateSetupData());
        }

    }
}
