﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ProductReplacement.ascx.cs" Inherits="BregVision.Admin.UserControls.ProductReplacement" %>

<script runat="server" type="text/C#">

	public bool CheckBoxChecked(object x)
	{
		return (x == null || x.ToString() == "True");
	}

</script>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="grdReplacementMasterCatalog">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="grdReplacementMasterCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
			</UpdatedControls>
		</telerik:AjaxSetting>
		<telerik:AjaxSetting AjaxControlID="grdReplacementThirdPartyCatalog">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="grdReplacementThirdPartyCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
			</UpdatedControls>
		</telerik:AjaxSetting>
		<telerik:AjaxSetting AjaxControlID="grdReplacementCombinedCatalog">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="grdReplacementCombinedCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
			</UpdatedControls>
		</telerik:AjaxSetting>
	</AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" InitialDelayTime="500" EnableAjaxSkinRendering="True" BackgroundPosition="Center" Skin="Breg" EnableEmbeddedSkins="False">
	<img src="~/App_Themes/Breg/images/Common/loading.gif" />
</telerik:RadAjaxLoadingPanel>
<div class="content" style="background-color:lightgoldenrodyellow; margin: 0px; border:2px;padding-left:30px; padding-bottom:30px; padding-right:30px">
	<asp:HiddenField ID="ParentMasterCatalogProductID" runat="server" />
	<asp:HiddenField ID="ParentThirdPartyProductID" runat="server" />
	<asp:HiddenField ID="ParentIsShowCombinedCatalogReplacement" runat="server" />
	<asp:HiddenField ID="ParentIsPracticeSpecific" runat="server" />
	<div>
		<div>
			<div>
				<span style="font-size: 17px"><br /></span>
				<span style="font-size: 17px">Current Replacement Product</span>
				<span style="font-size: 7px"><br /></span>
			</div>
		</div>
		<div>
			<div id="ReplacementProductStatus" runat="server">
				<br /><span style="margin-left: 20px; font-size: 15px; color:red">No Replacement Product Selected</span>
			</div>
		</div>
		<bvu:VisionRadGridAjax ID="grdProductReplacement" runat="server" EnableLinqExpressions="false" AllowPaging="false"
			AllowMultiRowSelection="True" OnNeedDataSource="grdProductReplacement_NeedDataSource" OnItemCommand="grdProductReplacement_ItemCommand"
			OnPageIndexChanged="grdProductReplacement_PageIndexChanged" OnPreRender="grdProductReplacement_PreRender"
			AddNonBreakingSpace="false" Skin="Breg" EnableEmbeddedSkins="False" ShowGroupPanel="false"
			PagerStyle-AlwaysVisible="true">
			<MasterTableView DataKeyNames="ProductReplacementID" HierarchyLoadMode="ServerOnDemand">
				<Columns>
					<telerik:GridTemplateColumn ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" UniqueName="DeleteColumn">
						<ItemTemplate>
							<asp:ImageButton ID="btnDelete" Visible='<%# Eval("IsDeletable") %>' runat="server" CommandName="Delete" ImageAlign="Middle" ImageUrl="~/App_Themes/Breg/images/Common/delete.png" />
						</ItemTemplate>
					</telerik:GridTemplateColumn>
					<telerik:GridBoundColumn DataField="Name" HeaderText="Name" UniqueName="Name" Visible="true" />
					<telerik:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code" Visible="true" />
					<telerik:GridBoundColumn DataField="Side" HeaderText="Side" UniqueName="Side" Visible="true" />
					<telerik:GridBoundColumn DataField="Size" HeaderText="Size" UniqueName="Size" Visible="true" />
					<telerik:GridBoundColumn DataField="ProductReplacementID" HeaderText="ProductReplacementID" UniqueName="ProductReplacementID" Visible="false" />
					<telerik:GridBoundColumn DataField="OldMasterCatalogProductID" HeaderText="OldMasterCatalogProductID" UniqueName="OldMasterCatalogProductID" Visible="false" />
					<telerik:GridBoundColumn DataField="NewMasterCatalogProductID" HeaderText="NewMasterCatalogProductID" UniqueName="NewMasterCatalogProductID" Visible="false" />
					<telerik:GridBoundColumn DataField="NewThirdPartyProductID" HeaderText="NewThirdPartyProductID" UniqueName="NewThirdPartyProductID" Visible="false" />
					<telerik:GridBoundColumn DataField="StartDate" HeaderText="Start Date" UniqueName="StartDate" Visible="false" />
					<telerik:GridBoundColumn DataField="EndDate" HeaderText="End Date" UniqueName="EndDate" Visible="false" />
					<telerik:GridBoundColumn DataField="CreatedUserID" HeaderText="CreatedUserID" UniqueName="CreatedUserID" Visible="false" />
					<telerik:GridBoundColumn DataField="CreatedDate" HeaderText="Last Updated Date" UniqueName="CreatedDate" />
					<telerik:GridBoundColumn DataField="ModifiedUserID" HeaderText="ModifiedUserID" UniqueName="ModifiedUserID" Visible="false" />
					<telerik:GridBoundColumn DataField="ModifiedDate" HeaderText="ModifiedDate" UniqueName="ModifiedDate" Visible="false" />
					<telerik:GridCheckBoxColumn DataField="IsActive" HeaderText="Active" UniqueName="IsActive" Visible="false" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" />
				</Columns>
			</MasterTableView>
		</bvu:VisionRadGridAjax>
	</div>

	<div>
		<div style='<%# ShowMasterCatalogReplacement %>' runat="server">
			<div>
				<div>
					<span style="font-size: 17px"><br /></span>
					<span style="font-size: 17px">New Master Catalog Replacement Product</span>
					<span style="font-size: 7px"><br /></span>
				</div>
			</div>
			<bvu:VisionRadGridAjax ID="grdReplacementMasterCatalog" runat="server" EnableLinqExpressions="false"
				AllowMultiRowSelection="False" OnNeedDataSource="grdReplacementMasterCatalog_NeedDataSource" OnItemCommand="grdReplacementMasterCatalog_ItemCommand"
				OnDetailTableDataBind="grdReplacementMasterCatalog_OnDetailTableDataBind" OnPageIndexChanged="grdReplacementMasterCatalog_PageIndexChanged"
				OnPreRender="grdReplacementMasterCatalog_PreRender" AddNonBreakingSpace="false" Skin="Breg" EnableEmbeddedSkins="False" ShowGroupPanel="false"
				PagerStyle-AlwaysVisible="true" ClientSettings-EnablePostBackOnRowClick="true">
				<MasterTableView DataKeyNames="SupplierID" HierarchyLoadMode="ServerOnDemand">
					<DetailTables>
						<telerik:GridTableView runat="server" DataKeyNames="MasterCatalogProductID" Width="100%">
							<ParentTableRelation>
								<telerik:GridRelationFields DetailKeyField="SupplierID" MasterKeyField="SupplierID" />
							</ParentTableRelation>
							<Columns>
								<telerik:GridBoundColumn DataField="ShortName" HeaderText="Name" UniqueName="ShortName" />
								<telerik:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code" />
								<telerik:GridBoundColumn DataField="Packaging" HeaderText="Pkg." UniqueName="Packaging" />
								<telerik:GridBoundColumn DataField="LeftRightSide" HeaderText="Side" UniqueName="LeftRightSide" ItemStyle-HorizontalAlign="Center"
									ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" />
								<telerik:GridBoundColumn DataField="Modifiers" HeaderText="Modifiers" UniqueName="Modifiers" />
								<telerik:GridBoundColumn DataField="Size" HeaderText="Size" UniqueName="Size" />
								<telerik:GridBoundColumn DataField="Color" HeaderText="Color" UniqueName="Color" />
								<telerik:GridBoundColumn DataField="WholesaleListCost" DataFormatString="{0:C}" HeaderText="WLC" UniqueName="WholesaleListCost" ItemStyle-HorizontalAlign="Right"
									ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Right" />
								<telerik:GridCheckBoxColumn DataField="IsLogoAblePart" HeaderText="Logo" UniqueName="IsLogoAblePart" ItemStyle-HorizontalAlign="Center"
									ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" />
								<telerik:GridBoundColumn DataField="MastercatalogProductID" Display="false" UniqueName="MastercatalogProductID" />
								<telerik:GridBoundColumn DataField="SupplierID" UniqueName="SupplierID" Visible="false" />
								<telerik:GridBoundColumn DataField="MasterCatalogSubCategoryID" Display="false" UniqueName="MasterCatalogSubCategoryID" />
							</Columns>
						</telerik:GridTableView>
					</DetailTables>
					<Columns>
						<telerik:GridBoundColumn DataField="SupplierID" UniqueName="SupplierID" Visible="False" />
						<telerik:GridBoundColumn DataField="Brand" HeaderText="Brand" UniqueName="Brand" />
						<telerik:GridBoundColumn DataField="Name" HeaderText="First Name" UniqueName="Name" />
						<telerik:GridBoundColumn DataField="LastName" HeaderText="Last Name" UniqueName="LastName" />
						<telerik:GridBoundColumn DataField="PhoneWork" HeaderText="Phone" UniqueName="PhoneWork" />
						<telerik:GridBoundColumn DataField="Address" HeaderText="Address" UniqueName="Address" />
						<telerik:GridBoundColumn DataField="City" HeaderText="City" UniqueName="City" />
						<telerik:GridBoundColumn DataField="ZipCode" HeaderText="Zip Code" UniqueName="ZipCode" />
					</Columns>
				</MasterTableView>
			</bvu:VisionRadGridAjax>
		</div>
		<div style='<%# ShowThirdPartyCatalogReplacement %>' runat="server">
			<div>
				<div>
					<span style="font-size: 17px"><br /></span>
					<span style="font-size: 17px">New Third Party Replacement Product</span>
					<span style="font-size: 7px"><br /></span>
				</div>
			</div>
			<bvu:VisionRadGridAjax ID="grdReplacementThirdPartyCatalog" runat="server" EnableLinqExpressions="false" AllowPaging="false"
				OnNeedDataSource="grdReplacementThirdPartyCatalog_NeedDataSource" OnItemCommand="grdReplacementThirdPartyCatalog_ItemCommand" ShowGroupPanel="false"
				OnDetailTableDataBind="grdReplacementThirdPartyCatalog_OnDetailTableDataBind" OnPageIndexChanged="grdReplacementThirdPartyCatalog_PageIndexChanged"
				OnPreRender="grdReplacementThirdPartyCatalog_PreRender" PageSize="100" Skin="Breg" EnableEmbeddedSkins="False" AddNonBreakingSpace="False"
				ClientSettings-EnablePostBackOnRowClick="true">
			<MasterTableView DataKeyNames="PracticeID, ThirdPartySupplierID, LinkThirdPartySupplierID, PracticeCatalogSupplierBrandID" EnableNoRecordsTemplate="true" HierarchyLoadMode="ServerOnDemand" Name="mtvVendor">
			<Columns>
			  <telerik:GridBoundColumn DataField="Vendor" HeaderText="Supplier" ItemStyle-HorizontalAlign="left"></telerik:GridBoundColumn>
			  <telerik:GridBoundColumn DataField="PracticeID" HeaderText="" Visible="false"></telerik:GridBoundColumn>
			  <telerik:GridBoundColumn DataField="ThirdPartySupplierID" HeaderText="" Visible="false"></telerik:GridBoundColumn>
			  <telerik:GridBoundColumn DataField="LinkThirdPartySupplierID" HeaderText="LinkThirdPartySupplierID" Visible="false"></telerik:GridBoundColumn>
			  <telerik:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" HeaderText="PracticeCatalogSupplierBrandID" Visible="false"></telerik:GridBoundColumn>
			  <telerik:GridBoundColumn DataField="IsVendor" DataType="System.Byte" HeaderText="IsVendor" Visible="false"></telerik:GridBoundColumn>
			  <telerik:GridBoundColumn DataField="Sequence" HeaderText="LinkThirdPartySupplierID" Visible="false"></telerik:GridBoundColumn>
			</Columns>
			<DetailTables>
			<telerik:GridTableView EnableColumnsViewState="false" Caption="" DataKeyNames="PracticeCatalogSupplierBrandID, ThirdPartyProductID, PracticeCatalogProductID"
				HierarchyLoadMode="ServerOnDemand" Name="gtvSupplierProduct" PagerStyle-HorizontalAlign="Center" PageSize="15" AllowPaging="false">
			  <ParentTableRelation>
				<telerik:GridRelationFields DetailKeyField="PracticeCatalogSupplierBrandID" MasterKeyField="PracticeCatalogSupplierBrandID"/>
			  </ParentTableRelation>
			  <Columns>
				<telerik:GridBoundColumn DataField="ShortName" DataType="System.String" HeaderText="Product" UniqueName="ShortName" Visible="true"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="Code" DataType="System.String" HeaderText="Code" UniqueName="Code" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="Packaging" DataType="System.String" HeaderText="Pkg." Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="Side" DataType="System.String" HeaderText="Side" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="Size" DataType="System.String" HeaderText="Size" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
				<telerik:GridTemplateColumn HeaderText="Modifiers" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
				  <ItemTemplate>
					<span class="comma-list">
					  <span><%#  Eval("Mod1") %></span>
					  <span><%#  Eval("Mod2") %></span>
					  <span><%#  Eval("Mod3") %></span>
					</span>
				  </ItemTemplate>
				</telerik:GridTemplateColumn>
				<telerik:GridBoundColumn DataField="Mod1" DataType="System.String" HeaderText="Mod" Visible="false"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="Mod2" DataType="System.String" HeaderText="Mod" Visible="false"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="Mod3" DataType="System.String" HeaderText="Mod" Visible="false"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="Gender" DataType="System.String" HeaderText="Gender" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="Color" DataType="System.String" HeaderText="Color" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="HCPCSString" DataType="System.String" HeaderText="HCPCs" Visible="true"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="WholesaleCost" DataFormatString="{0:C}" DataType="System.Decimal" HeaderText="Wholesale Cost" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"/>
				<telerik:GridBoundColumn DataField="UPCCode" DataType="System.String" HeaderText="UPC Code" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="IsDiscontinued" DataType="System.Boolean" HeaderText="IsDiscontinued" Visible="false"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="IsDeletable" DataType="System.Boolean" HeaderText="IsDeletable" Visible="false"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="ThirdPartyProductID" DataType="System.Int32" HeaderText="ThirdPartyProductID" Visible="false"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" DataType="System.Int32" HeaderText="PracticeCatalogSupplierBrandID" Visible="false"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="PracticeCatalogProductID" DataType="System.Int32" HeaderText="PracticeCatalogProductID" Visible="false"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="UserMessage" DataType="System.String" HeaderText="UserMessage" Visible="false"></telerik:GridBoundColumn>
			  </Columns>
			</telerik:GridTableView>
			<telerik:GridTableView AutoGenerateColumns="false" DataKeyNames="PracticeID, LinkThirdPartySupplierID, ThirdPartySupplierID, PracticeCatalogSupplierBrandID"
				HierarchyLoadMode="ServerOnDemand" Name="gtvSupplier" GridLines="Both" AllowPaging="false">
			  <ParentTableRelation>
				<telerik:GridRelationFields DetailKeyField="LinkThirdPartySupplierID" MasterKeyField="LinkThirdPartySupplierID"/>
			  </ParentTableRelation>
			  <Columns>
				<telerik:GridBoundColumn DataField="Brand" HeaderText="Brand" Visible="true"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="IsVendorBrand" DataType="System.Byte" HeaderText="IsVendorBrand" Visible="false"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" HeaderText="" Visible="false"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="PracticeID" HeaderText="" Visible="false"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="ThirdPartySupplierID" HeaderText="" Visible="false"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="LinkThirdPartySupplierID" HeaderText="LinkThirdPartySupplierID" Visible="false"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn DataField="Sequence" HeaderText="Sequence" Visible="false"></telerik:GridBoundColumn>
			  </Columns>
			  <DetailTables>
				<telerik:GridTableView AllowFilteringByColumn="false" AllowPaging="false" EnableColumnsViewState="false" AutoGenerateColumns="false"
					Caption="" CommandItemDisplay="Top" DataKeyNames="PracticeCatalogSupplierBrandID, ThirdPartyProductID, PracticeCatalogProductID"
					HierarchyLoadMode="ServerOnDemand" Name="gtvProductReplacement" PageSize="15">
				  <CommandItemSettings ShowRefreshButton="false" ShowAddNewRecordButton="false" />
				  <ParentTableRelation>
					<telerik:GridRelationFields DetailKeyField="PracticeCatalogSupplierBrandID" MasterKeyField="PracticeCatalogSupplierBrandID"/>
				  </ParentTableRelation>
				  <Columns>
					<telerik:GridBoundColumn DataField="ShortName" DataType="System.String" HeaderText="Product" UniqueName="ShortName" Visible="true"></telerik:GridBoundColumn>
					<telerik:GridBoundColumn DataField="Code" DataType="System.String" HeaderText="Code" UniqueName="Code" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
					<telerik:GridBoundColumn DataField="Packaging" DataType="System.String" HeaderText="Pkg." Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
					<telerik:GridBoundColumn DataField="Side" DataType="System.String" HeaderText="Side" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
					<telerik:GridBoundColumn DataField="Size" DataType="System.String" HeaderText="Size" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
					<telerik:GridTemplateColumn HeaderText="Modifiers" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
					  <ItemTemplate>
						<span class="comma-list">
						  <span><%#  Eval("Mod1") %></span>
						  <span><%#  Eval("Mod2") %></span>
						  <span><%#  Eval("Mod3") %></span>
						</span>
					  </ItemTemplate>
					</telerik:GridTemplateColumn>
					<telerik:GridBoundColumn DataField="Mod1" DataType="System.String" HeaderText="Mod" Visible="false"></telerik:GridBoundColumn>
					<telerik:GridBoundColumn DataField="Mod2" DataType="System.String" HeaderText="Mod" Visible="false"></telerik:GridBoundColumn>
					<telerik:GridBoundColumn DataField="Mod3" DataType="System.String" HeaderText="Mod" Visible="false"></telerik:GridBoundColumn>
					<telerik:GridBoundColumn DataField="Gender" DataType="System.String" HeaderText="Gender" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
					<telerik:GridBoundColumn DataField="Color" DataType="System.String" HeaderText="Color" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
					<telerik:GridBoundColumn DataField="HCPCSString" DataType="System.String" HeaderText="HCPCs" Visible="true"></telerik:GridBoundColumn>
					<telerik:GridBoundColumn DataField="WholesaleCost" DataFormatString="{0:C}" DataType="System.Decimal" HeaderText="Wholesale Cost" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"/>
					<telerik:GridBoundColumn DataField="UPCCode" DataType="System.String" HeaderText="UPC Code" Visible="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
					<telerik:GridBoundColumn DataField="IsDiscontinued" DataType="System.Boolean" HeaderText="IsDiscontinued" Visible="false"></telerik:GridBoundColumn>
					<telerik:GridBoundColumn DataField="IsDeletable" DataType="System.Boolean" HeaderText="IsDeletable" Visible="false"></telerik:GridBoundColumn>
					<telerik:GridBoundColumn DataField="ThirdPartyProductID" DataType="System.Int32" HeaderText="ThirdPartyProductID" Visible="false"></telerik:GridBoundColumn>
					<telerik:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" DataType="System.Int32" HeaderText="PracticeCatalogSupplierBrandID" Visible="false"></telerik:GridBoundColumn>
					<telerik:GridBoundColumn DataField="PracticeCatalogProductID" DataType="System.Int32" HeaderText="PracticeCatalogProductID" Visible="false"></telerik:GridBoundColumn>
					<telerik:GridBoundColumn DataField="UserMessage" DataType="System.String" HeaderText="UserMessage" Visible="false"></telerik:GridBoundColumn>
				  </Columns>
				</telerik:GridTableView>
			  </DetailTables>
			</telerik:GridTableView>
			</DetailTables>
			</MasterTableView>
			</bvu:VisionRadGridAjax>
		</div>
		<div style='<%# ShowCombinedCatalogReplacement %>' runat="server">
			<div>
				<div>
					<span style="font-size: 17px"><br /></span>
					<span style="font-size: 17px">New Replacement Product</span>
					<span style="font-size: 7px"><br /></span>
				</div>
			</div>
			<bvu:VisionRadGridAjax ID="grdReplacementCombinedCatalog" runat="server" EnableLinqExpressions="false" AllowPaging="false"
				AllowMultiRowSelection="False" OnNeedDataSource="grdReplacementCombinedCatalog_NeedDataSource" OnItemCommand="grdReplacementCombinedCatalog_ItemCommand"
				OnDetailTableDataBind="grdReplacementCombinedCatalog_OnDetailTableDataBind" OnPageIndexChanged="grdReplacementCombinedCatalog_PageIndexChanged"
				OnPreRender="grdReplacementCombinedCatalog_PreRender" AddNonBreakingSpace="false" Skin="Breg" EnableEmbeddedSkins="False" ShowGroupPanel="false"
				PagerStyle-AlwaysVisible="true" ClientSettings-EnablePostBackOnRowClick="true">
				<MasterTableView DataKeyNames="PracticeCatalogSupplierBrandID" HierarchyLoadMode="ServerOnDemand">
					<DetailTables>
						<telerik:GridTableView runat="server" DataKeyNames="MasterCatalogProductID" AllowPaging="false" Width="100%">
							<ParentTableRelation>
								<telerik:GridRelationFields DetailKeyField="PracticeCatalogSupplierBrandID" MasterKeyField="PracticeCatalogSupplierBrandID" />
							</ParentTableRelation>
							<Columns>
								<telerik:GridBoundColumn DataField="ShortName" HeaderText="Name" UniqueName="ShortName" />
								<telerik:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code" />
								<telerik:GridBoundColumn DataField="LeftRightSide" HeaderText="Side" UniqueName="LeftRightSide" ItemStyle-HorizontalAlign="Center"
									ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" />
								<telerik:GridBoundColumn DataField="Size" HeaderText="Size" UniqueName="Size" />
								<telerik:GridBoundColumn DataField="Color" HeaderText="Color" UniqueName="Color" />
								<telerik:GridBoundColumn DataField="WholesaleCost" DataFormatString="{0:C}" HeaderText="WC" UniqueName="WholesaleCost" ItemStyle-HorizontalAlign="Right"
									ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Right" />
								<telerik:GridCheckBoxColumn DataField="IsLogoAblePart" HeaderText="Logo" UniqueName="IsLogoAblePart" ItemStyle-HorizontalAlign="Center"
									ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" />
								<telerik:GridBoundColumn DataField="MastercatalogProductID" Display="false" UniqueName="MastercatalogProductID" />
								<telerik:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" UniqueName="PracticeCatalogSupplierBrandID" Visible="false" />
								<telerik:GridBoundColumn DataField="PracticeCatalogProductID" UniqueName="PracticeCatalogProductID" Visible="false" />
								<telerik:GridBoundColumn DataField="IsThirdPartyProduct" UniqueName="IsThirdPartyProduct" Visible="false" />
							</Columns>
						</telerik:GridTableView>
					</DetailTables>
					<Columns>
						<telerik:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" UniqueName="PracticeCatalogSupplierBrandID" Visible="False" />
						<telerik:GridBoundColumn DataField="SupplierName" HeaderText="Supplier" UniqueName="SupplierName" />
						<telerik:GridBoundColumn DataField="BrandName" HeaderText="Brand" UniqueName="BrandName" />
					</Columns>
				</MasterTableView>
			</bvu:VisionRadGridAjax>
		</div>
	</div>
</div>
