using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class PracticeAdminBreadcrumbs : System.Web.UI.UserControl
{
	protected void Page_Load( object sender, EventArgs e )
	{

		if ( !Page.IsPostBack )
		{
			SiteMapPath1.SiteMapProvider = ( ( Session["SiteMapPath1_SiteMapProvider"] != null ) ? Session["SiteMapPath1_SiteMapProvider"].ToString() : "" );

			if ( SiteMapPath1.SiteMapProvider == "" )
			{
				if ( Session["UserRole"] != null )
				{
					if ( Session["UserRole"].ToString() == "BregAdmin" )
					{
						SiteMapPath1.SiteMapProvider = "WebBregAdmin";
						Session["SiteMapPath1_SiteMapProvider"] = SiteMapPath1.SiteMapProvider.ToString();
					}
					if ( Session["UserRole"].ToString() == "PracticeAdmin" )
					{
						SiteMapPath1.SiteMapProvider = "PracticeAdmin";
						Session["SiteMapPath1_SiteMapProvider"] = SiteMapPath1.SiteMapProvider.ToString();
					}
				}
			}
		}
	}
}
