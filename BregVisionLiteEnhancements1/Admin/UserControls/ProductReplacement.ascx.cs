﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.Admin.PracticeSetup;
using BregVision.TelerikAjax;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.WebControls;
using ClassLibrary.DAL;

namespace BregVision.Admin.UserControls
{
    public partial class ProductReplacement : Bases.UserControlBase
	{
		public event EventHandler WasClickedOn;

		protected string ShowMasterCatalogReplacement
		{
			get
			{
				return IsShowMasterCatalogReplacement ? "" : "display: none";
			}
		}
		private bool IsShowMasterCatalogReplacement
		{
			get
			{
				return !IsShowCombinedCatalogReplacement;
			}
		}
		protected string ShowThirdPartyCatalogReplacement
		{
			get
			{
				return IsShowThirdPartyCatalogReplacement ? "" : "display: none";
			}
		}
		private bool IsShowThirdPartyCatalogReplacement
		{
			get
			{
				return (!IsShowCombinedCatalogReplacement && IsPracticeSpecific);
			}
		}
		protected string ShowCombinedCatalogReplacement
		{
			get
			{
				return IsShowCombinedCatalogReplacement ? "" : "display: none";
			}
		}
		public int MasterCatalogProductID
		{
			get
			{
				string mcpID = ParentMasterCatalogProductID.Value;
				int mcpIDVal = 0;
				if (Int32.TryParse(mcpID, out mcpIDVal))
					return mcpIDVal;
				return 0;
			}
			set
			{
				ParentMasterCatalogProductID.Value = value.ToString();
				grdProductReplacement.DataSource = null;
				grdProductReplacement.Rebind();
			}
		}
		public int ThirdPartyProductID
		{
			get
			{
				string tppID = ParentThirdPartyProductID.Value;
				int tppIDVal = 0;
				if (Int32.TryParse(tppID, out tppIDVal))
					return tppIDVal;
				return 0;
			}
			set
			{
				ParentThirdPartyProductID.Value = value.ToString();
				grdProductReplacement.DataSource = null;
				grdProductReplacement.Rebind();
			}
		}
		public bool IsPracticeSpecific
		{
			get
			{
				string ips = ParentIsPracticeSpecific.Value;
				bool ipsVal = false;
				if (bool.TryParse(ips, out ipsVal))
					return ipsVal;
				return false;
			}
			set
			{
				ParentIsPracticeSpecific.Value = value.ToString();
				grdProductReplacement.DataSource = null;
				grdProductReplacement.Rebind();
			}
		}
		public bool IsShowCombinedCatalogReplacement
		{
			get
			{
				string ips = ParentIsShowCombinedCatalogReplacement.Value;
				bool ipsVal = false;
				if (bool.TryParse(ips, out ipsVal))
					return ipsVal;
				return false;
			}
			set
			{
				ParentIsShowCombinedCatalogReplacement.Value = value.ToString();
				grdReplacementCombinedCatalog.DataSource = null;
				grdReplacementCombinedCatalog.Rebind();
			}
		}
		protected void page_Init(object sender, EventArgs e)
        {
            RadInputManagerImplementation_Setup();
        }


        #region RadInputManager implementation

        private bool _IsDetailTableBinding = false;
        
        private class RadInputManagerSettingItem
        {
            public string RadInputManagerBehaviorID { get; set; }
            public string TargetControlClientID { get; set; }
        }

        private List<RadInputManagerSettingItem> RadInputManagerSettings = new List<RadInputManagerSettingItem>();

        private static class RadInputBehaviorIDs
        {
            public static readonly string CurrencyValue = "currencySetting";
            public static readonly string StockValue = "stockSetting";
        }

        protected void RadNumericTextBox_Load1(object sender, EventArgs e)
        {
            if (!_IsDetailTableBinding)
                return;

            var t = sender as TextBox;
            if (t == null)
                return;

            var s =
                new RadInputManagerSettingItem
                {
                    RadInputManagerBehaviorID = RadInputBehaviorIDs.StockValue,
                    TargetControlClientID = t.ClientID
                };
            RadInputManagerSettings.Add(s);
        }

        protected void RadNumericTextBox_Load2(object sender, EventArgs e)
        {
            if (!_IsDetailTableBinding)
                return;

            var t = sender as TextBox;
            if (t == null)
                return;

            var s =
                new RadInputManagerSettingItem
                {
                    RadInputManagerBehaviorID = RadInputBehaviorIDs.CurrencyValue,
                    TargetControlClientID = t.ClientID
                };
            RadInputManagerSettings.Add(s);
        }

        //private void RadInputManager_EmitClientSideJavaScript(object sender, EventArgs e)
        //{
        //    // output script storage
        //    var script = new System.Text.StringBuilder();

        //    // fill script if there are settings to attach to RadInputManager, leave as empty script if not
        //    // NOTE: helps prevent unintentional script execution on each page load (from other async postbacks)
        //    if (RadInputManagerSettings.Count > 0)
        //    {
        //        // convert list into a lookup table to group settings by behavior id
        //        var behavior_lookup_table = RadInputManagerSettings.ToLookup(x => x.RadInputManagerBehaviorID);

        //        // loop through each behavior and render script
        //        foreach (var behavior_group in behavior_lookup_table)
        //        {
        //            // get behavior id
        //            var behavior_id = behavior_group.Key;

        //            // set up a local var to contain the InputSetting object
        //            script.Append(string.Format(@"var {0}_s=$find('{1}').get_inputSettings('{0}');", behavior_id, RadInputManager1.ClientID));

        //            // output lines to add target control to behavior
        //            foreach (var target in behavior_group)
        //                script.Append(string.Format(@"{0}_s.addTargetInput('{1}');", behavior_id, target.TargetControlClientID));
        //        }
        //    }

            //// register the startup script
            //var ajaxmanager = Telerik.Web.UI.RadAjaxManager.GetCurrent(Page);
            //if (ajaxmanager != null)
            //{
            //    ScriptManager.RegisterStartupScript(
            //        Page,
            //        typeof(Page),
            //        "BregRadInputManagerLoad",
            //        string.Format(
            //            @"var BregPracticeCatalogRadInputManagerLoadHandler=function(){{Sys.Application.remove_load(BregPracticeCatalogRadInputManagerLoadHandler);{0}}};Sys.Application.add_load(BregPracticeCatalogRadInputManagerLoadHandler);",
            //            script.ToString()),
            //        true);
            //}
        //}

        private void RadInputManagerImplementation_Setup()
        {
            //Page.PreRenderComplete += new EventHandler(RadInputManager_EmitClientSideJavaScript);
            //Page.PreRenderComplete += new EventHandler(SplitCodeToggle_EmitClientSideJavaScript);
        }

        #endregion

        private void SplitCodeToggle_EmitClientSideJavaScript(object sender, EventArgs e)
        {
            //string scriptstring = "bindInitialSplitState();";
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "bindInitialSplitState", scriptstring, true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
		}
		protected void grdReplacementMasterCatalog_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
		{
			if (this.WasClickedOn != null)
				this.WasClickedOn(this, EventArgs.Empty);

			if (e.CommandName == "RowClick")
			{
				try
				{
					Telerik.Web.UI.GridDataItem item = e.Item as Telerik.Web.UI.GridDataItem;
					var mcpID = item.GetDataKeyValue("MasterCatalogProductID");
					var tppID = item.GetDataKeyValue("ThirdPartyProductID");
					var pcpID = item.GetDataKeyValue("PracticeCatalogProductID");
					int mcpIDVal = 0;
					if (mcpID != null)
					{
						if (!Int32.TryParse(mcpID.ToString(), out mcpIDVal))
							mcpID = null;
					}
					int tppIDVal = 0;
					if (tppID != null)
					{
						if (!Int32.TryParse(tppID.ToString(), out tppIDVal))
							tppID = null;
					}
					int? masterCatalogProductID = MasterCatalogProductID == 0 ? null : (int?)MasterCatalogProductID;
					int? thirdPartyProductID = ThirdPartyProductID == 0 ? null : (int?)ThirdPartyProductID;
					int? pID = ((practiceID == 0 || !IsPracticeSpecific) ? null : (int?)practiceID);
					if ((masterCatalogProductID != null || thirdPartyProductID != null) && (mcpID != null || tppID != null))
					{
						DAL.ProductReplacement.ProductReplacement.UpdateProductReplacement(masterCatalogProductID, thirdPartyProductID,
							mcpID == null ? null : (int?)mcpIDVal, tppID == null ? null : (int?)tppIDVal, pID, null, null, 0, true);
						grdProductReplacement.Rebind();
					}
				}
				catch (Exception ex)
				{
				}
			}
		}

		protected void grdReplacementCombinedCatalog_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
		{
			if (this.WasClickedOn != null)
				this.WasClickedOn(this, EventArgs.Empty);

			if (e.CommandName == "RowClick")
			{
				try
				{
					Telerik.Web.UI.GridDataItem item = e.Item as Telerik.Web.UI.GridDataItem;
					var isThirdPartyProduct = item.GetColumnBoolean("IsThirdPartyProduct");
					var mcpID = item.GetDataKeyValue("MasterCatalogProductID");
					var tppID = item.GetDataKeyValue("ThirdPartyProductID");
					var pcpID = item.GetDataKeyValue("PracticeCatalogProductID");

					if (isThirdPartyProduct)
					{
						tppID = mcpID;
						mcpID = null;
					}
					int mcpIDVal = 0;
					if (mcpID != null)
					{
						if (!Int32.TryParse(mcpID.ToString(), out mcpIDVal))
							mcpID = null;
					}
					int tppIDVal = 0;
					if (tppID != null)
					{
						if (!Int32.TryParse(tppID.ToString(), out tppIDVal))
							tppID = null;
					}
					int? masterCatalogProductID = MasterCatalogProductID == 0 ? null : (int?)MasterCatalogProductID;
					int? thirdPartyProductID = ThirdPartyProductID == 0 ? null : (int?)ThirdPartyProductID;
					int? pID = ((practiceID == 0 || !IsPracticeSpecific) ? null : (int?)practiceID);
					if ((masterCatalogProductID != null || thirdPartyProductID != null) && (mcpID != null || tppID != null))
					{
						DAL.ProductReplacement.ProductReplacement.UpdateProductReplacement(masterCatalogProductID, thirdPartyProductID,
							mcpID == null ? null : (int?)mcpIDVal, tppID == null ? null : (int?)tppIDVal, pID, null, null, 0, true);
						grdProductReplacement.Rebind();
					}
				}
				catch (Exception ex)
				{
				}
			}
		}

		protected void grdReplacementThirdPartyCatalog_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
		{
			if (this.WasClickedOn != null)
				this.WasClickedOn(this, EventArgs.Empty);

			if (e.CommandName == "RowClick")
			{
				try
				{
					Telerik.Web.UI.GridDataItem item = e.Item as Telerik.Web.UI.GridDataItem;
					var mcpID = item.GetDataKeyValue("MasterCatalogProductID");
					var tppID = item.GetDataKeyValue("ThirdPartyProductID");
					int mcpIDVal = 0;
					if (mcpID != null)
					{
						if (!Int32.TryParse(mcpID.ToString(), out mcpIDVal))
							mcpID = null;
					}
					int tppIDVal = 0;
					if (tppID != null)
					{
						if (!Int32.TryParse(tppID.ToString(), out tppIDVal))
							tppID = null;
					}
					int? masterCatalogProductID = MasterCatalogProductID == 0 ? null : (int?)MasterCatalogProductID;
					int? thirdPartyProductID = ThirdPartyProductID == 0 ? null : (int?)ThirdPartyProductID;
					int? pID = ((practiceID == 0 || !IsPracticeSpecific) ? null : (int?)practiceID);
					if ((masterCatalogProductID != null || thirdPartyProductID != null) && (mcpID != null || tppID != null))
					{
						DAL.ProductReplacement.ProductReplacement.UpdateProductReplacement(masterCatalogProductID, thirdPartyProductID,
							mcpID == null ? null : (int?)mcpIDVal, tppID == null ? null : (int?)tppIDVal, pID, null, null, 0, true);
						grdProductReplacement.Rebind();
					}
				}
				catch (Exception ex)
				{
				}
			}
		}

		protected void grdProductReplacement_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
		{
			if (this.WasClickedOn != null)
				this.WasClickedOn(this, EventArgs.Empty);

			if (e.CommandName == "Delete")
			{
				try
				{
					Telerik.Web.UI.GridDataItem item = e.Item as Telerik.Web.UI.GridDataItem;
					string id = item.GetDataKeyValue("ProductReplacementID").ToString();
					int idVal = 0;
					if (Int32.TryParse(id, out idVal))
					{
						DAL.ProductReplacement.ProductReplacement.DeleteProductReplacement(idVal);
						grdProductReplacement.DataSource = null;
						grdProductReplacement.Rebind();
					}
				}
				catch (Exception ex)
				{
				}
			}
		}

		private void InitializeAjaxSettings()
        {
            //var radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);
            //if (radAjaxManager != null)
            //{
            //    radAjaxManager.AjaxRequest += new Telerik.Web.UI.RadAjaxControl.AjaxRequestDelegate(RadAjaxManager_AjaxRequest);
            //    radAjaxManager.AjaxSettings.AddAjaxSetting(radAjaxManager, RadInputManager1);
            //}
        }

        public bool WriteAjaxScript()
        {
            //if (!Context.User.IsInRole("BregAdmin"))
            //{
            //    if (Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page) != null)
            //        return true;
            //}
            return false;
        }

		protected void grdReplacementMasterCatalog_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
		{
			if (this.WasClickedOn != null)
				this.WasClickedOn(this, EventArgs.Empty);

			grdReplacementMasterCatalog.SetExpansionOnPageIndexChanged();
		}

		protected void grdReplacementThirdPartyCatalog_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
		{
			if (this.WasClickedOn != null)
				this.WasClickedOn(this, EventArgs.Empty);

			grdReplacementThirdPartyCatalog.SetExpansionOnPageIndexChanged();
		}

		protected void grdReplacementCombinedCatalog_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
		{
			if (this.WasClickedOn != null)
				this.WasClickedOn(this, EventArgs.Empty);

			grdReplacementCombinedCatalog.SetExpansionOnPageIndexChanged();
		}

		protected void grdProductReplacement_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
		{
			if (this.WasClickedOn != null)
				this.WasClickedOn(this, EventArgs.Empty);

			grdProductReplacement.SetExpansionOnPageIndexChanged();
		}

		protected void grdReplacementMasterCatalog_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
		{
			if (IsShowMasterCatalogReplacement && (!e.IsFromDetailTable || TelerikAjax.UIHelper.IsGridRebindReasonSet(e.RebindReason, Telerik.Web.UI.GridRebindReason.InitialLoad)))
				grdReplacementMasterCatalog.DataSource = DAL.Practice.Catalog.GetMasterCatalogSuppliers();
		}

		protected void grdReplacementThirdPartyCatalog_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
		{
			if (IsShowThirdPartyCatalogReplacement && (!e.IsFromDetailTable || TelerikAjax.UIHelper.IsGridRebindReasonSet(e.RebindReason, Telerik.Web.UI.GridRebindReason.InitialLoad)))
				grdReplacementThirdPartyCatalog.DataSource = GetVendors(practiceID);
		}

		protected void grdReplacementCombinedCatalog_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
		{
			if (IsShowCombinedCatalogReplacement && (!e.IsFromDetailTable || TelerikAjax.UIHelper.IsGridRebindReasonSet(e.RebindReason, Telerik.Web.UI.GridRebindReason.InitialLoad)))
				grdReplacementCombinedCatalog.DataSource = DAL.Practice.Catalog.GetPracticeCatalogSuppliers(string.Empty, string.Empty, practiceID);
		}

		private DataTable GetVendors(int practiceID)
		{
			DAL.Practice.ThirdPartyProduct dalTPP = new DAL.Practice.ThirdPartyProduct();
			DataTable dt = dalTPP.GetVendors(practiceID);
			return dt;
		}

		public DataTable GetBrands(int thirdPartySupplierID)
		{
			DAL.Practice.ThirdPartyProduct dalTPP = new DAL.Practice.ThirdPartyProduct();
			DataTable dt = dalTPP.GetBrands(thirdPartySupplierID);                  // .GetVendors(practiceID);
			return dt;
		}

		public DataTable GetProducts(int practiceCatalogSupplierBrandID)
		{
			DAL.Practice.ThirdPartyProduct dalTPP = new DAL.Practice.ThirdPartyProduct();
			DataTable dt = dalTPP.GetProducts(practiceCatalogSupplierBrandID);      // .GetVendors(practiceID);
			return dt;
		}

		protected void grdProductReplacement_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
		{
			if (!e.IsFromDetailTable || TelerikAjax.UIHelper.IsGridRebindReasonSet(e.RebindReason, Telerik.Web.UI.GridRebindReason.InitialLoad))
			{
				int? masterCatalogProductID = MasterCatalogProductID == 0 ? null : (int?)MasterCatalogProductID;
				int? thirdPartyProductID = ThirdPartyProductID == 0 ? null : (int?)ThirdPartyProductID;
				int? pID = ((practiceID == 0 || !IsPracticeSpecific) ? null : (int?)practiceID);
				grdProductReplacement.DataSource = null;
				if (masterCatalogProductID != null || thirdPartyProductID != null)
				{
					DataTable dt = DAL.ProductReplacement.ProductReplacement.GetProductReplacements(masterCatalogProductID, thirdPartyProductID, pID);
					ReplacementProductStatus.Visible = true;
					if (dt.Rows.Count > 0)
					{
						grdProductReplacement.DataSource = dt;
						ReplacementProductStatus.Visible = false;
					}
					grdProductReplacement.Visible = !ReplacementProductStatus.Visible;
				}
			}
		}

        private DAL.Practice.Catalog _catalogInstance = new DAL.Practice.Catalog();
		protected void grdReplacementMasterCatalog_OnDetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
		{
			_IsDetailTableBinding = true;

			var data = _catalogInstance.GetMasterCatalogDataBySupplier2(grdReplacementMasterCatalog.SearchCriteria, grdReplacementMasterCatalog.SearchText, e.DetailTableView.ParentItem.GetColumnInt32("SupplierID"));
			e.DetailTableView.DataSource = data;
			grdReplacementMasterCatalog.SetRecordCount(data.Count(x => !string.IsNullOrEmpty(x.ShortName)));
		}

		// Occurs during the data binding of the detail tables.
		// Check which detail table view is being bound.
		protected void grdReplacementThirdPartyCatalog_OnDetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
		{
			//AppendToLabel("RadGrid1_DetailTableDataBind: GTV: " + e.DetailTableView.Name.ToString() + ".");

			//  If the DetailTableView is in edit mode then return.
			Telerik.Web.UI.GridDataItem parentItem = (Telerik.Web.UI.GridDataItem)e.DetailTableView.ParentItem;
			if (parentItem.Edit)
			{
				return;
			}

			// Get the DetailTableView's name.
			string detailTableViewName = e.DetailTableView.Name.ToString();
			if (detailTableViewName == "gtvSupplier")
			{
				//  Check gtvSupplier's primary key, first make sure it is safe (exists)
				if (e.DetailTableView.GetColumnSafe("PracticeCatalogSupplierBrandID") != null)
				{
					int practiceCatalogSupplierBrandID = Convert.ToInt32(parentItem["PracticeCatalogSupplierBrandID"].Text.ToString());

					//If pcsbID = 0 and OptionC is false continue.
					if (practiceCatalogSupplierBrandID == 0 /*|| !isOptionC*/)  // If pcsbID = 0 and OptionC is false continue.check for option.)  
					{
						// Get the Brands of the Supplier via the gtvSupplier's Supplier to Brand linking key.
						if (e.DetailTableView.GetColumnSafe("ThirdPartySupplierID") != null)
						{
							int thirdPartySupplierID = Convert.ToInt32(parentItem["ThirdPartySupplierID"].Text.ToString());
							DataTable dtSupplier = GetBrands(thirdPartySupplierID);
							e.DetailTableView.DataSource = dtSupplier;
						}
					}
				}
			}

			//  Check if the Detail Table View is gtvProductReplacement.
			if (detailTableViewName == "gtvProductReplacement")
			{
				if (e.DetailTableView.GetColumnSafe("PracticeCatalogSupplierBrandID") != null)
				{
					int practiceCatalogSupplierBrandID = Convert.ToInt32(parentItem["PracticeCatalogSupplierBrandID"].Text.ToString());
					DataTable dtProduct = GetProducts(practiceCatalogSupplierBrandID);
					e.DetailTableView.DataSource = dtProduct;

					////  Clear the user message if neccessary.
					//if (clearProductCaption)
					//{
					//	e.DetailTableView.Caption = "";
					//	clearProductCaption = false;
					//}
					int count = e.DetailTableView.Items.Count;
				}
			}


			// Check if the Detail Table View is gtvSupplierProduct.
			if (detailTableViewName == "gtvSupplierProduct")
			{
				if (e.DetailTableView.GetColumnSafe("PracticeCatalogSupplierBrandID") != null)
				{
					int practiceCatalogSupplierBrandID = Convert.ToInt32(parentItem["PracticeCatalogSupplierBrandID"].Text.ToString());
					// If the supplier is the brand then connect the supplier directly to the products.
					if (practiceCatalogSupplierBrandID > 0 /*&& isOptionC*/)  // check for option.
					{
						DataTable dtProduct = GetProducts(practiceCatalogSupplierBrandID);
						e.DetailTableView.DataSource = dtProduct;
						//if (clearProductCaption)
						//{
						//	e.DetailTableView.Caption = "";
						//	clearProductCaption = false;
						//}
					}
				}
			}
		}

		protected void grdReplacementCombinedCatalog_OnDetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
		{
			_IsDetailTableBinding = true;

			var data = DAL.Practice.Catalog.GetPracticeCatalogDataBySupplier2("Code", string.Empty, practiceID,
				e.DetailTableView.ParentItem.GetColumnInt32("PracticeCatalogSupplierBrandID"));
			e.DetailTableView.DataSource = data;
			grdReplacementCombinedCatalog.SetRecordCount(data.Count(x => !string.IsNullOrEmpty(x.ShortName)));
		}

		protected void grdReplacementMasterCatalog_PreRender(object sender, EventArgs e)
		{
			grdReplacementMasterCatalog.ExpandGrid();
		}

		protected void grdReplacementThirdPartyCatalog_PreRender(object sender, EventArgs e)
		{
			grdReplacementThirdPartyCatalog.ExpandGrid();
		}

		protected void grdReplacementCombinedCatalog_PreRender(object sender, EventArgs e)
		{
			grdReplacementCombinedCatalog.ExpandGrid();
		}

		protected void grdProductReplacement_PreRender(object sender, EventArgs e)
		{
			grdProductReplacement.ExpandGrid();
		}

		public DataSet GetMasterCatalogDataBySupplier(string SearchCriteria, string SearchText)
		{
			if (grdReplacementMasterCatalog.VisionDataSet == null)
			{
				grdReplacementMasterCatalog.VisionDataSet = DAL.Practice.Catalog.GetMasterCatalogDataBySupplier(SearchCriteria, SearchText);
			}

			return grdReplacementMasterCatalog.VisionDataSet;
		}

		public DataSet GetThirdPartyCatalogDataBySupplier(string SearchCriteria, string SearchText)
		{
			if (grdReplacementThirdPartyCatalog.VisionDataSet == null)
					grdReplacementThirdPartyCatalog.VisionDataSet = DAL.Practice.Catalog.GetPracticeCatalogSuppliers(SearchCriteria, SearchText, practiceID);

			return grdReplacementMasterCatalog.VisionDataSet;
		}
    }
}
