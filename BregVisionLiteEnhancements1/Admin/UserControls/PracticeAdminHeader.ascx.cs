using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BLL;

public partial class PracticeAdminHeader : System.Web.UI.UserControl
{

	int practiceID;
	string practiceName;

	const string PRACTICE_LOCATIONS = "./PracticeLocation_All.aspx";	
	
	const string PRACTICE_CONTACT = "./Practice_Contact.aspx";
	const string PRACTICE_BILL_TO_ADDRESS = "./Practice_BillingAddress.aspx";
	const string PRACTICE_PHYSICIANS = "./Practice_Physicians.aspx";
	//const string PRACTICE_PHYSICIANS = "./Test/Practice_Physicians.aspx";

	const string PRACTICE_SUPPLIER = "Practice_Admin.aspx";   //Only for alpha  //Use This When Developed    "./Practice_Supplier.aspx";
	const string PRACTICE_USER = "Practice_Admin.aspx";    //Only for alpha  //Use This When Developed  "./Practice_User_Detail.aspx";	

	//  const string PRACTICE_USER = "./Practice_User.aspx";
	//	const string PRACTICE_USER = "./Practice_User_Detail.aspx";   //only because real page does not compile.
	
	protected void Page_Load(object sender, EventArgs e)
    {
		if ( !Page.IsPostBack  )
		{
			practiceID = ( Session["PracticeID"] != null ) ? Convert.ToInt32( Session["PracticeID"] ) : -1;
			practiceName = ( Session["PracticeName"] != null ) ? Session["PracticeName"].ToString() : "";

			if ( practiceName == "" )
			{
				practiceName = GetPracticeName( practiceID );
				Session["PracticeName"] = practiceName;
			}
			
			lblPractice.Text = practiceName;
		}
    }

	protected string GetPracticeName( int practiceID )
	{
		// The following line is not used because the SelectName method is now static.
		//BLL.Practice.Practice bllPractice = new BLL.Practice.Practice();
		string practiceName = BLL.Practice.Practice.SelectName( practiceID );
		return practiceName;
	}


	protected void btnGoToPracticeLocations_Click( object sender, EventArgs e )
	{
		Response.Redirect( PRACTICE_LOCATIONS );
	}
			
	protected void btnGoToPracticeContact_Click( object sender, EventArgs e )
	{
		Response.Redirect( PRACTICE_CONTACT );			
	}
	protected void btnGoToPracticeBillToAddress_Click( object sender, EventArgs e )
	{
		Response.Redirect( PRACTICE_BILL_TO_ADDRESS );	
	}

	protected void btnGoToPracticePhysicians_Click( object sender, EventArgs e )
	{
		Response.Redirect( PRACTICE_PHYSICIANS  );		
	}


	protected void btnGoTo_Practice_Supplier_Click( object sender, EventArgs e )
	{
		Response.Redirect( PRACTICE_SUPPLIER );	
	}

	protected void btnGoTo_Practice_User_Click( object sender, EventArgs e )
	{
		Response.Redirect( PRACTICE_USER );	
	}
	
}
