<%@ Control Language="C#" AutoEventWireup="true" Inherits="PracticeAdminHeader" Codebehind="PracticeAdminHeader.ascx.cs" %>
<!-- PracticeAdminHeader -->
                
                
<table id="tablePracticeAdminHeader" style="text-align:justify; background-color:#f0f2f4; width:auto;" >
    <tr>
        <td align="left" colspan="5" style="height: 18px">
        </td>
    </tr>
    <tr valign="middle">
        <td  style="font-size: 12pt" align="left">
            <asp:Label ID="lblPractice" runat="server" Font-Bold="True" Font-Size="Larger" Text="Practice"></asp:Label><br />
        </td>
        <td style="font-size: 12pt" align="center">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        
        <td style="font-size: 12pt" align="center">
            <asp:LinkButton ID="btnGoToPracticeBillToAddress" OnClick="btnGoToPracticeBillToAddress_Click" AccessKey="1" OnClientClick="btnGoToPracticeBillToAddress_Click()" 
                Text="Bill To Address" ToolTip="Practice Centralized Bill To Address" runat="server" CausesValidation="false" ></asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
        </td>
        <td style="font-size: 12pt;" align="center">
            <asp:LinkButton ID="btnGoToPracticeContact" OnClick="btnGoToPracticeContact_Click" AccessKey="2"
                Text="Contact" ToolTip="Practice Main Contact" runat="server" CausesValidation="false">Contact</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
        </td>
        <td style="font-size: 12pt" align="center">
            <asp:LinkButton ID="btnGoToPracticePhysicians" OnClick="btnGoToPracticePhysicians_Click"
                Text="Physicians" ToolTip="Practice Physicians" runat="server" CausesValidation="false">Physicians</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
        </td>
    </tr>
    
    
    <tr  valign="middle" >
        <td colspan="2" align="left">
            &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="btnGoToPracticeLocations" OnClick="btnGoToPracticeLocations_Click" 
            Text="All Practice Locations" ToolTip="All Practice Locations" runat="server" CausesValidation="false"></asp:LinkButton>
        </td>
        <td style="font-size: 12pt" align="center">
            &nbsp;&nbsp;&nbsp;
        </td>
        
        <!-- Supplier and Users are not ready yet so set to practice header -->
        <td style="font-size: 12pt" align="center">
            <asp:LinkButton ID="btnGoTo_Practice_Supplier" OnClick="btnGoTo_Practice_Supplier_Click"  Text="Suppliers*" ForeColor="Black"
            ToolTip="*Under Construction:  Practice's Third Party Suppliers" runat="server" CausesValidation="false"></asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
        </td>
        <td style="font-size: 12pt" align="center">
            <asp:LinkButton ID="btnGoTo_Practice_User" Text="Users*" OnClick="btnGoTo_Practice_User_Click" ForeColor="Black"
            ToolTip="*Under Construction:  Practice Users" runat="server" CausesValidation="false"></asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
        </td>
    </tr>
    <tr>
        <td align="left" colspan="5" style="height: 18px">
        </td>
    </tr>
</table>                