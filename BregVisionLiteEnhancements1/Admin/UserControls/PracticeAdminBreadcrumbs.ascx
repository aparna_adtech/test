<%@ Control Language="C#" AutoEventWireup="true" Inherits="PracticeAdminBreadcrumbs" Codebehind="PracticeAdminBreadcrumbs.ascx.cs" %>
<%@ OutputCache Duration="15" VaryByParam="none" %>

<table border="0" cellpadding="1" cellspacing="1" style="width:auto;text-align:left;" summary="Breadcrumbs">
    <tr align="left">
        <td align="left">
            <asp:SiteMapPath ID="SiteMapPath1" runat="server" ShowToolTips="true"  />
        </td>
    </tr>
    <tr>
        <td align="right">
            &nbsp;
        </td>
    </tr>
</table>