using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using BLL;

public partial class PracticeAdminSettingsHeader : System.Web.UI.UserControl
{
	// variables to hold session values;
	int userID;
	int practiceID;
	int practiceLocationID;
	string practiceName;
	string practiceLocationName;

	// constants for web pages
	const string PRACTICE_ADMIN = "./PracticeAdmin.aspx";
	const string PRACTICE_SETTINGS_ALL = "./PracticeSettings_All.aspx";
	const string PRACTICE_SETTINGS_GENERAL = "./PracticeSettings_General.aspx";
	const string PRACTICE_SETTINGS_SITEBUILDER = "./PracticeSettings_SiteBuilder.aspx";


	protected void Page_Load( object sender, EventArgs e )
	{
		if ( !Page.IsPostBack )
		{
			GetSessionVariables( out userID, out practiceID, out practiceName, out practiceLocationID, out practiceLocationName );

			if ( practiceName == "" )
			{
				if ( practiceID > 0 )
				{
					practiceName = GetPracticeName( practiceID );
					Session["PracticeName"] = practiceName;
				}
			}

			//if ( practiceLocationName == "" )
			//{
			//	if ( practiceLocationID > 0 )
			//	{
			//		// implement the following line.
			//		//practiceLocationName = GetPracticeLocationName( practiceID );
			//		practiceLocationName = "Location Not Set";		//remove later.
			//		Session["PracticeLocationName"] = practiceLocationName;
			//	}
			//}

			lblPractice.Text = practiceName;
			lblPracticeLocation.Text = practiceLocationName;
		}
		else
		{
			practiceLocationID = ( Session["PracticeLocationID"] != null ) ? Convert.ToInt32( Session["PracticeLocationID"].ToString() ) : -1;
		}
    }

	protected void GetSessionVariables( out int userID, out int practiceID, out string practiceName,
					out int practiceLocationID, out string practiceLocationName )
	{
		userID = ( Session["UserID"] != null ) ? Convert.ToInt32( Session["UserID"].ToString() ) : -1;
		practiceID = ( Session["PracticeID"] != null ) ? Convert.ToInt32( Session["PracticeID"].ToString() ) : -1;
		practiceLocationID = ( Session["PracticeLocationID"] != null ) ? Convert.ToInt32( Session["PracticeLocationID"].ToString() ) : -1;
		practiceName = ( Session["PracticeName"] != null ) ? Session["PracticeName"].ToString() : "";
		practiceLocationName = ( Session["PracticeLocationName"] != null ) ? Session["PracticeLocationName"].ToString() : "";
	}


	protected string GetPracticeName( int practiceID )
	{
		// The following line is not used because the SelectName method is now static.
		//BLL.Practice.Practice bllPractice = new BLL.Practice.Practice();
		string practiceName = BLL.Practice.Practice.SelectName( practiceID );
		return practiceName;
	}

	// To Do Implement the BLL and DAL Classes for this.
	protected string GetPracticeLocationName( int practiceLocationID )
	{
		// The following line is not used because the SelectName method is now static.
		//BLL.PracticeLocation.PracticeLocation bllPL = new BLL.PracticeLocation.PracticeLocation();
		string practiceLocationName = BLL.PracticeLocation.PracticeLocation.SelectName( practiceID );
		return practiceLocationName;
	}

	// Practice Level
	protected void btnGoToPracticeAdmin_Click( object sender, EventArgs e )
	{
		Response.Redirect( PRACTICE_ADMIN );		
	}
	protected void btnGoToPracticeSettings_Click( object sender, EventArgs e )
	{
		Response.Redirect( PRACTICE_SETTINGS_ALL );
	}	
	protected void btnGoToPracticeSettingsGeneral_Click( object sender, EventArgs e )
	{
		Response.Redirect( PRACTICE_SETTINGS_GENERAL );			
	}
	protected void btnGoToPracticeSettings_SiteBuilder_Click( object sender, EventArgs e )
	{
		Response.Redirect( PRACTICE_SETTINGS_SITEBUILDER );	
	}
}