﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeCatalogProduct.ascx.cs" Inherits="BregVision.Admin.UserControls.General.PracticeCatalogProduct" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
  <AjaxSettings>
    <telerik:AjaxSetting AjaxControlID="grdProducts">
      <UpdatedControls>
        <telerik:AjaxUpdatedControl ControlID="grdProducts" LoadingPanelID="RadAjaxLoadingPanel1"/>
      </UpdatedControls>
    </telerik:AjaxSetting>
  </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" InitialDelayTime="500" EnableAjaxSkinRendering="True" BackgroundPosition="Center" Skin="Breg" EnableEmbeddedSkins="False">
  <img src="../../App_Themes/Breg/images/Common/loading.gif"/>
</telerik:RadAjaxLoadingPanel>
<table id="tblMasterTable" runat="server" border="1" cellpadding="0" cellspacing="0" summary="Summary Description" width="855px" style="margin: 0 auto;">
  <tr>
    <td align="left" style="color: Blue; width: auto">
      <asp:Label ID="Label2" runat="server" Font-Size="XX-Small" Text=""></asp:Label>
    </td>
  </tr>
  <tr>
    <td>
      <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <!-- Content Header-->
        <tr align="center" class="AdminPageTitle">
          <td>
            <asp:Label ID="lbl" runat="server" Text="Practice Catalog"></asp:Label>
            <bvu:Help ID="help1" runat="server" HelpID="127"/>
            <br/>
          </td>
        </tr>
        <tr>
          <td>
            <!-- Grid -->
            <bvu:VisionRadGridAjax AutoGenerateColumns="False" ID="grdProducts"
                                   DataSourceID="LinqDataSource1"
                                   OnDetailTableDataBind="grdSuppliers_OnDetailTableDataBind"
                                   EnableViewState="true"
                                   AllowFilteringByColumn="True" AllowPaging="True"
                                   AllowSorting="True" runat="server" ClientSettings-Selecting-AllowRowSelect="true">
              <PagerStyle Mode="NextPrevAndNumeric"/>
              <GroupingSettings CaseSensitive="false"/>
              <MasterTableView TableLayout="Fixed" AllowFilteringByColumn="false" DataKeyNames="PracticeCatalogSupplierBrandID">
                <Columns>
                  <telerik:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" HeaderStyle-BorderColor="#A2B6CB"
                                           HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" UniqueName="PracticeCatalogSupplierBrandID"
                                           Visible="false">
                  </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="SupplierName" HeaderStyle-BorderColor="#A2B6CB"
                                           HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderText="Supplier"
                                           UniqueName="SupplierName">
                  </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="BrandName" HeaderStyle-BorderColor="#A2B6CB"
                                           HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderText="Brand"
                                           UniqueName="BrandName">
                  </telerik:GridBoundColumn>
                </Columns>
                <DetailTables>
                  <telerik:GridTableView runat="server" DataKeyNames="PracticeCatalogSupplierBrandID" DataSourceID="LinqDataSource2" AllowFilteringByColumn="true" CommandItemDisplay="Top">
                    <ParentTableRelation>
                      <telerik:GridRelationFields DetailKeyField="PracticeCatalogSupplierBrandID" MasterKeyField="PracticeCatalogSupplierBrandID"/>
                    </ParentTableRelation>
                    <Columns>
                      <telerik:GridTemplateColumn HeaderStyle-Width="50" HeaderStyle-Wrap="true" HeaderText=""
                                                  ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="middle" UniqueName="DeleteColumn"
                                                  HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                        <HeaderTemplate>
                          <bvu:Help ID="Help116" runat="server" HelpID="13"/>
                        </HeaderTemplate>
                        <ItemTemplate>
                          <asp:ImageButton ID="btnDelete" runat="server" CommandName="Delete" ImageAlign="Middle" ImageUrl="~/App_Themes/Breg/images/Common/delete.png"/>
                        </ItemTemplate>
                      </telerik:GridTemplateColumn>
                      <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn2" HeaderStyle-BorderColor="#A2B6CB"
                                                      HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" ItemStyle-HorizontalAlign="Center"
                                                      ItemStyle-VerticalAlign="Middle">
                      </telerik:GridClientSelectColumn>
                      <telerik:GridBoundColumn DataField="ShortName" HeaderStyle-BorderColor="#A2B6CB"
                                               HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderText="Name:"
                                               UniqueName="ShortName">
                      </telerik:GridBoundColumn>
                      <telerik:GridBoundColumn DataField="Code" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                                               HeaderStyle-BorderWidth="1px" HeaderText="Code" UniqueName="Code">
                      </telerik:GridBoundColumn>
                      <telerik:GridBoundColumn DataField="LeftRightSide" HeaderStyle-BorderColor="#A2B6CB"
                                               HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderText="Side:"
                                               UniqueName="LeftRightSide">
                      </telerik:GridBoundColumn>
                      <telerik:GridBoundColumn DataField="Size" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                                               HeaderStyle-BorderWidth="1px" HeaderText="Size:" UniqueName="Size">
                      </telerik:GridBoundColumn>
                      <telerik:GridTemplateColumn DataField="HCPCSString" UniqueName="HCPCs" HeaderText="Edit HCPCs"
                                                  HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px">
                        <HeaderStyle/>
                        <ItemTemplate>
                          <asp:TextBox ID="txtHcpcs" runat="server" Text='<%# Eval("HCPCSString") %>'></asp:TextBox>
                        </ItemTemplate>
                      </telerik:GridTemplateColumn>
                      <telerik:GridTemplateColumn DataField="StockingUnits" HeaderStyle-BorderColor="#A2B6CB"
                                                  HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderStyle-Width="50"
                                                  HeaderStyle-Wrap="true" UniqueName="TemplateColumn7" ItemStyle-HorizontalAlign="Center"
                                                  ItemStyle-VerticalAlign="Middle">
                        <HeaderTemplate>
                          <asp:Label ID="Label1" runat="server">Stocking Units</asp:Label>
                          <bvu:Help ID="Help27" runat="server" HelpID="27"/>
                        </HeaderTemplate>
                        <ItemTemplate>
                          <telerik:RadNumericTextBox ID="txtStockingUnits" runat="server" Culture="English (United States)"
                                                     Font-Size="x-small" MaxValue="999" MinValue="1" NumberFormat-DecimalDigits="0"
                                                     ShowSpinButtons="False" Skin="Web20" Text='<%# Eval("StockingUnits") %>' Type="Number"
                                                     Width="50px">
                          </telerik:RadNumericTextBox>
                        </ItemTemplate>
                      </telerik:GridTemplateColumn>
                      <telerik:GridTemplateColumn DataField="WholesaleCost" HeaderStyle-Width="50" HeaderStyle-Wrap="true"
                                                  UniqueName="TemplateColumn2" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px"
                                                  HeaderStyle-BorderColor="#A2B6CB" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                        <HeaderTemplate>
                          <asp:Label ID="Label3" runat="server">Wholesale Cost</asp:Label>
                          <bvu:Help ID="Help28" runat="server" HelpID="28"/>
                        </HeaderTemplate>
                        <ItemTemplate>
                          <asp:TextBox ID="txtWholesaleCost" runat="server" Font-Size="X-Small" Text='<%# Eval("WholesaleCost") %>'
                                       Width="50px">
                          </asp:TextBox>
                        </ItemTemplate>
                      </telerik:GridTemplateColumn>
                      <telerik:GridTemplateColumn DataField="BillingCharge" HeaderStyle-Width="50" HeaderStyle-Wrap="true"
                                                  UniqueName="TemplateColumn3" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px"
                                                  HeaderStyle-BorderColor="#A2B6CB" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                        <HeaderTemplate>
                          <asp:Label ID="Label4" runat="server">Billing&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Charge</asp:Label>
                          <bvu:Help ID="Help29" runat="server" HelpID="29"/>
                        </HeaderTemplate>
                        <ItemTemplate>
                          <asp:TextBox ID="txtBillingCharge" runat="server" Font-Size="X-Small" Text='<%# Eval("BillingCharge") %>'
                                       Width="50px">
                          </asp:TextBox>
                        </ItemTemplate>
                      </telerik:GridTemplateColumn>
                      <telerik:GridTemplateColumn DataField="BillingChargeCash" HeaderStyle-Width="100"
                                                  ItemStyle-Width="100" HeaderStyle-Wrap="true" UniqueName="TemplateColumn4" HeaderStyle-BorderStyle="Solid"
                                                  HeaderStyle-BorderWidth="1px" HeaderStyle-BorderColor="#A2B6CB" ItemStyle-HorizontalAlign="Center"
                                                  ItemStyle-VerticalAlign="Middle">
                        <HeaderTemplate>
                          <asp:Label ID="Label5" runat="server">Billing&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Charge Cash</asp:Label>
                          <bvu:Help ID="Help30" runat="server" HelpID="30"/>
                        </HeaderTemplate>
                        <ItemTemplate>
                          <asp:TextBox ID="txtBillingChargeCash" runat="server" Font-Size="X-Small" Text='<%# Eval("BillingChargeCash") %>'
                                       Width="100px">
                          </asp:TextBox>
                        </ItemTemplate>
                      </telerik:GridTemplateColumn>
                      <telerik:GridTemplateColumn DataField="DMEDeposit" HeaderStyle-Width="50" HeaderStyle-Wrap="true"
                                                  UniqueName="TemplateColumn5" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px"
                                                  HeaderStyle-BorderColor="#A2B6CB" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                        <HeaderTemplate>
                          <asp:Label ID="Label6" runat="server">DME Deposit&nbsp; </asp:Label>
                          <bvu:Help ID="Help31" runat="server" HelpID="31"/>
                        </HeaderTemplate>
                        <ItemTemplate>
                          <asp:TextBox ID="txtDMEDeposit" runat="server" Font-Size="X-Small" Text='<%# Eval("DMEDeposit") %>'
                                       Width="70px">
                          </asp:TextBox>
                        </ItemTemplate>
                      </telerik:GridTemplateColumn>
                      <telerik:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" UniqueName="PracticeCatalogSupplierBrandID"
                                               Visible="false">
                      </telerik:GridBoundColumn>
                      <telerik:GridBoundColumn DataField="PracticeCatalogProductID" Display="false" UniqueName="PracticeCatalogProductID">
                      </telerik:GridBoundColumn>
                      <telerik:GridBoundColumn DataField="IsDeletable" Visible="false" UniqueName="IsDeletable">
                      </telerik:GridBoundColumn>
                      <telerik:GridBoundColumn DataField="MasterCatalogProductID" Visible="false" UniqueName="MasterCatalogProductID">
                      </telerik:GridBoundColumn>
                    </Columns>
                  </telerik:GridTableView>
                </DetailTables>
              </MasterTableView>
            </bvu:VisionRadGridAjax>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<asp:LinqDataSource AutoPage="true" ID="LinqDataSource1" runat="server" ContextTypeName="ClassLibrary.VisionDataContext" onselecting="LinqDataSource1_Selecting">
</asp:LinqDataSource>
<asp:LinqDataSource AutoPage="true" ID="LinqDataSource2" runat="server" ContextTypeName="ClassLibrary.VisionDataContext" onselecting="LinqDataSource2_Selecting">
</asp:LinqDataSource> 
