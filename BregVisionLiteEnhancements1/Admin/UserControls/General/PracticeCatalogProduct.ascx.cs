﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using BregVision.Admin.PracticeSetup;
using ClassLibrary.DAL;
using Telerik.Web.UI;

namespace BregVision.Admin.UserControls.General
{
    public partial class PracticeCatalogProduct : Bases.UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LinqDataSource1_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            if (Cache["products"] != null)
            {
                e.Result = Cache["products"];

            }
            else
            {
                VisionDataContext context = VisionDataContext.GetVisionDataContext();
                using (context)
                {
                    var query = from pcsb in context.PracticeCatalogSupplierBrands
                                where
                                    pcsb.PracticeID == practiceID
                                    && pcsb.IsActive == true
                                    && (
                                            from pcp in context.PracticeCatalogProducts
                                            where pcp.PracticeCatalogSupplierBrandID == pcsb.PracticeCatalogSupplierBrandID
                                            && pcp.IsActive == true
                                            && 
                                            pcp.IsThirdPartyProduct == false 
                                            ||
                                            ((
                                                  from tpp in context.ThirdPartyProducts
                                                  where 
                                                  pcp.ThirdPartyProductID == tpp.ThirdPartyProductID
                                                  && tpp.IsActive == true
                                                  && pcp.IsThirdPartyProduct == true
                                                  select tpp
                                               ).Count() > 0
                                            )
                                            select pcp
                                       ).Count() > 0
                                select new
                                {
                                    pcsb.PracticeCatalogSupplierBrandID,
                                    SupplierName = pcsb.SupplierShortName,
                                    BrandName = pcsb.BrandShortName,
                                    IsThirdPartySupplier = pcsb.IsThirdPartySupplier,
                                    DerivedSequence = pcsb.Sequence ?? 999
                                };


                    var orderedQuery = query.OrderBy(a => a.IsThirdPartySupplier).ThenBy(a=> a.DerivedSequence).ThenBy(a => a.SupplierName).ThenBy(a =>  a.BrandName).ToList();

                    Cache["products"] = orderedQuery;

                    e.Result = orderedQuery;
                }
            }
        }

        protected void LinqDataSource2_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {

            string key = LinqDataSource2.WhereParameters[0].DefaultValue;
            int pbspID = 0;
            int.TryParse(key, out pbspID);

            if (pbspID > 0)
            {
                VisionDataContext context = VisionDataContext.GetVisionDataContext();
                using (context)
                {
                    var query = from pcp in context.PracticeCatalogProducts
                                from mcp in context.MasterCatalogProducts.Where(a => a.MasterCatalogProductID == pcp.MasterCatalogProductID && pcp.IsThirdPartyProduct == false).DefaultIfEmpty()
                                from tpp in context.ThirdPartyProducts.Where(a => a.ThirdPartyProductID == pcp.ThirdPartyProductID && pcp.IsThirdPartyProduct == true).DefaultIfEmpty()
                                where pcp.IsActive == true
                                select new
                                {
                                    pcp.PracticeID,
                                    pcp.PracticeCatalogProductID,
                                    pcp.MasterCatalogProductID,
                                    pcp.PracticeCatalogSupplierBrandID,
                                    ShortName = mcp != null ? mcp.ShortName : (tpp != null ? tpp.ShortName : ""),
                                    Code = mcp != null ? mcp.Code : (tpp != null ? tpp.Code : ""),
                                    LeftRightSide = mcp != null ? mcp.LeftRightSide + ", " + mcp.Size : (tpp != null ? tpp.LeftRightSide + ", " + tpp.Size : ""),
                                    SideSize = mcp != null ? mcp.LeftRightSide : (tpp != null ? tpp.LeftRightSide : ""),
                                    Size = mcp != null ? mcp.Size : (tpp != null ? tpp.Size : ""),
                                    Gender = mcp != null ? mcp.Gender : (tpp != null ? tpp.Gender : ""),
                                    pcp.WholesaleCost,
                                    pcp.BillingCharge,
                                    pcp.DMEDeposit,
                                    pcp.BillingChargeCash,
                                    pcp.StockingUnits,
                                    pcp.BuyingUnits,
                                    pcp.IsThirdPartyProduct,
                                    HCPCSString = "GetMeLater"
                                };


                    var orderedQuery = query.OrderBy(b => b.ShortName).ThenBy(c => c.Code).ToList();

                    e.Result = orderedQuery;
                }
            }
        }

        protected void grdSuppliers_OnDetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
        {

            string key = e.DetailTableView.ParentItem["PracticeCatalogSupplierBrandID"].Text;
            int pcsbID = 0;
            int.TryParse(key, out pcsbID);
            if (pcsbID > 0)
            {
                LinqDataSource2.WhereParameters.Clear();
                LinqDataSource2.WhereParameters.Add("PracticeCatalogSupplierBrandID", pcsbID.ToString());
            }
        }
    }
}