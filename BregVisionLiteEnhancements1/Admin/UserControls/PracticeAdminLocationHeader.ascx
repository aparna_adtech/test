<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="PracticeAdminLocationHeader" Codebehind="PracticeAdminLocationHeader.ascx.cs" %>

<!-- PracticeAdminLocationHeader -->
<table id="tablePracticeAdminLocationHeader" style="text-align: justify; background-color: #f9f6dd" width="auto" >
    <tr>
        <td align="left" colspan="2" style="height: 18px; font-size: 11pt">
            <asp:Label ID="lblPractice" runat="server" Font-Bold="True" Font-Size="Larger" Text="Practice" />
        </td>
        <td colspan="1" align="left" >
            &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="btnGoToPracticeLocations" OnClick="btnGoToPracticeLocations_Click" 
            Text="All Practice Locations" ToolTip="All Practice Locations" runat="server" CausesValidation="false"></asp:LinkButton>
        </td>
        <td colspan="2"> &nbsp;</td>
    </tr>
    <tr><td colspan="5" style="height:1px;"><hr /></td></tr>
    
    <tr valign="middle">
        <td style="font-size: 12pt" align="left">
            <asp:Label ID="lblPracticeLocation" runat="server" Font-Bold="True" Font-Size="Larger"
                Text="Practice"></asp:Label><br />
        </td>
        <td style="font-size: 12pt" align="center">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
          <td style="font-size: 12pt" align="center">
            <asp:LinkButton ID="btnGoToPracticeLocationBillToAddress" OnClick="btnGoToPracticeLocation_ShippingAddress_Click"
                Text="Addresses" ToolTip="Practice Location's Shipping and Bill To Addresses" runat="server" CausesValidation="false"></asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
        </td>
        <td style="font-size: 12pt;" align="center">
            <asp:LinkButton ID="btnGoToPracticeLocationContact" OnClick="btnGoToPracticeLocationContact_Click"
                Text="Contact" ToolTip="Practice Location Main Contact" runat="server" CausesValidation="false">Contact</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
        </td>
        <td style="font-size: 12pt" align="center">
            <asp:LinkButton ID="btnGoToPracticeLocationPhysicians" OnClick="btnGoToPracticeLocationPhysicians_Click"
                ToolTip="Practice Location Physicians" Text="Physicians" runat="server" CausesValidation="false">Physicians</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
        </td>
    </tr>
    <tr valign="middle">
        <td colspan="2" style="font-size: 12pt" align="left">
            &nbsp;&nbsp;&nbsp;<asp:Label ID="lblLocation" runat="server" Font-Bold="True" Font-Size="Larger" Text="Location" ></asp:Label><br />
        </td>
        <td style="font-size: 12pt" align="center">
            <asp:LinkButton ID="btnGoTo_PracticeLocation_Email" Text="Email*" ToolTip="*Under Construction:  Practice Location Email Info" ForeColor="Black"
            OnClick="btnGoTo_PracticeLocation_Email_Click" runat="server" CausesValidation="false"></asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
        </td>
        <td style="font-size: 12pt" align="center">
            <asp:LinkButton ID="btnGoToPracticeLocationsSupplierCodes" OnClick="btnGoToPracticeLocationsSupplierCodes_Click" Text="Supplier Codes*" ForeColor="Black"
                ToolTip="*Under Construction:  Practice Location's Supplier Account Codes" runat="server" CausesValidation="false"></asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
        </td>
        <td style="font-size: 12pt" align="center">
            <asp:LinkButton ID="btnGoToPracticeLocationUsers" OnClick="btnGoToPracticeLocationUsers_Click" Text="Users*" ForeColor="Black"
                ToolTip="*Under Construction:  Practice Location Users" runat="server" CausesValidation="false"></asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
        </td>
    </tr>
    <tr>
        <td align="left" colspan="5" style="height: 18px">
        </td>
    </tr>
</table>
<!-- PracticeAdminLocationHeader End-->
