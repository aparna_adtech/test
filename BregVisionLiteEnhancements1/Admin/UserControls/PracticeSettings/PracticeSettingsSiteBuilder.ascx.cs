﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.Admin.PracticeSetup;
using ClassLibrary.DAL;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Text.RegularExpressions;
using Telerik.Web.UI;
using Excel;

namespace BregVision.Admin.UserControls.PracticeSettings
{
    public partial class PracticeSettingsSiteBuilder : PracticeSetupBase, IVisionWizardControl
    {

        /// <summary>
        /// IVisionWizardControl Interface.
        /// </summary>
        public bool NeedsInitialBind { get; set; }
        private bool isInEdit = false;

		private bool ProductsTriggered { get; set; }

		protected bool SpreadsheetsProcessed
		{
			get
			{
				string spreadsheetsProcessed = ParentSpreadsheetsProcessed.Value;
				bool spreadsheetsProcessedVal = false;
				if (bool.TryParse(spreadsheetsProcessed, out spreadsheetsProcessedVal))
					return spreadsheetsProcessedVal;
				return false;
			}
			set
			{
				ParentSpreadsheetsProcessed.Value = value.ToString();
			}
		}


		private DataSet uploadedConfig { get; set; }

		const string CHECKED = "checked";

		const string SUPPLIER_BREG = "BREG";

		const string PRODUCT_CODE_COLUMN = "PRODUCT #";
		const string PRODUCT_VENDOR_COLUMN = "VENDOR";
		const string PRODUCT_KEYCODE_COLUMN = "KEY CODE";
		const string PRODUCT_DEPOSIT_COLUMN = "DEPOSIT";
		const string PRODUCT_COST_COLUMN = "COST";
		const string PRODUCT_BILLINGCHARGE_COLUMN = "BILLING CHARGE";
		const string PRODUCT_CASHCHARGE_COLUMN = "CASH & CARRY";
		const string PRODUCT_NAME_COLUMN = "PRODUCT DESCRIPTION";
		const string PARS_LIST_COLUMN = "_PARS_LIST_COLUMN_";
		const int PARS_FIRST_LOCATION_COLUMN = 5;

		const string PARS_VENDOR_COLUMN = "SUPPLIER";
		const string PARS_LOCATION_COLUMN = "Practice Location";
		const string PARS_PARTNUMBER_COLUMN = "PRODUCT CODE/NUMBER";
		const string PARS_PAR_COLUMN = "ParLevel";
		const string PARS_REORDER_COLUMN = "ReorderLevel";
		const string PARS_CRITICAL_COLUMN = "CriticalLevel";
		const string PARS_LOCATIONID_COLUMN = "_PracticeLocationID";

		const string PRODUCT_ADDON = "ADD-ON";

		const string SPAN_START = "<span ";
		const string SPAN_END = "</span>";
		const string BR = "<br/>";
		const string BRBR = BR;// + BR;
		const string SPAN_END_BR = SPAN_END + BRBR;
		const string SPAN_ERROR = "  " + SPAN_START + "class='err'>";
		const string SPAN_WARN = " " + SPAN_START + "class='warn'>";
		const string SPAN_PROCESS = SPAN_START + "class='process'>";
		const string SPAN_INFO = " " + SPAN_START + "class='info'>";

		const string ERROR_CODE = SPAN_ERROR + "Invalid Code" + SPAN_END_BR;
		const string ERROR_VENDOR = SPAN_ERROR + "Invalid Vendor" + SPAN_END_BR;
		const string ERROR_NAME = SPAN_ERROR + "Invalid Name" + SPAN_END_BR;
		const string ERROR_KEYCODE = SPAN_ERROR + "Invalid Key Code" + SPAN_END_BR;
		const string ERROR_MISSING_MCP_CODE = SPAN_ERROR + "Missing Master Catalog Code" + SPAN_END_BR;
		const string WARN_NO_PAR = SPAN_WARN + "No PAR Match" + SPAN_END_BR;
		const string WARN_INVENTORY_EXISTS = SPAN_WARN + "Inventory Already Exists" + SPAN_END_BR;
		const string BAD_NUMBER = SPAN_INFO + "Bad Number" + SPAN_END_BR;
		const string CREATE_NEW_VENDOR = SPAN_PROCESS + "Create Third Party Vendor" + SPAN_END_BR;
		const string CREATE_NEW_TPP_CODE = SPAN_PROCESS + "Create Third Party Product" + SPAN_END_BR;
		const string CREATE_NEW_PCP_CODE = SPAN_PROCESS + "Create Practice Catalog Product" + SPAN_END_BR;

		const string SESSION_PRODUCTS_DATA = "PracticeConfigProductsData";
		const string SESSION_PRODUCTS_PATH = "PracticeConfigProductsPath";
		const string SESSION_PARS_DATA = "PracticeConfigParData";
		const string SESSION_PARS_PATH = "PracticeConfigParPath";

		const int MAX_FUZZY_NAME_DISTANCE = 300;

		List<object> InsertOnSubmits = new List<object>();

		protected void Page_Load(object sender, EventArgs e)
		{
			var practice = DAL.Practice.Practice.GetPracticeById(practiceID);
		}

		protected void Page_PreRender(object sender, EventArgs e)
		{
			object text = Session[SESSION_PRODUCTS_PATH];
			if (text == null)
				text = string.Empty;
			lblProductsPath.Text = text.ToString();

			text = Session[SESSION_PARS_PATH];
			if (text == null)
				text = string.Empty;
			lblParsPath.Text = text.ToString();
		}

		protected void btnUpload_Click(object sender, EventArgs e)
		{
			SpreadsheetsProcessed = false;
			Stream stream = null;
			DataTable pars = (DataTable)Session[SESSION_PARS_DATA];
			if (UploadPars.PostedFile != null &&
				UploadPars.PostedFile.InputStream != null &&
				UploadPars.PostedFile.InputStream.Length > 0)
			{
				Session[SESSION_PARS_DATA] = null;
				Session[SESSION_PARS_PATH] = UploadPars.PostedFile.FileName;

				stream = UploadPars.PostedFile.InputStream;
				IExcelDataReader excelReader = null;
				switch (Path.GetExtension(UploadPars.PostedFile.FileName))
				{
					case ".xls":
						excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
						break;

					case ".xlsx":
						excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
						break;
				}

				if (excelReader != null)
				{
					excelReader.IsFirstRowAsColumnNames = false;
					DataSet ds = excelReader.AsDataSet(true);
					foreach (DataTable dt in ds.Tables)
					{
						pars = CopyParsDataTable(dt);
						Session[SESSION_PARS_DATA] = pars;
						break;
					}
				}
			}
			if (UploadProducts.PostedFile != null &&
				UploadProducts.PostedFile.InputStream != null &&
				UploadProducts.PostedFile.InputStream.Length > 0)
			{
				Session[SESSION_PRODUCTS_DATA] = null;
				Session[SESSION_PRODUCTS_PATH] = UploadProducts.PostedFile.FileName;

				stream = UploadProducts.PostedFile.InputStream;
				IExcelDataReader excelReader = null;
				switch (Path.GetExtension(UploadProducts.PostedFile.FileName))
				{
					case ".xls":
						excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
						break;

					case ".xlsx":
						excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
						break;
				}

				if (excelReader != null)
				{
					excelReader.IsFirstRowAsColumnNames = false;
					DataSet ds = excelReader.AsDataSet(false);
					foreach (DataTable dt in ds.Tables)
					{
						if (dt.TableName.ToUpper().Trim() == "FLOW")
						{
							bool errorFound;
							DataTable newdt = CopyProductsDataTable(dt, pars, out errorFound);
							Session[SESSION_PRODUCTS_DATA] = newdt;
							if (errorFound)
							{
								btnProcess.Enabled = false;
								btnProcess.CssClass = string.Empty;
							}
							else
							{
								btnProcess.Enabled = true;
								btnProcess.CssClass = "button-primary";
							}
						}
					}

					GridSortExpression expression = new GridSortExpression();
					expression.FieldName = PRODUCT_CODE_COLUMN;
					expression.SortOrder = GridSortOrder.Ascending;
					grdProducts.MasterTableView.SortExpressions.AddSortExpression(expression);
					grdProducts.Rebind();
				}
			}
		}

		protected void btnProcess_Click(object sender, EventArgs e)
		{
			DataTable dt = (DataTable)Session[SESSION_PRODUCTS_DATA];
			if (dt != null)
			{
				using (var visionDb = VisionDataContext.GetVisionDataContext())
				{
					// Add all the new Third Party Suppliers
					Dictionary<string, PracticeCatalogSupplierBrand> newPCSBs = new Dictionary<string, PracticeCatalogSupplierBrand>();
					foreach (DataRow row in dt.Rows)
					{
						string code = row[PRODUCT_CODE_COLUMN].ToString();
						if (!code.Contains(CREATE_NEW_PCP_CODE) && !code.Contains(CREATE_NEW_TPP_CODE))
							continue;

						string vendor = row[PRODUCT_VENDOR_COLUMN].ToString();
						vendor = CleanCell(vendor);
						vendor = vendor.Length > 100 ? vendor.Substring(0, 100) : vendor;

						int pcsbid = (int)row["_PracticeCatalogSupplierBrandID"];
						bool isThirdParty = (bool)row["_IsThirdPartyProduct"];
						PracticeCatalogSupplierBrand pcsb = GetPCSBForVendor(visionDb, pcsbid, vendor, isThirdParty, newPCSBs);

						string name = CleanCell(row[PRODUCT_NAME_COLUMN].ToString());
						string packaging = row["_Packaging"].ToString();
						string side = row["_Side"].ToString();
						string size = row["_Size"].ToString();
						decimal cost = GetDecimal(row, PRODUCT_COST_COLUMN);
						decimal cashCarry = GetDecimal(row, PRODUCT_CASHCHARGE_COLUMN);
						decimal deposit = GetDecimal(row, PRODUCT_DEPOSIT_COLUMN);
						decimal billingCharge = GetDecimal(row, PRODUCT_BILLINGCHARGE_COLUMN);
						int createdUserID = (int)row["_CreatedUserID"];
						DateTime createdDate = (DateTime)row["_CreatedDate"];
						int modifiedUserID = (int)row["_ModifiedUserID"];
						DateTime modifiedDate = (DateTime)row["_ModifiedDate"];
						bool isActive = (bool)row["_IsActive"];
						bool isLogoAblePart = false;

						// If we're creating a new ThirdPartyProduct...
						ThirdPartyProduct tpp = null;
						int? mcpID = isThirdParty ? null : (int?)row["_MasterCatalogProductID"];
						int? tppID = isThirdParty ? (int?)row["_ThirdPartyProductID"] : null;
						if (code.Contains(CREATE_NEW_TPP_CODE))
						{
							tppID = null;
							mcpID = null;
							isThirdParty = true;
							isActive = true;

							tpp = new ThirdPartyProduct();
							tpp.PracticeCatalogSupplierBrand = pcsb;
							tpp.Code = CleanCell(code);
							tpp.Name = name.Length > 100 ? name.Substring(0, 100) : name;
							tpp.ShortName = name.Length > 50 ? name.Substring(0, 50) : name;
							tpp.Packaging = packaging;
							tpp.LeftRightSide = side;
							tpp.Size = size;
							tpp.Color = string.Empty;
							tpp.Gender = string.Empty;
							tpp.WholesaleListCost = cost;
							tpp.Description = string.Empty;
							tpp.IsDiscontinued = false;
							tpp.CreatedUserID = 1;
							tpp.CreatedDate = DateTime.Now;
							tpp.ModifiedUserID = 1;
							tpp.ModifiedDate = DateTime.Now;
							tpp.IsActive = true;
							tpp.IsLogoAblePart = false;
							tpp.Mod1 = string.Empty;
							tpp.Mod2 = string.Empty;
							tpp.Mod3 = string.Empty;
							visionDb.ThirdPartyProducts.InsertOnSubmit(tpp);
							InsertOnSubmits.Add(tpp);
						}

						// Write out the Practice Catalog Product and inventory information
						if ((mcpID != 0 && mcpID != null) || (tppID != 0 && tppID != null) || (tpp != null))
						{
							PracticeCatalogProduct pcp = new PracticeCatalogProduct();
							pcp.PracticeCatalogSupplierBrand = pcsb;
							pcp.PracticeID = practiceID;
							pcp.MasterCatalogProductID = mcpID;
							pcp.ThirdPartyProductID = tppID;
							pcp.ThirdPartyProduct = tpp;
							pcp.IsThirdPartyProduct = isThirdParty;
							pcp.WholesaleCost = cost;
							pcp.BillingCharge = billingCharge;
							pcp.DMEDeposit = deposit;
							pcp.BillingChargeCash = cashCarry;
							pcp.BuyingUnits = 1;
							pcp.StockingUnits = 1;
							pcp.CreatedUserID = createdUserID;
							pcp.CreatedDate = createdDate;
							pcp.ModifiedUserID = modifiedUserID;
							pcp.ModifiedDate = modifiedDate;
							pcp.IsActive = isActive;
							pcp.IsLogoAblePart = isLogoAblePart;
							pcp.ProductHCPCs = GetHCPCs(row, pcp);
							visionDb.PracticeCatalogProducts.InsertOnSubmit(pcp);
							InsertOnSubmits.Add(pcp);

							// Add all the new Inventory
							DataTable inventorys = (DataTable)row[PARS_LIST_COLUMN];
							if (inventorys != null)
							{
								foreach (DataRow inventory in inventorys.Rows)
								{
									string par = inventory[PARS_PAR_COLUMN].ToString();
									string reorder = inventory[PARS_REORDER_COLUMN].ToString();
									string critical = inventory[PARS_CRITICAL_COLUMN].ToString();
									if (par.Contains(SPAN_PROCESS))
									{
										int parValue;
										int.TryParse(CleanCell(par), out parValue);
										int reorderValue;
										int.TryParse(CleanCell(reorder), out reorderValue);
										int criticalValue;
										int.TryParse(CleanCell(critical), out criticalValue);

										ProductInventory inv = new ProductInventory();
										inv.CreatedDate = DateTime.Now;
										inv.CreatedUserID = 1;
										inv.CriticalLevel = criticalValue;
										inv.ParLevel = parValue;
										inv.ReorderLevel = reorderValue;
										inv.IsActive = true;
										inv.IsSetToActive = true;
										inv.IsLogoAblePart = pcp.IsLogoAblePart;
										inv.ModifiedDate = DateTime.Now;
										inv.ModifiedUserID = 1;
										inv.PracticeCatalogProduct = pcp;
										inv.PracticeLocationID = (int)inventory[PARS_LOCATIONID_COLUMN];
										visionDb.ProductInventories.InsertOnSubmit(inv);
										InsertOnSubmits.Add(inv);
									}
								}
							}
						}
					}

					visionDb.SubmitChanges();
				}

				Session[SESSION_PRODUCTS_DATA] = null;
				SpreadsheetsProcessed = true;
				grdProducts.DataBind();
			}
			btnProcess.Enabled = false;
			btnProcess.CssClass = string.Empty;

			//var json = Newtonsoft.Json.JsonConvert.SerializeObject(InsertOnSubmits);
			//using (StreamWriter writer = File.CreateText(@"C:\Users\SMarks\Desktop\InsertOnSubmits\InsertOnSubmits." + DateTime.Now.Ticks + ".json"))
			//{
			//	writer.Write(json);
			//	writer.Close();
			//}
		}

		private System.Data.Linq.EntitySet<ProductHCPC> GetHCPCs(DataRow row, PracticeCatalogProduct pcp)
		{
			System.Data.Linq.EntitySet<ProductHCPC> hcpcs = new System.Data.Linq.EntitySet<ProductHCPC>();

			if (row.Table.Columns.Contains(PRODUCT_KEYCODE_COLUMN))
			{
				string keyCodes = row[PRODUCT_KEYCODE_COLUMN].ToString();
				keyCodes = keyCodes.Replace(',', ' ').Replace('\r', ' ').Replace('\n', ' ').Replace('\t', ' ');
				string[] keyCodeList = keyCodes.Split(" ".ToCharArray());
				foreach (string keyCode in keyCodeList)
				{
					ProductHCPC hcpc = new ProductHCPC();
					hcpc.CreatedDate = DateTime.Now;
					hcpc.CreatedUserID = 1;
					hcpc.IsActive = true;
					hcpc.IsOffTheShelf = false;
					hcpc.ModifiedDate = DateTime.Now;
					hcpc.HCPCS = keyCode;
					hcpc.PracticeCatalogProduct = pcp;
					hcpcs.Add(hcpc);
				}
			}
			return hcpcs;
		}

		private PracticeCatalogSupplierBrand GetPCSBForVendor(VisionDataContext visionDb, int pcsbid, string vendor, bool isThirdParty,
			Dictionary<string, PracticeCatalogSupplierBrand> newPCSBs)
		{
			if (pcsbid == 0)
			{
				PracticeCatalogSupplierBrand pcsb = visionDb.PracticeCatalogSupplierBrands
					.FirstOrDefault(c => (c.SupplierName == vendor) && (c.PracticeID == practiceID) && (c.IsThirdPartySupplier == isThirdParty));
				if (pcsb != null)
					return pcsb;

				foreach (string vendorName in newPCSBs.Keys)
				{
					if (vendorName == vendor)
						return newPCSBs[vendorName];
				}

				pcsb = new PracticeCatalogSupplierBrand();
				pcsb.PracticeID = practiceID;
				pcsb.BrandName = vendor;
				pcsb.BrandShortName = vendor.Length > 50 ? vendor.Substring(0, 50) : vendor;
				pcsb.SupplierName = vendor;
				pcsb.SupplierShortName = vendor.Length > 50 ? vendor.Substring(0, 50) : vendor;
				pcsb.CreatedDate = DateTime.Now;
				pcsb.CreatedUserID = 1;
				pcsb.IsActive = true;
				pcsb.ModifiedDate = DateTime.Now;
				pcsb.ModifiedUserID = 1;

				string upperVendor = vendor.ToUpper();
				bool found = false;
				foreach (MasterCatalogSupplier supplier in visionDb.MasterCatalogSuppliers)
				{
					if (supplier.SupplierName.ToUpper() == upperVendor)
					{
						pcsb.MasterCatalogSupplier = supplier;
						pcsb.IsThirdPartySupplier = false;
						found = true;
						break;
					}
				}
				if (!found)
				{
					foreach (string supplier in newPCSBs.Keys)
					{
						if (supplier.ToUpper() == upperVendor)
						{
							pcsb.ThirdPartySupplier = newPCSBs[supplier].ThirdPartySupplier;
							pcsb.IsThirdPartySupplier = true;
							found = true;
							break;
						}
					}
				}
				if (!found)
				{
					foreach (ThirdPartySupplier supplier in visionDb.ThirdPartySuppliers)
					{
						if (supplier.SupplierName.ToUpper() == upperVendor && supplier.PracticeID == practiceID)
						{
							pcsb.ThirdPartySupplier = supplier;
							pcsb.IsThirdPartySupplier = true;
							found = true;
							break;
						}
					}
				}
				if (!found)
				{
					ThirdPartySupplier tps = new ThirdPartySupplier();
					tps.SupplierName = vendor;
					tps.SupplierShortName = vendor.Length > 50 ? vendor.Substring(0, 50) : vendor;
					tps.SupplierName = vendor;
					tps.SupplierShortName = vendor.Length > 50 ? vendor.Substring(0, 50) : vendor;
					tps.PracticeID = practiceID;
					tps.CreatedDate = DateTime.Now;
					tps.CreatedUserID = 1;
					tps.IsActive = true;
					tps.ModifiedDate = DateTime.Now;
					tps.ModifiedUserID = 1;
					visionDb.ThirdPartySuppliers.InsertOnSubmit(tps);
					InsertOnSubmits.Add(tps);

					pcsb.ThirdPartySupplier = tps;
					pcsb.IsThirdPartySupplier = true;
				}
				visionDb.PracticeCatalogSupplierBrands.InsertOnSubmit(pcsb);
				InsertOnSubmits.Add(pcsb);
				newPCSBs.Add(upperVendor, pcsb);
				return pcsb;
			}
			else
			{
				return visionDb.PracticeCatalogSupplierBrands.Single(c => c.PracticeCatalogSupplierBrandID == pcsbid);
			}
		}

		private DataTable CopyProductsDataTable(DataTable source, DataTable pars, out bool errorFound)
		{
			errorFound = false;

			DataTable products = GetAllProducts();
			DataTable practiceLocations = GetPracticeLocations();
			DataTable masterCatalogSuppliers = GetMasterCatalogSuppliers();
			DataTable thirdPartySuppliers = GetThirdPartySuppliers();
			DataTable inventory = GetProductInventory();

			DataTable newdt = new DataTable("UploadedProducts");
			newdt.TableName = source.TableName;
			foreach (DataColumn column in source.Columns)
			{
				DataColumn newcolumn = new DataColumn(column.ColumnName, typeof(string));
				newdt.Columns.Add(newcolumn);
			}

			newdt.Columns.Add(new DataColumn("_Packaging"));
			newdt.Columns.Add(new DataColumn("_Side"));
			newdt.Columns.Add(new DataColumn("_Size"));
			newdt.Columns.Add(new DataColumn("_WholesaleListCost", typeof(decimal)));
			newdt.Columns.Add(new DataColumn("_IsPracticeProduct", typeof(bool)));
			newdt.Columns.Add(new DataColumn("_IsThirdPartyProduct", typeof(bool)));
			newdt.Columns.Add(new DataColumn("_MasterCatalogProductID", typeof(int)));
			newdt.Columns.Add(new DataColumn("_ThirdPartyProductID", typeof(int)));
			newdt.Columns.Add(new DataColumn("_ThirdPartySupplierID", typeof(int)));
			newdt.Columns.Add(new DataColumn("_PracticeCatalogSupplierBrandID", typeof(int)));
			newdt.Columns.Add(new DataColumn("_CreatedUserID", typeof(int)));
			newdt.Columns.Add(new DataColumn("_CreatedDate", typeof(DateTime)));
			newdt.Columns.Add(new DataColumn("_ModifiedUserID", typeof(int)));
			newdt.Columns.Add(new DataColumn("_ModifiedDate", typeof(DateTime)));
			newdt.Columns.Add(new DataColumn("_IsActive", typeof(bool)));
			newdt.Columns.Add(new DataColumn("_IsLogoAblePart", typeof(bool)));

			bool firstRowFound = false;
			int vendorColumnIndex = -1;
			int codeColumnIndex = -1;
			int nameColumnIndex = -1;
			int keyColumnIndex = -1;
			foreach (DataRow row in source.Rows)
			{
				if (row[0] == DBNull.Value || (source.Columns.Count > 1 && row[1] == DBNull.Value))
					continue;

				if (firstRowFound)
				{
					if (codeColumnIndex >= 0 && vendorColumnIndex >= 0 && nameColumnIndex >= 0 && keyColumnIndex >= 0)
					{
						string sourceVendor = row[vendorColumnIndex].ToString().ToUpper().Trim();
						string sourceCode = row[codeColumnIndex].ToString().ToUpper().Trim();
						string sourceName = row[nameColumnIndex].ToString().ToUpper().Trim();
						string sourceKeyCode = row[keyColumnIndex].ToString().ToUpper().Trim();
						List<Tuple<string, object>> additionalParams = new List<Tuple<string, object>>();
						bool invalidFound = false;

						if (sourceCode == PRODUCT_ADDON)
							continue;

						if (sourceCode == string.Empty || sourceCode == "#N/A" || sourceCode == "#REF!")
						{
							additionalParams.Add(new Tuple<string, object>(PRODUCT_CODE_COLUMN, ERROR_CODE + sourceCode));
							errorFound = true;
							invalidFound = true;
						}
						if (sourceVendor == string.Empty || sourceVendor == "#N/A" || sourceVendor == "#REF!")
						{
							additionalParams.Add(new Tuple<string, object>(PRODUCT_VENDOR_COLUMN, ERROR_VENDOR + sourceVendor));
							errorFound = true;
							invalidFound = true;
						}
						if (sourceName == string.Empty || sourceName == "#N/A" || sourceName == "#REF!")
						{
							additionalParams.Add(new Tuple<string, object>(PRODUCT_NAME_COLUMN, ERROR_NAME + sourceName));
							errorFound = true;
							invalidFound = true;
						}
						if (sourceKeyCode == string.Empty || sourceKeyCode == "#N/A" || sourceKeyCode == "#REF!")
						{
							additionalParams.Add(new Tuple<string, object>(PRODUCT_KEYCODE_COLUMN, ERROR_KEYCODE + sourceKeyCode));
							errorFound = true;
							invalidFound = true;
						}
						if (invalidFound)
						{
							AddFormattedRow(newdt, row, null, additionalParams.ToArray());
							continue;
						}

						bool isThirdParty;
						if (!FindVendor(masterCatalogSuppliers, thirdPartySuppliers, sourceVendor, out isThirdParty))
							additionalParams.Add(new Tuple<string, object>(PRODUCT_VENDOR_COLUMN, CREATE_NEW_VENDOR + sourceVendor));

						bool isPracticeProduct;
						Dictionary<string, Tuple<string, DataRow>> expandedProducts = ExpandCodes(products, sourceVendor, sourceName, sourceCode, isThirdParty, out isPracticeProduct);
						foreach (string expandedCode in expandedProducts.Keys)
						{
							List<Tuple<string, object>> expandedParams = new List<Tuple<string, object>>(additionalParams);

							bool foundCode;
							bool foundInventory;
							DataTable codePars = GetParsForProduct(sourceVendor, expandedCode, practiceLocations, inventory, pars, isThirdParty, out foundCode, out foundInventory);
							expandedParams.Add(new Tuple<string, object>(PARS_LIST_COLUMN, codePars));

							string codeColumn = expandedCode;
							Tuple<string, DataRow> expandedProduct = expandedProducts[expandedCode];

							string name = expandedProduct.Item1;
							DataRow product = expandedProduct.Item2;
							if (product != null)
							{
								expandedParams.Add(new Tuple<string, object>(PRODUCT_NAME_COLUMN, name));
								if (foundInventory)
									codeColumn = WARN_INVENTORY_EXISTS + expandedCode;
								else if (foundCode && !isThirdParty && !isPracticeProduct)
									codeColumn = CREATE_NEW_PCP_CODE + expandedCode;
								else
								{
									if (!isThirdParty && !isPracticeProduct)
										codeColumn = CREATE_NEW_PCP_CODE + WARN_NO_PAR + expandedCode;
									else
										codeColumn = WARN_NO_PAR + expandedCode;
								}
							}
							else
							{
								if (isThirdParty)
									codeColumn = CREATE_NEW_TPP_CODE + expandedCode;
								else
								{
									codeColumn = ERROR_MISSING_MCP_CODE + expandedCode;
									errorFound = true;
								}
							}
							expandedParams.Add(new Tuple<string, object>(PRODUCT_CODE_COLUMN, codeColumn));
							AddFormattedRow(newdt, row, product, expandedParams.ToArray());
						}
					}
					else
					{
						AddFormattedRow(newdt, row);
					}
				}
				else
				{
					for (int i = 0; i < row.Table.Columns.Count; i++)
					{
						string columnName = row[i] == null ? string.Empty : row[i].ToString().Trim();
						if (columnName != string.Empty)
						{
							bool found = false;
							foreach (DataColumn column in newdt.Columns)
							{
								if (column.ColumnName == columnName)
								{
									found = true;
									break;
								}
							}
							if (!found)
							{
								newdt.Columns[i].ColumnName = columnName;
								if (columnName.ToUpper() == PRODUCT_VENDOR_COLUMN)
									vendorColumnIndex = i;
								else if (columnName.ToUpper() == PRODUCT_CODE_COLUMN)
									codeColumnIndex = i;
								else if (columnName.ToUpper() == PRODUCT_NAME_COLUMN)
									nameColumnIndex = i;
								else if (columnName.ToUpper() == PRODUCT_KEYCODE_COLUMN)
									keyColumnIndex = i;
							}
						}
					}
					newdt.Columns.Add(new DataColumn(PARS_LIST_COLUMN, typeof(DataTable)));
					firstRowFound = true;
				}
			}
			return newdt;
		}

		private bool FindVendor(DataTable masterCatalogSuppliers, DataTable thirdPartySuppliers, string vendor, out bool isThirdParty)
		{
			isThirdParty = true;
			vendor = vendor.ToUpper();
			foreach (DataRow row in masterCatalogSuppliers.Rows)
			{
				if (row["Brand"].ToString().ToUpper() == vendor)
				{
					isThirdParty = false;
					return true;
				}
			}
			foreach (DataRow row in thirdPartySuppliers.Rows)
			{
				if (row["SupplierName"].ToString().ToUpper() == vendor)
				{
					isThirdParty = true;
					return true;
				}
			}
			return false;
		}

		private DataTable GetParsForProduct(string vendor, string code, DataTable practiceLocations, DataTable inventory, DataTable pars,
			bool isThirdPartyProduct, out bool foundCode, out bool foundInventory)
		{
			foundCode = false;
			foundInventory = false;

			DataTable dt = new DataTable("ProductPars");
			dt.Columns.Add(new DataColumn(PARS_LOCATION_COLUMN));
			dt.Columns.Add(new DataColumn(PARS_PAR_COLUMN));
			dt.Columns.Add(new DataColumn(PARS_REORDER_COLUMN));
			dt.Columns.Add(new DataColumn(PARS_CRITICAL_COLUMN));
			dt.Columns.Add(new DataColumn(PARS_LOCATIONID_COLUMN, typeof(int)));

			foreach (DataRow row in inventory.Rows)
			{
				if (row["SupplierName"].ToString() == vendor && row["Code"].ToString() == code && (bool)row["IsThirdPartyProduct"] == isThirdPartyProduct)
				{
					foundInventory = true;
					return dt;
				}
			}

			DataRow productParRow = null;
			if (pars != null)
			{
				string partNumberColumnName = null;
				foreach (DataColumn column in pars.Columns)
				{
					if (column.ColumnName.ToUpper().Contains(PARS_PARTNUMBER_COLUMN))
					{
						partNumberColumnName = column.ColumnName;
						break;
					}
				}
				if (partNumberColumnName != null)
				{
					foreach (DataRow parRow in pars.Rows)
					{
						if ((parRow[partNumberColumnName].ToString() == code) && parRow[PARS_VENDOR_COLUMN].ToString() == vendor)
						{
							productParRow = parRow;
							foundCode = true;
							break;
						}
					}
				}
			}

			foreach (DataRow location in practiceLocations.Rows)
			{
				DataRow newRow = dt.NewRow();
				string locationName = location["Name"].ToString().ToUpper();
				newRow[PARS_LOCATION_COLUMN] = locationName;
				newRow[PARS_PAR_COLUMN] = SPAN_PROCESS + "0" + SPAN_END;
				newRow[PARS_REORDER_COLUMN] = SPAN_PROCESS + "0" + SPAN_END;
				newRow[PARS_CRITICAL_COLUMN] = SPAN_PROCESS + "0" + SPAN_END;
				string locID = location["PracticeLocationID"].ToString();
				int locIDVal = 0;
				int.TryParse(locID, out locIDVal);
				newRow[PARS_LOCATIONID_COLUMN] = locIDVal;

				bool found = false;
				if (productParRow != null)
				{
					for (int i = PARS_FIRST_LOCATION_COLUMN; i < pars.Columns.Count; i++)
					{
						DataColumn column = pars.Columns[i];
						string parLocation = CleanCell(column.ColumnName);
						if (locationName == parLocation)
						{
							int parValue = 0;
							int.TryParse(productParRow[column].ToString(), out parValue);
							newRow[PARS_PAR_COLUMN] = SPAN_PROCESS + parValue.ToString() + SPAN_END;
							newRow[PARS_REORDER_COLUMN] = SPAN_PROCESS + (Math.Ceiling(parValue / 2f)).ToString() + SPAN_END;
							newRow[PARS_CRITICAL_COLUMN] = SPAN_PROCESS + (Math.Ceiling(parValue / 4f)).ToString() + SPAN_END;
							found = true;
							break;
						}
					}
				}
				if (!found)
				{
					locationName = SPAN_WARN + locationName + SPAN_END;
					newRow[PARS_LOCATION_COLUMN] = locationName;
				}
				dt.Rows.Add(newRow);
			}
			return dt;
		}

		private DataTable CopyParsDataTable(DataTable source)
		{
			DataTable newdt = new DataTable("ParsCopy");
			newdt.TableName = source.TableName;
			foreach (DataColumn column in source.Columns)
			{
				DataColumn newcolumn = new DataColumn(column.ColumnName);
				newdt.Columns.Add(newcolumn);
			}
			bool firstRowFound = false;
			foreach (DataRow row in source.Rows)
			{
				if (row[0] == DBNull.Value || (source.Columns.Count > 1 && row[1] == DBNull.Value))
					continue;

				if (firstRowFound)
				{
					AddFormattedRow(newdt, row);
				}
				else
				{
					for (int i = 0; i < newdt.Columns.Count; i++)
					{
						if (i >= row.Table.Columns.Count)
							break;
						string columnName = row[i].ToString().ToUpper().Trim();
						if (columnName != string.Empty)
						{
							bool found = false;
							foreach (DataColumn column in newdt.Columns)
							{
								if (column.ColumnName == columnName)
								{
									found = true;
									break;
								}
							}
							if (!found)
							{
								newdt.Columns[i].ColumnName = columnName;
							}
						}
					}
					firstRowFound = true;
				}
			}
			return newdt;
		}

		private Dictionary<string, Tuple<string, DataRow>> ExpandCodes(DataTable products, string sourceVendor, string sourceName, string sourceCode, bool isThirdParty, out bool isPracticeProduct)
		{
			isPracticeProduct = false;

			Dictionary<string, Tuple<string, DataRow>> expandedCodes = new Dictionary<string, Tuple<string, DataRow>>();

			sourceCode = sourceCode.Replace(',', ' ').Replace('\r', ' ').Replace('\n', ' ').Replace('\t', ' ');
			string[] codes = sourceCode.Split(" ".ToCharArray());
			string upperVendor = sourceVendor.Trim();
			string upperName = sourceName.Trim();
			foreach (string code in codes)
			{
				string upperCode = code.ToUpper();
				bool productMatched = ExpandCode(products, upperVendor, upperName, upperCode, expandedCodes, isThirdParty, out isPracticeProduct);
				if (!productMatched)
				{
					if (upperCode.Length > 2)
					{
						string unprefixedCode = upperCode;
						switch (unprefixedCode.Substring(0, 2))
						{
							case "L-":
							case "R-":
							case "M-":
							case "W-":
							case "LL":
							case "LR":
							case "ML":
							case "MR":
								unprefixedCode = unprefixedCode.Substring(2);
								break;
						}
						productMatched = ExpandCode(products, upperVendor, upperName, unprefixedCode, expandedCodes, isThirdParty, out isPracticeProduct);
					}
				}
				if (!productMatched)
				{
					if (upperCode.Length > 1)
					{
						string unprefixedCode = upperCode;
						switch (unprefixedCode.Substring(0, 1))
						{
							case "L":
							case "R":
							case "M":
							case "W":
								unprefixedCode = unprefixedCode.Substring(1);
								break;
						}
						productMatched = ExpandCode(products, upperVendor, upperName, unprefixedCode, expandedCodes, isThirdParty, out isPracticeProduct);
					}
				}
				if (!productMatched && !expandedCodes.ContainsKey(code))
				{
					expandedCodes.Add(code, new Tuple<string, DataRow>(sourceName, null));
				}
			}

			return expandedCodes;
		}

		private bool ExpandCode(DataTable products, string vendor, string name, string code,
			Dictionary<string, Tuple<string, DataRow>> expandedCodes, bool isThirdParty, out bool isPracticeProduct)
		{
			isPracticeProduct = false;
			bool productMatched = false;
			Regex regex = new Regex("^" + code.Replace("X", ".") + "$");
			foreach (DataRow product in products.Rows)
			{
				if ((bool)product["_IsThirdPartyProduct"] == isThirdParty)
				{
					string productVendor = product["SupplierName"].ToString().ToUpper();
					if (vendor == productVendor)
					{
						string productCode = product["Code"].ToString().ToUpper();
						if (regex.IsMatch(productCode))
						{
							productMatched = true;
							isPracticeProduct = (bool)product["_IsPracticeProduct"];
							if (!expandedCodes.ContainsKey(productCode))
							{
								string productName = product["Name"].ToString().ToUpper();
								if (code == productCode)
								{
									expandedCodes.Add(productCode, new Tuple<string, DataRow>(productName, product));
								}
								else
								{
									int distance = WordDistance(name, productName);
									if (distance <= MAX_FUZZY_NAME_DISTANCE)
										expandedCodes.Add(productCode, new Tuple<string, DataRow>(productName, product));
									else
									{
										int i = 0;
									}
								}
							}
						}
					}
				}
			}
			return productMatched;
		}

		private void AddFormattedRow(DataTable dt, DataRow row, DataRow product = null, Tuple<string, object>[] additionalItems = null)
		{
			DataRow newrow = dt.NewRow();
			for (int i = 0; i < dt.Columns.Count; i++)
			{
				string col = dt.Columns[i].ColumnName.ToUpper();
				object val = null;
				if (i < row.Table.Columns.Count)
					val = row[i];
				if (additionalItems != null)
				{
					for (int j = 0; j < additionalItems.Length; j++)
					{
						string addColumn = additionalItems[j].Item1;
						object addValue = additionalItems[j].Item2;
						if (col == addColumn)
						{
							if ((col == PRODUCT_NAME_COLUMN) && (val.ToString().ToUpper().Trim() != addValue.ToString().ToUpper()))
								val = addValue.ToString() + BRBR + SPAN_WARN + val + SPAN_END;
							else
								val = addValue;
						}
					}
				}
				if (col == PARS_LIST_COLUMN)
					newrow[i] = val;
				else
					FormatNumbers(newrow, i, val);
			}

			if (product != null)
			{
				newrow["_Packaging"] = product["_Packaging"];
				newrow["_Side"] = product["_Side"];
				newrow["_Size"] = product["_Size"];
				newrow["_WholesaleListCost"] = product["_WholesaleListCost"];
				newrow["_IsPracticeProduct"] = product["_IsPracticeProduct"];
				newrow["_IsThirdPartyProduct"] = product["_IsThirdPartyProduct"];
				int mcpid = (int)product["_MasterCatalogProductID"];
				newrow["_MasterCatalogProductID"] = product["_MasterCatalogProductID"];
				newrow["_PracticeCatalogSupplierBrandID"] = product["_PracticeCatalogSupplierBrandID"];
				newrow["_CreatedUserID"] = product["_CreatedUserID"];
				newrow["_CreatedDate"] = product["_CreatedDate"];
				newrow["_ModifiedUserID"] = product["_ModifiedUserID"];
				newrow["_ModifiedDate"] = product["_ModifiedDate"];
				newrow["_IsActive"] = product["_IsActive"];
				newrow["_IsLogoAblePart"] = product["_IsLogoAblePart"];
			}

			dt.Rows.Add(newrow);
		}

		private void FormatNumbers(DataRow row, int columnIndex, object cell)
		{
			DataColumn column = row.Table.Columns[columnIndex];
			string columnName = column.ColumnName;
			if (column.DataType == typeof(string))
			{
				string cellValue = (cell ?? string.Empty).ToString();
				columnName = columnName.ToUpper();
				if (
					(columnName == PRODUCT_DEPOSIT_COLUMN) ||
					(columnName == PRODUCT_COST_COLUMN) ||
					columnName.Contains(PRODUCT_BILLINGCHARGE_COLUMN) ||
					columnName.Contains(PRODUCT_CASHCHARGE_COLUMN)
				)
				{
					decimal currency = 0;
					if (decimal.TryParse(cellValue, out currency))
					{
						cellValue = string.Format("${0,8:#,##0.00}", currency);
					}
					else
					{
						cellValue = BAD_NUMBER + cellValue;
					}
				}
				row[columnIndex] = cellValue;
				return;
			}
			if (cell != null)
			{
				row[columnIndex] = cell;
				return;
			}

			if (column.DataType == typeof(decimal) || column.DataType == typeof(int))
			{
				row[columnIndex] = 0;
			}
			else if (column.DataType == typeof(bool))
			{
				row[columnIndex] = false;
			}
			else if (column.DataType == typeof(DateTime))
			{
				row[columnIndex] = DateTime.Now;
			}
			else
			{
				int i = 1;
			}
		}

		private decimal GetDecimal(DataRow row, string columnName)
		{
			decimal val = 0;
			if (row.Table.Columns.Contains(columnName))
			{
				string str = row[columnName].ToString().Replace("$", string.Empty).Trim();
				decimal.TryParse(str, out val);
			}
			return val;
		}

		protected void grdProducts_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
		{
			if (!ProductsTriggered)
			{
				RadGrid grid = (RadGrid)source;
				grid.DataSource = Session[SESSION_PRODUCTS_DATA];
			}
		}

		protected void grdPars_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
		{
			RadGrid grid = (RadGrid)source;
			GridNestedViewItem item = (GridNestedViewItem)grid.NamingContainer;
			GridDataItem data = (GridDataItem)item.ParentItem;
			object dt = data.GetDataKeyValue(PARS_LIST_COLUMN);
			if (dt is DataTable)
				grid.DataSource = (DataTable)dt;
		}

		protected void grdProducts_PreRender(object sender, EventArgs e)
		{
			RadGrid grid = (RadGrid)sender;
			foreach (GridColumn column in grid.MasterTableView.AutoGeneratedColumns)
			{
				column.Visible = false;
				string columnName = column.UniqueName.ToUpper();
				switch (columnName)
				{
					case PRODUCT_NAME_COLUMN:
					case PRODUCT_KEYCODE_COLUMN:
					case PRODUCT_VENDOR_COLUMN:
						column.Visible = true;
						break;
				}

				if (
					(columnName == PRODUCT_DEPOSIT_COLUMN) ||
					(columnName == PRODUCT_COST_COLUMN) ||
					columnName.Contains(PRODUCT_BILLINGCHARGE_COLUMN) ||
					columnName.Contains(PRODUCT_CASHCHARGE_COLUMN)
				)
				{
					column.Visible = true;
					column.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
					if (column is GridBoundColumn)
					{
						((GridBoundColumn)column).HtmlEncode = false;
					}
				}
				if (
					(columnName == PRODUCT_CODE_COLUMN)
				)
				{
					column.Visible = true;
					column.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
				}
				if (column is GridBoundColumn)
					((GridBoundColumn)column).HtmlEncode = false;
			}
			grid.Rebind();
		}

		protected void grdPars_PreRender(object sender, EventArgs e)
		{
			RadGrid grid = (RadGrid)sender;
			foreach (GridColumn column in grid.MasterTableView.AutoGeneratedColumns)
			{
				if (column.UniqueName == PARS_LOCATIONID_COLUMN)
					column.Visible = false;
				else
					column.Visible = true;
				column.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
				if (column is GridBoundColumn)
					((GridBoundColumn)column).HtmlEncode = false;
			}
			grid.Rebind();
		}
		protected void grdProducts_ItemCommand(object source, GridCommandEventArgs e)
		{
			if (e.CommandName == RadGrid.ExpandCollapseCommandName && e.Item is GridDataItem)
			{
				ProductsTriggered = true;
				if (!e.Item.Expanded)
				{
					RadGrid grid = (RadGrid)((GridDataItem)e.Item).ChildItem.FindControl("grdPars");
					grid.Rebind();
				}
			}
		}

		private DataTable GetAllProducts()
		{
			DataTable dt = new DataTable("AllProducts");
			dt.Columns.Add(new DataColumn("Code"));
			dt.Columns.Add(new DataColumn("Name"));
			dt.Columns.Add(new DataColumn("SupplierName"));
			dt.Columns.Add(new DataColumn("_Packaging"));
			dt.Columns.Add(new DataColumn("_Side"));
			dt.Columns.Add(new DataColumn("_Size"));
			dt.Columns.Add(new DataColumn("_WholesaleListCost", typeof(decimal)));
			dt.Columns.Add(new DataColumn("_IsPracticeProduct", typeof(bool)));
			dt.Columns.Add(new DataColumn("_IsThirdPartyProduct", typeof(bool)));
			dt.Columns.Add(new DataColumn("_MasterCatalogProductID", typeof(int)));
			dt.Columns.Add(new DataColumn("_ThirdPartyProductID", typeof(int)));
			dt.Columns.Add(new DataColumn("_ThirdPartySupplierID", typeof(int)));
			dt.Columns.Add(new DataColumn("_PracticeCatalogSupplierBrandID", typeof(int)));
			dt.Columns.Add(new DataColumn("_CreatedUserID", typeof(int)));
			dt.Columns.Add(new DataColumn("_CreatedDate", typeof(DateTime)));
			dt.Columns.Add(new DataColumn("_ModifiedUserID", typeof(int)));
			dt.Columns.Add(new DataColumn("_ModifiedDate", typeof(DateTime)));
			dt.Columns.Add(new DataColumn("_IsActive", typeof(bool)));
			dt.Columns.Add(new DataColumn("_IsLogoAblePart", typeof(bool)));

			using (var visionDb = VisionDataContext.GetVisionDataContext())
			{
				var pcpQuery =
					from pcp in visionDb.PracticeCatalogProducts
					join mcp in visionDb.MasterCatalogProducts on pcp.MasterCatalogProductID equals mcp.MasterCatalogProductID
					join pcsb in visionDb.PracticeCatalogSupplierBrands on pcp.PracticeCatalogSupplierBrandID equals pcsb.PracticeCatalogSupplierBrandID
					join mcs in visionDb.MasterCatalogSuppliers on pcsb.MasterCatalogSupplierID equals mcs.MasterCatalogSupplierID into gmcs
					from smcs in gmcs.DefaultIfEmpty()
					where pcsb.PracticeID == practiceID && !pcsb.IsThirdPartySupplier && pcp.IsActive.HasValue && pcp.IsActive.Value
					select new
					{
						mcp.MasterCatalogProductID,
						mcp.Code,
						mcp.Name,
						mcp.Packaging,
						Side = mcp.LeftRightSide,
						mcp.Size,
						mcp.WholesaleListCost,
						pcsb.PracticeCatalogSupplierBrandID,
						smcs.SupplierName,
						pcp.CreatedUserID,
						pcp.CreatedDate,
						pcp.ModifiedUserID,
						pcp.ModifiedDate,
						pcp.IsActive,
						pcp.IsLogoAblePart
					};
				foreach (var rec in pcpQuery)
				{
					DataRow row = dt.NewRow();
					row["Code"] = rec.Code;
					row["Name"] = rec.Name;
					row["SupplierName"] = rec.SupplierName ?? SUPPLIER_BREG;
					row["_Packaging"] = rec.Packaging;
					row["_Side"] = rec.Side;
					row["_Size"] = rec.Size;
					row["_WholesaleListCost"] = rec.WholesaleListCost ?? 0;
					row["_IsPracticeProduct"] = true;
					row["_IsThirdPartyProduct"] = false;
					row["_MasterCatalogProductID"] = rec.MasterCatalogProductID;
					row["_PracticeCatalogSupplierBrandID"] = rec.PracticeCatalogSupplierBrandID;
					row["_ThirdPartyProductID"] = 0;
					row["_ThirdPartySupplierID"] = 0;
					row["_CreatedUserID"] = rec.CreatedUserID;
					row["_CreatedDate"] = rec.CreatedDate;
					row["_ModifiedUserID"] = rec.ModifiedUserID ?? 1;
					row["_ModifiedDate"] = rec.ModifiedDate ?? DateTime.Now;
					row["_IsActive"] = rec.IsActive;
					row["_IsLogoAblePart"] = rec.IsLogoAblePart;

					dt.Rows.Add(row);
				}

				var mcpQuery =
					from mcp in visionDb.MasterCatalogProducts
					join mcs in visionDb.MasterCatalogSuppliers on mcp.MasterCatalogSupplierID equals mcs.MasterCatalogSupplierID into gmcs
					from smcs in gmcs.DefaultIfEmpty()
					join pcsb in visionDb.PracticeCatalogSupplierBrands on
						new { a = mcp.MasterCatalogSupplierID, b = practiceID } equals new { a = pcsb.MasterCatalogSupplierID, b = pcsb.PracticeID } into gpcsb
					from spcsb in gpcsb.DefaultIfEmpty()
					select new
					{
						mcp.MasterCatalogProductID,
						mcp.Code,
						mcp.Name,
						mcp.Packaging,
						Side = mcp.LeftRightSide,
						mcp.Size,
						mcp.WholesaleListCost,
						mcp.CreatedUserID,
						mcp.CreatedDate,
						mcp.ModifiedUserID,
						mcp.ModifiedDate,
						mcp.IsActive,
						mcp.IsLogoAblePart,
						smcs,
						spcsb
					};
				foreach (var rec in mcpQuery)
				{
					DataRow row = dt.NewRow();
					row["Code"] = rec.Code;
					row["Name"] = rec.Name;
					row["SupplierName"] = rec.smcs != null ? rec.smcs.SupplierName : SUPPLIER_BREG;
					row["_Packaging"] = rec.Packaging;
					row["_Side"] = rec.Side;
					row["_Size"] = rec.Size;
					row["_WholesaleListCost"] = rec.WholesaleListCost ?? 0;
					row["_IsPracticeProduct"] = false;
					row["_IsThirdPartyProduct"] = false;
					row["_MasterCatalogProductID"] = rec.MasterCatalogProductID;
					row["_PracticeCatalogSupplierBrandID"] = rec.spcsb != null ? rec.spcsb.PracticeCatalogSupplierBrandID : 0;
					row["_ThirdPartyProductID"] = 0;
					row["_ThirdPartySupplierID"] = 0;
					row["_CreatedUserID"] = rec.CreatedUserID;
					row["_CreatedDate"] = rec.CreatedDate;
					row["_ModifiedUserID"] = rec.ModifiedUserID ?? 1;
					row["_ModifiedDate"] = rec.ModifiedDate ?? DateTime.Now;
					row["_IsActive"] = rec.IsActive;
					row["_IsLogoAblePart"] = rec.IsLogoAblePart;

					dt.Rows.Add(row);
				}

				var tppQuery =
					from tpp in visionDb.ThirdPartyProducts
					join pcsb in visionDb.PracticeCatalogSupplierBrands on tpp.PracticeCatalogSupplierBrandID equals pcsb.PracticeCatalogSupplierBrandID
					join tps in visionDb.ThirdPartySuppliers on pcsb.ThirdPartySupplierID equals tps.ThirdPartySupplierID
					where pcsb.PracticeID == practiceID && pcsb.IsThirdPartySupplier
					select new
					{
						tpp.ThirdPartyProductID,
						tpp.Code,
						tpp.Name,
						tpp.Packaging,
						Side = tpp.LeftRightSide,
						tpp.Size,
						tpp.WholesaleListCost,
						PracticeCatalogSupplierBrandID = pcsb.PracticeCatalogSupplierBrandID,
						tps.ThirdPartySupplierID,
						tps.SupplierName,
						tpp.CreatedUserID,
						tpp.CreatedDate,
						tpp.ModifiedUserID,
						tpp.ModifiedDate,
						tpp.IsActive,
						tpp.IsLogoAblePart
					};
				foreach (var rec in tppQuery)
				{
					DataRow row = dt.NewRow();
					row["Code"] = rec.Code;
					row["Name"] = rec.Name;
					row["SupplierName"] = rec.SupplierName;
					row["_Packaging"] = rec.Packaging;
					row["_Side"] = rec.Side;
					row["_Size"] = rec.Size;
					row["_WholesaleListCost"] = rec.WholesaleListCost ?? 0;
					row["_IsPracticeProduct"] = true;
					row["_IsThirdPartyProduct"] = true;
					row["_MasterCatalogProductID"] = 0;
					row["_PracticeCatalogSupplierBrandID"] = rec.PracticeCatalogSupplierBrandID;
					row["_ThirdPartyProductID"] = rec.ThirdPartyProductID;
					row["_ThirdPartySupplierID"] = rec.ThirdPartySupplierID;
					row["_CreatedUserID"] = rec.CreatedUserID;
					row["_CreatedDate"] = rec.CreatedDate;
					row["_ModifiedUserID"] = rec.ModifiedUserID ?? 1;
					row["_ModifiedDate"] = rec.ModifiedDate ?? DateTime.Now;
					row["_IsActive"] = rec.IsActive;
					row["_IsLogoAblePart"] = rec.IsLogoAblePart;

					dt.Rows.Add(row);
				}
			}
			return dt;
		}

		private DataTable GetPracticeLocations()
		{
			Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
			DbCommand dbCommand = db.GetStoredProcCommand("usp_PracticeLocation_Select_All_By_PracticeID");
			db.AddInParameter(dbCommand, "PracticeID", DbType.String, practiceID);
			DataSet codes = db.ExecuteDataSet(dbCommand);

			if (codes.Tables.Count > 0)
				return codes.Tables[0];
			return null;
		}

		private DataTable GetMasterCatalogSuppliers()
		{
			Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
			DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetMasterCatalogSuppliers");
			DataSet codes = db.ExecuteDataSet(dbCommand);

			if (codes.Tables.Count > 0)
				return codes.Tables[0];

			return null;
		}

		private DataTable GetThirdPartySuppliers()
		{
			Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
			DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_ThirdPartySupplier_Select_All_By_PracticeID");
			db.AddInParameter(dbCommand, "PracticeID", DbType.String, practiceID);
			DataSet codes = db.ExecuteDataSet(dbCommand);

			if (codes.Tables.Count > 0)
				return codes.Tables[0];

			return null;
		}


		private DataTable GetProductInventory()
		{
			DataTable dt = new DataTable("ProductInventory");
			dt.Columns.Add(new DataColumn("Code"));
			dt.Columns.Add(new DataColumn("SupplierName"));
			dt.Columns.Add(new DataColumn("ParLevel"));
			dt.Columns.Add(new DataColumn("ReorderLevel"));
			dt.Columns.Add(new DataColumn("CriticalLevel"));
			dt.Columns.Add(new DataColumn("IsThirdPartyProduct", typeof(bool)));
			using (var visionDb = VisionDataContext.GetVisionDataContext())
			{
				var prodQuery =
					from pi in visionDb.ProductInventories
					join pcp in visionDb.PracticeCatalogProducts on pi.PracticeCatalogProductID equals pcp.PracticeCatalogProductID
					join pcsb in visionDb.PracticeCatalogSupplierBrands on pcp.PracticeCatalogSupplierBrandID equals pcsb.PracticeCatalogSupplierBrandID
					join mcp in visionDb.MasterCatalogProducts on pcp.MasterCatalogProductID equals mcp.MasterCatalogProductID into gmcp
					from smcp in gmcp.DefaultIfEmpty()
					join mcs in visionDb.MasterCatalogSuppliers on pcsb.MasterCatalogSupplierID equals mcs.MasterCatalogSupplierID into gmcs
					from smcs in gmcs.DefaultIfEmpty()
					join tpp in visionDb.ThirdPartyProducts on pcp.ThirdPartyProductID equals tpp.ThirdPartyProductID into gtpp
					from stpp in gtpp.DefaultIfEmpty()
					join tps in visionDb.ThirdPartySuppliers on pcsb.ThirdPartySupplierID equals tps.ThirdPartySupplierID into gtps
					from stps in gtps.DefaultIfEmpty()
					where pcp.PracticeID == practiceID
					select new
					{
						pi,
						pcp,
						smcp,
						stpp,
						smcs,
						stps
					};
				foreach (var rec in prodQuery)
				{
					DataRow row = dt.NewRow();
					row["IsThirdPartyProduct"] = rec.pcp.IsThirdPartyProduct;
					row["ParLevel"] = rec.pi.ParLevel;
					row["ReorderLevel"] = rec.pi.ReorderLevel;
					row["CriticalLevel"] = rec.pi.CriticalLevel;
					if (rec.pcp.IsThirdPartyProduct)
					{
						row["Code"] = rec.stpp.Code.ToUpper();
						row["SupplierName"] = rec.stps.SupplierName.ToUpper();
					}
					else
					{
						row["Code"] = rec.smcp.Code.ToUpper();
						row["SupplierName"] = rec.smcs.SupplierName.ToUpper();
					}
					dt.Rows.Add(row);
				}
			}
			return dt;
		}

		private int WordDistance(string S1, string S2)
		{
			if (S2.Length > S1.Length)
			{
				string temp = S2;
				S2 = S1;
				S1 = temp;
			}
			string[] wordsS1 = S1.Replace(",", "").Split(" ".ToCharArray());
			string[] wordsS2 = S2.Replace(",", "").Split(" ".ToCharArray());

			int word1, word2, thisD, wordbest;
			int wordsTotal = 0;
			int wordsTotalCount = 0;
			for (word1 = 0; word1 < wordsS1.Length; word1++)
			{
				wordbest = S2.Length;
				for (word2 = 0; word2 < wordsS2.Length; word2++)
				{
					thisD = LevenshteinDistance(wordsS1[word1], wordsS2[word2]);
					if (thisD < wordbest)
						wordbest = thisD;

					if (thisD == 0)
						break;
				}
				wordsTotal = wordsTotal + wordbest;
				wordsTotalCount++;
			}
			if (wordsTotalCount == 0)
				return 0;
			return (int)(((float)wordsTotal / (float)wordsTotalCount) * 100f);
		}

		// Calculate the Levenshtein Distance between two strings (the number of insertions,
		// deletions, and substitutions needed to transform the first string into the second)
		private int LevenshteinDistance(string S1, string S2)
		{
			int L1, L2;
			int[,] D; // Length of input strings and distance matrix
			int i, j, cost; // loop counters and cost of substitution for current letter
			int cI, cD, cS; // cost of next Insertion, Deletion and Substitution
			L1 = S1.Length; L2 = S2.Length;

			D = new int[L1 + 1, L2 + 1];
			for (i = 0; i <= L1; i++) D[i, 0] = i;
			for (j = 0; j <= L2; j++) D[0, j] = j;

			for (j = 1; j <= L2; j++)
			{
				for (i = 1; i <= L1; i++)
				{
					cost = Math.Abs(string.Compare(S1.Substring(i - 1, 1), S2.Substring(j - 1, 1)));
					cI = D[i - 1, j] + 1;
					cD = D[i, j - 1] + 1;
					cS = D[i - 1, j - 1] + cost;

					if (cI <= cD) // Insertion or Substitution
					{
						if (cI <= cS)
							D[i, j] = cI;
						else
							D[i, j] = cS;
					}
					else // Deletion or Substitution
						if (cD <= cS)
						D[i, j] = cD;
					else
						D[i, j] = cS;
				}
			}
			return D[L1, L2];
		}

		private string CleanCell(string cell)
		{
			cell = cell.Trim()
				.Replace(ERROR_CODE, string.Empty)
				.Replace(ERROR_VENDOR, string.Empty)
				.Replace(ERROR_MISSING_MCP_CODE, string.Empty)
				.Replace(WARN_NO_PAR, string.Empty)
				.Replace(WARN_INVENTORY_EXISTS, string.Empty)
				.Replace(BAD_NUMBER, string.Empty)
				.Replace(CREATE_NEW_VENDOR, string.Empty)
				.Replace(CREATE_NEW_TPP_CODE, string.Empty)
				.Replace(CREATE_NEW_PCP_CODE, string.Empty);

			while (cell.Contains(SPAN_START))
			{
				int start = cell.IndexOf(SPAN_START);
				int end = start;
				if (start >= 0)
					end = cell.IndexOf(">", start);
				if (end > start)
				{
					cell = cell.Remove(start, end + 1 - start);
				}
			}
			return cell;
		}

		protected eSetupStatus ValidateSetupData()
		{
			Page.Validate();
			if (Page.IsValid)
			{
				return eSetupStatus.Complete;
			}
			return eSetupStatus.Incomplete;
		}

		/// <summary>
		/// IVisionWizardControl interface save.  Called by wizard control on tab change
		/// </summary>
		public void Save()
		{
			//do nothing.  Grid rows are saved individually

			//SaveStatus(practiceID, -1, this.ToString(), ValidateSetupData());
		}
	}
}