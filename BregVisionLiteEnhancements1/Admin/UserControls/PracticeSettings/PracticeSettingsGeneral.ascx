﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeSettingsGeneral.ascx.cs" Inherits="BregVision.Admin.UserControls.PracticeSettings.PracticeSettingsGeneral" %>
<div class="content">
	<script type="text/javascript">

	    $(document).ready(function () {	     

			$('#updatePractice').click(function() {
                $(this).attr("disabled", true);

				var isDisplayBillChargeOnReceiptVal = ( 
					$('#displayBillChargeOnReceipt').checked || 
                    $('#displayBillChargeOnReceipt').is(':checked'));

				var isLateralityRequiredVal = (
					$('#isLateralityRequired').checked ||
                    $('#isLateralityRequired').is(':checked'));

                var generalLedgerNumber = $('#txtGeneralLedgerId').val();

				var bcsSubInventoryId = null;
                var isInventoryIdRequired = '<%= IsBcsSubInventoryIdReqForAdmin %>';
				if (isInventoryIdRequired == 'True') {
				    bcsSubInventoryId = $('#inventoryid').val();
                }

				var practice = {
					'practiceId': 1,
					'isDisplayBillChargeOnReceipt': isDisplayBillChargeOnReceiptVal,
					'isLateralityRequired': isLateralityRequiredVal,
					'generalLedgerNumber': generalLedgerNumber,
					'bcsSubInventoryId': bcsSubInventoryId ? bcsSubInventoryId : null
                };

				updatePractice(practice);
			});
        });

		function updatePractice(practice) {

			var options = {
				error: function (msg) {
					alert(msg);
					displayAlert(msg);
				},
				type: "POST",
				url: "PracticeSettings_General.aspx/UpdatePractice",
				data: JSON.stringify(practice),
				contentType: "application/json; charset=utf-8",
				success: function (response) {
				    window.location.reload(false);
				    alert("Details Updated Successfully!!");
				},
				failure: function (msg) { alert("fail"); }
			};

			$.ajax(options);
		}

		function displayAlert(msg) {
			alert("fail");
			alert(msg);
		}

	</script>

	<div class="content">
		<h2>General Settings</h2>
		<div>
			<input type="checkbox" id="displayBillChargeOnReceipt" <%=IsDisplayBillChargeOnReceipt %> />
			<label for="displayBillChargeOnReceipt">Display bill charge on receipt</label>
            </div>
        <br />
        <div>
            <input type="checkbox" id="isLateralityRequired" <%=IsLateralityRequired %> />
			<label for="isLateralityRequired">Is Laterality required for dispensement</label>
        </div>
        <br/>
        <asp:Panel id="BCSSubInventoryIDPanel" Visible="false" runat="server">
         <div>
          <span style="margin-right: 0.3%"></span>
			<label for="inventoryid">BCS SubInventory ID :</label>
        </div>
        <div>
            <span style="margin-right: 0.3%"></span>
            <input type="text" id="inventoryid" value="<%=BCSSubInventoryID %>" maxlength="50" style="margin-top:0.5%" />		
       </div>	
       </asp:Panel>
        <br/>
        <div>
          <span style="margin-right: 0.3%"></span>
			<label for="txtGeneralLedgerId">General Ledger Id :</label>
        </div>
        <div>
            <span style="margin-right: 0.3%"></span>
            <input type="text" id="txtGeneralLedgerId" value="<%=GeneralLedgerId %>" maxlength="50" style="margin-top:0.5%" />		
       </div>	
        <br/>
        <div>
            <span style="margin-right: 0.3%"></span>
			<input type="button" id="updatePractice" value="Update" class="button-primary" />
		</div>
	</div>
</div>
