﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeSettingsSiteBuilder.ascx.cs" Inherits="BregVision.Admin.UserControls.PracticeSettings.PracticeSettingsSiteBuilder" %>
<div class="content">
	<style>
		.err
		{
			background-color: red;
			color: white;
			padding: 4px;
			line-height: 1.8;
		}
		.warn
		{
			background-color: yellow;
			color: black;
			padding: 4px;
			line-height: 1.8;
		}
		.process
		{
			background-color: green;
			color: white;
			padding: 4px;
			line-height: 1.8;
		}
		.info
		{
			background-color: orange;
			color: white;
			padding: 4px;
			line-height: 1.8;
		}

		.btnUploadSiteConfig
		{
			margin: 10px;
		}
	</style>

	<style type="text/css">
		div.RadGrid .rgPager .rgPagePrev {
			display: none;
		}
		div.RadGrid .rgPager .rgPageNext {
			display: none;
		}
		div.RadGrid .rgPager .rgPageFirst {
			display: none;
		}
		div.RadGrid .rgPager .rgPageLast {
			display: none;
		}
	</style>

	<script>
		function enableUpload()
		{
			document.getElementById("<%=btnUpload.ClientID%>").className = 'button-primary';
			document.getElementById("<%=btnUpload.ClientID%>").disabled = false;
		}
	</script>

	<asp:HiddenField ID="ParentSpreadsheetsProcessed" runat="server" />

	<div class="content">
		<h2>Site Builder Settings</h2>
		<div>
			<table>
				<tr>
					<td rowspan="2" style="width:200px" align="center">
						<span style="margin:5px">
							<asp:Button ID="btnUpload" Enabled="false" runat="server" Text="Upload Spreadsheets" OnClick="btnUpload_Click" />
						</span>
					</td>
					<td style="width: 130px">
						<span>Uploaded Products:&nbsp;</span>
					</td>
					<td>
						<asp:Label ID="lblProductsPath" runat="server"></asp:Label>
					</td>
					<td style="width: 20px">
					</td>
					<td>
						<asp:FileUpload ID="UploadProducts" ClientIDMode="Static" onchange="enableUpload()" runat="server" ViewStateMode="Enabled" width="100%" />
					</td>
					<td rowspan="2" style="width:200px" align="center">
						<span style="margin:5px">
							<asp:Button ID="btnProcess" Enabled="false" runat="server" Text="Process Spreadsheets" OnClick="btnProcess_Click" />
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<span>Uploaded Pars:&nbsp;</span>
					</td>
					<td>
						<asp:Label ID="lblParsPath" runat="server"></asp:Label>
					</td>
					<td>
					</td>
					<td>
						<asp:FileUpload ID="UploadPars" runat="server" ViewStateMode="Enabled" Width="100%" />
					</td>
				</tr>
			</table>
		</div>
		<br />
		<br />
		<div class="flex-row">
			<telerik:RadGrid ID="grdProducts" runat="server"
				AutoGenerateColumns="True" HierarchyLoadMode="ServerOnDemand" PageSize="10"
				OnNeedDataSource="grdProducts_NeedDataSource" OnPreRender="grdProducts_PreRender" OnItemCommand="grdProducts_ItemCommand"
				AllowPaging="True" AllowSorting="True" GroupingEnabled="False">
				<MasterTableView HeaderStyle-HorizontalAlign="Center" DataKeyNames="_PARS_LIST_COLUMN_">
					<NestedViewTemplate>
						<telerik:RadGrid ID="grdPars" runat="server"
							AutoGenerateColumns="True" HierarchyLoadMode="ServerOnDemand"
							OnNeedDataSource="grdPars_NeedDataSource" OnPreRender="grdPars_PreRender"
							AllowSorting="True" GroupingEnabled="False">
							<MasterTableView HeaderStyle-HorizontalAlign="Center">
							</MasterTableView>
						</telerik:RadGrid>
					</NestedViewTemplate>
				</MasterTableView>
			</telerik:RadGrid>
		</div>
		<div class="flex-row" style='<%=SpreadsheetsProcessed ? "" : "display: none" %>'>
			<h3>Spreadsheets Processed!</h3>
		</div>
	</div>
</div>
