﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.Admin.PracticeSetup;
using ClassLibrary.DAL;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Text.RegularExpressions;
using Telerik.Web.UI;
using Excel;
using System.Web.Services;
using DAL;

namespace BregVision.Admin.UserControls.PracticeSettings
{
    public partial class PracticeSettingsGeneral : PracticeSetupBase, IVisionWizardControl
    {
		/// <summary>
		/// IVisionWizardControl Interface.
		/// </summary>
		public bool NeedsInitialBind { get; set; }
		public string IsDisplayBillChargeOnReceipt { get; set; }
        public string IsLateralityRequired { get; set; }
        public bool IsBcsSubInventoryIdReqForAdmin { get; set; }
        public string GeneralLedgerId { get; set; }
		private bool ProductsTriggered { get; set; }
        public string BCSSubInventoryID { get; set; }
        private DataSet uploadedConfig { get; set; }

		const string CHECKED = "checked";


		protected void Page_Load(object sender, EventArgs e)
		{
            IsBcsSubInventoryIdReqForAdmin = Context.User.IsInRole("BregAdmin");
            var practice = DAL.Practice.Practice.GetPracticeById(practiceID);

            if (IsBcsSubInventoryIdReqForAdmin && practice != null)
            {
                BCSSubInventoryID = practice.BCSSubInventoryID;
            }

            if (!Page.IsPostBack && IsBcsSubInventoryIdReqForAdmin)
                BCSSubInventoryIDPanel.Visible = true;
           
            var isdisplayBillChargeOnReceipt = practice.IsDisplayBillChargeOnReceipt;
            var isLateralityRequired = practice.IsLateralityRequired;
			string generalLedgerNumber = DAL.Practice.Practice.GeneralLedgerNumber(practiceID);

			IsDisplayBillChargeOnReceipt = isdisplayBillChargeOnReceipt ? CHECKED : "";

            if (isLateralityRequired.HasValue && isLateralityRequired.Value)
                IsLateralityRequired = CHECKED;
            else
                IsLateralityRequired = "";

            GeneralLedgerId = generalLedgerNumber;
		}

		protected void Page_PreRender(object sender, EventArgs e)
		{
		}

		
		protected eSetupStatus ValidateSetupData()
		{
			Page.Validate();
			if (Page.IsValid)
			{
				return eSetupStatus.Complete;
			}
			return eSetupStatus.Incomplete;
		}

		/// <summary>
		/// IVisionWizardControl interface save.  Called by wizard control on tab change
		/// </summary>
		public void Save()
		{
			//do nothing.  Grid rows are saved individually

			SaveStatus(practiceID, -1, this.ToString(), ValidateSetupData());
		}
	}
}