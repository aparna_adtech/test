﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Practice_Catalog1.aspx.cs" Inherits="BregVision.Admin.Practice_Catalog1" MasterPageFile="~/MasterPages/PracticeAdmin.Master" %>
<%@ Register Src="~/Admin/UserControls/Practice/PracticeCatalog1.ascx" TagName="PracticeCatalog" TagPrefix="bvl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true" Visible="true">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    </telerik:RadAjaxManager>
    <div style="text-align: center">
        <table id="OuterTable" width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center">
                    <br />
                    <bvl:PracticeCatalog ID="ctlPracticeCatalog" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
