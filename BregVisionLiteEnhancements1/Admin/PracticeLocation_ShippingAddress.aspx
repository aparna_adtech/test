<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PracticeLocationAdmin.Master"
  Inherits="PracticeLocation_ShippingAddress" CodeBehind="PracticeLocation_ShippingAddress.aspx.cs" %>

<%@ Register Src="~/Admin/UserControls/PracticeLocation/PracticeLocationAddress.ascx"
  TagName="PracticeLocationAddress" TagPrefix="bvl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true"
  Visible="true">
  <bvl:PracticeLocationAddress ID="ctlPracticeLocationAddress" runat="server" />
</asp:Content>
