<%@ Control Language="C#" AutoEventWireup="True" Codebehind="PracticeAdminUserPractice.ascx.cs"
    Inherits="BregVision.Admin.PracticeAdminUserPractice" %>
<%@ Register Assembly="RadComboBox.Net2" Namespace="Telerik.WebControls" TagPrefix="radC" %>
<table cellspacing="3">
    <!--Used to specify Clinic and location, Drop down for clinic location will also be included -->
    <tr>
        <td style="height: 22px; width: 100%">
            <span>
                <asp:LoginName ID="LoginName1" runat="server" FormatString="Welcome, {0}" Font-Size="XX-Small" />
                <%--<asp:LoginStatus ID="lsUSer" runat="server" Font-Size="XX-Small" LogoutText="(Log Out)" LogoutPageUrl="~/default.aspx" LogoutAction="RedirectToLoginPage" />--%>
                <asp:HyperLink ID="lnkLogOut" runat="server" NavigateUrl="~/processlogin.aspx" Text="(Log Out)" Font-Size="xx-small" ></asp:HyperLink>    
            </span>&nbsp; 
            <span>
                <asp:Label ID="lblClinic" runat="server" Text="Clinic XYZ" Font-Size="XX-Small"></asp:Label>&nbsp;
                <asp:Label ID="lblLocation" runat="server" Text="Location: ABC" Font-Size="XX-Small" ForeColor="Blue" Visible="False"></asp:Label>
            </span>&nbsp; 
            <span id="spnChangeLocation" style="visibility: visible">
                <asp:Label ID="lblChangeLocation" runat="server" Text="Change Location:" Font-Size="XX-Small" Visible="False"></asp:Label>&nbsp;
                <radC:RadComboBox
                    ID="cbxChangeLocation" runat="server" Skin="ClassicBlue" SkinsPath="~/RadControls/ComboBox/Skins"
                    Width="110px" ExpandEffect="Fade" AutoPostBack="True" OnSelectedIndexChanged="cbxChangeLocation_SelectedIndexChanged"
                    ShowWhileLoading="False" ToolTip="Select a practice location to change your view." Visible="False">
                </radC:RadComboBox>
                &nbsp; </span>
        </td>
    </tr>
</table>
