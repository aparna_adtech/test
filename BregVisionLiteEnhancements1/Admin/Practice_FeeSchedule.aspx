﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PracticeAdmin.Master"
CodeBehind="Practice_FeeSchedule.aspx.cs" Inherits="BregVision.Admin.Practice_FeeSchedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true" Visible="true">
<meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1"/>
<style type="text/css">
  .fsColumn { word-wrap: normal; }

  #editPayerHeader {
    -moz-appearance: textfield;
    -webkit-appearance: textfield;
    background-color: white;
    background-color: -moz-field;
    border: 1px solid darkgray;
    box-shadow: 1px 1px 1px 0 lightgray inset;
    font: -moz-field;
    font: -webkit-small-control;
    margin-top: 5px;
    padding: 2px 3px 2px 3px;
    width: 100px;
  }

  .editPayerHeader input {
      color: #000;
      padding: 2px;
      display: block;
    }
  .editPayerHeader button {
      padding: 2px;
     }

</style>

<link rel="stylesheet" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css" type="text/css"/>

<script type="text/javascript">

  var PRACTICE_ID = <%= PracticeId %>;
  var feeSchedules = <%= FeeScheduleAsJson %>;

  function displayAlert(msg) {
    alert("fail");
    alert(msg);
  }

  function sendUpdate(feeScheduleId, value) {
    var options = {
      error: function(msg) {
        alert(msg);
        displayAlert(msg);
      },
      type: "POST",
      url: "Practice_FeeSchedule.aspx/UpdateFeeSchedule",
      data: "{'feeScheduleId':'" + feeScheduleId + "', 'amount':'" + value + "' }",
      contentType: "application/json; charset=utf-8",
      success: function(response) {},
      failure: function(msg) { alert("fail"); }
    };

    $.ajax(options);
  }

  function PayerEnableClicked(cb, payerId) {
    sendPayerUpdate(payerId, null, cb.checked);
  }

  function sendPayerUpdate(payerId, name, enabled) {
    if (name == null)
      name = '';
    if (enabled == null)
      enabled = '';
    console.log('PayerEnableUpdate: payerId:' + payerId + ' name:' + name + ' enabled:' + enabled);
    var options = {
      error: function(msg) {
        alert(msg);
        displayAlert(msg);
      },
      type: "POST",
      url: "Practice_FeeSchedule.aspx/UpdatePayer",
      data: "{'payerId':'" + payerId + "', 'name':'" + name + "', 'enabled':'" + enabled + "' }",
      contentType: "application/json; charset=utf-8",
      success: function(response) {},
      failure: function(msg) { alert("fail"); }
    };

    $.ajax(options);
  }

  $(document).ready(function() {

    var columns = [
      {
        sTitle: "HCPCS"
      }
    ];
    $.each(feeSchedules.payerNamesList, function(i, val) {
      var obj = {
        sTitle: '<div id="payer' + val.PracticePayerID + '" class="editPayerHeader strong" contenteditable="true">' + val.PayerName + '</div><div style="font-weight:normal; font-size:90%;margin-left:15px;" title="Check or uncheck this box to activate or inactivate payor. Payor will be active as default, which means it will show in your dispensing dropdown as a selection choice." class="no-wrap"><input type="checkbox" onclick="PayerEnableClicked(this, ' +
          val.PracticePayerID + ');" ' + (val.IsActive == '1' ? 'checked' : '') + '>Active</div>'
      };
      columns.push(obj);
    });

    $('#payerTable').dataTable({
      'bSort': false,
      'aoColumns': columns,
      //                'asSorting': [[0,'asc']],
      'pagingType': 'numbers',
      'fnDrawCallback': function() {
        $('td.edit').editable(function(value, settings) {
          value = value.replace('$', '');
          if (isNaN(value)) {
            alert("Please enter numeric value.");
            return (this.revert);
          }
          sendUpdate(this.id, value);

          return ('$' + parseFloat(value).toFixed(2));
        },
        {
          cancel: "&times;",
          submit: 'Ok',
          tooltip: "Click to Add/Edit Allowable",
          placeholder: '<img src="../App_Themes/Breg/images/Common/edit.png">'
        });

        $('.editPayerHeader').editable(function(value, settings) {
          var payerId = this.id.replace("payer", "");
          sendPayerUpdate(payerId, value, null);
          return (value);
        },
        {
          cancel: "&times;",
          submit: "Ok",
          tooltip: "Click to edit Payer name"
        });
      }
    });
    $("#payerTable_filter label").replaceWith($("#payerTable_filter input"));
    $("#payerTable_filter input").attr("placeholder", "Search for HCPC");

    $.each(feeSchedules.hcpcsList, function(i, hcpcs) {
      var hcpcsArray = [];
      hcpcsArray.push(hcpcs.Code);
      $.each(hcpcs.AllowableAmounts, function(amtIndex, amtVal) {
        var allowAmt = '';
        if (amtVal.Amount) {
          allowAmt = '$' + amtVal.Amount.toFixed(2);
        }
        hcpcsArray.push(allowAmt);
      });
      var oTable = $('#payerTable').dataTable();
      var newRow = oTable.fnAddData(hcpcsArray);
      var oSettings = oTable.fnSettings();
      var nTr = oSettings.aoData[newRow[0]].nTr;
      for (var j = 0; j < hcpcs.AllowableAmounts.length; j++) {
        var cell = $('td', nTr)[j + 1];
        cell.setAttribute("id", hcpcs.AllowableAmounts[j].Id);
        cell.setAttribute("class", "edit");
      }
    });

    $('#addPayer').click(function() {
      $(this).attr("disabled", true);
      var pyrName = $('#payerName');
      var newPyrName = pyrName.val();
      var newPayer = {
        'practiceId': PRACTICE_ID,
        'payerName': newPyrName,
      };
      pyrName.val('');
      addNewPayer(newPayer);
      //                reloadFeeSchedules();
    });
  });

  function addNewPayer(newPayer) {

    //            AddPayer(int practiceId, string payerName, string groupId,string memberId)
    var options = {
      error: function(msg) {
        alert(msg);
        displayAlert(msg);
      },
      type: "POST",
      url: "Practice_FeeSchedule.aspx/AddPayer",
      data: JSON.stringify(newPayer),
      contentType: "application/json; charset=utf-8",
      success: function(response) {
        window.location.reload(true);
      },
      failure: function(msg) { alert("fail"); }
    };

    $.ajax(options);
  }

</script>

<div class="content">
  <div class="flex-row">
    <h2>Payer Fee Schedule</h2>
    <div id="payer">
      <label for="payerName">New Payer</label>
      <input type="text" id="payerName" placeholder="Payer Name"/>
      <input type="button" id="addPayer" value="Add" class="button-primary"/>
    </div>
  </div>
  <table id="payerTable">
    <thead></thead>
    <tbody></tbody>
  </table>
</div>

</asp:Content>