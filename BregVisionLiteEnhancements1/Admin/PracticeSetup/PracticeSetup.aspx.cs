﻿using System;
using System.Data;
using System.Web.UI.WebControls;

namespace BregVision.Admin.PracticeSetup
{
    public partial class PracticeSetup : Bases.PageBase
    {
        public bool IsSetupComplete
        {
            get { return BLL.Practice.Practice.IsSetupComplete(practiceID); }
        }

        public Telerik.Web.UI.RadAjaxManager RadAjaxManagerMain
        {
            get { return RadAjaxManager1; }
        }

        public Telerik.Web.UI.RadAjaxLoadingPanel DefaultLoadingPanel
        {
            get { return RadAjaxLoadingPanel1; }
        }


        public System.Web.UI.UserControl StatusPage
        {
            get { return ctlStatusPage; }
        }

        public Telerik.Web.UI.RadTabStrip WizardTabStrip
        {
            get { return rtsWizard; }
        }

        public Telerik.Web.UI.RadMultiPage WizardMultiPage
        {
            get { return rmpWizard; }
        }

        //public System.Web.UI.HtmlControls.HtmlAnchor HomeButton
        //{
        //    get { return null; }
        //}

        public Telerik.Web.UI.RadTabStrip MainMenuTabStrip
        {
            get
            {
                return ctlMainMenu.MainMenuTabStrip;
            }
        }

        public Button NavigateHomeButton
        {
            get { return ctlNavPanel.NavigateHomeButton; }
        }

        public Telerik.Web.UI.RadScriptManager RadScriptManagerSetup
		{
			get { return RadScriptManager1; }
		}



        protected void Page_Init(object sender, EventArgs e) 
        {
            ctlMainMenu.MainMenuTabStrip.TabClick += new Telerik.Web.UI.RadTabStripEventHandler(MainMenuTabStrip_TabClick);
            ctlMainMenu.SetTabVisibility("Dispense", false);
            ctlMainMenu.SetTabVisibility("Inventory", false);
            ctlMainMenu.SetTabVisibility("CheckIn", false);
            ctlMainMenu.SetTabVisibility("Reports", false);
            ctlMainMenu.SetTabVisibility("Admin", false);

            DAL.Practice.Practice.SetPracticeSession();

            if (BLL.Website.IsVisionExpress())
            {
                //((BregVision)Page.Master).HideAllMenus();

                VisionLitePageView dispensePage = rmpWizard.FindPageViewByID("stepPracticeLocationDispensementModifications") as VisionLitePageView;
                rmpWizard.PageViews.Remove(dispensePage);

                VisionLitePageView icd9Page = rmpWizard.FindPageViewByID("stepPracticeICD9s") as VisionLitePageView;
                rmpWizard.PageViews.Remove(icd9Page);
            }

        }



        void MainMenuTabStrip_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            if (e.Tab.Value == "Home" && e.Tab.NavigateUrl == "#")
            {
                SetUnableToNavigateLabel();
                ctlMainMenu.FindToolbarTab("Home").ToolTip = "Please complete all steps before using Vision Express. Please click the 'Setup Status' tab on the right to view your status.";
                ctlMainMenu.NavigateLabel.Text = "Please complete all steps before using Vision Express. Please click the 'Setup Status' tab on the right to view your status.";
                ctlMainMenu.NavigateLabel.Visible = true;
                ctlMainMenu.NavLabelTimer.Enabled = true;
                ctlMainMenu.FindToolbarTab("Home").Selected = false;
            }
        }

        protected void Page_Load(object sender, EventArgs e) 
        {
            AddAjaxSettingWizardUpdate();
        }

        public void AddAjaxSettingLoadingPanelUpdate(System.Web.UI.Control control)
        {
            RadAjaxManager1.AjaxSettings.AddAjaxSetting(control, control, RadAjaxLoadingPanel1);
        }

        public void AddAjaxSettingLoadingPanelUpdate(System.Web.UI.Control control, System.Web.UI.Control controlToUpdate)
        {
            RadAjaxManager1.AjaxSettings.AddAjaxSetting(control, controlToUpdate, RadAjaxLoadingPanel1);
        }


        public void AddAjaxSettingWizardUpdate(System.Web.UI.Control control)
        {
            
            //((PracticeSetup)Page).RadAjaxManagerMain.AjaxSettings.AddAjaxSetting(control, WizardTabStrip);
            RadAjaxManager1.AjaxSettings.AddAjaxSetting(control, WizardTabStrip);
            RadAjaxManager1.AjaxSettings.AddAjaxSetting(control, WizardMultiPage, RadAjaxLoadingPanel1);
            RadAjaxManager1.AjaxSettings.AddAjaxSetting(control, StatusPage);

            RadAjaxManager1.AjaxSettings.AddAjaxSetting(control, ctlMainMenu.MainMenuTabStrip);
            RadAjaxManager1.AjaxSettings.AddAjaxSetting(control, ctlNavPanel.NavigateHomeButton);
        }

        private void AddAjaxSettingWizardUpdate()
        {
            RadAjaxManager1.AjaxSettings.AddAjaxSetting(WizardTabStrip, ctlMainMenu.MainMenuTabStrip);
            RadAjaxManager1.AjaxSettings.AddAjaxSetting(ctlNavPanel.PreviousButton, ctlMainMenu.MainMenuTabStrip);
            RadAjaxManager1.AjaxSettings.AddAjaxSetting(ctlNavPanel.NextButton, ctlMainMenu.MainMenuTabStrip);

            RadAjaxManager1.AjaxSettings.AddAjaxSetting(WizardTabStrip, ctlNavPanel.NavigateHomeButton);
            RadAjaxManager1.AjaxSettings.AddAjaxSetting(ctlNavPanel.PreviousButton, ctlNavPanel.NavigateHomeButton);
            RadAjaxManager1.AjaxSettings.AddAjaxSetting(ctlNavPanel.NextButton, ctlNavPanel.NavigateHomeButton);

            RadAjaxManager1.AjaxSettings.AddAjaxSetting(ctlMainMenu.MainMenuTabStrip, ctlMainMenu.NavigateLabel);
            RadAjaxManager1.AjaxSettings.AddAjaxSetting(ctlMainMenu.MainMenuTabStrip, RadSlidingZone1);

        }

        private void EnableHomeMenu()
        {
            if (IsSetupComplete == false)
            {
                //aHome.HRef = "#";
                //aHome.Title = "Please complete all steps before using Vision Express";
                //lblNavigate.Text = "Please complete all steps before using Vision Express";
                SetUnableToNavigateLabel();

                ctlNavPanel.NavigateHomeButton.Enabled = false;
            }
            else
            {
                //aHome.HRef = "../../home.aspx";
                //aHome.Title = "";

                ctlMainMenu.FindToolbarTab("Home").NavigateUrl = "~/home.aspx";
                ctlMainMenu.FindToolbarTab("Home").ToolTip = "";
                ctlMainMenu.NavigateLabel.Text = "";
                ctlNavPanel.NavigateHomeButton.Enabled = true;
            }
        }

        private void SetUnableToNavigateLabel()
        {
            ctlMainMenu.FindToolbarTab("Home").NavigateUrl = "#";
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (rmpWizard.SelectedIndex == 0)
            {
#if(!DEBUG)
                rwWelcomeWindow.Enabled = true;
#else
                rwWelcomeWindow.Enabled = false;
                rwWelcomeWindow.VisibleOnPageLoad = false;
#endif
            }
            else
            {
                rwWelcomeWindow.Enabled = false;
                rwWelcomeWindow.VisibleOnPageLoad = false;
            }
            EnableHomeMenu();
        }

    }
}
