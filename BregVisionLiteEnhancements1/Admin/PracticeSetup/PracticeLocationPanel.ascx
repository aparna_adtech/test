﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeLocationPanel.ascx.cs" Inherits="BregVision.Admin.PracticeSetup.PracticeLocationPanel" %>
<%@ Register Src="~/Admin/PracticeSetup/VisionLiteLocationNavPanel.ascx" TagName="VLLocationNavPanel" TagPrefix="bvl" %>

<table>
    <tr>
        <td valign="top" style="width:130px">
            <div style="width:130px">
                <bvl:VLLocationNavPanel ID="ctlVLLocationNavPanel" runat="server" />
            </div>
        </td>
        <td>
            <asp:PlaceHolder runat="server" ID="plPracticeLocationControl" />
        </td>
    </tr>
</table>