﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.ComponentModel;
using System.Security.Permissions;
using System.Web;
using System.Web.UI;


namespace BregVision.Admin.PracticeSetup
{

    public partial class VisionLiteLocationNavPanel : Bases.UserControlBase
    {

        public int PreviouslySelectedIndex
        {
            get { return Convert.ToInt32(ViewState["PSLI"]); }
            set { ViewState["PSLI"] = value; }
        }


        new protected void Page_Init(object sender, EventArgs e)  
        {
            base.Page_Init(sender, e);
            LoadPracticeLocationTab();

            //GetSelectedTab();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();
        }

        private void InitializeAjaxSettings()
        {
            ((PracticeSetup)Page).RadAjaxManagerMain.AjaxSettings.AddAjaxSetting(rtsLocationWizard, rtsLocationWizard);
        }

        private void GetSelectedTab()
        {
            //this is a hack.  I needed to know earlier in the page lifecycle what the practiceLocation was changed to;
            string eventTarget = Request.Params["__EVENTTARGET"];
            if (eventTarget == "rtsLocationWizard")
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"];
                int selectedTabIndex = -1;
                if (int.TryParse(eventArgument, out selectedTabIndex))
                {
                    RadTab activeTab = rtsLocationWizard.Tabs[selectedTabIndex];
                    SetCurrentPracticeLocation(activeTab);
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            LoadPracticeLocationTab();

            foreach (RadTab locationTab in rtsLocationWizard.Tabs)
            {
                SelectCurrentPracticeLocation(locationTab);
            }
        }

        private void LoadPracticeLocationTab()
        {
            rtsLocationWizard.Tabs.Clear();
            BLL.PracticeLocation.PracticeLocation bllPracticeLocation = new BLL.PracticeLocation.PracticeLocation();
            DataTable dtPracticeLocation = null;
            dtPracticeLocation = bllPracticeLocation.SelectAll(practiceID);

            
            //This was to Auto Load Locations
            int i = 0;
            foreach (DataRow row in dtPracticeLocation.Rows)
            {
                //
                RadTab locationTab = new RadTab();

                locationTab.Text = row["Name"].ToString();
                locationTab.Value = row["PracticeLocationID"].ToString();

                if (i == 0 && practiceLocationID < 1)
                {
                    SetCurrentPracticeLocation(locationTab);
                }

                rtsLocationWizard.Tabs.Add(locationTab);
                i++;
            }
        }

        private void SelectCurrentPracticeLocation(RadTab locationTab)
        {
            if (practiceLocationID > 0)
            {
                int currentTabPracticeLocationID = Convert.ToInt32(locationTab.Value);

                if (practiceLocationID == currentTabPracticeLocationID)
                {
                    rtsLocationWizard.SelectedIndex = locationTab.Index;
                }
            }

        }

        protected void rtsLocationWizard_OnTabClick(object sender, RadTabStripEventArgs e)
        {
            //Could Move this to the page_init because I needed the active tab sooner in the page lifecycle
            
            //Save the currentPage
            SaveControl(PreviouslySelectedIndex);


            RadTab activeTab = e.Tab;
            SetCurrentPracticeLocation(activeTab);
            SetCurrentPageStatus();
            PreviouslySelectedIndex = activeTab.Index;
        }

        private void SetCurrentPageStatus()
        {
            //need to get the pageStatusControl and tell it what the current page is
            PracticeLocationPanel practiceLocationPanel = this.Parent as PracticeLocationPanel;
            if (practiceLocationPanel != null)
            {
                //this is kindof a hack.  it was difficult to pass this control all the way here so I hard coded the name
                Controls.IndividualPageStatus pageStatus = Page.FindControl("ctlIndividualPageStatus") as Controls.IndividualPageStatus;
                pageStatus.CurrentPageView = practiceLocationPanel.Parent as VisionLitePageView;
                
            }

        }

        private void SaveControl(int PreviouslySelectedIndex)
        {
            PracticeLocationPanel practiceLocationPanel = this.Parent as PracticeLocationPanel;
            if (practiceLocationPanel != null)
            {
                practiceLocationPanel.Save();
            }
        }

        private void SetCurrentPracticeLocation(RadTab activeTab)
        {
            practiceLocationID = Convert.ToInt32(activeTab.Value);
            practiceLocationName = activeTab.Text;
        }

    }
}