﻿using System.Linq;
using ClassLibrary.DAL;

namespace BregVision.Admin.PracticeSetup
{
    public class PracticeSetupBase: Bases.UserControlBase
    {
        public enum eSetupStatus
        {
            Incomplete,
            Complete
        }
        public void SaveStatus(int savedPracticeID, int savedPracticeLocationID, string userControlName, eSetupStatus status)
        {
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                var exists = from ss in visionDB.SetupStatus
                                 where ss.PracticeID == savedPracticeID
                                 && ss.PracticeLocationID == savedPracticeLocationID
                                 && ss.UserControlName == userControlName
                                 select ss;
                
                ClassLibrary.DAL.SetupStatus setupStatus = exists.FirstOrDefault<SetupStatus>();
                if (setupStatus == null)
                {
                    setupStatus = new SetupStatus();
                    visionDB.SetupStatus.InsertOnSubmit(setupStatus);
                }
                setupStatus.PracticeID = savedPracticeID;
                setupStatus.PracticeLocationID = savedPracticeLocationID;
                setupStatus.UserControlName = userControlName;
                setupStatus.PageStatus = (int)status;

                visionDB.SubmitChanges();
            }
        }

    }
}
