﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace BregVision.Admin.PracticeSetup
{
    public partial class VisionLiteNavPanel : Bases.UserControlBase
    {
        public string TabControlName { get; set; }
        public string MultiPageControlName { get; set; }
        public string PageStatusControlName { get; set; }

        public RadTabStrip TabControl { get; private set; }
        public RadMultiPage MultiPageControl { get; private set; }
        public Controls.IndividualPageStatus PageStatusControl { get; private set; }

        public Button NavigateHomeButton
        {
            get { return NavigateHome; }
            set { NavigateHome = value; }
        }

        public Button PreviousButton
        {
            get { return btnPrevious; }
            set { btnPrevious = value; }
        }

        public Button NextButton
        {
            get { return btnNext; }
            set { btnNext = value; }
        }

        public int PreviouslySelectedIndex 
        {
            get { return Convert.ToInt32(ViewState["PSI"]); }
            set { ViewState["PSI"] = value; } 
        }


        new protected void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);
            if (this.TabControlName != null)
            {
                this.TabControl = Page.FindControl(TabControlName) as RadTabStrip;
                if (this.TabControl != null)
                {
                    //attach to the tab changed event
                    TabControl.TabClick += new RadTabStripEventHandler(TabControl_TabClick);
                }
            }
            if (this.MultiPageControlName != null)
            {
                this.MultiPageControl = Page.FindControl(MultiPageControlName) as RadMultiPage;
            }

            if (this.PageStatusControlName != null)
            {
                this.PageStatusControl = Page.FindControl(PageStatusControlName) as Controls.IndividualPageStatus;
            }

            if (MultiPageControl != null && TabControl != null)
            {
                
                foreach (VisionLitePageView pageView in MultiPageControl.PageViews)
                {


                    //remove the tabs that are not for vision express
                    DAL.Practice.Practice.SetPracticeSession();
                    if (BLL.Website.IsVisionExpress())
                    {
                        if (pageView.ID != "stepPracticeLocationDispensementModifications" && pageView.ID != "stepPracticeICD9s")
                        {
                            string tabTitle = string.Format("Step {0} - {1}", pageView.Index +1, pageView.Title);
                            RadTab newTab = new RadTab(tabTitle);
                            //add style here to left justify the text  style="text-align:left"
                            newTab.Style.Add("text-align", "left");
                            TabControl.Tabs.Add(newTab);
                        }
                    }
                   
                }

            }

            

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();
        }

        private void InitializeAjaxSettings()
        {
            //btnPrevious
            ((PracticeSetup)Page).AddAjaxSettingWizardUpdate(btnPrevious);

            //btnNext
            ((PracticeSetup)Page).AddAjaxSettingWizardUpdate(btnNext);

            //tabControl
            ((PracticeSetup)Page).AddAjaxSettingWizardUpdate(TabControl);
        }

        private void SetCurrentPageStatus(int newIndex)
        {
            if (this.PageStatusControl != null)
            {
                this.PageStatusControl.CurrentPageView = MultiPageControl.PageViews[newIndex] as VisionLitePageView;
            }
        }




        void TabControl_TabClick(object sender, RadTabStripEventArgs e)
        {
            //Save the currentPage
            SaveControl(PreviouslySelectedIndex);

            NavigateWizard(e.Tab.Index);
        }

        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            NavigateWizard(false);
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            NavigateWizard(true);
        }

        protected void NavigateHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/home.aspx", false);
        }

        private void SaveControl(int selectedMultiPageIndex)
        {
            VisionLitePageView step = MultiPageControl.PageViews[selectedMultiPageIndex] as VisionLitePageView;
            if (step != null)
            {
                if (step.PrimaryControl != null)
                {
                    //IF I MODIFY SAVE TO RETURN A BOOLEAN, I CAN SAVE THE STATUS OF THE USER CONTROL HERE
                    //NEED TO SAVE PRACTICEID, USERCONTROL NAME, AND STATUS.
                    step.PrimaryControl.Save();
                }
            }
        }



        //catch the next / previous events from the buttons and the tab changed event from the tab control and change the multiview
        public void NavigateWizard(bool forward)
        {
            //Save the currentPage
            SaveControl(PreviouslySelectedIndex);

            EnableHomePageButtonOnCompletion();

            int direction = (forward == true) ? 1 : -1;
            int selectedIndex = -1;
            if (MultiPageControl != null && TabControl != null)
            {
                selectedIndex = TabControl.SelectedIndex + direction;

                if (selectedIndex < 0)
                    selectedIndex = 0;

                if (selectedIndex > TabControl.Tabs.Count - 1)
                    selectedIndex = TabControl.Tabs.Count - 1;

                NavigateWizard(selectedIndex);

            }
        }

        private void EnableHomePageButtonOnCompletion()
        {
            bool setupComplete = ((PracticeSetup)this.Page).IsSetupComplete;
            if (setupComplete == true)
            {
                NavigateHome.Enabled = true;
            }
        }

        private void NavigateWizard(int newIndex)
        {


            //MWS I don't think i need these here.  Should be in NavigateWizard (bool... right before call to this function
            TabControl.SelectedIndex = newIndex;
            TabControl.SelectedTab.Enabled = true;

            //Navigate to the next page
            MultiPageControl.SelectedIndex = newIndex;
            PreviouslySelectedIndex = newIndex;
            SetCurrentPageStatus(newIndex);
        }



    }
}