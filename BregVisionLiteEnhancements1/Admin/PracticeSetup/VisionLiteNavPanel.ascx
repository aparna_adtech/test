﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VisionLiteNavPanel.ascx.cs" Inherits="BregVision.Admin.PracticeSetup.VisionLiteNavPanel" %>

<table border="0">
    <tr>
        <td>
            <asp:Button ID="btnPrevious" runat="server"  Text="< Previous" CausesValidation="false" 
                onclick="btnPrevious_Click" />
        </td>
        <td>
            <asp:Button ID="btnNext" runat="server" Text="Next >" CausesValidation="false" onclick="btnNext_Click" />
        </td>
        <td>
            <asp:Button ID="NavigateHome" runat="server" Text="Go To Home Page" OnClick="NavigateHome_Click" Enabled="false" /> 
        </td>
    </tr>
</table>
