﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PracticeSetup.aspx.cs"
    Inherits="BregVision.Admin.PracticeSetup.PracticeSetup" EnableViewState="true"
    EnableEventValidation="false" %>

<%@ Register Assembly="BregVision" Namespace="BregVision.Admin.PracticeSetup" TagPrefix="bvw" %>
<%@ Register Src="~/MasterPages/UserControls/MainMenu.ascx" TagName="MainMenu" TagPrefix="bvu" %>
<%@ Register Src="~/Admin/PracticeSetup/VisionLiteNavPanel.ascx" TagName="VLNavPanel"
    TagPrefix="bvl" %>
<%@ Register Src="~/Admin/PracticeSetup/PracticeLocationPanel.ascx" TagName="PracticeLocationPanel"
    TagPrefix="bvl" %>
<%--<%@ Register Src="~/Admin/PracticeSetup/VisionLiteLocationNavPanel.ascx" TagName="VLLocationNavPanel" TagPrefix="bvl" %>--%>
<%@ Register Src="~/Admin/PracticeSetup/StatusPage.ascx" TagName="StatusPage" TagPrefix="bvl" %>
<%@ Register Src="~/Admin/PracticeSetup/Controls/IndividualPageStatus.ascx" TagName="IndividualPageStatus"
    TagPrefix="bvl" %>
<%@ Register Src="~/Admin/UserControls/Practice/PracticeCreditApp.ascx" TagName="PracticeCreditApp"
    TagPrefix="bvl" %>
<%@ Register Src="~/Admin/UserControls/Practice/PracticeLocations.ascx" TagName="PracticeLocations"
    TagPrefix="bvl" %>
<%@ Register Src="~/Admin/UserControls/Practice/PracticeBillingAddress.ascx" TagName="PracticeBillingAddress"
    TagPrefix="bvl" %>
<%@ Register Src="~/Admin/UserControls/Practice/PracticeContact.ascx" TagName="PracticeContact"
    TagPrefix="bvl" %>
<%@ Register Src="~/Admin/UserControls/Practice/PracticePhysicians.ascx" TagName="PracticePhysicians"
    TagPrefix="bvl" %>
<%@ Register Src="~/Admin/UserControls/Practice/PracticeSuppliers.ascx" TagName="PracticeSuppliers"
    TagPrefix="bvl" %>
<%@ Register Src="~/Admin/UserControls/Practice/PracticeProducts.ascx" TagName="PracticeProducts"
    TagPrefix="bvl" %>
<%@ Register Src="~/Admin/UserControls/Practice/PracticeCatalog.ascx" TagName="PracticeCatalog"
    TagPrefix="bvl" %>
<%@ Register Src="~/Admin/UserControls/Practice/PracticeUsers.ascx" TagName="PracticeUsers"
    TagPrefix="bvl" %>
<%@ Register Src="~/Admin/UserControls/PracticeLocation/PracticeLocationAddress.ascx"
    TagName="PracticeLocationAddress" TagPrefix="bvl" %>
<%@ Register Src="~/Admin/UserControls/PracticeLocation/PracticeLocationContacts.ascx"
    TagName="PracticeLocationContacts" TagPrefix="bvl" %>
<%@ Register Src="~/Admin/UserControls/PracticeLocation/PracticeLocationPhysicians.ascx"
    TagName="PracticeLocationPhysicians" TagPrefix="bvl" %>
<%@ Register Src="~/Admin/UserControls/PracticeLocation/PracticeLocationEmails.ascx"
    TagName="PracticeLocationEmails" TagPrefix="bvl" %>
<%@ Register Src="~/Admin/UserControls/PracticeLocation/PracticeLocationSupplierCodes.ascx"
    TagName="PracticeLocationSupplierCodes" TagPrefix="bvl" %>
<%@ Register Src="~/Admin/UserControls/PracticeLocation/PracticeLocationInventoryLevels.ascx"
    TagName="PracticeLocationInventoryLevels" TagPrefix="bvl" %>
<%@ Register Src="~/Admin/UserControls/PracticeLocation/PracticeLocationDispensementModifications.ascx"
    TagName="PracticeLocationDispensementModifications" TagPrefix="bvl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/tr/xhtml11/dtd/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <style type="text/css">
        html, body, form
        {
            height: 100%;
            margin: 0px;
            padding: 0px;
            overflow: hidden;
        }
    </style>
    <link href="~/Admin/Styles/AdminMasterStyle.css" rel="stylesheet" type="text/css" />
</head>
<body scroll="no">
    <form id="form1" runat="server">
    <div id="ParentDivElement" style="width: 100%; height: 100%;">
        <div id="headerBlock">
            <telerik:RadWindow ID="rwWelcomeWindow" runat="server" Modal="true" Enabled="false"
                Animation="Fade" VisibleOnPageLoad="true" NavigateUrl="WelcomeSetupWizard.aspx"
                Skin="Web20" Height="540px" Width="760px" AnimationDuration="2000" Behaviors="Close,Move"
                Title="Welcome" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
                <Scripts>
                    <asp:ScriptReference Path="~/Include/HelpSystem.js" />
                    <asp:ScriptReference Path="~/Include/Silverlight.js" />
                    <asp:ScriptReference Path="~/Include/SilverlightExpress.js" />
                    <asp:ScriptReference Path="~/Include/jquery/js/jquery-1.4.4.min.js" />
                </Scripts>
            </telerik:RadScriptManager>
            <telerik:RadStyleSheetManager ID="stylesheetmanager" runat="server">
            </telerik:RadStyleSheetManager>
            <telerik:RadCodeBlock ID="rcbHelp" runat="server" EnableViewState="true">
                <% 
                    var radPane2 = RadSplitter1.FindControl("RadPane2");
                    var radSlidingZone = radPane2.FindControl("RadSlidingZone1");
                    var rspHelp = radSlidingZone.FindControl("RSPHelp");
                %>
                <script type="text/javascript">
                    function GetHelpPane() {
                        try {
                            var slidingZone1 = $find("<%= radSlidingZone.ClientID %>");

                            var pane = slidingZone1.getPaneById("<%= rspHelp.ClientID==null ? String.Empty : rspHelp.ClientID %>");
                            //alert(pane==null);
                            return pane;
                        }
                        catch (ex) { /*alert("GetHelpPane() " + ex.message);*/ }
                    }

                    function ExpandHelpPane() {
                        try {
                            var pane = GetHelpPane();
                            if (pane) {

                                if (/*pane.get_expanded() == false &&*/pane.get_docked() == false) {

                                    var slidingZone1 = $find("<%= radSlidingZone.ClientID %>");
                                    slidingZone1.dockPane("<%= rspHelp.ClientID==null ? String.Empty : rspHelp.ClientID %>");

                                }
                            }
                        }
                        catch (ex) { /*"ExpandHelpPane() " + alert(ex.message);*/ }
                    }
                </script>
                <script type="text/javascript">
                    function showToolTip(element, context) {
                        var tooltipManager = $find("<%= RadToolTipManager1.ClientID %>");

                        if (!tooltipManager) return;

                        var tooltip = tooltipManager.getToolTipByElement(element);

                        if (!tooltip) {
                            tooltip = tooltipManager.createToolTip(element);

                            tooltip.set_value(context);
                        }

                        element.onmouseover = null;

                        try {
                            tooltip.show();
                        }
                        catch (ex) {
                            alert(ex.description);
                        }
                    }

                    function GetRelatedArticle(entityId) {
                        var load = window.open('../../Help.aspx?parentId=' + entityId.toString(), '', 'scrollbars=no,menubar=no,height=600,width=800,resizable=no,toolbar=no,location=yes,status=no');
                    }
                </script>
                <script type="text/javascript">
                    $(function () {
                        resizeMainContent();
                        $(window).resize(resizeMainContent);
                    });

                    function getMainContentHeight() {
                        var wh = $(window).height();
                        var hh = $('#headerBlock').height();
                        return (wh - hh);
                    }

                    function resizeMainContent() {
                        var h = getMainContentHeight();
                        $('#divMainContent').height(h);
                    }

                    function setInitialRadSplitterHeight(sender) {
                        sender.set_height(getMainContentHeight());
                    }
                </script>
            </telerik:RadCodeBlock>
            <telerik:RadToolTipManager ID="RadToolTipManager1" Width="300px" Height="100px" RelativeTo="Element"
                Position="BottomRight" runat="server" AnimationDuration="200">
                <WebServiceSettings Path="~/HelpSystem/ToolTipWebService.asmx" Method="GetToolTip" />
            </telerik:RadToolTipManager>
            <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" EnableHistory="false"
                EnableAJAX="true">
                <AjaxSettings>
                    <telerik:AjaxSetting AjaxControlID="aHome">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="lblNavigate" />
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                </AjaxSettings>
            </telerik:RadAjaxManager>
            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" InitialDelayTime="500" EnableAjaxSkinRendering="True" BackgroundPosition="Center" Skin="Breg" EnableEmbeddedSkins="False">
    <img src="App_Themes/Breg/images/Common/loading.gif" />
  </telerik:RadAjaxLoadingPanel>
            <table id="Table1" width="99%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" colspan="2">
                        <img id="MockUpHomeMouseOver_01" src="../../images/BregExpressSetupWizard.gif" width="185"
                            height="80" alt="" />
                    </td>
                </tr>
            </table>
            <asp:Label ID="lblScript" runat="server"></asp:Label>
            <bvu:MainMenu ID="ctlMainMenu" runat="server" />
            <div style="display: none">
                <telerik:RadToolTip ID="ctlIAmAHackToKeepTheGridStylesCorrectInCatalogTab1" runat="server"
                    Skin="Web20">
                </telerik:RadToolTip>
                <bvu:SearchToolbar ID="ctlIAmAHackToKeepTheToolbarStylesCorrectInCatalogTab" runat="server" />
            </div>
        </div>
        <div id="divMainContent" style="width: 100%; height: 100%;">
            <telerik:RadSplitter ID="RadSplitter1" Skin="Web20" VisibleDuringInit="false" runat="server"
                Orientation="Vertical" Height="100%" Width="100%" RegisterWithScriptManager="true"
                OnClientLoaded="setInitialRadSplitterHeight">
                <telerik:RadPane ID="LeftPane" runat="server" Width="300" Scrolling="Y">
                    <telerik:RadTabStrip ID="rtsWizard" runat="server" Orientation="VerticalLeft" ScrollButtonsPosition="Left"
                        SelectedIndex="0" CausesValidation="false" Skin="Web20" Width="270" Style="padding: 0 0 0 0;
                        margin-left: 0; margin-right: 0;">
                    </telerik:RadTabStrip>
                    <br />
                    <bvl:VLNavPanel ID="ctlNavPanel" runat="server" TabControlName="rtsWizard" MultiPageControlName="rmpWizard"
                        PageStatusControlName="ctlIndividualPageStatus"></bvl:VLNavPanel>
                </telerik:RadPane>
                <telerik:RadPane ID="CenterPane" runat="server" Scrolling="Both" Height="100%">
                    <bvl:IndividualPageStatus ID="ctlIndividualPageStatus" runat="server" />
                    <br />
                    <telerik:RadMultiPage ID="rmpWizard" runat="server" RenderSelectedPageOnly="true"
                        SelectedIndex="0" Height="100%" Width="100%" ScrollBars="None">
                        <bvw:VisionLitePageView ID="stepPracticeCreditApp" runat="server" Title="Practice Credit Application"
                            PrimaryControlName="ctlPracticeCreditApp">
                            <bvl:PracticeCreditApp ID="ctlPracticeCreditApp" runat="server" />
                        </bvw:VisionLitePageView>
                        <bvw:VisionLitePageView ID="stepPracticeBillingAddress" runat="server" Title="Practice Billing Address"
                            PrimaryControlName="ctlPracticeBillingAddress">
                            <bvl:PracticeBillingAddress ID="ctlPracticeBillingAddress" runat="server" />
                        </bvw:VisionLitePageView>
                        <bvw:VisionLitePageView ID="stepPracticeContact" runat="server" Title="Practice Contact"
                            PrimaryControlName="ctlPracticeContact">
                            <bvl:PracticeContact ID="ctlPracticeContact" runat="server"></bvl:PracticeContact>
                        </bvw:VisionLitePageView>
                        <bvw:VisionLitePageView ID="stepPracticePhysicians" runat="server" Title="Practice Physicians"
                            PrimaryControlName="ctlPracticePhysicians">
                            <bvl:PracticePhysicians ID="ctlPracticePhysicians" runat="server" />
                        </bvw:VisionLitePageView>
                        <bvw:VisionLitePageView ID="stepPracticeSuppliers" runat="server" Title="Practice Suppliers"
                            PrimaryControlName="ctlPracticeSuppliers">
                            <bvl:PracticeSuppliers ID="ctlPracticeSuppliers" runat="server" />
                        </bvw:VisionLitePageView>
                        <bvw:VisionLitePageView ID="stepPracticeProducts" runat="server" Title="Practice Products"
                            PrimaryControlName="ctlPracticeProducts">
                            <bvl:PracticeProducts ID="ctlPracticeProducts" runat="server" NeedsInitialBind="true" />
                        </bvw:VisionLitePageView>
                        <bvw:VisionLitePageView ID="stepPracticeCatalog" runat="server" EnableViewState="true"
                            Title="Practice Catalog" PrimaryControlName="ctlPracticeCatalog">
                            <bvl:PracticeCatalog ID="ctlPracticeCatalog" runat="server" NeedsInitialBind="true" />
                        </bvw:VisionLitePageView>
                        <bvw:VisionLitePageView ID="stepPracticeUsers" runat="server" Title="Practice Users"
                            PrimaryControlName="ctlPracticeUsers">
                            <bvl:PracticeUsers ID="ctlPracticeUsers" runat="server" NeedsInitialBind="true" />
                        </bvw:VisionLitePageView>
                        <bvw:VisionLitePageView ID="stepPracticeLocations" runat="server" Title="Practice Location(s)"
                            PrimaryControlName="ctlPracticeLocations">
                            <bvl:PracticeLocations ID="ctlPracticeLocations" runat="server" DisableLink="true">
                            </bvl:PracticeLocations>
                        </bvw:VisionLitePageView>
                        <bvw:VisionLitePageView ID="stepPracticeLocationAddresses" runat="server" Title="Practice Location - Addresses"
                            PrimaryControlName="ctlPracticeLocationAddressPanel">
                            <bvl:PracticeLocationPanel ID="ctlPracticeLocationAddressPanel" runat="server" PrimaryControlName="ctlPracticeLocationAddress">
                                <Content>
                                    <bvl:PracticeLocationAddress ID="ctlPracticeLocationAddress" runat="server" NeedsInitialBind="true" />
                                </Content>
                            </bvl:PracticeLocationPanel>
                        </bvw:VisionLitePageView>
                        <bvw:VisionLitePageView ID="stepPracticeLocationContacts" runat="server" Title="Practice Location - Contacts"
                            PrimaryControlName="ctlPracticeLocationContactPanel">
                            <bvl:PracticeLocationPanel ID="ctlPracticeLocationContactPanel" runat="server" PrimaryControlName="ctlPracticeLocationContacts">
                                <Content>
                                    <bvl:PracticeLocationContacts ID="ctlPracticeLocationContacts" runat="server" NeedsInitialBind="true" />
                                </Content>
                            </bvl:PracticeLocationPanel>
                        </bvw:VisionLitePageView>
                        <bvw:VisionLitePageView ID="stepPracticeLocationPhysicians" runat="server" Title="Practice Location - Physicians"
                            PrimaryControlName="ctlPracticeLocationPhysiciansPanel">
                            <bvl:PracticeLocationPanel ID="ctlPracticeLocationPhysiciansPanel" runat="server"
                                PrimaryControlName="ctlPracticeLocationPhysicians">
                                <Content>
                                    <bvl:PracticeLocationPhysicians ID="ctlPracticeLocationPhysicians" runat="server"
                                        NeedsInitialBind="true" />
                                </Content>
                            </bvl:PracticeLocationPanel>
                        </bvw:VisionLitePageView>
                        <bvw:VisionLitePageView ID="stepPracticeLocationEmails" runat="server" Title="Practice Location - Emails"
                            PrimaryControlName="ctlPracticeLocationEmailPanel">
                            <bvl:PracticeLocationPanel ID="ctlPracticeLocationEmailPanel" runat="server" PrimaryControlName="ctlPracticeLocationEmails">
                                <Content>
                                    <bvl:PracticeLocationEmails ID="ctlPracticeLocationEmails" runat="server" NeedsInitialBind="true" />
                                </Content>
                            </bvl:PracticeLocationPanel>
                        </bvw:VisionLitePageView>
                        <bvw:VisionLitePageView ID="stepPracticeLocationSupplierCodes" runat="server" Title="Practice Location - Supplier Acct#"
                            PrimaryControlName="ctlPracticeLocationSupplierCodePanel">
                            <bvl:PracticeLocationPanel ID="ctlPracticeLocationSupplierCodePanel" runat="server"
                                PrimaryControlName="ctlPracticeLocationSupplierCodes">
                                <Content>
                                    <bvl:PracticeLocationSupplierCodes ID="ctlPracticeLocationSupplierCodes" runat="server"
                                        NeedsInitialBind="true" />
                                </Content>
                            </bvl:PracticeLocationPanel>
                        </bvw:VisionLitePageView>
                        <bvw:VisionLitePageView ID="stepPracticeLocationInventoryLevels" runat="server" Title="Practice Location - Inventory Levels"
                            PrimaryControlName="ctlPracticeLocationInventoryLevelsPanel">
                            <bvl:PracticeLocationPanel ID="ctlPracticeLocationInventoryLevelsPanel" runat="server"
                                PrimaryControlName="ctlPracticeLocationInventoryLevels">
                                <Content>
                                    <bvl:PracticeLocationInventoryLevels ID="ctlPracticeLocationInventoryLevels" runat="server"
                                        NeedsInitialBind="true" />
                                </Content>
                            </bvl:PracticeLocationPanel>
                        </bvw:VisionLitePageView>
                        <bvw:VisionLitePageView ID="stepPracticeLocationDispensementModifications" runat="server"
                            Title="Practice Location - DispensementModifications" PrimaryControlName="ctlPracticeLocationDispensementModificationsPanel">
                            <bvl:PracticeLocationPanel ID="ctlPracticeLocationDispensementModificationsPanel"
                                runat="server" PrimaryControlName="ctlPracticeLocationDispensementModifications">
                                <Content>
                                    <bvl:PracticeLocationDispensementModifications ID="ctlPracticeLocationDispensementModifications"
                                        runat="server" NeedsInitialBind="true" />
                                </Content>
                            </bvl:PracticeLocationPanel>
                        </bvw:VisionLitePageView>
                    </telerik:RadMultiPage>
                </telerik:RadPane>
                <telerik:RadSplitBar ID="RadSplitBar1" RegisterWithScriptManager="true" runat="server" />
                <telerik:RadPane ID="RadPane2" runat="server" Width="22" Scrolling="None" ToolTip="Help"
                    RegisterWithScriptManager="true">
                    <telerik:RadSlidingZone ID="RadSlidingZone1" RegisterWithScriptManager="true" enableclientdebug="true"
                        runat="server" SlideDirection="Left" ClickToOpen="true" DockedPaneId="RSPStatus">
                        <telerik:RadSlidingPane ID="RSPHelp" Title="Help" Scrolling="None" RegisterWithScriptManager="true"
                            runat="server" Style="height: 100%;" Width="350px" IconUrl="~/images/info16.png"
                            SkinID="WebBlue">
                            <style type="text/css">
                                #article
                                {
                                    visibility: hidden;
                                    background-color: White;
                                    position: absolute;
                                    top: 50px;
                                    left: 0px;
                                    width: 100%;
                                    height: 100%;
                                    z-index: 100;
                                    font-size: 12px;
                                    font-family: Arial, Helvetica, sans-serif;
                                    color: Black;
                                }
                            </style>
                            <div style="position: relative; height: 100%; width: 100%;">
                                <div id="silverlightControlHost" style="height: 100%; width: 100%;">
                                    <object id="HelpControl" data="data:application/x-silverlight-2," type="application/x-silverlight-2"
                                        style="height: 100%; width: 100%;">
                                        <param name="windowless" value="true" />
                                        <param name="source" value="../../ClientBin/HelpSystem.xap" />
                                        <param name="onError" value="onSilverlightError" />
                                        <param name="background" value="white" />
                                        <param name="minRuntimeVersion" value="3.0.40624.0" />
                                        <param name="autoUpgrade" value="true" />
                                        <param name="initParams" value="<%= string.Format("UserRole={0}", Session["UserID"])%>,IsFullMode=false,<%= string.Format("AdminNode={0}", System.Configuration.ConfigurationManager.AppSettings["HelpEntityAdminTopNode"])%>,<%= string.Format("UserNode={0}", System.Configuration.ConfigurationManager.AppSettings["HelpEntityUserTopNode"])%>" />
                                        <a href="http://go.microsoft.com/fwlink/?LinkID=149156&v=3.0.40624.0" style="text-decoration: none">
                                            <img src="http://go.microsoft.com/fwlink/?LinkId=108181" alt="Get Microsoft Silverlight"
                                                style="border-style: none" />
                                        </a>
                                    </object>
                                    <iframe id="_sl_historyFrame" style="visibility: hidden; height: 0px; width: 0px;
                                        border: 0px"></iframe>
                                </div>
                                <div id="article">
                                    This is content
                                </div>
                            </div>
                        </telerik:RadSlidingPane>
                        <telerik:RadSlidingPane ID="RSPStatus" Title="Setup Status" Scrolling="Both" RegisterWithScriptManager="true"
                            runat="server" Style="height: 100%;" Width="350px" IconUrl="~/images/info16.png"
                            SkinID="WebBlue">
                            <bvl:StatusPage ID="ctlStatusPage" runat="server" />
                        </telerik:RadSlidingPane>
                    </telerik:RadSlidingZone>
                </telerik:RadPane>
            </telerik:RadSplitter>
        </div>
    </div>
    </form>
</body>
</html>
