﻿using System;
using System.Linq;
//using ClassLibrary.DAL;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;


namespace BregVision.Admin.PracticeSetup
{

    public partial class StatusPage : PracticeSetupBase, IVisionWizardControl
    {
        /// <summary>
        /// IVisionWizardControl Interface.
        /// </summary>
        public bool NeedsInitialBind { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //Practice Status
            DataTable dtPractice = BLL.Practice.Practice.GetPracticeSetupStatus(practiceID);

            grdPractice.DataSource = dtPractice;
            grdPractice.DataBind();

            //Practice Location Status
            DataTable tblLocations = BLL.PracticeLocation.PracticeLocation.GetPracticeLocationSetupStatus(practiceID);
            grdPracticeLocation.DataSource = tblLocations;
            grdPracticeLocation.DataBind();

        }



        protected void grdPractice_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                foreach (DataColumn column in ((DataRowView)e.Row.DataItem).Row.Table.Columns)
                {
                    if (column.ColumnName != "Page")
                    {
                        InsertStatusImage(e, column);

                        if (e.Row.DataItemIndex == 0)
                        {
                            ((GridView)sender).HeaderRow.Cells[column.Ordinal].Text = column.Caption;
                        }
                    }
                }

            }

        }

        private static void InsertStatusImage(GridViewRowEventArgs e, DataColumn column)
        {
            string columnName = column.ColumnName;
            string columnValue = DataBinder.Eval(e.Row.DataItem, columnName).ToString();
            //string imageName = columnValue == "Y" ? "Update" : "Cancel";
            string imageName = columnValue == "Y" ? "GreenLight" : "RedLight";
            string imagePath = "~/images/{0}.gif";
            Image img = new Image();
            img.ImageUrl = string.Format(imagePath, imageName);
            e.Row.Cells[column.Ordinal].Text = "";
            e.Row.Cells[column.Ordinal].Controls.Add(img);
        }


        /// <summary>
        /// IVisionWizardControl interface save.  Called by wizard control on tab change
        /// </summary>
        public void Save()
        {
            //Do Nothing
        }
    }
}