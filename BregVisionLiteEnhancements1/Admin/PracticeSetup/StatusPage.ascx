﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StatusPage.ascx.cs" Inherits="BregVision.Admin.PracticeSetup.StatusPage" %>


<table>
    <tr>
        <td valign="bottom">
            <img id="Img1"  src="../../images/SetupStatus.gif" alt="" border="0" /></td>
        <td valign="bottom">
            <img id="lblPracticeSetup"  src="../../images/PracticeSetup.jpg" alt="" border="0" /></td>
        <td>
            <asp:GridView ID="grdPractice" runat="server" AutoGenerateColumns="true" 
                OnRowDataBound="grdPractice_RowDataBound" 
                 GridLines="None" ShowHeader="False">
                <RowStyle BorderStyle="None" Font-Size="XX-Small" />
                <HeaderStyle Font-Bold="True" Font-Size="XX-Small" />
                <AlternatingRowStyle Font-Size="X-Small" />
            </asp:GridView>
        </td>
        <td>&nbsp
        </td>
        <td valign="bottom">
            <img id="lblLocationSetup"  src="../../images/LocationSetup.jpg" alt="" /></td>
        <td>
            <asp:GridView ID="grdPracticeLocation" runat="server" 
                AutoGenerateColumns="true" OnRowDataBound="grdPractice_RowDataBound" 
                GridLines="None" CellSpacing="2">
                <RowStyle Font-Size="XX-Small" />
                <HeaderStyle Font-Bold="True" Font-Size="XX-Small" />
            </asp:GridView>
    </td>

</tr>

</table>