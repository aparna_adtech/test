﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BregVision.Admin.PracticeSetup
{
    [PersistChildren(false), ParseChildren(true)] //might need to reverse these
    public partial class PracticeLocationPanel : System.Web.UI.UserControl, IVisionWizardControl
    {
        private ITemplate content = null;
        public IVisionWizardControl PrimaryControl = null;

        public bool NeedsInitialBind { get; set; }

        public string PrimaryControlName { get; set; }

        protected void Page_Init()
        {
            if (content != null)
            {
                ContentClass container = new ContentClass();
                Content.InstantiateIn(container);
                
                
                plPracticeLocationControl.Controls.Add(container);
                if (plPracticeLocationControl.Controls.Count == 1)
                {
                    if (plPracticeLocationControl.Controls[0].Controls.Count > 0) //This is the placeholder control
                    {
                        IVisionWizardControl primaryControlFound = plPracticeLocationControl.Controls[0].FindControl(PrimaryControlName) as IVisionWizardControl;
                        if (primaryControlFound != null)
                        {
                            PrimaryControl = primaryControlFound;
                        }
                    }
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();
        }

        private void InitializeAjaxSettings()
        {
            if (PrimaryControl != null)
            {
                Control control = plPracticeLocationControl.Controls[0].FindControl(PrimaryControlName);
                ((Admin.PracticeSetup.PracticeSetup)Page).RadAjaxManagerMain.AjaxSettings.AddAjaxSetting(ctlVLLocationNavPanel, control);
            }
        }

        [TemplateContainer(typeof(Content)), TemplateInstance(TemplateInstance.Single), PersistenceMode(PersistenceMode.InnerProperty)]
        public ITemplate Content
        {
            get { return this.content; }
            set { this.content = value; }
        }

        public void Save()
        {
            if (PrimaryControl != null)
            {
                PrimaryControl.Save();
            }
        }



    }

    public class ContentClass : Control, INamingContainer 
    { 
    }

}