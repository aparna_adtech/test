﻿
namespace BregVision.Admin.PracticeSetup
{
    public interface IVisionWizardControl
    {
        void Save();
        bool NeedsInitialBind { get; set; }
    }
}
