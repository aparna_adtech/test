﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BregVision.Admin.PracticeSetup
{
    public class VisionLitePageView : Telerik.Web.UI.RadPageView
    {
        public string PrimaryControlName { get; set; }

        public string Title { get; set; }

        public IVisionWizardControl PrimaryControl
        {
            get
            {
                if (this.PrimaryControlName != null)
                {
                    return this.FindControl(PrimaryControlName) as IVisionWizardControl;
                }
                return null;
            }
        }
    }
}
