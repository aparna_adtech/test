﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WelcomeSetupWizard.aspx.cs" Inherits="BregVision.Admin.PracticeSetup.WelcomeSetupWizard" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
         <style type="text/css"> 


.style1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	text-align: left;
}
.style2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight:bold;
	text-align: left;
}
.style3 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight:bold;
	text-align: left;
}

.addToCart 
{
    height: 48px;
    width: 224px;
    background-image: url(Images/DefaultRotaterBaseImageRight.gif);    
}
 .Copyright
 {
    text-align: center;
    font-family: Verdana, Arial;
    font-size: 8pt;
    color: Black;
    /*background-color: #F4F8FA;
    height: 10px;*/
 }
.jewelryImage 
{
    height: 251px;
    width: 153px;    
    float: left;
}
.jewelryRightPane 
{
    height: 251px;
    width: 224px;
    float: left;
}
.jewelryDescription 
{
    height: 168px;
    width: 600px;
    background-image: url(Images/DefaultRotaterBaseImageRight.gif);
}
.jewelryDescription_Text 
{
    font: normal 14px Arial, Verdana, Helvetica;
    color: #575759;
    margin-left: 10px;
    padding-top: 10px;
    margin-right: 5px;
}
.jewelryName
{
    height: 35px;
    width: 224px;
}

</style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
       <img id="MockUpHomeMouseOver_01" runat="server"  src="../../images/SiteMasterHeader/MockUpHomeMouseOver_01.gif" width="253" height="91" alt="" />
       <p class="style3">Welcome to the BREG Vision Express Setup Wizard</p> 
       
       <p class="style1">The Wizard will guide you through the setup of your Vision Express site.  The goal of the Wizard is to ensure your site is complete to obtain the most valuable experience possible.


</p> 
<table>
    <tr>
        <td>
        <img id="Img1" runat="server" src="../../images/stoplightsmall.jpg" /></td>
        <td class="style1">You will notice a a series of red and green lights at the top of the web page and on each page related to the section where you are working. These lights represent
         if a particular area of your website has been completed. As you visit and complete each step, the lights will change to green or red to indicate if you have entered enought information 
         for a particular area to continue. <br /><br />

         Some sections you are not required to do anything more than visit the page to understand what you need to do.   Others you will be required to enter the basic information needed to make sure your site is functioning correctly.
        <br /><br />
        Once you have complete each step you will then be able to access and use your Vision Express Site.
        </td>
    </tr>
</table>
<table>
    <tr>
        <td class="style1">
        Remember, if  you have any questions, there is an extensive help system at your fingertips. Just point your mouse over any of the blue icons
        <img id="Img2" runat="server" src="../../images/info20.jpg" />
         to get more information. If you need a more detailed explanations to your problem, simply click the
         <img id="Img3" runat="server" src="../../images/info20.jpg" />
           button or type your questions or keywords in the help area
          on the right side of the screen.
        </td>

</tr>

</table>
 <p class="style2">Thanks for subscribing to Vision Express!</p> 
    </div>
    </form>
</body>
</html>
