﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace BregVision.Admin.PracticeSetup.Controls
{
    public partial class IndividualPageStatus : Bases.UserControlBase
    {
        public VisionLitePageView CurrentPageView { get; set; }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            string currentPage = "";
            divPageStatus.Visible = false;

            if (CurrentPageView != null)
            {
                if (CurrentPageView.PrimaryControl.GetType().ToString() == "ASP.admin_practicesetup_practicelocationpanel_ascx")
                {
                    PracticeLocationPanel practiceLocationPanel = CurrentPageView.PrimaryControl as PracticeLocationPanel;
                    currentPage = practiceLocationPanel.PrimaryControl.GetType().ToString();
                    DataRow statusRow = BLL.PracticeLocation.PracticeLocation.GetSetupPageStatus(currentPage, practiceID, practiceLocationID);
                    SetPageStatus(statusRow);
                }
                else
                {
                    currentPage = CurrentPageView.PrimaryControl.GetType().ToString();
                    DataRow statusRow = BLL.Practice.Practice.GetSetupPageStatus(currentPage, practiceID);
                    SetPageStatus(statusRow);
 
                }
            }

        }

        private void SetPageStatus(DataRow statusRow)
        {
            if (statusRow != null)
            {
                lblPageName.Text = statusRow["Page"].ToString();
                string PageStatus = statusRow["PageStatus"].ToString();
                string imageName = PageStatus == "Y" ? "Update" : "Cancel";
                string imagePath = "~/RadControls/Grid/Skins/Outlook/{0}.gif";
                imgStatus.ImageUrl = string.Format(imagePath, imageName);
                divPageStatus.Visible = true;
            }
        }




    }
}