﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IndividualPageStatus.ascx.cs" Inherits="BregVision.Admin.PracticeSetup.Controls.IndividualPageStatus" %>
<div id="divPageStatus" runat="server" visible="false">
    <span><asp:Image id="imgStatus" runat="server" />&nbsp;Setup Status for <asp:Label ID="lblPageName" runat="server" /></span>
</div>
