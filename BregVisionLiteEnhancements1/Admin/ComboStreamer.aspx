﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ComboStreamer.aspx.cs" Inherits="BregVision.Admin.ComboStreamer" %>
<%@ Register Assembly="RadComboBox.Net2" Namespace="Telerik.WebControls" TagPrefix="radC" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <radC:RadComboBox  ID="cboPhysicianName" Text='<%# bind("PhysicianName") %>' runat="server" Skin="WebBlue" selectedvalue='<%# bind("PhysicianName") %>'
                                       DataTextField="PhysicianName" DataValueField="PhysicianID" 
                                        DataSource ='<%# dsPopulatePhysicians() %>' 
                                                                                 
                                         OnItemsRequested="cboPhysicianName_ItemsRequested"  EnableLoadOnDemand="true"
                                     Font-Size="X-Small"  >
                                     
                                       </radC:RadComboBox>
    </div>
    </form>
</body>
</html>
