﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PracticeAdmin.Master"
  CodeBehind="barcode.aspx.cs" Inherits="BregVision.Admin.BarCode" %>

<%@ Register Src="~/Admin/UserControls/Practice/PracticeBarCode.ascx" TagName="PracticeBarCode"
  TagPrefix="bvl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true"
  Visible="true">
  <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" EnableHistory="false"
    EnableAJAX="True">
  </telerik:RadAjaxManager>
  <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" InitialDelayTime="500" EnableAjaxSkinRendering="True" BackgroundPosition="Center" Skin="Breg" EnableEmbeddedSkins="False">
    <img src="../App_Themes/Breg/images/Common/loading.gif" />
  </telerik:RadAjaxLoadingPanel>
  <bvl:PracticeBarCode ID="ctlPracticeBarCode" runat="server" />
</asp:Content>
