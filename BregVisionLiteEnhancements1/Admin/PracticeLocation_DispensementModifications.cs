using System;
using System.Web.UI;


namespace BregVision.Admin
{
    public partial class PracticeLocation_DispensementModifications : System.Web.UI.Page
    {
        public Telerik.Web.UI.RadAjaxLoadingPanel DefaultLoadingPanel
        {
            get { return RadAjaxLoadingPanel1; }
        }

        public System.Web.UI.WebControls.Label HeaderLabel
        {
            get { return lblHeader; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ((BregVision.MasterPages.PracticeLocationAdmin)Page.Master).SelectCurrentTab("TabLocationDispensementModifications");
        }
    }
}
