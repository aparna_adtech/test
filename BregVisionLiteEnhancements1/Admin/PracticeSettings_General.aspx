﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PracticeSettingsAdmin.Master" Inherits="PracticeSettings_General" CodeBehind="PracticeSettings_General.aspx.cs" %>

<%@ Register Src="~/Admin/UserControls/PracticeSettings/PracticeSettingsGeneral.ascx" TagName="PracticeSettingsGeneral" TagPrefix="bvl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true" Visible="true">
  <bvl:PracticeSettingsGeneral ID="ctlPracticeSettingsGeneral" runat="server" />
</asp:Content>
