﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PracticeAdmin.Master"
     CodeBehind="Practice_UserAdmin.aspx.cs" Inherits="BregVision.Admin.Practice_UserAdmin" %>

<%@ Register Src="~/Admin/UserControls/Practice/PracticeUsers.ascx" TagName="PracticeUsers"
    TagPrefix="bvl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true"
    Visible="true">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    </telerik:RadAjaxManager>

<bvl:PracticeUsers ID="ctlPracticeUsers" runat="server" />

</asp:Content>
