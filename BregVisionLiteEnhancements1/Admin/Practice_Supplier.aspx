<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PracticeAdmin.Master"
  Inherits="Practice_Supplier" CodeBehind="Practice_Supplier.aspx.cs" %>
<%@ Register Src="~/Admin/UserControls/Practice/PracticeSuppliers.ascx" TagName="PracticeSuppliers" TagPrefix="bvl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true" Visible="true">

  <bvl:PracticeSuppliers ID="ctlPracticeSuppliers" runat="server" />

</asp:Content>
