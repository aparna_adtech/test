﻿using ClassLibrary.DAL;
using System;
using System.Linq;
using System.Web.Services;
using System.Web.UI;

public partial class PracticeSettings_General : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		((BregVision.MasterPages.PracticeSettingsAdmin)Page.Master).SelectCurrentTab("TabSettingsGeneral");
	}

    [WebMethod]
    public static bool UpdatePractice(string practiceId, string isDisplayBillChargeOnReceipt, string isLateralityRequired, string generalLedgerNumber, string bcsSubInventoryId)
    {
        try
        {
            using (var visionDb = VisionDataContext.GetVisionDataContext())
            {
                using (visionDb)
                {
                    var updatePractice = visionDb.Practices.Single(practice => practice.PracticeID == int.Parse(practiceId));
                    updatePractice.IsDisplayBillChargeOnReceipt = bool.Parse(isDisplayBillChargeOnReceipt);
                    updatePractice.IsLateralityRequired = bool.Parse(isLateralityRequired);
                    updatePractice.GeneralLedgerNumber = generalLedgerNumber;
                    updatePractice.BCSSubInventoryID = bcsSubInventoryId;

                    visionDb.SubmitChanges();
                }
            }

            return true;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

}
