﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PracticeLocationAdmin.Master" CodeBehind="PracticeLocation_InventoryCount.aspx.cs" Inherits="BregVision.Admin.PracticeLocation_InventoryCount" %>

<%@ Register Src="~/UserControls/General/RecordsPerPage.ascx" TagPrefix="bvl" TagName="RecordsPerPage" %>
<%@ Register Src="~/Admin/UserControls/PracticeLocation/PracticeLocationInventoryCount.ascx" TagPrefix="bvl" TagName="PracticeLocationInventoryCount" %>
<%@ Register Src="~/BregAdmin/UserControls/ConsignmentOrder.ascx" TagPrefix="bvl" TagName="ConsignmentOrder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true"
  Visible="true">
  <telerik:RadToolTipManager ID="RadToolTipManager1" Width="300px" Height="100px" RelativeTo="Element"
    Position="BottomRight" runat="server" AnimationDuration="200">
    <WebServiceSettings Path="~/HelpSystem/ToolTipWebService.asmx" Method="GetToolTip" />
  </telerik:RadToolTipManager>
  <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server"></telerik:RadAjaxManager>
  <div class="flex-row-align-end">
    <bvl:RecordsPerPage ID="ctlRecordsPerPage" runat="server" />
  </div>
  <bvl:PracticeLocationInventoryCount ID="ctlPracticeLocationInventoryCount" runat="server"></bvl:PracticeLocationInventoryCount>
  <bvl:ConsignmentOrder ID="ctlConsignmentOrder" runat="server"></bvl:ConsignmentOrder>
</asp:Content>
