using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Reporting.WebForms;
using Telerik.WebControls;

namespace BregVision.Admin
{
    public class Column
    {
        public int ColumnID { get; set; }
        public string Name { get; set; }
    }
    
    public partial class PracticeLocation_Superbill : System.Web.UI.Page
    {
        public DataTable dtHCPCs = null;

        int practiceID;
	    string practiceName;
	    int practiceLocationID;
	    string practiceLocationName;
	    int userID;
        //private bool _RebindInventoryGrid = false;

        private UIHelper _PracticeCatalog_UIHelper; //mws
        private DataSet _dsPracticeCatalog = null;

        private UIHelper _Inventory_UIHelper;
        //private DataSet _dsInventory = null;

        //MWS use these for practice catalog
        string SearchCriteria = "";
        string SearchText = "";
        //Boolean IsActive = true;

        //MWS use these for Inventory -- these are not used yet.  
        //string InvSearchCriteria = "";
        //string InvSearchText = "";
        //Boolean InvIsActive = true;

        int intPCItems = 0;

        //MWS - these 2 grid data properties don't seem to be used
        public DataTable Grid1Data
        {
            get { return (DataTable)Session["_grid1Data"]; }
            set { Session["_grid1Data"] = value; }
        }
        public DataTable Grid2Data
        {
            get { return (DataTable)Session["_grid2Data"]; }
            set { Session["_grid2Data"] = value; }
        }
    

    protected void Page_Load(object sender, EventArgs e)
    {
        _PracticeCatalog_UIHelper = new UIHelper(ViewState, grdPracticeCatalog, true); //mws
        _PracticeCatalog_UIHelper.SetGridLabel("", false, lblMCStatus, true);//set the default text on the label

        _Inventory_UIHelper = new UIHelper(ViewState, grdSuperbill, true); //mws
        //_Inventory_UIHelper.SetGridLabel("", false, lblPCStatus, true);//set the default text on the label

        
        if (!Page.IsPostBack)
        {
            GetSessionVariables(out userID, out practiceID, out practiceName, out practiceLocationID, out practiceLocationName);

            if (Session["RecordsDisplayed"] == null)
            {
                Session["RecordsDisplayed"] = 10;
            }

            //txtRecordsDisplayed.Text = Session["RecordsDisplayed"].ToString();
            grdSuperbill.PageSize = Convert.ToInt16(Session["RecordsDisplayed"].ToString());
            grdPracticeCatalog.PageSize = Convert.ToInt16(Session["RecordsDisplayed"].ToString());

            _PracticeCatalog_UIHelper.HasExpanded = true; //mws
            _Inventory_UIHelper.HasExpanded = true; //mws

        }
        else
        {
            practiceLocationID = (Session["PracticeLocationID"] != null) ? Convert.ToInt32(Session["PracticeLocationID"].ToString()) : -1;
            userID = (Session["UserID"] != null) ? Convert.ToInt32(Session["UserID"].ToString()) : -1;
        }

        dtHCPCs = getSuperbillHCPCS(practiceLocationID).Tables[0];
        
        UserControl ucTabStripLocation = (UserControl)Page.Master.FindControl("PracticeAdminTabStripLocation1");
        ucTabStripLocation.Visible = true;

        Telerik.WebControls.RadTabStrip tabStripLocation = (Telerik.WebControls.RadTabStrip)ucTabStripLocation.FindControl("rmpLocation");
        Telerik.WebControls.Tab tabSelected = (Telerik.WebControls.Tab)tabStripLocation.FindControl("TabLocationSuperbill");
        tabSelected.Selected = true;
        tabSelected.Font.Bold = true;
        tabSelected.Font.Italic = true;
        tabSelected.Text = tabSelected.Text.ToUpper().ToString();
        tabSelected.ForeColor = System.Drawing.Color.Blue;

        UserControl ucUserLoginInfo = (UserControl)Page.Master.FindControl("PracticeAdminUserPractice1");
        Label lblPractice = (Label)ucUserLoginInfo.FindControl("lblLocation");
        lblPractice.Visible = true;

      //Set up the textbox so the search button is click on 'enter' key on 
        Telerik.Web.UI.RadTextBox tbFilter = (Telerik.Web.UI.RadTextBox)rttbFilter.FindControl("tbFilter");
        RadToolbarToggleButton rtbFilter = (RadToolbarToggleButton)RadToolbar1.FindControl("rtbFilter");
        UIHelper.SetEnterPressTarget(tbFilter, rtbFilter);

        //Set up the textbox so the search button is click on 'enter' key on 
        //TextBox tbFilter1 = (TextBox)rttbFilter2.FindControl("tbFilter");
        //RadToolbarToggleButton rtbFilter1 = (RadToolbarToggleButton)RadToolbar2.FindControl("RadToolbarToggleButton1");
        //UIHelper.SetEnterPressTarget(tbFilter1, rtbFilter1);

}        
        
        
        
        protected void InitializeComponent()
        {

            grdPracticeCatalog.PageIndexChanged += new GridPageChangedEventHandler(grdPracticeCatalog_PageIndexChanged);
            this.Load += new System.EventHandler(this.Page_Load);
            this.PreRender += new EventHandler(this.Page_PreRender);

        }

        protected void GetSessionVariables(out int userID, out int practiceID, out string practiceName,
                    out int practiceLocationID, out string practiceLocationName)
        {
            userID = (Session["UserID"] != null) ? Convert.ToInt32(Session["UserID"].ToString()) : -1;
            practiceID = (Session["PracticeID"] != null) ? Convert.ToInt32(Session["PracticeID"].ToString()) : -1;
            practiceLocationID = (Session["PracticeLocationID"] != null) ? Convert.ToInt32(Session["PracticeLocationID"].ToString()) : -1;
            practiceName = (Session["PracticeName"] != null) ? Session["PracticeName"].ToString() : "";
            practiceLocationName = (Session["PracticeLocationName"] != null) ? Session["PracticeLocationName"].ToString() : "";
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {   
            string uploadPath = ConfigurationManager.AppSettings["SuperBillLogoLocation"];
            string currentPath = Server.MapPath(uploadPath);
            practiceLocationID = (Session["PracticeLocationID"] != null) ? Convert.ToInt32(Session["PracticeLocationID"].ToString()) : -1;
            string newFilename = string.Format("PracticeLogo{0}.jpg", practiceLocationID.ToString());
            currentPath += newFilename;
            if (File.Exists(currentPath))
            {
                imgPracticeLogo.ImageUrl = uploadPath + newFilename;
                imgPracticeLogo.Visible = true;
            }
        }

        protected void RadToolbar1_OnClick(object sender, Telerik.WebControls.RadToolbarClickEventArgs e)
        {
            if (e.Button.CommandName == "Browse")
            {
                SearchCriteria = "Browse";
                ViewState["SearchCriteria"] = SearchCriteria;

                SearchText = "";
                ViewState["SearchText"] = SearchText;


                grdPracticeCatalog.DataSource = GetPracticeCatalogSuppliers(Convert.ToInt32(Session["PracticeLocationID"]));
                grdPracticeCatalog.Rebind();

                _PracticeCatalog_UIHelper.HasExpanded = true; //mws
            }

            if (e.Button.CommandName == "Search")
            {
                _PracticeCatalog_UIHelper.IsSearch = true;
                Telerik.Web.UI.RadTextBox searchText = (Telerik.Web.UI.RadTextBox)rttbFilter.FindControl("tbFilter");
                ViewState["SearchText"] = searchText.Text.ToString();

                RadComboBox SearchCriteria = (RadComboBox)rttbSearchFilter.FindControl("cboSearchFilter");
                ViewState["SearchCriteria"] = SearchCriteria.Value.ToString();

                grdPracticeCatalog.Rebind();

                _PracticeCatalog_UIHelper.HasExpanded = false; //mws
                _Inventory_UIHelper.HasExpanded = false; //mws  I want the inventory table to close and disable on empty rows too
            }
        }

        protected void grdPracticeCatalog_PageIndexChanged(object source, Telerik.WebControls.GridPageChangedEventArgs e)
        {
            //this.grdPracticeCatalog.CurrentPageIndex = e.NewPageIndex;

            if (ViewState["SearchCriteria"] == null ||
                ViewState["SearchCriteria"].ToString() == "Browse" ||
                ViewState["SearchCriteria"].ToString() == "")
            {
                _PracticeCatalog_UIHelper.HasExpanded = true; //mws
            }

            else
            {
                _PracticeCatalog_UIHelper.HasExpanded = false; //mws
            }

        }

        public DataSet GetMasterCatalogSuppliers()
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetMasterCatalogSuppliers");

            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;


        }

        public DataSet GetPracticeCatalogSuppliers(Int32 practiceID)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetPracticeCatalogSupplierBrands");

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;


        }

        protected void grdPracticeCatalog_DetailTableDataBind(object source, Telerik.WebControls.GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;

            if (ViewState["SearchCriteria"] != null)
            {
                SearchCriteria = (string)ViewState["SearchCriteria"];
            }

            if (ViewState["SearchText"] != null)
            {
                SearchText = (string)ViewState["SearchText"];
            }
            e.DetailTableView.DataSource = GetPracticeCatalogDataBySupplier(Convert.ToInt32(Session["practiceid"]), SearchCriteria, SearchText);

        }

        protected void grdSuperbill_DetailTableDataBind(object source, Telerik.WebControls.GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;

            if (ViewState["SearchCriteria"] != null)
            {
                SearchCriteria = (string)ViewState["SearchCriteria"];
            }
            
            if (ViewState["SearchText"] != null)
            {
                SearchText = (string)ViewState["SearchText"];
            }

            //////switch (e.DetailTableView.Name)
            //////{
            //////    case "grdtvCategory":
            //////        {
            //////            e.DetailTableView.DataSource = getSuperbillCategories(Convert.ToInt32(Session["practiceLocationid"]));
            //////            break;
            //////        }
            //////    case "grdtvSuperbill":
            //////        {
            //////            e.DetailTableView.DataSource = getSuperbillData(Convert.ToInt32(Session["practiceLocationid"]));
            //////            //e.DetailTableView.EditMode = GridEditMode.InPlace;
            //////            break;
            //////        }
            //////}
        }

        public DataSet GetMasterCatalogDataBySupplier()
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetMasterCatalogProductInfo");

            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;

        }

        public DataSet GetPracticeCatalogDataBySupplier(Int32 practiceID, string SearchCriteria, string SearchText)
        {
            DataSet ds = null;
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetPracticeCatalogProductInfoMCTOPC");
            db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
            db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);
            
            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);

            if (_dsPracticeCatalog == null)
            {
                ds = db.ExecuteDataSet(dbCommand);
                _dsPracticeCatalog = ds;
                _PracticeCatalog_UIHelper.SetRecordCount(ds, "ShortName");
                _PracticeCatalog_UIHelper.SetGridLabel(SearchCriteria, Page.IsPostBack, lblMCStatus); //lblMCStatus is actually Practice Catalog
            }
            else
            {
                ds = _dsPracticeCatalog;
            }

            return ds;

        }

        protected void grdSuperbill_NeedDataSource(object source, Telerik.WebControls.GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable)
            {
                grdSuperbill.DataSource = getSuperbill(Convert.ToInt32(Session["PracticeLocationID"]));
            }
            
            grdSuperbill.MasterTableView.DetailTables[0].DataSource= getSuperbillCategories(Convert.ToInt32(Session["practiceLocationid"]));

            grdSuperbill.MasterTableView.DetailTables[0].DetailTables[0].DataSource = getSuperbillData(Convert.ToInt32(Session["practiceLocationid"]));

            grdSuperbill.MasterTableView.DetailTables[0].DetailTables[0].DetailTables[0].DataSource = getSuperbillAddOnData(Convert.ToInt32(Session["practiceLocationid"]));
        }

        protected void grdSuperbill_OnItemDataBound(object sender, Telerik.WebControls.GridItemEventArgs e)
        {
            int practiceLocationID = Convert.ToInt32(Session["PracticeLocationID"]);
            int practiceCatalogProductID = 0;
            string hCPCs = "";
            RadComboBox rcbHCPCs = null;

            //The problem here is I can't find the dropdown / combo box in the grid 
            if (e.Item is GridEditableItem && (e.Item as GridEditableItem).IsInEditMode)
            {
                GridEditableItem editedItem = e.Item as GridEditableItem;

                if (editedItem.OwnerTableView.Name == "grdtvSuperbill" || editedItem.OwnerTableView.Name == "grdtvAddOn")
                {
                    if (editedItem.OwnerTableView.Name == "grdtvSuperbill")
                    {
                        practiceCatalogProductID = Convert.ToInt32(editedItem["PracticeCatalogProductID"].Text);
                        hCPCs = editedItem["HCPCs"].Text;
                    }
                    else if (editedItem.OwnerTableView.Name == "grdtvAddOn")
                    {
                        //since this is a user defined editable area in the aspx, I need to find the pcpid by find control method
                        practiceCatalogProductID = Convert.ToInt32(editedItem.OwnerTableView.ParentItem["PracticeCatalogProductID"].Text);
                    }
                    //sdsHCPCLookup.FilterExpression = string.Format("PracticeCatalogProductID={0}", practiceCatalogProductID);
                    dtHCPCs.DefaultView.RowFilter = string.Format("PracticeCatalogProductID={0}", practiceCatalogProductID);

                    if(editedItem.OwnerTableView.Name == "grdtvSuperbill" )
                    {
                        rcbHCPCs = editedItem.FindControl("ddlHCPCs") as RadComboBox;
                    }
                    else if(editedItem.OwnerTableView.Name == "grdtvAddOn" )
                    {
                        rcbHCPCs = editedItem.FindControl("ddlAddOnHCPCs") as RadComboBox;
                    }

                    rcbHCPCs.DataSource = dtHCPCs.DefaultView;
                    rcbHCPCs.DataTextField = "HCPCsCode";
                    rcbHCPCs.DataValueField = "HCPCsCode";
                    rcbHCPCs.DataBind();
                    rcbHCPCs.Text = hCPCs;


                }

            }                
                

        }

        protected void grdSuperbill_ItemCommand(object source, GridCommandEventArgs e)
        {
        }

        protected void grdSuperbill_OnEditCommand(object source, GridCommandEventArgs e)
        {
            //int practiceLocationID = Convert.ToInt32(Session["PracticeLocationID"]);
            //int practiceCatalogProductID = 0;

            //GridEditableItem editedItem = e.Item as GridEditableItem;

            //if (editedItem.OwnerTableView.Name == "grdtvSuperbill")
            //{

            //    practiceCatalogProductID = Convert.ToInt32(editedItem["PracticeCatalogProductID"].Text);

            //    dtHCPCs.DefaultView.RowFilter = string.Format("PracticeCatalogProductID={0}", practiceCatalogProductID);

            //    RadComboBox rcbHCPCs = editedItem.FindControl("ddlHCPCs") as RadComboBox;

            //    rcbHCPCs.DataSource = dtHCPCs.DefaultView;
            //    rcbHCPCs.DataTextField = "HCPCsCode";
            //    rcbHCPCs.DataValueField = "HCPCsCode";
            //    rcbHCPCs.DataBind();

            //}
        }

        protected void grdSuperbill_InsertCommand(object source, GridCommandEventArgs e)
        {
            int practiceLocationID = Convert.ToInt32(Session["PracticeLocationID"]);

            
            GridEditableItem editedItem = e.Item as GridEditableItem;

            if (editedItem.OwnerTableView.Name == "grdtvCategory")
            {
                int columnNumber = 0;
                string categoryName = "";
                columnNumber = Convert.ToInt32(editedItem.OwnerTableView.ParentItem["ColumnID"].Text);

                //Update new values
                Hashtable newValues = new Hashtable();
                //The GridTableView will fill the values from all editable columns in the hash
                e.Item.OwnerTableView.ExtractValuesFromItem(newValues, editedItem);

                foreach (DictionaryEntry entry in newValues)
                {
                    //changedRow[(string)entry.Key] = entry.Value;
                    switch ((string)entry.Key)
                    {
                        case "CategoryName":
                            categoryName = (string)entry.Value;
                            break;

                    }
                }

                AddSuperbillCategory(practiceLocationID, columnNumber, categoryName);
            }
            if (editedItem.OwnerTableView.Name == "grdtvAddOn")
            {
                int practiceCatalogProductID = 0;
                int categoryID = 0;
                int parentSuperBillProductID = 0;
                int superBillProductAddOnID = 0;
                decimal deposit = 0;
                decimal charge = 0;
                decimal cost = 0;
                int userID = 1;
                string hcPCs = "";
                bool addOnFlowSheetOnly = false;
                string productName = "";

                categoryID = Convert.ToInt32(editedItem.OwnerTableView.ParentItem.OwnerTableView.ParentItem["CategoryID"].Text);

                parentSuperBillProductID = Convert.ToInt32(editedItem.OwnerTableView.ParentItem["SuperBillProductID"].Text);

                //superBillProductAddOnID = Convert.ToInt32(editedItem["SuperBillProductAddOnID"].Text);

                practiceCatalogProductID = Convert.ToInt32(editedItem.OwnerTableView.ParentItem["PracticeCatalogProductID"].Text);

                RadComboBox rcbHCPCs = editedItem.FindControl("ddlAddOnHCPCs") as RadComboBox;
                hcPCs = rcbHCPCs.Text;

                TextBox txtProductName = editedItem.FindControl("txtAddOnProductName") as TextBox;
                productName = txtProductName.Text;

                TextBox txtDeposit = editedItem.FindControl("txtDeposit") as TextBox;
                Decimal.TryParse(txtDeposit.Text, NumberStyles.Currency, CultureInfo.CurrentCulture, out deposit);
                //deposit = Decimal.Parse(txtDeposit.Text, NumberStyles.Currency);

                TextBox txtCost = editedItem.FindControl("txtCost") as TextBox;
                Decimal.TryParse(txtCost.Text, NumberStyles.Currency, CultureInfo.CurrentCulture, out cost);
                // cost = Decimal.Parse(txtCost.Text, NumberStyles.Currency);

                TextBox txtCharge = editedItem.FindControl("txtCharge") as TextBox;
                Decimal.TryParse(txtCharge.Text, NumberStyles.Currency, CultureInfo.CurrentCulture, out charge);
                //charge = Decimal.Parse(txtCharge.Text, NumberStyles.Currency);

                CheckBox chkFlowsheetOnly = editedItem.FindControl("chkAddOnFlowsheetOnly") as CheckBox;
                addOnFlowSheetOnly = chkFlowsheetOnly.Checked;

                UpdateAddOn(parentSuperBillProductID, superBillProductAddOnID, practiceLocationID, practiceCatalogProductID, userID, categoryID, hcPCs,
                            deposit, cost, charge, addOnFlowSheetOnly, productName);
            }
        }
        protected void grdSuperbill_UpdateCommand(object source, GridCommandEventArgs e)
        {
            int practiceLocationID = Convert.ToInt32(Session["PracticeLocationID"]);
            int practiceCatalogProductID = 0;
            int categoryID = 0;
            int columnNumber = 0;
            string categoryName = "";
            int userID = 1;
            string hcPCs = "";
            bool? flowSheetOnly = null;
            string productName = null;
            
            GridEditableItem editedItem = e.Item as GridEditableItem;

            if (editedItem.OwnerTableView.Name == "grdtvCategory")
            {
                categoryID = Convert.ToInt32(editedItem["CategoryID"].Text);

                columnNumber = Convert.ToInt32(editedItem["ColumnNumber"].Text);

                //categoryName = editedItem["CategoryName"].Text;
                //Update new values
                Hashtable newValues = new Hashtable();
                //The GridTableView will fill the values from all editable columns in the hash
                e.Item.OwnerTableView.ExtractValuesFromItem(newValues, editedItem);

                foreach (DictionaryEntry entry in newValues)
                {
                    //changedRow[(string)entry.Key] = entry.Value;
                    switch ((string)entry.Key)
                    {
                        case "CategoryName":
                            categoryName = (string)entry.Value;
                            break;

                    }
                }

                UpdateSuperbillCategory(categoryID, practiceLocationID, columnNumber, categoryName);
            }
            else if (editedItem.OwnerTableView.Name == "grdtvSuperbill")
            {
                int superBillProductID = 0;

                categoryID = Convert.ToInt32(editedItem.OwnerTableView.ParentItem["CategoryID"].Text);

                superBillProductID = Convert.ToInt32(editedItem["SuperBillProductID"].Text);

                practiceCatalogProductID = Convert.ToInt32(editedItem["PracticeCatalogProductID"].Text);

                RadComboBox rcbHCPCs = editedItem.FindControl("ddlHCPCs") as RadComboBox;
                hcPCs = rcbHCPCs.Text;

                TextBox txtProductName = editedItem.FindControl("txtProductName") as TextBox;
                if (txtProductName.Text != editedItem["ProductName"].Text)
                {
                    productName = txtProductName.Text;
                }

                CheckBox chkFlowsheetOnly = editedItem.FindControl("chkFlowsheetOnly") as CheckBox;
                flowSheetOnly = chkFlowsheetOnly.Checked;

                #region old value lookup

                ////Update new values
                //Hashtable newValues = new Hashtable();
                ////The GridTableView will fill the values from all editable columns in the hash
                //e.Item.OwnerTableView.ExtractValuesFromItem(newValues, editedItem);

                //foreach (DictionaryEntry entry in newValues)
                //{
                //    //changedRow[(string)entry.Key] = entry.Value;
                //    switch ((string)entry.Key)
                //    {
                //        case "HCPCs":
                //            hcPCs = (string)entry.Value;
                //            break;
                //        //case "PracticeCatalogProductID":
                //        //    practiceCatalogProductID = Convert.ToInt32(entry.Value);
                //        //    break;

                //        case "FlowsheetOnly":
                //            flowSheetOnly = Convert.ToBoolean(entry.Value);
                //            break;
                //    }
                //}
                #endregion

                UpdateSuperbill(superBillProductID, practiceLocationID, practiceCatalogProductID, userID, categoryID, hcPCs, flowSheetOnly, productName);
            }
            else if (editedItem.OwnerTableView.Name == "grdtvAddOn")
            {   //this is update code, but this could be an insert or an update since I'm using a custom editForm
                int parentSuperBillProductID = 0;
                int superBillProductAddOnID = 0;
                decimal deposit = 0;
                decimal charge = 0;
                decimal cost = 0;
                bool addOnFlowSheetOnly = false;
                //need to get this

                categoryID = Convert.ToInt32(editedItem.OwnerTableView.ParentItem.OwnerTableView.ParentItem["CategoryID"].Text);

                parentSuperBillProductID = Convert.ToInt32(editedItem.OwnerTableView.ParentItem["SuperBillProductID"].Text);

                superBillProductAddOnID = Convert.ToInt32(editedItem["SuperBillProductAddOnID"].Text);

                practiceCatalogProductID = Convert.ToInt32(editedItem["PracticeCatalogProductID"].Text);

                RadComboBox rcbHCPCs = editedItem.FindControl("ddlAddOnHCPCs") as RadComboBox;
                hcPCs = rcbHCPCs.Text;

                TextBox txtProductName = editedItem.FindControl("txtAddOnProductName") as TextBox;
                productName = txtProductName.Text;

                TextBox txtDeposit = editedItem.FindControl("txtDeposit") as TextBox;
                deposit = Decimal.Parse(txtDeposit.Text, NumberStyles.Currency);

                TextBox txtCost = editedItem.FindControl("txtCost") as TextBox;
                cost = Decimal.Parse(txtCost.Text, NumberStyles.Currency);

                TextBox txtCharge = editedItem.FindControl("txtCharge") as TextBox;
                charge = Decimal.Parse(txtCharge.Text, NumberStyles.Currency);

                CheckBox chkFlowsheetOnly = editedItem.FindControl("chkAddOnFlowsheetOnly") as CheckBox;
                addOnFlowSheetOnly = chkFlowsheetOnly.Checked;

                UpdateAddOn(parentSuperBillProductID, superBillProductAddOnID, practiceLocationID, practiceCatalogProductID, userID, categoryID, hcPCs,
                            deposit, cost, charge, addOnFlowSheetOnly, productName);
            }
        }
        protected void grdSuperbill_DeleteCommand(object source, GridCommandEventArgs e)
        {
            int practiceLocationID = Convert.ToInt32(Session["PracticeLocationID"]);
            int SuperBillProductID = 0;
            int categoryID = 0;

            GridEditableItem editedItem = e.Item as GridEditableItem;

            //Update new values
            Hashtable newValues = new Hashtable();
            //The GridTableView will fill the values from all editable columns in the hash
            e.Item.OwnerTableView.ExtractValuesFromItem(newValues, editedItem);

            if (editedItem.OwnerTableView.Name == "grdtvAddOn")
            {

                SuperBillProductID = Convert.ToInt32(editedItem["SuperBillProductAddOnID"].Text);

                DeleteSuperbill(SuperBillProductID);
            }
            else if (editedItem.OwnerTableView.Name == "grdtvSuperbill")
            {

                SuperBillProductID = Convert.ToInt32(editedItem["SuperBillProductID"].Text);

                DeleteSuperbill(SuperBillProductID);
            }
            else if (editedItem.OwnerTableView.Name == "grdtvCategory")
            {
                categoryID = Convert.ToInt32(editedItem["CategoryID"].Text);

                DeleteSuperbillCategory(practiceLocationID, categoryID);
            }
        }
        protected void grdSuperbill_CancelCommand(object source, GridCommandEventArgs e)
        {
            
        }


        protected void grdPracticeCatalog_NeedDataSource(object source, Telerik.WebControls.GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable)
            {
                grdPracticeCatalog.DataSource = GetPracticeCatalogSuppliers(Convert.ToInt32(Session["practiceid"]));
            }
        }

        protected void grdPracticeCatalog_PreRender(object sender, EventArgs e)
        {
            if (_PracticeCatalog_UIHelper.HasExpanded == false) //mws
            {

                UIHelper.ExpandFormatGrid(grdPracticeCatalog);

                _PracticeCatalog_UIHelper.HasExpanded = true; //mws

            }

            RadComboBox SearchCriteria = (RadComboBox)rttbSearchFilter.FindControl("cboSearchFilter");

        }

        private DataTable GetMasterCatalogData()
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetMasterCatalogProductInfo");

            DataTable dt = db.ExecuteDataSet(dbCommand).Tables[0];
            return dt;
        }

        private DataTable GetPracticeCatalogData()
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetPracticeCatalogProductInfo");
            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, Session["PracticeID"]);
            DataTable dt = db.ExecuteDataSet(dbCommand).Tables[0];
            return dt;
        }
        
        private Boolean findSelectColumn(GridDataItem gridDataItem)
        {
            TableCell cell = gridDataItem["ClientSelectColumn"];
            CheckBox checkBox = (CheckBox)cell.Controls[0];
            if (checkBox.Checked)
                return true;
            else
            {
                return false;
            }


        }

        private Int32 findSuperbillCategoryID(GridDataItem gridDataItem)
        {

            string SuperbillCategoryID = (string)gridDataItem["CategoryID"].Text;
            if (SuperbillCategoryID != null)
            {
                return Int32.Parse(SuperbillCategoryID);

            }
            else
            {
                return 0;
            }

        }
        
        private Int32 findPracticeCatalogProductID(GridDataItem gridDataItem)
        {

            string PracticeCatalogProductID = (string)gridDataItem["PracticeCatalogProductID"].Text;
            if (PracticeCatalogProductID != null)
            {
                return Int32.Parse(PracticeCatalogProductID);

            }
            else
            {
                return 0;
            }

        }

        private Int32 findMasterCatalogProductID(GridDataItem gridDataItem)
        {

            string PracticeID = (string)gridDataItem["Mastercatalogproductid"].Text;
            if (PracticeID != null)
            {
                return Int32.Parse(PracticeID);

            }
            else
            {
                return 0;
            }

        }

        public List<Column> getSuperbill(int practiceLocationID)
        {

            List<Column> columns = new List<Column>() {
                new Column { ColumnID=1, Name="Column1"},
                new Column { ColumnID=2, Name="Column2"}
            };
            return columns;
        }
        public static DataSet getSuperbillCategories(int practiceLocationID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetSuperbillCategories");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public static DataSet getSuperbillData(int practiceLocationID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetSuperbill");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public static DataSet getSuperbillAddOnData(int practiceLocationID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetSuperbillAddOns");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        /// <summary>
        /// Get the HCPCS for the Combo Boxes
        /// </summary>
        /// <param name="practiceLocationID"></param>
        /// <returns></returns>
        public static DataSet getSuperbillHCPCS(int practiceLocationID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetSuperbillHCPCs");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        protected void btnMoveToPC_Click(object sender, EventArgs e)
        {
            LoopHierarchyRecursive(grdPracticeCatalog.MasterTableView);
        }
        
        private void LoopHierarchyRecursive(GridTableView gridTableView)
        {

            foreach (GridNestedViewItem nestedViewItem in gridTableView.GetItems(GridItemType.NestedView))
            {
                if (nestedViewItem.NestedTableViews.Length > 0)
                {
                    foreach (GridDataItem gridDataItem in nestedViewItem.NestedTableViews[0].Items)
                    {
                        if (findSelectColumn(gridDataItem))
                        {
                            Int32 categoryID = LoopHierarchyRecursiveFindCategoryID(grdSuperbill.MasterTableView);
                            //MWS The second (1) needs to be replaced with a search for the selected category to get CategoryID
                            if (categoryID > 0)
                            {
                                Int32 practiceCatalogProductID = UpdateSuperbill(null, Convert.ToInt32(Session["PracticeLocationID"]), findPracticeCatalogProductID(gridDataItem), 1, categoryID, null, null, null);
                                intPCItems += 1;
                            }
                            else
                            {
                                lblPCStatus.Text = "Please Select a Category!";
                            }
                        }
                    }

                }
                if (intPCItems > 0)
                {
                    //grdSuperbill.Rebind();
                    string strPCStatus = String.Concat("Item(s) added to Super Bill: ", intPCItems.ToString());
                    lblPCStatus.Text = strPCStatus;
                    UIHelper.WriteJavascriptToLabel(grdPracticeCatalog, lblPCStatus, strPCStatus);
                }
            }

        }


        private Int32 LoopHierarchyRecursiveFindCategoryID(GridTableView gridTableView)
        {
            
            foreach (GridDataItem item in grdSuperbill.MasterTableView.Items)
            {
                if (item.HasChildItems)
                {
                    if (item.ChildItem.NestedTableViews.Length > 0)
                    {
                        foreach (GridDataItem categoryItem in item.ChildItem.NestedTableViews[0].Items)
                        {
                            if (categoryItem.OwnerTableView.Name == "grdtvCategory")
                            {
                               if (categoryItem.Selected == true)
                               {
                                   return findSuperbillCategoryID(categoryItem);
                                   //return item["CategoryID"];
                               }
                            }
                        }
                    }

                }
               
            } 
            //foreach (GridNestedViewItem nestedViewItem in gridTableView.GetItems(GridItemType.NestedView))
            //{
            //    if (nestedViewItem.NestedTableViews.Length > 0)
            //    {
            //        foreach (GridDataItem gridDataItem in nestedViewItem.NestedTableViews[0].Items)
            //        {
            //            if (findSelectColumn(gridDataItem))
            //            {
            //                return findSuperbillCategoryID(gridDataItem);
            //            }
            //        }
            //    }
            //}
            return 0;
        }

        // add superbillProductID to Params
        private Int32 UpdateSuperbill(Int32? SuperBillProductID,
                                      Int32 PracticeLocationID, 
                                      Int32 PracticeCatalogProductID, 
                                      Int32 UserID, 
                                      Int32 CategoryID, 
                                      string HCPCs, 
                                      bool? FlowsheetOnly, 
                                      string ProductName)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_TransferFromPracticeCatalogtoSuperbill");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);
            db.AddInParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, PracticeCatalogProductID);
            db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
            db.AddInParameter(dbCommand, "SuperbillCategoryID", DbType.Int32, CategoryID);
            db.AddInParameter(dbCommand, "FlowsheetOnly", DbType.Boolean, FlowsheetOnly ?? false);

            if (HCPCs != null)
            {
                db.AddInParameter(dbCommand, "HCPCs", DbType.String, HCPCs);
            }
           
            if (ProductName != null)
            {
                db.AddInParameter(dbCommand, "ProductName", DbType.String, ProductName);
            }

            if (SuperBillProductID.HasValue)
            {
                db.AddInParameter(dbCommand, "SuperBillProductID", DbType.Int32, SuperBillProductID);
            }

            Int32 SQLReturn = db.ExecuteNonQuery(dbCommand);

            return SQLReturn;
        }

        private Int32 UpdateAddOn(Int32 SuperBillProductID,
                                      Int32 SuperBillProductAddOnID,
                                      Int32 PracticeLocationID,
                                      Int32 PracticeCatalogProductID,
                                      Int32 UserID,
                                      Int32 CategoryID,
                                      string HCPCs,
                                      Decimal Deposit,
                                      Decimal Cost,
                                      Decimal Charge,
                                      bool FlowsheetOnly,
                                      string ProductName)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_SuperbillAddOn_InsertUpdate");

            db.AddInParameter(dbCommand, "SuperBillProductID", DbType.Int32, SuperBillProductID);
            db.AddInParameter(dbCommand, "SuperBillProductAddOnID", DbType.Int32, SuperBillProductAddOnID);
            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);
            db.AddInParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, PracticeCatalogProductID);
            db.AddInParameter(dbCommand, "SuperbillCategoryID", DbType.Int32, CategoryID);
            db.AddInParameter(dbCommand, "HCPCs", DbType.String, HCPCs);
            db.AddInParameter(dbCommand, "Deposit", DbType.Decimal, Deposit);
            db.AddInParameter(dbCommand, "Cost", DbType.Decimal, Cost);
            db.AddInParameter(dbCommand, "Charge", DbType.Decimal, Charge);
            db.AddInParameter(dbCommand, "FlowsheetOnly", DbType.Boolean, FlowsheetOnly);
            db.AddInParameter(dbCommand, "ProductName", DbType.String, ProductName);
            db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);


            Int32 SQLReturn = db.ExecuteNonQuery(dbCommand);

            return SQLReturn;
        }

        private Int32 DeleteSuperbill(Int32 SuperBillProductID)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_DeleteSuperbill");

            db.AddInParameter(dbCommand, "SuperBillProductID", DbType.Int32, SuperBillProductID);

            Int32 SQLReturn = db.ExecuteNonQuery(dbCommand);

            return SQLReturn;
        }

        private Int32 DeleteSuperbillCategory(Int32 PracticeLocationID, Int32 CategoryID)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_DeleteSuperbillCategory");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);
            db.AddInParameter(dbCommand, "SuperbillCategoryID", DbType.Int32, CategoryID);

            Int32 SQLReturn = db.ExecuteNonQuery(dbCommand);

            return SQLReturn;
        }

        private Int32 AddSuperbillCategory(Int32 PracticeLocationID, Int32 ColumnNumber, String CategoryName)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_SuperbillCategory_Insert");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);
            db.AddInParameter(dbCommand, "ColumnNumber", DbType.Int32, ColumnNumber);
            db.AddInParameter(dbCommand, "CategoryName", DbType.String, CategoryName);
            Int32 SQLReturn = db.ExecuteNonQuery(dbCommand);

            return SQLReturn;
        }

        private Int32 UpdateSuperbillCategory(Int32 CategoryID, Int32 PracticeLocationID, Int32 ColumnNumber, String CategoryName)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_SuperbillCategory_Update");

            db.AddInParameter(dbCommand, "CategoryID", DbType.Int32, CategoryID);
            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);
            db.AddInParameter(dbCommand, "ColumnNumber", DbType.Int32, ColumnNumber);
            db.AddInParameter(dbCommand, "CategoryName", DbType.String, CategoryName);
            Int32 SQLReturn = db.ExecuteNonQuery(dbCommand);

            return SQLReturn;
        }

        public void ASPNET_MsgBox(string message)
        {

        //string deleteMessage = "'<b style=\"color:black;background-color:#a0ffff\">'" + message + "</b>";
        string deleteMessage = "";
        deleteMessage  =  deleteMessage + "<SCRIPT LANGUAGE=\"JavaScript\">\n ";
        deleteMessage  =  deleteMessage + "alert('" + message + "')\n ";
        deleteMessage  =  deleteMessage + "</SCRIPT>";

        System.Web.HttpContext.Current.Response.Write( deleteMessage );

        }
        protected void btnViewSuperbill_Click(object sender, EventArgs e)
        {
            RunReport("1");
        }

        protected void btnViewFlowSheet_Click(object sender, EventArgs e)
        {
            RunReport("2");
        }

        protected void btnShowPage_Click(object sender, EventArgs e)
        {
            ReportViewer1.Visible = false;
            ReportViewer1.Enabled = false;
            btnShowPage.Visible = false;

            tblUpload.Visible = true;
            RadAjaxPanel1.Visible = true;
        }
        protected void RunReport(string ReportNumber)
        {
            //RadAjaxPanel1.Enabled = false;
            tblUpload.Visible = false;
            RadAjaxPanel1.Visible = false;

            btnShowPage.Visible = true;
            ReportViewer1.Visible = true;
            ReportViewer1.Enabled = true;
            ReportViewer1.CurrentPage = 1;
            GetSelectedReportPath(ReportNumber);
        }

        protected string GetSelectedReportPath(string selectedReportName)
        {
            string reportName = "";
            //string selectedReportName = ddlReport.SelectedValue.ToString();
            List<ReportParameter> paramList = new List<ReportParameter>();

            switch (selectedReportName)
            {
                case "1":
                    reportName = @"/PracticeAdminReports/Superbill";
                    break;

                case "2":
                    reportName = @"/PracticeAdminReports/FlowSheet";
                    break;

                default:
                    reportName = "";
                    ReportViewer1.Visible = false;
                    ReportViewer1.Enabled = false;
                    break;
            }

            if (reportName.Length > 0)
            {
                SetReportName(reportName);
                paramList.Add(new ReportParameter("PracticeLocationID", practiceLocationID.ToString(), false));
                SetReportParameters(paramList);
            }

            return reportName;

        }

        protected void SetReportName(string reportName)
        {
            ReportViewer1.ProcessingMode = ProcessingMode.Remote;

            // Set report server and report path
            ReportViewer1.ServerReport.ReportServerUrl = new Uri("http://Vision/reportserver");  //This is used for Prod.
#if(DEBUG)
            ReportViewer1.ServerReport.ReportServerUrl = new Uri("http://msneen-gateway/reportserver");
#endif
            //// ReportViewer1.ServerReport.ReportServerUrl = new Uri("http://www.bregvision.com/reportserver");  //Think this also works.

            ReportViewer1.ServerReport.ReportPath = reportName;
            //ReportViewer1.ZoomMode = ZoomMode.PageWidth;
            ReportViewer1.ZoomMode = ZoomMode.Percent;
            ReportViewer1.ZoomPercent = 100;

        }

        protected void SetReportParameters(List<ReportParameter> paramList)
        {
            ReportViewer1.ServerReport.SetParameters(paramList);
            // Process and render the report
            ReportViewer1.ServerReport.Refresh();
        }

        protected void btnbtnUploadLogo_Click(object sender, EventArgs e)
        {
            string currentFilename = "";
            string fileExtension = "";
            currentFilename = LogoUploader.FileName;
            fileExtension = Path.GetExtension(currentFilename);

            if (fileExtension != ".jpg")
            {
                lblLogoFilename.Text = "Please choose a .jpg file";
                return;
            }

            if (LogoUploader.PostedFile.ContentLength > 2000000)
            {
                lblLogoFilename.Text = "Please choose a smaller file";
                return;
            }

            string uploadPath = ConfigurationManager.AppSettings["SuperBillLogoLocation"];
            string currentPath = Server.MapPath(uploadPath);


            if (LogoUploader.HasFile)
            {
                practiceLocationID = (Session["PracticeLocationID"] != null) ? Convert.ToInt32(Session["PracticeLocationID"].ToString()) : -1;
                string newFilename = string.Format("PracticeLogo{0}.jpg", practiceLocationID.ToString());
                currentPath += newFilename;
                LogoUploader.SaveAs(currentPath);

                lblLogoFilename.Text = "";
            }
            else
            {
                lblLogoFilename.Text = "Can not load the file !";
            }


        }
        
        
    }
}
