<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HCPCs.aspx.cs" Inherits="BregVision.Admin.HCPCs" %>
<%@ Register Assembly="RadComboBox.Net2" Namespace="Telerik.WebControls" TagPrefix="radC" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >


<HTML>
  <HEAD>
   <title>HCPCs</title>
   <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
   <meta name="CODE_LANGUAGE" Content="C#">
   <meta name="vs_defaultClientScript" content="JavaScript">
   <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
   
  </HEAD>

<body>
  
    <form id="form1" runat="server">
    <script  language="javascript" type="text/javascript">
     var isDirty;
     isDirty = false;
     function GetRadWindow()
    {
        var oWindow = null;
        if (window.radWindow) oWindow = window.radWindow;//Will work in Moz in all cases, including clasic dialog
        else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;//IE (and Moz az well)
        return oWindow;
    }  

    function CloseOnReload()
    {
        if(isDirty=='true')
        {
            RefreshParentPage();
        }
        GetRadWindow().Close();
    }

    function RefreshParentPage()
    {
        GetRadWindow().BrowserWindow.location.reload();
    }

     
     
     function a(){
        alert("hi");
     window.opener = null;
     window.close();
     }
    </script>    

    <div>
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td colspan="4" style="font-family:Arial; font-size: small; font-weight:bold">
                    Product        
                </td>
            </tr>
            <tr style="height: 25px">
                <td colspan="4">
                    <asp:Label ID="lblProductInfo" runat="server"  Font-Size="Small" Font-Names ="Arial"></asp:Label>        
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td  style="font-family:Arial; font-size: small; font-weight:400">
                    <radG:RadGrid runat="server" ID="grdHCPCs" OnNeedDataSource="grdHCPCs_NeedDataSource"
                          Skin="Office2007" SkinsPath="~/RadControls/Grid/Skins" 
                        AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" EnableAJAX="False" PageSize="5"
                         OnDeleteCommand="grdHCPCs_DeleteCommand">
                        <MasterTableView ShowFooter="True">
                        <RowIndicatorColumn Visible="False">
                            <HeaderStyle Width="20px" />
                        </RowIndicatorColumn>
                        <Columns>
                            
							<radG:GridButtonColumn Text="Delete" ButtonType="ImageButton" ImageUrl="~/App_Themes/Breg/images/Common/delete.png" ConfirmText="Delete this product?" CommandName="Delete" UniqueName="Delete"></radG:GridButtonColumn>
                            <radg:GridBoundColumn DataField="ProductHCPCSID" display="false"></radg:GridBoundColumn>
							<radg:GridBoundColumn DataField="HCPCs" HeaderText="HCPC" UniqueName="HCPCs"></radg:GridBoundColumn>
						</Columns>
						</MasterTableView>
                            <ClientSettings AllowDragToGroup="True">
                            </ClientSettings>
                            <GroupPanel Visible="True">
                            </GroupPanel>
                    </radG:RadGrid>
                  </td>
                <td>
                   <table cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td>
                            <asp:Label ID="lblStatus" runat="server"  Font-Size="Small" style="color:Red" ></asp:Label>    
                        </td>
                    </tr>
                   <tr>
                    <td  style="font-family:Arial; font-size: small; font-weight:400">
                        Add new HCPC <asp:TextBox runat="server" ID="txtNewHCPC" MaxLength="20"></asp:TextBox>    
                    </td>
                   </tr>
                   <tr>
                    <td>
                        <asp:Button  runat="server" ID="btnAddHCPC" Text="Add HCPC" OnClick="btnAddHCPC_Click" />
                        <input type="button" onclick="javascript:CloseOnReload();" value="close" />
                    </td>
                   </tr>

                   </table>
                </td>
            </tr>
        
        </table>
        
    </div>
    </form>
</body>
</html>
