<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PracticeAdmin.Master"
    Inherits="Practice_Contact" Codebehind="Practice_Contact.aspx.cs" %>
<%@ Register Src="~/Admin/UserControls/Practice/PracticeContact.ascx" TagName="PracticeContact" TagPrefix="bvl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true" Visible="true">
  <bvl:PracticeContact ID="ctlPracticeContact" runat="server"></bvl:PracticeContact>
</asp:Content>