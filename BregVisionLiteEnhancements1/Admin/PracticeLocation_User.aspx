<%@ Page Language="C#" AutoEventWireup="true" SmartNavigation="true" MasterPageFile="~/Admin/PracticeAdmin.Master"
    Inherits="PracticeLocation_User" Codebehind="PracticeLocation_User.aspx.cs" %>
<%@ Register Src="~/Admin/UserControls/PracticeLocation/PracticeLocationUsers.ascx" TagName="PracticeLocationUsers" TagPrefix="bvl" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true"
    Visible="true">
    <div style="text-align: center">
        <table id="OuterTable" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <br />
                    <bvl:PracticeLocationUsers id="ctlPracticeLocationUsers" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
