﻿using System;
using System.Web.UI;


namespace BregVision.Admin
{
    public partial class Practice_UserAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((MasterPages.PracticeAdmin)Page.Master).SelectCurrentTab("TabUsers");
        }
    }
}
