using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DAL;

namespace BregVision
{


    public partial class login : System.Web.UI.Page
    {

		const string BregAdminHomePage = "~/BregAdmin/BregAdmin.aspx";
		const string PracticeAdminHomePage = "~/Admin/Practice_Admin.aspx";
        const string PracticeUserHomePage = "~/default.aspx";

		
		
		protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
            {
                Response.Redirect("default.aspx");
            }
            else
            {
                //int userID = DAL.LoggedInUser.SelectUserID(Context.User.Identity.Name.ToString() );
                //Session["UserID"] = userID; 
                //int userID = DAL.
                // Get the User's ID and the User's Name store in Session.
                //   Create in DAL.LoginUser.  LoginUserID LoginUserName
                //   Set the session variables for UserID and UserName.



                if (Roles.IsUserInRole(User.Identity.Name, "BregAdmin"))
                {
                    int userID = DAL.LoggedInUser.SelectUserID(Context.User.Identity.Name.ToString());
                    Session["UserID"] = userID; 
                    Response.Redirect(BregAdminHomePage);

                }
                else
                    if (Roles.IsUserInRole(User.Identity.Name, "ClinicAdmin") || Roles.IsUserInRole(User.Identity.Name, "PracticeAdmin"))
                    {
                        int userID;
                        int practiceID;
                        string practiceName;
                        DAL.LoggedInUser.SelectUserIDandPractice(Context.User.Identity.Name.ToString(), out userID, out practiceID, out practiceName);
                        Session["UserID"] = userID;
                        Session["PracticeID"] = practiceID;
                        Session["PracticeName"] = practiceName;




                        Response.Redirect(PracticeAdminHomePage);
                    }
                    else
                        if (Roles.IsUserInRole(User.Identity.Name, "ClinicLocationUser") || Roles.IsUserInRole(User.Identity.Name, "PracticeLocationUser"))
                        {
                            // Get the Logged in User's PracticeID and PracticeName and set in session variables.
                            ////   Set the session variables for Login in user's PracticeID and PracticeName

                            Response.Redirect(PracticeUserHomePage);
                        }
            }
        }
    }
}
