﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.UserControls.General;
using BregWcfService;


namespace BregVision.Admin
{
    public partial class PracticeLocation_InventoryCount : BregVision.Bases.PageBase
    {

        protected void Page_Init(object sender, EventArgs e)
        {
            this.ctlConsignmentOrder.RecordsPerPage = ctlRecordsPerPage;
            this.ctlPracticeLocationInventoryCount.RecordsPerPage = ctlRecordsPerPage;
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            ((BregVision.MasterPages.PracticeLocationAdmin)Page.Master).SelectCurrentTab("TabInventoryCount");

            //ctlConsignmentOrder.Visible = Context.User.IsInRole("BregAdmin"); //2/1/12 mws allow practiceadmins to add the consignment order to the cart, per Kevin Lord

            this.ctlPracticeLocationInventoryCount.InventoryCycleControl.InventoryCycleEvent += new BregVision.UserControls.InventoryCount.InventoryCycleEventHandler(InventoryCycleControl_InventoryCycleEvent);

            RadAjaxManager1.AjaxSettings.AddAjaxSetting(ctlRecordsPerPage.RecordsDisplayedTextBox, ctlPracticeLocationInventoryCount.InventoryGrid);
            
        }

        void InventoryCycleControl_InventoryCycleEvent(object sender, BregVision.UserControls.InventoryCount.InventoryCycleEventArgs e)
        {
            RadAjaxManager1.AjaxSettings.AddAjaxSetting(this.ctlPracticeLocationInventoryCount.InventoryCycleControl.InventoryCycleTypeControl, this.ctlConsignmentOrder);



            //Don't allow them to see the 'Add to Cart Control' unless:
            // X they have selected a consignment cycle
            // there are no counted items in that cycle
            // X there is a previous consignment cycle that was closed within 2 weeks? that has all items counted
            if (this.ctlPracticeLocationInventoryCount.InventoryCycleControl.IsConsignmentCycle() == true)
            {
                bool currentCycleOpenWithNoCountedItems = BregWcfService.InventoryDiscrepencyItem.VerifyCycleOpenWithNoCountedItems(
                                                                                                                                    practiceLocationID, 
                                                                                                                                    this.ctlPracticeLocationInventoryCount.InventoryCycleControl.CurrentInventoryCycle, 
                                                                                                                                    isConsignmentCount: true);
                if (currentCycleOpenWithNoCountedItems == true)
                {
                    string username = Context.User.Identity.Name;
                    ClassLibrary.DAL.InventoryCycle consignmentCycle = InventorySetup.FindMostRecentlyClosedCycle(practiceLocationID, username, isConsignmentCount: true);
                    if (consignmentCycle != null)
                    {
                        bool previousCycleClosedWithFullCount = BregWcfService.InventoryDiscrepencyItem.VerifyCycleClosedWithFullCount(practiceLocationID, consignmentCycle, isConsignmentCount: true);

                        if (previousCycleClosedWithFullCount == true)
                        {
                            this.ctlConsignmentOrder.Visible = true;
                            return;
                        }
                    }
                }
               
            }
            this.ctlConsignmentOrder.Visible = false;
        }

     
    }
}