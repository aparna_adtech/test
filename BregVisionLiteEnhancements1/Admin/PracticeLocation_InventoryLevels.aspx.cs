using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary.DAL;
using DAL.PracticeLocation;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.WebControls;


namespace BregVision.Admin
{
    public partial class PracticeLocation_InventoryLevels : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((BregVision.MasterPages.PracticeLocationAdmin)Page.Master).SelectCurrentTab("TabPracticeLocationInventoryLevel");
        }
    }
}
