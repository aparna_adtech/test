<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/MasterPages/PracticeLocationAdmin.Master"
    CodeBehind="PracticeLocation_DispensementModifications.cs" Inherits="BregVision.Admin.PracticeLocation_DispensementModifications" %>

<%@ Register Src="~/Admin/UserControls/PracticeLocation/PracticeLocationDispensementModifications.ascx"
    TagName="PracticeLocationDispensementModifications" TagPrefix="bvl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true"
    Visible="true">
    <telerik:RadToolTipManager ID="RadToolTipManager1" Width="300px" Height="100px" RelativeTo="Element"
        Position="BottomRight" runat="server" AnimationDuration="200">
        <WebServiceSettings Path="~/HelpSystem/ToolTipWebService.asmx" Method="GetToolTip" />
    </telerik:RadToolTipManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    </telerik:RadAjaxManager>
  <div class="flex-row">
    <h2>
       <asp:Label ID="lblHeader" runat="server" Text="Dispensement Modification"></asp:Label>
    </h2>
  </div>

    <bvl:PracticeLocationDispensementModifications ID="ctlPracticeLocationDispensementModifications"
            runat="server" />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" InitialDelayTime="500" EnableAjaxSkinRendering="True" BackgroundPosition="Center" Skin="Breg" EnableEmbeddedSkins="False">
    <img src="App_Themes/Breg/images/Common/loading.gif" />
  </telerik:RadAjaxLoadingPanel>
</asp:Content>
