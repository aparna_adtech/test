using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.WebControls; 

namespace BregVision.Admin
{
    public partial class HCPCs : System.Web.UI.Page
    {
        Int32 practiceCatalogProductID = 0;
        protected void InitializeComponent()
        {
            
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            
            //Get HCPC Product Info
            grdHCPCs.DeleteCommand += new GridCommandEventHandler(grdHCPCs_DeleteCommand);

            if (Request.QueryString["PCID"].Length > 0)
            {
                practiceCatalogProductID = Convert.ToInt32(Request.QueryString["PCID"].ToString());
                lblProductInfo.Text = GetHCPCProductInfo(practiceCatalogProductID);
            }
        }

        protected void grdHCPCs_NeedDataSource(object source, Telerik.WebControls.GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable)
            {
                grdHCPCs.DataSource = GetHCPCS(practiceCatalogProductID);
            }
        }

        public string GetHCPCProductInfo(Int32 PracticeCatalogProductID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.USP_GetHCPCProductInfo");

            db.AddInParameter(dbCommand, "PracticeCatalogProductID", DbType.String, PracticeCatalogProductID);

            IDataReader reader = db.ExecuteReader(dbCommand);
  
            string ProductInfo;
            if (reader.Read())
            {
                ProductInfo = string.Concat(reader["ShortName"].ToString(), " ", reader["Code"].ToString(), " ", reader["Packaging"].ToString(), " ", reader["LeftRightSide"].ToString(), " ", reader["Size"].ToString());
                reader.Close();
                return ProductInfo;
                
            }
            else
            {
                return "";
            }
            
        }
        public DataSet GetHCPCS(int practiceCatalogID)
        {
          
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetHCPCs");

            db.AddInParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, practiceCatalogID);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;


        }

        protected void btnAddHCPC_Click(object sender, EventArgs e)
        {
            
            //HCPC Validator
            if (txtNewHCPC.Text.Length == 0)
                {
                    Response.Write("<script language='javascript'>");
                    Response.Write("alert('HCPC needs to be entered');");
                    Response.Write("</script>");
                    return;

                }

            //Add new HCPC
            AddHCPC(practiceCatalogProductID, txtNewHCPC.Text.ToString(), 1);
            //Refresh Grid
            grdHCPCs.DataSource = GetHCPCS(practiceCatalogProductID);
            grdHCPCs.Rebind();
            lblStatus.Text = "HCPC added.";
            //Response.Write("<script language='javascript'>");
            //Response.Write("alert('isDirty=true;');");
            //Response.Write("</script>");
        }

        protected void AddHCPC(Int32 PracticeCatalogProductID, string HCPC, Int32 UserID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_ADDHCPC");

            db.AddInParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, PracticeCatalogProductID);
            db.AddInParameter(dbCommand, "HCPC", DbType.String, HCPC);
            db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
            
            int SQLReturn = db.ExecuteNonQuery(dbCommand);
        }

        protected void grdHCPCs_DeleteCommand(object sender, Telerik.WebControls.GridCommandEventArgs e)
        {
            //delete item
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = e.Item as GridDataItem;
                Int32 productHCPCSID = Int32.Parse(dataItem["ProductHCPCSID"].Text);
                ////Next line calls function to delete item from cart
                deleteItemFromHCPCs(productHCPCSID);
                grdHCPCs.DataSource = GetHCPCS(practiceCatalogProductID);
                grdHCPCs.Rebind();

            }
        }

        private void deleteItemFromHCPCs(Int32 productHCPCSID)
        {
          

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_DeleteItemfromHCPCs");

            db.AddInParameter(dbCommand, "HCPCSID", DbType.Int32, productHCPCSID);

            int SQLReturn = db.ExecuteNonQuery(dbCommand);

        }

    }
}
