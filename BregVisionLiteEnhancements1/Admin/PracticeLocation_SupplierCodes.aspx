<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PracticeLocationAdmin.Master" Inherits="PracticeLocation_SupplierCodes" CodeBehind="PracticeLocation_SupplierCodes.aspx.cs" %>

<%@ Register Src="~/Admin/UserControls/PracticeLocation/PracticeLocationSupplierCodes.ascx"
  TagName="PracticeLocationSupplierCodes" TagPrefix="bvl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true"
  Visible="true">

  <bvl:PracticeLocationSupplierCodes ID="ctlPracticeLocationSupplierCodes" runat="server" />

</asp:Content>
