﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Consignment_InventoryCount.aspx.cs" Inherits="BregVision.Admin.Consignment_InventoryCount" %>
<%@ Register Src="~/UserControls/General/RecordsPerPage.ascx" TagPrefix="bvl" TagName="RecordsPerPage" %>
<%@ Register Src="~/Admin/UserControls/PracticeLocation/PracticeLocationInventoryCount.ascx" TagPrefix="bvl" TagName="PracticeLocationInventoryCount" %>
<%@ Register Src="~/BregAdmin/UserControls/ConsignmentOrder.ascx" TagPrefix="bvl" TagName="ConsignmentOrder" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server"></head>
<body>
<form id="Form1" runat="server" style="width: 100%; height: 100%;">
     <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        <Scripts>
            <asp:ScriptReference Path="~/Include/HelpSystem.js" />
            <asp:ScriptReference Path="~/Include/Silverlight.js" />
            <asp:ScriptReference Path="~/FusionCharts/FusionCharts.js" />
            <asp:ScriptReference Path="~/Include/jquery/js/jquery-1.4.4.min.js" />
            <%--<asp:ScriptReference Path="~/Include/jquery/js/jquery-ui-1.8.9.custom.min.js" />--%>
            <%--<asp:ScriptReference Path="http://jquery-ui.googlecode.com/svn/tags/1.6rc2/ui/ui.core.js" />
            <asp:ScriptReference Path="http://jquery-ui.googlecode.com/svn/tags/1.6rc2/ui/ui.spinner.js" />--%>
            <asp:ScriptReference Path="~/Include/jquery-ui/ui/jquery.ui.core.js" />
            <asp:ScriptReference Path="~/Include/jquery-ui/ui/jquery.ui.widget.js" />
            <asp:ScriptReference Path="~/Include/jquery-ui/ui/jquery.ui.button.js" />
            <asp:ScriptReference Path="~/Include/jquery-ui/ui/jquery.ui.spinner.js" />
        </Scripts>
        <Services>
        </Services>
    </telerik:RadScriptManager>
    <telerik:RadToolTipManager ID="RadToolTipManager1" Width="300px" Height="100px" RelativeTo="Element"
        Position="BottomRight" runat="server" AnimationDuration="200">
        <WebServiceSettings Path="~/HelpSystem/ToolTipWebService.asmx" Method="GetToolTip" />
    </telerik:RadToolTipManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    </telerik:RadAjaxManager>
    <table>
        <tr valign="middle">
            <td align="right">
                <bvl:RecordsPerPage ID="ctlRecordsPerPage" runat="server" />
            </td>
        </tr>
    </table>                
    
    <bvl:PracticeLocationInventoryCount id="ctlPracticeLocationInventoryCount" runat="server"></bvl:PracticeLocationInventoryCount>                 
    <bvl:consignmentorder ID="ctlConsignmentOrder" runat="server"></bvl:consignmentorder>
</form>
</body>
</html>
