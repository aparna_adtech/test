<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PracticeLocationAdmin.Master" Inherits="PracticeLocation_Contact" CodeBehind="PracticeLocation_Contact.aspx.cs" %>

<%@ Register Src="~/Admin/UserControls/PracticeLocation/PracticeLocationContacts.ascx" TagName="PracticeLocationContacts" TagPrefix="bvl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true" Visible="true">
  <bvl:PracticeLocationContacts ID="ctlPracticeLocationContacts" runat="server" />
</asp:Content>
