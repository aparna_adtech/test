<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PracticeAdmin.Master"
  Inherits="Practice_BillingAddress" CodeBehind="Practice_BillingAddress.aspx.cs" %>

<%@ Register Src="~/Admin/UserControls/Practice/PracticeBillingAddress.ascx" TagName="PracticeBillingAddress" TagPrefix="bvl" %>


<asp:Content ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true" Visible="true">

  <bvl:PracticeBillingAddress ID="ctlPracticeBillingAddress" runat="server" />

</asp:Content>
