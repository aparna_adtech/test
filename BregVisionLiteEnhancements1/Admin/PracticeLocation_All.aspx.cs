using System;
using System.Reflection.Emit;
using System.Web.UI;
using Telerik.OpenAccess;
using Label = System.Web.UI.Control;


public partial class PracticeLocation_All : System.Web.UI.Page
{
  //this might be set in the master page on the init event
  private bool _ShowLocations = true;
  public bool ShowLocations
  {
    get
    {
      return _ShowLocations;
    }
    set
    {
      _ShowLocations = value;
    }
  }

  protected void Page_Load(object sender, EventArgs e)
  {
    //this might be set in the master page on the init event
    this.ctlPracticeLocations.Visible = this.ShowLocations;

    Label pageTitleLabel = Master.FindControl("practiceLocationViewName");
    if (pageTitleLabel != null)
    {
     // pageTitleLabel.Text = "All Locations;";
    }
    //((BregVision.MasterPages.PracticeAdmin)Page.Master).SelectCurrentTab("TabLocations");

  }

}

