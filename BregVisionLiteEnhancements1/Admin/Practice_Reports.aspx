<%@ Page Language="C#" MasterPageFile="~/MasterPages/PracticeAdmin.Master" AutoEventWireup="true"
    CodeBehind="Practice_Reports.aspx.cs" Inherits="Practice_Reports" Title="Breg Vision - Practice Reports" %>

<%@ Register Src="~/Admin/UserControls/Practice/PracticeReports.ascx" TagName="PracticeReports"
    TagPrefix="bvl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    </telerik:RadAjaxManager>
    <bvl:PracticeReports ID="ctlPracticeReports" runat="server" />
</asp:Content>
