﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PracticeAdmin.Master" Inherits="Practice_Product" CodeBehind="Practice_Product.aspx.cs" %>

<%@ Register Src="~/Admin/UserControls/Practice/PracticeProducts.ascx" TagName="PracticeProducts" TagPrefix="bvl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true" Visible="true">
  <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" EnableAJAX="true">
  </telerik:RadAjaxManager>
  <bvl:PracticeProducts ID="ctlPracticeProducts" runat="server" />
</asp:Content>
