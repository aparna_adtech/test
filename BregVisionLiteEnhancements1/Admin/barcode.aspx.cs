﻿using System;
using System.Web.UI;
using Telerik.Web.UI;

namespace BregVision.Admin
{
    public partial class BarCode : System.Web.UI.Page
    {

    public RadAjaxLoadingPanel RadAjaxLoadingPanel1Panel
    {
      get { return RadAjaxLoadingPanel1; }
    }

    protected void Page_Load(object sender, EventArgs e)
        {
            
            ((MasterPages.PracticeAdmin)Page.Master).SelectCurrentTab("TabBarCode");
        }
    }
}
