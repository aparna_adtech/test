<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/PracticeAdmin.Master" CodeBehind="Practice_Vendor_Brands.aspx.cs" Inherits="Practice_Vendor_Brands" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" EnableViewState="true" Visible="true">


  <h2><asp:Label ID="lblContentHeader" runat="server" Text="Third Party Vendor Brands"></asp:Label></h2>

  <asp:GridView ID="GridSupplier" runat="server" AutoGenerateColumns="False" ShowFooter="True"
    DataKeyNames="PracticeCatalogSupplierBrandID, ThirdPartySupplierID, PracticeID, SupplierName, SupplierShortName, BrandName , BrandShortName"
    GridLines="Both" CssClass="FieldLabel"
    OnRowEditing="GridSupplier_RowEditing" OnRowCancelingEdit="GridSupplier_RowCancelingEdit"
    OnRowUpdating="GridSupplier_RowUpdating" OnRowDeleting="GridSupplier_RowDeleting"
    OnRowDataBound="GridSupplier_RowDataBound" OnRowCommand="GridSupplier_RowCommand"
    Width="780px" BorderColor="Silver" BorderWidth="1px" CellPadding="3">

    <Columns>
      <asp:ButtonField ButtonType="Image" CommandName="Delete" ImageUrl="~/App_Themes/Breg/images/Common/delete.png" HeaderStyle-Width="50px" />

      <asp:BoundField DataField="PracticeCatalogSupplierBrandID" HeaderText="PracticeCatalogSupplierBrandID"
        Visible="False" />

      <asp:BoundField DataField="PracticeID" HeaderText="PracticeID"
        Visible="False" />

      <asp:BoundField DataField="ThirdPartySupplierID" HeaderText="ThirdPartySupplierID"
        Visible="False" />

      <asp:TemplateField ShowHeader="False" HeaderStyle-Width="50px" ItemStyle-Height="30px">
        <EditItemTemplate>
          <asp:ImageButton ID="btnEditUpdate" runat="server" CausesValidation="True" CommandName="Update"
            ToolTip="Update" ImageUrl="~/App_Themes/Breg/images/Common/checkCircle.png" />&nbsp;
                                                               
          <asp:ImageButton ID="btnEditCancel" runat="server" CausesValidation="True" ToolTip="Cancel"
            CommandName="Cancel" ImageUrl="~/App_Themes/Breg/images/Common/cancelCircle.png" />
        </EditItemTemplate>
        <ItemTemplate>
          <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="False" CommandName="Edit"
            ImageUrl="~/App_Themes/Breg/images/Common/edit.png" ToolTip="Edit" />
        </ItemTemplate>
        <FooterTemplate>
          <asp:ImageButton ID="ImageButtonAddNew" runat="server" CommandName="AddNew" ImageUrl="~\App_Themes\Breg\images\Common\add-record.png"
            ToolTip="Add New Vendor Brand" />
          <asp:ImageButton ID="ImageButtonAddNewCancel" runat="server" Visible="false" CommandName="AddNewCancel"
            OnClientClick="AddNewCancel_OnClientClick" ImageUrl="~/App_Themes/Breg/images/Common/cancelCircle.png"
            ToolTip="Cancel" />
        </FooterTemplate>
      </asp:TemplateField>

      <asp:TemplateField HeaderText="PracticeCatalogSupplierBrandID" Visible="False">
        <ItemTemplate>
          <asp:Label ID="lblPracticeCatalogSupplierBrandIDEdit" runat="server" Text='<%# Bind("PracticeCatalogSupplierBrandID") %>'
            Visible="false"></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
          <asp:Label ID="lblPracticeCatalogSupplierBrandID" runat="server" Text='<%# Eval("PracticeCatalogSupplierBrandID") %>' Visible="false"></asp:Label>
        </EditItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField HeaderText="ThirdPartySupplierID" Visible="False">
        <ItemTemplate>
          <asp:Label ID="lblThirdPartySupplierIDEdit" runat="server" Text='<%# Bind("ThirdPartySupplierID") %>'
            Visible="false"></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
          <asp:Label ID="lblThirdPartySupplierID" runat="server" Text='<%# Eval("ThirdPartySupplierID") %>' Visible="false"></asp:Label>
        </EditItemTemplate>
        <FooterTemplate>
          <asp:Label ID="lblNewThirdPartySupplierID" runat="server" Text='<%# Bind("ThirdPartySupplierID") %>' Visible="false" />
        </FooterTemplate>
      </asp:TemplateField>

      <asp:TemplateField HeaderText="PracticeID" Visible="False">
        <ItemTemplate>
          <asp:Label ID="lblPracticeIDEdit" runat="server" Text='<%# Bind("practiceID") %>'
            Visible="false"></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
          <asp:Label ID="lblPracticeID" runat="server" Text='<%# Eval("practiceID") %>' Visible="false"></asp:Label>
        </EditItemTemplate>
        <FooterTemplate>
          <asp:Label ID="lblNewPracticeID" runat="server" Text='<%# Bind("practiceID") %>' Visible="false" />
        </FooterTemplate>
      </asp:TemplateField>

      <asp:TemplateField HeaderText="Supplier Full Name" Visible="false" HeaderStyle-Wrap="true">
        <EditItemTemplate>
          <asp:Label ID="lblSupplierName" runat="server" Text='<%# Bind("SupplierName") %>' Visible="false" />
        </EditItemTemplate>
        <ItemTemplate>
          <asp:Label ID="lblSupplierName" runat="server" Text='<%# Bind("SupplierName") %>' Visible="false" />
        </ItemTemplate>
        <FooterTemplate>
          <asp:Label ID="lblNewSupplierName" runat="server" Visible="false" />
        </FooterTemplate>
      </asp:TemplateField>

      <asp:TemplateField HeaderText="Supplier Display Name" HeaderStyle-Wrap="true" HeaderStyle-Width="210px">
        <EditItemTemplate>
          <asp:Label ID="lblSupplierShortName" runat="server" Text='<%# Bind("SupplierShortName") %>' Width="100px" />
        </EditItemTemplate>
        <ItemTemplate>
          <asp:Label ID="lblSupplierShortName" runat="server" Text='<%# Bind("SupplierShortName") %>' />
        </ItemTemplate>

        <FooterTemplate>
          <asp:Label ID="lblNewSupplierShortName" runat="server" Visible="false" />
        </FooterTemplate>
      </asp:TemplateField>


      <asp:TemplateField HeaderText="Brand Full Name" SortExpression="BrandName" HeaderStyle-Wrap="true" HeaderStyle-Width="180px" ItemStyle-Width="180px">
        <EditItemTemplate>
          <asp:TextBox ID="txtBrandName" runat="server" Text='<%# Bind("BrandName") %>'
            MaxLength="100" CssClass="FieldLabel" />
          <asp:RequiredFieldValidator ID="rfvBrandName" runat="server" ControlToValidate="txtBrandName"
            Text="*" ErrorMessage="Required" />
        </EditItemTemplate>
        <ItemTemplate>
          <asp:Label ID="lblBrandName" runat="server" Text='<%# Bind("BrandName") %>' />
        </ItemTemplate>
        <FooterTemplate>
          <asp:TextBox ID="txtNewBrandName" runat="server" Visible="false" />
          <asp:RequiredFieldValidator ID="rfvNewBrandName" runat="server" Visible="false" ControlToValidate="txtNewBrandName"
            Text="*" ErrorMessage="Required" />
        </FooterTemplate>
      </asp:TemplateField>

      <asp:TemplateField HeaderText="Brand Display Name" Visible="true" HeaderStyle-Wrap="true">
        <EditItemTemplate>
          <asp:TextBox ID="txtBrandShortName" runat="server" Text='<%# Bind("BrandShortName") %>' Width="125px"
            MaxLength="25" CssClass="FieldLabel"></asp:TextBox>
          <asp:RequiredFieldValidator ID="rfvBrandShortName" runat="server" ControlToValidate="txtBrandShortName"
            Text="*" ErrorMessage="Required" />
        </EditItemTemplate>
        <ItemTemplate>
          <asp:Label ID="lblBrandShortName" runat="server" Text='<%# Bind("BrandShortName") %>'></asp:Label>
        </ItemTemplate>
        <FooterTemplate>
          <asp:TextBox ID="txtNewBrandShortName" runat="server" Visible="False" Width="120px"></asp:TextBox>
          <asp:RequiredFieldValidator ID="rfvNewBrandShortName" runat="server" Visible="false" ControlToValidate="txtNewBrandShortName"
            Text="*" ErrorMessage="Required" />
        </FooterTemplate>
      </asp:TemplateField>

    </Columns>
    <FooterStyle BackColor="White" ForeColor="#000066" />
    <RowStyle ForeColor="#000066" />
    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
    <HeaderStyle CssClass="AdminSubTitle" />
  </asp:GridView>

</asp:Content>
