using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DAL;


    public partial class Practice_Vendor_Brands : System.Web.UI.Page
    {
        int userID;
        int practiceID;
        string practiceName;
        int thirdPartySupplierID;
        string supplierName;
        string supplierShortName;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetSessionVariables(out userID, out practiceID, out practiceName);  //, out practiceLocationID, out practiceLocationName );
                thirdPartySupplierID = (Session["ThirdPartySupplierID"] != null) ? Convert.ToInt32(Session["ThirdPartySupplierID"].ToString()) : -1;

                //might not be available
                supplierName = (Session["SupplierName"] != null) ? (Session["SupplierName"].ToString()) : "";
                supplierShortName = (Session["SupplierShortName"] != null) ? Session["SupplierShortName"].ToString() : "";

                if (supplierName == null || supplierName.Length < 1)
                {
                    GetVendorVariables( thirdPartySupplierID, out supplierName, out supplierShortName);
                }
                lblContentHeader.Text = practiceName + " - Vendor " + supplierName + " - " + " Brands";
                BindGridSupplier( thirdPartySupplierID );  //GetThirdPartySuppliers( practiceID );
            }
            else
            {
                practiceID = (Session["PracticeID"] != null) ? Convert.ToInt32(Session["PracticeID"].ToString()) : -1;
                thirdPartySupplierID = (Session["ThirdPartySupplierID"] != null) ? Convert.ToInt32(Session["ThirdPartySupplierID"].ToString()) : -1;
                supplierName = (Session["SupplierName"] != null) ? (Session["SupplierName"].ToString()) : "";
                supplierShortName = (Session["SupplierShortName"] != null) ? Session["SupplierShortName"].ToString() : "";
            }

            if (this.Master is BregVision.MasterPages.PracticeAdmin)
            {
                BregVision.MasterPages.PracticeAdmin practiceAdminMaster = this.Master as BregVision.MasterPages.PracticeAdmin;
                practiceAdminMaster.SelectCurrentTab("TabVendorBrands");
            }
            //Telerik.WebControls.RadTabStrip tabStrip = (Telerik.WebControls.RadTabStrip)Page.Master.FindControl("PracticeAdminTabStrip");
            //Telerik.WebControls.Tab tabSelected = (Telerik.WebControls.Tab)tabStrip.FindControl("TabVendorBrands");
            ////tabSelected.Text = "Vendor Brands";
            //tabSelected.Visible = true;
            //tabSelected.Selected = true;
            //tabSelected.Font.Bold = true;
            //tabSelected.Font.Italic = true;
            //tabSelected.Text = tabSelected.Text.ToUpper().ToString();
            //tabSelected.ForeColor = System.Drawing.Color.Blue;


            //if (tabStrip.FindControl("TabVendorBrands") == null)
            //{
            //    Telerik.WebControls.Tab tabVendorBrands = new Telerik.WebControls.Tab();
            //    tabVendorBrands.ID = "TabVendorBrands";

            //    tabStrip.Tabs.Insert(5, tabVendorBrands);

            //    tabVendorBrands.Text = "Vendor Brands";
            //    tabVendorBrands.Font.Bold = true;
            //    tabVendorBrands.Font.Italic = true;
            //    tabVendorBrands.Text = tabVendorBrands.Text.ToUpper().ToString();
            //    tabVendorBrands.ForeColor = System.Drawing.Color.Blue;

            //    tabVendorBrands.Selected = true;
            //}
            //else
            //{
            //    Telerik.WebControls.Tab tabVendorBrands = (Telerik.WebControls.Tab)tabStrip.FindControl("tabVendorBrands");
            //    tabVendorBrands.Text = "Vendor Brands";
            //    tabVendorBrands.Font.Bold = true;
            //    tabVendorBrands.Font.Italic = true;
            //    tabVendorBrands.Text = tabVendorBrands.Text.ToUpper().ToString();
            //    tabVendorBrands.ForeColor = System.Drawing.Color.Blue;

            //    tabVendorBrands.Selected = true;
            //}
                

            
            //Telerik.WebControls.Tab tabSelected = (Telerik.WebControls.Tab)tabStrip.FindControl("TabSuppliers");
            //tabSelected.Text = "Vendor " + supplierName + " - " + " Brands";
            //tabSelected.Selected = true;
            //tabSelected.Font.Bold = true;
            //tabSelected.Font.Italic = true;
            //tabSelected.Text = tabSelected.Text.ToUpper().ToString();
            //tabSelected.ForeColor = System.Drawing.Color.Blue;

        }


        protected void GetVendorVariables(int thirdPartySupplierID, out string supplierName, out string supplierShortName)  //,
        //out int practiceLocationID, out string practiceLocationName )
        {
            thirdPartySupplierID = (Session["ThirdPartySupplierID"] != null) ? Convert.ToInt32(Session["ThirdPartySupplierID"].ToString()) : -1;

            GetSupplierNameandSupplierShortName(thirdPartySupplierID, out supplierName, out supplierShortName );

            

            //practiceLocationID = ( Session["PracticeLocationID"] != null ) ? Convert.ToInt32( Session["PracticeLocationID"].ToString() ) : -1;

            //practiceLocationName = ( Session["PracticeLocationName"] != null ) ? Session["PracticeLocationName"].ToString() : "";
        }

        private void GetSupplierNameandSupplierShortName(int thirdPartySupplierID, out string supplierName, out string supplierShortName)
        {
            DAL.Practice.ThirdPartyVendorBrand dalPThirdPartyVendorBrand = new DAL.Practice.ThirdPartyVendorBrand();
            dalPThirdPartyVendorBrand.GetVendorNames(thirdPartySupplierID, out supplierName, out supplierShortName);

            Session["SupplierName"] = supplierName;  // = (!= null) ? (Session["SupplierName"].ToString()) : "";
            Session["SupplierShortName"] = supplierShortName; //= (!= null) ? Session["SupplierShortName"].ToString() : "";

            //supplierName = (Session["SupplierName"] != null) ? (Session["SupplierName"].ToString()) : "";
            //supplierShortName = (Session["SupplierShortName"] != null) ? Session["SupplierShortName"].ToString() : "";
        }

        protected void GetSessionVariables(out int userID, out int practiceID, out string practiceName)  //,
        //out int practiceLocationID, out string practiceLocationName )
        {
            userID = (Session["UserID"] != null) ? Convert.ToInt32(Session["UserID"].ToString()) : -1;

            practiceID = (Session["PracticeID"] != null) ? Convert.ToInt32(Session["PracticeID"].ToString()) : -1;

            practiceName = (Session["PracticeName"] != null) ? Session["PracticeName"].ToString() : "";

            //practiceLocationID = ( Session["PracticeLocationID"] != null ) ? Convert.ToInt32( Session["PracticeLocationID"].ToString() ) : -1;

            //practiceLocationName = ( Session["PracticeLocationName"] != null ) ? Session["PracticeLocationName"].ToString() : "";
        }

        protected void BindGridSupplier(int thirdPartySupplierID)
        {
            DataTable dtThirdPartySuppliers = GetThirdPartySuppliers(thirdPartySupplierID);

            if (dtThirdPartySuppliers.Rows.Count > 0)
            {
                GridSupplier.DataSource = dtThirdPartySuppliers;
                GridSupplier.DataBind();
            }
            else  //No records found - insert a display row so the GridView will display.
            {
                // Sorting Works but is commented out for now.
                //GridSupplier.AllowSorting  = false;
                DisplayNoRecordsFoundRow(dtThirdPartySuppliers);
            }
        }

        protected DataTable GetThirdPartySuppliers(int thirdPartySupplierID)  //change to GetVendorBrands
        {
            //BLL.Practice.ThirdPartySupplier bllSupplier = new BLL.Practice.ThirdPartySupplier();
            DAL.Practice.ThirdPartyVendorBrand dalPThirdPartyVendorBrand = new DAL.Practice.ThirdPartyVendorBrand();  //#rdPartyVendorBrand
            DataTable dtThirdPartyVendorBrands = dalPThirdPartyVendorBrand.SelectAll(thirdPartySupplierID);
            return dtThirdPartyVendorBrands;

            //GridSupplier.DataSource = dtThirdPartySuppliers;
            //GridSupplier.DataBind();
        }


        //  Display "No Records Found" when the grid's data source has now records.
        protected void DisplayNoRecordsFoundRow(DataTable dtThirdPartySuppliers)
        {
            dtThirdPartySuppliers.Rows.Add(dtThirdPartySuppliers.NewRow());
            GridSupplier.DataSource = dtThirdPartySuppliers;
            GridSupplier.DataBind();

            int totalColumns = GridSupplier.Rows[0].Cells.Count;
            GridSupplier.Rows[0].Cells.Clear();
            GridSupplier.Rows[0].Cells.Add(new TableCell());
            GridSupplier.Rows[0].Cells[0].ColumnSpan = totalColumns;
            GridSupplier.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
            GridSupplier.Rows[0].Cells[0].Text = "No Records Found";
        }

        ////  User Clicked Edit, so allow the row to be edited.
        //protected void GridSupplier_RowEditing(object sender, GridViewEditEventArgs e)
        //{
        //    GridSupplier.EditIndex = e.NewEditIndex;
        //    BindGridSupplier( practiceID );
        //}



        protected void GridSupplier_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

            //Update THIS!!
            int practiceCatalogSupplierBrandID = Convert.ToInt32(GridSupplier.DataKeys[e.RowIndex].Values[0].ToString());

            int thirdPartySupplierID = Convert.ToInt32(GridSupplier.DataKeys[e.RowIndex].Values[1].ToString());
            TextBox txtBrandName = (TextBox)GridSupplier.Rows[e.RowIndex].FindControl("txtBrandName");
            TextBox txtBrandShortName = (TextBox)GridSupplier.Rows[e.RowIndex].FindControl("txtBrandShortName");
            //DropDownList ddlSupplierType = (DropDownList)GridSupplier.Rows[e.RowIndex].FindControl("ddlSupplierType");
            //DropDownList ddlOrderPlacementType = (DropDownList)GridSupplier.Rows[e.RowIndex].FindControl("ddlOrderPlacementType");
            //TextBox txtFaxOrderPlacement = (TextBox)GridSupplier.Rows[e.RowIndex].FindControl("txtFaxOrderPlacement");
            //TextBox txtEmailOrderPlacement = (TextBox)GridSupplier.Rows[e.RowIndex].FindControl("txtEmailOrderPlacement");

            string brandName = txtBrandName.Text.ToString().Trim();
            string brandShortName = txtBrandShortName.Text.ToString().Trim();

            ////string supplierType = ddlSupplierType.SelectedValue.ToString();
            ////string orderPlacementType = ddlOrderPlacementType.SelectedValue.ToString();

            ////string faxOrderPlacement = txtFaxOrderPlacement.Text.ToString().Trim();
            ////string emailOrderPlacement = txtEmailOrderPlacement.Text.ToString().Trim();

            ////bool isVendor = (supplierType == "Vendor") ? true : false;
            ////bool isEmailOrderPlacement = (orderPlacementType == "Email") ? true : false;

            DAL.Practice.ThirdPartyVendorBrand dalVendorBrand = new DAL.Practice.ThirdPartyVendorBrand();

            dalVendorBrand.Update(practiceCatalogSupplierBrandID, brandName, brandShortName, userID);

            //////BLL.Practice.Physician bllPP = new BLL.Practice.Physician();

            ////////bllPP.PhysicianID = physicianID;                  //  This is unneccessary for the update!
            //////bllPP.ContactID = Convert.ToInt32(lblContactIDEdit.Text.ToString());
            //////bllPP.Salutation = ddlSalutation.SelectedValue;
            //////bllPP.FirstName = txtFirstName.Text.ToString();
            //////bllPP.MiddleName = txtMiddleName.Text.ToString();
            //////bllPP.LastName = txtLastName.Text.ToString();
            //////bllPP.Suffix = ddlSuffix.SelectedValue;
            //////bllPP.UserID = userID;

            //////bllPP.Update(bllPP);

            // Set Edit Mode back to Read Only.
            GridSupplier.EditIndex = -1;
            BindGridSupplier( thirdPartySupplierID );
        }


        // Set the Drop Down List
        protected void GridSupplier_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            ////Update THIS!!
            ////// use if neccessary
            ////// ddlSupplierType   ddlOrderPlacementType
            ////if (e.Row.RowType == DataControlRowType.DataRow)
            ////{
            ////    DropDownList ddlSupplierType = (DropDownList)e.Row.FindControl("ddlSupplierType");
            ////    DropDownList ddlOrderPlacementType = (DropDownList)e.Row.FindControl("ddlOrderPlacementType");

            ////    if (ddlSupplierType != null)
            ////    {

            ////        //ddlSalutation.DataSource = GetSalutationList();
            ////        //ddlSalutation.DataBind();
            ////        // Retain the value of the Salutation when in EditMode.
            ////        // DataKey 0 is practiceID, DataKey 1 is salutation, DataKey 2 is suffix.

            ////        ddlSupplierType.SelectedValue = (Convert.ToBoolean(GridSupplier.DataKeys[e.Row.RowIndex].Values[2]) ? "Vendor" : "Manufacturer");

            ////    }

            ////    if (ddlOrderPlacementType != null)
            ////    {
            ////        //ddlOrderPlacementType.DataSource = GetSuffixList();
            ////        //ddlOrderPlacementType.DataBind();
            ////        // Retain the value of the Suffix when in EditMode.
            ////        // DataKey 0 is practiceID, DataKey 1 is salutation, DataKey 2 is suffix.
            ////        ddlOrderPlacementType.SelectedValue = (Convert.ToBoolean(GridSupplier.DataKeys[e.Row.RowIndex].Values[3]) ? "Email" : "Fax");

            ////        bool isEmailDisplayed = (Convert.ToBoolean(GridSupplier.DataKeys[e.Row.RowIndex].Values[3]));

            ////        //TextBox txtFaxOrderPlacement = (TextBox)e.Row.FindControl("txtFaxOrderPlacement");
            ////        //TextBox txtEmailOrderPlacement = (TextBox)e.Row.FindControl("txtEmailOrderPlacement");
            ////        //txtFaxOrderPlacement.Visible = !isEmailDisplayed;
            ////        //txtEmailOrderPlacement.Visible = isEmailDisplayed;               

            ////    }
            ////}

        }


        //  User Clicked Edit, so allow the row to be edited.
        protected void GridSupplier_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridSupplier.EditIndex = e.NewEditIndex;
            BindGridSupplier(thirdPartySupplierID);
        }

        //private void BindGridSupplier()
        //{
        //    GetThirdPartySuppliers( practiceID );
        //}
        //  Get all Physicians and bind to the GridSuppliers.




        //  User Canceled the Row Edit, so remove the editing from the row.
        protected void GridSupplier_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridSupplier.EditIndex = -1;
            BindGridSupplier(thirdPartySupplierID);
        }



        //  User click Delete so Delete the Practice.
        public void GridSupplier_RowDeleting(Object sender, GridViewDeleteEventArgs e)
        {
            ////  Delete
            
            //  Do not delete brands with active products.

            int brandProductCount = 0;  // number of active product assoc. with the ThirdPartyVendorBrand / practiceCatalogSupplierBrandID

            int practiceCatalogSupplierBrandID = Convert.ToInt32(GridSupplier.DataKeys[e.RowIndex].Values[0].ToString());
            DAL.Practice.ThirdPartyVendorBrand dalTPVB = new DAL.Practice.ThirdPartyVendorBrand();
            dalTPVB.Delete(practiceCatalogSupplierBrandID, userID, out brandProductCount);
            GridSupplier.EditIndex = -1;
            BindGridSupplier(thirdPartySupplierID );
        }

        //GoToPracticeVendorBrands
        // User Clicked "AddNew" or "Insert" or "Cancel" (ed) the AddNew.
        protected void GridSupplier_RowCommand(object sender, GridViewCommandEventArgs e)
        {
             // Fix this.
                // Check if AddNew Button was clicked.  Note: this could be AddNew or Insert button that was clicked.
                if (e.CommandName.Equals("AddNew"))
                {
                    // Get the Controls within the footer.
                    //Label lblThirdPartySupplierID = (Label)GridSupplier.FooterRow.FindControl("lblThirdPartySupplierID");  //Convert.ToInt32(GridSupplier.DataKeys[e.RowIndex].Values[1].ToString());
                    //int thirdPartySupplierID;
                    
                    //  int practiceID = GridSupplier.FooterRow.FindControl("lblPracticeID");  //Convert.ToInt32(GridSupplier.DataKeys[e.RowIndex].Values[2].ToString());
                    //Label txtNewSupplierName = (Label)GridSupplier.FooterRow.FindControl("txtNewSupplierName");
                    //Label txtNewSupplierShortName = (Label)GridSupplier.FooterRow.FindControl("txtNewSupplierShortName");
                    TextBox txtNewBrandName = (TextBox)GridSupplier.FooterRow.FindControl("txtNewBrandName");
                    TextBox txtNewBrandShortName = (TextBox)GridSupplier.FooterRow.FindControl("txtNewBrandShortName");
                    //DropDownList ddlNewSupplierType = (DropDownList)GridSupplier.FooterRow.FindControl("ddlNewSupplierType");
                    //DropDownList ddlNewOrderPlacementType = (DropDownList)GridSupplier.FooterRow.FindControl("ddlNewOrderPlacementType");
                    //TextBox txtNewFaxOrderPlacement = (TextBox)GridSupplier.FooterRow.FindControl("txtNewFaxOrderPlacement");
                    //TextBox txtNewEmailOrderPlacement = (TextBox)GridSupplier.FooterRow.FindControl("txtNewEmailOrderPlacement");

                    ImageButton imageButtonAddNew = (ImageButton)GridSupplier.FooterRow.FindControl("ImageButtonAddNew");
                    ImageButton imageButtonAddNewCancel = (ImageButton)GridSupplier.FooterRow.FindControl("ImageButtonAddNewCancel");

                    //  Check if the AddNew button was clicked to Add a New record as opposed to Inserting a New Record
                    if (imageButtonAddNewCancel.Visible == false)
                    {
                        // User clicked to Add a New Record.  
                        //Display the New Fields for input and their validation controls.
                        //Display the Insert and Cancel buttons.
                        
                        //txtNewSupplierName.Visible = true;
                        //txtNewSupplierShortName.Visible = true;
                        txtNewBrandName.Visible = true;
                        txtNewBrandShortName.Visible = true;
                        //ddlNewSupplierType.Visible = true;
                        //ddlNewOrderPlacementType.Visible = true;
                        //txtNewFaxOrderPlacement.Visible = true;
                        //txtNewEmailOrderPlacement.Visible = true;

                        imageButtonAddNew.ImageUrl = @"~\RadControls\Grid\Skins\Outlook\Insert.gif";
                        imageButtonAddNew.ToolTip = "Insert New Practice Physician";
                        imageButtonAddNewCancel.Visible = true;

                        // rfvNewFirstName.Visible = true;
                        // rfvNewLastName.Visible = true;

                        // Sorting Works but is commented out for now.
                        //GridSupplier.AllowSorting = false;

                        // If the "No Records Found" was displayed before the user clicked AddNew than clear that row.
                        if (GridSupplier.Rows[0].Cells[0].Text == "No Records Found")
                        {
                            GridSupplier.Rows[0].Cells.Clear();
                            //BindGridSupplier();   // Test commenting this out
                        }



                    }
                    else    //  User clicked the Insert button.
                    {
                    //    // User clicked to Insert a New Record, so Insert the New Record.
                    //    // Hide the Add New Fields for input and their validators.
                    //    // Hide the Insert and Cancel buttons.
                    //    // Show the Add New Button


                        string brandName = txtNewBrandName.Text.ToString().Trim();
                        string brandShortName = txtNewBrandShortName.Text.ToString().Trim();

                    //    string supplierType = ddlNewSupplierType.SelectedValue.ToString();
                    //    string orderPlacementType = ddlNewOrderPlacementType.SelectedValue.ToString();

                    //    string faxOrderPlacement = txtNewFaxOrderPlacement.Text.ToString().Trim();
                    //    string emailOrderPlacement = txtNewEmailOrderPlacement.Text.ToString().Trim();

                    //    bool isVendor = (supplierType == "Vendor") ? true : false;
                    //    bool isEmailOrderPlacement = (orderPlacementType == "Email") ? true : false;    


                    //    //RequiredFieldValidator rfvNewFirstName = (RequiredFieldValidator)GridSupplier.FooterRow.FindControl("rfvNewFirstName");
                    //    //RequiredFieldValidator rfvNewLastName = (RequiredFieldValidator)GridSupplier.FooterRow.FindControl("rfvNewLastName");

                    //    ImageButton imageButtonAddNew = (ImageButton)GridSupplier.FooterRow.FindControl("ImageButtonAddNew");
                    //    ImageButton imageButtonAddNewCancel = (ImageButton)GridSupplier.FooterRow.FindControl("ImageButtonAddNewCancel");

                    //  Check if the AddNew button was clicked to Add a New record as opposed to Inserting a New Record
                    if (imageButtonAddNewCancel.Visible == false)
                    {
                        // User clicked to Add a New Record.  
                        //Display the New Fields for input and their validation controls.
                        //Display the Insert and Cancel buttons.

                        //txtNewSupplierName.Visible = true;
                        //txtNewSupplierName.Visible = true;
                        //txtNewSupplierShortName.Visible = true;
                        txtNewBrandName.Visible = true;
                        txtNewBrandShortName.Visible = true;
                        //ddlNewSupplierType.Visible = true;
                        //ddlNewOrderPlacementType.Visible = true;
                        //txtNewFaxOrderPlacement.Visible = true;
                        //txtNewEmailOrderPlacement.Visible = true;

                        imageButtonAddNew.ImageUrl = @"~\RadControls\Grid\Skins\Outlook\Insert.gif";
                        imageButtonAddNew.ToolTip = "Insert New Practice Physician";
                        imageButtonAddNewCancel.Visible = true;

                        //rfvNewFirstName.Visible = true;
                        //rfvNewLastName.Visible = true;



                        // If the "No Records Found" was displayed before the user clicked AddNew than clear that row.
                        if (GridSupplier.Rows[0].Cells[0].Text == "No Records Found")
                        {
                            GridSupplier.Rows[0].Cells.Clear();
                        }


                    }
                    else    //  User clicked the Insert button.
                    {
                        // User clicked to Insert a New Record, so Insert the New Record.
                        // Hide the Add New Fields for input and their validators.
                        // Hide the Insert and Cancel buttons.
                        // Show the Add New Button

                        //int thirdPartySupplierID = Convert.ToInt32(GridSupplier.DataKeys[e.RowIndex].Values[1].ToString());
                        //int practiceID = Convert.ToInt32(GridSupplier.DataKeys[e.RowIndex].Values[2].ToString());
                   
                        //  int thirdPartySupplier;
                        //string supplierName = txtNewSupplierName.Text.ToString().Trim();
                        //string supplierShortName = txtNewSupplierShortName.Text.ToString().Trim();
                        string brandName1 = txtNewBrandName.Text.ToString().Trim();
                        string brandShortName1 = txtNewBrandShortName.Text.ToString().Trim();

                        ////string supplierType = ddlNewSupplierType.SelectedValue.ToString();
                        ////string orderPlacementType = ddlNewOrderPlacementType.SelectedValue.ToString();

                        ////string faxOrderPlacement = txtNewFaxOrderPlacement.Text.ToString().Trim();
                        ////string emailOrderPlacement = txtNewEmailOrderPlacement.Text.ToString().Trim();

                        ////bool isVendor = (supplierType == "Vendor") ? true : false;
                        ////bool isEmailOrderPlacement = (orderPlacementType == "Email") ? true : false;

                        int practiceCatalogSupplierBrandID;
                        DAL.Practice.ThirdPartyVendorBrand dalPTPS = new DAL.Practice.ThirdPartyVendorBrand();
                        dalPTPS.Insert( out practiceCatalogSupplierBrandID, practiceID, thirdPartySupplierID, supplierName, supplierShortName, brandName1, brandShortName1, userID);


                        //txtNewSupplierName.Visible = false;
                        //txtNewSupplierName.Visible = false;
                        //txtNewSupplierShortName.Visible = false;
                        txtNewBrandName.Visible = false;
                        txtNewBrandShortName.Visible = false;
                        //ddlNewSupplierType.Visible = false;
                        //ddlNewOrderPlacementType.Visible = false;
                        //txtNewFaxOrderPlacement.Visible = false;
                        //txtNewEmailOrderPlacement.Visible = false;

                        imageButtonAddNew.ImageUrl = @"~\App_Themes\Breg\images\Common\add-record.png";
                        imageButtonAddNew.ToolTip = "Add New Third Party Supplier";
                        imageButtonAddNewCancel.Visible = false;

                        //rfvNewFirstName.Visible = false;
                        //rfvNewLastName.Visible = false;

                        BindGridSupplier(thirdPartySupplierID);
                    }
                }

                //  User Canceled the insertion of a new record.  Therefore, revert mode back to AddNew.
                //  Hide the Add New Fields for input and their validators.
                //  Hide the Insert and Cancel buttons.
                //  Show the Add New Button
                }

                if (e.CommandName.Equals("AddNewCancel"))
                {

                    //int thirdPartySupplierID = Convert.ToInt32(GridSupplier.DataKeys[e.RowIndex].Values[1].ToString());
                    //int practiceID = Convert.ToInt32(GridSupplier.DataKeys[e.RowIndex].Values[2].ToString());

                    //GridSupplier.FooterRow.FindControl("txtNewSupplierName").Visible = false;
                    //GridSupplier.FooterRow.FindControl("txtNewSupplierShortName").Visible = false; 
                    GridSupplier.FooterRow.FindControl("txtNewBrandName").Visible = false; 
                    GridSupplier.FooterRow.FindControl("txtNewBrandShortName").Visible = false; 
 
                    //TextBox txtNewSupplierName = (TextBox)GridSupplier.FooterRow.FindControl("txtNewSupplierName");
                    //TextBox txtNewSupplierShortName = (TextBox)GridSupplier.FooterRow.FindControl("txtNewSupplierShortName");
                    //TextBox txtNewBrandName = (TextBox)GridBrand.FooterRow.FindControl("txtNewBrandName");
                    //TextBox txtNewBrandShortName = (TextBox)GridBrand.FooterRow.FindControl("txtNewBrandShortName");
 
                    //DropDownList ddlSupplierTypa = (DropDownList)GridSupplier.FooterRow.FindControl("ddlSupplierType");
                    //DropDownList ddlOrderPlacementType = (DropDownList)GridSupplier.FooterRow.FindControl("ddlOrderPlacementType");
                    //TextBox txtNewFaxOrderPlacement = (TextBox)GridSupplier.FooterRow.FindControl("txtNewFaxOrderPlacement");
                    //TextBox txtNewEmailOrderPlacement = (TextBox)GridSupplier.FooterRow.FindControl("txtNewEmailOrderPlacement");

                    //RequiredFieldValidator rfvNewFirstName = (RequiredFieldValidator)GridSupplier.FooterRow.FindControl("rfvNewFirstName");
                    //RequiredFieldValidator rfvNewLastName = (RequiredFieldValidator)GridSupplier.FooterRow.FindControl("rfvNewLastName");

                    ImageButton imageButtonAddNew = (ImageButton)GridSupplier.FooterRow.FindControl("ImageButtonAddNew");
                    ImageButton imageButtonAddNewCancel = (ImageButton)GridSupplier.FooterRow.FindControl("ImageButtonAddNewCancel");

                    //txtNewSupplierName.Visible = false;
                    //txtNewSupplierName.Visible = false;
                    //txtNewSupplierShortName.Visible = false;
                //txtNewBrandName.Visible = false;
                //txtNewBrandShortName.Visible = false;
                    //ddlSupplierType.Visible = false;
                    //ddlOrderPlacementType.Visible = false;
                    //txtNewFaxOrderPlacement.Visible = false;
                    //txtNewEmailOrderPlacement.Visible = false;

                    imageButtonAddNew.ImageUrl = @"~\App_Themes\Breg\images\Common\add-record.png";
                    imageButtonAddNew.ToolTip = "Add New Third Party Supplier";

                    imageButtonAddNewCancel.Visible = false;
                    //rfvNewFirstName.Visible = false;
                    //rfvNewLastName.Visible = false;

                    // Check if user cancel the add new record when adding the first record.
                    // Is so call the Bind method so that the "No Records Found" message is displayed.
                    if (GridSupplier.Rows.Count < 2)
                    {
                        // Sorting Works but is commented out for now.
                        //GridSupplier.AllowSorting = false;                
                    }

                    BindGridSupplier(thirdPartySupplierID);
                }
            }
        }


