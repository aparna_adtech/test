using System;
using System.Web.UI;

public partial class Practice_Reports : System.Web.UI.Page
{

    void Page_PreInit(Object sender, EventArgs e)
    {
        if (BLL.Website.IsVisionExpress() || IsFromMainMenu())
        {
            this.MasterPageFile = "~/MasterPages/SiteMaster.Master";
        }
    }

    private bool IsFromMainMenu()
    {
        if (Request.QueryString["f"] != null)
        {
            if (Request.QueryString["f"].ToString() == "m" || Session["MainMenuReport"].ToString() == "true")
            {
                Session["MainMenuReport"] = "true";
                return true;
            }
        }
        return false;
    }


    protected void Page_Load(object sender, EventArgs e)
    {

        if (BLL.Website.IsVisionExpress() || IsFromMainMenu())
        {
            ((BregVision.MasterPages.SiteMaster)Master).SetLocationDropdownVisibility(true);
        }
        else
        {
            ((BregVision.MasterPages.PracticeAdmin)Page.Master).Master.SetLocationDropdownVisibility(true);

            if (User.IsInRole("PracticeAdmin") || User.IsInRole("BregAdmin"))
            {
                ((BregVision.MasterPages.PracticeAdmin)Page.Master).SelectCurrentTab("TabReports");
            }
            else
            {
                ((BregVision.MasterPages.PracticeAdmin)Page.Master).HideAllMenus();
            }  
        }
    }
}