﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.UserControls.General;


namespace BregVision.Admin
{
    public partial class Consignment_InventoryCount : Bases.PageBase
    {

        protected void Page_Init(object sender, EventArgs e)
        {
            this.ctlConsignmentOrder.RecordsPerPage = ctlRecordsPerPage;
            this.ctlPracticeLocationInventoryCount.RecordsPerPage = ctlRecordsPerPage;

            int id = 0;
            int.TryParse(Request.QueryString["id"], out id);

            practiceLocationID = id;

        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            //((BregVision.MasterPages.PracticeLocationAdmin)Page.Master).SelectCurrentTab("TabInventoryCount");

            //((BregVision.MasterPages.PracticeLocationAdmin)Page.Master).HideMenu();

            //ctlConsignmentOrder.Visible = Context.User.IsInRole("BregAdmin");
        }
    }
}