<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderConfirmationReceipt.aspx.cs" Inherits="BregVision.Authentication.OrderConfirmationReceipt" %>
<%@ Register src="~/UserControls/DispensePage/CustomBraceJPG.ascx" tagname="CustomBrace" tagprefix="bvu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Order Confirmation Receipt</title>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
</head>
<body>
    <form id="form1" runat="server">
	<asp:HiddenField ID="ParentBregVisionOrderID" runat="server" />
    <table width="96%" border="0">
    <tr>
        <td colspan=3>
            <table width="100%">
                <tr>
                    <td>
                        <b>Order Confirmation</b>
                    </td>
                   </tr>
            </table>
        </td>

     </tr>
  <tr>
    <td width="21">&nbsp;</td>
    <td width="100%"  style='font-size:x-small'>
        Your Purchase Order has been successfully processed. 
    </td>
    <td width="21">&nbsp;</td>
  </tr>
   <tr>
    <td width="21">&nbsp;</td>
    <td align=center><br />
         <asp:Label runat=server ID="lblPONumber" Font-Bold=true></asp:Label>
         <br />
    </td>
    <td width="21">&nbsp;</td>
  </tr>
   <tr>
    <td width="21">&nbsp;</td>
    <td align=center><br />
         <asp:Label runat=server ID="lblPODetail"></asp:Label>
         <br />
    </td>
    <td width="21">&nbsp;</td>
  </tr>
  <tr>
    <td width="21">&nbsp;</td>
    <td align=center><br />
         <asp:PlaceHolder ID="plPODetail" runat="server"></asp:PlaceHolder>
         <br />
    </td>
    <td width="21">&nbsp;</td>
  </tr> 
  <tr>
    <td width="21">&nbsp;</td>
    <td  style='font-size:x-small'>
          <br /><b> Please print this page and keep for your records.</b>
    </td>
    <td width="21">&nbsp;</td>
  </tr>
  <tr>
    <td width="21">&nbsp;</td>
    <td  style='font-size:x-small'>
           <b>Contact your practice administrator if you need more assistance. </b>
    </td>
    <td width="21">&nbsp;</td>
  </tr> 
 
  </table>

  <div style='overflow-x: hidden; overflow-y: hidden;<%=IsCustomBrace ? "" : "display: none" %>'>
	<bvu:CustomBrace ID="CustomBrace" runat="server" BregVisionOrderID='<%# BregVisionOrderID %>' />
  </div>

  </form>
</body>
</html>
