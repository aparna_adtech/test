﻿using System;
using System.Data;
using System.Linq;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using ClassLibrary.DAL;

namespace BregVision.Authentication
{
    public partial class DispenseReceiptCtl : System.Web.UI.UserControl
    {
        public int DispenseID { get; set; }
		public Practice Practice { get; set; }
		public int PracticeLocationID { get; set; }
		public string PracticeLocationName { get; set; }
        public bool LastItem { get; set; }
        

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void LoadControlInfo()
        {
            if (DispenseID > 0)
            {

                BuildReceipt(divReceipt);
                if (LastItem == true)
                {
                    divReceipt.Style.Clear();
                }
            }
            else
                Response.Write("No Data found. Please contact your system administrator if you need additional assistance.");
        }

        private IDataReader GetBillingAddressByPracticeLocation(Int32 PracticeLocationID)
        {
            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetBillingAddressByPracticeLocation");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);
            IDataReader reader = db.ExecuteReader(dbCommand);
            return reader;
        }

        private IDataReader GetDispenseReceipt(Int32 DispenseID)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetDispenseReceipt");

            db.AddInParameter(dbCommand, "DispenseID", DbType.Int32, DispenseID);
            IDataReader reader = db.ExecuteReader(dbCommand);
            return reader;

        }

        private DateTime GetCreatedDate(Int32 DispenseID)
        {
            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.[usp_GetCreatedDateForReceipt]");

            db.AddInParameter(dbCommand, "DispenseID", DbType.Int32, DispenseID);
            DateTime DateDispensed = (DateTime)db.ExecuteScalar(dbCommand);

            return DateDispensed;
        }

        private void BuildReceipt(HtmlGenericControl receipt_container)
        {
            string patientCode = "";
            string physicianName = "";
            string icd9Code = "";

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            //Build header information
            sb.Append("<table width='788' bgcolor=white><tr><td>");
            sb.Append("<table width='98%'>");

            sb.Append("<tr><td style='font-size:smaller' style='font-weight:bold' align=left>BREG Vision Patient Receipt</td>");
            sb.Append("<td align=right>");

            if (ViewState["DispenseID"] != null)
            {
                DispenseID = (Int32)ViewState["DispenseID"];
            }

            DateTime test2 = GetCreatedDate(DispenseID);
            string test = test2.ToString("MM/dd/yyyy");
            sb.Append(test);

            //sb.Append(DateTime.Now);
            sb.Append("</td></tr>");

            sb.Append("<tr><td style='font-size:smaller' align=left colspan=2>");
            sb.Append(Practice.PracticeName.ToString());
            sb.Append("</td></tr>");

            sb.Append("<tr><td style='font-size:smaller' align=left colspan=2>");
            sb.Append(PracticeLocationName.ToString());
            sb.Append("</td></tr>");
            sb.Append("</table>");



            IDataReader reader = null; //GetBillingAddressByPracticeLocation(PracticeLocationID);
            DataRow drPracticeLocationBillingAddress = null;
            DAL.PracticeLocation.BillingAddress dalPLBA = new DAL.PracticeLocation.BillingAddress();
            string attentionOf = "";
            int addressID = 0;
            string addressLine1 = "";
            string addressLine2 = "";
            string city = "";
            string state = "";
            string zipCode = "";
            string zipCodePlus4 = "";
            dalPLBA.Select(PracticeLocationID,
                                out attentionOf,
                                 out addressID,
                                 out addressLine1,
                                 out addressLine2,
                                 out city,
                                 out state,
                                 out zipCode,
                                 out zipCodePlus4);

            string fax = "";
            DataSet dsPracticeLocationBillingAddress = dalPLBA.Select(PracticeLocationID);
            if (dsPracticeLocationBillingAddress.Tables[0].Rows.Count > 0)
            {
                drPracticeLocationBillingAddress = dsPracticeLocationBillingAddress.Tables[0].Rows[0];
                fax = drPracticeLocationBillingAddress["Fax"].ToString();
            }


            //Get Practice BillingInfo
            sb.Append("<table width='98%'>");
            //DataSet ds = db.ExecuteDataSet(dbCommand);
            //if (reader.Read())
            // {
            sb.Append("<tr><td style='font-size:smaller' align=left>");
            sb.Append(addressLine1);
            sb.Append("</td></tr>");
            sb.Append("<tr><td style='font-size:smaller' align=left>");
            sb.Append(city);
            sb.Append(", ");
            sb.Append(state);
            sb.Append(" ");
            sb.Append(zipCode);
            sb.Append("</td></tr>");
            sb.Append("<tr><td style='font-size:smaller' align=left>Fax: ");
            sb.Append(fax);
            sb.Append("</td></tr>");

            //}
            // reader.Close();
            sb.Append("</table><br><br>");

            ////Get Dispense Detail Information
            //if (ViewState["DispenseID"] != null)
            //{
            //    DispenseID = (Int32)ViewState["DispenseID"];
            //}
            //Detailed written order heading
            sb.Append("<div>Mike Was Here</div>");

            reader = GetDispenseReceipt(DispenseID);
            //DataSet ds = db.ExecuteDataSet(dbCommand);
            if (reader.Read())
            {
                physicianName = string.Concat(reader["Title"].ToString(), " ", reader["FirstName"].ToString(), " ", reader["LastName"].ToString(), " ", reader["Suffix"].ToString());
                patientCode = reader["PatientCode"].ToString();
                icd9Code = reader["ICD9Code"].ToString();
                //Doctor Information
                sb.Append("<table><tr><td style='font-size:smaller' align=left width='100px'>Provider: </td><td align=left style='font-size:smaller' >");
                sb.Append(physicianName);
                sb.Append("</td><td style='font-size:smaller' align=left>Signature: _____________________________________</td><td style='font-size:smaller' align=left>Date: __________________</td></tr>");
                sb.Append("<tr><td align=left style='font-size:smaller' >Patient ID Number: </td><td  align=left style='font-size:smaller' >");
                sb.Append(patientCode);
                sb.Append("</td><td colspan=2></td></tr></table><br><br>");

                //Dispense Detail
                sb.Append("<table cellspacing=0 cellpadding=3 border=1><tr><td style='font-size:smaller' align=left>Supplier</td><td style='font-size:smaller' align=left>Product Name</td><td style='font-size:smaller' align=left>Part Number</td><td style='font-size:smaller' align=left>Quantity");
				sb.Append("<td style='font-size:smaller' align=left>HCPCs</td><td style='font-size:smaller' align=left>Dx Code</td><td style='font-size:smaller' align=left>DME Deposit</td>");
                sb.Append("<td style='font-size:smaller' align=left>RT/LT Modifier</td><td style='font-size:smaller' align=left>Size</td><td style='font-size:smaller' align=left>Gender</td><td style='font-size:smaller' align=left>Billing Charge</td>");
                //sb.Append("<td style='font-size:smaller' align=left>Total</td>");
                sb.Append("</tr>");

                //loop through collection and populate all dispense detail records
                //string grandTotal;
                do
                {
                    sb.Append("<tr><td style='font-size:smaller' align=left>");
                    sb.Append(reader["SupplierName"].ToString());
                    sb.Append("</td><td style='font-size:smaller' align=left>");
                    sb.Append(reader["ShortName"].ToString());
                    sb.Append("</td><td style='font-size:smaller' align=left>");
                    sb.Append(reader["Code"].ToString());
                    sb.Append("</td><td style='font-size:smaller' align=left>");
                    sb.Append(reader["Quantity"].ToString());
                    sb.Append("</td><td style='font-size:smaller' align=left>&nbsp");

                    sb.Append(reader["HCPCsString"].ToString());
                    sb.Append("</td><td style='font-size:smaller' align=left>&nbsp");

                    //mws ICD9 code here
                    sb.Append(reader["ICD9Code"].ToString());
                    sb.Append("</td><td style='font-size:smaller' align=left>$");

                    decimal DMEDeposit = Convert.ToDecimal(reader["DMEDeposit"] == DBNull.Value ? 0 : reader["DMEDeposit"]) * Convert.ToInt16(reader["Quantity"] == DBNull.Value ? 0 : reader["Quantity"]);
                    sb.Append(DMEDeposit.ToString("#,##0.00"));
                    //sb.Append(Convert.ToDecimal(reader["DMEDeposit"]).ToString("#,##0.00"));


                    sb.Append("</td><td style='font-size:smaller' align=left>");

                    sb.Append(reader["Side"].ToString());
                    sb.Append("</td><td style='font-size:smaller' align=left>");
                    sb.Append(reader["Size"].ToString());
                    sb.Append("</td><td style='font-size:smaller' align=left>");
                    sb.Append(reader["Gender"].ToString());
                    sb.Append("</td><td style='font-size:smaller' align=left>$");


                    decimal Total = Convert.ToDecimal(reader["Total"]) * Convert.ToInt16(reader["Quantity"]);
                    sb.Append(Total.ToString("#,##0.00"));
                    //sb.Append(Convert.ToDecimal(reader["Total"]).ToString("#,##0.00"));



                    //sb.Append("</td><td style='font-size:smaller' align=left>$");
                    //sb.Append(Convert.ToDecimal(reader["LineTotal"]).ToString("#,##0.00"));
                    sb.Append("</td></tr>");
                    //grandTotal = Convert.ToDecimal(reader["GrandTotal"]).ToString("#,##0.00");
                } while (reader.Read());

                reader.Close();
                //Grand total line
                //sb.Append("<tr><td style='font-size:smaller' colspan=9 align=right>Total:</td><td style='font-size:smaller;font-weight:bold' align=left>$");
                //sb.Append(grandTotal);
                //sb.Append("</td></tr></table>");
                sb.Append("</table>");

                sb.Append("</td></tr><tr><td>&nbsp</td></tr></table>");


                ////
                sb.Append("<br />");
                sb.Append("<table cellspacing=1 cellpadding=1 bgcolor=gray width='100%'>");
                sb.Append("<tr><td><table bgcolor=white width='100%'>");
                sb.Append("<tr><td align=right style='font-size:small'>Patient Name:</td>");
                sb.Append("<td  style='font-size:small'>");
                sb.Append(patientCode);
                sb.Append("</td>");
                sb.Append("<td  align=right style='font-size:small'>Physician: </td>");
                sb.Append("<td  style='font-size:small'>");
                sb.Append(physicianName);
                sb.Append("</td>");
                sb.Append("<td  style='font-size:small'>Surgery?<br />Yes/No</td>");
                sb.Append("</tr>");
                sb.Append("<tr><td colspan=5>&nbsp</td></tr>");
				sb.Append("<tr><td  align=right style='font-size:small'>Dx Code:</td>");
                sb.Append("<td  style='font-size:small'>");
                sb.Append(icd9Code);
                sb.Append("</td>");
                sb.Append("<td  align=right style='font-size:small'>Insurance: </td>");
                sb.Append("<td  colspan=2 align=left style='font-size:small'>____________________________</td></tr>");
                sb.Append("<tr><td colspan=5>&nbsp</td></tr>");
                sb.Append("<tr><td colspan=5 style=' font-size:small; font-weight:bold'>Durable Medical Equipment and Orthotics Patient Consent</td></tr>");
                sb.Append("<tr><td colspan=5>&nbsp</td></tr>");
                sb.Append("<tr><td align=left colspan=5 style='font-size:small'>- I understand that my physician has prescribed this medical supply as part of my treatment plan </td></tr>");
                sb.Append("<tr><td align=left colspan=5 style='font-size:small'>- I understand that I have a choice in where I receive my prescribed orthopaedic supplies and services</td></tr>");
				sb.Append("<tr><td align=left colspan=5 style='font-size:small'>- I authorize this orthopedic practice to furnish this service/product and to provide my insurance provider with any information requested for payment</td></tr>");
				if (Practice.IsBregBilling)
				{
					sb.Append("<tr><td align=left colspan=5 style='font-size:small'>- Your services/supplies may be provided and/or billed through any of Breg's family of companies including Breg, Inc. and certain other Breg Affiliates</td></tr>");
				}
				sb.Append("<tr><td align=left colspan=5 style='font-size:small'>- I instruct my insurance provider to pay this orthopedic practice directly for these services/products</td></tr>");
                sb.Append("<tr><td align=left colspan=5 style='font-size:small'>- I understand that my insurance company may deny payment for this supply because it is a non-covered item or deemed not medically necessary</td></tr>");
                sb.Append("<tr><td align=left colspan=5 style='font-size:small'>- I understand that I am fully responsible for any deductible or co-insurance cost related to this service/supply</td></tr>");
                sb.Append("<tr><td align=left colspan=5 style='font-size:small'>- I understand any costs not covered by my insurance provider will be my financial responsibility</td></tr>");
                sb.Append("<tr><td align=left colspan=5 style='font-size:small'>- I have received the prescribed item and have been fully instructed on the use of the above products/services</td></tr>");
				sb.Append("<tr><td align=left colspan=5 style='font-size:small'>- I understand that all medical devices are not returnable unless there is a material defect</td></tr>");
				if (Practice.IsBregBilling)
				{
					sb.Append("<tr><td align=left colspan=5 style='font-size:small'>- I have received and reviewed the Patient Information Sheet with Deposit, Return Policy, Warranty Information, Fitting/Safety Instructions, Notice of Privacy Practices, Patient Bill of Rights, Contact and Supplier Information, and Supplier Standards (if applicable)</td></tr>");
				}
				sb.Append("<tr><td colspan=5>&nbsp</td></tr>");
                sb.Append("<tr><td  align=right style='font-size:small'>Patient/Guarantor Signature: </td><td  colspan=4 align=left style='font-size:small'>_____________________________________</td></tr>");
                sb.Append("<tr><td colspan=5>&nbsp</td></tr>");
                sb.Append("<tr><td  align=right style='font-size:small'>Date: </td><td  colspan=4 align=left style='font-size:small'>_____________________________________ </td>");
                sb.Append("</tr><tr><td colspan=5>&nbsp</td></tr>");
                sb.Append("<tr><td  align=right style='font-size:small'>Service Refusal: </td><td  colspan=4 style='font-size:small'></td></tr>");
                sb.Append("<tr><td  align=right style='font-size:small'></td>");
                sb.Append("<td  colspan=4 align=left style='font-size:small'>I have decided not to receive this item from this orthopedic practice </td></tr>");
                sb.Append("<tr><td  align=right style='font-size:small'></td><td  colspan=4 align=left style='font-size:small'>I understand that my physician has prescribed this item as part of my treatment</td></tr>");
                sb.Append("<tr><td colspan=5>&nbsp</td></tr>");
                sb.Append("<tr><td  align=right style='font-size:small'>Patient/Guarantor Signature: </td>");
                sb.Append("<td  colspan=4 align=left style='font-size:small'>_____________________________________</td></tr>");
                sb.Append("<tr><td colspan=5>&nbsp</td></tr>");
                sb.Append("<tr><td  align=right style='font-size:small'>Date: </td>");
                sb.Append("<td  colspan=4 align=left style='font-size:small'>_____________________________________</td>");
                sb.Append("</tr><tr><td colspan=5>&nbsp</td></tr>");
                sb.Append("</table></td></tr></table>");

                ////


            }

            // add receipt to container
            var receipt = new HtmlGenericControl();
            receipt.InnerHtml = sb.ToString();
            receipt_container.Controls.Add(receipt);

            // check if order was a custom brace order and attach custom brace details
            AttachCustomBraceDetails(receipt_container);
        }

        private void AttachCustomBraceDetails(HtmlGenericControl receipt_container)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                // look up BregVisionOrderID from dispenseID (this must exist)
                var d = visionDB.Dispenses.Where(x => x.DispenseID == DispenseID).First();

                // loop through dispense details and grab dispense queue
                foreach (var detail in d.DispenseDetails)
                {
                    // skip over details that have no dispense queue id
                    if (!detail.DispenseQueueID.HasValue)
                        continue;
                    // load dispense queue (only consider queue with BregVisionOrderID)
                    var dq =
                        visionDB.DispenseQueues
                            .Where(
                                x =>
                                    (x.DispenseQueueID == detail.DispenseQueueID.Value)
                                    && x.BregVisionOrderID.HasValue)
                            .Select(x => new { BregVisionOrderID = x.BregVisionOrderID.Value })
                            .FirstOrDefault();
                    // add control to page if there was a match
                    if ((dq != null)
                        && visionDB.ShoppingCartCustomBraces.Any(x => x.BregVisionOrderID.Value == dq.BregVisionOrderID))
                    {
                        var ctl = Page.LoadControl("~/UserControls/DispensePage/CustomBraceJPG.ascx") as UserControls.DispensePage.CustomBrace;
                        ctl.BregVisionOrderID = dq.BregVisionOrderID;
                        //ctl.IsReadOnly = true;
                        receipt_container.Controls.Add(ctl);
                    }
                }
            }
        }
    }
}