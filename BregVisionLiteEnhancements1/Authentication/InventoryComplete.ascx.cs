﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BregVision.Authentication
{
    public partial class InventoryComplete : System.Web.UI.UserControl
    {
        public string PracticeName { get; set; }
        public string PracticeLocationName { get; set; }
        public int PracticeLocationID { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LoadControlInfo()
        {
            lblPractice.Text = PracticeName;
            lblPracticeLocation.Text = PracticeLocationName;

            grdProducts.DataSource = BregWcfService.ConsignmentItem.GetConsignmentReplenishmentList(PracticeLocationID);
            grdProducts.DataBind();
        }
    }
}