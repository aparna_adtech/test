using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using ClassLibrary.DAL;

namespace BregVision.Authentication
{
    public partial class DispenseReceipt : Bases.PageBase
    {
        
        protected void Page_Init(object sender, EventArgs e)
        {
            //VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            try
            {

                if (Request.QueryString["DispenseID"].Length > 0)
                {
                    bool IsFaxResend = (Request.QueryString["faxresend"] != null);

                    string strDispenseID = Server.UrlDecode(Request.QueryString["DispenseID"].ToString());
                    string[] DispenseIDs = strDispenseID.Split(new Char[] { '|' });

                    //int[] intDispenseIDs = (from d in DispenseIDs
                    //          select Convert.ToInt32(d)).ToArray<int>();

                    int[] intDispenseIDs = DispenseIDs.Select(d => Convert.ToInt32(d)).ToArray<int>();

                    //int[] intDispenseIDs = DispenseIDs.Cast<int>().ToArray<int>();
                    byte[] CompressedPDF;
                    System.Text.StringBuilder html = BregWcfService.Classes.Email.SendPatientEmail(
                                                                                                    intDispenseIDs, 
                                                                                                    practiceLocationID, null,out CompressedPDF,
                                                                                                    fromVision: true, 
                                                                                                    createPDF: false, 
                                                                                                    sendEmail: false,
                                                                                                    hidePrintButton: IsFaxResend,
                                                                                                    sendFax: false,
                                                                                                    queueOutboundMessages: false);


                    litHTML.Text = html.ToString();
                }

            }
            catch(Exception ex)
            {
                throw ex;
            }
            
            
        }
    }
}
