﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InventoryComplete.ascx.cs" Inherits="BregVision.Authentication.InventoryComplete" %>

Hi Debbie,
<br /><br />
<p>A consignment inventory count was just completed by <asp:Label ID="lblPracticeLocation" runat="server"></asp:Label>  Location of  <asp:Label ID="lblPractice" runat="server"></asp:Label> Practice</p>

<br />
<asp:DataGrid ID="grdProducts" runat="server" AutoGenerateColumns="false">
    <Columns>
        <asp:BoundColumn DataField="Product" ItemStyle-Width="400" HeaderText="Product"></asp:BoundColumn>
        <asp:BoundColumn DataField="Code" HeaderText="Code"></asp:BoundColumn>
        <asp:BoundColumn DataField="Side" HeaderText="Side"></asp:BoundColumn>
        <asp:BoundColumn DataField="Size" HeaderText="Size"></asp:BoundColumn>
        <asp:BoundColumn DataField="Gender" HeaderText="Gender"></asp:BoundColumn>
        <asp:BoundColumn DataField="WholesaleCost" HeaderText="Wholesale Cost" DataFormatString="{0:C}"></asp:BoundColumn>
        <asp:BoundColumn DataField="ConsignmentQuantity" HeaderText="Consignment Quantity"></asp:BoundColumn>
        <asp:BoundColumn DataField="QOH" HeaderText="QOH"></asp:BoundColumn>
        <asp:BoundColumn DataField="QuantityOnOrder" HeaderText="Quantity On Order"></asp:BoundColumn>
        <asp:BoundColumn DataField="CalculatedReOrderQuantity" HeaderText="Reorder Quantity"></asp:BoundColumn>
    </Columns>
</asp:DataGrid>
<br />
The Vision System
<br />