using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace BregVision.Authentication
{
    public partial class AUOrderApproval : System.Web.UI.Page
    {
       protected void Page_Load(object sender, EventArgs e)
        {
            //Session["OrderApprovalPage"] = Request.QueryString["OrderApprovalPage"];
            Session["OrderApprovalQueryString"] = Request.QueryString["PracticeLocationID"];
            Session["ShippingTypeID"] = Request.QueryString["ShippingTypeID"];
            Response.Write("User Authenticated:");
            Response.Write(User.Identity.IsAuthenticated);
            Response.Write(Context.User.Identity.Name.ToString());
           if (User.Identity.IsAuthenticated == false)
            {
                Response.Write("User not authenticated"); 
               Response.Redirect("../default.aspx");
            
            }
            else
                //if (Roles.IsUserInRole(User.Identity.Name, "BregAdmin") || Roles.IsUserInRole(User.Identity.Name, "PracticeAdmin"))
                //{
                    //Process Order and create Purchase Order
                    Response.Write("Order Approval"); 
                    Response.Redirect("../OrderApproval.aspx");
               // }
        }
    
    }
}
