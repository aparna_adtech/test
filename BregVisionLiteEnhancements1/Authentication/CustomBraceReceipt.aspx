﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomBraceReceipt.aspx.cs" Inherits="BregVision.Authentication.CustomBraceReceipt" %>
<%@ Register Src="../UserControls/DispensePage/CustomBraceJPG.ascx" TagName="CustomBrace" TagPrefix="bvu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Receipt</title>
</head>
<body>
    <form id="form1" runat="server">
	<asp:HiddenField ID="ParentBregVisionOrderID" runat="server" />
    <div>
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Path="~/Include/jquery/js/jquery-1.4.4.min.js" />
            </Scripts>
        </telerik:RadScriptManager>
        <bvu:CustomBrace ID="CustomBrace" runat="server" BregVisionOrderID='<%# BregVisionOrderID %>' />
    </div>
    </form>
</body>
</html>
