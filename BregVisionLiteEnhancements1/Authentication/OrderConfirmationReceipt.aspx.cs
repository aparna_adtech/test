using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace BregVision.Authentication
{
    public partial class OrderConfirmationReceipt : System.Web.UI.Page
    {
		#region properties

		public int? BregVisionOrderID
		{
			get
			{
				string bvoID = ParentBregVisionOrderID.Value;
				int bvoIDVal = 0;
				if (Int32.TryParse(bvoID, out bvoIDVal))
					return bvoIDVal;
				return 0;
			}
			set
			{
				ParentBregVisionOrderID.Value = value.ToString();
			}
		}

		#endregion

		protected bool IsCustomBrace { get; set; }

		protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["BVOID"].Length > 0)
                {
                    var BVOID = Convert.ToInt32(Request.QueryString["BVOID"].ToString());
					BregVisionOrderID = BVOID;
					CustomBrace.DataBind();

                    FaxEmailPOs FEPos = new FaxEmailPOs();
                    //Creates Order Confirmation data
                    string PODetail = FEPos.GetSuppliersAndLineItems(BVOID);
                    lblPODetail.Text = PODetail;

					//send this string into "PrepareFaxEmailforSupplier" and append it to the string builder
					DataSet dsCustomBraceInfo = DAL.PracticeLocation.Inventory.GetCustomBraceExtendedInfoFromOrder(null, BVOID);
					if (dsCustomBraceInfo.Tables[0].Rows.Count == 1)
						IsCustomBrace = true;
				} 
            }
            #region Exception Handler
            catch (Exception ex)
            {
                BregVision.ErrHandler.VisionErrHandler.RaiseError(ex);
            }
            #endregion
        }
    }
}
