using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace BregVision.Authentication
{
    public partial class AuthenticateUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["GoToPage"] = Request.QueryString["CodePage"];
            Session["GoToPageQueryString"] = Request.QueryString["PracticeLocationID"];
            if (User.Identity.IsAuthenticated == false)
            {
                Response.Redirect("../default.aspx");
            }
        }
    }
}
