﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BregVision.Authentication
{
    public partial class CustomBraceReceipt : System.Web.UI.Page
    {
		#region properties

		public int? BregVisionOrderID
		{
			get
			{
				string bvoID = ParentBregVisionOrderID.Value;
				int bvoIDVal = 0;
				if (Int32.TryParse(bvoID, out bvoIDVal))
					return bvoIDVal;
				return 0;
			}
			set
			{
				ParentBregVisionOrderID.Value = value.ToString();
			}
		}

		#endregion

		protected void Page_Load(object sender, EventArgs e)
        {
            CustomBrace.BregVisionOrderID = Convert.ToInt32(Request["i"].ToString());
        }
    }
}