﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ABNFormCtl.ascx.cs" Inherits="BregVision.Authentication.ABNFormCtl" %>

<style type="text/css">
 
span.cls_111{font-family:Times New Roman;font-size:20px;color:rgb(0,0,0);font-weight:normal;font-style:normal} 
 
span.cls_002{font-family:Arial,serif;font-size:17px;color:rgb(0,0,0);font-weight:bold;font-style:italic}
 
div.cls_002{font-family:Arial,serif;font-size:17px;color:rgb(0,0,0);font-weight:bold;font-style:italic}
 
span.cls_003{font-family:Arial,serif;font-size:16px;color:rgb(0,0,0);font-weight:bold;font-style:normal}
 
div.cls_003{font-family:Arial,serif;font-size:16px;color:rgb(0,0,0);font-weight:bold;font-style:normal}
 
/*span.cls_004{font-family:Arial,serif;font-size:15px;color:rgb(0,0,0);font-weight:bold;font-style:italic}*/
 
/*div.cls_004{font-family:Arial,serif;font-size:15px;color:rgb(0,0,0);font-weight:bold;font-style:italic}*/
 
span.cls_005{font-family:Arial,serif;font-size:16px;color:rgb(0,0,0);font-weight:normal;font-style:normal}
 
div.cls_005{font-family:Arial,serif;font-size:16px;color:rgb(0,0,0);font-weight:normal;font-style:normal}
 
span.cls_006{font-family:Arial,serif;font-size:24px;color:rgb(0,0,0);font-weight:bold;font-style:normal}
 
div.cls_006{font-family:Arial,serif;font-size:24px;color:rgb(0,0,0);font-weight:bold;font-style:normal}
 
span.cls_007{font-family:Arial,serif;font-size:17px;color:rgb(0,0,0);font-weight:bold;font-style:normal}
 
div.cls_007{font-family:Arial,serif;font-size:17px;color:rgb(0,0,0);font-weight:bold;font-style:normal}
 
/*span.cls_008{font-family:Arial,serif;font-size:19px;color:rgb(0,0,0);font-weight:normal;font-style:normal}*/
 
/*div.cls_008{font-family:Arial,serif;font-size:19px;color:rgb(0,0,0);font-weight:normal;font-style:normal}*/
 
span.cls_009{font-family:Arial,serif;font-size:18px;color:rgb(0,0,0);font-weight:bold;font-style:italic}
 
div.cls_009{font-family:Arial,serif;font-size:18px;color:rgb(0,0,0);font-weight:bold;font-style:italic}
 
/*span.cls_011{font-family:Arial,serif;font-size:17px;color:rgb(0,0,0);font-weight:normal;font-style:italic}*/
 
/*div.cls_011{font-family:Arial,serif;font-size:17px;color:rgb(0,0,0);font-weight:normal;font-style:italic}*/
 
span.cls_012{font-family:Arial,serif;font-size:19px;color:rgb(0,0,0);font-weight:bold;font-style:normal}
 
div.cls_012{font-family:Arial,serif;font-size:19px;color:rgb(0,0,0);font-weight:bold;font-style:normal}
 
span.cls_013{font-family:Arial,serif;font-size:15px;color:rgb(0,0,0);font-weight:bold;font-style:normal}
 
div.cls_013{font-family:Arial,serif;font-size:15px;color:rgb(0,0,0);font-weight:bold;font-style:normal}
 
span.cls_014{font-family:Times,serif;font-size:17px;color:rgb(0,0,0);font-weight:normal;font-style:normal}
 
div.cls_014{font-family:Times,serif;font-size:17px;color:rgb(0,0,0);font-weight:normal;font-style:normal}
 
span.cls_015{font-family:Times,serif;font-size:21px;color:rgb(0,0,0);font-weight:bold;font-style:normal}
 
div.cls_015{font-family:Times,serif;font-size:21px;color:rgb(0,0,0);font-weight:bold;font-style:normal}
 
span.cls_016{font-family:Arial,serif;font-size:21px;color:rgb(0,0,0);font-weight:bold;font-style:normal}
 
div.cls_016{font-family:Arial,serif;font-size:21px;color:rgb(0,0,0);font-weight:bold;font-style:normal}
 
span.cls_018{font-family:"MS Mincho",serif;font-size:21px;color:rgb(0,0,0);font-weight:normal;font-style:normal}
 
div.cls_018{font-family:"MS Mincho",serif;font-size:21px;color:rgb(0,0,0);font-weight:normal;font-style:normal}
 
/*span.cls_019{font-family:Times,serif;font-size:23px;color:rgb(0,0,0);font-weight:bold;font-style:normal}*/
 
/*div.cls_019{font-family:Times,serif;font-size:25px;color:rgb(0,0,0);font-weight:bold;font-style:normal}*/
 
/*span.cls_020{font-family:Times,serif;font-size:19px;color:rgb(0,0,0);font-weight:bold;font-style:normal}*/
 
/*div.cls_020{font-family:Times,serif;font-size:19px;color:rgb(0,0,0);font-weight:bold;font-style:normal}*/
 
/*span.cls_021{font-family:Times,serif;font-size:25px;color:rgb(0,0,0);font-weight:bold;font-style:normal}*/
 
/*div.cls_021{font-family:Times,serif;font-size:25px;color:rgb(0,0,0);font-weight:bold;font-style:normal}*/
 
/*span.cls_023{font-family:Arial,serif;font-size:21px;color:rgb(0,0,0);font-weight:normal;font-style:normal}*/
 
/*div.cls_023{font-family:Arial,serif;font-size:21px;color:rgb(0,0,0);font-weight:normal;font-style:normal}*/
 
span.cls_025{font-family:Times,serif;font-size:10px;color:rgb(0,0,0);font-weight:normal;font-style:normal}
 
div.cls_025{font-family:Times,serif;font-size:10px;color:rgb(0,0,0);font-weight:normal;font-style:normal}
 
/*span.cls_027{font-family:Times,serif;font-size:17px;color:rgb(0,0,0);font-weight:bold;font-style:normal}*/
 
/*div.cls_027{font-family:Times,serif;font-size:17px;color:rgb(0,0,0);font-weight:bold;font-style:normal}*/
 
/*span.cls_028{font-family:Times,serif;font-size:17px;color:rgb(0,0,255);font-weight:normal;font-style:normal}*/
 
/*div.cls_028{font-family:Times,serif;font-size:17px;color:rgb(0,0,255);font-weight:normal;font-style:normal}*/
 
/*span.cls_029{font-family:Times,serif;font-size:17px;color:rgb(0,0,0);font-weight:bold;font-style:italic}*/
 
/* div.cls_029{font-family:Times,serif;font-size:17px;color:rgb(0,0,0);font-weight:bold;font-style:italic}*/
 
 </style>
<div style="page-break-before:always; margin:0 0 0 0" >

<table width="100%"> 
    <tr>
        <td>
            <span class="cls_003"><asp:Label ID="lblPracticeName" runat="server"></asp:Label></span>
        </td>

        <td>
            <span class="cls_003"><asp:Label ID="lblAddress1" runat="server"></asp:Label></span>
        </td>

        <td>
            <span class="cls_003"><asp:Label ID="lblAddress2" runat="server"></asp:Label></span>
        </td>

        <td>
            <span class="cls_003"><asp:Label ID="lblCity" runat="server"></asp:Label>,&nbsp;<asp:Label ID="lblState" runat="server"></asp:Label>&nbsp;&nbsp;<asp:Label ID="lblZip" runat="server"></asp:Label></span>
        </td>

        <td colspan="2" width="200px">
            <span class="cls_003"><asp:Label ID="lblPhone" runat="server"></asp:Label></span><br />

            <span class="cls_003"><asp:Label ID="lblFax" runat="server"></asp:Label></span>
        </td>
    </tr>                
</table>
<table width="100%" border="0">
    <tr>
        <td>
            <div class="cls_002">A.<span class="cls_003">Notifier:</span></div>
        </td>
        <td colspan="2">              
        </td>
    </tr>
    <tr>
        <td>
            <div class="cls_002">B.<span class="cls_003">Patient Name:</span></div>
        </td>
        <td> 
            <span class="cls_111"><asp:Label ID="lblPatientCode" runat="server"></asp:Label> </span>     
        </td>
        <td>
            <div class="cls_002">C.<span class="cls_003">Identification Number:</span></div>
        </td>
        <%--<td>       
        </td>--%>
    </tr>
    <tr>
        <td colspan="3">
            <hr size="6" noshade="noshade" />
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <div class="cls_006">A<span class="cls_007">DVANCE </span><span class="cls_006">B</span><span class="cls_007">ENEFICIARY </span><span class="cls_006">N</span><span class="cls_007">OTICE OF </span><span class="cls_006">N</span><span class="cls_007">ONCOVERAGE </span><span class="cls_006">(ABN)</span></div>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="left">
            <div class="cls_002"><u>NOTE:</u><span class="cls_005">  If Medicare doesn’t pay for </span><span class="cls_002">D.</span><span class="cls_005">&nbsp;&nbsp;&nbsp;&nbsp;ITEM</span><span class="cls_005">____________ below, you may have to pay.</span></div>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="left">
            <div class="cls_005">Medicare does not pay for everything, even some care that you or your health care provider have</div>
            <div class="cls_005">good reason to think you need. We expect Medicare may not pay for the <span class="cls_002">D.</span><span class="cls_005">&nbsp;&nbsp;&nbsp;&nbsp;ITEM</span><span class="cls_015">____________</span><span class="cls_005"> below.</span></div>
        </td>
    </tr>
</table>
<table width="100%" cellpadding="5" cellspacing="0"  style="border:1px solid black">
<col width="254px">
 <col width="350px">
 <col width="244px">
    <tr style="background-color:Silver">
        <td style="border-bottom:1px solid black;border-right:1px solid black">
            <div class="cls_002">D.<span class="cls_003">&nbsp;&nbsp;&nbsp;&nbsp;ITEM</span></div>
        </td>
        <td style="border-bottom:1px solid black">
            <div class="cls_002">E.<span class="cls_003"> Reason Medicare May Not Pay:</span></div>
        </td>
        <td style="border-bottom:1px solid black;border-left:1px solid black">
            <div class="cls_002">F.<span class="cls_003"> Estimated</span></div><div class="cls_003">Cost:</div>

        </td>
    </tr>
    <tr>
        <td valign="top" style="border-right:1px solid black">&nbsp;<br /><span class="cls_005"><asp:Label ID="lblProductName" runat="server"></asp:Label></span><br /><br /></td>
        <td valign="top" style="border-right:1px solid black"><br /><span class="cls_005"></span><br /><br /></td>
        <td valign="top"><br /><span class="cls_005"><asp:Label ID="lblCost" runat="server"></asp:Label></span><br /><br /></td>
    </tr>
</table>
<div class="cls_012">W<span class="cls_013">HAT YOU NEED TO DO NOW</span><span class="cls_012">:</span></div>
 
<div class="cls_014" style="margin-left: 15px">•<span class="cls_005">  Read this notice, so you can make an informed decision about your care.</span></div>
 
<div class="cls_014" style="margin-left: 15px">•<span class="cls_005">  Ask us any questions that you may have after you finish reading.</span></div>
 
<div class="cls_014" style="margin-left: 15px">•<span class="cls_005">  Choose an option below about whether to receive the </span><span class="cls_002">D._____</span><span class="cls_005">&nbsp;&nbsp;&nbsp;&nbsp;ITEM</span><span class="cls_005">____________listed above.</span></div>
 
<div class="cls_003" style="margin-left: 35px">Note:  <span class="cls_005">If you choose Option 1 or 2, we may help you to use any other</span></div>
 
<div class="cls_005" style="margin-left: 85px">insurance that you might have, but Medicare cannot require us to do this.</div>

<table width="100%" cellpadding="5" cellspacing="0" style="border:1px solid black">
    <tr style="background-color:Silver">
        <td  style="border-bottom:1px solid black">
            <div class="cls_009">G.<span class="cls_015"> </span><span class="cls_016">O</span><span class="cls_007">PTIONS</span><span class="cls_016">:</span></div>
            <div class="cls_003">Check only one box.  We cannot choose a box for you.</div>
        </td>    
    </tr>
    <tr>
        <td>
            <div class="cls_018">❏<span class="cls_016">OPTION 1.</span><span class="cls_005"> I want the </span><span class="cls_002">D.</span><span class="cls_005">&nbsp;&nbsp;&nbsp;&nbsp;ITEM</span><span class="cls_005">__________ listed above.  You may ask to be paid now, but I</span></div>
            <div class="cls_005">also want Medicare billed for an official decision on payment, which is sent to me on a Medicare</div>
            <div class="cls_005">Summary Notice (MSN).  I understand that if Medicare doesn’t pay, I am responsible for</div>
            <div class="cls_005">payment, but <span class="cls_003">I can appeal to Medicare </span><span class="cls_005">by following the directions on the MSN</span><span class="cls_003">.  </span><span class="cls_005">If Medicare</span></div>
            <div class="cls_005">does pay, you will refund any payments I made to you, less co-pays or deductibles.</div>
            <div class="cls_018">❏<span class="cls_016">OPTION 2.</span><span class="cls_005"> I want the </span><span class="cls_002">D.</span><span class="cls_005">&nbsp;&nbsp;&nbsp;&nbsp;ITEM</span><span class="cls_005">__________ listed above, but do not bill Medicare.  You may</span></div>
            <div class="cls_005">ask to be paid now as I am responsible for payment. <span class="cls_003">I cannot appeal if Medicare is not billed</span><span class="cls_005">.</span></div>
            <div class="cls_018">❏<span class="cls_016">OPTION 3. </span><span class="cls_005"> I don’t want the </span><span class="cls_002">D.</span><span class="cls_005">&nbsp;&nbsp;&nbsp;&nbsp;ITEM</span><span class="cls_005">__________</span><span class="cls_005">listed above.  I understand with this choice</span></div>
            <div class="cls_005">I am <span class="cls_003">not</span><span class="cls_005"> responsible for payment, </span><span class="cls_005">and</span><span class="cls_016"> </span><span class="cls_003">I cannot appeal to see if Medicare would pay.</span></div>
        </td>    
    </tr>
</table>
<div class="cls_002" style="margin-bottom: 4px">H.<span class="cls_005"> </span><span class="cls_003">Additional Information:</span></div>
<div class="cls_003">This notice gives our opinion, not an official Medicare decision.<span class="cls_005">  If you have other questions</span></div>
<div class="cls_005">on this notice or Medicare billing, call <span class="cls_003">1-800-MEDICARE</span><span class="cls_005"> (1-800-633-4227/</span><span class="cls_003">TTY</span><span class="cls_005">: 1-877-486-2048).</span></div>
<div class="cls_005">Signing below means that you have received and understand this notice. You also receive a copy.</div>
<table width="100%" cellpadding="5" cellspacing="0"  style="border:1px solid black">
    <tr>
        <td  style="border-right:1px solid black">
            <div class="cls_002">I.<span class="cls_003"> Signature:</span></div>
            <br />
        </td>
        <td>
            <div class="cls_002">J.<span class="cls_003"> Date:</span></div>
            <br />
        </td>
    </tr>
</table>
<div class="cls_025">According to the Paperwork Reduction Act of 1995, no persons are required to respond to a collection of information unless it displays a valid OMB control</div>
<div class="cls_025">number.  The valid OMB control number for this information collection is 0938-0566.  The time required to complete this information collection is estimated to</div>
<div class="cls_025">average 7 minutes per response, including the time to review instructions, search existing data resources, gather the data needed, and complete and review the</div>
<div class="cls_025">information collection.   If you have comments concerning the accuracy of the time estimate or suggestions for improving this form, please write to: CMS, 7500</div>
<div class="cls_025">Security Boulevard, Attn: PRA Reports Clearance Officer, Baltimore, Maryland 21244-1850.</div>
<hr />
<table width="100%" cellpadding="0" cellspacing="0" border="0">
    <%--<tr valign="top">
        <td colspan="2"><hr /></td>
    </tr>--%>
    <tr valign="top">
        <td valign="top">
            <div class="cls_014">Form CMS-R-131 (03/11)</div>
        </td>
        <td valign="top">
            <div class="cls_014">Form Approved OMB No. 0938-0566</div>
        </td>
    </tr>
</table>
</div>