﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary.DAL;

namespace BregVision.Authentication
{
    public partial class ABNFormCtl : System.Web.UI.UserControl
    {
        public int PracticeLocationID { get; set; }
        public string PracticeLocationName { get; set; }
        public string PracticeName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string BillingPhone { get; set; }
        public string BillingFax { get; set; }

        public string ProductName { get; set; }
        public Decimal ProductCost { get; set; }

        public string PatientCode { get; set; }
        

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadControlInfo();
        }

        public void LoadControlInfo()
        {
            lblPracticeName.Text = PracticeName.ToString();
            lblAddress1.Text = Address1.ToString();
            lblAddress2.Text = Address2.ToString();
            lblCity.Text = City.ToString();
            lblState.Text = State.ToString();
            lblZip.Text = Zip.ToString();
            if (!string.IsNullOrEmpty(BillingPhone))
            {
                lblPhone.Text = "Ph" + (BillingPhone.Length == 10 ? "(" + BillingPhone.Substring(0, 3) + ")" + BillingPhone.Substring(3, 3) + "-" + BillingPhone.Substring(6, 4) : BillingPhone.ToString());
            }
            if (!string.IsNullOrEmpty(BillingFax))
            {
                lblFax.Text = "F " + (BillingFax.Length == 10 ? "(" + BillingFax.Substring(0, 3) + ")" + BillingFax.Substring(3, 3) + "-" + BillingFax.Substring(6, 4) : BillingFax.ToString());
            }
            lblProductName.Text = ProductName;
            lblCost.Text = ProductCost.ToString("c");
            lblPatientCode.Text = PatientCode;
        }
    }
}