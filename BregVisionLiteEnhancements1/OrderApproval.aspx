<%@ Page Language="C#" MasterPageFile="~/MasterPages/SiteMaster.Master" AutoEventWireup="true" CodeBehind="OrderApproval.aspx.cs" Inherits="BregVision.OrderApproval" Title="Untitled Page" %>
<%@ Register Assembly="RadGrid.Net2" Namespace="Telerik.WebControls" TagPrefix="radG" %>

 <asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">


<table width="96%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="4" class="AdminPageTitle">
        Order Approval
        </td>
     </tr>
  <tr>
    <td></td>
    <td colspan="2">
        <asp:Label id="lblStatus" Font-Size="Small" style="color:Red" runat="server"></asp:Label>
    </td>
    <td>
    </td>
  </tr>
    <tr>
        <td></td>
        <td colspan="2">
            <div runat="server" id="divOrderDetails">
            <table width="98%" cellpadding="0" cellspacing="0" border="0">
                  <tr>
                    <td width="21">&nbsp;</td>
                    <td class="TextMed">A purchase order request has been submitted for your approval. Please select whether you want to approve or deny the purchase order request.</td>
                    <td class="TextMed"><asp:RadioButtonList ID="radOrderStatus" runat="server">
                    <asp:ListItem Text="Approved" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Denied" Value="1"></asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:Button runat="server" id="btnSubmit" Text="Submit" OnClick="btnSubmit_Click"/>
                    </td>
                    <td width="21">&nbsp;</td>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           

                  </tr>
                  <tr>
  	                <td colspan="1">
  	                </td>
                    <td colspan="2" class="TextMed">Comments (if order denied) &nbsp;  &nbsp;
                        <textarea id="txtComments" rows="2" style="width: 390px" runat="server"></textarea></td>
                    <td></td>
                  </tr> 
                  <tr>
  	                <td colspan="1">
  	                </td>
                    <td colspan="2" class="TextMed">Order details are below</td>
                    <td></td>
                 
                  </tr>
                </table>
            </div>        
          </td>
          <td></td>
        </tr>
        <tr>
    	<td colspan="1">
  	    </td>
        <td colspan="2" align="center">
       <table width="98%">
        <tr>
         <td>
                     <radG:RadGrid ID="grdInvoiceSnapshot" runat="server" GridLines="None" 
                        OnNeedDataSource="grdInvoiceSnapshot_NeedDataSource"

                        OnDetailTableDataBind="grdInvoiceSnapshot_DetailTableDataBind" 
                        OnItemDataBound="grdInvoiceSnapshot_ItemDataBound" 
                        EnableAJAX="True" EnableAJAXLoadingTemplate="True" 
                        LoadingTemplateTransparency="10" AllowPaging="True" AllowSorting="True" ShowGroupPanel="True" Skin="Office2007" 
                        Width="98%" AllowMultiRowSelection="True" EnableViewState="True">
                        
                        <MasterTableView DataKeyNames="PracticeCatalogSupplierBrandID" AutoGenerateColumns="False" EnableTheming="true" ShowFooter="true">
                        
                            <ExpandCollapseColumn>
                                <HeaderStyle Width="19px" />
                            </ExpandCollapseColumn>
                        
                            <DetailTables>
                                <radG:GridTableView runat="server" DataKeyNames="PracticeCatalogSupplierBrandID" AutoGenerateColumns="False" Width="100%" ShowFooter="true">
                                    <ParentTableRelation>
                                      <radG:GridRelationFields DetailKeyField="PracticeCatalogSupplierBrandID" MasterKeyField="PracticeCatalogSupplierBrandID" />
                                    </ParentTableRelation>
                                    <ExpandCollapseColumn Visible="False">
                                        <HeaderStyle Width="19px" />
                                    </ExpandCollapseColumn>
                                    <RowIndicatorColumn Visible="False">
                                        <HeaderStyle Width="20px" />
                                    </RowIndicatorColumn>
                                    <Columns>
                                        

                                        <radG:GridBoundColumn DataField="PracticeCatalogProductID" UniqueName="PracticeCatalogProductID" Visible="False">
                                        </radG:GridBoundColumn>
                                         <radG:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" UniqueName="PracticeCatalogSupplierBrandID" Visible="False">
                                        </radG:GridBoundColumn>
                                                                                
                                        <radG:GridBoundColumn DataField="BrandShortName" HeaderText="Brand" UniqueName="BrandShortName">
                                        </radG:GridBoundColumn>
                                        <radG:GridBoundColumn DataField="ShortName" HeaderText="Product" UniqueName="ShortName">
                                        </radG:GridBoundColumn>
                                        <radG:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code">
                                        </radG:GridBoundColumn>
                                        <radG:GridBoundColumn DataField="Packaging" HeaderText="Pkg." UniqueName="Packaging">
                                        </radG:GridBoundColumn>
                                        
                                         <radG:GridBoundColumn DataField="LeftRightSide" HeaderText="Side." UniqueName="LeftRightSide">
                                        </radG:GridBoundColumn>
                                         <radG:GridBoundColumn DataField="Size" HeaderText="Size" UniqueName="Size">
                                        </radG:GridBoundColumn>
                                         <radG:GridBoundColumn DataField="Color" HeaderText="Color" UniqueName="Color">
                                        </radG:GridBoundColumn>
                                         <radG:GridBoundColumn DataField="Gender" HeaderText="Gender" UniqueName="Gender">
                                        </radG:GridBoundColumn>
                                         <radG:GridBoundColumn DataField="ActualWholesaleCost" DataType="System.Decimal" DataFormatString="{0:$#,#.00}" HeaderText="AW Cost" UniqueName="ActualWholesaleCost">
                                        </radG:GridBoundColumn>
                                           <radG:GridBoundColumn DataField="Quantity" HeaderText="Qty." UniqueName="Quantity">
                                        </radG:GridBoundColumn>
                                         <radG:GridTemplateColumn HeaderText="Line Total" UniqueName="LineTotal">
                                             <ItemTemplate>

                                                  <asp:Label id="lblLineTotal"  Text='<%# bind("TotalCost", "{0:$#,#.00}")%>' runat="server"></asp:Label>
                                             </ItemTemplate>
                            
                                        </radG:GridTemplateColumn>
                                        
                                        
                                                                               
                                    </Columns>


                                </radG:GridTableView>
                            </DetailTables>
                        
                        
                            <RowIndicatorColumn Visible="False">
                                <HeaderStyle Width="20px" />
                            </RowIndicatorColumn>
                        
                            <Columns>
                                <radG:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" UniqueName="PracticeCatalogSupplierBrandID" Visible="False">
                                </radG:GridBoundColumn>
                              
                                
                                 <radG:GridTemplateColumn HeaderText="Brand" UniqueName="BrandShortNameParent">
                                     <ItemTemplate>
                                          <asp:Label id="lblBrandItem" Text='<%# bind("BrandShortName")%>' runat="server"></asp:Label>
                                     </ItemTemplate>
                                     <FooterTemplate><asp:Label id="lblGrandTotalLabel" Text="GrandTotal:" runat="server" /></FooterTemplate>
                                </radG:GridTemplateColumn>
                                
                                
                                  <radG:GridTemplateColumn HeaderText="Subtotal" UniqueName="TotalCost">
                                     <ItemTemplate>
                                          <asp:Label id="lblTotalCost2" Text='<%# bind("TotalCost", "{0:$#,#.00}")%>' runat="server"></asp:Label>
                                     </ItemTemplate>
                                     <FooterTemplate><asp:Label id="lblGrandTotal"  runat="server" /></FooterTemplate>
                                </radG:GridTemplateColumn>
                                
                                
                              
                                
                            </Columns>
                        </MasterTableView>
                        <GroupPanel Visible="True">
                        </GroupPanel>
                        <ClientSettings AllowColumnsReorder="false" AllowDragToGroup="false" ReorderColumnsOnClient="false">
                            <Selecting AllowRowSelect="True"/>
                             
                        </ClientSettings>
                                              
                        </radG:RadGrid>
          </td>
      
         </tr>
         <tr>
            <td >
            <div runat="server" id="divOrderConf" visible="false">
                <table width="100%" cellpadding="0" cellpadding="0" border="0">
                    <tr>
                        <td>
                            <b>Order Confirmation</b>
                        </td>
                        <td align="right">
                            <asp:Button runat="server" ID="btnPrintReceipt" OnClick="btnPrintReceipt_Click" Text="Printer Friendly Format" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label runat="server" ID="lblPODetail"></asp:Label>
                        </td>
                     </tr>
                </table>
            
            </div>
            </td>
         </tr>

         

        </table>
    
    </td>
    <td></td>


  </table>
  
  
 </asp:Content>

