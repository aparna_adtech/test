using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.WebControls;

namespace BregVision
{
    public partial class Dispense1 : System.Web.UI.Page
    {

        string SearchCriteria = "";
        string SearchText = "";
        private int userID =0;
        private int practiceID=0;
        private string practiceName = "";

        private UIHelper _UIHelper; //mws
        private DataSet _dsSearch = null;

        private UIHelper _grdCustomBrace_UIHelper;
        private DataSet _dsCustomBrace = null;

        ArrayList list = new ArrayList();

        
        protected void InitializeComponent()
        {
            grdSummary.ItemDataBound += new GridItemEventHandler(grdSummary_ItemDataBound);
            grdInventory.PageIndexChanged += new GridPageChangedEventHandler(grdInventory_PageIndexChanged);
            grdInventory.ItemCommand += new GridCommandEventHandler(grdInventory_ItemCommand);
            //grdInventory.PreRender += new GridViewPageEventHandler(grdInventory_PreRender);
            rmpMain.TabClick += new TabStripEventHandler(rmpMain_TabClick);
            grdSummary.DeleteCommand += new GridCommandEventHandler(grdSummary_DeleteCommand);
            this.Load += new System.EventHandler(this.Page_Load);
            this.PreRender += new EventHandler(this.Page_PreRender);
            this.Error += new System.EventHandler(this.Page_Error);
        }

        public static class ResponseHelper
        {
            public static void Redirect(string url, string target, string windowFeatures)
            {
                try
                {
                    HttpContext context = HttpContext.Current;
                    if ((String.IsNullOrEmpty(target) || target.Equals("_self", StringComparison.OrdinalIgnoreCase)) && String.IsNullOrEmpty(windowFeatures))
                    {
                        context.Response.Redirect(url);
                    }
                    else
                    {
                        Page page = (Page)context.Handler; if (page == null)
                        {
                            throw new InvalidOperationException("Cannot redirect to new window outside Page context.");
                        }
                        url = page.ResolveClientUrl(url); string script;
                        if (!String.IsNullOrEmpty(windowFeatures))
                        {
                            script = @"window.open(""{0}"", ""{1}"", ""{2}"");";
                        }
                        else
                        {
                            script = @"window.open(""{0}"", ""{1}"");";
                        }
                        script = String.Format(script, url, target, windowFeatures);
                        ScriptManager.RegisterStartupScript(page, typeof(Page), "Redirect", script, true);
                    }
                }
                #region Exception Handler
                catch (Exception ex)
                {
                    
                    StringBuilder sb = new StringBuilder();
                    System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                    foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                    {
                        sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                    }

                    ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler();
                    string result =  sb.ToString();

                    KEH.RaiseError(result);
                    throw;

                }
                #endregion
            }
        }

        protected void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();

            StringBuilder sb = new StringBuilder();
            System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

            foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
            {
                sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
            }
            ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

            
            KEH.RaiseError(result);
            throw ex;
        }

        protected string GetUserErrorInfo()
        {
            string userName = Context.User.Identity.Name;
            int eUserID = (Session["UserID"] != null) ? Convert.ToInt32(Session["UserID"].ToString()) : -1;
            int ePracticeID = (Session["PracticeID"] != null) ? Convert.ToInt32(Session["PracticeID"].ToString()) : -1;
            string ePracticeName = (Session["PracticeName"] != null) ? Session["PracticeName"].ToString() : "Not Set";

            string errString = string.Format(":CurrentUserID={0}:CurrentUserName={1}:CurrentPracticeID={2}:CurrentPracticeName='{3}': \r\n<br>", eUserID.ToString(), userName, ePracticeID.ToString(), ePracticeName);
            return errString;
        }

        protected void GetSessionVariables()
        {
            userID = (Session["UserID"] != null) ? Convert.ToInt32(Session["UserID"].ToString()) : -1;
            practiceID = (Session["PracticeID"] != null) ? Convert.ToInt32(Session["PracticeID"].ToString()) : -1;
            practiceName = (Session["PracticeName"] != null) ? Session["PracticeName"].ToString() : "";
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                    _UIHelper = new UIHelper(ViewState, grdInventory, true); //mws
                    _UIHelper.SetGridLabel("", Page.IsPostBack, lblBrowseDispense, true);//set the default text on the label

                    _grdCustomBrace_UIHelper = new UIHelper(ViewState, grdCustomBrace, true); //mws
                    _grdCustomBrace_UIHelper.SetGridLabel("", Page.IsPostBack, lblBrowseDispenseCustomBrace, true);//set the default text on the label

                    // Form the script that is to be registered at client side.
                    String scriptString = "<script language=JavaScript> function DoClick() {";
                    scriptString += "myForm.show.value='Welcome to Microsoft .NET'}<";
                    scriptString += "/";
                    scriptString += "script>";

                    if (!this.IsClientScriptBlockRegistered("clientScript"))
                        this.RegisterClientScriptBlock("clientScript", scriptString);

                    if (!IsPostBack)
                    {
                        ViewState["SearchCriteria"] = "";

                        //hasExpanded = true;
                        //ViewState["hasExpanded"] = hasExpanded;
                        rmpMain.SelectedIndex = 0;
                        pvDispense.SelectedIndex = 0;

                        //Set initial value for record count
                        txtRecordsDisplayed.Text = Session["RecordsDisplayed"].ToString();

                        //Load Physician Information

                        DataSet ds = GetPhysicians(Convert.ToInt32(Session["practiceLocationID"]));
                        cboPhysicians.DataSource = ds;

                        cboPhysicians.DataValueField = "PhysicianId";
                        cboPhysicians.DataTextField = "PhysicianName";
                        cboPhysicians.DataBind();

                        _UIHelper.HasExpanded = true; //mws
                        _grdCustomBrace_UIHelper.HasExpanded = true; //mws

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            cboPhysicians.Items[0].Selected = true;
                        }

                        //Sets date in the summary tab to todays date
                        if (dtDispensed.SelectedDate == null)
                        {
                            dtDispensed.SelectedDate = DateTime.Today;
                        }
                        //Initial Step legend

                        btnBack.Visible = false;
                        btnNext.Visible = true;

                        lblSteps.Text = "Step 1 of 4:Please search or browse for product(s)";

                    }
                    else
                    {
                        if (rmpMain.SelectedIndex == 4 || rmpMain.SelectedIndex == 5)//Receipt
                        {

                            //Next two line are required to turn off Ajax so that page will pop up and redirect to new web page
                            //for printer friendly reciept
                            RadWindowManager1.EnableStandardPopups = true;
                            RadAjaxPanel2.EnableAJAX = false;
                            //RadAjaxPanel2.RaisePostBackEvent();

                        }
                        else
                        {
                            //RadAjaxPanel2.EnableAJAX = true;
                        }
                    }
                    grdInventory.PageSize = Convert.ToInt16(txtRecordsDisplayed.Text);
                    //grdSearch.PageSize = Convert.ToInt16(txtRecordsDisplayed.Text);
                    grdSummary.PageSize = Convert.ToInt16(txtRecordsDisplayed.Text);

                    //Set up the textbox so the search button is click on 'enter' key
                    TextBox tbFilter = (TextBox)rttbFilter.FindControl("tbFilter");
                    RadToolbarToggleButton rtbFilter = (RadToolbarToggleButton)RadToolbar1.FindControl("rtbFilter");
                    UIHelper.SetEnterPressTarget(tbFilter, rtbFilter);


             }
            #region Exception Handler
            catch (Exception ex)
            {
                
                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }

                ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler();
                string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (Session["DefaultDispenseSearchCriteria"] != null && Page.IsPostBack == false)
                {
                    RadToolbarTemplateButton rttbSearchFilter = (RadToolbarTemplateButton)RadToolbar1.FindControl("rttbSearchFilter");
                    RadComboBox cboSearchFilter = (RadComboBox)rttbSearchFilter.FindControl("cboSearchFilter");
                    int index = cboSearchFilter.FindItemIndexByValue(Session["DefaultDispenseSearchCriteria"].ToString());
                    cboSearchFilter.SelectedIndex = index;
                }
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        protected void grdInventory_ItemCommand(Object source, Telerik.WebControls.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ExpandCollapse")
                {
                    LoopHierarchyRecursive(grdInventory.MasterTableView);
                }
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion

        }

        protected void grdSummary_DeleteCommand(object sender, Telerik.WebControls.GridCommandEventArgs e)
        {
            try
            {
                //delete item
                if (e.Item is GridDataItem)
                {
                    GridDataItem dataItem = e.Item as GridDataItem;
                    Int32 productInventoryID = Int32.Parse(dataItem["ProductInventoryID"].Text);
                    if (ViewState["ArrayList"] != null)
                    {
                        list = (ArrayList)ViewState["ArrayList"];
                        list.Remove(productInventoryID);

                        //deleteItemFromShoppingCart(shoppingCartItemID);

                        grdSummary.DataSource = grdSummary.DataSource = GetSummaryData(BuildArrayString(list));
                        grdSummary.Rebind();

                    }
                }
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }



        protected void grdSummary_OnLoad(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    grdSummary.DataSource = new Object[] { };
                    grdSummary.DataBind(); //Used to show a 'No Records Found' when the page initially loads
                }
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        protected void rmpMain_TabClick(object sender, EventArgs e)
        {

            try
            {
                if (rmpMain.SelectedIndex == 0) //Search Tab
                {

                    btnBack.Visible = false;
                    btnNext.Visible = true;
                    btnNext.Enabled = true;
                    lblSteps.Text = "Step 1 of 4:Please search or browse for product(s)";

                }
                if (rmpMain.SelectedIndex == 1) //Search Tab
                {

                    btnBack.Visible = false;
                    btnNext.Visible = true;
                    btnNext.Enabled = true;
                    lblSteps.Text = "Step 1 of 4:Please search or browse for product(s) Custom Brace";

                }


                if (rmpMain.SelectedIndex == 2) //Physican / Patient Tab
                {

                    btnBack.Visible = true;
                    btnNext.Visible = true;
                    btnNext.Enabled = true;
                    lblSteps.Text = "Step 2 of 4:Choose a physician and enter patient ID if applicable";
                    if (cboPhysicians.DataSource == null)
                    {
                        if (cboPhysicians.SelectedValue.Length == 0)
                        {
                            DataSet ds = GetPhysicians(Convert.ToInt32(Session["practiceLocationID"]));
                            cboPhysicians.DataSource = ds.Tables[0];
                            cboPhysicians.DataValueField = "PhysicianId";
                            cboPhysicians.DataTextField = "PhysicianName";
                            cboPhysicians.DataBind();
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                cboPhysicians.Items[0].Selected = true;
                            }
                        }
                    }

                }


                if (rmpMain.SelectedIndex == 3) //Summary tab
                {
                    btnBack.Visible = true;
                    btnNext.Visible = true;
                    if (btnDispenseProducts.Enabled)
                    {
                        btnNext.Enabled = false;
                    }
                    else
                    {
                        btnNext.Enabled = true;
                    }
                    lblSteps.Text = "Step 3 of 4:Review items/quantities and click dispense button when satisified";

                    if (ViewState["ArrayList"] != null)
                    {
                        list = (ArrayList)ViewState["ArrayList"];
                    }

                    lblSummaryPhysician.Text = cboPhysicians.Text;
                    if (txtPatientCode.Text.Length > 0)
                    {
                        lblSummaryPatientIDLegend.Visible = true;
                        lblSummaryPatientID.Text = txtPatientCode.Text;
                    }
                    else
                    {
                        lblSummaryPatientIDLegend.Visible = false;
                        lblSummaryPatientID.Text = "";
                    }
                    grdSummary.DataSource = GetSummaryData(BuildArrayString(list));
                    grdSummary.DataBind(); //Binds to data whether or not we have any so that the grid will show a "No Records Found" statement if no data                
                }

                if (rmpMain.SelectedIndex == 4)//Receipt
                {
                    lblSteps.Text = "Step 4 of 4:Review and print receipt";
                    btnNext.Visible = false;
                    Receipt.InnerHtml = BuildReceipt();

                }


                if (rmpMain.SelectedIndex == 5)//Receipt History
                {
                    //Turn off buttons and any text for steps
                    lblSteps.Text = "";
                    btnNext.Visible = false;
                    btnBack.Visible = false;
                    grdReceiptHistory.Rebind();

                }
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion

        }

        private IDataReader GetBillingAddressByPracticeLocation(Int32 PracticeLocationID)
        {

            try
            {
                Database db = DatabaseFactory.CreateDatabase("BregVisionLocalConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetBillingAddressByPracticeLocation");

                db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);
                IDataReader reader = db.ExecuteReader(dbCommand);
                return reader;
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        private IDataReader GetDispenseReceipt(Int32 DispenseID)
        {

            try
            {
                Database db = DatabaseFactory.CreateDatabase("BregVisionLocalConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetDispenseReceipt");

                db.AddInParameter(dbCommand, "DispenseID", DbType.Int32, DispenseID);
                IDataReader reader = db.ExecuteReader(dbCommand);
                return reader;
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion

        }

        private DateTime GetCreatedDate(Int32 DispenseID)
        {

            try
            {
                Database db = DatabaseFactory.CreateDatabase("BregVisionLocalConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.[usp_GetCreatedDateForReceipt]");

                db.AddInParameter(dbCommand, "DispenseID", DbType.Int32, DispenseID);
                DateTime DateDispensed = (DateTime)db.ExecuteScalar(dbCommand);

                return DateDispensed;
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        private string BuildReceipt()
        {
            try
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                //Build header information
                sb.Append("<table width='96%' bgcolor=white><tr><td>");
                sb.Append("<table width='98%'>");

                sb.Append("<tr><td style='font-size:smaller' style='font-weight:bold' align=left>BREG Vision Patient Receipt</td>");
                sb.Append("<td align=right>");

                //Get Created Date
                //Get Dispense Detail Information
                if (ViewState["DispenseID"] != null)
                {
                    DispenseID = (Int32)ViewState["DispenseID"];
                }

                DateTime test2 = GetCreatedDate(DispenseID);
                string test = test2.ToString("MM/dd/yyyy");
                //DateTime test2 = dtDispensed.SelectedDate.Value;
                //string test = dtDispensed.SelectedDate.ToString();
                //test = test2.ToString("MM/dd/yyyy");

                sb.Append(test);
                //sb.Append(DateTime.Now);
                sb.Append("</td></tr>");

                sb.Append("<tr><td style='font-size:smaller' align=left colspan=2>");
                sb.Append(Session["PracticeName"]);
                sb.Append("</td></tr>");

                sb.Append("<tr><td style='font-size:smaller' align=left colspan=2>");
                sb.Append(Session["PracticeLocationName"]);
                sb.Append("</td></tr>");
                sb.Append("</table>");
                IDataReader reader = GetBillingAddressByPracticeLocation(Convert.ToInt32(Session["practiceLocationID"]));

                //Get Practice BillingInfo
                sb.Append("<table width='98%'>");
                //DataSet ds = db.ExecuteDataSet(dbCommand);
                if (reader.Read())
                {
                    sb.Append("<tr><td style='font-size:smaller' align=left>");
                    sb.Append(reader["BillAddress1"].ToString());
                    sb.Append("</td></tr>");
                    sb.Append("<tr><td style='font-size:smaller' align=left>");
                    sb.Append(reader["BillCity"].ToString());
                    sb.Append(", ");
                    sb.Append(reader["BillState"].ToString());
                    sb.Append(" ");
                    sb.Append(reader["BillZipCode"].ToString());
                    sb.Append("</td></tr>");
                    sb.Append("<tr><td style='font-size:smaller' align=left>Fax: ");
                    sb.Append(reader["FAX"].ToString());
                    sb.Append("</td></tr>");

                }
                reader.Close();
                sb.Append("</table><br><br>");

                //Get Dispense Detail Information
                if (ViewState["DispenseID"] != null)
                {
                    DispenseID = (Int32)ViewState["DispenseID"];
                }


                reader = GetDispenseReceipt(DispenseID);
                //DataSet ds = db.ExecuteDataSet(dbCommand);
                if (reader.Read())
                {
                    //Doctor Information
                    sb.Append("<table width='98%'><tr><td style='font-size:smaller' align=left width='130px'>Provider: </td><td align=left style='font-size:smaller' width='100%' >");
                    sb.Append(reader["Title"].ToString());
                    sb.Append(" ");
                    sb.Append(reader["FirstName"].ToString());
                    sb.Append(" ");
                    sb.Append(reader["LastName"].ToString());
                    sb.Append(" ");
                    sb.Append(reader["Suffix"].ToString());
                    sb.Append("</td></tr>");
                    sb.Append("<tr><td align=left style='font-size:smaller' >Patient ID Number: </td><td  align=left style='font-size:smaller' >");
                    sb.Append(reader["PatientCode"].ToString());
                    sb.Append("</td></tr></table><br><br>");

                    //Dispense Detail
                    sb.Append("<table cellspacing=0 cellpadding=3 border=1><tr><td style='font-size:smaller' align=left>Product Name</td><td style='font-size:smaller' align=left>Part Number</td><td style='font-size:smaller' align=left>Quantity</td>");
                    sb.Append("<td style='font-size:smaller' align=left>HCPCs</td><td style='font-size:smaller' align=left>DME Deposit</td>");
                    sb.Append("<td style='font-size:smaller' align=left>Side</td><td style='font-size:smaller' align=left>Size</td><td style='font-size:smaller' align=left>Gender</td><td style='font-size:smaller' align=left>Billing Charge</td>");
                    //sb.Append("<td style='font-size:smaller' align=left>Total</td>");
                    sb.Append("</tr>");


                    //loop through collection and populate all dispense detail records
                    string grandTotal;
                    do
                    {
                        sb.Append("<tr><td style='font-size:smaller' align=left>");
                        sb.Append(reader["ShortName"].ToString());
                        sb.Append("</td><td style='font-size:smaller' align=left>");
                        sb.Append(reader["Code"].ToString());
                        sb.Append("</td><td style='font-size:smaller' align=left>");
                        sb.Append(reader["Quantity"].ToString());
                        sb.Append("</td><td style='font-size:smaller' align=left>&nbsp");

                        sb.Append(reader["HCPCsString"].ToString());
                        sb.Append("</td><td style='font-size:smaller' align=left>$");

                        decimal DMEDeposit = Convert.ToDecimal(reader["DMEDeposit"]) * Convert.ToInt16(reader["Quantity"]);
                        sb.Append(DMEDeposit.ToString("#,##0.00"));

                        //sb.Append(Convert.ToDecimal(reader["DMEDeposit"]).ToString("#,##0.00"));


                        sb.Append("</td><td style='font-size:smaller' align=left>");

                        sb.Append(reader["Side"].ToString());
                        sb.Append("</td><td style='font-size:smaller' align=left>");
                        sb.Append(reader["Size"].ToString());
                        sb.Append("</td><td style='font-size:smaller' align=left>");
                        sb.Append(reader["Gender"].ToString());
                        sb.Append("</td><td style='font-size:smaller' align=left>$");

                        decimal Total = Convert.ToDecimal(reader["Total"]) * Convert.ToInt16(reader["Quantity"]);
                        sb.Append(Total.ToString("#,##0.00"));

                        //sb.Append(Convert.ToDecimal(reader["Total"]).ToString("#,##0.00"));


                        //sb.Append("</td><td style='font-size:smaller' align=left>$");
                        //sb.Append(Convert.ToDecimal(reader["LineTotal"]).ToString("#,##0.00"));
                        sb.Append("</td></tr>");
                        //grandTotal = Convert.ToDecimal(reader["GrandTotal"]).ToString("#,##0.00");
                    } while (reader.Read());

                    reader.Close();
                    //Grand total line
                    //sb.Append("<tr><td style='font-size:smaller' colspan=9 align=right>Total:</td><td style='font-size:smaller;font-weight:bold' align=left>$");
                    //sb.Append(grandTotal);
                    //sb.Append("</td></tr></table>");
                    sb.Append("</table>");
                    sb.Append("</td></tr><tr><td>&nbsp</td></tr></table>");


                    ////
                    sb.Append("<br />");
                    sb.Append("<table cellspacing=1 cellpadding=1 bgcolor=gray width='100%'>");
                    sb.Append("<tr><td><table bgcolor=white width='100%'>");
                    sb.Append("<tr><td align=right style='font-size:small'>Patient Name:</td>");
                    sb.Append("<td  style='font-size:small'>____________________________</td>");
                    sb.Append("<td  align=right style='font-size:small'>Physician: </td>");
                    sb.Append("<td  style='font-size:small'>____________________________</td>");
                    sb.Append("<td  style='font-size:small'>Surgery?<br />Yes/No</td>");
                    sb.Append("</tr>");
                    sb.Append("<tr><td colspan=5>&nbsp</td></tr>");
                    sb.Append("<tr><td  align=right style='font-size:small'>Dx Code:</td>");
                    sb.Append("<td  style='font-size:small'>____________________________ </td>");
                    sb.Append("<td  align=right style='font-size:small'>Insurance: </td>");
                    sb.Append("<td  colspan=2 align=left style='font-size:small'>____________________________</td></tr>");
                    sb.Append("<tr><td colspan=5>&nbsp</td></tr>");
                    sb.Append("<tr><td colspan=5 style=' font-size:small; font-weight:bold'>Durable Medical Equipment and Orthotics Patient Consent</td></tr>");
                    sb.Append("<tr><td colspan=5>&nbsp</td></tr>");
                    sb.Append("<tr><td align=left colspan=5 style='font-size:small'>- I understand that my physician has prescribed this medical supply as part of my treatment plan </td></tr>");
                    sb.Append("<tr><td align=left colspan=5 style='font-size:small'>- I understand that I have a choice in where I receive my prescribed orthopaedic supplies and services</td></tr>");
                    sb.Append("<tr><td align=left colspan=5 style='font-size:small'>- I authorize this orthopedic practice to furnish this service/product and to provide my insurance provider with any information requested for payment</td></tr>");
                    sb.Append("<tr><td align=left colspan=5 style='font-size:small'>- I instruct my insurance provider to pay this orthopedic practice directly for these services/products</td></tr>");
                    sb.Append("<tr><td align=left colspan=5 style='font-size:small'>- I understand that my insurance company may deny payment for this supply because it is a non-covered item or deemed not medically necessary</td></tr>");
                    sb.Append("<tr><td align=left colspan=5 style='font-size:small'>- I understand that I am fully responsible for any deductible or co-insurance cost related to this service/supply</td></tr>");
                    sb.Append("<tr><td align=left colspan=5 style='font-size:small'>- I understand any costs not covered by my insurance provider will be my financial responsibility</td></tr>");
                    sb.Append("<tr><td align=left colspan=5 style='font-size:small'>- I have received the prescribed item and have been fully instructed on the use of the above products/services</td></tr>");
                    sb.Append("<tr><td align=left colspan=5 style='font-size:small'>- I understand that all medical devices are not returnable unless there is a material defect</td></tr>");
                    sb.Append("<tr><td colspan=5>&nbsp</td></tr>");
                    sb.Append("<tr><td  align=right style='font-size:small'>Patient/Guarantor Signature: </td><td  colspan=4 align=left style='font-size:small'>_____________________________________</td></tr>");
                    sb.Append("<tr><td colspan=5>&nbsp</td></tr>");
                    sb.Append("<tr><td  align=right style='font-size:small'>Date: </td><td  colspan=4 align=left style='font-size:small'>_____________________________________ </td>");
                    sb.Append("</tr><tr><td colspan=5>&nbsp</td></tr>");
                    sb.Append("<tr><td  align=right style='font-size:small'>Service Refusal: </td><td  colspan=4 style='font-size:small'></td></tr>");
                    sb.Append("<tr><td  align=right style='font-size:small'></td>");
                    sb.Append("<td  colspan=4 align=left style='font-size:small'>I have decided not to receive this item from this orthopedic practice </td></tr>");
                    sb.Append("<tr><td  align=right style='font-size:small'></td><td  colspan=4 align=left style='font-size:small'>I understand that my physician has prescribed this item as part of my treatment</td></tr>");
                    sb.Append("<tr><td colspan=5>&nbsp</td></tr>");
                    sb.Append("<tr><td  align=right style='font-size:small'>Patient/Guarantor Signature: </td>");
                    sb.Append("<td  colspan=4 align=left style='font-size:small'>_____________________________________</td></tr>");
                    sb.Append("<tr><td colspan=5>&nbsp</td></tr>");
                    sb.Append("<tr><td  align=right style='font-size:small'>Date: </td>");
                    sb.Append("<td  colspan=4 align=left style='font-size:small'>_____________________________________</td>");
                    sb.Append("</tr><tr><td colspan=5>&nbsp</td></tr>");
                    sb.Append("</table></td></tr></table>");

                    ////


                }

                return sb.ToString();
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        private string BuildArrayString(ArrayList list)
        {
            try
            {
                string arrayValues = "";
                foreach (Int32 item in list)
                {

                    arrayValues += String.Concat(item.ToString(), ",");

                }

                if (arrayValues.Length > 0)
                {
                    arrayValues = arrayValues.Substring(0, arrayValues.Length - 1);

                }
                return arrayValues;
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }


        public DataSet GetSummaryData(string arrayValues)
        {
            try
            {

                Database db = DatabaseFactory.CreateDatabase("BregVisionLocalConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetPreDispensementSummary");

                db.AddInParameter(dbCommand, "ProductInventoryIDArray", DbType.String, arrayValues);
                DataSet ds = db.ExecuteDataSet(dbCommand);
                return ds;
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion

        }

        public DataSet GetPhysicians(int practiceLocationID)
        {
            try
            {

                Database db = DatabaseFactory.CreateDatabase("BregVisionLocalConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_Physician_Select_All_Names_By_PracticeLocationID");

                db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
                DataSet ds = db.ExecuteDataSet(dbCommand);

                return ds;

            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        protected void grdInventory_PreRender(object sender, EventArgs e)
        {
            try
            {

                if (_UIHelper.HasExpanded == false) //mws
                {
                    UIHelper.ExpandFormatGrid(grdInventory);
                    _UIHelper.HasExpanded = true; //mws


                }

                RemoveExpandIconWhenNoRecords(grdInventory.MasterTableView);
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        protected void grdCustomBrace_PreRender(object sender, EventArgs e)
        {
            try
            {

                //if (_grdCustomBrace_UIHelper.HasExpanded == false) //mws
                //{
                //    UIHelper.ExpandFormatGrid(grdCustomBrace);
                //    _grdCustomBrace_UIHelper.HasExpanded = true; //mws


                //}

                //RemoveExpandIconWhenNoRecords(grdCustomBrace.MasterTableView);
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
                ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler();
                string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();


                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }


       private void RemoveExpandIconWhenNoRecords(GridTableView view)
        {
            try
            {
                //GridItem item;

                if (view.Controls.Count > 0)
                {
                    foreach (GridItem item in view.Controls[0].Controls)
                    {
                        if (item is GridNestedViewItem)
                        {
                            GridNestedViewItem nestedViewItem = (GridNestedViewItem)item;
                            if (nestedViewItem.NestedTableViews[0].Items.Count == 0)
                            {
                                TableCell cell = nestedViewItem.NestedTableViews[0].ParentItem["ExpandColumn"];
                                ImageButton expandCollapseButton = (ImageButton)cell.Controls[0];

                                expandCollapseButton.Visible = false;
                                nestedViewItem.Visible = false;

                            }
                            else
                            {
                                RemoveExpandIconWhenNoRecords(nestedViewItem.NestedTableViews[0]);
                                //item.Expanded = false;
                                //item.Enabled = false;           
                            }
                        }
                    }

                }

            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }


        protected void grdInventory_PageIndexChanged(object source, Telerik.WebControls.GridPageChangedEventArgs e)
        {
            try
            {
                //this.grdInventory.CurrentPageIndex = e.NewPageIndex;
                LoopHierarchyRecursive(grdInventory.MasterTableView);


                //Next line makes sure items on new page get expanded when applicable - GR 

                if (ViewState["SearchCriteria"] == null ||
                    ViewState["SearchCriteria"].ToString() == "Browse" ||
                    ViewState["SearchCriteria"].ToString() == "")
                {
                    _UIHelper.HasExpanded = true; //mws
                }

                else
                {
                    _UIHelper.HasExpanded = false; //mws
                }
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }



        protected void grdCustomBrace_PageIndexChanged(object source, Telerik.WebControls.GridPageChangedEventArgs e)
        {
            try
            {
                //this.grdInventory.CurrentPageIndex = e.NewPageIndex;
                LoopHierarchyRecursive(grdCustomBrace.MasterTableView);


                //Next line makes sure items on new page get expanded when applicable - GR 

                if (ViewState["grdCustomBrace_SearchCriteria"] == null ||
                    ViewState["grdCustomBrace_SearchCriteria"].ToString() == "Browse" ||
                    ViewState["grdCustomBrace_SearchCriteria"].ToString() == "")
                {
                    _grdCustomBrace_UIHelper.HasExpanded = true; //mws
                }

                else
                {
                    _grdCustomBrace_UIHelper.HasExpanded = false; //mws
                }
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
                ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler();
                string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();


                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }



        protected void rtbCustomBrace_OnClick(object sender, Telerik.WebControls.RadToolbarClickEventArgs e)
        {
            try
            {
                if (e.Button.CommandName == "Browse")
                {

                    SearchCriteria = "Browse";
                    ViewState["grdCustomBrace_SearchCriteria"] = SearchCriteria;

                    SearchText = "";
                    ViewState["grdCustomBrace_SearchText"] = SearchText;


                    //grdCustomBrace.DataSource = GetSuppliers(Convert.ToInt32(Session["practiceLocationID"]));
                    grdCustomBrace.Rebind();
                    _grdCustomBrace_UIHelper.HasExpanded = true; //mws

                }

                if (e.Button.CommandName == "Search")
                {

                    RadToolbar toolbarCustomBrace = (RadToolbar)sender;
                    RadToolbarTemplateButton rttbFilterCustomBrace1 = (RadToolbarTemplateButton)toolbarCustomBrace.FindControl("rttbFilterCustomBrace");
                    _grdCustomBrace_UIHelper.IsSearch = true;
                    TextBox searchText = (TextBox)rttbFilterCustomBrace1.FindControl("tbFilterCustomBrace");
                    ViewState["grdCustomBrace_SearchText"] = searchText.Text;

                    RadToolbarTemplateButton rttbSearchFilterCustomBrace1 = (RadToolbarTemplateButton)toolbarCustomBrace.FindControl("rttbSearchFilterCustomBrace");
                    RadComboBox SearchCriteria = (RadComboBox)rttbSearchFilterCustomBrace1.FindControl("cboSearchFilterCustomBrace");
                    ViewState["grdCustomBrace_SearchCriteria"] = SearchCriteria.Value.ToString();

                    grdCustomBrace.Rebind();
                    _grdCustomBrace_UIHelper.HasExpanded = false; //mws

                }
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
                ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler();
                string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();


                KEH.RaiseError(result);
                throw;

            }
            #endregion

        }

        protected void RadToolbar1_OnClick(object sender, Telerik.WebControls.RadToolbarClickEventArgs e)
        {
            try
            {
                if (e.Button.CommandName == "Browse")
                {

                    SearchCriteria = "Browse";
                    ViewState["SearchCriteria"] = SearchCriteria;

                    SearchText = "";
                    ViewState["SearchText"] = SearchText;


                    grdInventory.DataSource = GetSuppliers(Convert.ToInt32(Session["practiceLocationID"]));
                    grdInventory.Rebind();
                    _UIHelper.HasExpanded = true; //mws

                }

                if (e.Button.CommandName == "Search")
                {
                    _UIHelper.IsSearch = true;
                    TextBox searchText = (TextBox)rttbFilter.FindControl("tbFilter");
                    ViewState["SearchText"] = searchText.Text.ToString();

                    RadComboBox SearchCriteria = (RadComboBox)rttbSearchFilter.FindControl("cboSearchFilter");
                    ViewState["SearchCriteria"] = SearchCriteria.Value.ToString();

                    grdInventory.Rebind();
                    _UIHelper.HasExpanded = false; //mws

                }
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion

        }

        public DataSet SearchForItemsInventory(int practiceLocationID, string searchCriteria, string searchText)
        {
            try
            {

                Database db = DatabaseFactory.CreateDatabase("BregVisionLocalConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_SearchforItemsInInventory");

                db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
                db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, searchCriteria);
                db.AddInParameter(dbCommand, "SearchText", DbType.String, searchText);
                DataSet ds = db.ExecuteDataSet(dbCommand);
                return ds;
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }


        protected void grdSummary_NeedDataSource(object source, Telerik.WebControls.GridNeedDataSourceEventArgs e)
        {
        }

        protected void grdCustomBrace_NeedDataSource(object source, Telerik.WebControls.GridNeedDataSourceEventArgs e)
        {
            try
            {
                string searchCriteria = "";
                string searchText = "";

                if (ViewState["grdCustomBrace_SearchCriteria"] != null)
                {
                    searchCriteria = (string)ViewState["grdCustomBrace_SearchCriteria"];
                }

                if (ViewState["grdCustomBrace_SearchText"] != null)
                {
                    searchText = (string)ViewState["grdCustomBrace_SearchText"];
                }

                if (!e.IsFromDetailTable)
                {
                    grdCustomBrace.DataSource = GetCustomBraceOrdersforDispense_ParentLevel(Convert.ToInt32(Session["PracticeLocationID"]), searchCriteria, searchText);
                }
                grdCustomBrace.MasterTableView.DetailTables[0].DataSource = GetCustomBraceOrdersforDispense_Detail1Level(Convert.ToInt32(Session["PracticeLocationID"]), searchCriteria, searchText);
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
                ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler();
                string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();


                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        protected void grdSearch_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            try
            {
                TextBox searchText = (TextBox)rttbFilter.FindControl("tbFilter");

                RadComboBox SearchCriteria = (RadComboBox)rttbSearchFilter.FindControl("cboSearchFilter");
                grdInventory.DataSource = SearchForItemsInventory(Convert.ToInt32(Session["practiceLocationID"]), SearchCriteria.Value.ToString(), searchText.Text);
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        protected void grdInventory_NeedDataSource(object source, Telerik.WebControls.GridNeedDataSourceEventArgs e)
        {
            try
            {
                if (!e.IsFromDetailTable)
                {
                    grdInventory.DataSource = GetSuppliers(Convert.ToInt16(Session["PracticeLocationID"]));
                }
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }
        

        protected void grdReceiptHistory_NeedDataSource(object source, Telerik.WebControls.GridNeedDataSourceEventArgs e)
        {
            try
            {

                if (rmpMain.SelectedIndex == 5)//Only load if on the resepective tab
                {
                    if (!e.IsFromDetailTable)
                    {
                        grdReceiptHistory.DataSource = GetReceiptHistoryParent(Convert.ToInt16(Session["PracticeLocationID"]));
                    }

                }
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }
        protected void grdInventory_DetailTableDataBind(object source, Telerik.WebControls.GridDetailTableDataBindEventArgs e)
        {
            try
            {
                GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;

                if (ViewState["SearchCriteria"] != null)
                {
                    SearchCriteria = (string)ViewState["SearchCriteria"];
                }

                if (ViewState["SearchText"] != null)
                {
                    SearchText = (string)ViewState["SearchText"];
                }
                e.DetailTableView.DataSource = GetAllProductDatabyLocation(Convert.ToInt16(Session["PracticeID"]), Convert.ToInt16(Session["PracticeLocationID"]), SearchCriteria, SearchText);

                Telerik.WebControls.GridDataItem ParentItem = e.DetailTableView.ParentItem as Telerik.WebControls.GridDataItem;
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }


        protected void grdReceiptHistory_DetailTableDataBind(object source, Telerik.WebControls.GridDetailTableDataBindEventArgs e)
        {
            try
            {
                e.DetailTableView.DataSource = GetAllReceiptHistoryDataByLocation(Convert.ToInt16(Session["PracticeLocationID"]));
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }


        public DataSet GetAllReceiptHistoryDataByLocation(int practiceLocationID)
        {
            try
            {

                Database db = DatabaseFactory.CreateDatabase("BregVisionLocalConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_ReceiptHistory_Detail");

                db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
                DataSet ds = db.ExecuteDataSet(dbCommand);
                return ds;
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }



        public DataSet GetAllProductDatabyLocation(int practiceID, int practiceLocationID, string SearchCriteria, string SearchText)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase("BregVisionLocalConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_Get_Dispense_Records_for_Browse_Grid_NewTEst");

                db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
                db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
                db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
                db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);
                DataSet ds = db.ExecuteDataSet(dbCommand);

                if (_dsSearch == null)
                {
                    ds = db.ExecuteDataSet(dbCommand);
                    _dsSearch = ds;
                    _UIHelper.SetRecordCount(ds, "ProductName");
                    _UIHelper.SetGridLabel(SearchCriteria, Page.IsPostBack, lblBrowseDispense);
                }
                else
                {
                    ds = _dsSearch;
                }
                return ds;
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        private void LoopHierarchyRecursive2(GridTableView gridTableView)
        {
            try
            {
                Int16 numProducts = 0;
                foreach (GridDataItem gridDataItem in gridTableView.Items)
                    if (findSelectColumn(gridDataItem))
                    {
                        //lblStatus.Text = "Adding items to the cart.";
                        UpdateSummaryArray(findProductInventoryID(gridDataItem));
                        numProducts += 1;



                    }
                string searchDispense = string.Concat(numProducts.ToString(), " product(s) added to dispensement");
                //lblSearchDispense.Text = searchDispense;
                //USer has added items - we need to reset all the tabs to allow dispensing
                grdSummary.Enabled = true;
                btnDispenseProducts.Enabled = true;
                rmpMain.Tabs[5].Enabled = false;

            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }


        private void LoopHierarchyRecursive(GridTableView gridTableView)
        {
            try
            {
                Int16 numProducts = 0;
                foreach (GridNestedViewItem nestedViewItem in gridTableView.GetItems(GridItemType.NestedView))
                {
                    if (nestedViewItem.NestedTableViews.Length > 0)
                    {

                        foreach (GridDataItem gridDataItem in nestedViewItem.NestedTableViews[0].Items)
                        {

                            if (findSelectColumn(gridDataItem))
                            {
                                //lblStatus.Text = "Adding items to the cart.";
                                UpdateSummaryArray(findProductInventoryID(gridDataItem));
                                numProducts += 1;
                            }

                        }

                    }
                    string browseDispense = string.Concat(numProducts.ToString(), " product(s) added to dispensement");
                    if (numProducts != 0)
                        lblBrowseDispense.Text = browseDispense;

                    //USer has added items - we need to reset all the tabs to allow dispensing
                    grdSummary.Enabled = true;
                    btnDispenseProducts.Enabled = true;
                    rmpMain.Tabs[4].Enabled = false;
                }
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        private void LoopHierarchyCustomBrace(GridTableView gridTableView)
        {
            try
            {
                Int16 numProducts = 0;

                foreach (GridDataItem poItem in gridTableView.Items)
                {
                    
                    if (poItem.Selected == true)
                    {
                        Int32 bregVisionOrderID = poItem.GetColumnInt32("BregVisionOrderID");
                        string patientName = poItem.GetColumnString("PatientName");
                        String physicianID = poItem.GetColumnString("PhysicianID");

                        ViewState["ArrayList"] = null;//clear the list of products
                        foreach (GridDataItem customBraceItem in poItem.ChildItem.NestedTableViews[0].Items)
                        {
                            UpdateSummaryArray(findProductInventoryID(customBraceItem));
                            numProducts += 1;
                        }
                        ViewState["BregVisionOrderID"] = bregVisionOrderID;
                        ViewState["PatientName"] = patientName;
                        txtPatientCode.Text = patientName;
                        if (physicianID != "")
                        {
                            cboPhysicians.SelectedValue = physicianID;
                        }
                        break;
                    }
                }

                string browseDispense = string.Concat(numProducts.ToString(), " product(s) added to dispensement");
                if (numProducts != 0)
                    lblBrowseDispenseCustomBrace.Text = browseDispense;

                //USer has added items - we need to reset all the tabs to allow dispensing
                grdSummary.Enabled = true;
                btnDispenseProducts.Enabled = true;
                rmpMain.Tabs[4].Enabled = false;
                
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
                ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler();
                string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();


                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        private Int32 findProductInventoryID(GridDataItem gridDataItem)
        {
            try
            {

                string ProductInventoryID = (string)gridDataItem["ProductInventoryID"].Text;
                if (ProductInventoryID != null)
                {
                    return Int32.Parse(ProductInventoryID);
                }
                else
                {
                    return 0;
                }
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion

        }

        protected void UpdateSummaryArray(Int32 InventoryID)
        {
            try
            {


                if (ViewState["ArrayList"] != null)
                {
                    list = (ArrayList)ViewState["ArrayList"];

                    //Checks for dups and exits the loop if one exists



                    Int32 listItemtoRemove = -1;
                    foreach (Int32 item in list)
                    {
                        if (Convert.ToInt32(item) == Convert.ToInt32(InventoryID))
                            listItemtoRemove = item;

                    }

                    if (listItemtoRemove >= 0)
                    {
                        list.Remove(listItemtoRemove);
                    }

                    list.Add(InventoryID);
                }
                else
                {
                    //Checks for dups and exits the loop if one exists
                    Int32 listItemtoRemove = -1;
                    foreach (Int32 item in list)
                    {
                        if (Convert.ToInt32(item) == Convert.ToInt32(InventoryID))
                            listItemtoRemove = item;

                    }

                    if (listItemtoRemove >= 0)
                    {
                        list.Remove(listItemtoRemove);
                    }
                    list.Add(InventoryID);
                    ViewState["ArrayList"] = list;
                }

                //list = ArrayList ViewState["ArrayList"];
                //list.Add("test 1");
                //list.Add("test 3");
                //list.Add("test 4");
                //ViewState["ArrayList"] = list;


            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }
        private Boolean findSelectColumn(GridDataItem gridDataItem)
        {
            try
            {
                if (gridDataItem != null)
                {
                    return gridDataItem.Selected;
                }
                else
                {
                    return false;
                }
                //TableCell cell = gridDataItem["ClientSelectColumn"];
                //CheckBox checkBox = (CheckBox)cell.Controls[0];
                //if (checkBox.Checked)
                //    return true;
                //else
                //{
                //    return false;
                //}


            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        public DataSet GetSuppliers(int practiceLocationID)
        {
            try
            {

                Database db = DatabaseFactory.CreateDatabase("BregVisionLocalConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetSuppliers");

                db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
                DataSet ds = db.ExecuteDataSet(dbCommand);

                return ds;


            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        public DataSet GetCustomBraceOrdersforDispense_ParentLevel(int practiceLocationID, string SearchCriteria, string SearchText)
        {
            try
            {

                Database db = DatabaseFactory.CreateDatabase("BregVisionLocalConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetCustomBraceOrdersforDispense_ParentLevel");

                db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
                db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
                db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);

                DataSet ds = db.ExecuteDataSet(dbCommand);
                return ds;

            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
                ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler();
                string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();


                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        public DataSet GetCustomBraceOrdersforDispense_Detail1Level(int practiceLocationID, string SearchCriteria, string SearchText)
        {
            try
            {

                Database db = DatabaseFactory.CreateDatabase("BregVisionLocalConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetCustomBraceOrdersforDispense_Detail1Level");

                db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
                db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, SearchCriteria);
                db.AddInParameter(dbCommand, "SearchText", DbType.String, SearchText);

                DataSet ds = db.ExecuteDataSet(dbCommand);
                return ds;

            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
                ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler();
                string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();


                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }


        public DataSet GetReceiptHistoryParent(int practiceLocationID)
        {
            try
            {

                Database db = DatabaseFactory.CreateDatabase("BregVisionLocalConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_ReceiptHistory");

                db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
                DataSet ds = db.ExecuteDataSet(dbCommand);

                return ds;


            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        
        public DataSet GetInventoryBySupplier(int practiceLocationID)
        {
            try
            {

                Database db = DatabaseFactory.CreateDatabase("BregVisionLocalConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetInventoryBySupplier");

                db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);

                DataSet ds = db.ExecuteDataSet(dbCommand);
                return ds;

            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }



        protected void quantityBoxDefault_ChildrenCreated(object sender, System.EventArgs e)
        {
            try
            {
                WebControl ctrl;
                ctrl = (WebControl)sender;

                HtmlAnchor spin;
                spin = (HtmlAnchor)ctrl.Controls[0].Controls[0].Controls[0];
                spin.Attributes["onclick"] = "ButtonClicked(this, event);";

                HtmlAnchor spin2;
                spin2 = (HtmlAnchor)ctrl.Controls[0].Controls[1].Controls[0];
                spin2.Attributes["onclick"] = "ButtonClicked(this, event);";
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        protected void btnAddSearchtoDispensement_Click(object sender, EventArgs e)
        {
            try
            {
                LoopHierarchyRecursive2(grdInventory.MasterTableView);
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        protected void btnAddBrowseToDispensement_Click(object sender, EventArgs e)
        {
            try
            {
                LoopHierarchyRecursive(grdInventory.MasterTableView);
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        protected void btnAddCustomBraceToDispensement_Click(object sender, EventArgs e)
        {
            //new for custom brace
            LoopHierarchyCustomBrace(grdCustomBrace.MasterTableView);

            //there can only be one po in the dispersement so we can go to the next page
            rmpMain.SelectedIndex = 3;
            PageView5.Selected = true;
            rmpMain_TabClick(rmpMain, e);
        }

        private Boolean ValidatePage()
        {
            try
            {
                //Validate Dispense date
                if (dtDispensed.SelectedDate.ToString().Length == 0)
                {
                    lblSummary.Text = "Dispense Date must be entered";
                    dtDispensed.Focus();
                    return false;
                }

                //Check if valid date
                if (!isValidDate(dtDispensed.SelectedDate.ToString()))
                {
                    lblSummary.Text = "Valid Date must be entered";
                    dtDispensed.Focus();
                    return false;
                }

                return true;
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        public bool isValidDate(string dateString)
        {
            try
            {
                bool result = true;
                try
                {
                    DateTime tDate = Convert.ToDateTime(dateString);
                }
                catch (Exception ex)
                {
                    result = false;
                }
                return result;
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        } 


        protected void btnDispenseProducts_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidatePage())
                    return;
                RadAjaxPanel2.EnableAJAX = false;
                LoopThroughSummaryGrid(grdSummary.MasterTableView);
                UpdateDispenseTotals(DispenseID);
                if (ViewState["BregVisionOrderID"] != null)
                {
                    int bvoid = 0;
                    bool bVOIDFound = Int32.TryParse(ViewState["BregVisionOrderID"].ToString(), out bvoid);
                    UpdateCustomBrace(DispenseID, bvoid);
                }
                this.Supplemental.Visible = true;

                grdSummary.Enabled = false;
                list.Clear();
                ViewState["ArrayList"] = list;
                btnDispenseProducts.Enabled = false;
                rmpMain.Tabs[4].Enabled = true;
                rmpMain.Tabs[4].Selected = true;
                PageView6.Selected = true;
                Receipt.InnerHtml = BuildReceipt();
                rmpMain_TabClick(rmpMain, e);
                ViewState["BregVisionOrderID"] = null;
                ViewState["PatientName"] = null;
                grdCustomBrace.Rebind();
                txtPatientCode.Text = "";
                lblBrowseDispense.Text = "";
                lblBrowseDispenseCustomBrace.Text = "";
                //Set Search box back to empty
                TextBox tbFilter = (TextBox)rttbFilter.FindControl("tbFilter");
                tbFilter.Text = "";
                //clear custom brace search box here like the line above
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion

        }

        Int32 DispenseID;

        private void LoopThroughSummaryGrid(GridTableView gridTableView)
        {
            try
            {
                foreach (GridDataItem gridDataItem in gridTableView.Items)
                {

                    Int32 PracticeLocationID = Convert.ToInt32(Session["practiceLocationID"]);
                    Int32 ProductInventoryID = Convert.ToInt32((string)gridDataItem["ProductInventoryID"].Text);
                    string txtPatientCode = this.txtPatientCode.Text;
                    Int32 PracticeCatalogProductID = Convert.ToInt32((string)gridDataItem["PracticeCatalogProductID"].Text);

                    // if cboPhysician.value is null or empty use -1
                    Int32 PhysicianID = ((cboPhysicians.Value != null) && (cboPhysicians.Value.Length > 0) //mws changed cboPhysicians.Value.Length > 1 to cboPhysicians.Value.Length > 0
                        ? Convert.ToInt32(cboPhysicians.Value.ToString()) : -1);

                    decimal BillingCharge = Convert.ToDecimal((string)gridDataItem["BillingCharge"].Text.Replace("$", ""));
                    decimal BillingChargeCash = Convert.ToDecimal((string)gridDataItem["BillingChargeCash"].Text.Replace("$", ""));
                    RadNumericTextBox txtQuantity = (RadNumericTextBox)gridDataItem.FindControl("txtQuantityDispensed");
                    Int32 Quantity = Convert.ToInt32(txtQuantity.Value.ToString());
                    //Int32 Quantity = Convert.ToInt32(gridDataItem.FindControl("txtQuantityDispensed"));
                    CheckBox checkBox = (CheckBox)gridDataItem.FindControl("chkDMEDeposit");
                    Boolean IsCashCollection = checkBox.Checked;
                    decimal WholesaleCost = Convert.ToDecimal((string)gridDataItem["WholesaleCost"].Text.Replace("$", ""));
                    RadNumericTextBox txtDMEDeposit = (RadNumericTextBox)gridDataItem.FindControl("txtDMEDeposit");
                    decimal DMEDeposit = Convert.ToDecimal(txtDMEDeposit.Text.Replace("$", ""));
                    decimal ActualChargeBilled;
                    if (IsCashCollection)
                    {
                        ActualChargeBilled = BillingChargeCash;
                    }
                    else
                    {
                        ActualChargeBilled = BillingCharge;
                    }

                    DateTime DispenseDate = Convert.ToDateTime(dtDispensed.SelectedDate.ToString());
                    DispenseID = DispenseProducts(1, PracticeLocationID, ProductInventoryID, txtPatientCode,
                        PracticeCatalogProductID, PhysicianID, ActualChargeBilled, Quantity, IsCashCollection,
                        WholesaleCost, DMEDeposit, DispenseID, DispenseDate, false);

                    //Set ViewState for Dispense ID
                    ViewState["DispenseID"] = DispenseID;


                }
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        private Boolean LoopThroughReceiptHistoryGrid(GridTableView gridTableView)
        {

            try
            {
                Boolean isItem = false;
                foreach (GridDataItem gridDataItem in gridTableView.Items)
                {

                    if (findSelectColumn(gridDataItem))
                    {
                        Int32 DispenseID = Convert.ToInt32((string)gridDataItem["DispenseID"].Text);
                        string strURL = String.Concat("Authentication/DispenseReceipt.aspx?DispenseID=", DispenseID.ToString(), "&PLID=", Session["practiceLocationID"].ToString(), "&PLName=", Session["PracticeLocationName"].ToString(), "&PracticeName=", Session["PracticeName"].ToString());
                        ResponseHelper.Redirect(strURL, "_blank", "");
                        //RadAjaxPanel2.EnableAJAX = true;
                        isItem = true;
                    }



                }
                return isItem;
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }


        public Int32 DispenseProducts(int UserID, int practiceLocationID, int ProductInventoryID,
                string PatientCode, int PracticeCatalogProductID, int PhysicianID,
                decimal ActualChargeBilled, int Quantity, Boolean IsCashCollection,
                decimal WholesaleCost,decimal DMEDeposit, int DispenseID, DateTime DispenseDate, Boolean CreatedWithHandheld)
        {
            try
            {
                int rowsAffected = 0;

                Database db = DatabaseFactory.CreateDatabase("BregVisionLocalConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_DispenseProductsNew");

                db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
                db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
                db.AddInParameter(dbCommand, "ProductInventoryID", DbType.Int32, ProductInventoryID);
                db.AddInParameter(dbCommand, "PatientCode", DbType.String, PatientCode);
                db.AddInParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, PracticeCatalogProductID);

                db.AddInParameter(dbCommand, "PhysicianID", DbType.Int32, PhysicianID);

                db.AddInParameter(dbCommand, "ActualChargeBilled", DbType.Decimal, ActualChargeBilled);
                db.AddInParameter(dbCommand, "Quantity", DbType.Int32, Quantity);
                db.AddInParameter(dbCommand, "IsCashCollection", DbType.Boolean, IsCashCollection);
                db.AddInParameter(dbCommand, "WholesaleCost", DbType.Decimal, WholesaleCost);
                db.AddInParameter(dbCommand, "DMEDeposit", DbType.Decimal, DMEDeposit);
                db.AddInParameter(dbCommand, "DispenseIDIn", DbType.Decimal, DispenseID);
                db.AddInParameter(dbCommand, "DispenseDate", DbType.DateTime, DispenseDate);
                db.AddInParameter(dbCommand, "CreatedWithHandheld", DbType.Boolean, CreatedWithHandheld);
                db.AddOutParameter(dbCommand, "DispenseID", DbType.Int32, 4);
                rowsAffected = db.ExecuteNonQuery(dbCommand);
                Int32 intStatus = Convert.ToInt32(db.GetParameterValue(dbCommand, "DispenseID"));
                return intStatus;


            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        private void UpdateDispenseTotals(int DispenseID)
        {
            try
            {
                int rowsAffected = 0;

                Database db = DatabaseFactory.CreateDatabase("BregVisionLocalConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_DispenseProducts_UpdateTotals");

                db.AddInParameter(dbCommand, "DispenseID", DbType.Int32, DispenseID);
                rowsAffected = db.ExecuteNonQuery(dbCommand);
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        private void UpdateCustomBrace(int DispenseID, int BregVisionOrderID)
        {
            try
            {
                int rowsAffected = 0;

                Database db = DatabaseFactory.CreateDatabase("BregVisionLocalConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_DispenseProducts_UpdateCustomBrace");

                db.AddInParameter(dbCommand, "DispenseID", DbType.Int32, DispenseID);
                db.AddInParameter(dbCommand, "BregVisionOrderID", DbType.Int32, BregVisionOrderID);
                rowsAffected = db.ExecuteNonQuery(dbCommand);
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
                ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler();
                string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();


                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }



        protected void btnPrintReceiptHistory_Click(object sender, EventArgs e)
        {
            try
            {

                Boolean isItem = LoopThroughReceiptHistoryGrid(grdReceiptHistory.MasterTableView);
                if (!isItem)
                {
                    lblReceiptHistory.ForeColor = System.Drawing.Color.Red;
                    lblReceiptHistory.Text = "No receipt selected. Please choose a receipt and press the 'Print Preview' button. ";
                }
                else
                {
                    lblReceiptHistory.ForeColor = System.Drawing.Color.Black;
                    lblReceiptHistory.Text = "Please choose a receipt and press the 'Print Preview' button. ";
                }
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion

        }


        protected void btnPrintReceipt_Click(object sender, EventArgs e)
        {
            try
            {
                string strURL = String.Concat("Authentication/DispenseReceipt.aspx?DispenseID=", ViewState["DispenseID"].ToString(), "&PLID=", Session["practiceLocationID"].ToString(), "&PLName=", Session["PracticeLocationName"].ToString(), "&PracticeName=", Session["PracticeName"].ToString());

                // RadWindowManager1.EnableStandardPopups = true;

                //RadAjaxPanel2.EnableAJAX = false;

                ResponseHelper.Redirect(strURL, "_blank", "");
                RadAjaxPanel2.EnableAJAX = true;
                //RadAjaxPanel2.EnableAJAX = true;
                // RadAjaxPanel2.Enabled = true;
                // RadWindowManager1.Enabled = true;
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion

        }

        protected void txtRecordsDisplayed_TextChanged(object sender, EventArgs e)
        {

            try
            {
                Session["RecordsDisplayed"] = txtRecordsDisplayed.Text.ToString();
                grdInventory.PageSize = Convert.ToInt16(Session["RecordsDisplayed"].ToString());
                //grdSearch.PageSize = Convert.ToInt16(Session["RecordsDisplayed"].ToString());
                grdSummary.PageSize = Convert.ToInt16(Session["RecordsDisplayed"].ToString());
                grdReceiptHistory.PageSize = Convert.ToInt16(Session["RecordsDisplayed"].ToString());
                //next code refreshes grid for particular page that is displayed
                switch (rmpMain.SelectedIndex)
                {
                    case 0:
                        {
                            grdInventory.Rebind();
                            break;
                        }
                    case 2:
                        {
                            grdSummary.Rebind();
                            break;
                        }
                    case 5:
                        {
                            grdReceiptHistory.Rebind();
                            break;
                        }


                }

            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        protected void grdSummary_ItemDataBound(object sender, GridItemEventArgs e)
        {

            try
            {

                if (e.Item is GridDataItem)
                {

                    GridDataItem dataItem = (GridDataItem)e.Item;

                    try
                    {
                        RadNumericTextBox txtQuantityDispensed = dataItem.FindControl("txtQuantityDispensed") as RadNumericTextBox;
                        if (txtQuantityDispensed != null)
                        {
                            // Comment this out to remove the safeguard on the dispense.
                            if (lblTurnOffDispenseSummarySafeguard.Text.Length > 0)
                            {
                                // do not set the maxvalue for the quantity to dispense
                            }
                            else
                            {
                                // RadNumericTextBox txtQuantityDispensed = (RadNumericTextBox)dataItem.FindControl("txtQuantityDispensed");
                                txtQuantityDispensed.MaxValue = System.Convert.ToInt32(dataItem["QuantityOnHandPerSystem"].Text);
                            }
                            if (ViewState["BregVisionOrderID"] != null)
                            {
                                //mws turn this off to handle bilat custom braces
                                //txtQuantityDispensed.Enabled = false;
                            }
                        }

                    }
                    catch
                    {
                        return;
                    }
                }


            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion

        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                switch (rmpMain.SelectedIndex)
                {
                    case 2:
                        {
                            rmpMain.SelectedIndex = 0;

                            PageView1.Selected = true;
                            //pvDispense.SelectedIndex = 1;
                            break;
                        }
                    case 3:
                        {
                            rmpMain.SelectedIndex = 2;
                            PageView3.Selected = true;
                            //pvDispense.SelectedIndex = 2;
                            break;
                        }
                    case 4:
                        {
                            rmpMain.SelectedIndex = 3;
                            PageView5.Selected = true;
                            //pvDispense.SelectedIndex = 1;
                            break;
                        }


                }
                rmpMain_TabClick(rmpMain, e);
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                switch (rmpMain.SelectedIndex)
                {
                    case 0:
                        {
                            rmpMain.SelectedIndex = 2;

                            PageView3.Selected = true;
                            //pvDispense.SelectedIndex = 1;
                            break;
                        }
                    case 1:
                        {
                            rmpMain.SelectedIndex = 2;

                            PageView3.Selected = true;
                            //pvDispense.SelectedIndex = 1;
                            break;
                        }
                    case 2:
                        {
                            rmpMain.SelectedIndex = 3;
                            PageView5.Selected = true;
                            //pvDispense.SelectedIndex = 2;
                            break;
                        }
                    case 3:
                        {
                            rmpMain.SelectedIndex = 4;
                            PageView6.Selected = true;
                            //pvDispense.SelectedIndex = 1;
                            break;
                        }
                }
                rmpMain_TabClick(rmpMain, e);
            }
            #region Exception Handler
            catch (Exception ex)
            {

                StringBuilder sb = new StringBuilder();
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);

                foreach (System.Diagnostics.StackFrame frame in st.GetFrames())
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }
               ErrHandler.VisionErrHandler KEH = new ErrHandler.VisionErrHandler(); 
				string result = "Error in function '" + System.Reflection.MethodBase.GetCurrentMethod().Name + "':" + Request.Url.ToString() + ":" + ex.Message + ":" + ex.Source + ":" + GetUserErrorInfo() + sb.ToString() + ":" + KEH.GetUserData();

                
                KEH.RaiseError(result);
                throw;

            }
            #endregion
        }
    }
}
