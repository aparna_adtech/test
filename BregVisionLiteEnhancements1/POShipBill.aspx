<%@ Page Language="C#" MasterPageFile="~/MasterPages/SiteMaster.Master" AutoEventWireup="true" CodeBehind="POShipBill.aspx.cs" Inherits="BregVision.POShipBill" Title="Untitled Page" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">

  <script type="text/javascript">
    function showToolTip(element, context) {
      var tooltipManager = $find("<%= RadToolTipManager1.ClientID %>");

      if (!tooltipManager) return;

      var tooltip = tooltipManager.getToolTipByElement(element);

      if (!tooltip) {
        tooltip = tooltipManager.createToolTip(element);

        tooltip.set_value(context);
      }

      element.onmouseover = null;

      try {
        tooltip.show();
      } catch (ex) {
        alert(ex.description);
      }
    }
  </script>

  <telerik:RadToolTipManager ID="RadToolTipManager1" Width="300px" Height="100px" RelativeTo="Element" Position="BottomRight" runat="server" AnimationDuration="200">
    <WebServiceSettings Path="HelpSystem/ToolTipWebService.asmx" Method="GetToolTip" />
  </telerik:RadToolTipManager>


  <div class="flex-row subWelcome-bar">
    <h1 class="flex-row align-center justify-start">Purchase Order&nbsp;&nbsp;<small class="light">Shipping, Billing, and Shipping Method</small>
    </h1>
    <asp:Button ID="btnNext" runat="server" Text="Next" OnClick="btnNext_Click" CssClass="button-primary" />
  </div>
  <div class="subTabContent-container">
    <div class="content">
      <div class="flex-row align-start justify-spaceBetween">
        <div class="flex-column align-start flex-grow">
          <h2>Shipping Address&nbsp;<bvu:Help ID="help120" runat="server" HelpID="120" />
          </h2>
          <table class="RadGrid_Breg flex-grow">
            <tr>
              <td>
                <strong>Attention</strong>
              </td>
              <td>
                <asp:Label ID="ShipAttention" runat="server"></asp:Label>
              </td>
            </tr>
            <tr>
              <td>
                <strong>Address</strong>
              </td>
              <td>
                <asp:Label ID="ShipAddress1" runat="server"></asp:Label><br />
                <asp:Label ID="ShipAddress2" runat="server"></asp:Label>
              </td>
            </tr>
            <tr>
              <td>
                <strong>City</strong>
              </td>
              <td>
                <asp:Label ID="ShipCity" runat="server"></asp:Label>
              </td>
            </tr>
            <tr>
              <td>
                <strong>State</strong>
              </td>
              <td>
                <asp:Label ID="ShipState" runat="server"></asp:Label>
              </td>
            </tr>
            <tr>
              <td>
                <strong>Zip Code</strong>
              </td>
              <td>
                <asp:Label ID="ShipZipCode" runat="server"></asp:Label>
              </td>
            </tr>
          </table>
        </div>
        <div class="flex-column align-start flex-grow">
          <h2>Billing Address&nbsp;<bvu:Help ID="help108" runat="server" HelpID="108" />
          </h2>
          <table class="RadGrid_Breg flex-grow">
            <tr>
              <td><strong>Attention</strong></td>
              <td>
                <asp:Label ID="BillAttention" runat="server"></asp:Label></td>
            </tr>
            <tr>
              <td><strong>Address</strong></td>
              <td>
                <asp:Label ID="BillAddress1" runat="server"></asp:Label><br />
                <asp:Label ID="BillAddress2" runat="server"></asp:Label></td>
            </tr>
            <tr>
              <td><strong>City</strong></td>
              <td>
                <asp:Label ID="BillCity" runat="server"></asp:Label></td>
            </tr>
            <tr>
              <td><strong>State</strong></td>
              <td>
                <asp:Label ID="BillState" runat="server"></asp:Label></td>
            </tr>
            <tr>
              <td><strong>Zip Code</strong></td>
              <td>
                <asp:Label ID="BillZipCode" runat="server"></asp:Label></td>
            </tr>
          </table>
        </div>
        <div class="flex-column align-start flex-grow">
          <h2>Shipping Method&nbsp;<bvu:Help ID="help109" runat="server" HelpID="109" /></h2>
          <asp:RadioButtonList ID="radShippingType" runat="server" CssClass="RadioButtons flex">
            <asp:ListItem Selected="True" Value="4">Ground</asp:ListItem>
            <asp:ListItem Value="1">Next Day</asp:ListItem>
            <asp:ListItem Value="8">Next Day Saver</asp:ListItem>
            <asp:ListItem Value="2">Second Day</asp:ListItem>
            <asp:ListItem Value="3">Third Day</asp:ListItem>
          </asp:RadioButtonList>
        </div>
      </div>
    </div>
  </div>

</asp:Content>

