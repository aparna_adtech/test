using System;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using Microsoft.Practices.EnterpriseLibrary.Data;
using ClassLibrary.DAL;
using System.Linq;
using BregVision.Purchase;
using J2.eFaxDeveloper.Outbound;
using BLL.PDFUtils;

namespace BregVision
{
    public class FaxEmailPOs
    {

        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        System.Text.StringBuilder sbShip = new System.Text.StringBuilder();
        System.Text.StringBuilder sbBill = new System.Text.StringBuilder();

		private Int32 practiceLocationID;
		decimal grandTotal;
        private string practiceName;
        private string practiceLocationName;
        private string mailBody;

		public Int32 PracticeLocationID
		{
			get { return practiceLocationID; }
			set { practiceLocationID = value; }
		}


		public string MailBody
        {
            get { return mailBody; }
            set { mailBody = value; }
        }

        public string PracticeName
        {
            get { return practiceName; }
            set { practiceName = value; }
        }

        public string PracticeLocationName
        {
            get { return practiceLocationName; }
            set { practiceLocationName = value; }
        }
        private int bregVisionOrderID;

		public int BregVisionOrderID
		{
			get { return bregVisionOrderID; }
			set { bregVisionOrderID = value; }
		}
		private HttpContext Context
		{
			get;  set;
		}

        //public string BuildHTMLEMailFax() 
        //{
        //    sb.Append("<table><tr> <td style='font-size:x-small'>Breg Vision</td></tr></table><br><br>");
        //}

        // Get confirmationEmailAddress for the practiceLocation of the BregVisionOrderID

        public byte[] PrepareFaxEmailforCustomBrace(int PracticeID, int BregVisionOrderID, DataRow drCustomBraceInfo)
        {
			byte[] pdfBytes = CustomBracePDFUtils.BuildPDF(PracticeID, PracticeLocationID, BregVisionOrderID, 0, 0, true, "PDF", 0);
			return pdfBytes;
		}

        public void PrepareFaxEmailforSupplier(int BregVisionOrderID, byte[] customBracePDF, int shoppingCartID, BLL.IVisionSessionProvider sessionProvider)
        {
            PrepareFaxEmailforSupplier(BregVisionOrderID, null, customBracePDF, shoppingCartID, sessionProvider);
        }

        public void PrepareFaxEmailforSupplier(int BregVisionOrderID, int? supplierOrderID, byte[] customBracePDF, int? shoppingCartID, BLL.IVisionSessionProvider sessionProvider)
        {
            BLL.SecureMessaging secMsgModule = new BLL.SecureMessaging(sessionProvider);

            bool isConsignment = false;
            if (shoppingCartID.HasValue)
            {
                isConsignment = DAL.PracticeLocation.ShoppingCart.GetConsignment(
                    shoppingCartID.Value,
                    BregVisionOrderID);
            }

            //  2007.11.13 John Bongiorni  Modifications to send out fax/email copies via email
            //     to the practice locations confirmationEmailAddress.
            //  get the confirmationEmailAddress for the practice location given the BregVisionOrderID
            string confirmationEmailAddress = GetConfirmationEmailAddress(BregVisionOrderID);


            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetBVOSupplierInfoForFaxEmail_New");

            db.AddInParameter(dbCommand, "BregVisionOrderID", DbType.Int32, BregVisionOrderID);

            IDataReader reader = db.ExecuteReader(dbCommand);

            Int32 PONumber;
            string CustomPO;
            string SupplierName;
            //string BrandName;
            string ShippingTypeName;
            string PhoneWork;
            decimal SupplierTotal;
            DateTime DateCreated;
            Int32 SupplierOrderID;
            string EmailAddress;
            string Fax;
            string AccountCode;
            string logoPartNumber = "";

            while (reader.Read())
            {
                //Resets string builder
                sb.Remove(0, sb.Length);
                PONumber = Convert.ToInt32(reader["PurchaseOrder"].ToString());
                CustomPO = reader["CustomPurchaseOrderCode"].ToString();
                SupplierName = reader["SupplierName"].ToString();   // For Vendor Brand Consolidation, this is now used.
                //BrandName = reader["BrandName"].ToString();         // For Vendor Brand Consolidation this is no longer used
                ShippingTypeName = reader["ShippingTypeName"].ToString();
                PhoneWork = reader["PhoneWork"].ToString();
                SupplierTotal = Convert.ToDecimal(reader["Total"].ToString());
                DateCreated = Convert.ToDateTime(reader["CreatedDate"].ToString());

                // match supplier order ID if we were provided one (only process a match)
                SupplierOrderID = Convert.ToInt32(reader["supplierOrderID"].ToString());
                if (supplierOrderID.HasValue && (supplierOrderID.Value != SupplierOrderID))
                    continue;

                EmailAddress = reader["EmailAddress"].ToString();
                Fax = reader["Fax"].ToString();
                if (reader["AccountCode"].ToString() == null)
                {
                    AccountCode = "";
                }
                else
                {
                    AccountCode = reader["AccountCode"].ToString();
                }
                logoPartNumber = reader["LogoPartNumber"].ToString();

                //Opening Line
                sb.Append("<table border=0 style='width:100%'><tr bgcolor=White style='color:black'><td style='font-size:x-small'>Dear ");
                sb.Append(SupplierName);                // Show Supplier not Brand
                sb.Append(":");
                sb.Append("</td></tr><tr><td>&nbsp;</td></tr><tr><td  style='font-size:x-small'>");
                sb.Append("  PLEASE SHIP OUT THIS ORDER TODAY FOR:");

                sb.Append("</td></tr><tr><td>&nbsp;</td></tr>");


                sb.Append("<tr><td>");
                sb.Append("<table border=0 style='width:80%'>");  //nested table            
                sb.Append("<tr><td style='width:200px;font-size:x-small'>ACCOUNT NAME: ");
                sb.Append("</td><td style='width:400px;font-size:x-small'>");
                sb.Append(practiceName);
                sb.Append("</td></tr>");

                sb.Append("<tr><td style='font-size:x-small'>ACCOUNT NO: ");
                sb.Append("</td><td  style='font-size:x-small'>");
                sb.Append(AccountCode);


                if (CustomPO.Length == 0)
                {
                    sb.Append("</td></tr><tr><td style='font-size:x-small'>PURCHASE ORDER NO: ");
                    sb.Append("</td><td  style='font-size:x-small'>");
                    sb.Append(PONumber);
                }
                else
                {
                    sb.Append("</td></tr><tr><td style='font-size:x-small'>Custom PO: ");
                    sb.Append("</td><td  style='font-size:x-small'>");
                    sb.Append(CustomPO);
                }

                sb.Append("</td></tr><tr><td style='font-size:x-small'>Order Date: ");
                sb.Append("</td><td  style='font-size:x-small'>");
                sb.Append(System.DateTime.Now);

                //sb.Append(System.DateTime.Today);
                //sb.Append("&nbsp;&nbsp;");
                //sb.Append(System.DateTime.Today.ToShortTimeString);

                sb.Append(" PST");          // Denote Pacific Standard Time.

                string shippingTypeBold = ShippingTypeName.ToLower().Contains("next day") ? "font-weight:bolder;" : "";
                string shippingTypeSize = ShippingTypeName.ToLower().Contains("next day") ? "font-size: medium;" : "font-size:x-small;";
                string shippingTypeRedForEmail = (ShippingTypeName.ToLower().Contains("next day") && (IsemailOrderPlacement(BregVisionOrderID, SupplierOrderID))) ? "color:Red;" : "";

                // Delivery Method is now here.
                sb.Append("</td></tr><tr><td style='font-size:x-small'>Delivery Method: ");
                sb.Append(string.Format("</td><td  style='{0}{1}{2}'>", shippingTypeSize, shippingTypeBold, shippingTypeRedForEmail));

                sb.Append(ShippingTypeName);
                //sb.Append("***");
                sb.Append("</td></tr>");
                //sb.Append("</table>");

                //Location Phone number is now here.        
                sb.Append("<tr><td style='width:200px;font-size:x-small'>Location Phone #: ");
                sb.Append("</td><td style='width:400px;font-size:x-small'>");
                sb.Append(PhoneWork);

                if (SupplierName.ToLower().Contains("breg") && !string.IsNullOrEmpty(logoPartNumber))
                {
                    //Logo Part number is now here.        
                    sb.Append("</td></tr><tr><td style='width:200px;font-size:x-small'>LogoPart #: ");
                    sb.Append("</td><td style='width:400px;font-size:x-small'>");
                    sb.Append(logoPartNumber);
                }

                sb.Append("</td></tr><tr><td colspan=2>&nbsp;</td></tr><tr><td colspan=2 style='font-size:x-small'>Confirm receipt of order immediately to ");
                sb.Append(EmailAddress);
                sb.Append(" or FAX To ");
                sb.Append(Fax);
                sb.Append("</td></tr><tr><td colspan=2 style='font-size:x-small'>Issues with order should be addressed to ");
                sb.Append(EmailAddress);
                sb.Append(" (admin email) as soon as possible. ");

                sb.Append("</td></tr></table><br>");

                //Bold - Ship to and Bill to addresses could be different
                sb.Append("<table style='width:100%'><tr><td colspan=2 style='font-size:medium;font-weight:bolder;'>PLEASE CONFIRM BILL TO ADDRESS FOR BILLING PURPOSES AS IT MAY BE DIFFERENT FROM THE SHIP TO ADDRESS. </td></tr></table><br>");

                //Get Shipping and Billing info. 
                //Shipping
                sbShip.Remove(0, sbShip.Length);  //reset sbShip
                IDataReader readerShipTo = PopulateShippingAddress(practiceLocationID);
                if (readerShipTo.Read())
                {

                    sbShip.Append("<table style='width:100%'><tr><td colspan=2 style='font-size:x-small'>SHIP TO:</td></tr>");
                    sbShip.Append("<tr><td style='width:25px'></td><td style='font-size:x-small'>");
                    sbShip.Append(practiceName);
                    sbShip.Append("</td></tr>");
                    sbShip.Append("<tr><td style='width:25px'></td><td style='font-size:x-small'>");
                    sbShip.Append(practiceLocationName);
                    sbShip.Append("</td></tr>");
                    sbShip.Append("<tr><td style='width:25px'></td><td style='font-size:x-small'>");
                    sbShip.Append("Attn: ");
                    sbShip.Append(readerShipTo["ShipAttentionOf"].ToString());
                    sbShip.Append("</td></tr>");
                    sbShip.Append("<tr><td style='width:25px'></td><td style='font-size:x-small'>");
                    sbShip.Append(readerShipTo["ShipAddress1"].ToString());
                    sbShip.Append("</td></tr>");
                    if (readerShipTo["ShipAddress2"].ToString().Length > 0)
                    {
                        sbShip.Append("<tr><td style='width:25px'></td><td style='font-size:x-small'>");
                        sbShip.Append(readerShipTo["ShipAddress2"].ToString());
                        sbShip.Append("</td></tr>");
                    }
                    sbShip.Append("<tr><td style='width:25px'></td><td style='font-size:x-small'>");
                    sbShip.Append(readerShipTo["ShipCity"].ToString());
                    sbShip.Append(", ");
                    sbShip.Append(readerShipTo["ShipState"].ToString());
                    sbShip.Append(" ");
                    sbShip.Append(readerShipTo["ShipZipCode"].ToString());
                    sbShip.Append("</td></tr>");
                    sbShip.Append("</table><br>");

                    readerShipTo.Close();

                }

                //Billing
                sbBill.Remove(0, sbBill.Length);  //reset sbBill
                IDataReader readerBillTo = PopulateBillingAddress(practiceLocationID);
                if (readerBillTo.Read())
                {

                    sbBill.Append("<table style='width:100%'><tr><td colspan=2 style='font-size:x-small'>Bill TO:</td></tr>");
                    sbBill.Append("<tr><td style='width:25px'></td><td style='font-size:x-small'>");
                    sbBill.Append(practiceName);
                    sbBill.Append("</td></tr>");
                    sbBill.Append("<tr><td style='width:25px'></td><td style='font-size:x-small'>");
                    sbBill.Append("Attn: ");
                    sbBill.Append(readerBillTo["BillAttentionOf"].ToString());
                    sbBill.Append("</td></tr>");
                    sbBill.Append("<tr><td style='width:25px'></td><td style='font-size:x-small'>");
                    sbBill.Append(readerBillTo["BillAddress1"].ToString());
                    sbBill.Append("</td></tr>");
                    if (readerBillTo["BillAddress2"].ToString().Length > 0)
                    {
                        sbBill.Append("<tr><td style='width:25px'></td><td style='font-size:x-small'>");
                        sbBill.Append(readerBillTo["BillAddress2"].ToString());
                        sbBill.Append("</td></tr>");
                    }
                    sbBill.Append("<tr><td style='width:25px'></td><td style='font-size:x-small'>");
                    sbBill.Append(readerBillTo["BillCity"].ToString());
                    sbBill.Append(", ");
                    sbBill.Append(readerBillTo["BillState"].ToString());
                    sbBill.Append(" ");
                    sbBill.Append(readerBillTo["BillZipCode"].ToString());
                    sbBill.Append("</td></tr>");
                    sbBill.Append("</table><br>");

                    readerBillTo.Close();

                }

                //table for Ship To and Bill To nested tables.
                sb.Append("<table border=0 style='width:100%'>");
                sb.Append("<tr><td>");
                sb.Append(sbShip.ToString());  //nested Ship to table
                sb.Append("</td>");

                sb.Append("<td>");
                sb.Append(sbBill.ToString());  //nested Bill to table
                sb.Append("</td></tr>");
                sb.Append("</table>");


                GetSupplierLineItems(SupplierOrderID);


                //Supplier Total
                sb.Append("<table  border=0 style='width:100%'><tr> <td style='font-size:x-small' align=right>Supplier Total $");
                sb.Append(SupplierTotal.ToString("#,##0.00"));
                sb.Append("</td></tr></table><br>");

				//sb.Append("<table  border=0 style='width:100%'><tr> <td style='font-size:x-small' align=right>Grand Total $");
				//sb.Append(grandTotal.ToString("#,##0.00"));
				//sb.Append("</td></tr></table><br><br>");

				sb.Append("***Note: In case the fax is not received, resend to ");
                sb.Append(EmailAddress);

                sb.Append("</td></tr></table>"); //added by u= 02/25/2010

                // Create and attach the custom brace file if any
                string pdfPath = null;
                if (customBracePDF != null && customBracePDF.Length > 0)
                    pdfPath = CreateEmailFaxCustomBracePDF(customBracePDF);

                //sb.Append(CustomBraceHTML);

                //  2007.11.13 John Bongiorni  Modifications to send out fax/email copies via email
                //     to the practice locations confirmationEmailAddress.

                #region Breg Oracle Integration
                // alwaysEmailBregOrders is true if the AppSetting for AlwaysEmailBregOrders is a true value
                // NOTE: this is primarily for testing so orders go via XML and email and should be turned off for production use
                bool alwaysEmailBregOrders = true;
                try
                {
                    alwaysEmailBregOrders = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["AlwaysEmailBregOrders"]);
                }
                catch { }

                // isBregXMLOrderEnabled is true if the AppSetting for BregXMLOrderInterfaceEnabled is a true value
                bool isBregXMLOrderEnabled = false;
                try
                {
                    isBregXMLOrderEnabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["BregXMLOrderInterfaceEnabled"]);
                }
                catch { }

                // isBregXMLOrder is true if this was an xml order to breg
                bool isBregXMLOrder = false;
                bool didBregXMLThrowException = false;
                try
                {
                    if (isBregXMLOrderEnabled && (customBracePDF == null || customBracePDF.Length == 0))
                    {
                        // old concurrent job version of Vision-to-Oracle
                        //isBregXMLOrder = BregOracle.BregOracleOrder.CreateXMLOrder(BregVisionOrderID, SupplierOrderID);

                        // new web service version of Vision-to-Oracle
                        var submit_response = new Vision2OracleV2Service.Vision2OracleClient().SubmitOrderSyncV2(BregVisionOrderID, SupplierOrderID, EmailAddress);
                        isBregXMLOrder = submit_response.Status == Vision2OracleV2Service.SubmitOrderStatus.Success;
                        didBregXMLThrowException = submit_response.Status == Vision2OracleV2Service.SubmitOrderStatus.Exception;
                    }
                }
                catch (Exception ex)
                {
                    SendEmail("VisionMaintenance@breg.com", ex.Message + " " + ex.StackTrace + " " + ex.Source, string.Format(@"An error occurred in the PrepareFaxEmailForSupplier method attempting to create a bregXML order: {0}_{1}.", BregVisionOrderID, SupplierOrderID), BLL.SecureMessaging.MessageClass.BregMaintenance);
                }
                #endregion Breg Oracle Integration

                #region email or fax order
                {
                    // generate e-mail message subject (used by email to supplier, confirmation e-mail, and consignment notification)
                    string MailSubject = string.Empty;
                    string MailSubjectPrefix = string.Empty;
                    {
                        // add failsafe notice
                        if (didBregXMLThrowException)
                            MailSubjectPrefix += System.Configuration.ConfigurationManager.AppSettings["Vis2OraFailsafeSubjectTag"] ?? "*** FAILSAFE *** ";
                        // add regular subject contents
                        if (CustomPO.Length == 0)
                            MailSubject += String.Concat("Breg Vision Order - PO: ", PONumber);
                        else
                            MailSubject += String.Concat("Breg Vision Order - Custom PO: ", CustomPO.ToString());
                    }

                    // only send e-mail if it's not handled by Vision-to-Oracle and we're not flagged to always email
                    if (!isBregXMLOrder || alwaysEmailBregOrders)
                    {
                        // send e-mail or fax to supplier
                        // NOTE: IsemailOrderPlacement populates Email and FaxNumber with supplier's information
                        if (IsemailOrderPlacement(BregVisionOrderID, SupplierOrderID))
                            SendEmail(Email, sb.ToString(), MailSubjectPrefix + MailSubject, pdfPath, BLL.SecureMessaging.MessageClass.Orders, sessionProvider);
                        else
                            SendFax(FaxNumber, sb.ToString(), pdfPath);
                    }

                    // send confirmation e-mail if address is available
                    if (confirmationEmailAddress.Length > 1)
                        SendEmail(confirmationEmailAddress, sb.ToString(), MailSubject, BLL.SecureMessaging.MessageClass.Orders, sessionProvider);

                    // send consignment notification if this is a consignment replishment order
                    //MWS_TODO_Later: send the consignment email if it is an xml order
                    if (isConsignment == true) // is consignment
                        SendEmail(DAL.PracticeLocation.ShoppingCart.GetConsignmentAdminEmail(), sb.ToString(), MailSubject, BLL.SecureMessaging.MessageClass.Orders, sessionProvider);

#if(!DEBUG)
                    // send notification to vision maintenance
                    SendEmail("VisionMaintenance@breg.com", sb.ToString(), MailSubject, BLL.SecureMessaging.MessageClass.BregMaintenance);
#endif
                }
                #endregion email or fax order
            }

            reader.Close();
            //Supplier Total

        }

		public string CreateEmailFaxCustomBracePDF(byte[] customBracePDF)
		{
			string fullPath = null;
			if (customBracePDF != null && customBracePDF.Length != 0)
			{
                string subPath = System.Configuration.ConfigurationManager.AppSettings["EmailFaxCustomBracePDFFilePath"];
                if (subPath == null)
                    subPath = "EmailFaxPDFs";
                string file = "EmailFaxPDF_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".pdf";
                string[] paths = new string[] { HttpContext.Current.Request.MapPath(string.Empty), subPath, file };
                fullPath = Path.Combine(paths);
                string fullDirectory = Path.GetDirectoryName(fullPath);
                if (!Directory.Exists(fullDirectory))
                    Directory.CreateDirectory(fullDirectory);

                using (FileStream pdfFile = File.OpenWrite(fullPath))
				{
					pdfFile.Write(customBracePDF, 0, customBracePDF.Length);
					pdfFile.Close();
				}
			}
			return fullPath;
		}


		public static string GetConfirmationEmailAddress(int bregVisionOrderID)
        {
            int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_Select_ConfirmationEmailAddress_Given_BregVisionOrderID");

            db.AddInParameter(dbCommand, "BregVisionOrderID", DbType.Int32, bregVisionOrderID);
            db.AddOutParameter(dbCommand, "EmailForOrderConfirmation", DbType.String, 200);

            rowsAffected = db.ExecuteNonQuery(dbCommand);

            string emailForOrderConfirmation = ((db.GetParameterValue(dbCommand, "EmailForOrderConfirmation") != DBNull.Value) ? Convert.ToString(db.GetParameterValue(dbCommand, "EmailForOrderConfirmation")) : "");
            //emailForOrderConfirmation += ",VisionMaintenance@breg.com";
            return emailForOrderConfirmation; //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
        }


        public string GetSuppliersAndLineItems(int BregVisionOrderID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetBVOSupplierInfoForFaxEmail_New");

            db.AddInParameter(dbCommand, "BregVisionOrderID", DbType.Int32, BregVisionOrderID);

            IDataReader reader = db.ExecuteReader(dbCommand);

            Int32 PONumber;
            string customPONumber = "";
            string SupplierName;            // Supplier Name.
            //string BrandName;
            string ShippingTypeName;
            decimal SupplierTotal;
            DateTime DateCreated;
            Int32 SupplierOrderID;

            while (reader.Read())
            {

                PONumber = Convert.ToInt32(reader["PurchaseOrder"].ToString());
                customPONumber = reader["CustomPurchaseOrderCode"].ToString();

                SupplierName = reader["SupplierName"].ToString();                  //Supplier Name, not brand
                ShippingTypeName = reader["ShippingTypeName"].ToString();
                SupplierTotal = Convert.ToDecimal(reader["Total"].ToString());
                DateCreated = Convert.ToDateTime(reader["CreatedDate"].ToString());
                SupplierOrderID = Convert.ToInt32(reader["supplierOrderID"].ToString());
                //sb.Append("<table><tr> <td style='font-size:x-small'>Breg Vision</td></tr></table><br><br>");

                sb.Append("<table bgcolor=black cellpadding=0 cellspacing=1 border=0><tr bgcolor=White style='width:100%;color:black'> <td style='font-size:x-small'>");

                sb.Append("<table bgcolor=Black  style='color:black;width:100%' cellpadding=2 cellspacing=1  border=0><tr bgcolor=White style='color:black'> <td style='font-size:x-small'>Supplier:");
                sb.Append(SupplierName);
                sb.Append("&nbsp</td> <td style='font-size:x-small'>Purchase Order No:");
                if (customPONumber != null && customPONumber.Trim() != "")
                {
                    sb.Append(customPONumber);
                }
                else
                {
                    sb.Append(PONumber);
                }
                sb.Append("</td> <td style='font-size:x-small'>Shipping Method:");
                sb.Append(ShippingTypeName);
                sb.Append("</td> <td style='font-size:x-small'>Date:");
                sb.Append(DateCreated);
                sb.Append("</td><tr></table></td></tr></table><br>");
                //Get Supplier Order Details
                GetSupplierLineItems(SupplierOrderID);

                //Supplier Total
                sb.Append("<table  border=0 style='width:100'%><tr> <td style='font-size:x-small' align=right>Supplier Total $");
                sb.Append(SupplierTotal.ToString("#,##0.00"));
                sb.Append("</td></tr></table><br>");
                //sb.Append("Please contact your supplier if you have any questions.");

                //if (IsemailOrderPlacement(BregVisionOrderID,SupplierOrderID))
                //{
                //    //Send email
                //    string MailSubject = String.Concat("Breg Vision Order - PO: ", PONumber);
                //    SendEmail(Email, sb.ToString(), MailSubject);
                //}
                //else
                //{
                //    //Send fax
                //    SendFax(FaxNumber, sb.ToString());
                //}


            }
            reader.Close();
            //Supplier Total
            sb.Append("<table  border=0 style='width:100%'><tr> <td style='font-size:x-small' align=right>Grand Total $");
            sb.Append(grandTotal.ToString("#,##0.00"));
            sb.Append("</td></tr></table><br>");

            return sb.ToString();

        }

        private void GetSupplierLineItems(int SupplierOrderID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetBVOSupplierLineItemInfoForFaxEmail_New");

            db.AddInParameter(dbCommand, "supplierOrderID", DbType.Int32, SupplierOrderID);

            IDataReader reader = db.ExecuteReader(dbCommand);

            string ShortName;
            string Code;
            string Packaging;
            string Side;
            string Size;   // Add Size to proc!!!
            string Color;
            string Gender;
            decimal ActualWholesaleCost;
            Int32 QuantityOrdered;
            decimal LineTotal;
            //string SupplierName;        // Do not need SupplierName here.
            string BrandName;           // BrandName is in place of SupplierName
            bool isLogoPart = false;

            sb.Append("<table bgcolor=black cellpadding=0 cellspacing=1 style='width:100%' border=0><tr bgcolor=White style='color:black'> <td style='font-size:x-small'>");
            sb.Append("<table bgcolor=Black  style='color:black; width:100%' cellpadding=2 cellspacing=1  border=0><tr bgcolor=White style='color:black'> <td style='font-size:x-small' align=left>Brand</td> <td style='font-size:x-small' align=left>Product</td><td style='font-size:x-small'>Code</td> <td style='font-size:x-small'>Pkg.</td> <td style='font-size:x-small'>Side</td>  <td style='font-size:x-small'>Size</td> <td style='font-size:x-small'>Gender</td> <td style='font-size:x-small'>Color</td> <td style='font-size:x-small'>Logo</td> <td style='font-size:x-small'>Quantity</td> <td style='font-size:x-small'>Cost</td> <td style='font-size:x-small'>Total</td></tr>");
            while (reader.Read())
            {

                ShortName = reader["ShortName"].ToString();
                Code = reader["Code"].ToString();
                Packaging = reader["Packaging"].ToString();
                Side = reader["Side"].ToString();

                Size = reader["Size"].ToString();       // add Size to proc.

                Gender = reader["Gender"].ToString();

                Color = reader["Color"].ToString();

                ActualWholesaleCost = Convert.ToDecimal(reader["ActualWholesaleCost"].ToString());
                QuantityOrdered = Convert.ToInt32(reader["QuantityOrdered"].ToString());
                LineTotal = Convert.ToDecimal(reader["LineTotal"].ToString());
                BrandName = reader["BrandName"].ToString();  // Brand Name not Supplier Name
                grandTotal += LineTotal;

                isLogoPart = reader.GetBoolean(reader.GetOrdinal("IsLogoPart"));



                sb.Append("<tr bgcolor=White style='color:blue'> <td style='font-size:x-small' align=left>");
                sb.Append(BrandName);
                sb.Append("</td> <td style='font-size:x-small'>");
                sb.Append(ShortName);
                sb.Append("</td> <td style='font-size:x-small'>");
                sb.Append(Code);
                sb.Append("</td> <td style='font-size:x-small'>");
                sb.Append(Packaging);
                sb.Append("</td> <td style='font-size:x-small'>");
                sb.Append(Side);
                sb.Append("</td> <td style='font-size:x-small'>");

                sb.Append(Size);
                sb.Append("</td> <td style='font-size:x-small'>");  // Add size to proc.

                sb.Append(Gender);
                sb.Append("</td> <td style='font-size:x-small'>");

                sb.Append(Color);
                sb.Append("</td> <td style='font-size:x-small'>");
                if (isLogoPart == true)
                    sb.Append("Y");
                else
                    sb.Append("N");
                sb.Append("</td> <td style='font-size:x-small'>");

                sb.Append(QuantityOrdered);
                sb.Append("</td> <td style='font-size:x-small' align=right>$");
                sb.Append(ActualWholesaleCost.ToString("#,##0.00"));
                sb.Append("</td> <td style='font-size:x-small' align=right>$");
                sb.Append(LineTotal.ToString("#,##0.00"));
                sb.Append("</td></tr>");


                //Get Supplier Order Details

            }
            sb.Append("</table>");
            sb.Append("</td></tr></table>");
            reader.Close();
        }

        public string CreatePOSupplierEmailRequest()
        {

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(System.DateTime.Now);
            sb.Append("<br><br>");
            sb.Append("A purchase order request has been submitted for the location: ");
            sb.Append("<br><br>");
            //sb.Append(Session["PracticeLocationName"].ToString());

            sb.Append("<a href='http://www.bregvision.com/Authentication/AUorderapproval.aspx?PracticeLocationID=");
            //sb.Append(Session["PracticeLocationID"].ToString());
            //sb.Append("&ShippingTypeID=");
            //sb.Append(Session["ShippingTypeID"].ToString());
            sb.Append(" target='_blank'>Click this link to view the Purchase Order Request</a>");
            sb.Append("<br><br>");

            sb.Append("� Breg Copyright 2007");

            return sb.ToString();
        }
        string FaxNumber = "";
        string Email = "";

        // Fix the following to use Supplier Brand etc as above.
        private Boolean IsemailOrderPlacement(Int32 BregVisionOrderID, Int32 SupplierOrderID)
        {

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_IsEmailOrderPlacement");

            db.AddInParameter(dbCommand, "BregVisionOrderID", DbType.Int32, BregVisionOrderID);
            db.AddInParameter(dbCommand, "supplierOrderID", DbType.Int32, SupplierOrderID);

            IDataReader reader = db.ExecuteReader(dbCommand);

            //string ShortName;
            //string Code;
            //string Packaging;
            //string Side;
            //string Color;
            //string Gender;
            //decimal ActualWholesaleCost;
            //Int32 QuantityOrdered;
            //decimal LineTotal;

            while (reader.Read())
            {
                //#if(DEBUG)
                //               if(true==true)
                //#else
                if (Convert.ToBoolean(reader["IsemailOrderPlacement"].ToString()) == true)
                //#endif
                {
                    Email = reader["EmailOrderPlacement"].ToString();
#if(DEBUG)
                   Email = "klord@breg.com";
#endif
                    reader.Close();
                    return true;
                }
                else
                {
                    FaxNumber = reader["FaxOrderPlacement"].ToString();
                    reader.Close();
                    return false;
                }

                //ShortName = reader["ShortName"].ToString();
                //Code = reader["Code"].ToString();
                //Packaging = reader["Packaging"].ToString();
                //Side = reader["Side"].ToString();
                //Color = reader["Color"].ToString();
                //Gender = reader["Gender"].ToString();
                //ActualWholesaleCost = Convert.ToDecimal(reader["ActualWholesaleCost"].ToString());
                //QuantityOrdered = Convert.ToInt32(reader["ActualWholesaleCost"].ToString());
                //LineTotal = Convert.ToDecimal(reader["LineTotal"].ToString());



            }
            //reader.Close();


            return true;
        }


        public void SendFax(string faxNumber, String faxBody, string pdfPath)
        {
            string transmissionID = "Name Not Set";
            long currentFileTime = System.DateTime.Now.ToFileTimeUtc();
            transmissionID = currentFileTime.ToString().Substring(0, 15);

			string mergedPDF = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("N") + ".fax.pdf");
            try
            {
                PDFHelper.InsertHTMLIntoPDF(pdfPath, mergedPDF, faxBody, 1);
                SendFax(faxNumber, mergedPDF, 0);
            }
            finally
            {
                if (File.Exists(mergedPDF))
                    File.Delete(mergedPDF);
            }
        }

        private static J2.eFaxDeveloper.FaxFileType _GetFaxFileTypeFromFilename(string filename)
        {
            // empty returns txt
            if (string.IsNullOrEmpty(filename))
                return J2.eFaxDeveloper.FaxFileType.txt;

            // no extension returns txt
            var extension = Path.GetExtension(filename);
            if (string.IsNullOrEmpty(extension))
                return J2.eFaxDeveloper.FaxFileType.txt;

            // valid extension... match and return suitable document type
            switch (extension.ToLower())
            {
                case ".pdf":
                    return J2.eFaxDeveloper.FaxFileType.pdf;
                case ".htm":
                case ".html":
                    return J2.eFaxDeveloper.FaxFileType.html;
                case ".tif":
                case ".tiff":
                    return J2.eFaxDeveloper.FaxFileType.tif;
                default:
                    return J2.eFaxDeveloper.FaxFileType.txt;
            }
        }

        public void SendFax(string FaxNumber, String FaxName, Int32 FaxMessageID)
        {

            string transmissionID = "Name Not Set";
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            FaxMessage faxMessage = null;
            if (FaxMessageID == 0)
            {
                faxMessage = new FaxMessage();
                visionDB.FaxMessages.InsertOnSubmit(faxMessage);
            }
            else
            {
                var query = from fm in visionDB.FaxMessages
                            where fm.FaxMessageID == FaxMessageID
                            select fm;
                faxMessage = query.First<FaxMessage>();
            }

            //done - Yuiko, add one to the SendAttempts Property ()
            faxMessage.FaxNumber = FaxNumber;
            faxMessage.FaxFileName = FaxName;
            faxMessage.SentTime = DateTime.Now;
            faxMessage.CreatedDate = DateTime.Now;

            visionDB.SubmitChanges();
            if (faxMessage.Skip == true)
            {
                return;
            }

            //check to see if the fax is turned off - This turns the whole system on and off
            FaxServer faxController = visionDB.FaxServers.FirstOrDefault<FaxServer>();

            if (faxController.FaxEnabled == true)//|| (faxMessage.Skip == false ))
            {
                faxMessage.SendAttempts += 1;
                visionDB.SubmitChanges();
            }
            else
            { 
                //visionDB.SubmitChanges();
                return;
            }

            //visionDB.SubmitChanges(); // Yuiko, test this to make sure it saved here, and also saves the status at the end of this function

            int TransmissionIDStart = FaxName.IndexOf("test2") + 5;
            transmissionID = FaxName.Substring(TransmissionIDStart, 14); //done--Yuiko, make this 14 characters and add the first letter of the server name (System.Environment.MachineName)
            transmissionID = System.Environment.MachineName.Substring(0, 1) + transmissionID;

            if ((faxMessage.SendAttempts == 6) && faxMessage.Skip == false)
            {
                //done--Yuiko, get the transmission id out of the message.
                //Yuiko, Send Bobby an email "We have attempted to send the message with transmission id {0} 5 times and it has failed each time.  Please correct it or send it manually."

                SendEmail("VisionMaintenance@breg.com", string.Format(@"We have attempted to send the message with transmission id {0} 5 times and it has failed each time.  Please correct it or send it manually.", transmissionID), "Fax Failed", BLL.SecureMessaging.MessageClass.BregMaintenance);

                return;
            }

            try
            {
                //Instantiate the Main object 
                var faxclient = new OutboundClient();

                //Set your eFax Developer account identifier (required) 
                faxclient.AccountId = "7604776221";

                //Set your eFax Developer user name (required) 
                faxclient.UserName = "sromeo";

                //Set your eFax Developer password (required) 
                faxclient.Password = "victor";

                //Instantiate outbound request object
                var request = new OutboundRequest();

                //Set transmission ID
                request.TransmissionControl.TransmissionID = transmissionID;

                //Set "Dynamic Fax Header"
                request.TransmissionControl.FaxHeader = "";

                //Set fax disposition details
                request.DispositionControl.DispositionMethod = DispositionMethod.Post;
                request.DispositionControl.DispositionLevel = DispositionLevel.Error;
                request.DispositionControl.DispositionURL = System.Configuration.ConfigurationManager.AppSettings["EFaxNotificationURL"];
                //#if(DEBUG)
                //                request.DispositionControl.DispositionURL = "http://72.130.170.214/bregvision/EFaxNotification.aspx";
                //#endif

#if(!DEBUG)
                //outbound_req.DispositionControl.DispositionEmails.Add(
                //    new DispositionEmail
                //    {
                //        DispositionAddress = "Gregory.armstrong.ross@gmail.com",
                //        DispositionRecipient = "Greg Ross"
                //    });
                //outbound_req.DispositionControl.DispositionEmails.Add(
                //    new DispositionEmail
                //    {
                //        DispositionAddress = "VisionMaintenance@breg.com",
                //        DispositionRecipient = "Robert Haywood"
                //    });
#endif
                //outbound_req.DispositionControl.DispositionEmails.Add(
                //    new DispositionEmail
                //    {
                //        DispositionAddress = "michael_sneen@yahoo.com",
                //        DispositionRecipient = "Mike Sneen"
                //    });

                //Set the recipient fax number for this transmission (required) 
#if(DEBUG)
                //(619) 342-8639 Mike Sneen's free  online fax number
                FaxNumber = "6193428639";
                //FaxNumber = "6199224340"; //send to my cell so the fax won't go through
                //FaxNumber = "1112223333"; //Yuiko, Change this.  send to home # so the fax won't go through

                faxMessage.FaxNumber = FaxNumber;
                visionDB.SubmitChanges();
#endif
                request.Recipient.RecipientFax = FaxNumber;

                //Attach fax document
                request.Files.Add(
                    new FaxFile
                    {
                        FileContents = File.ReadAllBytes(FaxName),
                        FileType = _GetFaxFileTypeFromFilename(FaxName)
                    });

                //Perform the Post to eFax Developer 
                try
                {
                    if (FaxServerEnabled() == true)
                    {
                        var response = faxclient.SendOutboundRequest(request); //This is where it actually sends to EFax

                        if (response.StatusCode == StatusCode.Success)
                        {
                            faxMessage.Sent = true;
                            visionDB.SubmitChanges();
                        }
                        else // fax send failed
                        {
                            //string message = string.Format(@"StatusCode:{0}     StatusDescription:{1}     ErrorLevel:{2}     ErrorDescription:{3}", api.GetStatusCode(), api.GetStatusDescription(), api.GetErrorLevel(), api.GetErrorMessage());
                            //SendEmail("VisionMaintenance@breg.com", message, string.Format(@"An response code other than 1 was returned in the SendFax function trying to send {0} to efaxdeveloper to fax number{1}", transmissionID, FaxNumber));
                        }
                    }
                    else
                    {
                        //SendEmail("VisionMaintenance@breg.com", string.Format(@"The Fax Server is Disabled and Vision is trying to send {0} to efaxdeveloper to fax number{1}", transmissionID, FaxNumber), string.Format(@"The Fax Server is Disabled and Vision is trying to send {0} to efaxdeveloper to fax number{1}", transmissionID, FaxNumber));
                    }
                }
                catch (Exception ex)
                {
                    // A java.net.MalformedURLException may occur if you selected the POST Protocol HTTPS 
                    SendEmail("VisionMaintenance@breg.com", ex.Message + " " + ex.StackTrace + " " + ex.Source, string.Format(@"An error occurred trying to send {0} to efaxdeveloper to fax number{1}", transmissionID, FaxNumber), BLL.SecureMessaging.MessageClass.BregMaintenance);
                    //Interaction.MsgBox(ex.Message + ";" + Constants.vbCrLf + Constants.vbCrLf + ex.Source + ";" + Constants.vbCrLf + Constants.vbCrLf + ex.StackTrace); 
                    //return;
                }

            }
            catch (Exception exx)
            {
                SendEmail("VisionMaintenance@breg.com", exx.Message + " " + exx.StackTrace + " " + exx.Source, string.Format(@"An error occurred in the SendFax function trying to send {0} to efaxdeveloper to fax number{1}", transmissionID, FaxNumber), BLL.SecureMessaging.MessageClass.BregMaintenance);
            }
            //if (FaxMessageID == 0)
            //{
            //    visionDB.FaxMessages.InsertOnSubmit(faxMessage);
            //}
            //visionDB.SubmitChanges();
        }

        public static bool SendTestFax(string faxNumber, string message)
        {
            //Instantiate the Main object 
            var faxclient = new OutboundClient();

            //Set your eFax Developer account identifier (required) 
            faxclient.AccountId = "7604776221";

            //Set your eFax Developer user name (required) 
            faxclient.UserName = "sromeo";

            //Set your eFax Developer password (required) 
            faxclient.Password = "victor";

            //Instantiate outbound request object
            var request = new OutboundRequest();

            //Set transmission ID
            request.TransmissionControl.TransmissionID = "tf" + faxNumber;

            //Set "Dynamic Fax Header"
            request.TransmissionControl.FaxHeader = "";

            //Set fax disposition details
            request.DispositionControl.DispositionMethod = DispositionMethod.Post;
            request.DispositionControl.DispositionLevel = DispositionLevel.None;

//#if(DEBUG)
//            //(619) 342-8639 Mike Sneen's free  online fax number
//            faxNumber = "6193428639";
//#endif

            request.Recipient.RecipientFax = faxNumber;


            request.Files.Add(
            new FaxFile
            {

                FileContents = new System.Text.UTF8Encoding().GetBytes("This is a test fax from Breg Vision.  This message is just to verify that this is a valid fax number, and no action is necessary."),
                FileType = J2.eFaxDeveloper.FaxFileType.txt
            });


            var response = faxclient.SendOutboundRequest(request); //This is where it actually sends to EFax

            if (response.StatusCode == StatusCode.Success)
            {
                return true;
            }
            return false;
        }

        public static string GetStatus(String TransmissionID)
        {
            //Instantiate the Main object 
            var faxclient = new OutboundClient();

            //Set your eFax Developer account identifier (required) 
            faxclient.AccountId = "7604776221";

            //Set your eFax Developer user name (required) 
            faxclient.UserName = "sromeo";

            //Set your eFax Developer password (required) 
            faxclient.Password = "victor";

            try
            {
                // this will fail if we're dealing with multiple machines with different hostnames...
                var response = faxclient.SendOutboundStatusRequest(System.Environment.MachineName.Substring(0, 1) + TransmissionID, "");
                return response.Recipient.Status.Message + "; " + response.Recipient.Status.Classification;
            }
            catch
            {
                return "Error Returning Status";
            }
        }


        private bool FaxServerEnabled()
        {
            try
            {
                VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();
                var query = from fs in visionDB.FaxServers
                            select fs;

                FaxServer faxServer = query.First<FaxServer>();

                return faxServer.FaxEnabled;
            }
            catch
            {
                return true;
            }
        }

        private void ReadWriteStream(Stream readStream, Stream writeStream)
        {
            int Length = 256;
            Byte[] buffer = new Byte[Length];
            int bytesRead = readStream.Read(buffer, 0, Length);
            // write the required bytes
            while (bytesRead > 0)
            {
                writeStream.Write(buffer, 0, bytesRead);
                bytesRead = readStream.Read(buffer, 0, Length);
            }
            readStream.Close();
            writeStream.Close();
        }

		public void SendEmail(String MailTo, string MailBody, string MailSubject, string pdfPath, BLL.SecureMessaging.MessageClass messageClass, BLL.IVisionSessionProvider sessionProvider = null)
		{
			SendEmail("OrderConfirmation", MailTo, MailBody, MailSubject, pdfPath, messageClass, sessionProvider);
		}

		public void SendEmail(String MailTo, string MailBody, string MailSubject, BLL.SecureMessaging.MessageClass messageClass, BLL.IVisionSessionProvider sessionProvider = null)
        {
            SendEmail("OrderConfirmation", MailTo, MailBody, MailSubject, null, messageClass, sessionProvider);
        }

        public static void SendBillingNotificationEmail(int practiceLocationID, BLL.IVisionSessionProvider sessionProvider)
        {
            using (var visionDB = VisionDataContext.GetVisionDataContext())
            {
                var practiceQuery = from p in visionDB.Practices
                                    join pl in visionDB.PracticeLocations on p.PracticeID equals pl.PracticeID
                                    where pl.PracticeLocationID == practiceLocationID
                                    select new
                                    {
                                        PracticeName = p.PracticeName,
                                        PracticeLocationName = pl.Name
                                    };

                var practiceInfo = practiceQuery.FirstOrDefault();

                Page page = new Page();
                var ctlAll = new System.Web.UI.WebControls.Panel();


                BregVision.Authentication.InventoryComplete inventoryComplete = page.LoadControl("~/Authentication/InventoryComplete.ascx") as BregVision.Authentication.InventoryComplete;
                inventoryComplete.PracticeName = practiceInfo.PracticeName;
                inventoryComplete.PracticeLocationName = practiceInfo.PracticeLocationName;
                inventoryComplete.PracticeLocationID = practiceLocationID;

                inventoryComplete.LoadControlInfo();
                ctlAll.Controls.Add(inventoryComplete);
                ctlAll.Controls.Add(new LiteralControl("<br/><br/>"));

                var sb = new StringBuilder();
                var sw = new StringWriter(sb);
                var htmlTW = new HtmlTextWriter(sw);
                ctlAll.RenderControl(htmlTW);

                var html = sb.ToString();


                //    //--mws was turned on
                var MailTo = "";

                MailTo = System.Configuration.ConfigurationManager.AppSettings["ConsignmentAdminEmailAddress"];

#if(DEBUG)
                MailTo += ";klord@breg.com";
#endif

                if (string.IsNullOrEmpty(MailTo.Trim()) == false)
                {
                    FaxEmailPOs fe = new FaxEmailPOs();

                    fe.SendEmail("OrderConfirmation", MailTo, html, string.Format("DME Dispensement Information"), BLL.SecureMessaging.MessageClass.Billing, sessionProvider);
                }

            }
        }

		public void SendEmail(string MailFromProfile, String MailTo, string MailBody, string MailSubject, string pdfPath, BLL.SecureMessaging.MessageClass messageClass, BLL.IVisionSessionProvider sessionProvider = null)
		{
			try
			{
				BregWcfService.Classes.Email.SendEmail(MailFromProfile, MailTo, MailBody, MailSubject, pdfPath, messageClass, sessionProvider);
			}
			catch
			{
			}
		}

		private IDataReader PopulateBillingAddress(int practiceLocationID)
        {

            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.USP_GetBillingAddressByPracticeLocation");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.String, practiceLocationID);

            IDataReader reader = db.ExecuteReader(dbCommand);

            return reader;
            //if (reader.Read())
            //{
            //    BillAttention.Text = reader["BillAttentionOf"].ToString();
            //    BillAddress1.Text = reader["BillAddress1"].ToString();
            //    BillAddress2.Text = reader["BillAddress2"].ToString();
            //    BillCity.Text = reader["BillCity"].ToString();
            //    BillState.Text = reader["BillState"].ToString();
            //    BillZipCode.Text = reader["BillZipCode"].ToString();
            //}


        }

        private IDataReader PopulateShippingAddress(int practiceLocationID)
        {

            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.USP_GetShippingAddressByPracticeLocation");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.String, practiceLocationID);

            IDataReader reader = db.ExecuteReader(dbCommand);

            return reader;
            //if (reader.Read())
            //{
            //    ShipAttention.Text = reader["ShipAttentionOf"].ToString();
            //    ShipAddress1.Text = reader["ShipAddress1"].ToString();
            //    ShipAddress2.Text = reader["ShipAddress2"].ToString();
            //    ShipCity.Text = reader["ShipCity"].ToString();
            //    ShipState.Text = reader["ShipState"].ToString();
            //    ShipZipCode.Text = reader["ShipZipCode"].ToString();
            //}

        }

        public static void SendCancellationEmail(CancellationEmail cancellationEmail, System.Web.UI.Page page)
        {

            string mailBody = GetEmailText(cancellationEmail, page);

            FaxEmailPOs faxEmailPOs = new FaxEmailPOs();
            string toEmail = "NewVisionExpress@breg.com";
#if(DEBUG)
            toEmail = "klord@breg.com";
#endif
            faxEmailPOs.SendEmail("OrderConfirmation", toEmail, mailBody, "Cancellation Notice from Pay Pal", BLL.SecureMessaging.MessageClass.BregMaintenance);
        }

        public static void SendWelcomeEmail(ref ClassLibrary.DAL.PracticeApplication practiceApplication, System.Web.UI.Page page)
        {
            string controlPath = "~/Purchase/WelcomeEmail.ascx";
            WelcomeEmail welcomeEmail = page.LoadControl(controlPath) as WelcomeEmail;
            welcomeEmail.PracticeApplication = practiceApplication;
            welcomeEmail.LoadControlInfo();

            string mailBody = GetEmailText(welcomeEmail, page);

            FaxEmailPOs faxEmailPOs = new FaxEmailPOs();
            faxEmailPOs.SendEmail("OrderConfirmation", practiceApplication.EmailAddress, mailBody, "Welcome to Vision Express", BLL.SecureMessaging.MessageClass.Orders);
            practiceApplication.IsWelcomeEmailSent = true;
            DAL.Practice.PracticeApplication.UpdatePracticeApplication(practiceApplication);
        }

        public static void RequestContactEmail(string mailTo, string mailBody, string mailSubject)
        {
            FaxEmailPOs faxEmailPOs = new FaxEmailPOs();
            faxEmailPOs.SendEmail(mailTo, mailBody, mailSubject, BLL.SecureMessaging.MessageClass.BregMaintenance);
        }

        private static string GetEmailText(Control email, System.Web.UI.Page page)
        {
            System.Web.UI.WebControls.Panel ctlAll = new System.Web.UI.WebControls.Panel();

            ctlAll.Controls.Add(email);

            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htmlTW = new HtmlTextWriter(sw);
            ctlAll.RenderControl(htmlTW);

            return sb.ToString();

        }

    }
}
