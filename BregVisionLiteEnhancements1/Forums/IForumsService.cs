﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ClassLibrary.BLL.Forums;

namespace BregVision.Forums
{
    [ServiceContract]
    public interface IForumsService
    {
        [OperationContract]
        ForumGroupsResponse GetForumGroups(bool includeArticles, int depth);

        [OperationContract]
        ForumArticlesResponse GetForumArticlesByForumGroup(int groupId, int articleDepth, bool isActive);

        [OperationContract]
        ForumArticlesResponse GetForumArticlesByUserId(int userId, int articleDepth, bool isActive);

        [OperationContract]
        ForumArticlesResponse GetForumArticlesBySearchText(string searchText, int articleDepth, bool isActive);
        

        [OperationContract]
        ForumArticlesResponse GetRecentArticles(int count, int depth, bool isActive);

        [OperationContract]
        ForumArticlesResponse SearchArticles(string searchText, int count, int articleDepth, bool isActive);
       
        [OperationContract]
        ForumArticlesResponse GetRecentArticlesByGroupId(int groupId, int count, int depth, bool isActive);

        [OperationContract]
        ForumArticleResponse GetForumArticleTreeByTopArticle(int id, int depth, bool isActiveOnly);

        [OperationContract]
        UserPublicInfoResponse UpdateUserPublicInfo(UserPublicInfo userPublicInfo);

        [OperationContract]
        PracticePublicInfoResponse UpdatePracticePublicInfo(PracticePublicInfo practicePublicInfo);

        [OperationContract]
        PracticePublicInfoResponse GetPracticePublicInfo(int userId);

        [OperationContract]
        UserPublicInfoResponse GetUserPublicInfo(int userId);

        [OperationContract]
        ForumArticleResponse InsertForumArticle(ClassLibrary.BLL.Forums.ForumArticle forumArticle);

        [OperationContract]
        ForumArticleResponse UpdateForumArticle(ClassLibrary.BLL.Forums.ForumArticle forumArticle);

        [OperationContract]
        void InsertForumArticleImpression(int userId, int forumArticleId);

        [OperationContract]
        bool SetForumArticleIsActive(int articleId, bool isActive);

         [OperationContract]
        bool DeleteForumArticle(int articleId);
    }
}
