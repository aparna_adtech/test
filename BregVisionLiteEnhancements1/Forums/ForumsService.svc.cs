﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ClassLibrary.BLL.Forums;

namespace BregVision.Forums
{
    
    public class ForumsService : IForumsService
    {
        public ForumGroupsResponse GetForumGroups(bool includeArticles, int articleDepth)
        {
            ForumGroupsResponse ret = new ForumGroupsResponse();

            try
            {
                ret = ForumGroup.GetForumGroups(includeArticles, articleDepth);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public ForumArticlesResponse GetForumArticlesByForumGroup(int groupId, int articleDepth, bool isActive)
        {
            ForumArticlesResponse ret = new ForumArticlesResponse();

            try
            {
                ret = ClassLibrary.BLL.Forums.ForumArticle.GetForumArticlesByForumGroup(groupId, articleDepth, isActive);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public ForumArticlesResponse GetForumArticlesByUserId(int userId, int articleDepth, bool isActive)
        {
            ForumArticlesResponse ret = new ForumArticlesResponse();

            try
            {
                ret = ClassLibrary.BLL.Forums.ForumArticle.GetForumArticlesByUserId(userId, articleDepth, isActive);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public ForumArticlesResponse GetForumArticlesBySearchText(string searchText, int articleDepth, bool isActive)
        {
            ForumArticlesResponse ret = new ForumArticlesResponse();

            try
            {
                ret = ClassLibrary.BLL.Forums.ForumArticle.GetForumArticlesBySearchText(searchText, articleDepth, isActive);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public ForumArticlesResponse GetRecentArticles(int count, int articleDepth, bool isActive)
        {
            ForumArticlesResponse ret = new ForumArticlesResponse();

            try
            {
                ret = ClassLibrary.BLL.Forums.ForumArticle.GetRecentArticles(count, articleDepth, isActive);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }
        
        public bool SetForumArticleIsActive(int articleId, bool isActive)
        {
            bool ret = false;

            try
            {
                ret = ClassLibrary.BLL.Forums.ForumArticle.SetForumArticleIsActive(articleId, isActive);
            }
            catch (Exception ex)
            {
                ret= false;
            }

            return ret;
        }

        public bool DeleteForumArticle(int articleId)
        {
            bool ret = false;

            try
            {
                ret = ClassLibrary.BLL.Forums.ForumArticle.DeleteForumArticle(articleId);
            }
            catch (Exception ex)
            {
                ret = false;
            }

            return ret;
        }

        public ForumArticlesResponse SearchArticles(string searchText, int count, int articleDepth, bool isActive)
        {
            ForumArticlesResponse ret = new ForumArticlesResponse();

            try
            {
                ret = ClassLibrary.BLL.Forums.ForumArticle.SearchArticles(searchText,count, articleDepth, isActive);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public ForumArticlesResponse GetRecentArticlesByGroupId(int groupId, int count, int articleDepth, bool isActive)
        {
            ForumArticlesResponse ret = new ForumArticlesResponse();

            try
            {
                ret = ClassLibrary.BLL.Forums.ForumArticle.GetRecentArticlesByGroupId(groupId, count, articleDepth, isActive);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public ForumArticleResponse GetForumArticleTreeByTopArticle(int id, int depth, bool isActiveOnly)
        {
            ForumArticleResponse ret = new ForumArticleResponse();

            try
            {
                ret = ClassLibrary.BLL.Forums.ForumArticle.GetForumArticleTreeByTopArticle(id, depth, isActiveOnly);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }


        public ForumArticleResponse InsertForumArticle(ClassLibrary.BLL.Forums.ForumArticle forumArticle)
        {
            ForumArticleResponse ret = new ForumArticleResponse();

            try
            {
                ret = ClassLibrary.BLL.Forums.ForumArticle.Insert(forumArticle);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public ForumArticleResponse UpdateForumArticle(ClassLibrary.BLL.Forums.ForumArticle forumArticle)
        {
            ForumArticleResponse ret = new ForumArticleResponse();

            try
            {
                ret = ClassLibrary.BLL.Forums.ForumArticle.Update(forumArticle);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public UserPublicInfoResponse UpdateUserPublicInfo(UserPublicInfo userPublicInfo)
        {
            UserPublicInfoResponse ret = new UserPublicInfoResponse();

            try
            {
                ret = ClassLibrary.BLL.Forums.UserPublicInfo.UpdateUserPublicInfo(userPublicInfo);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public UserPublicInfoResponse GetUserPublicInfo(int userId)
        {
            UserPublicInfoResponse ret = new UserPublicInfoResponse();

            try
            {
                ret = ClassLibrary.BLL.Forums.UserPublicInfo.GetUserPublicInfo(userId);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public PracticePublicInfoResponse UpdatePracticePublicInfo(PracticePublicInfo practicePublicInfo)
        {
            PracticePublicInfoResponse ret = new PracticePublicInfoResponse();

            try
            {
                ret = ClassLibrary.BLL.Forums.PracticePublicInfo.UpdatePracticePublicInfo(practicePublicInfo);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public PracticePublicInfoResponse GetPracticePublicInfo(int userId)
        {
            PracticePublicInfoResponse ret = new PracticePublicInfoResponse();

            try
            {
                ret = ClassLibrary.BLL.Forums.PracticePublicInfo.GetPracticePublicInfo(userId);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public void InsertForumArticleImpression(int userId, int forumArticleId)
        {
            ClassLibrary.BLL.Forums.ForumArticle.InsertForumArticleImpression(userId, forumArticleId);
        }
    }
}
