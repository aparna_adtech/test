﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/SiteMaster.Master" AutoEventWireup="true" 
    CodeBehind="InvoiceManager.aspx.cs" Inherits="BregVision.InvoiceManager" %>

<%@ MasterType VirtualPath="~/MasterPages/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<div class="flex-row subWelcome-bar"><h1>Invoice Manager</h1></div>
<div class="subTabContent-container">
    	<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
        <link href="Styles/InvoiceManager.css" rel="stylesheet" />
    	<!-- <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script> -->
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
    
    <script src="InvoiceManager/invoiceApp.js"></script>
    <script src="InvoiceManager/Invoice/invoice.controller.js"></script>
    <script src="InvoiceManager/Utilities/supplier.filter.js"></script>
    <script src="InvoiceManager/Invoice/dateRange.filter.js"></script>
    <script src="InvoiceManager/Invoice/exported.filter.js"></script>
    <script src="InvoiceManager/PurchaseOrder/productName.filter.js"></script>
    <script src="InvoiceManager/PurchaseOrder/productCode.filter.js"></script>
    <script src="InvoiceManager/InvoiceExport/invoiceExport.filter.js"></script>    
    <script src="InvoiceManager/Utilities/datePicker.directive.js"></script>
    <script src="InvoiceManager/Utilities/currencyFormatter.directive.js"></script>
    <script src="InvoiceManager/Utilities/yesNo.filter.js"></script>
    <script src="InvoiceManager/Invoice/invoice.factory.js"></script>
    <script src="InvoiceManager/InvoiceExport/invoiceExport.controller.js"></script>
    <script src="InvoiceManager/InvoiceExport/invoiceExport.factory.js"></script>


    <script src="Include/jquery-ui/ui/jquery.ui.datepicker.js"></script>
    <script type="text/javascript">
        window.IMGR_show_action_search_invoice = function() { $("#imgr-invoice-action-create").hide(); $("#imgr-invoice-action-search").show(); };
        window.IMGR_show_action_create_invoice = function() { $("#imgr-invoice-action-search").hide(); $("#imgr-invoice-action-create").show(); };
        window.IMGR_show_current_invoice_overlay = function() { $("#imgr-current-invoice .imgr-overlay").show(); $("#imgr-current-invoice .imgr-overlay .imgr-overlay-text").show(); };
        window.IMGR_show_current_invoice_overlay_noprompt = function() { $("#imgr-current-invoice .imgr-overlay").show(); $("#imgr-current-invoice .imgr-overlay .imgr-overlay-text").hide(); };
        window.IMGR_hide_current_invoice_overlay = function() { $("#imgr-current-invoice .imgr-overlay").hide(); };
        window.IMGR_show_current_po_overlay = function() { $("#imgr-current-po .imgr-overlay").show(); $("#imgr-current-po .imgr-overlay .imgr-overlay-text").show(); };
        window.IMGR_show_current_po_overlay_noprompt = function() { window.IMGR_show_current_po_overlay(); $("#imgr-current-po .imgr-overlay .imgr-overlay-text").hide(); };
        window.IMGR_hide_current_po_overlay = function() { $("#imgr-current-po .imgr-overlay").hide(); };
        window.IMGR_show_invoice_search_results = function() {
            var div = $("#imgr-invoice-action-searchresults");
            var divWidth = div[0].style.width;
            if (divWidth !== '700px') {
                div.css('width','0').show().animate({width:'700px'},200,function(){});
            }
        };
        window.IMGR_hide_invoice_search_results = function() {
            var div = $("#imgr-invoice-action-searchresults");
            var divWidth = div[0].style.width;
            if (divWidth !== '0px') {
                div.css('width', divWidth).animate({ width: '0' }, 200, function() {});
            }
        };
        window.IMGR_show_po_search_results = function() {
            var div = $("#imgr-po-action-searchresults");
            var divWidth = div[0].style.width;
            if (divWidth !== '700px') {
                div.css('width','0').show().animate({width:'700px'},200,function(){});
            }
        };
        window.IMGR_hide_po_search_results = function() { $("#imgr-po-action-searchresults").css('width','700px').animate({width:'0'},200,function(){}); };
		
        window.IMGR_button_searchinvoice_enable = function() { var o = $("#imgr-invoice-action-search .imgr-confirm-button"); o.removeClass("imgr-inactive-button"); o.addClass("imgr-searchcolor"); };
        window.IMGR_button_searchinvoice_disable = function() { var o = $("#imgr-invoice-action-search .imgr-confirm-button"); o.removeClass("imgr-searchcolor"); o.addClass("imgr-inactive-button"); };
		
        window.IMGR_button_searchpo_enable = function() { var o = $("#imgr-po-search .imgr-confirm-button"); o.removeClass("imgr-inactive-button"); o.addClass("imgr-searchcolor"); };
        window.IMGR_button_searchpo_disable = function() { var o = $("#imgr-po-search .imgr-confirm-button"); o.removeClass("imgr-searchcolor"); o.addClass("imgr-inactive-button"); };
		
        window.IMGR_button_deleteinvoice_enable = function() { var o = $("#imgr-current-invoice .imgr-destructive-button"); o.removeClass("imgr-inactive-button"); o.addClass("imgr-deletecolor"); };
        window.IMGR_button_deleteinvoice_disable = function() { var o = $("#imgr-current-invoice .imgr-destructive-button"); o.addClass("imgr-inactive-button"); o.removeClass("imgr-deletecolor"); };
        window.IMGR_button_saveinvoice_enable = function() { var o = $("#imgr-current-invoice .imgr-confirm-button"); o.removeClass("imgr-inactive-button"); o.addClass("imgr-savecolor"); };
        window.IMGR_button_saveinvoice_disable = function() { var o = $("#imgr-current-invoice .imgr-confirm-button"); o.addClass("imgr-inactive-button"); o.removeClass("imgr-savecolor"); };
		
        window.IMGR_button_addtoinvoice_enable = function() { var o = $("#imgr-current-po .imgr-confirm-button"); o.removeClass("imgr-inactive-button"); o.addClass("imgr-addcolor"); };
        window.IMGR_button_addtoinvoice_disable = function() { var o = $("#imgr-current-po .imgr-confirm-button"); o.addClass("imgr-inactive-button"); o.removeClass("imgr-addcolor"); };
		

        var waitForFinalEvent = (function () {
            var timers = {};
            return function (callback, ms, uniqueId) {
                if (!uniqueId) {
                    uniqueId = "Don't call this twice without a uniqueId";
                }
                if (timers[uniqueId]) {
                    clearTimeout (timers[uniqueId]);
                }
                timers[uniqueId] = setTimeout(callback, ms);
            };
        })();


        window.IMGR_resize = function() {
            window.resizeWindow();

            var t1 = $("#imgr-invoice-items").position().top;
            var t2 = $("#imgr-current-invoice .imgr-invoice-actionrow").first().position().top;
            $("#imgr-invoice-items").css("height", Math.abs(t2 - t1));

            var t3 = $("#imgr-po-items").position().top;
            var t4 = $("#imgr-current-po .imgr-po-actionrow").first().position().top;
            $("#imgr-po-items").css("height", Math.abs(t4 - t3));

            var t5 = $("#imgr-export-items").position().top;
            var t6 = $("#imgr-export-invoice .imgr-export-actionrow").first().position().top;
            $("#imgr-export-items").css("height", Math.abs(t6 - t5));
        };

        $(function() {
            $("#aspnetForm").attr("novalidate","");

            $(window).unbind("resize");
            $(window).resize(function () {
                waitForFinalEvent(function(){
                    window.IMGR_resize();
                }, 500, "some unique string");
            });
            window.IMGR_resize();

            $("#imgr-action-create-title").mouseover(function() { $("#imgr-action-create-title img").attr('src', 'images/Add_Inv_Hover.png'); }).mouseout(function() { $("#imgr-action-create-title img").attr('src', 'images/Add_Inv_Default.png'); }).click(function() { window.IMGR_show_action_create_invoice(); });
            $("#imgr-action-search-title").mouseover(function() { $("#imgr-action-search-title img").attr('src', 'images/Search_Inv_Hover.png'); }).mouseout(function() { $("#imgr-action-search-title img").attr('src', 'images/Search_Inv_Default.png'); }).click(function() { window.IMGR_show_action_search_invoice(); });
			
            $("#imgr-action-create-title").click(function() { window.IMGR_hide_invoice_search_results(); });
            $("#imgr-invoice-searchresults-title .imgr-action-expandcollapse").click(function() { window.IMGR_hide_invoice_search_results(); });
            $("#imgr-po-searchresults-title .imgr-action-expandcollapse").click(function() { window.IMGR_hide_po_search_results(); });
			
            $(".imgr-invoice-search").click(function() { window.IMGR_show_invoice_search_results(); });
            $(".imgr-purchase-order-search").click(function() { window.IMGR_show_po_search_results(); });
			
            $(".imgr-search-dateinput input").datepicker({dateFormat:'yy-mm-dd'});
        });
    </script>
	<div id="imgr-container" data-ng-app="invoiceApp" data-ng-controller="invoiceController as invCtrl">
        <div id="imgr-editor-container">
            
            <div id="imgr-export-invoice" data-ng-controller="exportController as vm" data-ng-hide="!invCtrl.showExportInvoices" >
                <div id="imgr-export-title">Export Invoices</div>
                <div class="imgr-export-headerrow">
                    <div class="imgr-export-header-item0">All<br/><input type="checkbox" data-ng-model="vm.SelectAll" data-ng-click="vm.toggleSelectAll(invCtrl)" /></div>
                    <div class="imgr-export-header-item1">Invoice<br/>Number</div>
                    <div class="imgr-export-header-item2">Supplier</div>
                    <div class="imgr-export-header-item3">Total</div>
                    <div class="imgr-export-header-item4">Invoice<br/>Date</div>
                    <div class="imgr-export-header-item5">Is<br/>Paid</div>
                    <div class="imgr-export-header-item6">Tax</div>
                    <div class="imgr-export-header-item7">Shipping</div>
                    <div class="imgr-export-header-item8"></div>
                </div>
                <div id="imgr-export-items">
                    <div class="imgr-export-itemrow" 
                            data-ng-repeat="inv in invCtrl.Invoices 
                                            | exportInvoicesFilter : invCtrl
                                            | limitTo: 10"
                            data-ng-class="{exported:inv.IsExported}" data-ng-class-even="'imgr-results-row-even'" data-ng-class-odd="'imgr-results-row-odd'">
                        <div class="imgr-export-row-item0"><input type="checkbox" data-ng-model="inv.IsSelected" /></div>
                        <div class="imgr-export-row-item1" data-ng-click="vm.selectInvoice(inv)">{{inv.Number}}</div>
                        <div class="imgr-export-row-item2" data-ng-click="vm.selectInvoice(inv)">{{inv.Supplier.Name}}</div>
                        <div class="imgr-export-row-item3" data-ng-click="vm.selectInvoice(inv)">{{inv.Total | currency}}</div>
                        <div class="imgr-export-row-item4" data-ng-click="vm.selectInvoice(inv)">{{inv.DateAsJSON | date: 'MM/dd/yyyy'}}</div>
                        <div class="imgr-export-row-item5" data-ng-click="vm.selectInvoice(inv)">{{inv.IsPaid | yesNo}}</div>
                        <div class="imgr-export-row-item6" data-ng-click="vm.selectInvoice(inv)">{{inv.Tax | currency}}</div>
                        <div class="imgr-export-row-item7" data-ng-click="vm.selectInvoice(inv)">{{inv.ShippingCharges | currency}}</div>
                        <div class="imgr-export-row-item8"><button type="button" data-ng-click="invCtrl.SelectInvoice(inv);invCtrl.showExportInvoices=false">View Invoice</button></div>
                    </div>
                </div>
                <div class="imgr-export-actionrow">
                    <div class="imgr-export-legend"><div class="exported imgr-export-legend-coloritem"></div> - exported</div>
                    <button type="button" class="imgr-add-to-invoice-button" data-ng-click="vm.exportSelectedInvoices(invCtrl)" data-ng-disabled="!vm.isExportEnabled(invCtrl)">Export Invoices</button>
                </div>
            </div>
	    						
			    <div id="imgr-current-invoice" data-ng-hide="invCtrl.showExportInvoices">
			        <ng-form name="activeInvoice" >
				        <div id="imgr-invoice-title">Current Invoice</div>
				        <div class="imgr-invoice-details">
					        <div class="imgr-invoice-invoiceno">Invoice No<br/><input type="text" data-ng-model="invCtrl.CurrentInvoice.Number" required="true" /></div>
					        <div class="imgr-invoice-suppliername">Supplier Name<br/><input type="text" readonly data-ng-model="invCtrl.CurrentInvoice.Supplier.Name" /></div>
					        <div class="imgr-invoice-invoicedate">Invoice Date<br/><input type="text" 
                                datepicker1="invCtrl.CurrentInvoice.DateString"  
                                data-ng-model ="invCtrl.CurrentInvoice.DateString"/></div>
				        </div>
				        <div class="imgr-invoice-headerrow">
					        <div class="imgr-invoice-header-item1">Product</div>
					        <div class="imgr-invoice-header-item2">Code</div>
					        <div class="imgr-invoice-header-item3">P.O. No</div>
					        <div class="imgr-invoice-header-item4">Qty</div>
					        <div class="imgr-invoice-header-item5">Unit Cost</div>
					        <div class="imgr-invoice-header-item6">Total Cost</div>
					        <div class="imgr-invoice-header-item7">&nbsp;</div>
				        </div>
				        <div id="imgr-invoice-items">
					        <div data-ng-repeat="invoiceLineItem in invCtrl.CurrentInvoice.InvoiceLineItems" class="imgr-invoice-itemrow" data-ng-class-even="'imgr-results-row-even'" data-ng-class-odd="'imgr-results-row-odd'">
						        <div class="imgr-invoice-row-item1">{{invoiceLineItem.ProductName}}</div>
						        <div class="imgr-invoice-row-item2">{{invoiceLineItem.ProductCode}}</div>
						        <div class="imgr-invoice-row-item3">{{invoiceLineItem.PurchaseOrderNumber}}</div>
						        <div class="imgr-invoice-row-item4">
							        <ng-form name="quantityForm"  >
							            <div class="imgr-warn" data-ng-show="quantityForm.invQty.$invalid">Maximum: {{invoiceLineItem.QuantityInvoicedMax}}</div>
								        <input name="invQty"  type="number" data-ng-model="invoiceLineItem.QuantityInvoiced" min="0" max="{{invoiceLineItem.QuantityInvoicedMax}}" />						    
							        </ng-form>
						        </div>
						        <div class="imgr-invoice-row-item5">
							        <input type="text" data-ng-model="invoiceLineItem.WholesaleCost" currency-formatter currency-value='invoiceLineItem.WholesaleCost'/>
							        <a href="#" data-ng-click="invCtrl.initiateWholesaleCostUpdate(invoiceLineItem)" >
							            <img src="images/Set_MasterCat_price_Default.png" alt="[Update practice catalog wholesale cost]"/>
							        </a>
						        </div>
						        <div class="imgr-invoice-row-item6">{{invoiceLineItem.QuantityInvoiced * invoiceLineItem.WholesaleCost | currency}}</div>
						        <div class="imgr-invoice-row-item7">
							        <a href="#" data-ng-click="invCtrl.initiateInvoiceItemDeletion(invoiceLineItem)">
							            <img src="images/LineItem_INV_Del_Default.png" alt="[Delete invoice lineitem]"/>
							        </a>
						        </div>
					        </div>
					    <div class="imgr-invoice-totalrow">
							    <div class="imgr-invoice-row-label">Extended Price:</div>
							    <div class="imgr-invoice-row-value-readonly"><input type="text" readonly value="{{invCtrl.getCurrentInvoiceExtendedPrice() | currency}}"/></div>
						    </div>
						    <div class="imgr-invoice-totalrow">
							    <div class="imgr-invoice-row-label">Shipping Charges:</div>
							    <div class="imgr-invoice-row-value">
							        <input style="height:19px" type="text" data-ng-model="invCtrl.CurrentInvoice.ShippingCharges" currency-formatter currency-value='invCtrl.CurrentInvoice.ShippingCharges'  />
							    </div>
						    </div>
						    <div class="imgr-invoice-totalrow">
							    <div class="imgr-invoice-row-label">Sales Tax:</div>
							    <div class="imgr-invoice-row-value">
				                    <input style="height:19px" data-ng-model="invCtrl.CurrentInvoice.Tax" currency-formatter currency-value='invCtrl.CurrentInvoice.Tax'/>
							    </div>
						    </div>
						    <div class="imgr-invoice-totalrow">
							    <div class="imgr-invoice-row-label">Total Amount:</div>
							    <div class="imgr-invoice-row-value-readonly"><input type="text" readonly value="{{invCtrl.getCurrentInvoiceTotal() | currency}}"/></div>
						    </div>
						    <div class="imgr-invoice-totalrow">
							    <div class="imgr-invoice-row-label">Paid:</div>
							    <div class="imgr-invoice-row-value"><input type="checkbox" data-ng-model="invCtrl.CurrentInvoice.IsPaid" /></div>
						    </div>
					    </div>
					    <div class="imgr-invoice-actionrow">
                            <button type="button" class="imgr-destructive-button imgr-inactive-button" data-ng-click="invCtrl.initiateInvoiceDeletion()">Delete Invoice</button>
					        <button type="button" class="imgr-save-invoice-button" data-ng-click="invCtrl.updateInvoice()" data-ng-disabled="activeInvoice.$invalid || !activeInvoice.$dirty">
					            Save Invoice
					        </button>
				        </div>
				        <div class="imgr-overlay" data-ng-show="!invCtrl.CurrentInvoice || invCtrl.CurrentInvoice === null"><div class="imgr-overlay-text">Select or create an invoice...</div></div>
				        <div class="imgr-modal-overlay" 
                            data-ng-show="invCtrl.showInvoiceDeletionConfirm 
                            || invCtrl.showInvoiceItemDeletionConfirm 
                            || invCtrl.showWholesaleCostUpdateConfirm 
                            || invCtrl.showLoseInvoiceChangesConfirm  
                            || invCtrl.showInvoiceDuplicateConfirm">
				        
				        </div>
				        <div class="imgr-modal" id="imgr-invoice-dialog-deleteinvoice" data-ng-show="invCtrl.showInvoiceDeletionConfirm">
					        <div class="imgr-dialog-prompt">Delete this invoice?</div>
					        <div class="imgr-dialog-buttons">
						        <button type="button" class="imgr-safe-button" data-ng-click="invCtrl.hideInvoiceDeletionConfirm()"><span class="imgr-button-yesno">No!</span> Go Back</button>
						        <button type="button" class="imgr-destructive-button" ng-click="invCtrl.deleteInvoice()" ><span class="imgr-button-yesno">Yes!</span> Delete</button>
					        </div>
				        </div>
				        <div class="imgr-modal"  id="imgr-invoice-dialog-removeitem" data-ng-show="invCtrl.showInvoiceItemDeletionConfirm">
					        <div class="imgr-dialog-prompt">Remove this line item?</div>
					        <div class="imgr-dialog-buttons">
						        <button type="button" class="imgr-safe-button" data-ng-click="invCtrl.hideInvoiceItemDeletionConfirm()"><span class="imgr-button-yesno">No!</span> Go Back</button>
						        <button type="button" class="imgr-destructive-button" data-ng-click="invCtrl.invoiceItemDeletionConfirmed()"><span class="imgr-button-yesno">Yes!</span> Remove</button>
					        </div>
				        </div>
				        <div class="imgr-modal"  id="imgr-invoice-dialog-duplicat" data-ng-show="invCtrl.showInvoiceDuplicateConfirm">
					        <div class="imgr-dialog-prompt">Do you want to continue in creating a duplicate invoice?</div>
					        <div class="imgr-dialog-buttons">
						        <button type="button" class="imgr-safe-button" data-ng-click="invCtrl.hideInvoiceDuplicateConfirm()"><span class="imgr-button-yesno">No!</span> Go Back</button>
						        <button type="button" class="imgr-destructive-button" data-ng-click="invCtrl.createNewInvoiceContinue()"><span class="imgr-button-yesno">Yes!</span></button>
					        </div>
				        </div>
				        <div class="imgr-modal"  id="imgr-invoice-dialog-updatepracticecatalog" data-ng-show="invCtrl.showWholesaleCostUpdateConfirm">
					        <div class="imgr-dialog-prompt">Change the practice catalog to current<br/>wholesale cost value?</div>
					        <div class="imgr-dialog-buttons">
						        <button type="button" class="imgr-safe-button" data-ng-click="invCtrl.hideWholesaleCostUpdateConfirm()"><span class="imgr-button-yesno">No!</span> Go Back</button>
						        <button type="button" class="imgr-destructive-button" data-ng-click="invCtrl.wholesaleCostUpdateConfirmed()"><span class="imgr-button-yesno">Yes!</span> Update</button>
					        </div>
				        </div>
				        <div class="imgr-modal"  id="imgr-invoice-dialog-loseChanges" data-ng-show="invCtrl.showLoseInvoiceChangesConfirm">
					        <div class="imgr-dialog-prompt">Lose changes to this invoice?</div>
					        <div class="imgr-dialog-buttons">
						        <button type="button" class="imgr-safe-button" data-ng-click="invCtrl.hideLoseInvoiceChangesPrompt()"><span class="imgr-button-yesno">No!</span> Go Back</button>
						        <button type="button" class="imgr-destructive-button" data-ng-click="invCtrl.loseInvoiceChangesConfirmed()"><span class="imgr-button-yesno">Yes!</span></button>
					        </div>
				        </div>
                    </ng-form>
			    </div>
			    <div id="imgr-current-po">
				    <div id="imgr-po-title">Current Purchase Order</div>
				    <div class="imgr-po-details">
					    <div>P.O. No:<br/><input type="text" value="{{invCtrl.CurrentPurchaseOrder.PONumber}}" readonly /></div>
				    </div>
				    <div class="imgr-po-headerrow">
				        <div class="imgr-po-header-item1">All<br/><input type="checkbox" data-ng-model="invCtrl.SelectAll" data-ng-click="invCtrl.toggleSelectAll()" /></div>
					    <div class="imgr-po-header-item2">Product</div>
					    <div class="imgr-po-header-item3">Code</div>
					    <div class="imgr-po-header-item4">Ord.<br/>Qty</div>
				        <div class="imgr-po-header-item5">Chk-In<br/>Qty</div>
					    <div class="imgr-po-header-item6">Invc'd<br/>Qty</div>
					    <div class="imgr-po-header-item7">Unit Cost</div>
					    <div class="imgr-po-header-item8">Invoiced Details<br/><span class="imgr-po-item8-subitem1">P.O.</span><span class="imgr-po-item8-subitem2">Qty</span></div>
				    </div>
				    <div id="imgr-po-items">
					    <div class="imgr-po-itemrow" 
                            data-ng-repeat="lineItem in invCtrl.CurrentPurchaseOrder.LineItems" data-ng-class-even="'imgr-results-row-even'" data-ng-class-odd="'imgr-results-row-odd'">
						    <div class="imgr-po-row-item1">
						        <input type="checkbox" data-ng-disabled="invCtrl.CurrentInvoiceContainsPoItem(lineItem)" data-ng-model="lineItem.IsSelected" />
						    </div>
						    <div class="imgr-po-row-item2" data-ng-click="invCtrl.SelectPoLineItem(lineItem)">{{lineItem.Name}}</div>
						    <div class="imgr-po-row-item3" data-ng-click="invCtrl.SelectPoLineItem(lineItem)">{{lineItem.ProductCode}}</div>
						    <div class="imgr-po-row-item4" data-ng-click="invCtrl.SelectPoLineItem(lineItem)">{{lineItem.QuantityOrdered}}</div>
						    <div class="imgr-po-row-item5" data-ng-click="invCtrl.SelectPoLineItem(lineItem)">{{lineItem.QuantityCheckedIn}}</div>
						    <div class="imgr-po-row-item6" data-ng-click="invCtrl.SelectPoLineItem(lineItem)">{{invCtrl.totalSupplierOrderLineItemInvoicedCount(lineItem)}}</div>
						    <div class="imgr-po-row-item7" data-ng-click="invCtrl.SelectPoLineItem(lineItem)">{{lineItem.Cost | currency}}</div>
						    <div class="imgr-po-row-item8" data-ng-click="invCtrl.SelectPoLineItem(lineItem)">
								<div data-ng-repeat="invoice in lineItem.Invoices">
									<span class="imgr-po-item8-subitem1">{{invoice.Number}}</span>
									<span class="imgr-po-item8-subitem2">{{invoice.Quantity}}</span>
								</div>
						    </div>
					    </div>
				    </div>
				    <div class="imgr-po-actionrow">
					    <button type="button" class="imgr-add-to-invoice-button" 
                            data-ng-click="invCtrl.addSelectedPoItemsToCurrentInvoice()"
                            data-ng-disabled ="!invCtrl.isPoItemAddEnabled()">Add to Invoice</button>
				    </div>
				    <div class="imgr-overlay" data-ng-show="!invCtrl.CurrentInvoice || invCtrl.CurrentInvoice === null"><div class="imgr-overlay-text">Select a purchase order...</div></div>
			    </div>
		    </div>
		    <div id="imgr-actions-container">
			    <div id="imgr-invoice-actions">
				    <div id="imgr-action-search-title"><div class="imgr-action-title "><img src="images/Search_Inv_Default.png" alt="Search Invoice"/></div></div>
				    <div id="imgr-action-create-title"><div class="imgr-action-title"><img src="images/Add_Inv_Default.png" alt="Create Invoice"/></div></div>
				    <div id="imgr-invoice-action-search">
					    <div class="imgr-invoice-action-select-decoration"><img src="images/Search_Select_Triangle_Dark_Blue.png"/></div>
					    <div class="imgr-search-parameter"><input id="inactiveSuppliers" type="checkbox" /><label for="inactiveSuppliers"> Include inactive suppliers</label></div>
					    <div class="imgr-search-parameter">
					        <input type="checkbox" id="includeExported" data-ng-model="invCtrl.includeExportedInvoices"/><label for="includeExported"> Include exported invoices</label>
					    </div>
					    <div class="imgr-search-parameter" title="With Smart Search enabled search criteria apply automatically.">
					        <input type="checkbox" id="invoiceSmartSearch"  data-ng-model="invCtrl.isInvoiceSmartSearchEnabled"/><label for="invoiceSmartSearch"> Enable Smart Search</label>
					    </div>
					    <div class="imgr-search-parameter">Supplier Name<br/>
                            <select data-ng-options= "supplier.Name for supplier in invCtrl.InvoiceSearchSuppliers track by supplier.Id" 
                                data-ng-model="invCtrl.SearchInvoice.SupplierDisplay"
                                data-ng-change="invCtrl.InvoiceSupplierChanged()">
                            </select>
					    </div>
					    <div class="imgr-search-parameter">
						    <div class="imgr-search-dateinputsmall"><label>From:</label> <input type="text" datepicker1="invCtrl.dateFromInvoices" data-ng-model="invCtrl.dateFromInvoices" /></div>
						    <div class="imgr-search-dateinputsmall"><label>To:</label>   <input type="text" datepicker1="invCtrl.dateToInvoices"   data-ng-model="invCtrl.dateToInvoices" /></div>
					    </div>
					    <div class="imgr-search-parameter">Invoice Number<br/><input type="text" data-ng-model="invCtrl.SearchInvoice.NumberDisplay" data-ng-change="invCtrl.InvoiceNumberChanged()"/></div>
					    <div class="imgr-search-action">
						    <button type="button" class="imgr-confirm-button imgr-inactive-button imgr-invoice-search" data-ng-click="invCtrl.SearchInvoices()">Search</button>
						    <button type="button" class="imgr-confirm-button imgr-inactive-button imgr-invoice-refresh" data-ng-click="invCtrl.RefreshInvoices()">Refresh</button>
					    </div>
                        <div class="imgr-search-parameter"><input type="checkbox" id="showExport" data-ng-model="invCtrl.showExportInvoices"/><label for="showExport"> Show Export Invoices View</label></div>
				    </div>
				    <div id="imgr-invoice-action-create">
					    <div class="imgr-invoice-action-select-decoration"><img src="images/Create_INV_Triangle_Dark_Green.png"/></div>
					    <ng-form name="createInvoiceForm" >
						    <div class="imgr-search-parameter">Invoice Number<br/><input type="text" data-ng-model="invCtrl.NewInvoice.Number" data-ng-required="true"/></div>
						    <div class="imgr-search-parameter">Invoice Date<br/><input type="text" datepicker1="invCtrl.NewInvoice.DateString" data-ng-model="invCtrl.NewInvoice.DateString"/></div>
						    <div class="imgr-search-parameter">Supplier Name<br/><select data-ng-options= "supplier.Name for supplier in invCtrl.InvoiceCreateSuppliers track by supplier.Id" data-ng-model="invCtrl.NewInvoice.Supplier" data-ng-required="true"></select></div>
						    <div class="imgr-search-action">
							    <button type="button" class="imgr-confirm-button imgr-inactive-button imgr-invoice-create" data-ng-click="invCtrl.createNewInvoiceClick()" data-ng-disabled="createInvoiceForm.$invalid">Create</button>
						    </div>
					    </ng-form>
				    </div>
				    <div id="imgr-invoice-action-searchresults">
					    <div class="imgr-searchresults-decoration"><img src="images/Search_Select_Triangle_Dark_Blue_Alt.png"/></div>
					    <div class="imgr-searchresults-content">
						    <div id="imgr-invoice-searchresults-title">Invoice Search Results<div class="imgr-action-expandcollapse">X</div></div>
						    <div class="imgr-invoice-searchresults-headerrow">
							    <div class="imgr-invoice-searchresults-header-item1">Invoice Number</div>
							    <div class="imgr-invoice-searchresults-header-item2">Supplier</div>
							    <div class="imgr-invoice-searchresults-header-item3">Total</div>
							    <div class="imgr-invoice-searchresults-header-item4">Invoice Date</div>
						    </div>
						    <div  class="imgr-invoice-searchresults-itemrow" data-ng-click="invCtrl.SelectInvoice(inv)" 
                                data-ng-repeat="inv in invCtrl.Invoices 
                                | filter: {Number: invCtrl.SearchInvoice.NumberSearch} 
                                | supplier: invCtrl.SearchInvoice.SupplierSearch 
                                | exported : invCtrl.includeExportedInvoices
                                | dateRange: invCtrl.dateFromInvoicesDate.getTime(): invCtrl.dateToInvoicesDate.getTime()
                                | limitTo: 10">
							    <div class="imgr-invoice-searchresults-row-item1" data-ng-class="{exported:inv.IsExported}" data-ng-class-even="'imgr-results-row-even'" data-ng-class-odd="'imgr-results-row-odd'">{{inv.Number}}</div>
							    <div class="imgr-invoice-searchresults-row-item2" data-ng-class="{exported:inv.IsExported}" data-ng-class-even="'imgr-results-row-even'" data-ng-class-odd="'imgr-results-row-odd'">{{inv.Supplier.Name}}</div>
							    <div class="imgr-invoice-searchresults-row-item3" data-ng-class="{exported:inv.IsExported}" data-ng-class-even="'imgr-results-row-even'" data-ng-class-odd="'imgr-results-row-odd'">{{inv.Total | currency}}&nbsp;</div>
							    <div class="imgr-invoice-searchresults-row-item4" data-ng-class="{exported:inv.IsExported}" data-ng-class-even="'imgr-results-row-even'" data-ng-class-odd="'imgr-results-row-odd'">{{inv.DateAsJSON | date: 'MM/dd/yyyy'}}</div>
						    </div>
					    </div>
				    </div>
			    </div>
			    <div id="imgr-po-actions">
				    <div id="imgr-action-po-title"><div class="imgr-action-title">Search P.O.</div></div>
				    <div id="imgr-po-search">
				        <ng-form name="poSearchForm">
    					    <div class="imgr-search-parameter" title="With Smart Search enabled search criteria apply automatically.">
    					        <input type="checkbox" id="poSmartSearch" data-ng-model="invCtrl.isPurchaseOrderSmartSearchEnabled" />
                                <label for="poSmartSearch"> Enable Smart Search</label>
    					    </div>
					        <div class="imgr-search-parameter">P.O. Number<br/><input type="text" data-ng-model="invCtrl.SearchPurchaseOrder.PONumberDisplay" data-ng-change="invCtrl.PONumberChanged()"/></div>
                            <div class="imgr-search-parameter">
                                <div class="imgr-search-dateinputsmall">From:<br/><input name="poFromDate" type="text" datepicker1="invCtrl.dateFromPOs" data-ng-model="invCtrl.dateFromPOs" data-ng-change="invCtrl.fromDatePosChanged()" data-ng-pattern='/^(0?[1-9]|1[012])\/(0?[1-9]|[12][0-9]|3[01])\/(199\d)|([2-9]\d{3})$/'/></div>
                                <div class="imgr-search-dateinputsmall">To:<br/><input type="text" datepicker1="invCtrl.dateToPOs" data-ng-model="invCtrl.dateToPOs"/></div>
                            </div>
					        <div class="imgr-search-parameter">Product Name<br/><input type="text" data-ng-model="invCtrl.SearchPurchaseOrder.ProductNameDisplay" data-ng-change="invCtrl.ProductNameChanged()"/></div>
					        <div class="imgr-search-parameter">Product Code<br/><input type="text" data-ng-model="invCtrl.SearchPurchaseOrder.ProductCodeDisplay" data-ng-change="invCtrl.ProductCodeChanged()"/></div>
					        <div class="imgr-search-action">
						        <button type="button" class="imgr-confirm-button imgr-inactive-button imgr-purchase-order-search" data-ng-click="invCtrl.SearchPOs()">Search</button>
    						    <button type="button" class="imgr-confirm-button imgr-inactive-button imgr-po-refresh" data-ng-click="invCtrl.RefreshPurchaseOrders()">Refresh</button>
					        </div>
                        </ng-form>
				    </div>
				    <div id="imgr-po-action-searchresults">
					    <div class="imgr-searchresults-decoration"><img src="images/Search_PO_Triangle_Dark_Purple.png"/></div>
					    <div class="imgr-searchresults-content">
						    <div id="imgr-po-searchresults-title">Purchase Order Search Results<div class="imgr-action-expandcollapse">X</div></div>
                            <div class="imgr-po-searchresults-detailsrow">
                                <div class="imgr-po-searchresults-detailsitem">Supplier Selected: &quot;{{invCtrl.CurrentInvoice.Supplier.Name == null ? "ALL" : invCtrl.CurrentInvoice.Supplier.Name}}&quot;</div>
                            </div>
						    <div class="imgr-po-searchresults-headerrow">
							    <div class="imgr-po-searchresults-header-item1">P.O. Number</div>
							    <div class="imgr-po-searchresults-header-item2">Supplier</div>
							    <div class="imgr-po-searchresults-header-item3">Order Date</div>
						    </div>
						    <div class="imgr-po-searchresults-itemrow" data-ng-click="invCtrl.SelectPurchaseOrder(po)" 
                                data-ng-repeat="po in invCtrl.PurchaseOrders 
                                | filter: {PONumber: invCtrl.SearchPurchaseOrder.PONumberSearch} 
                                | supplier: invCtrl.CurrentInvoice.Supplier 
                                | productName: invCtrl.SearchPurchaseOrder.ProductNameSearch 
                                | productCode: invCtrl.SearchPurchaseOrder.ProductCodeSearch 
                                | dateRange:invCtrl.dateFromPOsDate.getTime():invCtrl.dateToPOsDate.getTime() 
                                | limitTo: 10">
							    <div class="imgr-po-searchresults-row-item1" data-ng-class-even="'imgr-results-row-even'" data-ng-class-odd="'imgr-results-row-odd'">{{po.PONumber}}</div>
							    <div class="imgr-po-searchresults-row-item2" data-ng-class-even="'imgr-results-row-even'" data-ng-class-odd="'imgr-results-row-odd'">{{po.Supplier.Name}}</div>
							    <div class="imgr-po-searchresults-row-item3" data-ng-class-even="'imgr-results-row-even'" data-ng-class-odd="'imgr-results-row-odd'">{{po.DateAsJSON | date: 'MM/dd/yyyy'}}</div>
						    </div>
					    </div>
				    </div>
			    </div>
		    </div>
	</div>
	 <script type="text/javascript">
 	    var PRACTICE_ID = <%=PracticeId%>;
 	    var Token = '<%=Token%>';
 	</script>
</div>
</asp:Content>
