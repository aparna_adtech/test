﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="contact.aspx.cs" Inherits="BregVision.contact" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head runat="server">
    <title>Inventory Management Software for Orthopedic Practices | Breg Vision</title>
    <link rel="shortcut icon" href="http://www.breg.com/sites/default/files/breg_favicon.ico"
        type="image/x-icon" />
    <link rel="stylesheet" href="Styles/LandingPage/reset.css" />
    <link rel="stylesheet" href="Styles/LandingPage/text.css" />
    <link rel="stylesheet" href="Styles/LandingPage/style.css" />
    <link rel="stylesheet" href="Styles/LandingPage/960_16_col.css" />
    <!--[if IE]>
	<link rel="stylesheet" type="text/css" href="Styles/LandingPage/all-ie-only.css" />
<![endif]-->
    <!--[if IE 7]>
	<link rel="stylesheet" type="text/css" href="Styles/LandingPage/ie7.css" />
<![endif]-->
    <script src="Include/LandingPage/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="Include/LandingPage/jquery.validate.min.js" type="text/javascript"></script>
    <script src="Include/LandingPage/functions.js" type="text/javascript"></script>
</head>
<body class="secondary">
    <div class="wrapper" style="margin-top: 56px;">
        <div class="container_16" style="padding-bottom: 20px" id="topmenu">
            <div id="logo" class="grid_5">
                <a href="Default.aspx" title="Breg Vision Orthopedic Inventory Management">
                    <img src="Images/LandingPage/breg-vision-logo.png" alt="Breg Logo" /></a></div>
            <div id="main-menu" class="grid_11">
                <ul class="main-nav">
                    <li><a href="http://www.breg.com/?source=vision">Breg Home</a></li>
                    <li><a href="http://www.breg.com/administrators/breg-orthoselect/?source=vision">OrthoSelect</a></li>
                    <li><a href="downloads/VisionROI&PricingCalc5_28_14.xlsx">ROI Calculator</a></li>
                    <li><a href="http://www.breg.com/products/?source=vision">Breg Products</a></li>
                </ul>
            </div>
            <div class="clear">
            </div>
        </div>
        <div class="intro-wrapper">
            <!--Intro paragraph-->
            <asp:Panel ID="ContactPanel" runat="server" Visible="true">
                <div class="intro" style="width: 960px; padding-left: 50px">
                    <h1>
                        Get Started with Vision</h1>
                    <p>
                        Please fill out the form to get started and we will contact you soon.</p>
                    <form id="contactForm" class="box login" runat="server">
                    <table border="0" cellpadding="5" cellspacing="0">
                        <tr>
                            <td>
                                <label for="practicename">
                                    Practice Name</label>
                                <asp:TextBox ID="practicename" MaxLength="100" runat="server" TabIndex="1"></asp:TextBox>
                            </td>
                            <td class="seprow">
                                &nbsp;
                            </td>
                            <td>
                                <label for="physiciannum">
                                    Number of Physicians</label>
                                <asp:TextBox ID="physiciannum" MaxLength="5" runat="server" TabIndex="2"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="firstname">
                                    First Name*</label>
                                <asp:TextBox ID="firstname" MaxLength="100" runat="server" TabIndex="3"></asp:TextBox>
                            </td>
                            <td class="seprow">
                                &nbsp;
                            </td>
                            <td>
                                <label for="lastname">
                                    Last Name*</label>
                                <asp:TextBox ID="lastname" MaxLength="100" runat="server" TabIndex="4"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="phone">
                                    Phone Number</label>
                                <asp:TextBox ID="phone" MaxLength="50" runat="server" TabIndex="5"></asp:TextBox>
                            </td>
                            <td class="seprow">
                                &nbsp;
                            </td>
                            <td>
                                <label for="email">
                                    Email*</label>
                                <asp:TextBox ID="email" MaxLength="100" runat="server" TabIndex="6"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <footer><br /><br />
                                <asp:Button ID="contactformsubmit" runat="server" CssClass="btnLogin" Text="Submit" OnClick="contactformsubmit_Click" TabIndex="7"></asp:Button>
                            </footer>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <br />
                                <br />
                                *Required field
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </asp:Panel>
            <asp:Panel ID="ThankYouPanel" runat="server" Visible="false">
                <div class="intro" style="width: 960px; padding-left: 50px">
                    <h1>
                        Thank You for Your Vision Inquiry</h1>
                    <p>
                        We will be in contact with you soon.</p>
                </div>
            </asp:Panel>
            <div class="clear" style="padding-bottom: 30px">
            </div>
        </div>
        <div class="clear">
        </div>
        <div class="push">
        </div>
        <br />
        <br />
    </div>
    <div id="footer-wrapper" class="footerpull">
        <div id="footer">
            <div id="footer-inner">
                <h3>
                    Breg Vision Customer Care Contact Info:</h3>
                <p>
                    888-290-8481</p>
                <p>
                    <a class="contactform" href="contact.aspx">Email Us</a></p>
                <div style="padding-top: 10px;">
                    <img name="social" src="Images/LandingPage/social.png" width="74" height="20" border="0"
                        id="social" usemap="#m_social" alt="" />
                </div>
            </div>
            <div id="footer-links">
                <p>
                    <a href="http://www.breg.com/privacy-policy">Privacy Policy</a> | <a href="http://www.breg.com/terms-of-use">
                        Terms of Use</a> | <a href="/sitemap">Sitemap</a> | <a class="contactform" href="contact.aspx">
                            Contact Us</a> | <a href="http://www.breg.com/medical-professionals/instructions-use-ifus">
                                Instructions for Use</a></p>
                <p>
                    &copy;
                    <script language="javascript" type="text/javascript">
                        var today = new Date()
                        var year = today.getFullYear()
                        document.write(year)
                    </script>
                    Breg, Inc. All rights reserved. All trademarks and registered trademarks are owned
                    by the copyright holder.</p>
            </div>
            <div id="footer-breg">
                <a href="http://www.breg.com">
                    <img alt="Breg" src="Images/LandingPage/breg-logo.png"></a>
            </div>
        </div>
    </div>
    <map name="m_social" id="m_social">
        <area shape="rect" coords="53,0,74,20" href="http://www.linkedin.com/company/breg"
            alt="" />
        <area shape="rect" coords="26,0,48,20" href="http://www.youtube.com/breginc" alt="" />
        <area shape="rect" coords="0,0,21,20" href="http://www.facebook.com/breginc" alt="" />
    </map>
</body>
</html>
