using System;
using BregVision.Admin.PracticeSetup;
using System.Web.UI.WebControls;

public partial class UserControls_ResetSaveButtonTable : System.Web.UI.UserControl
{

	public event EventHandler Reset;
	public event EventHandler Save;

    public Button btnSaveButton
	{
		get { return btnSave; }
	}

    public Button btnResetButton
	{
		get { return btnReset; }
	}

	protected void Page_Load( object sender, EventArgs e )
	{
        if (this.Page is PracticeSetup)
        {
            ((PracticeSetup)Page).AddAjaxSettingWizardUpdate(btnReset);
            ((PracticeSetup)Page).AddAjaxSettingWizardUpdate(btnSave);

            ((PracticeSetup)Page).RadAjaxManagerMain.AjaxSettings.AddAjaxSetting(btnReset, ((PracticeSetup)Page).MainMenuTabStrip);
            ((PracticeSetup)Page).RadAjaxManagerMain.AjaxSettings.AddAjaxSetting(btnReset, ((PracticeSetup)Page).NavigateHomeButton);

            ((PracticeSetup)Page).RadAjaxManagerMain.AjaxSettings.AddAjaxSetting(btnSave, ((PracticeSetup)Page).MainMenuTabStrip);
            ((PracticeSetup)Page).RadAjaxManagerMain.AjaxSettings.AddAjaxSetting(btnSave, ((PracticeSetup)Page).NavigateHomeButton);
        }
	}

	protected void btnReset_Click( object sender, EventArgs e )
	{
		Reset( this, e );
	}
	protected void btnSave_Click( object sender, EventArgs e )
	{
		Save( this, e );
	}
}
