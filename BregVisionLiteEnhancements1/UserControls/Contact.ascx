<%@ Control Language="C#" AutoEventWireup="true" Inherits="Contact" CodeBehind="Contact.ascx.cs" %>

<div class="admin-form">
  <div class="flex-grow">
    <asp:Label ID="lblTitle" runat="server" Text="Title" CssClass="FieldLabel"></asp:Label>
    <asp:TextBox ID="txtTitle" runat="server" MaxLength="50" CssClass="FieldLabel"></asp:TextBox>
  </div>
  <div class="flex-row align-start">
    <div>
      <asp:Label ID="lblSalutation" runat="server" Text="Salutation" CssClass="FieldLabel"></asp:Label>
      <div class="FieldLabelSelect-Container">
        <asp:DropDownList ID="ddlSalutation" runat="server" CssClass="RadComboBox_Breg">
          <asp:ListItem></asp:ListItem>
          <asp:ListItem>Dr.</asp:ListItem>
          <asp:ListItem>Mr.</asp:ListItem>
          <asp:ListItem>Mrs.</asp:ListItem>
          <asp:ListItem>Ms.</asp:ListItem>
          <asp:ListItem>Miss</asp:ListItem>
        </asp:DropDownList>
      </div>
    </div>
    <div class="flex-grow">
      <asp:Label ID="lblFirstName" runat="server" Text="First Name" CssClass="FieldLabel"></asp:Label>
      <asp:TextBox ID="txtFirstName" runat="server" MaxLength="50" CssClass="FieldLabel"></asp:TextBox>
      <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" Display="Dynamic" ErrorMessage="* Required" ControlToValidate="txtFirstName" ValidationGroup="Address"></asp:RequiredFieldValidator>
    </div>
    <div>
      <asp:Label ID="lblMiddleName" runat="server" Text="Middle Initial" CssClass="FieldLabel"></asp:Label>
      <asp:TextBox ID="txtMiddleName" runat="server" Width="15px" MaxLength="1" CssClass="FieldLabel"></asp:TextBox>
    </div>
    <div class="flex-grow">
      <asp:Label ID="lblLastName" runat="server" Text="Last Name" CssClass="FieldLabel"></asp:Label>
      <asp:TextBox ID="txtLastName" runat="server" MaxLength="50" CssClass="FieldLabel"></asp:TextBox>
      <asp:RequiredFieldValidator ID="rfvLastName" runat="server" Display="Dynamic" ErrorMessage="* Required" ControlToValidate="txtLastName" ValidationGroup="Address"></asp:RequiredFieldValidator>
    </div>
    <div>
      <asp:Label ID="lblSuffix" runat="server" Text="Suffix" CssClass="FieldLabel"></asp:Label>
      <div class="FieldLabelSelect-Container">
        <asp:DropDownList ID="ddlSuffix" runat="server" CssClass="FieldLabelSelect">
          <asp:ListItem></asp:ListItem>
          <asp:ListItem>M.D.</asp:ListItem>
          <asp:ListItem>Sr.</asp:ListItem>
          <asp:ListItem>Jr.</asp:ListItem>
          <asp:ListItem>III</asp:ListItem>
        </asp:DropDownList>
      </div>
    </div>
  </div>
  <div class="flex-grow">
    <asp:Label ID="lblEmailAddress" runat="server" Text="Email Address" CssClass="FieldLabel"></asp:Label>
    <asp:TextBox ID="txtEmailAddress" runat="server" Width="200px" Rows="100" CssClass="FieldLabel"></asp:TextBox>
    <asp:RequiredFieldValidator ID="rfvEmailAddress" runat="server" ControlToValidate="txtEmailAddress" ErrorMessage="* Required" ValidationGroup="Address" />
    <asp:RegularExpressionValidator ID="revEmailAddress" runat="server" ErrorMessage="Fix: format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmailAddress" ValidationGroup="Address" ></asp:RegularExpressionValidator>
  </div>
  <div class="flex-row align-start">
    <div class="flex-grow flex-50">
      <div class="flex-row">
        <div class="flex-grow">
          <asp:Label ID="lblPhoneWork" runat="server" Text="Phone Work" CssClass="FieldLabel"></asp:Label>
          <telerik:RadMaskedTextBox ID="txtPhoneWork" runat="server" CssClass="FieldLabel" Mask="(###) ###-####" Rows="1" Columns="50" DisplayMask="(###) ###-####" SelectionOnFocus="CaretToBeginning" Skin="Breg" EnableEmbeddedSkins="False" TextWithLiterals="() -" ValidationGroup="PhoneWork">
          </telerik:RadMaskedTextBox>
        </div>
        <div>
          <asp:Label ID="lblPhoneWorkExtension" runat="server" Text="Extension" CssClass="FieldLabel"></asp:Label>
          <telerik:RadMaskedTextBox ID="txtPhoneWorkExtension" runat="server" CssClass="FieldLabel" Mask="##########" Rows="1" Columns="50" DisplayMask="##########" SelectionOnFocus="CaretToBeginning" Skin="Breg" EnableEmbeddedSkins="False" ValidationGroup="PhoneWork" EmptyMessage="Extension">
          </telerik:RadMaskedTextBox>
        </div>
      </div>
      <asp:RequiredFieldValidator ID="rfvPhoneWork" runat="server" ControlToValidate="txtPhoneWork" ErrorMessage="* Required" ValidationGroup="Address" ></asp:RequiredFieldValidator>
      <asp:RegularExpressionValidator ID="revPhoneWork" runat="server" ControlToValidate="txtPhoneWork" Display="Dynamic" ErrorMessage="RegularExpressionValidator" Font-Size="Smaller" ToolTip="Phone Work must be 10 digits or blank" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}" ValidationGroup="Address" >10 digits</asp:RegularExpressionValidator>
    </div>
    <div class="flex-grow flex-50">
      <asp:Label ID="lblPhoneCell" runat="server" Text="Phone Cell" CssClass="FieldLabel"></asp:Label>
      <telerik:RadMaskedTextBox ID="txtPhoneCell" runat="server" CssClass="FieldLabel" Mask="(###) ###-####" Rows="1" Columns="50" DisplayMask="(###) ###-####" SelectionOnFocus="CaretToBeginning" Skin="Breg" EnableEmbeddedSkins="False" TextWithLiterals="() -" ValidationGroup="PhoneWork">
      </telerik:RadMaskedTextBox>
      <asp:RegularExpressionValidator ID="revPhoneCell" runat="server" ControlToValidate="txtPhoneCell" Display="Dynamic" ErrorMessage="RegularExpressionValidator" Font-Size="Smaller" ToolTip="Phone Cell must be 10 digits or blank" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}" ValidationGroup="Address" >10 digits or blank</asp:RegularExpressionValidator>
    </div>
  </div>
  <div class="flex-row">
    <div class="flex-grow">
      <asp:Label ID="lblPhoneHome" runat="server" Text="Phone Home" CssClass="FieldLabel"></asp:Label>
      <telerik:RadMaskedTextBox ID="txtPhoneHome" runat="server" CssClass="FieldLabel" Mask="(###) ###-####" Rows="1" Columns="50" DisplayMask="(###) ###-####" SelectionOnFocus="CaretToBeginning" Skin="Breg" EnableEmbeddedSkins="False" TextWithLiterals="() -" ValidationGroup="PhoneWork">
      </telerik:RadMaskedTextBox>
      <asp:RegularExpressionValidator ID="revPhoneHome" runat="server" ControlToValidate="txtPhoneHome" Display="Dynamic" ErrorMessage="RegularExpressionValidator" Font-Size="Smaller" ToolTip="Phone Home must be 10 digits or blank" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}" ValidationGroup="Address" >10 digits or blank</asp:RegularExpressionValidator>
    </div>
    <div class="flex-grow">
      <asp:Label ID="lblFax" runat="server" Text="Fax" CssClass="FieldLabel"></asp:Label>
      <telerik:RadMaskedTextBox ID="txtFax" runat="server" CssClass="FieldLabel" Mask="(###) ###-####" Rows="1" Columns="50" DisplayMask="(###) ###-####" SelectionOnFocus="CaretToBeginning" Skin="Breg" EnableEmbeddedSkins="False" TextWithLiterals="() -" ValidationGroup="PhoneWork">
      </telerik:RadMaskedTextBox>
      <asp:RegularExpressionValidator ID="revFax" runat="server" ControlToValidate="txtFax" Display="Dynamic" ErrorMessage="RegularExpressionValidator" Font-Size="Smaller" ToolTip="Fax must be 10 digits or blank" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}" ValidationGroup="Address" >10 digits or blank</asp:RegularExpressionValidator>
    </div>
  </div>
  <div id="trIsSpecificContact" class="flex-row">
    <telerik:RadButton runat="server" AutoPostBack="False" ID="chkIsSpecificContact" ToggleType="CheckBox" ButtonType="ToggleButton">
    </telerik:RadButton>
    <asp:Label ID="lblIsSpecificContact" runat="server" Text="Display Contact Info on Patient Receipt" CssClass="FieldLabel"></asp:Label>
  </div>
</div>
