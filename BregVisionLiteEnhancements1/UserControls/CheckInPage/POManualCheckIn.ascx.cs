﻿using System;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.Web.UI;
using ClassLibrary.DAL;
using System.Linq;
using BregVision.UserControls.CheckInPage;
using BregVision.TelerikAjax;



namespace BregVision.UserControls.CheckInPage
{
    public partial class POManualCheckIn : Bases.UserControlBase
    {

        #region Private Variables

        private CheckIn CheckInPage;

        #endregion

        #region Public Properties

        public RadGrid grdManualCheckInRadGrid
		{
			get { return grdManualCheckIn; }
		}


        #endregion


        #region Page Events

        new protected void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);

            CheckInPage = this.Page as CheckIn;

            CheckInPage.ctlRecordsPerPageUserControl.recordsPerPageChangedEvent += new BregVision.UserControls.General.RecordsPerPageEventHandler(ctlRecordsPerPageUserControl_recordsPerPageChangedEvent);

            ctlSearchToolbar.searchEvent += new BregVision.UserControls.General.SearchEventHandler(ctlSearchToolbar_searchEvent);
        }



        void ctlRecordsPerPageUserControl_recordsPerPageChangedEvent(object sender, BregVision.UserControls.General.RecordsPerPageEventArgs e)
        {
            grdManualCheckIn.Rebind();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();
            grdManualCheckIn.SetGridLabel("", false, lblBrowseDispense);
        }        
        
        #endregion

        private void InitializeAjaxSettings()
        {
            Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);

            radAjaxManager.AjaxSettings.AddAjaxSetting(grdManualCheckIn, grdManualCheckIn, CheckInPage.RadAjaxLoadingPanel1Panel);
            radAjaxManager.AjaxSettings.AddAjaxSetting(grdManualCheckIn, lblBrowseDispense);
            radAjaxManager.AjaxSettings.AddAjaxSetting(grdManualCheckIn, lblManualCheckIn);

            radAjaxManager.AjaxSettings.AddAjaxSetting(btnManualCheckIn, grdManualCheckIn, CheckInPage.RadAjaxLoadingPanel1Panel);
            radAjaxManager.AjaxSettings.AddAjaxSetting(btnManualCheckIn, lblBrowseDispense);
            radAjaxManager.AjaxSettings.AddAjaxSetting(btnManualCheckIn, lblManualCheckIn);

            radAjaxManager.AjaxSettings.AddAjaxSetting(ctlSearchToolbar.Toolbar, grdManualCheckIn, CheckInPage.RadAjaxLoadingPanel1Panel);
            radAjaxManager.AjaxSettings.AddAjaxSetting(ctlSearchToolbar.Toolbar, lblBrowseDispense);
            radAjaxManager.AjaxSettings.AddAjaxSetting(ctlSearchToolbar.Toolbar, lblManualCheckIn);

            radAjaxManager.AjaxSettings.AddAjaxSetting(CheckInPage.ctlRecordsPerPageUserControl.RecordsDisplayedTextBox, grdManualCheckIn, CheckInPage.RadAjaxLoadingPanel1Panel);


        }

        void ctlSearchToolbar_searchEvent(object sender, BregVision.UserControls.General.SearchEventArgs e)
        {
            grdManualCheckIn.SearchText = e.SearchText;
            grdManualCheckIn.SearchCriteria = e.SearchCriteria;
            grdManualCheckIn.SearchType = e.SearchType;
            grdManualCheckIn.SetSearchCriteria(e.SearchType);
            btnManualCheckIn.Enabled = true;
        }



        #region Grid Events

        protected void grdManualCheckIn_OnLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                grdManualCheckIn.DataSource = new Object[] { };
                grdManualCheckIn.DataBind(); //Used to show a 'No Records Found' when the page initially loads
            }
        } 
        protected void grdManualCheckIn_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (CheckInPage.rtsCheckInRadTabStrip.SelectedIndex == 2)
            {
                string searchCriteria = grdManualCheckIn.SearchCriteria;
                string searchText = grdManualCheckIn.SearchText;

                if (searchText != null && !string.IsNullOrEmpty(searchCriteria))
                {
                    grdManualCheckIn.Enabled = true;
                    DataSet ds = DAL.PracticeLocation.CheckIn.SearchForCodeOrSupplierManualCheckIn(practiceLocationID, searchCriteria, searchText);
                    grdManualCheckIn.DataSource = ds;

                    
                    grdManualCheckIn.SetRecordCount(ds, "BrandShortname");
                    grdManualCheckIn.SetGridLabel(searchCriteria, Page.IsPostBack, lblBrowseDispense);
                }
            }
            grdManualCheckIn.PageSize = recordsDisplayedPerPage;
        }




        #endregion

        #region Control Events

        protected void btnManualCheckIn_Click(object sender, EventArgs e)
        {
            string StatusText = "";
            //Validation

            StatusText = LoopThroughManualCheckInGrid(grdManualCheckIn.MasterTableView).ToString();
            StatusText = string.Concat(StatusText, " products checked in.");
            grdManualCheckIn.Enabled = false;
            btnManualCheckIn.Enabled = false;
            lblManualCheckIn.Text = StatusText;
            lblManualCheckIn.CssClass = "SuccessMsg";
        }


        #endregion

        #region Methods

        private int LoopThroughManualCheckInGrid(GridTableView gridTableView)
        {
            // initialize number of items checked in counter
            int rowsAffected = 0;
            // loop through grid and find items that are marked for check-in and check them in
            // NOTE: keeps track of number of items checked in to clear the comments box below
            foreach (GridDataItem gridDataItem in gridTableView.Items)
                if (findSelectColumn(gridDataItem))
                {

                    Int32 PracticeLocationID = Convert.ToInt32(Session["PracticeLocationID"]);
                    Int32 PracticeCatalogProductID = Convert.ToInt32((string)gridDataItem["PracticeCatalogProductID"].Text);
                    Int32 ProductInventoryID = Convert.ToInt32((string)gridDataItem["ProductInventoryID"].Text);
                    decimal WholesaleCost = Convert.ToDecimal((string)gridDataItem["WholesaleCost"].Text.Replace("$", ""));
                    RadNumericTextBox QuantityCheckedIn = (RadNumericTextBox)gridDataItem.FindControl("QuantityCheckedIn");
                    Int32 Quantity = Convert.ToInt32(QuantityCheckedIn.Value.ToString());
                    string Comments = txtComments.Text;
                    DAL.PracticeLocation.CheckIn.ManualCheckIn_InsertUpdate(1, PracticeLocationID, PracticeCatalogProductID, ProductInventoryID,
                           WholesaleCost, Quantity, Comments);
                    rowsAffected += Quantity;
                }
            // clear the comments box if more than 1 item was checked in
            if (rowsAffected > 0)
                txtComments.Text = string.Empty;
            // return number of items checked in
            return rowsAffected;
        }

        private Boolean findSelectColumn(GridDataItem gridDataItem)
        {
            if (gridDataItem != null)
            {
                return gridDataItem.Selected;
            }
            else
            {
                return false;
            }
        }


        #endregion

    }
}