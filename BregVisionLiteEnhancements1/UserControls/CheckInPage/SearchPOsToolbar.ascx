﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchPOsToolbar.ascx.cs"
    Inherits="BregVision.UserControls.CheckInPage.SearchPOsToolbar" %>
<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script language="javascript" type="text/javascript">
        function SetPressEnterTarget(toolBarClientID) {
            if (event.which || event.keyCode) {
                if ((event.which == 13) || (event.keyCode == 13)) {
                    var toolBar = $find(toolBarClientID);
                    if (toolBar != null) {
                        var searchButton = toolBar.findItemByValue('search');
                        if (searchButton != null) {
                            searchButton.click()
                        }
                    }
                    return false;
                }
            }
            else {
                return true;
            }
        }
        function stopRKey(evt) {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
        }

        document.onkeypress = stopRKey;
    </script>
</telerik:RadScriptBlock>
<telerik:RadToolBar ID="rtbToolbar" runat="server" AutoPostBack="True" EnableViewState="true" OnButtonClick="rtbToolbar_OnClick" Skin="Breg" EnableEmbeddedSkins="False">
    <Items>
        <%--Radio Buttons--%>
        <telerik:RadToolBarButton ID="rtbSearchCriteria" runat="server">
            <ItemTemplate>
                <span class="FieldLabel">Search By</span>
                <asp:RadioButtonList ID="radSearchCriteria" runat="server" RepeatDirection="Horizontal" TextAlign="Right" ToolTip="You can search both by supplier and a date range or individually by either supplier or date range">
                    <asp:ListItem Value="0">Supplier</asp:ListItem>
                    <asp:ListItem Value="1">Dates</asp:ListItem>
                    <asp:ListItem Selected="true" Value="2">Both</asp:ListItem>
                </asp:RadioButtonList>
            </ItemTemplate>
        </telerik:RadToolBarButton>
        <telerik:RadToolBarButton IsSeparator="true" />
        <%--Supplier Textbox--%>
        <telerik:RadToolBarButton ID="rttbFilter" runat="server" CommandName="FilterText" Value="FilterText">
            <ItemTemplate>
                <span class="FieldLabel">Supplier</span>
                <asp:TextBox ID="txtFilter" runat="server" TabIndex="2" Placeholder="Supplier"></asp:TextBox>
            </ItemTemplate>
        </telerik:RadToolBarButton>
        <telerik:RadToolBarButton IsSeparator="true"/>
        <%--Date Picker--%>
        <telerik:RadToolBarButton ID="rtbDateFilter" runat="server">
            <ItemTemplate>
              <span class="FieldLabel">Dates</span>
              <div class="flex-row search-po-datepicker">
                <div class="flex-row">
                  <span class="FieldLabelLeft">From</span>
                  <telerik:RadDatePicker ID="dtFrom" runat="server" DateInput-DisplayDateFormat="MM/dd/yy" ToolTip="Dates can be entered in the following formats: 01/01/2016, January 1,2016, 01012016" Culture="en-US">
                      <Calendar ID="Calendar1" runat="server" ImagesPath="../../App_Themes/Breg/images/Common/" ShowRowHeaders="False"></Calendar>
                  </telerik:RadDatePicker>                
                </div>
                <div class="flex-row">
                  <span class="FieldLabelLeft">To</span>
                  <telerik:RadDatePicker ID="dtTo" runat="server" DateInput-DisplayDateFormat="MM/dd/yy" ToolTip="Dates can be entered in the following formats: 01/01/2016, January 1,2016, 01012016">
                      <Calendar runat="server" ImagesPath="../../App_Themes/Breg/images/Common/" ShowRowHeaders="False"></Calendar>
                  </telerik:RadDatePicker>
                </div>
              </div>
            </ItemTemplate>
        </telerik:RadToolBarButton>
        <telerik:RadToolBarButton IsSeparator="true"/>
        <%--Search Button--%>
        <telerik:RadToolBarButton ID="rtbFilter" runat="server" CommandName="search" Value="search" ImageUrl="~/App_Themes/Breg/images/Common/search.png" ToolTip="Search" />
    </Items>
</telerik:RadToolBar>
