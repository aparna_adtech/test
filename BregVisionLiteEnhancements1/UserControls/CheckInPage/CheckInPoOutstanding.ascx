﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CheckInPoOutstanding.ascx.cs"
  Inherits="BregVision.UserControls.CheckInPage.CheckInPoOutstanding" %>
<div class="actions-bar flex-row">
  <div>
    <bvu:Help ID="Help102" runat="server" HelpID="102" />
    <asp:Button ID="btnOutStandingPOsSaveChangesInvoice" runat="server" OnClick="btnOutStandingPOsSaveChangesInvoice_Click" Text="Save Changes" CssClass="button-accent" />
    <bvu:Help ID="Help1" runat="server" HelpID="103" />
    <asp:Button ID="btnCheckInProducts" runat="server" Enabled="true" OnClick="btnCheckInProducts_Click" Text="Check In Products" ToolTip="Check In all products selected." CssClass="button-primary" />
  </div>
</div>
<div>
  <asp:Label ID="lblBrowseCheckIn" runat="server" CssClass="WarningMsg"></asp:Label>
</div>
<bvu:VisionRadGridAjax ID="grdPOOutstanding" runat="server" OnDetailTableDataBind="grdPOOutstanding_DetailTableDataBind"
  OnItemDataBound="grdPOOutstanding_ItemDataBound" OnNeedDataSource="grdPOOutstanding_NeedDataSource"
  Width="99%" AddNonBreakingSpace="false" Skin="Breg" EnableEmbeddedSkins="False">
  <MasterTableView DataKeyNames="BregVisionOrderID" HierarchyLoadMode="ServerOnDemand">
    <DetailTables>
      <telerik:GridTableView runat="server" DataKeyNames="BregVisionOrderID,SupplierOrderID"
        Name="PurchaseOrder">
        <Columns>
          <telerik:GridTemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="70px"
            HeaderStyle-Wrap="true" ItemStyle-HorizontalAlign="Center" UniqueName="InvoicePaid">
            <HeaderTemplate>
              <span>Invoice Paid(Y/N)</span>&nbsp;<bvu:Help ID="Help104" runat="server" HelpID="104" />
            </HeaderTemplate>
            <ItemTemplate>
              <asp:CheckBox ID="chkInvoicePaid" runat="server" Checked='<%# Eval("InvoicePaid") %>' />
            </ItemTemplate>
          </telerik:GridTemplateColumn>
          <telerik:GridTemplateColumn HeaderStyle-Wrap="true" HeaderStyle-HorizontalAlign="Center" 
            ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" UniqueName="InvoiceNumber" HeaderStyle-Width="100px" ItemStyle-Width="100px">
            <HeaderTemplate>
              <span>Invoice Number</span>&nbsp;<bvu:Help ID="Help105" runat="server" HelpID="105" />
            </HeaderTemplate>
            <ItemTemplate>
              <asp:TextBox ID="txtInvoiceNumber" runat="server" CssClass="FieldLabel" Text='<%# Eval("InvoiceNumber") %>'
                Width="100px"></asp:TextBox>
            </ItemTemplate>
          </telerik:GridTemplateColumn>
          <telerik:GridBoundColumn DataField="SupplierShortName" HeaderText="Supplier" UniqueName="SupplierShortName" />
          <telerik:GridBoundColumn DataField="BrandShortName" HeaderText="Brand" UniqueName="BrandShortName" />
          <telerik:GridBoundColumn DataField="PurchaseOrder" HeaderText="PO Number" UniqueName="PurchaseOrder" />
          <telerik:GridBoundColumn DataField="Status" HeaderText="Status" UniqueName="Status" />
          <telerik:GridBoundColumn DataField="Total" DataFormatString="{0:C}" HeaderText="Total" UniqueName="Total" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
          <telerik:GridBoundColumn DataField="SupplierOrderID" UniqueName="SupplierOrderID"
            Visible="False" />
          <telerik:GridBoundColumn DataField="BregVisionOrderID" UniqueName="BregVisionOrderID"
            Visible="False" />
        </Columns>
        <DetailTables>
          <telerik:GridTableView runat="server" DataKeyNames="SupplierOrderID,SupplierOrderLineItemID,ProductInventoryID,PracticeCatalogProductID,SupplierOrderLineItemStatusID"
            Name="SupplierOrder">
            <Columns>
              <telerik:GridClientSelectColumn HeaderStyle-HorizontalAlign="Center" HeaderText=""
                ItemStyle-HorizontalAlign="Center" UniqueName="ClientSelectColumn" />
              <telerik:GridBoundColumn DataField="ShortName" HeaderText="Product" UniqueName="ShortName" />
              <telerik:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code" />
              <telerik:GridBoundColumn DataField="LeftRightSide" HeaderText="Side" UniqueName="Side" />
              <telerik:GridBoundColumn DataField="Size" HeaderText="Size" UniqueName="Size" />
              <telerik:GridBoundColumn DataField="Status" HeaderText="Status" UniqueName="Status" />
              <telerik:GridBoundColumn DataField="Packaging" HeaderText="Pkg" UniqueName="Packaging" />
              <telerik:GridBoundColumn DataField="Color" HeaderText="Color" UniqueName="Color" />
              <telerik:GridBoundColumn DataField="Gender" HeaderText="Gender" UniqueName="Gender" />
              <telerik:GridBoundColumn DataField="ActualWholesaleCost" DataFormatString="{0:C}"
                HeaderText="AWC" UniqueName="ActualWholesaleCost" />
              <telerik:GridBoundColumn DataField="LineTotal" DataFormatString="{0:C}" HeaderText="Total"
                UniqueName="LineTotal" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
              <telerik:GridBoundColumn DataField="QuantityOrdered" HeaderStyle-HorizontalAlign="Center"
                HeaderStyle-Width="50px" HeaderStyle-Wrap="true" HeaderText="Qty. Ordered" ItemStyle-HorizontalAlign="Center"
                UniqueName="QuantityOrdered" />
              <telerik:GridBoundColumn DataField="QuantityCheckedIn" HeaderStyle-HorizontalAlign="Center"
                HeaderStyle-Width="50px" HeaderStyle-Wrap="true" HeaderText="Qty. Checked In"
                ItemStyle-HorizontalAlign="Center" UniqueName="QuantityCheckedIn" />
              <telerik:GridTemplateColumn DataField="QuantityToCheckIn" HeaderStyle-HorizontalAlign="Center"
                HeaderStyle-Wrap="true" ItemStyle-HorizontalAlign="Center" UniqueName="QuantityToCheckIn">
                <HeaderTemplate>
                  <span>Qty. To Check In</span>
                  <bvu:Help ID="Help106" runat="server" HelpID="106" />
                </HeaderTemplate>
                <ItemTemplate>&nbsp;<telerik:RadNumericTextBox ID="tbQuantityToCheckIn" runat="server" Culture="English (United States)"
                  LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" NumberFormat-DecimalDigits="0"
                   ShowSpinButtons="False" Text='<%# Eval("QuantityToCheckIn") %>' Width="41px"></telerik:RadNumericTextBox>
                </ItemTemplate>
              </telerik:GridTemplateColumn>
              <telerik:GridBoundColumn DataField="SupplierOrderID" UniqueName="SupplierOrderID"
                Visible="False" />
              <telerik:GridBoundColumn DataField="SupplierOrderLineItemID" Visible="False" UniqueName="SupplierOrderLineItemID" />
              <telerik:GridBoundColumn DataField="ProductInventoryID" Visible="False" UniqueName="ProductInventoryID" />
              <telerik:GridBoundColumn DataField="PracticeCatalogProductID" Visible="False" UniqueName="PracticeCatalogProductID" />
              <telerik:GridBoundColumn DataField="SupplierOrderLineItemStatusID" Visible="False"
                UniqueName="SupplierOrderLineItemStatusID" />
            </Columns>
          </telerik:GridTableView>
        </DetailTables>
      </telerik:GridTableView>
    </DetailTables>
    <Columns>
      <telerik:GridBoundColumn DataField="PurchaseOrder" HeaderText="Purchase Order" UniqueName="PurchaseOrder" />
      <telerik:GridBoundColumn DataField="CustomPurchaseOrderCode" HeaderStyle-HorizontalAlign="Center"
        HeaderText="Custom PO Number" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Center"
        UniqueName="CustomPurchaseOrderCode" />
      <telerik:GridBoundColumn DataField="Status" HeaderText="Status" UniqueName="Status" />
      <telerik:GridBoundColumn DataField="ShippingType" HeaderText="Shipping Type" UniqueName="ShippingType" />
      <telerik:GridBoundColumn DataField="Total" DataFormatString="{0:C}" HeaderText="Total"
        UniqueName="Total" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
      <telerik:GridDateTimeColumn DataField="CreatedDate" HeaderText="Date Created" UniqueName="CreatedDate" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" DataFormatString="" />
      <telerik:GridBoundColumn DataField="BregVisionOrderID" UniqueName="BregVisionOrderID"
        Visible="False" />
    </Columns>
  </MasterTableView>
</bvu:VisionRadGridAjax>
