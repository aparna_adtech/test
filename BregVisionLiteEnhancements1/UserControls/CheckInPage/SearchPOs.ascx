﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchPOs.ascx.cs" Inherits="BregVision.UserControls.CheckInPage.SearchPOs" %>
<%@ Register Src="~/UserControls/CheckInPage/SearchPOsToolbar.ascx" TagName="SearchPOsToolbar"
  TagPrefix="bvu" %>
<div class="actions-bar flex-row">
  <bvu:SearchPOsToolbar ID="ctlSearchPOsToolbar" runat="server" />
  <div>
    <asp:Button ID="btnSearchPOsSaveChangesInvoice" runat="server" Enabled="false" OnClick="btnSearchPOsSaveChangesInvoice_Click" Text="Save Changes" CssClass="button-accent" />
    <bvu:Help ID="Help1" runat="server" HelpID="103" />
    <asp:Button ID="btnPOSearchCheckInProducts" runat="server" Enabled="false" OnClick="btnPOSearchCheckInProducts_Click" Text="Check In Products" ToolTip="Check In all products selected." CssClass="button-primary" />
  </div>
</div>
<div>
  <asp:Label ID="lblBrowseDispense1" runat="server" CssClass="WarningMsg"></asp:Label>
  <asp:Label ID="lblPOSearchStatus" runat="server" CssClass="WarningMsg"></asp:Label>
</div>
<bvu:VisionRadGridAjax ID="grdSearch" runat="server" AllowMultiRowSelection="True"
  OnItemDataBound="grdSearch_ItemDataBound" OnLoad="grdSearch_OnLoad" OnNeedDataSource="grdSearch_NeedDataSource"
  SortingSettings-SortedAscToolTip="Sort Ascending" SortingSettings-SortedDescToolTip="Sort Descending"
  SortingSettings-SortToolTip="Click here to Sort" AddNonBreakingSpace="false" Skin="Breg" EnableEmbeddedSkins="False">
  <MasterTableView AutoGenerateColumns="False">
    <Columns>
      <telerik:GridClientSelectColumn HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="35px"
        HeaderText="" ItemStyle-HorizontalAlign="Center" UniqueName="ClientSelectColumn" />
      <telerik:GridTemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="true"
        ItemStyle-HorizontalAlign="Center" UniqueName="InvoicePaid">
        <HeaderTemplate>
          <span>Invoice Paid(Y/N)</span>
          <bvu:Help ID="Help3" runat="server" HelpID="104" />
        </HeaderTemplate>
        <ItemTemplate>
          <asp:CheckBox ID="chkInvoicePaidPOSearch" runat="server" Checked='<%# Eval("InvoicePaid") %>' />
        </ItemTemplate>
      </telerik:GridTemplateColumn>
      <telerik:GridTemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="true"
        ItemStyle-HorizontalAlign="Center" UniqueName="InvoiceNumber">
        <HeaderTemplate>
          <span>Invoice Number</span>
          <bvu:Help ID="Help2" runat="server" HelpID="105" />
        </HeaderTemplate>
        <ItemTemplate>
          <asp:TextBox ID="txtInvoiceNumberPOSearch" runat="server" CssClass="FieldLabel" Text='<%# Eval("InvoiceNumber") %>'
            Width="180px"></asp:TextBox>
        </ItemTemplate>
      </telerik:GridTemplateColumn>
      <telerik:GridBoundColumn DataField="BrandShortName" HeaderStyle-Width="50px" HeaderStyle-Wrap="true"
        HeaderText="Supplier" ItemStyle-Wrap="true" UniqueName="BrandShortName" />
      <telerik:GridBoundColumn DataField="PurchaseOrder" HeaderStyle-HorizontalAlign="Center"
        HeaderStyle-Width="50px" HeaderStyle-Wrap="true" HeaderText="PO Number" ItemStyle-HorizontalAlign="Center"
        UniqueName="PurchaseOrder" />
      <telerik:GridBoundColumn DataField="CustomPurchaseOrderCode" HeaderStyle-HorizontalAlign="Center"
        HeaderStyle-Wrap="true" HeaderText="Custom PO Number" ItemStyle-Width="70px"
        UniqueName="CustomPurchaseOrderCode" />
      <telerik:GridBoundColumn DataField="ShortName" HeaderText="Short Name" UniqueName="ShortName" />
      <telerik:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code" />
      <telerik:GridBoundColumn DataField="LeftRightSide" HeaderText="Side" ItemStyle-HorizontalAlign="Center"
        HeaderStyle-HorizontalAlign="Center" UniqueName="Side" />
      <telerik:GridBoundColumn DataField="Size" HeaderText="Size" ItemStyle-HorizontalAlign="Center"
        HeaderStyle-HorizontalAlign="Center" UniqueName="Size" />
      <telerik:GridBoundColumn DataField="Status" HeaderText="Status" ItemStyle-HorizontalAlign="Center"
        HeaderStyle-HorizontalAlign="Center" UniqueName="Status" />
      <telerik:GridBoundColumn DataField="Packaging" HeaderText="Pkg." ItemStyle-HorizontalAlign="Center"
        HeaderStyle-HorizontalAlign="Center" UniqueName="Packaging" />
      <telerik:GridBoundColumn DataField="Color" HeaderText="Color" UniqueName="Color" />
      <telerik:GridBoundColumn DataField="Gender" HeaderText="Gender" ItemStyle-HorizontalAlign="Center"
        HeaderStyle-HorizontalAlign="Center" UniqueName="Gender" />
      <telerik:GridBoundColumn DataField="CreatedDate" HeaderText="Date" UniqueName="CreatedDate" />
      <telerik:GridBoundColumn DataField="QuantityOrdered" HeaderStyle-HorizontalAlign="Center"
        HeaderStyle-Width="50px" HeaderStyle-Wrap="true" HeaderText="Qty. Ordered" ItemStyle-HorizontalAlign="Center"
        UniqueName="QuantityOrdered" />
      <telerik:GridBoundColumn DataField="QuantityCheckedIn" HeaderStyle-HorizontalAlign="Center"
        HeaderStyle-Width="50px" HeaderStyle-Wrap="true" HeaderText="Qty. Checked In"
        ItemStyle-HorizontalAlign="Center" UniqueName="QuantityCheckedIn" />
      <telerik:GridTemplateColumn DataField="QuantityToCheckIn" HeaderStyle-HorizontalAlign="Center"
        HeaderStyle-Width="50px" HeaderStyle-Wrap="true" ItemStyle-HorizontalAlign="Center"
        UniqueName="QuantityToCheckIn">
        <HeaderTemplate>
          <span>Qty. To Check In</span>
          <bvu:Help ID="Help33" runat="server" HelpID="106" />
        </HeaderTemplate>
        <ItemTemplate>
          <telerik:RadNumericTextBox ID="tbQuantityToCheckIn" runat="server" Culture="English (United States)"
            LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" NumberFormat-DecimalDigits="0"
             ShowSpinButtons="False" Skin="Web20" SpinDownCssClass="Web20" SpinUpCssClass="Web20"
            Text='<%# Eval("QuantityToCheckIn") %>' Width="41px">
          </telerik:RadNumericTextBox>
        </ItemTemplate>
      </telerik:GridTemplateColumn>
      <telerik:GridBoundColumn DataField="PracticeCatalogProductID" Display="False" UniqueName="PracticeCatalogProductID" />
      <telerik:GridBoundColumn DataField="ProductInventoryID" Display="False" UniqueName="ProductInventoryID" />
      <telerik:GridBoundColumn DataField="BregVisionOrderID" Display="False" UniqueName="BregVisionOrderID" />
      <telerik:GridBoundColumn DataField="SupplierOrderID" Display="False" UniqueName="SupplierOrderID" />
      <telerik:GridBoundColumn DataField="ActualWholesaleCost" DataFormatString="{0:C}"
        Display="False" UniqueName="ActualWholesaleCost" />
      <telerik:GridBoundColumn DataField="SupplierOrderLineItemID" Display="False" UniqueName="SupplierOrderLineItemID" />
      <telerik:GridBoundColumn DataField="SupplierOrderLineItemStatusID" Display="False"
        UniqueName="SupplierOrderLineItemStatusID" />
    </Columns>
  </MasterTableView>
</bvu:VisionRadGridAjax>
