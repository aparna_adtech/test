﻿using System;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.Web.UI;
using ClassLibrary.DAL;
using System.Linq;
using BregVision.UserControls.CheckInPage;
using BregVision.TelerikAjax;

namespace BregVision.UserControls.CheckInPage
{
    public partial class SearchPOs :Bases.UserControlBase
    {
        #region Private Variables

        private CheckIn CheckInPage;

        #endregion

        #region Public Properties

        public RadGrid grdSearchRadGrid
        {
            get { return grdSearch; }
        } 

        public Label lblPOSearchStatusLabel
		{
			get { return lblPOSearchStatus; }
		}

        #endregion

        #region Page Events

        new protected void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);


            CheckInPage = this.Page as CheckIn;

            CheckInPage.ctlRecordsPerPageUserControl.recordsPerPageChangedEvent += new BregVision.UserControls.General.RecordsPerPageEventHandler(ctlRecordsPerPageUserControl_recordsPerPageChangedEvent);
            ctlSearchPOsToolbar.poSearchEvent += new POSearchEventHandler(ctlSearchPOsToolbar_poSearchEvent);
            
        }

        void ctlSearchPOsToolbar_poSearchEvent(object sender, POSearchEventArgs e)
        {
            lblPOSearchStatus.Text = "";
            lblBrowseDispense1.Text = "";
            grdSearch.SearchText = e.SearchText;
            grdSearch.SearchCriteria = e.SearchCriteria;
            grdSearch.SearchDateFrom = e.FromDate;
            grdSearch.SearchDateTo = e.ToDate;
            grdSearch.SearchType = e.SearchType;
            grdSearch.SetSearchCriteria(e.SearchType);
            grdSearch.Visible = true;
            btnPOSearchCheckInProducts.Enabled = true;
            btnSearchPOsSaveChangesInvoice.Enabled = true;

            

        }

        void ctlRecordsPerPageUserControl_recordsPerPageChangedEvent(object sender, BregVision.UserControls.General.RecordsPerPageEventArgs e)
        {
            grdSearch.Rebind();

        }

        protected void Page_Load(object sender, EventArgs e)
        {

            InitializeAjaxSettings();
            
        }

        #endregion

        private void InitializeAjaxSettings()
        {
            Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);

            radAjaxManager.AjaxSettings.AddAjaxSetting(grdSearch, grdSearch, CheckInPage.RadAjaxLoadingPanel1Panel);
            radAjaxManager.AjaxSettings.AddAjaxSetting(grdSearch, lblPOSearchStatus);
            radAjaxManager.AjaxSettings.AddAjaxSetting(grdSearch, lblBrowseDispense1);

            radAjaxManager.AjaxSettings.AddAjaxSetting(btnSearchPOsSaveChangesInvoice, grdSearch, CheckInPage.RadAjaxLoadingPanel1Panel);
            radAjaxManager.AjaxSettings.AddAjaxSetting(btnSearchPOsSaveChangesInvoice, lblPOSearchStatus);
            radAjaxManager.AjaxSettings.AddAjaxSetting(btnSearchPOsSaveChangesInvoice, lblBrowseDispense1);

            radAjaxManager.AjaxSettings.AddAjaxSetting(btnPOSearchCheckInProducts, grdSearch, CheckInPage.RadAjaxLoadingPanel1Panel);
            radAjaxManager.AjaxSettings.AddAjaxSetting(btnPOSearchCheckInProducts, lblPOSearchStatus);
            radAjaxManager.AjaxSettings.AddAjaxSetting(btnPOSearchCheckInProducts, lblBrowseDispense1);

            radAjaxManager.AjaxSettings.AddAjaxSetting(CheckInPage.ctlRecordsPerPageUserControl.RecordsDisplayedTextBox, grdSearch, CheckInPage.RadAjaxLoadingPanel1Panel);

        }



        #region GridEvents

        protected void grdSearch_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (CheckInPage.rtsCheckInRadTabStrip.SelectedIndex == 1)
            {
                DataSet ds = DAL.PracticeLocation.Supplier.SearchForPOsBySupplierAndOrDateRange(practiceLocationID, Convert.ToInt32(grdSearch.SearchCriteria), grdSearch.SearchText, grdSearch.SearchDateFrom, grdSearch.SearchDateTo);
                grdSearch.DataSource = ds;

                
                grdSearch.SetRecordCount(ds, "BrandShortname");
                grdSearch.SetGridLabel("DateOrSupplier", Page.IsPostBack, lblBrowseDispense1);

            }
            grdSearch.PageSize = recordsDisplayedPerPage;
        }

        protected void grdSearch_OnLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                grdSearch.DataSource = new Object[] { };
                grdSearch.DataBind(); //Used to show a 'No Records Found' when the page initially loads
            }
        }

        protected void grdSearch_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

                GridDataItem dataItem = (GridDataItem)e.Item;
                //Int32 supplierOrderID = dataItem.GetColumnInt32("SupplierOrderID");
                //bool invoicePaid = dataItem.GetColumnChecked("chkInvoicePaidPOSearch");
                //string invoiceNumber = dataItem.GetColumnText("txtInvoiceNumberPOSearch");

                //Sets max value of the Quantity to check in box
                Int32 QuantityOrdered = dataItem.GetColumnInt32("QuantityOrdered");       //Int16 QuantityOrdered = System.Convert.ToInt16(dataItem["QuantityOrdered"].Text);
                Int32 QuantityCheckedIn = dataItem.GetColumnInt32("QuantityCheckedIn");        //Int16 QuantityCheckedIn = System.Convert.ToInt16(dataItem["QuantityCheckedIn"].Text);
                RadNumericTextBox tbQuantityToCheckIn = (RadNumericTextBox)dataItem.FindControl("tbQuantityToCheckIn");
                if ((QuantityOrdered - QuantityCheckedIn) < 0)
                {
                    tbQuantityToCheckIn.MaxValue = 0;
                }
                else
                {
                    tbQuantityToCheckIn.MaxValue = QuantityOrdered - QuantityCheckedIn;
                }

                //Greys out and disables products that have already been checked in
                Int32 supplierOrderLineItemStatusID = dataItem.GetColumnInt32("SupplierOrderLineItemStatusID");
                if (supplierOrderLineItemStatusID == 2)
                {
                    dataItem.BackColor = System.Drawing.Color.WhiteSmoke;
                    dataItem.Enabled = false;
                }
            }
        }

        #endregion

        #region Control Events

        protected void btnPOSearchCheckInProducts_Click(object sender, EventArgs e)
        {
            string StatusText = "";
            StatusText = LoopThroughPOSearchGrid(grdSearch.MasterTableView, true).ToString();
            StatusText = string.Concat(StatusText, " product(s) checked in.");
            grdSearch.Enabled = false;
            btnPOSearchCheckInProducts.Enabled = false;
            btnSearchPOsSaveChangesInvoice.Enabled = false;
            lblPOSearchStatus.Text = StatusText;
        }

        protected void btnSearchPOsSaveChangesInvoice_Click(object sender, EventArgs e)
        {
            string StatusText = "";
            StatusText = LoopThroughPOSearchGrid(grdSearch.MasterTableView, false).ToString();
            StatusText = string.Concat(StatusText, " product(s) checked in.");
            grdSearch.Enabled = false;
            btnPOSearchCheckInProducts.Enabled = false;
            btnSearchPOsSaveChangesInvoice.Enabled = false;
            lblPOSearchStatus.Text = StatusText;
        }


        #endregion

        #region Methods

        private int LoopThroughPOSearchGrid(GridTableView gridTableView, bool IsCheckIn)
        {
            int rowsAffected = 0;
            foreach (GridDataItem gridDataItem in gridTableView.Items)
            {

                if (findSelectColumn(gridDataItem) && (Convert.ToInt32((string)gridDataItem["SupplierOrderLineItemStatusID"].Text) != 2) && IsCheckIn == true)
                {
                    Int32 PracticeCatalogProductID = Convert.ToInt32((string)gridDataItem["PracticeCatalogProductID"].Text);
                    Int32 ProductInventoryID = Convert.ToInt32((string)gridDataItem["ProductInventoryID"].Text);
                    Int32 BregVisionOrderID = Convert.ToInt32((string)gridDataItem["BregVisionOrderID"].Text);
                    Int32 SupplierOrderID = Convert.ToInt32((string)gridDataItem["supplierOrderID"].Text);
                    Int32 SupplierOrderLineItemID = Convert.ToInt32((string)gridDataItem["SupplierOrderLineItemID"].Text);
                    decimal ActualWholesaleCost = Convert.ToDecimal((string)gridDataItem["ActualWholesaleCost"].Text.Replace("$", ""));
                    RadNumericTextBox tbQuantityToCheckIn = (RadNumericTextBox)gridDataItem.FindControl("tbQuantityToCheckIn");
                    Int32 Quantity = Convert.ToInt32(tbQuantityToCheckIn.Value.ToString());
                    DAL.PracticeLocation.CheckIn.POCheckIn_InsertUpdate(1, PracticeCatalogProductID, ProductInventoryID,
                        BregVisionOrderID, SupplierOrderID, SupplierOrderLineItemID, ActualWholesaleCost, Quantity);

                    rowsAffected += Quantity;
                }
                else 
                {
                    //added by u=  Get the Invoice Paid and Number  added Dec 31/2009
                    Int32 supplierOrderID = gridDataItem.GetColumnInt32("supplierOrderID");
                    bool invoicePaid = gridDataItem.GetColumnChecked("chkInvoicePaidPOSearch");
                    string invoiceNumber = gridDataItem.GetColumnText("txtInvoiceNumberPOSearch");
                    if (supplierOrderID > 0)
                    {
                        DAL.PracticeLocation.CheckIn.SaveInvoicePaymentInfo(supplierOrderID, invoicePaid, invoiceNumber);
                    }
                }
            }
            return rowsAffected;
        }


        private Boolean findSelectColumn(GridDataItem gridDataItem)
        {
            if (gridDataItem != null)
            {
                return gridDataItem.Selected;
            }
            else
            {
                return false;
            }
        }

        #endregion
    }
}