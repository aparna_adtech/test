﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace BregVision.UserControls.CheckInPage
{
    public delegate void POSearchEventHandler(object sender, POSearchEventArgs e);

    public partial class SearchPOsToolbar : Bases.UserControlBase
    {

        //protected void Page_Load(object sender, EventArgs e)
        //{
            //Sets date in the summary tab to todays date
            //RadDatePicker dtFrom = (RadDatePicker)rttbDateFilter.FindControl("dtFrom");
            //if (dtFrom.SelectedDate == null)
            //{
            //    dtFrom.SelectedDate = DateTime.Today.AddMonths(-1);
            //}
            //RadDatePicker dtTo = (RadDatePicker)rttbDateFilter.FindControl("dtTo");
            //if (dtTo.SelectedDate == null)
            //{
            //    dtTo.SelectedDate = DateTime.Today;
            //}
        //}

        //protected void RadToolbar1_OnClick(object sender, Telerik.WebControls.RadToolbarClickEventArgs e)
        //{

            //lblPOSearchStatus.Text = ""; //Sets status to blank
            //if (e.Button.CommandName == "Search")
            //{
            //    _UIHelper_grdSearch.IsSearch = true;

            //    lblPOSearchStatus.Text = "Searching...";
            //    RadioButtonList radSearchCriteria = (RadioButtonList)SearchCriteria.FindControl("radSearchCriteria");
            //    TextBox searchText = (TextBox)rttbFilter.FindControl("tbFilter");

            //    RadDatePicker dtFrom = (RadDatePicker)rttbDateFilter.FindControl("dtFrom");

            //    RadDatePicker dtTo = (RadDatePicker)rttbDateFilter.FindControl("dtTo");

            //    if (radSearchCriteria.SelectedIndex == 0) //User has select both
            //    {
            //        if (searchText.Text.Length == 0)
            //        {
            //            lblPOSearchStatus.Text = "A supplier must be entered";
            //            searchText.Focus();
            //            return;
            //        }
            //        if (dtFrom.IsEmpty)
            //        {
            //            lblPOSearchStatus.Text = "A valid 'From Date' needs to be entered";
            //            dtFrom.Focus();
            //            return;
            //        }

            //        if (dtTo.IsEmpty)
            //        {
            //            lblPOSearchStatus.Text = "A valid 'To Date' needs to be entered";
            //            dtTo.Focus();
            //            return;
            //        }

            //    }


            //    if (radSearchCriteria.SelectedIndex == 1) //User has selected Supplier
            //    {
            //        if (searchText.Text.Length == 0)
            //        {
            //            lblPOSearchStatus.Text = "A supplier must be entered";
            //            searchText.Focus();
            //            return;
            //        }

            //    }

            //    if (radSearchCriteria.SelectedIndex == 2) //User has selected Supplier
            //    {
            //        if (dtFrom.IsEmpty)
            //        {
            //            lblPOSearchStatus.Text = "A valid 'From Date' needs to be entered";
            //            dtFrom.Focus();
            //            return;
            //        }

            //        if (dtTo.IsEmpty)
            //        {
            //            lblPOSearchStatus.Text = "A valid 'To Date' needs to be entered";
            //            dtTo.Focus();
            //            return;
            //        }

            //    }

            //    grdSearch.DataSource = DAL.PracticeLocation.Supplier.SearchForPOsBySupplierAndOrDateRange(Convert.ToInt32(Session["PracticeLocationID"]),
            //        radSearchCriteria.SelectedIndex, searchText.Text, Convert.ToDateTime(dtFrom.SelectedDate),
            //        Convert.ToDateTime(dtTo.SelectedDate));

            //    _UIHelper_grdSearch.SetGridLabel("DateOrSupplier", Page.IsPostBack, lblBrowseDispense1);

            //    grdSearch.Enabled = true;
            //    grdSearch.DataBind();

            //    if (grdSearch.Items.Count > 0)
            //    {
            //        lblPOSearchStatus.Text = "";
            //        btnPOSearchCheckInProducts.Enabled = true;
            //        btnSearchPOsSaveChangesInvoice.Enabled = true;
            //    }
            //    else
            //    {
            //        lblPOSearchStatus.Text = "No records found.";
            //    }
            //}
        //}
        
        public event POSearchEventHandler poSearchEvent;

        private RadGrid _Grid;
        public RadGrid Grid
        {
            get
            {
                return _Grid;
            }
            set { _Grid = value; }
        }

        public RadToolBar Toolbar
        {
            get
            {
                return rtbToolbar;
            }
        }

        public RadAjaxLoadingPanel DefaultLoadingPanel { get; set; }

        public void HideBrowse()
        {
            //rtbBrowse.Visible = false;
            //rtbToolbar.Width = Unit.Parse((rtbToolbar.Width.Value - 50).ToString());
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();

            InitializeToolbarDates();


        }

        private void InitializeToolbarDates()
        {
            //Sets date in the summary tab to todays date
            RadDatePicker dtFrom = (RadDatePicker)rtbDateFilter.FindControl("dtFrom");
            if (dtFrom.SelectedDate == null)
            {
                dtFrom.SelectedDate = DateTime.Today.AddMonths(-1);
            }
            RadDatePicker dtTo = (RadDatePicker)rtbDateFilter.FindControl("dtTo");
            if (dtTo.SelectedDate == null)
            {
                dtTo.SelectedDate = DateTime.Today;
            }
        }

        private void InitializeAjaxSettings()
        {
            if (Grid != null)
            {
                Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);
                if (radAjaxManager != null)
                {
                    radAjaxManager.AjaxSettings.AddAjaxSetting(rtbToolbar, Grid, DefaultLoadingPanel);
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            SetPressEnterTarget();
            HideUnusedButtons();

            SelectDefaultDispenseSearchCriteria();
        }

        private void SelectDefaultDispenseSearchCriteria()
        {
            if (this.Page is BregVision.Dispense && Session["DefaultDispenseSearchCriteria"] != null && Page.IsPostBack == false)
            {
                RadToolBarItem button = FindToolbarButton(Session.GetString("DefaultDispenseSearchCriteria"));

                RadToolBarSplitButton filterCriteria = FindDropDownList();

                if (button != null && filterCriteria != null)
                {
                    filterCriteria.EnableDefaultButton = true;
                    filterCriteria.DefaultButtonIndex = button.Index;

                }
            }
        }
        //  <Buttons>
        //    <telerik:RadToolBarButton runat="server" Text="Product Code" Value="Code" CheckOnClick="true" PostBack="false" Checked="true" EnableViewState="true" />
        //    <telerik:RadToolBarButton runat="server" Text="Product Name" Value="Name" CheckOnClick="true" PostBack="false" EnableViewState="true" />
        //    <telerik:RadToolBarButton runat="server" Text="Manufacturer" Value="Brand" CheckOnClick="true" PostBack="false" EnableViewState="true" />
        //    <telerik:RadToolBarButton runat="server" Text="Category" Value="Category" CheckOnClick="true" PostBack="false" EnableViewState="true" />
        //    <telerik:RadToolBarButton runat="server" Text="HCPCs" Value="HCPCs" CheckOnClick="true" PostBack="false" EnableViewState="true" />
        //    <telerik:RadToolBarButton runat="server" Text="Patient Code" Value="PatientID" CheckOnClick="true" PostBack="false" EnableViewState="true" />
        //    <telerik:RadToolBarButton runat="server" Text="PO Number" Value="PONumber" CheckOnClick="true" PostBack="false" EnableViewState="true" />
        //    <telerik:RadToolBarButton runat="server" Text="Physician Name" Value="PhysicianName" CheckOnClick="true" PostBack="false" EnableViewState="true" />
        //    <telerik:RadToolBarButton runat="server" Text="Patient Code" Value="PatientCode" CheckOnClick="true" PostBack="false" EnableViewState="true" />
        //    <telerik:RadToolBarButton runat="server" Text="Date Dispensed" Value="DispenseDate" CheckOnClick="true" PostBack="false" EnableViewState="true" />
        //</Buttons>

        //Dispense Modifications
        //<radC:RadComboBoxItem ID="RadComboBoxItem5" runat="server" Text="Product Code" Value="ProductCode" />
        //<radC:RadComboBoxItem ID="RadComboBoxItem3" runat="server" Text="Patient Code" Value="PatientCode" />
        //<radC:RadComboBoxItem ID="RadComboBoxItem2" runat="server" Text="Physician Name" Value="PhysicianName" />
        //<radC:RadComboBoxItem ID="RadComboBoxItem4" runat="server" Text="Date Dispensed" Value="DispenseDate" />


        public void ClearDropdownButtons()
        {
            RadToolBarSplitButton filterCriteria = FindDropDownList();
            filterCriteria.Buttons.Clear();
        }



        public void AddButton(string text, string value)
        {
            Telerik.Web.UI.RadToolBarButton newButton = new RadToolBarButton();
            newButton.Text = text;
            newButton.Value = value;
            newButton.CheckOnClick = true;
            newButton.PostBack = false;
            newButton.EnableViewState = true;

            RadToolBarSplitButton filterCriteria = FindDropDownList();
            filterCriteria.Buttons.Add(newButton);
        }


        private void HideUnusedButtons()
        {
            if (BLL.Website.IsVisionExpress())
            {
                RemoveButton("Brand");
                RemoveButton("HCPCs");
                RemoveButton("Supplier");
                RemoveButton("Category");
            }
        }

        private void RemoveButton(string buttonValue)
        {
            RadToolBarItem button = FindToolbarButton(buttonValue);

            RadToolBarSplitButton filterCriteria = FindDropDownList();

            if (button != null && filterCriteria != null)
            {
                filterCriteria.Buttons.Remove(button);
            }
        }

        private RadToolBarSplitButton FindDropDownList()
        {

            RadToolBarItem filterCriteriaItem = rtbToolbar.FindItemByValue("FilterCriteria");
            if (filterCriteriaItem is RadToolBarSplitButton)
            {
                return filterCriteriaItem as RadToolBarSplitButton;
            }
            return null;
        }

        private RadToolBarItem FindToolbarButton(string buttonValue)
        {
            RadToolBarItem button;
            button = rtbToolbar.FindItemByValue(buttonValue);
            return button;
        }

        private void SetPressEnterTarget()
        {
            TextBox searchTextBox = (TextBox)rttbFilter.FindControl("txtFilter");
            TelerikAjax.UIHelper.SetEnterPressTarget(searchTextBox, rtbFilter);
        }

        protected void rtbToolbar_OnClick(object sender, RadToolBarEventArgs e)
        {
            RadToolBarButton button = e.Item as RadToolBarButton;
            switch (button.CommandName.ToLower())
            {
                case "search":
                    ProcessSearch(e);
                    break;
                case "browse":
                    ProcessBrowse(e);
                    break;
            }
        }

        private void ProcessSearch(RadToolBarEventArgs e)
        {
            SearchPOs searchPOsControl = this.Parent as SearchPOs;

            RadioButtonList radSearchCriteria = (RadioButtonList)rtbSearchCriteria.FindControl("radSearchCriteria");
            TextBox searchTextBox = (TextBox)rttbFilter.FindControl("txtFilter");

            RadDatePicker dtFrom = (RadDatePicker)rtbDateFilter.FindControl("dtFrom");

            RadDatePicker dtTo = (RadDatePicker)rtbDateFilter.FindControl("dtTo");

            if (radSearchCriteria.SelectedIndex == 0) //User has selected supplier
            {
                if (searchTextBox.Text.Length == 0)
                {
                    searchPOsControl.lblPOSearchStatusLabel.Text = "A supplier must be entered";
                    searchTextBox.Focus();
                    return;
                }
           }
            if (radSearchCriteria.SelectedIndex == 1) //User has selected Date
            {
                if (dtFrom.IsEmpty)
                {
                    searchPOsControl.lblPOSearchStatusLabel.Text = "A valid 'From Date' needs to be entered";
                    dtFrom.Focus();
                    return;
                }

                if (dtTo.IsEmpty)
                {
                    searchPOsControl.lblPOSearchStatusLabel.Text = "A valid 'To Date' needs to be entered";
                    dtTo.Focus();
                    return;
                }
            }

            if (radSearchCriteria.SelectedIndex == 2) //User has selected Both
            {
                if (searchTextBox.Text.Length == 0)
                {
                    searchPOsControl.lblPOSearchStatusLabel.Text = "A supplier must be entered";
                    searchTextBox.Focus();
                    return;
                }
                if (dtFrom.IsEmpty)
                {
                    searchPOsControl.lblPOSearchStatusLabel.Text = "A valid 'From Date' needs to be entered";
                    dtFrom.Focus();
                    return;
                }

                if (dtTo.IsEmpty)
                {
                    searchPOsControl.lblPOSearchStatusLabel.Text = "A valid 'To Date' needs to be entered";
                    dtTo.Focus();
                    return;
                }

            }

            if (radSearchCriteria.SelectedIndex > -1 && radSearchCriteria.SelectedIndex < 3)
            {
                //find search text
                string searchText = searchTextBox.Text;

                //FindSearchCriteria
                string searchCriteria = radSearchCriteria.SelectedIndex.ToString();



                string searchType = "Search";


                if (!string.IsNullOrEmpty(searchCriteria) && poSearchEvent != null)
                {
                    POSearchEventArgs searchEventArgs = new POSearchEventArgs(searchCriteria, searchText, Convert.ToDateTime(dtFrom.SelectedDate), Convert.ToDateTime(dtTo.SelectedDate), searchType);
                    poSearchEvent(this, searchEventArgs);
                }
            }
        }

        private void ProcessBrowse(RadToolBarEventArgs e)
        {






            //    grdSearch.DataSource = DAL.PracticeLocation.Supplier.SearchForPOsBySupplierAndOrDateRange(Convert.ToInt32(Session["PracticeLocationID"]),
            //        radSearchCriteria.SelectedIndex, searchText.Text, Convert.ToDateTime(dtFrom.SelectedDate),
            //        Convert.ToDateTime(dtTo.SelectedDate));

            //    _UIHelper_grdSearch.SetGridLabel("DateOrSupplier", Page.IsPostBack, lblBrowseDispense1);

            //    grdSearch.Enabled = true;
            //    grdSearch.DataBind();

            //    if (grdSearch.Items.Count > 0)
            //    {
            //        lblPOSearchStatus.Text = "";
            //        btnPOSearchCheckInProducts.Enabled = true;
            //        btnSearchPOsSaveChangesInvoice.Enabled = true;
            //    }
            //    else
            //    {
            //        lblPOSearchStatus.Text = "No records found.";
            //    }
            //}

        }

        private string GetSearchCriteria(RadToolBarEventArgs e)
        {
            RadToolBarButton selectedButton = GetCriteriaButton(e.Item.ToolBar);
            if (selectedButton != null)
            {
                return selectedButton.Value;
            }
            return string.Empty;
        }

        private static RadToolBarButton GetCriteriaButton(RadToolBar radToolBar)
        {
            RadToolBarItem filterCriteriaItem = radToolBar.FindItemByValue("FilterCriteria");
            if (filterCriteriaItem is RadToolBarSplitButton)
            {
                RadToolBarSplitButton filterCriteria = filterCriteriaItem as RadToolBarSplitButton;
                foreach (RadToolBarButton button in filterCriteria.Buttons)
                {
                    if (button.Checked == true)
                    {
                        return button;
                    }
                }
                if (filterCriteria.Buttons.Count > 0)
                {
                    return filterCriteria.Buttons[0];
                }
            }
            return null;
        }

        private static void ClearSearchText(RadToolBarEventArgs e)
        {
            TextBox txtFilter = GetFilterTextbox(e.Item.ToolBar);
            txtFilter.Text = "";
        }

        private static string GetSeachText(RadToolBarEventArgs e)
        {
            TextBox txtFilter = GetFilterTextbox(e.Item.ToolBar);
            return txtFilter.Text;
        }

        private static TextBox GetFilterTextbox(RadToolBar radToolBar)
        {
            
            RadToolBarItem filterButton = radToolBar.FindItemByValue("FilterText");
            TextBox txtFilter = filterButton.FindControl("txtFilter") as TextBox;
            return txtFilter;
        }



    }
    
    public class POSearchEventArgs : EventArgs
    {
        public POSearchEventArgs(string searchCriteria, string searchText, DateTime fromDate, DateTime toDate, string searchType)
        {
            SearchCriteria = searchCriteria;
            SearchText = searchText;
            FromDate = fromDate;
            ToDate = toDate;
            SearchType = searchType;
        }
        public string SearchCriteria { get; set; }
        public string SearchText { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string SearchType { get; set; }
    }
}