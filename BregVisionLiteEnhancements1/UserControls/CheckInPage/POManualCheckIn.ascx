﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="POManualCheckIn.ascx.cs"
  Inherits="BregVision.UserControls.CheckInPage.POManualCheckIn" %>
<div class="actions-bar flex-row">
  <bvu:SearchToolbar ID="ctlSearchToolbar" runat="server" />
  <div>
    <bvu:Help ID="Help1" runat="server" HelpID="103" />
    <asp:Button ID="btnManualCheckIn" runat="server" Enabled="false" OnClick="btnManualCheckIn_Click" Text="Check In Products" ToolTip="Check In all products selected." class="button-primary" />
  </div>
</div>
  <div>
    <asp:Label ID="lblManualCheckIn" runat="server" CssClass="WarningMsg"></asp:Label>
    <asp:Label ID="lblBrowseDispense" runat="server" CssClass="WarningMsg"></asp:Label>
  </div>
<bvu:VisionRadGridAjax ID="grdManualCheckIn" runat="server" AllowMultiRowSelection="True" OnLoad="grdManualCheckIn_OnLoad" OnNeedDataSource="grdManualCheckIn_NeedDataSource" AddNonBreakingSpace="false" Skin="Breg" EnableEmbeddedSkins="False">
  <MasterTableView>
    <Columns>
      <telerik:GridClientSelectColumn HeaderStyle-HorizontalAlign="Center" HeaderText=""
        ItemStyle-HorizontalAlign="Center" UniqueName="ClientSelectColumn" />
      <telerik:GridBoundColumn DataField="SupplierOrderID" Visible="False" UniqueName="SupplierOrderID" />
      <telerik:GridBoundColumn DataField="ProductInventoryID" Visible="False" UniqueName="ProductInventoryID" />
      <telerik:GridBoundColumn DataField="PracticeCatalogProductID" Visible="False" UniqueName="PracticeCatalogProductID" />
      <telerik:GridBoundColumn DataField="BrandShortName" HeaderText="Supplier" UniqueName="BrandShortName" />
      <telerik:GridBoundColumn DataField="ShortName" HeaderText="Name" UniqueName="ShortName" />
      <telerik:GridBoundColumn DataField="Code" HeaderText="Product Code" HeaderStyle-HorizontalAlign="Center"
        ItemStyle-HorizontalAlign="Center" UniqueName="Code" />
      <telerik:GridBoundColumn DataField="leftrightSide" HeaderText="Side" HeaderStyle-HorizontalAlign="Center"
        ItemStyle-HorizontalAlign="Center" UniqueName="Side" />
      <telerik:GridBoundColumn DataField="Size" HeaderText="Size" ItemStyle-HorizontalAlign="Center"
        HeaderStyle-HorizontalAlign="Center" UniqueName="Size" />
      <telerik:GridBoundColumn DataField="Packaging" HeaderText="Packaging" ItemStyle-HorizontalAlign="Center"
        HeaderStyle-HorizontalAlign="Center" UniqueName="Pkg." />
      <telerik:GridBoundColumn DataField="LeftRightSide" HeaderText="Side" ItemStyle-HorizontalAlign="Center"
        HeaderStyle-HorizontalAlign="Center" UniqueName="LeftRightSide" />
      <telerik:GridBoundColumn DataField="Color" HeaderText="Color" ItemStyle-HorizontalAlign="Center"
        HeaderStyle-HorizontalAlign="Center" UniqueName="Color" />
      <telerik:GridBoundColumn DataField="Gender" HeaderText="Gender" ItemStyle-HorizontalAlign="Center"
        HeaderStyle-HorizontalAlign="Center" UniqueName="Gender" />
      <telerik:GridBoundColumn DataField="WholesaleCost" DataFormatString="{0:C}" HeaderText="Cost"
        UniqueName="WholesaleCost" />
      <telerik:GridBoundColumn DataField="QuantityOnHandPerSystem" HeaderStyle-HorizontalAlign="Center"
        HeaderStyle-Width="50px" HeaderStyle-Wrap="true" HeaderText="On Hand" ItemStyle-HorizontalAlign="Center"
        UniqueName="QuantityOnHandPerSystem" />
      <telerik:GridTemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px"
        HeaderText="Qty. Checked In" ItemStyle-HorizontalAlign="Center" UniqueName="QuantityCheckedIn">
        <ItemTemplate>
          &nbsp;<telerik:RadNumericTextBox ID="QuantityCheckedIn" runat="server" Culture="English (United States)"
            LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" NumberFormat-DecimalDigits="0"
             ShowSpinButtons="False" Value="1" Width="50px">
          </telerik:RadNumericTextBox>
        </ItemTemplate>
      </telerik:GridTemplateColumn>
    </Columns>
  </MasterTableView>
</bvu:VisionRadGridAjax>
<br />
<div>
  <asp:TextBox ID="txtComments" runat="server" Columns="50" Rows="5" TextMode="MultiLine" Placeholder="Comments" CssClass="RadInputMgr_Breg RadInputMultiline_Breg"></asp:TextBox>
  <telerik:RadSpell ID="spell1" runat="server" ButtonCssClass="button-accent" ControlToCheck="txtComments"
    Skin="Breg" EnableEmbeddedSkins="False" SupportedLanguages="en-US,English,fr-FR,French,es-ES,Spanish" DialogsCssFile="~/App_Themes/Breg/RadSpell.Breg.css" />
</div>
