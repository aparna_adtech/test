﻿using System;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.Web.UI;
using ClassLibrary.DAL;
using System.Linq;
using BregVision.UserControls.CheckInPage;
using BregVision.TelerikAjax;

namespace BregVision.UserControls.CheckInPage
{
    public partial class CheckInCustomBrace : Bases.UserControlBase
    {
        #region Private Variables

        private CheckIn CheckInPage;

        #endregion

        #region Public Properties

        public RadGrid grdCustomBraceOutstandingRadGrid
		{
			get { return grdCustomBraceOutstanding; }
		}


        #endregion


        #region Page Events

        new protected void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);

            CheckInPage = this.Page as CheckIn;

            CheckInPage.ctlRecordsPerPageUserControl.recordsPerPageChangedEvent += new BregVision.UserControls.General.RecordsPerPageEventHandler(ctlRecordsPerPageUserControl_recordsPerPageChangedEvent);

        }



        void ctlRecordsPerPageUserControl_recordsPerPageChangedEvent(object sender, BregVision.UserControls.General.RecordsPerPageEventArgs e)
        {
            grdCustomBraceOutstanding.Rebind();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();

        }

        #endregion

        private void InitializeAjaxSettings()
        {
            Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);

            radAjaxManager.AjaxSettings.AddAjaxSetting(grdCustomBraceOutstanding, grdCustomBraceOutstanding, CheckInPage.RadAjaxLoadingPanel1Panel);

            radAjaxManager.AjaxSettings.AddAjaxSetting(btnCheckInCustomBrace, grdCustomBraceOutstanding, CheckInPage.RadAjaxLoadingPanel1Panel);

            radAjaxManager.AjaxSettings.AddAjaxSetting(CheckInPage.ctlRecordsPerPageUserControl.RecordsDisplayedTextBox, grdCustomBraceOutstanding, CheckInPage.RadAjaxLoadingPanel1Panel);
        }

        void ctlSearchToolbar_searchEvent(object sender, BregVision.UserControls.General.SearchEventArgs e)
        {
            grdCustomBraceOutstanding.SearchText = e.SearchText;
            grdCustomBraceOutstanding.SearchCriteria = e.SearchCriteria;
            grdCustomBraceOutstanding.SearchType = e.SearchType;
            grdCustomBraceOutstanding.SetSearchCriteria(e.SearchType);
            grdCustomBraceOutstanding.Enabled = true;
        }

        #region Grid Events

        protected void grdCustomBraceOutstanding_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (CheckInPage.rtsCheckInRadTabStrip.SelectedIndex == 3)
            {
                if (!e.IsFromDetailTable)
                {
                    DataSet ds = DAL.PracticeLocation.CheckIn.GetOustandingCustomBracesforCheckIn_ParentLevel(practiceLocationID);
                    grdCustomBraceOutstanding.DataSource = ds;

                    
                    grdCustomBraceOutstanding.SetRecordCount(ds, "Status");
                    //grdCustomBraceOutstanding.SetGridLabel("Search", Page.IsPostBack, lblBrowseCustomBrace);
                }
                grdCustomBraceOutstanding.MasterTableView.DetailTables[0].DataSource = DAL.PracticeLocation.CheckIn.GetOustandingCustomBracesforCheckIn_Detail1Level(practiceLocationID);
                grdCustomBraceOutstanding.MasterTableView.DetailTables[0].DetailTables[0].DataSource = DAL.PracticeLocation.CheckIn.GetOustandingCustomBracesforCheckIn_Detail2Level(practiceLocationID);

            }
            grdCustomBraceOutstanding.PageSize = recordsDisplayedPerPage;
        }
        protected void grdCustomBraceOutstanding_OnDetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {
            if (e.DetailTableView.Name == "mtvCustomBraceOutstanding_Detail1Level")
            {
                TelerikAjax.UIHelper.FilterOnJoinKey(e, "BregVisionOrderID", false);
            }
            else if (e.DetailTableView.Name == "mtvCustomBraceOutstanding_Detail2Level")
            {
                TelerikAjax.UIHelper.FilterOnJoinKey(e, "supplierOrderID", false);
            }
        }


        #endregion

        #region Control Events

        protected void btnCheckInCustomBrace_Click(object sender, EventArgs e)
        {
            string StatusText = "";
            StatusText = CheckInAndSaveItems(grdCustomBraceOutstanding.MasterTableView, true).ToString(); // check this logic
            StatusText = string.Concat(StatusText, " product(s) checked in.");
            lblBrowseCustomBrace.Text = StatusText;
            lblBrowseCustomBrace.CssClass = "SuccessMsg";
            ////Next line is so that grid will lose selection when rebinded
            grdCustomBraceOutstanding.EnableViewState = false;
            grdCustomBraceOutstanding.Rebind();
            grdCustomBraceOutstanding.EnableViewState = true;

        }

        protected void ShowReceiptButton_PreRender(object sender, EventArgs e)
        {
            var b = sender as Button;
            if (b != null)
            {
                if (string.IsNullOrEmpty(b.CommandArgument))
                    b.Visible = false;
                else
                    b.Attributes["onclick"] =
                        string.Format(
                            @"window.open('{0}?i={1}','','width=800,height=600,resizable=yes,scrollbars=yes,toolbar=no,location=no,status=no'); return false;",
                            BLL.Website.GetAbsPath("~/Authentication/CustomBraceReceipt.aspx"),
                            System.Web.HttpUtility.UrlEncode(b.CommandArgument));
            }
        }

        #endregion

        #region Methods

        public int CheckInAndSaveItems(GridTableView gridTableView, bool IsCheckIn)
        {
            int totalQuantityCheckedIn = 0;

            foreach (GridNestedViewItem nestedViewItem in gridTableView.GetItems(GridItemType.NestedView))
            {

                if (nestedViewItem.NestedTableViews.Length > 0)
                {

                    foreach (GridDataItem gridDataItem in nestedViewItem.NestedTableViews[0].Items)
                    {

                        Int32 BregVisionOrderID = 0;
                        bool bregVisionOrderIDFound = Int32.TryParse(gridDataItem["BregVisionOrderID"].Text, out BregVisionOrderID);

                        if (bregVisionOrderIDFound == true && IsCheckIn == true)
                        {
                            foreach (GridDataItem gridDataItem2 in nestedViewItem.NestedTableViews[0].ChildSelectedItems)
                            {

                                if (findSelectColumn(gridDataItem2))
                                {
                                    int quantityToCheckIn = 0;
                                    int quantityOrdered = 0;
                                    Int32 PracticeCatalogProductID = 0;
                                    Int32 ProductInventoryID = 0;
                                    Int32 SupplierOrderID = 0;
                                    Int32 SupplierOrderLineItemID = 0;
                                    decimal ActualWholesaleCost = 0;


                                    quantityOrdered = gridDataItem2.GetColumnInt32("QuantityOrdered");

                                    PracticeCatalogProductID = gridDataItem2.GetColumnInt32("PracticeCatalogProductID");

                                    ProductInventoryID = gridDataItem2.GetColumnInt32("ProductInventoryID");

                                    SupplierOrderID = gridDataItem2.GetColumnInt32("supplierOrderID");

                                    SupplierOrderLineItemID = gridDataItem2.GetColumnInt32("SupplierOrderLineItemID");

                                    //bool actualWholesaleCostFound = Decimal.TryParse(gridDataItem2["ActualWholesaleCost"].Text.Replace("$", ""), out ActualWholesaleCost);
                                    ActualWholesaleCost = gridDataItem2.GetColumnDecimal("ActualWholesaleCost");

                                    RadNumericTextBox tbQuantityToCheckIn = (RadNumericTextBox)gridDataItem2.FindControl("tbQuantityToCheckIn");
                                    if (tbQuantityToCheckIn != null)
                                    {
                                        bool quantityFound = Int32.TryParse(tbQuantityToCheckIn.Value.ToString(), out quantityToCheckIn);

                                        if (quantityOrdered > 0 && PracticeCatalogProductID > 0 && ProductInventoryID > 0 && SupplierOrderID > 0 && SupplierOrderLineItemID > 0 && quantityFound == true && quantityToCheckIn > 0)
                                        {
                                            DAL.PracticeLocation.CheckIn.POCheckIn_InsertUpdate(1, PracticeCatalogProductID, ProductInventoryID,
                                            BregVisionOrderID, SupplierOrderID, SupplierOrderLineItemID, ActualWholesaleCost, quantityToCheckIn);

                                            totalQuantityCheckedIn += quantityToCheckIn;
                                        }
                                    }
                                }

                            }
                            break;
                        }
                        else
                        {
                            //MWS - Get the Invoice Paid and Number  added Dec 31/2009
                            Int32 supplierOrderID = gridDataItem.GetColumnInt32("supplierOrderID");
                            bool invoicePaid = gridDataItem.GetColumnChecked("chkInvoicePaid");
                            string invoiceNumber = gridDataItem.GetColumnText("txtInvoiceNumber");

                            if (supplierOrderID > 0)
                            {
                                DAL.PracticeLocation.CheckIn.SaveInvoicePaymentInfo(supplierOrderID, invoicePaid, invoiceNumber);
                            }
                        }
                    }
                }
            }

            return totalQuantityCheckedIn;
        }


        private Boolean findSelectColumn(GridDataItem gridDataItem)
        {
            if (gridDataItem != null)
            {
                return gridDataItem.Selected;
            }
            else
            {
                return false;
            }
        }



        #endregion
    }
}