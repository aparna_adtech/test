﻿using System;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.Web.UI;
using ClassLibrary.DAL;
using System.Linq;
using BregVision.TelerikAjax;


namespace BregVision.UserControls.CheckInPage
{
    public partial class CheckInPoOutstanding : Bases.UserControlBase
    {
        private CheckIn CheckInPage; 

        public RadGrid grdPOOutstandingRadGrid
		{
			get { return grdPOOutstanding; }
		}

        new protected void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);

            CheckInPage = this.Page as CheckIn;

            CheckInPage.ctlRecordsPerPageUserControl.recordsPerPageChangedEvent += new BregVision.UserControls.General.RecordsPerPageEventHandler(ctlRecordsPerPageUserControl_recordsPerPageChangedEvent);

            
        }

        void ctlRecordsPerPageUserControl_recordsPerPageChangedEvent(object sender, BregVision.UserControls.General.RecordsPerPageEventArgs e)
        {
            grdPOOutstanding.Rebind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();
        }

        private void InitializeAjaxSettings()
        {
            Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);

            radAjaxManager.AjaxSettings.AddAjaxSetting(grdPOOutstanding, grdPOOutstanding, CheckInPage.RadAjaxLoadingPanel1Panel);
            radAjaxManager.AjaxSettings.AddAjaxSetting(grdPOOutstanding, lblBrowseCheckIn);

            radAjaxManager.AjaxSettings.AddAjaxSetting(btnOutStandingPOsSaveChangesInvoice, grdPOOutstanding, CheckInPage.RadAjaxLoadingPanel1Panel);
            radAjaxManager.AjaxSettings.AddAjaxSetting(btnOutStandingPOsSaveChangesInvoice, lblBrowseCheckIn);

            radAjaxManager.AjaxSettings.AddAjaxSetting(btnCheckInProducts, grdPOOutstanding, CheckInPage.RadAjaxLoadingPanel1Panel);
            radAjaxManager.AjaxSettings.AddAjaxSetting(btnCheckInProducts, lblBrowseCheckIn);

            radAjaxManager.AjaxSettings.AddAjaxSetting(CheckInPage.ctlRecordsPerPageUserControl.RecordsDisplayedTextBox, grdPOOutstanding, CheckInPage.RadAjaxLoadingPanel1Panel);
        }

        #region Grid Events

        protected void grdPOOutstanding_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (CheckInPage.rtsCheckInRadTabStrip.SelectedIndex == 0)
            {
                if (!e.IsFromDetailTable)
                {
                    grdPOOutstanding.DataSource = DAL.PracticeLocation.CheckIn.GetOustandingPOsforCheckIn_ParentLevel(practiceLocationID);
                    
                }
            }
            grdPOOutstanding.PageSize = recordsDisplayedPerPage;
        }

        protected void grdPOOutstanding_DetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            switch (e.DetailTableView.Name)
            {
                case "PurchaseOrder":
                    {
                        string BregVisionOrderID = dataItem.GetDataKeyValue("BregVisionOrderID").ToString();

                        e.DetailTableView.DataSource = DAL.PracticeLocation.CheckIn.GetOutstandingPOsforCheckIn_Detail1Level(Convert.ToInt32(BregVisionOrderID));
                        e.DetailTableView.PageSize = recordsDisplayedPerPage;
                        break;
                    }

                case "SupplierOrder":
                    {
                        string SupplierOrderID = dataItem["supplierOrderID"].Text;
                        e.DetailTableView.DataSource = DAL.PracticeLocation.CheckIn.GetOutstandingPOsforCheckIn_Detail2Level(Convert.ToInt32(SupplierOrderID), practiceLocationID);
                        e.DetailTableView.PageSize = recordsDisplayedPerPage;
                        break;
                    }
            }
        }

        protected void grdPOOutstanding_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                try
                {
                    //Sets max value of the Quantity to check in box
                    Int16 QuantityOrdered = 0;
                    Int16 QuantityCheckedIn = 0;
                    bool quantityOrderedFound = false;
                    bool quantityCheckedInFound = false;

                    GridColumn quantityOrderedColumn = e.Item.OwnerTableView.GetColumnSafe("QuantityOrdered");
                    if (quantityOrderedColumn != null)
                    {
                        quantityOrderedFound = Int16.TryParse(dataItem["QuantityOrdered"].Text, out QuantityOrdered);
                    }

                    GridColumn quantityCheckedInColumn = e.Item.OwnerTableView.GetColumnSafe("QuantityCheckedIn");
                    if (quantityCheckedInColumn != null)
                    {
                        quantityCheckedInFound = Int16.TryParse(dataItem["QuantityCheckedIn"].Text, out QuantityCheckedIn);
                    }

                    if (quantityOrderedFound && quantityCheckedInFound)
                    {
                        RadNumericTextBox tbQuantityToCheckIn = (RadNumericTextBox)dataItem.FindControl("tbQuantityToCheckIn");
                        if (tbQuantityToCheckIn != null)
                        {
                            double newMaxValue = QuantityOrdered - QuantityCheckedIn;
                            if (newMaxValue > 0)
                            {
                                tbQuantityToCheckIn.MaxValue = newMaxValue;
                            }
                            else
                            {
                                tbQuantityToCheckIn.MaxValue = 0.0;
                            }
                        }
                    }
                }
                catch
                {
                    return;
                }

                //Greys out and disables products that have already been checked in
                try
                {
                    Int32 supplierOrderLineItemStatusID = 0;
                    bool supplierOrderLineItemStatusIDFound = false;

                    GridColumn supplierOrderLineItemStatusColumn = e.Item.OwnerTableView.GetColumnSafe("SupplierOrderLineItemStatusID");
                    if (supplierOrderLineItemStatusColumn != null)
                    {
                        supplierOrderLineItemStatusIDFound = Int32.TryParse(dataItem["SupplierOrderLineItemStatusID"].Text, out supplierOrderLineItemStatusID);
                    }

                    if (supplierOrderLineItemStatusIDFound == true)
                    {
                        if (supplierOrderLineItemStatusID == 2)
                        {
                            dataItem.BackColor = System.Drawing.Color.WhiteSmoke;
                            dataItem.Enabled = false;
                        }
                    }

                }
                catch
                {
                    return;
                }
            }
        }


        protected void grdPOOutstanding_PreRender(object sender, EventArgs e)
        {

            grdPOOutstanding.ExpandGrid();
        }
        #endregion


        #region Control Events
        protected void btnOutStandingPOsSaveChangesInvoice_Click(object sender, EventArgs e)
        {
            string StatusText = "";
            StatusText = CheckInAndSaveItems(grdPOOutstanding.MasterTableView, false).ToString();
            StatusText = string.Concat(StatusText, " product(s) checked in.");
            lblBrowseCheckIn.Text = StatusText;
            lblBrowseCheckIn.CssClass = "SuccessMsg";
            //Next line is so that grid will lose selection when rebinded
            grdPOOutstanding.EnableViewState = false;
            grdPOOutstanding.Rebind();
            grdPOOutstanding.EnableViewState = true;
        }

        protected void btnCheckInProducts_Click(object sender, EventArgs e)
        {
            string StatusText = "";
            StatusText = CheckInAndSaveItems(grdPOOutstanding.MasterTableView, true).ToString();
            StatusText = string.Concat(StatusText, " product(s) checked in.");
            lblBrowseCheckIn.Text = StatusText;
            lblBrowseCheckIn.CssClass = "SuccessMsg";
            //Next line is so that grid will lose selection when rebinded
            grdPOOutstanding.EnableViewState = false;
            grdPOOutstanding.Rebind();
            grdPOOutstanding.EnableViewState = true;
            //since both grids hold state, we need to rebind both of them or any product visible in both will still show up
            CheckInPage.grdCustomBraceOutstandingRadGrid.EnableViewState = false;
            CheckInPage.grdCustomBraceOutstandingRadGrid.Rebind();
            CheckInPage.grdCustomBraceOutstandingRadGrid.EnableViewState = true;
        }

        #endregion

        #region Methods
        public int CheckInAndSaveItems(GridTableView gridTableView, bool IsCheckIn)
        {
            int totalQuantityCheckedIn = 0;

            foreach (GridNestedViewItem nestedViewItem in gridTableView.GetItems(GridItemType.NestedView))
            {

                if (nestedViewItem.NestedTableViews.Length > 0)
                {

                    foreach (GridDataItem gridDataItem in nestedViewItem.NestedTableViews[0].Items)
                    {

                        Int32 BregVisionOrderID = 0;
                        bool bregVisionOrderIDFound = Int32.TryParse(gridDataItem["BregVisionOrderID"].Text, out BregVisionOrderID);

                        if (bregVisionOrderIDFound == true && IsCheckIn == true)
                        {
                            foreach (GridDataItem gridDataItem2 in nestedViewItem.NestedTableViews[0].ChildSelectedItems)
                            {

                                if (findSelectColumn(gridDataItem2))
                                {
                                    int quantityToCheckIn = 0;
                                    int quantityOrdered = 0;
                                    Int32 PracticeCatalogProductID = 0;
                                    Int32 ProductInventoryID = 0;
                                    Int32 SupplierOrderID = 0;
                                    Int32 SupplierOrderLineItemID = 0;
                                    decimal ActualWholesaleCost = 0;


                                    quantityOrdered = gridDataItem2.GetColumnInt32("QuantityOrdered");

                                    PracticeCatalogProductID = gridDataItem2.GetColumnInt32("PracticeCatalogProductID");

                                    ProductInventoryID = gridDataItem2.GetColumnInt32("ProductInventoryID");

                                    SupplierOrderID = gridDataItem2.GetColumnInt32("supplierOrderID");

                                    SupplierOrderLineItemID = gridDataItem2.GetColumnInt32("SupplierOrderLineItemID");

                                    //bool actualWholesaleCostFound = Decimal.TryParse(gridDataItem2["ActualWholesaleCost"].Text.Replace("$", ""), out ActualWholesaleCost);
                                    ActualWholesaleCost = gridDataItem2.GetColumnDecimal("ActualWholesaleCost");

                                    RadNumericTextBox tbQuantityToCheckIn = (RadNumericTextBox)gridDataItem2.FindControl("tbQuantityToCheckIn");
                                    if (tbQuantityToCheckIn != null)
                                    {
                                        bool quantityFound = Int32.TryParse(tbQuantityToCheckIn.Value.ToString(), out quantityToCheckIn);

                                        if (quantityOrdered > 0 && PracticeCatalogProductID > 0 && ProductInventoryID > 0 && SupplierOrderID > 0 && SupplierOrderLineItemID > 0 && quantityFound == true && quantityToCheckIn > 0)
                                        {
                                            DAL.PracticeLocation.CheckIn.POCheckIn_InsertUpdate(1, PracticeCatalogProductID, ProductInventoryID,
                                            BregVisionOrderID, SupplierOrderID, SupplierOrderLineItemID, ActualWholesaleCost, quantityToCheckIn);

                                            totalQuantityCheckedIn += quantityToCheckIn;
                                        }
                                    }
                                }

                            }
                            break;
                        }
                        else
                        {
                            //MWS - Get the Invoice Paid and Number  added Dec 31/2009
                            Int32 supplierOrderID = gridDataItem.GetColumnInt32("supplierOrderID");
                            bool invoicePaid = gridDataItem.GetColumnChecked("chkInvoicePaid");
                            string invoiceNumber = gridDataItem.GetColumnText("txtInvoiceNumber");

                            if (supplierOrderID > 0)
                            {
                                DAL.PracticeLocation.CheckIn.SaveInvoicePaymentInfo(supplierOrderID, invoicePaid, invoiceNumber);
                            }
                        }
                    }
                }
            }

            return totalQuantityCheckedIn;
        }

        private Boolean findSelectColumn(GridDataItem gridDataItem)
        {
            if (gridDataItem != null)
            {
                return gridDataItem.Selected;
            }
            else
            {
                return false;
            }
        }

        #endregion
    }
}