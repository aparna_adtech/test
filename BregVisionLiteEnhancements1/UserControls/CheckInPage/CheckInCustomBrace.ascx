﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CheckInCustomBrace.ascx.cs"
  Inherits="BregVision.UserControls.CheckInPage.CheckInCustomBrace" %>
<div class="actions-bar flex-row">
  <div>
    <bvu:Help ID="Help1" runat="server" HelpID="103" />
    <asp:Button ID="btnCheckInCustomBrace" runat="server" Enabled="true" OnClick="btnCheckInCustomBrace_Click" Text="Check In Custom Brace" ToolTip="Check-In all custom brace orders selected." CssClass="button-primary" />
  </div>
</div>  
<div>
  <asp:Label ID="lblBrowseCustomBrace" runat="server" CssClass="WarningMsg"></asp:Label>
</div>
<bvu:VisionRadGridAjax ID="grdCustomBraceOutstanding" runat="server" OnNeedDataSource="grdCustomBraceOutstanding_NeedDataSource"
  OnDetailTableDataBind="grdCustomBraceOutstanding_OnDetailTableDataBind" Skin="Breg" EnableEmbeddedSkins="False"
  AddNonBreakingSpace="false">
  <MasterTableView DataKeyNames="BregVisionOrderID" Name="mtvCustomBraceOutstanding_ParentLevel"
    ItemStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center"
    HeaderStyle-HorizontalAlign="Center">
    <Columns>
      <telerik:GridBoundColumn DataField="PurchaseOrder" HeaderText="Purchase Order" UniqueName="PurchaseOrder" />
      <telerik:GridBoundColumn DataField="CustomPurchaseOrderCode" HeaderText="Custom PO Number"
        UniqueName="CustomPurchaseOrderCode" />
      <telerik:GridBoundColumn DataField="PatientName" HeaderText="Patient ID" UniqueName="PatientName" />
      <telerik:GridBoundColumn DataField="Status" HeaderText="Status" UniqueName="Status" />
      <telerik:GridBoundColumn DataField="ShippingType" HeaderText="Shipping Type" UniqueName="ShippingType" />
      <telerik:GridBoundColumn DataField="Total" DataFormatString="{0:C}" HeaderText="Total"
        UniqueName="Total" />
      <telerik:GridBoundColumn DataField="CreatedDate" HeaderText="Date Created" UniqueName="CreatedDate" />
      <telerik:GridTemplateColumn HeaderText="Receipt" UniqueName="Receipt">
        <ItemTemplate>
          <asp:Button ID="ShowReceiptButton" runat="server" Text="View" CommandArgument='<%# Eval("BregVisionOrderID") %>'
            OnPreRender="ShowReceiptButton_PreRender"></asp:Button>
        </ItemTemplate>
      </telerik:GridTemplateColumn>
      <telerik:GridBoundColumn DataField="BregVisionOrderID" UniqueName="BregVisionOrderID"
        Visible="False" />
    </Columns>
    <DetailTables>
      <telerik:GridTableView runat="server" DataKeyNames="SupplierOrderID" Name="mtvCustomBraceOutstanding_Detail1Level">
        <ParentTableRelation>
          <telerik:GridRelationFields DetailKeyField="BregVisionOrderID" MasterKeyField="BregVisionOrderID" />
        </ParentTableRelation>
        <Columns>
          <telerik:GridBoundColumn DataField="SupplierShortName" HeaderText="Supplier" UniqueName="SupplierShortName" />
          <telerik:GridBoundColumn DataField="BrandShortName" HeaderText="Brand" UniqueName="BrandShortName" />
          <telerik:GridBoundColumn DataField="PurchaseOrder" HeaderText="PO Number" UniqueName="PurchaseOrder" />
          <telerik:GridBoundColumn DataField="Status" HeaderText="Status" UniqueName="Status" />
          <telerik:GridBoundColumn DataField="Total" DataFormatString="{0:C}" HeaderText="Total"
            UniqueName="Total" />
          <telerik:GridBoundColumn DataField="SupplierOrderID" UniqueName="SupplierOrderID"
            Visible="False" />
          <telerik:GridBoundColumn DataField="BregVisionOrderID" UniqueName="BregVisionOrderID"
            Visible="False" />
        </Columns>
        <DetailTables>
          <telerik:GridTableView runat="server" DataKeyNames="SupplierOrderID" Name="mtvCustomBraceOutstanding_Detail2Level">
            <ParentTableRelation>
              <telerik:GridRelationFields DetailKeyField="SupplierOrderID" MasterKeyField="SupplierOrderID" />
            </ParentTableRelation>
            <Columns>
              <telerik:GridClientSelectColumn HeaderText="" UniqueName="ClientSelectColumn" HeaderStyle-HorizontalAlign="Center"
                ItemStyle-HorizontalAlign="Center" />
              <telerik:GridBoundColumn DataField="ShortName" HeaderText="Product" UniqueName="ShortName" />
              <telerik:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code" />
              <telerik:GridBoundColumn DataField="LeftRightSide" HeaderText="Side" UniqueName="Side" />
              <telerik:GridBoundColumn DataField="Size" HeaderText="Size" UniqueName="Size" />
              <telerik:GridBoundColumn DataField="Status" HeaderText="Status" UniqueName="Status" />
              <telerik:GridBoundColumn DataField="Packaging" HeaderText="Pkg" UniqueName="Packaging" />
              <telerik:GridBoundColumn DataField="Color" HeaderText="Color" UniqueName="Color" />
              <telerik:GridBoundColumn DataField="Gender" HeaderText="Gender" UniqueName="Gender" />
              <telerik:GridBoundColumn DataField="ActualWholesaleCost" DataFormatString="{0:C}"
                HeaderText="AWC" UniqueName="ActualWholesaleCost" />
              <telerik:GridBoundColumn DataField="LineTotal" DataFormatString="{0:C}" HeaderText="Total"
                UniqueName="LineTotal" />
              <telerik:GridBoundColumn DataField="QuantityOrdered" HeaderStyle-HorizontalAlign="Center"
                HeaderStyle-Width="50px" HeaderStyle-Wrap="true" HeaderText="Qty. Ordered" ItemStyle-HorizontalAlign="Center"
                UniqueName="QuantityOrdered" />
              <telerik:GridBoundColumn DataField="QuantityCheckedIn" HeaderStyle-HorizontalAlign="Center"
                HeaderStyle-Width="50px" HeaderStyle-Wrap="true" HeaderText="Qty. Checked In"
                ItemStyle-HorizontalAlign="Center" UniqueName="QuantityCheckedIn" />
              <telerik:GridTemplateColumn DataField="QuantityToCheckIn" HeaderStyle-HorizontalAlign="Center"
                HeaderStyle-Width="50px" HeaderStyle-Wrap="true" HeaderText="Qty. To Check In"
                ItemStyle-HorizontalAlign="Center" UniqueName="QuantityToCheckIn">
                <ItemTemplate>&nbsp;<telerik:RadNumericTextBox ID="tbQuantityToCheckIn" runat="server" Culture="English (United States)"
                  LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" NumberFormat-DecimalDigits="0"
                   ShowSpinButtons="False" Text='<%# Eval("QuantityToCheckIn") %>' Width="41px"></telerik:RadNumericTextBox>
                </ItemTemplate>
              </telerik:GridTemplateColumn>
              <telerik:GridBoundColumn DataField="SupplierOrderID" UniqueName="SupplierOrderID"
                Visible="False" />
              <telerik:GridBoundColumn DataField="SupplierOrderLineItemID" Visible="False" UniqueName="SupplierOrderLineItemID" />
              <telerik:GridBoundColumn DataField="ProductInventoryID" Visible="False" UniqueName="ProductInventoryID" />
              <telerik:GridBoundColumn DataField="PracticeCatalogProductID" Visible="False" UniqueName="PracticeCatalogProductID" />
              <telerik:GridBoundColumn DataField="SupplierOrderLineItemStatusID" Visible="False"
                UniqueName="SupplierOrderLineItemStatusID" />
            </Columns>
          </telerik:GridTableView>
        </DetailTables>
      </telerik:GridTableView>
    </DetailTables>
  </MasterTableView>
</bvu:VisionRadGridAjax>
