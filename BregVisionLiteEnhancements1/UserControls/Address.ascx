<%@ Control Language="C#" AutoEventWireup="true" Inherits="Address" CodeBehind="Address.ascx.cs" %>
<div class="admin-form">
  <div>
    <div>
      <asp:Label ID="lblAttentionOf" runat="server" CssClass="FieldLabel" Text="Attention of"></asp:Label>
      <asp:TextBox ID="txtAttentionOf" runat="server" MaxLength="50" CssClass="FieldLabel"></asp:TextBox>
    </div>
  </div>
  <div>
    <div>
      <asp:Label ID="lblAddressLine1" runat="server" CssClass="FieldLabel" Text="Address Line 1"></asp:Label>
      <asp:TextBox ID="txtAddressLine1" runat="server" MaxLength="50" CssClass="FieldLabel"></asp:TextBox>
      <asp:RequiredFieldValidator ID="rfvAddressLine1" runat="server" CssClass="WarningMsg" ControlToValidate="txtAddressLine1" ErrorMessage="Address Line 1 is a required field" ToolTip="Address Line 1 is a required field" Display="Dynamic" ValidationGroup="Address">* Required</asp:RequiredFieldValidator>
    </div>
  </div>
  <div>
    <div>
      <asp:Label ID="lblAddressLine2" runat="server" CssClass="FieldLabel" Text="Address Line 2"></asp:Label>
      <asp:TextBox ID="txtAddressLine2" runat="server" MaxLength="50" CssClass="FieldLabel"></asp:TextBox>
    </div>
  </div>
  <div>
    <div>
      <asp:Label ID="lblCity" runat="server" CssClass="FieldLabel" Text="City"></asp:Label>
      <asp:TextBox ID="txtCity" runat="server" MaxLength="50" CssClass="FieldLabel"></asp:TextBox>
      <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="txtCity" ErrorMessage="City is a required field" CssClass="WarningMsg" ToolTip="City is a required field" Display="Dynamic" ValidationGroup="Address" >* Required</asp:RequiredFieldValidator>
    </div>
  </div>
  <div>
    <div>
      <asp:Label ID="lblState" runat="server" CssClass="FieldLabel" Text="State"></asp:Label>
      <div class="FieldLabelSelect-Container">
        <asp:DropDownList ID="cmbState" runat="server" CssClass="FieldLabelSelect" DataTextField="Name" DataValueField="StateID"></asp:DropDownList>
      </div>
      <asp:RequiredFieldValidator ID="rfvState" runat="server" ControlToValidate="cmbState" CssClass="WarningMsg" ErrorMessage="State is a required field" ToolTip="State is a required field" Display="Dynamic" ValidationGroup="Address" >* Required</asp:RequiredFieldValidator>
    </div>
  </div>
  <div>
    <div>
      <asp:Label ID="lblZipCode" runat="server" CssClass="FieldLabel" Text="Zip Code"></asp:Label>
      <div class="flex-row">
        <asp:TextBox ID="txtZipCode" runat="server" MaxLength="5" CssClass="FieldLabel"></asp:TextBox>
        <span>&nbsp;-&nbsp;</span>
        <asp:TextBox ID="txtZipCodePlus4" runat="server" MaxLength="4" CssClass="FieldLabel"></asp:TextBox>
      </div>
      <asp:RequiredFieldValidator ID="rfvZipCode" runat="server" ControlToValidate="txtZipCode" ErrorMessage="ZipCode is a required field" CssClass="WarningMsg" ToolTip="* ZipCode is a required field" Display="Dynamic" ValidationGroup="Address" >* Required</asp:RequiredFieldValidator>
      <asp:RegularExpressionValidator ID="revZipCode" runat="server" ControlToValidate="txtZipCode" CssClass="WarningMsg" ErrorMessage="ZipCode must be 5 digits" ToolTip="ZipCode must be 5 digits" ValidationExpression="\d{5}" Display="Dynamic" ValidationGroup="Address" >* Fix: 5 digits</asp:RegularExpressionValidator>
       <asp:RegularExpressionValidator ID="revZipCodePlus4" runat="server" ControlToValidate="txtZipCodePlus4" ErrorMessage="ZipCodePlus4 must be 4 digits or empty" CssClass="WarningMsg" ToolTip="ZipCodePlus4 must be 4 digits" ValidationExpression="(\d{4})?" Display="Dynamic" ValidationGroup="Address" >* Fix: 4 digits</asp:RegularExpressionValidator>
    </div>
  </div>
</div>
