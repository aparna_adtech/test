<%@ Control Language="C#" AutoEventWireup="true"
  Inherits="UserControls_ResetSaveButtonTable" CodeBehind="ResetSaveButtonTable.ascx.cs" %>

<div class="flex-row align-center justify-end reset-save">

  <asp:Button ID="btnReset" CausesValidation="false" runat="server" Text="Reset" OnClick="btnReset_Click"
    ToolTip="Reset data and Undo any unsaved modifications" CssClass="button-accent" />&nbsp;

  <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click"
    ToolTip="Validate data, Save if valid" CssClass="button-primary"  ValidationGroup="Address" />

</div>
