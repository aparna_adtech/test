﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BregVision.UserControls.Help
{
    public partial class Help : System.Web.UI.UserControl
    {
        public String HelpID { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            imgHelp.Attributes.Add("onclick", string.Format("SelectHelpContext({0});showToolTip(this, {0});", HelpID));
            //imgHelp.Attributes.Add("onmouseleave", "stopCount();");
        }
    }
}