﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Reporting.WebForms;

namespace BregVision.UserControls
{
    public class BregVisionSSRSReportViewer : Microsoft.Reporting.WebForms.ReportViewer
    {
        public BregVisionSSRSReportViewer()
            : base()
        {
        }

        public bool ConfigureReportViewer(string report_path)
        {
            // hide report viewer if no report path provided
            if (string.IsNullOrEmpty(report_path))
            {
                this.Visible = false;
                this.Enabled = false;
                return false;
            }
            else
            {
                this.Visible = true;
                this.Enabled = true;
            }

            // set report viewer credentials
            SetReportViewerCredentials(this.ServerReport);

            // set report to remote processing mode
            this.ProcessingMode = ProcessingMode.Remote;

            // set report server url
            // NOTE: if report server url is not provided in web.config, this
            // provides a graceful fallback to previous report server URL
            // implementation.
            {
                var u = System.Configuration.ConfigurationManager.AppSettings["ReportServerUrl"];
                if (string.IsNullOrEmpty(u))
                {
                    this.ServerReport.ReportServerUrl =
                        new Uri(string.Format("http://{0}/reportserver",
                            System.Environment.MachineName));  //This is used for Prod.
                }
                else
                {
                    this.ServerReport.ReportServerUrl = new Uri(u);
                }
            }

            // set report path
            this.ServerReport.ReportPath = report_path;

            // set zoom
            //ReportViewer1.ZoomMode = ZoomMode.PageWidth;
            this.ZoomMode = ZoomMode.Percent;
            this.ZoomPercent = 100;

            // return a success value
            return true;
        }

        #region report authentication implementation

        [Serializable]
        private class MyReportCredentials : IReportServerCredentials
        {
            /// <summary>
            /// This class does not provide forms credential functionality.
            /// Null values are returned for each output parameter to indicate
            /// that no forms credentials have been provided.
            /// </summary>
            /// <param name="authCookie"></param>
            /// <param name="userName"></param>
            /// <param name="password"></param>
            /// <param name="authority"></param>
            /// <returns></returns>
            public bool GetFormsCredentials(out System.Net.Cookie authCookie, out string userName, out string password, out string authority)
            {
                authCookie = null;
                userName = null;
                password = null;
                authority = null;
                return false;
            }

            /// <summary>
            /// This class does not provide impersonation functionality.
            /// Null is returned to indicate that impersonation is not to be used.
            /// </summary>
            public System.Security.Principal.WindowsIdentity ImpersonationUser
            {
                get { return null; }
            }

            /// <summary>
            /// A System.Net.NetworkCredential object is returned if AppSettings
            /// are present for ReportServerUser. Null is returned otherwise.
            /// </summary>
            public System.Net.ICredentials NetworkCredentials
            {
                get
                {
                    var u = System.Configuration.ConfigurationManager.AppSettings["ReportServerUser"] ?? string.Empty;
                    var p = System.Configuration.ConfigurationManager.AppSettings["ReportServerPassword"] ?? string.Empty;
                    if (string.IsNullOrEmpty(u))
                        return null;
                    else
                        return new System.Net.NetworkCredential(u, p);
                }
            }
        }

        private static void SetReportViewerCredentials(ServerReport sr)
        {
            if (sr != null)
            {
                // NOTE: test for null network credential (in case web.config
                // does not contain report server auth info). This handles
                // the fallback compatibility with the older authentication
                // method implementation.
                var c = new MyReportCredentials();
                if (c.NetworkCredentials != null)
                    sr.ReportServerCredentials = c;
            }
        }

        #endregion
    }
}