﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchToolbar.ascx.cs"
  Inherits="BregVision.UserControls.General.SearchToolbar" %>
<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
  <script language="javascript" type="text/javascript">
    function SetPressEnterTarget(toolBarClientID) {
      if (event.which || event.keyCode) {
        if ((event.which == 13) || (event.keyCode == 13)) {
          var toolBar = $find(toolBarClientID);
          if (toolBar != null) {
            var searchButton = toolBar.findItemByValue('search');
            if (searchButton != null) {
              searchButton.click()
            }
          }
          return false;
        }
      }
      else {
        return true;
      }
    }
    function stopRKey(evt) {
      var evt = (evt) ? evt : ((event) ? event : null);
      var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
      if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
    }

    document.onkeypress = stopRKey;
  </script>
</telerik:RadScriptBlock>
<div id="searchToolBarBlock" runat="server">
  <telerik:RadToolBar ID="rtbToolbar" runat="server" AutoPostBack="True" EnableViewState="true"
    OnButtonClick="rtbToolbar_OnClick" EnableEmbeddedSkins="False" Skin="Breg" style="z-index: 3000;">
    <Items>
      <telerik:RadToolBarButton ID="rttbFilter" runat="server" CommandName="FilterText" Value="FilterText">
        <ItemTemplate>
          <asp:TextBox ID="txtFilter" runat="server" TabIndex="2"></asp:TextBox>
        </ItemTemplate>
      </telerik:RadToolBarButton>
      <telerik:RadToolBarButton runat="server" EnableViewState="true" Value="FilterCriteriaButton" CommandName="FilterCriteriaButton">
        <ItemTemplate>
          <telerik:RadComboBox ID="rtbComboBox" runat="server" CommandName="FilterCriteria" Value="FilterCriteria" EnableViewState="true" Skin="Breg" EnableEmbeddedSkins="False">
            <Items>
              <telerik:RadComboBoxItem runat="server" Text="Product Code" Value="Code" CheckOnClick="true"
                PostBack="false" Selected="true" EnableViewState="true" />
              <telerik:RadComboBoxItem runat="server" Text="Product Name" Value="Name" CheckOnClick="true"
                PostBack="false" EnableViewState="true" />
              <telerik:RadComboBoxItem runat="server" Text="Manufacturer" Value="Brand" CheckOnClick="true"
                PostBack="false" EnableViewState="true" />
              <telerik:RadComboBoxItem runat="server" Text="Category" Value="Category" CheckOnClick="true"
                PostBack="false" EnableViewState="true" />
              <telerik:RadComboBoxItem runat="server" Text="HCPCs" Value="HCPCs" CheckOnClick="true"
                PostBack="false" EnableViewState="true" />
            </Items>
          </telerik:RadComboBox>
        </ItemTemplate>
      </telerik:RadToolBarButton>
      <telerik:RadToolBarButton ID="rtbFilter" runat="server" CommandName="search" Value="search"
        ImageUrl="~/App_Themes/Breg/images/Common/search.png" ToolTip="Search" CssClass="button-primary-container" />
      <telerik:RadToolBarButton ID="rtbBrowse" runat="server" CommandName="browse" Value="browse"
        ImageUrl="~/App_Themes/Breg/images/Common/browse.png" ToolTip="Browse" CssClass="button-primary-container" />
    </Items>
  </telerik:RadToolBar>
</div>
