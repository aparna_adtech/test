﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace BregVision.UserControls.General
{
  public delegate void SearchEventHandler(object sender, SearchEventArgs e);


  public partial class SearchToolbar : Bases.UserControlBase
  {

    public event SearchEventHandler searchEvent;

    private RadGrid _Grid;
    public RadGrid Grid
    {
      get
      {
        return _Grid;
      }
      set { _Grid = value; }
    }

    public RadToolBar Toolbar
    {
      get
      {
        return rtbToolbar;
      }
    }

    public string Style { get; set; }

    public bool DropDownHidden { get; set; }

    public string TextboxPlaceholderText = "Search for Products";

    public RadAjaxLoadingPanel DefaultLoadingPanel { get; set; }

    public void HideBrowse()
    {
      rtbBrowse.Visible = false;
      rtbToolbar.Width = Unit.Parse((rtbToolbar.Width.Value - 50).ToString());
    }


    protected void Page_Load(object sender, EventArgs e)
    {
      var textbox = (TextBox) rttbFilter.FindControl("txtFilter");
      textbox.Attributes.Add("placeholder", TextboxPlaceholderText);
      
      PassCSSPropertiesToDiv();

      if (Grid != null)
      {
        Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);
        if (radAjaxManager != null)
        {
          radAjaxManager.AjaxSettings.AddAjaxSetting(rtbToolbar, Grid, DefaultLoadingPanel);
        }
      }
    }

    private void PassCSSPropertiesToDiv()
    {
      if (!string.IsNullOrWhiteSpace(this.Style))
      {
        string[] styleProperties = this.Style.Split(';');
        foreach (string styleProperty in styleProperties)
        {
          string[] property = styleProperty.Split(':');
          string key = property[0];
          string value = property[1];
          searchToolBarBlock.Style.Add(key, value);
        }
      }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
      SetPressEnterTarget();
      HideUnusedItems();

      SelectDefaultDispenseSearchCriteria();
    }

    private void SelectDefaultDispenseSearchCriteria()
    {
      if (this.Page is BregVision.Dispense && Session["DefaultDispenseSearchCriteria"] != null && Page.IsPostBack == false)
      {
        RadToolBarItem button = FindToolbarButton(Session.GetString("DefaultDispenseSearchCriteria"));

        RadComboBox filterCriteria = FindDropDownList();

        //if (button != null && filterCriteria != null)
        //{
        //    filterCriteria.EnableDefaultButton = true;
        //    filterCriteria.DefaultButtonIndex = button.Index;                   
        //}
      }
    }
    //  <Buttons>
    //    <telerik:RadToolBarButton runat="server" Text="Product Code" Value="Code" CheckOnClick="true" PostBack="false" Checked="true" EnableViewState="true" />
    //    <telerik:RadToolBarButton runat="server" Text="Product Name" Value="Name" CheckOnClick="true" PostBack="false" EnableViewState="true" />
    //    <telerik:RadToolBarButton runat="server" Text="Manufacturer" Value="Brand" CheckOnClick="true" PostBack="false" EnableViewState="true" />
    //    <telerik:RadToolBarButton runat="server" Text="Category" Value="Category" CheckOnClick="true" PostBack="false" EnableViewState="true" />
    //    <telerik:RadToolBarButton runat="server" Text="HCPCs" Value="HCPCs" CheckOnClick="true" PostBack="false" EnableViewState="true" />
    //    <telerik:RadToolBarButton runat="server" Text="Patient Code" Value="PatientID" CheckOnClick="true" PostBack="false" EnableViewState="true" />
    //    <telerik:RadToolBarButton runat="server" Text="PO Number" Value="PONumber" CheckOnClick="true" PostBack="false" EnableViewState="true" />
    //    <telerik:RadToolBarButton runat="server" Text="Physician Name" Value="PhysicianName" CheckOnClick="true" PostBack="false" EnableViewState="true" />
    //    <telerik:RadToolBarButton runat="server" Text="Patient Code" Value="PatientCode" CheckOnClick="true" PostBack="false" EnableViewState="true" />
    //    <telerik:RadToolBarButton runat="server" Text="Date Dispensed" Value="DispenseDate" CheckOnClick="true" PostBack="false" EnableViewState="true" />
    //</Buttons>

    //Dispense Modifications
    //<radC:RadComboBoxItem ID="RadComboBoxItem5" runat="server" Text="Product Code" Value="ProductCode" />
    //<radC:RadComboBoxItem ID="RadComboBoxItem3" runat="server" Text="Patient Code" Value="PatientCode" />
    //<radC:RadComboBoxItem ID="RadComboBoxItem2" runat="server" Text="Physician Name" Value="PhysicianName" />
    //<radC:RadComboBoxItem ID="RadComboBoxItem4" runat="server" Text="Date Dispensed" Value="DispenseDate" />


    public void ClearDropdownButtons()
    {
      RadComboBox filterCriteria = FindDropDownList();
      filterCriteria.Items.Clear();
    }



    public void AddItem(string text, string value)
    {
      Telerik.Web.UI.RadComboBoxItem newItem = new RadComboBoxItem();
      newItem.Text = text;
      newItem.Value = value;
      //newItem.CheckOnClick = true;
      //newItem.PostBack = false;
      newItem.EnableViewState = true;

      RadComboBox filterCriteria = FindDropDownList();
      filterCriteria.Items.Add(newItem);
    }


    private void HideUnusedItems()
    {
      if (BLL.Website.IsVisionExpress())
      {
        RemoveItem("Brand");
        RemoveItem("HCPCs");
        RemoveItem("Supplier");
        RemoveItem("Category");
      }

      if (DropDownHidden == true)
      {
        RadComboBox filterCriteria = FindDropDownList();
        if (filterCriteria != null)
        {
          filterCriteria.Visible = false;
        }
      }
    }

    private void RemoveItem(string itemValue)
    {
      RadComboBoxItem item = FindComboBoxItem(itemValue);

      RadComboBox filterCriteria = FindDropDownList();

      if (item != null && filterCriteria != null)
      {
        filterCriteria.Items.Remove(item);
      }
    }

    private RadComboBox FindDropDownList()
    {

      RadToolBarButton filterCriteriaToolBarButton = (RadToolBarButton) rtbToolbar.FindItemByValue("FilterCriteriaButton");

      RadComboBox filterCriteriaItem = (RadComboBox)filterCriteriaToolBarButton.FindControl("rtbComboBox");

      if (filterCriteriaItem is RadComboBox)
      {
        return filterCriteriaItem as RadComboBox;
      }
      return null;
    }

    private RadComboBoxItem FindComboBoxItem(string itemValue)
    {
      RadComboBox filterCriteriaItem = FindDropDownList();
      RadComboBoxItem item = filterCriteriaItem.FindItemByValue(itemValue);

      return item;
    }

    private RadToolBarButton FindToolbarButton(string buttonValue)
    {
      RadToolBarButton button = (RadToolBarButton) rtbToolbar.FindItemByValue(buttonValue);
      return button;
    }

    private void SetPressEnterTarget()
    {
      TelerikAjax.UIHelper.SetEnterPressTarget(GetFilterTextbox(rtbToolbar), rtbFilter);
    }

    protected void rtbToolbar_OnClick(object sender, RadToolBarEventArgs e)
    {
      RadToolBarButton button = e.Item as RadToolBarButton;
      switch (button.CommandName.ToLower())
      {
        case "search":
          ProcessSearch(e);
          break;
        case "browse":
          ProcessBrowse(e);
          break;
      }
    }

    private void ProcessSearch(RadToolBarEventArgs e)
    {
      //find search text
      string searchText = GetSearchText(e).Trim();

      //FindSearchCriteria
      string searchCriteria = GetSearchCriteria(e);

      string searchType = "Search";


      if (!string.IsNullOrEmpty(searchCriteria) && !string.IsNullOrEmpty(searchText) && searchEvent != null)
      {
        SearchEventArgs searchEventArgs = new SearchEventArgs(searchCriteria, searchText, searchType);
        searchEvent(this, searchEventArgs);
      }
    }

    private void ProcessBrowse(RadToolBarEventArgs e)
    {
      ClearSearchText(e);

      string searchCriteria = "Browse";

      string searchType = "Browse";

      if (searchEvent != null)
      {
        SearchEventArgs searchEventArgs = new SearchEventArgs(searchCriteria, "", searchType);
        searchEvent(this, searchEventArgs);
      }
    }

    private string GetSearchCriteria(RadToolBarEventArgs e)
    {
      
      RadComboBoxItem selectedItem = GetCriteriaItem(e.Item.ToolBar);

      if (selectedItem != null)
      {
        return selectedItem.Value;
      }
      return string.Empty;
    }

    private static RadComboBoxItem GetCriteriaItem(RadToolBar radToolBar)
    {
      RadToolBarButton filterCriteriaItem = (RadToolBarButton) radToolBar.FindButtonByCommandName("FilterCriteriaButton");

      if (filterCriteriaItem is RadToolBarButton)
      {
        RadComboBox filterCriteria = filterCriteriaItem.FindControl("rtbComboBox") as RadComboBox;
        foreach (RadComboBoxItem item in filterCriteria.Items)
        {
          if (item.Selected == true)
          {
            return item;
          }
        }
        if (filterCriteria.Items.Count > 0)
        {
          return filterCriteria.Items[0];
        }
      }
      return null;
    }

    private static void ClearSearchText(RadToolBarEventArgs e)
    {
      TextBox txtFilter = GetFilterTextbox(e.Item.ToolBar);
      txtFilter.Text = "";
    }

    private static string GetSearchText(RadToolBarEventArgs e)
    {
      TextBox txtFilter = GetFilterTextbox(e.Item.ToolBar);
      return txtFilter.Text;
    }

    private static TextBox GetFilterTextbox(RadToolBar radToolBar)
    {
      RadToolBarItem filterButton = radToolBar.FindItemByValue("FilterText");
      TextBox txtFilter = filterButton.FindControl("txtFilter") as TextBox;
      return txtFilter;
    }


  }



  public class SearchEventArgs : EventArgs
  {
    public SearchEventArgs(string searchCriteria, string searchText, string searchType)
    {
      SearchCriteria = searchCriteria;
      SearchText = searchText;
      SearchType = searchType;
    }
    public string SearchCriteria { get; set; }
    public string SearchText { get; set; }
    public string SearchType { get; set; }
  }
}