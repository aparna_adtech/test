﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RecordsPerPage.ascx.cs" Inherits="BregVision.UserControls.General.RecordsPerPage" %>

<div class="flex-row align-center justify-center">
  <telerik:RadNumericTextBox ID="txtRecordsDisplayed" runat="server" ShowSpinButtons="false" Culture="English (United States)"
    MaxValue="100" MinValue="1" NumberFormat-DecimalDigits="0" OnTextChanged="txtRecordsDisplayed_TextChanged" AutoPostBack="true" Label="Records per Page" CssClass="records-per-page" Width="145px" />
  <bvu:Help ID="Help2" runat="server" HelpID="86" />
</div>