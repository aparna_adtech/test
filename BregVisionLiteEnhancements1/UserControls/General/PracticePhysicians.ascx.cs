﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace BregVision.UserControls.General
{
    public delegate void PhysicianEventHandler(object sender, PhysicianEventArgs e);

    public partial class PracticePhysicians : Bases.UserControlBase
    {
        public event PhysicianEventHandler PhysicianEvent;

        /// <summary>
        /// cboPhysicians control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        public global::System.Web.UI.WebControls.DropDownList cboPhysicians;

        protected void Page_PreRender(object sender, EventArgs e)
        {

            if (cboPhysicians != null)
            {
                //if (Cache["Physicians"] == null)
                //{
                //    DataSet dsPhysicians = DAL.PracticeLocation.Physician.GetPhysicians(practiceLocationID);
                //    Cache["Physicians"] = dsPhysicians.Tables[0];
                //}
                cboPhysicians.DataSource = DAL.PracticeLocation.Physician.GetPhysicians(practiceLocationID); //Cache["Products"] as DataTable;
                cboPhysicians.DataValueField = "ClinicianId";
                cboPhysicians.DataTextField = "ClinicianName";
                cboPhysicians.DataBind();

                cboPhysicians.Items.Insert(0, new ListItem("Select Clinician", "0"));
            }
        }

        protected void cboPhysicians_SelectedIndexChanged(object sender, EventArgs e)
        {
            string physicianIDValue = cboPhysicians.SelectedValue;
            Int32 physicianID = 0;
            Int32.TryParse(physicianIDValue, out physicianID);

            if (PhysicianEvent != null)
            {
                PhysicianEvent(this, new PhysicianEventArgs(physicianID));
            }
        }

        public Int32 GetPhysicianID()
        {
            Int32 physicianID = 0;
            Int32.TryParse(cboPhysicians.SelectedValue, out physicianID);
            return physicianID;
        }
    }

    public class PhysicianEventArgs : EventArgs
    {
        public PhysicianEventArgs(Int32 physicianID)
        {
            PhysicianID = physicianID;
        }
        public Int32 PhysicianID { get; set; }
    }
}