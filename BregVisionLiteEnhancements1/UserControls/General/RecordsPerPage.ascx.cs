﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace BregVision.UserControls.General
{
    public delegate void RecordsPerPageEventHandler(object sender, RecordsPerPageEventArgs e);

    public partial class RecordsPerPage : Bases.UserControlBase
    {
        public event RecordsPerPageEventHandler recordsPerPageChangedEvent;

        //This is initially set from user preferences in the database in the SiteMaster.Master.cs

        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            txtRecordsDisplayed.Value = recordsDisplayedPerPage;
        }

        public RadNumericTextBox RecordsDisplayedTextBox
        {
            get
            {
                return txtRecordsDisplayed;
            }
        }

        private void InitializeAjaxSettings()
        {
            Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);
            if (radAjaxManager != null)
            {
                radAjaxManager.AjaxSettings.AddAjaxSetting(txtRecordsDisplayed, txtRecordsDisplayed);
            }
        }

        protected void txtRecordsDisplayed_TextChanged(object sender, EventArgs e)
        {
            Int32 newRecordsToDisplayPerPage = 0;

            Int32.TryParse(txtRecordsDisplayed.Value.ToString(), out newRecordsToDisplayPerPage);
            if (newRecordsToDisplayPerPage > 0)
            {
                recordsDisplayedPerPage = Convert.ToInt32(txtRecordsDisplayed.Value);

                //fire event
                if ( recordsPerPageChangedEvent != null)
                {
                    RecordsPerPageEventArgs recordsPerPageEventArgs = new RecordsPerPageEventArgs(newRecordsToDisplayPerPage);
                    recordsPerPageChangedEvent(this, recordsPerPageEventArgs);
                }
            }
        }

    }        
        public class RecordsPerPageEventArgs : EventArgs
        {
            public RecordsPerPageEventArgs(Int32 newRecordsPerPage)
            {
                NewRecordsPerPage = newRecordsPerPage;
            }
            public Int32 NewRecordsPerPage { get; set; }
        }
}