﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VisionLanding.ascx.cs"
    Inherits="BregVision.UserControls.LandingPage.VisionLanding" %>
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<div id="login_form" class="loginContainer">
    <div id="loginTopMask"></div>
    <div class="loginFieldsContainer">
        <asp:Login ID="Login2" runat="server" DestinationPageUrl="~/processlogin.aspx" RenderOuterTable="false">
            <LayoutTemplate>
                <div class="logoContainer">
                </div>
                <div class="loginTextBoxContainer" id="usernameContainer">
                    <asp:TextBox type="text" ID="UserName" ClientIDMode="Static" runat="server" MaxLength="50"
                        placeholder="username" CssClass="required loginTextBox" />
                </div>
                <div class="loginTextBoxContainer" id="passwordContainer">
                    <asp:TextBox type="password" ID="PassWord" ClientIDMode="Static" runat="server" MaxLength="50"
                        placeholder="password" CssClass="required loginTextBox" />
                </div>
                <asp:Button ID="Login" ClientIDMode="Static" CommandName="Login" runat="server" Text="Login"
                    CssClass="loginButton" TabIndex="7"></asp:Button>

                <div id="login_password" class="loginError">
                    <div id="login_error">
                        <asp:Label ID="FailureText" runat="server" ForeColor="Red"></asp:Label>
                    </div>
                </div>
            </LayoutTemplate>
        </asp:Login>
    </div>
    <div id="contactContainer">
        <div class="wrapper">
            <div>
                <asp:Label runat="server" Text="Breg Vision Customer Care:" CssClass="customerCareLabel"></asp:Label>
            </div>
            <div>
                 <asp:Label runat="server" Text="888-290-8481" CssClass="customerCareNumber"></asp:Label>
            </div>
        </div>
    </div> 
</div>
<div class="loginFooter">
    <div class="bregUrl"><a href="http://www.breg.com">Breg.com</a></div>
</div>

