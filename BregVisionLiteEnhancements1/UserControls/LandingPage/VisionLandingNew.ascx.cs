﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BregVision.UserControls.LandingPage
{
    public partial class VisionLandingNew : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Login.Focus();

#if(DEBUG)
            Login.UserName = "TestAdmin";
            if (System.Environment.MachineName.ToLower() == "msneen-gateway")
            {
                if (BLL.Website.IsVisionExpress())
                {
                    Login.UserName = "msneen13";
                }
                else
                {
                    Login.UserName = "TestAdmin";
                }
            }
#endif
        }


        public string GetAbsPath(string imagePath)
        {
            return BLL.Website.GetAbsPath(imagePath);
        } 
    }
}