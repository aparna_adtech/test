using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;



public partial class Address : System.Web.UI.UserControl
{
	private string _state;
	private bool _isDataRefreshNeeded = true;
    private bool _isValidationEnabled = true;

	public string State
	{
		get { return _state; }
		set { _state = value; }
	}

	public bool IsDataRefreshNeeded
	{
		get { return _isDataRefreshNeeded; }
		set { _isDataRefreshNeeded = value; }
	}

    public bool IsValidationEnabled
    {
        get { return _isValidationEnabled; }
        set { _isValidationEnabled = value; }
    }

    public TextBox txtAttentionOfTextBox
	{
		get { return txtAttentionOf; }
	}

    public TextBox txtAddressLine1TextBox
	{
		get { return txtAddressLine1; }
	}
    public TextBox txtAddressLine2TextBox
	{
		get { return txtAddressLine2; }
	}
	public TextBox txtCityTextBox
	{
		get { return txtCity; }
	}	
	public DropDownList cmbStateDropDownList
	{
		get { return cmbState; }
	}
    public TextBox txtZipCodeTextBox
	{
		get { return txtZipCode; }
	}
    public TextBox txtZipCodePlus4TextBox
	{
		get { return txtZipCodePlus4; }
	}

	protected void Page_Load(object sender, EventArgs e)
    {

	}

    protected void Page_PreRender(object sender, EventArgs e)
    {
        string selectedState = "  "; //the two empty spaces are used to select the first blank line if there is no selection

        if (!string.IsNullOrEmpty(_state))
        {
            selectedState = _state;
        }

        cmbState.ClearSelection();

        PopulateStateList();

        cmbState.SelectedValue = selectedState;

        SetValidation();
    }

    private void SetValidation()
    {
        rfvAddressLine1.Enabled = IsValidationEnabled;
        rfvCity.Enabled = IsValidationEnabled;
        rfvState.Enabled = IsValidationEnabled;
        rfvZipCode.Enabled = IsValidationEnabled;
    }


	protected void PopulateStateList()
    {
	
		DAL.State dalS = new DAL.State();
		List<DAL.State> sList = new List<DAL.State>();		
		sList = dalS.GetStates();

        cmbState.SelectedValue = null;
        cmbState.DataSource = sList;
        cmbState.DataValueField = "StateAbbr";
        cmbState.DataTextField = "StateAbbr";
        cmbState.DataBind();

        ListItem cmbHeader = new ListItem();
        cmbHeader.Text = " Select State...";
        cmbHeader.Value = "0";
        cmbState.Items.Insert( 0, cmbHeader );
        cmbState.DataBind();

    }
}
