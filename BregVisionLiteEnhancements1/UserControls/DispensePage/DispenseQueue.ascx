﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DispenseQueue.ascx.cs" Inherits="BregVision.UserControls.DispensePage.DispenseQueue" %>

<telerik:RadScriptBlock ID="RadScriptBlockQueue1" runat="server">
    <script type="text/javascript" language="javascript">
        function ChangeRowColor(rowID) {
            var color = document.getElementById(rowID).style.backgroundColor;

            if (color != '#f6d28d') {
                oldColor = color;
            }
            if (color == '#f6d28d') {
                document.getElementById(rowID).style.backgroundColor = oldColor;
            }
            else {
                document.getElementById(rowID).style.backgroundColor = '#f6d28d';
            }
        }
    </script>
</telerik:RadScriptBlock>
<br />
<hr />
<asp:Label ID="Label4" runat="server" CssClass="PageTitle">Dispense Queue</asp:Label>
<bvu:Help ID="Help2" runat="server" HelpID="83" />

<asp:Panel ID="pnlDispense" runat="server" HorizontalAlign="Center" ScrollBars="None"
    Width="100%">
    <table border="0" width="100%">
        <tr>
            <td align="left" style="width: 30%">
                <asp:Button ID="btnSaveDispenseQueue" runat="server" OnClick="btnSaveDispenseQueue_Click"
                    Text="Save Changes" ToolTip="Save Dispense Queue" ValidationGroup="vgGroup" Width="140px" />&nbsp;
                <asp:Button ID="btnDispenseProducts" runat="server" OnClick="btnDispenseProducts_Click"
                    Text="Dispense Product(s)" ToolTip="Dispense product(s)" ValidationGroup="vgGroup"
                    Width="140px" />
                <bvu:Help ID="Help1" runat="server" HelpID="84" />
            </td>
            <td align="left" style="widows: 70%">
                <asp:Label ID="lblSummary" runat="server" CssClass="WarningMsg" Visible="true"> &nbsp</asp:Label>
                <asp:Label ID="lblErrorMsg" runat="server" CssClass="WarningMsg"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2"> 
                <!--grdSummary here-->
                <asp:GridView ID="grdSummary" runat="server" AllowPaging="True" AutoGenerateColumns="false"
                    BorderColor="Gray" GridLines="Both" OnPageIndexChanging="grdSummary_PageIndexChanging"
                    OnRowCreated="grdSummary_OnRowCreated" OnRowDataBound="grdSummary_RowDataBound"
                    OnRowDeleting="grdSummary_OnRowDeleting">
                    <HeaderStyle CssClass="TextSml" HorizontalAlign="Left" />
                    <RowStyle Height="35px" />
                    <PagerStyle CssClass="GridPagerBG" ForeColor="White" />
                    <Columns>
                        <asp:TemplateField HeaderStyle-BackColor="#DFE6EF" HeaderStyle-BorderColor="#A2B6CB"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" ItemStyle-BorderColor="#969696"
                            ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1px">
                            <ItemTemplate>
                                <asp:CheckBox ID="ClientSelectColumn" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-BackColor="#DFE6EF" HeaderStyle-BorderColor="#A2B6CB"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderStyle-Width="25px"
                            ItemStyle-BorderColor="#969696" ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1px">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CommandName="Delete" ImageUrl="~/RadControls/Grid/Skins/Office2007/Delete.gif"
                                    OnClientClick='return confirm("Are you sure you want delete this?");' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-BackColor="#DFE6EF" HeaderStyle-BorderColor="Gray"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderStyle-Width="35px"
                            HeaderText="Is Medi care" ItemStyle-BorderColor="#969696" ItemStyle-BorderStyle="Solid"
                            ItemStyle-BorderWidth="1px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkIsMedicare" runat="server" Checked='<%# bind("IsMedicare") %>'
                                    Enabled="true" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-BackColor="#DFE6EF" HeaderStyle-BorderColor="Gray"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderText="ABN Form"
                            ItemStyle-BorderColor="#969696" ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1px">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkABNForm" runat="server" Checked='<%# bind("ABNForm") %>' Enabled="true" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ABN_Needed" Visible="false" />
                        <asp:BoundField DataField="DispenseQueueID" />
                        <asp:BoundField DataField="PracticeLocationID" Visible="false" />
                        <asp:BoundField DataField="BregVisionOrderID" Visible="false" />
                        <asp:TemplateField HeaderStyle-BackColor="#DFE6EF" HeaderStyle-BorderColor="Gray"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderText="Dispense Date"
                            ItemStyle-BorderColor="#969696" ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1px"
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <telerik:raddatepicker id="txtDateDispensed" runat="server" dateinput-displaydateformat="MM/dd/yy"
                                    tooltip="Dates can be entered in the following formats(01012007,01/01/2007,January 1,2007)"
                                    width="70px" Skin="Web20">
                                    <Calendar ID="Calendar1" runat="server" Skin="Web20" ShowRowHeaders="false" BorderStyle="Solid" 
                                        CalendarTableStyle-BorderStyle="Solid" CalendarTableStyle-BorderColor="Blue" CalendarTableStyle-BorderWidth="1px" 
                                        TitleStyle-BorderColor="Blue" TitleStyle-BorderStyle="Solid" TitleStyle-BorderWidth="1px" TitleStyle-Font-Bold="true">
                                    </Calendar>
                                </telerik:raddatepicker>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-BackColor="#DFE6EF" HeaderStyle-BorderColor="Gray"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderText="Physician"
                            ItemStyle-BorderColor="#969696" ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1px">
                            <ItemTemplate>
                                <asp:DropDownList ID="cboPhysicians" runat="server">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ControlStyle-CssClass="TextSml" HeaderStyle-BackColor="#DFE6EF"
                            HeaderStyle-BorderColor="Gray" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px"
                            HeaderText="Patient Code" ItemStyle-BorderColor="#969696" ItemStyle-BorderStyle="Solid"
                            ItemStyle-BorderWidth="1px">
                            <ItemTemplate>
                                <asp:TextBox ID="txtPatientCode" runat="server" MaxLength="50" Text='<%# bind("PatientCode") %>'
                                    Width="185px"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="BrandShortName" HeaderStyle-BackColor="#DFE6EF" HeaderStyle-BorderColor="Gray"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderText="Brand"
                            ItemStyle-BorderColor="#969696" ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1px"
                            ItemStyle-CssClass="TextXXSml" Visible="true" />
                        <asp:BoundField DataField="ShortName" HeaderStyle-BackColor="#DFE6EF" HeaderStyle-BorderColor="Gray"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderText="Product"
                            ItemStyle-BorderColor="#969696" ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1px"
                            ItemStyle-CssClass="TextXXSml" Visible="true" />
                        <asp:BoundField DataField="Code" HeaderStyle-BackColor="#DFE6EF" HeaderStyle-BorderColor="Gray"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderText="Code"
                            ItemStyle-BorderColor="#969696" ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1px"
                            ItemStyle-CssClass="TextXXSml" Visible="true" />
                        <asp:TemplateField HeaderStyle-BackColor="#DFE6EF" HeaderStyle-BorderColor="Gray"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderText="RT/LT Modifier"
                            ItemStyle-BorderColor="#969696" ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1px"
                            ItemStyle-CssClass="TextXXSml" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:DropDownList ID="cboSide" runat="server" SelectedValue='<%# bind("Side") %>'>
                                    <asp:ListItem Text="U" Value="U"></asp:ListItem>
                                    <asp:ListItem Text="LT" Value="L"></asp:ListItem>
                                    <asp:ListItem Text="RT" Value="R"></asp:ListItem>
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Size" HeaderStyle-BackColor="#DFE6EF" HeaderStyle-BorderColor="Gray"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderStyle-Width="50px"
                            HeaderText="Size" ItemStyle-BorderColor="#969696" ItemStyle-BorderStyle="Solid"
                            ItemStyle-BorderWidth="1px" ItemStyle-CssClass="TextXXSml" />
                        <asp:BoundField DataField="HCPCsString" HeaderStyle-BackColor="#DFE6EF" HeaderStyle-BorderColor="Gray"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderText="HCPCs"
                            ItemStyle-BorderColor="#969696" ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1px"
                            ItemStyle-CssClass="TextXXSml" />
                        <asp:BoundField DataField="WholesaleCost" DataFormatString="{0:C}" HeaderStyle-BackColor="#DFE6EF"
                            HeaderStyle-BorderColor="Gray" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px"
                            ItemStyle-BorderColor="#969696" ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1px" />
                        <asp:BoundField DataField="QuantityOnHandPerSystem" HeaderStyle-BackColor="#DFE6EF"
                            HeaderStyle-BorderColor="Gray" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px"
                            HeaderText="Qty. On Hand" ItemStyle-BorderColor="#969696" ItemStyle-BorderStyle="Solid"
                            ItemStyle-BorderWidth="1px" ItemStyle-CssClass="TextSml" />
                        <asp:TemplateField HeaderStyle-BackColor="#DFE6EF" HeaderStyle-BorderColor="Gray"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderText="Qty. to Dispense"
                            ItemStyle-BorderColor="#969696" ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1px"
                            ItemStyle-CssClass="TextSml">
                            <ItemTemplate>
                                &#160;<telerik:radnumerictextbox id="txtQuantityDispensed" runat="server" cssclass="TextSml"
                                    culture="English (United States)" labelcssclass="radLabelCss_Default" maxvalue="999"
                                    minvalue="0" numberformat-decimaldigits="0" showspinbuttons="True" skin="Office2007"
                                    spindowncssclass="Office2007" spinupcssclass="Office2007" text='<%# bind("QuantityToDispense") %>'
                                    width="41px">
                                                </telerik:radnumerictextbox>
                                <asp:RequiredFieldValidator ID="rfvQuantityDispensed" runat="server" ControlToValidate="txtQuantityDispensed"
                                    CssClass="WarningMsgSml" Display="Dynamic" ErrorMessage="*" ValidationGroup="vgGroup"></asp:RequiredFieldValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-BackColor="#DFE6EF" HeaderStyle-BorderColor="Gray"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" ItemStyle-BorderColor="#969696"
                            ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1px" ItemStyle-CssClass="TextSml"
                            ItemStyle-HorizontalAlign="Center">
                            <HeaderTemplate>
                                <asp:Label ID="Label1" runat="server" Text="Cash Over ride"></asp:Label>
                                <bvu:Help ID="Help3" runat="server" HelpID="128" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkDMEDeposit" runat="server" Width="35px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-BackColor="#DFE6EF" HeaderStyle-BorderColor="Gray"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" ItemStyle-BorderColor="#969696"
                            ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1px" ItemStyle-CssClass="TextSml">
                            <HeaderTemplate>
                                <asp:Label ID="Label1" runat="server" Text="DME Deposit"></asp:Label>
                                <bvu:Help ID="Help4" runat="server" HelpID="31" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <telerik:radnumerictextbox id="txtDMEDeposit" runat="server" cssclass="TextSml" culture="English (United States)"
                                    labelcssclass="radLabelCss_Default" maxvalue="9999" minvalue="0" numberformat-decimaldigits="2"
                                    showspinbuttons="True" skin="Office2007" spindowncssclass="Office2007" spinupcssclass="Office2007"
                                    text='<%# bind("DMEDeposit") %>' type="Currency" width="85px">
                                                </telerik:radnumerictextbox>
                                <asp:RequiredFieldValidator ID="rfvDMEDeposit" runat="server" ControlToValidate="txtDMEDeposit"
                                    CssClass="WarningMsgSml" Display="Dynamic" ErrorMessage="*" ValidationGroup="vgGroup"></asp:RequiredFieldValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="hdnDMEDeposit" Visible="false" />
                        <asp:BoundField DataField="BillingCharge" DataFormatString="{0:C}" HeaderStyle-BackColor="#DFE6EF"
                            HeaderStyle-BorderColor="Gray" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px"
                            HeaderText="Billing Charge" ItemStyle-BorderColor="#969696" ItemStyle-BorderStyle="Solid"
                            ItemStyle-BorderWidth="1px" ItemStyle-CssClass="TextXXSml" />
                        <asp:BoundField DataField="BillingChargeCash" DataFormatString="{0:C}" HeaderStyle-BackColor="#DFE6EF"
                            HeaderStyle-BorderColor="Gray" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px"
                            HeaderText="Billing Charge Cash" ItemStyle-BorderColor="#969696" ItemStyle-BorderStyle="Solid"
                            ItemStyle-BorderWidth="1px" ItemStyle-CssClass="TextXXSml" />
                        <asp:TemplateField HeaderStyle-BackColor="#DFE6EF" HeaderStyle-BorderColor="Gray"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center"
                            HeaderText="ICD9" ItemStyle-BorderColor="#969696" ItemStyle-BorderStyle="Solid"
                            ItemStyle-BorderWidth="1px" ItemStyle-CssClass="TextSml">
                            <ItemTemplate>
                                <telerik:radcombobox id="cboICD9" runat="server" allowcustomtext="true" expandeffect="fade" Skin="Web20"></telerik:radcombobox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ProductInventoryID" />
                        <asp:BoundField DataField="PracticeCatalogProductID" />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
