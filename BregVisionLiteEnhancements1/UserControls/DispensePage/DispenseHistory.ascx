﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DispenseHistory.ascx.cs" Inherits="BregVision.UserControls.DispensePage.DispenseHistory" %>

 <table border="0" width="96%">
    <tr>
        <td >
            <asp:Label id="lblReceiptHistory" runat="server" Font-Size="Small">Please select a receipt and click the 'Print Preview' button.</asp:Label>
        </td>
        <td align="right">
            <asp:Button ID="btnPrintReceiptHistory" runat="server" OnClick="btnPrintReceiptHistory_Click" Text="Print Preview" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <bvu:SearchToolbar ID="ctlSearchToolbar" runat="server" />
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center">
            <div id="Div1" runat="server">
                <bvu:VisionRadGridAjax ID="grdReceiptHistory" runat="server" 
                         OnDetailTableDataBind="grdReceiptHistory_OnDetailTableDataBind"
                         OnNeedDataSource="grdReceiptHistory_NeedDataSource" 
                         Width="100%">
                    <mastertableview datakeynames="DispenseID" enabletheming="true" hierarchyloadmode="ServerOnDemand" Width="100%">
                        <Columns>
                            <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="35px" 
                                    HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid"/>
                            <telerik:GridBoundColumn DataField="DispenseID" UniqueName="DispenseID" Visible="False">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="DispenseBatchID" UniqueName="DispenseBatchID" Visible="False">
                            </telerik:GridBoundColumn>
                            <telerik:GridCheckBoxColumn DataField="HasABNForm" 
                                    UniqueName="HasABNForm" 
                                    HeaderText="Has ABN Form(s)" 
                                    HeaderStyle-Width="100px"
                                    DataType="System.Boolean" 
                                    HeaderStyle-BorderColor="#A2B6CB" 
                                    HeaderStyle-BorderWidth="1px" 
                                    HeaderStyle-BorderStyle="Solid">
                            </telerik:GridCheckBoxColumn>
                            <telerik:GridBoundColumn DataField="CreatedWithHandheld" 
                                    HeaderText="Created w/ Handheld" 
                                    UniqueName="Handheld" 
                                    ItemStyle-HorizontalAlign="center" 
                                    ItemStyle-Width="120" 
                                    HeaderStyle-Width="120" 
                                    HeaderStyle-BorderColor="#A2B6CB" 
                                    HeaderStyle-BorderWidth="1px" 
                                    HeaderStyle-BorderStyle="Solid">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="PatientCode" HeaderText="Patient Code" 
                                    UniqueName="PatientCode" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Total" HeaderText="Total" 
                               DataFormatString="{0:C}" UniqueName="Total" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="DateDispensed" HeaderText="Date Dispensed" UniqueName="DateDispensed" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                            </telerik:GridBoundColumn>
                        </Columns>
                        <detailtables>
                            <telerik:GridTableView runat="server" DataKeyNames="DispenseID" Width="100%" BorderColor="#A2B6CB" BorderWidth="1px" BorderStyle="Solid">
                                 <parenttablerelation>
                                    <telerik:GridRelationFields DetailKeyField="DispenseID" MasterKeyField="DispenseID" />
                                 </parenttablerelation>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="DispenseID" UniqueName="DispenseID" Visible="False">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridCheckBoxColumn DataField="ABNForm" HeaderText="ABN Form" UniqueName="ABNForm" DataType="System.Boolean" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridBoundColumn DataField="PhysicianName"  HeaderText="Physician" UniqueName="PhysicianName" Visible="true" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="SupplierShortName" HeaderText="Brand" UniqueName="SupplierShortName" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Name" HeaderText="Product" UniqueName="Product" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Code" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50" HeaderStyle-Wrap="true" HeaderText="Code" ItemStyle-HorizontalAlign="center" ItemStyle-Width="50" UniqueName="Code" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid"/>
                                    <telerik:GridBoundColumn DataField="Side" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50" HeaderStyle-Wrap="true" HeaderText="RT / LT Modifier" ItemStyle-HorizontalAlign="center" ItemStyle-Width="50" UniqueName="Side" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid"/>
                                    <telerik:GridBoundColumn DataField="Size" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50" HeaderStyle-Wrap="true" HeaderText="Size" ItemStyle-HorizontalAlign="center" ItemStyle-Width="50" UniqueName="Size"  HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid"/>
                                    <telerik:GridBoundColumn DataField="DMEDeposit" HeaderText="DME Deposit" DataFormatString="{0:C}" UniqueName="DMEDeposit" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Quantity" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50" HeaderStyle-Wrap="true" HeaderText="Qty" ItemStyle-HorizontalAlign="center" ItemStyle-Width="50" UniqueName="Quantity" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid"/>
                                    <telerik:GridBoundColumn DataField="LineTotal" HeaderText="Billing Charge" DataFormatString="{0:C}" UniqueName="LineTotal" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </telerik:GridTableView>
                        </detailtables>
                    </mastertableview>
                </bvu:VisionRadGridAjax>
            </div>
        </td>
    </tr>
</table>