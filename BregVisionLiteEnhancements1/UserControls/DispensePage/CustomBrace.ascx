﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomBrace.ascx.cs" Inherits="BregVision.UserControls.DispensePage.CustomBrace" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<div class="flex-row subWelcome-bar">
  <h1 class="flex-row align-center justify-start">Purchase Order <small class="light">Shipping, Billing, and Shipping Method</small>
    </h1>
  <asp:Button ID="btnNext" runat="server" Text="Next" OnClick="btnNext_Click" CssClass="button-primary" />
</div>
<div class="subTabContent-container">
  <div class="content">
    <div id="customBraceReadOnlyDiv" style="display: none; z-index: 9999; position: fixed;
            top: 0; left: 0; width: 100%; height: 100%; background-color: White; opacity: .0;
            filter: alpha(opacity=0);">
    </div>
    <div id="customBracePOBlock" style="padding-left: 5px; padding-right: 20px;">
      <asp:ValidationSummary ShowMessageBox="true" runat="server" ShowSummary="False" ID="ValidationSummary1" />
      <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <%--billing/shipping information--%>
          <tr>
            <td width="2%">
            </td>
            <td valign="top" width="47%">
              <div style="border: 1px solid #CCCCCC;" class="rgEditForm customBraceForm">
                <table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="white">
                  <tr>
                    <td colspan="2" style="background-color: #EEEEEE;">
                      <h2>Billing</h2>
                    </td>
                  </tr>
                  <tr>
                    <td width="5%" align="right">Customer #</td>
                    <td>
                      <asp:TextBox ID="CustomerNumber" runat="server"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="CustomerNumberValidator1" runat="server" ControlToValidate="CustomerNumber" ErrorMessage="Billing customer number is required.">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>Tel</td>
                    <td>
                      <telerik:RadMaskedTextBox ID="txtBillPhone" runat="server" DisplayFormatPosition="left" DisplayMask="###.###.####x#####" Mask="###############">
                      </telerik:RadMaskedTextBox>
                      <asp:RequiredFieldValidator ID="txtBillPhoneValidator1" runat="server" ControlToValidate="txtBillPhone" Text="*" ErrorMessage="Billing customer telephone number is required."></asp:RequiredFieldValidator>
                    </td>
                  </tr>
                  <tr>
                    <td width="5%" align="right">Attention</td>
                    <td>
                      <asp:TextBox ID="BillAttention" runat="server"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="BillAttentionValidator1" runat="server" ControlToValidate="BillAttention" ErrorMessage="Billing attention-to is required.">*</asp:RequiredFieldValidator>
                    </td>
                  </tr>
                  <tr>
                    <td width="5%" align="right">Address</td>
                    <td>
                      <asp:TextBox ID="BillAddress1" runat="server"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="BillAddress1Validator1" runat="server" ControlToValidate="BillAddress1" ErrorMessage="Billing address is required.">*</asp:RequiredFieldValidator>
                    </td>
                  </tr>
                  <tr>
                    <td width="5%" align="right">&nbsp;</td>
                    <td>
                      <asp:TextBox ID="BillAddress2" runat="server"></asp:TextBox>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" valign="middle">
                      <div style="font-size: small; text-align: center;">City
                        <asp:TextBox ID="BillCity" runat="server" Width="10em"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="BillCityValidator1" runat="server" ControlToValidate="BillCity" ErrorMessage="Billing address city is required.">*</asp:RequiredFieldValidator>State
                        <asp:TextBox ID="BillState" runat="server" Width="10em"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="BillStateValidator1" runat="server" ControlToValidate="BillState" ErrorMessage="Billing address state is required.">*</asp:RequiredFieldValidator>Zip Code
                        <asp:TextBox ID="BillZipCode" runat="server" Width="5em"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="BillZipCodeValidator1" runat="server" ControlToValidate="BillZipCode" ErrorMessage="Billing address zip code is required.">*</asp:RequiredFieldValidator>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
            </td>
            <td width="2%">&nbsp;</td>
            <td valign="top" width="47%">
              <div style="border: 1px solid #CCCCCC;" class="rgEditForm customBraceForm">
                <table border="0" cellpadding="2" cellspacing="0" width="100%">
                  <tr>
                    <td colspan="2" style="background-color: #EEEEEE;">
                      <h2>Shipping</h2>
                    </td>
                  </tr>
                  <tr>
                    <td width="5%" align="right">Attention</td>
                    <td>
                      <asp:TextBox ID="ShipAttention" runat="server"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="ShipAttentionValidator1" runat="server" ControlToValidate="ShipAttention" ErrorMessage="Shipping attention-to is required.">*</asp:RequiredFieldValidator>
                    </td>
                  </tr>
                  <tr>
                    <td width="5%" align="right">Address</td>
                    <td>
                      <asp:TextBox ID="ShipAddress1" runat="server"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="ShipAddress1Validator1" runat="server" ControlToValidate="ShipAddress1" ErrorMessage="Shipping address is required.">*</asp:RequiredFieldValidator>
                    </td>
                  </tr>
                  <tr>
                    <td width="5%" align="right">&nbsp;</td>
                    <td>
                      <asp:TextBox ID="ShipAddress2" runat="server"></asp:TextBox>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" align="center" valign="middle">
                      <div>City
                        <asp:TextBox ID="ShipCity" runat="server" Width="10em"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="ShipCityValidator1" runat="server" ControlToValidate="ShipCity" ErrorMessage="Shipping address city is required.">*</asp:RequiredFieldValidator>State
                        <asp:TextBox ID="ShipState" runat="server" Width="10em"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="ShipStateValidator1" runat="server" ControlToValidate="ShipState" ErrorMessage="Shipping address state is required.">*</asp:RequiredFieldValidator>Zip Code
                        <asp:TextBox ID="ShipZipCode" runat="server" Width="5em"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="ShipZipCodeValidator1" runat="server" ControlToValidate="ShipZipCode" ErrorMessage="Shipping address zip code is required.">*</asp:RequiredFieldValidator>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
            </td>
            <td width="2%">&nbsp;</td>
          </tr>
          <%--spacer--%>
            <tr>
              <td colspan="5"> </td>
            </tr>
            <%--Patient Information--%>
              <tr>
                <td width="2%">
                </td>
                <td colspan="3">
                  <div style="border: 1px solid #CCCCCC;" class="rgEditForm customBraceForm">
                    <table border="0" cellpadding="2" cellspacing="0" width="100%">
                      <tr>
                        <td colspan="2" style="background-color: #EEEEEE;">
                          <h2>Patient Information</h2>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class="rgEditForm customBraceForm">
                            <table >
                              <tr id="patientInfoRow">
                                <td style="text-align:left !important" width="50%" valign="top">
                                  <div>Patient's Name
                                    <asp:TextBox ID="txtPatientName" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="txtPatientNameValidator1" runat="server" ControlToValidate="txtPatientName" ErrorMessage="Patient's name is required.">*</asp:RequiredFieldValidator>Physician
                                    <asp:DropDownList ID="ddlPhysician" runat="server">
                                    </asp:DropDownList>
                                  </div>
                                  <div style="margin-top: 6px;">Age
                                    <asp:TextBox runat="server" ID="txtAge" Width="30px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="txtAgeValidator1" runat="server" ControlToValidate="txtAge" ErrorMessage="Patient age is required.">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="txtAgeNumericValidator" runat="server" ControlToValidate="txtAge" ValidationExpression="[0-9]*" ErrorMessage="Patient age must be a number.">*</asp:RegularExpressionValidator>Weight
                                    <asp:TextBox ID="txtWeight" runat="server" Width="30px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="txtWeightValidator1" runat="server" ControlToValidate="txtWeight" ErrorMessage="Patient weight is required.">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="txtWeighNumericValidator" runat="server" ControlToValidate="txtWeight" ValidationExpression="[0-9]*" ErrorMessage="Patient weight must be a number.">*</asp:RegularExpressionValidator>Height
                                    <asp:TextBox ID="txtHeight" runat="server" Width="40px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="txtHeightValidator1" runat="server" ControlToValidate="txtHeight" ErrorMessage="Patient height is required.">*</asp:RequiredFieldValidator>Sex
                                    <asp:RadioButtonList ID="rblSex" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                      <asp:ListItem Selected="True" Text="M" Value="Male"></asp:ListItem>
                                      <asp:ListItem Text="F" Value="Female"></asp:ListItem>
                                    </asp:RadioButtonList>
                                  </div>
                                </td>
                                <td width="50%" valign="top">
                                    <div class="rgEditForm customBraceForm">
                                      <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                          <td>
                                            <p>Leg Measurements</p>
                                          </td>
                                          <td>
                                            <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
                                              <script type="text/javascript">
                                              <!--
                                              function rblLegMeasurements_change() {
                                                if ($('#<%= rblLegMeasurements.ClientID %> input:checked').val() == "Bilat") {
                                                  $('#<%= rblBraceFor.ClientID %> input[value="Both Legs"]')[0].checked = true;
                                                } else {
                                                  if ($('#<%= rblBraceFor.ClientID %> input:checked').val() == "Both Legs") {
                                                    $('#<%= rblBraceFor.ClientID %> input[value="Left Leg"]')[0].checked = true;
                                                  }
                                                }
                                                setLegMeasurementRowsVisibility();
                                              }

                                              function rblBraceFor_change() {
                                                if ($('#<%= rblBraceFor.ClientID %> input:checked').val() == "Both Legs") {
                                                  $('#<%= rblLegMeasurements.ClientID %> input[value="Bilat"]')[0].checked = true;
                                                } else {
                                                  if ($('#<%= rblLegMeasurements.ClientID %> input:checked').val() == "Bilat") {
                                                    $('#<%= rblLegMeasurements.ClientID %> input[value="None"]')[0].checked = true;
                                                  }
                                                }
                                                setLegMeasurementRowsVisibility();
                                              }

                                              function setLegMeasurementRowsVisibility() {
                                                var bf = $('#<%= rblBraceFor.ClientID %> input:checked').val();
                                                var lr = $('#leftLegMeasurementsRow')[0];
                                                var lra = $('#copyFromRightLegAnchor')[0];
                                                var rr = $('#rightLegMeasurementsRow')[0];
                                                var rra = $('#copyFromLeftLegAnchor')[0];
                                                if ((bf == "Left Leg") || (bf == "Both Legs")) {
                                                  lr.style.display = "table-row";
                                                  rra.style.display = "inline";
                                                } else {
                                                  lr.style.display = "none";
                                                  rra.style.display = "none";
                                                }
                                                if ((bf == "Right Leg") || (bf == "Both Legs")) {
                                                  rr.style.display = "table-row";
                                                  lra.style.display = "inline";
                                                } else {
                                                  rr.style.display = "none";
                                                  lra.style.display = "none";
                                                }
                                              }

                                              $(function() {
                                                $('#<%= rblLegMeasurements.ClientID %>').change(rblLegMeasurements_change);
                                                $('#<%= rblBraceFor.ClientID %>').change(rblBraceFor_change);
                                                setLegMeasurementRowsVisibility();
                                              });
                                              //-->
                                              </script>
                                            </telerik:RadScriptBlock>
                                            <asp:RadioButtonList ID="rblLegMeasurements" runat="server" RepeatDirection="Horizontal">
                                              <asp:ListItem Text="Bilat" Value="Bilat"></asp:ListItem>
                                              <asp:ListItem Text="Cast" Value="Cast"></asp:ListItem>
                                              <asp:ListItem Text="Reform" Value="Reform"></asp:ListItem>
                                              <asp:ListItem Text="None" Value="None" Selected="True"></asp:ListItem>
                                            </asp:RadioButtonList>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td>BRACE FOR</td>
                                          <td>
                                            <asp:RadioButtonList ID="rblBraceFor" runat="server" RepeatDirection="Horizontal">
                                              <asp:ListItem Text="Left Leg" Value="Left Leg" Selected="True"></asp:ListItem>
                                              <asp:ListItem Text="Right Leg" Value="Right Leg"></asp:ListItem>
                                              <asp:ListItem Text="Both Legs" Value="Both Legs"></asp:ListItem>
                                            </asp:RadioButtonList>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td colspan="2">Measurements taken by
                                            <asp:TextBox ID="txtMeasurementsTakenBy" runat="server"></asp:TextBox>
                                          </td>
                                        </tr>
                                      </table>
                                    </div>
                                </td>
                              </tr>
                              <tr id="leftLegMeasurementsRow">
                                <td colspan="2">
                                  <div style="border: 1px solid #dddddd; margin-top: 2px;" class="rgEditForm customBraceForm">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                      <tr>
                                        <td colspan="2">
                                          <telerik:RadScriptBlock ID="RadScriptBlock2" runat="server">
                                            <script type="text/javascript">
                                            <!--
                                            function copyRightLegMeasurements() {
                                              var f1 = function(targetID, sourceID) {
                                                var idT = '#' + targetID;
                                                var idS = '#' + sourceID;
                                                $(idT).val($(idS).val());
                                              };
                                              var f2 = function(targetID, sourceID) {
                                                var idS = '#' + sourceID + ' input:checked';
                                                var idT = '#' + targetID + ' input[value="' + $(idS).val() + '"]';
                                                $(idT)[0].checked = true;
                                              };

                                              f2('<%= rblInstability.ClientID %>', '<%= rblInstabilityR.ClientID %>');
                                              if ($('#<%= trX2KUnloading.ClientID %>').length > 0) {
                                                f2('<%= rblCounterForce.ClientID %>', '<%= rblCounterForceR.ClientID %>');
                                                f1('<%= txtCounterForceDegrees.ClientID %>', '<%= txtCounterForceDegreesR.ClientID %>');
                                              }
                                              f1('<%= txtThigh.ClientID %>', '<%= txtThighR.ClientID %>');
                                              f1('<%= txtKneeOffset.ClientID %>', '<%= txtKneeOffsetR.ClientID %>');
                                              f1('<%= txtCalf.ClientID %>', '<%= txtCalfR.ClientID %>');
                                              f1('<%= txtKneeWidth.ClientID %>', '<%= txtKneeWidthR.ClientID %>');
                                              f2('<%= rblExtension.ClientID %>', '<%= rblExtensionR.ClientID %>');
                                              f2('<%= rblFlexion.ClientID %>', '<%= rblFlexionR.ClientID %>');
                                            }

                                            function clearLeftLegMeasurements() {
                                              $('#<%= rblInstability.ClientID %>input[value="None"]')[0].checked = true;
                                              if ($('#<%= trX2KUnloading.ClientID %>').length > 0) {
                                                $('#<%= rblCounterForce.ClientID %>input').each(
                                                  function() {
                                                    this.checked = false;
                                                  }
                                                );
                                                $('#<%= txtCounterForceDegrees.ClientID %>').val("");
                                              }
                                              $('#<%= txtThigh.ClientID %>').val("");
                                              $('#<%= txtKneeOffset.ClientID %>').val("");
                                              $('#<%= txtCalf.ClientID %>').val("");
                                              $('#<%= txtKneeWidth.ClientID %>').val("");
                                              $('#<%= rblExtension.ClientID %>input[value="10"]')[0].checked = true;
                                              $('#<%= rblFlexion.ClientID %>input[value="None"]')[0].checked = true;
                                            }
                                            //-->
                                            </script>
                                          </telerik:RadScriptBlock>
                                          <h2>Left Leg Measurements</h2> <a id="copyFromRightLegAnchor" href="#" style="padding: 0 8px;
                                                                    margin: 2px; background-color: #dddddd; text-decoration: none; color: Black;
                                                                    cursor: pointer;" onclick="copyRightLegMeasurements(); return false;">Copy Measurements
                                                                    From Right Leg</a> <a id="clearLeftLegMeasurementsAnchor" href="#" style="padding: 0 8px; margin: 2px; background-color: #dddddd;
                                                                        text-decoration: none; color: Black; cursor: pointer; " onclick="clearLeftLegMeasurements(); return false;">Clear Left Leg Measurements</a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td width="50%" valign="top">
                                            <div class="rgEditForm customBraceForm">
                                              <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                  <td>INSTABILITY</td>
                                                  <td>
                                                    <asp:RadioButtonList ID="rblInstability" runat="server" RepeatDirection="Horizontal">
                                                      <asp:ListItem Text="None" Value="None" Selected="True"></asp:ListItem>
                                                      <asp:ListItem Text="ACL" Value="ACL"></asp:ListItem>
                                                      <asp:ListItem Text="PCL" Value="PCL"></asp:ListItem>
                                                      <asp:ListItem Text="MCL/LCL" Value="MCL/LCL"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                  </td>
                                                </tr>
                                                <tr id="trX2KUnloading" runat="server" visible="false">
                                                  <td>CounterForce*</td>
                                                  <td>
                                                    <asp:RadioButtonList ID="rblCounterForce" runat="server" RepeatDirection="Vertical">
                                                      <asp:ListItem Text="OA - Medial Unloading" Value="OA - Medial Unloading"></asp:ListItem>
                                                      <asp:ListItem Text="OA - Lateral Unloading" Value="OA - Lateral Unloading"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <div style="padding: 0 7px;">
                                                      <asp:TextBox ID="txtCounterForceDegrees" runat="server" Width="60px" />°
                                                    </div>
                                                  </td>
                                                </tr>
                                                <tr id="trFusionUnloading" runat="server" visible="false">
                                                  <td colspan="2"  >FUSION Solus OA - Medial unloading only.
                                                  </td>
                                                </tr>
                                              </table>
                                            </div>
                                        </td>
                                        <td width="50%" valign="top">
                                           
                                          <table cellpadding="2" cellspacing="0" width="100%" border="0">
                                            <tr>
                                              <td width="25%" align="right">
                                                1. Thigh Circumference
                                              </td>
                                              <td width="25%">
                                                <asp:TextBox ID="txtThigh" runat="server"></asp:TextBox>
                                              </td>
                                              <td width="25%" align="right">
                                                3. Knee Offset
                                              </td>
                                              <td width="25%">
                                                <asp:TextBox ID="txtKneeOffset" runat="server"></asp:TextBox>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td align="right">
                                                2. Calf Circumference
                                              </td>
                                              <td>
                                                <asp:TextBox ID="txtCalf" runat="server"></asp:TextBox>
                                              </td>
                                              <td align="right">
                                                4. Knee Width
                                              </td>
                                              <td>
                                                <asp:TextBox ID="txtKneeWidth" runat="server"></asp:TextBox>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td colspan="4">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td colspan="2">
                                                <h4 ">Extension</h4>
                                                <asp:RadioButtonList ID="rblExtension" runat="server" RepeatDirection="Horizontal">
                                                  <asp:ListItem Text="0°" Value="0"></asp:ListItem>
                                                  <asp:ListItem Text="10°" Value="10" Selected="True"></asp:ListItem>
                                                  <asp:ListItem Text="20°" Value="20"></asp:ListItem>
                                                  <asp:ListItem Text="30°" Value="30"></asp:ListItem>
                                                  <asp:ListItem Text="40°" Value="40"></asp:ListItem>
                                                </asp:RadioButtonList>
                                              </td>
                                              <td colspan="2">
                                                <h4 >Flexion</h4>
                                                <asp:RadioButtonList ID="rblFlexion" runat="server" RepeatDirection="Horizontal">
                                                  <asp:ListItem Text="None" Value="None" Selected="True"></asp:ListItem>
                                                  <asp:ListItem Text="45°" Value="45"></asp:ListItem>
                                                  <asp:ListItem Text="60°" Value="60"></asp:ListItem>
                                                  <asp:ListItem Text="75°" Value="75"></asp:ListItem>
                                                  <asp:ListItem Text="90°" Value="90"></asp:ListItem>
                                                </asp:RadioButtonList>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td colspan="4" >
                                                <small>* New braces ship with 10° extension stops installed.</small>
                                              </td>
                                            </tr>
                                          </table>
                                        </td>
                                      </tr>
                                    </table>
                                  </div>
                                </td>
                              </tr>
                              <tr id="rightLegMeasurementsRow">
                                <td colspan="2">
                                  <div style="border: 1px solid #dddddd; margin-top: 2px;" class="rgEditForm customBraceForm">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                      <tr>
                                        <td colspan="2">
                                          <telerik:RadScriptBlock ID="RadScriptBlock3" runat="server">
                                            <script type="text/javascript">
                                            <!--
                                            function copyLeftLegMeasurements() {
                                              var f1 = function(targetID, sourceID) {
                                                var idT = '#' + targetID;
                                                var idS = '#' + sourceID;
                                                $(idT).val($(idS).val());
                                              };
                                              var f2 = function(targetID, sourceID) {
                                                var idS = '#' + sourceID + ' input:checked';
                                                var idT = '#' + targetID + ' input[value="' + $(idS).val() + '"]';
                                                $(idT)[0].checked = true;
                                              };

                                              f2('<%= rblInstabilityR.ClientID %>', '<%= rblInstability.ClientID %>');
                                              if ($('#<%= trX2KUnloadingR.ClientID %>').length > 0) {
                                                f2('<%= rblCounterForceR.ClientID %>', '<%= rblCounterForce.ClientID %>');
                                                f1('<%= txtCounterForceDegreesR.ClientID %>', '<%= txtCounterForceDegrees.ClientID %>');
                                              }
                                              f1('<%= txtThighR.ClientID %>', '<%= txtThigh.ClientID %>');
                                              f1('<%= txtKneeOffsetR.ClientID %>', '<%= txtKneeOffset.ClientID %>');
                                              f1('<%= txtCalfR.ClientID %>', '<%= txtCalf.ClientID %>');
                                              f1('<%= txtKneeWidthR.ClientID %>', '<%= txtKneeWidth.ClientID %>');
                                              f2('<%= rblExtensionR.ClientID %>', '<%= rblExtension.ClientID %>');
                                              f2('<%= rblFlexionR.ClientID %>', '<%= rblFlexion.ClientID %>');
                                            }

                                            function clearRightLegMeasurements() {
                                              $('#<%= rblInstabilityR.ClientID %>input[value="None"]')[0].checked = true;
                                              if ($('#<%= trX2KUnloadingR.ClientID %>').length > 0) {
                                                $('#<%= rblCounterForceR.ClientID %>input').each(
                                                  function() {
                                                    this.checked = false;
                                                  }
                                                );
                                                $('#<%= txtCounterForceDegreesR.ClientID %>').val("");
                                              }
                                              $('#<%= txtThighR.ClientID %>').val("");
                                              $('#<%= txtKneeOffsetR.ClientID %>').val("");
                                              $('#<%= txtCalfR.ClientID %>').val("");
                                              $('#<%= txtKneeWidthR.ClientID %>').val("");
                                              $('#<%= rblExtensionR.ClientID %>input[value="10"]')[0].checked = true;
                                              $('#<%= rblFlexionR.ClientID %>input[value="None"]')[0].checked = true;
                                            }
                                            //-->
                                            </script>
                                          </telerik:RadScriptBlock>
                                          <h3>Right Leg Measurements</h3> <a id="copyFromLeftLegAnchor" href="#" style="padding: 0 8px;
                                                                    margin: 2px; background-color: #dddddd; text-decoration: none; color: Black;
                                                                    cursor: pointer;" onclick="copyLeftLegMeasurements(); return false;">Copy Measurements
                                                                    From Left Leg</a> <a id="clearRightLegMeasurementsAnchor" href="#" style="padding: 0 8px; margin: 2px; background-color: #dddddd;
                                                                        text-decoration: none; color: Black; cursor: pointer;" onclick="clearRightLegMeasurements(); return false;">Clear Right Leg Measurements</a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td width="50%" valign="top">
                                          <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                              <td>INSTABILITY</td>
                                              <td>
                                                <asp:RadioButtonList ID="rblInstabilityR" runat="server" RepeatDirection="Horizontal">
                                                  <asp:ListItem Text="None" Value="None" Selected="True"></asp:ListItem>
                                                  <asp:ListItem Text="ACL" Value="ACL"></asp:ListItem>
                                                  <asp:ListItem Text="PCL" Value="PCL"></asp:ListItem>
                                                  <asp:ListItem Text="MCL/LCL" Value="MCL/LCL"></asp:ListItem>
                                                </asp:RadioButtonList>
                                              </td>
                                            </tr>
                                            <tr id="trX2KUnloadingR" runat="server" visible="false">
                                              <td>CounterForce*</td>
                                              <td>
                                                <asp:RadioButtonList ID="rblCounterForceR" runat="server" RepeatDirection="Vertical">
                                                  <asp:ListItem Text="OA - Medial Unloading" Value="OA - Medial Unloading"></asp:ListItem>
                                                  <asp:ListItem Text="OA - Lateral Unloading" Value="OA - Lateral Unloading"></asp:ListItem>
                                                </asp:RadioButtonList>
                                                <div style="padding: 0 7px;">
                                                  <asp:TextBox ID="txtCounterForceDegreesR" runat="server" Width="60px" />°
                                                </div>
                                              </td>
                                            </tr>
                                            <tr id="trFusionUnloadingR" runat="server" visible="false">
                                              <td colspan="2">FUSION Solus OA - Medial unloading only.
                                              </td>
                                            </tr>
                                          </table>
                                        </td>
                                        <td width="50%" valign="top">
                                          <table cellpadding="2" cellspacing="0" width="100%" border="0">
                                            <tr>
                                              <td width="25%" align="right">
                                                1. Thigh Circumference
                                              </td>
                                              <td width="25%">
                                                <asp:TextBox ID="txtThighR" runat="server"></asp:TextBox>
                                              </td>
                                              <td width="25%" align="right">
                                                3. Knee Offset
                                              </td>
                                              <td width="25%">
                                                <asp:TextBox ID="txtKneeOffsetR" runat="server"></asp:TextBox>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td align="right">
                                                2. Calf Circumference
                                              </td>
                                              <td>
                                                <asp:TextBox ID="txtCalfR" runat="server"></asp:TextBox>
                                              </td>
                                              <td align="right">
                                                4. Knee Width
                                              </td>
                                              <td>
                                                <asp:TextBox ID="txtKneeWidthR" runat="server"></asp:TextBox>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td colspan="4">
                                              </td>
                                            </tr>
                                            <tr>
                                              <td colspan="2">
                                                <h4>Extension</h4>
                                                <asp:RadioButtonList ID="rblExtensionR" runat="server" RepeatDirection="Horizontal">
                                                  <asp:ListItem Text="0°" Value="0"></asp:ListItem>
                                                  <asp:ListItem Text="10°" Value="10" Selected="True"></asp:ListItem>
                                                  <asp:ListItem Text="20°" Value="20"></asp:ListItem>
                                                  <asp:ListItem Text="30°" Value="30"></asp:ListItem>
                                                  <asp:ListItem Text="40°" Value="40"></asp:ListItem>
                                                </asp:RadioButtonList>
                                              </td>
                                              <td colspan="2">
                                                <h4>Flexion</h4>
                                                <asp:RadioButtonList ID="rblFlexionR" runat="server" RepeatDirection="Horizontal">
                                                  <asp:ListItem Text="None" Value="None" Selected="True"></asp:ListItem>
                                                  <asp:ListItem Text="45°" Value="45"></asp:ListItem>
                                                  <asp:ListItem Text="60°" Value="60"></asp:ListItem>
                                                  <asp:ListItem Text="75°" Value="75"></asp:ListItem>
                                                  <asp:ListItem Text="90°" Value="90"></asp:ListItem>
                                                </asp:RadioButtonList>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td colspan="4">
                                                <small>* New braces ship with 10° extension stops installed.</small>
                                              </td>
                                            </tr>
                                          </table>
                                        </td>
                                      </tr>
                                    </table>
                                  </div>
                                </td>
                              </tr>
                            </table>
                          </div>
                        </td>
                      </tr>
                    </table>
                  </div>
                </td>
                <td width="2%">
                </td>
              </tr>
              <%--spacer--%>
                <tr>
                  <td colspan="5"> </td>
                </tr>
                <%--Brace Information--%>
                  <tr>
                    <td width="2%">
                    </td>
                    <td colspan="3">
                      <div style="border: 1px solid #CCCCCC;" class="rgEditForm customBraceForm">
                        <table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="white">
                          <%--Fusion Brace Information--%>
                            <tr id="trFusionTitle" runat="server" visible="false">
                              <td colspan="4" style="background-color: #EEEEEE;">
                                <h2>Fusion Brace Information</h2>
                              </td>
                            </tr>
                            <tr id="trFusionBody" runat="server" visible="false">
                              <td colspan="3">
                              
                                <table cellpadding="0" cellspacing="0" border="0" width="100%" >
                                  <%--Leave this border--%>
                                    <tr>
                                      <%--Standard Hinge Information--%>
                                        <td width="30%" valign="top" rowspan="2" class="customFormRow">
                                          <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                              <td colspan="2">
<%--                                                <asp:Label ID="lblStdHingeTitle" runat="server" Text="Standard Hinge"></asp:Label>--%>
                                                <h3>Standard Hinge</h3>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <table cellpadding="5" cellspacing="0" border="0" width="100%">
                                                  <tr>
                                                    <td>
                                                      <asp:Label ID="lblFusion" runat="server"></asp:Label>
                                                    </td>
                                                  </tr>
                                                </table>
                                              </td>
                                              <td valign="top">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                  <tr>
                                                    <td>
                                                      <asp:RadioButtonList ID="rblFusionCode" runat="server" RepeatDirection="Vertical" Enabled="false">
                                                      </asp:RadioButtonList>
                                                    </td>
                                                  </tr>
                                                </table>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td colspan="2">
                                                <br />
                                              </td>
                                            </tr>
                                            <%--FUSION Brace Color--%>
                                              <tr>
                                                <td>
                                                  <br />
                                                </td>
                                              </tr>
                                              <%--Color Enhancement--%>
                                                <tr>
                                                  <td colspan="2">
                                                    <asp:RadioButtonList ID="rblColorPatternEnhancement" runat="server" RepeatDirection="Vertical" Width="299px">
                                                      <asp:ListItem Text="None" Value="None" Selected="True"></asp:ListItem>
                                                      <asp:ListItem Text="Color Enhancement  (03161)" Value="03161"></asp:ListItem>
                                                      <asp:ListItem Text="Pattern Enhancement  (03162)" Value="03162"></asp:ListItem>
                                                      <asp:ListItem Text="Custom Color / Pattern Enhancement  (03163)" Value="03163"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                  </td>
                                                  <td>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td colspan="2" style="height: 30px">
                                                  </td>
                                                </tr>
                                                <tr id="tr1" runat="server" height="30px" valign="bottom">
                                                  <td colspan="2" style="background-color: #EEEEEE;" >
                                                    <h3><asp:Label ID="lblFusionShippingMethod" runat="server" Text="Select your shipping method"></asp:Label></h3>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td colspan="3">
                                                    <%--Fusion Shipping--%>
                                                      <asp:RadioButtonList ID="rblFusionShippingType" runat="server">
                                                        <asp:ListItem Value="5">Saturday</asp:ListItem>
                                                        <asp:ListItem Value="6">Next Day Early AM</asp:ListItem>
                                                        <asp:ListItem Value="7">Next Day AM</asp:ListItem>
                                                        <asp:ListItem Value="1">Next Day</asp:ListItem>
                                                        <asp:ListItem Value="8">Next Day Saver</asp:ListItem>
                                                        <asp:ListItem Value="2">2nd Day</asp:ListItem>
                                                        <asp:ListItem Value="3">3rd Day</asp:ListItem>
                                                        <asp:ListItem Selected="true" Value="4">Ground</asp:ListItem>
                                                      </asp:RadioButtonList>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td colspan="3">
                                                    <asp:RadioButtonList ID="rblFusionShippingCarrier" runat="server">
                                                      <asp:ListItem Value="1" Selected="True">UPS</asp:ListItem>
                                                      <asp:ListItem Value="2">Fed Ex</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                  </td>
                                                </tr>
                                          </table>
                                          
                                        </td>
                                        <%--OA Information--%>
                                        <%--  <td valign="top" align="center" width="15%">
                                          </td>--%>
                                          <%--Note/Special Information--%>
                                            <td valign="top" width="20%" class="customFormRow">
                                              <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                <tr valign="top">
                                                  <td>
                                                    <table cellpadding="0" cellspacing="0" height="150px" border="0">
                                                      <tr>
                                                        <td valign="top">
                                                          <asp:Label ID="lblNotesSpecial" runat="server" Text="NOTES/SPECIAL INSTRUCTIONS"></asp:Label>
                                                          <asp:TextBox ID="txtNotes" runat="server" Width="270px" Height="130px" TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                      </tr>
                                                    </table>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td>
                                                    <h3><asp:Label ID="lblColorTitle" runat="server" Text="Color"></asp:Label></h3>
                                                    <asp:RadioButtonList ID="rblColor" runat="server" RepeatDirection="Horizontal" RepeatColumns="4" Width="299px">
                                                      <asp:ListItem Text="None" Value="None" Selected="True"></asp:ListItem>
                                                      <asp:ListItem Text="Forest" Value="Forest"></asp:ListItem>
                                                      <asp:ListItem Text="Orange" Value="Orange"></asp:ListItem>
                                                      <asp:ListItem Text="Navy" Value="Nave"></asp:ListItem>
                                                      <asp:ListItem Text="Pink" Value="Pink"></asp:ListItem>
                                                      <asp:ListItem Text="Royal" Value="Royal"></asp:ListItem>
                                                      <asp:ListItem Text="Yellow" Value="Yellow"></asp:ListItem>
                                                      <asp:ListItem Text="Sage" Value="Sage"></asp:ListItem>
                                                      <asp:ListItem Text="Custom" Value="Custom"></asp:ListItem>
                                                      <asp:ListItem Text="Red" Value="Red"></asp:ListItem>
                                                      <asp:ListItem Text="Charcoal" Value="Charcoal"></asp:ListItem>
                                                      <asp:ListItem Text="Mauve" Value="Maruve"></asp:ListItem>
                                                      <asp:ListItem Text="Pantone*" Value="Pantone"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td align="right">
                                                    <asp:TextBox ID="txtPantone" runat="server" Width="70px"></asp:TextBox>
                                                  </td>
                                                </tr>
                                                <tr height="35px">
                                                  <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                  <td>
                                                    <h3><asp:Label ID="lblPattern" runat="server" Text="Pattern"></asp:Label></h3>
                                                    <asp:RadioButtonList ID="rblPattern" runat="server" RepeatDirection="Horizontal" RepeatColumns="2">
                                                      <asp:ListItem Text="None" Value="None" Selected="True"></asp:ListItem>
                                                      <asp:ListItem Text="Flames" Value="Flames"></asp:ListItem>
                                                      <asp:ListItem Text="Camouflage" Value="Camouflage"></asp:ListItem>
                                                      <asp:ListItem Text="Flag" Value="Flag"></asp:ListItem>
                                                      <asp:ListItem Text="Custom Pattern*" Value="CustomPattern"></asp:ListItem>
                                                      <asp:ListItem Text="Ripples" Value="Ripples"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:Label ID="lblNotes" runat="server" Text="Notes:" Width="70px"></asp:Label>
                                                    <asp:TextBox ID="txtColorPattern" runat="server"></asp:TextBox>
                                                  </td>
                                                </tr>
                                              </table>
                                            </td>
                                            <%--Fusion Frame Pads / Accessories--%>
                                              <td align="left" valign="top" width="35%" class="customFormRow">
                                                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                  <tr>
                                                    <td>
                                                      <h3><asp:Label ID="lblFusioFramePadsTitle" runat="server" Text="FUSION FRAME PADS"></asp:Label></h3>
                                                      <asp:Label ID="lblFusionFramePadsNote" runat="server" Text="All FUSION braces available with black frame pads only."></asp:Label>
                                                      <br />
                                                      <br />
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                        <tr>
                                                          <td>
                                                            <h3><asp:Label ID="Label8" runat="server" Text="FUSION ACCESSORIES"></asp:Label></h3>
                                                          </td>
                                                        </tr>
                                                        <tr>
                                                          <td>
                                                            <asp:GridView ID="grdFusionAccessories" runat="server" AutoGenerateColumns="false" CssClass="RadGrid_Breg">
                                                              <Columns>
                                                                <asp:TemplateField>
                                                                  <ItemTemplate>
                                                                    <asp:TextBox ID="txtQuantity" runat="server" Text='<%# Bind("Quantity") %>'></asp:TextBox>
                                                                  </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="DisplayName" />
                                                                <asp:TemplateField>
                                                                  <ItemTemplate>
                                                                    <asp:HiddenField ID="MasterCatalogProductID" runat="server" Value='<%# Bind("MasterCatalogProductID") %>' />
                                                                  </ItemTemplate>
                                                                </asp:TemplateField>
                                                              </Columns>
                                                            </asp:GridView>
                                                            <asp:CheckBoxList ID="chkFusionAccessories" runat="server" RepeatDirection="Vertical">
                                                            </asp:CheckBoxList>
                                                          </td>
                                                        </tr>
                                                      </table>
                                                    </td>
                                                  </tr>
                                                </table>
                                              </td>
                                    </tr>
                                </table>
                        
                                   </td>
                            </tr>
                            <%--X2K Brace Information --%>
                              <tr id="trX2KAll" runat="server" visible="false">
                                <td colspan="3">
                                  <table border="1" cellpadding="2" cellspacing="0" width="100%" bgcolor="white">
                                    <%--X2K--%>
                                      <tr id="trX2KTitle" runat="server">
                                        <td colspan="6" style="background-color: #EEEEEE;">
                                          <h2>
                                                    <asp:Label ID="Label7" runat="server" Text=" X2K Brace Information"></asp:Label></h2>
                                        </td>
                                      </tr>
                                      <tr id="trX2KBody" runat="server" style="display: none">
                                        <td colspan="3">
                                          <table cellpadding="0" cellspacing="0" border="1" width="100%">
                                            <%--Leave this border--%>
                                              <tr>
                                                <%--Standard Hinge Information--%>
                                                  <td width="30%" valign="top" rowspan="2">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                      <tr>
                                                        <td colspan="2">
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td>
                                                          <table cellpadding="5" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                              <td>
                                                                <asp:Label ID="lblX2K" runat="server"></asp:Label>
                                                              </td>
                                                            </tr>
                                                          </table>
                                                        </td>
                                                        <td valign="top">
                                                          <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                              <td>
                                                                <asp:RadioButtonList ID="rblX2KCode" runat="server" RepeatDirection="Vertical" Enabled="false">
                                                                </asp:RadioButtonList>
                                                              </td>
                                                            </tr>
                                                          </table>
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2">
                                                          <br />
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2">
                                                          <br />
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2" style="background-color: #EEEEEE;">
                                                          <h3><asp:Label ID="Label11" runat="server" Text="X2K Frame Pad Colors"></asp:Label></h3>
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2">
                                                          <asp:Label ID="Label12" runat="server" Text="Please choose 2 colors (Unless specified, braces will come with standard colors as noted):"></asp:Label>
                                                          <br />
                                                          <br />
                                                          <div>X2K: <strong>Black &amp; Black</strong></div>
                                                          <div>Women's X2K: <strong>Black &amp; Silver</strong></div>
                                                          <div>Compact X2K: <strong>Black &amp; Black</strong></div>
                                                          <div>X2K Unlimited: <strong>Black &amp; Black</strong></div>
                                                          <br />
                                                          <br />
                                                          <asp:CheckBoxList ID="chkX2KColor" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Text="None" Value="None" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="Silver" Value="silver"></asp:ListItem>
                                                            <asp:ListItem Text="Black" Value="black"></asp:ListItem>
                                                            <asp:ListItem Text="Navy" Value="navy"></asp:ListItem>
                                                            <asp:ListItem Text="Forest" Value="forest"></asp:ListItem>
                                                            <asp:ListItem Text="Burgundy" Value="burgundy"></asp:ListItem>
                                                          </asp:CheckBoxList>
                                                          <br />
                                                          <br />
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2">
                                                          <br />
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td>
                                                          <br />
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2">
                                                        </td>
                                                        <td>
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2" style="height: 30px">
                                                        </td>
                                                      </tr>
                                                      <tr id="tr3" runat="server" height="30px" valign="bottom">
                                                        <td colspan="2" style="background-color: #EEEEEE; ">
                                                          <h3><asp:Label ID="Label3" runat="server" Text="Select your shipping method"></asp:Label></h3>
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="3">
                                                          <%--Fusion Shipping--%>
                                                            <asp:RadioButtonList ID="rblX2KShippingType" runat="server">
                                                              <asp:ListItem Value="5">Saturday</asp:ListItem>
                                                              <asp:ListItem Value="6">Next Day Early AM</asp:ListItem>
                                                              <asp:ListItem Value="7">Next Day AM</asp:ListItem>
                                                              <asp:ListItem Value="1">Next Day</asp:ListItem>
                                                              <asp:ListItem Value="8">Next Day Saver</asp:ListItem>
                                                              <asp:ListItem Value="2">2nd Day</asp:ListItem>
                                                              <asp:ListItem Value="3">3rd Day</asp:ListItem>
                                                              <asp:ListItem Selected="true" Value="4">Ground</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="3">
                                                          <asp:RadioButtonList ID="rblX2KShippingCarrier" runat="server">
                                                            <asp:ListItem Value="1" Selected="True">UPS</asp:ListItem>
                                                            <asp:ListItem Value="2">Fed Ex</asp:ListItem>
                                                          </asp:RadioButtonList>
                                                        </td>
                                                      </tr>
                                                    </table>
                                                  </td>
                                                  <td valign="top" align="center" width="15%">
                                                  </td>
                                                  <td valign="top" width="20%">
                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                      <tr valign="top">
                                                        <td>
                                                          <table cellpadding="0" cellspacing="0" height="150px" border="0">
                                                            <tr>
                                                              <td valign="top">&nbsp;</td>
                                                            </tr>
                                                          </table>
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td>&nbsp;</td>
                                                      </tr>
                                                      <tr>
                                                        <td align="right">&nbsp;</td>
                                                      </tr>
                                                      <tr height="35px">
                                                        <td>&nbsp;</td>
                                                      </tr>
                                                      <tr>
                                                        <td>&nbsp;</td>
                                                      </tr>
                                                    </table>
                                                  </td>
                                                  <td align="left" valign="top" width="35%">
                                                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                      <tr>
                                                        <td>
                                                          <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                                            <tr>
                                                              <td style="background-color: #EEEEEE;">
                                                                <h3><asp:Label ID="Label13" runat="server" Text="X2K Accessories"></asp:Label></h3>
                                                              </td>
                                                            </tr>
                                                            <tr>
                                                              <td>
                                                                <asp:GridView ID="grdX2KAccessories" runat="server" AutoGenerateColumns="false" BorderStyle="None" BorderWidth="0px" CssClass="RadGrid_Breg">
                                                                  <Columns>
                                                                    <asp:TemplateField ControlStyle-Width="30px" HeaderStyle-Width="30px">
                                                                      <HeaderTemplate>
                                                                        <asp:Label ID="lblQuantity" runat="server" Text="Qty"></asp:Label>
                                                                      </HeaderTemplate>
                                                                      <ItemTemplate>
                                                                        <asp:TextBox ID="txtQuantity" Width="5px" runat="server" Text='<%# Bind("Quantity") %>'></asp:TextBox>
                                                                      </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="DisplayName" HeaderText="Product" />
                                                                    <asp:TemplateField>
                                                                      <ItemTemplate>
                                                                        <asp:HiddenField ID="MasterCatalogProductID" runat="server" Value='<%# Bind("MasterCatalogProductID") %>' />
                                                                      </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                  </Columns>
                                                                </asp:GridView>
                                                                <asp:CheckBoxList ID="cblX2KAccessories" runat="server" RepeatDirection="Vertical">
                                                                </asp:CheckBoxList>
                                                              </td>
                                                            </tr>
                                                          </table>
                                                        </td>
                                                      </tr>
                                                    </table>
                                                  </td>
                                              </tr>
                                          </table>
                                        </td>
                                      </tr>
                                  </table>
                                </td>
                              </tr>
                        </table>
                      </div>
                    </td>
                    <td width="2%">&nbsp;</td>
                  </tr>
      </table>
    </div>
  </div>
</div>
