﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Collections.Specialized;
using BLL.PDFUtils;

namespace BregVision
{
	/// <summary>	
	/// Summary description for CustomBracePDFHandler
	/// </summary>
	public partial class CustomBracePDFHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
	{
		private bool _Flatten = false;
		private string _DisplayType = "PDF";
		private int _ImageDPI = 0;
		private int _BregVisionOrderID = 0;
		private int _PracticeID = 0;
		private int _PracticeLocationID = 0;
		private int _ShoppingCartID = 0;
		private int _Page = 0;

		public CustomBracePDFHandler()
		{
		}

		public void ProcessRequest(HttpContext context)
		{
			var request = context.Request;
			var response = context.Response;

			_PracticeID = Convert.ToInt32(context.Session["PracticeID"]);
			_PracticeLocationID = Convert.ToInt32(context.Session["PracticeLocationID"]);

			NameValueCollection queryString = context.Request.QueryString;
			bool foundSubmit = false;
			bool foundUseDBFdf = false;
			foreach (string key in queryString)
			{
				switch (key.ToUpper())
				{
					case "SUBMIT":
						{
							foundSubmit = true;
							break;
						}

					case "USEDBFDF":
						{
							foundUseDBFdf = true;
							break;
						}

					case "BREGVISIONORDERID":
						{
							string[] values = queryString.GetValues(key);
							if (values.Length > 0)
							{
								try
								{
									_BregVisionOrderID = CustomBracePDFUtils.ToInt(values[0]);
								}
								catch (Exception ex)
								{

								}
							}
							break;
						}

					case "SHOPPINGCARTID":
						{
							string[] values = queryString.GetValues(key);
							if (values.Length > 0)
							{
								try
								{
									_ShoppingCartID = CustomBracePDFUtils.ToInt(values[0]);
								}
								catch (Exception ex)
								{

								}
							}
							break;
						}

					case "FLATTEN":
						{
							string[] values = queryString.GetValues(key);
							if (values.Length > 0)
							{
								try
								{
									_Flatten = CustomBracePDFUtils.ToBool(values[0]);
								}
								catch (Exception ex)
								{

								}
							}
							break;
						}

					case "DISPLAYTYPE":
						{
							string[] values = queryString.GetValues(key);
							if (values.Length > 0)
							{
								try
								{
									_DisplayType = CustomBracePDFUtils.ToString(values[0]).ToUpper();
								}
								catch (Exception ex)
								{

								}
							}
							break;
						}

					case "DPI":
						{
							string[] values = queryString.GetValues(key);
							if (values.Length > 0)
							{
								try
								{
									_ImageDPI = CustomBracePDFUtils.ToInt(values[0]);
								}
								catch (Exception ex)
								{

								}
							}
							break;
						}

					case "PAGE":
						{
							string[] values = queryString.GetValues(key);
							if (values.Length > 0)
							{
								try
								{
									_Page = CustomBracePDFUtils.ToInt(values[0]);
								}
								catch (Exception ex)
								{

								}
							}
							break;
						}
				}
			}
			if (_DisplayType == "JPG" || _DisplayType == "PNG")
				_Flatten = true;

			if (foundSubmit)
			{
				if (!foundUseDBFdf)
				{
					Dictionary<string, string> fdf = PDFHelper.ParseFdf(request.InputStream);
					DataSet dsMappedPDFInfo = DAL.PracticeLocation.Inventory.GetMappedPDFInfo(true);
					CustomBracePDFUtils.SavePDFData(_PracticeLocationID, _ShoppingCartID, fdf, _Flatten);
				}

				string userAgent = context.Request.UserAgent;
				if (foundUseDBFdf || (userAgent != null && (userAgent.IndexOf("Edge") > -1 || userAgent.IndexOf("MSIE") > -1 || userAgent.IndexOf(") like Gecko") > -1)))
				{
					context.Response.Redirect("~/InvoiceSnapshot.aspx", false);
				}
			}
			else
			{
				byte[] pdfBytes = CustomBracePDFUtils.BuildPDF(
					_PracticeID, _PracticeLocationID, _BregVisionOrderID, _ShoppingCartID, _ImageDPI, _Flatten, _DisplayType, _Page);
				response.ClearContent();
				switch (_DisplayType)
				{
					case "PDF":
						response.ContentType = "application/pdf";
						break;

					case "JPG":
						response.ContentType = "image/jpeg";
						break;

					case "PNG":
						response.ContentType = "image/png";
						break;
				}
				response.AddHeader("Content-Disposition", "inline");
				response.AddHeader("Content-Length", pdfBytes.Length.ToString());
				response.BinaryWrite(pdfBytes);
				response.End();
			}
		}


		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}