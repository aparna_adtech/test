﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomBracePDF.ascx.cs" Inherits="BregVision.UserControls.DispensePage.CustomBracePDF" %>

<style>
	.FormDiv {
		height: -moz-calc(100% - 180px);
		height: -webkit-calc(100% - 180px);
		height: calc(100% - 230px);
		display: block;
	}
	.groupFieldset {
		height: -moz-calc(100%-15px);
		height: -webkit-calc(100%-15px);
		height:calc(100% - 15px);
		vertical-align:top;
		padding:8px 12px;
		border: 2px solid;
	}
	.groupDiv {
		margin:7px;
	}
	.groupPanel {
		display:flex;
		flex-wrap:wrap;
		padding:5px
	}
	.groupFieldset legend {
		font-size: 120%;
	}
</style>

<asp:HiddenField ID="ParentShoppingCartID" runat="server" />

<div class="flex-row subWelcome-bar">
	<h1 class="flex-row align-center justify-start">Purchase Order&nbsp;&nbsp;
		<small class="light">Shipping, Billing, and Shipping Method</small>
	</h1>
  <asp:Button ID="btnNext" runat="server" Text="Next" OnClick="btnNext_Click" CssClass="button-primary" />
</div>

<h2 class="flex-row align-center justify-start" style="margin-left:10px">Custom Brace Form</h2>
<div class="FormDiv" style="margin-left:20px; overflow-y: scroll">
	<asp:Panel ID="pnlFormControls" CssClass="" runat="server"></asp:Panel>
</div>

<%--<div class="subTabContent-container" style="overflow-x: hidden; overflow-y: hidden;">
	<object style="width: 100%; height: 100%" data="<%=BLL.PDFUtils.CustomBracePDFUtils.RootURL %>/UserControls/DispensePage/CustomBracePDFHandler.ashx?shoppingcartid=<%= ShoppingCartID %>&displaytype=PDF"></object>
</div>--%>
