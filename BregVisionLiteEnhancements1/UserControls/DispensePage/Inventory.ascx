﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Inventory.ascx.cs" Inherits="BregVision.UserControls.DispensePage.Inventory" %>
<table width="100%">
    <tr>
        <td width="5">
        </td>
        <td>
            <asp:Label ID="lblBrowseDispense" runat="server" CssClass="WarningMsg"></asp:Label>&#160;
        </td>
        <td align="right">
            <asp:Button ID="btnAddBrowseToDispensement" runat="server" OnClick="btnAddBrowseToDispensement_Click"
                 Text="Add to Dispensement" ToolTip="Add all products selected to dispensement on Summary/Dispense tab."
                Width="135px" />
            <bvu:Help ID="Help1" runat="server" HelpID="77" />
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            &#160;
            <table>
                <tr>
                    <td>
                        <bvu:SearchToolbar ID="ctlSearchToolbar" runat="server" />
                    </td>
                    <td>
                        <bvu:Help ID="Help2" runat="server" HelpID="78" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" colspan="4">
            <bvu:VisionRadGridAjax ID="grdInventory" runat="server" 
                    AllowMultiRowSelection="True"
                    OnItemCommand="grdInventory_ItemCommand" 
                    OnItemDataBound="grdInventory_ItemDataBound"
                    OnNeedDataSource="grdInventory_NeedDataSource" 
                    OnPageIndexChanged="grdInventory_PageIndexChanged"
                    OnPreRender="grdInventory_PreRender" 
                    OnDetailTableDataBind="grdInventory_OnDetailTableDataBind" 
                    Width="100%">
                <MasterTableView DataKeyNames="SupplierID"
                    HierarchyLoadMode="ServerOnDemand" Name="mtvSupplier" Width="100%">
                    <Columns>
                        <telerik:GridBoundColumn DataField="SupplierID" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                            HeaderStyle-BorderWidth="1px" UniqueName="SupplierID" Visible="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Brand" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                            HeaderStyle-BorderWidth="1px" HeaderText="Brand" UniqueName="Brand">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Name" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                            HeaderStyle-BorderWidth="1px" HeaderText="Name" UniqueName="Name">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PhoneWork" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                            HeaderStyle-BorderWidth="1px" HeaderText="Phone" UniqueName="PhoneWork">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Address" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                            HeaderStyle-BorderWidth="1px" HeaderText="Address" UniqueName="Address">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="City" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                            HeaderStyle-BorderWidth="1px" HeaderText="City" UniqueName="City">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ZipCode" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                            HeaderStyle-BorderWidth="1px" HeaderText="Zip Code" UniqueName="ZipCode">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <DetailTables>
                        <telerik:GridTableView runat="server" DataKeyNames="SupplierName"
                            Name="gvwSupplier" Width="100%" BorderColor="#A2B6CB" BorderWidth="1px" BorderStyle="Solid">
                            <ParentTableRelation>
                                <telerik:GridRelationFields DetailKeyField="SupplierID" MasterKeyField="SupplierID" />
                            </ParentTableRelation>
                            <Columns>
                                <telerik:GridClientSelectColumn HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                                    HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="35px"
                                    ItemStyle-HorizontalAlign="Center" UniqueName="ClientSelectColumn">
                                </telerik:GridClientSelectColumn>
                                <telerik:GridBoundColumn DataField="SupplierID" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                                    HeaderStyle-BorderWidth="1px" UniqueName="SupplierID" Visible="False">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ProductInventoryID" HeaderStyle-BorderColor="#A2B6CB"
                                    HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" UniqueName="ProductInventoryID"
                                    Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DMEDeposit" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                                    HeaderStyle-BorderWidth="1px" UniqueName="DMEDeposit" Visible="False">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                                    HeaderStyle-BorderWidth="1px" HeaderStyle-Width="20" HeaderStyle-Wrap="true"
                                    ItemStyle-HorizontalAlign="Center" UniqueName="IsMedicare">
                                    <HeaderTemplate>
                                        <asp:Label ID="Label2" runat="server" Text="Is Medicare"></asp:Label>
                                        <bvu:Help ID="Help3" runat="server" HelpID="81" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkIsMedicare" runat="server" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                                    HeaderStyle-BorderWidth="1px" HeaderStyle-Width="20" HeaderStyle-Wrap="true"
                                    ItemStyle-HorizontalAlign="Center" UniqueName="ABNForm">
                                    <HeaderTemplate>
                                        <asp:Label ID="Label3" runat="server" Text="ABN Form"></asp:Label>
                                        <bvu:Help ID="Help4" runat="server" HelpID="82" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkABNForm" runat="server" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="ABN_Needed" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                                    HeaderStyle-BorderWidth="1px" HeaderText="ABN_Needed" UniqueName="ABN_Needed"
                                    Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="SupplierName" HeaderStyle-BorderColor="#A2B6CB"
                                    HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderText="Supplier"
                                    UniqueName="Category" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="BrandName" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                                    HeaderStyle-BorderWidth="1px" HeaderText="Brand" UniqueName="SubCategory">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ProductName" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                                    HeaderStyle-BorderWidth="1px" HeaderText="Product" UniqueName="Product">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Code" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                                    HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50"
                                    HeaderStyle-Wrap="true" HeaderText="Code" ItemStyle-HorizontalAlign="center"
                                    ItemStyle-Width="50" UniqueName="Code">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Side" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                                    HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50"
                                    HeaderStyle-Wrap="true" HeaderText="Side" ItemStyle-HorizontalAlign="center"
                                    ItemStyle-Width="50" UniqueName="Side">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Size" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                                    HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50"
                                    HeaderStyle-Wrap="true" HeaderText="Size" ItemStyle-HorizontalAlign="center"
                                    ItemStyle-Width="50" UniqueName="Size">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Gender" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                                    HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50"
                                    HeaderStyle-Wrap="true" HeaderText="Gender" ItemStyle-HorizontalAlign="center"
                                    ItemStyle-Width="50" UniqueName="Gender">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="QuantityOnHand" HeaderStyle-BorderColor="#A2B6CB"
                                    HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="50" HeaderStyle-Wrap="true" HeaderText="Qty. On Hand" ItemStyle-HorizontalAlign="center"
                                    ItemStyle-Width="50" UniqueName="QuantityOnHand">
                                </telerik:GridBoundColumn>
                            </Columns>
                        </telerik:GridTableView>
                    </DetailTables>
                </MasterTableView>
            </bvu:VisionRadGridAjax>
        </td>
    </tr>
</table>
