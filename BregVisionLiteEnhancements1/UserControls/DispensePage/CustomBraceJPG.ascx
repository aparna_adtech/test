﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomBraceJPG.ascx.cs" Inherits="BregVision.UserControls.DispensePage.CustomBraceJPG" %>

<style>
	@media print {
		.page-break {
			display: block;
			page-break-before: always;
		}
	}
</style>

<asp:HiddenField ID="ParentBregVisionOrderID" runat="server" />
<asp:HiddenField ID="ParentShoppingCartID" runat="server" />

<div class="page-break">
	<img style="width: 100%; height: 100%" src="<%=BLL.PDFUtils.CustomBracePDFUtils.RootURL %>/UserControls/DispensePage/CustomBracePDFHandler.ashx?bregvisionorderid=<%= BregVisionOrderID %>&shoppingcartid=<%= ShoppingCartID %>&displaytype=JPG&page=1" />
</div>
<div class="page-break">
	<img style="width: 100%; height: 100%" src="<%=BLL.PDFUtils.CustomBracePDFUtils.RootURL %>/UserControls/DispensePage/CustomBracePDFHandler.ashx?bregvisionorderid=<%= BregVisionOrderID %>&shoppingcartid=<%= ShoppingCartID %>&displaytype=JPG&page=2" />
</div>
