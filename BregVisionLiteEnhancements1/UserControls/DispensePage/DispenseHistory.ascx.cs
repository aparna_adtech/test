﻿using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.TelerikAjax;
using BregVision.UserControls.General;
using ClassLibrary.DAL;
using Telerik.Web.UI;

namespace BregVision.UserControls.DispensePage
{
    public partial class DispenseHistory : Bases.UserControlBase
    {
        #region Private Variables

        private Dispense DispensePage;

        #endregion

        #region Properties

        public RadGrid Grid
        {
            get
            {
                return grdReceiptHistory;
            }
        }

        public Label StatusLabel
        {
            get
            {
                return lblReceiptHistory;
            }
        }


        public General.SearchToolbar SearchToolbarControl
        {
            get
            {
                return ctlSearchToolbar;
            }
        }

        #endregion

        #region Page Events

        new protected void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);

            DispensePage = ((Dispense)this.Page);

            ctlSearchToolbar.Grid = grdReceiptHistory;
            ctlSearchToolbar.DefaultLoadingPanel = DispensePage.DefaultLoadingPanel;
            ctlSearchToolbar.searchEvent += new SearchEventHandler(ctlSearchToolbar_searchEvent);

            ctlSearchToolbar.ClearDropdownButtons();
            ctlSearchToolbar.AddButton("Patient Code", "PatientCode");
            ctlSearchToolbar.AddButton("Physician Name", "PhysicianName");
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();
            if (grdReceiptHistory.HasInitiallyBound == false)
            {
                grdReceiptHistory.SetGridLabel("", Page.IsPostBack, lblReceiptHistory, true);//set the default text on the label
                grdReceiptHistory.PageSize = recordsDisplayedPerPage;
                grdReceiptHistory.InitializeSearch();
            }



        }


        #endregion

        private void InitializeAjaxSettings()
        {
            Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);

            radAjaxManager.AjaxSettings.AddAjaxSetting(grdReceiptHistory, grdReceiptHistory);
            radAjaxManager.AjaxSettings.AddAjaxSetting(ctlSearchToolbar.Toolbar, grdReceiptHistory, DispensePage.DefaultLoadingPanel);
            radAjaxManager.AjaxSettings.AddAjaxSetting(btnPrintReceiptHistory, grdReceiptHistory, DispensePage.DefaultLoadingPanel);
            radAjaxManager.AjaxSettings.AddAjaxSetting(btnPrintReceiptHistory, lblReceiptHistory);
            radAjaxManager.AjaxSettings.AddAjaxSetting(btnPrintReceiptHistory, DispensePage.DefaultWindowManager);
            radAjaxManager.AjaxSettings.AddAjaxSetting(ctlSearchToolbar.Toolbar, lblReceiptHistory);
        }

        void ctlSearchToolbar_searchEvent(object sender, SearchEventArgs e)
        {
            grdReceiptHistory.SearchText = e.SearchText;
            grdReceiptHistory.SearchCriteria = e.SearchCriteria;
            grdReceiptHistory.SearchType = e.SearchType;
            grdReceiptHistory.SetSearchCriteria(e.SearchType);

            lblReceiptHistory.Text = "";
        }

        #region Grid Events

        protected void grdReceiptHistory_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
        {

        }
        protected void grdReceiptHistory_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {

            if (DispensePage.MainTabStrip.SelectedIndex == 2)//Only load if on the receipt history tab
            {
                string searchCriteria = grdReceiptHistory.SearchCriteria;
                string searchText = grdReceiptHistory.SearchText;

                if (!e.IsFromDetailTable)
                {
                    grdReceiptHistory.DataSource = DAL.PracticeLocation.Receipt.GetReceiptHistoryParent(practiceLocationID, searchCriteria, searchText);

                    if (searchCriteria == "Batch")
                    {
                        DataSet dsReceiptHistory = grdReceiptHistory.DataSource as DataSet;
                        var dsHistoryCount = (from hist in dsReceiptHistory.Tables[0].AsEnumerable()
                                              select hist).Count();
                        grdReceiptHistory.PageSize = dsHistoryCount + 5;

                    }
                    else
                    {
                        grdReceiptHistory.PageSize = recordsDisplayedPerPage;
                    }

                }
                grdReceiptHistory.MasterTableView.DetailTables[0].DataSource = DAL.PracticeLocation.Receipt.GetAllReceiptHistoryDataByLocation(practiceLocationID, searchCriteria, searchText);

            }
        }

        protected void grdReceiptHistory_OnDetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {
            TelerikAjax.UIHelper.FilterOnJoinKey(e, "DispenseID", false);
        }

        #endregion

        #region Control Events

        protected void btnPrintReceiptHistory_Click(object sender, EventArgs e)
        {
            Boolean isItem = PopupSelectedReceipt(grdReceiptHistory.MasterTableView);
            if (!isItem)
            {
                lblReceiptHistory.ForeColor = System.Drawing.Color.Red;
                lblReceiptHistory.Text = "No receipt selected. Please choose a receipt and press the 'Print Preview' button. ";
            }
            else
            {
                lblReceiptHistory.ForeColor = System.Drawing.Color.Black;
                lblReceiptHistory.Text = "Please choose a receipt and press the 'Print Preview' button. ";
            }
        }

        #endregion

        #region Methods

        public void SelectLastDispenseBatchInReceiptHistory(int DispenseBatchID)
        {

            //set search criteria to Batch and SearchText to BatchID
            ViewState["grdReceiptHistory_SearchCriteria"] = "Batch";
            ViewState["grdReceiptHistory_SearchText"] = DispenseBatchID.ToString();

            grdReceiptHistory.Rebind();

            var selectedDataItems = (from GridDataItem receiptHistoryItem in (grdReceiptHistory.Items as IEnumerable)
                                     where receiptHistoryItem.GetColumnInt32("DispenseBatchID") == DispenseBatchID
                                     select receiptHistoryItem).Update(rhi => { rhi.Selected = true; });

        }

        private Boolean PopupSelectedReceipt(GridTableView gridTableView)
        {
            var selectedDataItems = from GridDataItem receiptHistoryItem in (gridTableView.Items as IEnumerable)
                                    where receiptHistoryItem.Selected == true
                                    select receiptHistoryItem.GetColumnInt32("DispenseID");

            int[] dispenseIDs = selectedDataItems.ToArray<int>();
            var strDispenseIDs = String.Join("|", dispenseIDs.Select(x => x.ToString()).ToArray()); //join the dispense ids with a pipe ex. 32|23|2
            if (selectedDataItems.Count() > 0)
            {
                string strURL = String.Concat("Authentication/DispenseReceipt.aspx?DispenseID=", Server.UrlEncode(strDispenseIDs), "&PLID=", practiceLocationID.ToString(), "&PLName=", Session["PracticeLocationName"].ToString(), "&PracticeName=", practiceName);
                ResponseHelper.Redirect(strURL, "_blank", "");
                //RadAjaxPanel2.EnableAJAX = true;
                return true;
            }
            return false;
        }

        #endregion

    }
}