﻿using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary.DAL;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace BregVision.UserControls.DispensePage
{
    public partial class CustomBraceJPG : Bases.UserControlBase
    {
		#region properties

		public int? BregVisionOrderID
		{
			get
			{
				string bvoID = ParentBregVisionOrderID.Value;
				int bvoIDVal = 0;
				if (Int32.TryParse(bvoID, out bvoIDVal))
					return bvoIDVal;
				return 0;
			}
			set
			{
				ParentBregVisionOrderID.Value = value.ToString();
			}
		}

		public int? ShoppingCartID
		{
			get
			{
				string shoppingCartID = ParentShoppingCartID.Value;
				int shoppingCartIDVal = 0;
				if (Int32.TryParse(shoppingCartID, out shoppingCartIDVal))
					return shoppingCartIDVal;
				return 0;
			}
			set
			{
				ParentShoppingCartID.Value = value.ToString();
			}
		}

		public bool IsReadOnly
		{
			get
			{
				return true;
			}
			set
			{
			}
		}

		#endregion

		protected void Page_Load(object sender, EventArgs e)
        {
		}

		protected void Page_PreRender(object sender, EventArgs e)
        {
        }
    }
}