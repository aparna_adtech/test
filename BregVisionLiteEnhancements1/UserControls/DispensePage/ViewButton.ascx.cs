﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.WebControls;

namespace BregVision.UserControls.DispensePage
{
    public delegate void ViewDispenseQueueEventHandler(object sender, EventArgs e);

    public partial class ViewButton : Bases.UserControlBase
    {
        public event ViewDispenseQueueEventHandler ViewDispenseQueueEvent;

        public Button btnShowQueueButton
		{
			get { return btnShowQueue; }
		}

        //public RadAjaxTimer tmrDelayShowQueueRadAjaxTimer
        //{
        //    get { return tmrDelayShowQueue; }
        //}

        public Timer AjaxTimerTimer
        {
            get { return AjaxTimer; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void ProcessEvent()
        {
            if (ViewDispenseQueueEvent != null)
            {
                EventArgs viewEventArgs = new EventArgs();
                ViewDispenseQueueEvent(this, viewEventArgs);
            }
            lblMessage.Visible = false;
            btnShowQueue.Visible = false;
        }

        protected void btnShowQueue_OnClick(object sender, EventArgs e)
        {
            ProcessEvent();
        }

        protected void AjaxTimer_OnTick(object sender, EventArgs e)
        {
            AjaxTimer.Enabled = false;

            ProcessEvent();
        }

        //protected void tmrDelayShowQueue_OnTick(object sender, Telerik.WebControls.TickEventArgs e)
        //{
        //    tmrDelayShowQueue.Stop();
        //    tmrDelayShowQueue.Visible = false;

        //    ProcessEvent();

        //}
    }
}