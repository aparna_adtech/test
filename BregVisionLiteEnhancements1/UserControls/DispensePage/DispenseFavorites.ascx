﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DispenseFavorites.ascx.cs" Inherits="BregVision.UserControls.DispensePage.DispenseFavorites" %>
<%@ Register Src="~/UserControls/General/PracticePhysicians.ascx" TagName="PracticePhysicians" TagPrefix="bvu" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="grdProducts">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdProducts" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnAddToDispensement">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdProducts" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" InitialDelayTime="500" EnableAjaxSkinRendering="True" BackgroundPosition="Center" Skin="Breg" EnableEmbeddedSkins="False">
    <img src="App_Themes/Breg/images/Common/loading.gif" />
  </telerik:RadAjaxLoadingPanel>

<telerik:RadSlidingZone ID="radDockZone" runat="server" Height="100%" ClickToOpen="true">
    <telerik:RadSlidingPane ID="rdFavorites" runat="server" Text="Top 10" DefaultCommands="ExpandCollapse" Title="Top 10" IconUrl="~/Images/Favorites.jpg" Width="620" >
    
        <bvu:PracticePhysicians ID="ctlPracticePhysicians" runat="server" /> 
        <br />               
        <bvu:VisionRadGridAjax AutoGenerateColumns="False" ID="grdProducts" Width="600" ShowGroupPanel="false"
            DataSourceID="LinqDataSource1" 
            OnItemCommand="grdProducts_ItemCommand" 
            EnableViewState="true"
            runat="server" 
            ClientSettings-Selecting-AllowRowSelect="true" Skin="WebBlue">
            <MasterTableView DataKeyNames="PracticeCatalogProductID,ProductInventoryID,DMEDeposit,Side" >
                <Columns>
                    <telerik:GridClientSelectColumn ItemStyle-Wrap="false" />
                    <telerik:GridButtonColumn ButtonType="PushButton" CommandName="addtoqueue" ItemStyle-Wrap="false" Text="Add" />
                    <telerik:GridBoundColumn HeaderText="Code" DataField="Code" UniqueName="Code" />
                    <telerik:GridBoundColumn HeaderText="ProductName" DataField="ProductName" UniqueName="ProductName" HeaderStyle-Width="400px" ItemStyle-Width="400px" />
                    <telerik:GridBoundColumn HeaderText="On Hand" DataField="Qty" UniqueName="Qty" />
                    <telerik:GridBoundColumn HeaderText="Billing Charge" DataField="BillingCharge" UniqueName="BillingCharge" DataType="System.Decimal" DataFormatString="{0:C}" />
                </Columns>
            </MasterTableView>
        </bvu:VisionRadGridAjax>
        <br />
        <asp:Button ID="btnAddToDispensement" runat="server" OnClick="btnToDispensement_Click"
            Text="Add to Dispensement" ToolTip="Add all products selected to dispensement on Summary/Dispense tab."
            Width="140px" />
        <bvu:Help ID="Help7" runat="server" HelpID="77" />
    </telerik:RadSlidingPane>
</telerik:RadSlidingZone>
    <asp:LinqDataSource AutoPage="true" ID="LinqDataSource1" runat="server" ContextTypeName="ClassLibrary.VisionDataContext" onselecting="LinqDataSource1_Selecting">
    </asp:LinqDataSource>