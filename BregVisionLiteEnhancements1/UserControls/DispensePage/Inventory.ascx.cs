﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregVision.TelerikAjax;
using BregVision.UserControls.General;
using Telerik.Web.UI;

namespace BregVision.UserControls.DispensePage
{
    public partial class Inventory : Bases.UserControlBase
    {
        #region Private Variables

        private Dispense DispensePage;
        private bool _CurrentUserIsAllowedZeroQtyDispense = false;

        #endregion

        #region Properties

        public RadGrid Grid
        {
            get
            {
                return grdInventory;
            }
        }

        public Label StatusLabel
        {
            get
            {
                return lblBrowseDispense;
            }
        }

        public Button AddToDispensement
        {
            get
            {
                return btnAddBrowseToDispensement;
            }
        }

        public General.SearchToolbar SearchToolbarControl 
        {
            get
            {
                return ctlSearchToolbar;
            }
        }

        #endregion


        #region Page Events

        new protected void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);

            DispensePage = ((Dispense)this.Page);

            ctlSearchToolbar.Grid = grdInventory;
            ctlSearchToolbar.DefaultLoadingPanel = DispensePage.DefaultLoadingPanel;
            ctlSearchToolbar.searchEvent += new SearchEventHandler(ctlSearchToolbar_searchEvent);

            ctlSearchToolbar.ClearDropdownButtons();
            ctlSearchToolbar.AddButton("Product Code", "Code");
            ctlSearchToolbar.AddButton("Product Name", "Name");
            ctlSearchToolbar.AddButton("Manufacturer", "Brand");
            ctlSearchToolbar.AddButton("HCPCs", "HCPCs");
        }



        
        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();
            BLL.Practice.User user = new BLL.Practice.User();
            _CurrentUserIsAllowedZeroQtyDispense = user.IsAllowedZeroQtyDispense(Context.User.Identity.Name.ToString());

            if (grdInventory.HasInitiallyBound == false)
            {
                grdInventory.SetGridLabel("", Page.IsPostBack, lblBrowseDispense, true);//set the default text on the label
                grdInventory.PageSize = recordsDisplayedPerPage;
                grdInventory.InitializeSearch();
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {

        }

        #endregion


        private void InitializeAjaxSettings()
        {
            Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);

            radAjaxManager.AjaxSettings.AddAjaxSetting(grdInventory, grdInventory, DispensePage.DefaultLoadingPanel);
            radAjaxManager.AjaxSettings.AddAjaxSetting(ctlSearchToolbar.Toolbar, grdInventory, DispensePage.DefaultLoadingPanel);
            radAjaxManager.AjaxSettings.AddAjaxSetting(btnAddBrowseToDispensement, grdInventory, DispensePage.DefaultLoadingPanel);
            radAjaxManager.AjaxSettings.AddAjaxSetting(btnAddBrowseToDispensement, lblBrowseDispense);
            radAjaxManager.AjaxSettings.AddAjaxSetting(ctlSearchToolbar.Toolbar, lblBrowseDispense);
        }

        void ctlSearchToolbar_searchEvent(object sender, SearchEventArgs e)
        {
            grdInventory.SearchText = e.SearchText;
            grdInventory.SearchCriteria = e.SearchCriteria;
            grdInventory.SearchType = e.SearchType;
            grdInventory.SetSearchCriteria(e.SearchType);

            lblBrowseDispense.Text = "";
        }

        #region Grid Commands

        protected void grdInventory_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {


            if (DispensePage.MainTabStrip.SelectedIndex == 0)//Only load if on the inventory tab
            {
                string searchCriteria = grdInventory.SearchCriteria;
                string searchText = grdInventory.SearchText;
                if (!e.IsFromDetailTable)
                {
                    grdInventory.DataSource = DAL.PracticeLocation.Supplier.GetSuppliers(practiceLocationID, searchCriteria, searchText);
                }
                if (Page.IsPostBack)
                {
                    System.Data.DataSet ds = DAL.PracticeLocation.Inventory.GetAllProductDataByLocation(practiceID, practiceLocationID, searchCriteria, searchText);
                    grdInventory.MasterTableView.DetailTables[0].DataSource = ds;

                    grdInventory.SetRecordCount(ds, "ProductName");
                }
                

                grdInventory.SetGridLabel(searchCriteria, Page.IsPostBack, lblBrowseDispense);
                grdInventory.PageSize = recordsDisplayedPerPage;
                
            }
        }

        protected void grdInventory_OnDetailTableDataBind(object source, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {
            TelerikAjax.UIHelper.FilterOnJoinKey(e, "SupplierID", false);
        }

        protected void grdInventory_ItemDataBound(object source, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem && e.Item.OwnerTableView.Name == "gvwSupplier")
            {
                try
                {
                    DataRowView gridDataItem = e.Item.DataItem as DataRowView;

                    CheckBox chkABNForm1 = e.Item.FindControl("chkABNForm") as CheckBox;
                    CheckBox IsMedicare1 = e.Item.FindControl("chkIsMedicare") as CheckBox;
                    bool abnNeeded = Convert.ToBoolean(gridDataItem["ABN_Needed"]);

                    chkABNForm1.Attributes.Add("onclick", "ChangeChecked('" + IsMedicare1.ClientID + "','" + chkABNForm1.ClientID + "','" + chkABNForm1.ClientID + "'," + abnNeeded.ToString().ToLower() + ")");
                    IsMedicare1.Attributes.Add("onclick", "ChangeChecked('" + IsMedicare1.ClientID + "','" + chkABNForm1.ClientID + "','" + IsMedicare1.ClientID + "'," + abnNeeded.ToString().ToLower() + ")");

                }
                catch
                {

                }
            }
        }

        protected void grdInventory_ItemCommand(Object source, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "ExpandCollapse")
            {
                AddInventoryItemsToDispenseQueue();
            }
        }

        protected void grdInventory_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            //MWS Merge this event code with the event below it and send in parameters
            AddInventoryItemsToDispenseQueue(); //MWS might need to reimplement this for new telerik controls
        }

        protected void grdInventory_PreRender(object sender, EventArgs e)
        {
            grdInventory.ExpandGrid(); //UIHelper.ExpandFormatGrid(grdInventory);

        }
        #endregion

        #region Control Events

        protected void btnAddBrowseToDispensement_Click(object sender, EventArgs e)
        {
            //lblSummary.Text = "";

            AddInventoryItemsToDispenseQueue();
            
        }

        #endregion

        #region Methods

        public void Rebind()
        {
            grdInventory.Rebind();
        }

        public void ClearInventorySearch()
        {
            
            //TextBox searchText = (TextBox)rttbFilter.FindControl("tbFilter");
            //searchText.Text = "";
            //ViewState["SearchText"] = searchText.Text.ToString();

            //RadComboBox SearchCriteria = (RadComboBox)rttbSearchFilter.FindControl("cboSearchFilter");
            //SearchCriteria.SelectedIndex = 0;
            //ViewState["SearchCriteria"] = SearchCriteria.Value.ToString();
            
            //ctlSearchToolbar.ClearControls(); //MWS TODO might need to clear the search toolbar and rebind the grid

            grdInventory.Rebind();

        }

        private void AddInventoryItemsToDispenseQueue()
        {
            Int16 numProducts = 0;
            List<string> zeroQuantityItems = new List<string>();

            var selectedDataItems = from GridDataItem inventoryItem in (grdInventory.Items as IEnumerable)
                                   where inventoryItem.Selected == true
                                   && inventoryItem.OwnerTableView.Name == "gvwSupplier"
                                   select inventoryItem;

            foreach (GridDataItem gridDataItem in selectedDataItems)    
            {
                    Int32 productInventoryID = gridDataItem.GetColumnInt32("ProductInventoryID");
                    //string txtPatientCode = gridDataItem.GetColumnText("txtPatientCode");
                    //Int32 physicianID = gridDataItem.GetColumnSelectedValueInt32("cboPhysicians");
                    bool isMedicare = gridDataItem.GetColumnChecked("chkIsMedicare");
                    bool isABNForm = gridDataItem.GetColumnChecked("chkABNForm");
                    string side = gridDataItem.GetColumnString("Side");
                    int quantityOnHand = gridDataItem.GetColumnInt32("QuantityOnHand");
                    string productName = gridDataItem.GetColumnString("Product");
                    Decimal dmeDeposit = gridDataItem.GetColumnDecimal("DMEDeposit");
                    int quantity = 1;

                    if (isMedicare == true)
                    {
                        dmeDeposit = 0M;
                    }

                    if (quantityOnHand < quantity && _CurrentUserIsAllowedZeroQtyDispense == false)
                    {
                        //Warn user that they will not be able to dispense this without admin override
                        zeroQuantityItems.Add(productName);
                    }

                    DAL.PracticeLocation.Dispensement.AddUpdateDispensement(0, practiceLocationID, productInventoryID, "", 0, "", isMedicare, isABNForm, false, side, dmeDeposit, null, 1, null, 1, DateTime.Now);
                    numProducts += 1;
                    gridDataItem.Selected = false;
                    try
                    {
                        CheckBox chkMed = gridDataItem.FindControl("chkIsMedicare") as CheckBox;
                        chkMed.Checked = false;

                        CheckBox chkABM = gridDataItem.FindControl("chkABNForm") as CheckBox;
                        chkABM.Checked = false;

                    }
                    catch{}
            }

            string browseDispense = string.Concat(numProducts.ToString(), " product(s) added to dispensement");
            string warnings = string.Join("<br/>", zeroQuantityItems.ToArray());

            if (numProducts > 0)
            {
                string DispenseMessage = "";
                if (zeroQuantityItems.Count() > 0)
                {
                    DispenseMessage = string.Concat(browseDispense, "<br/>", "Warning: The following items have Zero Quantity and require admin override<br/>", warnings);
                }
                else
                {
                    DispenseMessage = browseDispense;
                }

                lblBrowseDispense.Text = DispenseMessage;
                BregVision.TelerikAjax.UIHelper.WriteJavascriptToLabel(grdInventory, lblBrowseDispense, DispenseMessage);//MWS TODO this probably doesn't work with new ajax

            }


        }

        #endregion
    }
}