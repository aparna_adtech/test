﻿using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Reflection;
using BLL.PDFUtils;

namespace BregVision.UserControls.DispensePage
{
    public partial class CustomBracePDF : Bases.UserControlBase
    {
        #region properties

		public int? ShoppingCartID
		{
			get
			{
				string shoppingCartID = ParentShoppingCartID.Value;
				int shoppingCartIDVal = 0;
				if (Int32.TryParse(shoppingCartID, out shoppingCartIDVal))
					return shoppingCartIDVal;
				return 0;
			}
			set
			{
				ParentShoppingCartID.Value = value.ToString();
			}
		}

		public string DisplayType
		{
			get; set;
		}

		#endregion

		public CustomBracePDF()
		{
			DisplayType = "PDF";
		}

		protected void Page_Init(object sender, EventArgs e)
		{
			DataSet dsMappedPDFInfo = DAL.PracticeLocation.Inventory.GetMappedPDFInfo(true);
			int mappedPDFID;
			DataTable mappedPDFFields = CustomBracePDFUtils.GetMappedPDFFields(dsMappedPDFInfo, out mappedPDFID);
			if (dsMappedPDFInfo.Tables.Count >= 1 && dsMappedPDFInfo.Tables[0].Rows.Count > 0)
			{
				Dictionary<string, List<List<Control>>> groups = new Dictionary<string, List<List<Control>>>();
				foreach (DataRow row in dsMappedPDFInfo.Tables[1].Rows)
				{
					List<List<Control>> controlRows = null;
					string htmlFormGroup = row["HTMLFormGroup"].ToString();
					if (!groups.ContainsKey(htmlFormGroup))
					{
						controlRows = new List<List<Control>>();
						groups.Add(htmlFormGroup, controlRows);
					}
					else
					{
						controlRows = groups[htmlFormGroup];
					}

					try
					{
						string htmlFormType = row["HTMLFormType"].ToString();
						if (htmlFormType != string.Empty)
						{
							Type controlType = typeof(Control).Assembly.GetType(htmlFormType, true);
							WebControl control = Activator.CreateInstance(controlType) as WebControl;
							if (control != null)
							{
								string id = row["PDFFieldPath"].ToString();
								if (id != string.Empty)
								{
									control.ID = id;
									string labelText = row["HTMLFormLabel"].ToString();
									labelText = labelText.Replace(" ", "&nbsp;");

									object width = row["HTMLFormWidth"];
									if (width != DBNull.Value)
									{
										control.Width = (int)width;
									}

									List<Control> controlRow = new List<Control>();
									controlRow.Add(new LiteralControl("<tr><td style='text-align:right'>" + labelText + "</td><td>"));
									controlRow.Add(control);
									controlRow.Add(new LiteralControl("</td></tr>"));
									controlRows.Add(controlRow);
								}
							}
						}
					}
					catch (Exception ex)
					{

					}
				}

				foreach (string groupName in groups.Keys)
				{
					pnlFormControls.Attributes.Add("class", "groupPanel");
					List<List<Control>> controlRows = groups[groupName];
					if (controlRows.Count > 0)
					{
						PlaceHolder placeholder = new PlaceHolder();
						placeholder.Controls.Add(new LiteralControl("<fieldset class='groupFieldset'><legend>" + (groupName == string.Empty ? "-" : groupName) + "</legend><table>"));
						foreach (List<Control> controlRow in controlRows)
						{
							foreach (Control control in controlRow)
							{
								placeholder.Controls.Add(control);
							}
						}
						placeholder.Controls.Add(new LiteralControl("</table></fieldset>"));

						pnlFormControls.Controls.Add(new LiteralControl("<div class='groupDiv'>"));
						pnlFormControls.Controls.Add(placeholder);
						pnlFormControls.Controls.Add(new LiteralControl("</div>"));
					}
				}
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				Page.ClientScript.RegisterStartupScript(
					this.GetType(),
					"PreventBackButton",
					"<script language=JavaScript>try {window.history.forward(1);} catch (e) {}</script>");

				DataSet dsMappedPDFInfo = DAL.PracticeLocation.Inventory.GetMappedPDFInfo(true);
				int mappedPDFID;
				DataTable mappedPDFFields = CustomBracePDFUtils.GetMappedPDFFields(dsMappedPDFInfo, out mappedPDFID);
				if (dsMappedPDFInfo.Tables.Count >= 1 && dsMappedPDFInfo.Tables[0].Rows.Count > 0)
				{
					Dictionary<string, string> fdf = CustomBracePDFUtils.PopulateFDF(practiceID, practiceLocationID, 0, ShoppingCartID.Value, mappedPDFFields);

					foreach (DataRow row in dsMappedPDFInfo.Tables[1].Rows)
					{
						string id = row["PDFFieldPath"].ToString();
						if (id != string.Empty)
						{
							Control control = FindControl(id);
							if (control != null)
							{
								PropertyInfo info = null;
								if (fdf.ContainsKey(id))
								{
									info = control.GetType().GetProperty("Checked");
									if (info != null)
									{
										string value = fdf[id].ToUpper();
										info.SetValue(control, value == "ON" || value == "MALE", null);
									}
									else
									{
										info = control.GetType().GetProperty("Text");
										if (info != null)
										{
											info.SetValue(control, fdf[id], null);
										}
									}
								}
								info = control.GetType().GetProperty("GroupName");
								if (info != null)
								{
									string groupName = row["HTMLFormGroup"].ToString();
									info.SetValue(control, groupName, null);
								}
							}
						}
					}
				}
			}
		}

		protected void Page_PreRender(object sender, EventArgs e)
        {
		}

		protected void btnNext_Click(object sender, EventArgs e)
		{
			DataSet dsMappedPDFInfo = DAL.PracticeLocation.Inventory.GetMappedPDFInfo(true);
			Dictionary<string, string> fdf = new Dictionary<string, string>(); 

			foreach (DataRow row in dsMappedPDFInfo.Tables[1].Rows)
			{
				string id = row["PDFFieldPath"].ToString();
				if (id != string.Empty)
				{
					Control control = FindControl(id);
					if (control != null)
					{
						string value = string.Empty;
						PropertyInfo info = control.GetType().GetProperty("Checked");
						if (info != null)
						{
							object obj = info.GetValue(control, null);
							if (obj != null)
							{
								bool isGender = (id == "Gender");
								value = isGender ? "Female" : "Off";
								if ((bool)obj)
									value = isGender ? "Male" : "On";
							}
						}
						else
						{
							info = control.GetType().GetProperty("Text");
							if (info != null)
							{
								object obj = info.GetValue(control, null);
								if (obj != null)
									value = obj.ToString();
							}
						}
						fdf.Add(id, value);
					}
				}
			}

			CustomBracePDFUtils.SavePDFData(practiceLocationID, ShoppingCartID.Value, fdf, false);

			string redirectUrl = BLL.PDFUtils.CustomBracePDFUtils.RootURL +
				"/UserControls/DispensePage/CustomBracePDFHandler.ashx?shoppingcartid=" + ShoppingCartID + "&submit=&usedbfdf=";
			Response.Redirect(redirectUrl);
		}
	}
}