﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Text;
using System.Web.Security;
using BregVision.BregAdmin;
using ClassLibrary.DAL;
using DAL.PracticeLocation;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.Web.UI;

namespace BregVision.UserControls.DispensePage
{
    public partial class DispenseQueue : Bases.UserControlBase
    {

        #region private variables
        private Dispense DispensePage;

        private bool _CurrentUserIsAllowedZeroQtyDispense = false;
        private IEnumerable<ICD9> _ICD9_Codes = null;
        private Dictionary<string, int> summaryFields = new Dictionary<string, int>();

        #endregion

        #region Properties

        public GridView Grid
        {
            get
            {
                return grdSummary;
            }
        }

        public Label StatusLabel
        {
            get
            {
                return lblSummary;
            }
        }

        public Button DispensementProducts
        {
            get
            {
                return btnDispenseProducts;
            }
        }

        #endregion

        #region Page Events

        new protected void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);

            DispensePage = ((Dispense)this.Page);
            //for the queue
            summaryFields.Add("ClientSelectColumn", 0);
            summaryFields.Add("chkIsMedicare", 2);
            summaryFields.Add("chkABNForm", 3);
            summaryFields.Add("DispenseQueueID", 5);
            summaryFields.Add("txtDateDispensed", 7);
            summaryFields.Add("cboPhysicians", 9);
            summaryFields.Add("txtPatientCode", 10);
            summaryFields.Add("Code", 13);
            summaryFields.Add("cboSide", 14);

            summaryFields.Add("HCPCs", 16);
            summaryFields.Add("WholesaleCost", 17);
            summaryFields.Add("QuantityOnHandPerSystem", 18);
            summaryFields.Add("txtQuantityDispensed", 19);
            summaryFields.Add("chkDMEDeposit", 20);
            summaryFields.Add("txtDMEDeposit", 21);
            summaryFields.Add("BillingCharge", 23);
            summaryFields.Add("BillingChargeCash", 24);
            summaryFields.Add("cboICD9", 25);
            summaryFields.Add("ProductInventoryID", 26);
            summaryFields.Add("PracticeCatalogProductID", 27);

            GridViewExtensions.fieldNames = summaryFields;


        }



        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();

            lblSummary.Text = "";
            lblErrorMsg.Text = ""; //added by u= 02/04/2010

            grdSummary.PageSize = recordsDisplayedPerPage;

            BLL.Practice.User user = new BLL.Practice.User();
            _CurrentUserIsAllowedZeroQtyDispense = user.IsAllowedZeroQtyDispense(Context.User.Identity.Name.ToString());
        }

        #endregion

        #region Events


        private void InitializeAjaxSettings()
        {
            Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);
            
            radAjaxManager.AjaxSettings.AddAjaxSetting(grdSummary, grdSummary);
            radAjaxManager.AjaxSettings.AddAjaxSetting(DispensePage.InventoryControl.AddToDispensement, grdSummary, DispensePage.DefaultLoadingPanel);

            radAjaxManager.AjaxSettings.AddAjaxSetting(btnSaveDispenseQueue, grdSummary, DispensePage.DefaultLoadingPanel);
            radAjaxManager.AjaxSettings.AddAjaxSetting(btnDispenseProducts, grdSummary, DispensePage.DefaultLoadingPanel);
            
            radAjaxManager.AjaxSettings.AddAjaxSetting(btnSaveDispenseQueue, lblSummary);
            radAjaxManager.AjaxSettings.AddAjaxSetting(btnSaveDispenseQueue, lblErrorMsg);
            radAjaxManager.AjaxSettings.AddAjaxSetting(btnSaveDispenseQueue, DispensePage.InventoryControl.StatusLabel); 
            //radAjaxManager.AjaxSettings.AddAjaxSetting(btnSaveDispenseQueue, DispensePage.CustomBraceControl.StatusLabel); //MWS might not need this with new ajax

            radAjaxManager.AjaxSettings.AddAjaxSetting(btnDispenseProducts, lblSummary);
            radAjaxManager.AjaxSettings.AddAjaxSetting(btnDispenseProducts, lblErrorMsg);
            radAjaxManager.AjaxSettings.AddAjaxSetting(btnDispenseProducts, DispensePage.InventoryControl.StatusLabel);
            radAjaxManager.AjaxSettings.AddAjaxSetting(btnDispenseProducts, DispensePage.MainTabStrip);
            
            //radAjaxManager.AjaxSettings.AddAjaxSetting(btnDispenseProducts, DispensePage.CustomBraceControl.StatusLabel); //MWS might not need this with new ajax

            radAjaxManager.AjaxSettings.AddAjaxSetting(grdSummary, lblSummary);
            radAjaxManager.AjaxSettings.AddAjaxSetting(grdSummary, lblErrorMsg);


            radAjaxManager.AjaxSettings.AddAjaxSetting(DispensePage.InventoryControl.SearchToolbarControl.Toolbar, lblSummary);
            radAjaxManager.AjaxSettings.AddAjaxSetting(DispensePage.InventoryControl.AddToDispensement, lblSummary);
        }

        #endregion

        #region Grid Methods

        protected void grdSummary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //Yuiko, put Try Catch around each of these sections like the commented function below this
            ClassLibrary.DAL.DispenseQueueInterface dq = e.Row.DataItem as ClassLibrary.DAL.DispenseQueueInterface;

            if (dq != null)
            {
                bool abn_Needed = dq.ABN_Needed;
                bool isMedicare = dq.IsMedicare;
                bool abnForm = dq.ABNForm;

                CheckBox chkABNForm = e.Row.GetCheckBox("chkABNForm");
                CheckBox IsMedicare = e.Row.GetCheckBox("chkIsMedicare");
                CheckBox cashOverride = e.Row.GetCheckBox("chkDMEDeposit");

                CheckBox chkClientSelectColumn = e.Row.GetCheckBox("ClientSelectColumn"); //added by u= 02/09/2010

                //ABN logic here | look at abn_Needed, IsMedicare, ABN Form 
                if (abn_Needed == true && chkABNForm != null)
                {
                    chkABNForm.BackColor = System.Drawing.Color.Yellow;
                }

                DropDownList cboPhysician = e.Row.GetDropDownList("cboPhysicians");

                if (cboPhysician != null)
                {
                    cboPhysician.DataSource = DispensePage.Physicians;
                    cboPhysician.DataValueField = "PhysicianId";
                    cboPhysician.DataTextField = "PhysicianName";
                    cboPhysician.DataBind();

                    int? physicianID = dq.PhysicianID; //dataItem.GetDataProperty<int?>("PhysicianID");
                    if (physicianID != null)
                    {
                        cboPhysician.SelectedValue = physicianID.ToString();
                    }
                }

                DropDownList cboSide = e.Row.GetDropDownList("cboSide");
                if (cboSide != null)
                {
                    string side = dq.Side;
                    if (!string.IsNullOrEmpty(side))
                    {
                        cboSide.SelectedValue = side;
                    }
                }

                RadDatePicker txtDateDispensed = e.Row.GetRadDatePicker("txtDateDispensed");

                if (txtDateDispensed != null)
                {
                    DateTime? dispenseDate = dq.DispenseDate;
                    if (dispenseDate != null)
                    {
                        txtDateDispensed.SelectedDate = dispenseDate;
                    }
                    else
                    {
                        txtDateDispensed.SelectedDate = DateTime.Now;
                    }
                }

                RadComboBox cboICD9 = e.Row.GetRadComboBox("cboICD9");

                if (cboICD9 != null)
                {
                    if (!BLL.Website.IsVisionExpress())
                    {
                        string hcpcs = dq.HCPCsString;//dataItem.GetDataProperty<string>("HCPCsString");
                        if (!string.IsNullOrEmpty(hcpcs))
                        {
                            hcpcs = hcpcs.Replace(" ", "");

                            string[] hcpcsArray = hcpcs.Split(",".ToCharArray());

                            //add "if" statement by u= 
                            if (hcpcsArray.GetUpperBound(0) > -1  /*hcpcs.Length > 0*/)
                            {

                                for (int i = 0; i < hcpcsArray.Length; i++)
                                {

                                    string truncatedHCPC = hcpcsArray[i].Length > 4 ? hcpcsArray[i].Substring(0, 5) : hcpcsArray[i];

                                    if (hcpcsArray[i] != truncatedHCPC)
                                    {
                                        hcpcsArray[i] = truncatedHCPC;
                                    }
                                }

                            }//end


                            List<string> hcpcList = hcpcsArray.ToList<string>();

                            var hcpcsForThisRow = (from icd in _ICD9_Codes where hcpcList.Contains(icd.HCPCs) select new { ICD9Code = icd.ICD9Code.Trim() }).Distinct();

                            cboICD9.DataSource = hcpcsForThisRow;

                            cboICD9.DataTextField = "ICD9Code";
                            cboICD9.DataValueField = "ICD9Code";
                            cboICD9.DataBind();

                            cboICD9.Text.Replace("&nbsp;", "");
                            cboICD9.Text.Trim();
                            cboICD9.Items.Insert(0, new RadComboBoxItem(""));//u= 01/04/10
                        }
                    }
                    else
                    {
                        //cboICD9.Enabled = false;
                    }

                }
                RadNumericTextBox dmeDeposit = e.Row.GetRadNumericTextBox("txtDMEDeposit");

                bool abnNeeded = dq.ABN_Needed;
                decimal hdnDMEDeposit = dq.hdnDMEDeposit;

                chkABNForm.Attributes.Add("onclick", "ChangeChecked('" + IsMedicare.ClientID + "','" + chkABNForm.ClientID + "','" + chkABNForm.ClientID + "'," + abnNeeded.ToString().ToLower() + ",'" + dmeDeposit.ClientID + "'," + hdnDMEDeposit.ToString() + ",'" + cashOverride.ClientID + "')");
                IsMedicare.Attributes.Add("onclick", "ChangeChecked('" + IsMedicare.ClientID + "','" + chkABNForm.ClientID + "','" + IsMedicare.ClientID + "'," + abnNeeded.ToString().ToLower() + ",'" + dmeDeposit.ClientID + "'," + hdnDMEDeposit.ToString() + ",'" + cashOverride.ClientID + "')");
                cashOverride.Attributes.Add("onclick", "ChangeChecked('" + IsMedicare.ClientID + "','" + chkABNForm.ClientID + "','" + IsMedicare.ClientID + "'," + abnNeeded.ToString().ToLower() + ",'" + dmeDeposit.ClientID + "'," + hdnDMEDeposit.ToString() + ",'" + cashOverride.ClientID + "')");

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    chkClientSelectColumn.Attributes.Add("onclick", "ChangeRowColor('" + e.Row.ClientID + "')");
                }

            }
        }

        protected void grdSummary_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            SetCellInvisible(e, "DispenseQueueID");
            SetCellInvisible(e, "ProductInventoryID");
            SetCellInvisible(e, "PracticeCatalogProductID");
            SetCellInvisible(e, "WholesaleCost");
        }

        protected void grdSummary_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdSummary.PageIndex = e.NewPageIndex;
            grdSummary.DataBind();

        }

        protected void grdSummary_OnRowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = grdSummary.Rows[e.RowIndex];
            Int32 dispenseQueueId = row.GetBoundColumnInt32("DispenseQueueID");
            DAL.PracticeLocation.Dispensement.DeleteDispenseQueueItem(dispenseQueueId);
        }

        private void SetCellInvisible(GridViewRowEventArgs e, string fieldName)
        {
            if (e.Row.Cells.Count >= summaryFields[fieldName])
            {
                e.Row.Cells[summaryFields[fieldName]].Visible = false;
            }
        }

        #endregion

        #region Control Events

        protected void btnSaveDispenseQueue_Click(object sender, EventArgs e)
        {
            DispensePage.InventoryControl.StatusLabel.Text = "";

            List<int> zeroQtyProductInventoryIDs = null;
            List<int> selectedDispenseQueueIDs = null;

            List<int> savedProducts = new List<int>();
            SaveDispenseQueue(grdSummary.Rows, out zeroQtyProductInventoryIDs, out selectedDispenseQueueIDs);

            lblSummary.Text = "Dispense Queue saved.";

        }

        protected void btnDispenseProducts_Click(object sender, EventArgs e)
        {
            lblSummary.Text = "";
            DispensePage.InventoryControl.StatusLabel.Text = "";

            int dispenseBatchID = 0;
            List<int> zeroQtyProductInventoryIDs = null;
            List<int> selectedDispenseQueueIDs = null;

            List<int> selectedProductInventoryIDs = SaveDispenseQueue(grdSummary.Rows, out zeroQtyProductInventoryIDs, out selectedDispenseQueueIDs);
            List<int> missingProductIDs = null;
            List<int> dispensedProducts = new List<int>();


            if (Dispensement.DispensementContainsFullCustomBraceOrders(practiceLocationID, selectedProductInventoryIDs, out missingProductIDs) == true)
            {
                dispenseBatchID = Dispensement.DispenseProducts(practiceLocationID, selectedProductInventoryIDs, dispensedProducts, selectedDispenseQueueIDs);
                string message = string.Concat(dispensedProducts.Sum().ToString(), " item(s) dispensed.  <br/>");

                lblSummary.Text = message;

                if (zeroQtyProductInventoryIDs.Count > 0)
                {
                    message += string.Concat(zeroQtyProductInventoryIDs.Count().ToString(), " item(s) not dispensed because dispense quantity exceeded on hand quantity");

                    lblSummary.Text = message;
                }
                if (selectedProductInventoryIDs.Count() > 0)
                {
                    lblSummary.Visible = true;
                    //Send billing email here
                    SendBillingEmail(dispenseBatchID);

                    DispensePage.CustomBraceControl.Rebind();
                    //grdSummary.Rebind();
                    DispensePage.InventoryControl.Rebind();

                    DispensePage.InventoryControl.StatusLabel.Text = "";  //lblBrowseDispense.Text = "";
                    DispensePage.CustomBraceControl.StatusLabel.Text = "";

                }
            }
            else
            {
                //Please dispense all items in custom brace order together.
                lblSummary.Text = "Please dispense all items in custom brace order(s) together.";
                lblSummary.Visible = true;
                //MWS missingProductIDs contains the ProductID's needed to do the dispensement
            }

            if (selectedProductInventoryIDs.Count() > 0)
            {

                DispensePage.MainTabStrip.Tabs[2].Enabled = true;
                DispensePage.MainTabStrip.Tabs[2].Selected = true;
                DispensePage.ReceiptHistoryPageView.Selected = true;
                DispensePage.InventoryControl.ClearInventorySearch();
                DispensePage.CustomBraceControl.ClearCustomBraceSearch();

                DispensePage.DispenseHistoryControl.SelectLastDispenseBatchInReceiptHistory(dispenseBatchID);
            }
        }



        #endregion

        #region Methods

        private void SendBillingEmail(int DispenseBatchID)
        {
            if (DispenseBatchID < 1)
            {
                return;
            }

            VisionDataContext visionDB = null;
            visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                var itemsDispensed = from db in visionDB.DispenseBatches
                                     join d in visionDB.Dispenses on db.DispenseBatchID equals d.DispenseBatchID
                                     join dd in visionDB.DispenseDetails on d.DispenseID equals dd.DispenseID
                                     where db.DispenseBatchID == DispenseBatchID
                                     //&& dd.IsCashCollection == false
                                     select new
                                     {
                                         dd.IsMedicare,
                                         dd.ABNForm,
                                         Cash = dd.IsCashCollection,
                                         DispensedDate = d.DateDispensed.ToShortDateString(),
                                         Physician = dd.Physician.Contact.FirstName + " " + dd.Physician.Contact.LastName,
                                         d.PatientCode,
                                         //dd.PracticeCatalogProduct //brand
                                         Name = dd.PracticeCatalogProduct.IsThirdPartyProduct == true ? dd.PracticeCatalogProduct.ThirdPartyProduct.Name : dd.PracticeCatalogProduct.MasterCatalogProduct.Name,
                                         Code = dd.PracticeCatalogProduct.IsThirdPartyProduct == true ? dd.PracticeCatalogProduct.ThirdPartyProduct.Code : dd.PracticeCatalogProduct.MasterCatalogProduct.Code,
                                         dd.LRModifier,
                                         Size = dd.PracticeCatalogProduct.IsThirdPartyProduct == true ? dd.PracticeCatalogProduct.ThirdPartyProduct.Size : dd.PracticeCatalogProduct.MasterCatalogProduct.Size,
                                         HCPCs = visionDB.ConcatHCPCS(dd.PracticeCatalogProductID),
                                         dd.ICD9Code,
                                         ActualChargeBilled = string.Format("{0:C}", dd.ActualChargeBilled),
                                         DMEDeposit = string.Format("{0:C}", dd.DMEDeposit),
                                         QuantityToDispense = dd.Quantity,
                                     };


                DataRow drPracticeLocationBillingAddress = null;
                DAL.PracticeLocation.BillingAddress dalPLBA = new DAL.PracticeLocation.BillingAddress();
                DataSet dsPracticeLocationBillingAddress = dalPLBA.Select(practiceLocationID);
                if (dsPracticeLocationBillingAddress.Tables[0].Rows.Count > 0)
                {
                    drPracticeLocationBillingAddress = dsPracticeLocationBillingAddress.Tables[0].Rows[0];
                }

                string attentionOf = "";
                int addressID = 0;
                string addressLine1 = "";
                string addressLine2 = "";
                string city = "";
                string state = "";
                string zipCode = "";
                string zipCodePlus4 = "";
                dalPLBA.Select(practiceLocationID,
                                    out attentionOf,
                                     out addressID,
                                     out addressLine1,
                                     out addressLine2,
                                     out city,
                                     out state,
                                     out zipCode,
                                     out zipCodePlus4);

                //make a grid for the dispense queue to render for an email
                GridView dispenseEmail = new GridView();
                dispenseEmail.DataSource = itemsDispensed;
                dispenseEmail.DataBind();

                //add abn forms to email
                var abnNeeded = from abn in itemsDispensed
                                where abn.ABNForm == true
                                select abn;

                System.Web.UI.WebControls.Panel ctlAll = new System.Web.UI.WebControls.Panel();
                ctlAll.Controls.Add(dispenseEmail);
                ctlAll.Controls.Add(new LiteralControl("<br/><br/>"));

                //foreach (var abnForm in abnNeeded)
                //{
                //    Control ctl = Page.LoadControl("~/Authentication/ABNFormCtl.ascx");
                //    Authentication.ABNFormCtl abnControl = ctl as Authentication.ABNFormCtl;
                //    abnControl.PracticeName = practiceName;

                //    abnControl.Address1 = addressLine1; 
                //    abnControl.Address2 = addressLine2; 
                //    abnControl.City = city; 
                //    abnControl.State = state; 
                //    abnControl.Zip = zipCode; 

                //    if (drPracticeLocationBillingAddress != null)
                //    {
                //        abnControl.BillingPhone = drPracticeLocationBillingAddress["Fax"].ToString();
                //        abnControl.BillingFax = drPracticeLocationBillingAddress["WorkPhone"].ToString();
                //    }

                //    abnControl.PatientCode = abnForm.PatientCode.ToString();
                //    abnControl.ProductName = abnForm.Name.ToString();
                //    abnControl.ProductCost = Convert.ToDecimal(abnForm.ActualChargeBilled.Replace("$", ""));
                //    abnControl.LoadControlInfo();
                //    ctlAll.Controls.Add(abnControl);
                //    ctlAll.Controls.Add(new LiteralControl("<br/><br/>"));
                //}


                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htmlTW = new HtmlTextWriter(sw);
                ctlAll.RenderControl(htmlTW);

                string html = sb.ToString();


                string MailTo = BLL.PracticeLocation.Email.GetBillingEmail(practiceLocationID);
                FaxEmailPOs emailSender = new FaxEmailPOs();
                string dispenseBatchIDDebugInfo = "";

                if (string.IsNullOrEmpty(MailTo.Trim()) == false)
                {
                    emailSender.SendEmail("OrderConfirmation", MailTo, html, string.Format("DME Dispensement Information {0}", dispenseBatchIDDebugInfo));
                }
            }
        }

        private List<int> SaveDispenseQueue(GridViewRowCollection rows, out List<int> zeroQtyProductInventoryIDs, out List<int> selectedDispenseQueueIDs)
        {
            List<int> selectedProductInventoryIDs = new List<int>();
            zeroQtyProductInventoryIDs = new List<int>();
            selectedDispenseQueueIDs = new List<int>();

            foreach (GridViewRow row in rows)
            {
                Int32 dispenseQueueID = row.GetBoundColumnInt32("DispenseQueueID");

                Int32 ProductInventoryID = row.GetBoundColumnInt32("ProductInventoryID");

                Int32 quantityOnHand = row.GetBoundColumnInt32("QuantityOnHandPerSystem");

                RadNumericTextBox txtQuantity = row.GetRadNumericTextBox("txtQuantityDispensed"); 
                Int32 Quantity = Convert.ToInt32(txtQuantity.Value.ToString());

                //To Do for Mike -- u=  might need to combine this statment with below 02/05/2010
                if (row.GetCheckBoxValue("ClientSelectColumn") == true)
                {
                    if (quantityOnHand < Quantity && _CurrentUserIsAllowedZeroQtyDispense == false)
                    {
                        zeroQtyProductInventoryIDs.Add(ProductInventoryID);
                    }
                    else
                    {
                        selectedProductInventoryIDs.Add(ProductInventoryID);
                    }
                }

                bool isMedicare = row.GetCheckBoxValue("chkIsMedicare"); 

                bool isABNForm = row.GetCheckBoxValue("chkABNForm");

                Int32? bregVisionOrderID = null;

                string txtPatientCode = row.GetTextBoxValue("txtPatientCode");  

                Int32 PracticeCatalogProductID = row.GetBoundColumnInt32("PracticeCatalogProductID"); 

                Int32 PhysicianID = row.GetDropDownListValueInt32("cboPhysicians"); 

                decimal BillingCharge = row.GetBoundColumnDecimal("BillingCharge");

                decimal BillingChargeCash = row.GetBoundColumnDecimal("BillingChargeCash");

                string side = row.GetDropDownListValue("cboSide");

                bool IsCashCollection = row.GetCheckBoxValue("chkDMEDeposit");

                decimal WholesaleCost = row.GetBoundColumnDecimal("WholesaleCost");

                decimal DMEDeposit = row.GetRadNumericTextBoxDecimal("txtDMEDeposit");

                DateTime? DispenseDate = row.GetRadDatePickerDateTime("txtDateDispensed");

                string icd9Code = "";
                //if (!BLL.Website.IsVisionExpress())
                //{
                    icd9Code = row.GetRadComboBoxText("cboICD9");
                //}
                
                string hcpcsString = row.GetBoundColumnString("HCPCs"); 

                hcpcsString = hcpcsString.Replace("&nbsp;", "");

                SaveNewICD9(icd9Code, hcpcsString);

                string code = row.GetBoundColumnString("Code");

                int savedDispenseQueueID = 0;
                if (ProductInventoryID > 0 && PracticeCatalogProductID > 0)
                {
                    savedDispenseQueueID = DAL.PracticeLocation.Dispensement.AddUpdateDispensement(dispenseQueueID, practiceLocationID, ProductInventoryID, txtPatientCode, PhysicianID, icd9Code, isMedicare, isABNForm, IsCashCollection,
                                                                                    side, DMEDeposit, bregVisionOrderID, Quantity, DispenseDate, 1, DateTime.Now);
                }
                ////We decided not use this because it makes slower
                //if( SearchForPracticeLocaionIDAndProductInventoryID(practiceLocationID, ProductInventoryID) != true)
                //{
                //    lblErrorMsg.Text = string.Format("Item(s) {0} don't belong to the location.", code);
                //}

                if (row.GetCheckBoxValue("ClientSelectColumn") == true && savedDispenseQueueID > 0)
                {
                    if (quantityOnHand < Quantity && _CurrentUserIsAllowedZeroQtyDispense == false) //don't dispense, add to can't be dispensed list instead.
                    {
                    }
                    else
                    {
                        selectedDispenseQueueIDs.Add(savedDispenseQueueID);
                    }
                }

            }
            return selectedProductInventoryIDs;
        }

        private void SaveNewICD9(string UserICD9Code, string HCPCsString)
        {
            if (!BLL.Website.IsVisionExpress())
            {
                if (string.IsNullOrEmpty(UserICD9Code.Trim()) || string.IsNullOrEmpty(HCPCsString.Trim()))
                {
                    return;
                }
                //parse the HCPCsString from a comma separated string to a list usable by LINQ
                string hcpcs = HCPCsString;
                hcpcs = hcpcs.Replace(" ", "");
                string[] hcpcsArray = hcpcs.Split(",".ToCharArray());
                List<string> hcpcList = hcpcsArray.ToList<string>();

                //Load the IDC9's 
                _ICD9_Codes = DAL.PracticeLocation.Dispensement.GetDispenseQueueICD9s(practiceLocationID);

                //Find the the ICD9 items that match these HCPCs
                var hcpcsForThisRow = from icd in _ICD9_Codes where hcpcList.Contains(icd.HCPCs) select icd;

                //Se if the user ICD9 Code(can be picked from the list Items in the comboBox or typed in) is in the list
                var icd9Exists = from existing_icd9 in hcpcsForThisRow where existing_icd9.ICD9Code == UserICD9Code select existing_icd9;
                if (icd9Exists.Count() < 1)
                {
                    //user typed a new ICD9 code that wasn't in the dropdown
                    foreach (string hcpc in hcpcList)
                    {
                        //Add the ICD9 the user typed in to both the ICD9 and Practice_ICD9 tables
                        if (string.IsNullOrEmpty(hcpc.Trim()) == false && string.IsNullOrEmpty(UserICD9Code.Trim()) == false)
                        {
                            int icd9ID = DAL.PracticeLocation.Dispensement.AddPracticeICD9(practiceID, hcpc.Trim(), UserICD9Code.Trim());
                            DAL.PracticeLocation.Dispensement.InsertPracticeICD9(icd9ID, practiceID);
                        }
                    }
                }
            }
        }
        public void LoadDispenseQueue()
        {
            Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);
            if (radAjaxManager.IsAjaxRequest == true)
            {
                //Load the IDC9's every time we reload the summary datasource
                _ICD9_Codes = DAL.PracticeLocation.Dispensement.GetDispenseQueueICD9s(practiceLocationID);

                grdSummary.Visible = true;
                grdSummary.DataSource = DAL.PracticeLocation.Dispensement.GetDispenseQueue(practiceLocationID);
                grdSummary.DataBind();
            }
            else
            {
                List<ClassLibrary.DAL.DispenseQueueInterface> queueList = DAL.PracticeLocation.Dispensement.GetDispenseQueue(0);
                ClassLibrary.DAL.DispenseQueueInterface emptyRow = new ClassLibrary.DAL.DispenseQueueInterface();
                emptyRow.BrandShortName = "";
                emptyRow.Code = "";
                emptyRow.DispenseDate = DateTime.Now;
                //emptyRow.HCPCsString = "";
                emptyRow.PatientCode = "";
                emptyRow.ShortName = "";
                emptyRow.Side = "L";
                emptyRow.Size = "";
                emptyRow.SupplierShortName = "";


                queueList.Add(emptyRow);
                grdSummary.DataSource = queueList;
                grdSummary.DataBind();

                int totalColumns = grdSummary.Rows[0].Cells.Count;
                grdSummary.Rows[0].Cells.Clear();
                grdSummary.Rows[0].Cells.Add(new TableCell());
                grdSummary.Rows[0].Cells[0].ColumnSpan = totalColumns;
                grdSummary.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
                grdSummary.Rows[0].Cells[0].Text = "Loading Records...";

            }
        }

        #endregion
    }
}