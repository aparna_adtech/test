﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewButton.ascx.cs" Inherits="BregVision.UserControls.DispensePage.ViewButton" %>

<div>
    <asp:Button ID="btnShowQueue" runat="server" Text="Show Dispense Queue" OnClick="btnShowQueue_OnClick" Visible="false" />
    <br /><br />
    <asp:Label ID="lblMessage" runat="server" CssClass="FieldLabel" Text="The dispense queue will load in a few seconds..." Visible="false"></asp:Label>
    <asp:Timer ID="AjaxTimer" runat="server" OnTick="AjaxTimer_OnTick" Interval="2000" ></asp:Timer>

</div>

