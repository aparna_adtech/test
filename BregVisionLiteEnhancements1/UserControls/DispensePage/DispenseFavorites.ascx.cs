﻿using System;
using System.Collections;
using System.Linq;
using System.Web.UI.WebControls;
using ClassLibrary.DAL;
using Telerik.Web.UI;


namespace BregVision.UserControls.DispensePage
{
    public partial class DispenseFavorites : Bases.UserControlBase
    {
        /// <summary>
        /// btnAddToDispensement control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        public global::System.Web.UI.WebControls.Button btnAddToDispensement;

        /// <summary>
        /// grdProducts control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        public global::BregVision.UserControls.VisionRadGridAjax grdProducts;


        #region favorite(s) post-add event
        public event EventHandler AddToDispensementClicked;

        public virtual void OnAddToDispensementClicked(EventArgs e)
        {
            var h = AddToDispensementClicked;
            if (h != null)
                h(this, e);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();
        }

        protected void grdProducts_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "addtoqueue")
            {
                GridDataItem poItem = e.Item as GridDataItem;
                if (poItem != null)
                {
                    AddToDispenseQueue(poItem);
                    OnAddToDispensementClicked(EventArgs.Empty);
                }
            }
        }

        private void InitializeAjaxSettings()
        {
            var radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);
            if (radAjaxManager != null)
            {
                radAjaxManager.AjaxSettings.AddAjaxSetting(btnAddToDispensement, ctlPracticePhysicians.cboPhysicians);
            }
        }


        protected void LinqDataSource1_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            if (Cache["Products"] != null)
            {
                e.Result = Cache["Products"];

            }
            else
            {
                VisionDataContext context = VisionDataContext.GetVisionDataContext();
                using (context)
                {
                    var query = from dd in context.DispenseDetails
                                join d in context.Dispenses on dd.DispenseID equals d.DispenseID
                                join pcp in context.PracticeCatalogProducts on dd.PracticeCatalogProductID equals pcp.PracticeCatalogProductID
                                //join pi in context.ProductInventories on pcp.PracticeCatalogProductID equals pi.ProductInventoryID
                                join mcp in context.MasterCatalogProducts on pcp.MasterCatalogProductID equals mcp.MasterCatalogProductID into mm
                                from mcp in mm.DefaultIfEmpty()
                                join tpp in context.ThirdPartyProducts on pcp.ThirdPartyProductID equals tpp.ThirdPartyProductID into tt
                                from tpp in tt.DefaultIfEmpty()
                                where
                                    dd.IsActive == true
                                    & d.IsActive == true
                                    && pcp.IsActive == true
                                    && (mcp == null || mcp.IsActive == true)
                                    && (tpp == null || tpp.IsActive == true)
                                    && d.PracticeLocationID == practiceLocationID
                                //&& pi.PracticeLocationID == practiceLocationID
                                select new
                                {
                                    ProductInventoryID = ((from pi in context.ProductInventories where pi.PracticeCatalogProductID == pcp.PracticeCatalogProductID && pi.PracticeLocationID == practiceLocationID select (int?)pi.ProductInventoryID).FirstOrDefault()),
                                    PracticeCatalogProductID = dd.PracticeCatalogProductID,
                                    Side = (mcp != null ? mcp.LeftRightSide : (tpp != null ? tpp.LeftRightSide : "")),
                                    DMEDeposit = pcp.DMEDeposit,
                                    Code = (mcp != null ? mcp.Code : (tpp != null ? tpp.Code : "")),
                                    Qty = ((from pi in context.ProductInventories where pi.PracticeCatalogProductID == pcp.PracticeCatalogProductID && pi.PracticeLocationID == practiceLocationID select (int?)pi.QuantityOnHandPerSystem).FirstOrDefault()),
                                    ProductName = (mcp != null ? mcp.Name : (tpp != null ? tpp.Name : "")) + " " + (mcp != null ? mcp.Size : (tpp != null ? tpp.Size : "")) + " " + (mcp != null ? mcp.LeftRightSide : (tpp != null ? tpp.LeftRightSide : "")),
                                    WholesaleCost = pcp.WholesaleCost,
                                    BillingCharge = pcp.BillingCharge,
                                    TotalQuantity = dd.Quantity,
                                };

                    var groupedQuery = query
                                      .Where(q => q.Qty.HasValue)
                                      .GroupBy(a => new { a.PracticeCatalogProductID, a.Code, a.ProductInventoryID, a.Qty, a.ProductName, a.DMEDeposit, a.Side, a.BillingCharge })
                                      .Select(group => new { PracticeCatalogProductID = group.Key.PracticeCatalogProductID, Code = group.Key.Code, ProductInventoryID = group.Key.ProductInventoryID, Qty = group.Key.Qty, ProductName = group.Key.ProductName, Side = group.Key.Side, DMEDeposit = group.Key.DMEDeposit, TotalQuantity = group.Sum(b => b.TotalQuantity), TotalCost = group.Sum(c => c.WholesaleCost), BillingCharge = group.Key.BillingCharge })
                                      .OrderByDescending(g => g.TotalQuantity);
                    var orderedQuery = groupedQuery.ToList().Take(10);

                    Cache["Products"] = orderedQuery;

                    e.Result = orderedQuery;
                }
            }
        }

        protected void btnToDispensement_Click(object sender, EventArgs e)
        {


            var selectedDataItem = from GridDataItem poItem in (grdProducts.MasterTableView.Items as IEnumerable)
                                   where poItem.Selected == true
                                   select poItem;

            foreach (GridDataItem poItem in selectedDataItem)
            {
                AddToDispenseQueue(poItem);
                poItem.Selected = false;
            }

            OnAddToDispensementClicked(EventArgs.Empty);
        }



        private void AddToDispenseQueue(GridDataItem poItem)
        {
            Int32 physicianID = ctlPracticePhysicians.GetPhysicianID();
            Int32 inventoryID = Convert.ToInt32(grdProducts.MasterTableView.DataKeyValues[poItem.ItemIndex]["ProductInventoryID"]);
            Decimal dmeDeposit = Convert.ToDecimal(grdProducts.MasterTableView.DataKeyValues[poItem.ItemIndex]["DMEDeposit"]);
            string side = (grdProducts.MasterTableView.DataKeyValues[poItem.ItemIndex]["Side"]).ToString();

            bool isCustomFitDefaultForObsoleteMethod = false;
            DAL.PracticeLocation.Dispensement.AddUpdateDispensement(
                                                                        DispenseQueueID: 0,
                                                                        PracticeLocationID: practiceLocationID,
                                                                        InventoryID: inventoryID,
                                                                        PatientCode: "",
                                                                        PhysicianID: physicianID,
                                                                        ICD9_Code: "",
                                                                        IsMedicare: false,
                                                                        ABNForm: false,
                                                                        IsCashCollection: false,
                                                                        Side: side,
                                                                        DMEDeposit: dmeDeposit,
                                                                        AbnType: "",
                                                                        BregVisionOrderID: null,
                                                                        Quantity: 1, 
                                                                        isCustomFit: isCustomFitDefaultForObsoleteMethod,
                                                                        mod1: "",
                                                                        mod2: "",
                                                                        mod3: "",
                                                                        DispenseDate: null,
                                                                        CreatedUser: 1,
                                                                        CreatedDate: DateTime.Now,
                                                                        QueuedReason: "",
                                                                        dispenseSignatureID: 0,
                                                                        isDeleted: false,
                                                                        IsCalledFromV1Endpoint: false
                                                                    );
        }
    }
}