﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregWcfService;

namespace BregVision.UserControls.InventoryCount
{
    public delegate void InventoryCycleTypeEventHandler(object sender, InventoryCycleTypeEventArgs e);

    public partial class InventoryCycleType : Bases.UserControlBase
    {
        public event InventoryCycleTypeEventHandler InventoryCycleTypeEvent;

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (cboInventoryCycleType.Items.Count < 1)
            {
                string userName = Context.User.Identity.Name;
                cboInventoryCycleType.DataSource = InventorySetup.GetInventoryCycleTypes(practiceLocationID, userName);
                cboInventoryCycleType.DataTextField = "CycleName";
                cboInventoryCycleType.DataValueField = "InventoryCycleTypeID";
                cboInventoryCycleType.DataBind();

                if (cboInventoryCycleType.Items.Count > 0)
                {
                    FireEvent(cboInventoryCycleType.Items[0].Value);
                }
            }
        }

        public void CauseInventoryCycleReload()
        {
            if (cboInventoryCycleType.Items.Count > 0)
            {
                FireEvent(cboInventoryCycleType.Items[0].Value);
            }
        }

        protected void cboInventoryCycleType_SelectedIndexChanged(object sender, EventArgs e)
        {
            FireEvent(cboInventoryCycleType.SelectedValue);
        }

        private void FireEvent(string inventoryCycleTypeIDValue)
        {
            Int32 inventoryCycleTypeID = 0;
            Int32.TryParse(inventoryCycleTypeIDValue, out inventoryCycleTypeID);

            if (InventoryCycleTypeEvent != null)
            {
                InventoryCycleTypeEvent(this, new InventoryCycleTypeEventArgs(inventoryCycleTypeID));
            }
        }

        public Int32 GetInventoryCycleTypeID()
        {
            Int32 inventoryCycleTypeID = 0;
            Int32.TryParse(cboInventoryCycleType.SelectedValue, out inventoryCycleTypeID);
            return inventoryCycleTypeID;
        }
    }

    public class InventoryCycleTypeEventArgs : EventArgs
    {
        public InventoryCycleTypeEventArgs(Int32 inventoryCycleTypeID)
        {
            InventoryCycleTypeID = inventoryCycleTypeID;
        }
        public Int32 InventoryCycleTypeID { get; set; }
    }
}