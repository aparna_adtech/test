﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BregWcfService;


namespace BregVision.UserControls.InventoryCount
{
    public delegate void InventoryCycleEventHandler(object sender, InventoryCycleEventArgs e);

    public partial class InventoryCycle : Bases.UserControlBase
    {
        public event InventoryCycleEventHandler InventoryCycleEvent;

        public InventoryCycleType InventoryCycleTypeControl;

        private ClassLibrary.DAL.InventoryCycle _CurrentInventoryCycle;
        public ClassLibrary.DAL.InventoryCycle CurrentInventoryCycle 
        {
            get
            {
                GetInventoryCycle();
                return _CurrentInventoryCycle; 
            }
            private set
            { 
                _CurrentInventoryCycle = value; 
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            InventoryCycleTypeControl.InventoryCycleTypeEvent += new InventoryCycleTypeEventHandler(InventoryCycleTypeControl_InventoryCycleTypeEvent);
        }

        public void InventoryCycleTypeControl_InventoryCycleTypeEvent(object sender, InventoryCycleTypeEventArgs e)
        {
            GetInventoryCycles(e);
        }

        private void GetInventoryCycles(InventoryCycleTypeEventArgs e)
        {
            string userName = Context.User.Identity.Name;

            ClassLibrary.DAL.InventoryCycleType inventoryCycleType = (from ict in InventorySetup.GetInventoryCycleTypes(practiceLocationID, userName)
                                                                      where ict.InventoryCycleTypeID == e.InventoryCycleTypeID
                                                                      select ict).FirstOrDefault<ClassLibrary.DAL.InventoryCycleType>();


            cboInventoryCycle.DataSource = InventorySetup.GetOpenInventoryCyclesByType(practiceLocationID, userName, inventoryCycleType);

            cboInventoryCycle.DataTextField = "StartDate";
            cboInventoryCycle.DataTextFormatString = "{0:d}";
            cboInventoryCycle.DataValueField = "InventoryCycleID";
            cboInventoryCycle.DataBind();

            if (cboInventoryCycle.Items.Count > 0)
            {
                FireEvent(cboInventoryCycle.Items[0].Value);
            }
        }

        

        protected void Page_PreRender(object sender, EventArgs e)
        {

        }

        protected void cboInventoryCycle_SelectedIndexChanged(object sender, EventArgs e)
        {
            FireEvent(cboInventoryCycle.SelectedValue);
        }

        private void FireEvent(string inventoryCycleIDValue)
        {
            Int32 inventoryCycleID = 0;
            Int32.TryParse(inventoryCycleIDValue, out inventoryCycleID);

            if (InventoryCycleEvent != null)
            {
                _CurrentInventoryCycle = BregWcfService.InventorySetup.GetInventoryCycle(inventoryCycleID);
                InventoryCycleEvent(this, new InventoryCycleEventArgs(inventoryCycleID, CurrentInventoryCycle));
            }
        }

        public Int32 GetInventoryCycleID()
        {
            Int32 inventoryCycleID = 0;
            if (!string.IsNullOrEmpty(cboInventoryCycle.SelectedValue))
            {
                Int32.TryParse(cboInventoryCycle.SelectedValue, out inventoryCycleID);
            }
            return inventoryCycleID;
        }

        public void GetInventoryCycle()
        {
            int inventoryCycleID = 0;
            inventoryCycleID = GetInventoryCycleID();
            if ((_CurrentInventoryCycle == null || _CurrentInventoryCycle.InventoryCycleID != inventoryCycleID) && inventoryCycleID != 0)
            {
                _CurrentInventoryCycle = BregWcfService.InventorySetup.GetInventoryCycle(inventoryCycleID);
            }
        }

        public bool IsConsignmentCycle()
        {
            GetInventoryCycle();

            if (    _CurrentInventoryCycle != null
                    && _CurrentInventoryCycle.InventoryCycleType != null 
                    && !string.IsNullOrEmpty(_CurrentInventoryCycle.InventoryCycleType.CycleName) 
                    && _CurrentInventoryCycle.InventoryCycleType.CycleName.ToLower().Contains("consignment"))
            {
                return true;
            }
            return false;
        }
    }

    public class InventoryCycleEventArgs : EventArgs
    {
        public InventoryCycleEventArgs(Int32 inventoryCycleID, ClassLibrary.DAL.InventoryCycle InventoryCycle = null)
        {
            InventoryCycleID = inventoryCycleID;

            if (InventoryCycle == null)
            {
                InventoryCycle = InventorySetup.GetInventoryCycle(inventoryCycleID);
            }
        }
        public Int32 InventoryCycleID { get; set; }
        public ClassLibrary.DAL.InventoryCycle InventoryCycle { get; set; }
    }
}