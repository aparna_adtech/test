﻿using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.Web.UI;

namespace BregVision.UserControls
{
    public partial class CustomBracesDispensed : Bases.UserControlBase
    {
        #region Properties

        public RadGrid Grid
        {
            get
            {
                return grdCustomBracesToBeDispensed;
            }
        }

        #endregion


        #region Page Events
        protected void Page_Load(object sender, EventArgs e)
        {
            
            
            
            if (BLL.Website.IsVisionExpress()) 
            {
                trCustomBraceTitle.Visible = false;
                trCustomBrace.Visible = false;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (this.Visible == true)
            { 
                grdCustomBracesToBeDispensed.Rebind();
            }
        }

        #endregion

        #region Grid Events

        protected void grdCustomBracesToBeDispensed_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable && !BLL.Website.IsVisionExpress())
            {
                DataSet ds = DAL.PracticeLocation.Dispensement.GetCustomBracesForDispense(practiceLocationID);
                grdCustomBracesToBeDispensed.DataSource = ds.Tables[0];
            }
        }



        #endregion

        #region Methods

        public void Rebind()
        {
            grdCustomBracesToBeDispensed.Rebind();
        }


        #endregion
    }
}