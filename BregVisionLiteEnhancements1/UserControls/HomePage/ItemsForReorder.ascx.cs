﻿using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.Web.UI;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BregVision.UserControls
{
    public partial class ItemsForReorder : Bases.UserControlBase
    {
        #region Private Variables

        int productsAdded = 0;
        Int32 ShoppingCartItemID = 0;

        #endregion

        #region Properties

        public RadGrid Grid
        {
            get
            {
                return grdItemsforReorder;
            }
        }

        #endregion

        #region Page Events

        new protected void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);
            //    grdItemsforReorder.Width = Unit.Percentage(80);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (this.Visible == true)
            {
                grdItemsforReorder.Rebind();
            }
        }

        private void InitializeAjaxSettings()
        {
            Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);
            if (radAjaxManager != null)
            {
                radAjaxManager.AjaxSettings.AddAjaxSetting(btnAddItemsReOrderGrid, grdItemsforReorder, ((Home)this.Page).DefaultLoadingPanel);
                radAjaxManager.AjaxSettings.AddAjaxSetting(btnAddItemsReOrderGrid, lblStatus);
                radAjaxManager.AjaxSettings.AddAjaxSetting(btnAddItemsReOrderGrid, ((Home)this.Page).Master.SiteCart.lblItemsInCartLabel);
            }
        }

        #endregion

        #region Grid Events

        protected void grdItemsforReorder_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            AddItemsForReorder();
        }

        protected void grdItemsforReorder_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                try
                {

                    GridDataItem dataItem = (GridDataItem)e.Item;
                    //Set Reorder Level
                    if (System.Convert.ToInt32(dataItem["Priority"].Text) == 1)
                    {
                        //dataItem.BackColor = System.Drawing.Color.Red;
                        dataItem.ForeColor = System.Drawing.Color.White;
                        dataItem.CssClass = "RedRow_Vision";
                        dataItem.BackColor = System.Drawing.ColorTranslator.FromHtml("#D40200");
                        ////dataItem.CssClass = "SelectedRow_Office2007";
                    }
                    if (System.Convert.ToInt32(dataItem["Priority"].Text) == 2)
                    {
                        dataItem.CssClass = "YellowRow_Vision";
                        dataItem.ForeColor = System.Drawing.Color.Black;
                        dataItem.BackColor = System.Drawing.ColorTranslator.FromHtml("#EDCD01");
                    }
                    if (System.Convert.ToInt32(dataItem["Priority"].Text) == 3)
                    {
                        dataItem.CssClass = "BlueRow_Vision";
                        dataItem.BackColor = System.Drawing.ColorTranslator.FromHtml("#0023BA");
                        dataItem.ForeColor = System.Drawing.Color.White;
                    }
                    dataItem.Cells[1].BackColor = System.Drawing.Color.White;
                }
                catch
                {
                }





            }
        }

        protected void grdItemsforReorder_OnNeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable)
            {
                DataSet ds = DAL.PracticeLocation.Inventory.GetInventoryOrderStatus(practiceID, practiceLocationID);
                grdItemsforReorder.DataSource = ds.Tables[0];

                grdItemsforReorder.PageSize = recordsDisplayedPerPage;
            }
        }

        #endregion

        #region Control Events

        protected void btnAddItemsReOrderGrid_Click(object sender, EventArgs e)
        {
            AddItemsForReorder();
        }

        #endregion

        #region Methods

        public void Rebind()
        {
            grdItemsforReorder.Rebind();
        }

        public void AddItemsForReorder()
        {
            AddItemsForReorder(grdItemsforReorder.MasterTableView);
        }

        private void AddItemsForReorder(GridTableView gridTableView)
        {
            ShoppingCartItemID = 0;
            foreach (GridDataItem gridDataItem in gridTableView.Items)
                //foreach (GridDataItem item in RadGrid1.SelectedItems) 
                if (findSelectColumn(gridDataItem))
                {
                    //lblStatus.Text = "Adding items to the cart.";


                    //  20071019 1359 JB get the suggestedReorderLevel to add to the cart.
                    //Int16 ReOrderQuantity = Convert.ToInt16(findReorderLevel(gridDataItem) - findQOH(gridDataItem));
                    Int16 ReOrderQuantity = Convert.ToInt16(findReorderQuantity(gridDataItem));
                    // jb end change


                    //Int16 ReOrderQuantity = findQOH(gridDataItem); 
                    if (ReOrderQuantity <= 0)
                    {
                        ReOrderQuantity = 0;
                    }
                    else
                    {
                        try
                        {
                            ShoppingCartItemID = UpdateGrdInventoryData(Convert.ToInt16(Session["PracticeLocationID"]), findPracticeCatalogProductID(gridDataItem), 1, ReOrderQuantity);
                            productsAdded += ReOrderQuantity;
                            gridDataItem.Selected = false;
                        }
                        catch (System.Data.SqlClient.SqlException ex)
                        {
                            if (ex.Class == 17 && ex.Number == 50000)
                            {
                                string x = ex.Message;
                                lblStatus.Text = "No products added to cart (Custom Braces must be the only item in the cart)";
                                lblStatus.Visible = true;
                                return;
                            }
                            if (ex.Class == 18 && ex.Number == 50000)
                            {
                                string x = ex.Message;
                                lblStatus.Text = "No products added to cart (Custom Braces quantity must be 1)";
                                lblStatus.Visible = true;
                                return;
                            }
                        }
                    }

                    //Set Status indicator

                    if (productsAdded == 0)
                    {
                        //string StatusText = string.Concat("Number of products added to cart: ", productsAdded.ToString(), " No products added due to QOH higher than reorder level. Please visit the Inventory page to add products.");
                        string StatusText = "No products added to cart";
                        lblStatus.Text = StatusText;
                    }
                    else
                    {
                        string StatusText = string.Concat("Number of products added to cart: ", productsAdded.ToString());
                        lblStatus.Text = StatusText;
                    }
                    lblStatus.Visible = true;

                }




        }

        private Boolean findSelectColumn(GridDataItem gridDataItem)
        {
            TableCell cell = gridDataItem["ClientSelectColumn"];
            CheckBox checkBox = (CheckBox)cell.Controls[0];
            if (checkBox.Checked)
                return true;
            else
            {
                return false;
            }
        }

        // 20071019 JB  Added Copied from greg's inventory page.
        private Int16 findReorderQuantity(GridDataItem gridDataItem)
        {
            RadNumericTextBox nTextBox = (RadNumericTextBox)gridDataItem.FindControl("txtSuggReorderLevel");
            //CheckBox checkBox = (CheckBox)gridDataItem.FindControl("chkAddtoCart");

            if (nTextBox != null)
            {
                return Int16.Parse(nTextBox.Value.ToString());
                //Response.Write(gridDataItem.ItemIndex.ToString() + " is: " + nTextBox.Value.ToString() + "<br />");
            }
            else
            {
                return 0;
                //throw new Exception("Cannot find object with ID 'CheckBox1' in item " + gridDataItem.ItemIndex.ToString());
            }
        }

        private Int32 findPracticeCatalogProductID(GridDataItem gridDataItem)
        {
            string PracticeID = (string)gridDataItem["practicecatalogproductid"].Text;
            if (PracticeID != null)
            {
                return Int32.Parse(PracticeID);
                //Response.Write(gridDataItem.ItemIndex.ToString() + " is: " + PracticeID.ToString() + "<br />");
            }
            else
            {
                return 0;
            }

        }

        private Int32 UpdateGrdInventoryData(Int16 practiceLocationID, Int32 practiceCatalogProductID, Int16 UserID, Int16 Quantity)
        {
            return DAL.PracticeLocation.Inventory.UpdateInventoryData(practiceLocationID, practiceCatalogProductID, UserID, Quantity);
        }

        #endregion

    }
}