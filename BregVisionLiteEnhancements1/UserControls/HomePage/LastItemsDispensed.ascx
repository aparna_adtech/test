﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LastItemsDispensed.ascx.cs" Inherits="BregVision.UserControls.LastItemsDispensed" %>

<table width="100%">
    <tr id="trLast20Title" runat="server">
        <td style="height: 28px" valign="bottom" class="GridSubTitle">
        Last 20 items Dispensed</td>
    </tr>
    <tr id="trLast20" runat="server">
            <td>
                <bvu:VisionRadGridAjax ID="grdLast10ItemsDispensed" runat="server" 
                        Width="99%" EnableViewState="false" AllowAutomaticUpdates="True"
                        OnNeedDataSource="grdLast10ItemsDispensed_OnNeedDataSource" >
                    <MasterTableView Width="100%">
                          <Columns>
                            <telerik:GridBoundColumn DataField="SupplierName" HeaderText="Supplier" UniqueName="SupplierName" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                                <HeaderStyle Width="180px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Name" HeaderText="Product" UniqueName="Name" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                                <HeaderStyle Width="180px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center" HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="left" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                                <HeaderStyle Width="60px" />
                            </telerik:GridBoundColumn>

                            <telerik:GridBoundColumn DataField="FirstName " HeaderText="Physician" UniqueName="FirstName" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                                <HeaderStyle Width="40px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="LastName" HeaderText="Patient Code" UniqueName="LastName" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                                <HeaderStyle Width="70px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Side" HeaderText="Side" UniqueName="Side" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                                <HeaderStyle Width="40px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Size" HeaderText="Size" UniqueName="Size" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                                <HeaderStyle Width="40px" />
                            </telerik:GridBoundColumn>

                             <telerik:GridBoundColumn DataField="Quantity" HeaderText="Quantity" UniqueName="Quantity" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center"  HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                                <HeaderStyle Width="40px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="WholesaleCost" DataFormatString="{0:$###,###.##}" HeaderText="Unit Cost"
                                UniqueName="Total" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center"  HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid"/>

                            <telerik:GridBoundColumn DataField="Total" DataFormatString="{0:$###,###.##}" HeaderText="Billing Cost"
                                UniqueName="Total" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center"  HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid"/>
                                
                            <telerik:GridBoundColumn DataField="DateDispensed" HeaderText="Date Dispensed" UniqueName="DateDispensed" ItemStyle-Width="100" ItemStyle-HorizontalAlign="center"  HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid"/>

                        </Columns>
                    </MasterTableView>
                </bvu:VisionRadGridAjax>
            </td>
        </tr>
</table>
