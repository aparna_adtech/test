﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DeadStock.ascx.cs" Inherits="BregVision.UserControls.DeadStock" %>
<table width="100%">
    <tr id="trDeadStockTitle" runat="server">
        <td style="height: 28px" valign="bottom" class="GridSubTitle">
        Dead Stock
        </td>
    </tr>
    <tr id="trDeadStock" runat="server">
        <td>
            <bvu:VisionRadGridAjax ID="grdDeadStock" runat="server" 
                        OnNeedDataSource="grdDeadStock_OnNeedDataSource" 
                        EnableViewState="false"
                        Width="99%" >
                <MasterTableView Width="100%">                   
                    <Columns>
                        <telerik:GridBoundColumn DataField="SupplierName" HeaderText="Supplier" UniqueName="SupplierName" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Name" HeaderText="Product" UniqueName="Name" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code" ItemStyle-Width="150" HeaderStyle-Wrap="true" HeaderStyle-Width="150" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="QuantityonHandPerSystem" HeaderText="Qty. On Hand" UniqueName="QuantityonHandPerSystem" ItemStyle-Width="100" ItemStyle-HorizontalAlign="center"  HeaderStyle-Wrap="true" HeaderStyle-Width="100" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ParLevel" HeaderText="Par Level" UniqueName="ParLevel" ItemStyle-Width="80" ItemStyle-HorizontalAlign="center" HeaderStyle-Wrap="true" HeaderStyle-Width="80" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ReorderLevel" HeaderText="Reorder Level" UniqueName="ReorderLevel" ItemStyle-Width="80" ItemStyle-HorizontalAlign="center"  HeaderStyle-Wrap="true" HeaderStyle-Width="80" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CriticalLevel" HeaderText="Critical Level" UniqueName="CriticalLevel" ItemStyle-Width="80" ItemStyle-HorizontalAlign="center"  HeaderStyle-Wrap="true" HeaderStyle-Width="80" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </bvu:VisionRadGridAjax>
        </td>
    </tr> 
</table>
