﻿using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.Web.UI;

namespace BregVision.UserControls
{
    public partial class DeadStock : Bases.UserControlBase
    {
        #region Properties

        public RadGrid Grid
        {
            get
            {
                return grdDeadStock;
            }
        }

        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (BLL.Website.IsVisionExpress()) 
            {
                trDeadStockTitle.Visible = false;
                trDeadStock.Visible = false;
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (this.Visible == true)
            {
                grdDeadStock.Rebind();
            }
        }
        #endregion

        #region Grid Events

        protected void grdDeadStock_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable && !BLL.Website.IsVisionExpress())
            {
                
                DataSet ds = DAL.PracticeLocation.Inventory.GetDeadStock(practiceID, practiceLocationID);
                grdDeadStock.DataSource = ds.Tables[0];

                grdDeadStock.PageSize = recordsDisplayedPerPage;
            }
        }

        #endregion

        #region Methods


        public void Rebind()
        {
            grdDeadStock.Rebind();
        }

        #endregion
    }
}