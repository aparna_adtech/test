﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ItemsForReorder.ascx.cs" Inherits="BregVision.UserControls.ItemsForReorder" %>


<table width="100%">
    <tr>
        <td>
            <asp:Label ID="lblStatus" runat="server" CssClass="WarningMsg"></asp:Label>
        </td>
    </tr>
   <tr>
        <td>
            <table width="99%">
                <tr>
                    <td class="GridSubTitle">
                        Inventory Order Status&nbsp;</td>
                    <td  align="right" valign="bottom">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/OrderStatusLegend.gif" />
                    </td>
                    <td valign="middle" align="right">
                        <asp:Button runat="server" ID="btnAddItemsReOrderGrid" Text="Add to Cart" OnClick="btnAddItemsReOrderGrid_Click" />
                        <bvu:Help ID="ctlHelp1" runat="server" HelpID="71" />
                    </td>
                </tr>
           </table>
      </td>              
   </tr>
    <tr>
        <td>
            <bvu:VisionRadGridAjax ID="grdItemsforReorder" runat="server" 
                    AllowAutomaticUpdates="True" EnableViewState="false" 
                    AllowFilteringByColumn="false" 
                    Width="100%" 
                    AllowMultiRowSelection="True" 
                    OnNeedDataSource="grdItemsforReorder_OnNeedDataSource" 
                    OnPageIndexChanged="grdItemsforReorder_PageIndexChanged" 
                    OnItemDataBound="grdItemsforReorder_ItemDataBound">
                <MasterTableView>
                    <Columns>
                        <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" HeaderStyle-Width="35px" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid" />
                        <telerik:GridBoundColumn DataField="SupplierName" HeaderText="Supplier" HeaderStyle-Width="280px" UniqueName="SupplierName" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                            
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="MCName" HeaderText="Product" HeaderStyle-Width="375px" UniqueName="MCName" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                            
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center" HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid"/>
                        
                        
                        <telerik:GridBoundColumn DataField="LeftRightSide" HeaderText="Side" UniqueName="LeftRightSide" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                        </telerik:GridBoundColumn>
                        
                        <telerik:GridBoundColumn DataField="Size" HeaderText="Size" UniqueName="Size" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                        </telerik:GridBoundColumn>    
                        
                            
                        <telerik:GridTemplateColumn  UniqueName="QuantityOnHand" DataField="QuantityOnHand" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center"  HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                            <HeaderTemplate>
                                <asp:Label ID="Label1" runat="server">Qty. On Hand</asp:Label>
                                <bvu:Help ID="help65" runat="server" HelpID="65" />
                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="labQuantityOnHand" runat="server" Text='<%# bind("QuantityOnHand") %>' />                                </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        
                        <telerik:GridTemplateColumn  UniqueName="QuantityOnOrder" DataField="QuantityOnOrder" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center"  HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                            <HeaderTemplate>
                                <asp:Label ID="Label2" runat="server">Qty. On Order</asp:Label>
                                <bvu:Help ID="help66" runat="server" HelpID="66" />
                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="labQuantityOnOrder"  runat="server" Text='<%# bind("QuantityOnOrder") %>' />                                </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        
                        <telerik:GridTemplateColumn  UniqueName="ParLevel" DataField="ParLevel" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center"  HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                            <HeaderTemplate>
                                <asp:Label ID="Label3" runat="server">Par Level</asp:Label>
                                <bvu:Help ID="help67" runat="server" HelpID="67" />
                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="labParLevel" runat="server" Text='<%# bind("ParLevel") %>' />                                </ItemTemplate>
                        </telerik:GridTemplateColumn>
                                                                                    
                        <telerik:GridTemplateColumn  UniqueName="ReorderLevel" DataField="ReorderLevel" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center"  HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                            <HeaderTemplate>
                                <asp:Label ID="Label4" runat="server">Reorder Level</asp:Label>
                                <bvu:Help ID="help68" runat="server" HelpID="68" />
                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="labReorderLevel" runat="server" Text='<%# bind("ReorderLevel") %>' />                                
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        
                        <telerik:GridTemplateColumn  UniqueName="CriticalLevel" DataField="CriticalLevel" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center"  HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                            <HeaderTemplate>
                                <asp:Label ID="Label5" runat="server">Critical Level</asp:Label>
                                <bvu:Help ID="help69" runat="server" HelpID="69" />
                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="labCriticalLevel" runat="server" Text='<%# bind("CriticalLevel") %>' />                                
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        
                        
                        <telerik:GridBoundColumn DataField="Priority" UniqueName="Priority" Visible="False">
                        </telerik:GridBoundColumn>
                        
                        <telerik:GridTemplateColumn  UniqueName="SuggestedReorderLevel" DataField="SuggestedReorderLevel" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center"  HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center"  HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                            <HeaderTemplate>
                                <asp:Label ID="Label6" runat="server">Suggested Order Qty.</asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                &nbsp;<telerik:RadNumericTextBox  ID="txtSuggReorderLevel" Text='<%# bind("SuggestedReorderLevel") %>' runat="server" ShowSpinButtons="True" Culture="English (United States)" MaxValue="999" MinValue="0" Skin="Web20" SpinDownCssClass="Web20" SpinUpCssClass="Web20" Width="40" NumberFormat-DecimalDigits="0">
                                    <NumberFormat DecimalDigits="0" />
                                </telerik:RadNumericTextBox>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        
                        <telerik:GridTemplateColumn  UniqueName="StockingUnits" DataField="StockingUnits" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center"  HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                            <HeaderTemplate>
                                <asp:Label ID="Label7" runat="server">SU</asp:Label>
                                <bvu:Help ID="help72" runat="server" HelpID="72" />
                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="labStockingUnits" runat="server" Text='<%# bind("StockingUnits") %>' />                                
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        
                        <telerik:GridBoundColumn DataField="practicecatalogproductid" UniqueName="practicecatalogproductid"
                            Display="False">
                            
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings AllowColumnsReorder="True" AllowDragToGroup="True" ReorderColumnsOnClient="True">
                  <Selecting AllowRowSelect="True"/>
                </ClientSettings>
                    
            </bvu:VisionRadGridAjax>
        </td>
    </tr>
 </table>