﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomBracesDispensed.ascx.cs" Inherits="BregVision.UserControls.CustomBracesDispensed" %>
<table width="100%">
    <tr  id="trCustomBraceTitle" runat="server">
        <td style="height: 28px" valign="bottom" class="GridSubTitle">
        Custom Braces to be Dispensed</td>
    </tr>
    <tr id="trCustomBrace" runat="server">
        <td>
            <bvu:VisionRadGridAjax ID="grdCustomBracesToBeDispensed" runat="server" 
                    OnNeedDataSource="grdCustomBracesToBeDispensed_OnNeedDataSource" 
                    EnableViewState="false" 
                    Width="99%" >
                <MasterTableView Width="100%">
                      <Columns>
                        <telerik:GridBoundColumn DataField="SupplierName" HeaderText="Supplier" UniqueName="SupplierName" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                            <HeaderStyle Width="80px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ProductName" HeaderText="Product" UniqueName="ProductName" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                            <HeaderStyle Width="180px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code" ItemStyle-Width="50" HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                            <HeaderStyle Width="60px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PurchaseOrder" HeaderText="PO Number" UniqueName="PurchaseOrder" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                            <HeaderStyle Width="80px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CustomPurchaseOrderCode" HeaderText="Custom PO Number" UniqueName="CustomPurchaseOrderCode" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                            <HeaderStyle Width="80px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PatientCode" HeaderText="Patient ID" UniqueName="PatientCode" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                            <HeaderStyle Width="80px" />
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn DataField="FirstName" HeaderText="Physician" UniqueName="FirstName"  HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid">
                            <HeaderStyle Width="40px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LastName" HeaderText="" UniqueName="LastName">
                            <HeaderStyle Width="50px" />
                        </telerik:GridBoundColumn>
                        
                        <telerik:GridBoundColumn DataField="WholesaleCost" DataFormatString="{0:$###,###.##}" HeaderText="Unit Cost"
                            UniqueName="WholesaleCost" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center" HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid"/>
                            
                        <telerik:GridBoundColumn DataField="BillingCost" DataFormatString="{0:$###,###.##}" HeaderText="Billing Cost"
                            UniqueName="BillingTotal" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center" HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid"/>
                            
                        <telerik:GridBoundColumn DataField="DateCheckedIn" HeaderText="Date Checked In" UniqueName="DateCheckedIn" ItemStyle-Width="120" ItemStyle-HorizontalAlign="center" HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid"/>
                            
                    </Columns>
                </MasterTableView>
            </bvu:VisionRadGridAjax>
        </td>
    </tr>
</table>
