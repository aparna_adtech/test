﻿using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.Web.UI;


namespace BregVision.UserControls
{
    public partial class LastItemsDispensed : Bases.UserControlBase
    {
        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if(BLL.Website.IsVisionExpress()) 
            {
                trLast20Title.Visible = false;
                trLast20.Visible = false;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (this.Visible == true)
            {
                grdLast10ItemsDispensed.Rebind();
            }
        }
        #endregion

        #region Grid Events

        public void grdLast10ItemsDispensed_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable && !BLL.Website.IsVisionExpress())
            {
                DataSet ds = DAL.PracticeLocation.Dispensement.GetLastItemsDispensed(practiceID, practiceLocationID);
                grdLast10ItemsDispensed.DataSource = ds.Tables[0];
            }
        }

        #endregion

        #region Methods

        public void Rebind()
        {
            grdLast10ItemsDispensed.Rebind();
        }

        #endregion
    }
}