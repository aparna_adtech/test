﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchProducts.ascx.cs" Inherits="BregVision.UserControls.SearchProducts" %>


<telerik:RadWindow ID="radWindowCampaignInfo" runat="server" Modal="true" Enabled="true" 
            IconUrl="~/Images/People16.png"
            AnimationDuration="0"
            Animation="None" VisibleOnPageLoad="false" 
            EnableEmbeddedSkins="true" Skin="Office2007" 
            Height="275px" Width="600px" 
            Behaviors="Move,Close"  
            Title="Would you like us to contact you?" 
            VisibleStatusbar="false">
            <ContentTemplate >
                <div align="center" style="padding:10px">
                    <asp:Label ID="Label1" runat="server" Text="These items have been loaded in your search grid." />
                    </br>
                    <asp:Label ID="Label2" runat="server" Text="Would you like a Breg Account rep or Product" />
                    </br>
                    <asp:Label ID="Label4" runat="server" Text="Specialist to contact you for more information?" />
                    </br>
                    </br>
                    
                    <table style="padding:0px;height:60px;width:335px">
                        <tr><td align="left">Name:</td><td><asp:TextBox runat="server" ID="txtCampaignUserName" Width="200" /></td></tr>
                        <tr><td align="left">Phone:</td><td><asp:TextBox runat="server" ID="txtCampaignUserPhone" Width="200" /></td></tr>
                        <tr><td align="left">Best time to call:</td><td><asp:TextBox runat="server" ID="txtBestTimeToCall" Width="200" /></td></tr>
                    </table>
                    
                    </br>
                    </br>
                    <asp:Button runat="server" ID="btnSendCampaignRequest" Text="Yes" OnClick="btnSendCampaignRequest_Click" Width="120" />
                    &nbsp;
                    <asp:Button runat="server" ID="btnCloseCampaignInfoWindow" Text="No Thanks" Width="120" />
                </div>
            </ContentTemplate>
</telerik:RadWindow>

<telerik:RadCodeBlock ID="radWindowCampaignInfoWindow" runat="server">
    <script type="text/javascript">
        //<![CDATA[
        function radWindowCampaignInfo_Close(clientID) {
            var radWindow = $find(clientID);
            radWindow.close();
            return false;
        }
        //]]>
    </script>
</telerik:RadCodeBlock>


<table cellpadding="0" cellspacing="0" width="100%" border="0">
    <tr>
        <td colspan="4">
            <asp:Label ID="lblStatus2" runat="server" CssClass="WarningMsg"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <asp:Label ID="lblBrowseDispense" runat="server" CssClass="WarningMsg"></asp:Label>&#160;
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        <bvu:SearchToolbar id="ctlSearchToolbar" runat="server"></bvu:SearchToolbar>
                    </td>
                    <td>
                        <bvu:Help ID="help21" runat="server" HelpID="21" />
                        
                    </td>
                </tr>
            </table>
        </td>
        <td>&nbsp
            <asp:Button runat="server" ID="btnAddItemsSearch" Text="Add to Cart" Enabled="false" Visible="false" OnClick="btnAddItemsSearch_Click" />
            <bvu:Help ID="Help3" runat="server" HelpID="64" Visible="false" />
        </td>
        <td align="right">
            <bvu:RecordsPerPage ID="ctlRecordsPerPage" runat="server" />
        </td>
     </tr>
     <tr style="width:100%">
        <td colspan="4" style="width:100%">
        
            <bvu:VisionRadGridAjax ID="grdSearch" runat="server" 
                    AllowMultiRowSelection="True" 
                    OnLoad="grdSearch_OnLoad" 
                    Visible="false" 
                    OnPageIndexChanged="grdSearch_PageIndexChanged" 
                    OnNeedDataSource="grdSearch_NeedDataSource" Width="99%">
            <MasterTableView AutoGenerateColumns="False" Width="100%">
                <Columns>
                    <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" HeaderText="" HeaderStyle-Width="35px" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid"/>

                    <telerik:GridBoundColumn DataField="BrandName" HeaderText="Brand" UniqueName="BrandName" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid" />
                    
                    <telerik:GridBoundColumn DataField="ProductName" HeaderText="Product" UniqueName="ProductName" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid" />
                   
                    <telerik:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center" HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid" />
                    
                    <telerik:GridBoundColumn DataField="Side" HeaderText="Side" UniqueName="LeftRightSide" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center" HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid" />
                   
                    <telerik:GridBoundColumn DataField="Size" HeaderText="Size" UniqueName="Size" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center" HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid" />
                    
                    <telerik:GridBoundColumn DataField="QOH" HeaderText="Qty. On Hand" UniqueName="QOH" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center" HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid" />
                    
                    <telerik:GridTemplateColumn DataField="SuggestedReorderLevel" HeaderStyle-BorderColor="#A2B6CB"
                        HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderStyle-Width="50" 
                         HeaderStyle-Wrap="true" UniqueName="SuggestedReorderLevel">
                        <HeaderTemplate>
                            <asp:Label ID="Label3" runat="server" Text="Suggested Order Qty." ></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <telerik:RadNumericTextBox  ID="txtSuggReorderLevel" runat="server" Culture="English (United States)" 
                                    ShowSpinButtons="True"  MaxValue="999" MinValue="0" 
                                     Skin="Web20"
                                     Type="Number" Width="30px" Value="1"><NumberFormat DecimalDigits="0" />
                                
                            </telerik:RadNumericTextBox>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    
                    <telerik:GridBoundColumn DataField="ProductInventoryID" UniqueName="ProductInventoryID" Display="False" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid" />
                      
                    
                    <telerik:GridBoundColumn DataField="SupplierName" HeaderText="Supplier" UniqueName="SupplierName" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid" />
                    
                    <telerik:GridBoundColumn DataField="MasterCatalogProductID" UniqueName="MasterCatalogProductID" Display="false" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid" />
                           
                    <telerik:GridBoundColumn DataField="PracticeCatalogProductID" UniqueName="PracticeCatalogProductID" Display="false" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid" />
                             
                </Columns>
            </MasterTableView>
        </bvu:VisionRadGridAjax>&nbsp;
        </td>
     </tr>
</table>