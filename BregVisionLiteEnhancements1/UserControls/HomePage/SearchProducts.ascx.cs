﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.Web.UI;
using BregVision.Campaigns;
using BregVision.UserControls.General;

namespace BregVision.UserControls
{
    public delegate void RecordsPerPageChangedEventHandler(object sender, RecordsPerPageEventArgs e);

    public partial class SearchProducts : Bases.UserControlBase
    {
        #region Private Variables
        private Home HomePage;
       
        int productsAdded = 0;
        Int32 ShoppingCartItemID = 0;
        public event RecordsPerPageChangedEventHandler recordsPerPageChangedEvent;

        #endregion

        #region Public Properties

        public RadNumericTextBox RecordsToDisplay
        {
            get
            {
                return ctlRecordsPerPage.RecordsDisplayedTextBox; 
            }
        }

         

        #endregion

        #region Page Events

        new protected void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);
            HomePage = ((Home)this.Page);

            ctlRecordsPerPage.recordsPerPageChangedEvent += new RecordsPerPageEventHandler(ctlRecordsPerPage_recordsPerPageChangedEvent);
            ctlSearchToolbar.searchEvent += new SearchEventHandler(ctlSearchToolbar_searchEvent);

            ctlSearchToolbar.HideBrowse();
            ctlSearchToolbar.Grid = grdSearch;
            ctlSearchToolbar.DefaultLoadingPanel = HomePage.DefaultLoadingPanel;

            ctlSearchToolbar.ClearDropdownButtons();
            ctlSearchToolbar.AddButton("Product Code", "Code");
            ctlSearchToolbar.AddButton("Product Name", "Name");
            ctlSearchToolbar.AddButton("Manufacturer", "Brand");
            ctlSearchToolbar.AddButton("HCPCs", "HCPCs");
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeAjaxSettings();
            btnCloseCampaignInfoWindow.OnClientClick = String.Format("return radWindowCampaignInfo_Close('{0}')", radWindowCampaignInfo.ClientID);

            if (Page.IsPostBack)
            {
                ShowEmailCampaignWindow(false);
            }
            else
            {
                if (Request["CampaignId"] != null)
                {
                    try
                    {
                        string searchCriteria = "CampaignId";
                        string searchText = Request["CampaignId"];
                        DoCampaignSearch(searchCriteria, searchText);
                    }
                    catch { }
                }
            }
        }

        private void InitializeAjaxSettings()
        {
            Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);

            radAjaxManager.AjaxSettings.AddAjaxSetting(grdSearch, grdSearch);
            radAjaxManager.AjaxSettings.AddAjaxSetting(ctlSearchToolbar.Toolbar, grdSearch, HomePage.DefaultLoadingPanel);
            radAjaxManager.AjaxSettings.AddAjaxSetting(ctlSearchToolbar.Toolbar, btnAddItemsSearch);
            radAjaxManager.AjaxSettings.AddAjaxSetting(ctlSearchToolbar.Toolbar, Help3);
            radAjaxManager.AjaxSettings.AddAjaxSetting(btnAddItemsSearch, grdSearch, HomePage.DefaultLoadingPanel);
            radAjaxManager.AjaxSettings.AddAjaxSetting(btnAddItemsSearch, lblStatus2);
            radAjaxManager.AjaxSettings.AddAjaxSetting(btnAddItemsSearch, ((Home)this.Page).Master.SiteCart.lblItemsInCartLabel);
         
        }

        void ctlSearchToolbar_searchEvent(object sender, General.SearchEventArgs e)
        {
            grdSearch.SearchText = e.SearchText;
            grdSearch.SearchCriteria = e.SearchCriteria;
            grdSearch.SearchType = e.SearchType;
            grdSearch.SetSearchCriteria(e.SearchType);
            grdSearch.Visible = true;
            btnAddItemsSearch.Enabled = true;
            btnAddItemsSearch.Visible = true;
            Help3.Visible = true;
        }

        #endregion

        #region Grid Events

        protected void grdSearch_OnLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["CampaignId"] == null)
                {
                    //grdSearch.DataSource = new Object[] { };
                    //grdSearch.DataBind(); //Used to show a 'No Records Found' when the page initially loads
                    grdSearch.Visible = true;
                }
            }
        }

        protected void grdSearch_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {

            string searchCriteria = grdSearch.SearchCriteria;
            string searchText = grdSearch.SearchText ;

            if (searchText != null && !string.IsNullOrEmpty(searchCriteria))
            {
                grdSearch.DataSource = SearchForItemsInventory(practiceLocationID, searchCriteria, searchText); ;
            }
            grdSearch.SetGridLabel(searchCriteria, Page.IsPostBack, lblBrowseDispense);
            grdSearch.PageSize = recordsDisplayedPerPage;
        }

        protected void grdSearch_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            grdSearch.SetExpansionOnPageIndexChanged();
            AddSearchItemsToCart(grdSearch.MasterTableView); //MWS might need to reimplement this for new telerik controls
        }


        #endregion

        #region Toolbar Events


        protected void btnAddItemsSearch_Click(object sender, EventArgs e)
        {
            lblBrowseDispense.Visible = true;
            lblStatus2.Visible = true;

            AddSearchItemsToCart(grdSearch.MasterTableView);
        }

        #endregion

        #region Control Events
        void ctlRecordsPerPage_recordsPerPageChangedEvent(object sender, BregVision.UserControls.General.RecordsPerPageEventArgs e)
        {
            if (recordsPerPageChangedEvent != null)
            {
                RecordsPerPageEventArgs eventArgs = new RecordsPerPageEventArgs(e.NewRecordsPerPage);
                recordsPerPageChangedEvent(this, eventArgs);
            }
        }



        protected void btnSendCampaignRequest_Click(object sender, EventArgs e)
        {
            DataSet productsDataSet = SearchForItemsInventory(practiceLocationID, "CampaignId", Request["CampaignId"]);

            CampaignService.RequestContact(userID, practiceName, int.Parse(Request["CampaignId"]), txtCampaignUserName.Text, txtCampaignUserPhone.Text, txtBestTimeToCall.Text, productsDataSet);
        }

        #endregion

        #region Methods

        private void DoCampaignSearch(string searchCriteria, string searchText)
        {
            //grdSearch.DataSource = SearchForItemsInventory(practiceLocationID, searchCriteria, searchText);

            //grdSearch.SetGridLabel(searchCriteria, Page.IsPostBack, lblBrowseDispense);

            grdSearch.SearchCriteria = searchCriteria;
            grdSearch.SearchText = searchText;
            grdSearch.SearchType = "Search";
            grdSearch.SetSearchCriteria("Search");

            grdSearch.Rebind();
            if (grdSearch.Items.Count == 0)
            {
                btnAddItemsSearch.Enabled = false;
            }
            else
            {
                btnAddItemsSearch.Enabled = true;
            }

            //btnAddItemsSearch.Enabled = true;
            grdSearch.Visible = true;

            ShowEmailCampaignWindow(true);

        }

        private void ShowEmailCampaignWindow(bool isVisible)
        {
            radWindowCampaignInfo.VisibleOnPageLoad = isVisible;
            radWindowCampaignInfo.Enabled = isVisible;
        }

        public DataSet SearchForItemsInventory(int practiceLocationID, string searchCriteria, string searchText)
        {
            DataSet ds = DAL.PracticeLocation.Inventory.SearchForInventoryItems(practiceLocationID, searchCriteria, searchText);

            return ds;
        }

        private void AddSearchItemsToCart(GridTableView gridTableView)
        {
            ShoppingCartItemID = 0;
            foreach (GridDataItem gridDataItem in gridTableView.Items)
                if (findSelectColumn(gridDataItem))
                {
                    Int16 reorderQuantity = Convert.ToInt16(findReorderQuantity(gridDataItem));
                    if (reorderQuantity <= 0)
                    {
                        reorderQuantity = 0;
                    }
                    else
                    {
                        try
                        {
                            string productName = findProductName(gridDataItem);

                            int productInventoryID = findProductInventoryID(gridDataItem);
                            int practiceCatalogProductID = findPracticeCatalogProductID(gridDataItem);

                            if (practiceCatalogProductID == 0)
                            { //the item hasn't been added to the practice Catalog yet
                                int masterCatalogProductID = findMasterCatalogProductID(gridDataItem);
                                if (masterCatalogProductID > 0)
                                {
                                    //we got a masterCatalogID so add to the Practice Catalog
                                    practiceCatalogProductID = DAL.Practice.Catalog.UpdatePracticeCatalog(practiceID, masterCatalogProductID, userID);
                                }
                            }

                            if (productInventoryID == 0)
                            {
                                //we don't have a productInventoryID so add to product inventory
                                DAL.PracticeLocation.Inventory.TransferToProductInventory(practiceLocationID, practiceCatalogProductID, userID);
                            }

                            if (practiceCatalogProductID > 0)
                            {
                                ShoppingCartItemID = UpdateGrdInventoryData(practiceLocationID, practiceCatalogProductID, 1, reorderQuantity);
                                if (productName.ToLower().Contains("boa"))
                                {
                                    string userName = Context.User.Identity.Name;
                                    BLL.Practice.UserClick userClick = new BLL.Practice.UserClick(userID, userName, practiceID, 0, "Home", "", "RadDock", "Boa Product added to cart", "BOA Line", practiceCatalogProductID.ToString(), "", "", "");
                                }
                                productsAdded += reorderQuantity;
                            }
                        }
                        catch (System.Data.SqlClient.SqlException ex)
                        {
                            if (ex.Class == 17 && ex.Number == 50000)
                            {
                                string x = ex.Message;
                                lblStatus2.Text = "No products added to cart (Custom Braces must be the only item in the cart)";
                                lblStatus2.Visible = true;
                                return;
                            }
                            if (ex.Class == 18 && ex.Number == 50000)
                            {
                                string x = ex.Message;
                                lblStatus2.Text = "No products added to cart (Custom Braces quantity must be 1)";
                                lblStatus2.Visible = true;
                                return;
                            }
                        }
                    }

                    //Set Status indicator
                    if (productsAdded == 0)
                    {
                        string StatusText = "No products added to cart";
                        lblStatus2.Text = StatusText;
                    }
                    else
                    {
                        string StatusText = string.Concat("Number of products added to cart: ", productsAdded.ToString());
                        lblStatus2.Text = StatusText;
                    }
                    gridDataItem.Selected = false;
                    lblStatus2.Visible = true;

                    lblStatus2.Visible = true;
                    grdSearch.Visible = true;

                }


        }

        private Int32 UpdateGrdInventoryData(Int32 practiceLocationID, Int32 practiceCatalogProductID, Int32 UserID, Int32 Quantity)
        {
            return DAL.PracticeLocation.Inventory.UpdateInventoryData(practiceLocationID, practiceCatalogProductID, UserID, Quantity);
        }

        private Boolean findSelectColumn(GridDataItem gridDataItem)
        {
            TableCell cell = gridDataItem["ClientSelectColumn"];
            CheckBox checkBox = (CheckBox)cell.Controls[0];
            if (checkBox.Checked)
                return true;
            else
            {
                return false;
            }
        }

        private string findProductName(GridDataItem gridDataItem)
        {
            string productName = (string)gridDataItem["productname"].Text;
            if (productName != null)
            {
                return productName;
            }
            else
            {
                return "";
            }

        }

        private Int32 findProductInventoryID(GridDataItem gridDataItem)
        {
            string productInventoryID = (string)gridDataItem["productinventoryid"].Text;
            if (productInventoryID != null)
            {
                return Int32.Parse(productInventoryID);
                //Response.Write(gridDataItem.ItemIndex.ToString() + " is: " + PracticeID.ToString() + "<br />");
            }
            else
            {
                return 0;
            }

        }

        private Int32 findPracticeCatalogProductID(GridDataItem gridDataItem)
        {
            string PracticeID = (string)gridDataItem["practicecatalogproductid"].Text;
            if (PracticeID != null)
            {
                return Int32.Parse(PracticeID);
                //Response.Write(gridDataItem.ItemIndex.ToString() + " is: " + PracticeID.ToString() + "<br />");
            }
            else
            {
                return 0;
            }

        }

        private Int32 findMasterCatalogProductID(GridDataItem gridDataItem)
        {
            GridColumn mcpID = gridDataItem.OwnerTableView.GetColumnSafe("mastercatalogproductid");
            if (mcpID != null)
            {
                string masterCatalogProductID = (string)gridDataItem["mastercatalogproductid"].Text;
                if (masterCatalogProductID != null)
                {
                    return Int32.Parse(masterCatalogProductID);
                    //Response.Write(gridDataItem.ItemIndex.ToString() + " is: " + PracticeID.ToString() + "<br />");
                }
                else
                {
                    return 0;
                }
            }
            return -1;

        }

        private Int16 findQOH(GridDataItem gridDataItem)
        {
            string QOH = (string)gridDataItem["QuantityOnHand"].Text;
            if (QOH != null)
            {
                return Int16.Parse(QOH);

            }
            else
            {
                return 0;
            }

        }

        // 20071019 JB  Added Copied from greg's inventory page.
        private Int16 findReorderQuantity(GridDataItem gridDataItem)
        {
            RadNumericTextBox nTextBox = (RadNumericTextBox)gridDataItem.FindControl("txtSuggReorderLevel");
            //CheckBox checkBox = (CheckBox)gridDataItem.FindControl("chkAddtoCart");

            if (nTextBox != null)
            {
                return Int16.Parse(nTextBox.Value.ToString());
                //Response.Write(gridDataItem.ItemIndex.ToString() + " is: " + nTextBox.Value.ToString() + "<br />");
            }
            else
            {
                return 0;
                //throw new Exception("Cannot find object with ID 'CheckBox1' in item " + gridDataItem.ItemIndex.ToString());
            }
        }


        #endregion
    }

    public class RecordsPerPageEventArgs : EventArgs
    {
        public RecordsPerPageEventArgs(Int32 recordsPerPage)
        {
            RecordsPerPage = recordsPerPage;
        }
        public Int32 RecordsPerPage { get; set; }

    }
}