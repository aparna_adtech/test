﻿using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.WebControls;

namespace BregVision.UserControls
{
    public class VisionRadGrid : RadGrid
    {
        private UIHelper _UIHelper_RadGrid;


        public VisionRadGrid()
        {
            _UIHelper_RadGrid = new UIHelper(this.ViewState, (RadGrid)this, true);
        }

        public DataSet VisionDataSet { get; set; }

        public bool NeedsRebind
        {
            get
            {
                return _UIHelper_RadGrid.NeedsRebind;
            }
            set
            {
                _UIHelper_RadGrid.NeedsRebind = value;
            }
        }


        public string SearchCriteria
        {
            get
            {
                return _UIHelper_RadGrid.SearchCriteria;
            }
            set
            {
                _UIHelper_RadGrid.SearchCriteria = value;
            }
        }

        public string SearchText
        {
            get
            {
                return _UIHelper_RadGrid.SearchText;
            }
            set
            {
                _UIHelper_RadGrid.SearchText = value;
            }
        }





        public void SetGridLabel(string SearchCriteria, bool IsPostBack, Label lblBrowseDispense)
        {
            _UIHelper_RadGrid.SetGridLabel(SearchCriteria, IsPostBack, lblBrowseDispense);
        }

        public void SetGridLabel(string SearchCriteria, bool IsPostBack, Label lblBrowseDispense, bool ForceDefaultText)
        {
            _UIHelper_RadGrid.SetGridLabel(SearchCriteria, IsPostBack, lblBrowseDispense, ForceDefaultText);
        }

        public void SetExpansionOnPageIndexChanged()
        {
            _UIHelper_RadGrid.PageIndexChanged();
        }


        public void SetSearchCriteria(string commandName)
        {
            _UIHelper_RadGrid.SetSearchCriteria(commandName);
        }

        public void ExpandGrid()
        {
            _UIHelper_RadGrid.ExpandGrid();
        }

        public void ExpandGrid(bool ExpandOnly)
        {
            _UIHelper_RadGrid.ExpandGrid(ExpandOnly);
        }

        public void SetRecordCount(DataSet ds, string FieldName)
        {
            _UIHelper_RadGrid.SetRecordCount(ds, FieldName);
        }

        public void CauseRebindOnDelete(GridDataItem Parent)
        {
            _UIHelper_RadGrid.CauseRebindOnDelete(Parent);
        }


    }
}
