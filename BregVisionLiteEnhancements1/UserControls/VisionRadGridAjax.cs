﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;


namespace BregVision.UserControls
{
    public class VisionRadGridAjax : RadGrid
    {
        public enum GridColor
        {
            Red,
            Yellow,
            Blue,
            Green
        }

        private TelerikAjax.UIHelper _UIHelper_RadGrid;


        public VisionRadGridAjax()
        {
            this.PreRender += new EventHandler(VisionRadGridAjax_PreRender);
            this.ItemDataBound += new GridItemEventHandler(VisionRadGridAjax_ItemDataBound);
            _UIHelper_RadGrid = new TelerikAjax.UIHelper(this.ViewState, (RadGrid)this, true);
            base.AutoGenerateColumns = false;
            base.GridLines = GridLines.None;
            base.AllowPaging = true;
            base.AllowSorting = true;
            base.ShowGroupPanel = true;
            base.EnableViewState = true;
            base.AllowMultiRowSelection = true;

            base.EnableEmbeddedSkins = true;
            //base.Skin = "Web20";
            base.Skin = "WebBlue";

            //Pager
            //base.SelectedItemStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#EDCD01");
            base.PagerStyle.Mode = GridPagerMode.NumericPages;
            //base.PagerStyle.PagerTextFormat = "{4} | Displaying page {0} of {1}, items {2} to {3} of {5}";

            base.GroupPanel.Visible = true;
            //base.GroupPanel.Width = Unit.Percentage(100);

            base.ClientSettings.AllowColumnsReorder = true;
            base.ClientSettings.AllowDragToGroup = true;
            base.ClientSettings.ReorderColumnsOnClient = true;
            base.ClientSettings.Selecting.AllowRowSelect = true;

            //Master Table
            FormatTableView(base.MasterTableView);


            foreach (GridTableView childTableView in base.MasterTableView.DetailTables)
            {
                FormatTableView(childTableView);
                foreach (GridTableView grandChildTableView in childTableView.DetailTables)
                {
                    FormatTableView(grandChildTableView);
                }
            }

        }

        void VisionRadGridAjax_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (!AddNonBreakingSpace)
                return;

            if (e.Item is GridDataItem)
            {
                foreach (TableCell c in e.Item.Cells)
                {
                    if (String.IsNullOrEmpty(c.Text.Trim()) && c.Controls.Count == 0)
                    {
                        c.Text = "&nbsp;";
                    }
                    else if (c.Controls.Count > 0)
                    {
                        c.Controls.Add(new System.Web.UI.LiteralControl("&nbsp;"));
                    }
                }
            }
        }

        void VisionRadGridAjax_PreRender(object sender, EventArgs e)
        {
            //Master Table
            FormatTableView(base.MasterTableView);


            foreach (GridTableView childTableView in base.MasterTableView.DetailTables)
            {
                FormatTableView(childTableView);
                foreach (GridTableView grandChildTableView in childTableView.DetailTables)
                {
                    FormatTableView(grandChildTableView);
                }
            }
        }

        public void AddColorStylesToPage()
        {
            System.Web.UI.Page page = this.Page;
            page.Header.StyleSheet.CreateStyleRule(new VisionRadGridAjax.RedStyle(), page, ".RedRow_Vision");
            page.Header.StyleSheet.CreateStyleRule(new VisionRadGridAjax.YellowStyle(), page, ".YellowRow_Vision");
            page.Header.StyleSheet.CreateStyleRule(new VisionRadGridAjax.BlueStyle(), page, ".BlueRow_Vision");
            page.Header.StyleSheet.CreateStyleRule(new VisionRadGridAjax.GreenStyle(), page, ".GreenRow_Vision");
        }


        private static void FormatTableView(GridTableView tableView)
        {
            tableView.AutoGenerateColumns = false;
            tableView.ExpandCollapseColumn.Resizable = false;
            //tableView.ExpandCollapseColumn.HeaderStyle.Width = Unit.Pixel(15);
            //tableView.ExpandCollapseColumn.HeaderStyle.BorderStyle = BorderStyle.Solid;
            //tableView.ExpandCollapseColumn.HeaderStyle.BorderWidth = Unit.Pixel(1);
            //tableView.ExpandCollapseColumn.HeaderStyle.BorderColor = System.Drawing.ColorTranslator.FromHtml("#A2B6CB");

            //tableView.ExpandCollapseColumn.ItemStyle.Width = Unit.Pixel(15); 

            tableView.RowIndicatorColumn.Visible = false;

            //tableView.BorderStyle = BorderStyle.Solid;
            //tableView.BorderWidth = Unit.Pixel(1);
            //tableView.BorderColor = System.Drawing.ColorTranslator.FromHtml("#A2B6CB");

            if (tableView.DetailTables.Count < 1)
            {
                tableView.ExpandCollapseColumn.Visible = false;
                tableView.RowIndicatorColumn.Visible = false;
            }
            //foreach (GridColumn column in tableView.Columns)
            //{
            //    column.HeaderStyle.BorderColor = System.Drawing.ColorTranslator.FromHtml("#A2B6CB");
            //    column.HeaderStyle.BorderStyle = BorderStyle.Solid;
            //    column.HeaderStyle.BorderWidth = Unit.Pixel(1); 
            //}
        }

        public DataSet VisionDataSet { get; set; }

        private string DetailTablePreloadedDataSet_SessionKey
        {
            get { return this.ClientID + "_DetailTablePreloadedDataSet"; }
        }
        public DataSet DetailTablePreloadedDataSet
        {
            get
            {
                return (DataSet)Page.Session[DetailTablePreloadedDataSet_SessionKey];
            }
            set
            {
                Page.Session[DetailTablePreloadedDataSet_SessionKey] = value;
            }
        }

        public void InitializeSearch()
        {
            this.SearchText = "";
            this.SearchCriteria = "Browse";
            this.SearchType = "Browse";
            this.SetSearchCriteria("Browse");
            this.Rebind();
            this.HasInitiallyBound = true;
        }

        /// <summary>
        /// this is for use in the Wizard so we can tell if the grid has been initially bound
        /// </summary>
        public bool HasInitiallyBound
        {
            get
            {
                if (this.ViewState[this.ClientID + "_HasInitiallyBound"] == null || this.ViewState[this.ClientID + "_HasInitiallyBound"].ToString().ToLower() == "false")
                {
                    return false;
                }
                return true;
            }
            set
            {
                this.ViewState[this.ClientID + "_HasInitiallyBound"] = value.ToString();
            }
        }

        public bool NeedsRebind
        {
            get
            {
                return _UIHelper_RadGrid.NeedsRebind;
            }
            set
            {
                _UIHelper_RadGrid.NeedsRebind = value;
            }
        }


        public string SearchCriteria
        {
            get
            {
                if (this.Page.Session[this.ClientID + "_SearchCriteria"] != null)
                {
                    return this.Page.Session[this.ClientID + "_SearchCriteria"].ToString();
                }
                return "";
            }
            set
            {
                this.Page.Session[this.ClientID + "_SearchCriteria"] = value;
            }
        }

        public string SearchText
        {
            get
            {
                if (this.Page.Session[this.ClientID + "_SearchText"] != null)
                {
                    return this.Page.Session[this.ClientID + "_SearchText"].ToString();
                }
                return "";
            }
            set
            {
                this.Page.Session[this.ClientID + "_SearchText"] = value;
            }
        }

        public DateTime SearchDateFrom
        {
            get
            {
                if (this.Page.Session[this.ClientID + "_SearchDateFrom"] != null)
                {
                    return Convert.ToDateTime(this.Page.Session[this.ClientID + "_SearchDateFrom"]);
                }
                return DateTime.MinValue;
            }
            set
            {
                this.Page.Session[this.ClientID + "_SearchDateFrom"] = value;
            }
        }

        public DateTime SearchDateTo
        {
            get
            {
                if (this.Page.Session[this.ClientID + "_SearchDateTo"] != null)
                {
                    return Convert.ToDateTime(this.Page.Session[this.ClientID + "_SearchDateTo"]);
                }
                return DateTime.MinValue;
            }
            set
            {
                this.Page.Session[this.ClientID + "_SearchDateTo"] = value;
            }
        }

        public string SearchType
        {
            get
            {
                return this.Page.Session[this.ClientID + "_SearchType"].ToString();
            }
            set
            {
                this.Page.Session[this.ClientID + "_SearchType"] = value;
            }
        }

        public bool AddNonBreakingSpace
        {
            get
            {
                var o = ViewState[this.ID + "_AddNonBreakingSpace"];
                if (o == null || !(o is bool))
                    return true;
                else
                    return (bool)o;
            }
            set
            {
                ViewState[this.ID + "_AddNonBreakingSpace"] = value;
            }
        }



        public void SetGridLabel(string SearchCriteria, bool IsPostBack, Label lblBrowseDispense)
        {
            _UIHelper_RadGrid.SetGridLabel(SearchCriteria, IsPostBack, lblBrowseDispense);
        }

        public void SetGridLabel(string SearchCriteria, bool IsPostBack, Label lblBrowseDispense, bool ForceDefaultText)
        {
            _UIHelper_RadGrid.SetGridLabel(SearchCriteria, IsPostBack, lblBrowseDispense, ForceDefaultText);
        }

        public void SetExpansionOnPageIndexChanged()
        {
            _UIHelper_RadGrid.PageIndexChanged();
        }

        public void RebindWithExpandState()
        {
            //int index = -1;
            List<int> indexes = new List<int>();
            foreach (Telerik.Web.UI.GridDataItem row in base.MasterTableView.Items)
            {
                if (row.Expanded == true)
                {
                    indexes.Add(row.ItemIndex);
                }
            }
            base.Rebind();

            foreach (int index in indexes)
            {
                base.MasterTableView.Items[index].Expanded = true;
            }
        }


        public void SetSearchCriteria(string commandName)
        {
            _UIHelper_RadGrid.SetSearchCriteria(commandName);
        }

        public void ExpandGrid()
        {
            _UIHelper_RadGrid.ExpandGrid();
        }

        public void ExpandGrid(bool ExpandOnly)
        {
            _UIHelper_RadGrid.ExpandGrid(ExpandOnly);
        }

        public void SetRecordCount(DataSet ds, string FieldName)
        {
            _UIHelper_RadGrid.SetRecordCount(ds, FieldName);
        }

        public void SetRecordCount(int count)
        {
            _UIHelper_RadGrid.SetRecordCount(count);
        }

        public void CauseRebindOnDelete(GridDataItem Parent)
        {
            _UIHelper_RadGrid.CauseRebindOnDelete(Parent);
        }

        public class RedStyle : Style
        {
            protected override void FillStyleAttributes(CssStyleCollection attributes, IUrlResolutionService urlResolver)
            {
                base.FillStyleAttributes(attributes, urlResolver);

                string imagePath = "~/RadControls/Grid/Skins/Office2007/Img/gridRedRow.gif";
                string imageUrl = BLL.Website.GetAbsPath(imagePath);
                attributes[HtmlTextWriterStyle.BackgroundImage] = "url('" + imageUrl + "'); repeat-x !important ;";
            }
        }

        public class YellowStyle : Style
        {
            protected override void FillStyleAttributes(CssStyleCollection attributes, IUrlResolutionService urlResolver)
            {
                base.FillStyleAttributes(attributes, urlResolver);

                string imagePath = "~/RadControls/Grid/Skins/Office2007/Img/gridYellowRow.gif";
                string imageUrl = BLL.Website.GetAbsPath(imagePath);
                attributes[HtmlTextWriterStyle.BackgroundImage] = "url('" + imageUrl + "'); repeat-x !important ;";
            }
        }

        public class BlueStyle : Style
        {
            protected override void FillStyleAttributes(CssStyleCollection attributes, IUrlResolutionService urlResolver)
            {
                base.FillStyleAttributes(attributes, urlResolver);

                string imagePath = "~/RadControls/Grid/Skins/Office2007/Img/gridBlueRow.gif";
                string imageUrl = BLL.Website.GetAbsPath(imagePath);
                attributes[HtmlTextWriterStyle.BackgroundImage] = "url('" + imageUrl + "'); repeat-x !important ;";
            }
        }

        public class GreenStyle : Style
        {
            protected override void FillStyleAttributes(CssStyleCollection attributes, IUrlResolutionService urlResolver)
            {
                base.FillStyleAttributes(attributes, urlResolver);

                string imagePath = "~/RadControls/Grid/Skins/Office2007/Img/gridGreenRow.gif";
                string imageUrl = BLL.Website.GetAbsPath(imagePath);
                attributes[HtmlTextWriterStyle.BackgroundImage] = "url('" + imageUrl + "'); repeat-x !important ;";
            }
        }

    }

    public static class ColorExtension
    {
        public static void SetColor(this GridDataItem gridDataItem, VisionRadGridAjax.GridColor color)
        {
            try
            {
                if (color == VisionRadGridAjax.GridColor.Red)
                {
                    gridDataItem.ForeColor = System.Drawing.Color.White;
                    gridDataItem.CssClass = "RedRow_Vision"; // "RedRow_Office2007";
                    gridDataItem.BackColor = System.Drawing.ColorTranslator.FromHtml("#D40200");
                }
                else if (color == VisionRadGridAjax.GridColor.Yellow)
                {
                    gridDataItem.ForeColor = System.Drawing.Color.Black;
                    gridDataItem.CssClass = "YellowRow_Vision"; // "RedRow_Office2007";
                    gridDataItem.BackColor = System.Drawing.ColorTranslator.FromHtml("#EDCD01");
                }
                else if (color == VisionRadGridAjax.GridColor.Blue)
                {
                    gridDataItem.ForeColor = System.Drawing.Color.White;
                    gridDataItem.CssClass = "BlueRow_Vision"; // "RedRow_Office2007";
                    gridDataItem.BackColor = System.Drawing.ColorTranslator.FromHtml("#0023BA");
                }
                else if (color == VisionRadGridAjax.GridColor.Green) 
                {
                    gridDataItem.ForeColor = System.Drawing.Color.White;
                    gridDataItem.CssClass = "GreenRow_Vision"; // "RedRow_Office2007";
                    gridDataItem.BackColor = System.Drawing.ColorTranslator.FromHtml("#1C8F00");
                }
                gridDataItem.Cells[1].BackColor = System.Drawing.Color.White;

            }
            catch
            {
            }
        }
    }
}
