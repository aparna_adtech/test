﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/SiteMaster.Master" AutoEventWireup="true" 
    CodeBehind="InvoiceManagerAngular.aspx.cs" Inherits="BregVision.InvoiceManager" %>

<%@ MasterType VirtualPath="~/MasterPages/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<%--    <script src="http://code.angularjs.org/1.3.11/angular.js"></script>--%>
<style type="text/css">
    .invDiv {
        border-bottom-color: black;
        border-style: solid;
        border-width: 2px;
        width: 80%;
        display: flex;
        justify-content: center;

    }

    .over { opacity: 0.5 }

    .InvoiceColumn {
        width: 50%;
        border-bottom-color: black;
        border-style: solid;
        border-width: 2px
    }

    .InvoiceSection {
        margin: 20px;
        border-bottom-color: black;
        border-style: solid;
        border-width: 2px
    }

    .poTable {
    border-collapse:collapse;
}
.poTable  tr td  {
    border: 1px solid #ccc;
    padding:8px;
}


</style>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>

    <div class="invDiv" ng-app="invoiceApp">
        <div class="invDiv" ng-controller="InvoiceCtrl as invCtrl">
            <div class="InvoiceColumn">
                <div class="InvoiceSection">
                    <ng-form name="invoiceForm" id="invoiceForm" >
                    Invoice #: <input type="text" name="invoiceNumber" ng-model="invCtrl.NewInvoice.Number" required="" />
                    {{invoiceForm.invoiceNumber.$error.required}}
                    <div role="alert">
                         <span class="error" ng-show="invoiceForm.invoiceNumber.$error.required">
                             Required!</span>
                    </div>
                    Supplier:<select name="newInvoiceSupplierSelect" id="newInvoiceSupplierSelect" 
                            ng-options="supplier.Name for supplier in invCtrl.Suppliers track by supplier.Id"
                            ng-model="invCtrl.NewInvoice.Supplier">
                    </select>
                    Sales Tax:<input type="number" step="any"  ng-model="invCtrl.NewInvoice.Tax"/>
                    Shipping Charges:<input type="number" step="any" ng-model="invCtrl.NewInvoice.ShippingCharges"/>
                    Total:<input type="number" step="any" ng-model="invCtrl.NewInvoice.Total"/>
                    <input type="button" ng-click="invCtrl.createNewInvoiceClick()" value = "Create new Invoice" >
                    Range start:<input type="date" name="rangeStart" ng-model="invCtrl.dateFrom" ng-change="ctrl.startTimeChange()"
                        placeholder="yyyy-MM-dd" required="">
                    </ng-form>
                    Range end:<input type="date" ng-model="invCtrl.dateTo">

                    <div>
                        Invoices:

                        <table>
                            <tr><th>Number</th><th>Supplier</th><th>Total</th><th>Date</th></tr>
                            <tr draggable="true" ng-click="invCtrl.SelectInvoice(inv)"
                                ng-repeat="inv in invCtrl.Invoices|filter: {Number: invCtrl.NewInvoice.Number}
                                    | supplier: invCtrl.NewInvoice.Supplier
                                    | dateRange:invCtrl.dateFrom.getTime():invCtrl.dateTo.getTime()" >
                                <td>{{inv.Number}}</td><td>{{inv.Supplier.Name}}</td><td>{{inv.Total | currency}}</td><td>{{inv.Date | date: 'MM/dd/yyyy'}}</td>
                            </tr>
                        </table> 
                    </div>            

                </div>
                <div class="InvoiceSection" ondragover="return false" ondragend="alert('cool');">
                    Current Invoice:<br/>
                    Invoice #: <input type="text" ng-model="invCtrl.CurrentInvoice.Number" />
                    Supplier:<select name="currentInvoiceSupplierSelect" id="currentInvoiceSupplierSelect"  
                            ng-options="supplier.Name for supplier in invCtrl.Suppliers track by supplier.Id"
                            ng-model="invCtrl.CurrentInvoice.Supplier">
                    </select>
                    Sales Tax:<input type="number" step="any"  ng-model="invCtrl.CurrentInvoice.Tax"/>
                    Shipping Charges:<input type="number" step="any" ng-model="invCtrl.CurrentInvoice.ShippingCharges"/>
                    Total:<input type="number" step="any" ng-model="invCtrl.CurrentInvoice.Total"/>
                    <label for="isPaid" style="white-space: nowrap">Is Paid:</label> <input id="isPaid" type="checkbox" ng-model="invCtrl.CurrentInvoice.IsPaid"/>
                    <div>
                        <div droppableinvoice drop="handleDrop">
                            <table >
                                <tr><td>Product</td><td>Code</td><td>Purchase Order</td><td style="text-align: center">Quantity</td><td>Wholesale Cost</td><td>Total</td><td></td></tr>
                                <tr draggable="true" ng-repeat="invoiceLineItem in invCtrl.CurrentInvoice.InvoiceLineItems">
                                    <td>{{invoiceLineItem.ProductName}}</td>
                                    <td>{{invoiceLineItem.ProductCode}}</td>
                                    <td>{{invoiceLineItem.PurchaseOrderNumber}}</td>
                                    <td><input type="number" ng-model="invoiceLineItem.QuantityInvoiced" ng-change="invCtrl.updateLineItemTotal(invoiceLineItem)"/>
                                    <td><input type="number" step="0.01" ng-model="invoiceLineItem.WholesaleCost"/></td>
                                    <td>{{invoiceLineItem.QuantityInvoiced * invoiceLineItem.WholesaleCost}}</td>
                                    <td><input type="button" ng-click="invCtrl.removeItemFromCurrentInvoice(invoiceLineItem)"/>Delete</td>
                                    <td><input type="button" ng-click="invCtrl.updateWholesaleCost(invoiceLineItem)"/>Update Catalog</td>
                                </tr>
                            </table>
                        </div> 
                    </div>
                    <input type="button" ng-click="invCtrl.updateInvoice()" value = "Save Invoice" >
                    <input type="button" ng-click="invCtrl.deleteInvoice()" value = "Delete Invoice" >
                </div>
            </div>
            <div class="InvoiceColumn">
                <div class="InvoiceSection">
                    Purchase Orders:
                    <label>Search: <input ng-model="searchText"></label>

                    <table>
                        <tr><th>Number</th><th>Supplier</th><th>Order Date</th></tr>
                        <tr draggable="true" ng-repeat="po in invCtrl.PurchaseOrders 
                            | filter:searchText
                            | purchaseOrderSupplierFilter: invCtrl.CurrentInvoice.Supplier" 
                            ng-click="invCtrl.SelectPurchaseOrder(po)">
                            <td>{{po.PONumber}}</td><td>{{po.Supplier.Name}}</td><td>{{po.OrderDate  | date: 'MM/dd/yyyy'}}</td>
                        </tr>
                    </table> 
                </div>
                <div class="InvoiceSection">
                    <div>PO#:{{invCtrl.CurrentPurchaseOrder.PONumber}} Supplier:{{invCtrl.CurrentPurchaseOrder.Supplier.Name}} Order Date:{{invCtrl.CurrentPurchaseOrder.OrderDate | date: 'MM/dd/yyyy'}} </div>
<%--                    <div>--%>
<%--                        <div id="{{lineItem.Id}}" draggablepoitem ng-repeat="lineItem in invCtrl.CurrentPurchaseOrder.LineItems" item="{{lineItem}}">--%>
<%--                            {{lineItem.Id}}-{{lineItem.Name}}--%>
<%--                        </div>--%>
<%--                    </div>--%>
                    <table class="poTable">
                        <tr><td colspan="3">Product</td><td colspan="2" style="text-align: center ">Quantity</td><td>Cost</td><td>Activity</td></tr>
                        <tr><td><input type="checkbox" ng-model ="invCtrl.SelectAll" ng-click="invCtrl.ToggleSelectAll()"/></td><td>Name</td><td>Code</td><td>Checked in</td><td>Ordered</td><td></td><td></td></tr>
                        <tr class="poTable" id='{{lineItem.PONumber}}' draggablepoitem ng-repeat="lineItem in invCtrl.CurrentPurchaseOrder.LineItems" item="{{lineItem}}">
                            <td ><input type="checkbox" ng-model="lineItem.Selected" /></td>
                            <td >{{lineItem.Name}}</td>
                            <td >{{lineItem.ProductCode}}</td>
                            <td style="text-align: center ">{{lineItem.QuantityCheckedIn}}</td>
                            <td style="text-align: center ">{{lineItem.QuantityOrdered}}</td>
                            <td>{{lineItem.Cost}}</td>
                            <td>
                                <div ng-repeat="invoice in lineItem.Invoices">Number:{{invoice.Number}} Date:{{invoice.Date | date: 'MM/dd/yyyy'}} Qty:{{invoice.Quantity}} </div>
                            </td>
                        </tr>
                    </table>
                    <input type="button" ng-click="invCtrl.addSelectedPoItemsToCurrentInvoice()" value = "Add to Invoice" >
 
                </div>            
            </div>            
<%--        <div class="bin" droppable drop="handleDrop" ng-repeat="bin in [1,2,3]" bin="bin" id="bin{{bin}}" >Drop here{{bin}}</div>--%>
<%--        <div class="item" id="item{{item}}" ng-repeat="item in [1,2,3]" draggable item="item">drag me{{item}}</div>--%>
   
        </div>
         
 
    

    <script type="text/javascript">
        var app = angular.module("invoiceApp", []);

        app.controller("InvoiceCtrl",
            function($scope, $http) {
                var self = this;
                self.dateFrom = new Date();
                self.dateFrom.setMonth(self.dateFrom.getMonth() - 1);
                self.dateTo = new Date();
                self.dateTo.setMonth(self.dateTo.getMonth() + 1);
                self.NewInvoice = {
                    Number: '',
                    Supplier: null,
                    Tax: null,
                    ShippingCharges: null,
                    Total: null
                };
                self.CurrentInvoice = null;
//                {
//                    Number: '',
//                    Supplier: null,
//                    Tax: null,
//                    ShippingCharges: null,
//                    Total: null
//                };

                this.CurrentPurchaseOrder = {
                    PONumber: '',
                    Supplier: null,
                    OrderDate:null,
                    LineItems: null
                };

                self.Invoices = [
                    {
                        Id: 1, Date: '08/10/2015', Number: 'ABC120', Supplier: { Id: 1, Name: 'Breg' }, Tax: 7.75, ShippingCharges: 10.22, Total: 100.00, InvoiceLineItems: [
                        {
                            IPIId: 1,
                            POId: 104,
                            PONumber: 'PO1',
                            Name: 'Ankle',
                            QuantityInvoiced: 2,
                            WholesaleCost: 11.00
                        }] },
                    { Id: 2,Date:'08/12/2015', Number: 'ABC121', Tax: 7.75, ShippingCharges: 10.22, Total: 101.00, InvoiceLineItems: [], Supplier: { Id: 1, Name: 'Breg' }, },
                    { Id: 3,Date:'08/12/2015', Number: 'ABC132', Tax: 7.75, ShippingCharges: 10.22, Total: 102.00, InvoiceLineItems: [], Supplier: { Id: 2, Name: 'CostCo' }, },
                    { Id: 4,Date:'08/13/2015', Number: 'ABD123', Tax: 7.75, ShippingCharges: 10.22, Total: 103.00, InvoiceLineItems: [], Supplier: { Id: 3, Name: 'Acme Brace' }, },
                    { Id: 4,Date:'08/14/2015', Number: 'ABD124', Tax: 7.75, ShippingCharges: 10.22, Total: 104.00, InvoiceLineItems: [], Supplier: { Id: 4, Name: 'Staples' }, },
                    { Id: 5,Date:'08/15/2015', Number: 'BBD125', Tax: 7.75, ShippingCharges: 10.22, Total: 105.00, InvoiceLineItems: [], Supplier: { Id: 5, Name: 'Toolbox' }, }
                ];

                self.Suppliers = [
                    { Id: -1, Name: 'Select Supplier' }
//                    ,
//                    { Id: 1, Name: 'Breg' },
//                    { Id: 2, Name: 'CostCo' },
//                    { Id: 3, Name: 'Acme Brace' },
//                    { Id: 4, Name: 'Staples' },
//                    { Id: 5, Name: 'Toolbox' },
                ];

                self.NewInvoice.Supplier = self.Suppliers[0];

//                this.PurchaseOrders = null;
//                this.PurchaseOrders = [
//                    {
//                        Id: 1, PONumber: 'PO1', Supplier: 'Breg', OrderDate: '06-12-2015',
//                        LineItems: [
//                            {Id:101,ProductCode:'B123', Name: 'Brace', QuantityOrdered: 3, QuantityCheckedIn: 2, Cost: 10.00 ,
//                            CheckIns: [
//                                { Date: '06-15-2015', Quantity: 1 },
//                                { Date: '06-25-2015', Quantity: 1 }
//                            ],
//                            Invoices: [
//                                { Date: '06-15-2015', Quantity: 1 , Number:'ABC123'}
//                            ]}, 
//                        {Id:102,ProductCode:'S847', Name: 'Sleeve', QuantityOrdered: 1, QuantityCheckedIn: 0, Cost: 20.50 }, 
//                        { Id: 103, ProductCode: 'C848', Name: 'Crutch', QuantityOrdered: 1, QuantityCheckedIn: 1, Cost: 100.22 }]
//                    },
//                    { Id: 2, PONumber: 'PO2', Supplier: 'Breg', OrderDate: '07-12-2015' },
//                    { Id: 3, PONumber: 'PO3', Supplier: 'Breg', OrderDate: '08-12-2015' },
//                    { Id: 4, PONumber: 'PO4', Supplier: 'ACME', OrderDate: '08-13-2015' },
//                    { Id: 5, PONumber: 'PO5', Supplier: 'ACME', OrderDate: '08-14-2015' },
//                    { Id: 6, PONumber: 'PO6', Supplier: 'Breg', OrderDate: '08-15-2015' },
//                ];
//

                self.createNewInvoiceClick = function() {
                    var newInvoice = {
                        Number: self.NewInvoice.Number,
                        Supplier: self.NewInvoice.Supplier,
                        Tax: self.NewInvoice.Tax,
                        Total: self.NewInvoice.Total,
                        ShippingCharges: self.NewInvoice.ShippingCharges,
                        Date: self.dateFrom,
                        InvoiceLineItems : []
                    };
                    self.createInvoice(newInvoice);
                    self.Invoices.push(newInvoice);
                    self.CurrentInvoice = newInvoice;
                };


                self.createInvoice = function (newInvoice) {
                    $scope.working = true;
                    $scope.answered = true;

                    $http.post('http://localhost/BregRestfulService/api/invoice/', newInvoice).success(function (data, status, headers, config) {
                        newInvoice.Id = data;
                        $scope.working = false;
                    }).error(function (data, status, headers, config) {
                        $scope.title = "Oops... something went wrong";
                        $scope.working = false;
                    });
                };

                self.updateInvoice = function () {
                    $scope.working = true;
                    $scope.answered = true;

                    $http.put('http://localhost/BregRestfulService/api/invoice/', self.CurrentInvoice).success(function (data, status, headers, config) {
                        $scope.correctAnswer = (data === true);
                        $scope.working = false;
                    }).error(function (data, status, headers, config) {
                        $scope.title = "Oops... something went wrong";
                        $scope.working = false;
                    });
                };

                self.updateWholesaleCost = function (invoiceLineItem) {
                    $scope.working = true;
                    $scope.answered = true;

                    $http.put('http://localhost/BregRestfulService/api/invoice/product/', invoiceLineItem).success(function (data, status, headers, config) {
                        $scope.correctAnswer = (data === true);
                        $scope.working = false;
                    }).error(function (data, status, headers, config) {
                        $scope.title = "Oops... something went wrong";
                        $scope.working = false;
                    });
                };

                self.ToggleSelectAll = function() {
                    angular.forEach(self.CurrentPurchaseOrder.LineItems, function (lineItem, key) {
                        lineItem.Selected = self.SelectAll;
                    });

                }

                self.addSelectedPoItemsToCurrentInvoice = function () {
                    if (!self.CurrentInvoice || self.CurrentInvoice == null) {
                        return;
                    }
                    angular.forEach(self.CurrentPurchaseOrder.LineItems, function(lineItem, key) {
                        if (lineItem.Selected && lineItem.Selected === true) {
                            self.addPoItemToCurrentInvoice(lineItem);
                        }
                    });
                };

                self.removeItemFromCurrentInvoice = function (lineItem) {
                    $scope.working = true;
                    $scope.answered = true;
                    if (lineItem.Id !== null) {
                        $http.delete('http://localhost/BregRestfulService/api/Invoice/LineItem/' + lineItem.InvoiceLineItemId).success(function(data, status, headers, config) {
                            $scope.correctAnswer = (data === true);
                            $scope.working = false;
                        }).error(function(data, status, headers, config) {
                            $scope.title = "Oops... something went wrong";
                            $scope.working = false;
                        });
                    }

                    var index = self.CurrentInvoice.InvoiceLineItems.indexOf(lineItem);
                    self.CurrentInvoice.InvoiceLineItems.splice(index, 1);
                };

                self.deleteInvoice = function () {
                    $scope.working = true;
                    $scope.answered = true;
                    if (self.CurrentInvoice == null || self.CurrentInvoice.Id == null) {
                        return;
                    }

                    $http.delete('http://localhost/BregRestfulService/api/Invoice/' + self.CurrentInvoice.Id).success(function (data, status, headers, config) {
                        $scope.correctAnswer = (data === true);
                        $scope.working = false;
                    }).error(function (data, status, headers, config) {
                        $scope.title = "Oops... something went wrong";
                        $scope.working = false;
                    });

                    var index = self.Invoices.indexOf(self.CurrentInvoice);
                    self.Invoices.splice(index, 1);
                    self.CurrentInvoice = null;
                };

                self.addPoItemToCurrentInvoice = function (lineItem) {
                    self.CurrentInvoice.InvoiceLineItems.push({
                        InvoiceLineItemId: null,
                        PurchaseOrderNumber: self.CurrentPurchaseOrder.PONumber,
                        SupplierOrderLineItemId: lineItem.Id,
                        ProductName: lineItem.Name,
                        ProductCode: lineItem.ProductCode,
                        QuantityCheckedIn: lineItem.QuantityCheckedIn,
                        QuantityOrdered: lineItem.QuantityOrdered,
                        QuantityInvoiced: null,
                        WholesaleCost: lineItem.Cost

                    });
                };

                self.SelectInvoice = function(selectedInvoice) {
                    self.CurrentInvoice = selectedInvoice;
                };

                self.SelectPurchaseOrder = function(selectedPurchaseOrder) {
                    self.CurrentPurchaseOrder = selectedPurchaseOrder;
                };

                $scope.handleDrop = function (data) {
                    self.addPoItemToCurrentInvoice(data);
                };

                self.getPurchaseOrders = function () {
                    $scope.working = true;
                    $scope.answered = false;
                    $scope.title = "loading purchase orders...";
                    $scope.options = [];

                    $http.get("http://localhost/BregRestfulService/api/supplierorder/302").success(function (data, status, headers, config) {
                        self.PurchaseOrders = data;
                        self.PopulatePurchaseOrderLineItems();
                        $scope.title = data.title;
                        $scope.answered = false;
                        $scope.working = false;
                    }).error(function (data, status, headers, config) {
                        $scope.title = "Oops... something went wrong";
                        $scope.working = false;
                    });

                };

                self.PopulatePurchaseOrderLineItems = function() {
                    angular.forEach(self.PurchaseOrders, function (purchaseOrder, key) {
                        self.getPurchaseOrdersLineItems(purchaseOrder);
                    });
                }

                self.getPurchaseOrdersLineItems = function (selectedPurchaseOrder) {
                    $scope.working = true;
                    $scope.answered = false;
                    $scope.title = "loading purchase order line items...";
                    $scope.options = [];

                    $http.get("http://localhost/BregRestfulService/api/supplierorder/lineitems/" + selectedPurchaseOrder.SupplierOrderId).success(function (data, status, headers, config) {
                        selectedPurchaseOrder.LineItems = data;
                        $scope.title = data.title;
                        $scope.answered = false;
                        $scope.working = false;
                    }).error(function (data, status, headers, config) {
                        $scope.title = "Oops... something went wrong";
                        $scope.working = false;
                    });
                };

                self.getPurchaseOrders();

                self.getInvoices = function () {
                    $scope.working = true;
                    $scope.answered = false;
                    $scope.title = "loading invoices...";
                    $scope.options = [];

                    $http.get("http://localhost/BregRestfulService/api/invoice/274").success(function (data, status, headers, config) {
                        self.Invoices = data;
                        self.populateInvoiceItems();
                        $scope.title = data.title;
                        $scope.answered = false;
                        $scope.working = false;
                    }).error(function (data, status, headers, config) {
                        $scope.title = "Oops... something went wrong";
                        $scope.working = false;
                    });
                };

                self.populateInvoiceItems = function() {
                    angular.forEach(self.Invoices, function(invoice, key) {
                        $http.get("http://localhost/BregRestfulService/api/invoice/lineitems/" + invoice.Id).success(function (data, status, headers, config) {
                            invoice.InvoiceLineItems = data;
                            $scope.title = data.title;
                            $scope.answered = false;
                            $scope.working = false;
                        }).error(function (data, status, headers, config) {
                            $scope.title = "Oops... something went wrong";
                            $scope.working = false;
                        });

                    })
                }

                self.getInvoices();

                self.getSuppliers = function () {
                    $scope.working = true;
                    $scope.answered = false;
                    $scope.title = "loading suppliers...";
                    $scope.options = [];

                    $http.get("http://localhost/BregRestfulService/api/supplier/274").success(function (data, status, headers, config) {
                        Array.prototype.push.apply(self.Suppliers,data);
                        $scope.title = data.title;
                        $scope.answered = false;
                        $scope.working = false;
                    }).error(function (data, status, headers, config) {
                        $scope.title = "Oops... something went wrong";
                        $scope.working = false;
                    });
                };

                self.getSuppliers();

            }
        );
        app.directive('draggablepoitem', function() {
            return function(scope, element) {
                var el = element[0];

                el.draggable = true;

                el.addEventListener(
                    'dragstart',
                    function(e) {
                        e.dataTransfer.effectAllowed = 'move';
                        var item = this.getAttribute("item");
                        e.dataTransfer.setData('text', item);
                        this.classList.add('drag');
                        return false;
                    },
                    false
                );

                el.addEventListener();
            }
        });
        app.directive('droppableinvoice', function() {
            return {
                scope: {
                    drop: "&", //parent
                    bin: '='
                },
                link: function(scope, element) {
                    var el = element[0];

                    el.addEventListener(
                        'dragover',
                        function(e) {
                            e.dataTransfer.dropEffect = 'move';
                            //allows us to drop
                            if (e.preventDefault) e.preventDefault();
                            this.classList.add('over');
                            return false;
                        },
                        false
                    );

                    el.addEventListener(
                        'dragenter',
                        function(e) {
                            this.classList.add('over');
                            return false;
                        },
                        false
                    );

                    el.addEventListener(
                        'dragleave',
                        function(e) {
                            this.classList.remove('over');
                            return false;
                        },
                        false
                    );

                    el.addEventListener(
                        'drop',
                        function(e) {
                            //Stops some browsers from redirecting
                            if (e.stopPropagation) e.stopPropagation();

                            this.classList.remove('over');

                            var binId = this.id;
                            var data = JSON.parse(e.dataTransfer.getData('text'));
                            var item = document.getElementById(data.Id);
                           // this.appendChild(item);

                            // call the drop passed drop function
                            scope.$apply(function(scope) {
                                var fn = scope.drop();
                                if ('undefined' !== typeof fn) {
                                    fn(data);
            }
                            });

                            return false;
                        },
                        false
        );
                }
            }
        });
        app.directive('draggable', function() {
            return function(scope, element) {
                var el = element[0];

                el.draggable = true;

                el.addEventListener(
                    'dragstart',
                    function(e) {
                        e.dataTransfer.effectAllowed = 'move';
                        e.dataTransfer.setData('Text', this.id);
                        this.classList.add('drag');
                        return false;
                    },
                    false
                );
            }
        });
        app.directive('droppable', function() {
                return {
                    scope: {
                        drop: "&", //parent
                        bin: '='
                    },
                    link: function(scope, element) {
                        var el = element[0];

                        el.addEventListener(
                            'dragover',
                            function(e) {
                                e.dataTransfer.dropEffect = 'move';
                                //allows us to drop
                                if (e.preventDefault) e.preventDefault();
                                this.classList.add('over');
                                return false;
                            },
                            false
                        );

                        el.addEventListener(
                            'dragenter',
                            function(e) {
                                this.classList.add('over');
                                return false;
                            },
                            false
                        );

                        el.addEventListener(
                            'dragleave',
                            function(e) {
                                this.classList.remove('over');
                                return false;
                            },
                            false
                        );

                        el.addEventListener(
                            'drop',
                            function(e) {
                                //Stops some browsers from redirecting
                                if (e.stopPropagation) e.stopPropagation();

                                this.classList.remove('over');

                                var binId = this.id;
                                var data = e.dataTransfer.getData('Text');
                                var item = document.getElementById(data);
                                this.appendChild(item);

                                // call the drop passed drop function
                                scope.$apply(function(scope) {
                                    var fn = scope.drop(data);
                                    if ('undefined' !== typeof fn) {
                                        fn(item.id, binId);
                                    }
                                });

                                return false;
                            },
                            false
                        );
                    }
                }
            })
            .filter("dateRange", [
                function() {
                    return function(invoices, dateFrom, dateTo) {
                        var filtered = [];
                        angular.forEach(invoices, function (invoice) {
                            var invDate = new Date(invoice.Date).setHours(0, 0, 0, 0);
                            var dateFromFloor = new Date(dateFrom).setHours(0, 0, 0, 0);
                            var dateToFloor = new Date(dateTo).setHours(0, 0, 0, 0);
                            if (invDate >= dateFromFloor && invDate <= dateToFloor) {
                                filtered.push(invoice);
                            }
                        });
                        return filtered;
                    };
                }
            ])
            .filter("supplier", [
                function() {
                    return function(invoices, supplier) {
                        var filtered = [];
                        angular.forEach(invoices, function (invoice) {
                            if (supplier === null || supplier.Id === -1 || supplier.Id === invoice.Supplier.Id ) {
                                filtered.push(invoice);
                            }
                        });
                        return filtered;
                    };
                }
            ]).filter("purchaseOrderSupplierFilter", [
                function() {
                    return function(purchaseOrders, supplier) {
                        var filtered = [];
                        angular.forEach(purchaseOrders, function (purchaseOrder) {
                            if (supplier === undefined || supplier.Id === -1 || supplier.Id === purchaseOrder.Supplier.Id) {
                                filtered.push(purchaseOrder);
                            }
                        });
                        return filtered;
                    };
                }
            ]);


    </script>


    </div>



</asp:Content>
