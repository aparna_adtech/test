﻿using BregVision.Domain.PracticeReportScheduling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BregVision.Domain.PracticeReportScheduling.Utility
{
    public static class ReportsListItemGenerator
    {
        public static ReportsListItem[] Generate(string reportServerReportPath)
        {
            return new[]
            {
                new ReportsListItem { ID = "0", Path = null },
                new ReportsListItem { ID = "1", Path = reportServerReportPath + @"/Products Dispensed Report" },
                new ReportsListItem { ID = "2", Path = reportServerReportPath + @"/Products Purchased Report" },
                new ReportsListItem { ID = "3", Path = reportServerReportPath + @"/Products On Hand Report" },
                new ReportsListItem { ID = "4", Path = reportServerReportPath + @"/PO Reconciliation Report" },
                new ReportsListItem { ID = "5", Path = reportServerReportPath + @"/Products Dispensed By Physician Report" },
                new ReportsListItem { ID = "6", Path = reportServerReportPath + @"/Dead Stock Report" },
                new ReportsListItem { ID = "7", Path = reportServerReportPath + @"/Products Dispensed By Practice Report" },
                new ReportsListItem { ID = "8", Path = reportServerReportPath + @"/Products Purchased By Practice Report" },
                new ReportsListItem { ID = "9", Path = reportServerReportPath + @"/Transfer" },
                new ReportsListItem { ID = "10", Path = reportServerReportPath + @"/Products Dispensed By HCPCS Report" },
                new ReportsListItem { ID = "11", Path = reportServerReportPath + @"/Products On Hand By Practice Report" },
                new ReportsListItem { ID = "12", Path = reportServerReportPath + @"/Manual CheckIn With Comments Report" },
                new ReportsListItem { ID = "13", Path = reportServerReportPath + @"/Products Dispensed By Physician HCPCs Location Report" },
                new ReportsListItem { ID = "14", Path = reportServerReportPath + @"/Product Shrinkage Report" },
                new ReportsListItem { ID = "15", Path = reportServerReportPath + @"/Products Dispensed by Payer" },
                new ReportsListItem { ID = "16", Path = reportServerReportPath + @"/Products On Hand With Supplier Wide" },
                new ReportsListItem { ID = "17", Path = reportServerReportPath + @"/Product Catalog By Practice Report" },
                new ReportsListItem { ID = "18", Path = reportServerReportPath + @"/Products Dispensed Custom Fit" },
                new ReportsListItem { ID = "19", Path = reportServerReportPath + @"/Products Dispensed with Payer Allowable" },
                new ReportsListItem { ID = "20", Path = reportServerReportPath + @"/Products Dispensed Report (Export)" },
                new ReportsListItem { ID = "21", Path = reportServerReportPath + @"/Products Purchased Report (Export)" },
                new ReportsListItem { ID = "22", Path = reportServerReportPath + @"/Products On Hand Report (Export)" },
                new ReportsListItem { ID = "23", Path = reportServerReportPath + @"/PO Reconciliation Report (Export)" },
                new ReportsListItem { ID = "24", Path = reportServerReportPath + @"/Products Dispensed By Physician Report (Export)" },
                new ReportsListItem { ID = "25", Path = reportServerReportPath + @"/Dead Stock Report (Export)" },
                new ReportsListItem { ID = "26", Path = reportServerReportPath + @"/Products Dispensed By Practice Report (Export)" },
                new ReportsListItem { ID = "27", Path = reportServerReportPath + @"/Products Purchased By Practice Report (Export)" },
                new ReportsListItem { ID = "28", Path = reportServerReportPath + @"/Transfer (Export)" },
                new ReportsListItem { ID = "29", Path = reportServerReportPath + @"/Products Dispensed By HCPCS Report (Export)" },
                new ReportsListItem { ID = "30", Path = reportServerReportPath + @"/Products On Hand By Practice Report (Export)" },
                new ReportsListItem { ID = "31", Path = reportServerReportPath + @"/Manual CheckIn With Comments Report (Export)" },
                new ReportsListItem { ID = "32", Path = reportServerReportPath + @"/Products Dispensed By Physician HCPCs Location Report (Export)" },
                new ReportsListItem { ID = "33", Path = reportServerReportPath + @"/Product Shrinkage Report (Export)" },
                new ReportsListItem { ID = "34", Path = reportServerReportPath + @"/Products Dispensed by Payer (Export)" },
                new ReportsListItem { ID = "35", Path = reportServerReportPath + @"/Products On Hand With Supplier Wide (Export)" },
                new ReportsListItem { ID = "36", Path = reportServerReportPath + @"/Product Catalog By Practice Report (Export)" },
                new ReportsListItem { ID = "37", Path = reportServerReportPath + @"/Products Dispensed Custom Fit (Export)" },
                new ReportsListItem { ID = "38", Path = reportServerReportPath + @"/Products Dispensed with Payer Allowable (Export)" }
            };
        }
    }
}