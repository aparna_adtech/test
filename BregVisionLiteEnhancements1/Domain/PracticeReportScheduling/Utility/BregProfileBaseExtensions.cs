﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BregVision.Domain.PracticeReportScheduling.Utility
{

    /// <summary>
    /// Extensions to ProfileBase to provide an easy way to access property values.
    /// This is chosen in stead of using the built-in ProfileCommon because Web Applications
    /// don't auto-generate the stub class in the project on-the-fly.
    /// 
    /// NOTE: this should probably be moved into a file by itself; but, this is the only
    /// place where we use this so, it's here for now...
    /// 
    /// UPDATE (01/15/16): Moved this class out of PracticeReports code-behind (MB)
    /// </summary>

    public static class BregProfileBaseExtensions
    {
        public static string GetSavedReportParameters(this System.Web.Profile.ProfileBase profile)
        {
            try
            {
                return profile.GetPropertyValue("SavedReportParameters").ToString();
            }
            catch
            {
#if (DEBUG)
                if (System.Diagnostics.Debugger.IsAttached)
                    System.Diagnostics.Debugger.Break();
#endif

                return string.Empty;
            }
        }

        public static void SetSavedReportParameters(this System.Web.Profile.ProfileBase profile, string value)
        {
            profile.SetPropertyValue("SavedReportParameters", value);
        }
    }

}