﻿using BregVision.Domain.PracticeReportScheduling.Models;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BregVision.Domain.PracticeReportScheduling.Utility
{
    public static class UserProfileUtility
    {
        #region Private
        private static System.Web.Profile.ProfileBase GetCurrentUserProfile()
        {
            return HttpContext.Current.Profile;
        }

        private static SavedReportParametersCollectionDictionary GetProfileReportParametersCollectionDictionary(System.Web.Profile.ProfileBase profile)
        {
            var settings_xml = profile.GetSavedReportParameters();
            var settings = (SavedReportParametersCollectionDictionary)null;
            if (string.IsNullOrEmpty(settings_xml))
            {
                // empty settings returns a new dictionary
                settings = new SavedReportParametersCollectionDictionary();
            }
            else
            {
                settings = SavedReportParametersCollectionDictionary.DeserializeFromXML(settings_xml);
            }
            return settings;
        }
        #endregion Private

        #region Public
        public static SavedReportParametersCollection GetProfileReportParametersCollection(ServerReport sr)
        {
            var profile = GetCurrentUserProfile();
            var dictionary = GetProfileReportParametersCollectionDictionary(profile);
            if (dictionary.ContainsKey(sr.ReportPath))
                return dictionary[sr.ReportPath];
            else
                return new SavedReportParametersCollection();
        }

        public static bool UpdateProfileStoredReportParameters(ServerReport sr, SavedReportParametersCollection c)
        {
            bool successflag;
            string settings_xml;

            // grab and update dictionary
            var profile = GetCurrentUserProfile();
            var dictionary = GetProfileReportParametersCollectionDictionary(profile);
            dictionary[sr.ReportPath] = c;

            // serialize dictionary
            settings_xml = SavedReportParametersCollectionDictionary.SerializeToXML(dictionary);
            successflag = string.IsNullOrEmpty(settings_xml) ? false : true;

            // assign and save updated profile to database
            profile.SetSavedReportParameters(settings_xml);
            profile.Save();

            return successflag;
        }

        public static void ClearProfileStoredReportParameters(ServerReport sr)
        {
            // grab dictionary and remove entry for report
            var profile = GetCurrentUserProfile();
            var dictionary = GetProfileReportParametersCollectionDictionary(profile);
            if (dictionary.ContainsKey(sr.ReportPath))
                dictionary.Remove(sr.ReportPath);

            // serialize dictionary
            var settings_xml = SavedReportParametersCollectionDictionary.SerializeToXML(dictionary);

            // assign and save updated profile to database
            profile.SetSavedReportParameters(settings_xml);
            profile.Save();
        }
        #endregion Public
    }
}