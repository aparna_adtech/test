﻿using BregVision.Domain.PracticeReportScheduling.Enums;
using System;

namespace BregVision.Models.PracticeReportScheduling
{
    public class ReportSchedule
    {
        private RecurrenceType _type;

        public RecurrenceType Type
        {
            get { return _type; }
        }

        public DateTime? StartDate { get; set; }

        public ReportSchedule(RecurrenceType t)
        {
            _type = t;
        }
    }
}

