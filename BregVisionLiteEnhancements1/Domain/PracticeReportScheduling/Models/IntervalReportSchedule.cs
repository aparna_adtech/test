﻿using BregVision.Models.PracticeReportScheduling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BregVision.Domain.PracticeReportScheduling.Enums;

namespace BregVision.Domain.PracticeReportScheduling.Models
{
    public class IntervalReportSchedule<Tinterval> : ReportSchedule
    {
        private Dictionary<Tinterval, bool> _enabled_intervals = new Dictionary<Tinterval, bool>();

        public Tinterval[] EnabledIntervals
        {
            get
            {
                return
                    _enabled_intervals
                        .Where(x => x.Value)
                        .Select(x => x.Key)
                        .ToArray();
            }
        }

        public void EnableInterval(Tinterval interval)
        {
            if (_enabled_intervals.ContainsKey(interval))
                _enabled_intervals[interval] = true;
            else
                _enabled_intervals.Add(interval, true);
        }

        public void DisableInterval(Tinterval interval)
        {
            if (_enabled_intervals.ContainsKey(interval))
                _enabled_intervals.Remove(interval);
        }

        public IntervalReportSchedule(RecurrenceType t)
            : base(t)
        {
        }
    }
}