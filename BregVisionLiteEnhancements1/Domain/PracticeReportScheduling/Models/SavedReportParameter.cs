﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BregVision.Domain.PracticeReportScheduling.Models
{
    [Serializable]
    public class SavedReportParameter
    {
        public string Key { get; set; }
        public List<string> Values { get; set; }

        public SavedReportParameter() { }

        public SavedReportParameter(string key, IEnumerable<string> values)
        {
            Key = key;
            Values = new List<string>();
            foreach (var v in values)
                Values.Add(v);
        }
    }
}