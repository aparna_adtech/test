﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BregVision.Domain.PracticeReportScheduling.Models
{
    [Serializable]
    public class SavedReportParametersCollection : List<SavedReportParameter>
    {
        public SavedReportParametersCollection()
            : base()
        {
        }
    }

    [Serializable]
    public class SavedReportParametersCollectionPair
    {
        public string ReportPath { get; set; }
        public SavedReportParametersCollection Parameters { get; set; }
    }

    public class SavedReportParametersCollectionDictionary : Dictionary<string, SavedReportParametersCollection>
    {
        public SavedReportParametersCollectionDictionary()
            : base()
        {
        }

        public static SavedReportParametersCollectionDictionary DeserializeFromXML(string xml)
        {
            // non-empty settings, try to de-serialize xml
            // NOTE: returns empty settings collection if de-serialization fails
            var dictionary = new SavedReportParametersCollectionDictionary();
            try
            {
                var serializer =
                    new System.Xml.Serialization.XmlSerializer(
                        typeof(List<SavedReportParametersCollectionPair>));
                var pairs =
                    (List<SavedReportParametersCollectionPair>)
                    serializer.Deserialize(new System.IO.StringReader(xml));
                foreach (var pair in pairs)
                    dictionary.Add(pair.ReportPath, pair.Parameters);
            }
            catch
            {
#if (DEBUG)
                if (System.Diagnostics.Debugger.IsAttached)
                    System.Diagnostics.Debugger.Break();
#endif
            }
            return dictionary;
        }

        public static string SerializeToXML(SavedReportParametersCollectionDictionary dictionary)
        {
            try
            {
                var serializer =
                    new System.Xml.Serialization.XmlSerializer(
                        typeof(List<SavedReportParametersCollectionPair>));
                var pairs = new List<SavedReportParametersCollectionPair>();
                foreach (var item in dictionary)
                    pairs.Add(new SavedReportParametersCollectionPair { ReportPath = item.Key, Parameters = item.Value });
                var settings_xml_tmp = new System.Text.StringBuilder();
                serializer.Serialize(new System.IO.StringWriter(settings_xml_tmp), pairs);
                return settings_xml_tmp.ToString();
            }
            catch
            {
#if (DEBUG)
                if (System.Diagnostics.Debugger.IsAttached)
                    System.Diagnostics.Debugger.Break();
#endif

                // leave settings_xml empty in case of failure
                return string.Empty;
            }
        }
    }

    public class LocalReportParametersCollection : List<ReportParameter>, IList<ReportParameter>, IEnumerable<ReportParameter>
    {
        private Dictionary<string, ReportParameter> _parameter_cache;

        public LocalReportParametersCollection()
            : base()
        {
            _parameter_cache = new Dictionary<string, ReportParameter>();
        }

        public new void Insert(int index, ReportParameter item)
        {
            var itemkey = _getReportParameterItemKey(item);
            if (_parameter_cache.ContainsKey(itemkey))
            {
                throw new ApplicationException("ReportParameter already exists in collection!");
            }
            else
            {
                _parameter_cache.Add(itemkey, item);
                base.Insert(index, item);
            }
        }

        public new void Add(ReportParameter item)
        {
            var itemkey = _getReportParameterItemKey(item);
            if (_parameter_cache.ContainsKey(itemkey))
            {
                // find item in base list and replace
                _parameter_cache[itemkey] = item;
                base[base.FindIndex(x => x.Name == item.Name)] = item;
            }
            else
            {
                _parameter_cache.Add(itemkey, item);
                base.Add(item);
            }
        }

        private string _getReportParameterItemKey(ReportParameter rp)
        {
            return rp.Name;
        }
    }


}