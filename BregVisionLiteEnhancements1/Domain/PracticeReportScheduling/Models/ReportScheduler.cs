﻿using BregVision.Domain.PracticeReportScheduling.Enums;
using BregVision.Models.PracticeReportScheduling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BregVision.Domain.PracticeReportScheduling.Models
{

    public class ReportScheduler
    {
        #region properties

        public string UserId { get; set; }
        public string PracticeId { get; set; }

        #endregion

        #region report event types

        private static class ReportEventType
        {
            public const string TimedSubscription = "TimedSubscription";
        }

        #endregion

        #region report formats

        public static class ReportFormats
        {


            public class FormatItem
            {
                public int ID { get; set; }
                public FormatTypes Format { get; set; }
                public string Description { get; set; }
                public string SSRSReportFormatString { get; set; }
            }

            private static FormatItem[] _Descriptions =
                new FormatItem[]
                {
                    new FormatItem
                    {
                        ID = 1,
                        Format = FormatTypes.PDF,
                        Description = "PDF",
                        SSRSReportFormatString = "PDF"
                    },
                    new FormatItem
                    {
                        ID = 2,
                        Format = FormatTypes.Excel,
                        Description = "Excel",
                        SSRSReportFormatString = "EXCEL"
                    },
                    new FormatItem
                    {
                        ID = 3,
                        Format = FormatTypes.CSV,
                        Description = "CSV",
                        SSRSReportFormatString = "CSV"
                    }
                };

            public static IEnumerable<FormatItem> GetFormats()
            {
                foreach (var d in _Descriptions)
                    yield return d;
            }

            public static FormatItem GetFormatItemForType(FormatTypes type)
            {
                return GetFormats().Where(f => f.Format == type).FirstOrDefault() ?? _Descriptions[0];
            }
        }

        #endregion

        #region report schedule

        private static string GetReportScheduleXml(DateTime? end_date, ReportSchedule schedule_type)
        {
            // create new schedule definition object
            var s = new SSRS2008.ScheduleDefinition();

            // set schedule start date (defaults to tomorrow if not provided)
            if (schedule_type.StartDate.HasValue)
                s.StartDateTime = schedule_type.StartDate.Value;
            else
                s.StartDateTime = DateTime.Today.AddDays(1);

            // set an end date, if applicable
            if (end_date.HasValue)
            {
                s.EndDate = end_date.Value;
                s.EndDateSpecified = true;
            }
            else
            {
                s.EndDateSpecified = false;
            }

            // set recurrence type
            if (schedule_type is DailyReportSchedule)
                s.Item = GetDailyRecurrence();
            else if (schedule_type is WeeklyReportSchedule)
                s.Item = GetWeeklyRecurrence(((WeeklyReportSchedule)schedule_type).EnabledIntervals);
            else if (schedule_type is MonthlyReportSchedule)
            {
                var st = (MonthlyReportSchedule)schedule_type;
                s.Item = GetMonthlyRecurrence(st.RecurrenceDate, st.EnabledIntervals);
            }
            else
                throw new ArgumentException("Unsupported type: " + schedule_type.GetType().FullName + ".", "schedule_type");

            // serialize object to xml
            var xmloutput = new System.Text.StringBuilder();
            new
                System.Xml.Serialization.XmlSerializer(typeof(SSRS2008.ScheduleDefinition))
                    .Serialize(new System.IO.StringWriter(xmloutput), s);

            // return xml output
            return xmloutput.ToString();
        }

        #endregion

        #region report extension settings

        private static SSRS2008.ExtensionSettings GetVisionExtensionSettings(string email_recipient)
        {
            var p =
                new SSRS2008.ParameterValue[] {
                    new SSRS2008.ParameterValue { Name = "TO", Value = email_recipient },
                    new SSRS2008.ParameterValue { Name = "ReplyTo", Value = "reports@bregvision.com" },
                    new SSRS2008.ParameterValue { Name = "IncludeReport", Value = "True" },
                    new SSRS2008.ParameterValue { Name = "RenderFormat", Value = ReportFormats.GetFormatItemForType(FormatTypes.Excel).SSRSReportFormatString },
                    new SSRS2008.ParameterValue { Name = "Subject", Value = "BregVision Scheduled Report: @ReportName (@ExecutionTime)" },
                    new SSRS2008.ParameterValue { Name = "Comment", Value = "Your scheduled report is attached." },
                    new SSRS2008.ParameterValue { Name = "IncludeLink", Value = "False" },
                    new SSRS2008.ParameterValue { Name = "Priority", Value = "NORMAL" }
                };

            return new SSRS2008.ExtensionSettings { ParameterValues = p, Extension = "Report Server Email" };
        }

        #endregion

        #region scheduled report summary data

        public interface IScheduledReportSummary
        {
            string SubscriptionId { get; set; }
            string UserId { get; set; }
            string PracticeId { get; set; }
            string ReportName { get; set; }
            string EmailRecipient { get; set; }
            string ScheduleRecurrenceType { get; set; }
            DateTime DateScheduled { get; set; }
        }

        public class ScheduledReportSummary : IScheduledReportSummary
        {
            public string SubscriptionId { get; set; }
            public string UserId { get; set; }
            public string PracticeId { get; set; }
            public string ReportName { get; set; }
            public string EmailRecipient { get; set; }
            public string ScheduleRecurrenceType { get; set; }
            public DateTime DateScheduled { get; set; }

            public string EncodeDescription()
            {
                return
                    string.Format(
                        @"{0};{1};{2};{3};{4:dq}",
                        UserId,
                        PracticeId,
                        EmailRecipient,
                        ScheduleRecurrenceType,
                        DateScheduled);
            }

            public static IScheduledReportSummary DecodeObjectFromSubscription(SSRS2008.Subscription subscription_obj)
            {
                try
                {
                    var values = subscription_obj.Description.Split(';');
                    var dateScheduled = DateTime.Now;
                    try
                    {
                        dateScheduled = DateTime.Parse(values[4]);
                    }
                    catch { }

                    return
                        new ScheduledReportSummary
                        {
                            SubscriptionId = subscription_obj.SubscriptionID,
                            UserId = values[0],
                            PracticeId = values[1],
                            ReportName = subscription_obj.Report,
                            EmailRecipient = values[2],
                            ScheduleRecurrenceType = values[3],
                            DateScheduled = dateScheduled
                        };
                }
                catch
                {
                    // skip over things we can't decode (wrong format)
                    return null;
                }
            }

            private ScheduledReportSummary()
            {
            }

            public ScheduledReportSummary(string user_id, string practice_id, string email_recipient, RecurrenceType schedule_recurrence_type, DateTime date_scheduled)
            {
                UserId = user_id;
                PracticeId = practice_id;
                EmailRecipient = email_recipient;
                ScheduleRecurrenceType = schedule_recurrence_type.ToString();
                DateScheduled = date_scheduled;
            }
        }

        #endregion

        #region helper methods

        /// <summary>
        /// Sets up credentials for authenticating the connection to the report service SOAP endpoint.
        /// </summary>
        /// <param name="rs"></param>
        private static void SetReportCredentials(SSRS2008.ReportingService2005 rs)
        {
            if (rs == null)
                return;

            rs.Credentials = new MyReportServerCredentials();
        }

        /// <summary>
        /// Set webservice URL if one is defined in Web.config. If not defined, leave
        /// the embedded URL in place.
        /// </summary>
        /// <param name="rs">Reporting service instance to apply URL setting to.</param>
        private static void SetReportServiceUrl(SSRS2008.ReportingService2005 rs)
        {
            if (rs == null)
                return;

            var u = System.Configuration.ConfigurationManager.AppSettings["ReportServerServiceUrl"] ?? string.Empty;
            if (!string.IsNullOrEmpty(u))
                rs.Url = u;
        }

        /// <summary>
        /// Returns a RecurrencePattern object representing a daily recurrence.
        /// </summary>
        /// <returns></returns>
        private static SSRS2008.RecurrencePattern GetDailyRecurrence()
        {
            // create and return an instance that has a 1-day recurrence interval (hard-coded for now)
            return new SSRS2008.DailyRecurrence { DaysInterval = 1 };
        }

        /// <summary>
        /// Returns a RecurrencePattern object representing a recurrence that happens
        /// every week; but, on specified enabled weekdays.
        /// </summary>
        /// <param name="enabled_weekdays">Array containing all days of the week that recurrence should happen on.</param>
        /// <returns></returns>
        private static SSRS2008.RecurrencePattern GetWeeklyRecurrence(RecurrenceWeekdays[] enabled_weekdays)
        {
            // validate input
            if (enabled_weekdays == null || enabled_weekdays.Length < 1)
                throw new ArgumentException("At least 1 weekday must be enabled.", "enabled_weekdays", null);

            // create new weekly recurrence instance
            var r = new SSRS2008.WeeklyRecurrence();

            // specify recurrence of every week (hard-coded for now)
            r.WeeksInterval = 1;
            r.WeeksIntervalSpecified = true;

            // specify days of week
            r.DaysOfWeek =
                new SSRS2008.DaysOfWeekSelector
                {
                    Sunday = enabled_weekdays.Contains(RecurrenceWeekdays.Sunday),
                    Monday = enabled_weekdays.Contains(RecurrenceWeekdays.Monday),
                    Tuesday = enabled_weekdays.Contains(RecurrenceWeekdays.Tuesday),
                    Wednesday = enabled_weekdays.Contains(RecurrenceWeekdays.Wednesday),
                    Thursday = enabled_weekdays.Contains(RecurrenceWeekdays.Thursday),
                    Friday = enabled_weekdays.Contains(RecurrenceWeekdays.Friday),
                    Saturday = enabled_weekdays.Contains(RecurrenceWeekdays.Saturday)
                };

            // return completed recurrence object
            return r;
        }

        /// <summary>
        /// Returns a RecurrencePattern object representing a recurrence that happens
        /// every year on specified months and date of month.
        /// </summary>
        /// <param name="recurrence_date">Date of month that recurrence should happen on.</param>
        /// <param name="enabled_months">Months of the year that recurrence should happen on.</param>
        /// <returns></returns>
        private static SSRS2008.RecurrencePattern GetMonthlyRecurrence(int recurrence_date, RecurrenceMonths[] enabled_months)
        {
            // validate input
            if (recurrence_date < 1 || recurrence_date > 31)
                throw new ArgumentOutOfRangeException("recurrence_date", "recurrence_date must be between 1 and 31.");
            if (enabled_months == null || enabled_months.Length < 1)
                throw new ArgumentException("At least 1 month must be enabled.", "enabled_months", null);

            // create new monthly recurrence instance
            var r = new SSRS2008.MonthlyRecurrence();

            // set recurrence date
            r.Days = recurrence_date.ToString();

            // set recurrence month(s)
            r.MonthsOfYear =
                new SSRS2008.MonthsOfYearSelector
                {
                    January = enabled_months.Contains(RecurrenceMonths.January),
                    February = enabled_months.Contains(RecurrenceMonths.February),
                    March = enabled_months.Contains(RecurrenceMonths.March),
                    April = enabled_months.Contains(RecurrenceMonths.April),
                    May = enabled_months.Contains(RecurrenceMonths.May),
                    June = enabled_months.Contains(RecurrenceMonths.June),
                    July = enabled_months.Contains(RecurrenceMonths.July),
                    August = enabled_months.Contains(RecurrenceMonths.August),
                    September = enabled_months.Contains(RecurrenceMonths.September),
                    October = enabled_months.Contains(RecurrenceMonths.October),
                    November = enabled_months.Contains(RecurrenceMonths.November),
                    December = enabled_months.Contains(RecurrenceMonths.December),
                };

            // return completed recurrence object
            return r;
        }

        #endregion

        #region credential implementation

        /// <summary>
        /// Implementation of System.Net.ICredentials that provides either current execution
        /// context credentials for network access or credentials based on Web.config configured
        /// ReportServerUser and ReportServerPassword.
        /// </summary>
        private class MyReportServerCredentials : System.Net.ICredentials
        {
            public System.Net.NetworkCredential GetCredential(Uri uri, string authType)
            {
                var u = System.Configuration.ConfigurationManager.AppSettings["ReportServerUser"] ?? string.Empty;
                var p = System.Configuration.ConfigurationManager.AppSettings["ReportServerPassword"] ?? string.Empty;
                if (!string.IsNullOrEmpty(u))
                    return new System.Net.NetworkCredential(u, p);
                else
                    return System.Net.CredentialCache.DefaultNetworkCredentials;
            }
        }

        #endregion

        #region Private Methods

        private SSRS2008.ParameterValue[] GetSSRSParametersFromReport(Microsoft.Reporting.WebForms.ServerReport report, ReportSchedule schedule)
        {
            // validate parameters
            if (report == null)
                throw new ArgumentNullException("report");
            // email_recipient and schedule validated in main method...

            // generate report parameters array
            var report_parameters = (from r in report.GetParameters()
                                     from rp in r.Values
                                     where (r.Name.ToLower() != "startdate") && (r.Name.ToLower() != "enddate")
                                     select new SSRS2008.ParameterValue
                                     {
                                         Name = r.Name,
                                         Value = r.Name.Equals("ScheduleType", StringComparison.OrdinalIgnoreCase) == true ? (((int)schedule.Type) + 1).ToString() : rp
                                     }).ToArray();
            return report_parameters;
        }
        #endregion Private Methods

        #region public methods

        public void ScheduleReport(Microsoft.Reporting.WebForms.ServerReport report, string email_recipient, ReportSchedule schedule)
        {
            // validate parameters
            if (report == null)
                throw new ArgumentNullException("report");

            var parameters = GetSSRSParametersFromReport(report, schedule);
           
            // schedule the report
            ScheduleReport(report.ReportPath, email_recipient, schedule, parameters);
        }

        public void ScheduleReport(string report_path, string email_recipient, ReportSchedule schedule, string practice_id)
        {
            // generate report parameters array
            var report_parameters =
                new SSRS2008.ParameterValue[] { new SSRS2008.ParameterValue { Name = "PracticeID", Value = practice_id } };

            // schedule the report
            ScheduleReport(report_path, email_recipient, schedule, report_parameters);
        }

        public void ScheduleReport(string report_path, string email_recipient, ReportSchedule schedule, SSRS2008.ParameterValue[] report_parameters)
        {
            // validate parameters
            if (string.IsNullOrEmpty(report_path))
                throw new ArgumentException("Null or empty report path.", "report_path");
            if (string.IsNullOrEmpty(email_recipient))
                throw new ArgumentException("Null or empty report recipient.", "email_recipient");
            if (schedule == null)
                throw new ArgumentNullException("schedule");

            // generate report description
            var desc = new ScheduledReportSummary(UserId, PracticeId, email_recipient, schedule.Type, DateTime.Now).EncodeDescription();

            // instantiate ssrs proxy and set credentials/url
            var rs = new SSRS2008.ReportingService2005();
            SetReportCredentials(rs);
            SetReportServiceUrl(rs);

            // call method to create subscription
            rs.CreateSubscription(
                report_path,
                GetVisionExtensionSettings(email_recipient),
                desc,
                ReportEventType.TimedSubscription,
                GetReportScheduleXml(null, schedule),
                report_parameters);
        }

        public IEnumerable<IScheduledReportSummary> ListScheduledReports()
        {
            var rs = new SSRS2008.ReportingService2005();
            SetReportCredentials(rs);
            SetReportServiceUrl(rs);
            var s = rs.ListSubscriptions(null, null);
            return
                s.Select(x => ScheduledReportSummary.DecodeObjectFromSubscription(x))
                    .Where(x => (x != null) && (x.UserId == UserId) && (x.PracticeId == PracticeId))
                    .AsEnumerable();
        }

        public IScheduledReportSummary GetScheduledReportBySubscriptionId(string subscriptionId)
        {
            var rs = new SSRS2008.ReportingService2005();
            SetReportCredentials(rs);
            SetReportServiceUrl(rs);
            var s = rs.ListSubscriptions(null, null);
            return
                s.Select(x => ScheduledReportSummary.DecodeObjectFromSubscription(x))
                    .FirstOrDefault(x => (x != null) &&(x.SubscriptionId == subscriptionId) && (x.UserId == UserId) && (x.PracticeId == PracticeId));
        }

        public void RemoveScheduledReport(string subscription_id)
        {
            var rs = new SSRS2008.ReportingService2005();
            SetReportCredentials(rs);
            SetReportServiceUrl(rs);
            rs.DeleteSubscription(subscription_id);
        }

        public void UpdateScheduledReportParameters(string subscription_id, Microsoft.Reporting.WebForms.ServerReport report, string email_recipient, ReportSchedule schedule)
        {
            using( var rs = new SSRS2008.ReportingService2005())
            {
                SetReportCredentials(rs);
                SetReportServiceUrl(rs);
                var parameters = GetSSRSParametersFromReport(report, schedule);
                var desc = new ScheduledReportSummary(UserId, PracticeId, email_recipient, schedule.Type, DateTime.Now).EncodeDescription();
                var extensions = GetVisionExtensionSettings(email_recipient);
                var matchData = GetReportScheduleXml(null, schedule);
                rs.SetSubscriptionProperties(subscription_id.ToUpper(), extensions, desc, ReportEventType.TimedSubscription, matchData, parameters);
            }
            
        }

        #endregion

        #region constructor

        public ReportScheduler(string user_id, string practice_id)
        {
            UserId = user_id;
            PracticeId = practice_id;
        }

        public ReportScheduler(int user_id, int practice_id)
        {
            UserId = user_id.ToString();
            PracticeId = practice_id.ToString();
        }

        #endregion
    }


    public static class ReportSchedulerExtensions
    {
        public static System.Data.DataTable AsDataTable(this IEnumerable<ReportScheduler.IScheduledReportSummary> o)
        {
            var d = new System.Data.DataTable();
            d.Columns.Add(new System.Data.DataColumn { AllowDBNull = false, ColumnName = "SubscriptionId", DataType = typeof(string) });
            d.Columns.Add(new System.Data.DataColumn { AllowDBNull = false, ColumnName = "UserId", DataType = typeof(string) });
            d.Columns.Add(new System.Data.DataColumn { AllowDBNull = false, ColumnName = "PracticeId", DataType = typeof(string) });
            d.Columns.Add(new System.Data.DataColumn { AllowDBNull = false, ColumnName = "ReportName", DataType = typeof(string) });
            d.Columns.Add(new System.Data.DataColumn { AllowDBNull = false, ColumnName = "EmailRecipient", DataType = typeof(string) });
            d.Columns.Add(new System.Data.DataColumn { AllowDBNull = false, ColumnName = "ScheduleRecurrenceType", DataType = typeof(string) });
            d.Columns.Add(new System.Data.DataColumn { AllowDBNull = false, ColumnName = "DateScheduled", DataType = typeof(DateTime) });

            foreach (var item in o)
            {
                var r = d.NewRow();
                r["SubscriptionId"] = item.SubscriptionId;
                r["UserId"] = item.UserId;
                r["PracticeId"] = item.PracticeId;
                r["ReportName"] = item.ReportName;
                r["EmailRecipient"] = item.EmailRecipient;
                r["ScheduleRecurrenceType"] = item.ScheduleRecurrenceType;
                r["DateScheduled"] = item.DateScheduled;

                d.Rows.Add(r);
            }

            return d;
        }
    }
}