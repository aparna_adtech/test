﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BregVision.Domain.PracticeReportScheduling.Models
{
    public class ReportsListItem
    {
        public string ID { get; set; }
        public string Path { get; set; }
    }

}