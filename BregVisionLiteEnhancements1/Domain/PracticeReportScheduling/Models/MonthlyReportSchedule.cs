﻿using BregVision.Domain.PracticeReportScheduling.Enums;
using BregVision.Models.PracticeReportScheduling;
using System;

namespace BregVision.Domain.PracticeReportScheduling.Models
{
    public class MonthlyReportSchedule : IntervalReportSchedule<RecurrenceMonths>
    {
        private int _recurrence_date;

        public int RecurrenceDate
        {
            get { return _recurrence_date; }
            set
            {
                if (value < 1 || value > 31)
                    throw new ArgumentException("RecurrenceDate must be between 1 and 31.");
                _recurrence_date = value;
            }
        }

        public MonthlyReportSchedule()
            : base(RecurrenceType.Monthly)
        {
        }

        public MonthlyReportSchedule(int recurrence_date, RecurrenceMonths[] enabled_months)
            : this()
        {
            RecurrenceDate = recurrence_date;

            foreach (var m in enabled_months)
                EnableInterval(m);
        }

        public MonthlyReportSchedule(int recurrence_date, RecurrenceMonths[] enabled_months, DateTime start_date)
            : this(recurrence_date, enabled_months)
        {
            StartDate = start_date;
        }
    }
}