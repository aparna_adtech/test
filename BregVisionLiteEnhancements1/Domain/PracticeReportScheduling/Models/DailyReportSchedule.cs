﻿using BregVision.Domain.PracticeReportScheduling.Enums;
using BregVision.Models.PracticeReportScheduling;
using System;

namespace BregVision.Domain.PracticeReportScheduling.Models
{
    public class DailyReportSchedule : ReportSchedule
    {
        public DailyReportSchedule()
            : base(RecurrenceType.Daily)
        {
        }

        public DailyReportSchedule(DateTime start_date)
            : this()
        {
            StartDate = start_date;
        }
    }
}