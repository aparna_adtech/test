﻿using BregVision.Domain.PracticeReportScheduling.Enums;
using System;

namespace BregVision.Domain.PracticeReportScheduling.Models
{
    public class WeeklyReportSchedule : IntervalReportSchedule<RecurrenceWeekdays>
    {
        public WeeklyReportSchedule()
            : base(RecurrenceType.Weekly)
        {
        }

        public WeeklyReportSchedule(RecurrenceWeekdays[] enabled_weekdays)
            : this()
        {
            foreach (var wd in enabled_weekdays)
                EnableInterval(wd);
        }

        public WeeklyReportSchedule(RecurrenceWeekdays[] enabled_weekdays, DateTime start_date)
            : this(enabled_weekdays)
        {
            StartDate = start_date;
        }
    }

}