﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BregVision.Domain.PracticeReportScheduling.Enums
{
    public enum RecurrenceType
    {
        Daily,
        Weekly,
        Monthly
    }
}