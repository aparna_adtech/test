﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BregVision.Domain.DispenseHistory.Models
{
    public class MonthlyHistoricalProductReceiptAggregate
    {
        public double TotalQuantity { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int PracticeCatalogProductID {get;set;}

    }
}