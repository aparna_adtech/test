﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BregVision.Domain.DispenseHistory.Models
{
    public class HistoricalProductReceiptSummary
    {
        public int PracticeCatalogProductID { get; set; }
        public MonthlyHistoricalProductReceiptAggregate HighestMonth {get;set;}
        public MonthlyHistoricalProductReceiptAggregate LowestMonth { get; set; }
        public MonthlyHistoricalProductReceiptAggregate AverageMonth { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}