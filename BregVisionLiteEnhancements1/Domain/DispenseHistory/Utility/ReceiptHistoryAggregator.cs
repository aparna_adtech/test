﻿using BregVision.Domain.DispenseHistory.Models;
using ClassLibrary.DAL;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BregVision.Domain.DispenseHistory.Utility
{
    public static class ReceiptHistoryAggregator
    {
        public static List<MonthlyHistoricalProductReceiptAggregate> GetMonthlyReceiptHistoryByPracticeLocationID(int practiceLocationID, DateTime startDate, DateTime endDate)
        {          
            using (var ctx = VisionDataContext.GetVisionDataContext())
            {

                var receipts = ctx.Dispenses
                    .Where(d => d.PracticeLocationID == practiceLocationID && d.DateDispensed > startDate && d.DateDispensed < endDate)
                    .Join(ctx.DispenseDetails, d => d.DispenseID, dd => dd.DispenseID,
                        (d, dd) => new
                        {
                            dd.PracticeCatalogProductID,
                            dd.Quantity,
                            d.DateDispensed
                        }
                    )
                    .GroupBy(jn => new { jn.PracticeCatalogProductID, jn.DateDispensed.Year, jn.DateDispensed.Month }, jn => jn.Quantity.HasValue ? jn.Quantity.Value : 0,
                        (key, qty) => new MonthlyHistoricalProductReceiptAggregate
                        {
                            PracticeCatalogProductID = key.PracticeCatalogProductID,
                            Year = key.Year,
                            Month = key.Month,
                            TotalQuantity = qty.Sum(v => v)
                        })
                    .OrderBy(hpra => hpra.PracticeCatalogProductID).ThenBy(hpra => hpra.Year).ThenBy(hpra => hpra.Month);

                return receipts.ToList();
            }          
        }

        public static List<MonthlyHistoricalProductReceiptAggregate> GetMonthlyReceiptHistoryByPracticeCatalogProductID(int practiceCatalogProductID, int practiceLocationID, DateTime startDate, DateTime endDate)
        {
            using (var ctx = VisionDataContext.GetVisionDataContext())
            {

                var receipts = ctx.Dispenses
                    .Where(d => d.PracticeLocationID == practiceLocationID && d.DateDispensed > startDate && d.DateDispensed < endDate)
                    .Join(ctx.DispenseDetails, d => d.DispenseID, dd => dd.DispenseID,
                        (d, dd) => new
                        {
                            dd.PracticeCatalogProductID,
                            dd.Quantity,
                            d.DateDispensed
                        }
                    )
                    .Where(jn => jn.PracticeCatalogProductID == practiceCatalogProductID) 
                    .GroupBy(jn => new { jn.DateDispensed.Year, jn.DateDispensed.Month }, jn => jn.Quantity.HasValue ? jn.Quantity.Value : 0,
                        (key, qty) => new MonthlyHistoricalProductReceiptAggregate
                        {
                            PracticeCatalogProductID = practiceCatalogProductID,
                            Year = key.Year,
                            Month = key.Month,
                            TotalQuantity = qty.Sum(v => v)
                        })
                    .OrderBy(hpra => hpra.Year).ThenBy(hpra => hpra.Month);

                return receipts.ToList();
            }
        }

        public static HistoricalProductReceiptSummary GenerateReceiptHistorySummary(List<MonthlyHistoricalProductReceiptAggregate> monthlyAggregates, DateTime startDate, DateTime endDate)
        {
            if (monthlyAggregates.Count != 0)
            {
                var high = monthlyAggregates.First();
                var low = monthlyAggregates.First();
                double total = 0, monthsInRange = Math.Ceiling(endDate.Subtract(startDate).Days/(365.2425/12));
                for (int i = 0; i < monthlyAggregates.Count; i++)
                {
                    var qty = monthlyAggregates[i].TotalQuantity;
                    if (qty > high.TotalQuantity) high = monthlyAggregates[i];
                    else if (qty < low.TotalQuantity) low = monthlyAggregates[i];
                    total += qty;
                }
                return new HistoricalProductReceiptSummary()
                {
                    HighestMonth = high,
                    LowestMonth = low,
                    AverageMonth =
                        new MonthlyHistoricalProductReceiptAggregate()
                        {
                            TotalQuantity = total/monthsInRange,
                            PracticeCatalogProductID = high.PracticeCatalogProductID
                        },
                    StartDate = startDate,
                    EndDate = endDate
                };
            }

            return new HistoricalProductReceiptSummary();
        }

        public static HistoricalProductReceiptSummary GenerateReceiptHistorySummary(int practiceCatalogProductID, int practiceLocationID, DateTime startDate, DateTime endDate)
        {
            var monthlyAggs = GetMonthlyReceiptHistoryByPracticeCatalogProductID(practiceCatalogProductID, practiceLocationID, startDate, endDate);
            return GenerateReceiptHistorySummary(monthlyAggs, startDate, endDate);
        }
    }
}