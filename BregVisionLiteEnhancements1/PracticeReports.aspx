<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PracticeReports.aspx.cs"
	Inherits="PracticeReports" Title="Breg Vision - Practice Reports" %>

<%@ Register Src="~/MasterPages/UserControls/MainMenu.ascx" TagName="MainMenu" TagPrefix="bvu" %>
<%@ Register Src="~/MasterPages/UserControls/SiteHeaderLogo.ascx" TagName="SiteHeaderLogo"
	TagPrefix="bvu" %>
<%@ Register Src="~/MasterPages/UserControls/SiteCart.ascx" TagName="SiteCart" TagPrefix="bvu" %>
<%@ Register Src="~/MasterPages/UserControls/SiteHelp.ascx" TagName="SiteHelp" TagPrefix="bvu" %>
<%--<%@ Register Src="~/Campaigns/UserControls/BannerAd.ascx" TagName="BannerAd" TagPrefix="ad" %>--%>
<%@ Register Src="~/Admin/UserControls/Practice/PracticeReports.ascx" TagName="PracticeReports"
	TagPrefix="bvl" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>Practice Reports</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	
	<%-- INTERNET EXPLORER 9 AND BELOW --%>
	<!--[if lt IE 10]>
        <script type="text/javascript">
            document.documentElement.className += ' ie9';
        </script>

		<style type="text/css">
		    #ctlMainMenu_MainRadTabStrip {
		    	margin: 0 !important;
		    }

		    #ctlMainMenu_MainRadTabStrip .rtsUL {
		    	display: block;
		    }

		    #ctlMainMenu_MainRadTabStrip .rtsLI {
		    	display: inline-block;
		    	margin-right: 25px;
		    }
		</style>
	<![endif]-->

	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,900" rel="stylesheet" type="text/css"/>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <link runat="server" type="text/css" href="App_Themes/Breg/BrowserOverrides.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/Upload.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/AutoCompleteBox.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/Button.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/Calendar.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/ColorPicker.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/ComboBox.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/DataForm.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/DataPager.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/Dock.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/DropDownList.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/DropDownTree.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/Editor.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/FileExplorer.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/Filter.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/FormDecorator.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/Grid.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/ImageEditor.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/Input.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/ListBox.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/Menu.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/Notification.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/OrgChart.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/PanelBar.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/ProgressArea.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/ProgressBar.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/Rating.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/RibbonBar.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/Rotator.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/Scheduler.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/SearchBox.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/Slider.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/SocialShare.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/Splitter.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/TabStrip.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/TagCloud.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/Tile.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/TileList.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/ToolBar.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/ToolTip.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/TreeList.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/TreeView.Breg.css" rel="stylesheet" />
	<link runat="server" type="text/css" href="App_Themes/Breg/Window.Breg.css" rel="stylesheet" />
    <link runat="server" type="text/css" href="Include/jquery/css/redmond/jquery-ui-1.8.9.custom.css" rel="stylesheet" />
	<script type="text/javascript" src="Include/jquery/js/jquery-1.4.4.min.js"></script>
    <script src="Include/jquery/js/jquery-ui-1.8.9.custom.min.js"></script>
    <script type="text/javascript">
        isIE10 = (document.documentMode != null && document.documentMode === 10);
        isIE11 = (document.documentMode != null && document.documentMode >= 11);
        if (isIE10) {
            document.documentElement.className += ' ie10';
        }
        if (isIE11) {
            document.documentElement.className += '  ie';
        }
        isFirefox = (navigator.userAgent.toLowerCase().indexOf('firefox') > -1);
        if (isFirefox) {
            document.documentElement.className += ' ff';
        }
        var browser = navigator.userAgent.toLowerCase();
        if (browser.indexOf('safari') != -1) {
            if (browser.indexOf('chrome') > -1) {
                //chrome
            } else {
                document.documentElement.className += ' saf';
            }
        }

      </script>

	<telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
	</telerik:RadStyleSheetManager>
</head>
<body>
	<form id="form1" runat="server">
		
		<telerik:RadScriptManager ID="radScriptManager1" runat="server">
		</telerik:RadScriptManager>
		<telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
		</telerik:RadAjaxManager>

		<div id="container" class="main-container">
			<div id="headerBlock" class="header-container">
				<div class="site-navbar flex-row">
					<div>
						<bvu:SiteHeaderLogo ID="ctlSiteHeaderLogo" runat="server" />
					</div>
					 <div class="flex-row site-navbar-actions">
							<div class="flex-row flex-grow justify-end">
							<asp:Image runat="server" ImageUrl="~/App_Themes/Breg/images/Common/user.png" Height="30" Width="30" />
							<asp:LoginName ID="LoginName1" runat="server" FormatString="{0}" CssClass="strong" />
							&nbsp;<asp:HyperLink ID="lnkLogOut" runat="server" NavigateUrl="~/processlogin.aspx?action=logout" Text="(log out)"></asp:HyperLink>
						</div>
					</div>
				</div>
				<div class="welcome-bar flex-row">
					<div class="practice-location flex-row justify-start no-wrap">
						<h1>
							<asp:Label ID="lblClinic" runat="server" Text="" Visible="True"></asp:Label><span class="light">&thinsp;&raquo;&thinsp;</span>
						</h1>
						<span id="spnChangeLocation" class="location-static" style="visibility: visible">
							<asp:Label ID="lblLocation" runat="server" Text="Location: ABC"></asp:Label>
						</span>
					</div>
					<div class="flex-row justify-end">
						<bvu:MainMenu ID="ctlMainMenu" runat="server" EnableTheming="True" />
					</div>
				</div>
				<div class="welcome-bar-shadow"></div>
			</div>
			<div class="tabContent-container" id="divMainContent">
				<bvl:PracticeReports ID="ctlPracticeReports" runat="server" EnableTheming="false" />
			</div>
		</div>
	</form>
</body>
</html>

<script>
    function pageLoad() {
        var control = $find("ctlPracticeReports_ReportViewer1");
        if (control != null)
            control.add_propertyChanged(viewerPropertyChanged);        
    }
    
    function viewerPropertyChanged(sender, e) {
        if (e.get_propertyName() == "isLoading") {
            if ($.browser.webkit) {
                var datetext = $(":hidden[id*='DatePickers']").val();
                if (datetext != null && datetext != "") {
                    $(datetext.split(",")).each(function (i, item) {
                        //checking if the image for the calendar icon is already added as the isLoading property gets set multiple times during the report lifecycle
                        var s = $("table[id*='ParametersGrid'] span").filter(function (i) {
                            var v = "[" + $(this).text() + "]";
                            return (v != null && v.indexOf(item) >= 0);
                        }).parent("td").next("td").find("img");

                        if (s.length == 0) {
                            //only add the image if it there is no img tag next to input textbox of the date report parameters
                            var h = $("table[id*='ParametersGrid'] span").filter(function (i) {
                                var v = "[" + $(this).text() + "]";
                                return (v != null && v.indexOf(item) >= 0);
                            }).parent("td").next("td").find("input").datepicker({
                                showOn: "button"
                              , buttonImage: "Images/SSRSCalendarIcon.gif"
                              , buttonImageOnly: true
                              , dateFormat: 'mm/dd/yy'
                              , changeMonth: true
                              , changeYear: true
                            });
                        }
                    });
                }
            }
        }
    }
</script>