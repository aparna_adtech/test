﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BregVision
{
    /// <summary>
    /// Summary description for Version
    /// </summary>
    public class Version : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Write(GetVersionString());
        }

        public static string GetVersionString()
        {
            var v = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(3);
#if DEBUG
            v += " (debug)";
#endif
            return v;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}