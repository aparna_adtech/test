<%@ Page Language="C#" MasterPageFile="~/MasterPages/SiteMaster.Master" AutoEventWireup="true"
  CodeBehind="CheckIn.aspx.cs" Inherits="BregVision.CheckIn" Title="Breg Vision - Check-In" %>

<%@ MasterType VirtualPath="~/MasterPages/SiteMaster.Master" %>
<%@ Register Src="~/UserControls/CheckInPage/CheckInPoOutstanding.ascx" TagName="CheckInPoOutstanding"
  TagPrefix="bvu" %>
<%@ Register Src="~/UserControls/CheckInPage/SearchPOs.ascx" TagName="SearchPOs"
  TagPrefix="bvu" %>
<%@ Register Src="~/UserControls/CheckInPage/POManualCheckIn.ascx" TagName="POManualCheckIn"
  TagPrefix="bvu" %>
<%@ Register Src="~/UserControls/CheckInPage/CheckInCustomBrace.ascx" TagName="CheckInCustomBrace"
  TagPrefix="bvu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
  
  <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" EnableViewState="True">
    <AjaxSettings>
      <telerik:AjaxSetting AjaxControlID="rtsCheckIn">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="rtsCheckIn" />
          <telerik:AjaxUpdatedControl ControlID="rmpCheckIn" LoadingPanelID="RadAjaxLoadingPanel1" />
        </UpdatedControls>
      </telerik:AjaxSetting>
    </AjaxSettings>
  </telerik:RadAjaxManager>
  
  <div class="flex-row subWelcome-bar">
    <h1>Check-In</h1>
    <bvu:RecordsPerPage ID="ctlRecordsPerPage" runat="server" />
    <telerik:RadTabStrip ID="rtsCheckIn" runat="server" AutoPostBack="True" MultiPageID="rmpCheckIn"
      SelectedIndex="0">
      <Tabs>
        <telerik:RadTab ID="TabOutStandingPO" runat="server" PageViewID="PageView1" Selected="True">
          <TabTemplate>Outstanding P.O.s
                       
            <bvu:Help ID="Help2" runat="server" HelpID="99" />
          </TabTemplate>
        </telerik:RadTab>
        <telerik:RadTab ID="TabSearchPO" runat="server" PageViewID="PageView2">
          <TabTemplate>Search P.O.s
                    </TabTemplate>
        </telerik:RadTab>
        <telerik:RadTab ID="TabManualCheckIn" runat="server" PageViewID="PageView3">
          <TabTemplate>Manual Check-In
                       
            <bvu:Help ID="Help4" runat="server" HelpID="100" />
          </TabTemplate>
        </telerik:RadTab>
        <telerik:RadTab ID="TabOutStandingOrder" runat="server" PageViewID="PageView4">
          <TabTemplate>Outstanding Custom Brace Orders
                       
            <bvu:Help ID="Help5" runat="server" HelpID="101" />
          </TabTemplate>
        </telerik:RadTab>
      </Tabs>
    </telerik:RadTabStrip>
  </div>
  <div class="subTabContent-container">
    <div class="content">
      <div class="section">
        <telerik:RadMultiPage ID="rmpCheckIn" runat="server" SelectedIndex="0" RenderSelectedPageOnly="True">
          <telerik:RadPageView ID="PageView1" runat="server">
            <bvu:CheckInPoOutstanding ID="ctlCheckInPoOutstanding" runat="server" Visible="true" />
          </telerik:RadPageView>
          <telerik:RadPageView ID="PageView2" runat="server">
            <bvu:SearchPOs ID="ctlSearchPOs" runat="server" />
          </telerik:RadPageView>
          <telerik:RadPageView ID="PageView3" runat="server">
            <bvu:POManualCheckIn ID="ctlPOManualCheckIn" runat="server" />
          </telerik:RadPageView>
          <telerik:RadPageView ID="PageView4" runat="server">
            <bvu:CheckInCustomBrace ID="ctlCheckInCustomBrace" runat="server" />
          </telerik:RadPageView>
        </telerik:RadMultiPage>
      </div>
      <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" InitialDelayTime="500" EnableAjaxSkinRendering="True" BackgroundPosition="Center" Skin="Breg" EnableEmbeddedSkins="False">
    <img src="App_Themes/Breg/images/Common/loading.gif" />
  </telerik:RadAjaxLoadingPanel>
    </div>
  </div>
</asp:Content>
