<%@ Page Language="C#" MasterPageFile="~/MasterPages/SiteMaster.Master" AutoEventWireup="true"
    CodeBehind="POShipBillCustomBrace.aspx.cs" Inherits="BregVision.POShipBillCustomBrace"
    Title="PO" %>
<%@ Register src="UserControls/DispensePage/CustomBracePDF.ascx" tagname="CustomBrace" tagprefix="bvu" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<asp:HiddenField ID="ParentBregVisionOrderID" runat="server" />
	<asp:HiddenField ID="ParentShoppingCartID" runat="server" />
    <bvu:CustomBrace ID="CustomBrace" runat="server" BregVisionOrderID='<%# BregVisionOrderID %>' ShoppingCartID='<%# ShoppingCartID %>' IsReadOnly="false" />
</asp:Content>
