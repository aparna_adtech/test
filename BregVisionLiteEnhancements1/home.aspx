<%@ Page Language="C#" MasterPageFile="~/MasterPages/SiteMaster.master" AutoEventWireup="True"
  CodeBehind="home.aspx.cs" Inherits="BregVision.Home" Title="Breg Vision - Home" %>

<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Data" %>
<%@ Register Assembly="RadComboBox.Net2" Namespace="Telerik.WebControls" TagPrefix="radC" %>
<%@ Register Assembly="RadToolbar.Net2" Namespace="Telerik.WebControls" TagPrefix="radTlb" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ContentPlaceHolderID="MainContent" ID="MainContent" runat="server">
  <telerik:RadCodeBlock runat="server">
    <script type="text/javascript">
      Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(startAjaxRequest);
      Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endAjaxRequest);

      var btnAddItemsSearchDisabled = false;
      var btnAddItemsReOrderGridDisabled = false;

      function startAjaxRequest(sender, args) {
        var b;

        b = document.getElementById('<%= btnAddItemsSearch.ClientID %>');
      btnAddItemsSearchDisabled = b.disabled;
      b.disabled = true;

      b = document.getElementById('<%= btnAddItemsReOrderGrid.ClientID %>');
      btnAddItemsReOrderGridDisabled = b.disabled;
      b.disabled = true;
    }

    function endAjaxRequest(sender, args) {
      if (!btnAddItemsSearchDisabled) {
        document.getElementById('<%= btnAddItemsSearch.ClientID %>').disabled = btnAddItemsSearchDisabled;
      }
      document.getElementById('<%= btnAddItemsReOrderGrid.ClientID %>').disabled = btnAddItemsReOrderGridDisabled;
    }
    </script>
  </telerik:RadCodeBlock>
  <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    <AjaxSettings>
      <telerik:AjaxSetting AjaxControlID="grdSearch">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="grdSearch" LoadingPanelID="LoadingPanel1" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="grdItemsforReorder">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="grdItemsforReorder" LoadingPanelID="LoadingPanel1" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="grdLast10ItemsDispensed">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="grdLast10ItemsDispensed" LoadingPanelID="LoadingPanel1" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="btnAddItemsSearch">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="grdSearch" LoadingPanelID="LoadingPanel1" />
          <telerik:AjaxUpdatedControl ControlID="grdItemsforReorder" LoadingPanelID="LoadingPanel1" />
          <telerik:AjaxUpdatedControl ControlID="lblStatusReplacedText2" />
          <telerik:AjaxUpdatedControl ControlID="lblStatusReplaced2" />
          <telerik:AjaxUpdatedControl ControlID="lblStatus2" />
          <telerik:AjaxUpdatedControl ControlID="lblBrowseDispense" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="txtRecordsDisplayed">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="grdSearch" LoadingPanelID="LoadingPanel1" />
          <telerik:AjaxUpdatedControl ControlID="grdItemsforReorder" LoadingPanelID="LoadingPanel1" />
          <telerik:AjaxUpdatedControl ControlID="grdLast10ItemsDispensed" LoadingPanelID="LoadingPanel1" />
          <telerik:AjaxUpdatedControl ControlID="grdCustomBracesToBeDispensed" LoadingPanelID="LoadingPanel1" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="btnAddItemsReOrderGrid">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="grdSearch" LoadingPanelID="LoadingPanel1" />
          <telerik:AjaxUpdatedControl ControlID="grdItemsforReorder" LoadingPanelID="LoadingPanel1" />
          <telerik:AjaxUpdatedControl ControlID="lblStatusReplacedText" />
          <telerik:AjaxUpdatedControl ControlID="lblStatusReplaced" />
          <telerik:AjaxUpdatedControl ControlID="lblStatus" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="RadToolbar1">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="grdSearch" LoadingPanelID="LoadingPanel1" />
          <telerik:AjaxUpdatedControl ControlID="grdItemsforReorder" LoadingPanelID="LoadingPanel1" />
        </UpdatedControls>
      </telerik:AjaxSetting>
    </AjaxSettings>
  </telerik:RadAjaxManager>
  <telerik:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server" InitialDelayTime="500" EnableAjaxSkinRendering="True" BackgroundPosition="Center" Skin="Breg" EnableEmbeddedSkins="False">
    <img src="App_Themes/Breg/images/Common/loading.gif" />
  </telerik:RadAjaxLoadingPanel>
  <script type="text/javascript">
    var grid;

    function GridCreated() {
      grid = this;
    }

    function clickButton(e, buttonid) {
      var evt = e ? e : window.event;
      var bt = document.getElementById(buttonid);
      if (bt) {
        if (evt.keyCode == 13) {
          bt.click();
          return false;
        }
      }
    }
  </script>
  <div class="flex-row subWelcome-bar">
    <h1>Home</h1>
    <div class="recordsDisplayed">
      <telerik:RadNumericTextBox AutoPostBack="true" Culture="English (United States)" ID="txtRecordsDisplayed" MaxValue="100" MinValue="0" NumberFormat-DecimalDigits="0" OnTextChanged="txtRecordsDisplayed_TextChanged" runat="server" ShowSpinButtons="false" Label="Records per Page" />
    </div>
    <div class="flex-grow">&nbsp;</div>
  </div>
  <div class="subTabContent-container home">
    <div class="content">
      <div class="section">
        <div class="actions-bar flex-row">
          <div class="search-container home-search-container">
            <radTlb:RadToolbar AutoPostBack="True" EnableViewState="false" ID="RadToolbar1" OnOnClick="RadToolbar1_OnClick" runat="server" Skin="Breg" EnableEmbeddedStylesheets="False" UseFadeEffect="True">
              <Items>
                <radTlb:RadToolbarTemplateButton ID="rttbFilter" runat="server" Skin="Breg" EnableEmbeddedStylesheets="False">
                  <ButtonTemplate>
                    <telerik:RadTextBox runat="server" ID="tbFilter" SelectionOnFocus="SelectAll" EmptyMessage="Search for Product" />
                  </ButtonTemplate>
                </radTlb:RadToolbarTemplateButton>
                <radTlb:RadToolbarTemplateButton ID="rttbSearchFilter" runat="server" Skin="Breg" EnableEmbeddedStylesheets="False">
                  <ButtonTemplate>
                    <radC:RadComboBox ExpandEffect="fade" CssClass="RadComboBox_Breg" ID="cboSearchFilter" runat="server" Skin="Breg" EnableEmbeddedStylesheets="False">
                      <Items>
                        <radC:RadComboBoxItem ID="RadComboBoxItem3" runat="server" Text="Product Code" Value="Code" />
                      </Items>
                      <Items>
                        <radC:RadComboBoxItem ID="RadComboBoxItem5" runat="server" Text="Product Name" Value="Name" />
                      </Items>
                      <Items>
                        <radC:RadComboBoxItem ID="RadComboBoxItem2" runat="server" Text="Manufacturer" Value="Brand" />
                      </Items>
                      <Items>
                        <radC:RadComboBoxItem ID="RadComboBoxItem1" runat="server" Text="HCPCs" Value="HCPCs" />
                      </Items>
                    </radC:RadComboBox>
                  </ButtonTemplate>
                </radTlb:RadToolbarTemplateButton>
                <radTlb:RadToolbarToggleButton ButtonText="Search" CausesValidation="true" CommandName="Search" Hidden="False" ID="rtbFilter" runat="server" ToolTip="Search" CssClass="search-button" DisplayType="ImageOnly" ButtonImage="App_Themes/Breg/images/Common/search.png" />
                <radTlb:RadToolbarTemplateButton runat="server">
                  <ButtonTemplate>
                    <bvu:Help HelpID="21" ID="help21" runat="server" />
                  </ButtonTemplate>
                </radTlb:RadToolbarTemplateButton>
              </Items>
            </radTlb:RadToolbar>
          </div>

          <div class="addToCart-container">
            <bvu:Help HelpID="64" ID="Help3" runat="server" />
            <asp:Button CssClass="RadButton_Breg rbButton rbPrimaryButton" Enabled="false" ID="btnAddItemsSearch" OnClick="btnAddItemsSearch_Click" runat="server" Text="Add to Cart" />
          </div>
        </div>
        <div>
			<table>
				<tr>
					<td>
						<asp:Label CssClass="WarningMsgReplacement" ID="lblStatusReplaced2" OnPreRender="LabelPreRenderHideShow" runat="server" Text=" "></asp:Label>
					</td>
					<td>
						<asp:Label CssClass="WarningMsgReplacementText" ID="lblStatusReplacedText2" OnPreRender="LabelPreRenderHideShow" runat="server"></asp:Label>
					</td>
				</tr>
			</table>
            <asp:Label CssClass="WarningMsg" ID="lblStatus2" OnPreRender="LabelPreRenderHideShow" runat="server"></asp:Label>
            <asp:Label CssClass="WarningMsg" ID="lblBrowseDispense" OnPreRender="LabelPreRenderHideShow" runat="server"></asp:Label>
          </div>
        <%-- SEARCH RESULTS GRID (hidden by default) --%>
        <telerik:RadGrid AllowMultiRowSelection="True" AllowPaging="True" AutoGenerateColumns="false" EnableViewState="true" GridLines="None" ID="grdSearch" OnNeedDataSource="grdSearch_NeedDataSource" runat="server" Visible="false" Width="100%" OnItemDataBound="grdSearch_OnItemDataBound">
          <PagerStyle AlwaysVisible="true" Mode="NumericPages" />
          <MasterTableView>
            <Columns>
              <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" />
              <telerik:GridBoundColumn DataField="ProductInventoryID" Display="False" UniqueName="ProductInventoryID" />
              <telerik:GridBoundColumn DataField="SupplierName" HeaderText="Supplier" UniqueName="SupplierName" />
              <telerik:GridBoundColumn DataField="BrandName" HeaderText="Brand" UniqueName="BrandName" />
              <telerik:GridBoundColumn DataField="IsShoppingCart" Visible="False" />
              <telerik:GridBoundColumn DataField="NewProductName" Visible="False" HeaderText="NewProductName" UniqueName="NewProductName" />
              <telerik:GridBoundColumn DataField="NewProductCode" Visible="False" HeaderText="NewProductCode" UniqueName="NewProductCode" />
              <telerik:GridBoundColumn DataField="Priority" UniqueName="Priority" HeaderText="Priority" Visible="False"/>
              <telerik:GridTemplateColumn HeaderStyle-Width="30px" ItemStyle-HorizontalAlign="Center" >
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblPriority">
                      <span class="priorityIcon">
                        <asp:Label ID="prioritySplit" runat="server" Visible='<%# Boolean.Parse(Eval("IsShoppingCart").ToString()) %>' >
                            <div class="priority-split"></div>
                        </asp:Label>           
                      </span>
                    </asp:Label>
                </ItemTemplate>
              </telerik:GridTemplateColumn>
              <telerik:GridBoundColumn DataField="ProductName" HeaderText="Product" UniqueName="ProductName" />
              <telerik:GridBoundColumn DataField="Code" HeaderStyle-Width="50px" HeaderText="Code" UniqueName="Code" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
              <telerik:GridBoundColumn DataField="Side" HeaderStyle-Width="50px" HeaderText="Side" UniqueName="LeftRightSide" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
              <telerik:GridBoundColumn DataField="Size" HeaderStyle-Width="50px" HeaderText="Size" UniqueName="Size" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
              <telerik:GridBoundColumn DataField="PracticeCatalogProductID" Display="false" UniqueName="PracticeCatalogProductID" />
              <telerik:GridBoundColumn DataField="MasterCatalogProductID" Display="false" UniqueName="MasterCatalogProductID" />
              <telerik:GridBoundColumn DataField="NewMasterCatalogProductID" Display="false" UniqueName="NewMasterCatalogProductID" />
              <telerik:GridBoundColumn DataField="NewThirdPartyProductID" Display="false" UniqueName="NewThirdPartyProductID" />
              <telerik:GridBoundColumn DataField="NewPracticeCatalogProductID" Display="false" UniqueName="NewPracticeCatalogProductID" />
              <telerik:GridBoundColumn DataField="QuantityOnHand" HeaderStyle-Width="50px" HeaderText="On Hand" UniqueName="QuantityOnHand" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
              <telerik:GridTemplateColumn DataField="SuggestedReorderLevel"  HeaderStyle-Width="66px" ItemStyle-Width="66px"  HeaderText="Sugg. Order #" UniqueName="SuggestedReorderLevel" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <div style="width: 66px; overflow: visible;">
                        <telerik:RadNumericTextBox Culture="English (United States)" ID="txtSuggReorderLevel" LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" NumberFormat-DecimalDigits="0" runat="server" CssClass="suggestedOrderTextBox" ShowSpinButtons="True" Value="1">
                            <NumberFormat DecimalDigits="0"/>
                        </telerik:RadNumericTextBox>
                    </div>
                </ItemTemplate>
              </telerik:GridTemplateColumn>
            </Columns>
          </MasterTableView>
          <ClientSettings AllowColumnsReorder="false" AllowDragToGroup="false" ReorderColumnsOnClient="false">
            <Scrolling AllowScroll="false" />
            <Selecting AllowRowSelect="True" />
            <ClientEvents OnGridCreated="GridCreated"></ClientEvents>
          </ClientSettings>
        </telerik:RadGrid>
      </div>
      <div class="section">
        <div class="flex-row">
          <h2>Inventory Order Status</h2>
          <div class="legend">
            <div class="priority-1">
              <span class="priorityIcon"></span>
              <span>Critical</span>
            </div>
            <div class="priority-2">
              <span class="priorityIcon"></span>
              <span>Reorder</span>
            </div>
            <div class="shoppingCart">
              <span class="priorityIcon"></span>
              <span>In Cart</span>
            </div>
            <div class="priority-3">
              <span class="priorityIcon"></span>
              <span>On Order</span>
            </div>
          </div>
          <div class="addToCart-container">
            <bvu:Help HelpID="71" ID="ctlHelp1" runat="server" />

            <asp:Button CssClass="RadButton_Breg rbButton rbPrimaryButton" ID="btnAddItemsReOrderGrid" OnClick="btnAddItemsReOrderGrid_Click" runat="server" Text="Add to Cart" />
          </div>
        </div>
        <div>
			<table>
				<tr>
					<td>
						<asp:Label CssClass="WarningMsgReplacement" ID="lblStatusReplaced" OnPreRender="LabelPreRenderHideShow" runat="server" Text=" "></asp:Label>
					</td>
					<td>
						<asp:Label CssClass="WarningMsgReplacementText" ID="lblStatusReplacedText" OnPreRender="LabelPreRenderHideShow" runat="server"></asp:Label>
					</td>
				</tr>
			</table>
          <asp:Label CssClass="WarningMsg" ID="lblStatus" OnPreRender="LabelPreRenderHideShow" runat="server"></asp:Label>
        </div>
        <%-- INVENTORY ORDER STATUS (Main Grid) --%>
        <radG:RadGrid AllowAutomaticUpdates="True" AllowMultiRowSelection="True" AllowPaging="True" AutoGenerateColumns="False" EnableViewState="True" ID="grdItemsforReorder" runat="server" Skin="Breg" EnableEmbeddedStylesheets="False" OnNeedDataSource="grdItemsforReorder_NeedDataSource" OnItemDataBound="grdItemsforReorder_ItemDataBound">
          <PagerStyle AlwaysVisible="true" Mode="NumericPages" />
          <MasterTableView CssClass="RadGrid_Breg">
            <Columns>
              <radG:GridClientSelectColumn HeaderStyle-Width="20px" UniqueName="ClientSelectColumn" />
              <radG:GridBoundColumn DataField="IsShoppingCart" Visible="False" />
              <radG:GridBoundColumn DataField="NewPracticeCatalogProductID" Visible="False" />
              <radG:GridBoundColumn DataField="NewProductName" Visible="False" />
              <radG:GridBoundColumn DataField="NewProductCode" Visible="False" />
              <radG:GridBoundColumn DataField="SupplierName" HeaderStyle-Width="280px" HeaderText="Supplier" UniqueName="SupplierName"></radG:GridBoundColumn>
              <radG:GridBoundColumn DataField="PriorityWithCart" UniqueName="PriorityWithCart" Visible="False" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
              </radG:GridBoundColumn>
              <radG:GridTemplateColumn HeaderStyle-Width="30px" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                  <span class="priorityIcon">
                      <asp:Label ID="prioritySplit" runat="server" Visible='<%# Boolean.Parse(Eval("IsShoppingCart").ToString()) %>' >
                        <div class="priority-split"></div>
                    </asp:Label>           
                  </span>
                </ItemTemplate>
              </radG:GridTemplateColumn>
              <radG:GridBoundColumn DataField="MCName" HeaderStyle-Width="375px" HeaderText="Product" UniqueName="MCName"></radG:GridBoundColumn>
              <radG:GridBoundColumn DataField="Code" HeaderStyle-Width="50" HeaderText="Code" ItemStyle-Width="50" UniqueName="Code" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
              <radG:GridBoundColumn DataField="LeftRightSide" HeaderStyle-Width="50" HeaderText="Side" UniqueName="LeftRightSide" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></radG:GridBoundColumn>
              <radG:GridBoundColumn DataField="Size" HeaderStyle-Width="50" HeaderText="Size" UniqueName="Size" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></radG:GridBoundColumn>
                <radG:GridBoundColumn DataField="QuantityOnHand" HeaderText="On Hand" UniqueName="QuantityOnHand"
                            HeaderStyle-Wrap="true" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="50px"
                            HeaderStyle-HorizontalAlign="Center">
                        </radG:GridBoundColumn>
              <radG:GridTemplateColumn DataField="QuantityOnOrder" HeaderStyle-Width="50" UniqueName="QuantityOnOrder" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                <HeaderTemplate>
                  On&nbsp;Order
            <bvu:Help HelpID="66" ID="help66" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                  <asp:Label ID="labQuantityOnOrder" runat="server" Text='<%# Eval("QuantityOnOrder") %>' />
                </ItemTemplate>
              </radG:GridTemplateColumn>
              <radG:GridTemplateColumn DataField="ParLevel" HeaderStyle-Width="50px" UniqueName="ParLevel" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                <HeaderTemplate>
                  Par
            <bvu:Help HelpID="67" ID="help67" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                  <asp:Label ID="labParLevel" runat="server" Text='<%# Eval("ParLevel") %>' />
                </ItemTemplate>
              </radG:GridTemplateColumn>
              <radG:GridTemplateColumn DataField="ReorderLevel" HeaderStyle-Width="50px" UniqueName="ReorderLevel" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                <HeaderTemplate>
                  Reorder
            <bvu:Help HelpID="68" ID="help68" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                  <asp:Label ID="labReorderLevel" runat="server" Text='<%# Eval("ReorderLevel") %>' />
                </ItemTemplate>
              </radG:GridTemplateColumn>
              <radG:GridTemplateColumn DataField="CriticalLevel" HeaderStyle-Width="50px" UniqueName="CriticalLevel" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                <HeaderTemplate>
                  Critical
            <bvu:Help HelpID="69" ID="help69" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                  <asp:Label ID="labCriticalLevel" runat="server" Text='<%# Eval("CriticalLevel") %>' />
                </ItemTemplate>
              </radG:GridTemplateColumn>
              
              <radG:GridTemplateColumn DataField="SuggestedReorderLevel" HeaderText="Sugg. Order #" HeaderStyle-Width="85px" ItemStyle-Width="85px" UniqueName="SuggestedReorderLevel" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" >
                <ItemTemplate>
                    <div style="width: 66px; overflow: visible;">
                  <telerik:RadNumericTextBox Culture="English (United States)" DbValue='<%# Eval("SuggestedReorderLevel") %>' ID="txtSuggReorderLevel" MaxValue="999" MinValue="0" CssClass="suggestedOrderTextBox"    ShowSpinButtons="True" NumberFormat-DecimalDigits="0" runat="server">
                    <NumberFormat DecimalDigits="0" />
                  </telerik:RadNumericTextBox>
                        </div>
                </ItemTemplate>
              </radG:GridTemplateColumn>
                   
              <radG:GridTemplateColumn DataField="StockingUnits" HeaderStyle-Width="50px" UniqueName="StockingUnits" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                <HeaderTemplate>
                  <span>SU</span>
                  <bvu:Help HelpID="72" ID="help72" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                  <asp:Label ID="labStockingUnits" runat="server" Text='<%# Eval("StockingUnits") %>' />
                </ItemTemplate>
              </radG:GridTemplateColumn>
              <radG:GridBoundColumn DataField="practicecatalogproductid" UniqueName="practicecatalogproductid"
                Display="False">
              </radG:GridBoundColumn>
            </Columns>
          </MasterTableView>
          <ClientSettings AllowColumnsReorder="false" AllowDragToGroup="false" ReorderColumnsOnClient="false">
            <Selecting AllowRowSelect="True" />
            <ClientEvents OnGridCreated="GridCreated"></ClientEvents>
            <Scrolling AllowScroll="False" UseStaticHeaders="True"/>
          </ClientSettings>
        </radG:RadGrid>
      </div>
      <div class="section">
        <h2>Last 20 Items Dispensed</h2>
        <telerik:RadGrid AllowPaging="True" AutoGenerateColumns="False" EnableViewState="true" ID="grdLast10ItemsDispensed" OnNeedDataSource="grdLast10ItemsDispensed_NeedDataSource" runat="server" Width="100%">
            <ClientSettings>
                <Scrolling AllowScroll="False" UseStaticHeaders="True"/>
            </ClientSettings>
           <PagerStyle AlwaysVisible="True" Mode="NumericPages" />
          <MasterTableView>
            <Columns>
              <telerik:GridBoundColumn DataField="SupplierName" HeaderText="Supplier" UniqueName="SupplierName" />
              <telerik:GridBoundColumn DataField="Name" HeaderText="Product" UniqueName="Name" />
              <telerik:GridBoundColumn DataField="Code" HeaderStyle-Width="60px" HeaderText="Code" UniqueName="Code" ItemStyle-Wrap="False" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
              <telerik:GridBoundColumn DataField="PhysicianName" HeaderText="Provider" UniqueName="PhysicianName" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
              <telerik:GridBoundColumn DataField="PatientCode" HeaderStyle-Width="70px" HeaderText="Patient Code" UniqueName="PatientCode" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
              <telerik:GridTemplateColumn HeaderText="Patient Name" UniqueName="PatientFirstName" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                <ItemTemplate>
                  <%# Eval("PatientFirstName") + " " + Eval("PatientLastName") %>
                </ItemTemplate>
              </telerik:GridTemplateColumn>
              <telerik:GridBoundColumn DataField="Side" HeaderStyle-Width="40px" HeaderText="Side" UniqueName="Side" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
              <telerik:GridTemplateColumn HeaderText="Modifiers" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                <ItemTemplate>
                  <%# Eval("Mod1") + " " +  Eval("Mod2") + " " + Eval("Mod3") %>
                </ItemTemplate>
              </telerik:GridTemplateColumn>
              <telerik:GridBoundColumn DataField="Size" HeaderStyle-Width="50px" HeaderText="Size" UniqueName="Size" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
              <telerik:GridBoundColumn DataField="Quantity" HeaderStyle-Width="40px" HeaderText="Qty" UniqueName="Quantity" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
              <telerik:GridBoundColumn DataField="Payer" HeaderText="Payer" UniqueName="Payer" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
              <telerik:GridBoundColumn DataField="DMEDeposit" DataFormatString="{0:$###,##0.00}" EmptyDataText="&nbsp;" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="70" HeaderText="DME Deposit" UniqueName="Total" />
              <telerik:GridBoundColumn DataField="WholesaleCost" DataFormatString="{0:$###,##0.00}" EmptyDataText="&nbsp;" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="70" HeaderText="Unit Cost" UniqueName="Total" />
              <telerik:GridBoundColumn DataField="Total" DataFormatString="{0:$###,##0.00}" EmptyDataText="&nbsp;" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="70" HeaderText="Billing Cost" UniqueName="Total"  />
              <telerik:GridBoundColumn DataField="DateDispensed" HeaderStyle-Width="80px" HeaderText="Date Dispensed" UniqueName="DateDispensed" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
            </Columns>
          </MasterTableView>
        </telerik:RadGrid>
      </div>
      <div class="section">
        <h2>Custom Braces to be Dispensed</h2>
        <telerik:RadGrid AllowPaging="true" AutoGenerateColumns="False" EnableViewState="true" ID="grdCustomBracesToBeDispensed" OnNeedDataSource="grdCustomBracesToBeDispensed_NeedDataSource" runat="server" Width="100%">
          <PagerStyle AlwaysVisible="True" Mode="NumericPages" />
            <ClientSettings>
                <Scrolling AllowScroll="False" UseStaticHeaders="True"/>
            </ClientSettings>
          <MasterTableView>
            <Columns>
              <telerik:GridBoundColumn DataField="SupplierName" HeaderText="Supplier" UniqueName="SupplierName" />
              <telerik:GridBoundColumn DataField="ProductName" HeaderText="Product" UniqueName="ProductName" />
              <telerik:GridBoundColumn DataField="Code" HeaderStyle-Width="60px" HeaderText="Code" UniqueName="Code" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
              <telerik:GridBoundColumn DataField="PurchaseOrder" HeaderStyle-Width="80px" HeaderText="PO Number" UniqueName="PurchaseOrder" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
              <telerik:GridBoundColumn DataField="CustomPurchaseOrderCode" HeaderStyle-Width="80px" HeaderText="Custom PO Number" UniqueName="CustomPurchaseOrderCode" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
              <telerik:GridBoundColumn DataField="PatientCode" HeaderStyle-Width="80px" HeaderText="Patient ID" UniqueName="PatientCode" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
              <telerik:GridTemplateColumn HeaderStyle-Width="90px" HeaderText="Provider" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                <ItemTemplate>
                  <%# Eval("FirstName") + " " + Eval("LastName") %>
                </ItemTemplate>
              </telerik:GridTemplateColumn>
              <telerik:GridBoundColumn DataField="WholesaleCost" DataFormatString="{0:$###,##0.00}" EmptyDataText="&nbsp;" HeaderStyle-Width="70px" HeaderText="Unit Cost" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" UniqueName="WholesaleCost" />
              <telerik:GridBoundColumn DataField="BillingCost" DataFormatString="{0:$###,##0.00}" EmptyDataText="&nbsp;" HeaderStyle-Width="70px" HeaderText="Billing Cost" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" UniqueName="BillingTotal" />
              <telerik:GridBoundColumn DataField="DateCheckedIn" HeaderStyle-Width="140px" HeaderText="Date Checked In" UniqueName="DateCheckedIn" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
            </Columns>
          </MasterTableView>
        </telerik:RadGrid>
      </div>
    </div>
  </div>
</asp:Content>
