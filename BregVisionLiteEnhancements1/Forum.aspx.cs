﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace BregVision
{
    public partial class Forum : System.Web.UI.Page
    {
        public string _initParams = "None";
        public string _thisUser = string.Empty;
        public string _BregAdmin = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
            {
                Response.Redirect("default.aspx");
            }
            else
            {
                int userID = DAL.LoggedInUser.SelectUserID(Context.User.Identity.Name.ToString());
                _thisUser = userID.ToString();
                _BregAdmin = Roles.IsUserInRole(User.Identity.Name, "BregAdmin").ToString();

                if (!Page.IsPostBack)
                {
                    if (Request.QueryString["ArticleId"] != null)
                    {
                        _initParams = "ArticleId=" + Request.QueryString["ArticleId"].ToString()
                            + ", LoggedInUser=" + _thisUser
                            + ", BregAdmin=" + _BregAdmin;

                    }
                    else if (Request.QueryString["UserId"] != null)
                    {
                        _initParams = "UserId=" + Request.QueryString["UserId"].ToString()
                            + ", UserName=" + Request.QueryString["UserName"].ToString()
                            + ", LoggedInUser=" + _thisUser
                            + ", BregAdmin=" + _BregAdmin;
                    }
                    else
                    {
                        _initParams = "LoggedInUser=" + _thisUser
                        + ", BregAdmin=" + _BregAdmin;
                    }
                }
            }
        }

        
    }
}
