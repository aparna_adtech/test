using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.WebControls;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Linq;

namespace BregVision
{
    public partial class POShipBill : Bases.PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // hide location drop down from masterpage because it's not valid here
                {
                    var m = this.Master as BregVision.MasterPages.SiteMaster;
                    if (m != null)
                        m.SetLocationDropdownVisibility(false);
                }
                // load billing and shipping information for display
                PopulateBillingAddress(practiceLocationID);
                PopulateShippingAddress(practiceLocationID);
                RestoreSavedShippingMethod();
            }
        }

        private void PopulateBillingAddress(int practiceLocationID)
        {
            var db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            var dbCommand = db.GetStoredProcCommand("dbo.USP_GetBillingAddressByPracticeLocation");
            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.String, practiceLocationID);

            using (var reader = db.ExecuteReader(dbCommand))
            {
                if (reader.Read())
                {
                    BillAttention.Text = reader["BillAttentionOf"].ToString();
                    BillAddress1.Text = reader["BillAddress1"].ToString();
                    BillAddress2.Text = reader["BillAddress2"].ToString();
                    BillCity.Text = reader["BillCity"].ToString();
                    BillState.Text = reader["BillState"].ToString();
                    BillZipCode.Text = reader["BillZipCode"].ToString();
                }
            }
        }

        private void PopulateShippingAddress(int practiceLocationID)
        {
            var db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            var dbCommand = db.GetStoredProcCommand("dbo.USP_GetShippingAddressByPracticeLocation");
            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.String, practiceLocationID);

            using (var reader = db.ExecuteReader(dbCommand))
            {
                if (reader.Read())
                {
                    ShipAttention.Text = reader["ShipAttentionOf"].ToString();
                    ShipAddress1.Text = reader["ShipAddress1"].ToString();
                    ShipAddress2.Text = reader["ShipAddress2"].ToString();
                    ShipCity.Text = reader["ShipCity"].ToString();
                    ShipState.Text = reader["ShipState"].ToString();
                    ShipZipCode.Text = reader["ShipZipCode"].ToString();
                }
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            var shippingType = Convert.ToInt32(radShippingType.SelectedValue);
            Session["ShippingTypeID"] = shippingType; 
            DAL.PracticeLocation.ShoppingCart.UpdateShippingType(practiceLocationID, shippingType);
            Response.Redirect("InvoiceSnapshot.aspx");
        }

        private void RestoreSavedShippingMethod()
        {
            var shippingTypeId = DAL.PracticeLocation.ShoppingCart.GetShippingType(practiceLocationID);
            if (shippingTypeId > 0)
            {
                var item = radShippingType.Items.FindByValue(shippingTypeId.ToString());
                if (item != null)
                    radShippingType.SelectedValue = item.Value;
            }
        }
    }
}
