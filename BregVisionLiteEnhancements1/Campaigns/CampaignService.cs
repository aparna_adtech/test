﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ClassLibrary.DAL;
using System.Data.Linq;
using System.Data;
using System.Text;
using System.Configuration;

namespace BregVision.Campaigns
{
    public class CampaignService
    {
        private static List<ClassLibrary.DAL.Campaign> GetAvailableCampaigns(System.Web.SessionState.HttpSessionState Session)
        {
            List<ClassLibrary.DAL.Campaign> campaigns = null;

            if (Session["Campaigns"] != null)
            {
                campaigns = (List<ClassLibrary.DAL.Campaign>)Session["Campaigns"];
            }
            else
            {
                VisionDataContext ctx = VisionDataContext.GetVisionDataContext();

                using (ctx)
                {
                    DataLoadOptions loadOptions = new DataLoadOptions();
                    loadOptions.LoadWith<ClassLibrary.DAL.Campaign>(c => c.AdMedia);
                    loadOptions.LoadWith<ClassLibrary.DAL.AdMedia>(m => m.AdMediaType);
                    loadOptions.LoadWith<ClassLibrary.DAL.AdMedia>(m => m.AdLocation);

                    ctx.LoadOptions = loadOptions;

                    var query = from c in ctx.Campaigns
                                where c.StartDateTime <= DateTime.Now
                                && c.EndDateTime >= DateTime.Now
                                && c.IsActive == true
                                && c.AdMedia.IsActive == true
                                select c;

                    campaigns = query.OrderBy(c => c.StartDateTime).ToList<ClassLibrary.DAL.Campaign>();
                    Session["Campaigns"] = campaigns;
                }
            }

            return campaigns;
        }

        public static CampaignResult GetCampaign(int userId, int adLocationId, System.Web.SessionState.HttpSessionState Session)
        {
            CampaignResult campaignResult = null;

            List<ClassLibrary.DAL.Campaign> campaigns = CampaignService.GetAvailableCampaigns(Session);

            if (campaigns != null)
            {
                ClassLibrary.DAL.CampaignUserState campaignUserState = CampaignService.GetCampaignUserState(userId, adLocationId);
                int? campaignId = null;
                if (campaignUserState != null)
                {
                    campaignId = campaignUserState.CampaignId;
                    TimeSpan ts = DateTime.Now - campaignUserState.ModifiedDateTime;

                    //check to see if the duration for showing this ad has expired 
                    //as the user may just be navigating between pages in the site.
                    foreach (ClassLibrary.DAL.Campaign c in campaigns)
                    {
                        if (c.Id == campaignUserState.CampaignId)
                        {
                            if (c.ImpressionDuration > ts.TotalSeconds)
                            {
                                int impressionDuration = c.ImpressionDuration - (int)ts.TotalSeconds;

                                return new CampaignResult(c, (impressionDuration <= 0) ? c.ImpressionDuration : impressionDuration);
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }

                bool found = false;
                foreach (ClassLibrary.DAL.Campaign c in campaigns)
                {
                    if (found && adLocationId == c.AdMedia.AdLocationId)
                    {
                        campaignResult = new CampaignResult(c,c.ImpressionDuration);
                        break;
                    }
                    if (campaignId == null)
                        found = true;

                    if (campaignId.Equals(c.Id))
                        found = true;
                }

                if (campaignResult == null) //go grab the 1st available in the list for the given location
                {
                    foreach (ClassLibrary.DAL.Campaign c in campaigns)
                    {
                        if (adLocationId == c.AdMedia.AdLocationId)
                        {
                            campaignResult = new CampaignResult(c, c.ImpressionDuration);
                            break;
                        }
                    }
                }

                if (campaignResult != null)
                {
                    UpdateCampaignUserState(userId, campaignResult.Campaign);
                    InsertCampaignImpression(userId, campaignResult.Campaign.Id);
                }
            }

            return campaignResult;
        }

        private static void UpdateCampaignUserState(int userId, ClassLibrary.DAL.Campaign campaign)
        {
            if (userId > 0)
            {
                VisionDataContext ctx = VisionDataContext.GetVisionDataContext();

                using (ctx)
                {
                    ClassLibrary.DAL.CampaignUserState delCampaignUserState = ctx.CampaignUserStates.Where(c => c.UserId == userId && c.AdLocationId == campaign.AdMedia.AdLocationId).FirstOrDefault();

                    if (delCampaignUserState != null)
                    {
                        ctx.CampaignUserStates.DeleteOnSubmit(delCampaignUserState);
                        ctx.SubmitChanges();
                    }

                    ClassLibrary.DAL.CampaignUserState newCampaignUserState = new CampaignUserState();
                    newCampaignUserState.UserId = userId;
                    newCampaignUserState.CampaignId = campaign.Id;
                    newCampaignUserState.AdLocationId = campaign.AdMedia.AdLocationId;
                    newCampaignUserState.ModifiedDateTime = DateTime.Now;

                    ctx.CampaignUserStates.InsertOnSubmit(newCampaignUserState);
                    ctx.SubmitChanges();
                }
            }
        }

        private static void InsertCampaignImpression(int userId, int campaignId)
        {
            if (userId > 0)
            {
                VisionDataContext ctx = VisionDataContext.GetVisionDataContext();

                using (ctx)
                {
                    ClassLibrary.DAL.CampaignImpression campaignImpression = new CampaignImpression();
                    campaignImpression.UserId = userId;
                    campaignImpression.CampaignId = campaignId;
                    campaignImpression.CreatedDateTime = DateTime.Now;

                    ctx.CampaignImpressions.InsertOnSubmit(campaignImpression);
                    ctx.SubmitChanges();
                }
            }
        }

        public static void InsertCampaignClickThrough(int userId, int campaignId)
        {
            VisionDataContext ctx = VisionDataContext.GetVisionDataContext();

            using (ctx)
            {
                ClassLibrary.DAL.CampaignClickThrough campaignClickThrough = new CampaignClickThrough();
                campaignClickThrough.UserId = userId;
                campaignClickThrough.CampaignId = campaignId;
                campaignClickThrough.CreatedDateTime = DateTime.Now;

                ctx.CampaignClickThroughs.InsertOnSubmit(campaignClickThrough);
                ctx.SubmitChanges();
            }
        }

        public static void RequestContact(int userId, string practiceName, int campaignId, string userName, string userPhone, string bestTimeToCall, DataSet productsDataset)
        {
            int impressionCount = 0;
            int clickThroughCount = 0;

            string baseUrl = ConfigurationManager.AppSettings["BaseUrl"];
            
            VisionDataContext ctx = VisionDataContext.GetVisionDataContext();

            DataLoadOptions loadOptions = new DataLoadOptions();
            loadOptions.LoadWith<ClassLibrary.DAL.Campaign>(c => c.AdMedia);
            
            ctx.LoadOptions = loadOptions;

            ClassLibrary.DAL.Campaign campaign = null;
            using (ctx)
            {
                campaign = ctx.Campaigns.Where(c => c.Id == campaignId).FirstOrDefault();
            }

            ctx = VisionDataContext.GetVisionDataContext();
            
            using (ctx)
            {
                var q = (from clicks in ctx.CampaignClickThroughs

                         where clicks.UserId == userId && clicks.CampaignId == campaignId

                         select clicks.CreatedDateTime).Count();
                
                clickThroughCount = q;
            }

            ctx = VisionDataContext.GetVisionDataContext();

            using (ctx)
            {
                var q = (from impressions in ctx.CampaignImpressions

                         where impressions.UserId == userId && impressions.CampaignId == campaignId

                         select impressions.CreatedDateTime).Count();

                impressionCount = q;
            }



            string subject = "Campaign Contact Request";

            StringBuilder body = new StringBuilder();
            body.Append("<html style=\"font-family:Arial;font-size:small\">");

            body.Append("Campaign Name: " + campaign.Name + "<br>");
            body.Append("Campaign Id: " + campaign.Id.ToString() + "<br>");
            body.Append("Campaign Start Date: " + campaign.StartDateTime.ToShortDateString() + "<br>");
            body.Append("Campaign End Date: " + campaign.EndDateTime.ToShortDateString() + "<br>");
            body.Append("Campaign Impression Duration: " + campaign.ImpressionDuration.ToString() + "<br>");

            body.Append("<br>");

            body.Append("Practice Name: " + practiceName + "<br>");
            body.Append("Contact User: " + userName + "<br>");
            body.Append("Contact Phone: " + userPhone + "<br>");
            body.Append("Best Time to Call: " + bestTimeToCall + "<br>");

            body.Append("<br>");
            body.Append("This user has clicked on this ad " + clickThroughCount.ToString() + " times." + "<br>");


            body.Append("<br>");
            TimeSpan ts = new TimeSpan(0, 0, impressionCount * campaign.ImpressionDuration);
            body.Append("This user has seen this ad " + impressionCount.ToString() + " times for a total duration of " + ts.Hours.ToString() + " hours "  + ts.Minutes.ToString() + " minutes " + ts.Seconds.ToString() + " and seconds. " + "<br>");
            
            body.Append("<br>");

            if (productsDataset.Tables[0] != null)
            {
                body.Append("<table width=\"800px\">");

                body.Append("<td>");
                body.Append("<b>Product Name</b>");
                body.Append("</td>");

                body.Append("<td>");
                body.Append("<b>Code</b>");
                body.Append("</td>");

                body.Append("<td>");
                body.Append("<b>Id</b>");
                body.Append("</td>");

                foreach (DataRow dr in productsDataset.Tables[0].Rows)
                {
                    body.Append("<tr>");

                    body.Append("<td>");
                    body.Append((string)dr["ProductName"]);
                    body.Append("</td>");

                    body.Append("<td>");
                    body.Append((string)dr["Code"]);
                    body.Append("</td>");

                    body.Append("<td>");
                    body.Append(((int)dr["MasterCatalogProductID"]).ToString());
                    body.Append("</td>");

                    body.Append("</tr>");
                }

                body.Append("</table>" + "<br>");
            }

            body.Append("<br>");

            body.Append("<img src='" + baseUrl + "/Campaigns/Media/" + campaign.AdMedia.Url + "' />");

            body.Append("</html>");

            FaxEmailPOs.RequestContactEmail(campaign.ContactEmail, body.ToString(), subject);
            
        }

        private static ClassLibrary.DAL.CampaignUserState GetCampaignUserState(int userId, int adLocationId)
        {
            ClassLibrary.DAL.CampaignUserState campaignUserState = null; ;

            VisionDataContext ctx = VisionDataContext.GetVisionDataContext();

            using (ctx)
            {
                campaignUserState = ctx.CampaignUserStates.Where(c => c.UserId == userId && c.AdLocationId == adLocationId).FirstOrDefault();
            }

            return campaignUserState;
        }


    }

    //This needs to match the AdLocation table in the database.  
    //It is a property of the BannerAd UserControl.
    public enum AdLocation
    {
        HorizontalBannerAd = 1,
        VerticalBannerAd,
        LargeHorizontalFeaturedProductsAd
    }

    public class CampaignResult
    {
        Campaign _Campaign;
        public Campaign Campaign
        {
            get { return _Campaign; }
            set { _Campaign = value; }
        }

        int _ImpressionDuration;
        public int ImpressionDuration
        {
            get { return _ImpressionDuration; }
            set { _ImpressionDuration = value; }
        }

        public CampaignResult(Campaign campaign, int impressionDuration)
        {
            Campaign = campaign;
            ImpressionDuration = impressionDuration;
        }
    }
}
