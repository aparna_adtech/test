﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Text;
using Telerik.WebControls;

namespace BregVision.Campaigns.UserControls
{
    public partial class BannerAd : Bases.UserControlBase
    {
        public Timer UpdateTimer
        {
            get
            {
                return updateTimer;
            }
        }

        public AdLocation AdLocation
        {
            get { return (AdLocation)Enum.Parse(typeof(AdLocation), labAdLocation.Text); }
            set { labAdLocation.Text = value.ToString(); }
        }
        
        public int CampaignId
        {
            get { return int.Parse(labCampaignId.Text); }
            set { labCampaignId.Text = value.ToString(); }
        }

        public string Url
        {
            get { return labUrl.Text; }
            set 
            {
                if (value == null)
                {
                    labUrl.Text = string.Empty;
                }
                else
                {
                    labUrl.Text = value.ToString();
                }
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Session["BannerUpdate"] = Server.UrlEncode(System.DateTime.Now.ToString());
            }

            InitializeAjaxSettings();
        }

        protected void updateTimer_Tick(object sender, EventArgs e)
        {
            LoadAd();
        }

        private void InitializeAjaxSettings()
        {
            Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);
            if (radAjaxManager != null)
            {

                radAjaxManager.AjaxSettings.AddAjaxSetting(updateTimer, adImage);
                radAjaxManager.AjaxSettings.AddAjaxSetting(adImage, radWindowBannerAd);
                radAjaxManager.AjaxSettings.AddAjaxSetting(adImage, radWindowUrl);
                radAjaxManager.AjaxSettings.AddAjaxSetting(btnShowItems, radWindowUrl);


            }
            else
            {
                updateTimer.Enabled = false;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            ViewState["BannerUpdate"] = Session["BannerUpdate"];

            if (!Page.IsPostBack)
            {
                //adImage.OnClientClick = String.Format("return showAdWindow('{0}')", radWindowBannerAd.ClientID);
                //radWindowUrl.OnClientClose = String.Format("return hideAdWindowUrl('{0}')", radWindowUrl.ClientID);
                                
                LoadAd();
            }
        }

        
        protected void LoadAd()
        {
            if (Session["UserID"] != null)
            {
                int userId = (int)Session["UserID"];

                CampaignResult campaignresult = CampaignService.GetCampaign(userId, (int)this.AdLocation, Page.Session);

                if (campaignresult != null) //occurs if there are no campaigns available to show.  We should not allow this to happen in the DB.
                {
                    CampaignId = campaignresult.Campaign.Id;
                    Url = campaignresult.Campaign.Url;

                    adImage.ImageUrl = "~/Campaigns/Media/" + campaignresult.Campaign.AdMedia.Url;

                    updateTimer.Interval = campaignresult.ImpressionDuration * 1000;

                    radWindowUrl.VisibleOnPageLoad = false;
                }
            }
            else
            {
                //breg admins
                adImage.Visible = false;
            }
        }

        protected void btnShowItems_Click(object sender, EventArgs e)
        {
            if (ViewState["BannerUpdate"].ToString() == Session["BannerUpdate"].ToString())
            { 
                int userId = (int)Session["UserID"];
                CampaignService.InsertCampaignClickThrough(userId, CampaignId);

                if(Url.Length > 0 )
                {
                    radWindowUrl.NavigateUrl = Url;
                    radWindowUrl.VisibleOnPageLoad = true;
                
                
                }
                else
                {
                    ShowAdWindow(false); 
                    string qhref = BLL.Website.GetAbsPath( "~/Home.aspx?CampaignId=" + CampaignId);
                    string href = string.Format("location.href='{0}';", qhref);

                    //((Home)this.Page).HomeAjaxManager1.ResponseScripts.Add(href);
                }

                Session["BannerUpdate"] = Server.UrlEncode(System.DateTime.Now.ToString());
            }
        }

        protected void btnRequestMoreInfo_Click(object sender, EventArgs e)
        {
            if (ViewState["BannerUpdate"] == Session["BannerUpdate"])
            {
                ShowAdWindow(false);
                Session["BannerUpdate"] = Server.UrlEncode(System.DateTime.Now.ToString());
            }
        }

        private void ShowAdWindow(bool isVisible)
        {
            radWindowBannerAd.VisibleOnPageLoad = isVisible;
        }

        
    }
}