﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BannerAd.ascx.cs" Inherits="BregVision.Campaigns.UserControls.BannerAd" %>


<asp:Timer ID="updateTimer" runat="server" OnTick="updateTimer_Tick" Interval="60000"></asp:Timer> 

<telerik:RadWindow ID="radWindowBannerAd" runat="server" Modal="true" Enabled="true" 
            IconUrl="~/Images/info16.png"
            AnimationDuration="0"
            Animation="None" VisibleOnPageLoad="false"
            Skin="Web20"
            Height="125px" Width="250px" 
            Behaviors="Move,Close"  
            Title="Ad Info" 
            VisibleStatusbar="false">
            <ContentTemplate>
                <div align="center" style="padding:10px">
                <asp:Button runat="server" ID="btnShowItems" Text="Show Item(s)" OnClick="btnShowItems_Click" Width="200" />
                </br>
                </br>
                <asp:Button runat="server" ID="btnRequestMoreInfo" Text="Request More Info" OnClick="btnRequestMoreInfo_Click" Width="200" />
                </div>
            </ContentTemplate>
</telerik:RadWindow>

<telerik:RadWindow  ID="radWindowUrl" KeepInScreenBounds="true" AutoSize="false" runat="server" Modal="true" Enabled="true" 
            IconUrl="~/Images/info16.png"
            AnimationDuration="0"
            Animation="None" VisibleOnPageLoad="false"
            Skin="Web20"
            Height="600px" Width="600px" 
            Behaviors="Maximize,Move,Close,Resize"  
            Title="Ad Info" 
            VisibleStatusbar="false">
</telerik:RadWindow>


<asp:ImageButton runat="server" ID="adImage" BorderStyle="None" OnClick="btnShowItems_Click" />

<asp:Label runat="server" ID="labCampaignId" Visible="false" />
<asp:Label runat="server" ID="labUrl" Visible="false" />
<asp:Label runat="server" ID="labAdLocation" Visible="false" />

<telerik:RadCodeBlock ID="RadCodeBlockAdWindow" runat="server">
    <script type="text/javascript">
        //<![CDATA[
        function showAdWindow(clientID) {
            var radWindow = $find(clientID);
            radWindow.show();
            return false;
        }

        //]]>
    </script>
</telerik:RadCodeBlock>


