using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.WebControls;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common; 

namespace BregVision
{
    public partial class Fusion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
         
            loadStateDropDowns();
            lblDateTime.Text = System.DateTime.Today.ToString();
        }
        protected void loadStateDropDowns()
        {
             
            Database db = DatabaseFactory.CreateDatabase("BregVisionLocalConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.USP_State_select_All");

            DataSet ds = db.ExecuteDataSet(dbCommand);
            
            cbxBillToState.DataSource = ds;
            cbxBillToState.DataValueField = "StateAbbr";
            cbxBillToState.DataTextField = "StateAbbr";
            cbxBillToState.DataBind();

            cbxShipToState.DataSource = ds;
            cbxShipToState.DataValueField = "StateAbbr";
            cbxShipToState.DataTextField = "StateAbbr";
            cbxShipToState.DataBind();
            
            ds.Dispose();
            
            
            
            
        }

        protected void GetCityStateFromZipCode(string zipCode)
        {
            USZIP.USZip USZIP2 = new USZIP.USZip();
            USZIP2.GetInfoByZIP(zipCode);
            //string test = USZIP.GetInfoByZIPCompleted();
           
        
        }


        protected void Button1_Click(object sender, EventArgs e)
        {

            //GetCityStateFromZipCode("92109");
            
            //Validation
            if (txtBillToContact.Text.Length == 0)
            {
                lblStatus.Text = "Contact information must be completed";
                txtBillToContact.Focus();
                return;
            }

            if (txtBillToPhone2.Text.Length == 0)
            {
                lblStatus.Text = "Phone Number must be completed";
                txtBillToPhone2.Focus();
                return;
            }

            //Shipping Method Picked
            if (!radFedEx.Checked || !radUPS.Checked)
            {
                lblStatus.Text = "Shipping type needs to be chosen";
                return;
            }

            
            //Check to see if a brace has been chosen
            if (!IsBracePicked())
            {
                lblStatus.Text = "A brace needs to be chosen.";
                return;
            }
            
            //COlor brace chosen, must make sure color has been chosen
            if (IsColorBracePicked())
            {
                if (!chk03161.Checked && !chk03162.Checked && !chk03163.Checked) //No color enhancment chosen
                {
                    lblStatus.Text = "Color, pattern, combination must be checked when choosing a colored brace.";
                    return;
                }


                //if color or pattern is chosen, must pick respective data
                if (chk03161.Checked)
                {


                    if (IsColorPicked())
                    {
                        //Check if custom brace and pantone color chosen
                        if (chkCustom.Checked)
                        {
                            if (!IsPantoneColorPicked())
                            {
                                lblStatus.Text = "Pantone color must be chosen when choosing a custom colored brace";
                                txtPantone.Focus();
                                return;
                            }
                        }
                    }
                    else //False
                    {
                        lblStatus.Text = "Brace color must be chosen when choosing a colored brace.";
                        return;
                    }

                } //End of chk03161
                if (chk03162.Checked) 
                {
                    if (!IsPatternPicked())
                    {
                        lblStatus.Text = "Brace Pattern must be chosen when choosing a pattern enhancement.";
                        return;
                    }
                }

                if (chk03163.Checked)
                {
                    if (!IsColorPicked() && !IsPatternPicked())
                    {
                        lblStatus.Text = "A custom color and pattern must be chosen when choosing a custom color/pattern enhancement";
                        return;
                    }
                }
            }
            //Redirect if all validation passes
            Response.Redirect("FusionFormProcess.aspx");
        }


        protected Boolean IsColorBracePicked()
        {
            if (chk01215a.Checked || chk01218a.Checked || chk01209a.Checked || chk01216a.Checked
                || chk01214a.Checked || chk01217a.Checked)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        protected Boolean IsBracePicked()
        {
            if (chk01215a.Checked || chk01218a.Checked || chk01209a.Checked || chk01216a.Checked
                || chk01214a.Checked || chk01217a.Checked || chk01200.Checked || chk01205.Checked || chk01207.Checked || chk01208.Checked
                || chk01201.Checked || chk01204.Checked || chk01203.Checked || chk01202.Checked)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        
        protected Boolean IsColorPicked()
        {
            if (chkForest.Checked || chkOrange.Checked || chkNavy.Checked || chkPink.Checked
                || chkRoyal.Checked || chkYellow.Checked || chkSage.Checked || chkRed.Checked || chkCharcoal.Checked || chkCustom.Checked)
            {
                return true;
            }
            else

            {
                return false;
            }
        
        }

        protected Boolean IsPantoneColorPicked()
        {
            
            if (txtPantone.Text.Length == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
                


        }
        
        protected Boolean IsPatternPicked()
        {
            if ( chkFlames.Checked || chkCamouflage.Checked || chkFlag.Checked || chkRipples.Checked
                || chkCustomPattern.Checked)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        protected void chkBilat_CheckedChanged(object sender, EventArgs e)
        {
            if (chkBilat.Checked)
            {
                radLegsSame.Visible = true;
            }
            else
            {
                radLegsSame.Visible = false;
            }
        
        }

    }
}
