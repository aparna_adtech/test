﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Breg Fusion Custom Knee Brace Order Form</title>
</head>
<body>
<form runat=server>
<table width="98%" border="0">
    <tr>
        <td colspan=3>
        FUSION Custom Knee Brace Order Form
        </td>
        <td align=right> 
            
        </td>
        <td>
        </td>
    </tr>
  <tr>
  <tr>
    <td>
        <table  bgcolor=blue  cellpadding=0>
            <tr>
                <td>
                   <!--Bill To Area-->
                   <table width="300" border="0" cellpadding=0  bgcolor=white >
                      <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td bgcolor=LightGrey><label id="BillToHeader" style="font-size:12px">BILL TO</label></td>        
                                    </tr>
                                </table>
                             </td>   
                       </tr>
                        <tr>
                            <td>
                              <table border="0"  bgcolor=white width="100%" >
                                  <tr>
                                    <td width="80px"><label id="Label3" style="font-size:12px" >Customer #</label></td>
                                    <td colspan="2">
                                      <asp:TextBox ID="TextBox1" ToolTip="Enter Customer Number" TextMode="SingleLine" runat="server" Font-Size="12px" Width="90px" />        
       	                            </td>
                                    
                                    <td width="40px"><label id="Label4" style="font-size:12px" >PO #</label></td>
                                    <td colspan="2">
                                      <asp:TextBox ID="TextBox2" ToolTip="Enter PO Number" TextMode="SingleLine" runat="server" Width="90px" />        
                                    </td>
                                  </tr>
                                  <tr>
                                    <td><label style="font-size:12px">Contact</label> </td>
                                    <td colspan="2">
                                        <asp:TextBox ID="txtBillToContact" ToolTip="Enter contact name" TextMode="SingleLine" runat="server" Font-Size="12px" Width="90px" />        
                                    </td>
                                    <td><label style="font-size:12px">Tel #</label></td>
                                    <td  colspan="2">
                                        <asp:TextBox ID="txtBillToPhone" ToolTip="Enter telephone number" TextMode="SingleLine" runat="server" Font-Size="12px" Width="90px" />        
                                    </td>
                                  </tr>
                                  <tr>
                                    <td><label style="font-size:12px">Address</label> </td>
                                    <td colspan="5">
                                        <asp:TextBox ID="txtBillToAddress" ToolTip="Enter address info" TextMode="SingleLine" runat="server" Font-Size="12px" Width="97%" />        
                                    </td>
                                  </tr>

                              </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                              <table bgcolor=white>
                                  <tr>
                                    <td width="22"><label style="font-size:12px">City</label></td>
                                    <td width="123">
                                        <asp:TextBox ID="txtBillToCity" ToolTip="Enter city info" TextMode="SingleLine" runat="server" Font-Size="12px" Width="110px" />        
                                    </td>
                                    <td width="26"><label style="font-size:12px">State</label></td>
                                    <td width="27">
                                        <asp:TextBox ID="txtBillToState" ToolTip="Enter state info" TextMode="SingleLine" runat="server" Font-Size="12px" Width="25px" />        
                                    </td>
                                    <td width="21"><label style="font-size:12px">Zip</label></td>
                                    <td width="56">
                                        <asp:TextBox ID="txtBillToZip" ToolTip="Enter zip code info" TextMode="SingleLine" runat="server" Font-Size="12px" Width="37px" />        
                                    </td>
                                  </tr>
                              </table>
                            
                            
                            </td>
                        </tr>
                  
                   </table>
                 </td>
                 <!--Spacer-->
                 <td width=0>
                 </td>      
                  <td>
                   <!--Bill To Area-->
                   <table width="300" border="0" cellpadding=0  bgcolor=white >
                      <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td bgcolor=LightGrey><label id="Label1" style="font-size:12px">SHIP T0 (if different)</label></td>        
                                    </tr>
                                </table>
                             </td>   
                       </tr>
                        <tr>
                            <td>
                              <table border="0"  bgcolor=white width="100%" >
                                  <tr>
                                    <td width="45"><label style="font-size:12px">Name</label> </td>
                                    <td align=left>
                                        <asp:TextBox ID="txtShipToName" ToolTip="Enter name" TextMode="SingleLine" runat="server" Font-Size="12px" Width="97%" />        
                                    </td>
                                  </tr>

                                  <tr>
                                    <td><label style="font-size:12px">Address</label> </td>
                                    <td>
                                        <asp:TextBox ID="txtShipToAddress1" ToolTip="Enter address info" TextMode="SingleLine" runat="server" Font-Size="12px" Width="97%" />        
                                    </td>
                                  </tr>
                                  <tr>
                                    <td><label style="font-size:12px"></label> </td>
                                    <td>
                                        <asp:TextBox ID="txtShipToAddress2" ToolTip="Enter address info" TextMode="SingleLine" runat="server" Font-Size="12px" Width="97%" />        
                                    </td>
                                  </tr>
                              </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                              <table bgcolor=white>
                                  <tr>
                                    <td width="22"><label style="font-size:12px">City</label></td>
                                    <td width="123">
                                        <asp:TextBox ID="TextBox8" ToolTip="Enter city info" TextMode="SingleLine" runat="server" Font-Size="12px" Width="110px" />        
                                    </td>
                                    <td width="26"><label style="font-size:12px">State</label></td>
                                    <td width="27">
                                        <asp:TextBox ID="TextBox9" ToolTip="Enter state info" TextMode="SingleLine" runat="server" Font-Size="12px" Width="25px" />        
                                    </td>
                                    <td width="21"><label style="font-size:12px">Zip</label></td>
                                    <td width="56">
                                        <asp:TextBox ID="TextBox10" ToolTip="Enter zip code info" TextMode="SingleLine" runat="server" Font-Size="12px" Width="37px" />        
                                    </td>
                                  </tr>
                              </table>
                            
                            
                            </td>
                        </tr>
                  
                   </table>
                 </td>
                 <!--Spacer-->
                 <td width=0>
                 </td>      
                 <td height="100%">
                    <!--For Office Use Only Area-->
                   <table width="177" border="0" cellpadding=0  bgcolor=white >
                      <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td bgcolor=LightGrey><label id="Label2" style="font-size:12px">FOR OFFICE USE ONLY</label></td>        
                                    </tr>
                                </table>
                             </td>   
                       </tr>
                        <tr>
                            <td>
                              <table border="0"  bgcolor=white width="100%" >
                                  <tr>
                                    <td width="45"><label style="font-size:12px">DATE/TIME:</label> </td>
                                    <td align=left>
                                        <asp:TextBox ID="TextBox3" ToolTip="Enter name" TextMode="SingleLine" runat="server" Font-Size="12px" Width="97%" />        
                                    </td>
                                  </tr>

                                  <tr>
                                    <td><label style="font-size:12px">RA #</label> </td>
                                    <td>
                                        <asp:TextBox ID="TextBox4" ToolTip="Enter address info" TextMode="SingleLine" runat="server" Font-Size="12px" Width="97%" />        
                                    </td>
                                  </tr>
                              </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                              <table bgcolor=white>
                                  <tr>
                                    <td>
                                        <asp:CheckBox ID="chkNewAccount" Text="NEW ACCOUNT"/>New Account
                                    </td>
                                    <td>
                                        <asp:CheckBox id="chkHot" Text="HOT" />HOT
                                    </td>

                                  </tr>
                              </table>
                            
                            
                            </td>
                        </tr>
                  
                   </table>
                 </td>
            </tr>
        </table>
                    
                    
   
     
</form>
</body>
</html>
