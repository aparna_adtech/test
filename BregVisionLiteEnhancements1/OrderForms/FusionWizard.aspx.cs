﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace BregVision.OrderForms
{
    public partial class FusionWizard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                PageView4.Enabled = false;
                PageView5.Enabled = false;
                rmpMain.Tabs[3].Enabled = false;
                rmpMain.Tabs[4].Enabled = false;

             
                rmpMain.SelectedIndex = 0;
                pvFusionWebForm.SelectedIndex = 0;
            }
        }

        protected void rmpMain_TabClick(object sender, EventArgs e)
        {

            if (rmpMain.SelectedIndex == 0) //Search Tab
            {

                btnBack.Visible = false;
                btnNext.Visible = true;
                btnNext.Enabled = true;
                lblSteps.Text = "Step 1 of 5:Please enter shipping and billing information";

               
                //    //Load Physician Combo Box
                //    if (grdSearch. == null)
                //    {
                //        grdSearch.DataSource = new Object[] { };    
                //        grdSearch.DataBind(); //Used to show a 'No Records Found' when the page initially loads
                //    }

            }

            //if (rmpMain.SelectedIndex == 1) //Search Tab
            //{

            //    RadAjaxPanel2.EnableAJAX = true;
            //    //    //Load Physician Combo Box
            ////    if (grdSearch. == null)
            ////    {
            ////        grdSearch.DataSource = new Object[] { };    
            ////        grdSearch.DataBind(); //Used to show a 'No Records Found' when the page initially loads
            ////    }

            //}

            if (rmpMain.SelectedIndex == 1) //Physican / Patient Tab
            {
                //Load Physician Combo Box
                //RadWindowManager1.Enabled = false;
                //RadAjaxPanel2.EnableAJAX = false;
                btnBack.Visible = true;
                btnNext.Visible = true;
                btnNext.Enabled = true;
                lblSteps.Text = "Step 2 of 5:Enter Patient information";
                //RadAjaxPanel2.EnableAJAX = true;
                //RadWindowManager1.Enabled = true;
                

            }


            if (rmpMain.SelectedIndex == 2) //Summary tab
            {
                btnBack.Visible = true;
                btnNext.Visible = true;
                btnNext.Enabled = true;
                lblSteps.Text = "Step 3 of 5:Enter left leg Measurements";
                

            }

            if (rmpMain.SelectedIndex == 3) //Summary tab
            {
                btnBack.Visible = true;
                btnNext.Visible = true;
                btnNext.Enabled = true;
                lblSteps.Text = "Step 3 of 5:Enter right leg Measurements";


            }


            if (rmpMain.SelectedIndex == 4) //Summary tab
            {
                btnBack.Visible = true;
                btnNext.Visible = true;
                btnNext.Enabled = true;
                lblSteps.Text = "Step 4 of 5:Add accessories";


            }

            
            
            
            if (rmpMain.SelectedIndex == 5)//Receipt
            {
                lblSteps.Text = "Step 5 of 5:Select shipping method";
                btnNext.Visible = false;

                //Next two line are required to turn off Ajax so that page will pop up and redirect to new web page
                //for printer friendly reciept
                //RadWindowManager1.EnableStandardPopups = true;
                //RadAjaxPanel2.EnableAJAX = false;


            }


        }
        protected void chkShipSameAsBill_CheckedChanged(object sender, EventArgs e)
        {
           
            if (chkShipSameAsBill.Checked)
            {

                txtShipToName.Text = txtBillToContact.Text;
                txtShipToAddress1.Text = txtBillToAddress.Text;
                txtShipToCity.Text = txtBillToCity.Text;
                cbxShipToState.Text = cbxBillToState.Text;
                txtShipToZip.Text = txtBillToZip.Text;

                //spnShipTo.Disabled=true;
                //spnShipTo.Enabled = false;

            }
            else
            {
                //spnShipTo.Disabled=false;
                //spnShipTo.Enabled = true;
            }

        }


        protected void chkBilat_CheckedChanged(object sender, EventArgs e)
        {
           
            if (chkBilat.Checked)
            {
                chkLegsSame.Visible = true;
                chkLegsSame.Checked = false;

                PageView4.Enabled = true;
                PageView5.Enabled = true;
                rmpMain.Tabs[2].Enabled = true;
                rmpMain.Tabs[3].Enabled = true;


            }


        }

        protected void chkLeftLeg_CheckedChanged(object sender, EventArgs e)
        {

            if (chkLeftLeg.Checked)
            {
                Tab4.Text = "Leg Measurements - Left";
                chkLegsSame.Visible = false;
                PageView4.Enabled = true;
                PageView5.Enabled = false;

                rmpMain.Tabs[3].Enabled = true;
                rmpMain.Tabs[4].Enabled = false;

                //chkLegsSame.Visible = true;

                //txtShipToName.Text = txtBillToContact.Text;
                //txtShipToAddress1.Text = txtBillToAddress.Text;
                //txtShipToCity.Text = txtBillToCity.Text;
                //cbxShipToState.Text = cbxBillToState.Text;
                //txtShipToZip.Text = txtBillToZip.Text;

                //spnShipTo.Disabled=true;
                //spnShipTo.Enabled = false;

            }
            else
            {
                //chkLegsSame.Visible = false;
                //spnShipTo.Disabled=false;
                //spnShipTo.Enabled = true;
            }

        }

        protected void chkRightLeg_CheckedChanged(object sender, EventArgs e)
        {

            if (chkRightLeg.Checked)
            {
                Tab4.Text = "Leg Measurements - Left";
                chkLegsSame.Visible = false;
                //rmpMain.SelectedIndex = 2;
                PageView4.Enabled = false;
                PageView5.Enabled = true;

                rmpMain.Tabs[3].Enabled = false;
                rmpMain.Tabs[4].Enabled = true;

                //PageView3.Selected = true;
                
                //rmpMain_TabClick(rmpMain, e);
            
            }
        }

        protected void chkLegsSame_CheckedChanged(object sender, EventArgs e)
        {

            if (chkLegsSame.Checked)
            {
                Tab4.Text = "Both Legs";
                lblLeftLeg.Text = "Left and right legs";
                PageView4.Enabled = true;
                PageView5.Enabled = false;

                rmpMain.Tabs[3].Enabled = true;
                rmpMain.Tabs[4].Enabled = false;

            }
            else
            {
                Tab4.Text = "Leg Measurements - Left";
                lblLeftLeg.Text = "Left Leg";
                PageView4.Enabled = true;
                PageView5.Enabled = true;

                rmpMain.Tabs[3].Enabled = true;
                rmpMain.Tabs[4].Enabled = true;

            }

        }


        protected void chkForestLeft_CheckChanged(object sender, EventArgs e)
        {

           
        }


        protected void chkNoEnhancement_CheckChanged2(object sender, EventArgs e)
        {

            if (chkNoEnhancement2.Checked)
            {
                chkForestLeft.Checked = false;
                
            }

        }

        

 
    
    }

}
