﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FusionWizard.aspx.cs" Inherits="BregVision.OrderForms.FusionWizard" %>

<%@ Register Assembly="RadWindow.Net2" Namespace="Telerik.WebControls" TagPrefix="radW" %>
<%@ Register Assembly="RadAjax.Net2" Namespace="Telerik.WebControls" TagPrefix="radA" %>

<%@ Register Assembly="RadInput.Net2" Namespace="Telerik.WebControls" TagPrefix="radI" %>

<%@ Register Assembly="RadComboBox.Net2" Namespace="Telerik.WebControls" TagPrefix="radC" %>

<%@ Register Assembly="RadToolbar.Net2" Namespace="Telerik.WebControls" TagPrefix="radTlb" %>
<%@ Register Assembly="RadGrid.Net2" Namespace="Telerik.WebControls" TagPrefix="radG" %>
<%@ Register Assembly="RadTabStrip.Net2" Namespace="Telerik.WebControls" TagPrefix="radTS" %>
<%@ Register Assembly="RadCalendar.Net2" Namespace="Telerik.WebControls" TagPrefix="radCal" %>
 
 <body>
 <form runat=server>
 <radW:RadWindowManager ID="RadWindowManager1" runat="server">
             </radW:RadWindowManager>
             <radA:RadAjaxPanel ID="RadAjaxPanel2" runat="server" Width="100%" EnableViewState=true>
<table width="96%" border="0">
    

    <tr>
        

        <td colspan="2">
            
 <b>FUSION Custom Knee Brace Order Form</b>

        </td>
        

        <td align="right" colspan="1" style="font-size:x-small">
            

        </td>
        <td align="left" valign="bottom">
            
 


        </td>
        
 


        <td>
        </td>
        
 


    </tr>
    
 


    <tr>
        
 


        <td>
            
 



        </td>
        
 


        <td colspan="3">
            
 


            <table width="100%">
                
 


                <tr>
                    
 
                    


                    <td colspan="1" style="font-size:small">
                        
 
                        


                        <asp:Label ID="lblSteps" runat="server" Font-Size="Small" />
                        
 
                        


                    </td>
                    
 
                    


                    <td align="right" colspan="2">
                        
 
                        


                        <asp:Button ID="btnBack" runat="server" Text="Back" />
                        
 
                        


                        <asp:Button ID="btnNext" runat="server" Text="Next" />
                        

 
                        


                    </td>
                    
 
                    


                </tr>
                

            </table>
            
 


        </td>
        
 


        <td>
            
 


        </td>
        
 


    </tr>
    
 


    <tr>
        
 


        <td>
            
 



        </td>
        
 


        <td colspan="3">
            
 



            <radTS:RadTabStrip ID="rmpMain" runat="server" AutoPostBack="True" 
                MultiPageID="pvFusionWebForm" OnTabClick="rmpMain_TabClick" Skin="Telerik" 
                SkinID="Web20" Width="100%">
        <tabs>
            <radTS:Tab ID="Tab1" runat="server" PageViewID="PageView1" 
                Text="Bill To / Ship To">
            </radTS:Tab>
            <radTS:Tab ID="Tab7" runat="server" PageViewID="PageView7" 
                Text="Shipping Method">
            </radTS:Tab>
            <radTS:Tab ID="Tab3" runat="server" PageViewID="PageView3" 
                Text="Patient Information">
            </radTS:Tab>
            <radTS:Tab ID="Tab4" runat="server" PageViewID="PageView4" 
                Text="Leg Measurements - Left">
            </radTS:Tab>
            <radTS:Tab ID="Tab5" runat="server" PageViewID="PageView5" 
                Text="Leg Measurements - Right">
            </radTS:Tab>
            <radTS:Tab ID="Tab6" runat="server" PageViewID="PageView6" 
                Text="Accessories">
            </radTS:Tab>


        </tabs>
    </radTS:RadTabStrip>
            
 


            <radTS:RadMultiPage ID="pvFusionWebForm" runat="server">
        <radTS:PageView ID="PageView1" runat="server" BackColor="WhiteSmoke">
             
            <table width="98%">
                


                <tr>
                    

                    <td>
                        

                        <table width="50%">
                            

                            <tr>
                                

                                <td>
                                    

                                    <table width="100%">
                                        

                                        <tr>
                                            

                                            <td bgcolor="LightGrey">
                                                <label id="Label2" style="font-size:12px">
                                                BILL TO</label>

                                            </td>
                                            

                                            <td align="right">
                                                

                                                <asp:CheckBox ID="chkShipSameAsBill" runat="server"  oncheckedchanged="chkShipSameAsBill_CheckedChanged" AutoPostBack=true />
                                                

                                                <asp:Label ID="Label5" runat="server" Font-Size="Smaller">Shipping same as Billing</asp:Label>
                                                

                                            </td>
 

                                        </tr>
                                        

                                    </table>
                                    

                                </td>
 

                            </tr>
                            

                            <tr>
                                

                                <td>
                                    

                                    <table bgcolor="white" border="0">
                                        

                                       
                                       
                                       <tr>
                                            

                                            <td width="80px">
                                                <label id="Label7" style="font-size:12px">
                                                Name</label></td>
                                            

                                            <td colspan="5">
                                                
 
                                                <asp:TextBox ID="txtBillToName" runat="server" Font-Size="12px" 
                                                    TextMode="SingleLine" ToolTip="Enter Bill to name" Width="100%" />
 
 	
                                            </td>
                                            
 
                                        </tr>
                                       
                                       
                                       
                                       
                                        <tr>
                                            

                                            <td width="80px">
                                                <label id="Label3" style="font-size:12px">
                                                Customer #</label></td>
                                            

                                            <td colspan="2">
                                                
 
                                                

                                                <asp:TextBox ID="txtCustomerNo" runat="server" Font-Size="12px" 
                                                    TextMode="SingleLine" ToolTip="Enter Customer Number" Width="90px" />
 
 	
                                            </td>
                                            
 
                                            

                                            <td width="40px">
                                                <label id="Label4" style="font-size:12px">
                                                PO #</label></td>
                                            

                                            <td colspan="2">
                                                

                                                <asp:TextBox ID="txtPONo" ToolTip="Enter PO Number" TextMode="SingleLine" runat="server" Width="90px" />         
 
                                                

                                            </td>
                                            

                                        </tr>
                                        

                                        <tr>
                                            

                                            <td>
                                                <label style="font-size:12px">
                                                Contact</label>
                                            </td>
                                            

                                            <td colspan="2">
                                                
 
                                                

                                                <asp:TextBox ID="txtBillToContact" runat="server" Font-Size="12px" 
                                                    TextMode="SingleLine" ToolTip="Enter contact name" Width="90px" />
 
 
                                                

                                            </td>
                                            

                                            <td>
                                                <label style="font-size:12px">
                                                Tel #</label></td>
                                            

                                            <td colspan="2">
                                                

                                                <radI:RadMaskedTextBox ID="txtBillToPhone2" runat="server" 
                                                    Mask="(###) ###-####" skin="Office2007" width="95px"></radI:RadMaskedTextBox>
                                                


                                            </td>
                                            

                                        </tr>
                                        

                                        <tr>
                                            

                                            <td>
                                                <label style="font-size:12px">
                                                Address</label>
                                            </td>
                                            

                                            <td colspan="5">
                                                

                                                <asp:TextBox ID="txtBillToAddress" runat="server" Font-Size="12px" 
                                                    TextMode="SingleLine" ToolTip="Enter address info" Width="97%" />
 

                                            </td>
                                            

                                        </tr>
                                        


                                    </table>
                                    

                                </td>
                                

                            </tr>
                            

                            <tr>
                                

                                <td>
                                    

                                    <table bgcolor="white">
                                        

                                        <tr>
                                            

                                            <td width="22">
                                                <label style="font-size:12px">
                                                City</label></td>
                                            

                                            <td width="123">
                                                

                                                <asp:TextBox ID="txtBillToCity" runat="server" Font-Size="12px" 
                                                    TextMode="SingleLine" ToolTip="Enter city info" Width="110px" />
 

                                            </td>
                                            

                                            <td width="26">
                                                <label style="font-size:12px">
                                                State</label></td>
                                            

                                            <td width="27">
                                                

                                                <radC:RadComboBox ID="cbxBillToState" runat="server" 
                                                    AutoPostBack="false" ExpandEffect="Fade" MarkFirstMatch="true" 
                                                    ShowWhileLoading="False" Skin="WebBlue" 
                                                    SkinsPath="~/RadControls/ComboBox/Skins" ToolTip="Select a state." 
                                                    Visible="true" Width="50px">
                                        </radC:RadComboBox>
                                                

                                            </td>
                                            

                                            <td width="21">
                                                <label style="font-size:12px">
                                                Zip</label></td>
                                            

                                            <td width="56">
                                                

                                                <asp:TextBox ID="txtBillToZip" runat="server" Font-Size="12px" MaxLength="5" 
                                                    TextMode="SingleLine" ToolTip="Enter zip code info" Width="37px" />
 

                                            </td>
                                            

                                        </tr>
                                        

                                    </table>
                                    
 
                                    
 
                                    

                                </td>
                                

                            </tr>
                        </table>
                        

                        <!--Spacer-->

                    </td>
                    

                    <td>
                        

                        <!--Ship To Area-->

                        <span id="spnShipTo" runat=server>

                        <table bgcolor="white" border="0" cellpadding="0" width="50%">
                            

                            <tr>
                                

                                <td>
                                    

                                    <table>
                                        

                                        <tr>
                                            

                                            <td bgcolor="LightGrey">
                                                <label id="Label1" style="font-size:12px">
                                                SHIP T0 (if different)</label>
 
                                                

                                            </td>
 

                                        </tr>
                                        

                                    </table>
                                    

                                </td>
 

                            </tr>
                            

                            <tr>
                                

                                <td>
                                    

                                    <table bgcolor="white" border="0" width="100%">
                                        

                                        <tr>
                                            

                                            <td width="45">
                                                <label style="font-size:12px">
                                                Name</label>
                                            </td>
                                            

                                            <td align="left">
                                                

                                                <asp:TextBox ID="txtShipToName" runat="server" Font-Size="12px" 
                                                    TextMode="SingleLine" ToolTip="Enter name" Width="97%" />
 

                                            </td>
                                            

                                        </tr>
                                        


                                        <tr>
                                            

                                            <td>
                                                <label style="font-size:12px">
                                                Address</label>
                                            </td>
                                            

                                            <td>
                                                

                                                <asp:TextBox ID="txtShipToAddress1" runat="server" Font-Size="12px" 
                                                    TextMode="SingleLine" ToolTip="Enter address info" Width="97%" />
 

                                            </td>
                                            

                                        </tr>
                                        

                                        <tr>
                                            

                                            <td>
                                                <label style="font-size:12px">
                                                </label>
</td>
                                            

                                            <td>
                                                

                                                <asp:TextBox ID="txtShipToAddress2" runat="server" Font-Size="12px" 
                                                    TextMode="SingleLine" ToolTip="Enter address info" Width="97%" />
 

                                            </td>
                                            

                                        </tr>
                                        

                                    </table>
                                    

                                </td>
                                

                            </tr>
                            

                            <tr>
                                

                                <td>
                                    

                                    <table bgcolor="white">
                                        

                                        <tr>
                                            

                                            <td width="22">
                                                <label style="font-size:12px">
                                                City</label></td>
                                            

                                            <td width="123">
                                                

                                                <asp:TextBox ID="txtShipToCity" runat="server" Font-Size="12px" 
                                                    TextMode="SingleLine" ToolTip="Enter city info" Width="110px" />
 

                                            </td>
                                            

                                            <td width="26">
                                                <label style="font-size:12px">
                                                State</label></td>
                                            

                                            <td width="27">
                                                

                                                <radC:RadComboBox ID="cbxShipToState" runat="server" 
                                                    AutoPostBack="false" ExpandEffect="Fade" MarkFirstMatch="true" 
                                                    ShowWhileLoading="False" Skin="WebBlue" 
                                                    SkinsPath="~/RadControls/ComboBox/Skins" ToolTip="Select a state." 
                                                    Visible="true" Width="50px">
                                            </radC:RadComboBox>
                                                


                                            </td>
                                            

                                            <td width="21">
                                                <label style="font-size:12px">
                                                Zip</label></td>
                                            

                                            <td width="56">
                                                

                                                <asp:TextBox ID="txtShipToZip" runat="server" Font-Size="12px" MaxLength="5" 
                                                    TextMode="SingleLine" ToolTip="Enter zip code info" Width="37px" />
 

                                            </td>
                                            

                                        </tr>
                                        

                                    </table>
                                    
 
                                    
 
                                    

                                </td>
                                

                            </tr>
                            
 
                            

                        </table>
                        

                        </span>

                    </td>
                    


                </tr>
                

            </table>
            



        </radTS:PageView>


        <radTS:PageView ID="PageView3" runat="server" BackColor="WhiteSmoke">
            <table width="100%" bgcolor=white  border="0" cellpadding=0>                    
                <tr>
                    <td colspan=5 width=95%>
                        <table>
                             <tr>
                                
                                <td>
                                    <label style="font-size:12px">Patient's Name</label> 
                                </td>
                                <td align=left>
                                    <asp:TextBox ID="txtPatientsName" ToolTip="Enter name" TextMode="SingleLine" runat="server" Font-Size="12px" Width="220px" />        
                                </td>
                             </tr>
                         </table>   
                      </td>   
                </tr>
                <tr>
                    <!--Age - Weight-Height-Sex-->
                    <td colspan=5 width=95%>
                        <table>
                         <tr>
                            <td>
                                <label style="font-size:12px" >Age:</label>
                            </td>
                            <td>    
                                 
                                 <radI:RadNumericTextBox  ID="txtAge" Value="0" MaxValue=999 runat="server" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="50px" NumberFormat-DecimalDigits="0">
                                    <NumberFormat DecimalDigits="0" />
                                </radI:RadNumericTextBox>
                            </td>
                            <td>    
                                 
                                 
                                <label style="font-size:12px">Weight:</label>
                            </td>
                            <td>    

                                 <radI:RadNumericTextBox  ID="txtWeight"  Value="0" MaxValue=999 runat="server" ToolTip="Weight in lbs."  ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default"  MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="50px" NumberFormat-DecimalDigits="0">
                                    <NumberFormat DecimalDigits="0" />
                                </radI:RadNumericTextBox>
                            </td>
                        </tr>
                        <tr>
                            
                            <td>   
                             


                                <label style="font-size:12px">Height:</label>
                            </td>
                            <td>    

                                 <radI:RadNumericTextBox  ID="txtHeightFeet"  Value="0" MaxValue=9 runat="server" ToolTip="Height in Feet" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="50px" NumberFormat-DecimalDigits="0">
                                    <NumberFormat DecimalDigits="0" />
                                </radI:RadNumericTextBox>
                            </td>
                            <td>    

                                 <radI:RadNumericTextBox  ID="txtHeightInches"  Value="0" MaxValue=12 runat="server"  ToolTip="Height in Inches" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default"  MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="50px" NumberFormat-DecimalDigits="0">
                                    <NumberFormat DecimalDigits="0" />
                                </radI:RadNumericTextBox>
                            </td>

                            <td>    


                                <label style="font-size:12px">Sex:</label>
                            </td>
                            <td>    
                                 
                                <radC:RadComboBox ID="cbxSex"  runat="server"  Skin="WebBlue"  
                                     SkinsPath="~/RadControls/ComboBox/Skins" Visible=true Width="70px"
                                     ExpandEffect=Fade
                                     ShowWhileLoading="False" ToolTip="Select male or female."  AutoPostBack=false  MarkFirstMatch=true>
                                     
                                     <Items>
                                        <radC:RadComboBoxItem Text="Male"/>
                                        <radC:RadComboBoxItem Text="Female"/>
                                     </Items>
                                     
                                </radC:RadComboBox>
                            </td>
                         </tr>
                         <tr>
                            <td colspan=2 width="100">
                                <label style="font-size:12px">BRACE FOR:</label>
                            </td>
                            <td colspan=3>

                                <asp:RadioButton  ID='chkLeftLeg' runat=server Text='Left Leg' Font-Size=Small 
                                GroupName=chkLeftRightBoth oncheckedchanged="chkLeftLeg_CheckedChanged" AutoPostBack=true/>
                                <asp:RadioButton  ID='chkRightLeg' runat=server Text='Right Leg' Font-Size=Small 
                                GroupName=chkLeftRightBoth oncheckedchanged="chkRightLeg_CheckedChanged" AutoPostBack=true/>
                                <asp:RadioButton  ID='chkBilat' runat=server Text='Bilat' Font-Size=Small 
                                GroupName=chkLeftRightBoth oncheckedchanged="chkBilat_CheckedChanged" AutoPostBack=true/>
        
             
                            </td>
                         </tr>
                         <tr>
                            <td colspan=2>
                            
                            </td>
                            <td colspan=3>
                                <asp:CheckBox id=chkLegsSame AutoPostBack=true Visible=false Font-Size=Smaller runat=server Text="Both legs the same? (Measurements taken for left leg will be used for both legs)" OnCheckedChanged="chkLegsSame_CheckedChanged" />
                            </td>
                         </tr>
                         <tr>
                            <td colspan=2 width=100>
                                <label style="font-size:12px">INSTABILITY:</label>
                            </td>
                            <td colspan=3>
                                <input type="checkbox" id="radACL" name="Instability" /><label style="font-size:12px">ACL</label>
                                <input type="checkbox" id="radPCL" name="Instability" /><label style="font-size:12px">PCL</label>
                                <input type="checkbox" id="radMCL" name="Instability" /><label style="font-size:12px">MCL/LCL</label>
     
                            </td>
                         </tr>
                         <tr>
                            <td colspan=5>
                                <label style="font-size:12px">*FUSION OA - Medial unloading only.</label>
                            </td>
                         </tr>

                        </table>
                    </td>   
                   </tr>
                  </table>           

 


    
        </radTS:PageView>
        <radTS:PageView ID="PageView4" runat="server" BackColor="WhiteSmoke">
                              <table width="100%">
                                    <tr>
                                        <td colspan=2>
                                            <asp:label id="lblLeftLeg" runat=server Font-Bold=true ForeColor=Blue>Left Leg</asp:label>
                                        </td>
                                        <td colspan=9>
                                            <asp:RadioButton  ID='chkCast' runat=server Text='Cast' Font-Size=Small GroupName=chkLegMeasurement />
                                            <asp:RadioButton  ID='chkReform' runat=server Text='Reform' Font-Size=Small GroupName=chkLegMeasurement />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label style="font-size:12px">1.</label>
                                        </td>
                                        <td>
                                            <label style="font-size:12px">Thigh Circumference</label>
                                        </td>
                                        <td>

                                            <radI:RadNumericTextBox  ID="txtThighCircumference" Value="0"  runat="server" Width=35px ToolTip="Thigh Circumference in Inches" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="32" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" NumberFormat-DecimalDigits="0">
                                                <NumberFormat DecimalDigits="0" />
                                            </radI:RadNumericTextBox>
                                         
                                         </td>
                                        <td>
                                           <radC:RadComboBox ID="radThighCircumferenceInches"  runat="server"  Skin="WebBlue"  
                                                             SkinsPath="~/RadControls/ComboBox/Skins" Visible=true Width="60px"
                                                             ExpandEffect=Fade
                                                             ShowWhileLoading="False" ToolTip="Select Size. All measurements are in inches"  AutoPostBack=false  MarkFirstMatch=true>
                                                             
                                                             <Items>
                                                                <radC:RadComboBoxItem Text="0"/>
                                                                <radC:RadComboBoxItem Text="1/16"/>
                                                                <radC:RadComboBoxItem Text="1/8"/>
                                                                <radC:RadComboBoxItem Text="3/16"/>
                                                                <radC:RadComboBoxItem Text="1/4"/>
                                                                <radC:RadComboBoxItem Text="5/16"/>
                                                                <radC:RadComboBoxItem Text="3/8"/>
                                                                <radC:RadComboBoxItem Text="7/16"/>
                                                                <radC:RadComboBoxItem Text="1/2"/>                                                            
                                                                <radC:RadComboBoxItem Text="9/16"/>
                                                                <radC:RadComboBoxItem Text="5/8"/>
                                                                <radC:RadComboBoxItem Text="11/16"/>
                                                                <radC:RadComboBoxItem Text="3/4"/>     
                                                                <radC:RadComboBoxItem Text="13/16"/>     
                                                                <radC:RadComboBoxItem Text="7/8"/>   
                                                                <radC:RadComboBoxItem Text="15/16"/>   
                                                             </Items>
                                                             
                                            </radC:RadComboBox>
                                            </td>
                                        <td width="8px">&nbsp
                                        </td>
                                        <td>
                                            <label style="font-size:12px">3.</label>
                                        </td>
                                        <td>
                                            <label style="font-size:12px">Knee Offset</label>
                                        </td>
                                        <td>
                                             <radI:RadNumericTextBox  ID="txtKneeOffset"  Value="0" runat="server"   Width=35px ToolTip="Knee Offset in Inches" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="29" MinValue="-29" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" NumberFormat-DecimalDigits="0">
                                                <NumberFormat DecimalDigits="0" />
                                            </radI:RadNumericTextBox>
                                        
                                        </td>
                                        <td>
                                           <radC:RadComboBox ID="radKneeOffset"  runat="server"  Skin="WebBlue"  
                                                             SkinsPath="~/RadControls/ComboBox/Skins" Visible=true Width="60px"
                                                             ExpandEffect=Fade
                                                             ShowWhileLoading="False" ToolTip="Select Size. All measurements are in inches"  AutoPostBack=false  MarkFirstMatch=true>
                                                             
                                                             <Items>
                                                                 <radC:RadComboBoxItem Text="0"/>
                                                                <radC:RadComboBoxItem Text="1/16"/>
                                                                <radC:RadComboBoxItem Text="1/8"/>
                                                                <radC:RadComboBoxItem Text="3/16"/>
                                                                <radC:RadComboBoxItem Text="1/4"/>
                                                                <radC:RadComboBoxItem Text="5/16"/>
                                                                <radC:RadComboBoxItem Text="3/8"/>
                                                                <radC:RadComboBoxItem Text="7/16"/>
                                                                <radC:RadComboBoxItem Text="1/2"/>                                                            
                                                                <radC:RadComboBoxItem Text="9/16"/>
                                                                <radC:RadComboBoxItem Text="5/8"/>
                                                                <radC:RadComboBoxItem Text="11/16"/>
                                                                <radC:RadComboBoxItem Text="3/4"/>     
                                                                <radC:RadComboBoxItem Text="13/16"/>     
                                                                <radC:RadComboBoxItem Text="7/8"/>   
                                                                <radC:RadComboBoxItem Text="15/16"/> 
                                                             </Items>
                                                             
                                            </radC:RadComboBox>
                                        </td>
                                        <td width="8px">&nbsp
                                        </td>
                                        
                                        
                                        <td rowspan=2>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <label style="font-weight:bold;font-size:12px">Extension:</label>
                                                     </td>
                                                     <td>
                                                        <input type="radio" id="Radio1" name="Extension" /><label style="font-size:12px">0</label>
                                                     </td>
                                                     <td>
                                                        <input type="radio" id="Radio2" checked name="Extension" /><label style="font-size:12px">10</label>
                                                     </td>
                                                     <td>
                                                        <input type="radio" id="Radio3" name="Extension" /><label style="font-size:12px">20</label>
                                                     </td>
                                                     <td>
                                                        <input type="radio" id="Radio4" name="Extension" /><label style="font-size:12px">30</label>
                                                     </td>
                                                     <td>
                                                        <input type="radio" id="Radio5" name="Extension" /><label style="font-size:12px">40</label>
                                                     </td>

                                                  </tr>   
                                                  <tr>
                                                    <td>
                                                        <label style="font-weight:bold;font-size:12px">Flexion:</label>
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="Radio6" name="Extension" /><label style="font-size:12px">45</label>
                                                     </td>
                                                     <td>
                                                        <input type="radio" id="Radio7" name="Extension" /><label style="font-size:12px">60</label>
                                                     </td>
                                                     <td>
                                                        <input type="radio" id="Radio8" name="Extension" /><label style="font-size:12px">75</label>
                                                     </td>
                                                     <td colspan=2>
                                                        <input type="radio" id="Radio9" name="Extension" /><label style="font-size:12px">90</label>
                                                    </td>
                                                   </tr>
                                                    <tr>
                                                        <td colspan=5>
                                                            <label style="font-size:10px">* New braces ship with 10 extension stops installed.</label>
                                                        </td>
                                                    </tr>
                                                
                                                </table> 

                                        </td>
                                        
                                        
                                        
                                    </tr>                 
                                     <tr>
                                        <td>
                                            <label style="font-size:12px">2.</label>
                                        </td>
                                        <td>
                                            <label style="font-size:12px">Calf Circumference</label>
                                        </td>
                                        <td>
                                             <radI:RadNumericTextBox  ID="txtCalfCircumference"  Value="0" runat="server" Width=35px  ToolTip="Calf Circumference in Inches" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="24" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" NumberFormat-DecimalDigits="0">
                                                <NumberFormat DecimalDigits="0" />
                                            </radI:RadNumericTextBox>
                                        
                                        </td>
                                        <td>
                                           <radC:RadComboBox ID="RadCalfCircumference"  runat="server"  Skin="WebBlue"  
                                                             SkinsPath="~/RadControls/ComboBox/Skins" Visible=true Width="60px"
                                                             ExpandEffect=Fade
                                                             ShowWhileLoading="False" ToolTip="Select Size. All measurements are in inches"  AutoPostBack=false  MarkFirstMatch=true>
                                                             
                                                             <Items>
                                                                <radC:RadComboBoxItem Text="0"/>
                                                                <radC:RadComboBoxItem Text="1/16"/>
                                                                <radC:RadComboBoxItem Text="1/8"/>
                                                                <radC:RadComboBoxItem Text="3/16"/>
                                                                <radC:RadComboBoxItem Text="1/4"/>
                                                                <radC:RadComboBoxItem Text="5/16"/>
                                                                <radC:RadComboBoxItem Text="3/8"/>
                                                                <radC:RadComboBoxItem Text="7/16"/>
                                                                <radC:RadComboBoxItem Text="1/2"/>                                                            
                                                                <radC:RadComboBoxItem Text="9/16"/>
                                                                <radC:RadComboBoxItem Text="5/8"/>
                                                                <radC:RadComboBoxItem Text="11/16"/>
                                                                <radC:RadComboBoxItem Text="3/4"/>     
                                                                <radC:RadComboBoxItem Text="13/16"/>     
                                                                <radC:RadComboBoxItem Text="7/8"/>   
                                                                <radC:RadComboBoxItem Text="15/16"/> 
                                                             </Items>
                                                             
                                            </radC:RadComboBox>
                                        </td>
                                        <td width="8px">&nbsp
                                        </td>
                                        <td>
                                            <label style="font-size:12px">4.</label>
                                        </td>
                                        <td>
                                            <label style="font-size:12px">Knee Width</label>
                                        </td>
                                        <td>
                                             <radI:RadNumericTextBox  ID="txtKneeWidth"  Value="0"  runat="server" Width=35px  ToolTip="Knee Width in Inches" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="175" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" NumberFormat-DecimalDigits="0">
                                                <NumberFormat DecimalDigits="0" />
                                            </radI:RadNumericTextBox>
                                        
                                        </td>
                                        <td>
                                           <radC:RadComboBox ID="RadKneeWidth"  runat="server"  Skin="WebBlue"  
                                                             SkinsPath="~/RadControls/ComboBox/Skins" Visible=true Width="60px"
                                                             ExpandEffect=Fade
                                                             ShowWhileLoading="False" ToolTip="Select Size. All measurements are in inches"  AutoPostBack=false  MarkFirstMatch=true>
                                                             
                                                             <Items>
                                                                <radC:RadComboBoxItem Text="0"/>
                                                                <radC:RadComboBoxItem Text="1/16"/>
                                                                <radC:RadComboBoxItem Text="1/8"/>
                                                                <radC:RadComboBoxItem Text="3/16"/>
                                                                <radC:RadComboBoxItem Text="1/4"/>
                                                                <radC:RadComboBoxItem Text="5/16"/>
                                                                <radC:RadComboBoxItem Text="3/8"/>
                                                                <radC:RadComboBoxItem Text="7/16"/>
                                                                <radC:RadComboBoxItem Text="1/2"/>                                                            
                                                                <radC:RadComboBoxItem Text="9/16"/>
                                                                <radC:RadComboBoxItem Text="5/8"/>
                                                                <radC:RadComboBoxItem Text="11/16"/>
                                                                <radC:RadComboBoxItem Text="3/4"/>     
                                                                <radC:RadComboBoxItem Text="13/16"/>     
                                                                <radC:RadComboBoxItem Text="7/8"/>   
                                                                <radC:RadComboBoxItem Text="15/16"/> 
                                                             </Items>
                                                             
                                            </radC:RadComboBox>
                                        </td>
                                        <td width="8px">&nbsp
                                        </td>
                                    </tr>           
                                    <tr>
                                        <td colspan=3>
                                            <label style="font-size:12px">Measurements Taken By:</label>
                                        </td>
                                        <td colspan=6>
                                            <asp:TextBox ID="txtTakenBy" runat=server Width="160px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    
                                   
                                    

 
             <tr>
                <td colspan=11 bgcolor=LightGrey >
                 FUSION BRACE INFORMATION
                 
                </td>
            </tr>

            <tr>
             
                        <td colspan=11>
                            <table bgcolor=white width=100%  cellpadding=0  style="border-bottom-width:thin;border-bottom-style:solid;border-bottom-color:Black;
                            border-left-width:thin;border-left-style:solid;border-left-color:Black;
                            border-right-width:thin;border-right-style:solid;border-right-color:Black;
                            border-top-width:thin;border-top-style:solid;border-top-color:Black"> 
                                <tr>
                                    <td bgcolor=LightGrey>
                                    </td>
                                    <td bgcolor=LightGrey>
                                        Standard Hinge
                                    </td>
                                    <td bgcolor=LightGrey align=center>
                                        *OA
                                    </td>
                                    <td bgcolor=LightGrey>
                                        NOTES/SPECIAL INSTRUCTIONS
                                    </td>

                                </tr>
                                <!--Second row in Brace Information--> 

                                <tr>
                                    <td>
                                        <label style="font-size:14px">FUSION XT</label>                                        
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01200" runat=server GroupName="Brace" Text="01200"  Font-Size="14px"/>
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01205" runat=server GroupName="Brace" Text="01205"  Font-Size="14px"/>
                                    </td>
                                    <td rowspan=4><asp:TextBox ID=txtNotes runat=server TextMode=MultiLine Width=300px Height=100px></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-size:14px">FUSION</label>                                        
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01207" runat=server GroupName="Brace" Text="01207"  Font-Size="14px"/>
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01208" runat=server GroupName="Brace" Text="01208"  Font-Size="14px"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-size:14px">FUSION Women's</label>                                        
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01201" runat=server GroupName="Brace" Text="01201"  Font-Size="14px"/>
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01204" runat=server GroupName="Brace" Text="01204"  Font-Size="14px"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-size:14px">FUSION Women's White</label>                                        
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01203" runat=server GroupName="Brace" Text="01203" Font-Size="14px"/>

                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01202" runat=server GroupName="Brace" Text="01202"  Font-Size="14px"/>

                                    </td>
                                </tr>
                                
                                
                                
                                
                            </table>
                        </td>
                        </tr>



                        <!--Accessoriesz-->

                        <!--Second area of Fusion Brace Information-->
                        
                        <tr>
                           <td colspan=11>
                            <table bgcolor=white width=100%  cellpadding=0  style="border-bottom-width:thin;border-bottom-style:solid;border-bottom-color:Black;
                            border-left-width:thin;border-left-style:solid;border-left-color:Black;
                            border-right-width:thin;border-right-style:solid;border-right-color:Black;
                            border-top-width:thin;border-top-style:solid;border-top-color:Black"> 
                                
                                <tr>
                                    <td>
                                        <label style="font-size:14px">FUSION XT Color</label>                                        
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01215a" runat=server GroupName="Brace" Text="01215" Font-Size="14px"/>
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01218a" runat=server GroupName="Brace" Text="01218" Font-Size="14px"/>
                                    </td>
                                    <td rowspan=3>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="chkForestLeft" runat=server GroupName="BraceColor" Text="Forest" Font-Size="14px"  OnCheckedChanged="chkForestLeft_CheckChanged"  AutoPostBack=true/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkOrange" runat=server GroupName="BraceColor" Text="Orange" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkNavy" runat=server GroupName="BraceColor" Text="Navy" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkPink" runat=server GroupName="BraceColor" Text="Pink" Font-Size="14px"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="chkRoyal" runat=server GroupName="BraceColor" Text="Royal" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkYellow" runat=server GroupName="BraceColor" Text="Yellow" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkSage" runat=server GroupName="BraceColor" Text="Sage" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkRed" runat=server GroupName="BraceColor" Text="Red" Font-Size="14px"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="chkCharcoal" runat=server GroupName="BraceColor" Text="Charcoal" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkMauve" runat=server GroupName="BraceColor" Text="Mauve" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    
                                                </td>
                                                <td>
                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan=4>
                                                    <asp:RadioButton ID="chkCustom" runat=server GroupName="BraceColor" Text="Custom" Font-Size="14px"/>
                                                     <label style="font-size:12px">Pantone</label>
                                                     <asp:TextBox ID=txtPantone runat=server Width=100px></asp:TextBox>    
                                                </td>
                                            </tr>
                                        </table>



                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-size:14px">FUSION Color</label>                                        
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01209a" runat=server GroupName="Brace" Text="01209" Font-Size="14px"/>
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01216a" runat=server GroupName="Brace" Text="01216" Font-Size="14px"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-size:14px">FUSION Women Color</label>                                        
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01214a" runat=server GroupName="Brace" Text="01214" Font-Size="14px" />
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01217a" runat=server GroupName="Brace" Text="01217" Font-Size="14px"/>
                                    </td>
                                </tr>
                        
                                
                                
                                
                            </table>
                        </td>
                        </tr>
                        <!--End of second area of Fusion Brace Information-->
                        
                       <!--Third area of Fusion Brace Information-->
                        
                        <tr>
                           <td colspan=11>
                            <table bgcolor=white width=100%  cellpadding=0  style="border-bottom-width:thin;border-bottom-style:solid;border-bottom-color:Black;
                            border-left-width:thin;border-left-style:solid;border-left-color:Black;
                            border-right-width:thin;border-right-style:solid;border-right-color:Black;
                            border-top-width:thin;border-top-style:solid;border-top-color:Black"> 
                                
                                <tr>
                                    <td style="width: 261px">
                                        <label style="font-size:14px">No Enhancement</label>                                        
                                    </td>
                                    <td style="width: 59px">
                                        <asp:RadioButton ID="chkNoEnhancement2" runat=server GroupName="Enhancement" Text="None" Font-Size="14px" Checked=true OnCheckedChanged="chkNoEnhancement_CheckChanged2" AutoPostBack=true/>
                                    </td>
                                   
                                    <td rowspan=4>
                                    
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="chkFlames" runat=server GroupName="Pattern" Text="Flames" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkCamouflage" runat=server GroupName="Pattern" Text="Camouflage" Font-Size="14px"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="chkFlag" runat=server GroupName="Pattern" Text="Flag" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkRipples" runat=server GroupName="Pattern" Text="Ripples" Font-Size="14px"/>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="chkCustomPattern" runat=server GroupName="Pattern" Text="Custom Pattern *" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    Notes: <asp:TextBox ID=txtPatternNotes runat=server Width=100px></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                                
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 261px">
                                        <label style="font-size:14px">Color Enhancement</label>                                        
                                    </td>
                                    <td style="width: 59px">
                                        <asp:RadioButton ID="chk03161" runat=server GroupName="Enhancement" Text="03161" Font-Size="14px"/>
                                    </td>
                                </tr>
                               
                                <tr>
                                    <td style="width: 261px">
                                        <label style="font-size:14px">Pattern Enhancement</label>                                        
                                    </td>
                                    <td style="width: 59px">
                                        <asp:RadioButton ID="chk03162" runat=server GroupName="Enhancement" Text="03162" Font-Size="14px"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 261px">
                                        <label style="font-size:14px">Custom Color / Pattern Enhancement</label>                                        
                                    </td>
                                    <td style="width: 59px">
                                        <asp:RadioButton ID="chk03163" runat=server GroupName="Enhancement" Text="03163" Font-Size="14px"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=2>
                                        <label style="font-size:10px;font-style:italic">*72 hr turnaround on FUSION custom Pantone / custom pattern orders</label>                                        
                                    </td>
                                </tr>
                        
                                
                                
                                
                            </table>
                        </td>
                        </tr>
                          
                          
                         </table>   
        
        
        
        </radTS:PageView>

        <radTS:PageView ID="PageView5" runat="server" BackColor="WhiteSmoke">
                             <table width="100%">
                                    <tr>
                                        <td colspan=2>
                                            <asp:label ID="Label6" runat=server Font-Bold=true ForeColor=Red>Right Leg</asp:label>
                                        </td>
                                        <td colspan=9>
                                            <asp:RadioButton  ID='RadioButton1' runat=server Text='Cast' Font-Size=Small GroupName=chkLegMeasurement />
                                            <asp:RadioButton  ID='RadioButton2' runat=server Text='Reform' Font-Size=Small GroupName=chkLegMeasurement />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label style="font-size:12px">1.</label>
                                        </td>
                                        <td>
                                            <label style="font-size:12px">Thigh Circumference</label>
                                        </td>
                                        <td>

                                            <radI:RadNumericTextBox  ID="RadNumericTextBox1" Value="0"  runat="server" Width=35px ToolTip="Thigh Circumference in Inches" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="32" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" NumberFormat-DecimalDigits="0">
                                                <NumberFormat DecimalDigits="0" />
                                            </radI:RadNumericTextBox>
                                         
                                         </td>
                                        <td>
                                           <radC:RadComboBox ID="RadComboBox1"  runat="server"  Skin="WebBlue"  
                                                             SkinsPath="~/RadControls/ComboBox/Skins" Visible=true Width="60px"
                                                             ExpandEffect=Fade
                                                             ShowWhileLoading="False" ToolTip="Select Size. All measurements are in inches"  AutoPostBack=false  MarkFirstMatch=true>
                                                             
                                                             <Items>
                                                                <radC:RadComboBoxItem Text="0"/>
                                                                <radC:RadComboBoxItem Text="1/16"/>
                                                                <radC:RadComboBoxItem Text="2/16"/>
                                                                <radC:RadComboBoxItem Text="3/16"/>
                                                                <radC:RadComboBoxItem Text="4/16"/>
                                                                <radC:RadComboBoxItem Text="5/16"/>
                                                                <radC:RadComboBoxItem Text="6/16"/>
                                                                <radC:RadComboBoxItem Text="7/16"/>
                                                                <radC:RadComboBoxItem Text="8/16"/>                                                            
                                                                <radC:RadComboBoxItem Text="9/16"/>
                                                                <radC:RadComboBoxItem Text="10/16"/>
                                                                <radC:RadComboBoxItem Text="11/16"/>
                                                                <radC:RadComboBoxItem Text="12/16"/>     
                                                                <radC:RadComboBoxItem Text="13/16"/>     
                                                                <radC:RadComboBoxItem Text="14/16"/>   
                                                                <radC:RadComboBoxItem Text="15/16"/>   
                                                             </Items>
                                                             
                                            </radC:RadComboBox>
                                            </td>
                                        <td width="8px">&nbsp
                                        </td>
                                        <td>
                                            <label style="font-size:12px">3.</label>
                                        </td>
                                        <td>
                                            <label style="font-size:12px">Knee Offset</label>
                                        </td>
                                        <td>
                                             <radI:RadNumericTextBox  ID="RadNumericTextBox2"  Value="0" runat="server"   Width=35px ToolTip="Knee Offset in Inches" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="29" MinValue="-29" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" NumberFormat-DecimalDigits="0">
                                                <NumberFormat DecimalDigits="0" />
                                            </radI:RadNumericTextBox>
                                        
                                        </td>
                                        <td>
                                           <radC:RadComboBox ID="RadComboBox2"  runat="server"  Skin="WebBlue"  
                                                             SkinsPath="~/RadControls/ComboBox/Skins" Visible=true Width="60px"
                                                             ExpandEffect=Fade
                                                             ShowWhileLoading="False" ToolTip="Select Size. All measurements are in inches"  AutoPostBack=false  MarkFirstMatch=true>
                                                             
                                                             <Items>
                                                                <radC:RadComboBoxItem Text="0"/>
                                                                <radC:RadComboBoxItem Text="1/16"/>
                                                                <radC:RadComboBoxItem Text="2/16"/>
                                                                <radC:RadComboBoxItem Text="3/16"/>
                                                                <radC:RadComboBoxItem Text="4/16"/>
                                                                <radC:RadComboBoxItem Text="5/16"/>
                                                                <radC:RadComboBoxItem Text="6/16"/>
                                                                <radC:RadComboBoxItem Text="7/16"/>
                                                                <radC:RadComboBoxItem Text="8/16"/>                                                            
                                                                <radC:RadComboBoxItem Text="9/16"/>
                                                                <radC:RadComboBoxItem Text="10/16"/>
                                                                <radC:RadComboBoxItem Text="11/16"/>
                                                                <radC:RadComboBoxItem Text="12/16"/>     
                                                                <radC:RadComboBoxItem Text="13/16"/>     
                                                                <radC:RadComboBoxItem Text="14/16"/>   
                                                                <radC:RadComboBoxItem Text="15/16"/>   
                                                             </Items>
                                                             
                                            </radC:RadComboBox>
                                        </td>
                                        <td width="8px">&nbsp
                                        </td>
                                        
                                        
                                        <td rowspan=2>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <label style="font-weight:bold;font-size:12px">Extension:</label>
                                                     </td>
                                                     <td>
                                                        <input type="radio" id="Radio10" name="Extension" /><label style="font-size:12px">0</label>
                                                     </td>
                                                     <td>
                                                        <input type="radio" id="Radio11" checked name="Extension" /><label style="font-size:12px">10</label>
                                                     </td>
                                                     <td>
                                                        <input type="radio" id="Radio12" name="Extension" /><label style="font-size:12px">20</label>
                                                     </td>
                                                     <td>
                                                        <input type="radio" id="Radio13" name="Extension" /><label style="font-size:12px">30</label>
                                                     </td>
                                                     <td>
                                                        <input type="radio" id="Radio14" name="Extension" /><label style="font-size:12px">40</label>
                                                     </td>

                                                  </tr>   
                                                  <tr>
                                                    <td>
                                                        <label style="font-weight:bold;font-size:12px">Flexion:</label>
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="Radio15" name="Extension" /><label style="font-size:12px">45</label>
                                                     </td>
                                                     <td>
                                                        <input type="radio" id="Radio16" name="Extension" /><label style="font-size:12px">60</label>
                                                     </td>
                                                     <td>
                                                        <input type="radio" id="Radio17" name="Extension" /><label style="font-size:12px">75</label>
                                                     </td>
                                                     <td colspan=2>
                                                        <input type="radio" id="Radio18" name="Extension" /><label style="font-size:12px">90</label>
                                                    </td>
                                                   </tr>
                                                    <tr>
                                                        <td colspan=5>
                                                            <label style="font-size:10px">* New braces ship with 10 extension stops installed.</label>
                                                        </td>
                                                    </tr>
                                                
                                                </table> 

                                        </td>
                                        
                                        
                                        
                                    </tr>                 
                                     <tr>
                                        <td>
                                            <label style="font-size:12px">2.</label>
                                        </td>
                                        <td>
                                            <label style="font-size:12px">Calf Circumference</label>
                                        </td>
                                        <td>
                                             <radI:RadNumericTextBox  ID="RadNumericTextBox3"  Value="0" runat="server" Width=35px  ToolTip="Calf Circumference in Inches" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="24" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" NumberFormat-DecimalDigits="0">
                                                <NumberFormat DecimalDigits="0" />
                                            </radI:RadNumericTextBox>
                                        
                                        </td>
                                        <td>
                                           <radC:RadComboBox ID="RadComboBox3"  runat="server"  Skin="WebBlue"  
                                                             SkinsPath="~/RadControls/ComboBox/Skins" Visible=true Width="60px"
                                                             ExpandEffect=Fade
                                                             ShowWhileLoading="False" ToolTip="Select Size. All measurements are in inches"  AutoPostBack=false  MarkFirstMatch=true>
                                                             
                                                             <Items>
                                                                <radC:RadComboBoxItem Text="0"/>
                                                                <radC:RadComboBoxItem Text="1/16"/>
                                                                <radC:RadComboBoxItem Text="2/16"/>
                                                                <radC:RadComboBoxItem Text="3/16"/>
                                                                <radC:RadComboBoxItem Text="4/16"/>
                                                                <radC:RadComboBoxItem Text="5/16"/>
                                                                <radC:RadComboBoxItem Text="6/16"/>
                                                                <radC:RadComboBoxItem Text="7/16"/>
                                                                <radC:RadComboBoxItem Text="8/16"/>                                                            
                                                                <radC:RadComboBoxItem Text="9/16"/>
                                                                <radC:RadComboBoxItem Text="10/16"/>
                                                                <radC:RadComboBoxItem Text="11/16"/>
                                                                <radC:RadComboBoxItem Text="12/16"/>     
                                                                <radC:RadComboBoxItem Text="13/16"/>     
                                                                <radC:RadComboBoxItem Text="14/16"/>   
                                                                <radC:RadComboBoxItem Text="15/16"/>   
                                                             </Items>
                                                             
                                            </radC:RadComboBox>
                                        </td>
                                        <td width="8px">&nbsp
                                        </td>
                                        <td>
                                            <label style="font-size:12px">4.</label>
                                        </td>
                                        <td>
                                            <label style="font-size:12px">Knee Width</label>
                                        </td>
                                        <td>
                                             <radI:RadNumericTextBox  ID="RadNumericTextBox4"  Value="0"  runat="server" Width=35px  ToolTip="Knee Width in Inches" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="175" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" NumberFormat-DecimalDigits="0">
                                                <NumberFormat DecimalDigits="0" />
                                            </radI:RadNumericTextBox>
                                        
                                        </td>
                                        <td>
                                           <radC:RadComboBox ID="RadComboBox4"  runat="server"  Skin="WebBlue"  
                                                             SkinsPath="~/RadControls/ComboBox/Skins" Visible=true Width="60px"
                                                             ExpandEffect=Fade
                                                             ShowWhileLoading="False" ToolTip="Select Size. All measurements are in inches"  AutoPostBack=false  MarkFirstMatch=true>
                                                             
                                                             <Items>
                                                                <radC:RadComboBoxItem Text="0"/>
                                                                <radC:RadComboBoxItem Text="1/16"/>
                                                                <radC:RadComboBoxItem Text="2/16"/>
                                                                <radC:RadComboBoxItem Text="3/16"/>
                                                                <radC:RadComboBoxItem Text="4/16"/>
                                                                <radC:RadComboBoxItem Text="5/16"/>
                                                                <radC:RadComboBoxItem Text="6/16"/>
                                                                <radC:RadComboBoxItem Text="7/16"/>
                                                                <radC:RadComboBoxItem Text="8/16"/>                                                            
                                                                <radC:RadComboBoxItem Text="9/16"/>
                                                                <radC:RadComboBoxItem Text="10/16"/>
                                                                <radC:RadComboBoxItem Text="11/16"/>
                                                                <radC:RadComboBoxItem Text="12/16"/>     
                                                                <radC:RadComboBoxItem Text="13/16"/>     
                                                                <radC:RadComboBoxItem Text="14/16"/>   
                                                                <radC:RadComboBoxItem Text="15/16"/>   
                                                             </Items>
                                                             
                                            </radC:RadComboBox>
                                        </td>
                                        <td width="8px">&nbsp
                                        </td>
                                    </tr>           
                                    <tr>
                                        <td colspan=3>
                                            <label style="font-size:12px">Measurements Taken By:</label>
                                        </td>
                                        <td colspan=6>
                                            <asp:TextBox ID="TextBox1" runat=server Width="160px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    
                                   
                                    

 
             <tr>
                <td colspan=11 bgcolor=LightGrey >
                 FUSION BRACE INFORMATION
                 
                </td>
            </tr>

            <tr>
             
                        <td colspan=11>
                            <table bgcolor=white width=100%  cellpadding=0  style="border-bottom-width:thin;border-bottom-style:solid;border-bottom-color:Black;
                            border-left-width:thin;border-left-style:solid;border-left-color:Black;
                            border-right-width:thin;border-right-style:solid;border-right-color:Black;
                            border-top-width:thin;border-top-style:solid;border-top-color:Black"> 
                                <tr>
                                    <td bgcolor=LightGrey>
                                    </td>
                                    <td bgcolor=LightGrey>
                                        Standard Hinge
                                    </td>
                                    <td bgcolor=LightGrey align=center>
                                        *OA
                                    </td>
                                    <td bgcolor=LightGrey>
                                        NOTES/SPECIAL INSTRUCTIONS
                                    </td>

                                </tr>
                                <!--Second row in Brace Information--> 

                                <tr>
                                    <td>
                                        <label style="font-size:14px">FUSION XT</label>                                        
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="RadioButton3" runat=server GroupName="Brace" Text="01200"  Font-Size="14px"/>
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="RadioButton4" runat=server GroupName="Brace" Text="01205"  Font-Size="14px"/>
                                    </td>
                                    <td rowspan=4><asp:TextBox ID=TextBox2 runat=server TextMode=MultiLine Width=300px Height=100px></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-size:14px">FUSION</label>                                        
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="RadioButton5" runat=server GroupName="Brace" Text="01207"  Font-Size="14px"/>
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="RadioButton6" runat=server GroupName="Brace" Text="01208"  Font-Size="14px"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-size:14px">FUSION Women's</label>                                        
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="RadioButton7" runat=server GroupName="Brace" Text="01201"  Font-Size="14px"/>
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="RadioButton8" runat=server GroupName="Brace" Text="01204"  Font-Size="14px"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-size:14px">FUSION Women's White</label>                                        
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="RadioButton9" runat=server GroupName="Brace" Text="01203" Font-Size="14px"/>

                                    </td>
                                    <td>
                                        <asp:RadioButton ID="RadioButton10" runat=server GroupName="Brace" Text="01202"  Font-Size="14px"/>

                                    </td>
                                </tr>
                                
                                
                                
                                
                            </table>
                        </td>
                        </tr>



                        <!--Accessoriesz-->

                        <!--Second area of Fusion Brace Information-->
                        
                        <tr>
                           <td colspan=11>
                            <table bgcolor=white width=100%  cellpadding=0  style="border-bottom-width:thin;border-bottom-style:solid;border-bottom-color:Black;
                            border-left-width:thin;border-left-style:solid;border-left-color:Black;
                            border-right-width:thin;border-right-style:solid;border-right-color:Black;
                            border-top-width:thin;border-top-style:solid;border-top-color:Black"> 
                                
                                <tr>
                                    <td>
                                        <label style="font-size:14px">FUSION XT Color</label>                                        
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="RadioButton11" runat=server GroupName="Brace" Text="01215" Font-Size="14px"/>
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="RadioButton12" runat=server GroupName="Brace" Text="01218" Font-Size="14px"/>
                                    </td>
                                    <td rowspan=3>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="RadioButton13" runat=server GroupName="BraceColor" Text="Forest" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="RadioButton14" runat=server GroupName="BraceColor" Text="Orange" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="RadioButton15" runat=server GroupName="BraceColor" Text="Navy" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="RadioButton16" runat=server GroupName="BraceColor" Text="Pink" Font-Size="14px"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="RadioButton17" runat=server GroupName="BraceColor" Text="Royal" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="RadioButton18" runat=server GroupName="BraceColor" Text="Yellow" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="RadioButton19" runat=server GroupName="BraceColor" Text="Sage" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="RadioButton20" runat=server GroupName="BraceColor" Text="Red" Font-Size="14px"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="RadioButton21" runat=server GroupName="BraceColor" Text="Charcoal" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="RadioButton22" runat=server GroupName="BraceColor" Text="Mauve" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    
                                                </td>
                                                <td>
                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan=4>
                                                    <asp:RadioButton ID="RadioButton23" runat=server GroupName="BraceColor" Text="Custom" Font-Size="14px"/>
                                                     <label style="font-size:12px">Pantone</label>
                                                     <asp:TextBox ID=TextBox3 runat=server Width=100px></asp:TextBox>    
                                                </td>
                                            </tr>
                                        </table>



                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-size:14px">FUSION Color</label>                                        
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="RadioButton24" runat=server GroupName="Brace" Text="01209" Font-Size="14px"/>
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="RadioButton25" runat=server GroupName="Brace" Text="01216" Font-Size="14px"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-size:14px">FUSION Women Color</label>                                        
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="RadioButton26" runat=server GroupName="Brace" Text="01214" Font-Size="14px" />
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="RadioButton27" runat=server GroupName="Brace" Text="01217" Font-Size="14px"/>
                                    </td>
                                </tr>
                        
                                
                                
                                
                            </table>
                        </td>
                        </tr>
                        <!--End of second area of Fusion Brace Information-->
                        
                       <!--Third area of Fusion Brace Information-->
                        
                        <tr>
                           <td colspan=11>
                            <table bgcolor=white width=100%  cellpadding=0  style="border-bottom-width:thin;border-bottom-style:solid;border-bottom-color:Black;
                            border-left-width:thin;border-left-style:solid;border-left-color:Black;
                            border-right-width:thin;border-right-style:solid;border-right-color:Black;
                            border-top-width:thin;border-top-style:solid;border-top-color:Black"> 
                                
                                <tr>
                                    <td style="width: 261px">
                                        <label style="font-size:14px">Color Enhancement</label>                                        
                                    </td>
                                    <td style="width: 59px">
                                        <asp:RadioButton ID="RadioButton28" runat=server GroupName="Enhancement" Text="03161" Font-Size="14px"/>
                                    </td>
                                   
                                    <td rowspan=3>
                                    
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="RadioButton29" runat=server GroupName="Pattern" Text="Flames" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="RadioButton30" runat=server GroupName="Pattern" Text="Camouflage" Font-Size="14px"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="chkFlagRight" runat=server GroupName="Pattern" Text="Flag" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkRipplesRight" runat=server GroupName="Pattern" Text="Ripples" Font-Size="14px"/>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="chkCustomPatternRight" runat=server GroupName="Pattern" Text="Custom Pattern *" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    Notes: <asp:TextBox ID=txtPatternNotesRight runat=server Width=100px MaxLength=200></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                                
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 261px">
                                        <label style="font-size:14px">Pattern Enhancement</label>                                        
                                    </td>
                                    <td style="width: 59px">
                                        <asp:RadioButton ID="chk03162Right" runat=server GroupName="Enhancement" Text="03162" Font-Size="14px"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 261px">
                                        <label style="font-size:14px">Custom Color / Pattern Enhancement</label>                                        
                                    </td>
                                    <td style="width: 59px">
                                        <asp:RadioButton ID="chk03163Right" runat=server GroupName="Enhancement" Text="03163" Font-Size="14px"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=2>
                                        <label style="font-size:10px;font-style:italic">*72 hr turnaround on FUSION custom Pantone / custom pattern orders</label>                                        
                                    </td>
                                </tr>
                        
                                
                                
                                
                            </table>
                        </td>
                        </tr>
                          
                          
                         </table>   
        </radTS:PageView>


        <radTS:PageView ID="PageView6" runat=server BackColor="WhiteSmoke">
                            <table>
                                 <td valign=top>
                            <!--Fusion Frame Pads-->
                            <table bgcolor=white width=98% height="100%" > 
                                <tr>
                                    <td  bgcolor=LightGrey>
                                        FUSION FRAME PADS
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style=" font-style:italic;font-size:12px">All FUSION braces available with black frame pads only.</label>
                                        
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td  bgcolor=LightGrey>
                                        FUSION ACCESSORIES
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table> 
                                            <tr>
                                                <td>
                                                   <radI:RadNumericTextBox  ID="radSlideGuardML"  Value="1"  runat="server" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="25px" NumberFormat-DecimalDigits="0">
                                                    <NumberFormat DecimalDigits="0" />
                                                   </radI:RadNumericTextBox>
                                                </td>
                                                <td style="font-size:12px">
                                                    <asp:CheckBox ID="chSlideGuardML" runat=server Text="Slide Guard, M/L"  Font-Size="12px" />
                                                    
                                                </td>
                                                <td style="font-size:12px">(22000)
                                                </td>    
                                            </tr>
                                            <tr>
                                                <td>
                                                   <radI:RadNumericTextBox  ID="radSlideGuardXL"  Value="1"  runat="server" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="25px" NumberFormat-DecimalDigits="0">
                                                    <NumberFormat DecimalDigits="0" />
                                                   </radI:RadNumericTextBox>
                                                </td>                                                
                                                <td style="font-size:12px">
                                                    <asp:Checkbox ID="chkSlideGuardXL" runat=server Text="Slide Guard, XL/XXL"  Font-Size="12px" />
                                                    
                                                </td>
                                                <td style="font-size:12px">(22001)
                                                </td>    
                                            </tr>
                                            <tr>
                                                <td>
                                                   <radI:RadNumericTextBox  ID="radCottonUndersleeve"  Value="1"  runat="server" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="25px" NumberFormat-DecimalDigits="0">
                                                    <NumberFormat DecimalDigits="0" />
                                                   </radI:RadNumericTextBox>
                                                </td>                                            
                                                <td style="font-size:12px">
                                                    <asp:Checkbox ID="chkCottonUndersleeve" runat=server Text="Cotton Undersleeve"  Font-Size="12px"/>
                                                    
                                                </td>
                                                <td style="font-size:12px">(0985X)
                                                </td>    
                                            </tr>
                                            <tr>
                                             <td>
                                                   <radI:RadNumericTextBox  ID="radNeoprene"  Value="1"  runat="server" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="25px" NumberFormat-DecimalDigits="0">
                                                    <NumberFormat DecimalDigits="0" />
                                                   </radI:RadNumericTextBox>
                                                </td>      
                                                <td style="font-size:12px">
                                                    <asp:Checkbox ID="chkNeoprene" runat=server  Text="Neoprene Undersleeve"  Font-Size="12px"/>
                                                    
                                                </td>
                                                <td style="font-size:12px">(0735X)
                                                </td>    
                                            </tr>
                                            <tr>
                                             <td>
                                                   <radI:RadNumericTextBox  ID="radFusionBraceCover"  Value="1"  runat="server" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="25px" NumberFormat-DecimalDigits="0">
                                                    <NumberFormat DecimalDigits="0" />
                                                   </radI:RadNumericTextBox>
                                                </td>      
                                                <td style="font-size:12px">
                                                    
                                                    <asp:Checkbox ID="chkFusionBraceCover" runat=server Text="FUSION Brace Cover"  Font-Size="12px"/>
                                                    
                                                </td>
                                                <td style="font-size:12px">(1008X)
                                                </td>    
                                            </tr>
                                        </table>
                                    </td>    
                                </tr>
                            </table>
                            <!--Fusion Frame Pads - END-->
                        </td>
                        </tr>
                    </table>
        
        </radTS:PageView>

        <radTS:PageView  ID="PageView7" runat=server BackColor="WhiteSmoke">
        <table width="100%" bgcolor=white  border="0" cellpadding=0>                    
                            <tr>
                                <td bgcolor="lightgrey" valign=top>
                                SHIP VIA
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    
                                   <radC:RadComboBox ID="cbxShipVia"  runat="server"  Skin="WebBlue"  
                                                     SkinsPath="~/RadControls/ComboBox/Skins" Visible=true Width="140px"
                                                     ExpandEffect=Fade
                                                     ShowWhileLoading="False" ToolTip="Select male or female."  AutoPostBack=false  MarkFirstMatch=true>
                                                     
                                                     <Items>
                                                        <radC:RadComboBoxItem Text="Saturday"/>
                                                        <radC:RadComboBoxItem Text="Next Day Early AM"/>
                                                        <radC:RadComboBoxItem Text="Next Day AM"/>
                                                        <radC:RadComboBoxItem Text="Next Day PM"/>
                                                        <radC:RadComboBoxItem Text="2nd Day"/>
                                    
                                                       <radC:RadComboBoxItem Text="3rd Day"/>
                                                        <radC:RadComboBoxItem Text="Ground"/>
                                                     </Items>
                                                     
                                    </radC:RadComboBox>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    
                                    <asp:RadioButton ID="radUPS" runat=server GroupName="Shipping" Text="UPS"  Font-Size="12px"/>
                                    <asp:RadioButton ID="radFedEx" runat=server GroupName="Shipping" Text="Fed Ex"  Font-Size="12px"/>
                                    
                                </td>
                            </tr>
                            <tr height=100%>
                                <td ></td>
                            </tr>
                          </table>

        </radTS:PageView>

   </radTS:RadMultiPage>
            
 

 


        </td>
        
 




    </tr>
    
 




 
 


    
 
 


  
  

  </table>
                 

                 

                 
 
                 

 
                 


                 

                 

 </radA:RadAjaxPanel>


</form>
</body>