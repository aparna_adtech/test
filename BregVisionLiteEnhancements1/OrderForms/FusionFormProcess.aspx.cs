using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.IO;
namespace BregVision.OrderForms
{
    public partial class FusionFormProcess : System.Web.UI.Page
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            PrepareEmail();
        }
 
        

        public void PrepareEmail()
        {

            //string confirmationEmailAddress = GetConfirmationEmailAddress(BregVisionOrderID);
            string confirmationEmailAddress = "gregory.armstrong.ross@gmail.com";


            //Database db = DatabaseFactory.CreateDatabase("BregVisionLocalConnectionString");
            //DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetBVOSupplierInfoForFaxEmail");

            //db.AddInParameter(dbCommand, "BregVisionOrderID", DbType.Int32, BregVisionOrderID);

            //IDataReader reader = db.ExecuteReader(dbCommand);

            //Int32 PONumber;
            //string BrandShortName;
            //string ShippingTypeName;
            //decimal SupplierTotal;
            //DateTime DateCreated;
            //Int32 SupplierOrderID;
            //string EmailAddress;
            //string Fax;
            //string AccountCode;

            //string Email;

            //while (reader.Read())
            //{
                //Resets string builder
                sb.Remove(0, sb.Length);
                //PONumber = Convert.ToInt32(reader["PurchaseOrder"].ToString());
                //BrandShortName = reader["BrandShortName"].ToString();
                //ShippingTypeName = reader["ShippingTypeName"].ToString();
                //SupplierTotal = Convert.ToDecimal(reader["Total"].ToString());
                //DateCreated = Convert.ToDateTime(reader["CreatedDate"].ToString());
                //SupplierOrderID = Convert.ToInt32(reader["SupplierOrderID"].ToString());
                //EmailAddress = reader["EmailAddress"].ToString();
                //Fax = reader["Fax"].ToString();
                //if (reader["AccountCode"].ToString() == null)
                //{
                //    AccountCode = "";
                //}
                //else
                //{
                //    AccountCode = reader["AccountCode"].ToString();
                //}


                //Opening Line

                sb.Append("<table width='98%' border='0'>");
                sb.Append("<tr><td colspan=3>FUSION Custom Knee Brace Order Form</td><td align=right></td><td></td></tr>");
                sb.Append("<tr><td><table  bgcolor=white  cellpadding=0><tr><td><!--Bill To Area-->");
                sb.Append("<table width='300' border='0' cellpadding=0  bgcolor=white><tr><td>");
                sb.Append("<table><tr><td bgcolor=LightGrey><label id='BillToHeader' style='font-size:12px'>BILL TO</label></td>");
                sb.Append("</tr></table></td></tr><tr><td><table border='0'  bgcolor=white width='100%'><tr><td width='80px'><label id='Label3' style='font-size:12px' >Customer #</label></td><td colspan='2'>");
                //Customer Number
                sb.Append(Request["txtCustomerNo"]);
                sb.Append("</td><td width='40px'><label id='Label4' style='font-size:12px' >PO #</label></td><td colspan='2'>");
                sb.Append(Request.Form["txtPONo"]);
                sb.Append("</td></tr><tr><td><label style='font-size:12px'>Contact</label></td><td colspan='2'>");
                sb.Append(Request.Form["txtBillToContact"]);
                sb.Append("</td><td><label style='font-size:12px'>Tel #</label></td><td  colspan='2'>");
                sb.Append(Request.Form["txtBillToPhone"]);
                sb.Append("</td></tr><tr><td><label style='font-size:12px'>Address</label> </td><td colspan='5'>");
                sb.Append(Request.Form["txtBillToAddress"]);
                sb.Append("</td></tr></table></td></tr><tr><td><table bgcolor=white><tr><td width='22'><label style='font-size:12px'>City</label></td><td width='123'>");
                sb.Append(Request.Form["txtBillToCity"]);
                sb.Append("</td><td width='26'><label style='font-size:12px'>State</label></td><td width='27'>");
                sb.Append(Request.Form["cbxBillToState"]);
                sb.Append("</td><td width='21'><label style='font-size:12px'>Zip</label></td><td width='56'>");
                sb.Append(Request.Form["txtBillToZip"]);
                sb.Append("</td></tr></table></td></tr></table></td><!--Spacer--><td><!--Bill To Area--><table width='300' border='0' cellpadding=0  bgcolor=white >");
                sb.Append("<tr><td><table><tr><td bgcolor=LightGrey><label id='Label1' style='font-size:12px'>SHIP T0 (if different)</label></td></tr></table></td></tr>");
                sb.Append("<tr><td><table border='0'  bgcolor=white width='100%' ><tr><td width='45'><label style='font-size:12px'>Name</label> </td><td align=left>");
                sb.Append(Request.Form["txtShipToName"]);
                sb.Append("</td></tr><tr><td><label style='font-size:12px'>Address</label> </td><td>");
                sb.Append(Request.Form["txtShipToAddress1"]);
                sb.Append("</td></tr><tr><td><label style='font-size:12px'></label> </td><td>");
                sb.Append(Request.Form["txtShipToAddress2"]);
                sb.Append("</td></tr></table></td></tr><tr><td><table bgcolor=white><tr><td width='22'><label style='font-size:12px'>City</label></td><td width='123'>");
                sb.Append(Request.Form["txtShipToCity"]);
                sb.Append("</td><td width='26'><label style='font-size:12px'>State</label></td><td width='27'>");
                sb.Append(Request.Form["cbxShipToState"]);
                sb.Append("</td><td width='21'><label style='font-size:12px'>Zip</label></td><td width='56'>");
                sb.Append(Request.Form["txtShipToZip"]);
                sb.Append("</td></tr></table></td></tr> </table></td><!--Spacer--><td height='100%'><!--For Office Use Only Area--><table width='177' border='0' cellpadding=0  bgcolor=white >");
                sb.Append("<tr><td height='100%'><table><tr><td bgcolor=LightGrey><label id='Label2' style='font-size:12px'>FOR OFFICE USE ONLY</label></td></tr></table></td></tr><tr><td>");
                sb.Append("<table border='0'  bgcolor=white width='100%'><tr><td width='45'><label style='font-size:12px'>DATE/TIME:</label> </td><td align=left>");
                sb.Append(Request.Form["txtDateTime"]);
                sb.Append("</td></tr><tr><td><label style='font-size:12px'>RA #</label> </td><td>");
                sb.Append(Request.Form["txtRANo"]);
                sb.Append("</td></tr></table></td></tr><tr><td><table bgcolor=white><tr><td>");
                sb.Append(Request.Form["chkNewAccount"]);
                sb.Append("</td><td>");
                sb.Append(Request.Form["chkHot"]);
                sb.Append("</td></tr></table></td></tr></table></td></tr>");
                sb.Append("<tr><td colspan=3 bgcolor=LightGrey>PATIENT INFORMATION</td></tr><tr><!--Patient Information--><td colspan=2><table width='100%' bgcolor=white  border='0' cellpadding=0>");
                sb.Append("<tr><td width='80px'><label style='font-size:12px'>Patient's Name</label> </td><td colspan=7 width=220px>");
                sb.Append(Request.Form["txtPatientsName"]);
                sb.Append("</td><td><label style='font-size:12px'>LEG MEASUREMENTS</label></td><td colspan=5>");
                sb.Append(Request.Form["chkLegMeasurement"]);
                sb.Append("</td></tr><tr><!--Age - Weight-Height-Sex--><td colspan=8 width=300px><table><tr><td><label style='font-size:12px' >Age:</label></td><td>");
                sb.Append(Request.Form["txtAge"]);
                sb.Append("</td><td><label style='font-size:12px'>Weight:</label></td><td>");
                sb.Append(Request.Form["txtWeight"]);
                sb.Append("</td><td><label style='font-size:12px'>Height:</label></td><td>");
                sb.Append(Request.Form["txtHeightFeet"]);
                sb.Append("' </td><td>");
                sb.Append(Request.Form["txtHeightInches"]);
                sb.Append("''</td><td><label style='font-size:12px'>Sex:</label></td><td>");
                sb.Append(Request.Form["cbxSex"]);
                sb.Append("</td></tr><tr><td colspan=2 width='100'><label style='font-size:12px'>BRACE FOR:</label></td><td colspan=6>");

                //Next lines are for bracing
                if (Request.Form["LeftLeg"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='Left Leg' />Left Leg");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='Left Leg' />Left Leg");
                }

                if (Request.Form["RightLeg"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='Right Leg' />Right Leg");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='Right Leg' />Right Leg");
                }    
            
            
            
            //if (Request.Form["chkBraceFor"] == "Left")
            //        {
            //            sb.Append("<asp:CheckBoxList runat=server ID='chkBraceFor'  RepeatDirection=Horizontal Font-Size='12px' Enabled=false ><asp:ListItem Value='Left' Selected=true>Left Leg</asp:ListItem>");
            //            sb.Append("<asp:ListItem Value='Right'>Right Leg</asp:ListItem></asp:CheckBoxList>");
            //        }
            //    else
            //        {
            //            sb.Append("<asp:CheckBoxList runat=server ID='chkBraceFor'  RepeatDirection=Horizontal Font-Size='12px' Enabled=false ><asp:ListItem Value='Left' >Left Leg</asp:ListItem>");
            //            sb.Append("<asp:ListItem Value='Right' Selected=true>Right Leg</asp:ListItem></asp:CheckBoxList>");
            //        }

                sb.Append("</td></tr><tr><td colspan=2 width=100><label style='font-size:12px'>INSTABILITY:</label></td><td colspan=6>");

                //Next lines are for instability

                    if (Request.Form["ACL"] == "on")
                    {
                        sb.Append("<input type=checkbox disabled=true checked=true value='ACL' />ACL");
                    }
                    else
                    {
                        sb.Append("<input type=checkbox disabled=true value='ACL' />ACL");
                    }
                    
                    if (Request.Form["PCL"] == "on")
                    {
                        sb.Append("<input type=checkbox disabled=true checked=true value='PCL' />PCL");
                    }
                    else
                    {
                        sb.Append("<input type=checkbox disabled=true value='PCL' />PCL");
                    }
                    
                    if (Request.Form["MCL"] == "on")
                    {
                        sb.Append("<input type=checkbox disabled=true checked=true value='MCL' />MCL/LCL");
                    }
                    else
                    {
                        sb.Append("<input type=checkbox disabled=true value='MCL' />MCL/LCL");
                    }





                sb.Append("</td></tr><tr><td colspan=8><label style='font-size:12px'>*FUSION OA - Medial unloading only.</label></td></tr></table></td><td colspan=6><table width='100%'><tr><td><label style='font-size:12px'>1.</label>");
                sb.Append("</td><td><label style='font-size:12px'>Thigh Circumference</label></td><td>");
                sb.Append(Request.Form["txtThighCircumference"]);


                sb.Append("</td><td width='8px'>&nbsp</td><td><label style='font-size:12px'>3.</label></td><td><label style='font-size:12px'>Knee Offset</label></td><td>");
                sb.Append(Request.Form["txtKneeOffset"]);

                sb.Append("</td><td width='8px'>&nbsp</td></tr><tr><td><label style='font-size:12px'>2.</label></td><td><label style='font-size:12px'>Calf Circumference</label></td><td>");
                sb.Append(Request.Form["txtCalfCircumference"]);

                sb.Append("</td><td width='8px'>&nbsp</td><td><label style='font-size:12px'>4.</label></td><td><label style='font-size:12px'>Knee Width</label></td><td>");
                sb.Append(Request.Form["txtKneeWidth"]);
                sb.Append("</td><td width='8px'>&nbsp</td></tr><tr><td colspan=2><label style='font-size:12px'>Measurements Taken By:</label></td><td colspan=6><asp:TextBox runat=server Width='160px'></asp:TextBox>");
                sb.Append("</td></tr><tr><td colspan=8><label style='font-weight:bold;font-size:14px'>Extension:</label><label style='font-size:14px'>0</label><label style='font-weight:bold;font-size:14px'>*10</label>");
                sb.Append("<label style='font-size:14px'>20 30 40</label><label style='font-weight:bold;font-size:14px'>Flexion:</label><label style='font-size:14px'>45 60 75 90</label></td></tr><tr><td colspan=8>");
                sb.Append("<label style='font-size:10px'>* New braces ship with 10 extension stops installed.</label></td></tr></table></td></tr></table></td><!--Ship Via--><td height='100%' bgcolor=white>&nbsp");
                sb.Append("<table width='100%' bgcolor=white  border='0' cellpadding=0><tr><td bgcolor='lightgrey' valign=top>SHIP VIA</td></tr><tr><td>");
                sb.Append(Request.Form["cbxShipVia"]);

                sb.Append("</td></tr><tr height=100%><td ></td></tr></table></td></tr><tr><td colspan=3 bgcolor=LightGrey >FUSION BRACE INFORMATION</td></tr><tr>");
                sb.Append("<td colspan=2><table bgcolor=white width=100%  cellpadding=0  style='border-bottom-width:thin;border-bottom-style:solid;border-bottom-color:Black;border-left-width:thin;border-left-style:solid;border-left-color:Black;");
                sb.Append("border-right-width:thin;border-right-style:solid;border-right-color:Black;border-top-width:thin;border-top-style:solid;border-top-color:Black'><tr><td bgcolor=LightGrey></td><td bgcolor=LightGrey>");
                sb.Append("Standard Hinge</td><td bgcolor=LightGrey align=center>*OA</td><td bgcolor=LightGrey>NOTES/SPECIAL INSTRUCTIONS</td></tr><!--Second row in Brace Information--> <tr><td><label style='font-size:14px'>FUSION XT</label></td><td>");


                if (Request.Form["chk01200"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='01200' />01200");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='01200' />01200");
                }

                sb.Append("</td><td>");

                if (Request.Form["chk01205"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='01205' />01205");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='01205' />01205");
                }

                sb.Append("</td><td rowspan=4>");
                sb.Append(Request.Form["txtNotes"]);

                sb.Append("</td></tr><tr><td><label style='font-size:14px'>FUSION</label></td><td>");

                if (Request.Form["chk01207"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='01207' />01207");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='01207' />01207");
                }

                sb.Append("</td><td>");

                if (Request.Form["chk01208"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='01208' />01208");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='01208' />01208");
                }


                sb.Append("</td></tr><tr><td><label style='font-size:14px'>FUSION Women's</label></td><td>");
                if (Request.Form["chk01201"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='01201' />01201");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='01201' />01201");
                }

                sb.Append("</td><td>");

                if (Request.Form["chk01204"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='01204' />01204");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='01204' />01204");
                }

                sb.Append("</td></tr><tr><td><label style='font-size:14px'>FUSION Women's White</label></td><td>");

                if (Request.Form["chk01203"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='01203' />01203");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='01203' />01203");
                }

                sb.Append("</td><td>");

                if (Request.Form["chk01202"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='01202' />01202");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='01202' />01202");
                }

                sb.Append("</td></tr></table></td><td rowspan=3 valign=top><!--Fusion Frame Pads--><table bgcolor=white width=190px height='100%' style='border-bottom-width:thin;border-bottom-style:solid;border-bottom-color:Black;");
                sb.Append("border-left-width:thin;border-left-style:solid;border-left-color:Black;border-right-width:thin;border-right-style:solid;border-right-color:Black;border-top-width:thin;border-top-style:solid;border-top-color:Black'>"); 
                sb.Append("<tr><td  bgcolor=LightGrey>FUSION FRAME PADS</td></tr><tr><td><label style='font-style:italic;font-size:12px'>All FUSION braces available with black frame pads only.</label></td></tr><tr><td  bgcolor=LightGrey>");
                sb.Append("FUSION ACCESSORIES</td></tr><tr><td><table><tr><td style='font-size:12px'>");
            
                if (Request.Form["chkSlideGuardML"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='SlideGuard' />Slide Guard, M/L");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='SlideGuard' />Slide Guard, M/L");
                }

                sb.Append("</td><td style='font-size:12px'>(22000)</td></tr><tr><td style='font-size:12px'>");
                
                 if (Request.Form["chkSlideGuardXL"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='SlideGuard' />Slide Guard, XL/XXL");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='SlideGuard' />Slide Guard, XL/XXL");
                }

                sb.Append("</td><td style='font-size:12px'>(22001)</td></tr><tr><td style='font-size:12px'>");

                if (Request.Form["chkCottonUnderSleeve"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='CottonUndersleeve' />Cotton Undersleeve");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='CottonUndersleeve' />Cotton Undesleeve");
                }
                
                sb.Append("</td><td style='font-size:12px'>(0985X)</td></tr><tr><td style='font-size:12px'>");
    
                if (Request.Form["chkNeopreneUndersleeve"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='NeopreneUndersleeve' />Neoprene Undersleeve");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='NeopreneUndersleeve' />Neoprene Undesleeve");
                }

                sb.Append("</td><td style='font-size:12px'>(0735X)</td></tr><tr><td style='font-size:12px'>");


                if (Request.Form["chkFusionBraceCover"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='FUSIONBraceCover' />FUSION Brace Cover");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='FUSIONBraceCover' />FUSION Brace Cover");
                }

                sb.Append("</td><td style='font-size:12px'>(1008X)</td></tr></table></td></tr></table><!--Fusion Frame Pads - END--></td></tr>");
                sb.Append("<!--Second area of Fusion Brace Information--><tr><td colspan=2><table bgcolor=white width=100%  cellpadding=0  style='border-bottom-width:thin;border-bottom-style:solid;border-bottom-color:Black;");
                sb.Append("border-left-width:thin;border-left-style:solid;border-left-color:Black;border-right-width:thin;border-right-style:solid;border-right-color:Black;border-top-width:thin;border-top-style:solid;border-top-color:Black'>");
                sb.Append("<tr> <td><label style='font-size:14px'>FUSION XT Color</label></td><td>");

                if (Request.Form["chk01215"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='01215' />01215");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='01215' />01215");
                }

                sb.Append("</td><td>");

                if (Request.Form["chk01218"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='01218' />01218");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='01218' />01218");
                }

                sb.Append("</td><td rowspan=3><table><tr><td>");

                if (Request.Form["chkForest"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='Forest' />Forest");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='Forest' />Forest");
                }

                sb.Append("</td><td>");

                if (Request.Form["chkOrange"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='Orange' />Orange");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='Orange' />Orange");
                }

                sb.Append("</td><td>");

                if (Request.Form["chkNavy"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='Navy' />Navy");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='Navy' />Navy");
                }
                sb.Append("</td><td>");

                if (Request.Form["chkPink"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='Pink' />Pink");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='Pink' />Pink");
                }

                sb.Append("</td></tr><tr><td>");

                if (Request.Form["chkRoyal"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='Royal' />Royal");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='Royal' />Royal");
                }

                sb.Append("</td><td>");

                if (Request.Form["chkYellow"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='Yellow' />Yellow");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='Yellow' />Yellow");
                }


                sb.Append("</td><td>");

                if (Request.Form["chkSage"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='Sage' />Sage");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='Sage' />Sage");
                }


                sb.Append("</td><td>");

                if (Request.Form["chkCustom"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='Custom' />Custom");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='Custom' />Custom");
                }

                sb.Append("</td></tr><tr><td>");

                if (Request.Form["chkRed"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='Red' />Red");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='Red' />Red");
                }

                sb.Append("</td><td>");

                if (Request.Form["chkCharcoal"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='Charcoal' />Charcoal");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='Charcoal' />Charcoal");
                }

                sb.Append("</td><td>");

                if (Request.Form["chkMauve"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='Muave' />Muave");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='Muave' />Muave");
                }

                sb.Append("</td><td></td></tr><tr><td colspan=4><label style='font-size:12px'>Pantone</label>");

                sb.Append(Request.Form["txtPantone"]);


                sb.Append("</td> </tr></table></td></tr><tr><td><label style='font-size:14px'>FUSION Color</label></td><td>");

                if (Request.Form["chk01209"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='01209' />01209");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='01209' />01209");
                }

                sb.Append("</td><td>");

                if (Request.Form["chk01216"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='01216' />01216");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='01216' />01216");
                }


                sb.Append("</td></tr><tr><td><label style='font-size:14px'>FUSION Women Color</label></td><td>");

                 if (Request.Form["chk01214"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='01214' />01214");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='01214' />01214");
                }

                sb.Append("</td><td>");

                if (Request.Form["chk01217"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='01217' />01217");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='01217' />01217");
                }

                sb.Append("</td></tr></table></td></tr><!--End of second area of Fusion Brace Information--><!--Third area of Fusion Brace Information-->");
                sb.Append("<tr><td colspan=2><table bgcolor=white width=100%  cellpadding=0  style='border-bottom-width:thin;border-bottom-style:solid;border-bottom-color:Black;");
                sb.Append("border-left-width:thin;border-left-style:solid;border-left-color:Black;border-right-width:thin;border-right-style:solid;border-right-color:Black;");
                sb.Append("border-top-width:thin;border-top-style:solid;border-top-color:Black'><tr><td><label style='font-size:14px'>Color Enhancement</label></td><td>");

                if (Request.Form["chk03161"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='03161' />03161");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='03161' />03161");
                }


                sb.Append("</td><td rowspan=3><table><tr><td>");

                if (Request.Form["chkFlames"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='Flames' />Flames");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='Flames' />Flames");
                }

                sb.Append("</td><td>");


                if (Request.Form["chkCamouflage"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='Camouflage' />Camouflage");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='Camouflage' />Camouflage");
                }

                sb.Append("</td></tr><tr><td>");


                if (Request.Form["chkFlag"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='Flag' />Flag");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='Flag' />Flag");
                }

                sb.Append("</td><td>");


                if (Request.Form["chkCustomPattern"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='CustomPattern' />Custom Pattern * ");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='Custom Pattern' />Custom Pattern *");
                }

                sb.Append("</td></tr><tr><td>");


                if (Request.Form["chkRipples"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='Ripples' />Ripples");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='Ripples' />Ripples");
                }

                sb.Append("</td><td>Notes:");
                sb.Append(Request.Form["txtPatternNotes"]);

                sb.Append("</td></tr></table></td></tr><tr><td> <label style='font-size:14px'>Pattern Enhancement</label></td><td>");

                if (Request.Form["chk03162"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='03162' />03162");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='03162' />03162");
                }

                sb.Append("</td></tr><tr><td><label style='font-size:14px'>Custom Color / Pattern Enhancement</label></td><td>");

                if (Request.Form["chk03163"] == "on")
                {
                    sb.Append("<input type=checkbox disabled=true checked=true value='03163' />03163");
                }
                else
                {
                    sb.Append("<input type=checkbox disabled=true value='03163' />03163");
                }


                sb.Append("</td></tr><tr><td colspan=2><label style='font-size:10px;font-style:italic'>*72 hr turnaround on FUSION custom Pantone / custom pattern orders</label>");
                sb.Append("</td></tr></table> </td></tr><!--End of third area of Fusion Brace Information--></table></td></tr></tr> </table>");



                //if (confirmationEmailAddress.Length > 1)
                //{
                //    Email = confirmationEmailAddress;
                //}

                //Send email
                //string MailSubject = "Breg";
                SendEmail(confirmationEmailAddress, sb.ToString(), "Fusion Cutom Knee Brace Order");



        }


        public void SendEmail(String MailTo, string MailBody, string MailSubject)
        {

            //int rowsAffected = 0;
            try
            {
                Database db = DatabaseFactory.CreateDatabase("BregVisionLocalConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("msdb.dbo.sp_send_dbmail");

                db.AddInParameter(dbCommand, "recipients", DbType.String, MailTo);
                db.AddInParameter(dbCommand, "subject", DbType.String, MailSubject);

                db.AddInParameter(dbCommand, "body", DbType.String, MailBody);
                db.AddInParameter(dbCommand, "body_format", DbType.String, "HTML");

                int SQLReturn = db.ExecuteNonQuery(dbCommand);
            }
            catch
            {

            }

        }


    }
}
