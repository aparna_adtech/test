﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Fusion.aspx.cs" Inherits="BregVision.Fusion" %>

<%@ Register Assembly="RadComboBox.Net2" Namespace="Telerik.WebControls" TagPrefix="radC" %>
<%@ Register Assembly="RadInput.Net2" Namespace="Telerik.WebControls" TagPrefix="radI" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Breg Fusion Custom Knee Brace Order Form</title>
</head>
<body>
<form id="Form1" runat=server>
<table width="98%" border="0">
    <tr>
        <td colspan=5>
        FUSION Custom Knee Brace Order Form

        </td>
    </tr>
    <tr>
        <td colspan=5>
        <asp:Label ID="lblStatus" runat=server style="font-size:large;color:Red"></asp:Label>
        </td>
    </tr>

  <tr>
    <td>
        <table  bgcolor=white  cellpadding=0>
            <tr>
                <td>
                   <!--Bill To Area-->
                   <table width="300" border="0" cellpadding=0  bgcolor=white >
                      <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td bgcolor=LightGrey><label id="BillToHeader" style="font-size:12px">BILL TO</label></td>        
                                    </tr>
                                </table>
                             </td>   
                       </tr>
                        <tr>
                            <td>
                              <table border="0"  bgcolor=white width="100%" >
                                  <tr>
                                    <td width="80px"><label id="Label3" style="font-size:12px" >Customer #</label></td>
                                    <td colspan="2">
                                    
                                      <asp:TextBox ID="txtCustomerNo" ToolTip="Enter Customer Number" TextMode="SingleLine" runat="server" Font-Size="12px" Width="90px" />        
       	                            </td>
                                    
                                    <td width="40px"><label id="Label4" style="font-size:12px" >PO #</label></td>
                                    <td colspan="2">
                                      <asp:
                                      <asp:TextBox ID="txtPONo" ToolTip="Enter PO Number" TextMode="SingleLine" runat="server" Width="90px" />        
                                    
                                    </td>
                                  </tr>
                                  <tr>
                                    <td><label style="font-size:12px">Contact</label> </td>
                                    <td colspan="2">
                                    
                                        <asp:TextBox ID="txtBillToContact" ToolTip="Enter contact name" TextMode="SingleLine" runat="server" Font-Size="12px" Width="90px" />        
                                    
                                    </td>
                                    <td><label style="font-size:12px">Tel #</label></td>
                                    <td  colspan="2">
                                        <radI:RadMaskedTextBox ID="txtBillToPhone2" runat=server  Mask="(###) ###-####"  skin="Office2007" width="95px"></radI:RadMaskedTextBox>

                                    </td>
                                  </tr>
                                  <tr>
                                    <td><label style="font-size:12px">Address</label> </td>
                                    <td colspan="5">
                                        <asp:TextBox ID="txtBillToAddress" ToolTip="Enter address info" TextMode="SingleLine" runat="server" Font-Size="12px" Width="97%" />        
                                    </td>
                                  </tr>

                              </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                              <table bgcolor=white>
                                  <tr>
                                    <td width="22"><label style="font-size:12px">City</label></td>
                                    <td width="123">
                                        <asp:TextBox ID="txtBillToCity" ToolTip="Enter city info" TextMode="SingleLine" runat="server" Font-Size="12px" Width="110px" />        
                                    </td>
                                    <td width="26"><label style="font-size:12px">State</label></td>
                                    <td width="27">
                                         <radC:RadComboBox ID="cbxBillToState"  runat="server"  Skin="WebBlue"  
                                             SkinsPath="~/RadControls/ComboBox/Skins" Visible=true Width="50px"
                                             ExpandEffect=Fade
                                             ShowWhileLoading="False" ToolTip="Select a state."  AutoPostBack=false  MarkFirstMatch=true>
                                        </radC:RadComboBox>
                                    </td>
                                    <td width="21"><label style="font-size:12px">Zip</label></td>
                                    <td width="56">
                                        <asp:TextBox ID="txtBillToZip" ToolTip="Enter zip code info" TextMode="SingleLine" runat="server" MaxLength=5 Font-Size="12px" Width="37px" />        
                                    </td>
                                  </tr>
                              </table>
                            
                            
                            </td>
                        </tr>
                  
                   </table>
                 </td>
                 <!--Spacer-->
     
                  <td>
                   <!--Bill To Area-->
                   <table width="300" border="0" cellpadding=0  bgcolor=white >
                      <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td bgcolor=LightGrey><label id="Label1" style="font-size:12px">SHIP T0 (if different)</label>
                                        <asp:CheckBox runat=server ID="chkSHipSameAsBill" /><asp:label runat=server Font-Size=Small >Same as Billing</asp:label>
                                        </td>        
                                    </tr>
                                </table>
                             </td>   
                       </tr>
                        <tr>
                            <td>
                              <table border="0"  bgcolor=white width="100%" >
                                  <tr>
                                    <td width="45"><label style="font-size:12px">Name</label> </td>
                                    <td align=left>
                                        <asp:TextBox ID="txtShipToName" ToolTip="Enter name" TextMode="SingleLine" runat="server" Font-Size="12px" Width="97%" />        
                                    </td>
                                  </tr>

                                  <tr>
                                    <td><label style="font-size:12px">Address</label> </td>
                                    <td>
                                        <asp:TextBox ID="txtShipToAddress1" ToolTip="Enter address info" TextMode="SingleLine" runat="server" Font-Size="12px" Width="97%" />        
                                    </td>
                                  </tr>
                                  <tr>
                                    <td><label style="font-size:12px"></label> </td>
                                    <td>
                                        <asp:TextBox ID="txtShipToAddress2" ToolTip="Enter address info" TextMode="SingleLine" runat="server" Font-Size="12px" Width="97%" />        
                                    </td>
                                  </tr>
                              </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                              <table bgcolor=white>
                                  <tr>
                                    <td width="22"><label style="font-size:12px">City</label></td>
                                    <td width="123">
                                        <asp:TextBox ID="txtShipToCity" ToolTip="Enter city info" TextMode="SingleLine" runat="server" Font-Size="12px" Width="110px" />        
                                    </td>
                                    <td width="26"><label style="font-size:12px">State</label></td>
                                    <td width="27">
                                        <radC:RadComboBox ID="cbxShipToState"  runat="server"  Skin="WebBlue"  
                                             SkinsPath="~/RadControls/ComboBox/Skins" Visible=true Width="50px"
                                             ExpandEffect=Fade 
                                             ShowWhileLoading="False" ToolTip="Select a state." AutoPostBack=false MarkFirstMatch=true>
                                        </radC:RadComboBox>

                                    </td>
                                    <td width="21"><label style="font-size:12px">Zip</label></td>
                                    <td width="56">
                                        <asp:TextBox ID="txtShipToZip" ToolTip="Enter zip code info" TextMode="SingleLine" runat="server" MaxLength=5 Font-Size="12px" Width="37px" />        
                                    </td>
                                  </tr>
                              </table>
                            
                            
                            </td>
                        </tr>
                  
                   </table>
                 </td>
                 <!--Spacer-->
     
                 <td height="100%">
                    <!--For Office Use Only Area-->
                   <table width="177" border="0" cellpadding=0  bgcolor=white >
                      <tr>
                            <td height="100%">
                                <table>
                                    <tr>
                                        <td bgcolor=LightGrey><label id="Label2" style="font-size:12px">FOR OFFICE USE ONLY</label></td>        
                                    </tr>
                                </table>
                             </td>   
                       </tr>
                        <tr>
                            <td>
                              <table border="0"  bgcolor=white width="100%" >
                                  <tr>
                                    <td width="45"><label style="font-size:10px">DATE/TIME:</label> </td>
                                    <td align=left>
                                        <asp:Label ID="lblDateTime" runat=server style="font-size:10px></asp:Label>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td><label style="font-size:12px">Order #</label> </td>
                                    <td>
                                        <asp:TextBox ID="txtOrderNo" ToolTip="Enter Order Number" TextMode="SingleLine" runat="server" Font-Size="12px" Width="97%" />        
                                    </td>
                                  </tr>
                                  <tr>
                                    <td><label style="font-size:12px">RA #</label> </td>
                                    <td>
                                        <asp:TextBox ID="txtRANo" ToolTip="Enter RA Number" TextMode="SingleLine" runat="server" Font-Size="12px" Width="97%" />        
                                    </td>
                                  </tr>
                              </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                              <table bgcolor=white>
                                  <tr>
                                    <td>
                                        <asp:CheckBox runat=server ID="chkNewAccount" Text="NEW ACCOUNT"  Font-Size="12px" Enabled=false />
                                    </td>
                                    <td>
                                        <asp:CheckBox runat=server id="chkHot" Text="HOT"  Font-Size="12px" Enabled=false />
                                    </td>

                                  </tr>
                              </table>
                            
                            
                            </td>
                        </tr>
                        
                   </table>
                 </td>
            </tr>
            <tr>
                <td colspan=3 bgcolor=LightGrey>
                 PATIENT INFORMATION
                </td>
            </tr>
            <tr>
                <!--Patient Information-->
                <td colspan=2>
                    <table width="100%" bgcolor=white  border="0" cellpadding=0>                    
                        <tr>
                            <td width="80px">
                                <label style="font-size:12px">Patient's Name</label> 
                            </td>
                            <td colspan=7 width=220px>
                                <asp:TextBox ID="txtPatientsName" ToolTip="Enter name" TextMode="SingleLine" runat="server" Font-Size="12px" Width="97%" />        
                            </td>
                            <td>
                                <label style="font-size:12px">LEG MEASUREMENTS</label> 
                            </td>
                            <td colspan=5>
                                <asp:RadioButton  ID='chkBilat' runat=server Text='Bilat' Font-Size=Small 
                                    GroupName=chkLegMeasurement oncheckedchanged="chkBilat_CheckedChanged"/>
                                <asp:RadioButton  ID='chkCast' runat=server Text='Cast' Font-Size=Small GroupName=chkLegMeasurement />
                                <asp:RadioButton  ID='chkReform' runat=server Text='Reform' Font-Size=Small GroupName=chkLegMeasurement />
                            </td>
                        </tr>
                        <tr>
                            <td colspan=8>
                            </td>
                            <td colspan=6>
                                
                                <input type=radio id=radLegsSame name=chkLegMeasurement visible=false  style="font-size:10px" runat=server onclick="chkLegMeasurement_onClick" /><label style="font-size:12px">Both Legs same?</label> 
                            </td>
                        </tr>
                        <tr>
                            <!--Age - Weight-Height-Sex-->
                            <td colspan=8 width=300px>
                                <table>
                                 <tr>
                                    <td>
                                        <label style="font-size:12px" >Age:</label>
                                    </td>
                                    <td>    
                                         
                                         <radI:RadNumericTextBox  ID="txtAge" Value="0" MaxValue=999 runat="server" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="25px" NumberFormat-DecimalDigits="0">
                                            <NumberFormat DecimalDigits="0" />
                                        </radI:RadNumericTextBox>
                                    </td>
                                    <td>    
                                         
                                         
                                        <label style="font-size:12px">Weight:</label>
                                    </td>
                                    <td>    

                                         <radI:RadNumericTextBox  ID="txtWeight"  Value="0" MaxValue=999 runat="server" ToolTip="Weight in lbs."  ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default"  MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="25px" NumberFormat-DecimalDigits="0">
                                            <NumberFormat DecimalDigits="0" />
                                        </radI:RadNumericTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    
                                    <td>   
                                     


                                        <label style="font-size:12px">Height:</label>
                                    </td>
                                    <td>    

                                         <radI:RadNumericTextBox  ID="txtHeightFeet"  Value="0" MaxValue=9 runat="server" ToolTip="Height in Feet" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="25px" NumberFormat-DecimalDigits="0">
                                            <NumberFormat DecimalDigits="0" />
                                        </radI:RadNumericTextBox>
                                    </td>
                                    <td>    

                                         <radI:RadNumericTextBox  ID="txtHeightInches"  Value="0" MaxValue=12 runat="server"  ToolTip="Height in Inches" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default"  MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="25px" NumberFormat-DecimalDigits="0">
                                            <NumberFormat DecimalDigits="0" />
                                        </radI:RadNumericTextBox>
                                    </td>

                                    <td>    


                                        <label style="font-size:12px">Sex:</label>
                                    </td>
                                    <td>    
                                         
                                        <radC:RadComboBox ID="cbxSex"  runat="server"  Skin="WebBlue"  
                                             SkinsPath="~/RadControls/ComboBox/Skins" Visible=true Width="50px"
                                             ExpandEffect=Fade
                                             ShowWhileLoading="False" ToolTip="Select male or female."  AutoPostBack=false  MarkFirstMatch=true>
                                             
                                             <Items>
                                                <radC:RadComboBoxItem Text="Male"/>
                                                <radC:RadComboBoxItem Text="Female"/>
                                             </Items>
                                             
                                        </radC:RadComboBox>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td colspan=2 width="100">
                                        <label style="font-size:12px">BRACE FOR:</label>
                                    </td>
                                    <td colspan=6>
                                        <input type="radio" id="LeftLeg" name="BraceFor" /><label style="font-size:12px">Left Leg</label>
                                        <input type="radio" id="RightLeg" name="BraceFor" /><label style="font-size:12px">Right Leg</label>
                     
                                    </td>
                                 </tr>
                                 <tr>
                                    <td colspan=2 width=100>
                                        <label style="font-size:12px">INSTABILITY:</label>
                                    </td>
                                    <td colspan=6>
                                        <input type="checkbox" id="radACL" name="Instability" /><label style="font-size:12px">ACL</label>
                                        <input type="checkbox" id="radPCL" name="Instability" /><label style="font-size:12px">PCL</label>
                                        <input type="checkbox" id="radMCL" name="Instability" /><label style="font-size:12px">MCL/LCL</label>
             
                                    </td>
                                 </tr>
                                 <tr>
                                    <td colspan=8>
                                        <label style="font-size:12px">*FUSION OA - Medial unloading only.</label>
                                    </td>
                                 </tr>

                                </table>
                            </td>

                            <td colspan=6>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <label style="font-size:12px">1.</label>
                                        </td>
                                        <td>
                                            <label style="font-size:12px">Thigh Circumference</label>
                                        </td>
                                        <td>

                                            <radI:RadNumericTextBox  ID="txtThighCircumference" Value="0"  runat="server"  ToolTip="Thigh Circumference in Inches" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="32" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="25px" NumberFormat-DecimalDigits="0">
                                                <NumberFormat DecimalDigits="0" />
                                            </radI:RadNumericTextBox>
                                         
                                         </td>
                                        <td>
                                           <radC:RadComboBox ID="radThighCircumferenceInches"  runat="server"  Skin="WebBlue"  
                                                             SkinsPath="~/RadControls/ComboBox/Skins" Visible=true Width="53px"
                                                             ExpandEffect=Fade
                                                             ShowWhileLoading="False" ToolTip="Select Size. All measurements are in inches"  AutoPostBack=false  MarkFirstMatch=true>
                                                             
                                                             <Items>
                                                                <radC:RadComboBoxItem Text="0"/>
                                                                <radC:RadComboBoxItem Text="1/16"/>
                                                                <radC:RadComboBoxItem Text="2/16"/>
                                                                <radC:RadComboBoxItem Text="3/16"/>
                                                                <radC:RadComboBoxItem Text="4/16"/>
                                                                <radC:RadComboBoxItem Text="5/16"/>
                                                                <radC:RadComboBoxItem Text="6/16"/>
                                                                <radC:RadComboBoxItem Text="7/16"/>
                                                                <radC:RadComboBoxItem Text="8/16"/>                                                            
                                                                <radC:RadComboBoxItem Text="9/16"/>
                                                                <radC:RadComboBoxItem Text="10/16"/>
                                                                <radC:RadComboBoxItem Text="11/16"/>
                                                                <radC:RadComboBoxItem Text="12/16"/>     
                                                                <radC:RadComboBoxItem Text="13/16"/>     
                                                                <radC:RadComboBoxItem Text="14/16"/>   
                                                                <radC:RadComboBoxItem Text="15/16"/>   
                                                             </Items>
                                                             
                                            </radC:RadComboBox>
                                            </td>
                                        <td width="8px">&nbsp
                                        </td>
                                        <td>
                                            <label style="font-size:12px">3.</label>
                                        </td>
                                        <td>
                                            <label style="font-size:12px">Knee Offset</label>
                                        </td>
                                        <td>
                                             <radI:RadNumericTextBox  ID="txtKneeOffset"  Value="0" runat="server"  ToolTip="Knee Offset in Inches" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="29" MinValue="-29" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="25px" NumberFormat-DecimalDigits="0">
                                                <NumberFormat DecimalDigits="0" />
                                            </radI:RadNumericTextBox>
                                        
                                        </td>
                                        <td>
                                           <radC:RadComboBox ID="radKneeOffset"  runat="server"  Skin="WebBlue"  
                                                             SkinsPath="~/RadControls/ComboBox/Skins" Visible=true Width="53px"
                                                             ExpandEffect=Fade
                                                             ShowWhileLoading="False" ToolTip="Select Size. All measurements are in inches"  AutoPostBack=false  MarkFirstMatch=true>
                                                             
                                                             <Items>
                                                                <radC:RadComboBoxItem Text="0"/>
                                                                <radC:RadComboBoxItem Text="1/16"/>
                                                                <radC:RadComboBoxItem Text="2/16"/>
                                                                <radC:RadComboBoxItem Text="3/16"/>
                                                                <radC:RadComboBoxItem Text="4/16"/>
                                                                <radC:RadComboBoxItem Text="5/16"/>
                                                                <radC:RadComboBoxItem Text="6/16"/>
                                                                <radC:RadComboBoxItem Text="7/16"/>
                                                                <radC:RadComboBoxItem Text="8/16"/>                                                            
                                                                <radC:RadComboBoxItem Text="9/16"/>
                                                                <radC:RadComboBoxItem Text="10/16"/>
                                                                <radC:RadComboBoxItem Text="11/16"/>
                                                                <radC:RadComboBoxItem Text="12/16"/>     
                                                                <radC:RadComboBoxItem Text="13/16"/>     
                                                                <radC:RadComboBoxItem Text="14/16"/>   
                                                                <radC:RadComboBoxItem Text="15/16"/>   
                                                             </Items>
                                                             
                                            </radC:RadComboBox>
                                        </td>
                                        <td width="8px">&nbsp
                                        </td>
                                    </tr>                 
                                     <tr>
                                        <td>
                                            <label style="font-size:12px">2.</label>
                                        </td>
                                        <td>
                                            <label style="font-size:12px">Calf Circumference</label>
                                        </td>
                                        <td>
                                             <radI:RadNumericTextBox  ID="txtCalfCircumference"  Value="0" runat="server"  ToolTip="Calf Circumference in Inches" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="24" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="25px" NumberFormat-DecimalDigits="0">
                                                <NumberFormat DecimalDigits="0" />
                                            </radI:RadNumericTextBox>
                                        
                                        </td>
                                        <td>
                                           <radC:RadComboBox ID="RadCalfCircumference"  runat="server"  Skin="WebBlue"  
                                                             SkinsPath="~/RadControls/ComboBox/Skins" Visible=true Width="53px"
                                                             ExpandEffect=Fade
                                                             ShowWhileLoading="False" ToolTip="Select Size. All measurements are in inches"  AutoPostBack=false  MarkFirstMatch=true>
                                                             
                                                             <Items>
                                                                <radC:RadComboBoxItem Text="0"/>
                                                                <radC:RadComboBoxItem Text="1/16"/>
                                                                <radC:RadComboBoxItem Text="2/16"/>
                                                                <radC:RadComboBoxItem Text="3/16"/>
                                                                <radC:RadComboBoxItem Text="4/16"/>
                                                                <radC:RadComboBoxItem Text="5/16"/>
                                                                <radC:RadComboBoxItem Text="6/16"/>
                                                                <radC:RadComboBoxItem Text="7/16"/>
                                                                <radC:RadComboBoxItem Text="8/16"/>                                                            
                                                                <radC:RadComboBoxItem Text="9/16"/>
                                                                <radC:RadComboBoxItem Text="10/16"/>
                                                                <radC:RadComboBoxItem Text="11/16"/>
                                                                <radC:RadComboBoxItem Text="12/16"/>     
                                                                <radC:RadComboBoxItem Text="13/16"/>     
                                                                <radC:RadComboBoxItem Text="14/16"/>   
                                                                <radC:RadComboBoxItem Text="15/16"/>   
                                                             </Items>
                                                             
                                            </radC:RadComboBox>
                                        </td>
                                        <td width="8px">&nbsp
                                        </td>
                                        <td>
                                            <label style="font-size:12px">4.</label>
                                        </td>
                                        <td>
                                            <label style="font-size:12px">Knee Width</label>
                                        </td>
                                        <td>
                                             <radI:RadNumericTextBox  ID="txtKneeWidth"  Value="0"  runat="server"  ToolTip="Knee Width in Inches" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="175" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="25px" NumberFormat-DecimalDigits="0">
                                                <NumberFormat DecimalDigits="0" />
                                            </radI:RadNumericTextBox>
                                        
                                        </td>
                                        <td>
                                           <radC:RadComboBox ID="RadKneeWidth"  runat="server"  Skin="WebBlue"  
                                                             SkinsPath="~/RadControls/ComboBox/Skins" Visible=true Width="53px"
                                                             ExpandEffect=Fade
                                                             ShowWhileLoading="False" ToolTip="Select Size. All measurements are in inches"  AutoPostBack=false  MarkFirstMatch=true>
                                                             
                                                             <Items>
                                                                <radC:RadComboBoxItem Text="0"/>
                                                                <radC:RadComboBoxItem Text="1/16"/>
                                                                <radC:RadComboBoxItem Text="2/16"/>
                                                                <radC:RadComboBoxItem Text="3/16"/>
                                                                <radC:RadComboBoxItem Text="4/16"/>
                                                                <radC:RadComboBoxItem Text="5/16"/>
                                                                <radC:RadComboBoxItem Text="6/16"/>
                                                                <radC:RadComboBoxItem Text="7/16"/>
                                                                <radC:RadComboBoxItem Text="8/16"/>                                                            
                                                                <radC:RadComboBoxItem Text="9/16"/>
                                                                <radC:RadComboBoxItem Text="10/16"/>
                                                                <radC:RadComboBoxItem Text="11/16"/>
                                                                <radC:RadComboBoxItem Text="12/16"/>     
                                                                <radC:RadComboBoxItem Text="13/16"/>     
                                                                <radC:RadComboBoxItem Text="14/16"/>   
                                                                <radC:RadComboBoxItem Text="15/16"/>   
                                                             </Items>
                                                             
                                            </radC:RadComboBox>
                                        </td>
                                        <td width="8px">&nbsp
                                        </td>
                                    </tr>           
                                    <tr>
                                        <td colspan=3>
                                            <label style="font-size:12px">Measurements Taken By:</label>
                                        </td>
                                        <td colspan=6>
                                            <asp:TextBox ID="txtTakenBy" runat=server Width="160px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan=8>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <label style="font-weight:bold;font-size:12px">Extension:</label>
                                                     </td>
                                                     <td>
                                                        <input type="radio" id="Rad0" name="Extension" /><label style="font-size:12px">0</label>
                                                     </td>
                                                     <td>
                                                        <input type="radio" id="Radio10" checked name="Extension" /><label style="font-size:12px">10</label>
                                                     </td>
                                                     <td>
                                                        <input type="radio" id="Radio20" name="Extension" /><label style="font-size:12px">20</label>
                                                     </td>
                                                     <td>
                                                        <input type="radio" id="Radio30" name="Extension" /><label style="font-size:12px">30</label>
                                                     </td>
                                                     <td>
                                                        <input type="radio" id="Radio40" name="Extension" /><label style="font-size:12px">40</label>
                                                     </td>

                                                  </tr>   
                                                  <tr>
                                                    <td>
                                                        <label style="font-weight:bold;font-size:12px">Flexion:</label>
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="Rad45" name="Extension" /><label style="font-size:12px">45</label>
                                                     </td>
                                                     <td>
                                                        <input type="radio" id="Radio60" name="Extension" /><label style="font-size:12px">60</label>
                                                     </td>
                                                     <td>
                                                        <input type="radio" id="Radio75" name="Extension" /><label style="font-size:12px">75</label>
                                                     </td>
                                                     <td colspan=2>
                                                        <input type="radio" id="Radio90" name="Extension" /><label style="font-size:12px">90</label>
                                                    </td>
                                                   </tr>
                                                </table> 

                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan=8>
                                            <label style="font-size:10px">* New braces ship with 10 extension stops installed.</label>
                                        </td>
                                    </tr>
                                    
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <!--Ship Via-->

                <td height="100%" bgcolor=white valign=top>&nbsp
                  <table width="100%" bgcolor=white  border="0" cellpadding=0>                    
                    <tr>
                        <td bgcolor="lightgrey" valign=top>
                        SHIP VIA
                        </td>
                    </tr>
                    <tr>
                        <td>
                            
                           <radC:RadComboBox ID="cbxShipVia"  runat="server"  Skin="WebBlue"  
                                             SkinsPath="~/RadControls/ComboBox/Skins" Visible=true Width="140px"
                                             ExpandEffect=Fade
                                             ShowWhileLoading="False" ToolTip="Select male or female."  AutoPostBack=false  MarkFirstMatch=true>
                                             
                                             <Items>
                                                <radC:RadComboBoxItem Text="Saturday"/>
                                                <radC:RadComboBoxItem Text="Next Day Early AM"/>
                                                <radC:RadComboBoxItem Text="Next Day AM"/>
                                                <radC:RadComboBoxItem Text="Next Day PM"/>
                                                <radC:RadComboBoxItem Text="2nd Day"/>
                            
                                               <radC:RadComboBoxItem Text="3rd Day"/>
                                                <radC:RadComboBoxItem Text="Ground"/>
                                             </Items>
                                             
                            </radC:RadComboBox>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            
                            <asp:RadioButton ID="radUPS" runat=server GroupName="Shipping" Text="UPS"  Font-Size="12px"/>
                            <asp:RadioButton ID="radFedEx" runat=server GroupName="Shipping" Text="Fed Ex"  Font-Size="12px"/>
                            
                        </td>
                    </tr>
                    <tr height=100%>
                        <td ></td>
                    </tr>
                  </table>
                </td>   
            </tr>
            <tr>
                <td colspan=3 bgcolor=LightGrey >
                 FUSION BRACE INFORMATION
                 
                </td>
            </tr>

            <tr>
             
                        <td colspan=2>
                            <table bgcolor=white width=100%  cellpadding=0  style="border-bottom-width:thin;border-bottom-style:solid;border-bottom-color:Black;
                            border-left-width:thin;border-left-style:solid;border-left-color:Black;
                            border-right-width:thin;border-right-style:solid;border-right-color:Black;
                            border-top-width:thin;border-top-style:solid;border-top-color:Black"> 
                                <tr>
                                    <td bgcolor=LightGrey>
                                    </td>
                                    <td bgcolor=LightGrey>
                                        Standard Hinge
                                    </td>
                                    <td bgcolor=LightGrey align=center>
                                        *OA
                                    </td>
                                    <td bgcolor=LightGrey>
                                        NOTES/SPECIAL INSTRUCTIONS
                                    </td>

                                </tr>
                                <!--Second row in Brace Information--> 

                                <tr>
                                    <td>
                                        <label style="font-size:14px">FUSION XT</label>                                        
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01200" runat=server GroupName="Brace" Text="01200"  Font-Size="14px"/>
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01205" runat=server GroupName="Brace" Text="01205"  Font-Size="14px"/>
                                    </td>
                                    <td rowspan=4><asp:TextBox ID=txtNotes runat=server TextMode=MultiLine Width=300px Height=100px></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-size:14px">FUSION</label>                                        
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01207" runat=server GroupName="Brace" Text="01207"  Font-Size="14px"/>
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01208" runat=server GroupName="Brace" Text="01208"  Font-Size="14px"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-size:14px">FUSION Women's</label>                                        
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01201" runat=server GroupName="Brace" Text="01201"  Font-Size="14px"/>
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01204" runat=server GroupName="Brace" Text="01204"  Font-Size="14px"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-size:14px">FUSION Women's White</label>                                        
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01203" runat=server GroupName="Brace" Text="01203" Font-Size="14px"/>

                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01202" runat=server GroupName="Brace" Text="01202"  Font-Size="14px"/>

                                    </td>
                                </tr>
                                
                                
                                
                                
                            </table>
                        </td>
                        <td rowspan=3 valign=top>
                            <!--Fusion Frame Pads-->
                            <table bgcolor=white width=225px height="100%" style="border-bottom-width:thin;border-bottom-style:solid;border-bottom-color:Black;
                            border-left-width:thin;border-left-style:solid;border-left-color:Black;
                            border-right-width:thin;border-right-style:solid;border-right-color:Black;
                            border-top-width:thin;border-top-style:solid;border-top-color:Black"> 
                                <tr>
                                    <td  bgcolor=LightGrey>
                                        FUSION FRAME PADS
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style=" font-style:italic;font-size:12px">All FUSION braces available with black frame pads only.</label>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td  bgcolor=LightGrey>
                                        FUSION ACCESSORIES
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table> 
                                            <tr>
                                                <td>
                                                   <radI:RadNumericTextBox  ID="radSlideGuardML"  Value="1"  runat="server" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="25px" NumberFormat-DecimalDigits="0">
                                                    <NumberFormat DecimalDigits="0" />
                                                   </radI:RadNumericTextBox>
                                                </td>
                                                <td style="font-size:12px">
                                                    <asp:CheckBox ID="chSlideGuardML" runat=server Text="Slide Guard, M/L"  Font-Size="12px" />
                                                    
                                                </td>
                                                <td style="font-size:12px">(22000)
                                                </td>    
                                            </tr>
                                            <tr>
                                                <td>
                                                   <radI:RadNumericTextBox  ID="radSlideGuardXL"  Value="1"  runat="server" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="25px" NumberFormat-DecimalDigits="0">
                                                    <NumberFormat DecimalDigits="0" />
                                                   </radI:RadNumericTextBox>
                                                </td>                                                
                                                <td style="font-size:12px">
                                                    <asp:Checkbox ID="chkSlideGuardXL" runat=server Text="Slide Guard, XL/XXL"  Font-Size="12px" />
                                                    
                                                </td>
                                                <td style="font-size:12px">(22001)
                                                </td>    
                                            </tr>
                                            <tr>
                                                <td>
                                                   <radI:RadNumericTextBox  ID="radCottonUndersleeve"  Value="1"  runat="server" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="25px" NumberFormat-DecimalDigits="0">
                                                    <NumberFormat DecimalDigits="0" />
                                                   </radI:RadNumericTextBox>
                                                </td>                                            
                                                <td style="font-size:12px">
                                                    <asp:Checkbox ID="chkCottonUndersleeve" runat=server Text="Cotton Undersleeve"  Font-Size="12px"/>
                                                    
                                                </td>
                                                <td style="font-size:12px">(0985X)
                                                </td>    
                                            </tr>
                                            <tr>
                                             <td>
                                                   <radI:RadNumericTextBox  ID="radNeoprene"  Value="1"  runat="server" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="25px" NumberFormat-DecimalDigits="0">
                                                    <NumberFormat DecimalDigits="0" />
                                                   </radI:RadNumericTextBox>
                                                </td>      
                                                <td style="font-size:12px">
                                                    <asp:Checkbox ID="chkNeoprene" runat=server  Text="Neoprene Undersleeve"  Font-Size="12px"/>
                                                    
                                                </td>
                                                <td style="font-size:12px">(0735X)
                                                </td>    
                                            </tr>
                                            <tr>
                                             <td>
                                                   <radI:RadNumericTextBox  ID="radFusionBraceCover"  Value="1"  runat="server" ShowSpinButtons="True" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="25px" NumberFormat-DecimalDigits="0">
                                                    <NumberFormat DecimalDigits="0" />
                                                   </radI:RadNumericTextBox>
                                                </td>      
                                                <td style="font-size:12px">
                                                    
                                                    <asp:Checkbox ID="chkFusionBraceCover" runat=server Text="FUSION Brace Cover"  Font-Size="12px"/>
                                                    
                                                </td>
                                                <td style="font-size:12px">(1008X)
                                                </td>    
                                            </tr>
                                        </table>
                                    </td>    
                                </tr>
                            </table>
                            <!--Fusion Frame Pads - END-->
                        </td>
                        </tr>
                        <!--Second area of Fusion Brace Information-->
                        
                        <tr>
                           <td colspan=2>
                            <table bgcolor=white width=100%  cellpadding=0  style="border-bottom-width:thin;border-bottom-style:solid;border-bottom-color:Black;
                            border-left-width:thin;border-left-style:solid;border-left-color:Black;
                            border-right-width:thin;border-right-style:solid;border-right-color:Black;
                            border-top-width:thin;border-top-style:solid;border-top-color:Black"> 
                                
                                <tr>
                                    <td>
                                        <label style="font-size:14px">FUSION XT Color</label>                                        
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01215a" runat=server GroupName="Brace" Text="01215" Font-Size="14px"/>
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01218a" runat=server GroupName="Brace" Text="01218" Font-Size="14px"/>
                                    </td>
                                    <td rowspan=3>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="chkForest" runat=server GroupName="BraceColor" Text="Forest" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkOrange" runat=server GroupName="BraceColor" Text="Orange" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkNavy" runat=server GroupName="BraceColor" Text="Navy" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkPink" runat=server GroupName="BraceColor" Text="Pink" Font-Size="14px"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="chkRoyal" runat=server GroupName="BraceColor" Text="Royal" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkYellow" runat=server GroupName="BraceColor" Text="Yellow" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkSage" runat=server GroupName="BraceColor" Text="Sage" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkRed" runat=server GroupName="BraceColor" Text="Red" Font-Size="14px"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="chkCharcoal" runat=server GroupName="BraceColor" Text="Charcoal" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkMauve" runat=server GroupName="BraceColor" Text="Mauve" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    
                                                </td>
                                                <td>
                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan=4>
                                                    <asp:RadioButton ID="chkCustom" runat=server GroupName="BraceColor" Text="Custom" Font-Size="14px"/>
                                                     <label style="font-size:12px">Pantone</label>
                                                     <asp:TextBox ID=txtPantone runat=server Width=100px></asp:TextBox>    
                                                </td>
                                            </tr>
                                        </table>



                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-size:14px">FUSION Color</label>                                        
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01209a" runat=server GroupName="Brace" Text="01209" Font-Size="14px"/>
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01216a" runat=server GroupName="Brace" Text="01216" Font-Size="14px"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-size:14px">FUSION Women Color</label>                                        
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01214a" runat=server GroupName="Brace" Text="01214" Font-Size="14px" />
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="chk01217a" runat=server GroupName="Brace" Text="01217" Font-Size="14px"/>
                                    </td>
                                </tr>
                        
                                
                                
                                
                            </table>
                        </td>
                        </tr>
                        <!--End of second area of Fusion Brace Information-->
                        
                       <!--Third area of Fusion Brace Information-->
                        
                        <tr>
                           <td colspan=2>
                            <table bgcolor=white width=100%  cellpadding=0  style="border-bottom-width:thin;border-bottom-style:solid;border-bottom-color:Black;
                            border-left-width:thin;border-left-style:solid;border-left-color:Black;
                            border-right-width:thin;border-right-style:solid;border-right-color:Black;
                            border-top-width:thin;border-top-style:solid;border-top-color:Black"> 
                                
                                <tr>
                                    <td style="width: 261px">
                                        <label style="font-size:14px">Color Enhancement</label>                                        
                                    </td>
                                    <td style="width: 59px">
                                        <asp:RadioButton ID="chk03161" runat=server GroupName="Enhancement" Text="03161" Font-Size="14px"/>
                                    </td>
                                   
                                    <td rowspan=3>
                                    
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="chkFlames" runat=server GroupName="Pattern" Text="Flames" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkCamouflage" runat=server GroupName="Pattern" Text="Camouflage" Font-Size="14px"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="chkFlag" runat=server GroupName="Pattern" Text="Flag" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="chkRipples" runat=server GroupName="Pattern" Text="Ripples" Font-Size="14px"/>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="chkCustomPattern" runat=server GroupName="Pattern" Text="Custom Pattern *" Font-Size="14px"/>
                                                </td>
                                                <td>
                                                    Notes: <asp:TextBox ID=txtPatternNotes runat=server Width=100px></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                                
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 261px">
                                        <label style="font-size:14px">Pattern Enhancement</label>                                        
                                    </td>
                                    <td style="width: 59px">
                                        <asp:RadioButton ID="chk03162" runat=server GroupName="Enhancement" Text="03162" Font-Size="14px"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 261px">
                                        <label style="font-size:14px">Custom Color / Pattern Enhancement</label>                                        
                                    </td>
                                    <td style="width: 59px">
                                        <asp:RadioButton ID="chk03163" runat=server GroupName="Enhancement" Text="03163" Font-Size="14px"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=2>
                                        <label style="font-size:10px;font-style:italic">*72 hr turnaround on FUSION custom Pantone / custom pattern orders</label>                                        
                                    </td>
                                </tr>
                        
                                
                                
                                
                            </table>
                        </td>
                        </tr>
                        <!--End of third area of Fusion Brace Information-->
                    
                        <tr> 
                            <td align=right colspan=3>
                                <asp:Button ID="Button1" runat=server Text="Submit" OnClick="Button1_Click" />
                            </td>
                        </tr>
                    </table>
                    

               
               
               
               
               
                </td>
                </tr>
                
     
            




     </table>
                    
                    
   
     
</form>
</body>
</html>
