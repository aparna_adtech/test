﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BregVision.MasterPages
{
	public partial class PracticeSettingsAdmin : System.Web.UI.MasterPage
	{

		protected void Page_Load(object sender, EventArgs e)
		{

			PracticeAdmin practiceAdminMaster = (PracticeAdmin)this.Master;

			practiceAdminMaster.SelectCurrentTab("TabSettings");

		}

		public void SelectCurrentTab(string tabName)
		{
			ctlPracticeSettingsMenu.SelectCurrentTab(tabName);
		}

		public void HideMenu()
		{
			ctlPracticeSettingsMenu.Visible = false;
		}
	}
}
