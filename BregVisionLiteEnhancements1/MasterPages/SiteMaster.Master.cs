using System;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web.UI.WebControls;
using InfoSoftGlobal;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.WebControls;
using BLL.PracticeLocation;
using System.Web.UI;
using System.Web.Security;
using Telerik.Web.UI;

namespace BregVision.MasterPages
{
  public partial class SiteMaster : Bases.MasterBase
  {
    public global::Telerik.Web.UI.RadStyleSheetManager RadStyleSheetManager1;

    public event CommandEventHandler LocationChanged;

    public string DefaultDispenseSearchCriteria { get; set; }

    //public Campaigns.UserControls.BannerAd HorizontalBannerAd
    //{
    //    get { return bannerAdTop; }
    //}

    //public Campaigns.UserControls.BannerAd VerticalBannerAd
    //{
    //    get { return cltVerticalBannerAd; }
    //}

    public UserControls.SiteCart SiteCart
    {
      get { return ctlSiteCart; }
    }

    public UserControls.MainMenu MainMenuControl
    {
      get { return ctlMainMenu; }
    }

    public RadScriptManager RadScriptManager1Manager
    {
      get { return RadScriptManager1; }
    }

    protected void cbxChangeLocation_SelectedIndexChanged(object o, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {

      lblLocation.Text = cbxChangeLocation.Text.ToString();
      practiceLocationID = Convert.ToInt32(cbxChangeLocation.SelectedValue);
      practiceLocationName = cbxChangeLocation.SelectedItem.Text;

      if (LocationChanged != null)
        LocationChanged(this, new CommandEventArgs(cbxChangeLocation.SelectedItem.Text, cbxChangeLocation.SelectedValue));
    }


    protected void Page_Init(object sender, EventArgs e)
    {

      PopulateClinicAndLocation();

      GetUserAccess();

    }

    private static void DisableSlidingPane(Telerik.Web.UI.RadSlidingPane slidingPane)
    {
      slidingPane.Visible = false;
      slidingPane.Enabled = false;
    }

    private static void DisablePane(Telerik.Web.UI.RadPane pane)
    {
      pane.Visible = false;
      pane.Enabled = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected override void OnPreRender(EventArgs e)
    {
      base.OnPreRender(e);
    }


    public void HideMenuAdminMode()
    {
      SetTabVisibility("Admin", false);
      ctlSiteCart.HideMenuAdminMode();

    }

    public void SetTabVisibility(string tabName, bool visible)
    {
      ctlMainMenu.SetTabVisibility(tabName, visible); ;
    }

    public void SetLocationDropdownVisibility(bool visible)
    {

      //cbxChangeLocation.Visible = visible;
    }

    public void SelectCurrentTab(string tabName)
    {
      ctlMainMenu.SelectCurrentTab(tabName);
    }


    public void HideAllMenus()
    {
      //tblCart.Visible = false;
     // cbxChangeLocation.Visible = false;
      ctlMainMenu.HideAllMenus();//Table_01.Visible = false; //home thru admin menu
                                 //tblClinicMenu.Visible = false;
    }

    public void GetUserAccess()
    {
      if (!(Context.User.IsInRole("BregAdmin") || Context.User.IsInRole("PracticeAdmin")))
      {
        ctlMainMenu.SetTabVisibility("Admin", false);
      }

      var defaultRecordsPerPage = 25;
      var defaultDispenseSearchCriteria = "";

      var user = new BLL.Practice.User();
      var userData = user.SelectUser(Context.User.Identity.Name.ToString(), practiceID);

      if (userData.Rows.Count > 0)
      {
        DataRow currentUser = userData.Rows[0];

        userID = Convert.ToInt32(currentUser["UserID"]);

        ctlSiteCart.Visible = Convert.ToBoolean(currentUser["AllowCart"]);
        ctlMainMenu.SetTabVisibility("Dispense", Convert.ToBoolean(currentUser["AllowDispense"]));
        ctlMainMenu.SetTabVisibility("Inventory", Convert.ToBoolean(currentUser["AllowInventory"]));
        ctlMainMenu.SetTabVisibility("CheckIn", Convert.ToBoolean(currentUser["AllowCheckIn"]));
        ctlMainMenu.SetTabVisibility("Reports", Convert.ToBoolean(currentUser["AllowReports"]));

        if (currentUser["DefaultRecordsPerPage"].ToString() != "")
          defaultRecordsPerPage = Convert.ToInt32(currentUser["DefaultRecordsPerPage"]);

        defaultDispenseSearchCriteria = currentUser["DefaultDispenseSearchCriteria"].ToString();

        ScrollEntirePage = Convert.ToBoolean(currentUser["ScrollEntirePage"]);
        DispenseQueueSortDescending = Convert.ToBoolean(currentUser["DispenseQueueSortDescending"]);
        AllowPracticeUserOrderWithoutApproval = Convert.ToBoolean(currentUser["AllowPracticeUserOrderWithoutApproval"]);
      }

      if (!Page.IsPostBack)
      {
        if (Session["RecordsDisplayed"] == null)
          recordsDisplayedPerPage = defaultRecordsPerPage;
        Session["DefaultDispenseSearchCriteria"] = defaultDispenseSearchCriteria;
      }
    }

    private void PopulateClinicAndLocation()
    {

      using (IDataReader reader = DAL.Practice.Practice.GetPracticeLocationInfo(Context.User.Identity.Name))
      {
        if (reader.Read())
        {

          //Check to see if session variables have been set.
          if (Session["PracticeName"] == null || Session["PracticeName"].ToString().Length == 0)
          {

            practiceName = reader["PracticeName"].ToString();

            practiceID = Convert.ToInt32(reader["PracticeID"].ToString());
          }
          // Clear location drop down
          cbxChangeLocation.ClearSelection();

          do
          {
            //Fill location drop down box
            if (reader["AllowAccess"].ToString() == "True")
            {
              Telerik.Web.UI.RadComboBoxItem item = new Telerik.Web.UI.RadComboBoxItem();

              item.Text = reader["Name"].ToString();
              item.Value = reader["PracticeLocationID"].ToString();

              //set the value in the drop down to selected if it is the primary location
              //if (reader["isPrimaryLocation"].ToString() == "True" && Session["PracticeLocationID"] == null)
              if ((reader["UserDefaultLocation"].ToString() == "True" && Session["PracticeLocationID"] == null) || (Convert.ToInt32(reader["PracticeLocationID"]) == Convert.ToInt32(Session["PracticeLocationID"])))
              {
                item.Selected = true;
                lblLocation.Text = reader["Name"].ToString();
                practiceLocationName = reader["Name"].ToString();
                practiceLocationID = Convert.ToInt32(reader["PracticeLocationID"].ToString());
              }

              cbxChangeLocation.Items.Add(item);
            }
          }
          while (reader.Read());

        }

      }
      lblClinic.Text = practiceName;
      lblLocation.Text = practiceLocationName;
    }



    public string GetAbsPath(string imagePath)
    {
      return BLL.Website.GetAbsPath(imagePath);
    }
  }
}
