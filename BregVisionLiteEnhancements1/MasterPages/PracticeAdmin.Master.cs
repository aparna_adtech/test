using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Telerik.WebControls;
using System.Collections.Generic;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Security;


namespace BregVision.MasterPages
{
  public partial class PracticeAdmin : System.Web.UI.MasterPage
  {

    bool blnIsVendorGridBregAdminOnly;

    public string practiceAdminPageTitle;


    protected void Page_Load(object sender, EventArgs e)   //protected  void Page_Load(object sender, EventArgs e)
    {
      Master.HideMenuAdminMode();
      //<%--<link href="../Admin/Styles/AdminMasterStyle.css" rel="stylesheet" type="text/css" />--%>
      //((SiteMaster)this.Master).SiteMasterHead.InnerHtml += "<link href=\"../Admin/Styles/AdminMasterStyle.css\" rel=\"stylesheet\" type=\"text/css\" />";
      if (!IsPostBack)
      {
        blnIsVendorGridBregAdminOnly = ((ConfigurationSettings.AppSettings["IsVendorGridBregAdminOnly"] != null)
        ? Convert.ToBoolean(ConfigurationSettings.AppSettings["IsVendorGridBregAdminOnly"].ToString()) : true);

        if (Roles.IsUserInRole(Context.User.Identity.Name, "BregAdmin"))
        {
          //Do not Get the User's Practice as this does not make any sense.
          ctlPracticeMenu.SetTabProductsVisiblility(true);
        }
        else if (Roles.IsUserInRole(Context.User.Identity.Name, "PracticeAdmin"))
        {
          DAL.Practice.Practice.SetPracticeSession();
          ctlPracticeMenu.SetTabProductsVisiblility(!blnIsVendorGridBregAdminOnly);
        }
        else if (Roles.IsUserInRole(Context.User.Identity.Name, "PracticeLocationUser"))
        {
          if (GetUserAccess() != true)
          {
            //Send them back to the referring page
            DenyAccess();
          }
        }
        else
        {
          //Send them back to the referring page
          DenyAccess();
        }

      }
    }


    public void DenyAccess()
    {
      //Send them back to the referring page
      string referringPage = "~/Home.aspx";
      if (Request.ServerVariables["HTTP_Referer"] != null)
      {
        referringPage = Request.ServerVariables["HTTP_Referer"].ToString();
      }

      Response.Redirect(referringPage);
    }

    public bool GetUserAccess()
    {
      try
      {
        string[] pageRawUrl = Request.RawUrl.Split(new char[] { '/' });
        string pageName = pageRawUrl[pageRawUrl.Length - 1].ToLower();

        BLL.Practice.User user = new BLL.Practice.User();
        DataTable userData = user.SelectUser(Context.User.Identity.Name.ToString());

        if (userData.Rows.Count != 1)
        {
          return false;
        }

        DataRow currentUser = userData.Rows[0];

        if (pageName == "Practice_Reports.aspx".ToLower() && Convert.ToBoolean(currentUser["AllowReports"]) == false)
        {
          return false;
        }

        if ((pageName == "PracticeLocation_DispensementModifications.aspx".ToLower() || pageName == "PracticeLocation.aspx".ToLower() /*|| pageName == "PracticeLocation_All.aspx".ToLower()*/) && Convert.ToBoolean(currentUser["AllowDispenseModification"]) == false)
        {
          return false;
        }

        return true;

      }
      catch
      {
        return false;
      }
    }
    public void HideAllMenus()
    {
      ctlPracticeMenu.Visible = false;
    }

    public void SelectCurrentTab(string tabName)
    {
      ctlPracticeMenu.SelectCurrentTab(tabName);
    }



  }
}
