﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
namespace BregVision.MasterPages.UserControls
{
  public partial class PracticeLocationMenu : System.Web.UI.UserControl
  {
    MainMenu mainMenu;
    protected void Page_Init(object sender, EventArgs e)
    {
      PracticeLocationAdmin practiceLocationMaster = (PracticeLocationAdmin)Page.Master;
      PracticeAdmin practiceAdminMaster = (PracticeAdmin)practiceLocationMaster.Master;
      SiteMaster siteMaster = (SiteMaster)practiceAdminMaster.Master;
      mainMenu = siteMaster.MainMenuControl;
    }


    protected void Page_Load(object sender, EventArgs e)
    {

      EnableSuperbillForOrthoSelect();

      EnableSparkFeatures();
    }

    private void EnableSparkFeatures()
    {
      int practiceID = GetPracticeID();
      bool isSpark = DAL.Practice.Practice.IsSpark(practiceID);
      SetTabVisibility("TabInventoryCount", isSpark);
    }

    private int GetPracticeID()
    {
      int practiceID = (Session["PracticeID"] != null) ? Convert.ToInt32(Session["PracticeID"].ToString()) : -1;
      return practiceID;
    }

    public void SetTabVisibility(string tabName, bool visible)
    {
      FindToolbarTab(tabName).Visible = visible;
    }

    public void SelectCurrentTab(string tabName)
    {
      Telerik.Web.UI.RadTab tabSelected = FindToolbarTab(tabName);
      tabSelected.Selected = true;
    }

    public RadTab FindToolbarTab(string tabValue)
    {
      return PracticeLocationTabStrip.FindTabByValue(tabValue);
      //RadTab tab;;W

      //tab = rmpLocation.FindTabByValue(tabValue);
      //return tab;
    }


    private void EnableSuperbillForOrthoSelect()
    {
      //BLL.Practice.Practice bllP = new BLL.Practice.Practice();
      //DataSet dsPractice = bllP.GetAllPractices();
      //int practiceID = (Session["PracticeID"] != null) ? Convert.ToInt32(Session["PracticeID"].ToString()) : -1;
      //bool isOrthoSelect = false;

      //var query = from practice in dsPractice.Tables[0].AsEnumerable()
      //            where practice.Field<int>("PracticeID") == practiceID
      //            select new
      //            {
      //              PracticeID = practice.Field<int>("PracticeID"),
      //              IsOrthoSelect = practice.Field<bool>("IsOrthoSelect")
      //            };

      //foreach (var practice in query)
      //{
      //  if (practice.PracticeID == practiceID)
      //  {
      //    isOrthoSelect = practice.IsOrthoSelect;
      //    break;
      //  }
      //}
      //if (Roles.IsUserInRole(Context.User.Identity.Name, "BregAdmin") || Roles.IsUserInRole(Context.User.Identity.Name, "PracticeAdmin")) //(TabLocationSuperbill.Visible == true)
      //{
      //  SetTabVisibility("TabLocationSuperbill", isOrthoSelect);
      //}

      // NOTE: disabling due to unused feature and access of tab causing crash.
      // Per discussion with Shari Matkin. -hchan 05242016
      SetTabVisibility("TabLocationSuperbill", false);
    }
  }
}
