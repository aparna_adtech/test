﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="News.ascx.cs" Inherits="BregVision.MasterPages.UserControls.News" %>

<div style="position:relative;margin-bottom:10px;">
<a href="http://www.breg.com/products/knee-bracing/post-op/t-scope%C2%AE-premier-post-op-knee-brace" target="_blank"><img src="/Images/cta-secondary-banner-premier.jpg" /></a>
<div style="position:absolute;top:155px;left:267px;">
<h2 style="color:#ccc;font-size:10px;font-family:arial,helvetica;">T Scope Premier Post Op Knee Brace</h2>
</div>
</div>

<hr/>

<table>
    <tr>
        <td colspan="1">
                <b>Medicare Pub 20100-08 for OrthoSelect Customers:</b>
        </td>
    </tr>
    <tr>
        <td>
             


                <br />Dear OrthoSelect Practices,
                <br />
                <br />While most of you have probably received the notice from Medicare, we wanted to share a recent announcement related to Compliance Standards for Consignment Closets and Stock and Bill Arrangements. This notice pertains to DMEPOS Suppliers that maintain inventory at a practice owned by a physician or non-physician practitioner and that bill one of the DME MACs for products dispensed to Medicare beneficiaries. The effective date for the new requirements is September 8, 2009 <span style="color:Red">(Deadline has been moved to March 1, 2010)</span> and some key provisions are included below: 
                 <br />
                <br />Medicare allows Medicare enrolled DMEPOS suppliers to maintain inventory at a practice location owned by a physician or non-physician practitioner for the purpose of DMEPOS distribution when the following conditions are met by the DMEPOS supplier and verified by the NSC-MAC: 
                 <br /><b>
                <br />1.	The title to the DMEPOS shall be transferred to the enrolled physician or non-physician practitioner’s practice at the time the DMEPOS is furnished to the beneficiary. 
                <br />
                <br />2.	The physician or non-physician practitioner’s practice shall bill for the DMEPOS supplies and services using their own enrolled DMEPOS billing number. 
                <br />
                <br />3.	All services provided to a Medicare beneficiary concerning fitting or use of the DMEPOS shall be performed by individuals being paid by the physician or non-physician practitioner’s practice, not by any other DMEPOS supplier. 
                <br />
                <br />4.	The beneficiary shall be advised that, if they have a problem or questions with the DMEPOS, they should contact the physician or non-physician practitioner’s practice, not the DMEPOS supplier who placed the DMEPOS at the physician or non-physician practitioner’s practice. 
                 <br /> </b> 
                 <br />                         
                <br />As stated, the new requirements pertain to existing arrangements that involve Medicare beneficiaries only. 
                <br />
                <br />What does this mean for the DMEPOS community?
                <br />                                    
                <br />The provision states that for Medicare patients the CMS will no longer allow any other supplier, besides the physician office, the ability to bill for the DMEPOS item if it was placed on a patient in the physician office.  I.E. Stock and Bill models (an outside company puts product in your office, they or you fit the patient, and then they bill for the item) will no longer be an acceptable means for billing items dispensed to Medicare beneficiaries.  The two alternative methods for in-office DMEPOS supplies (as of September 8, 2009) will be 1) Physician billing (OrthoSelect) or 2) Scripting out to a DMEPOS supplier.
                 <br />                           
                <br />Why is this ruling beneficial to you?
                <br />                        
                <br />As OrthoSelect Practices, you have already taken most, if not all, of the steps necessary to comply with the new rule.  This provision strengthens the CMS position that the physician should take full ownership of a physician to patient DMEPOS program or script the product to an outside DMEPOS supplier.
                <br />            
                <br />What could this affect in your practice?
                <br />
                <br />If you are an OrthoSelect practice that doesn’t bill Medicare but provides these products to your patients in-house, sending the billing out to a third party DMEPOS supplier, and having that supplier replace the product in your practice, as of September 8, 2009, you will need to write a script for these products and send the patient directly to the DMEPOS supplier.  This could be a slight inconvenience to your Medicare patients and is all the more reason to apply for your Medicare DMEPOS# and bill directly.
                <br />            
                <br />What facilities does this not affect?
                <br />                    
                <br />Hospitals and Surgery Centers that utilize Stock and Bill models are NOT affected by this provision.
                <br />
                <br />    
                <br />The instructions released by CMS are also attached for your reference.  If you have any questions or concerns please contact your respective BREG OrthoSelect Territory Manager.
                <br />                        
                <br />Sincerely,
                <br />                        
                <br />                   
                <br />BREG Orthopedic Practice Solutions Team
                <br />        
                <br />                                    
        </td>
    </tr>
</table>