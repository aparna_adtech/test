﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SilverlightForum.ascx.cs" Inherits="BregVision.MasterPages.UserControls.SilverlightForum" %>
<style type="text/css">
#article 
{ 
    visibility:hidden;
    background-color:White;
    position:absolute;
    top:50px;
    left:0px;
    width:100%;
    height:100%;
    z-index:100;
    font-size: 12px;
    font-family: Arial, Helvetica, sans-serif;
    color:Black;	
}
</style>
<div style="position: relative; height: 100%; width: 100%;">
    <div id="silverlightControlHost" style="height: 100%; width: 100%">
        <object id="HelpControl" data="data:application/x-silverlight-2," style="height: 100%;
            width: 100%" type="application/x-silverlight-2">
            <param name="windowless" value="true" />
            <param name="source" value='<%= GetAbsPath("~/ClientBin/Forums.xap") + "?v=" + BregVision.Version.GetVersionString() %>' />
            <param name="onError" value="onSilverlightError" />
            <param name="background" value="white" />
            <param name="minRuntimeVersion" value="4.0.50917" />
            <param name="autoUpgrade" value="true" />
            <param name="initParams" value="<%= _initParams %>" />
            <a href="http://go.microsoft.com/fwlink/?LinkID=149156" style="text-decoration: none">
                <img alt="Get Microsoft Silverlight" src="http://go.microsoft.com/fwlink/?LinkId=108181"
                    style="border-style: none" />
            </a>
        </object>
        <iframe id="_sl_historyFrame" style="visibility: hidden; height: 0px; width: 0px;
            border: 0px"></iframe>
    </div>
    <div id="article">
        This is content
    </div>
</div>