﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BregVision.MasterPages.UserControls
{
    public partial class SilverlightForum : System.Web.UI.UserControl
    {
        public string _initParams = "None";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                _initParams = "RecentView=True"; //causes the SL control to show RecentView.XMAL
            }
        }

        public string GetAbsPath(string imagePath)
        {
            return BLL.Website.GetAbsPath(imagePath);
        }

    }
}