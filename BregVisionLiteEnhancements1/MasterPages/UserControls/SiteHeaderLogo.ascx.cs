﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BregVision.MasterPages.UserControls
{
    public partial class SiteHeaderLogo : System.Web.UI.UserControl
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!BLL.Website.IsVisionExpress())
            {
                imgSiteLogo.ImageUrl = "~/Images/VisionLogo.gif";
            }
        }
    }
}