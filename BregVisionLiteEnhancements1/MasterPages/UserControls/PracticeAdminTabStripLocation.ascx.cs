using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace BregVision.MasterPages.UserControls
{
    public partial class PracticeAdminTabStripLocation : System.Web.UI.UserControl
    {
        public Telerik.Web.UI.RadTab MenuContact
        {
            get { return TabLocationContact; }
            set { TabLocationContact = value; }
        }
        public Telerik.Web.UI.RadTab MenuEmail
        {
            get { return TabLocationEmail; }
            set { TabLocationEmail = value; }
        }
        public Telerik.Web.UI.RadTab MenuPhysician
        {
            get { return TabLocationPhysician; }
            set { TabLocationPhysician = value; }
        }
        public Telerik.Web.UI.RadTab MenuShippingAddress
        {
            get { return TabLocationShippingAddress; }
            set { TabLocationShippingAddress = value; }
        }
        public Telerik.Web.UI.RadTab MenuSuperbill
        {
            get { return TabLocationSuperbill; }
            set { TabLocationSuperbill = value; }
        }
        public Telerik.Web.UI.RadTab MenuSupplierCodes
        {
            get { return TabLocationSupplierCodes; }
            set { TabLocationSupplierCodes = value; }
        }

        public Telerik.Web.UI.RadTab MenuInventoryLevel
        {
            get { return TabPracticeLocationInventoryLevel; }
            set { TabPracticeLocationInventoryLevel = value; }
        }
        public Telerik.Web.UI.RadTab MenuDispensementModifications
        {
            get { return TabLocationDispensementModifications; }
            set { TabLocationDispensementModifications = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            BLL.Practice.Practice bllP = new BLL.Practice.Practice();
            DataSet dsPractice = bllP.GetAllPractices();
            int practiceID = (Session["PracticeID"] != null) ? Convert.ToInt32(Session["PracticeID"].ToString()) : -1;
            bool isOrthoSelect = false;

            var query = from practice in dsPractice.Tables[0].AsEnumerable()
                        where practice.Field<int>("PracticeID") == practiceID
                        select new
                        {
                            PracticeID = practice.Field<int>("PracticeID"),
                            IsOrthoSelect = practice.Field<bool>("IsOrthoSelect")
                        };

            foreach(var practice in query)
            {
                if (practice.PracticeID == practiceID)
                {
                    isOrthoSelect = practice.IsOrthoSelect;
                    break;
                }
            }
            if (Roles.IsUserInRole(Context.User.Identity.Name, "BregAdmin") || Roles.IsUserInRole(Context.User.Identity.Name, "PracticeAdmin")) //(TabLocationSuperbill.Visible == true)
            {
                TabLocationSuperbill.Visible = isOrthoSelect;
            }

        }
    }
}