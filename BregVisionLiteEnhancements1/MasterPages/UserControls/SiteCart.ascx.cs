﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.PracticeLocation;

namespace BregVision.MasterPages.UserControls
{
  using Telerik.Web.UI;

  public partial class SiteCart : Bases.UserControlBase
  {
    public RadButton RadButtonItemsInCartButton
    {
      get { return RadButtonItemsInCart; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        var dsCart = ShoppingCart.GetShoppingCart(practiceLocationID);
      this.RadButtonItemsInCart.Text = string.Concat("Cart (", ShoppingCart.GetShoppingCartCount(dsCart).ToString(), " items; $" + ShoppingCart.GetShoppingCartTotal(dsCart).ToString() +")");
      this.DataBind();
    }

    public void HideMenuAdminMode()
    {
      this.Visible = false;
      //aCart.Disabled = true;
      //aCart.Visible = false;
    }
  }
}