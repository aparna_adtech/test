﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="MainMenu.ascx.cs" Inherits="BregVision.MasterPages.UserControls.MainMenu" %>
<!--.backGroundTab is defined in the code behind-->
<div id="divNavigateLabel" runat="server">
  <asp:Label ID="lblNavigate" runat="server" Text=""></asp:Label>
  <asp:Panel ID="NavigatePanel" runat="server">
    <asp:Timer ID="NavigateTimer" runat="server" OnTick="NavigateTimer_OnTick" Interval="10000" Enabled="false"></asp:Timer>
  </asp:Panel>
</div>
<telerik:RadTabStrip ID="MainRadTabStrip" runat="server" CssClass="rtsHorizontal rtsTop" Skin="Breg" EnableEmbeddedSkins="False">
  <Tabs>
    <telerik:RadTab runat="server" TabIndex="0" NavigateUrl="~/Home.aspx" Text="Home" Value="Home"></telerik:RadTab>
    <telerik:RadTab runat="server" TabIndex="1" NavigateUrl="~/Dispense.aspx" Text="Dispense" Value="Dispense"></telerik:RadTab>
    <telerik:RadTab runat="server" TabIndex="2" NavigateUrl="~/Inventory.aspx" Text="Inventory" Value="Inventory"></telerik:RadTab>
    <telerik:RadTab runat="server" TabIndex="3" NavigateUrl="~/CheckIn.aspx" Text="Check-In" Value="CheckIn"></telerik:RadTab>
    <telerik:RadTab runat="server" TabIndex="4" NavigateUrl="~/PracticeReports.aspx" Text="Reports" Value="Reports"></telerik:RadTab>
    <telerik:RadTab runat="server" TabIndex="5" NavigateUrl="~/InvoiceManager.aspx" Text="Invoice Manager" Value="Invoice"></telerik:RadTab>
    <telerik:RadTab runat="server" TabIndex="6" NavigateUrl="~/Admin/PracticeSetup/PracticeSetup.aspx" Text="Admin" Value="Admin"></telerik:RadTab>
  </Tabs>
</telerik:RadTabStrip>
