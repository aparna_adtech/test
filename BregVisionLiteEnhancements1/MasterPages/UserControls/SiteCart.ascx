﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SiteCart.ascx.cs" Inherits="BregVision.MasterPages.UserControls.SiteCart" %>
<telerik:RadButton NavigateUrl="~/cart.aspx" ButtonType="LinkButton" runat="server" Text="Cart" ID="RadButtonItemsInCart" CssClass="button-primary no-wrap text-large" >
  <Icon PrimaryIconUrl="~/App_Themes/Breg/images/Common/cart.png"></Icon>
</telerik:RadButton>
<%--<input type="button" onclick="" href="<%# System.Web.VirtualPathUtility.ToAbsolute("~/cart.aspx") %>">Checkout--%>

