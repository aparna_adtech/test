﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="KPICharts.ascx.cs" Inherits="BregVision.MasterPages.UserControls.KPICharts" %>


<table width="100%" border="1">
    <tr>
            <td><%=GetProductsDispensedChartHtml()%>
            </td>
            <td>
                <%=GetProductsCheckedInChartHtml()%>
            </td>
            <td>
                <%=GetProductsOnHandChartHtml()%>
            </td>
    </tr>
    <tr>
            <td>
                <%=GetProductAverageCostChartHtml()%>
            </td>
            <td>
                <%=GetProductInventoryOrderStatusChartHtml()%>
            </td>
            <td>
                <%=GetProductReOrderStatusChartHtml()%>
            </td>                    
</tr>
</table> 