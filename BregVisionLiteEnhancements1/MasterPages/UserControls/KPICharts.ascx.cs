﻿using System;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web.UI.WebControls;
using InfoSoftGlobal;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.WebControls;

namespace BregVision.MasterPages.UserControls
{
    public partial class KPICharts : Bases.UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string GetProductReOrderStatusChartHtml()
        {
            try
            {
                //In this example, we show how to connect FusionCharts to a database.
                //For the sake of ease, we've used an Access database which is present in
                //../App_Data/FactoryDB.mdb. It just contains two tables, which are linked to each
                //other. 

                //xmlData will be used to store the entire XML document generated
                StringBuilder xmlData = new StringBuilder();

                //Generate the chart element
                xmlData.Append("<chart caption='Product Order Status' subCaption='Number of Products' xAxisName='Priority' yAxisName='Number of Products' showValues='0' decimals='0' formatNumberScale='0' >");

                DataTable ReportData = GetChartData(practiceLocationID, "dbo.usp_ProductInventoryReOrderStatusKPI").Tables[0];

                int counter = 0;
                string[] barColor = { "ffdce2", "bbdbf4", "E6E7B2", "ffdce2", "bbdbf4", "E6E7B2" };

                foreach (DataRow dr in ReportData.Rows)
                {
                    xmlData.AppendFormat("<set label='{0}' value='{1}' color='{2}' />", dr["PriorityName"].ToString(), dr["products"].ToString(), barColor[counter]);
                    counter += 1;
                }
                //Close chart element
                xmlData.Append("</chart>");

                //Create the chart - Pie 3D Chart with data from xmlData
                return FusionCharts.RenderChart(GetAbsPath("~/FusionCharts/FusionCharts/Column3D.swf"), "", xmlData.ToString(), "ProductReOrderStatus", "225", "250", false, false);

            }
            catch
            {
                return "";
                //throw;
            }
        }

        public string GetProductInventoryOrderStatusChartHtml()
        {
            try
            {
                //In this example, we show how to connect FusionCharts to a database.
                //For the sake of ease, we've used an Access database which is present in
                //../App_Data/FactoryDB.mdb. It just contains two tables, which are linked to each
                //other. 

                //xmlData will be used to store the entire XML document generated
                StringBuilder xmlData = new StringBuilder();

                //Generate the chart element
                xmlData.Append("<chart caption='Inventory Order Status' subCaption='By Dollar Amount' xAxisName='Current' yAxisName='Dollars' showValues='0' decimals='0' formatNumberScale='0' numberPrefix='$'>");

                DataTable ReportData = GetChartData(practiceLocationID, "dbo.usp_ProductInventoryOrderStatusKPI").Tables[0];
                int counter = 0;
                string[] barColor = { "bcbcdb", "A5F08B", "ffdce2", "bbdbf4", "E6E7B2" };

                foreach (DataRow dr in ReportData.Rows)
                {
                    xmlData.AppendFormat("<set label='{0}' value='{1}' color='{2}' />", dr["Status"].ToString(), dr["TotalDollars"].ToString(), barColor[counter]);
                    counter += 1;
                }
                //Close chart element
                xmlData.Append("</chart>");

                //Create the chart - Pie 3D Chart with data from xmlData
                return FusionCharts.RenderChart(GetAbsPath("~/FusionCharts/FusionCharts/Column3D.swf"), "", xmlData.ToString(), "ProductOrderStatus", "225", "250", false, false);

            }
            catch
            {
                return "";
                //throw;
            }
        }

        public string GetProductAverageCostChartHtml()
        {
            try
            {
                //In this example, we show how to connect FusionCharts to a database.
                //For the sake of ease, we've used an Access database which is present in
                //../App_Data/FactoryDB.mdb. It just contains two tables, which are linked to each
                //other. 

                //xmlData will be used to store the entire XML document generated
                StringBuilder xmlData = new StringBuilder();

                //Generate the chart element
                xmlData.Append("<chart caption='Product Average Cost' subCaption='By Dollar Amount' xAxisName='Current' yAxisName='Dollars' showValues='0' decimals='0' formatNumberScale='0' numberPrefix='$'>");

                DataTable ReportData = GetChartData(practiceLocationID, "dbo.usp_ProductCostAverageKPI").Tables[0];
                foreach (DataRow dr in ReportData.Rows)
                {
                    xmlData.AppendFormat("<set label='{0}' value='{1}' color='FFDCE2' />", "Last 7 Days Cost", dr["Last7DaysCost"].ToString());
                    xmlData.AppendFormat("<set label='{0}' value='{1}' color='C6C6C6' />", "Last 7 Days Bill", dr["Last7DaysBillingCharge"].ToString());
                    xmlData.AppendFormat("<set label='{0}' value='{1}' color='BBDBF4' />", "Last 7 Days DME", dr["Last7DaysDMEDeposit"].ToString());

                    xmlData.AppendFormat("<set label='{0}' value='{1}' color='E6E7B2' />", "Last 30 Days Cost", dr["Last30DaysCost"].ToString());
                    xmlData.AppendFormat("<set label='{0}' value='{1}' color='D4D8DC' />", "Last 30 Days Bill", dr["Last30DaysBillingCharge"].ToString());
                    xmlData.AppendFormat("<set label='{0}' value='{1}' color='fbe1a9' />", "Last 30 Days DME", dr["Last30DaysDMEDeposit"].ToString());
                }
                //Close chart element
                xmlData.Append("</chart>");

                //Create the chart - Pie 3D Chart with data from xmlData
                return FusionCharts.RenderChart(GetAbsPath("~/FusionCharts/FusionCharts/Column3D.swf"), "", xmlData.ToString(), "ProductsAverageCost", "300", "250", false, false);

            }
            catch
            {
                return "";
                //throw;
            }
        }

        public string GetProductsCheckedInChartHtml()
        {
            //In this example, we show how to connect FusionCharts to a database.
            //For the sake of ease, we've used an Access database which is present in
            //../App_Data/FactoryDB.mdb. It just contains two tables, which are linked to each
            //other. 

            //xmlData will be used to store the entire XML document generated
            StringBuilder xmlData = new StringBuilder();

            //Generate the chart element
            xmlData.Append("<chart caption='Products Checked In' subCaption='By Dollar Amount'xAxisName='Current' yAxisName='Dollars' showValues='0' decimals='0' formatNumberScale='0' numberPrefix='$'>");

            DataTable ReportData = GetChartData(practiceLocationID, "dbo.usp_ProductsCheckedInKPI").Tables[0];
            foreach (DataRow dr in ReportData.Rows)
            {
                xmlData.AppendFormat("<set label='{0}' value='{1}' color='DFEEAC' />", "Last 7 Days", dr["Last7Days"].ToString());
                xmlData.AppendFormat("<set label='{0}' value='{1}' color='FBE1A9' />", "Last 30 Days", dr["Last30Days"].ToString());
            }
            //Close chart element
            xmlData.Append("</chart>");

            //Create the chart - Pie 3D Chart with data from xmlData
            return FusionCharts.RenderChart(GetAbsPath("~/FusionCharts/FusionCharts/Column3D.swf"), "", xmlData.ToString(), "ProductsCheckedIn", "225", "250", false, false);

        }

        public string GetProductsOnHandChartHtml()
        {
            try
            {
                //In this example, we show how to connect FusionCharts to a database.
                //For the sake of ease, we've used an Access database which is present in
                //../App_Data/FactoryDB.mdb. It just contains two tables, which are linked to each
                //other. 

                //xmlData will be used to store the entire XML document generated
                StringBuilder xmlData = new StringBuilder();

                //Generate the chart element
                xmlData.Append("<chart caption='Product on Hand' subCaption='By Dollar Amount'xAxisName='Current' yAxisName='Dollars' showValues='0' decimals='0' formatNumberScale='0' numberPrefix='$'>");

                DataTable ReportData = GetChartData(practiceLocationID, "dbo.usp_ProductOnHandKPI").Tables[0];
                foreach (DataRow dr in ReportData.Rows)
                {
                    xmlData.AppendFormat("<set label='{0}' value='{1}' color='DFDFE1' />", "Products on Hand", dr["WholesaleCost"].ToString());

                }
                //Close chart element
                xmlData.Append("</chart>");

                //Create the chart - Pie 3D Chart with data from xmlData
                return FusionCharts.RenderChart(GetAbsPath("~/FusionCharts/FusionCharts/Column3D.swf"), "", xmlData.ToString(), "ProductsOnHand", "225", "250", false, false);


            }
            catch
            {
                return "";
                //throw;
            }
        }

        public string GetProductsDispensedChartHtml()
        {
            try
            {
                //In this example, we show how to connect FusionCharts to a database.
                //For the sake of ease, we've used an Access database which is present in
                //../App_Data/FactoryDB.mdb. It just contains two tables, which are linked to each
                //other. 

                //xmlData will be used to store the entire XML document generated
                StringBuilder xmlData = new StringBuilder();

                //Generate the chart element
                xmlData.Append("<chart caption='Products Dispensed' subCaption='By Dollar Amount'xAxisName='Current' yAxisName='Dollars' showValues='0' decimals='0' formatNumberScale='0' numberPrefix='$'>");

                DataTable ReportData = GetChartData(practiceLocationID, "dbo.usp_ProductsDispensedKPI").Tables[0];
                foreach (DataRow dr in ReportData.Rows)
                {
                    xmlData.AppendFormat("<set label='{0}' value='{1}' color='A5F08B' />", "Last 7 Days", dr["Last7Days"].ToString());
                    xmlData.AppendFormat("<set label='{0}' value='{1}' color='BCBCDB' />", "Last 30 Days", dr["Last30Days"].ToString());
                }
                //Close chart element
                xmlData.Append("</chart>");

                //Create the chart - Pie 3D Chart with data from xmlData
                return FusionCharts.RenderChart(GetAbsPath("~/FusionCharts/FusionCharts/Column3D.swf"), "", xmlData.ToString(), "ProductsDispensed", "300", "250", false, false);

            }
            catch
            {
                return "";
                //throw;
            }
        }

        public DataSet GetChartData(Int32 practiceLocationID, string procName)
        {
            try
            {

                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand(procName);

                db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
                DataSet ds = db.ExecuteDataSet(dbCommand);
                return ds;
            }
            catch
            {
                return null;
                //throw;
            }
        }

        public string GetAbsPath(string imagePath)
        {
            return BLL.Website.GetAbsPath(imagePath);
        }
    }
}