﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SiteHelp.ascx.cs" Inherits="BregVision.MasterPages.UserControls.SiteHelp" %>

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
  <script type="text/javascript">
    function ToolbarSearch(str) {
      try {
        if (str == "") {
          alert("Search Field can not be blank");
          document.getElementById("<%=txtHelpSearch.ClientID%>").focus();
        } else {
          ExpandHelpPane();
    
          var plugin = document.getElementById('HelpControl');
          var dbl = plugin.Content.API.SearchForHelpArticlesAndHelpMeida(str);
        }
      } catch (ex) {
      }
    }
  </script>
</telerik:RadCodeBlock>
<div class="siteHelp-container">
  <telerik:RadTextBox ID='txtHelpSearch' runat="server" ToolTip="Type a question or keyword(s) and press 'Go'" EmptyMessage="Need Help?" />
  <img id="MockUpHome_17" runat="server" src="~/img/go.png" onmousedown='ToolbarSearch(document.getElementById("<%=txtHelpSearch.ClientID%>").value);' alt="Click to search" border="0" style="vertical-align: middle; cursor: pointer;" class="siteHelp-button" />
</div>
  