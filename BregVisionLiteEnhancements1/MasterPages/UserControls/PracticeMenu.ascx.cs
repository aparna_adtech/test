﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BregVision.MasterPages.UserControls
{
	public partial class PracticeMenu : System.Web.UI.UserControl
	{
		Telerik.Web.UI.RadTab adminTab;

		MainMenu mainMenu;
		protected void Page_Init(object sender, EventArgs e)
		{
			if (Page.Master is PracticeAdmin)
			{
				adminTab = ((SiteMaster)((PracticeAdmin)Page.Master).Master).MainMenuControl.MainMenuTabStrip.FindTabByValue("Admin") as Telerik.Web.UI.RadTab;
				mainMenu = ((SiteMaster)((PracticeAdmin)Page.Master).Master).MainMenuControl;
			}
			else if (Page.Master is PracticeLocationAdmin)
			{
				PracticeLocationAdmin pla = Page.Master as PracticeLocationAdmin;
				PracticeAdmin pa = pla.Master as PracticeAdmin;
				SiteMaster sm = pa.Master as SiteMaster;
				adminTab = sm.MainMenuControl.MainMenuTabStrip.FindTabByValue("Admin") as Telerik.Web.UI.RadTab;
				mainMenu = sm.MainMenuControl;
			}
			else if (Page.Master is PracticeSettingsAdmin)
			{
				PracticeSettingsAdmin psa = Page.Master as PracticeSettingsAdmin;
				PracticeAdmin pa = psa.Master as PracticeAdmin;
				SiteMaster sm = pa.Master as SiteMaster;
				adminTab = sm.MainMenuControl.MainMenuTabStrip.FindTabByValue("Admin") as Telerik.Web.UI.RadTab;
				mainMenu = sm.MainMenuControl;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{

			EnableSparkFeatures();
		}

		private void EnableSparkFeatures()
		{
			int practiceID = GetPracticeID();
			bool isSpark = DAL.Practice.Practice.IsSpark(practiceID);
			Telerik.Web.UI.RadTab invCountTab = FindToolbarTab("TabInventoryCount");  //SetTabVisibility("TabInventoryCount", isSpark);
			if (invCountTab != null)
			{
				invCountTab.Visible = isSpark;
			}
		}

		private int GetPracticeID()
		{
			int practiceID = (Session["PracticeID"] != null) ? Convert.ToInt32(Session["PracticeID"].ToString()) : -1;
			return practiceID;
		}

		public void SetTabVisibility(string tabName, bool visible)
		{
			if (this.PracticeAdminTabStrip.FindTabByText(tabName) != null)
			{
				this.PracticeAdminTabStrip.FindTabByText(tabName).Visible = visible;
			}
			else
			{
				mainMenu.FindAdminToolbarTab(tabName).Visible = visible;
			}
		}

		public void SetTabProductsVisiblility(bool isVisible)
		{
			Telerik.Web.UI.RadTab tabProducts = FindToolbarTab("TabProducts");
			tabProducts.Visible = isVisible;
		}

		public void SelectCurrentTab(string tabName)
		{
			Telerik.Web.UI.RadTab tabSelected = FindToolbarTab(tabName);
			if (tabSelected != null)
			{
				tabSelected.Visible = true;
				tabSelected.Selected = true;
			}
		}

		private Telerik.Web.UI.RadTab FindToolbarTab(string tabValue)
		{
			if (this.PracticeAdminTabStrip.FindTabByValue(tabValue) != null)
			{
				return this.PracticeAdminTabStrip.FindTabByValue(tabValue);
			}
			else
			{
				return mainMenu.FindAdminToolbarTab(tabValue);
			}
		}
	}
}