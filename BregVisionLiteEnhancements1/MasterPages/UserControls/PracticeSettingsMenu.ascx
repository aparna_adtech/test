﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeSettingsMenu.ascx.cs" Inherits="BregVision.MasterPages.UserControls.PracticeSettingsMenu" %>
  <h2 class="layout-padding no-margin-bottom">Settings</h2>
  <telerik:RadTabStrip ID="PracticeSettingsTabStrip" runat="server" Orientation="VerticalLeft" Skin="Breg" EnableEmbeddedSkins="False" CssClass="layout-padding">
    <Tabs>
      <telerik:RadTab ID="TabSettingsGeneral" Value="TabSettingsGeneral"
        runat="server" NavigateUrl="~/Admin/PracticeSettings_General.aspx"
        Text="General" ToolTip="General Settings">
      </telerik:RadTab>
      <telerik:RadTab ID="TabSettingsSiteBuilder" Value="TabSettingsSiteBuilder"
        runat="server" NavigateUrl="~/Admin/PracticeSettings_SiteBuilder.aspx"
        Text="Site Builder" ToolTip="Site Builder Settings">
      </telerik:RadTab>
    </Tabs>
  </telerik:RadTabStrip>
