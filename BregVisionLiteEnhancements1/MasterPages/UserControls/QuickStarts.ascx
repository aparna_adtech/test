﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QuickStarts.ascx.cs" Inherits="BregVision.MasterPages.UserControls.QuickStarts" %>

<table width="325px" border="0">
    <tr align="left">
        <td style="width:323px">
            <asp:HyperLink runat="server" ID="lnkUserManual" Text="Breg Vision User Guide v5" NavigateUrl="~/Documentation/Breg Vision User Guide v5.pdf" Target="_blank"></asp:HyperLink>
        </td>
    </tr>
    <tr align="left">
        <td style="width:323px">
            <asp:HyperLink runat="server" ID="lnkFAQ1" Text="FAQ" NavigateUrl="~/Documentation/BREG VISION FAQs.pdf" Target="_blank"></asp:HyperLink>
        </td>
    </tr>
    <tr align="left">
        <td style="width:323px">
            <asp:HyperLink runat="server" ID="lnkQuickStart1" Text="Dispensement Modification Quick Start Guide" NavigateUrl="~/Documentation/Dispensement Modification Quick Start Guide.pdf" Target="_blank"></asp:HyperLink>
        </td>
    </tr> 
    <tr align="left">
        <td style="width:323px">
            <asp:HyperLink runat="server" ID="lnkQuickStart2" Text="Dispensing Quick Start Guide" NavigateUrl="~/Documentation/Dispensing Quick Start Guide.pdf" Target="_blank"></asp:HyperLink>
        </td>
    </tr>   
    <tr align="left">
        <td style="width:323px">
            <asp:HyperLink runat="server" ID="lnkQuickStart3" Text="Ordering Quick Start Guide" NavigateUrl="~/Documentation/Ordering Quick Start Guide.pdf" Target="_blank"></asp:HyperLink>
        </td>
    </tr> 
    <tr align="left">
        <td style="width:323px">
            <asp:HyperLink runat="server" ID="lnkQuickStart4" Text="Pricing Add_Change Quick Start Guide" NavigateUrl="~/Documentation/Pricing Add_Change Quick Start Guide.pdf" Target="_blank"></asp:HyperLink>
        </td>
    </tr> 
    <tr align="left">
        <td style="width:323px">
            <asp:HyperLink runat="server" ID="lnkQuickStart5" Text="Product Check-In Quick Start Guide" NavigateUrl="~/Documentation/Product Check-In Quick Start Guide.pdf" Target="_blank"></asp:HyperLink>
        </td>
    </tr>    
    <tr align="left">
        <td style="width:323px">
            <asp:HyperLink runat="server" ID="lnkQuickStart6" Text="Setting Product Levels Quick Start Guide" NavigateUrl="~/Documentation/Setting Product Levels Quick Start Guide.pdf" Target="_blank"></asp:HyperLink>
        </td>
    </tr> 
    <tr align="left">
        <td style="width:323px">
            <asp:HyperLink runat="server" ID="lnkQuickStart7" Text="Stocking Units Quick Start Guide" NavigateUrl="~/Documentation/Stocking Units Quick Start Guide.pdf" Target="_blank"></asp:HyperLink>
        </td>
    </tr>                                                                                                
    <tr align="left">
        <td style="width:323px">
            <asp:HyperLink runat="server" ID="lnkQuickStart8" Text="Adding 3rd Party Suppliers and Products Quick Start Guide" NavigateUrl="~/Documentation/Adding 3rd Party Suppliers and Products Quick Start Guide.pdf" Target="_blank"></asp:HyperLink>
        </td>
    </tr>   
    
   <tr align="left">
        <td style="width:323px">
            <asp:HyperLink runat="server" ID="lnkQuickStart9" Text="Adding Products from Master Catalog Quick Start Guide" NavigateUrl="~/Documentation/Adding Products from Master Catalog Quick Start Guide.pdf" Target="_blank"></asp:HyperLink>
        </td>
    </tr>                                                                                               

   <tr align="left">
        <td style="width:323px">
            <asp:HyperLink runat="server" ID="lnkQuickStart10" Text="Browser Settings Quick Start Guide" NavigateUrl="~/Documentation/Browser Settings Quick Start Guide.pdf" Target="_blank"></asp:HyperLink>
        </td>
    </tr>
    <tr align="left">
        <td style="width:323px">
            <asp:HyperLink runat="server" ID="lnkQuickStart11" Text="Custom Brace Ordering, Check In and Dispensing Quick Start Guide" NavigateUrl="~/Documentation/Custom Brace Ordering, Check In and Dispensing Quick Start Guide.pdf" Target="_blank"></asp:HyperLink>
        </td>
    </tr>   
    
   <tr align="left">
        <td style="width:323px">
            <asp:HyperLink runat="server" ID="lnkQuickStart12" Text="Transfer Products Quick Start Guide" NavigateUrl="~/Documentation/Transfer Products Quick Start Guide.pdf" Target="_blank"></asp:HyperLink>
        </td>
    </tr>                                                                                               

   <tr align="left">
        <td style="width:323px">
            <asp:HyperLink runat="server" ID="lnkQuickStart13" Text="User Setup and Preferences Quick Start Guide" NavigateUrl="~/Documentation/User Setup and Preferences Quick Start Guide.pdf" Target="_blank"></asp:HyperLink>
        </td>
    </tr>     
    
   <tr align="left">
        <td style="width:323px">
            <asp:HyperLink runat="server" ID="lnkQuickStart14" Text="Dx Code Setup Quick Start Guide" NavigateUrl="~/Documentation/ICD-9 Setup Quick Start Guide.pdf" Target="_blank"></asp:HyperLink>
        </td>
    </tr>                                                                                                                                                         
</table>
