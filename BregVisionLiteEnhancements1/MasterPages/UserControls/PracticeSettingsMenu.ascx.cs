﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
namespace BregVision.MasterPages.UserControls
{
	public partial class PracticeSettingsMenu : System.Web.UI.UserControl
	{
		MainMenu mainMenu;
		protected void Page_Init(object sender, EventArgs e)
		{
			PracticeSettingsAdmin practiceSettingsMaster = (PracticeSettingsAdmin)Page.Master;
			PracticeAdmin practiceAdminMaster = (PracticeAdmin)practiceSettingsMaster.Master;
			SiteMaster siteMaster = (SiteMaster)practiceAdminMaster.Master;
			mainMenu = siteMaster.MainMenuControl;
		}


		protected void Page_Load(object sender, EventArgs e)
		{
			TabSettingsSiteBuilder.Visible = Context.User.IsInRole("BregAdmin");
		}

		public void SetTabVisibility(string tabName, bool visible)
		{
			FindToolbarTab(tabName).Visible = visible;
		}

		public void SelectCurrentTab(string tabName)
		{
			Telerik.Web.UI.RadTab tabSelected = FindToolbarTab(tabName);
			tabSelected.Selected = true;
		}

		public RadTab FindToolbarTab(string tabValue)
		{
			return PracticeSettingsTabStrip.FindTabByValue(tabValue);
		}
	}
}
