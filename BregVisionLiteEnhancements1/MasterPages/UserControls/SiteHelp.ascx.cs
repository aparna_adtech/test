﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BregVision.MasterPages.UserControls
{
  public partial class SiteHelp : Bases.UserControlBase
  {
    public Telerik.Web.UI.RadTextBox HelpSearchTextBox
    {
      get { return txtHelpSearch; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        txtHelpSearch.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {ToolbarSearch(document.getElementById('" + txtHelpSearch.ClientID + "').value);return false;}} else {return true}; ");
        MockUpHome_17.Attributes.Add("onmousedown", "ToolbarSearch(document.getElementById('" + txtHelpSearch.ClientID + "').value); ");
      }
    }
  }
}