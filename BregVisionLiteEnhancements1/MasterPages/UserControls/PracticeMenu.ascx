﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeMenu.ascx.cs" Inherits="BregVision.MasterPages.UserControls.PracticeMenu" %>

<telerik:RadTabStrip ID="PracticeAdminTabStrip" runat="server">
      <Tabs>
        <telerik:RadTab ID="TabLocations" runat="server" NavigateUrl="~/Admin/PracticeLocation_All.aspx"
          ToolTip="Practice Locations" Value="TabLocations" Text="Locations"></telerik:RadTab>
        <telerik:RadTab ID="TabBillToAddress" runat="server" NavigateUrl="~/Admin/Practice_BillingAddress.aspx"
          ToolTip="Practice Bill To Address" Value="TabBillToAddress"
          Text="Address"></telerik:RadTab>
        <telerik:RadTab ID="TabContact" runat="server" NavigateUrl="~/Admin/Practice_Contact.aspx"
          ToolTip="Practice Contact" Value="TabContact" Text="Contact"></telerik:RadTab>
        <telerik:RadTab ID="TabPhysicians" runat="server" NavigateUrl="~/Admin/Practice_Physicians.aspx"
          ToolTip="Practice Clinicians" Value="TabPhysicians" Text="Clinicians"></telerik:RadTab>
        <telerik:RadTab ID="TabSuppliers" runat="server" NavigateUrl="~/Admin/Practice_Supplier.aspx"
          ToolTip="Third Party Suppliers" Value="TabSuppliers" Text="Suppliers"></telerik:RadTab>
        <telerik:RadTab ID="TabVendorBrands" runat="server" Text="Vendor Brands" ToolTip="Practice Vendor Brands"
          Value="TabVendorBrands" Visible="false"></telerik:RadTab>
        <telerik:RadTab ID="TabProducts" runat="server" NavigateUrl="~/Admin/Practice_Product.aspx"
          ToolTip="Third Party Supplier Products" Value="TabProducts"
          Text="Products"></telerik:RadTab>
        <telerik:RadTab ID="TabCatalog" runat="server" NavigateUrl="~/Admin/Practice_Catalog.aspx"
          ToolTip="Practice Catalog Setup and Maintenance" Value="TabCatalog"
          Text="Catalog"></telerik:RadTab>
        <telerik:RadTab ID="TabBarCode" runat="server" NavigateUrl="~/Admin/BarCode.aspx"
          ToolTip="Print Bar Code " Value="TabBarCode" Text="Bar Codes"></telerik:RadTab>
        <telerik:RadTab ID="TabUsers" runat="server" NavigateUrl="~/Admin/Practice_UserAdmin.aspx"
          ToolTip="Practice User Setup and Maintenance" Value="TabUsers"
          Text="Users"></telerik:RadTab>
        <telerik:RadTab ID="TabFeeSchedule" runat="server" NavigateUrl="~/Admin/Practice_FeeSchedule.aspx"
          ToolTip="Payer Fee Schedule Setup and Maintenance" Value="TabFeeSchedule"
          Text="Payer Fee Schedule"></telerik:RadTab>
        <telerik:RadTab ID="TabSettings" runat="server" NavigateUrl="~/Admin/PracticeSettings_All.aspx"
          ToolTip="Practice Setup and Maintenance" Value="TabSettings"
          Text="Settings"></telerik:RadTab>
      </Tabs>
    </telerik:RadTabStrip>