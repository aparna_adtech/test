<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeAdminTabStripLocation.ascx.cs" Inherits="BregVision.MasterPages.UserControls.PracticeAdminTabStripLocation" %>
<%@ Register Assembly="RadTabStrip.Net2" Namespace="Telerik.WebControls" TagPrefix="radTS" %>

<div id="divTabStripLocation">
    <table cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>
                &nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td colspan="6">
                <telerik:RadTabStrip ID="rmpLocation" runat="server" AutoPostBack="true" EnableImmediateNavigation="true" 
                    CausesValidation="false" EnableEmbeddedSkins="false" Skin="Vision" Width="100%">
                    <Tabs>
                        <telerik:RadTab ID="TabLocationShippingAddress" runat="server" Text="Addresses" NavigateUrl="~/Admin/PracticeLocation_ShippingAddress.aspx"
                            ToolTip="Location Shipping and Bill To Addresses" />
                        <telerik:RadTab ID="TabLocationContact" runat="server" Text="Contact" NavigateUrl="~/Admin/PracticeLocation_Contact.aspx"
                            ToolTip="Location Main Contact" />
                        <telerik:RadTab ID="TabLocationPhysician" runat="server" Text="Physicians" NavigateUrl="~/Admin/PracticeLocation_Physician.aspx"
                            ToolTip="Location Physicians" />
                        <telerik:RadTab ID="TabLocationEmail" runat="server" Text="Email" NavigateUrl="~/Admin/PracticeLocation_Email.aspx"
                            ToolTip="Location Email Addresses" />
                        <telerik:RadTab ID="TabLocationSupplierCodes" runat="server" Text="Supplier Numbers" NavigateUrl="~/Admin/PracticeLocation_SupplierCodes.aspx"
                            ToolTip="Location Supplier Account Numbers" />
                        <telerik:RadTab ID="TabPracticeLocationInventoryLevel" runat="server" Text="Inventory Levels" NavigateUrl="~/Admin/PracticeLocation_InventoryLevels.aspx" 
                            ToolTip="Location Inventory Level Settings and Maintenance" />                       
                        <telerik:RadTab ID="TabLocationDispensementModifications" runat="server" Text="Dispensement Modifications" NavigateUrl="~/Admin/PracticeLocation_DispensementModifications.aspx"
                            ToolTip="Dispensement Modifications" />                          
                        <telerik:RadTab ID="TabLocationSuperbill" runat="server" Visible="false" Text="Billing Forms Setup" NavigateUrl="~/Admin/PracticeLocation_Superbill.aspx" 
                            ToolTip="Billing Forms Setup" />
                    </Tabs>
                </telerik:RadTabStrip>
            </td>
        </tr>
    </table>
</div>
