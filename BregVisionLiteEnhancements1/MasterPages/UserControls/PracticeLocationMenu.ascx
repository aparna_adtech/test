﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PracticeLocationMenu.ascx.cs" Inherits="BregVision.MasterPages.UserControls.PracticeLocationMenu" %>
  <h2 class="layout-padding no-margin-bottom">Locations</h2>
  <telerik:RadTabStrip ID="PracticeLocationTabStrip" runat="server" Orientation="VerticalLeft" Skin="Breg" EnableEmbeddedSkins="False" CssClass="layout-padding">
    <Tabs>
      <telerik:RadTab ID="TabLocationAll" Value="TabLocationAll"
        runat="server" NavigateUrl="~/Admin/PracticeLocation_All.aspx"
        Text="All Locations" ToolTip="Location Settings">
      </telerik:RadTab>
      <telerik:RadTab ID="TabLocationShippingAddress" Value="TabLocationShippingAddress"
        runat="server" NavigateUrl="~/Admin/PracticeLocation_ShippingAddress.aspx"
        Text="Addresses" ToolTip="Location Shipping and Bill To Addresses">
      </telerik:RadTab>
      <telerik:RadTab ID="TabLocationContact" Value="TabLocationContact" runat="server"
        NavigateUrl="~/Admin/PracticeLocation_Contact.aspx" Text="Contact"
        ToolTip="Location Main Contact">
      </telerik:RadTab>
      <telerik:RadTab ID="TabLocationPhysician" Value="TabLocationPhysician" runat="server"
        NavigateUrl="~/Admin/PracticeLocation_Physician.aspx" Text="Clinicians"
        ToolTip="Location Clinicians">
      </telerik:RadTab>
      <telerik:RadTab ID="TabLocationEmail" Value="TabLocationEmail" runat="server" NavigateUrl="~/Admin/PracticeLocation_Email.aspx"
        Text="Email" ToolTip="Location Email Addresses">
      </telerik:RadTab>
      <telerik:RadTab ID="TabLocationSupplierCodes" Value="TabLocationSupplierCodes" runat="server"
        NavigateUrl="~/Admin/PracticeLocation_SupplierCodes.aspx" Text="Supplier Numbers"
        ToolTip="Location Supplier Account Numbers">
      </telerik:RadTab>
      <telerik:RadTab ID="TabPracticeLocationInventoryLevel" Value="TabPracticeLocationInventoryLevel"
        runat="server" NavigateUrl="~/Admin/PracticeLocation_InventoryLevels.aspx"
        Text="Inventory Levels" ToolTip="Location Inventory Level Settings and Maintenance">
      </telerik:RadTab>
      <telerik:RadTab ID="TabLocationDispensementModifications" Value="TabLocationDispensementModifications"
        runat="server" NavigateUrl="~/Admin/PracticeLocation_DispensementModifications.aspx"
        Text="Dispensement Modifications" ToolTip="Dispensement Modifications">
      </telerik:RadTab>
      <telerik:RadTab ID="TabLocationSuperbill" Value="TabLocationSuperbill" runat="server"
        NavigateUrl="~/Admin/PracticeLocation_Superbill.aspx" Text="Billing Forms Setup"
        ToolTip="Billing Forms Setup" Visible="false">
      </telerik:RadTab>
      <telerik:RadTab ID="TabInventoryCount" Value="TabInventoryCount" runat="server"
        NavigateUrl="~/Admin/PracticeLocation_InventoryCount.aspx" Text="Inventory Count"
        ToolTip="Inventory Count">
      </telerik:RadTab>
    </Tabs>
  </telerik:RadTabStrip>
