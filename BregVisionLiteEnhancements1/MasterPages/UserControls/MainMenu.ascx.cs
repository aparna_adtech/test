﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace BregVision.MasterPages.UserControls
{
  public delegate void NavigateEventHandler(object sender, NavigateEventArgs e);

  public partial class MainMenu : Bases.UserControlBase
  {
    public event NavigateEventHandler navigateEvent;
    private RadTab adminTab;

    public RadTabStrip MainMenuTabStrip
    {
      get
      {
        return MainRadTabStrip;
      }
    }

    public System.Web.UI.Timer NavLabelTimer
    {
      get
      {
        return NavigateTimer;
      }
    }

    public Label NavigateLabel
    {
      get
      {
        return lblNavigate;
      }
    }


    new protected void Page_Init(object sender, EventArgs e)
    {
      base.Page_Init(sender, e);


      if (Context.User.IsInRole("BregAdmin") || Context.User.IsInRole("PracticeAdmin"))
      {
        adminTab = FindToolbarTab("Admin");
        if (Page.Master is SiteMaster || Page.Master == null)
        {
          adminTab.NavigateUrl = GetAdminPath();
        }
        else
        {
          adminTab.Value = "";
          adminTab.NavigateUrl = "";
          adminTab.Selected = true;

          //if (Page.Master is PracticeLocationAdmin)
          //{
          //  adminTab.Tabs[0].Selected = true;
          //}
        }
      }


      //AddTabStyle();
    }

    //<td style="background-image:transparent url(<%=GetAbsPath("~/MasterPages/Images/HomeLine.png")%>)  !important; background-repeat: repeat-x"></td>
    //private void AddTabStyle()
    //{
    //    string style = "<style>.backGroundTab{background: transparent url(" + GetAbsPath("~/MasterPages/Images/HomeLine.png") + ") repeat-x !important;padding-left: 0px !important;padding-right: 0px !important;}</style>";
    //    ltlTabStyle.Text = style;
    //    style = "<style>.backGroundTab2{background: transparent url(" + GetAbsPath("~/MasterPages/Images/spacer.png") + ") repeat-x !important;padding-left: 0px !important;padding-right: 0px !important;}</style>";
    //    LtlTabStyle2.Text = style;


    //}

    protected void Page_Load(object sender, EventArgs e)
    {
      InitializeAjaxSettings();

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
      //margin-left:100px;
      int buttonsVisible = 0;
      int offSetPixels = 0;
      foreach (RadTab tab in MainRadTabStrip.Tabs)
      {
        if (tab.Visible == true)
        {
          buttonsVisible += 1;
        }
      }
      offSetPixels = buttonsVisible * 100;
      divNavigateLabel.Style.Add("margin-left", string.Format("{0}px", offSetPixels.ToString()));

      // Set current tab to 'selected' based on URL
      RadTab currentTab = MainRadTabStrip.FindTabByUrl(Request.Url.PathAndQuery);
      if (currentTab != null) currentTab.Selected = true;

    }

    private void InitializeAjaxSettings()
    {
      Telerik.Web.UI.RadAjaxManager radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);
      if (radAjaxManager != null)
      {
        radAjaxManager.AjaxSettings.AddAjaxSetting(NavigateTimer, NavigatePanel);
        radAjaxManager.AjaxSettings.AddAjaxSetting(NavigateTimer, lblNavigate);
        radAjaxManager.AjaxSettings.AddAjaxSetting(MainRadTabStrip, NavigatePanel);
        radAjaxManager.AjaxSettings.AddAjaxSetting(MainRadTabStrip, lblNavigate);
      }
    }

    //OnTabClick="MainRadTabStrip_OnTabClick"  This is not enabled right now
    protected void MainRadTabStrip_OnTabClick(object sender, RadTabStripEventArgs e)
    {
      string controlName = e.Tab.Value;
      string navigateURL = e.Tab.NavigateUrl;

      FireNavigateEvent(controlName, navigateURL);
    }

    private void FireNavigateEvent(string controlName, string navigateURL)
    {
      if (!string.IsNullOrEmpty(navigateURL) && navigateEvent != null)
      {
        bool canNavigate = true;
        if (navigateURL == "#")
        {
          canNavigate = false;
        }
        NavigateEventArgs navigateEventArgs = new NavigateEventArgs(navigateURL, controlName, canNavigate);
        navigateEvent(this, navigateEventArgs);
        this.SelectCurrentTab(controlName);
      }
    }

    protected void NavigateTimer_OnTick(object sender, EventArgs e)
    {
      lblNavigate.Text = "";
      lblNavigate.Visible = false;

      ((Timer)sender).Enabled = false;
      NavigateTimer.Enabled = false;
    }

    public void HideAllMenus()
    {
      //Table_01.Visible = false; //home thru admin menu
      this.Visible = false;
    }

    public void SetTabVisibility(string tabName, bool visible)
    {
      if ((tabName.ToLower() != "admin") || !(Context.User.IsInRole("BregAdmin") || Context.User.IsInRole("PracticeAdmin")))
      {
        FindToolbarTab(tabName).Visible = visible;
      }
      else
      {
        //locate the adminTab tab
      }
    }

    public void SelectCurrentTab(string tabName)
    {
      FindToolbarTab(tabName).Selected = true;
    }
    public void SelectCurrentAdminTab(string tabName)
    {

    }

    public void SelectCurrentAdminLocationTab(string tabName)
    {
      //if (adminTab != null)
      //{
      //    RadTab selectorTab = adminTab.Tabs[0];
      //    if (selectorTab != null)
      //    {
      //        foreach (Telerik.Web.UI.RadTab t in selectorTab.Tabs)
      //        {
      //            if (t.Value == tabName)
      //            {
      //                return t;
      //            }
      //        }
      //    }
      //}
    }

    public RadTab FindToolbarTab(string tabValue)
    {
      RadTab tab;

      tab = MainRadTabStrip.FindTabByValue(tabValue);
      return tab;
    }

    public RadTab FindAdminToolbarTab(string tabValue)
    {
      if (adminTab != null)
      {
        foreach (Telerik.Web.UI.RadTab t in adminTab.Tabs)
        {
          if (t.Value == tabValue)
          {
            return t;
          }
        }
      }
      return null;
    }

    public string GetAbsPath(string imagePath)
    {
      return BLL.Website.GetAbsPath(imagePath);
    }

    public string GetAdminPath()
    {
      return BLL.Website.GetAdminPath(BLL.Website.IsVisionExpress());
    }
  }

  public class NavigateEventArgs : EventArgs
  {
    public NavigateEventArgs(string navigateURL, string buttonName, bool canNavigate)
    {
      NavigateURL = navigateURL;
      ButtonName = buttonName;
      CanNavigate = canNavigate;
    }
    public string ButtonName { get; set; }
    public string NavigateURL { get; set; }
    public bool CanNavigate { get; set; }
  }
}