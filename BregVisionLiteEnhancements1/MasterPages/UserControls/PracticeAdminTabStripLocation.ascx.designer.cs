namespace BregVision.MasterPages.UserControls {
    
    
    public partial class PracticeAdminTabStripLocation {
        
        /// <summary>
        /// rmpLocation control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadTabStrip rmpLocation;
        
        /// <summary>
        /// TabLocationShippingAddress control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadTab TabLocationShippingAddress;
        
        /// <summary>
        /// TabLocationContact control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadTab TabLocationContact;
        
        /// <summary>
        /// TabLocationPhysician control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadTab TabLocationPhysician;
        
        /// <summary>
        /// TabLocationEmail control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadTab TabLocationEmail;
        
        /// <summary>
        /// TabLocationSupplierCodes control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadTab TabLocationSupplierCodes;
        
        /// <summary>
        /// TabPracticeLocationInventoryLevel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadTab TabPracticeLocationInventoryLevel;
        
        /// <summary>
        /// TabLocationDispensementModifications control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadTab TabLocationDispensementModifications;
        
        /// <summary>
        /// TabLocationSuperbill control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadTab TabLocationSuperbill;
    }
}
