<%@ Page Language="C#"AutoEventWireup="true" CodeBehind="MCtoPC.aspx.cs" Inherits="BregVision.MCtoPC" %>

<%@ Register Assembly="RadAjax.Net2" Namespace="Telerik.WebControls" TagPrefix="radA" %>
<%@ Register Assembly="RadGrid.Net2" Namespace="Telerik.WebControls" TagPrefix="radG" %>
<%@ Register Assembly="RadInput.Net2" Namespace="Telerik.WebControls" TagPrefix="radI" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script type="text/javascript">
 var grid;

 

 function GridCreated()
 {
   grid = this;
 }
</script>  

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat=server>
    <title>Master Catalog</title>
    <meta content="JavaScript" name="vs_defaultClientScript">
    <script type="text/javascript" src="dragdrop.js" ></script>
</head>
<body >
    <form id="Form1" method="post" runat="server">
    <div>

        <radA:RadAjaxPanel ID="RadAjaxPanel1" runat="server"  Width="100%">
       <table width="100%" cellpadding="0" cellspacing="0">
				<tbody>
					
					
					<tr>
						<th id="Grid1_head" align=left>
							Master Catalog</th><th>&nbsp;</th><th id="Grid2_head" align=left>Practice Catalog</th>
					</tr>
                    <tr>
					    <td>
					        <asp:Label ID="lblMCStatus" runat=server  Font-Size=Small style="color:Red" >
					        </asp:Label>
					    </td>
					    <td>
					    </td>
					    <td>
					        <table width="100%">
					            <tr>
					                <td>
					                    <asp:Label ID="lblPCStatus" runat=server  Font-Size=Small style="color:Red" >
					                    </asp:Label>
					                
					                </td>
					                <td align=right>
					                    <asp:Button ID="btnUpdatePC" runat=server Text="Update Pricing" OnClick="btnUpdatePC_Click" />
					                   
					                </td>
					            </tr>
					        </table>
					        
					    </td>
					</tr>							
					<tr>
						<td width="47%" align="left" valign="top">
						
            <radG:RadGrid ID="grdMasterCatalog" runat="server" GridLines="None" OnPreRender="grdMasterCatalog_PreRender" OnNeedDataSource="grdMasterCatalog_NeedDataSource" OnDetailTableDataBind="grdMasterCatalog_DetailTableDataBind" OnPageIndexChanged="grdMasterCatalog_PageIndexChanged" EnableAJAX="True" EnableAJAXLoadingTemplate="True" LoadingTemplateTransparency="10" AllowPaging="True" AllowSorting="True" ShowGroupPanel="True" Skin="Office2007" Width="98%" AllowMultiRowSelection="True">
                <MasterTableView DataKeyNames="SupplierID" AutoGenerateColumns="False" EnableTheming=True>
                    <ExpandCollapseColumn Resizable="False">
                        <HeaderStyle Width="20px" />
                    </ExpandCollapseColumn>
                    <DetailTables>
                        <radG:GridTableView runat="server" DataKeyNames="SupplierID" AutoGenerateColumns="False">
                         <ParentTableRelation>
                            <radG:GridRelationFields DetailKeyField="SupplierID" MasterKeyField="SupplierID" />
                        </ParentTableRelation>
                            <ExpandCollapseColumn Visible="False" Resizable="False">
                                <HeaderStyle Width="20px" />
                            </ExpandCollapseColumn>
                            <RowIndicatorColumn Visible="False">
                                <HeaderStyle Width="20px" />
                            </RowIndicatorColumn>
								
								
								<Columns>
									<radG:GridClientSelectColumn UniqueName="ClientSelectColumn"/>
									<radg:GridBoundColumn DataField="MastercatalogProductID" display=False UniqueName="MastercatalogProductID"></radg:GridBoundColumn>
									<radg:GridBoundColumn DataField="SupplierID" visible=False UniqueName="SupplierID"></radg:GridBoundColumn>
									<radg:GridBoundColumn  DataField="MasterCatalogSubCategoryID" display=False UniqueName="MasterCatalogSubCategoryID"></radg:GridBoundColumn>
									<radg:GridBoundColumn  DataField="ShortName" HeaderText="Name:" UniqueName="ShortName"></radg:GridBoundColumn>
									<radg:GridBoundColumn  DataField="Code" HeaderText="Code:" UniqueName="Code"></radg:GridBoundColumn>
									<radg:GridBoundColumn  DataField="Packaging" HeaderText="pkg." UniqueName="Packaging"></radg:GridBoundColumn>
									<radg:GridBoundColumn  DataField="LeftRightSide" HeaderText="Side:" UniqueName="LeftRightSide"></radg:GridBoundColumn>
									<radg:GridBoundColumn  DataField="Size" HeaderText="Size:" UniqueName="Size"></radg:GridBoundColumn>
									<radg:GridBoundColumn  DataField="Color" HeaderText="Color:" UniqueName="Color"></radg:GridBoundColumn>
									<radg:GridBoundColumn  DataField="WholesaleListCost" HeaderText="WLC:" DataFormatString="{0:C}" UniqueName="WholesaleListCost"></radg:GridBoundColumn>
							</Columns>
								 
				              </radG:GridTableView>
                                </DetailTables>
                                <RowIndicatorColumn Visible="False">
                                    <HeaderStyle Width="20px" />
                                </RowIndicatorColumn>
                                <Columns>
							<radG:GridBoundColumn DataField="SupplierID" UniqueName="SupplierID" Visible="False">
                                    </radG:GridBoundColumn>
                                    <radG:GridBoundColumn DataField="Brand" HeaderText="Brand" UniqueName="Brand">
                                    </radG:GridBoundColumn>
                                    <radG:GridBoundColumn DataField="Name" HeaderText="Name" UniqueName="Name">
                                    </radG:GridBoundColumn>
                                    <radG:GridBoundColumn DataField="LastName" UniqueName="LastName">
                                    </radG:GridBoundColumn>
                                    <radG:GridBoundColumn DataField="PhoneWork" HeaderText="Phone" UniqueName="PhoneWork">
                                    </radG:GridBoundColumn>
                                    <radG:GridBoundColumn DataField="Address" HeaderText="Address" UniqueName="Address">
                                    </radG:GridBoundColumn>
                                    <radG:GridBoundColumn DataField="City" HeaderText="City" UniqueName="City">
                                    </radG:GridBoundColumn>
                                    <radG:GridBoundColumn DataField="ZipCode" HeaderText="Zip Code" UniqueName="ZipCode">
                                    </radG:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                            <GroupPanel Visible="True">
                            </GroupPanel>
                            <ClientSettings AllowColumnsReorder="false" AllowDragToGroup="false" ReorderColumnsOnClient="false">
                                <Selecting AllowRowSelect="True"/>
                                 <ClientEvents OnGridCreated="GridCreated" ></ClientEvents>
                            </ClientSettings>
                            <ExportSettings>
                                <Pdf PageBottomMargin="" PageFooterMargin="" PageHeaderMargin="" PageHeight="11in"
                                    PageLeftMargin="" PageRightMargin="" PageTopMargin="" PageWidth="8.5in" />
                            </ExportSettings>
                            
                            
                        </radG:RadGrid>
                           </td>
						<td width="6%" align=center>
						<asp:Button runat=server ID="btnMoveToPC" Text=">>" ToolTip="Move all selected items to Practice Catalog" OnClick="btnMoveToPC_Click" />
						<br />
						<asp:Button runat=server ID="btnMoveToMC" Text="<<" ToolTip="Remove all selected items from Practice Catalog" />
						</td>
						<td width="47%" align="left" valign="top">
						
             <radG:RadGrid ID="grdPracticeCatalog" runat="server" GridLines="None" OnPreRender="grdPracticeCatalog_PreRender" OnNeedDataSource="grdPracticeCatalog_NeedDataSource" OnDetailTableDataBind="grdPracticeCatalog_DetailTableDataBind" OnPageIndexChanged="grdPracticeCatalog_PageIndexChanged" EnableAJAX="True" EnableAJAXLoadingTemplate="True" LoadingTemplateTransparency="10" AllowPaging="True" AllowSorting="True" ShowGroupPanel="True" Skin="Office2007" Width="98%" AllowMultiRowSelection="True">
                <MasterTableView DataKeyNames="PracticeCatalogSupplierBrandID" AutoGenerateColumns="False"  EnableTheming=True>
                    <ExpandCollapseColumn Resizable="False">
                        <HeaderStyle Width="20px" />
                    </ExpandCollapseColumn>
                    

                    <DetailTables>

                        <radG:GridTableView DataKeyNames="PracticeCatalogSupplierBrandID" runat="server"  AutoGenerateColumns="False">
                            <ParentTableRelation>
                            <radG:GridRelationFields DetailKeyField="PracticeCatalogSupplierBrandID" MasterKeyField="PracticeCatalogSupplierBrandID" />
                        </ParentTableRelation>
                            <ExpandCollapseColumn Visible="False" Resizable="False">
                                <HeaderStyle Width="20px" />
                            </ExpandCollapseColumn>
                            <RowIndicatorColumn Visible="False">
                                <HeaderStyle Width="20px" />
                            </RowIndicatorColumn>
								
								
								<Columns>
									<radG:GridClientSelectColumn UniqueName="ClientSelectColumn2"/>
									<radg:GridBoundColumn  DataField="PracticeCatalogSupplierBrandID" Visible=False UniqueName="PracticeCatalogSupplierBrandID" ></radg:GridBoundColumn>
									<radg:GridBoundColumn  DataField="PracticeCatalogProductID" display=False UniqueName="PracticeCatalogProductID"></radg:GridBoundColumn>
									<radg:GridBoundColumn DataField="MasterCatalogProductID" display=False UniqueName="MasterCatalogProductID"></radg:GridBoundColumn>
									<radg:GridBoundColumn DataField="ShortName" HeaderText="Name:" UniqueName="ShortName"></radg:GridBoundColumn>
									<radg:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code"></radg:GridBoundColumn>
									
									<radG:GridTemplateColumn  UniqueName="TemplateColumn2" DataField="WholesaleCost"  HeaderText="Wholesale Cost" HeaderStyle-Wrap=true HeaderStyle-Width=50>
                                         <ItemTemplate>
                                             &nbsp;<radI:RadNumericTextBox  ID="txtWholesaleCost" Text='<%# bind("WholesaleCost") %>' runat="server"  ShowSpinButtons="False" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="20px" Type="Currency" Font-Size="X-Small" NumberFormat-DecimalDigits="2" >
                                            <NumberFormat DecimalDigits="0" />
                                        </radI:RadNumericTextBox>
                                        </ItemTemplate>
                                    </radG:GridTemplateColumn>
                                    <radG:GridTemplateColumn  UniqueName="TemplateColumn3" DataField="BillingCharge"  HeaderText="Billing Charge" HeaderStyle-Wrap=true HeaderStyle-Width=50>
                                         <ItemTemplate>
                                             &nbsp;<radI:RadNumericTextBox  ID="txtBillingCharge" Text='<%# bind("BillingCharge") %>' runat="server"  ShowSpinButtons="False" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="20px" Type="Currency"  Font-Size="X-Small"  NumberFormat-DecimalDigits="2">
                                            <NumberFormat DecimalDigits="0" />
                                        </radI:RadNumericTextBox>
                                        </ItemTemplate>
                                    </radG:GridTemplateColumn>
                                     <radG:GridTemplateColumn  UniqueName="TemplateColumn4" DataField="BillingChargeCash"  HeaderText="Billing Charge Cash" HeaderStyle-Wrap=true HeaderStyle-Width=50>
                                         <ItemTemplate>
                                             &nbsp;<radI:RadNumericTextBox  ID="txtBillingChargeCash" Text='<%# bind("BillingChargeCash") %>' runat="server"  ShowSpinButtons="False" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="20px" Type="Currency"  Font-Size="X-Small"  NumberFormat-DecimalDigits="2">
                                            <NumberFormat DecimalDigits="0" />
                                        </radI:RadNumericTextBox>
                                        </ItemTemplate>
                                    </radG:GridTemplateColumn>     
                                 <radG:GridTemplateColumn  UniqueName="TemplateColumn5" DataField="DMEDeposit" HeaderText="DME Deposit" HeaderStyle-Wrap=true HeaderStyle-Width=50>
                                         <ItemTemplate>
                                             &nbsp;<radI:RadNumericTextBox  ID="txtDMEDeposit" Text='<%# bind("DMEDeposit") %>' runat="server"  ShowSpinButtons="False" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="20px"  Type="Currency"   Font-Size="X-Small"  NumberFormat-DecimalDigits="2">
                                            <NumberFormat DecimalDigits="0" />
                                        </radI:RadNumericTextBox>
                                        </ItemTemplate>
                                    </radG:GridTemplateColumn>                                                                     
									
									
									
									
									
							</Columns>
								 
				              </radG:GridTableView>
                                </DetailTables>
                                <RowIndicatorColumn Visible="False">
                                    <HeaderStyle Width="20px" />
                                </RowIndicatorColumn>
                               
                                <Columns>
							        <radG:GridBoundColumn DataField="PracticeCatalogSupplierBrandID" UniqueName="PracticeCatalogSupplierBrandID"  Visible=False >
                                    </radG:GridBoundColumn>
                                    <radG:GridBoundColumn DataField="BrandName" HeaderText="Brand" UniqueName="BrandName">
                                    </radG:GridBoundColumn>
                                    
                                </Columns>
                            </MasterTableView>
                            <GroupPanel Visible="True">
                            </GroupPanel>
                            <ClientSettings AllowColumnsReorder="false" AllowDragToGroup="false" ReorderColumnsOnClient="false">
                                <Selecting AllowRowSelect="True"/>
                                 <ClientEvents OnGridCreated="GridCreated" ></ClientEvents>
                            </ClientSettings>
                            <ExportSettings>
                                <Pdf PageBottomMargin="" PageFooterMargin="" PageHeaderMargin="" PageHeight="11in"
                                    PageLeftMargin="" PageRightMargin="" PageTopMargin="" PageWidth="8.5in" />
                            </ExportSettings>
                            
                            
                        </radG:RadGrid>
                        </td>
					</tr>
				</tbody>
			</table>
			</radA:RadAjaxPanel>
			<br>
			<!--<div id="DeleteRow" class="DragDiv" onmouseover="drover(this);" onmouseout="drout(this);"><img src="Img/DeletedItems.gif" alt="Deleted Items">Drag 
				a row here to delete it.</div>-->
       
      
    </div>
    </form>
</body>

</html>
									