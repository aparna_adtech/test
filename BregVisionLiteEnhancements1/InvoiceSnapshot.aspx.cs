using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Text;
using System.Text.RegularExpressions;

namespace BregVision
{
    public partial class InvoiceSnapshot : System.Web.UI.Page
    {

        string strGrandTotal;

		public bool IsCustomBrace
		{
			get; set;
		}

        protected void Page_Load(object sender, EventArgs e)
        {
			txtCustomPO.DataBind();

			try
            {
                ctlRecordsPerPage.recordsPerPageChangedEvent += new UserControls.General.RecordsPerPageEventHandler(ctlRecordsPerPage_recordsPerPageChangedEvent);

                Page.ClientScript.RegisterStartupScript(
                    this.GetType(),
                    "PreventBackButton",
                    "try { window.history.forward(1); } catch (e) {}",
                    true);

                if (!Page.IsPostBack)
                {
                    // hide location dropdown selector because it's not valid here
                    {
                        var m = this.Master as BregVision.MasterPages.SiteMaster;
                        if (m != null)
                            m.SetLocationDropdownVisibility(false);
                    }

					var practice_location_id = Convert.ToInt32(Session["PracticeLocationID"]);
					var shopping_cart_id = GetShoppingCartID(practice_location_id);

					CustomBrace.ShoppingCartID = shopping_cart_id;

					IsCustomBrace = GetIsCustomBrace(shopping_cart_id, null);

					if (IsCustomBrace)
					{
						PopulatePatientShippingBilling(shopping_cart_id);
					}
					else
					{
						PopulateBillingAddress(practice_location_id);
						PopulateShippingAddress(practice_location_id);
					}
					lblShippingType.Text = GetShippingType(Convert.ToInt16(Session["ShippingTypeID"]));
                    PopulateCustomPONumber(practice_location_id);

                    var strGrandTotal = "notsetyet";
                    if (Session["PracticeLocationID"] != null)
                    {
                        var decGrandTotal = GetInvoiceSnapshot_GrandTotal(practice_location_id);
                        strGrandTotal = String.Format("{0:$#,0.00}", decGrandTotal);
                        Session["InvSnap_GrandTotal"] = strGrandTotal;
                    }
                }

                if (Session["InvSnap_GrandTotal"] != null)
                {
                    strGrandTotal = Session["InvSnap_GrandTotal"].ToString();
                }
            }
            catch { }

			btnModifyOrder.Visible = IsCustomBrace;
		}

		private Int32 GetShoppingCartID(Int32 PracticeLocationID)
		{
			Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
			DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetShoppingCartID");
			db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);

			Int32 SQLReturn = Convert.ToInt32(db.ExecuteScalar(dbCommand));
			return SQLReturn;
		}

		private bool GetIsCustomBrace(int? shoppingCartID, int? bregVisionOrderID)
		{
			Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
			DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetCustomBraceOrder");
			if (shoppingCartID != null)
				db.AddInParameter(dbCommand, "ShoppingCartID", DbType.Int32, shoppingCartID.Value);
			if (bregVisionOrderID != null)
				db.AddInParameter(dbCommand, "BregVisionOrderID", DbType.Int32, bregVisionOrderID.Value);

			DataSet ds = db.ExecuteDataSet(dbCommand);
			if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
				return true;
			return false;
		}

		void ctlRecordsPerPage_recordsPerPageChangedEvent(object sender, UserControls.General.RecordsPerPageEventArgs e)
        {
            grdInvoiceSnapshot.PageSize = e.NewRecordsPerPage;
            grdInvoiceSnapshot.Rebind();
            ExpandGrid();
        }






        //  20071019 JB  Get Parent with Total given the PracticeLocationID
        public DataSet GetInvoiceSnapshot_Parent(int PracticeLocationID)
        {

            try
            {

                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");

                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetInvoiceSnapshotShoppingCartItems_ParentLevel_Given_PracticeLocationID");

                db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);

                DataSet ds = db.ExecuteDataSet(dbCommand);

                return ds;
            }
            catch { return null; }
        }

        //[usp_GetInvoiceSnapshotShoppingCartItems_ParentLevel_GrandTotal_Given_PracticeLocationID]

        //  20071019 JB  Get Parent with Total given the PracticeLocationID
        public decimal GetInvoiceSnapshot_GrandTotal(int PracticeLocationID)
        {

            try
            {

                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");

                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetInvoiceSnapshotShoppingCartItems_ParentLevel_GrandTotal_Given_PracticeLocationID");

                db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);

                DataTable dt = db.ExecuteDataSet(dbCommand).Tables[0];
                DataRow dr = dt.Rows[0];
                string sGrandTotal = dr.ItemArray[0].ToString();
                //  ecimal grandTotal = Convert.ToDecimal( strGrandTotal );

                decimal decGTotal = Convert.ToDecimal(sGrandTotal);

                return decGTotal;
            }
            catch { return decimal.Zero; }
        }


        public DataSet GetInvoiceSnapshot_Detail(int PracticeLocationID)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetInvoiceSnapshotShoppingCartItems_Detail");

                db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);
                DataSet ds = db.ExecuteDataSet(dbCommand);
                return ds;
            }
            catch { return null; }

        }

        protected void grdInvoiceSnapshot_NeedDataSource(object source, Telerik.WebControls.GridNeedDataSourceEventArgs e)
        {
            try
            {
                if (!e.IsFromDetailTable)
                {
                    //  20071019 use PracticeLocationID
                    DataSet dsInvoiceParent = GetInvoiceSnapshot_Parent(Convert.ToInt16(Session["PracticeLocationID"]));
                    if (dsInvoiceParent != null)
                    {
                        grdInvoiceSnapshot.DataSource = dsInvoiceParent;
                    }
                }

                grdInvoiceSnapshot.MasterTableView.DetailTables[0].DataSource = GetInvoiceSnapshot_Detail(Convert.ToInt16(Session["PracticeLocationID"]));

            }
            catch { }
        }


        //decimal total;
        int quantity;
        decimal subtotal;
        protected void grdInvoiceSnapshot_ItemDataBound(object sender, Telerik.WebControls.GridItemEventArgs e)
        {
            try
            {
                if (e.Item is GridDataItem)
                {
                    try
                    {
                        //  20071022 JB
                        GridDataItem dataItem = e.Item as GridDataItem;
                        //decimal fieldValue = System.Convert.ToDecimal(dataItem["ActualWholesaleCost"].Text);
                        //decimal totalCostItem = Convert.ToDecimal(dataItem["TotalCost"].Text.Replace("$",""));           //fieldValue * System.Convert.ToDecimal(fieldValue2);
                        //dataItem["TotalCost"].Text = totalCostItem.ToString();
                        //Int16 fieldValue = 1;
                        //total += fieldValue;

                        //int fieldValue2 = (dataItem["Quantity"] != null) ? Convert.ToInt32(dataItem["Quantity"].Text) : 0;
                        int fieldValue2 = dataItem.GetColumnInt32("Quantity");

                        GridColumn safeLineTotal = dataItem.OwnerTableView.GetColumnSafe("LineTotal");
                        if (safeLineTotal != null)
                        {
                            Label lblLineTotalLabel = dataItem["LineTotal"].FindControl("lblLineTotal") as Label;
                            if (lblLineTotalLabel != null)
                            {
                                string lineTotalValue = lblLineTotalLabel.Text.Replace("$", "").Replace(",", "");

                                decimal lineTotal = 0; // Convert.ToDecimal(lineTotalValue);
                                if (Decimal.TryParse(lineTotalValue, out lineTotal))
                                {
                                    quantity += fieldValue2;
                                    subtotal += lineTotal;
                                }
                            }
                        }
                    }
                    catch
                    {

                    }



                }
                if (e.Item is GridFooterItem)
                {

                    GridFooterItem footerItem = e.Item as GridFooterItem;
                    //footerItem["UnitCost"].Text = "Total: " + total.ToString();
                    //footerItem["Quantity"].Text = "# Items in Cart: " + quantity.ToString();
                    //footerItem["totalCost"].Text = "Sub Total: " + totalCost.ToString();
                    ///footerItem["totalCost"].Text =  String.Format("Sub Total: {0:$#,#.00}", totalCost);

                    try
                    {
                        GridColumn safeTotalCost = footerItem.OwnerTableView.GetColumnSafe("TotalCost");
                        if (safeTotalCost != null)
                        {
                            if (footerItem["TotalCost"] != null)
                            {
                                Label lblGrandTotalLabel = footerItem["TotalCost"].FindControl("lblGrandTotal") as Label;
                                if (lblGrandTotalLabel != null)
                                {
                                    lblGrandTotalLabel.Text = String.Format("{0:$#,0.00}", strGrandTotal);
                                    lblGrandTotalLabel.Font.Underline = true;
                                    //footerItem["GrandTotal"].Text = "Grand Total: $44,444.44";
                                }
                            }
                        }
                    }
                    catch
                    {
                    }
                    try
                    {
                        GridColumn safeQuantity = footerItem.OwnerTableView.GetColumnSafe("Quantity");
                        GridColumn safeLineTotal = footerItem.OwnerTableView.GetColumnSafe("LineTotal");
                        GridColumn safeBrandShortName = footerItem.OwnerTableView.GetColumnSafe("BrandShortName");
                        if (safeQuantity != null && safeLineTotal != null && safeBrandShortName != null)
                        {
                            if (footerItem["Quantity"] != null && footerItem["LineTotal"] != null && footerItem["BrandShortName"] != null)
                            {
                                //footerItem["Quantity"].Text = String.Format("# Items in Cart: {0:#,#}", quantity);
                                //footerItem["LineTotal"].Text = String.Format("Sub Total: {0:$#,0.00}", subtotal);

                                footerItem["Quantity"].Text = String.Format("{0:#,#}", quantity);
                                footerItem["Quantity"].Font.Overline = true;

                                footerItem["LineTotal"].Text = String.Format("{0:$#,0.00}", subtotal);
                                footerItem["LineTotal"].Font.Overline = true;

                                footerItem["BrandShortName"].Text = "Sub Total:";
                                footerItem["BrandShortName"].Font.Overline = true;

                                //2008.01.28 JB  Since Sub Total Footer Rows has been set, reset the quantity and subtotal.
                                quantity = 0;      //reset to 0 for the Brand Qty and Subtotal shown in the footer.
                                subtotal = 0;      //reset to 0 for the Brand Qty and Subtotal shown in the footer.

                            }
                        }
                    }
                    catch
                    {
                    }
                    //GridBoundColumn column = (GridBoundColumn)grdShoppingCart.MasterTableView.GetColumn("UnitCost");
                    //column.DataFormatString = "{0:c}"; 
                }
            }
            catch { }
        }

		protected void ModifyPO_Click(object sender, EventArgs e)
		{
			try
			{
				Response.Redirect("POShipBillCustomBrace.aspx");
			}
			catch { }
		}

		protected void SubmitPO_Click(object sender, EventArgs e)
		{
			try
			{
				// store custom purchase order code in database for future retrieval
				UpdateShoppingCartCustomPOCode();

				// redirect to routing purchase order
				Response.Redirect("RoutePurchaseOrder.aspx");
			}
			catch { }
		}

		private void UpdateShoppingCartCustomPOCode()
        {
            // strip leading/trailing white space and replace double-space with single space
            var custPONum = Regex.Replace(txtCustomPO.Text.Trim(), @"\s+", " ");

            var db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            var cmd = db.GetStoredProcCommand("usp_ShoppingCart_Update_CustomPurchaseOrderCodeByPracticeLocationID");
            db.AddInParameter(
                cmd,
                "PracticeLocationID",
                DbType.Int32,
                Convert.ToInt32(Session["PracticeLocationID"]));
            db.AddInParameter(
                cmd,
                "CustomPurchaseOrderCode",
                DbType.String,
                string.IsNullOrEmpty(custPONum)
                    ? (object)DBNull.Value : custPONum);
            db.ExecuteNonQuery(cmd);
        }

        private void PopulateBillingAddress(int practiceLocationID)
        {
            try
            {
                //int rowsAffected = 0;

                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.USP_GetBillingAddressByPracticeLocation");

                db.AddInParameter(dbCommand, "PracticeLocationID", DbType.String, practiceLocationID);

                IDataReader reader = db.ExecuteReader(dbCommand);

                if (reader.Read())
                {
                    BillAttention.Text = reader["BillAttentionOf"].ToString();
                    BillAddress1.Text = reader["BillAddress1"].ToString();
                    BillAddress2.Text = reader["BillAddress2"].ToString();
                    BillCity.Text = reader["BillCity"].ToString();
                    BillState.Text = reader["BillState"].ToString();
                    BillZipCode.Text = reader["BillZipCode"].ToString();
                }
            }
            catch { }

        }

        private void PopulateShippingAddress(int practiceLocationID)
        {
            try
            {
                //int rowsAffected = 0;

                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.USP_GetShippingAddressByPracticeLocation");

                db.AddInParameter(dbCommand, "PracticeLocationID", DbType.String, practiceLocationID);

                IDataReader reader = db.ExecuteReader(dbCommand);

                if (reader.Read())
                {
                    ShipAttention.Text = reader["ShipAttentionOf"].ToString();
                    ShipAddress1.Text = reader["ShipAddress1"].ToString();
                    ShipAddress2.Text = reader["ShipAddress2"].ToString();
                    ShipCity.Text = reader["ShipCity"].ToString();
                    ShipState.Text = reader["ShipState"].ToString();
                    ShipZipCode.Text = reader["ShipZipCode"].ToString();
                }
            }
            catch { }
        }

		private void PopulatePatientShippingBilling(int shoppingCartID)
		{
			try
			{
				//int rowsAffected = 0;

				Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
				DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetCustomBraceOrder");

				db.AddInParameter(dbCommand, "ShoppingCartID", DbType.String, shoppingCartID);

				IDataReader reader = db.ExecuteReader(dbCommand);

				if (reader.Read())
				{
					PatientName.Text = reader["PatientName"].ToString();
					PatientAge.Text = reader["Patient_Age"].ToString();
					PatientWeight.Text = reader["Patient_Weight"].ToString();
					PatientHeight.Text = reader["Patient_Height"].ToString();

					ShipAttention.Text = reader["Shipping_Attention"].ToString();
					ShipAddress1.Text = reader["Shipping_Address1"].ToString();
					ShipAddress2.Text = reader["Shipping_Address2"].ToString();
					ShipCity.Text = reader["Shipping_City"].ToString();
					ShipState.Text = reader["Shipping_State"].ToString();
					ShipZipCode.Text = reader["Shipping_Zip"].ToString();

					BillAttention.Text = reader["Billing_Attention"].ToString();
					BillAddress1.Text = reader["Billing_Address1"].ToString();
					BillAddress2.Text = reader["Billing_Address2"].ToString();
					BillCity.Text = reader["Billing_City"].ToString();
					BillState.Text = reader["Billing_State"].ToString();
					BillZipCode.Text = reader["Billing_Zip"].ToString();
				}
			}
			catch { }
		}

		private string GetShippingType(int ShippingTypeID)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                DbCommand dbCommand = db.GetStoredProcCommand("dbo.USP_GetShippingType");

                db.AddInParameter(dbCommand, "ShippingTypeID", DbType.String, ShippingTypeID);

                string ShippingType = Convert.ToString(db.ExecuteScalar(dbCommand));
                return ShippingType;
            }
            catch { return "Unknown"; }

        }

        private void PopulateCustomPONumber(int practice_location_id)
        {
            try
            {
                var db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
                var cmd = db.GetStoredProcCommand("usp_ShoppingCart_Select_CustomPurchaseOrderCodeByPracticeLocationID");
                db.AddInParameter(cmd, "PracticeLocationID", DbType.Int32, practice_location_id);
                txtCustomPO.Text = db.ExecuteScalar(cmd).ToString();
            }
            catch { }
        }

        protected void grdInvoiceSnapshot_PreRender(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                ExpandGrid();
            }

        }

        private void ExpandGrid()
        {
            try
            {
                foreach (GridDataItem item in grdInvoiceSnapshot.MasterTableView.Items)
                {
                    //if (item.ItemIndex == 0)
                    //{
                    item.Expanded = true;
                    //item.ChildItem.NestedTableViews[0].Items[1].Expanded = true;
                    //}

                }
            }
            catch { }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            grdInvoiceSnapshot.PageSize = ctlRecordsPerPage.recordsDisplayedPerPage;

			if (IsCustomBrace)
				ValidatePO();
		}

		private void ValidatePO()
		{
			btnSubmitOrder.Enabled = true;

			if (PatientName.Text.Trim() == string.Empty)
			{
				ErrorName.Text = "Patient Name must be specified!";
				btnSubmitOrder.Enabled = false;
			}

			int age = 0;
			if (!TryParse(PatientAge.Text.Trim(), out age) || age <= 0)
			{
				ErrorAge.Text = "Patient Age is invalid!";
				btnSubmitOrder.Enabled = false;
			}

			int height = 0;
			if (!TryParse(PatientHeight.Text.Trim(), out height) || height <= 0)
			{
				ErrorHeight.Text = "Patient Height is invalid!";
				btnSubmitOrder.Enabled = false;
			}

			int weight = 0;
			if (!TryParse(PatientWeight.Text.Trim(), out weight) || weight <= 0)
			{
				ErrorWeight.Text = "Patient Weight is invalid!";
				btnSubmitOrder.Enabled = false;
			}

			if (btnSubmitOrder.Enabled)
				btnSubmitOrder.CssClass = "button-primary";
			else
				btnSubmitOrder.CssClass = "disabledbutton";
		}

		private bool TryParse(string str, out int val)
		{
			StringBuilder sb = new StringBuilder();
			sb.Clear();
			foreach (char c in str)
			{
				if ((c >= '0' && c <= '9') || c == ' ' || c == '-')
				{
					sb.Append(c);
				}
				else
					break;
			}
			return int.TryParse(sb.ToString(), out val);
		}
	}
}
