//Copyright(C)2002-2009 Pluginlab Limited. All rights reserved.
//http://www.pluginlab.com
//ProductID:PLHIM
//Registered to:akagboy
var JHDbZdD={dummy:0,
VERSION:'4.2.5.2',
MENU_NAME:'Cool Blue',
LAYER:false,
X:200,
Y:700,
VALIGN:0,
IS_FLOATING:false,
MENU_WIDTH:100,
MAIN_BACKGROUND:'',
DRAW_MAIN_BORDER:false,
MAIN_BORDER_COLOR:'#b9c5de',
DRAW_MAIN_BUTTON_BORDER:false,
MAIN_BUTTON_BORDER_COLOR:'#000000',
LEVEL2_IS_HORIZONTAL:false,
LEVEL2_LEFT_BACKGROUND:'',
LEVEL2_MIDDLE_BACKGROUND:'',
LEVEL2_RIGHT_BACKGROUND:'',
DRAW_LEVEL2_BORDER:false,
LEVEL2_BORDER_COLOR:'#000000',
DRAW_LEVEL2_BUTTON_BORDER:false,
LEVEL2_BUTTON_BORDER_COLOR:'#000000',
PRELOAD:true,
SHOW_SELECTED:false,
DEFAULT_WIDTH:169,
DEFAULT_HEIGHT:31,
DEFAULT_NORMAL:'*../../../images/05flyout.gif',
DEFAULT_OVER:'*../../../images/05flyoutover.gif',
DEFAULT_DOWN:'',
FO_HEIGHT:220,
FO_PADDING:1,
Z_INDEX:50,
OFFSET:true,
CENTER_FIRST_LEVEL:false,
OVERLAP:false,
PARENT_MO:true,
HAS_SHADOW:false,
OPEN_ANIMATION:2,
CLOSE_ANIMATION:2,
OPEN_SPEED:10,
CLOSE_SPEED:10,
DIRECTION:0,
SHOW_DELAY:400,
DRAW_FO_BORDER:false,
FO_BORDER_COLOR:'#b9c5de',
DRAW_FO_BUTTON_BORDER:false,
FO_BUTTON_BORDER_COLOR:'#b9c5de',
DRAW_SCROLLERS_BORDER:false,
SCROLLERS_BORDER_COLOR:'#b9c5de',
MAIN_OPACITY:100,
FO_OPACITY:100,
SEO_LINKS:0,
KEYBOARD:false,
TRANSFORM_PATHS:true,
MAIN_HEADING_CENTER:true,
MAIN_NORMAL_CENTER:true,
MAIN_VERTICAL_ALIGN:true,
MAIN_TOP_PADDING:6,
MAIN_PADDING:6,
MAIN_CROSSFADE:false,
MAIN_FONT:'Arial,Helvetica,sans-serif',
MAIN_FONT_SIZE:11,
MAIN_TEXT_COLOR:'#40465d',
MAIN_TEXT_MO_COLOR:'#ffffff',
MAIN_TEXT_MD_COLOR:'#ffffff',
MAIN_TEXT_SEL_COLOR:'#ffffff',
MAIN_TEXT_HEADING_COLOR:'#ffffff',
MAIN_WEIGHT:'bold',
MAIN_MO_WEIGHT:'bold',
MAIN_MD_WEIGHT:'bold',
MAIN_SEL_WEIGHT:'bold',
MAIN_HEADING_WEIGHT:'bold',
MAIN_DECORATION:0,
MAIN_MO_DECORATION:0,
MAIN_MD_DECORATION:0,
MAIN_SEL_DECORATION:0,
MAIN_HEADING_DECORATION:0,
MAIN_ITALIC:false,
MAIN_MO_ITALIC:false,
MAIN_MD_ITALIC:false,
MAIN_SEL_ITALIC:false,
MAIN_HEADING_ITALIC:false,
FLYOUT_HEADING_CENTER:true,
FLYOUT_NORMAL_CENTER:false,
FLYOUT_VERTICAL_ALIGN:true,
FLYOUT_TOP_PADDING:6,
FLYOUT_PADDING:25,
FLYOUT_CROSSFADE:false,
FLYOUT_FONT:'Arial,Helvetica,sans-serif',
FLYOUT_FONT_SIZE:11,
FLYOUT_TEXT_COLOR:'#40465d',
FLYOUT_TEXT_MO_COLOR:'#ffffff',
FLYOUT_TEXT_MD_COLOR:'#ffffff',
FLYOUT_TEXT_SEL_COLOR:'#ffffff',
FLYOUT_TEXT_HEADING_COLOR:'#ffffff',
FLYOUT_WEIGHT:'bold',
FLYOUT_MO_WEIGHT:'bold',
FLYOUT_MD_WEIGHT:'bold',
FLYOUT_SEL_WEIGHT:'bold',
FLYOUT_HEADING_WEIGHT:'bold',
FLYOUT_DECORATION:0,
FLYOUT_MO_DECORATION:0,
FLYOUT_MD_DECORATION:0,
FLYOUT_SEL_DECORATION:0,
FLYOUT_HEADING_DECORATION:0,
FLYOUT_ITALIC:false,
FLYOUT_MO_ITALIC:false,
FLYOUT_MD_ITALIC:false,
FLYOUT_SEL_ITALIC:false,
FLYOUT_HEADING_ITALIC:false,
LANGUAGE_RTL:false,
UP_ARROW:'*../../images/05scroller_up.gif',
UP_ARROW_OVER:'',
UP_ARROW_DISABLED:'*../../images/05scroller_up_dis.gif',
DOWN_ARROW:'*../../images/05scroller_down.gif',
DOWN_ARROW_OVER:'',
DOWN_ARROW_DISABLED:'*../../images/05scroller_down_dis.gif',
SCROLLER_COLOR:'#FFFFFF',
SCROLLER_MO_COLOR:'#ffffff',
PREVIEW_BACKGROUND_COLOR:'#FFFFFF',
STREAM:[0,9,'#40465d','#ffffff','#ffffff','#ffffff',1,"",13,41,'*../../images/05edge_left.gif','#ffffff',0,0,"","",176,41,'','','*../../images/05main.gif','','','','',0,0,0,0,"Home","",82,41,'home.aspx','_self','*../../images/05main.gif','*../../images/05mainover.gif','','','',0,0,0,0,"Dispense","",82,41,'dispense.aspx','_self','*../../images/05main.gif','*../../images/05mainover.gif','','','',0,0,0,0,"Inventory","",82,41,'Inventory.aspx','_self','*../../images/05main.gif','*../../images/05mainover.gif','','','',0,0,0,0,"Check-In","",82,41,'Checkin.aspx','','*../../images/05main.gif','*../../images/05mainover.gif','','','',0,0,0,0,"Admin","Administration",82,41,'Admin/Practice_Admin.aspx','_self','*../../images/05main.gif','*../../images/05mainover.gif','','','',0,0,0,0,"","",176,41,'','','*../../images/05main.gif','','','','',0,0,1,"",13,41,'*../../images/05edge_right.gif','#ffffff'],
browser:function(){var ua=navigator.userAgent.toLowerCase()
var ind=ua.indexOf('gecko')
this.mozilla=ind>0&&ua.substr(ind).length<17
this.opera=ua.indexOf('opera')>=0
this.safari=ua.indexOf('safari')>=0
this.ie=document.all&&!this.opera
this.ie5=this.ie&&ua.indexOf('msie 5')>0
this.macie=this.ie&&ua.indexOf('mac')>=0
this.winie=this.ie&&!this.macie
this.compatMode=document.compatMode=="CSS1Compat"
this.ieCanvas=this.compatMode?document.documentElement:document.body
return this},
setPathAdjustment:function(ID){var sl=''
var sc=document.getElementsByTagName('script')
for(var i=0;i<sc.length;i++){if(sc[i].innerHTML.search(ID)>-1)sl=sc[i].src}this.SCRIPT_LOCATION=sl.substr(0, sl.lastIndexOf('/')+1)},
adjustPath:function(path){if(path.charAt(0)!='*')return path
return this.SCRIPT_LOCATION+path.substr(1)},
linkScripts:function(aNewScripts){var scripts=document.getElementsByTagName('script')
for(var i=0;i<aNewScripts.length;i++){var bScriptLinked=false
for(var j=0;j<scripts.length;j++){if(aNewScripts[i]==scripts[j].src){bScriptLinked=true;break}}if(!bScriptLinked)document.write("<script src='"+this.adjustPath(aNewScripts[i])+"' type='text/javascript'><\/script>")}},
addLoadEvent:function(f){var done=0
function w(){if(!done){done=1
f()}}if(document.addEventListener){document.addEventListener('DOMContentLoaded', w, false)}if(this.br.ie&&window==top)(function(){try{document.documentElement.doScroll('left')}catch(e){setTimeout(arguments.callee, 0)
return}w()})()
var oldf=window.onload
if(typeof oldf!='function'){window.onload=w}else{window.onload=function(){try{oldf()}catch(e){}w()}}},
init:function(){var m=this
m.br=new m.browser
m.ID=PLHIM_ID
this.setPathAdjustment('PLHIMMenu script ID:'+m.ID+' ')
m.addLoadEvent(m.onload)},
onload:function(){setTimeout('JHDbZdD.start()',0)},
start:function(){var m=this
m.pref='PLHIM'
m.currentItem=null
m.links=new Array
m.flyouts=new Array
m.lastFoid=-1
m.defFoid=0
m.timeout=null
m.interval=null
m.opacity=100
m.filter=''
m.wrapper=document.getElementById(m.ID+'Div')
m.div=document.getElementById(m.ID+'Main')
m.flyouts[0]=new m.flyout(m, null, 0, 0)
var i=0, st=m.STREAM
while (i < st.length) {
var uss=""
var dss=""
var upScr=null, downScr=null
var index=st[i++]
var n=st[i++]
var curTxtColor=st[i++]
var curTMOColor=st[i++]
var curTMDColor=st[i++]
var curSelColor=st[i++]
var foo=m.flyouts[index]
var fo=foo.div
if(n==0){foo.isEmpty=true
continue}fo.onmouseover=m.onmouseover
fo.onmouseout=m.onmouseout
if(index){var nCol=st[i++]
var moCol=st[i++]
var nArrow=st[i++]
var moArrow=st[i++]
var dArrow=st[i++]
var arrowWidth=st[i++]
var arrowHeight=st[i++]
upScr=new m.scroller(m, index, nCol, moCol, nArrow, moArrow, dArrow, arrowWidth, arrowHeight)
nArrow=st[i++]
moArrow=st[i++]
dArrow=st[i++]
arrowWidth=st[i++]
arrowHeight=st[i++]
downScr=new m.scroller(m, index, nCol, moCol, nArrow, moArrow, dArrow, arrowWidth, arrowHeight)
foo.scrollers=upScr.isArrow()&&downScr.isArrow()
if(foo.scrollers){upScr.name=m.ID+'_UPSCROLLER'
downScr.name=m.ID+'_DOWNSCROLLER'
upScr.isDisabled=upScr.dArrow?1:0
foo.upScrKey=upScr.name+index
foo.downScrKey=downScr.name+index
m.links[foo.upScrKey]=upScr
m.links[foo.downScrKey]=downScr
uss=upScr.getTag(1)
dss=downScr.getTag(0)}}var btID=m.ID+'Bt_'+index+'_'
var linkID=m.ID+'Link_'+index+'_'
var decorID=m.ID+'Decor_'+index+'_'
var nFont, nAlign, nPadding, nDecor, nDecorEx, topPad, szFont=0
var hFont, hAlign, hPadding, hDecorEx
var FOWidth=0
var FOHeight=0
if(foo.level==0){szFont=11
nFont='font:normal normal bold 11px Arial,Helvetica,sans-serif;'
hFont='font-style:normal;font-weight:bold;text-decoration:none;'
nAlign='center'
hAlign='center'
nPadding='0px 0px 0px'
hPadding='0px 0px 0px'
nDecor='text-decoration:none;'
nDecorEx=''
hDecorEx=''
topPad='m.getTopPadding(h, szFont)'}else{szFont=11
nFont='font:normal normal bold 11px Arial,Helvetica,sans-serif;'
hFont='font-style:normal;font-weight:bold;text-decoration:none;'
nAlign='left'
hAlign='center'
nPadding='0px 0px 25px'
hPadding='0px 0px 0px'
nDecor='text-decoration:none;'
nDecorEx=''
hDecorEx=''
topPad='m.getTopPadding(h, szFont)'}var fos='<ul style="margin:0px;padding:0px;'+nFont+'">'
var btStl='list-style:none;float:left;'
for(var j=0;j<n;j++){var btStlEx=''
var txtStl='display:block;'
var type=st[i++]
var w=0, h=0
if(type==0){var lnk=m.links[linkID+j]=new Object
lnk.btID=btID+j
lnk.linkID=linkID+j
lnk.decorID=decorID+j
lnk.level=foo.level
lnk.co=0
lnk.foid=index
lnk.cfoid=st[i++]
if(lnk.cfoid>0)m.flyouts[lnk.cfoid]=new m.flyout(m, lnk, lnk.cfoid, foo.level+1)
var txt=st[i++]
var title=st[i++]
w=st[i++]
h=st[i++]
btStlEx+='width:'+w+'px;'
btStlEx+='height:'+h+'px;'
btStlEx+='text-align:'+nAlign+';'
if(!txt)btStlEx+='font-size:1px;'
lnk.href=m.adjustPath(st[i++])
lnk.target=st[i++]
if(lnk.target.substr(0,3)=='_PL'){lnk.func=st[i++]
lnk.params=st[i++]}lnk.imgn=m.adjustPath(st[i++])
lnk.imgo=m.adjustPath(st[i++])
lnk.imgd=m.adjustPath(st[i++])
lnk.icon=m.adjustPath(st[i++])
lnk.icoo=m.adjustPath(st[i++])
var icoh=st[i++]
var icow=st[i++]
lnk.curcolor=lnk.txtcolor=curTxtColor
lnk.tmocolor=curTMOColor
lnk.tmdcolor=curTMDColor
lnk.selcolor=curSelColor
if(lnk.imgn)btStlEx+='background-image:url('+lnk.imgn+');'
var iTopPad=eval(topPad)
txtStl+='height:'+(h-(txt?iTopPad:0))+'px;'
if(txt){lnk.txt=true
txtStl+='padding:'+iTopPad+'px '+nPadding+';'
txtStl+=nFont+nDecor
txtStl+='color:'+curTxtColor+';'}if(icoh>0){var icoURL=lnk.icon?'url('+lnk.icon+')':'none'
var icoY=Math.floor((h-icoh)/2)
if(icoY<0)icoY=0
txtStl+=';background:transparent '+icoURL+'  no-repeat 5px '+icoY+'px'}txt='<a id="'+lnk.linkID+'" href="'+(lnk.href?lnk.href:'#')+'" target="'+lnk.target+'" title="'+title+'" class="PLHIMLink" style="'+txtStl+'">'+txt+'</a>'
fos+='<li id="'+lnk.btID+'" style="'+btStl+btStlEx+'">'+txt+'</li>'
m.preload(lnk.imgo)
m.preload(lnk.imgd)
m.preload(lnk.icoo)}else if(type==1){btStlEx+='cursor:default;'
btStlEx+='text-align:'+hAlign+';'
var txt=st[i++]
w=st[i++]
h=st[i++]
btStlEx+='width:'+w+'px;'
btStlEx+='height:'+h+'px;'
if(!txt)btStlEx+='font-size:1px;'
var imgn=m.adjustPath(st[i++])
if(imgn)btStlEx+='background-image:url('+imgn+');'
var txtcol=st[i++]
if(txt){txtStl+='padding:'+eval(topPad)+'px '+hPadding
txtStl+=';color:'+txtcol
txtStl+=';'+hFont
if(hDecorEx)txt='<span style="'+hDecorEx+'">'+txt+'</span>'
txt='<div style="'+txtStl+'">'+txt+'</div>'}fos+='<li style="'+btStl+btStlEx+'">'+txt+'</li>'}else if(type==2){curTxtColor=st[i++]
curTMOColor=st[i++]
curTMDColor=st[i++]
curSelColor=st[i++]}if(foo.level>0){FOWidth=Math.max(FOWidth, w)
FOHeight+=h}}fos+="</ul>"
if(foo.scrollers)fos="<div align=\"center\">"+fos+"</div>"
else fo.align="center"
fo.innerHTML=uss+fos+dss
var items=fo.getElementsByTagName('A')
for(var j=0;j<items.length;j++){var a=items[j]
if(a.className=='PLHIMLink'){a.onmousedown=m.onmousedown
a.onmouseup=m.onmouseup
a.onfocus=function(){this.blur()};a.onclick=m.onclick
if(m.br.ie){a.onmouseenter=function(){JHDbZdD.over(this)};a.onmouseleave=function(){JHDbZdD.out(this)};}}}if(foo.level>0){var w=FOWidth
foo.scrollArea=fo.getElementsByTagName('UL')[0]
foo.scrollArea.style.width=w+'px'
if(foo.scrollers){w=Math.max(w, Math.max(upScr.arrowWidth, downScr.arrowWidth))
upScr.initElement(w, 1)
downScr.initElement(w, 0)
foo.scrollArea=foo.scrollArea.parentNode
foo.scrollArea.style.width=w+'px'}fo.style.width=w+'px'
fo.style.zIndex=50
foo.scrollArea.baseHeight=FOHeight}}m.updateFlyouts()},
onmouseover:function(evt){var m=JHDbZdD
var e=m.getSource(evt)
if(!(m.br.ie&&e&&e.className=='PLHIMLink'))m.over(e)},
over:function(e){var m=this
window.clearTimeout(m.timeout)
if(e&&e.className=='PLHIMLink'){var lnk=m.links[e.id]
if(lnk){m.lastFoid=lnk.cfoid?lnk.cfoid:lnk.foid
m.currentItem=e
m.showMO(lnk, e)
m.timeout=window.setTimeout('JHDbZdD.updateFlyouts()',400)}}},
onmouseout:function(evt){var m=JHDbZdD
var e=m.getSource(evt)
if(!(m.br.ie&&e&&e.className=='PLHIMLink'))m.out(e)},
out:function(e){var m=this
m.lastFoid=-1
window.clearTimeout(m.timeout)
if(e&&e.className=='PLHIMLink'){var lnk=m.links[e.id]
if(lnk)m.hideMO(lnk, e)}m.timeout=window.setTimeout('JHDbZdD.updateFlyouts()',400)},
onmousedown:function(evt){var m=JHDbZdD
var e=m.getSource(evt)
if(!e||e.className!='PLHIMLink')return
var lnk=m.links[e.id]
if(!lnk)return
m.isdown=true
if(lnk.imgd)e.parentNode.style.backgroundImage='url('+lnk.imgd+')'
if(!lnk.txt||lnk.selSet)return
var fnt=e
var decor=document.getElementById(lnk.decorID)
if(lnk.level==0){fnt.style.color=lnk.tmdcolor}else{fnt.style.color=lnk.tmdcolor}},
onmouseup:function(evt){var m=JHDbZdD
var e=m.getSource(evt)
m.isdown=false
m.clicked(e)},
clicked:function(e){var m=JHDbZdD
if(!e||e.className!='PLHIMLink')return
var lnk=m.links[e.id]
if(!lnk)return
if(lnk.func)eval(lnk.func+'("PLHIMMenu script ID:"+JHDbZdD.ID,"'+lnk.href+'",'+lnk.params+')')
else if(lnk.href){if(lnk.target)window.open(lnk.href,lnk.target)
else location=lnk.href}m.showMO(lnk, e)
m.lastFoid=-1
window.clearTimeout(m.timeout)
m.updateFlyouts()},
onclick:function(evt){return false},
showMO:function(lnk, e){if(lnk.sel)return
if(lnk.imgo)e.parentNode.style.backgroundImage='url('+lnk.imgo+')'
var fnt=e
if(lnk.icoo)fnt.style.backgroundImage='url('+lnk.icoo+')'
if(!lnk.txt||lnk.selSet)return
var decor=document.getElementById(lnk.decorID)
if(lnk.level==0){fnt.style.color=lnk.tmocolor}else{fnt.style.color=lnk.tmocolor}},
hideMO:function(lnk, e){if(lnk.co||lnk.sel)return
var fnt=e ? e:document.getElementById(lnk.linkID)
if(lnk.imgn&&lnk.imgo)fnt.parentNode.style.backgroundImage='url('+lnk.imgn+')'
fnt.style.backgroundImage=lnk.icon ? 'url('+lnk.icon+')':'none'
if(!lnk.txt||lnk.selSet)return
var decor=document.getElementById(lnk.decorID)
if(lnk.level==0){fnt.style.color=lnk.txtcolor}else{fnt.style.color=lnk.txtcolor}},
updateFlyouts:function(){var m=this
for(var i=1;i<m.flyouts.length;i++)m.flyouts[i].show=false
if(m.lastFoid<0)m.lastFoid=m.defFoid
if(m.lastFoid){var f=m.flyouts[m.lastFoid]
while(f.lnkPar){f.show=true
f=m.flyouts[f.lnkPar.foid]}}var l=m.flyouts.length
for(var i=1;i<l;i++){var f=m.flyouts[i]
if(!f.show&&f.shown)f.removeFlyout()}for(var i=1;i<l;i++){var f=m.flyouts[i]
if(f.show&&!f.shown)f.showFlyout()}},
getSource:function(evt){var e=this.br.ie?event.srcElement:evt.target
while(e&&!e.className)e=e.parentNode
return e},
getOpacity:function(i){var op=this.opacity
if(i>0)op=op*5*(6-i)/100
return op},
getTop:function(e, base){var m=this
var top=0
while(e&&((base==0&&e!=m.wrapper)||(base==1&&((e.style.position!='absolute'&&e.style.position!='relative')||e==m.wrapper))||(base==2))){top+=e.offsetTop
e=e.offsetParent
if(e&&!m.br.opera){var bw=parseInt(e.style.borderTopWidth)
if(!bw)bw=0
top+=bw}}return top},
getLeft:function(e, base){var m=this
var left=0
while(e&&((base==0&&e!=m.wrapper)||(base==1&&((e.style.position!='absolute'&&e.style.position!='relative')||e==m.wrapper))||(base==2))){left+=e.offsetLeft
e=e.offsetParent
if(e&&m.br.ie){var bw=parseInt(e.style.borderLeftWidth)
if(!bw)bw=0
left+=bw}}return left},
preload:function(img){if(!img)return
if(!this.preloads)this.preloads=new Array
var ind=this.preloads.length
this.preloads[ind]=new Image
this.preloads[ind].src=img},
getTopPadding:function(h, szFont){var topPad=Math.floor((h-szFont-1)/2)
if(topPad<0)topPad=0
return topPad},
dummy:null};JHDbZdD.flyout=function(m, lnkPar, index, level){var f=this
if(level==0){f.div=m.div}else{f.div=document.createElement('div')
f.div.style.position='absolute'
f.div.style.display='none'
f.div.style.border='none'
f.clip=f.div.style.clip
m.wrapper.appendChild(f.div)
f.intr=false}f.m=m
f.lnkPar=lnkPar
f.show=false
f.shown=!index
f.level=level
f.div.obj=f
f.obj='JHDbZdD.fo'+index
eval(f.obj+'=f')};JHDbZdD.flyout.prototype={showFlyout:function(){var f=this
var lnk=f.lnkPar
var fo=f.div
if(!fo||f.intr||f.isEmpty)return
var m=f.m
var e=document.getElementById(lnk.btID)
lnk.co=1
f.intr=true
f.pfoid=lnk.foid
var eTop=m.getTop(e,0)
var docTop=(m.br.ie?m.br.ieCanvas.scrollTop:window.pageYOffset)-m.getTop(m.wrapper,2)
var docLeft=(m.br.ie?m.br.ieCanvas.scrollLeft:window.pageXOffset)-m.getLeft(m.wrapper,2)
var docHeight=m.br.ie?m.br.ieCanvas.clientHeight:window.innerHeight
var docWidth=m.br.ie?m.br.ieCanvas.clientWidth:window.innerWidth
var bFirstFO=f.level==1
var topLimit
if(bFirstFO)topLimit=eTop+e.offsetHeight+1
else topLimit=docTop+2
var bottomLimit=docTop+docHeight-6
var foHeight=bottomLimit-topLimit
if(foHeight>220)foHeight=220
var upScr=f.upScrKey ? m.links[f.upScrKey]:null
var downScr=f.downScrKey ? m.links[f.downScrKey]:null
if(upScr&&downScr&&f.scrollArea.baseHeight>foHeight){f.scrollers=true
var scrSize=upScr.baseHeight+downScr.baseHeight
f.scrollArea.style.overflow='hidden'
if(foHeight<scrSize+4)foHeight=scrSize+4
f.scrollArea.style.height=(f.scrollAreaHeight=foHeight-scrSize)+'px'}else{f.scrollers=false
f.scrollArea.style.height=(foHeight=f.scrollArea.baseHeight)+'px'}fo.style.height=foHeight+'px'
fo.style.top='-10000px'
fo.style.display=''
f.baseWidth=fo.offsetWidth
f.baseHeight=foHeight
var pfo=m.flyouts[lnk.foid]
if(bFirstFO){f.baseTop=topLimit
f.baseLeft=m.getLeft(e,0)}else{f.baseTop=eTop
f.baseLeft=pfo.baseLeft+pfo.div.offsetWidth+1}if(f.baseLeft+f.baseWidth+22-docLeft>docWidth){f.baseLeft=(bFirstFO ? docWidth-22+docLeft:pfo.baseLeft)-f.baseWidth-1}if(f.baseTop<topLimit)f.baseTop=topLimit
if(foHeight+f.baseTop>bottomLimit){var t=bottomLimit-foHeight
f.baseTop=t<topLimit?topLimit:t}if(upScr&&downScr){upScr.display(f.scrollers)
downScr.display(f.scrollers)}fo.style.display='none'
fo.style.top=f.baseTop+'px'
fo.style.left=f.baseLeft+'px'
f.animate=f.rollright
f.openAnimated(0)},
removeFlyout:function(){var f=this
f.lnkPar.co=0
f.m.hideMO(f.lnkPar)
if(f.intr)return
f.intr=true
f.animate=f.rollright
f.closeAnimated(100)},
hideFlyout:function(){var f=this
f.div.style.display='none'
if(f.scrollers){f.scrollArea.style.clip='rect(auto auto auto auto)'
f.scrollArea.style.position=''
f.scrollArea.style.top=''
f.scrollArea.style.left=''
var upScr=f.m.links[f.upScrKey]
if(upScr)upScr.setDefault(1, upScr.dArrow)
var downScr=f.m.links[f.downScrKey]
if(downScr)downScr.setDefault(0, downScr.nArrow)
f.scrollers=false}f.clearClip(f.div)},
scrollInit:function(){var f=this
if(!f.scrollers)return
f.step=Math.max(4,Math.floor(f.scrollArea.baseHeight/50))
var h=f.div.offsetHeight
f.div.style.height=h+'px'
var upScr=f.m.links[f.upScrKey]
upScr.initPos(0, 0)
f.scrollArea.style.top=(f.scrBaseTop=upScr.baseHeight)+'px'
f.scrollArea.style.left='0px'
f.scrollArea.style.position='absolute'
var downScr=f.m.links[f.downScrKey]
downScr.initPos(0, h-downScr.baseHeight)
f.scrollArea.style.overflow=''
f.scrollArea.style.clip='rect(auto auto '+f.scrollAreaHeight+'px auto)'},
scroll:function(){var f=this
var m=f.m
if(!f||!f.scrollers||f.intr)return
var curtop=f.scrollArea.offsetTop
var topLimit=f.scrBaseTop-(f.bScrollUp?0:(f.scrollArea.baseHeight-f.scrollAreaHeight))
if(curtop==topLimit)return
curtop+=(f.bScrollUp?1:-1)*f.step
if((f.bScrollUp&&curtop>topLimit)||(!f.bScrollUp&&curtop<topLimit))curtop=topLimit
f.scrollArea.style.top=curtop+'px'
f.scrollArea.style.clip='rect('+(f.scrBaseTop-curtop)+'px auto '+(f.scrollAreaHeight+f.scrBaseTop-curtop)+'px auto)'
var upScr=m.links[f.upScrKey]
if(upScr&&upScr.dArrow)upScr.arrow.src=(upScr.isDisabled=(f.bScrollUp&&curtop==topLimit))? upScr.dArrow:upScr.arrowSrc
var downScr=m.links[f.downScrKey]
if(downScr&&downScr.dArrow)downScr.arrow.src=(downScr.isDisabled=(!f.bScrollUp&&curtop==topLimit))? downScr.dArrow:downScr.arrowSrc},
clearClip:function(o){if(o.style.display!='none')o.style.display='block'
o.style.clip=this.m.br.ie?'rect(auto auto auto auto)':this.clip},
openAnimated:function(p){var f=this
if(p>100)p=100
var t=f.animate(f.div,p,0)
if(p==0)f.div.style.display=''
if(p==100)f.finishAnimation(true)
else if(t)window.setTimeout(f.obj+'.openAnimated('+(p+10)+')',10)},
closeAnimated:function(p){var f=this
if(p<0)p=0
var t=f.animate(f.div,p,0)
if(p==0){f.finishAnimation(false)}else if(t){window.setTimeout(f.obj+'.closeAnimated('+(p-10)+')',10)}},
finishAnimation:function(open){var f=this
if(!open)f.hideFlyout()
f.shown=open
f.intr=false
if(f.show){if(f.shown)f.scrollInit()
else f.showFlyout()}if(!f.show&&f.shown)f.removeFlyout()},
rollright:function(o,p,i){var b=this.baseWidth*p/100
if(p==100)this.clearClip(o)
else o.style.clip='rect(auto '+b+'px auto auto)'
return true},
dummy:null};JHDbZdD.scroller=function(m, foid, nCol, moCol, nImg, moImg, dImg, w, h){var o=this
o.foid=foid
o.nColor=nCol
o.moColor=moCol
o.arrowSrc=o.nArrow=m.adjustPath(nImg)
o.moArrow=m.adjustPath(moImg)
o.dArrow=m.adjustPath(dImg)
o.arrowWidth=w
o.arrowHeight=h
if(!o.moArrow)o.moArrow=o.nArrow
o.baseHeight=o.arrowHeight
m.preload(o.nArrow)
m.preload(o.moArrow)
m.preload(o.dArrow)};JHDbZdD.scroller.prototype={onmouseover:function(){var m=JHDbZdD
var e=this
var lnk=m.links[e.id]
if(lnk){m.lastFoid=lnk.foid
var f=m.flyouts[lnk.foid]
f.bScrollUp=e.className==m.ID+'_UPSCROLLER'
lnk.arrowSrc=lnk.moArrow
e.style.backgroundColor=lnk.moColor
m.interval=window.setInterval(f.obj+'.scroll()',35)}},
onmouseout:function(){var m=JHDbZdD
window.clearInterval(m.interval)
var e=this
var lnk=m.links[e.id]
if(lnk){e.style.backgroundColor=lnk.nColor
lnk.arrowSrc=lnk.nArrow
if(!lnk.isDisabled)lnk.arrow.src=lnk.nArrow}},
isArrow:function(){return this.arrowWidth>0&&this.arrowHeight>0},
getTag:function(up){var o=this
var arrowSrc=up?(o.dArrow?o.dArrow:o.nArrow):o.nArrow
return "<div id=\""+o.name+o.foid+"\" class=\""+o.name+"\" style=\"text-align:center;cursor:pointer;background-color:"+o.nColor+"\"><img src=\""+arrowSrc+"\" style=\"width:"+o.arrowWidth+"px;height:"+o.arrowHeight+"px;\"></div>"},
initElement:function(foW, up){var m=JHDbZdD
var e=document.getElementById(this.name+this.foid)
if(e){if(m.br.ie){e.onmouseenter=this.onmouseover
e.onmouseleave=this.onmouseout}else{e.onmouseover=this.onmouseover
e.onmouseout=this.onmouseout}e.style.width=foW+'px'
this.arrow=e.getElementsByTagName('IMG')[0]}},
initPos:function(left, top){var e=document.getElementById(this.name+this.foid)
if(e){e.style.left=left+'px'
e.style.top=top+'px'
e.style.position='absolute'}},
setDefault:function(isDisabled, arrowSrc){this.isDisabled=isDisabled
if(arrowSrc)this.arrow.src=arrowSrc
var e=document.getElementById(this.name+this.foid)
if(e){e.style.position=''
e.style.left=''
e.style.top=''}},
display:function(show){var e=document.getElementById(this.name+this.foid)
if(e)e.style.display=show ? '':'none' },
dummy:null};JHDbZdD.init()
