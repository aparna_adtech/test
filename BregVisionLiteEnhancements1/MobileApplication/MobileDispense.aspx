﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MobileDispense.aspx.cs" Inherits="BregVision.MobileApplication.MobileDispense" %>
<%@ Register Assembly="RadComboBox.Net2" Namespace="Telerik.WebControls" TagPrefix="radC" %>
<%@ Register Assembly="RadInput.Net2" Namespace="Telerik.WebControls" TagPrefix="radI" %>
<%@ Register Assembly="RadCalendar.Net2" Namespace="Telerik.WebControls" TagPrefix="radCal" %>
 
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>

<body>
<form id="form1" runat="server">
    <table width="220px"><!--Mobile screen size-->
        <tr>
            <td colspan="3">
               <image runat="server" src="../Images/BregHeaderSM.jpg" />
            </td>
            <td colspan="2">
                <asp:Label runat="server" ID="lblTitle"  Font-Bold="true" ForeColor="Blue" Text="Search"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <asp:LoginName ID="LoginName1" Runat="server" 
                   FormatString ="Welcome, {0}" Font-Size="Small"/>
                <asp:LoginStatus id="lsUSer" runat="server" Font-Size="x-small" LogoutText="(Log Out)" LogoutPageUrl="default.aspx" LogoutAction="Redirect" /> 
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <asp:Label runat="server" ID="lblPractice2" Font-Size="X-Small"></asp:Label>
            </td>
        </tr>
    </table>
<div runat="server" id="divSearch" visible="true">           
    <table width="220px"><!--Mobile screen size-->
        <tr>
            <td colspan="2">
                <asp:HyperLink ID="HyperLink1" runat="server" Font-Size="Small" Text="Change Location"  NavigateUrl="practicelocation.aspx"></asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                 <asp:Label runat="server" ID="Label5" Font-Size="XX-Small" Text="Step 1 of 5"></asp:Label>
            </td>
            <td colspan="3" align="right">
                <asp:Button runat="server" ID="btnNext1" Text="Next" Font-Size="XX-Small" OnClick="btnNext1_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <asp:Label runat="server" ID="lblSearch" Font-Size="X-Small" Text="Enter search criteria and click Search"></asp:Label>
            </td>
        </tr>
        <tr>                
            <!--Search line with criteria-->
            <td colspan="5">
                <table>
                    <tr>
                        <td colspan="5"><asp:Label runat="server" ID="lblSearchStatus" ForeColor="Red" Font-Size="X-Small"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label runat="server" ID="Label1" Text="Search Text" Font-Size="X-Small"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:TextBox runat="server" ID="txtSearch" Width="80px"></asp:TextBox>
                        </td>
                    </tr>                                
                    <tr>
                        <td colspan="2">
                            <asp:Label runat="server" ID="Label2" Text="Search Criteria" Font-Size="X-Small"></asp:Label>
                        </td>
                        <td colspan="3">
                             <asp:DropDownList id="cboSearch3" runat="server">
                                <asp:ListItem Text="Product Code" Value="Code" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Product Name" Value="Name"></asp:ListItem>
                                <asp:ListItem Text="Manufacturer" Value="Brand"></asp:ListItem>
                                <asp:ListItem Text="HCPCs" Value="HCPCs"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" align="right">
                            <asp:Button runat="server" ID="btnSearch" Font-Size="X-Small" Width="50"
                                Text="Search" onclick="btnSearch_Click" />
                        </td>
                    </tr>                                                            
                </table>
            </td>
        </tr>    
    </table>
</div>                      
                            
<div runat="server" id="divGrid" visible="false">
    <table width="220px">     
        <tr>
            <td>
                <asp:Label runat="server" ID="Label4" Font-Size="XX-Small" Text="Step 2 of 5"></asp:Label></td>
            <td align="right">
                <asp:Button runat="server" ID="btnBack2" Text="Back" Font-Size="XX-Small" OnClick="btnBack2_Click" />
                <asp:Button runat="server" ID="btnNext2" Text="Next" Font-Size="XX-Small" OnClick="btnNext2_Click"/>
            </td>
        </tr>
        <%--<tr>
            <td colspan="2">
                <div style="font-size:XX-Small">Add To Dispense Queue:</div>
                <asp:CheckBox ID="chkAddToQueue" runat="server"  Font-Size="XX-Small" Checked="false" />
            </td>
        </tr>--%>
        <tr>
            <td><asp:Label runat="server" ID="lblSearchResults" Font-Size="XX-Small" Text="Select product(s) to dispense"></asp:Label></td>
            <td align="right">
                <asp:Button runat="server" ID="btnAddtoDispensement" Text="Add to DISP" Font-Size="XX-Small" OnClick="btnAddtoDispensement_Click" />
                <asp:Button runat="server" ID="btnAddToQueue" Text="Add to Queue" Font-Size="XX-Small" OnClick="btnAddToQueue_Click"/>
            </td>
            <%--<td><asp:Button runat="server" ID="btnAddToQ" Text="Add to Q" Font-Size="XX-Small"/></td>--%>
        </tr>
        <tr>
            <td colspan="2"><asp:Label runat="server" ID="lblSearchResultsStatus" ForeColor="Red" Font-Size="X-Small"></asp:Label>
            </td>
        </tr>
    </table>
   <radG:RadGrid ID="grdDispenseSearchMobile" 
        runat="server"
        Width="220px"
        AutoGenerateColumns="False"
        Skin="Office2007"
        BorderWidth="0"
        AllowMultiRowSelection="true"
        AllowAutomaticUpdates="True" AllowPaging="false"
        EnableAJAXLoadingTemplate="True"
        LoadingTemplateTransparency="25"
        EnableAJAX="true"
        EnableViewState="True"
        GridLines="None" PageSize="5">        
        <MasterTableView>
            <ExpandCollapseColumn Visible="False">
                <HeaderStyle Width="19px" />
            </ExpandCollapseColumn>
            <RowIndicatorColumn Visible="False">
                <HeaderStyle Width="20px" />
            </RowIndicatorColumn>
            <Columns>
                <radG:GridClientSelectColumn UniqueName="ClientSelectColumn2" />
                <radG:GridBoundColumn DataField="ProductInventoryID" 
                                UniqueName="ProductInventoryID" Visible="False"/>
                <radG:GridBoundColumn DataField="BrandName"  
                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30" Resizable="true"
                                HeaderStyle-Wrap="true" HeaderText="Brand" 
                                ItemStyle-HorizontalAlign="center" ItemStyle-Width="30" UniqueName="BrandName"
                                 ItemStyle-Font-Size="XX-Small"
                                 HeaderStyle-Font-Size="XX-Small" />
                <radG:GridBoundColumn DataField="ProductName" 
                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30" 
                                HeaderStyle-Wrap="true" HeaderText="Name" 
                                ItemStyle-HorizontalAlign="center" ItemStyle-Width="30" UniqueName="ProductName"
                                 ItemStyle-Font-Size="XX-Small"
                                 HeaderStyle-Font-Size="XX-Small" />
                <radG:GridBoundColumn DataField="Code" 
                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30" 
                                HeaderStyle-Wrap="true" HeaderText="Code" 
                                ItemStyle-HorizontalAlign="center" ItemStyle-Width="30" UniqueName="Code"
                                ItemStyle-Font-Size="XX-Small"
                                 HeaderStyle-Font-Size="XX-Small" />
                <radG:GridBoundColumn DataField="Packaging" 
                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="5" 
                                HeaderStyle-Wrap="true"  
                                ItemStyle-HorizontalAlign="center" ItemStyle-Width="8" Resizable="true" UniqueName="Packaging"
                                ItemStyle-Font-Size="XX-Small"
                                 HeaderStyle-Font-Size="XX-Small" />
                <radG:GridBoundColumn DataField="Side" 
                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="5" 
                                HeaderText="SD" 
                                HeaderStyle-Wrap="true"  
                                ItemStyle-HorizontalAlign="center" ItemStyle-Width="8" UniqueName="Side"
                                ItemStyle-Font-Size="XX-Small"
                                 HeaderStyle-Font-Size="XX-Small" />
                <radG:GridBoundColumn DataField="Size" 
                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="5" 
                                HeaderText="SZ" 
                                HeaderStyle-Wrap="true"  
                                ItemStyle-HorizontalAlign="center" ItemStyle-Width="8" UniqueName="Size"
                                ItemStyle-Font-Size="XX-Small"
                                 HeaderStyle-Font-Size="XX-Small" />
                <radG:GridBoundColumn DataField="Gender" 
                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="5" 
                                HeaderText="GN" 
                                HeaderStyle-Wrap="true"  
                                ItemStyle-HorizontalAlign="center" ItemStyle-Width="8" UniqueName="Gender"
                                ItemStyle-Font-Size="XX-Small"
                                 HeaderStyle-Font-Size="XX-Small" />       
            </Columns>
        </MasterTableView>
        <ClientSettings AllowColumnsReorder="false" AllowDragToGroup="false" ReorderColumnsOnClient="false">
          <Selecting AllowRowSelect="True"/>
        </ClientSettings>
    </radG:RadGrid>
</div>              

<div runat="server" id="divPhysicianPatient" visible="false">
    <table width="220px"> 
        <tr>
            <td colspan="2">
                <asp:Label runat="server" ID="Label3" Font-Size="XX-Small" Text="Step 3 of 5"></asp:Label></td>
            <td align="right">
                <asp:Button runat="server" ID="btnBack3" Text="Back" Font-Size="XX-Small" OnClick="btnBack3_Click"  />
                <asp:Button runat="server" ID="btnNext3" Text="Next" Font-Size="XX-Small" OnClick="btnNext3_Click"  />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label runat="server" ID="Label6" Font-Size="XX-Small" Text="Select physician and enter patient code if applicable."></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="Label8" Font-Size="XX-Small" Text="Physician:"></asp:Label>
            </td>
            <td colspan="2">
                <asp:DropDownList id="ddlPhysician" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="Label9" Font-Size="XX-Small" Text="Patient Code:"></asp:Label>
            </td>
            <td colspan="2">
                <asp:TextBox runat="server" ID="txtPatientCode" MaxLength="255"></asp:TextBox>
            </td>
        </tr>
    </table>               
</div>              

<div runat="server" id="divSummary" visible="false">
    <table width="220px"> 
        <tr>
            <td colspan="2">
                <asp:Label runat="server" ID="Label7" Font-Size="XX-Small" Text="Step 4 of 5"></asp:Label></td>
            <td align="right">
                <asp:Button runat="server" ID="btnBack4" Text="Back" Font-Size="XX-Small" OnClick="btnBack4_Click"  />
                <asp:Button runat="server" ID="bntNext4" Visible="false" Text="Next" Font-Size="XX-Small" OnClick="btnNext4_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="2"><asp:Label runat="server" ID="Label10" Font-Size="XX-Small" Text="Delete/Add quantities and dispense."></asp:Label>

            </td>
            <td align="right">
                <asp:Button runat="server" ID="btnDispenseProducts" Text="Dispense" Font-Size="XX-Small" OnClick="btnDispenseProducts_Click"  />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div style="font-size:XX-Small">Add To Dispense Queue:</div>
                <asp:CheckBox ID="chkAddToQueue" runat="server"  Font-Size="XX-Small" Checked="false" />
            </td>
        </tr>
    <tr>
        <td colspan="3">
            <asp:Label runat="server" ID="lblSummaryStatus" ForeColor="Red" Font-Size="X-Small"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="3">
           <table width="100%">
                <tr>
                    <td>
                        <label style="font-size:XX-Small">Physician:   </label><asp:Label 
                            ID="lblSummaryPhysician" runat="server" Font-Bold="true" Font-Size="XX-Small" />
                     </td>
                 </tr>
                 <tr>
                     <td>  
                          <asp:Label ID="lblSummaryPatientIDLegend" runat="server" Font-Size="XX-Small">Patient Code: </asp:Label>
                          <asp:Label ID="lblSummaryPatientID" runat="server" Font-Bold="true" Font-Size="XX-Small" />
                    </td>
                   </tr>
                   <tr>
                    <td align="left">
                        <label style="font-size:XX-Small">Date Dispensed:</label>
                          <asp:TextBox  runat="server" ID="dtDispensed"></asp:TextBox>                        
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3"><%--<asp:HiddenField ID="hdnTest" runat="server" Visible="false"/>--%>
             <radG:RadGrid ID="grdSummary" runat="server" 
                            AllowSorting="True" AutoGenerateColumns="False" EnableAJAX="false" 
                            GridLines="None" Height="100%" OnDeleteCommand="grdSummary_DeleteCommand" 
                            OnItemDataBound="grdSummary_ItemDataBound" OnLoad="grdSummary_OnLoad" 
                            Skin="Office2007" Width="205px"><PagerStyle Mode="NextPrevAndNumeric" />
                            <mastertableview autogeneratecolumns="False">
                                <expandcollapsecolumn visible="False">
                                    <HeaderStyle Width="19px" />
                                </expandcollapsecolumn>
                                <rowindicatorcolumn visible="False">
                                    <HeaderStyle Width="20px" />
                                </rowindicatorcolumn>
                                <Columns>
                                     <radG:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" 
                                        ConfirmText="Delete this product?" 
                                        ImageUrl="~/App_Themes/Breg/images/Common/delete.png" Text="Delete" 
                                        UniqueName="Delete"></radG:GridButtonColumn>
                                    <radG:GridBoundColumn DataField="PracticeLocationID" Display="False" Visible="false"
                                        UniqueName="PracticeLocationID">
                                    </radG:GridBoundColumn>
                                    <radG:GridBoundColumn DataField="QuantityOnHandPerSystem" Display="False" Visible="false"
                                        UniqueName="QuantityOnHandPerSystem">
                                    </radG:GridBoundColumn>
                                    <radG:GridBoundColumn DataField="HCPCsString" Display="False" Visible="false"
                                        UniqueName="HCPCsString">
                                    </radG:GridBoundColumn>
                                    <radG:GridBoundColumn DataField="ProductInventoryID" Display="False"  Visible="false" 
                                        UniqueName="ProductInventoryID">
                                    </radG:GridBoundColumn>
                                    <radG:GridBoundColumn DataField="PracticeCatalogProductID" 
                                        Display="False" Visible="false" UniqueName="PracticeCatalogProductID">
                                    </radG:GridBoundColumn>
                                    
                                    <radG:GridBoundColumn DataField="WholesaleCost" DataFormatString="{0:C}" 
                                        Visible="False" HeaderText="Cost" UniqueName="WholesaleCost">
                                    </radG:GridBoundColumn>
                                    <radG:GridBoundColumn DataField="BillingCharge" DataFormatString="{0:C}" 
                                        HeaderStyle-Width="20" HeaderStyle-Wrap="true" HeaderText="Billing Charge2" 
                                        UniqueName="BillingCharge" Visible="false">
                                    </radG:GridBoundColumn>
                                    <radG:GridBoundColumn DataField="BillingChargeCash" 
                                        DataFormatString="{0:C}" HeaderStyle-Width="20" HeaderStyle-Wrap="true" 
                                        HeaderText="Billing Charge Cash" UniqueName="BillingChargeCash" Visible="false">
                                    </radG:GridBoundColumn>                                    
                                    
                                    <radG:GridTemplateColumn DataField="Detail" 
                                        HeaderText="Description" UniqueName="Detail">

                                    <ItemTemplate>
                                       <%--<mobile:Form ID="mblDespense" Runat="server" Method="Post">
                                        <mobile:Panel ID="mblPanel1" Runat="server">
                                            <mobile:DeviceSpecific ID="DeviceSpecific1" Runat="server">
                                                <Choice Filter="supportsJavaScript">
                                                    <ContentTemplate>--%>
                                                    
                                            <asp:label runat="server" id="test" Text='<%# bind("Detail") %>'></asp:label>
                                            <br />Override:
                                            <asp:CheckBox ID="chkDMEDeposit" Checked="false" runat="server" AutoPostBack="true" OnCheckedChanged="chkDMEDeposit_CheckedChanged"/>
                                            Med:
                                            <asp:CheckBox ID="chkIsMedicare" Checked="false" runat="server" AutoPostBack="true" OnCheckedChanged="chkIsMedicare_CheckedChanged"/>
                                            ABN:
                                            <asp:CheckBox ID="chkABNForm" Checked="false" runat="server" AutoPostBack="true" OnCheckedChanged="chkABNForm_CheckedChanged" />
                                             <br />
                                            DME Deposit:
                                            <asp:HiddenField ID="hdnDMEDeposit" runat="server" Value="0" />
                                            <radI:RadNumericTextBox ID="txtDMEDeposit" runat="server" 
                                            Culture="English (United States)" Font-Size="x-small" 
                                            LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" 
                                            NumberFormat-DecimalDigits="2" ShowSpinButtons="false" Skin="Office2007" 
                                            SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" 
                                            Text='<%# bind("DMEDeposit") %>' Type="Currency" Width="50px" >
                                            </radI:RadNumericTextBox>
                                            <%--</ContentTemplate>
                                                </Choice>
                                            </mobile:DeviceSpecific>
                                        </mobile:Panel>
                                       </mobile:Form>--%>
                                             <br />
                                             L/R Mod:
                                                <%--<radC:RadComboBox ID="cboSideOrg" runat="server" Value='<%# bind("Side") %>' Width="45px" Skin="WebBlue" SkinsPath="~/RadControls/ComboBox/Skins">
                                                    <Items>
                                                        <radC:RadComboBoxItem Text="U" Value="U" />
                                                        <radC:RadComboBoxItem Text="LT" Value="L" />
                                                        <radC:RadComboBoxItem Text="RT" Value="R" />
                                                    </Items>
                                                </radC:RadComboBox>--%>
                                            <asp:DropDownList ID="cboSide" runat="server">
                                                <asp:ListItem Value="U" Text ="U"></asp:ListItem>
                                                <asp:ListItem Value="L" Text="LT"></asp:ListItem>
                                                <asp:ListItem Value="R" Text="RT"></asp:ListItem>
                                            </asp:DropDownList>
                                             <br />
                                             Dx Code:
                                                <asp:TextBox ID="cboICD9" runat="server" Width="60px"></asp:TextBox>
                                                <%--<radC:RadComboBox ID="cboICD9" runat="server" AllowCustomText="true" Width="60px" Skin="WebBlue" SkinsPath="~/RadControls/ComboBox/Skins"></radC:RadComboBox>--%>
                                             <br />
                                            Qty. to Dispense
                                            <radI:RadNumericTextBox ID="txtQuantityDispensedOrg" runat="server" 
                                            Culture="English (United States)" Font-Size="x-small" 
                                            LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" 
                                            NumberFormat-DecimalDigits="0" ShowSpinButtons="false" Skin="Office2007" 
                                            SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Value="1" Width="30px" MaxLength="2" Visible="false"> 
                                            </radI:RadNumericTextBox>
                                            <asp:TextBox ID="txtQuantityDispensed" runat="server" Width="30px" Text="1"></asp:TextBox>
                                        </ItemTemplate>
                                    </radG:GridTemplateColumn>
                                </Columns>
                           </mastertableview>
                  </radG:RadGrid>
                </td>
        </tr>
    </table>
</div>

<div runat="server" id="divReceipt" visible="false">
    <table width="220px"> 
        <tr>
            <td colspan="3">
                <asp:Label runat="server" ID="lblQueueStatus" ForeColor="Red" Font-Size="X-Small" Visible="false"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label runat="server" ID="Label11" Font-Size="XX-Small" Text="Step 5 of 5"></asp:Label>
            </td>
                
            <td align="right">
                <asp:Button runat="server" ID="btnBack5" Text="New Dispensement" Font-Size="XX-Small"  OnClick="btnBack5_Click"  />
                
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div id="Receipt" runat="server"></div>
            </td>
        </tr>   
    </table>

</div>
</form>

</body>

</html>
