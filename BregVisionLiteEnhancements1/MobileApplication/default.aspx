﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="BregVision.MobileApplication._default" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls"  %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head>
</head>

<body>
<script runat="server" language="c#">



    protected void cmdSelectLocation_Click(object sender, CommandEventArgs e)
    {
        Response.Redirect("MobileDispense.aspx");
    }
    
</script>


        <mobile:form ID="Form1" runat="server">
            <mobile:Image ID="Image1" Runat="server" ImageUrl="../Images/BregHeaderSM.jpg"></mobile:Image>       
            <mobile:Panel Runat="server" Alignment="Center">
                        <asp:Login ID="Login" runat="server" 
                            DestinationPageUrl="processlogin.aspx" 
                            DisplayRememberMe="false"
                            TextLayout="TextOnTop"
                            BackColor="#EFF3FB" 
                            BorderColor="#B5C7DE" 
                            BorderPadding="2" 
                            BorderStyle="none" 
                            BorderWidth="0px" 
                            Font-Names="Verdana" 
                            Font-Size="0.9em" 
                            ForeColor="#333333" FailureAction="RedirectToLoginPage" EnableViewState="false">
                            <TitleTextStyle BackColor="#507CD1"  Font-Bold="True" Font-Size="0.9em" ForeColor="White" />
                            <TextBoxStyle Font-Size="0.9em" />
                            <LabelStyle Font-Size="0.9em" />
                            <InstructionTextStyle Font-Size="0.9em" Font-Italic="True" ForeColor="Black" />
                            <LoginButtonStyle BackColor="White" BorderColor="#507CD1" BorderStyle="Solid" BorderWidth="1px"
                                Font-Names="Verdana" Font-Size="0.9em" ForeColor="#284E98"  />
                        </asp:Login>
            </mobile:Panel>
        </mobile:form>
</body>
</html>
