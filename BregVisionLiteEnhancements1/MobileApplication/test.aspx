﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="test.aspx.cs" Inherits="BregVision.MobileApplication.test" %>
<%@ Register Assembly="RadAjax.Net2" Namespace="Telerik.WebControls" TagPrefix="radA" %>
<%@ Register Assembly="RadInput.Net2" Namespace="Telerik.WebControls" TagPrefix="radI" %>

<%@ Register Assembly="RadWindow.Net2" Namespace="Telerik.WebControls" TagPrefix="radW" %>

<%@ Register Assembly="RadComboBox.Net2" Namespace="Telerik.WebControls" TagPrefix="radC" %>

<%@ Register Assembly="RadToolbar.Net2" Namespace="Telerik.WebControls" TagPrefix="radTlb" %>
<%@ Register Assembly="RadGrid.Net2" Namespace="Telerik.WebControls" TagPrefix="radG" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
           <asp:Button runat=server ID=btnAddtoDispensement Text="Add"  Font-Size=XX-Small OnClick="btnAddtoDispensement_Click" />
               
                <radG:RadGrid ID="grdItemsforReorder" runat="server" AllowAutomaticUpdates="True" AllowPaging="True"  EnableAJAXLoadingTemplate="True"  LoadingTemplateTransparency="25" Skin="Office2007" Width="100%" AutoGenerateColumns="False" EnableAJAX="True" EnableViewState="True" GridLines="None" AllowMultiRowSelection="True" OnPageIndexChanged="grdItemsforReorder_PageIndexChanged">
                    <PagerStyle Mode="NextPrevAndNumeric" />
                    <MasterTableView>
                        <ExpandCollapseColumn Visible="False">
                            <HeaderStyle Width="19px" />
                        </ExpandCollapseColumn>
                        <RowIndicatorColumn Visible="False">
                            <HeaderStyle Width="20px" />
                        </RowIndicatorColumn>
                        
                        <Columns>
                            <radG:GridClientSelectColumn UniqueName="ClientSelectColumn"/>
                            <radG:GridBoundColumn DataField="SupplierName" HeaderText="Supplier" UniqueName="SupplierName">
                                
                            </radG:GridBoundColumn>
                            <radG:GridBoundColumn DataField="MCName" HeaderText="Part" UniqueName="MCName">
                                
                            </radG:GridBoundColumn>
                            <radG:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center"  HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center" />
                            
                            
                            <radG:GridBoundColumn DataField="LeftRightSide" HeaderText="Side" UniqueName="LeftRightSide" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center">
                            </radG:GridBoundColumn>
                            
                            <radG:GridBoundColumn DataField="Size" HeaderText="Size" UniqueName="Size" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center">
                            </radG:GridBoundColumn>    
                            
                                
                            <radG:GridBoundColumn DataField="QuantityOnHand" HeaderText="On Hand" UniqueName="QuantityOnHand" HeaderStyle-Wrap="true" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center">
                                
                            </radG:GridBoundColumn>
                            <radG:GridBoundColumn DataField="QuantityOnOrder" HeaderText="Qty. On Order" UniqueName="QuantityOnHand" HeaderStyle-Wrap="true" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center">
                                
                               
                            </radG:GridBoundColumn>
                            <radG:GridBoundColumn DataField="ParLevel" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50" HeaderText="Par" UniqueName="ParLevel" HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center">
                             
                             </radG:GridBoundColumn>
                            <radG:GridBoundColumn DataField="ReorderLevel" HeaderText="Reorder Level" UniqueName="ReorderLevel" HeaderStyle-Wrap="true" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center">
                                
                            </radG:GridBoundColumn>
                            <radG:GridBoundColumn DataField="CriticalLevel" HeaderText="Critical Level" UniqueName="CriticalLevel" HeaderStyle-Wrap="true" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center">
                                
                            </radG:GridBoundColumn>
                            <radG:GridBoundColumn DataField="Priority" UniqueName="Priority" Visible="False">
                                
                            </radG:GridBoundColumn>
                            <radG:GridTemplateColumn  UniqueName="SuggestedReorderLevel" DataField="SuggestedReorderLevel" HeaderText="Sugg. Order #" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center"  HeaderStyle-Wrap="true" HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    &nbsp;<radI:RadNumericTextBox  ID="txtSuggReorderLevel" Text='<%# bind("SuggestedReorderLevel") %>' runat="server"  ShowSpinButtons="False" Culture="English (United States)" LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" Skin="Office2007" SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Width="20px" NumberFormat-DecimalDigits="0">
                                        <NumberFormat DecimalDigits="0" />
                                    </radI:RadNumericTextBox>
                                </ItemTemplate>
                            </radG:GridTemplateColumn>
                            
                            <radG:GridBoundColumn DataField="practicecatalogproductid" UniqueName="practicecatalogproductid"
                                Display="False">
                                
                            </radG:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings AllowColumnsReorder="false" AllowDragToGroup="false" ReorderColumnsOnClient="false">
                      <Selecting AllowRowSelect="True"/>
                      <ClientEvents OnGridCreated="GridCreated" ></ClientEvents>
                    </ClientSettings>
                    
                </radG:RadGrid>
    </form>
</body>
</html>
