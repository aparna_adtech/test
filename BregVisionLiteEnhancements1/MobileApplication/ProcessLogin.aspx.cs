﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace BregVision.MobileApplication
{
    public partial class ProcessLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //clear all session variables
            //if (Session["OrderApprovalQueryString"] == null)
            //{
            //    Session.Abandon();
            //    Session.Clear();
            //}//Session.Remove["PracticeLocation"];
            //else
            //{
            //This is done so that the Master page will reinitialize this variables when loading. 
            Session.Abandon();
            Session.Clear();
            //}


            if (User.Identity.IsAuthenticated == false)
            {
                Response.Redirect("default.aspx");
            }
            else
            {
                Response.Redirect("PracticeLocation.aspx");
            }   
                
        }
    }
}
