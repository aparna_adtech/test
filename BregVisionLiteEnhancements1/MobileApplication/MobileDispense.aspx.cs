﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.WebControls;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using ClassLibrary.DAL;
using System.Linq;
using DAL.PracticeLocation;
using System.Text.RegularExpressions;
using System.IO;
using System.Text;
using BregWcfService;

namespace BregVision.MobileApplication
{
    public partial class MobileDispense : System.Web.UI.Page
    {

        ArrayList list = new ArrayList();
        Boolean allReadyDispensed;
        private IEnumerable<ICD9> _ICD9_Codes = null;
        private bool _CurrentUserIsAllowedZeroQtyDispense = false;

        protected void InitializeComponent()
        {

            grdSummary.DeleteCommand += new GridCommandEventHandler(grdSummary_DeleteCommand);
            this.LoadComplete += new EventHandler(this.Page_Load);
            //grdDispenseSearchMobile.PageIndexChanged += new GridPageChangedEventHandler(grdDispenseSearchMobile_PageIndexChanged);
            //   grdSearch.PageIndexChanged += new GridPageChangedEventHandler(grdSearch_PageIndexChanged);
            //this.Load += new System.EventHandler(this.Page_Load);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BLL.Practice.User user = new BLL.Practice.User();
            _CurrentUserIsAllowedZeroQtyDispense = user.IsAllowedZeroQtyDispense(Context.User.Identity.Name.ToString());

            btnAddtoDispensement.Visible = true;
            btnAddToQueue.Visible = true;

            if (!IsPostBack)
            {
                ViewState["allReadyDispensed"] = false;
                if (dtDispensed.Text.Length == 0)
                {
                    dtDispensed.Text = DateTime.Today.ToString("MM/dd/yy");
                }

                //BLL.Practice.User user = new BLL.Practice.User();
                //_CurrentUserIsAllowedZeroQtyDispense = user.IsAllowedZeroQtyDispense(Context.User.Identity.Name.ToString());

            }

            //
            //{
            //tInventoryOrderStatus(Convert.ToInt16(Session["PracticeID"]), Convert.ToInt16(Session["PracticeLocationID"]));
            //if (!IsPostBack)
            //{
                //GetLast10ItemsDispensed(Convert.ToInt16(Session["PracticeID"]), Convert.ToInt16(Session["PracticeLocationID"]));
           // }
            //grdDispenseSearchMobile.ItemDataBound += new GridItemEventHandler(grdDispenseSearchMobile_ItemDataBound);
            //Session["PracticeLocationName"] = Request.Form["selPracticeLocation"].ToString();
            lblPractice2.Text = string.Concat(Session["PracticeName"].ToString()," - ",Session["PracticeLocationName"].ToString());
            LoadPhysicianData();
 
            // GetDeadStock(Convert.ToInt16(Session["PracticeID"]), Convert.ToInt16(Session["PracticeLocationID"]));
            //}
        }

        //protected void grdDispenseSearchMobile_PageIndexChanged(object source, Telerik.WebControls.GridPageChangedEventArgs e)
        //{
        //    this.grdDispenseSearchMobile.CurrentPageIndex = e.NewPageIndex;
        //    LoopThroughSearchGrid(grdDispenseSearchMobile.MasterTableView);

        //}

        public void GetLast10ItemsDispensed(int practiceID, int practiceLocationID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetLast10ItemsDispensed");

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);

            DataSet ds = db.ExecuteDataSet(dbCommand);

            grdDispenseSearchMobile.DataSource = ds.Tables[0];
            grdDispenseSearchMobile.DataBind();

            //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
        }

        protected void LoadPhysicianData()
        {
            if (!IsPostBack)
            {
                DataSet ds = GetPhysicians(Convert.ToInt32(Session["PracticeLocationID"]));

                ddlPhysician.DataSource = ds;

                ddlPhysician.DataValueField = "PhysicianId";
                ddlPhysician.DataTextField = "PhysicianName";
                ddlPhysician.DataBind();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlPhysician.Items[0].Selected = true;
                }
                //else 
                //{
                //    ddlPhysician.Visible = false;
                //}
            }
        }

        public DataSet GetPhysicians(int practiceLocationID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_PracticeLocation_Physician_Select_All_Names_By_PracticeLocationID");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            DataSet ds = db.ExecuteDataSet(dbCommand);

            return ds;
        }

        protected void btnAddToQueue_Click(object sender, EventArgs e) 
        {

            LoopThroughSearchGrid(grdDispenseSearchMobile.MasterTableView);//, false);

            lblSearchResultsStatus.Text = "";

            //Make sure we have added a dispensement
            if (ViewState["ArrayList"] == null)
            {
                lblSearchResultsStatus.Text = "No item(s) added. Please add item(s)";
                return;
            }

            if (ViewState["ArrayList"] != null)
            {
                list = (ArrayList)ViewState["ArrayList"];
                if (list.Count == 0)
                //Checks for dups and exits the loop if one exists
                {
                    lblSearchResultsStatus.Text = "No item(s) added. Please add item(s)";
                    return;
                }
            }

            if (!allReadyDispensed)
            {
                //Validate needed data
                if (dtDispensed.Text.Length == 0)
                {
                    lblSummaryStatus.Text = "Date needs to be entered.";
                    dtDispensed.Focus();
                    return;
                }
                //Valid Date
                if (!isDate(dtDispensed.Text.ToString()))
                {
                    lblSummaryStatus.Text = "Valid Date needs to be entered.";
                    dtDispensed.Focus();
                    return;
                }
                string errorMessage = "";
                if (!LoopThroughSummaryGridValidate(grdSummary.MasterTableView, out errorMessage))
                {
                    lblSummaryStatus.Text = "Data in grid needed." + "&nbsp;&nbsp;" + errorMessage;
                    return;
                }

                PopulateSummaryGrid();

                LoopThroughSummaryGrid(grdSummary.MasterTableView, false);

                allReadyDispensed = true;
                ViewState["allReadyDispensed"] = allReadyDispensed;

                divSearch.Visible = false;
                divGrid.Visible = false;
                divPhysicianPatient.Visible = false;
                divSummary.Visible = false;
                divReceipt.Visible = true;

                Receipt.Visible = false;

                btnAddtoDispensement.Visible = false;
            }
        }
        protected void btnAddtoDispensement_Click(object sender, EventArgs e)
        {
            LoopThroughSearchGrid(grdDispenseSearchMobile.MasterTableView);//, true);
            btnAddToQueue.Visible = false;
        }

        private void LoopThroughSearchGrid(GridTableView gridTableView)//, Boolean boolAddToDISP)
        {
            Int16 numProducts = 0;

            foreach (GridDataItem gridDataItem in grdDispenseSearchMobile.Items)
            {
                //foreach (GridDataItem item in RadGrid1.SelectedItems) 
                if (findSelectColumn(gridDataItem))
                {
                    //lblStatus.Text = "Adding items to the cart.";
                    UpdateSummaryArray(findProductInventoryID(gridDataItem));
                    numProducts += 1;
                }
            }

            if (_CurrentUserIsAllowedZeroQtyDispense == true)
            {
                lblSearchResultsStatus.Text = string.Concat(numProducts.ToString(), " product(s) added to dispensement");
            }
            else 
            {
                lblSearchResultsStatus.Text = string.Concat(numProducts.ToString(), " product(s) added to dispensement</br> but may not be dispensed");
            }

        }

        private Boolean findSelectColumn(GridDataItem gridDataItem)
        {
            TableCell cell = gridDataItem["ClientSelectColumn2"];
            CheckBox checkBox = (CheckBox)cell.Controls[0];
            if (checkBox.Checked)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private Int32 findProductInventoryID(GridDataItem gridDataItem)
        {
            string ProductInventoryID = (string)gridDataItem["ProductInventoryID"].Text;
            if (ProductInventoryID != null)
            {
                return Int32.Parse(ProductInventoryID);
            }
            else
            {
                return 0;
            }
        }

        protected void UpdateSummaryArray(Int32 InventoryID)
        {
            if (ViewState["ArrayList"] != null)
            {
                list = (ArrayList)ViewState["ArrayList"];

                //Checks for dups and exits the loop if one exists

                Int32 listItemtoRemove = -1;
                foreach (Int32 item in list)
                {
                    if (Convert.ToInt32(item) == Convert.ToInt32(InventoryID))
                        listItemtoRemove = item;
                }

                if (listItemtoRemove >= 0)
                {
                    list.Remove(listItemtoRemove);
                }

                list.Add(InventoryID);
            }
            else
            {
                //Checks for dups and exits the loop if one exists
                Int32 listItemtoRemove = -1;
                foreach (Int32 item in list)
                {
                    if (Convert.ToInt32(item) == Convert.ToInt32(InventoryID))
                        listItemtoRemove = item;
                }

                if (listItemtoRemove >= 0)
                {
                    list.Remove(listItemtoRemove);
                }
                list.Add(InventoryID);
                ViewState["ArrayList"] = list;
            }

            //list = ArrayList ViewState["ArrayList"];
            //list.Add("test 1");
            //list.Add("test 3");
            //list.Add("test 4");
            //ViewState["ArrayList"] = list;


        }

        public DataSet SearchForItemsInventory(int practiceLocationID, string searchCriteria, string searchText)
        {
           // int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_SearchforItemsInInventory");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, searchCriteria);
            db.AddInParameter(dbCommand, "SearchText", DbType.String, searchText);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
            //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
        }

        protected void grdSummary_DeleteCommand(object sender, Telerik.WebControls.GridCommandEventArgs e)
        {
            //delete item
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = e.Item as GridDataItem;
                Int32 productInventoryID = Int32.Parse(dataItem["ProductInventoryID"].Text);
                if (ViewState["ArrayList"] != null)
                {
                    list = (ArrayList)ViewState["ArrayList"];
                    list.Remove(productInventoryID);

                    //deleteItemFromShoppingCart(shoppingCartItemID);

                    grdSummary.DataSource = grdSummary.DataSource = GetSummaryData(BuildArrayString(list));
                    grdSummary.Rebind();
                }
            }
        }

        private string BuildArrayString(ArrayList list)
        {
            string arrayValues = "";
            foreach (Int32 item in list)
            {

                arrayValues += String.Concat(item.ToString(), ",");

            }

            if (arrayValues.Length > 0)
            {
                arrayValues = arrayValues.Substring(0, arrayValues.Length - 1);

            }
            return arrayValues;
        
        }
        protected void grdSummary_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;
                try
                {
                    //added by u= 01/27/2010
                    HiddenField hdnDMEDeposit = (HiddenField)dataItem.FindControl("hdnDMEDeposit");
                    RadNumericTextBox txtDMEDeposit = (RadNumericTextBox)dataItem.FindControl("txtDMEDeposit");
                    decimal DMEDeposit = Convert.ToDecimal(txtDMEDeposit.Text.Replace("$", ""));

                    hdnDMEDeposit.Value = DMEDeposit.ToString();
                }
                catch
                { 
                
                }
            }
        }

        protected void grdSummary_OnLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                grdSummary.DataSource = new Object[] { };
                grdSummary.DataBind(); //Used to show a 'No Records Found' when the page initially loads
            }
        }

        public DataSet GetSummaryData(string arrayValues)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetPreDispensementSummaryMobile");

            db.AddInParameter(dbCommand, "ProductInventoryIDArray", DbType.String, arrayValues);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        private bool isDate(string strDate)
        {
            try
            {
                DateTime.Parse(strDate);
                return true;
            }
            catch 
            {
                return false;
            }
        }

        private Boolean LoopThroughSummaryGridValidate(GridTableView gridTableView, out string message)
        {
            message = "";
            foreach (GridDataItem gridDataItem in gridTableView.Items)
            {
                //RadNumericTextBox txtQuantity = (RadNumericTextBox)gridDataItem.FindControl("txtQuantityDispensed");
                //if (txtQuantity.Text.Length == 0)
                //{
                //    return false;
                //}
               
                TextBox txtQuantity = (TextBox)gridDataItem.FindControl("txtQuantityDispensed");

                //Yuiko Check txtQuantity to make sure it is an int greater than 0
                if(txtQuantity.Text.Length == 0) 
                {
                    return false;
                }

                //added by u= 01/29/2010 to see if the quantity in the text box is greater than QuantityOnHandPerSystem
                int iQuantityOnHandPerSystem = System.Convert.ToInt32(gridDataItem["QuantityOnHandPerSystem"].Text);
                try
                {
                    if(txtQuantity.Text=="0")
                    {
                        txtQuantity.Focus();
                        txtQuantity.Text = "1";
                        message = "Cannot dispense zero quantity.";
                        return false;
                    }

                    if(chkAddToQueue.Checked == false)
                    {
                        if (iQuantityOnHandPerSystem < Convert.ToInt32(txtQuantity.Text) && _CurrentUserIsAllowedZeroQtyDispense == false)
                        {
                            //Warn user that they will not be able to dispense this without admin override
                            txtQuantity.Focus();
                            txtQuantity.Text = "1";//iQuantityOnHandPerSystem.ToString(); 
                            message = "Cannot dispense more than on hand.";
                            return false;
                        }
                    }
                }
                catch
                {
                    txtQuantity.Text = "";
                    txtQuantity.Focus();
                    message = "Numeric only.";
                    return false;
                }

                RadNumericTextBox txtDMEDeposit = (RadNumericTextBox)gridDataItem.FindControl("txtDMEDeposit");
                
                //Yuiko Check   DME Deposit to make sure it is a Decimal greater than zero or equal to zero
                if (txtDMEDeposit.Text.Length == 0)
                {
                    return false;
                } 
            }
            return true;
        }

        Int32 DispenseID;

        private void LoopThroughSummaryGrid(GridTableView gridTableView, Boolean boolAddToDISP)
        {
            Int32 PracticeLocationID = Convert.ToInt32(Session["PracticeLocationID"]);

            int dispenseBatchID = DAL.PracticeLocation.Dispensement.CreateNewDispenseBatch(PracticeLocationID);
            
            bool addToDispenseQueue = chkAddToQueue.Checked;

            foreach (GridDataItem gridDataItem in gridTableView.Items)
            {
                Int32 ProductInventoryID = Convert.ToInt32((string)gridDataItem["ProductInventoryID"].Text);
                
                string txtPatientCode = this.txtPatientCode.Text;
                Int32 PracticeCatalogProductID = Convert.ToInt32((string)gridDataItem["PracticeCatalogProductID"].Text);

                ////commented out by u= 01/25/2010
                //// if cboPhysician.value is null or empty use -1
                //Int32 PhysicianIDOld = ((ddlPhysician.SelectedValue != null) && (ddlPhysician.SelectedValue.Length > 1)
                //    ? Convert.ToInt32(ddlPhysician.SelectedValue.ToString()) : -1);

                Int32 PhysicianID = 0;
                if ((ddlPhysician.SelectedValue == "") && (ddlPhysician.SelectedValue.Length == 0))
                {
                    PhysicianID = -1;
                }
                else 
                {
                    PhysicianID = Convert.ToInt32(ddlPhysician.SelectedValue.ToString());  //added by u= 01/25/2010
                }
                

                decimal BillingCharge = Convert.ToDecimal((string)gridDataItem["BillingCharge"].Text.Replace("$", ""));
                decimal BillingChargeCash = Convert.ToDecimal((string)gridDataItem["BillingChargeCash"].Text.Replace("$", ""));

                //We're not going to use the NumericTextBox cuz even the qty is changed, it changed back to default value
                RadNumericTextBox txtQuantityOrg = (RadNumericTextBox)gridDataItem.FindControl("txtQuantityDispensedOrg");
                Int32 QuantityDispensedOrg = Convert.ToInt32(txtQuantityOrg.Value.ToString());
                
                TextBox txtQuantity = (TextBox)gridDataItem.FindControl("txtQuantityDispensed");
                Int32 QuantityDispensed = Convert.ToInt32(txtQuantity.Text.ToString());

                CheckBox checkBoxDMEDeposit = (CheckBox)gridDataItem.FindControl("chkDMEDeposit");
                Boolean IsCashCollection = checkBoxDMEDeposit.Checked;

                decimal WholesaleCost = Convert.ToDecimal((string)gridDataItem["WholesaleCost"].Text.Replace("$", ""));
                RadNumericTextBox txtDMEDeposit = (RadNumericTextBox)gridDataItem.FindControl("txtDMEDeposit");
                decimal DMEDeposit = Convert.ToDecimal(txtDMEDeposit.Text.Replace("$", ""));

                string icd9Code = gridDataItem.GetColumnText("cboICD9");
              
                string hcpcsString = gridDataItem.GetColumnString("HCPCsString");
                SaveNewICD9(icd9Code, hcpcsString);

                bool isMedicare = gridDataItem.GetColumnChecked("chkIsMedicare");
                CheckBox checkBoxIsMedicare = (CheckBox)gridDataItem.FindControl("chkIsMedicare");
                Boolean boolIsMedicare = checkBoxIsMedicare.Checked;

                //CheckBox chkDMEDeposit = (CheckBox)sender;
                //GridDataItem gridDataItem = (GridDataItem)chkDMEDeposit.NamingContainer;

                bool isABNForm = gridDataItem.GetColumnChecked("chkABNForm");
                CheckBox checkBoxABNForm = (CheckBox)gridDataItem.FindControl("chkABNForm");
                Boolean boolABNForm = checkBoxABNForm.Checked;

                string side = gridDataItem.GetColumnSelectedValue("cboSide");
                string abntype = gridDataItem.GetColumnSelectedValue("ddlAbnType");

                decimal ActualChargeBilled;
                if (IsCashCollection)
                {
                    ActualChargeBilled = BillingChargeCash;
                    DMEDeposit = 0;
                }
                else
                {
                    ActualChargeBilled = BillingCharge;
                }

                var isCustomFit = false;//TODO:Split code logic

                var mod1 = gridDataItem.GetColumnSelectedValue("ddlmod1");
                var mod2 = gridDataItem.GetColumnSelectedValue("ddlmod2");
                var mod3 = gridDataItem.GetColumnSelectedValue("ddlmod3");

                //RadNumericTextBox txtOverride = (RadNumericTextBox)gridDataItem.FindControl("txtOverride");
                //if (txtOverride.Value > 0)
                //{
                //    ActualChargeBilled = Convert.ToDecimal(txtOverride.Value);
                //}

                DateTime DispenseDate = Convert.ToDateTime(dtDispensed.Text);

                //If chkAddToQueue is unchecked and "Add to DISP" button is clicked --added by u= 02/01/2010
                if (addToDispenseQueue == false && boolAddToDISP == true)
                {
                    DispenseID = DAL.PracticeLocation.Dispensement.DispenseProducts(1, PracticeLocationID, ProductInventoryID, txtPatientCode,
                        PracticeCatalogProductID, PhysicianID, icd9Code, side, isMedicare, isABNForm, ActualChargeBilled, QuantityDispensed, IsCashCollection,
                        WholesaleCost, DMEDeposit, DispenseID, DispenseDate, true, dispenseBatchID, 0, false, "");

                    //Set ViewState for Dispense ID
                    ViewState["DispenseID"] = DispenseID;
                }
                //If chkAddToQueue is checked or "Add to Queue" button is clicked --added by u= 02/01/2010
                else if (addToDispenseQueue == true || boolAddToDISP == false)
                {
                    
                    DAL.PracticeLocation.Dispensement.AddUpdateDispensement(
                                                                                DispenseQueueID: 0,
                                                                                PracticeLocationID: PracticeLocationID,
                                                                                InventoryID: ProductInventoryID,
                                                                                PatientCode: txtPatientCode,
                                                                                PhysicianID: PhysicianID, 
                                                                                ICD9_Code: icd9Code,
                                                                                IsMedicare: isMedicare,
                                                                                ABNForm: isABNForm,
                                                                                IsCashCollection: IsCashCollection,
                                                                                Side: side,
                                                                                DMEDeposit: DMEDeposit,
                                                                                AbnType: abntype,
                                                                                BregVisionOrderID: null,
                                                                                Quantity: QuantityDispensed,
                                                                                isCustomFit: isCustomFit,
                                                                                DispenseDate: DispenseDate,
                                                                                mod1: mod1,
                                                                                mod2: mod2,
                                                                                mod3: mod3,
                                                                                CreatedUser: 1,
                                                                                CreatedDate: DateTime.Now,
                                                                                QueuedReason: "",
                                                                                IsCalledFromV1Endpoint: false);
                }
            }

            if (addToDispenseQueue == false)
            {
                SendBillingEmail(DispenseID, PracticeLocationID, Session["PracticeName"].ToString(), null);
                Receipt.Visible = true;
                Receipt.InnerHtml = BuildReceipt();
                lblSummaryStatus.Text = "Items dispensed. Click next to view receipt.";
                //Switch to receipt tab.
                lblTitle.Text = "Receipt";
            }
            else
            {
                lblSummaryStatus.Text = "Items added to queue";
                //Switch to receipt tab.
                lblQueueStatus.Text = "Items added to queue";
                lblQueueStatus.Visible = true;
                Receipt.Visible = false;
            }
        }

        private void SaveNewICD9(string UserICD9Code, string HCPCsString)
        {
            if (string.IsNullOrEmpty(UserICD9Code) || string.IsNullOrEmpty(HCPCsString))
            {
                return;
            }
            //parse the HCPCsString from a comma separated string to a list usable by LINQ
            string hcpcs = HCPCsString;
            hcpcs = hcpcs.Replace(" ", "");
            hcpcs = hcpcs.Replace("&nbsp;", "");
            string[] hcpcsArray = hcpcs.Split(",".ToCharArray());
            List<string> hcpcList = hcpcsArray.ToList<string>();

            //Load the IDC9's 
            _ICD9_Codes = DAL.PracticeLocation.Dispensement.GetDispenseQueueICD9s(Convert.ToInt32(Session["PracticeLocationID"]));

            //Find the the ICD9 items that match these HCPCs
            var hcpcsForThisRow = from icd in _ICD9_Codes where hcpcList.Contains(icd.HCPCs) select icd;

            //Se if the user ICD9 Code(can be picked from the list Items in the comboBox or typed in) is in the list
            var icd9Exists = from existing_icd9 in hcpcsForThisRow where existing_icd9.ICD9Code == UserICD9Code select existing_icd9;
            if (icd9Exists.Count() < 1)
            {
                //user typed a new ICD9 code that wasn't in the dropdown
                foreach (string hcpc in hcpcList)
                {
                    //Add the ICD9 the user typed in to both the ICD9 and Practice_ICD9 tables
                    int icd9ID = DAL.PracticeLocation.Dispensement.AddPracticeICD9(Convert.ToInt32(Session["PracticeID"]), hcpc, UserICD9Code);
                    DAL.PracticeLocation.Dispensement.InsertPracticeICD9(icd9ID, Convert.ToInt32(Session["PracticeID"]));
                }
            }
        }


        public Int32 DispenseProducts(int UserID, int practiceLocationID, int ProductInventoryID,
        string PatientCode, int PracticeCatalogProductID, int PhysicianID,
        decimal ActualChargeBilled, int QuantityDispensed, Boolean IsCashCollection,
        decimal WholesaleCost, decimal DMEDeposit, int DispenseID, DateTime DispenseDate, Boolean CreatedWithHandheld)
        {
            int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_DispenseProductsNew");

            db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            db.AddInParameter(dbCommand, "ProductInventoryID", DbType.Int32, ProductInventoryID);
            db.AddInParameter(dbCommand, "PatientCode", DbType.String, PatientCode);
            db.AddInParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, PracticeCatalogProductID);

            db.AddInParameter(dbCommand, "PhysicianID", DbType.Int32, PhysicianID);

            db.AddInParameter(dbCommand, "ActualChargeBilled", DbType.Decimal, ActualChargeBilled);
            db.AddInParameter(dbCommand, "Quantity", DbType.Int32, QuantityDispensed);
            db.AddInParameter(dbCommand, "IsCashCollection", DbType.Boolean, IsCashCollection);
            db.AddInParameter(dbCommand, "WholesaleCost", DbType.Decimal, WholesaleCost);
            db.AddInParameter(dbCommand, "DMEDeposit", DbType.Decimal, DMEDeposit);
            db.AddInParameter(dbCommand, "DispenseIDIn", DbType.Decimal, DispenseID);
            db.AddInParameter(dbCommand, "DispenseDate", DbType.DateTime, DispenseDate);
            db.AddInParameter(dbCommand, "CreatedWithHandheld", DbType.Boolean, CreatedWithHandheld);
            db.AddOutParameter(dbCommand, "DispenseID", DbType.Int32, 4);
            rowsAffected = db.ExecuteNonQuery(dbCommand);
            Int32 intStatus = Convert.ToInt32(db.GetParameterValue(dbCommand, "DispenseID"));
            return intStatus;

        }

        private string BuildReceipt()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            //Build header information
            sb.Append("<table width='220px' bgcolor=white><tr><td>");
            sb.Append("<table width='205px'>");

            sb.Append("<tr><td style='font-size:xx-small' style='font-weight:bold' align=left>BREG Vision Patient Receipt</td>");
            sb.Append("<td align=right>");
            //DateTime test2 = dtDispensed.SelectedDate.Value;
            DateTime test2 = System.DateTime.Now;
            //string test = dtDispensed.SelectedDate.ToString();
            string test = test2.ToString("MM/dd/yyyy");

            sb.Append(test);
            //sb.Append(DateTime.Now);
            sb.Append("</td></tr>");

            sb.Append("<tr><td style='font-size:xx-small' align=left colspan=2>");
            sb.Append(Session["PracticeName"]);
            sb.Append("</td></tr>");

            sb.Append("<tr><td style='font-size:xx-small' align=left colspan=2>");
            sb.Append(Session["PracticeLocationName"]);
            sb.Append("</td></tr>");
            sb.Append("</table>");
            IDataReader reader = GetBillingAddressByPracticeLocation(Convert.ToInt32(Session["PracticeLocationID"]));

            //Get Practice BillingInfo
            sb.Append("<table width='205px'>");
            //DataSet ds = db.ExecuteDataSet(dbCommand);
            if (reader.Read())
            {
                sb.Append("<tr><td style='font-size:xx-small' align=left>");
                sb.Append(reader["BillAddress1"].ToString());
                sb.Append("</td></tr>");
                sb.Append("<tr><td style='font-size:xx-small' align=left>");
                sb.Append(reader["BillCity"].ToString());
                sb.Append(", ");
                sb.Append(reader["BillState"].ToString());
                sb.Append(" ");
                sb.Append(reader["BillZipCode"].ToString());
                sb.Append("</td></tr>");
                sb.Append("<tr><td style='font-size:xx-small' align=left>Fax: ");
                sb.Append(reader["FAX"].ToString());
                sb.Append("</td></tr>");

            }
            reader.Close();
            sb.Append("</table><br><br>");

            //Get Dispense Detail Information
            if (ViewState["DispenseID"] != null)
            {
                DispenseID = (Int32)ViewState["DispenseID"];
            }


            reader = GetDispenseReceipt(DispenseID);
            //DataSet ds = db.ExecuteDataSet(dbCommand);
            if (reader.Read())
            {
                //Doctor Information
                sb.Append("<table width='205px'><tr><td style='font-size:xx-small' align=left width='130px'>Provider: </td><td align=left style='font-size:xx-small' width='100%' >");
                sb.Append(reader["Title"].ToString());
                sb.Append(" ");
                sb.Append(reader["FirstName"].ToString());
                sb.Append(" ");
                sb.Append(reader["LastName"].ToString());
                sb.Append(" ");
                sb.Append(reader["Suffix"].ToString());
                sb.Append("</td></tr>");
                sb.Append("<tr><td align=left style='font-size:xx-small' >Patient ID Number: </td><td  align=left style='font-size:xx-small' >");
                sb.Append(reader["PatientCode"].ToString());
                sb.Append("</td></tr></table><br><br>");

                //Dispense Detail
                sb.Append("<table width='205px' cellspacing=0 cellpadding=3 border=1><tr><td style='font-size:xx-small' align=left>Name</td><td style='font-size:xx-small' align=left>Code</td><td style='font-size:xx-small' align=left>Qty.</td>");
                //sb.Append("<td style='font-size:xx-small' align=left>HCPCs</td><td style='font-size:xx-small' align=left>Deposit</td>");
                //sb.Append("<td style='font-size:xx-small' align=left>Deposit</td>");
                sb.Append("<td style='font-size:xx-small' align=left>Side</td><td style='font-size:xx-small' align=left>Size</td><td style='font-size:xx-small' align=left>Gndr</td>");
                //sb.Append("<td style='font-size:xx-small' align=left>Total</td>");
                sb.Append("</tr>");


                //loop through collection and populate all dispense detail records
                //string grandTotal;
                do
                {
                    sb.Append("<tr><td style='font-size:xx-small' align=left>");
                    sb.Append(reader["ShortName"].ToString());
                    sb.Append("</td><td style='font-size:xx-small' align=left>");
                    sb.Append(reader["Code"].ToString());
                    sb.Append("</td><td style='font-size:xx-small' align=left>");
                    sb.Append(reader["Quantity"].ToString());
                   // sb.Append("</td><td style='font-size:xx-small' align=left>&nbsp");

                    //sb.Append(reader["HCPCsString"].ToString());
                    //sb.Append("</td><td style='font-size:xx-small' align=left>$");

                   // decimal DMEDeposit = Convert.ToDecimal(reader["DMEDeposit"]) * Convert.ToInt16(reader["Quantity"]);
                   // sb.Append(DMEDeposit.ToString("#,##0.00"));

                    //sb.Append(Convert.ToDecimal(reader["DMEDeposit"]).ToString("#,##0.00"));


                    sb.Append("</td><td style='font-size:xx-small' align=left>");

                    sb.Append(reader["Side"].ToString());
                    sb.Append("</td><td style='font-size:xx-small' align=left>");
                    sb.Append(reader["Size"].ToString());
                    sb.Append("</td><td style='font-size:xx-small' align=left>");
                    sb.Append(reader["Gender"].ToString());
                    sb.Append("</td>");
                        
                        //<td style='font-size:xx-small' align=left>$");

                    //decimal Total = Convert.ToDecimal(reader["Total"]) * Convert.ToInt16(reader["Quantity"]);
                    //sb.Append(Total.ToString("#,##0.00"));

                    //sb.Append(Convert.ToDecimal(reader["Total"]).ToString("#,##0.00"));


                    //sb.Append("</td><td style='font-size:smaller' align=left>$");
                    //sb.Append(Convert.ToDecimal(reader["LineTotal"]).ToString("#,##0.00"));
                    sb.Append("</tr>");
                    //grandTotal = Convert.ToDecimal(reader["GrandTotal"]).ToString("#,##0.00");
                } while (reader.Read());

                reader.Close();
                //Grand total line
                //sb.Append("<tr><td style='font-size:smaller' colspan=9 align=right>Total:</td><td style='font-size:smaller;font-weight:bold' align=left>$");
                //sb.Append(grandTotal);
                //sb.Append("</td></tr></table>");
                sb.Append("</table>");
                sb.Append("<table width='210px'><tr><td style='font-size:xx-small' align=center>Please visit SiteMaster.com to print out complete receipt(s).</td></tr></table>");


            }

            return sb.ToString();
        }

        private IDataReader GetDispenseReceipt(Int32 DispenseID)
        {
            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetDispenseReceipt");

            db.AddInParameter(dbCommand, "DispenseID", DbType.Int32, DispenseID);
            IDataReader reader = db.ExecuteReader(dbCommand);
            return reader;

        }
        private IDataReader GetBillingAddressByPracticeLocation(Int32 PracticeLocationID)
        {
            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetBillingAddressByPracticeLocation");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);
            IDataReader reader = db.ExecuteReader(dbCommand);
            return reader;
        }

        protected void CheckIfPageLoaded()
        {
            //do
            //{
                
            //    DoEvents();

            //} while (!LoadComplete());
        }

        protected Boolean ItemsInArray()
        {
            if (ViewState["ArrayList"] == null)
            {
                
                return false;
            }


            if (ViewState["ArrayList"] != null)
            {
                list = (ArrayList)ViewState["ArrayList"];
                if (list.Count == 0)
                //Checks for dups and exits the loop if one exists
                {
                    
                    return false;

                }

            }
            return true;
        }

        protected void PopulateSummaryGrid()
        {
            if (ViewState["ArrayList"] != null)
            {
                list = (ArrayList)ViewState["ArrayList"];
            }

            _ICD9_Codes = DAL.PracticeLocation.Dispensement.GetDispenseQueueICD9s(list, Convert.ToInt16(Session["PracticeLocationID"]));

            grdSummary.DataSource = GetSummaryData(BuildArrayString(list));
            grdSummary.DataBind();
        }

        #region Check Box CheckedChanged event

        //u= 01/22/2010
        //1. If chkABNForm is checked, chkIsMedicare has to be checked 
        //2. chkIsMedicare can be checked by itself -- DMEDeposit = 0
        //3. After chkABNForm is checked and chkIsMedicare is checked, uncheck chkIsMedicare,  chkABNForn is unchecked -- DMEDeposit is default
        //4. After chkABNForm is checked and chkIsMedicare is checked, uncheck chkABNForm, chkIsMedicare can still be checked -- DMEDeposit = 0
        //5. When chkDMEDeposit is checked, DMEDeposit = 0
        ////private CheckBox chkDMEDeposit;
        ////private GridDataItem gridDataItem;
        ////private HiddenField hdnDMEDeposit;
        ////private RadNumericTextBox txtDMEDeposit;

        protected void chkDMEDeposit_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkDMEDeposit = (CheckBox)sender;
            GridDataItem gridDataItem = (GridDataItem)chkDMEDeposit.NamingContainer;

            HiddenField hdnDMEDeposit = (HiddenField)gridDataItem.FindControl("hdnDMEDeposit");

            RadNumericTextBox txtDMEDeposit = (RadNumericTextBox)gridDataItem.FindControl("txtDMEDeposit");
            decimal DMEDeposit = Convert.ToDecimal(txtDMEDeposit.Text.Replace("$", ""));

            //hdnDMEDeposit.Value = DMEDeposit.ToString();

            CheckBox chkBoxABNForm = (CheckBox)gridDataItem.FindControl("chkABNForm");
            CheckBox chkBoxIsMedicare = (CheckBox)gridDataItem.FindControl("chkIsMedicare");

            if(chkDMEDeposit.Checked == true)
            {
                if(DMEDeposit != 0)
                {
                    DMEDeposit = 0;
                    txtDMEDeposit.Text = DMEDeposit.ToString();// "0";
                }
            }
            else 
            {
                if (chkBoxIsMedicare.Checked == true || chkBoxABNForm.Checked == true)
                {
                    //do nothing
                }
                else
                {
                    //PopulateSummaryGrid();
                    txtDMEDeposit.Text = hdnDMEDeposit.Value; //DMEDeposit.ToString();
                }
            }
        }

        protected void chkIsMedicare_CheckedChanged(object sender, EventArgs e)
        {
            ////CheckBox chkIsMedicare = (CheckBox)grdSummary.FindControl("chkIsMedicare");
            ////if (chkIsMedicare.Checked == true)
            ////{
            ////    RadNumericTextBox txtDMEDeposit = (RadNumericTextBox)grdSummary.FindControl("txtDMEDeposit");
            ////    //decimal DMEDeposit = Convert.ToDecimal(txtDMEDeposit.Text.Replace("$", ""));
            ////    txtDMEDeposit.Text = "0";
            ////}
            
            CheckBox chkIsMedicare = (CheckBox)sender;
            GridDataItem gridDataItem = (GridDataItem)chkIsMedicare.NamingContainer;

            HiddenField hdnDMEDeposit = (HiddenField)gridDataItem.FindControl("hdnDMEDeposit");

            RadNumericTextBox txtDMEDeposit = (RadNumericTextBox)gridDataItem.FindControl("txtDMEDeposit");
            decimal DMEDeposit = Convert.ToDecimal(txtDMEDeposit.Text.Replace("$", ""));

            //hdnDMEDeposit.Value = DMEDeposit.ToString();           

            CheckBox chkBoxABNForm = (CheckBox)gridDataItem.FindControl("chkABNForm");
            CheckBox chkBoxDMEDeposit = (CheckBox)gridDataItem.FindControl("chkDMEDeposit");

            if (chkIsMedicare.Checked == false)
            {
                chkBoxABNForm.Checked = false;

                if (chkBoxDMEDeposit.Checked == true)
                {
                    chkBoxDMEDeposit.Checked = true;
                }
                else 
                { 
                    //PopulateSummaryGrid();
                    txtDMEDeposit.Text = hdnDMEDeposit.Value;
                }
            }
            else if(chkIsMedicare.Checked == true)
            {
                txtDMEDeposit.Text = "0";            
            }

            if (chkIsMedicare.Checked == true && chkBoxABNForm.Checked == true)
            {
                if (chkBoxABNForm.Checked == false)
                {
                    chkIsMedicare.Checked = true;
                }
            }
        }

        protected void chkABNForm_CheckedChanged(object sender, EventArgs e) 
        {
            CheckBox chkABNForm = (CheckBox)sender;
            GridDataItem gridDataItem = (GridDataItem)chkABNForm.NamingContainer;

            HiddenField hdnDMEDeposit = (HiddenField)gridDataItem.FindControl("hdnDMEDeposit");

            RadNumericTextBox txtDMEDeposit = (RadNumericTextBox)gridDataItem.FindControl("txtDMEDeposit");
            decimal DMEDeposit = Convert.ToDecimal(txtDMEDeposit.Text.Replace("$", ""));

            //hdnDMEDeposit.Value = DMEDeposit.ToString();   

            CheckBox chkBoxIsMedicare = (CheckBox)gridDataItem.FindControl("chkIsMedicare");

            if (chkBoxIsMedicare.Checked == true && chkABNForm.Checked == false)
            {
                chkBoxIsMedicare.Checked = true;
                txtDMEDeposit.Text = "0";

            }

            if (chkABNForm.Checked == true)
            {
                chkBoxIsMedicare.Checked = true;
                txtDMEDeposit.Text = "0";
            }
            else if(chkABNForm.Checked == false && chkBoxIsMedicare.Checked == false)
            {
                //PopulateSummaryGrid();
                txtDMEDeposit.Text = hdnDMEDeposit.Value; //DMEDeposit.ToString();
            }
        }
        #endregion

        #region Button Click Event
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text.ToString().Length == 0)
            {
                lblSearchStatus.Text = "Please enter search text.";
                txtSearch.Focus();
                return;
            }

            lblSearchStatus.Text = "";

            //Search for items

            grdDispenseSearchMobile.DataSource = SearchForItemsInventory(Convert.ToInt32(Session["PracticeLocationID"]), cboSearch3.Text.ToString(), txtSearch.Text.ToString());
            grdDispenseSearchMobile.DataBind();
            lblTitle.Text = "Search Results";

            divSearch.Visible = false;
            divGrid.Visible = true;
        }

        protected void btnDispenseProducts_Click(object sender, EventArgs e)
        {
            lblSummaryStatus.Text = "";

            if (ViewState["ArrayList"] == null)
            {
                lblSummaryStatus.Text = "No item(s) to dispense.Please add item(s)";
                return;
            }

            if (ViewState["ArrayList"] != null)
            {
                list = (ArrayList)ViewState["ArrayList"];
                if (list.Count == 0)
                //Checks for dups and exits the loop if one exists
                {
                    lblSummaryStatus.Text = "No item(s) to dispense.Please add item(s)";
                    return;
                }
            }

            if (ViewState["allReadyDispensed"] != null)
            {
                allReadyDispensed = (Boolean)ViewState["allReadyDispensed"];
            }
            else
            {
                ViewState["allReadyDispensed"] = allReadyDispensed;
                //allReadyDispensed = false;
            }

            if (!allReadyDispensed)
            {
                //Validate needed data
                if (dtDispensed.Text.Length == 0)
                {
                    lblSummaryStatus.Text = "Date needs to be entered.";
                    dtDispensed.Focus();
                    return;
                }
                //Valid Date
                if (!isDate(dtDispensed.Text.ToString()))
                {
                    lblSummaryStatus.Text = "Valid Date needs to be entered.";
                    dtDispensed.Focus();
                    return;
                }
                string errorMessage = "";
                if (!LoopThroughSummaryGridValidate(grdSummary.MasterTableView, out errorMessage)) 
                {
                    lblSummaryStatus.Text = "Data in grid needed." + "&nbsp;&nbsp;" + errorMessage;
                    return;
                }

                LoopThroughSummaryGrid(grdSummary.MasterTableView, true);

                allReadyDispensed = true;
                ViewState["allReadyDispensed"] = allReadyDispensed;

                divSearch.Visible = false;
                divGrid.Visible = false;
                divPhysicianPatient.Visible = false;
                divSummary.Visible = false;
                divReceipt.Visible = true;
            }
            else
            {
                lblSummaryStatus.Text = "Item has already been dispensed.";
            }
        }
        #endregion

        #region Button Next/Back Click Event
        protected void btnNext1_Click(object sender, EventArgs e)
        {

            //Code to make sure page is loaded so errors do not occur. 
            CheckIfPageLoaded();

            //Validation - Make sure they have entered search text
            if (txtSearch.Text.Length == 0 && !ItemsInArray())
            {
                lblSearchStatus.Text = "Search text must be entered.";
                txtSearch.Focus();
                return;
            }

            //If search empty and items in array then show physician screen
            if (txtSearch.Text.Length == 0 && ItemsInArray())
            {
                lblTitle.Text = "Physician/Patient";

                divSearch.Visible = false;
                divGrid.Visible = false;
                divPhysicianPatient.Visible = true;
                divSummary.Visible = false;
                divReceipt.Visible = false;
                return;
            }

            lblTitle.Text = "Search Results";
            
            divSearch.Visible = true;
            divGrid.Visible = false;
            divPhysicianPatient.Visible = false;
            divSummary.Visible = false;
            divReceipt.Visible = false;
        }

        protected void btnNext2_Click(object sender, EventArgs e)
        {
            
            //Clears out status
            lblSearchResultsStatus.Text = "";

            //Make sure we have added a dispensement
            if (ViewState["ArrayList"] == null)
            {

                lblSearchResultsStatus.Text = "No item(s) added. Please add item(s)";
                return;
            }


            if (ViewState["ArrayList"] != null)
            {
                list = (ArrayList)ViewState["ArrayList"];
                if (list.Count == 0)
                //Checks for dups and exits the loop if one exists
                {
                    lblSearchResultsStatus.Text = "No item(s) added. Please add item(s)";
                    return;

                }

            }

            //Validation - Make sure they have entered search text
            //if (txtSearch.Text.Length == 0)
            //{
            //    lblSearchStatus.Text = "Search text must be entered.";
            //    txtSearch.Focus();
            //    return;
            //}

            lblTitle.Text = "Physician/Patient";

            divSearch.Visible = false;
            divGrid.Visible = false;
            divPhysicianPatient.Visible = true;
            divSummary.Visible = false;
            divReceipt.Visible = false;
        }

        protected void btnNext3_Click(object sender, EventArgs e)
        {

            //Validation - Make sure they have entered search text
            //if (txtSearch.Text.Length == 0)
            //{
            //    lblSearchStatus.Text = "Search text must be entered.";
            //    txtSearch.Focus();
            //    return;
            //}

            //Code to make sure page is loaded so errors do not occur. 
            CheckIfPageLoaded();

            lblTitle.Text = "Summary";
            lblSummaryStatus.Text = "";
            divSearch.Visible = false;
            divGrid.Visible = false;
            divPhysicianPatient.Visible = false;
            divSummary.Visible = true;
            divReceipt.Visible = false;
            //lblSummaryPhysician.Text = ddlPhysician.SelectedItem.ToString(); //<-- org 02/02/2010 commented out by u=

            if (ddlPhysician.SelectedValue == "")
            {
                lblSummaryPhysician.Text = ""; 
            }
            else 
            {
                lblSummaryPhysician.Text = ddlPhysician.SelectedItem.ToString();
            }

            if (txtPatientCode.Text.Length > 0)
            {
                lblSummaryPatientIDLegend.Visible = true;
                lblSummaryPatientID.Text = txtPatientCode.Text;
            }
            else
            {
                lblSummaryPatientIDLegend.Visible = false;
                lblSummaryPatientID.Text = "";
            }

            PopulateSummaryGrid();
        }

        protected void btnNext4_Click(object sender, EventArgs e)
        {

            //Validation - Make sure they have entered search text
            //if (txtSearch.Text.Length == 0)
            //{
            //    lblSearchStatus.Text = "Search text must be entered.";
            //    txtSearch.Focus();
            //    return;
            //}
                        
            
            if (ViewState["ArrayList"] == null)
            {
                lblSummaryStatus.Text = "No item(s) to dispense.Please add item(s)";
                return;
            }


            if (ViewState["ArrayList"] != null)
            {
                list = (ArrayList)ViewState["ArrayList"];
                if (list.Count == 0)
                //Checks for dups and exits the loop if one exists
                {
                    lblSummaryStatus.Text = "No item(s) to dispense.Please add item(s)";
                    return;

                }

            }

            lblTitle.Text = "Receipt";

            divSearch.Visible = false;
            divGrid.Visible = false;
            divPhysicianPatient.Visible = false;
            divSummary.Visible = false;
            divReceipt.Visible = true;

        }

        protected void btnBack2_Click(object sender, EventArgs e)
        {
            //Code to make sure page is loaded so errors do not occur. 
            CheckIfPageLoaded();
            lblTitle.Text = "Search";
            txtSearch.Text = "";
            divSearch.Visible = true;
            divGrid.Visible = false;
            divPhysicianPatient.Visible = false;
            divSummary.Visible = false;
            divReceipt.Visible = false;
        }

        protected void btnBack3_Click(object sender, EventArgs e)
        {
            //Code to make sure page is loaded so errors do not occur. 
            CheckIfPageLoaded();

            lblTitle.Text = "Search Results";

            divSearch.Visible = false;
            divGrid.Visible = true;
            divPhysicianPatient.Visible = false;
            divSummary.Visible = false;
            divReceipt.Visible = false;
        }

        protected void btnBack4_Click(object sender, EventArgs e)
        {
            lblTitle.Text = "Physician/Patient";

            divSearch.Visible = false;
            divGrid.Visible = false;
            divPhysicianPatient.Visible = true;
            divSummary.Visible = false;
            divReceipt.Visible = false;
        }

        protected void btnBack5_Click(object sender, EventArgs e)
        {
            //Code to make sure page is loaded so errors do not occur. 
            CheckIfPageLoaded();

            //New Dispensement
            allReadyDispensed = false;
            ViewState["allReadyDispensed"] = allReadyDispensed;
            lblTitle.Text = "Search";

            divSearch.Visible = true;
            divGrid.Visible = false;
            divPhysicianPatient.Visible = false;
            divSummary.Visible = false;
            divReceipt.Visible = false;

            ViewState["ArrayList"] = null;
            lblSearchResultsStatus.Text = "";
            lblSummaryStatus.Text = "";
            txtPatientCode.Text = "";
            txtSearch.Text = "";
            lblQueueStatus.Visible = false;
           


            //lblTitle.Text = "Summary";

            //divSearch.Visible = false;
            //divGrid.Visible = false;
            //divPhysicianPatient.Visible = false;
            //divSummary.Visible = true;
            //divReceipt.Visible = false;
            //if (txtPatientCode.Text.Length > 0)
            //{
            //    lblSummaryPatientIDLegend.Visible = true;
            //    lblSummaryPatientID.Text = txtPatientCode.Text;
            //}
            //else
            //{
            //    lblSummaryPatientIDLegend.Visible = false;
            //    lblSummaryPatientID.Text = "";
            //}

            //PopulateSummaryGrid();

        }
        #endregion

        private void SendBillingEmail(int DispenseBatchID, int _PracticeLocationID, string _PracticeName, BLL.IVisionSessionProvider sessionProvider)
        {
            if (DispenseBatchID < 1)
            {
                return;
            }

            VisionDataContext visionDB = null;
            visionDB = VisionDataContext.GetVisionDataContext();
            using (visionDB)
            {
                var itemsDispensed = from db in visionDB.DispenseBatches
                                     join d in visionDB.Dispenses on db.DispenseBatchID equals d.DispenseBatchID
                                     join dd in visionDB.DispenseDetails on d.DispenseID equals dd.DispenseID
                                     where db.DispenseBatchID == DispenseBatchID
                                     //&& dd.IsCashCollection == false
                                     select new
                                     {
                                         dd.IsMedicare,
                                         dd.ABNForm,
                                         Cash = dd.IsCashCollection,
                                         DispensedDate = d.DateDispensed.ToShortDateString(),
                                         Physician = dd.Clinician.Contact.FirstName + " " + dd.Clinician.Contact.LastName,
                                         d.PatientCode,
                                         //dd.PracticeCatalogProduct //brand
                                         Name = dd.PracticeCatalogProduct.IsThirdPartyProduct == true ? dd.PracticeCatalogProduct.ThirdPartyProduct.Name : dd.PracticeCatalogProduct.MasterCatalogProduct.Name,
                                         Code = dd.PracticeCatalogProduct.IsThirdPartyProduct == true ? dd.PracticeCatalogProduct.ThirdPartyProduct.Code : dd.PracticeCatalogProduct.MasterCatalogProduct.Code,
                                         dd.LRModifier,
                                         Size = dd.PracticeCatalogProduct.IsThirdPartyProduct == true ? dd.PracticeCatalogProduct.ThirdPartyProduct.Size : dd.PracticeCatalogProduct.MasterCatalogProduct.Size,
                                         HCPCs = visionDB.ConcatHCPCS(dd.PracticeCatalogProductID),
                                         dd.ICD9Code,
                                         ActualChargeBilled = string.Format("{0:C}", dd.ActualChargeBilled),
                                         DMEDeposit = string.Format("{0:C}", dd.DMEDeposit),
                                         QuantityToDispense = dd.Quantity,
                                     };


                DataRow drPracticeLocationBillingAddress = null;
                DAL.PracticeLocation.BillingAddress dalPLBA = new DAL.PracticeLocation.BillingAddress();
                DataSet dsPracticeLocationBillingAddress = dalPLBA.Select(_PracticeLocationID);
                if (dsPracticeLocationBillingAddress.Tables[0].Rows.Count > 0)
                {
                    drPracticeLocationBillingAddress = dsPracticeLocationBillingAddress.Tables[0].Rows[0];
                }

                string attentionOf = "";
                int addressID = 0;
                string addressLine1 = "";
                string addressLine2 = "";
                string city = "";
                string state = "";
                string zipCode = "";
                string zipCodePlus4 = "";
                dalPLBA.Select(_PracticeLocationID,
                                    out attentionOf,
                                     out addressID,
                                     out addressLine1,
                                     out addressLine2,
                                     out city,
                                     out state,
                                     out zipCode,
                                     out zipCodePlus4);

                //make a grid for the dispense queue to render for an email
                GridView dispenseEmail = new GridView();
                dispenseEmail.DataSource = itemsDispensed;
                dispenseEmail.DataBind();

                //add abn forms to email
                var abnNeeded = from abn in itemsDispensed
                                where abn.ABNForm == true
                                select abn;

                System.Web.UI.WebControls.Panel ctlAll = new System.Web.UI.WebControls.Panel();
                ctlAll.Controls.Add(dispenseEmail);
                ctlAll.Controls.Add(new LiteralControl("<br/><br/>"));

                foreach (var abnForm in abnNeeded)
                {
                    Control ctl = Page.LoadControl("~/Authentication/ABNFormCtl.ascx");
                    Authentication.ABNFormCtl abnControl = ctl as Authentication.ABNFormCtl;
                    abnControl.PracticeName = _PracticeName;

                    abnControl.Address1 = addressLine1; //drPracticeLocationBillingAddress["BillAddress1"].ToString();  //abnForm.BillingAddress.AddressLine1;
                    abnControl.Address2 = addressLine2; //drPracticeLocationBillingAddress["BillAddress2"].ToString();  //abnForm.BillingAddress.AddressLine2;
                    abnControl.City = city; // drPracticeLocationBillingAddress["BillCity"].ToString();  //abnForm.BillingAddress.City;
                    abnControl.State = state; // drPracticeLocationBillingAddress["BillState"].ToString();  //abnForm.BillingAddress.State;
                    abnControl.Zip = zipCode; // drPracticeLocationBillingAddress["BillZipCode"].ToString();  //abnForm.BillingAddress.ZipCode;

                    if (drPracticeLocationBillingAddress != null)
                    {
                        abnControl.BillingPhone = drPracticeLocationBillingAddress["Fax"].ToString();
                        abnControl.BillingFax = drPracticeLocationBillingAddress["WorkPhone"].ToString();
                    }

                    abnControl.PatientCode = abnForm.PatientCode.ToString();
                    abnControl.ProductName = abnForm.Name.ToString();
                    abnControl.ProductCost = Convert.ToDecimal(abnForm.ActualChargeBilled.Replace("$", ""));
                    abnControl.LoadControlInfo();
                    ctlAll.Controls.Add(abnControl);
                    ctlAll.Controls.Add(new LiteralControl("<br/><br/>"));
                }


                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htmlTW = new HtmlTextWriter(sw);
                ctlAll.RenderControl(htmlTW);

                string html = sb.ToString();

                //BLL.PracticeLocation.Email bllPLEmail = new BLL.PracticeLocation.Email();
                //bllPLEmail.PracticeLocationID = _PracticeLocationID;
                //bllPLEmail = bllPLEmail.SelectOne(bllPLEmail);
                //string MailTo = bllPLEmail.EmailForBillingDispense; //"Michael_Sneen@Yahoo.com";
                //if (string.IsNullOrEmpty(MailTo.Trim()))
                //{
                //    MailTo = GetCentralizedEmail(_PracticeLocationID);
                //}

                string MailTo = BLL.PracticeLocation.Email.GetBillingEmail(_PracticeLocationID);
                FaxEmailPOs emailSender = new FaxEmailPOs();
                string dispenseBatchIDDebugInfo = "";

                if (string.IsNullOrEmpty(MailTo.Trim()) == false)
                {
                    emailSender.SendEmail("OrderConfirmation", MailTo, html, string.Format("DME Dispensement Information {0}", dispenseBatchIDDebugInfo), BLL.SecureMessaging.MessageClass.Billing, sessionProvider);
                }

                //VisionDataContext.CloseConnection(visionDB);
            }
        }
    }
}
