﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.WebControls;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common; 


namespace BregVision.MobileApplication
{
    public partial class test : System.Web.UI.Page
    {
        ArrayList list = new ArrayList();

        protected void InitializeComponent()
        {

            grdItemsforReorder.PageIndexChanged += new GridPageChangedEventHandler(grdItemsforReorder_PageIndexChanged);
         //   grdSearch.PageIndexChanged += new GridPageChangedEventHandler(grdSearch_PageIndexChanged);
            //this.Load += new System.EventHandler(this.Page_Load);

        }
        
        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {

                GetInventoryOrderStatus(3, 4);
            }
            //if (!IsPostBack)
            //{
            //tInventoryOrderStatus(Convert.ToInt16(Session["PracticeID"]), Convert.ToInt16(Session["PracticeLocationID"]));
            //GetLast10ItemsDispensed(3,4);
            ////grdDispenseSearchMobile.ItemDataBound += new GridItemEventHandler(grdDispenseSearchMobile_ItemDataBound);
            //lblPractice2.Text = string.Concat(Session["PracticeName"].ToString(), " - ", Session["PracticeLocationName"].ToString());
            //LoadPhysicianData();

            // GetDeadStock(Convert.ToInt16(Session["PracticeID"]), Convert.ToInt16(Session["PracticeLocationID"]));
            //}

        }

        protected void btnAddtoDispensement_Click(object sender, EventArgs e)
        {


            LoopHierarchyRecursive(grdItemsforReorder.MasterTableView);

        }
        protected void grdItemsforReorder_PageIndexChanged(object source, Telerik.WebControls.GridPageChangedEventArgs e)
        {
            this.grdItemsforReorder.CurrentPageIndex = e.NewPageIndex;
            LoopHierarchyRecursive(grdItemsforReorder.MasterTableView);

        }
        private Boolean findSelectColumn(GridDataItem gridDataItem)
        {
            TableCell cell = gridDataItem["ClientSelectColumn"];
            CheckBox checkBox = (CheckBox)cell.Controls[0];
            if (checkBox.Checked)
                return true;
            else
            {
                return false;
            }
        }


        protected void btnAddItemsReOrderGrid_Click(object sender, EventArgs e)
        {
            LoopHierarchyRecursive(grdItemsforReorder.MasterTableView);
        }

        private Int16 findPracticeCatalogProductID(GridDataItem gridDataItem)
        {
            string PracticeID = (string)gridDataItem["practicecatalogproductid"].Text;
            if (PracticeID != null)
            {
                return Int16.Parse(PracticeID);
                //Response.Write(gridDataItem.ItemIndex.ToString() + " is: " + PracticeID.ToString() + "<br />");
            }
            else
            {
                return 0;
            }

        }

        private Int16 findQOH(GridDataItem gridDataItem)
        {
            string QOH = (string)gridDataItem["QuantityOnHand"].Text;
            if (QOH != null)
            {
                return Int16.Parse(QOH);

            }
            else
            {
                return 0;
            }

        }

        // 20071019 JB  Added Copied from greg's inventory page.
        private Int16 findReorderQuantity(GridDataItem gridDataItem)
        {
            RadNumericTextBox nTextBox = (RadNumericTextBox)gridDataItem.FindControl("txtSuggReorderLevel");
            //CheckBox checkBox = (CheckBox)gridDataItem.FindControl("chkAddtoCart");

            if (nTextBox != null)
            {
                return Int16.Parse(nTextBox.Value.ToString());
                //Response.Write(gridDataItem.ItemIndex.ToString() + " is: " + nTextBox.Value.ToString() + "<br />");
            }
            else
            {
                return 0;
                //throw new Exception("Cannot find object with ID 'CheckBox1' in item " + gridDataItem.ItemIndex.ToString());
            }
        }

        //  20071019 JB  Following is no longer used.
        private Int16 findReorderLevel(GridDataItem gridDataItem)
        {

            //  20071019 1400  JB  Change to get the SuggestedReorderLevel

            string ReorderLevel = (string)gridDataItem["txtSuggReorderLevel"].Text;
            //string ReorderLevel = (string)gridDataItem["ReorderLevel"].Text;

            if (ReorderLevel != null)
            {
                return Int16.Parse(ReorderLevel);

            }
            else
            {
                return 0;
            }

        }


        int productsAdded = 0;
        Int32 ShoppingCartItemID = 0;
        private void LoopHierarchyRecursive(GridTableView gridTableView)
        {
            ShoppingCartItemID = 0;
            foreach (GridDataItem gridDataItem in gridTableView.Items)
                //foreach (GridDataItem item in RadGrid1.SelectedItems) 
                if (findSelectColumn(gridDataItem))
                {
                    //lblStatus.Text = "Adding items to the cart.";


                    //  20071019 1359 JB get the suggestedReorderLevel to add to the cart.
                    //Int16 ReOrderQuantity = Convert.ToInt16(findReorderLevel(gridDataItem) - findQOH(gridDataItem));
                    Int16 ReOrderQuantity = Convert.ToInt16(findReorderQuantity(gridDataItem));
                    // jb end change


                    //Int16 ReOrderQuantity = findQOH(gridDataItem); 
                    if (ReOrderQuantity <= 0)
                    {
                        ReOrderQuantity = 0;
                    }
                    else
                    {
                        ShoppingCartItemID = UpdateGrdInventoryData(Convert.ToInt16(Session["PracticeLocationID"]), findPracticeCatalogProductID(gridDataItem), 1, ReOrderQuantity);
                        productsAdded += ReOrderQuantity;
                    }

                    //Set Status indicator

                    if (productsAdded == 0)
                    {
                        //string StatusText = string.Concat("Number of products added to cart: ", productsAdded.ToString(), " No products added due to QOH higher than reorder level. Please visit the Inventory page to add products.");
                        //string StatusText = "No products added to cart";
                        
                    }
                    else
                    {
                        //string StatusText = string.Concat("Number of products added to cart: ", productsAdded.ToString());
                        
                    }
           

                }




        }


        private void LoopHierarchyRecursivegrdSearch(GridTableView gridTableView)
        {
            ShoppingCartItemID = 0;
            foreach (GridDataItem gridDataItem in gridTableView.Items)
                //foreach (GridDataItem item in RadGrid1.SelectedItems) 
                if (findSelectColumn(gridDataItem))
                {
                    //lblStatus.Text = "Adding items to the cart.";
                    //Int16 ReOrderQuantity = Convert.ToInt16(findReorderLevel(gridDataItem) - findQOH(gridDataItem));
                    //Int16 ReOrderQuantity = findQOH(gridDataItem); 


                    Int16 reorderQuantity = Convert.ToInt16(findReorderQuantity(gridDataItem));
                    if (reorderQuantity <= 0)
                    {
                        reorderQuantity = 0;
                    }
                    else
                    {
                        ShoppingCartItemID = UpdateGrdInventoryData(Convert.ToInt16(Session["PracticeLocationID"]), findPracticeCatalogProductID(gridDataItem), 1, reorderQuantity);
                        productsAdded += reorderQuantity;
                    }

                    //Set Status indicator
                    //if (productsAdded == 0)
                    //{
                    //    string StatusText = "No products added to cart";
                       
                    //}
                    //else
                    //{
                    //    string StatusText = string.Concat("Number of products added to cart: ", productsAdded.ToString());
                        
                    //}
               

                    //Set Status indicator

                    //string StatusText = string.Concat("Number of products added to cart: ", productsAdded.ToString());
                    //lblStatus2.Text = StatusText;

                   
                    //grdSearch.Rebind();        
                 

                }


        }

        private Int32 UpdateGrdInventoryData(Int16 practiceLocationID, Int16 practiceCatalogProductID, Int16 UserID, Int16 Quantity)
        {
            int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_Inventory_Update_Insert");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            db.AddInParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, practiceCatalogProductID);
            db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
            db.AddInParameter(dbCommand, "Quantity", DbType.Int32, Quantity);
            db.AddOutParameter(dbCommand, "ShoppingCartIDReturn", DbType.Int32, rowsAffected);

            int SQLReturn = db.ExecuteNonQuery(dbCommand);
            //return ds;
            Int32 return_value = Convert.ToInt32(db.GetParameterValue(dbCommand, "ShoppingCartIDReturn"));
            return return_value;
        }



        public void GetInventoryOrderStatus(int practiceID, int practiceLocationID)
        {
            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetInventoryOrderStatus");

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);

            DataSet ds = db.ExecuteDataSet(dbCommand);

            grdItemsforReorder.EnableViewState = false;
            grdItemsforReorder.DataSource = ds.Tables[0];
            //grdItemsforReorder.Columns.Add("MCName");
            //grdItemsforReorder.Columns.a
            grdItemsforReorder.DataBind();


            //int return_value  = Convert.ToInt32(db.GetParameterValue(dbCommand, "RETURN_VALUE"));
        }





    }
}
