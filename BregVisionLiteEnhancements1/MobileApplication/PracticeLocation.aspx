﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PracticeLocation.aspx.cs" Inherits="System.Web.UI.MobileControls.MobilePage" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls"  %>
<%@ Register TagPrefix="SDT" Namespace="System.Data.SqlTypes"  %>
<%@ Register TagPrefix="SDT1" Namespace="Microsoft.Practices.EnterpriseLibrary.Data"  %>
<%@ Register TagPrefix="SDT2" Namespace="Microsoft.Practices.EnterpriseLibrary.Data.Sql"  %>
<%@ Register TagPrefix="SDT3" Namespace="System.Data.Common"  %>
<%@ Register TagPrefix="SDT4" Namespace="System.Data.SqlClient"  %>
<%@ Register TagPrefix="SDT5" Namespace="System.Data"  %>






<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
<script runat="server" language="c#">
    protected void Page_Load(object sender, EventArgs e)
    {
        Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
        DbCommand dbCommand = db.GetStoredProcCommand("dbo.USP_GetPracticeAndPracticeLocation2");
        
        db.AddInParameter(dbCommand, "UserName", DbType.String, Context.User.Identity.Name.ToString());

        IDataReader reader = db.ExecuteReader(dbCommand);


        if (reader.Read())
        {

            //Check to see if session variables have been set.
            if (Session["PracticeName"] == null || Session["PracticeName"].ToString().Length == 0)
            {

                Session["PracticeName"] = reader["PracticeName"].ToString();

                Session["PracticeID"] = reader["PracticeID"].ToString();
            }
            // Clear location drop down
            //                  cbxChangeLocation.ClearSelection();

            do
            {

                MobileListItem item = new MobileListItem();
                //RadComboBoxItem item = new RadComboBoxItem();

                item.Text = reader["Name"].ToString();
                item.Value = reader["PracticeLocationID"].ToString();
                //item.Font.Size = System.Web.UI.WebControls.FontUnit.XXSmall;
                
                //
                //                       //set the value in the drop down to selected if it is the primary location
                if (reader["isPrimaryLocation"].ToString() == "True" && Session["PracticeLocationID"] == null)
                {
                    item.Selected = true;
                    //                           lblLocation.Font.Bold=true;
                    //                         lblLocation.Text = reader["Name"].ToString();
                    Session["PracticeLocationName"] = reader["Name"].ToString();
                    Session["PracticeLocationID"] = reader["PracticeLocationID"].ToString();
                }
                //
                selPracticeLocation.Items.Add(item);
                //selPracticeLocation.Items.
                //                       cbxChangeLocation.Items.Add(item);

                //
            } while (reader.Read());
        }
        reader.Close();

        lblPractice.Text = Session["PracticeName"].ToString();
        //cbxChangeLocation.SelectedItem.Font.Size = System.Web.UI.WebControls.FontUnit.XXSmall;

        //lblClinic.Font.Bold = true;
        //lblClinic.Text = Session["PracticeName"].ToString();
        ////lblLocation.Text = "Location: " + Session["PracticeLocationName"].ToString();
        //lblLocation.Font.Bold = true;
        //lblLocation.Text = Session["PracticeLocationName"].ToString();

    }

    
    protected void cmdSelectLocation_Click(object sender, CommandEventArgs e)
    {

        //CheckBox Login credentials
       // ActiveForm = this.Form2;
        if (selPracticeLocation.Selection.Value.ToString() != Session["PracticeLocationid"])
        {
            ViewState["ArrayList"] = null;
        }
        Session["PracticeLocationName"] = selPracticeLocation.Selection.Text.ToString();
        Session["PracticeLocationid"] = selPracticeLocation.Selection.Value.ToString();
        Response.Redirect("MobileDispense.aspx");

    }
    
</script>

       <mobile:form ID="Form1" runat="server">
            
            <mobile:Panel Runat="server">
            <mobile:Image ID="Image2" Runat=server ImageUrl="../Images/BregHeaderSM.jpg"></mobile:Image>   
            <asp:LoginName ID="LoginName1" Runat="server" 
                 FormatString ="Welcome, {0}" Font-Size="Small"/>
                 <asp:LoginStatus id="lsUSer" runat="server" Font-Size="x-small" LogoutText="&nbsp  (Log Out)" LogoutPageUrl="default.aspx" LogoutAction=RedirectToLoginPage /> 
                 <br/><br />
                <mobile:Label Runat="server" ID="Label2" >You are logged into:</mobile:Label>
                <mobile:Label Runat="server" ID="lblPractice" ForeColor="Blue" ></mobile:Label>
                <br />
                <mobile:Label Runat="server" ID="Label3" >Please select practice location:</mobile:Label>
                <mobile:SelectionList Runat="server" ID="selPracticeLocation">

            </mobile:SelectionList>
            <br />
            <mobile:Command ID="cmdSelectLocation" Runat="server"  OnItemCommand="cmdSelectLocation_Click" >Go</mobile:Command>
            
            </mobile:Panel>
        </mobile:form>
</body>
</html>
