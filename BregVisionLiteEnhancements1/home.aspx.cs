using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Telerik.WebControls;
using System.Reflection;
using System.Collections.Generic;


namespace BregVision
{
    public partial class Home : Bases.PageBase
    {
        #region fields
        private UIHelper2 _UIHelper;
        private int productsAdded = 0;
        private Int32 ShoppingCartItemID = 0;
        private bool searchButtonPressed = false;
        #endregion

        #region search data
        [Serializable]
        private class grdSearchData
        {
            public BregWcfService.InventoryOrderStatusResult[] DataSource { get; set; }
            public string SearchCriteria { get; set; }
            public string SearchText { get; set; }
        }

        private grdSearchData _grdSearchData
        {
            get
            {
                var x = ViewState["grdSearchData"] as grdSearchData;
                return x;
            }
            set
            {
                ViewState["grdSearchData"] = value;
            }
        }
        #endregion

        #region page events
        protected void Page_Load(object sender, EventArgs e)
        {
            ((MasterPages.SiteMaster)Page.Master).LocationChanged +=
                new CommandEventHandler(LocationChangedFromMasterPage);

            InitializeAjaxSettings();

            //don't use this to expand grdSearch.  Just to track rec numbers
            _UIHelper = new UIHelper2(ViewState, grdSearch, true);
            _UIHelper.SetGridLabel("", Page.IsPostBack, lblBrowseDispense); //set the default text on the label

            if (!IsPostBack)
            {
                {
                    var comboBox1 = (Telerik.Web.UI.RadComboBox)Master.FindControl("cbxChangeLocation");
                    comboBox1.Visible = true;
                }

                txtRecordsDisplayed.Text = Session["RecordsDisplayed"].ToString();

                grdSearch.PageSize = recordsDisplayedPerPage;
                grdLast10ItemsDispensed.PageSize = recordsDisplayedPerPage;
                grdCustomBracesToBeDispensed.PageSize = recordsDisplayedPerPage;
                grdItemsforReorder.PageSize = recordsDisplayedPerPage;
            }
        }

        protected void Page_PreRender(Object sender, EventArgs e)
        {
            //Set up the textbox so the search button is click on 'enter' key
            var tbFilter = (Telerik.Web.UI.RadTextBox)rttbFilter.FindControl("tbFilter");
            var rtbFilter = (RadToolbarToggleButton)RadToolbar1.FindControl("rtbFilter");
            UIHelper.SetEnterPressTarget(tbFilter, rtbFilter);

            // if we have a bound search grid with data, show the grid and add button
            var hasSearchResults = (grdSearch.Items.Count > 0) || searchButtonPressed;
            btnAddItemsSearch.Enabled = hasSearchResults;
            grdSearch.Visible = hasSearchResults;
        }
        #endregion

        #region control event handlers
        protected void RadToolbar1_OnClick(object sender, Telerik.WebControls.RadToolbarClickEventArgs e)
        {
            if (e.Button.CommandName == "Search")
            {
                searchButtonPressed = true;

                var searchText = (Telerik.Web.UI.RadTextBox)rttbFilter.FindControl("tbFilter");
                var SearchCriteria = (RadComboBox)rttbSearchFilter.FindControl("cboSearchFilter");

                GetSearchResults(SearchCriteria.Value.ToString(), searchText.Text);
                grdSearch.Rebind();
                grdItemsforReorder.Rebind();

                _UIHelper.SetGridLabel(SearchCriteria.Value.ToString(), Page.IsPostBack, lblBrowseDispense);

                RadToolbarToggleButton button = e.Button as RadToolbarToggleButton;
                button.Toggled = false;
            }
        }

        protected void grdSearch_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            var d = _grdSearchData;
            grdSearch.DataSource = (d != null) ? d.DataSource : new BregWcfService.InventoryOrderStatusResult[] { };

            // make sure what we searched for is populated in the search field
            RadComboBox SearchCriteria = (RadComboBox)rttbSearchFilter.FindControl("cboSearchFilter");
            _UIHelper.SetGridLabel(SearchCriteria.Value.ToString(), Page.IsPostBack, lblBrowseDispense);
        }

        protected void grdItemsforReorder_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetInventoryOrderStatus");

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);

            DataSet ds = db.ExecuteDataSet(dbCommand);

            grdItemsforReorder.DataSource = ds.Tables[0];
        }

        protected void grdLast10ItemsDispensed_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetLast10ItemsDispensed");

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, practiceID);
            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);

            DataSet ds = db.ExecuteDataSet(dbCommand);

            grdLast10ItemsDispensed.DataSource = ds.Tables[0];
        }

        protected void grdCustomBracesToBeDispensed_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetCustomBracesToBeDispensed");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);

            DataSet ds = db.ExecuteDataSet(dbCommand);

            grdCustomBracesToBeDispensed.DataSource = ds.Tables[0];
        }

        protected void grdItemsforReorder_ItemDataBound(object sender, GridItemEventArgs e)
        {
            GridDataItem dataItem = e.Item as GridDataItem;
            if (dataItem != null)
            {
                var p = System.Convert.ToInt32(dataItem["PriorityWithCart"].Text);

                switch (p)
                {
                    case (int)BregWcfService.LandingData.InventoryOrderStatusPriorityWithCart.FlaggedCritical:
                        dataItem.CssClass = "priority-1";
                        break;
                    case (int)BregWcfService.LandingData.InventoryOrderStatusPriorityWithCart.FlaggedForReOrder:
                        dataItem.CssClass = "priority-2";
                        break;
                    case (int)BregWcfService.LandingData.InventoryOrderStatusPriorityWithCart.InShoppingCart:
                        dataItem.CssClass = "shoppingCart";
                        break;
                    case (int)BregWcfService.LandingData.InventoryOrderStatusPriorityWithCart.OnOrder:
                        dataItem.CssClass = "priority-3";
                        break;
                    case (int)BregWcfService.LandingData.InventoryOrderStatusPriorityWithCart.None:
                    default:
                        dataItem.CssClass = "";
                        break;
                }

				DataRowView view = ((DataRowView)dataItem.DataItem);
				if (view != null && view.Row.Table.Columns.Contains("QuantityOnHand") && view.Row.Table.Columns.Contains("OldQuantityOnHand"))
				{
					object quantityOnHand = view.Row["QuantityOnHand"];
					object oldQuantityOnHand = view.Row["OldQuantityOnHand"];
					if (quantityOnHand != DBNull.Value && quantityOnHand != null && oldQuantityOnHand != DBNull.Value && oldQuantityOnHand != null)
						dataItem["QuantityOnHand"].Text = ((int)quantityOnHand + (int)oldQuantityOnHand).ToString();
				}
			}
		}

        protected void grdSearch_OnItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            foreach (Telerik.Web.UI.GridDataItem dataItem in grdSearch.MasterTableView.Items)
            {
                var lblPriority = dataItem.FindControl("lblPriority") as Label;
                if (lblPriority == null)
                    continue;

                var p = System.Convert.ToInt32(dataItem["Priority"].Text);
                var c = Boolean.Parse(dataItem["IsShoppingCart"].Text);

				int newPracticeCatalogProductID = 0;
				try
				{
					newPracticeCatalogProductID = System.Convert.ToInt32(dataItem["NewPracticeCatalogProductID"].Text);
				}
				catch (Exception)
				{
				}
				if (newPracticeCatalogProductID != 0)
					p = 0;

                if (p == (int)BregWcfService.LandingData.InventoryOrderStatusPriority.FlaggedCritical)
                    lblPriority.CssClass = "priority-1";
                else if (p == (int)BregWcfService.LandingData.InventoryOrderStatusPriority.FlaggedForReOrder)
                    lblPriority.CssClass = "priority-2";
                else if (p == (int)BregWcfService.LandingData.InventoryOrderStatusPriority.OnOrder)
                    lblPriority.CssClass = "priority-3";
                else if (c)
                    lblPriority.CssClass = "shoppingCart";
            }
        }

        protected void btnAddItemsSearch_Click(object sender, EventArgs e)
        {
            RadToolbar1.Visible = true;
            lblBrowseDispense.Visible = true;
            lblStatus2.Visible = true;
			lblStatusReplaced2.Text = string.Empty;
			lblStatusReplacedText2.Text = string.Empty;

            LoopHierarchyRecursivegrdSearch(grdSearch.MasterTableView);
            RefreshSearchResults();
            grdItemsforReorder.Rebind();
        }

        protected void btnAddItemsReOrderGrid_Click(object sender, EventArgs e)
        {
			lblStatusReplaced.Text = string.Empty;
			lblStatusReplacedText.Text = string.Empty;
            LoopHierarchyRecursive(grdItemsforReorder.MasterTableView);
            grdItemsforReorder.Rebind();
            RefreshSearchResults();
        }

        protected void txtRecordsDisplayed_TextChanged(object sender, EventArgs e)
        {
            // store new value in session
            Session["RecordsDisplayed"] = txtRecordsDisplayed.Text.ToString();

            // update grids with new page size
            grdSearch.PageSize = Convert.ToInt16(Session["RecordsDisplayed"].ToString());
            grdItemsforReorder.PageSize = Convert.ToInt16(Session["RecordsDisplayed"].ToString());
            grdLast10ItemsDispensed.PageSize = Convert.ToInt16(Session["RecordsDisplayed"].ToString());
            grdCustomBracesToBeDispensed.PageSize = Convert.ToInt16(Session["RecordsDisplayed"].ToString());

            // rebind data
            RefreshSearchResults();
            grdItemsforReorder.Rebind();
            grdLast10ItemsDispensed.Rebind();
            grdCustomBracesToBeDispensed.Rebind();
        }
        #endregion

        #region event handlers
        private void LocationChangedFromMasterPage(object sender, CommandEventArgs e)
        {
            grdItemsforReorder.Rebind();
            grdLast10ItemsDispensed.Rebind();
            grdCustomBracesToBeDispensed.Rebind();

            // invalidate search results
            _grdSearchData = null;
            RefreshSearchResults();
        }
        #endregion

        #region data retrieval and binding methods
        private void GetSearchResults(string searchCriteria, string searchText)
        {
			var productInventory = SearchForItemsInventory(Convert.ToInt32(Session["practiceLocationID"]), searchCriteria, searchText);
			var productInventoryIds = productInventory.Tables[0].AsEnumerable().Select(dataRow => dataRow.Field<int>("ProductInventoryID"));
			var inventoryOrderStatuses = BregWcfService.LandingData.GetInventoryOrderStatusWithPriority(
				Convert.ToInt32(Session["practiceLocationID"]), productInventoryIds.ToList());

			foreach (DataRow inventoryItem in productInventory.Tables[0].Rows)
			{
				int practiceCatalogProductID = (int)inventoryItem["PracticeCatalogProductID"];
				foreach (var statusItem in inventoryOrderStatuses)
				{
					if (statusItem.PracticeCatalogProductID == practiceCatalogProductID)
					{
						SetColumnValue(statusItem, inventoryItem, "PracticeCatalogProductID");
						SetColumnValue(statusItem, inventoryItem, "ProductInventoryID");
						SetColumnValue(statusItem, inventoryItem, "MCName");
						SetColumnValue(statusItem, inventoryItem, "Code");
						SetColumnValue(statusItem, inventoryItem, "UPCCode");
						SetColumnValue(statusItem, inventoryItem, "LeftRightSide");
						SetColumnValue(statusItem, inventoryItem, "Size");
						SetColumnValue(statusItem, inventoryItem, "SupplierName");
						SetColumnValue(statusItem, inventoryItem, "IsThirdPartySupplier");
						SetColumnValue(statusItem, inventoryItem, "SupplierSequence");
						SetColumnValue(statusItem, inventoryItem, "QuantityOnHand");
						SetColumnValue(statusItem, inventoryItem, "QuantityOnOrder");
						SetColumnValue(statusItem, inventoryItem, "QuantityOnHandPlusOnOrder");
						SetColumnValue(statusItem, inventoryItem, "ParLevel");
						SetColumnValue(statusItem, inventoryItem, "ReorderLevel");
						SetColumnValue(statusItem, inventoryItem, "CriticalLevel");
						SetColumnValue(statusItem, inventoryItem, "SuggestedReorderLevel");
						SetColumnValue(statusItem, inventoryItem, "Priority");
						SetColumnValue(statusItem, inventoryItem, "PriorityWithCart");
						SetColumnValue(statusItem, inventoryItem, "StockingUnits");
						SetColumnValue(statusItem, inventoryItem, "IsSplitCode");
						SetColumnValue(statusItem, inventoryItem, "IsShoppingCart");

						SetColumnValue(statusItem, inventoryItem, "HcpcString");
						SetColumnValue(statusItem, inventoryItem, "HcpcOtsString");
						SetColumnValue(statusItem, inventoryItem, "IsConsignment");
						SetColumnValue(statusItem, inventoryItem, "IsABN");
						SetColumnValue(statusItem, inventoryItem, "IsABNNeeded");
						SetColumnValue(statusItem, inventoryItem, "ConsignmentQuantity");
						SetColumnValue(statusItem, inventoryItem, "MasterCatalogProductID");
						SetColumnValue(statusItem, inventoryItem, "ThirdPartyProductID");
						SetColumnValue(statusItem, inventoryItem, "IsThirdParty");
						SetColumnValue(statusItem, inventoryItem, "BillingCharge");
						SetColumnValue(statusItem, inventoryItem, "BillingChargeCash");
						SetColumnValue(statusItem, inventoryItem, "Packaging");
						SetColumnValue(statusItem, inventoryItem, "Gender");
						SetColumnValue(statusItem, inventoryItem, "Color");
						SetColumnValue(statusItem, inventoryItem, "WholesaleCost");
						SetColumnValue(statusItem, inventoryItem, "DMEDeposit");
						SetColumnValue(statusItem, inventoryItem, "UPCCode_List");
						SetColumnValue(statusItem, inventoryItem, "IsCustomBrace");
						SetColumnValue(statusItem, inventoryItem, "ModiferStrings");
						SetColumnValue(statusItem, inventoryItem, "IsAlwaysOffTheShelf");
						SetColumnValue(statusItem, inventoryItem, "IsPreAuthorization");
						SetColumnValue(statusItem, inventoryItem, "IsActive");
						SetColumnValue(statusItem, inventoryItem, "Side");
						SetColumnValue(statusItem, inventoryItem, "BrandName");
						SetColumnValue(statusItem, inventoryItem, "ProductName");
						SetColumnValue(statusItem, inventoryItem, "NewMasterCatalogProductID");
						SetColumnValue(statusItem, inventoryItem, "NewThirdPartyProductID");
						SetColumnValue(statusItem, inventoryItem, "NewPracticeCatalogProductID");
						SetColumnValue(statusItem, inventoryItem, "NewProductName");
						SetColumnValue(statusItem, inventoryItem, "NewProductCode");
					}
				}
			}

            // store values to allow for easy refresh later
            _grdSearchData = new grdSearchData { DataSource = inventoryOrderStatuses/*searchResults*/, SearchCriteria = searchCriteria, SearchText = searchText };
        }

        private void RefreshSearchResults()
        {
            // force a refresh of cached search data if there is any
            var d = _grdSearchData;
            if (d != null)
                GetSearchResults(d.SearchCriteria, d.SearchText);

            // rebind the grid
            grdSearch.Rebind();
        }
        #endregion

        #region data "finders"
        private Boolean _findSelectColumn(TableCell cell)
        {
            var c = (CheckBox)cell.Controls[0];
            return c.Checked;
        }

        private Boolean findSelectColumn(Telerik.Web.UI.GridDataItem gridDataItem)
        {
            TableCell cell = gridDataItem["ClientSelectColumn"];
            return _findSelectColumn(cell);
        }

        private Boolean findSelectColumn(GridDataItem gridDataItem)
        {
            TableCell cell = gridDataItem["ClientSelectColumn"];
            return _findSelectColumn(cell);
        }

        private string _findProductName(TableCell cell)
        {
            var n = cell.Text;
            return (n != null) ? n : string.Empty;
        }

        private string findProductName(Telerik.Web.UI.GridDataItem gridDataItem)
        {
            return _findProductName(gridDataItem["productname"]);
        }

        private string findProductName(GridDataItem gridDataItem)
        {
            return _findProductName(gridDataItem["productname"]);
        }

        private Int32 _findProductInventoryID(TableCell cell)
        {
            var id = cell.Text;
            return (id != null) ? Int32.Parse(id) : 0;
        }

		private string findMCName(GridDataItem gridDataItem)
		{
			var productName = (string)gridDataItem["mcname"].Text;
			if (productName != null)
			{
				return productName;
			}
			else
			{
				return "";
			}

		}

		private string findProductCode(Telerik.Web.UI.GridDataItem gridDataItem)
		{
			var productName = (string)gridDataItem["code"].Text;
			if (productName != null)
			{
				return productName;
			}
			else
			{
				return "";
			}
		}

		private string findProductCode(GridDataItem gridDataItem)
		{
			var productName = (string)gridDataItem["code"].Text;
			if (productName != null)
			{
				return productName;
			}
			else
			{
				return "";
			}
		}

        private Int32 findProductInventoryID(Telerik.Web.UI.GridDataItem gridDataItem)
        {
            return _findProductInventoryID(gridDataItem["productinventoryid"]);
        }

        private Int32 findProductInventoryID(GridDataItem gridDataItem)
        {
            return _findProductInventoryID(gridDataItem["productinventoryid"]);
        }

        private Int32 _findPracticeCatalogProductID(TableCell cell)
        {
            var id = cell.Text;
            return (id != null) ? Int32.Parse(id) : 0;
        }

		private Int32 findReplacementPracticeCatalogProductID(GridDataItem gridDataItem)
		{
			var PracticeID = (string)gridDataItem["newpracticecatalogproductid"].Text;
			if (PracticeID != null)
			{
				Int32 practiceCatalogProductID = 0;
				if (Int32.TryParse(PracticeID, out practiceCatalogProductID))
					return practiceCatalogProductID;
				return findPracticeCatalogProductID(gridDataItem);
			}
			else
			{
				return 0;
			}
		}

		private Int32 findReplacementPracticeCatalogProductID(Telerik.Web.UI.GridDataItem gridDataItem)
		{
			var PracticeID = (string)gridDataItem["newpracticecatalogproductid"].Text;
			if (PracticeID != null)
			{
				Int32 practiceCatalogProductID = 0;
				if (Int32.TryParse(PracticeID, out practiceCatalogProductID))
					return practiceCatalogProductID;
				return findPracticeCatalogProductID(gridDataItem);
			}
			else
			{
				return 0;
			}
		}

		private Int32 findNewMasterCatalogProductID(GridDataItem gridDataItem)
		{
			var mcpID = gridDataItem.OwnerTableView.GetColumnSafe("newmastercatalogproductid");
			if (mcpID != null)
			{
				var masterCatalogProductID = (string)gridDataItem["newmastercatalogproductid"].Text;
				if (masterCatalogProductID != null)
				{
					return Int32.Parse(masterCatalogProductID);
					//Response.Write(gridDataItem.ItemIndex.ToString() + " is: " + PracticeID.ToString() + "<br />");
				}
				else
				{
					return 0;
				}
			}
			return -1;
		}

		private string findReplacementProductName(GridDataItem gridDataItem)
		{
			var newProductName = gridDataItem["NewProductName"];
			if (newProductName != null && newProductName.Text.Replace("&nbsp;", "") != string.Empty)
				return newProductName.Text;
			return null;
		}

		private string findReplacementProductName(Telerik.Web.UI.GridDataItem gridDataItem)
		{
			var newProductName = gridDataItem["NewProductName"];
			if (newProductName != null && newProductName.Text.Replace("&nbsp;", "") != string.Empty)
				return newProductName.Text;
			return null;
		}

		private string findReplacementProductCode(GridDataItem gridDataItem)
		{
			var newProductCode = gridDataItem["NewProductCode"];
			if (newProductCode != null && newProductCode.Text.Replace("&nbsp;", "") != string.Empty)
				return newProductCode.Text;
			return null;
		}

		private string findReplacementProductCode(Telerik.Web.UI.GridDataItem gridDataItem)
		{
			var newProductCode = gridDataItem["NewProductCode"];
			if (newProductCode != null && newProductCode.Text.Replace("&nbsp;", "") != string.Empty)
				return newProductCode.Text;
			return null;
		}

        private Int32 findPracticeCatalogProductID(Telerik.Web.UI.GridDataItem gridDataItem)
        {
            return _findPracticeCatalogProductID(gridDataItem["practicecatalogproductid"]);
        }

        private Int32 findPracticeCatalogProductID(GridDataItem gridDataItem)
        {
            return _findPracticeCatalogProductID(gridDataItem["practicecatalogproductid"]);
        }

        private Int32 _findMasterCatalogProductID(TableCell cell)
        {
            var id = cell.Text;
            return (id != null) ? Int32.Parse(id) : 0;
        }

		private Int32 findNewMasterCatalogProductID(Telerik.Web.UI.GridDataItem gridDataItem)
		{
			var mcpID = gridDataItem.OwnerTableView.GetColumnSafe("newmastercatalogproductid");
			if (mcpID != null)
			{
				var masterCatalogProductID = (string)gridDataItem["newmastercatalogproductid"].Text;
				if (masterCatalogProductID != null)
				{
					return Int32.Parse(masterCatalogProductID);
					//Response.Write(gridDataItem.ItemIndex.ToString() + " is: " + PracticeID.ToString() + "<br />");
				}
				else
				{
					return 0;
				}
			}
			return -1;
		}

        private Int32 findMasterCatalogProductID(Telerik.Web.UI.GridDataItem gridDataItem)
        {
            var mcpID = gridDataItem.OwnerTableView.GetColumnSafe("mastercatalogproductid");
            return (mcpID != null) ? _findMasterCatalogProductID(gridDataItem["mastercatalogproductid"]) : -1;
        }

        private Int32 findMasterCatalogProductID(GridDataItem gridDataItem)
        {
            var mcpID = gridDataItem.OwnerTableView.GetColumnSafe("mastercatalogproductid");
            return (mcpID != null) ? _findMasterCatalogProductID(gridDataItem["mastercatalogproductid"]) : -1;
        }

        private short findReorderQuantity(WebControl gridDataItem)
        {
            var nTextBox = gridDataItem.FindControl("txtSuggReorderLevel") as Telerik.Web.UI.RadNumericTextBox;

            return (nTextBox != null) ? short.Parse(nTextBox.Value.ToString()) : (short)0;
        }
        #endregion

        private void InitializeAjaxSettings()
        {
            var radAjaxManager = Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page);
            if (radAjaxManager != null)
            {
                RadToolbarToggleButton rtbFilter = (RadToolbarToggleButton)RadToolbar1.FindControl("rtbFilter");
                RadAjaxManager1.AjaxSettings.AddAjaxSetting(rtbFilter, grdSearch, LoadingPanel1);
                RadAjaxManager1.AjaxSettings.AddAjaxSetting(RadToolbar1, grdSearch, LoadingPanel1);
                RadAjaxManager1.AjaxSettings.AddAjaxSetting(RadToolbar1, btnAddItemsSearch);
                RadAjaxManager1.AjaxSettings.AddAjaxSetting(RadToolbar1, RadToolbar1);

                RadAjaxManager1.AjaxSettings.AddAjaxSetting(btnAddItemsSearch,
                    (Master as MasterPages.SiteMaster).SiteCart.RadButtonItemsInCartButton, LoadingPanel1);
                RadAjaxManager1.AjaxSettings.AddAjaxSetting(btnAddItemsReOrderGrid,
                    (Master as MasterPages.SiteMaster).SiteCart.RadButtonItemsInCartButton, LoadingPanel1);
                RadAjaxManager1.AjaxSettings.AddAjaxSetting(lblStatus2, grdSearch);
                RadAjaxManager1.AjaxSettings.AddAjaxSetting(lblBrowseDispense, grdSearch);
            }
        }

		private bool SetColumnValue(object dest, DataRow row, string column)
		{
			if (!row.Table.Columns.Contains(column))
				row.Table.Columns.Add(column);

			bool status = false;
			Type type = dest.GetType();
			object source = row[column];
			if (source != null && source != DBNull.Value)
			{
				PropertyInfo prop = type.GetProperty(column);
				if (prop != null)
				{
					prop.SetValue(dest, source, null);
					status = true;
				}
			}
			return status;
		}

        private DataSet SearchForItemsInventory(int practiceLocationID, string searchCriteria, string searchText)
        {
            _UIHelper.NumChildRecs = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_SearchforItemsInInventory");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);
            db.AddInParameter(dbCommand, "SearchCriteria", DbType.String, searchCriteria);
            db.AddInParameter(dbCommand, "SearchText", DbType.String, searchText);

            DataSet ds = db.ExecuteDataSet(dbCommand);

            _UIHelper.NumChildRecs = ds.Tables[0].Rows.Count;

            return ds;
        }

        private void LoopHierarchyRecursive(GridTableView gridTableView)
        {
            ShoppingCartItemID = 0;
            foreach (GridDataItem gridDataItem in gridTableView.Items)
            {
                if (findSelectColumn(gridDataItem))
                {
                    Int16 ReOrderQuantity = Convert.ToInt16(findReorderQuantity(gridDataItem));

                    if (ReOrderQuantity <= 0)
                    {
                        ReOrderQuantity = 0;
                    }
                    else
                    {
                        try
                        {
							ShoppingCartItemID = UpdateGrdInventoryData(Convert.ToInt16(Session["PracticeLocationID"]),
								GetReplacementProduct(gridDataItem, lblStatusReplaced, lblStatusReplacedText), 1, ReOrderQuantity);
                            productsAdded += ReOrderQuantity;
                        }
                        catch (System.Data.SqlClient.SqlException ex)
                        {
                            if (ex.Class == 17 && ex.Number == 50000)
                            {
                                string x = ex.Message;
								lblStatus.Text = "No products added to cart (Custom Braces must be the only item in the cart)";
                                lblStatus.CssClass = "ErrorMsg";
                                lblStatus.Visible = true;
                                return;
                            }
							if (ex.Class == 18 && ex.Number == 50000)
							{
								string x = ex.Message;
								if (ex.Errors.Count > 0)
									x = ex.Errors[0].Message;
								lblStatus.Text = x;
								lblStatus.CssClass = "ErrorMsg";
								lblStatus.Visible = true;
								return;
							}
						}
					}

                    // Set Status indicator
                    if (productsAdded == 0)
                    {
                        //string StatusText = string.Concat("Number of products added to cart: ", productsAdded.ToString(), " No products added due to QOH higher than reorder level. Please visit the Inventory page to add products.");
                        string StatusText = "No products added to cart";
                        lblStatus.CssClass = "ErrorMsg";
                        lblStatus.Text = StatusText;
                    }
                    else
                    {
                        string StatusText = string.Concat("Number of products added to cart: ", productsAdded.ToString());
                        lblStatus.CssClass = "SuccessMsg";
                        lblStatus.Text = StatusText;
                    }
                    lblStatus.Visible = true;
                }
            }
        }

		private int GetReplacementProduct(GridDataItem gridDataItem, Label replaceStatusLabel, Label replaceStatusTextLabel)
		{
			string replacementProductCode = findReplacementProductCode(gridDataItem);
			string replacementProductName = findReplacementProductName(gridDataItem);
			if (replacementProductName != null)
			{
				string productCode = findProductCode(gridDataItem);
				string productName = findMCName(gridDataItem);
				int productCount = findReorderQuantity(gridDataItem);
				DisplayReplacementWarning(productCode, productName, productCount, replacementProductCode, replacementProductName, replaceStatusLabel, replaceStatusTextLabel);
				return findReplacementPracticeCatalogProductID(gridDataItem);
			}
			return findPracticeCatalogProductID(gridDataItem);
		}

		private int GetReplacementProduct(Telerik.Web.UI.GridDataItem gridDataItem, Label replaceStatusLabel, Label replaceStatusTextLabel)
		{
			string replacementProductCode = findReplacementProductCode(gridDataItem);
			string replacementProductName = findReplacementProductName(gridDataItem);
			if (replacementProductName != null)
			{
				string productCode = findProductCode(gridDataItem);
				string productName = findProductName(gridDataItem);
				int productCount = findReorderQuantity(gridDataItem);
				DisplayReplacementWarning(productCode, productName, productCount, replacementProductCode, replacementProductName, replaceStatusLabel, replaceStatusTextLabel);
				return findReplacementPracticeCatalogProductID(gridDataItem);
			}
			return findPracticeCatalogProductID(gridDataItem);
		}

		private void DisplayReplacementWarning(string productCode, string productName, int productCount, string replacementProductCode, string replacementProductName, Label replaceStatusLabel, Label replaceStatusTextLabel)
		{
			string lineStart = replaceStatusTextLabel.Text != string.Empty ? "<br>" : "NOTE: Where available, the following products will be replaced in the CART:<br><br>";// string.Empty;
			replaceStatusTextLabel.Text += lineStart + "(" + productCount.ToString() + ") [{" + productCode + "} " + productName + "] product(s) replaced with [{" + replacementProductCode + "} " + replacementProductName + "]";
			replaceStatusLabel.Text = "<span></span>";
			replaceStatusTextLabel.Visible = true;
			replaceStatusLabel.Visible = true;
		}

        private void LoopHierarchyRecursivegrdSearch(Telerik.Web.UI.GridTableView gridTableView)
        {
			lblStatusReplaced.Text = string.Empty;
			lblStatusReplacedText.Text = string.Empty;
			lblStatusReplaced2.Text = string.Empty;
			lblStatusReplacedText2.Text = string.Empty;
			ShoppingCartItemID = 0;
            foreach (Telerik.Web.UI.GridDataItem gridDataItem in gridTableView.Items)
            {
                if (findSelectColumn(gridDataItem))
                {
                    Int16 reorderQuantity = Convert.ToInt16(findReorderQuantity(gridDataItem));
                    if (reorderQuantity <= 0)
                    {
                        reorderQuantity = 0;
                    }
                    else
                    {
                        try
                        {
                            string productName = findProductName(gridDataItem);

                            int productInventoryID = findProductInventoryID(gridDataItem);
							int practiceCatalogProductID = GetReplacementProduct(gridDataItem, lblStatusReplaced2, lblStatusReplacedText2);//findPracticeCatalogProductID(gridDataItem);

                            if (practiceCatalogProductID == 0)
                            {
                                //the item hasn't been added to the practice Catalog yet
								int masterCatalogProductID = findNewMasterCatalogProductID(gridDataItem);
								if (masterCatalogProductID <= 0)
									masterCatalogProductID = findMasterCatalogProductID(gridDataItem);
                                if (masterCatalogProductID > 0)
                                {
                                    //we got a masterCatalogID so add to the Practice Catalog
                                    practiceCatalogProductID =
                                        UpdatePracticeCatalog(Convert.ToInt16(Session["PracticeID"]),
                                            masterCatalogProductID, Convert.ToInt16(Session["UserID"]));
                                }
                            }

                            if (productInventoryID == 0)
                            {
                                //we don't have a productInventoryID so add to product inventory
                                TransferToProductInventory(Convert.ToInt16(Session["PracticeLocationID"]),
                                    practiceCatalogProductID, Convert.ToInt16(Session["UserID"]));
                            }

                            if (practiceCatalogProductID > 0)
                            {
								ShoppingCartItemID = UpdateGrdInventoryData(Convert.ToInt16(Session["PracticeLocationID"]),
                                    practiceCatalogProductID, 1, reorderQuantity);
                                if (productName.ToLower().Contains("boa"))
                                {
                                    string userName = Context.User.Identity.Name;
                                    int eUserID = (Session["UserID"] != null)
                                        ? Convert.ToInt32(Session["UserID"].ToString())
                                        : -1;
                                    int ePracticeID = (Session["PracticeID"] != null)
                                        ? Convert.ToInt32(Session["PracticeID"].ToString())
                                        : -1;
                                    string ePracticeName = (Session["PracticeName"] != null)
                                        ? Session["PracticeName"].ToString()
                                        : "Not Set";

                                    BLL.Practice.UserClick userClick = new BLL.Practice.UserClick(eUserID, userName,
                                        ePracticeID, 0, "Home", "", "RadDock", "Boa Product added to cart", "BOA Line",
                                        practiceCatalogProductID.ToString(), "", "", "");
                                }
                                productsAdded += reorderQuantity;
                            }
                        }
                        catch (System.Data.SqlClient.SqlException ex)
                        {
                            if (ex.Class == 17 && ex.Number == 50000)
                            {
                                string x = ex.Message;
                                lblStatus2.Text = "No products added to cart (Custom Braces must be the only item in the cart)";
                                lblStatus2.CssClass = "ErrorMsg";
                                lblStatus2.Visible = true;
                                return;
                            }
							if (ex.Class == 18 && ex.Number == 50000)
                            {
								string x = ex.Message;
								if (ex.Errors.Count > 0)
									x = ex.Errors[0].Message;
								lblStatus2.Text = x;
                                lblStatus2.CssClass = "ErrorMsg";
                                lblStatus2.Visible = true;
                                return;
                            }
						}
					}

                    // Set Status indicator
                    if (productsAdded == 0)
                    {
                        string StatusText = "No products added to cart";
                        lblStatus2.CssClass = "ErrorMsg";
                        lblStatus2.Text = StatusText;
                    }
                    else
                    {
                        string StatusText = string.Concat("Number of products added to cart: ", productsAdded.ToString());
                        lblStatus2.CssClass = "SuccessMsg";
                        lblStatus2.Text = StatusText;
                    }
                    lblStatus2.Visible = true;
                }
            }
        }

        private Int32 UpdateGrdInventoryData(Int16 practiceLocationID, Int32 practiceCatalogProductID, Int16 UserID,
            Int16 Quantity)
        {
            return DAL.PracticeLocation.Inventory.UpdateInventoryData(practiceLocationID, practiceCatalogProductID,
                UserID, Quantity);
        }

        private Int32 UpdatePracticeCatalog(Int32 PracticeID, Int32 MasterCatalogProductID, Int32 UserID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand =
                db.GetStoredProcCommand("dbo.usp_PracticeCatalogProduct_Insert_By_MasterCatalogProductID");

            db.AddInParameter(dbCommand, "PracticeID", DbType.Int32, PracticeID);
            db.AddInParameter(dbCommand, "MasterCatalogProductID", DbType.Int32, MasterCatalogProductID);
            db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);
            db.AddOutParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, 4);

			db.AddInParameter(dbCommand, "TransferModifiers", DbType.Boolean, 1); // This stored proc requires this parameter which was never supplied before!

            int SQLReturn = db.ExecuteNonQuery(dbCommand);

            Int32 return_value = Convert.ToInt32(db.GetParameterValue(dbCommand, "PracticeCatalogProductID"));
            return return_value;
        }

        private Int32 TransferToProductInventory(Int32 PracticeLocationID, Int32 PracticeCatalogProductID, Int32 UserID)
        {
            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_TransferFromPracticeCatalogtoProductInventory");

            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);
            db.AddInParameter(dbCommand, "PracticeCatalogProductID", DbType.Int32, PracticeCatalogProductID);
            db.AddInParameter(dbCommand, "UserID", DbType.Int32, UserID);

            Int32 SQLReturn = db.ExecuteNonQuery(dbCommand);

            return SQLReturn;
        }
    }
}



