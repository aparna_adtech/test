﻿(function() {
    angular.module('invoiceApp')
        .directive('currencyFormatter', ['$filter','$window', currencyFormatter]);

    function currencyFormatter($filter,$window) {
            var currencyFilter = $filter('currency');
        
            return {
                restrict: 'A',
                require: '^ngModel',
                scope: {currencyValue:'='},
                link: function (scope, elem, attrs, ctrl) {
                    var rawElem = elem[0];
                    if (!ctrl) return;
            
                    elem.on('focus', function() { updateView(true); });
                    elem.on('blur', function() { updateView(false); });
            
                        
                    function updateView(hasFocus) {
                        if (!ctrl.$modelValue) { return; }
                        var displayValue = hasFocus
                            ? scope.currencyValue
                            : currencyFilter(scope.currencyValue);
                        rawElem.value = displayValue;
                    }
                        
                    scope.$watch(function () { return scope.currencyValue }, function (newvalue, oldvalue) {
                        if (!newvalue) {
                            return;
                        }
                        var reg = /^\d*(\.\d{0,2})?$/;
                        var isValid = reg.test(newvalue);
                        if(isValid===false){
                            scope.currencyValue = oldvalue;
                        }
                        updateView(rawElem === $window.document.activeElement);
                    },true); 
                }
            }

    }
})()

//    .directive('currencyFormatter',function($filter,$window){
//    var currencyFilter = $filter('currency');
//
//    return {
//        restrict: 'A',
//        require: '^ngModel',
//        scope: {currencyValue:'='},
//        link: function (scope, elem, attrs, ctrl) {
//            var rawElem = elem[0];
//            if (!ctrl) return;
//    
//            elem.on('focus', function() { updateView(true); });
//            elem.on('blur', function() { updateView(false); });
//    
//                
//            function updateView(hasFocus) {
//                if (!ctrl.$modelValue) { return; }
//                var displayValue = hasFocus
//                    ? scope.currencyValue
//                    : currencyFilter(scope.currencyValue);
//                rawElem.value = displayValue;
//            }
//                
//            scope.$watch(function () { return scope.currencyValue }, function (newvalue, oldvalue) {
//                if (!newvalue) {
//                    return;
//                }
//                var reg = /^\d*(\.\d{0,2})?$/;
//                var isValid = reg.test(newvalue);
//                if(isValid===false){
//                    scope.currencyValue = oldvalue;
//                }
//                updateView(rawElem === $window.document.activeElement);
//            },true); 
//        }
//    }
//})
//
//
