﻿(function() {
    angular.module('invoiceApp')
        .filter("supplier", supplierFilter);

    function supplierFilter() {
        return filter;

        function filter(items, supplier) {
            var filtered = [];
            angular.forEach(items, function(item) {
                if (supplier === undefined || supplier === null || supplier.Id === -1 || supplier.Id === item.Supplier.Id) {
                    filtered.push(item);
                }
            });
            return filtered;
        };
    }
})()