﻿(function() {
    angular.module('invoiceApp')
        .directive('datepicker1', ['$parse', datepicker1]);

    function datepicker1($parse) {
       return {
                    restrict: "A",
                    link: function(scope, element, attrs) {
                        //using $parse instead of scope[attrs.datepicker] for cases
                        //where attrs.datepicker is 'foo.bar.lol'
                        var dateString = $parse(attrs.datepicker1);
                        $(element).datepicker({
                            onSelect: function(dateText, inst) {
                                scope.$apply(function() {
                                    dateString.assign(scope, dateText);
                                });
                            }
                        });
                    }
                }
    }
})()