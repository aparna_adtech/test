﻿(function() {
    angular.module('invoiceApp')
        .filter("yesNo", yesNoFilter);

    function yesNoFilter() {

        return function(input) {
                return input ? 'Yes' : 'No';
            }
        };
})()