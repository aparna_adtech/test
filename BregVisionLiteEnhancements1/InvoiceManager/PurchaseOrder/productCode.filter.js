﻿(function() {
    angular.module('invoiceApp')
        .filter("productCode", productCodeFilter);

    function productCodeFilter() {
        return filter;

        function filter(purchaseOrders, productCode) {
            if (productCode === undefined || productCode === null || productCode === '') {
                return purchaseOrders;
            }
            var filtered = [];
            angular.forEach(purchaseOrders, function(purchaseOrder) {
                var isFound = false;
                angular.forEach(purchaseOrder.LineItems, function(poLineItem) {
                    if (!isFound) {
                        if (poLineItem.ProductCode.toLowerCase().indexOf(productCode.toLowerCase()) !== -1) {
                            filtered.push(purchaseOrder);
                            isFound = true;
                        }
                    }
                });
            });
            return filtered;
        };
    }
})()