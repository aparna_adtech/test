﻿(function() {
    angular.module('invoiceApp')
        .filter("productName", productNameFilter);

    function productNameFilter() {
        return filter;

        function filter(purchaseOrders, productName) {
            if (productName === undefined || productName === null || productName === '') {
                return purchaseOrders;
            }
            var filtered = [];
            angular.forEach(purchaseOrders, function(purchaseOrder) {
                var isFound = false;
                angular.forEach(purchaseOrder.LineItems, function(poLineItem) {
                    if (!isFound) {
                        if (poLineItem.Name.toLowerCase().indexOf(productName.toLowerCase()) !== -1) {
                            filtered.push(purchaseOrder);
                            isFound = true;
                        }
                    }
                });
            });
            return filtered;
        };
    }
})()