﻿(function() {
    angular.module('invoiceApp')
        .filter("exportInvoicesFilter",exportInvoices);

    function exportInvoices($filter) {
        return theFilter;

        function theFilter(collection, invCtrl) {
            var filterFilter = $filter('filter');
            var filtered = filterFilter(collection, { "Number": invCtrl.SearchInvoice.NumberSearch });

            var exportedFilter = $filter('exported');
            filtered = exportedFilter(filtered, invCtrl.includeExportedInvoices);

            var supplierFilter = $filter('supplier');
            filtered = supplierFilter(filtered, invCtrl.SearchInvoice.SupplierSearch);

            var dateRangeFilter = $filter('dateRange');
            filtered = dateRangeFilter(filtered, invCtrl.dateFromInvoicesDate.getTime(), invCtrl.dateToInvoicesDate.getTime());

            var excludedInvoices = $.grep(collection, function (invoice) { return $.inArray(invoice, filtered) == -1 });

            angular.forEach(excludedInvoices, function (invoice) {
                invoice.IsSelected = false;
            });



            return filtered;
        }

    };
})()