﻿(function() {
    angular.module('invoiceApp')
        .controller('exportController', exportController);

    function exportController($scope, $http, $window,$filter, invoiceExportFactory) {
        var vm = this;

        vm.SelectAll = false;

        vm.selectInvoice = function(selectedInvoice) {
            selectedInvoice.IsSelected = !selectedInvoice.IsSelected;
        };

        vm.toggleSelectAll = function (invoiceController) {
            var filteredInvoices = vm.getFilterInvoices(invoiceController);


            angular.forEach(filteredInvoices, function (invoice, key) {
                invoice.IsSelected = vm.SelectAll;
            });
        };

        vm.getFilterInvoices = function(invoiceController) {
            var filteredInvoices = [];

            var exportInvoicesFilter = $filter("exportInvoicesFilter");
            filteredInvoices = exportInvoicesFilter(invoiceController.Invoices, invoiceController);
            return filteredInvoices;

        }

        vm.exportSelectedInvoices = function (invoiceController) {
            var filteredInvoices = vm.getFilterInvoices(invoiceController);

            var selectedInvoices = $.grep(filteredInvoices, function(inv) { return inv.IsSelected === true; });
            $scope.working = true;
            $scope.answered = true;
            invoiceExportFactory.exportInvoices(selectedInvoices,Token, PRACTICE_ID)
                .success(function (data, status, headers, config) {
                vm.applyExportToLocalCollection(selectedInvoices);
                $window.open(invoiceExportFactory.getInvoiceExportPath(data));
                $scope.correctAnswer = (data === true);
                $scope.working = false;
            }).error(function(data, status, headers, config) {
                $scope.title = "Oops... something went wrong";
                $scope.working = false;
            });
        };

        vm.applyExportToLocalCollection = function(exportedInvoices) {
            angular.forEach(exportedInvoices, function (invoice, key) {
                invoice.IsSelected = false;
                invoice.IsExported = true;
            });
        }


        vm.isExportEnabled = function(invoiceController) {
            var found = false;
            angular.forEach(invoiceController.Invoices, function (invoice, key) {
                if (found === false) {
                    if (invoice.IsSelected === true) {
                        found = true;
                    }
                }
            });
            return found;
        }
    }
})()