﻿(function() {
    angular.module('invoiceApp')
        .factory('invoiceExportFactory', invoiceExportFactory);

    function invoiceExportFactory($http) {
        function getExportInvoices(practiceId, token) {
            $http.defaults.headers.common.token = token;
            return $http.get("/BregRestfulService/api/invoiceExport/" + practiceId);
        }

        function exportInvoices(selectedInvoices, token, practiceId) {
            $http.defaults.headers.common.token = token;
            return $http.put('/BregRestfulService/api/invoiceExport/' + practiceId, selectedInvoices);
        }

        function getInvoiceExportPath(fileName) {
            return '/BregRestfulService/InvoiceExportFiles/' + fileName;
        }

        return {
            getExportInvoices: getExportInvoices,
            exportInvoices: exportInvoices,
            getInvoiceExportPath : getInvoiceExportPath
        }
    }
})()