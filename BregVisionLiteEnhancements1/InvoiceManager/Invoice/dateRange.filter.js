﻿(function() {
    angular.module('invoiceApp')
        .filter("dateRange", dateRange);

    function dateRange() {
        return filter;

        function filter(collection, dateFrom, dateTo) {
            var filtered = [];
            angular.forEach(collection, function(item) {
                var invDate = new Date(item.DateAsJSON).setHours(0, 0, 0, 0);
                var dateFromFloor = new Date(dateFrom).setHours(0, 0, 0, 0);
                var dateToFloor = new Date(dateTo).setHours(0, 0, 0, 0);
                if ((isNaN(dateFromFloor) || invDate >= dateFromFloor) &&
                (isNaN(dateToFloor) || invDate <= dateToFloor)) {
                    filtered.push(item);
                }
            });
            return filtered;
        };
    }
})()