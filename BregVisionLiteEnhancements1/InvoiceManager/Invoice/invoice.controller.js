﻿(function() {
            angular.module('invoiceApp')
                .controller('invoiceController', invoiceController);

            function invoiceController($scope, $http, $window, invoiceFactory) {
                var self = this;
                //This is code for Expand and collapse Function//
                $("#openpsr").css('display', 'none');
                $("#vsractive").css('display', 'none');
                $scope.close = function() {
                    $window.IMGR_hide_invoice_search_results();
                };
                $scope.closepsr = function() {
                    $window.IMGR_hide_po_search_results();
                    $window.IMGR_hide_current_po_overlay();
                };
                $scope.openpsr = function() {
                    $("#poSearchResult").animate({
                        width: "toggle"
                    }, 10, function() {
                        $("#psrhead").css('display', 'block');
                        $("#poSearchResult").css('display', 'block');
                        $("#purchaseOrderSearch").css('display', 'block');
                        $("#openpsr").css('display', 'none');
                    });
                }; //End Expand and collapse Function//
                self.edittingShippingCharges = false;
                self.blurringShippingCharges = false;

                self.showExportInvoices = false;
                self.includeExportedInvoices = false;

                self.isInvoiceSmartSearchEnabled = true;
                self.isPurchaseOrderSmartSearchEnabled = true;


                self.getFormattedDate = function(date) {
                    var month = date.getMonth() + 1;
                    return month + "/" + date.getDate() + "/" + date.getFullYear();
                };
                self.dateFromInvoices = '';
                self.dateFromInvoicesDate = null;

                self.dateToInvoices = '';
                self.dateToInvoicesDate = null;

                self.dateFromPOs = "";
                self.dateFromPOsDate = new Date();
                self.dateToPOs = "";
                self.dateToPOsDate = new Date();
                self.dateFromPOsServerString = "";


                $scope.$watch(function() { return self.CurrentInvoice && self.CurrentInvoice.Supplier; }, function() {
                    if (self.CurrentPurchaseOrder && self.CurrentInvoice) {
                        if (self.CurrentPurchaseOrder.Supplier.Id !== self.CurrentInvoice.Supplier.Id) {
                            self.CurrentPurchaseOrder = null;
                        }
                    }
                }, true);

                $scope.$watch(function () { return self.dateFromInvoices; }, function () {
                    if (self.isInvoiceSmartSearchEnabled) {
                        self.dateFromInvoicesDate = new Date(self.dateFromInvoices);
                    }
                }, true);

                $scope.$watch(function() { return self.dateToInvoices; }, function() {
                    if (self.isInvoiceSmartSearchEnabled) {
                        self.dateToInvoicesDate = new Date(self.dateToInvoices);
                    }
                }, true);

                $scope.$watch(function() { return self.dateFromPOs; }, function() {
                    if ($scope.poSearchForm.poFromDate.$error.pattern) {
                        return;
                    }
                    if (self.isPurchaseOrderSmartSearchEnabled === true) {
                        self.dateFromPOsDate = new Date(self.dateFromPOs);
                    }
                    //	                 self.fromDatePosChanged();
                }, true);

                $scope.$watch(function() { return self.dateToPOs; }, function() {
                    if (self.isPurchaseOrderSmartSearchEnabled === true) {
                        self.dateToPOsDate = new Date(self.dateToPOs);
                    }
                }, true);

                $scope.$watch(function() {
                    if (!self.CurrentInvoice || self.CurrentInvoice === null) {
                        return undefined;
                    }
                    return self.CurrentInvoice.DateString;
                }, function(newValue, oldValue) {
                    if (!self.CurrentInvoice || self.CurrentInvoice === null) {
                        return;
                    }
                    if (newValue === oldValue) {
                        return;
                    }

                    if (self.CurrentInvoice.Date !== null && self.CurrentInvoice.Date.getTime() === new Date(newValue).getTime()) {
                        return;
                    }


                    self.CurrentInvoice.Date = new Date(newValue);
                    $scope.activeInvoice.$setDirty();
                }, true);


                $scope.NewInvoice = {
                    Number: '',
                    Supplier: null,
                    Date: null,
                    DateString: ''
                };

                self.SearchInvoice = {
                    NumberDisplay: '',
                    NumberSearch: '',
                    SupplierDisplay: null,
                    SupplierSearch: null,
                    Tax: null,
                    ShippingCharges: 0,
                    Total: null
                };
                self.CurrentInvoice = null;

                self.CurrentInvoiceDateString = '';
                this.CurrentPurchaseOrder = null;

                this.SearchPurchaseOrder = {
                    PONumberSearch: '',
                    PONumberDisplay: '',
                    Supplier: null,
                    DateAsJSON: null,
                    LineItems: null,
                    ProductNameSearch: null,
                    ProductNameDisplay: null,
                    ProductCodeSearch: null,
                    ProductCodeDisplay: null
                };

                self.SearchInvoices = function() {
                    self.SearchInvoice.SupplierSearch = self.SearchInvoice.SupplierDisplay;
                    self.dateFromInvoicesDate = new Date(self.dateFromInvoices);
                    self.dateToInvoicesDate = new Date(self.dateToInvoices);
                    self.SearchInvoice.NumberSearch = self.SearchInvoice.NumberDisplay;
                };
                self.InvoiceNumberChanged = function() {
                    if (self.isInvoiceSmartSearchEnabled === true) {
                        self.SearchInvoice.NumberSearch = self.SearchInvoice.NumberDisplay;
                    }
                };
                self.InvoiceSupplierChanged = function() {
                    if (self.isInvoiceSmartSearchEnabled === true) {
                        self.SearchInvoice.SupplierSearch = self.SearchInvoice.SupplierDisplay;
                    }
                };
                self.SearchPOs = function() {
                    self.SearchPurchaseOrder.PONumberSearch = self.SearchPurchaseOrder.PONumberDisplay;
                    self.dateFromPOsDate = new Date(self.dateFromPOs);
                    self.dateToPOsDate = new Date(self.dateToPOs);
                    self.SearchPurchaseOrder.ProductNameSearch = self.SearchPurchaseOrder.ProductNameDisplay;
                    self.SearchPurchaseOrder.ProductCodeSearch = self.SearchPurchaseOrder.ProductCodeDisplay;
                };
                self.PONumberChanged = function() {
                    if (self.isPurchaseOrderSmartSearchEnabled === true) {
                        self.SearchPurchaseOrder.PONumberSearch = self.SearchPurchaseOrder.PONumberDisplay;
                    }
                };
                self.ProductNameChanged = function() {
                    if (self.isPurchaseOrderSmartSearchEnabled === true) {
                        self.SearchPurchaseOrder.ProductNameSearch = self.SearchPurchaseOrder.ProductNameDisplay;
                    }
                };
                self.ProductCodeChanged = function() {
                    if (self.isPurchaseOrderSmartSearchEnabled === true) {
                        self.SearchPurchaseOrder.ProductCodeSearch = self.SearchPurchaseOrder.ProductCodeDisplay;
                    }
                };
                self.Invoices = [];

                self.InvoiceSearchSuppliers = [
                    { Id: -1, Name: 'Select Supplier' }
                ];

                self.InvoiceCreateSuppliers = [
                ];

                self.PurchaseOrderSuppliers = [
                    { Id: -1, Name: 'Select Supplier' }
                ];

                self.totalSupplierOrderLineItemInvoicedCount = function(lineItem) {
                    var sum = 0;
                    angular.forEach(lineItem.Invoices, function(invoice, key) {
                        sum += invoice.Quantity;
                    });
                    return sum;
                };
                self.getCurrentInvoiceExtendedPrice = function() {
                    if (self.CurrentInvoice == null) {
                        return 0;
                    }

                    var extendedPrice = 0;
                    angular.forEach(self.CurrentInvoice.InvoiceLineItems, function(lineItem, key) {
                        extendedPrice += parseInt(lineItem.QuantityInvoiced) * parseFloat(lineItem.WholesaleCost);
                    });
                    return extendedPrice;
                };

                self.getCurrentInvoiceTotal = function() {
                    if (self.CurrentInvoice == null) {
                        return '';
                    }
                    var total = 0;
                    if (self.CurrentInvoice.Tax && self.CurrentInvoice.Tax != null && self.CurrentInvoice.Tax != '') {
                        total = parseFloat(self.CurrentInvoice.Tax);
                    }
                    if (self.CurrentInvoice.ShippingCharges && self.CurrentInvoice.ShippingCharges != null && self.CurrentInvoice.ShippingCharges != '') {
                        total = total + parseFloat(self.CurrentInvoice.ShippingCharges);
                    }

                    total = total + self.getCurrentInvoiceExtendedPrice();

                    return total;
                };

                self.MatchingInvoiceExists = function(invoice) {
                    $scope.working = true;
                    $scope.answered = true;
                    var masterCatalogSupplierId = invoice.Supplier.MasterCatalogSupplierId;
                    if (masterCatalogSupplierId === null) {
                        masterCatalogSupplierId = -1;
                    }
                    var thirdPartySupplierId = invoice.Supplier.ThirdPartySupplierId;
                    if (thirdPartySupplierId === null) {
                        thirdPartySupplierId = -1;
                    }
                    $http.get("/BregRestfulService/api/invoice/" + PRACTICE_ID + "/" + invoice.Number + "/" + masterCatalogSupplierId + "/" + thirdPartySupplierId)
                        .success(function(data, status, headers, config) {
                            $scope.answered = false;
                            $scope.working = false;
                            if (data.length > 0) {
                                self.initiateInvoiceDuplication();
                            } else {
                                self.createNewInvoiceContinue();
                            };
                        }).error(function(data, status, headers, config) {
                            $scope.title = "Oops... something went wrong";
                            $scope.working = false;
                        });

                };

                self.createNewInvoiceClick = function () {
                    self.showExportInvoices = false;
                    if ($scope.activeInvoice.$dirty === true) {
                        self.showLoseInvoiceChangesConfirm = true;
                        return;
                    }
                    self.createNewInvoiceWithDuplicateCheck();
                };

                self.createNewInvoiceWithDuplicateCheck = function() {
                    var matchingInvoiceExists = self.MatchingInvoiceExists(self.NewInvoice);
                };

                self.createNewInvoiceContinue = function() {
                    var dateAsJSON = '';

                    self.NewInvoice.Date = null;

                    if (self.NewInvoice.DateString !== undefined && self.NewInvoice.DateString.trim() !== '') {
                        self.NewInvoice.Date = new Date(self.NewInvoice.DateString);
                    }
                    var isInvalidDate = self.NewInvoice.Date === null || isNaN(self.NewInvoice.Date);

                    if (!isInvalidDate) {
                        dateAsJSON = self.NewInvoice.Date.toISOString();
                    }

                    var newInvoice = {
                        Number: self.NewInvoice.Number,
                        Supplier: self.NewInvoice.Supplier,
                        Date: self.NewInvoice.Date,
                        DateAsJSON: dateAsJSON,
                        DateString: self.NewInvoice.DateString,
                        InvoiceLineItems: []
                    };
                    self.createInvoice(newInvoice);
                    self.Invoices.push(newInvoice);
                    self.CurrentInvoice = newInvoice;
                    self.NewInvoice.Number = '';
                    self.NewInvoice.Supplier = null;
                    self.NewInvoice.Date = null;
                    self.NewInvoice.DateString = '';
                    
                    $scope.activeInvoice.$setPristine();
                    self.hideInvoiceDuplicateConfirm();
                    $scope.openpsr();

                };
                self.createInvoice = function(newInvoice) {
                    $scope.working = true;
                    $scope.answered = true;

                    $http.post('/BregRestfulService/api/invoice/' + PRACTICE_ID, newInvoice).success(function(data, status, headers, config) {
                        newInvoice.Id = data;
                        $scope.activeInvoice.$setPristine();
                        $scope.working = false;
                    }).error(function(data, status, headers, config) {
                        $scope.title = "Oops... something went wrong";
                        $scope.working = false;
                    });
                };

                self.updateInvoice = function() {
                    self.CurrentInvoice.Date = new Date(self.CurrentInvoice.DateString);

                    $scope.working = true;
                    $scope.answered = true;

                    self.CurrentInvoice.Total = self.getCurrentInvoiceTotal();

                    $http.put('/BregRestfulService/api/invoice/', self.CurrentInvoice).success(function(data, status, headers, config) {
                        $scope.correctAnswer = (data === true);
                        $scope.activeInvoice.$setPristine();
                        $scope.working = false;
                    }).error(function(data, status, headers, config) {
                        $scope.title = "Oops... something went wrong";
                        $scope.working = false;
                    });
                };

                self.updateWholesaleCost = function(invoiceLineItem) {
                    $scope.working = true;
                    $scope.answered = true;

                    $http.put('/BregRestfulService/api/invoice/product/', invoiceLineItem).success(function(data, status, headers, config) {
                        $scope.correctAnswer = (data === true);
                        $scope.working = false;
                        self.hideWholesaleCostUpdateConfirm();
                    }).error(function(data, status, headers, config) {
                        $scope.title = "Oops... something went wrong";
                        $scope.working = false;
                    });
                };

                self.toggleSelectAll = function() {
                    angular.forEach(self.CurrentPurchaseOrder.LineItems, function(lineItem, key) {
                        if (self.CurrentInvoiceContainsPoItem(lineItem) === false) {
                            lineItem.IsSelected = self.SelectAll;
                        }
                    });

                };
                self.isPoItemAddEnabled = function() {
                    if (!self.CurrentInvoice || self.CurrentInvoice === null) {
                        return false;
                    }

                    if (!self.CurrentPurchaseOrder || self.CurrentPurchaseOrder === null) {
                        return false;
                    }

                    var found = false;
                    angular.forEach(self.CurrentPurchaseOrder.LineItems, function(lineItem, key) {
                        if (found === false) {
                            if (lineItem.IsSelected === true) {
                                found = true;
                            }
                        }
                    });
                    return found;
                };
                self.CurrentInvoiceContainsPoItem = function(poLineItem) {
                    if (self.CurrentInvoice === undefined || self.CurrentInvoice === null) {
                        return false;
                    }

                    var isInInvoice = false;
                    angular.forEach(self.CurrentInvoice.InvoiceLineItems, function(invLineItem, key) {
                        if (poLineItem.Id === invLineItem.SupplierOrderLineItemId) {
                            isInInvoice = true;
                        }
                    });
                    return isInInvoice;
                };
                self.addSelectedPoItemsToCurrentInvoice = function() {
                    if (!self.CurrentInvoice || self.CurrentInvoice == null) {
                        return;
                    }
                    angular.forEach(self.CurrentPurchaseOrder.LineItems, function(lineItem, key) {
                        if (lineItem.IsSelected && lineItem.IsSelected === true) {
                            $scope.activeInvoice.$setDirty();
                            self.addPoItemToCurrentInvoice(lineItem);
                        }
                    });
                    self.SelectAll = false;
                };


                self.showLoseInvoiceChangesConfirm = false;
                self.hideLoseInvoiceChangesPrompt = function() {
                    self.showLoseInvoiceChangesConfirm = false;
                };
                self.showInvoiceDuplicateConfirm = false;

                self.initiateInvoiceDuplication = function() {
                    self.showInvoiceDuplicateConfirm = true;
                };
                self.hideInvoiceDuplicateConfirm = function() {
                    self.showInvoiceDuplicateConfirm = false;
                };
                self.showInvoiceDeletionConfirm = false;

                self.initiateInvoiceDeletion = function() {
                    self.showInvoiceDeletionConfirm = true;
                };
                self.hideInvoiceDeletionConfirm = function() {
                    self.showInvoiceDeletionConfirm = false;
                };
                self.activeInvoiceLineItem = null;

                self.showWholesaleCostUpdateConfirm = false;

                self.initiateWholesaleCostUpdate = function(lineItem) {
                    self.activeInvoiceLineItem = lineItem;
                    self.showWholesaleCostUpdateConfirm = true;
                };
                self.wholesaleCostUpdateConfirmed = function() {
                    self.updateWholesaleCost(self.activeInvoiceLineItem);
                };
                self.hideWholesaleCostUpdateConfirm = function() {
                    self.showWholesaleCostUpdateConfirm = false;
                };
                self.showInvoiceItemDeletionConfirm = false;

                self.initiateInvoiceItemDeletion = function(lineItem) {
                    self.activeInvoiceLineItem = lineItem;
                    self.showInvoiceItemDeletionConfirm = true;
                };
                self.invoiceItemDeletionConfirmed = function() {
                    self.removeItemFromCurrentInvoice(self.activeInvoiceLineItem);
                };
                self.hideInvoiceItemDeletionConfirm = function() {
                    self.showInvoiceItemDeletionConfirm = false;
                };
                self.removeItemFromCurrentInvoiceLocal = function(lineItem) {
                    var index = self.CurrentInvoice.InvoiceLineItems.indexOf(lineItem);
                    self.CurrentInvoice.InvoiceLineItems.splice(index, 1);
                    self.showInvoiceItemDeletionConfirm = false;
                };
                self.removeItemFromCurrentInvoice = function(lineItem) {
                    $scope.working = true;
                    $scope.answered = true;
                    if (lineItem.InvoiceLineItemId !== null) {
                        $http.delete('/BregRestfulService/api/Invoice/LineItem/' + lineItem.InvoiceLineItemId).success(function(data, status, headers, config) {
                            $scope.correctAnswer = (data === true);
                            $scope.working = false;
                            self.removeItemFromCurrentInvoiceLocal(lineItem);
                        }).error(function(data, status, headers, config) {
                            $scope.title = "Oops... something went wrong";
                            $scope.working = false;
                        });
                    } else {
                        self.removeItemFromCurrentInvoiceLocal(lineItem);
                    }
                };

                self.deleteInvoice = function() {
                    $scope.working = true;
                    $scope.answered = true;
                    if (self.CurrentInvoice == null || self.CurrentInvoice.Id == null) {
                        return;
                    }

                    $http.delete('/BregRestfulService/api/Invoice/' + self.CurrentInvoice.Id).success(function(data, status, headers, config) {
                        $scope.correctAnswer = (data === true);
                        $scope.working = false;
                        var index = self.Invoices.indexOf(self.CurrentInvoice);
                        self.Invoices.splice(index, 1);
                        self.CurrentInvoice = null;
                        $scope.activeInvoice.$setPristine();
                        self.CurrentPurchaseOrder = null;
                        self.hideInvoiceDeletionConfirm();
                    }).error(function(data, status, headers, config) {
                        $scope.title = "Oops... something went wrong";
                        $scope.working = false;
                    });

                    self.PopulatePurchaseOrderLineItems();
                };

                self.addPoItemToCurrentInvoice = function(lineItem) {
                    var suggestedInvoicedQty = lineItem.QuantityOrdered - self.totalSupplierOrderLineItemInvoicedCount(lineItem);
                    if (suggestedInvoicedQty < 0) {
                        suggestedInvoicedQty = 0;
                    }

                    var newInvoiceLineItem = {
                        InvoiceLineItemId: null,
                        PurchaseOrderNumber: self.CurrentPurchaseOrder.PONumber,
                        SupplierOrderLineItemId: lineItem.Id,
                        ProductName: lineItem.Name,
                        ProductCode: lineItem.ProductCode,
                        QuantityCheckedIn: lineItem.QuantityCheckedIn,
                        QuantityOrdered: lineItem.QuantityOrdered,
                        QuantityInvoiced: suggestedInvoicedQty,
                        QuantityInvoicedMax: suggestedInvoicedQty,
                        WholesaleCost: lineItem.Cost

                    };
                    self.CurrentInvoice.InvoiceLineItems.push(newInvoiceLineItem);
                    lineItem.IsSelected = false;
                    lineItem.Invoices.push({
                        Number: self.CurrentInvoice.Number,
                        Date: self.CurrentInvoice.Date,
                        Quantity: suggestedInvoicedQty
                    });
                };

                self.SelectPoLineItem = function(selectedPoLineItem) {
                    selectedPoLineItem.IsSelected = !selectedPoLineItem.IsSelected;
                };
                window.onbeforeunload = function(event) {

                    //Check if there was any change, if no changes, then simply let the user leave
                    if (!$scope.activeInvoice.$dirty) {
                        return;
                    }

                    var message = 'The current invoice has unsaved changes.';
                    if (typeof event == 'undefined') {
                        event = window.event;
                    }
                    if (event) {
                        event.returnValue = message;
                    }
                    return message;
                };

                self.SelectedInvoice = null;

                self.SelectInvoice = function (selectedInvoice) {
                    self.showExportInvoices = false;
                    self.SelectedInvoice = selectedInvoice;
                    if ($scope.activeInvoice.$dirty === true) {

                        self.showLoseInvoiceChangesConfirm = true;
                        $scope.close();
                        return;
                    }
                    self.SelectInvoiceContinue(selectedInvoice);
                };
                self.loseInvoiceChangesConfirmed = function() {
                    if (self.SelectedInvoice !== null) {
                        self.SelectInvoiceContinue(self.SelectedInvoice);
                    } else {
                        self.createNewInvoiceWithDuplicateCheck();
                    }
                    self.showLoseInvoiceChangesConfirm = false;
                };
                self.SelectInvoiceContinue = function(selectedInvoice) {
                    $scope.working = true;
                    if (self.CurrentPurchaseOrder != null && selectedInvoice.Supplier.Id != self.CurrentPurchaseOrder.Supplier.Id) {
                        self.CurrentPurchaseOrder = null;
                    }
                    self.CurrentInvoice = selectedInvoice;
                    if (self.CurrentInvoice.Date === undefined || self.CurrentInvoice.Date === null || self.CurrentInvoice.Date === '0001-01-01T00:00:00') {
                        if (self.CurrentInvoice.DateAsJSON && self.CurrentInvoice.DateAsJSON !== null && self.CurrentInvoice.DateAsJSON.length >= 19) {
                            var tzDate = new Date(self.CurrentInvoice.DateAsJSON.substring(0, 19));
                            self.CurrentInvoice.Date = tzDate;
                        }
                    }

                    if (!(self.CurrentInvoice.Date === undefined || self.CurrentInvoice.Date === null || self.CurrentInvoice.Date === '0001-01-01T00:00:00')) {
                        self.CurrentInvoice.DateString = self.getFormattedDate(self.CurrentInvoice.Date);
                    }
                    self.SelectedInvoice = null;
                    $scope.activeInvoice.$setPristine();
                    $scope.close();
                    $scope.working = false;
                };

                self.SelectPurchaseOrder = function(selectedPurchaseOrder) {
                    self.SelectAll = false;
                    self.CurrentPurchaseOrder = selectedPurchaseOrder;
                    $scope.closepsr();
                };

                self.getSuppliers = function() {
                    $scope.working = true;
                    $scope.answered = false;
                    $scope.title = "loading suppliers...";
                    $scope.options = [];
                    $http.defaults.headers.common.token = Token;
                    var promise = invoiceFactory.getSuppliers(PRACTICE_ID);
                    promise
                        .success(function(data, status, headers, config) {
                            Array.prototype.push.apply(self.InvoiceSearchSuppliers, data);
                            Array.prototype.push.apply(self.InvoiceCreateSuppliers, data);
                            $scope.title = data.title;
                            $scope.answered = false;
                            $scope.working = false;
                        }).error(function(data, status, headers, config) {
                            $scope.title = "Oops... something went wrong";
                            $scope.working = false;
                        });
                };

                self.getSuppliers();

                self.search = { FromDate: '' };
                self.getPurchaseOrders = function() {
                    $scope.working = true;
                    $scope.answered = false;
                    $scope.title = "loading purchase orders...";
                    $scope.options = [];
                    $http.defaults.headers.common.token = Token;


                    $http.post("/BregRestfulService/api/supplierorder/" + PRACTICE_ID, self.search).success(function(data, status, headers, config) {
                        self.PurchaseOrders = data;
                        if (self.search.FromDate === '') {
                            self.dateFromPOs = self.getFormattedDate(new Date(data[data.length - 1].DateAsJSON));
                            self.dateFromPOsServer = new Date(data[data.length - 1].DateAsJSON);
                            self.dateFromPOsServer.setHours(0, 0, 0, 0);
                            self.dateToPOs = self.getFormattedDate(new Date(data[0].DateAsJSON));
                        }
                        self.PopulatePurchaseOrderLineItems();
                        $scope.title = data.title;
                        $scope.answered = false;
                        $scope.working = false;
                    }).error(function(data, status, headers, config) {
                        $scope.title = "Oops... something went wrong";
                        $scope.working = false;
                    });

                };

                self.fromDatePosChanged = function() {
                    var fromDateAsJson = '';
                    if (self.dateFromPOs && self.dateFromPOsDate.getTime() < self.dateFromPOsServer.getTime()) {
                        fromDateAsJson = self.dateFromPOsDate.toISOString();
                        self.dateFromPOsServer = self.dateFromPOsDate;
                        self.dateFromPOsServer.setHours(0, 0, 0, 0);
                        self.search = { FromDate: fromDateAsJson };
                        self.getPurchaseOrders();
                        return;
                    } else if (self.dateFromPOsDate === undefined && self.dateFromPOsServer !== undefined) {
                        var month = self.dateFromPOsServer.getMonth() - 2;
                        if (month < 0) {
                            self.dateFromPOsServer.setYear(self.dateFromPOsServer.getFullYear() - 1);
                            month += 12;
                        }
                        self.dateFromPOsServer.setMonth(month);
                        fromDateAsJson = self.dateFromPOsServer.toISOString();

                        self.search = { FromDate: fromDateAsJson };
                        self.getPurchaseOrders();
                        self.dateFromPOs = self.getFormattedDate(self.dateFromPOsServer);
                        return;
                    } else {
                        return;
                    }


                };
                self.PopulatePurchaseOrderLineItems = function() {
                    angular.forEach(self.PurchaseOrders, function(purchaseOrder, key) {
                        self.getPurchaseOrdersLineItems(purchaseOrder);
                    });
                };
                self.getPurchaseOrdersLineItems = function(selectedPurchaseOrder) {
                    $scope.working = true;
                    $scope.answered = false;
                    $scope.title = "loading purchase order line items...";
                    $scope.options = [];
                    $http.defaults.headers.common.token = Token;

                    $http.get("/BregRestfulService/api/supplierorder/lineitems/" + selectedPurchaseOrder.SupplierOrderId).success(function(data, status, headers, config) {
                        selectedPurchaseOrder.LineItems = data;
                        $scope.title = data.title;
                        $scope.answered = false;
                        $scope.working = false;
                    }).error(function(data, status, headers, config) {
                        $scope.title = "Oops... something went wrong";
                        $scope.working = false;
                    });
                };

                self.getPurchaseOrders();

                self.getInvoices = function() {
                    $scope.working = true;
                    $scope.answered = false;
                    $scope.title = "loading invoices...";
                    $scope.options = [];

                    invoiceFactory.getInvoices(PRACTICE_ID, Token)
                        .success(function(data, status, headers, config) {
                            self.Invoices = data;
                            self.populateInvoiceItems();
                            $scope.title = data.title;
                            $scope.answered = false;
                            $scope.working = false;
                        }).error(function(data, status, headers, config) {
                            $scope.title = "Oops... something went wrong";
                            $scope.working = false;
                        });
                };

                self.populateInvoiceItems = function() {
                    angular.forEach(self.Invoices, function(invoice, key) {
                        $http.get("/BregRestfulService/api/invoice/lineitems/" + invoice.Id).success(function(data, status, headers, config) {
                            invoice.InvoiceLineItems = data;
                            $scope.title = data.title;
                            $scope.answered = false;
                            $scope.working = false;
                        }).error(function(data, status, headers, config) {
                            $scope.title = "Oops... something went wrong";
                            $scope.working = false;
                        });

                    });
                };
                self.getInvoices();

                self.RefreshInvoices = function() {
                    self.getInvoices();
                };
                self.RefreshPurchaseOrders = function() {
                    self.getPurchaseOrders();
                };
            }
        }
    )()