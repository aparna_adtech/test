﻿(function () {
    angular.module('invoiceApp')
        .filter("exported", exported);

    function exported() {
        return filter;

        function filter(collection, showExported) {
            var filtered = [];
            angular.forEach(collection, function (item) {
                if (!item.IsExported || showExported) {
                    filtered.push(item);
                }
            });
            return filtered;
        };
    }
})()