﻿(function() {
    angular.module('invoiceApp')
        .factory('invoiceFactory', invoiceFactory);

    function invoiceFactory($http) {
        function getSuppliers(practiceId) {
            return $http.get("/BregRestfulService/api/supplier/" + practiceId);
        }

        function getInvoices(practiceId, token) {
            $http.defaults.headers.common.token = token;
            return $http.get("/BregRestfulService/api/invoice/" + practiceId);
        }

        return {
            getSuppliers: getSuppliers,
            getInvoices : getInvoices
        }

    }


})()