using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.WebControls;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

namespace BregVision
{
    public partial class RoutePurchaseOrder : BregVision.Bases.PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
            {
                Response.Redirect("default.aspx");
            }
            else
            {
                if (Roles.IsUserInRole(User.Identity.Name, "BregAdmin")
                    || Roles.IsUserInRole(User.Identity.Name, "PracticeAdmin")
                    || (Roles.IsUserInRole(User.Identity.Name, "PracticeLocationUser")
                            && Convert.ToBoolean(Session["AllowPracticeUserOrderWithoutApproval"])))
                {
                    //Process Order and create Purchase Order
                    Response.Redirect("OrderConfirmation.aspx");
                }
                else
                {
                    if (Roles.IsUserInRole(User.Identity.Name, "ClinicLocationUser")
                        || Roles.IsUserInRole(User.Identity.Name, "PracticeLocationUser"))
                    {
                        // Must route to Practice Admin
                        //Session["SessionGoToPage"] = "AUorderapproval.aspx";
                        new FaxEmailPOs().SendEmail(GetEmailForConfirmation(Convert.ToInt32(Session["PracticeLocationID"])), CreatePOAuthorizationEmailRequest(), "Breg Vision - Purchase Order Authorization Needed", BLL.SecureMessaging.MessageClass.Orders, visionSessionProvider);

                        Response.Redirect("OrderRequest.aspx");
                    }
                }
            }
        }

        protected string GetEmailForConfirmation(Int32 PracticeLocationID)
        {
            //int rowsAffected = 0;

            Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
            DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetEmailForPracticeLocation");
            db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, PracticeLocationID);

            string SQLReturn =db.ExecuteScalar(dbCommand).ToString();
            return SQLReturn;
        }
        
        protected string CreatePOAuthorizationEmailRequest()
        {

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(System.DateTime.Now);
            sb.Append("<br><br>");
            sb.Append("A purchase order request has been submitted for the location: ");
            sb.Append("<br><br>");
            sb.Append(Session["PracticeLocationName"].ToString());
            sb.Append("<br><br>");

            sb.Append("Please follow the next steps to approve this order:");
            sb.Append("<br><br>");
            sb.Append("1. Click the link below to go to BREG Vision");
            sb.Append("<br>");
            sb.Append("2. Login in with your administrator's password");
            sb.Append("<br>");
            sb.Append("3. Choose the practice location specified in this email");
            sb.Append("<br>");
            sb.Append("4. Click on the shopping cart and continue checkout");
            sb.Append("<br><br>");



            //sb.Append("<a href='http://www.bregvision.com/Authentication/AUorderapproval.aspx?PracticeLocationID=");
            sb.Append("<a href='http://www.bregvision.com");
            //sb.Append(Session["PracticeLocationID"].ToString());
            //sb.Append("&ShippingTypeID=");
            //sb.Append(Session["ShippingTypeID"].ToString());

            //sb.Append("'>Click this link to view the Purchase Order Request</a>");
            sb.Append("'>BREG Vision</a>");
            sb.Append("<br><br>");

            sb.Append("<b>You will have to login with your administrator's password before reviewing this order</b>");
            sb.Append("<br><br>");
            sb.Append("BREG Copyright 2007");


            //Code needs to be revisited for approval process
            //System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //sb.Append(System.DateTime.Now);
            //sb.Append("<br><br>");
            //sb.Append("A purchase order request has been submitted for the location: ");
            //sb.Append("<br><br>");
            //sb.Append(Session["PracticeLocationName"].ToString());
            //sb.Append("<br><br>");
            //sb.Append("<a href='http://www.bregvision.com/Authentication/AUorderapproval.aspx?PracticeLocationID=");
            //sb.Append(Session["PracticeLocationID"].ToString());
            //sb.Append("&ShippingTypeID=");
            //sb.Append(Session["ShippingTypeID"].ToString());
            
            //sb.Append("'>Click this link to view the Purchase Order Request</a>");
            //sb.Append("<br><br>");
            
            //sb.Append("<b>You may have to login with your administrator's password before reviewing this order</b>");
            //sb.Append("<br><br>");
            //sb.Append("Breg Copyright 2007");

            return sb.ToString();
        }
    }

}
