// JavaScript Document
$().ready(function() {
	// validate signup form. If it validates, submit and pop a thank you alert
	// assuming this will be ajax submit
	$("#contactForm").validate({
		
		rules: {			
			firstname: "required",
			lastname: "required",
			email: {
				required: true,
				email: true
			}
		},
		messages: {
			firstname: "Please enter your first name",
			lastname: "Please enter your last name",
			email: "Please enter a valid email address"
		},
		
  	    submitHandler: function(form) {
     		alert("Thanks for your submission");
      		form.submit();
        }
					
	})
	
	// validate login. If it validates, redirect to logged in page
	$("#loginForm").validate({		
		 errorPlacement: function(error, placement) {
			 //uses qtip jquery plugin
			$(placement).qtip({
				content: error.text(),
				show: { when: { event: 'none'}, ready: true },
				hide: { when: { event: 'click' } },
				position: {
				  corner: {
					 target: 'bottomMiddle',
					 tooltip: 'topMiddle'
				  }
			   },
			   style: {
				  border: {
					 width: 1,
					 radius: 2
				  },
				  top: 800,
				  tip: true,
				  name: 'red'
			   },
			  
			});
	},
		
  	    submitHandler: function(form) {
      		form.submit();
        }		

	});	
});
//modal box appear/disappear by animation to above top of viewport
	$(function() {
		$('.contactform').click(function(){
			$('#overlay').fadeIn('fast',function(){
				$('#contact_form').animate({'top':'160px'},500);
			});
		});				
		$('#boxclose').click(function(){
			$('#box').animate({'top':'-400px'},500,function(){
				$('#overlay').fadeOut('fast');
			});
		});
		$('#formclose').click(function(){
			$('#contact_form').animate({'top':'-800px'},500,function(){
				$('#overlay').fadeOut('fast');
			});
		});	
	});

	