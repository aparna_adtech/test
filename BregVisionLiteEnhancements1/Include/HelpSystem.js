﻿function SelectHelpContext(id) {
    try {
        ExpandHelpPane();
        var plugin = document.getElementById('HelpControl');
        var dbl = plugin.Content.API.GetHelpArticle(id);
    }
    catch (ex) { }
}
function LoadHelpContext(id) {
    try {
        var plugin = document.getElementById('HelpControl');
        var dbl = plugin.Content.API.LoadHelpContext(id, 1000);
    }
    catch (ex) { }
}
function SearchForHelpArticlesAndHelpMeida(searchText) {
    try {
        var plugin = document.getElementById('HelpControl');
        var dbl = plugin.Content.API.SearchForHelpArticlesAndHelpMeida(searchText);
    }
    catch (ex) { }
}
function SetHelpArticle(articleHtml) {
    try {
        var thisDiv = document.getElementById('article');
        thisDiv.innerHTML = articleHtml;
        
    }
    catch (ex) { }
}
function HideHelpArticle() {
    try {
        var thisDiv = document.getElementById('article');
        thisDiv.style.visibility = 'hidden';
        thisDiv.innerHTML = '';
    }
    catch (ex) { }
}
function ShowHelpArticle() {
    try {
        var thisDiv = document.getElementById('article');
        thisDiv.style.visibility = 'visible';
    }
    catch (ex) { }
}



