﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ClassLibrary.DAL.Helpers;
using BregWcfService.EMRIntegrationReference;
using System.Text.RegularExpressions;



namespace BregVision.Extensions
{
    public static class ScheduleItemExtensions
    {
        public const string TIME_EXPRESSION = @"^(?:(?:0?[0-9]|1[0-2]):[0-5][0-9]\s?(?:(?:[Aa]|[Pp])[Mm])?|(?:1[3-9]|2[0-3]):[0-5][0-9])$";

        public static schedule_item[] Search(this schedule_item[] scheduleItems, string searchText, string physicianName)
        {
            if (!string.IsNullOrWhiteSpace(searchText) || ! string.IsNullOrWhiteSpace(physicianName))
            {
                string[] keywords = searchText.Split(' ');

                var predicate = PredicateBuilder.False<BregWcfService.EMRIntegrationReference.schedule_item>();

                //if (!string.IsNullOrWhiteSpace(physicianName))
                //{
                //    predicate = predicate.Or(p => p.physician_name.ToLower().Contains(physicianName.ToLower()));
                //}

                foreach (string keyword in keywords)
                {
                    if (!string.IsNullOrWhiteSpace(keyword))
                    {
                        var temp = keyword;
                        #region These codes are created by Mike - comment them out for now u=
                        //predicate = predicate.Or(p => p.patient_name.ToLower().Contains(keyword.ToLower()));
                        //predicate = predicate.Or(p => p.patient_id.ToLower().Contains(keyword.ToLower()));
                        //predicate = predicate.Or(p => p.patient_email.ToLower().Contains(keyword.ToLower()));
                        ////predicate = predicate.Or(p => p.physician_name.ToLower().Contains(keyword.ToLower()));
                        //predicate = predicate.Or(p => p.diagnosis_icd9.ToLower().Contains(keyword.ToLower()));
                        //predicate = predicate.And(p => p.physician_name.ToLower().Contains(physicianName.ToLower()));  //added by u=
                        #endregion
                        predicate = predicate.Or(p => p.patient_name.ToLower().Contains(temp.ToLower()))
                            .Or(p => p.patient_id.ToLower().Contains(temp.ToLower()))
                            .Or(p => p.patient_email.ToLower().Contains(temp.ToLower()))
                            .Or(p => p.diagnosis_icd9.ToLower().Contains(temp.ToLower()))
                            .And(p => p.physician_name.ToLower().Contains(physicianName.ToLower()));

                        DateTime searchDate = DateTime.MinValue;
                        if (DateTime.TryParse(keyword, out searchDate))
                        {
                            Regex regex = new Regex(TIME_EXPRESSION);
                            if (regex.IsMatch(keyword))
                            {
                                predicate = predicate.Or(p => p.scheduled_visit_datetime.ToString("h:mmtt").ToLower().Contains(keyword.ToLower()));
                            }
                            else
                            {
                                predicate = predicate.Or(p => p.scheduled_visit_datetime.Date.Equals(searchDate.Date));
                            }
                        }
                    }
                    else  //If there is no patient infomation in the search box, we need to search only physician(s).
                    {
                        if (!string.IsNullOrWhiteSpace(physicianName))
                        {
                            predicate = predicate.Or(p => p.physician_name.ToLower().Contains(physicianName.ToLower()));
                        }
                    }
                }
               scheduleItems = scheduleItems
                                            .AsQueryable()
                                            .Where(predicate)
                                            .ToArray<BregWcfService.EMRIntegrationReference.schedule_item>();
            }
            return scheduleItems; 
        }
    }
}