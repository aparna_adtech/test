﻿using System;
using ClassLibrary.DAL;
using J2.eFaxDeveloper.Outbound;

namespace BregVision
{
    public partial class EFaxNotification : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {   //This function's job is to mark the message sent in's 'Sent' field as 'false'.
                //don't forget to deploy the location changes for this in the web.config
                //FaxEmailPOs fs = new FaxEmailPOs();
                //fs.SendEmail("michael_sneen@yahoo.com", "EfaxNotification", "EfaxNotification");

                string fullMessage = "The eFaxDeveloper has just notified us that message(s) with TransmissionID {0} were not sent.  {1} - {2} --{3} ";
                string messageDisposition = "";

                //Try to parse the XML that should have been sent when this page is opened
                var dispCatch = new OutboundClient().DeserializeOutboundDisposition(Request.Form["xml"]);

                string transmissionID = dispCatch.TransmissionID; //Yuiko, this will have the first Character of server name in it now
                string transmissionIDLast14Char = transmissionID.Substring(1, 14);

                Int64 intTransmissionID = 0;
                //done - Yuiko, take the first character off transmissionID before the TryParse in the next line and .Length == 14
                if (transmissionID.Length == 15 && (transmissionID.Substring(0, 1) == System.Environment.MachineName.Substring(0, 1)) && Int64.TryParse(transmissionIDLast14Char, out intTransmissionID) == true)
                {
                    try
                    {
                        if (FaxMessage.UpdateUnsentFaxMessagesByTransmissionID(transmissionIDLast14Char) == false)
                        {
                            messageDisposition = "Unable to find the message by TransmissionID in the Vision System.";
                            throw new ApplicationException(messageDisposition);
                        }
                    }
                    catch (Exception ex)
                    {
                        messageDisposition = string.Format("An error was encountered on {1} marking the message with transmissionID {0} as unsent.  Details are as follows:",transmissionID, System.Environment.MachineName) + ex.Message;

                        FaxEmailPOs faxServer = new FaxEmailPOs();
                        string emailAddress = "VisionMaintenance@breg.com"; 

                        try
                        {
                            faxServer.SendEmail(emailAddress, string.Format(fullMessage, transmissionID, messageDisposition), string.Format(fullMessage, transmissionID, messageDisposition, Request.Form["xml"], transmissionID), BLL.SecureMessaging.MessageClass.BregMaintenance);
                        }
                        catch{}
                            
                        faxServer = null;
                    }

                    Response.Write("POST SUCCESSFUL<BR>");
                }
                else
                {
                    Response.Write("POST FAILED");
                }
            }
            catch 
            {
                Response.Write("POST FAILED");
            }
        }
    }
}
