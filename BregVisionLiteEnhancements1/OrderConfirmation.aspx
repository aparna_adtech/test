<%@ Page Language="C#" MasterPageFile="~/MasterPages/SiteMaster.Master" AutoEventWireup="true" CodeBehind="OrderConfirmation.aspx.cs" Inherits="BregVision.OrderConfirmation" Title="Order Confirmation" %>
<%@ Register src="UserControls/DispensePage/CustomBraceJPG.ascx" tagname="CustomBrace" tagprefix="bvu" %>

<script runat="server">
protected void btnPrintReceipt_PreRender(object s, EventArgs e)
{
        var b = s as Button;
        if (b != null)
        {
            b.Attributes["onclick"] =
                string.Format(
                    "window.open('Authentication/OrderConfirmationReceipt.aspx?BVOID={0}', '_blank'); return false;",
                    Convert.ToInt32("0" + ViewState["BVOID"].ToString()));
}
    }
</script>



 <asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<asp:HiddenField ID="ParentBregVisionOrderID" runat="server" />
	<asp:HiddenField ID="ParentShoppingCartID" runat="server" />
    <div class="flex-row subWelcome-bar">
        <h1 class="flex-row align-center justify-start">Order Confirmation 
        </h1>
        <asp:Button runat="server" ID="btnPrintReceipt" OnPreRender="btnPrintReceipt_PreRender" Text="Printer Friendly Format" />
    </div>
    <div class="subTabContent-container">
    <div class="Content">
     
     <table width="96%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="21">&nbsp;</td>
        <td width="100%"  style="font-size:x-small">
            Your Purchase Order has been successfully processed. 
        </td>
        <td width="21">&nbsp;</td>
    </tr>
    <tr>
        <td width="21">&nbsp;</td>
        <td style="font-size:x-small">
             The following Purchase Order was created for your order:<br />
        </td>
        <td width="21">&nbsp;</td>
    </tr>
    <tr>
        <td width="21">&nbsp;</td>
        <td align="center"><br />
             <asp:Label runat="server" ID="lblPONumber" Font-Bold="true"></asp:Label>
             <br />
        </td>
        <td width="21">&nbsp;</td>
    </tr>

    <tr>
        <td width="21">&nbsp;</td>
        <td align="center"><br />
             <asp:Label runat="server" ID="lblCustomPONum" Font-Bold="true"></asp:Label>
             <br />
        </td>
        <td width="21">&nbsp;</td>
    </tr>
  
    <tr>
        <td width="21">&nbsp;</td>
        <td align="center"><br />
            
            <asp:PlaceHolder runat="server" ID="plPODetail"></asp:PlaceHolder>
             <asp:Label runat="server" ID="lblPODetail"></asp:Label>
             <br />
        </td>
        <td width="21">&nbsp;</td>
    </tr>
    <tr>
        <td width="21">&nbsp;</td>
        <td  style="font-size:x-small">
              <br /><b> Please print this page and keep for your records.</b>
        </td>
        <td width="21">&nbsp;</td>
    </tr>
    <tr>
        <td width="21">&nbsp;</td>
        <td  style="font-size:x-small">
               <b>Contact your practice administrator if you need more assistance. </b>
        </td>
        <td width="21">&nbsp;</td>
    </tr> 
  </table>
  </div>
	<bvu:CustomBrace Visible='<%# IsCustomBrace %>' ID="CustomBrace" runat="server" IsReadOnly="true" BregVisionOrderID='<%# BregVisionOrderID %>' ShoppingCartID='<%# ShoppingCartID %>' />
</div>

 </asp:Content>

