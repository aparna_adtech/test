﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HelpControl.ascx.cs" Inherits="BregVision.HelpControl" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
NOT USING THIS - USE THE SILVERLIGHT VERSION
<telerik:RadToolBar  
    ID="toolbar" runat="server"
    style="z-index:90001" Width="100%" 
    Skin="Office2007"  
    OnButtonClick="toolbar_ButtonClick">
    <Items>
            <telerik:RadToolBarButton ImageUrl="~/Images/Home16.png"  Text="" ToolTip="Home" />
            <telerik:RadToolBarButton ImageUrl="~/Images/back16.png"  Text="" ToolTip="Search" />
            <telerik:RadToolBarButton ImageUrl="~/Images/fwd16.png"  Text="" ToolTip="" />
            <telerik:RadToolBarButton>
	            <ItemTemplate>
		            <asp:TextBox id="txtSearchText" runat="server" />
	            </ItemTemplate>
            </telerik:RadToolBarButton>
            <telerik:RadToolBarButton ImageUrl="~/Images/Mag16.png"  Text="" ToolTip="" />
    </Items>
</telerik:RadToolBar>

<telerik:RadTreeView ID="treeView" runat="server" Height="100%" Width="100%"/>

