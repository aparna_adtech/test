﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PatientSchedulePopup.aspx.cs" Inherits="BregVision.PatientSchedulePopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" class="iFrame">
<head runat="server">
    
  <title></title>
  <link runat="server" href="https://fonts.googleapis.com/css?family=Roboto:400,500,900" rel="stylesheet" />
  <link runat="server" href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
  <link runat="server" type="text/css" href="App_Themes/Breg/Breg.css" rel="stylesheet" />
  <link runat="server" type="text/css" href="App_Themes/Breg/AutoCompleteBox.Breg.css" rel="stylesheet" />
  <link runat="server" type="text/css" href="App_Themes/Breg/Button.Breg.css" rel="stylesheet" />
  <link runat="server" type="text/css" href="App_Themes/Breg/Calendar.Breg.css" rel="stylesheet" />
  <link runat="server" type="text/css" href="App_Themes/Breg/ComboBox.Breg.css" rel="stylesheet" />
  <link runat="server" type="text/css" href="App_Themes/Breg/DataForm.Breg.css" rel="stylesheet" />
  <link runat="server" type="text/css" href="App_Themes/Breg/DataPager.Breg.css" rel="stylesheet" />
  <link runat="server" type="text/css" href="App_Themes/Breg/DropDownList.Breg.css" rel="stylesheet" />
  <link runat="server" type="text/css" href="App_Themes/Breg/FormDecorator.Breg.css" rel="stylesheet" />
  <link runat="server" type="text/css" href="App_Themes/Breg/Grid.Breg.css" rel="stylesheet" />
  <link runat="server" type="text/css" href="App_Themes/Breg/Input.Breg.css" rel="stylesheet" />
  <link runat="server" type="text/css" href="App_Themes/Breg/ListBox.Breg.css" rel="stylesheet" />
  <link runat="server" type="text/css" href="App_Themes/Breg/Menu.Breg.css" rel="stylesheet" />
  <link runat="server" type="text/css" href="App_Themes/Breg/Notification.Breg.css" rel="stylesheet" />
  <link runat="server" type="text/css" href="App_Themes/Breg/PanelBar.Breg.css" rel="stylesheet" />
  <link runat="server" type="text/css" href="App_Themes/Breg/SearchBox.Breg.css" rel="stylesheet" />
  <link runat="server" type="text/css" href="App_Themes/Breg/Splitter.Breg.css" rel="stylesheet" />
  <link runat="server" type="text/css" href="App_Themes/Breg/TabStrip.Breg.css" rel="stylesheet" />
  <link runat="server" type="text/css" href="App_Themes/Breg/ToolBar.Breg.css" rel="stylesheet" />
  <link runat="server" type="text/css" href="App_Themes/Breg/ToolTip.Breg.css" rel="stylesheet" />
  <link runat="server" type="text/css" href="App_Themes/Breg/Window.Breg.css" rel="stylesheet" />
</head>
<body>
  <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" >
     <Scripts>
       <asp:ScriptReference Path="https://code.jquery.com/jquery-2.2.1.min.js" />
    </Scripts>
    </telerik:RadScriptManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
      <script type="text/javascript">
        function PopupArguments() {
          this.DispenseQueueIndex = -1;
          this.CboICD9ClientId = "";
        }

        function PatientSchedule() {
          this.PatientName = "";
          this.PatientFirstName = "";
          this.PatientLastName = "";
          this.PatientId = "";
          this.PhysicianName = "";
          this.PatientEmail = "";
          this.ScheduledVisit = new Date("1/1/1900");
          this.ICD9 = "";
          this.PopupArguments = new PopupArguments();
        }

        function GetRadWindow() {
          var oWindow = null;
          if (window.radWindow)
            oWindow = window.radWindow;
          else if (window.frameElement.radWindow)
            oWindow = window.frameElement.radWindow;
          return oWindow;
        }

        function cancelAndClose() {
          var oWindow = GetRadWindow();
          oWindow.close(null);
        }

        function returnArg(patientSchedule) {
          var oWnd = GetRadWindow();
          patientSchedule.PopupArguments = oWnd.argument;
          oWnd.close(patientSchedule);
        }

        function RowSelected(sender, eventArgs) {
          var e = eventArgs.get_domEvent();
          var patientSchedule = new PatientSchedule();
          patientSchedule.PatientName = eventArgs.get_item().get_cell("PatientName").innerHTML;
          patientSchedule.PatientFirstName = eventArgs.get_item().get_cell("PatientFirstName").innerHTML;
          patientSchedule.PatientLastName = eventArgs.get_item().get_cell("PatientLastName").innerHTML;
          patientSchedule.PatientId = eventArgs.get_item().get_cell("patientidhdn").innerHTML;
          patientSchedule.PhysicianName = eventArgs.get_item().get_cell("PhysicianName").innerHTML;
          patientSchedule.ICD9 = $(eventArgs.get_item().get_cell("diagnosis_icd9")).text();//using text because empty values were being set to &nbsp; in the textbox vbcs-1824
          patientSchedule.ScheduledVisit = new Date(eventArgs.get_item().get_cell("scheduled_visit_datetime").innerHTML);
          patientSchedule.PatientEmail = eventArgs.get_item().get_cell("patientemailhdn").innerHTML;
          returnArg(patientSchedule);
        }
      </script>

    </telerik:RadCodeBlock>

    <div class="flex-row actions-bar">
      <bvu:SearchToolbar ID="PracticeCatalogSearch" runat="server" DropDownHidden="true" TextboxPlaceholderText="Search for Patients"></bvu:SearchToolbar>
      <asp:Label ID="lblPhysicianLable" runat="server" Text="Physician" Visible="False"></asp:Label>
      <div class="flex-row">
        <span class="light">Physician</span>&nbsp;<div class="Dropdown-Container">
          <asp:DropDownList ID="cboPhysicians" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboPhysicians_SelectedIndexChanged"></asp:DropDownList>
        </div>

      </div>
    </div>
    <div>
      <telerik:RadGrid ID="grdPatientSchedule" runat="server" OnNeedDataSource="grdPatientSchedule_NeedDataSource"
        OnDetailTableDataBind="grdPatientSchedule_DetailTableDataBind"
        AllowMultiRowSelection="false" AllowPaging="True" AllowSorting="true"
        ShowGroupPanel="True" Skin="Breg" EnableEmbeddedSkins="False">
        <PagerStyle Mode="NumericPages" AlwaysVisible="True" />
        <MasterTableView Name="mtvPatientSchedule" AutoGenerateColumns="False" DataKeyNames="patient_id"
          EnableTheming="true" AllowSorting="true">
          <ExpandCollapseColumn Visible="true" Resizable="false">
            <HeaderStyle Width="20px" />
          </ExpandCollapseColumn>
          <RowIndicatorColumn Visible="true">
            <HeaderStyle Width="20px" />
          </RowIndicatorColumn>
          <SortExpressions>
            <telerik:GridSortExpression FieldName="patient_name" SortOrder="Ascending" />
          </SortExpressions>
          <Columns>
            <telerik:GridBoundColumn DataField="schedule_item_id" Visible="False">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="patient_id" UniqueName="patientidhdn" Visible="True" Display="false">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="patient_email" UniqueName="patientemailhdn" Visible="True" Display="false">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="patient_name" HeaderText="Patient Name" UniqueName="PatientName">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="physician_name" HeaderText="Provider Name" UniqueName="PhysicianName">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="scheduled_visit_datetime" HeaderText="Appointment Time" UniqueName="scheduled_visit_datetime" DataFormatString="{0:M/dd/yyyy h:mmtt}">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="diagnosis_icd9" HeaderText="Dx Code" UniqueName="diagnosis_icd9" Visible="True">
            </telerik:GridBoundColumn>
<%--          </Columns>
          <DetailTables>
            <telerik:GridTableView Name="grdPatientScheduleDetail" runat="server" DataKeyNames="schedule_item_id" AutoGenerateColumns="false">
              <ParentTableRelation>
                <telerik:GridRelationFields DetailKeyField="patient_id" MasterKeyField="patient_id" />
              </ParentTableRelation>
              <ExpandCollapseColumn Visible="false" Resizable="False">
                <HeaderStyle Width="20px" />
              </ExpandCollapseColumn>
              <RowIndicatorColumn Visible="False">
                <HeaderStyle Width="20px" />
              </RowIndicatorColumn>
              <Columns>
                <telerik:GridBoundColumn DataField="schedule_item_id" Visible="False">
                </telerik:GridBoundColumn>--%>
                <telerik:GridBoundColumn DataField="patient_id" HeaderText="Patient ID" UniqueName="PatientID">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="patient_email" HeaderText="Patient Email" UniqueName="PatientEmail">
                </telerik:GridBoundColumn>
              <telerik:GridBoundColumn DataField="patient_first_name" HeaderText="Patient First Name" UniqueName="PatientFirstName" Visible="True" Display="false">
                </telerik:GridBoundColumn>
              <telerik:GridBoundColumn DataField="patient_last_name" HeaderText="Patient Last Name" UniqueName="PatientLastName" Visible="True" Display="false">
                </telerik:GridBoundColumn>
              </Columns>
<%--            </telerik:GridTableView>
          </DetailTables>--%>
        </MasterTableView>
        <ClientSettings AllowColumnsReorder="false" AllowDragToGroup="false" ReorderColumnsOnClient="false">
          <ClientEvents OnRowClick="RowSelected" />
          <Selecting AllowRowSelect="True" />
        </ClientSettings>
      </telerik:RadGrid>
    </div>
  </form>
</body>
</html>
