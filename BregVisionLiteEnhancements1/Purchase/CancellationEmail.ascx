﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CancellationEmail.ascx.cs" Inherits="BregVision.Purchase.CancellationEmail" %>
 <style type="text/css"> 


.style1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	text-align: left;
}
.style2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight:bold;
	text-align: left;
}
.style3 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight:bold;
	text-align: left;
}

.style4 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	text-align: left;
}

.addToCart 
{
    height: 48px;
    width: 224px;
    background-image: url(Images/DefaultRotaterBaseImageRight.gif);    
}
 .Copyright
 {
    text-align: center;
    font-family: Verdana, Arial;
    font-size: 8pt;
    color: Black;
    /*background-color: #F4F8FA;
    height: 10px;*/
 }
.jewelryImage 
{
    height: 251px;
    width: 153px;    
    float: left;
}
.jewelryRightPane 
{
    height: 251px;
    width: 224px;
    float: left;
}
.jewelryDescription 
{
    height: 168px;
    width: 600px;
    background-image: url(Images/DefaultRotaterBaseImageRight.gif);
}
.jewelryDescription_Text 
{
    font: normal 14px Arial, Verdana, Helvetica;
    color: #575759;
    margin-left: 10px;
    padding-top: 10px;
    margin-right: 5px;
}
.jewelryName
{
    height: 35px;
    width: 224px;
}

</style>

<p></p>
<p class="style3">Cancellation Notice from Pay Pal Posting Acceptor -</p>
<br /><br />
<p class="style4">Pay Pal just informed us that the following subscriber has cancelled their subscription to Vision Express. <asp:Label ID="Label1" runat="server" Font-Bold="true" /></p>
<br /><br />
<p class="style4">Practice <asp:Label ID="lblPracticeName" runat="server" Font-Bold="true" /></p>
<br />
<p class="style4">Subscriber <asp:Label ID="lblSubscriber" runat="server" Font-Bold="true" /></p>
 <br />
 <p class="style4">Subscriber email <asp:Label ID="lblSubscriberEmail" runat="server" Font-Bold="true" /></p>
 <br />
  <p class="style4">Subscriber Pay Pal Payer ID <asp:Label ID="lblPayPalPayerID" runat="server" Font-Bold="true" /></p>
 <br />
   <p class="style4">Subscriber Pay Pal Subscriber ID <asp:Label ID="lblPayPalSubscriptionID" runat="server" Font-Bold="true" /></p>
 <br />
    <p class="style4">Subscriber Pay Pal Subscription Date <asp:Label ID="lblSubscriptionDate" runat="server" Font-Bold="true" /></p>
 <br />
     <p class="style4">Subscriber Pay Pal Business Name <asp:Label ID="lblPayerBusinessName" runat="server" Font-Bold="true" /></p>
 <br />
 
