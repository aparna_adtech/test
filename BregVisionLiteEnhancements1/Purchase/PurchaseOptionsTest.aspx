﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PurchaseOptionsTest.aspx.cs" Inherits="BregVision.Purchase.PurchaseOptionsTest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
     <style type="text/css"> 


.style1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	text-align: left;
}
.style2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight:bold;
	text-align: left;
}
.style3 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight:bold;
	text-align: left;
}

.addToCart 
{
    height: 48px;
    width: 224px;
    background-image: url(Images/DefaultRotaterBaseImageRight.gif);    
}
 .Copyright
 {
    text-align: center;
    font-family: Verdana, Arial;
    font-size: 8pt;
    color: Black;
    /*background-color: #F4F8FA;
    height: 10px;*/
 }
.jewelryImage 
{
    height: 251px;
    width: 153px;    
    float: left;
}
.jewelryRightPane 
{
    height: 251px;
    width: 224px;
    float: left;
}
.jewelryDescription 
{
    height: 168px;
    width: 600px;
    background-image: url(Images/DefaultRotaterBaseImageRight.gif);
}
.jewelryDescription_Text 
{
    font: normal 14px Arial, Verdana, Helvetica;
    color: #575759;
    margin-left: 10px;
    padding-top: 10px;
    margin-right: 5px;
}
.jewelryName
{
    height: 35px;
    width: 224px;
}

</style>
</head>
<body>
           <table width="100%">
            <tr>
                <td >
                <img id="MockUpHomeMouseOver_01" runat="server"  src="~/images/MockUpHomeMouseOver_01.gif" width="253" height="91" alt="" />
                </td>
                <td align="right">
                    <table>
                        <tr>
                            <td align="right" style="color:Navy;font-size:xx-large;font-family:Arial">Step 2</td>
                          
                        </tr>
                        <tr>
                            <td align="right">
                                <a href="../default.aspx" >Home</a>
                            </td>
                        </tr>
                    </table>
                </td>

            </tr>
        </table>
    
    <p class="style3">TESTING PAYPAL PAGE - Choose Your Purchase Option - TESTING PAYPAL PAGE</p> 
    
     <p class="style3">Vision Express is a subscription service that costs .07 cents per month.<br />
     You have two options when subscribing to Vision Express. </p> 
    
    <p class="style1">1.  You can purchase your Vision Express subscription now using a credit card.  <br />
    You will have the option to choose reoccurring payments when subscribing. <br /><br /> Click on the 'Subscribe' button below to get started.</p> 

    
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
        <input type="hidden" name="cmd" value="_xclick-subscriptions" />
        <input type="hidden" name="business" value="ULEMACRJJX7BA" />
        <input type="hidden" name="lc" value="US" />
        <input type="hidden" name="item_name" value="Vision Express" />
        <input type="hidden" name="invoice" value="VisionExpress" />
        <input type="hidden" name="no_note" value="1" />
        <input type="hidden" name="no_shipping" value="1" />
        <input type="hidden" name="return" value="<%= ConfigurationSettings.AppSettings["PayPalReturnUrl"] %>" />
        <input type="hidden" name="cancel_return" value="<%= ConfigurationSettings.AppSettings["PayPalCancelReturnUrl"] %>" />
        <input type="hidden" name="notify_url" value="<%= ConfigurationSettings.AppSettings["PayPalNotifyReturnUrl"] %>"/>
        <input type="hidden" name="a3" value="00.07" />
        <input type="hidden" name="currency_code" value="USD" />
        <input type="hidden" name="src" value="1" />
        <input type="hidden" name="p3" value="1" />
        <input type="hidden" name="t3" value="M" />
        <input type="hidden" name="sra" value="1" />
        <[$ItemNumber$]/>
        <input type="hidden" name="bn" value="PP-SubscriptionsBF:btn_subscribeCC_LG.gif:NonHosted" />
        <input type="image" src="https://www.paypal.com/en_US/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" />
        <img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1" />
        <input type="hidden" name="image_url" value="https://engo.com/breg/images/image_url.png" />
    </form>
    <p class="style1" style="font-weight:bold">Or</p>
    <p class="style1">
    2.  If you are a current BREG customer or wish to open a BREG account and be invoiced for Vision Express, please click the button below to submit your information.<br /><br />
    <a id="SubmitInfo" runat="server" href="PurchaseOptionsComplete.aspx"><img id="MockUpHome_07" runat="server" src="~/images/submitinfo.gif" alt="" border="0" /></a><br /><br />
    If you would like to talk with a representative about setting up your account, please call the number below and ask for Vision Express Help.
    </p>
    <p class="style3">
    (888) 886-5290
    </p>



</body>
</html>
