﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MoreInformation.aspx.cs" Inherits="BregVision.Purchase.MoreInformation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    
    <style type="text/css"> 


.style1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	text-align: left;
}

.style1bold {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	text-align: left;
	font-weight:bold
}

.style2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 18px;
	text-align: left;
    font-weight:bold
}

.style3 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 18px;
	text-align: left;
    
}

.red {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	text-align: left;
    font-weight:bold;
    color:red
}
</style>

</head>
<body>
    <form id="form1" runat="server">
    <div>

       
        <table width="100%">
            <tr>
                <td >
                <img id="MockUpHomeMouseOver_01" runat="server"  src="~/images/SiteMasterHeader/MockUpHomeMouseOver_01.gif" width="253" height="91" alt="" />
                </td>
                <td align="right">
                    <table>
                        <tr>
                            <td align="right" style="color:Navy;font-size:xx-large;font-family:Arial"></td>
                          
                        </tr>
                        <tr>
                            <td align="right">
                                <a href="../default.aspx" >Home</a>
                            </td>
                        </tr>
                    </table>
                </td>

            </tr>
        </table>
    
    
    <table >
    <tr>
        <td width="55%">
        
    
        <table>
            <tr>
                <td colspan="2" class="style2">Welcome to Vision Express</td>
            </tr>
            <tr >
                <td class="style3" colspan="2" >
                    Here is a list of what you will get with your BREG Vision Express subscription! 
                </td>
            
            
            </tr>
            
             <tr >
                <td height="10" >
                 
                </td>
            
            
            </tr>
            <tr>
                <td rowspan="10" width="10"></td>
                <td  class="style1bold">
                -	Ordering of unlimited products per month, including access to all Master Catalog vendors 
                </td>
 
            </tr>
            <tr>
                <td  class="style1bold">
                -	Ability to input 5 additional vendors not included in the Master Catalog
                </td>
            </tr>
            <tr>
                 <td  class="style1bold">
                -	Check-in for all products to replenish your inventory and monitor all POs
                </td>
            </tr>
            <tr>
                 <td  class="style1bold">
                -	Dispensing to all patients, tracking by provider and patient ids
                </td>
            </tr>
           <tr>
                 <td  class="style1bold">
                -	Reports showing products dispensed, products ordered and products on hand 
                </td>
            </tr>
            <tr>
                 <td  class="style1bold">
                -	Up to 3 logins for users or administrators
                </td>
            </tr>
            <tr>
                 <td  class="style1bold">
                -	Ability to input all physicians and providers for tracking
                </td>
            </tr>
            <tr>
                 <td  class="style1bold">
                -	Access anywhere you have internet connection
                </td>
            </tr>
             <tr>
                 <td  class="style1bold">
                -	Ability to input HCPC codes, billing charges and cash charges for reporting purposes
                </td>
            </tr>
             <tr>
                 <td  class="style1bold">
                -	Online help at anytime <label style="color:Red;font-size:large">*</label>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <label class="red">* Please note, if you require live help anytime after your signup, there will be a $50 charge per incident.</label>
                </td>
            </tr>

        </table>
    </td>

         <td rowspan="10" valign="top">
            <table  height="100%">
                <tr>
                    <td valign="top" class="style2" height="30">
                        Sign Up
                    </td>
                
                </tr>

                <tr>
                    <td class="style1" >
                       For just $100 per month, you can get access to the same tools hundreds of orthopaedic practices are already using! 
                       <br />
                       <br />
                       You can sign up in a couple of easy steps! 
                    </td>
                
                </tr>
               <tr>
                    <td  class="style1" height="10">

                    </td>
                
                </tr>
                <tr>
                    <td  class="style1">
                       <label class="style1bold">Step 1</label> - Fill out the needed information to create your account login information and to read over the software terms and conditions.
                    </td>
                
                </tr>
               <tr>
                    <td  class="style1" height="5">

                    </td>
                
                </tr>
                <tr>
                
                    <td  class="style1">
                      <label class="style1bold">Step 2</label> - Choose your payment options.  To get started immediately, you can choose option 1 which is credit card payment.  If you wish to be invoiced for the software you can sign up under your current BREG account # by having your information emailed to us.  We will review your information and if your account is in good standing, your site will be activated and you will receive your welcome email within 24-48 hours.
                    </td>
                
                </tr>
                <tr>
                    <td  class="style1" height="10">

                    </td>
                
                </tr>

                <tr>
                    <td align="center">
                    
                     <a id="SignUp" href="NewPracticeApplication.aspx"><img id="MockUpHome_07" runat="server" src="~/images/signup.gif" alt="" border="0" /></a>
                    </td>
                
                </tr>
                  <tr>
                    <td  class="style1" height="10">

                    </td>
                
                </tr>
                <tr>
                    <td  class="style1">
                     If you have any questions with this process, please call 1 888 886 5290 and ask for Vision Express Help.
                    </td>
                
                </tr>

            
            </table>
        
        </td>


    
    </tr>        
 </table>


    
                                                                                                             
  </div>
     

    </form>
</body>
</html>
