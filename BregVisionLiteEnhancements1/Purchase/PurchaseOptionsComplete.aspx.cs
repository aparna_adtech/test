﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClassLibrary.DAL;

namespace BregVision.Purchase
{
    public partial class PurchaseOptionsComplete : System.Web.UI.Page
    {
        string _practiceApplicationID = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Merge code.  If this is the vision site, redirect them to express
            if (!BLL.Website.IsVisionExpress())
            {
                Response.Redirect(BLL.Website.GetOppositeSiteLoginUrl(), false);
            }



            _practiceApplicationID = Request["PAID"].ToString();

             VisionDataContext visionDB = VisionDataContext.GetVisionDataContext(); 
            
            using(visionDB)
            {
            var query = from pa in visionDB.PracticeApplications
                        where pa.PracticeApplicationID == Convert.ToInt32(_practiceApplicationID)
                        select pa;

            PracticeApplication practiceApplication = query.FirstOrDefault<PracticeApplication>();

                
                //send email
                FaxEmailPOs FE = new FaxEmailPOs();
                
                
                FE.SendEmail("GROSS@BREG.com", BuildEmailString(practiceApplication), "Vision Express Account Setup/Authorization needed!", BLL.SecureMessaging.MessageClass.BregMaintenance);
                


                //Set label on page
                lblStatus.Text = ("Thanks " + practiceApplication.FirstName + " " + practiceApplication.LastName + "  for submitting your Vision Express information.");
                lblStatus2.Text = ("Your confirmation number is: " + practiceApplication.PracticeApplicationID + " Please use this number in all correspondence.");

            
            }
       
        }
    
        public string BuildEmailString(PracticeApplication practiceApplication)
        {

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("A customer wants to subscribe to Vision Express. They have requested the option to to have a BREG account for billing.");
            sb.Append("Please verify if is user has an existing account that can be set up for Vision Express or if they need a new account created for billing.");
            sb.Append("<br><br>");
            sb.Append("The customer's information is as follows:<br><br>");
            sb.Append("Breg Account Number:");
            sb.Append(practiceApplication.BregAccountNumber);
            sb.Append("<br>");
            sb.Append("First Name:");
            sb.Append(practiceApplication.FirstName);
            sb.Append("<br>");
            sb.Append("Last Name:");
            sb.Append(practiceApplication.LastName);
            sb.Append("<br>");
            sb.Append("User Name:");
            sb.Append(practiceApplication.UserName);
            sb.Append("<br>");
            sb.Append("Practice Name:");
            sb.Append(practiceApplication.PracticeName);
            sb.Append("<br>");
            sb.Append("Practice ID:");
            sb.Append(practiceApplication.PracticeID);
            sb.Append("<br>");
            sb.Append("Cell Phone:");
            sb.Append(practiceApplication.PhoneCell);
            sb.Append("<br>");
            sb.Append("Work Phone:");
            sb.Append(practiceApplication.PhoneWork);
            sb.Append("<br>");
            sb.Append("Email:");
            sb.Append(practiceApplication.EmailAddress);
            sb.Append("<br><br>");
            sb.Append("Please make sure to call or email this customer with status on their account within 24 hours.");
            return sb.ToString();

        
        }
    
    }


}
