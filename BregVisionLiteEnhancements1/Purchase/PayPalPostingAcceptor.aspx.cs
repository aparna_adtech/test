﻿using System;
using System.IO;
using System.Text;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;


namespace BregVision.Purchase
{
    public partial class PayPalPostingAcceptor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ReadPayPalResponse();
        }

        private void ReadPayPalResponse()
        {
            string strResponse = VerifyMessage();

            if (strResponse == "VERIFIED")
            {
                string txType = GetRequestValue("txn_type");
                string itemNumber = GetRequestValue("item_number");

                DAL.PayPal.LogPostInfo(txType, itemNumber, Request.Form.ToString());
                
#if(!DEBUG)
                //check that receiver_email is your Primary PayPal email
                string receiverEmail = ConfigurationManager.AppSettings["PayPalReceiverEmail"];
                if (!(Request["receiver_email"].ToString().ToLower() == receiverEmail.ToLower()))
                    return;
#endif
                
                string payPalSubscriptionTxType = ConfigurationManager.AppSettings["PayPalSubscriptionTransactionType"];
                if (txType == payPalSubscriptionTxType)
                {
                    ProcessPayment();
                }
                else if (txType == "subscr_cancel")
                {
                    ProcessCancellation();
                }
                //else if (txType == "recurring_payment")
                //{
                //we can record "recurring_payment", "send_money", "subscr_cancel", "subscr_eot", "subscr_failed", "subscr_modify", "subscr_signup"
                //}

            }
            else if (strResponse == "INVALID")
            {
                //log for manual investigation
            }
            else
            {
                //log response/ipn data for manual investigation
            }
        }

        private void ProcessCancellation()
        {
            string controlPath = "~/Purchase/CancellationEmail.ascx";
            CancellationEmail cancellationEmail = Page.LoadControl(controlPath) as CancellationEmail;

            cancellationEmail.payPalSubscriptionID = GetRequestValue("subscr_id");
            cancellationEmail.firstName = GetRequestValue("first_name");
            cancellationEmail.lastName = GetRequestValue("last_name");
            cancellationEmail.emailAddress = GetRequestValue("payer_email");
            cancellationEmail.practiceApplicationID = GetRequestValue("item_number");
            cancellationEmail.payPalPayerID = GetRequestValue("payer_id");
            cancellationEmail.payerBusinessName = GetRequestValue("payer_business_name");
            cancellationEmail.subscriptionDate = GetRequestValue("subscr_date");

            ClassLibrary.DAL.PracticeApplication practiceApplication = BLL.Practice.PracticeApplication.GetPracticeApplication(Convert.ToInt32(cancellationEmail.practiceApplicationID));
            cancellationEmail.PracticeApplication = practiceApplication;

            if (practiceApplication.PracticeID != null)
            {
                ClassLibrary.DAL.Practice practice = BLL.Practice.Practice.Select(Convert.ToInt32(practiceApplication.PracticeID));
                cancellationEmail.Practice = practice;

                //Disable the practice
                BLL.Practice.Practice bllPractice = new BLL.Practice.Practice();
                bllPractice.PracticeID = practice.PracticeID;
                bllPractice.UserID = 1;
                DAL.Practice.Practice dalPractice = new DAL.Practice.Practice();
                dalPractice.UpdateIsActive(bllPractice, false); //inactivate or activate as needed
            
            }
            cancellationEmail.LoadControlInfo();

            FaxEmailPOs.SendCancellationEmail(cancellationEmail, Page);


            //txn_type=subscr_cancel&subscr_id=I-MBGX30MV2PL6&last_name=Thomas&residence_country=US&mc_currency=USD&item_name=Vision+Express&business=rhaywood%40breg.com
            //&amount3=0.07&recurring=1&verify_sign=A3OwthyRGNypRI-y8D5Fk.BONx.QAoWLstppfpEfnpfZ8zgPAcacqKYB&payer_status=verified&payer_email=sales%40engo.com
            //&first_name=Tom&receiver_email=rhaywood%40breg.com&payer_id=QD8P3SC33ZU2L&invoice=VisionExpress&reattempt=1&item_number=7&payer_business_name=ENGO.com
            //&subscr_date=14%3a56%3a55+Apr+28%2c+2010+PDT&charset=windows-1252&notify_version=2.9&period3=1+M&mc_amount3=0.07
        }



        private string GetRequestValue(string variableName)
        {
            if (Request[variableName] != null)
            {
                return Request[variableName].ToString();
            }
            return "";
        }

        private void ProcessPayment()
        {
            //check the payment_status is Completed
            if (Request["payment_status"] != null)
            {
                if (Request["payment_status"].ToString().ToLower() == "completed" || Request["payment_status"].ToString().ToLower() == "processed")
                {
                    //create their account based on their email
                    if (Request["item_number"] != null)
                    {
                        string itemNumber = Request["item_number"].ToString();

                        //check that txn_id has not been previously processed
                        CreatePracticeInfoFromApplicationInfo(itemNumber);
                    }
                }
            }
        }

        private string VerifyMessage()
        {
            HttpWebRequest req;
            string strRequest;

            CreateRequest(out req, out strRequest);

            //ConfigureProxy(req);

            //Send the request to PayPal and get the response
            return MakeRequest(req, strRequest);
        }

        private static string MakeRequest(HttpWebRequest req, string strRequest)
        {
            StreamWriter streamOut = new StreamWriter(req.GetRequestStream(), System.Text.Encoding.ASCII);
            streamOut.Write(strRequest);
            streamOut.Close();
            StreamReader streamIn = new StreamReader(req.GetResponse().GetResponseStream());
            string strResponse = streamIn.ReadToEnd();
            streamIn.Close();
            return strResponse;
        }

        private static void ConfigureProxy(HttpWebRequest req)
        {
            //for proxy
            WebProxy proxy = new WebProxy(new Uri("http://url:port#"));
            req.Proxy = proxy;
        }

        private void CreateRequest(out HttpWebRequest req, out string strRequest)
        {
            //Post back to either sandbox or live
#if(DEBUG)
            string strLive = "https://www.sandbox.paypal.com/cgi-bin/webscr";
#else
            string strLive = "https://www.paypal.com/cgi-bin/webscr";
#endif

            req = (HttpWebRequest)WebRequest.Create(strLive);

            //Set values for the request back
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            byte[] param = Request.BinaryRead(HttpContext.Current.Request.ContentLength);
            strRequest = Encoding.ASCII.GetString(param);
            strRequest += "&cmd=_notify-validate";
            req.ContentLength = strRequest.Length;
            
        }

        private void CreatePracticeInfoFromApplicationInfo(string itemNumber)
        {
            Int32 practiceApplicationID = 0;

            if (Int32.TryParse(itemNumber, out practiceApplicationID))
            {
                if (practiceApplicationID > 0)
                {
                    //FYI paypal sends duplicates so check state flags when processing

                    ClassLibrary.DAL.PracticeApplication practiceApplication = DAL.Practice.PracticeApplication.GetPracticeApplication(practiceApplicationID);
                    if (practiceApplication.IsPracticeCreated == false)
                    {
                        if (BLL.Practice.Practice.CreateMinimumPracticeInfo(ref practiceApplication, BLL.Practice.PaymentType.PaymentTypeEnum.PayPal, true))
                        {
                            FaxEmailPOs.SendWelcomeEmail(ref practiceApplication, this);
                            return;
                        }
                        else
                        {
                            throw new ApplicationException(string.Format("Failure to create Practice Information for account {0}", itemNumber));
                        }
                    }
                    else  
                    {
                        //this is a re activation or downgrade from Vision to VisionExpress
                        //practice and user are already created so just send the Welcome email
                        if (practiceApplication.IsWelcomeEmailSent == false) 
                        {
                            FaxEmailPOs.SendWelcomeEmail(ref practiceApplication, this);
                            return;
                        }
                    }
                }
                else
                {
                    throw new ApplicationException(string.Format("Failure to retrieve PracticeApplicationID for account {0}", itemNumber));
                }
            }
            else
            {
                throw new ApplicationException(string.Format("Failure to parse PracticeApplicationID for account {0}", itemNumber));
            }
        }

       




    }
}
