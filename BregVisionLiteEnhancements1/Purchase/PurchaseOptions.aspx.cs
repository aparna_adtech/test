﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BregVision.Purchase
{
    public partial class PurchaseOptions : System.Web.UI.Page
    {
        string _itemNumber = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Merge code.  If this is the vision site, redirect them to express
            if (!BLL.Website.IsVisionExpress())
            {
                Response.Redirect(BLL.Website.GetOppositeSiteLoginUrl(), false);
            }


            _itemNumber = Request["PAID"].ToString();
            SubmitInfo.HRef = string.Format("PurchaseOptionsComplete.aspx?PAID={0}", _itemNumber);
        }
        
        protected override void Render(HtmlTextWriter writer)    
        {        
            //Create our own mechanism to store the page output        
            StringBuilder pageSource = new StringBuilder();        
            StringWriter sw = new StringWriter(pageSource);        
            HtmlTextWriter htmlWriter = new HtmlTextWriter(sw);       
            base.Render(htmlWriter);        
            //Run replacements        
            RunPageReplacements(pageSource);                
            //Output replacements        
            writer.Write(pageSource.ToString());    
        }
        protected virtual void RunPageReplacements(StringBuilder pageSource)
        {
            pageSource.Replace("[$ItemNumber$]", string.Format("input type='hidden' name='item_number' value='{0}'", _itemNumber));
        }

    }
}
