﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BregVision.Purchase
{
    public partial class CancellationEmail : System.Web.UI.UserControl
    {
        public ClassLibrary.DAL.PracticeApplication PracticeApplication { get; set; }

        public ClassLibrary.DAL.Practice Practice { get; set; }

        public string payPalSubscriptionID { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string emailAddress { get; set; }
        public string practiceApplicationID { get; set; }
        public string payPalPayerID { get; set; }
        public string payerBusinessName { get; set; }
        public string subscriptionDate { get; set; }

        public void LoadControlInfo()
        {
            if (Practice != null)
            {
                lblPracticeName.Text = Practice.PracticeName + ":" + Practice.PracticeID.ToString();
            }
            lblSubscriber.Text = firstName + " " + lastName;
            lblSubscriberEmail.Text = emailAddress;
            lblPayerBusinessName.Text = payerBusinessName;
            lblPayPalPayerID.Text = payPalPayerID;
            lblSubscriptionDate.Text = subscriptionDate;
            lblPayPalSubscriptionID.Text = payPalSubscriptionID;
        }
    }
}