﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewPracticeApplication.aspx.cs" Inherits="BregVision.NewPracticeApplication" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css"> 


.style1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	text-align: left;
}
.style2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight:bold;
	text-align: left;
}
.style3 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight:bold;
	text-align: left;
}

.addToCart 
{
    height: 48px;
    width: 224px;
    background-image: url(Images/DefaultRotaterBaseImageRight.gif);    
}
 .Copyright
 {
    text-align: center;
    font-family: Verdana, Arial;
    font-size: 8pt;
    color: Black;
    /*background-color: #F4F8FA;
    height: 10px;*/
 }
.jewelryImage 
{
    height: 251px;
    width: 153px;    
    float: left;
}
.jewelryRightPane 
{
    height: 251px;
    width: 224px;
    float: left;
}
.jewelryDescription 
{
    height: 168px;
    width: 600px;
    background-image: url(Images/DefaultRotaterBaseImageRight.gif);
}
.jewelryDescription_Text 
{
    font: normal 14px Arial, Verdana, Helvetica;
    color: #575759;
    margin-left: 10px;
    padding-top: 10px;
    margin-right: 5px;
}
.jewelryName
{
    height: 35px;
    width: 224px;
}

 _filtered {font-family:"Cambria Math";
panose-1:2 4 5 3 5 4 6 3 2 4;}
 _filtered {font-family:Calibri;
panose-1:2 15 5 2 2 2 4 3 2 4;}
 
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0in;
margin-bottom:.0001pt;
font-size:10.0pt;
font-family:"sans-serif";}
h1
	{
margin:0in;
margin-bottom:.0001pt;
text-align:center;
 
font-size:16.0pt;
font-family:"serif";
font-style:italic;}
h2
	{
margin:0in;
margin-bottom:.0001pt;
text-align:center;
 
font-size:14.0pt;
font-family:"serif";
font-weight:normal;
font-style:italic;}
p.MsoTitle, li.MsoTitle, div.MsoTitle
	{
margin:0in;
margin-bottom:.0001pt;
text-align:center;
font-size:12.0pt;
font-family:"serif";
font-weight:bold;}
a:link, span.MsoHyperlink
	{
color:blue;
text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{
color:purple;
text-decoration:underline;}
span.EmailStyle17
	{
font-family:"sans-serif";
color:windowtext;}
span.Heading1Char
	{
 
font-family:"serif";
font-weight:bold;
font-style:italic;}
span.Heading2Char
	{
 
font-family:"serif";
font-style:italic;}
span.TitleChar
	{
 
font-family:"serif";
font-weight:bold;}
p.AgreementNormal, li.AgreementNormal, div.AgreementNormal
	{
margin-top:0in;
margin-right:0in;
margin-bottom:12.0pt;
margin-left:0in;
text-indent:1.0in;
font-size:10.0pt;
font-family:"serif";}
span.ParaNum
	{}
.MsoChpDefault
	{}
 _filtered {
margin:1.0in 1.0in 1.0in 1.0in;}
div.Section1
	{}
 
 _filtered {
 
}
 _filtered {
 
 
 
margin-left:1.5in;
}
 _filtered {
 
}
 _filtered {
 
}
 _filtered {
 
}
 _filtered {
 
}
 _filtered {
 
}
 _filtered {
 
}
 _filtered {
 
}
 _filtered {
 
}
 _filtered {
 
}
 _filtered {
 
 
 
 
margin-left:1.5in;
}
 _filtered {
 
}
 _filtered {
 
}
 _filtered {
 
}
 _filtered {
 
}
 _filtered {
 
}
 _filtered {
 
}
 _filtered {
 
}
 _filtered {
 
}
ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}

</style>

</head>
<body>
    <form id="form1" runat="server">
    
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>

    <telerik:RadCodeBlock ID="rcbHelp" runat="server" EnableViewState="true">
        <script type="text/javascript" language="javascript">
            function elementOnScroll(elementRef) {
                if (typeof elementRef == 'string') {
                    elementRef = document.getElementById(elementRef);
                }
                //alert(elementRef.scrollHeight + ":" + elementRef.scrollTop + ":" + elementRef.offsetHeight)
                if ((elementRef.scrollHeight - (elementRef.scrollTop + elementRef.offsetHeight)) < 20) {
                    var btnNext = document.getElementById("<%=btnNext.ClientID %>");
                    btnNext.disabled = false;
                }
            }
        </script>
    </telerik:RadCodeBlock>
    
    <div>
    
        <telerik:RadWindow ID="rwAuthWindow" runat="server" Modal="true" Enabled="false" 
            Animation="Fade" VisibleOnPageLoad="false" Skin="Web20" 
            Height="210px" Width="400px" AnimationDuration="2000" Behaviors="Move"  
            Title="Please Authenticate" VisibleStatusbar="false" RegisterWithScriptManager="true" >
            <ContentTemplate>
                <div align="center" style="padding:10px">
                    <table>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="labAuthStatus" runat="server" Text="Please enter your Practice (Administrator) User Name and Password to complete the upgrade process."></asp:Label>
                                </br>
                                </br>
                            </td>
                        </tr>   
                        <tr>
                            <td align="left">
                                <asp:Label ID="Label1" runat="server" Text="User Name"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtAuthUsername" runat="server" Width="120px"></asp:TextBox>
                            </td>
                        </tr>    
                        <tr>
                            <td align="left">
                                <asp:Label ID="Label2" runat="server" Text="Password"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtAuthPassword" TextMode="Password" runat="server" Width="120px"></asp:TextBox>
                            </td>
                        </tr>    
                        <tr>
                            <td align="left">
                                &nbsp;
                            </td>
                            <td align="right">
                                </br>
                                <asp:Button runat="server" ID="btnAuthSubmit" Text="Login" Width="80" OnClientClick="javascript:document.form1.submit'" />
                            </td>
                        </tr>    
                    </table>
                </div>
            </ContentTemplate>
        </telerik:RadWindow>
        
        <table width="100%">
            <tr>
                <td >
                <img id="MockUpHomeMouseOver_01" runat="server"  src="~/images/MockUpHomeMouseOver_01.gif" width="253" height="91" alt="" />
                </td>
                <td align="right">
                    <table>
                        <tr>
                            <td align="right" style="color:Navy;font-size:xx-large;font-family:Arial">Step 1</td>
                          
                        </tr>
                        <tr>
                            <td align="right">
                                <a href="../default.aspx" >Home</a>
                            </td>
                        </tr>
                    </table>
                </td>

            </tr>
        </table>
    
        <p class="style3" >Welcome to the Vision Express Setup</p>
        
        <asp:Panel ID="panelInputForm" runat="server" visible="false" >
        
        <table border="0" width="100%">
            <tr>
                <td valign="top" width="40%">
                    <table width="550px" class="style2" border="0">
                    <tr>
                        <td width="250px">
                            <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
                        </td>
                        <td width="300px">
                            <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblSalutation" runat="server" Text="Salutation"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtSalutation" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblFirstName" runat="server" Text="First Name"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName" ErrorMessage="*" Text="*"  />
                        </td>
                    </tr>                        
                    <tr>
                        <td>
                            <asp:Label ID="lblMiddleName" runat="server" Text="Middle Name"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtMiddleName" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <asp:Label ID="lblLastName" runat="server" Text="Last Name"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName" ErrorMessage="*" Text="*" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblSuffix" runat="server" Text="Suffix"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtSuffix" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblEmailAddress" runat="server" Text="Email Address"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmailAddress" runat="server"></asp:TextBox>
                             <asp:RequiredFieldValidator ID="rfvEmailAddress" runat="server" ControlToValidate="txtEmailAddress" ErrorMessage="*" Text="*" />
                             <asp:RegularExpressionValidator id="revEmailAddress" runat="server"
                                ControlToValidate="txtEmailAddress"
                                ValidationExpression=".*@.*\..*"
                                ErrorMessage="* Your entry is not a valid e-mail address."
                                display="dynamic">*
                            </asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblPhoneWork" runat="server" Text="Phone Work"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPhoneWork" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvPhoneWork" runat="server" ControlToValidate="txtPhoneWork" ErrorMessage="*" Text="*" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblPhoneWorkExtension" runat="server" Text="Phone Work Extension"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPhoneWorkExtension" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblPhoneCell" runat="server" Text="Phone Cell"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPhoneCell" runat="server"></asp:TextBox>
                        </td>
                    </tr>  
                    <tr>
                        <td>
                            <asp:Label ID="lblPracticeName" runat="server" Text="Practice Name"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPracticeName" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvPracticeName" runat="server" ControlToValidate="txtPracticeName" ErrorMessage="*" Text="*" />
                            <asp:RegularExpressionValidator ID="revPracticeName" runat="server" ControlToValidate="txtPracticeName" ErrorMessage="* Practice name must be between 6 and 50 Characters" ValidationExpression="[\w\s,.&()']{6,50}" />
                        </td>
                    </tr> 
                    <tr>
                        <td>
                            <asp:Label ID="lblUserName" runat="server" Text="User Name"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ControlToValidate="txtUserName" ErrorMessage="*" Text="*" />
                            <asp:RegularExpressionValidator ID="revUstrName" runat="server" ControlToValidate="txtUserName" ErrorMessage="* Username must be between 6 and 20 Characters" ValidationExpression="\w{6,20}" />
                        </td>
                    </tr>    
                    <tr>
                        <td valign="top">
                            <asp:Label ID="lblPassword" runat="server" Text="Password"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPassword" TextMode="Password" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator  ID="rfvPasswordValidator" runat="server" ControlToValidate="txtPassword" ErrorMessage="*" Text="*" />
                            <asp:RegularExpressionValidator ID="revPasswordValidator" runat="server" ControlToValidate="txtPassword" ErrorMessage="Your password must be at least 8 characters long, contain at least one one lower case letter, one upper case letter, one digit and one special character of @#$%^&+=" ValidationExpression="^.*(?=.{8,20})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$" />
                        </td>
                    </tr>    
                    <tr>
                        <td>
                            <asp:Label ID="lblBregAccountNumber" runat="server" Text="Breg Account Number"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtBregAccountNumber" runat="server"></asp:TextBox>
                        </td>
                    </tr>                                                                                                                   
                </table>
                                    
                </td>
                <td valign="top" width="60%" align="left">
                    <div class="jewelryDescription_Text">
                        
                        <p>Please complete the information on this page to create your Vision Express account.</p>     
                        <p>The items with a red asterisk next to them are required; your Username needs to be between 6 and 20 characters.</p> 
                        <p>When you have completed the information, scroll down to read the Terms and Conditions.  To move to the payment options you will need to accept the terms.</p>                    
                    </div>
                </td>
            </tr>
           
        </table>
        
        </asp:Panel>
        
        <table border="0" width="100%">
             <tr>
                <td colspan="2" align="left">
                    <div id="divTerms" runat="server" style="overflow:scroll; height:300px; width:99%; border:1px; border-style:solid">
                        <div class="Section1">
                            <asp:PlaceHolder ID="phTos" runat="server"></asp:PlaceHolder>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <asp:Button ID="btnNext" runat="server" Text="I Agree" onclick="btnNext_Click" Enabled="false" />
                    <asp:Button ID="btnDisagree" runat="server" Text="I Disagree" OnClientClick="javascript:document.location.href='../default.aspx'" />
                </td>
            </tr>
        </table>
        
        
    </div>
    </form>
</body>
</html>
