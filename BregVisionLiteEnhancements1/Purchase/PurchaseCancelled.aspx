﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PurchaseCancelled.aspx.cs" Inherits="BregVision.Purchase.PurchaseCancelled" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css"> 


.style1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	text-align: left;
}
.style2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight:bold;
	text-align: left;
}
.style3 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight:bold;
	text-align: left;
}

.addToCart 
{
    height: 48px;
    width: 224px;
    background-image: url(Images/DefaultRotaterBaseImageRight.gif);    
}
 .Copyright
 {
    text-align: center;
    font-family: Verdana, Arial;
    font-size: 8pt;
    color: Black;
    /*background-color: #F4F8FA;
    height: 10px;*/
 }
.jewelryImage 
{
    height: 251px;
    width: 153px;    
    float: left;
}
.jewelryRightPane 
{
    height: 251px;
    width: 224px;
    float: left;
}
.jewelryDescription 
{
    height: 168px;
    width: 600px;
    background-image: url(Images/DefaultRotaterBaseImageRight.gif);
}
.jewelryDescription_Text 
{
    font: normal 14px Arial, Verdana, Helvetica;
    color: #575759;
    margin-left: 10px;
    padding-top: 10px;
    margin-right: 5px;
}
.jewelryName
{
    height: 35px;
    width: 224px;
}

</style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <p class="style3">You have chosen to cancel your Vision Express subscription process.</p>
        <p class="style1">If this was done by mistake, please return to www.bregvisionexpress.com and you can start the process over.</p>
        <p class="style1">If you want to continue but have questions, please contact us at <a href="mailto:NewVisionExpress@breg.com">NewVisionExpress@breg.com</a> or call 1 888 886 5290.</p>
    </div>
    </form>
</body>
</html>
