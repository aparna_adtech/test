﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;

namespace BregVision.Purchase
{
    public partial class PurchaseOptionsTest : System.Web.UI.Page
    {
        string _itemNumber = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            _itemNumber = Request["PAID"].ToString();
            SubmitInfo.HRef = string.Format("PurchaseOptionsComplete.aspx?PAID={0}", _itemNumber);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            //Create our own mechanism to store the page output        
            StringBuilder pageSource = new StringBuilder();
            StringWriter sw = new StringWriter(pageSource);
            HtmlTextWriter htmlWriter = new HtmlTextWriter(sw);
            base.Render(htmlWriter);
            //Run replacements        
            RunPageReplacements(pageSource);
            //Output replacements        
            writer.Write(pageSource.ToString());
        }
        protected virtual void RunPageReplacements(StringBuilder pageSource)
        {
            pageSource.Replace("[$ItemNumber$]", string.Format("input type='hidden' name='item_number' value='{0}'", _itemNumber));
        }

    }
}
