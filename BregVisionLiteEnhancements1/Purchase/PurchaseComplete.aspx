﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PurchaseComplete.aspx.cs" Inherits="BregVision.Purchase.PurchaseComplete" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
<style type="text/css"> 
    .style1 {
	    font-family: Arial, Helvetica, sans-serif;
	    font-size: 12px;
	    text-align: left;
    }
    .style2 {
	    font-family: Arial, Helvetica, sans-serif;
	    font-size: 12px;
	    font-weight:bold;
	    text-align: left;
    }
    .style3 {
	    font-family: Arial, Helvetica, sans-serif;
	    font-size: 18px;
	    font-weight:bold;
	    text-align: left;
    }
</style>
</head>
<body>
    <form id="form1" runat="server">
         <table width="100%">
            <tr>
                <td >
                <img id="MockUpHomeMouseOver_01" runat="server"  src="~/images/MockUpHomeMouseOver_01.gif" width="253" height="91" alt="" />
                </td>
                <td align="right">
                    <table>
                        <tr>
                            <td align="right" style="color:Navy;font-size:xx-large;font-family:Arial"></td>
                          
                        </tr>
                        <tr>
                            <td align="right">
                                <a href="../default.aspx" >Home</a>
                            </td>
                        </tr>
                    </table>
                </td>

            </tr>
        </table>
    
    <div>
        <p class="style3">
        Your BREG Vision purchase is complete!  
        </p>
        <p class="style1">
            You will receive an email shortly with your login information and a link to the Vision Express login page.   
        </p>
        <p class="style1">
            If you do not receive the email or have problems with your login, please email us at <a href="mailto:NewVisionExpress@breg.com">NewVisionExpress@breg.com</a>.
        </p>
    </div>
    </form>
</body>
</html>
