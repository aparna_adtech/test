﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WelcomeEmail.ascx.cs" Inherits="BregVision.Purchase.WelcomeEmail" %>
     <style type="text/css"> 


.style1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	text-align: left;
}
.style2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight:bold;
	text-align: left;
}
.style3 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight:bold;
	text-align: left;
}

.style4 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	text-align: left;
}

.addToCart 
{
    height: 48px;
    width: 224px;
    background-image: url(Images/DefaultRotaterBaseImageRight.gif);    
}
 .Copyright
 {
    text-align: center;
    font-family: Verdana, Arial;
    font-size: 8pt;
    color: Black;
    /*background-color: #F4F8FA;
    height: 10px;*/
 }
.jewelryImage 
{
    height: 251px;
    width: 153px;    
    float: left;
}
.jewelryRightPane 
{
    height: 251px;
    width: 224px;
    float: left;
}
.jewelryDescription 
{
    height: 168px;
    width: 600px;
    background-image: url(Images/DefaultRotaterBaseImageRight.gif);
}
.jewelryDescription_Text 
{
    font: normal 14px Arial, Verdana, Helvetica;
    color: #575759;
    margin-left: 10px;
    padding-top: 10px;
    margin-right: 5px;
}
.jewelryName
{
    height: 35px;
    width: 224px;
}

</style>


<p></p>
<p class="style3">Welcome to Vision Express, Below is Your Login Information -</p>
<br /><br />


<p class="style4">Your username is <asp:Label ID="lblUsername" runat="server" Font-Bold="true" /></p>
<br />
<p class="style4">Your password is <asp:Label ID="lblPassword" runat="server" Font-Bold="true" /></p>
<br />
 
<p class="style4">The link below will direct you to the login page to access your site.  Once logged in, the setup wizard will guide you through setting up your information .</p>
 <br />
<p class="style3"><asp:HyperLink ID="hypVisionLite" runat="server" Text="BregVisionExpress" NavigateUrl="http://www.bregvisionexpress.com/Admin/PracticeSetup/PracticeSetup.aspx" /></p>


