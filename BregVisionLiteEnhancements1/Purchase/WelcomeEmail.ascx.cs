﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace BregVision.Purchase
{
    public partial class WelcomeEmail : System.Web.UI.UserControl
    {
        private const string  SETUP_URL = "{0}/default.aspx";

        public ClassLibrary.DAL.PracticeApplication PracticeApplication { get; set; }


        public void LoadControlInfo()
        {
            lblUsername.Text = PracticeApplication.UserName;
            //lblPassword.Text = "pa$$word.1";
            lblPassword.Text = PracticeApplication.Password;

            string baseUrl = ConfigurationManager.AppSettings["BaseUrl"];
            hypVisionLite.NavigateUrl = string.Format(SETUP_URL, baseUrl);
        }

    }
}