﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PurchaseOptionsComplete.aspx.cs" Inherits="BregVision.Purchase.PurchaseOptionsComplete" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css"> 


.style1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	text-align: left;
}
.style2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight:bold;
	text-align: left;
}
.style3 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight:bold;
	text-align: left;
}

.addToCart 
{
    height: 48px;
    width: 224px;
    background-image: url(Images/DefaultRotaterBaseImageRight.gif);    
}
 .Copyright
 {
    text-align: center;
    font-family: Verdana, Arial;
    font-size: 8pt;
    color: Black;
    /*background-color: #F4F8FA;
    height: 10px;*/
 }
.jewelryImage 
{
    height: 251px;
    width: 153px;    
    float: left;
}
.jewelryRightPane 
{
    height: 251px;
    width: 224px;
    float: left;
}
.jewelryDescription 
{
    height: 168px;
    width: 600px;
    background-image: url(Images/DefaultRotaterBaseImageRight.gif);
}
.jewelryDescription_Text 
{
    font: normal 14px Arial, Verdana, Helvetica;
    color: #575759;
    margin-left: 10px;
    padding-top: 10px;
    margin-right: 5px;
}
.jewelryName
{
    height: 35px;
    width: 224px;
}

</style>


</head>
<body>



    <form id="form1" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td >
                <img id="MockUpHomeMouseOver_01" runat="server"  src="~/images/MockUpHomeMouseOver_01.gif" width="253" height="91" alt="" />
                </td>
                <td align="right">
                    <table>
                        <tr>
                            <td align="right" style="color:Navy;font-size:xx-large;font-family:Arial"></td>
                          
                        </tr>
                        <tr>
                            <td align="right">
                                <a href="../Default.aspx" >Home</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

   <p class="style3">Purchase Status</p> 
    
    <asp:Label runat="server" ID="lblStatus" class="style1"></asp:Label><br /><br />
    <asp:Label runat="server" ID="lblStatus2" class="style1"></asp:Label><br /><br />
     <p class="style1">We will process your account and you should receive an activation email in the 24-48 hours.  If for some reason there is an issue with your account information, we will call or email you to discuss.  If you have any questions, please call 1 888 886 5290 and ask for Vision Express Help.   </p> 
    
                  
    </div>
    </form>
</body>
</html>
