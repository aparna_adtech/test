﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BLL.Practice;
using System.Web.Security;
using System.Data;

namespace BregVision
{
    public partial class NewPracticeApplication : System.Web.UI.Page
    {
        ClassLibrary.DAL.PracticeApplicationAgreementTerm _PracticeApplicationAgreementTerm;
        ClassLibrary.DAL.Practice _Practice;
        DataTable dataTableUsers;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            //Merge code.  If this is the vision site, redirect them to express
            if (!BLL.Website.IsVisionExpress())
            {
                Response.Redirect(BLL.Website.GetOppositeSiteLoginUrl(), false);
            }

            _PracticeApplicationAgreementTerm = PracticeApplicationAgreementTerm.GetTopPracticeApplicationAgreementTerm();
            if (_PracticeApplicationAgreementTerm != null)
            {
                phTos.Controls.Add(new LiteralControl(_PracticeApplicationAgreementTerm.Terms));
            }
            else
            {
                phTos.Controls.Add(new LiteralControl("PracticeApplicationAgreementTerm not found in database."));
            }
           
        }

        private void ShowAuthWindow(bool isVisible)
        {
            rwAuthWindow.VisibleOnPageLoad = isVisible;
            rwAuthWindow.Enabled = isVisible;
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            string scrollHandler = String.Format("JavaScript: elementOnScroll(this);");
            divTerms.Attributes.Add("onscroll", scrollHandler);

            if (Request.QueryString["PID"] == null)
            {
                panelInputForm.Visible = true;

                Page.Validate();

                if (txtUserName.Text.Length > 0)
                {
                    MembershipUserCollection muc = Membership.FindUsersByName(txtUserName.Text);
                    if (muc.Count > 0)
                    {
                        rfvUserName.Text = "Sorry, username in use";
                        rfvUserName.IsValid = false;
                    }
                }
            }
            else
            {
                if (txtAuthPassword.Text != "")
                {
                    Session.Remove("Password");
                    Session.Add("Password", txtAuthPassword.Text);
                }

                //if UID PWD window complete then
                if (!Membership.ValidateUser(txtAuthUsername.Text, Session.GetString("Password")))
                {
                    ShowAuthWindow(true);
                    return;
                }
                else
                {
                    //validate that this username is and admin for this practice
                    DAL.Practice.User dalPU = new DAL.Practice.User();
                    dataTableUsers = dalPU.SelectAllUserLogins(int.Parse(Request.QueryString["PID"]));

                    foreach (DataRow dr in dataTableUsers.Rows)
                    {
                        string userName = dr["UserName"].ToString(); //AU.UserName
                        string email = dr["Email"].ToString(); //AM.Email

                        if (userName.ToLower() == txtAuthUsername.Text.ToLower() && Roles.IsUserInRole(userName, "PracticeAdmin"))
                        {
                            txtUserName.Text = userName;
                            txtUserName.Enabled = false;
                            txtEmailAddress.Text = email;
                            txtEmailAddress.Enabled = false;
                            
                            //existing acts do not need a new password created
                            lblPassword.Visible = false;
                            txtPassword.Visible = false;
                            rfvPasswordValidator.Enabled = false;
                            revPasswordValidator.Enabled = false;

                            _Practice = BLL.Practice.Practice.Select(int.Parse(Request.QueryString["PID"]));

                            if (_Practice != null)
                            {
                                txtPracticeName.Text = _Practice.PracticeName;

                                //now get the contact for this practice and populate the UI if the 1st time loading
                                if (txtFirstName.Text == "" && _Practice.ContactID != null)
                                {
                                    BLL.Practice.Contact contact = new BLL.Practice.Contact((int)_Practice.ContactID);
                                    txtTitle.Text = contact.Title;
                                    txtSalutation.Text = contact.Salutation;
                                    txtFirstName.Text = contact.FirstName;
                                    txtLastName.Text = contact.LastName;
                                    txtMiddleName.Text = contact.MiddleName;
                                    txtPhoneWork.Text = contact.PhoneWork;
                                    txtPhoneCell.Text = contact.PhoneCell;
                                }
                            }
                            ShowAuthWindow(false);
                            return;
                        }
                    }
                    ShowAuthWindow(true); //authenticated user but not for this practice
                }
            }
        }
        
        protected void btnNext_Click(object sender, EventArgs e)
        {
            SavePracticeApplication();
        }

        private void SavePracticeApplication()
        {
            if (Page.IsValid)
            {
                ClassLibrary.DAL.PracticeApplication practiceApplication = new ClassLibrary.DAL.PracticeApplication();

                practiceApplication.BregAccountNumber = txtBregAccountNumber.Text;
                practiceApplication.EmailAddress = txtEmailAddress.Text;
                practiceApplication.FirstName = txtFirstName.Text;
                practiceApplication.LastName = txtLastName.Text;
                practiceApplication.MiddleName = txtMiddleName.Text;
                practiceApplication.PhoneCell = txtPhoneCell.Text;
                practiceApplication.PhoneWork = txtPhoneWork.Text;
                practiceApplication.PhoneWorkExtension = txtPhoneWorkExtension.Text;
                practiceApplication.PracticeName = txtPracticeName.Text;
                practiceApplication.Salutation = txtSalutation.Text;
                practiceApplication.Suffix = txtSuffix.Text;
                practiceApplication.Title = txtTitle.Text;
                practiceApplication.UserName = txtUserName.Text;
                
                practiceApplication.CreatedUserID = 1;
                practiceApplication.CreatedDate = DateTime.Now;
                practiceApplication.ModifiedUserID = 1;
                practiceApplication.ModifiedDate = DateTime.Now;
                practiceApplication.IsActive = true;
                practiceApplication.PracticeAppAgreementTermsID = _PracticeApplicationAgreementTerm.PracticeAppAgreementTermsID;

                if (Request.QueryString["PID"] != null)
                {
                    //Set all the users for this practice to VisionExpress users.
                    DAL.Practice.Practice.SetIsVisionLite(int.Parse(Request.QueryString["PID"]), true);

                    //add the vision lite user role to each user
                    DAL.Practice.User dalPU = new DAL.Practice.User();
                    dataTableUsers = dalPU.SelectAllUserLogins(int.Parse(Request.QueryString["PID"]));
                    foreach (DataRow dr in dataTableUsers.Rows)
                    {
                        string userName = dr["UserName"].ToString(); //AU.UserName

                        if (!Roles.IsUserInRole(userName, "VisionLite"))
                        {
                            List<string> users = new List<string>();
                            users.Add(userName);
                            Roles.AddUsersToRole(users.ToArray(), "VisionLite");
                        }
                    }

                    practiceApplication.IsWelcomeEmailSent = false;
                    practiceApplication.IsAdminUserCreated = true;
                    practiceApplication.IsPracticeCreated = true;
                    practiceApplication.PracticeID = int.Parse(Request.QueryString["PID"]);
                    practiceApplication.PracticeName = txtPracticeName.Text;
                    practiceApplication.Password = Session.GetString("Password"); //from txtAuthPassword
                }
                else
                {
                    practiceApplication.Password = txtPassword.Text;
                }

                PracticeApplication.SavePracticeApplication(practiceApplication);

                bool debug = false;

                if (Request.QueryString["Debug"] != null)
                {
                    if(Request.QueryString["Debug"] == "true")
                        debug = true;
                }
                
                if(debug)
                {
                    Response.Redirect(string.Format("PurchaseOptionsTest.aspx?PAID={0}", practiceApplication.PracticeApplicationID.ToString()), false);
                }
                else
                {
                    Response.Redirect(string.Format("PurchaseOptions.aspx?PAID={0}", practiceApplication.PracticeApplicationID.ToString()), false);
                }
            }
        }
    }
}
