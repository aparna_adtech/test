﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;


namespace BregVision.WebServices
{
    public class UserContext
    {
        public UserContext()
        {

        }
        public UserContext(string UserName)
        {
            SetUserName(UserName);

            GetPracticeAndLocations(UserName);

            GetUserAccess(UserName, this.Practice.ID);

            GetRoles(UserName);

        }

        private void GetPracticeAndLocations(string UserName)
        {
            using (System.Data.IDataReader reader = DAL.Practice.Practice.GetPracticeLocationInfo(UserName))
            {
                if (reader.Read())
                {
                    this.Practice = new PracticeEntity();
                    this.Practice.Name = reader["PracticeName"].ToString();
                    this.Practice.ID = Convert.ToInt32(reader["PracticeID"]);
                    List<PracticeEntity> locations = new List<PracticeEntity>();

                    do
                    {
                        //Fill location drop down box
                        if (reader["AllowAccess"].ToString() == "True")
                        {
                            PracticeEntity locationAccessible = new PracticeEntity();

                            locationAccessible.Name = reader["Name"].ToString();
                            locationAccessible.ID = Convert.ToInt32(reader["PracticeLocationID"]);

                            locations.Add(locationAccessible);

                            if (reader["UserDefaultLocation"].ToString() == "True")
                            {
                                this.PracticeLocation = locationAccessible;
                            }
                        }
                    }
                    while (reader.Read());

                    this.PracticeLocationsAccessible = locations.ToArray<PracticeEntity>();
                }
            }
        }

        private void SetUserName(string UserName)
        {
            this.User = new UserEntity();
            this.User.Username = UserName;
        }

        private void GetUserAccess(string userName, int practiceID)
        {
            BLL.Practice.User user = new BLL.Practice.User();
            DataTable userData = user.SelectUser(userName, practiceID);

            if (userData.Rows.Count > 0)
            {
                DataRow currentUser = userData.Rows[0];

                this.User.UserID = Convert.ToInt32(currentUser["UserID"]);

                this.Permissions = new Permissions();

                this.Permissions.Cart = Convert.ToBoolean(currentUser["AllowCart"]);
                this.Permissions.Dispense = Convert.ToBoolean(currentUser["AllowDispense"]);
                this.Permissions.Inventory = Convert.ToBoolean(currentUser["AllowInventory"]);
                this.Permissions.CheckIn = Convert.ToBoolean(currentUser["AllowCheckIn"]);

                this.Defaults = new Defaults();
                this.Defaults.SearchCriteria = currentUser["DefaultDispenseSearchCriteria"].ToString();
            }
        }

        private void GetRoles(string UserName)
        {
            string[] roles = System.Web.Security.Roles.GetRolesForUser(UserName);
            this.Roles = roles;
        }

        public UserEntity User { get; set; }

        public string[] Roles { get; set; }

        public PracticeEntity Practice { get; set; }

        public PracticeEntity PracticeLocation { get; set; }

        public PracticeEntity[] PracticeLocationsAccessible { get; set; }

        public Permissions Permissions { get; set; }

        public Defaults Defaults { get; set; }

        
    }
    public class PracticeEntity
    {
        public string Name { get; set; }
        public int ID { get; set; }
    }
    public class UserEntity
    {
        public string Username { get; set; }
        public int UserID { get; set; }
        public Guid UserAspNetID { get; set; }
    }
    public class Permissions
    {
        public bool Dispense { get; set; }
        public bool Inventory { get; set; }
        public bool CheckIn { get; set; }
        public bool Cart { get; set; }
    }

    public class Defaults
    {
        public string SearchCriteria { get; set; }
    }

}