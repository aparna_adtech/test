﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using ClassLibrary.DAL;

namespace BregVision.WebServices
{
    /// <summary>
    /// Summary description for FaxService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class FaxService : System.Web.Services.WebService
    {

        [WebMethod]
        public void ResendFaxes()
        {
            //added by u= next 4 lines including if statement 
            VisionDataContext visionDB = VisionDataContext.GetVisionDataContext();

            using (visionDB)
            {
                //check to see if the fax is turned off - This turns the whole system on and off
                FaxServer faxController = visionDB.FaxServers.FirstOrDefault<FaxServer>();

                if (faxController.FaxEnabled == true)
                {
                    FaxEmailPOs faxServer = new FaxEmailPOs();
                    
                    
                    IEnumerable<FaxMessage> _FaxMessages = FaxMessage.GetUnsentFaxMessages();

                    //Select the unsent fax messages.  These will be a combination of items in the FaxMessage table where:
                    //1.  We got an error in the sending process and didn't mark the fax as sent(although it still may have)
                    //2.  The fax went to eFax without an error, but they later informed us via the EFaxNotification.aspx XML HTTP post that they couldn't send the fax.
                    //3.  The SendFax.aspx page was used to look up and mark the fax as unsent(for example, maybe the vendor lost it and asked for a resend???)
                    var query = from fm in _FaxMessages.Take(10)
                                where fm.SendAttempts < 6
                                select new
                                {
                                    fm.FaxMessageID,
                                    fm.FaxFileName,
                                    fm.FaxNumber,
                                    TransmissionID = fm.FaxFileName.Substring(fm.FaxFileName.IndexOf("test2") + 5, 15),
                                    fm.CreatedDate,
                                    FaxFilename = fm.FaxFileName.Substring(fm.FaxFileName.LastIndexOf("/") + 1),
                                    FaxStatus = FaxEmailPOs.GetStatus(fm.FaxFileName.Substring(fm.FaxFileName.IndexOf("test2") + 5, 15)) //This checks the current Status
                                };

                    //MWS TODO  Don't use the failed messages.  Loop through all the messages in 'query' and:
                    //1.  if Success == true, Mark the faxMessage as 'Sent' and save it.
                    //2.  If Success == false, then try to resend it and add 1 to the SendAttempts field

                    //Make sure they really weren't sent because sometimes we get an error and mark unsent, but eFax gets them and actually sends them
                    //var failedMessages = query.Where(messageFields => messageFields.FaxStatus.Contains("Success") == false);

                    //loop through the ones that are actually unsent and resend them
                    foreach (var failedMessage in query)
                    {
                        if (failedMessage.FaxStatus.Contains("Success") == true)
                        {
                            var updatedFaxMessage = from fm in visionDB.FaxMessages
                                                    where fm.FaxMessageID == failedMessage.FaxMessageID
                                                    select fm;
                            FaxMessage faxMessage = updatedFaxMessage.FirstOrDefault<FaxMessage>();
                            if (faxMessage != null)
                            {
                                faxMessage.Sent = true;
                                visionDB.SubmitChanges();
                            }
                        }
                        else
                        {
                            //Comment this line out for now so I can call this web service from the windows service without actually changing the system
                            faxServer.SendFax(failedMessage.FaxNumber, failedMessage.FaxFileName, failedMessage.FaxMessageID);
                        }
                    }
                }
            }
        }
    }
}
