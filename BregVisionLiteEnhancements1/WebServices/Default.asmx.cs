﻿using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using ClassLibrary.DAL.Helpers;
using System.Web.Security;

namespace BregVision.WebServices
{
    /// <summary>
    /// This is the login page for web services access 
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Default : System.Web.Services.WebService
    {
        public SecuredWebServiceHeader SoapHeader; 
       
        [WebMethod]       
        [System.Web.Services.Protocols.SoapHeader("SoapHeader")]         
        public UserContext AuthenticateUser()         
        { 
           
            if (SoapHeader == null)                 
                throw new System.ApplicationException( "Please provide a Username and Password"); 
           
            if (string.IsNullOrEmpty(SoapHeader.Username) || string.IsNullOrEmpty(SoapHeader.Password))              
                throw new System.ApplicationException( "Please provide a Username and Password");

            string username = "";
            string password = "";

            username = SoapHeader.Username;
            password = SoapHeader.Password;


#if(DEBUG)
            username = "TestAdmin";
            password = "test.test";

#endif
           
            // Are the credentials valid?          
            if (!Login(username, password))
            {                
                throw new System.ApplicationException( "Invalid Username or Password");
            }


            UserContext context = new UserContext(username);

            return context;
        }

        [WebMethod]
        public void LogOut()
        {
            // Deprive client of the authentication key
            FormsAuthentication.SignOut();
        }

        public bool Login(string strUser, string strPwd)
        {
            bool strRole = AuthenticateUser(strUser, strPwd);

            if (strRole == true)
            {
                // Issue the authentication key to the client
                FormsAuthentication.SetAuthCookie(strUser, false);
                return true;
            }
            else
                return false;
        }

        private bool AuthenticateUser(string strUser, string strPwd)
        {
            return System.Web.Security.Membership.ValidateUser(strUser, strPwd);
        }
    }
}
