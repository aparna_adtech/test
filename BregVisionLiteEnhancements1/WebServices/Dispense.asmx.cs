﻿using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using ClassLibrary.DAL.Helpers;
using System.Web.Security;

namespace BregVision.WebServices
{
    /// <summary>
    /// Summary description for Dispense
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Dispense : System.Web.Services.WebService
    {
        public SecuredWebServiceHeader SoapHeader;

        [WebMethod]
        [System.Web.Services.Protocols.SoapHeader("SoapHeader")]
        public DataSet SearchForProducts(int practiceLocationID, string searchCriteria, string searchText)
        {
            return DAL.PracticeLocation.Inventory.SearchForInventoryItems(practiceLocationID, searchCriteria, searchText);
        }
    }
}
