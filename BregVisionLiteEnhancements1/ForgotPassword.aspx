﻿<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="utf-8" %>

<%@ Register Assembly="RadRotator.Net2" Namespace="Telerik.WebControls" TagPrefix="radR" %>
<%@ Import Namespace="System.Web.Security" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">



    protected void Page_Load(object sender, EventArgs e)
    {
       
        
    }

    
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Breg Vision</title>
<style type="text/css"> 

 .Copyright
 {
    text-align: center;
    font-family: Verdana, Arial;
    font-size: 8pt;
    color: Black;
    /*background-color: #F4F8FA;
    height: 10px;*/
 }
.jewelryImage 
{
    height: 251px;
    width: 153px;    
    float: left;
}
.jewelryRightPane 
{
    height: 251px;
    width: 224px;
    float: left;
}
.jewelryDescription 
{
    height: 168px;
    width: 600px;
    background-image: url(Images/DefaultRotaterBaseImageRight.gif);
}
.jewelryDescription_Text 
{
    font: normal 14px Arial, Verdana, Helvetica;
    color: #575759;
    margin-left: 10px;
    padding-top: 10px;
    margin-right: 5px;
}
.jewelryName
{
    height: 35px;
    width: 224px;
}
.addToCart 
{
    height: 48px;
    width: 224px;
    background-image: url(Images/DefaultRotaterBaseImageRight.gif);    
}
 
</style>


<script language="javascript" type="text/javascript">
// <!CDATA[

function IMG1_onclick() {

}

// ]]>
</script>
</head>
<body>
<form runat="server">
<table width="100%" border="0" cellpadding="0" cellspacing="2">
  <tr>
    <td width="20%">&nbsp;</td>
    <td style="width: 788px"><table width="785" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="500"  ><img src="Images/BregHeader.jpg" width="500" height="74" id="IMG1" onclick="return IMG1_onclick()" /></td>
        <td width="247" align=right>
            &nbsp;</td>
      </tr>
    </table></td>
    <td width="20%">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" style="width: 788px"><img src="Images/HeaderRow.jpg" width="779" height="25" /></td>
    <td>&nbsp;</td>
  </tr>
  <!--<tr>
    <td>&nbsp;</td>
    <td style="width: 788px"><table width="787" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="565"><table width="785" border="0" cellpadding="3">
          <tr>
            <td><h3>Introducing BREG  Vision, Advanced Inventory Management</h3></td>
          </tr>
          <tr>
            <td><p>Inventory control is one of the toughest  challenges for any practice dealing with in-office DME management. Accordingly,  excellent  inventory control can mean the difference between a successful DME program and  a struggling ancillary venture.</p>
              <p> BREG  Vision is an revolutionary inventory system  for your practice, offering   a  simple and efficient means by which to organize and manage orthopedic supplies  </p>
           
              <p>  Use BREG  Vision  to see DME  success in your practice - </p>
              <ul type="disc">
                <li>  Save time and       resources </li>
                <li>Maximize efficiency </li>
                <li>  Reduce ordering and       tracking hassle </li>
                <li>Simplify DME</li>
                <li>  Free       time to spend with patients </li>
              </ul>
             
              <p>To see the “Vision” of the  future in inventory management, click here or email <a href="mailto:orthopracticesolutions@breg.com">orthopracticesolutions@breg.com</a> for more information. </p></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="785" border="0" cellpadding="3">
          <tr>
            <td width="400"><img src="Images/OrthoSelectLogo.gif" width="200" height="112" /></td>
            <td width="367">&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
    <td>&nbsp;</td>
  </tr>-->
  <tr>
    <td style="height: 19px">&nbsp;</td>
    <td style="width: 788px; height: 19px;">
    
     <div style="WIDTH: 100%">
        <table width="100%"> 
            <tr>
                <td align="center">               
                    <asp:PasswordRecovery ID="PasswordRecovery1"
                         runat="server"
                         BackColor="#EFF3FB" 
                         BorderColor="#B5C7DE" 
                         BorderPadding="4" 
                         BorderStyle="Solid" 
                         BorderWidth="1px" 
                         Font-Names="Verdana" 
                         Font-Size="0.8em" 
                         MailDefinition-From="Support@Breg.com"
                         MailDefinition-Subject="Password Reminder">                        
                        <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
                        <SuccessTextStyle Font-Bold="True" ForeColor="#507CD1" />
                        <TextBoxStyle Font-Size="0.8em" />
                        <TitleTextStyle BackColor="#507CD1" Font-Bold="True" Font-Size="0.9em" ForeColor="White" />
                        <SubmitButtonStyle BackColor="White" BorderColor="#507CD1" BorderStyle="Solid" BorderWidth="1px"
                            Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284E98" />
                    </asp:PasswordRecovery>
                </td>
           </tr>
       </table>         
 </div>
    
    </td>
    <td style="height: 19px">&nbsp;</td>
  </tr>
  
</table>
    <table width="100%">
        <tr>
            <td class="Copyright">
                <p>&copy; Breg Copyright 2007</p>
            </td>
        </tr>
 </table>
</form>
</body>
</html>
