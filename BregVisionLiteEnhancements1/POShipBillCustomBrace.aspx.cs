using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Data;


namespace BregVision
{
	public partial class POShipBillCustomBrace : System.Web.UI.Page
	{
		#region properties

		public int? BregVisionOrderID
		{
			get
			{
				string bvoID = ParentBregVisionOrderID.Value;
				int bvoIDVal = 0;
				if (Int32.TryParse(bvoID, out bvoIDVal))
					return bvoIDVal;
				return 0;
			}
			set
			{
				ParentBregVisionOrderID.Value = value.ToString();
			}
		}

		public int? ShoppingCartID
		{
			get
			{
				string shoppingCartID = ParentShoppingCartID.Value;
				int shoppingCartIDVal = 0;
				if (Int32.TryParse(shoppingCartID, out shoppingCartIDVal))
					return shoppingCartIDVal;
				return 0;
			}
			set
			{
				ParentShoppingCartID.Value = value.ToString();
			}
		}

		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
			ShoppingCartID = GetShoppingCartID();

			CustomBrace.DataBind();
		}

		protected Int32 GetShoppingCartID()
		{
			int practiceLocationID = Convert.ToInt32(Session["PracticeLocationID"]);

			Database db = DatabaseFactory.CreateDatabase("BregVisionConnectionString");
			DbCommand dbCommand = db.GetStoredProcCommand("dbo.usp_GetShoppingCartID");
			db.AddInParameter(dbCommand, "PracticeLocationID", DbType.Int32, practiceLocationID);

			Int32 SQLReturn = Convert.ToInt32(db.ExecuteScalar(dbCommand));
			return SQLReturn;
		}
	}
}
