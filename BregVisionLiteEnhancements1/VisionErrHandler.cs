using System;
using System.Web;
using System.Diagnostics;
using System.Web.Mail;
using System.Configuration;
using System.Web.SessionState;
using System.IO;
using System.Text;
using System.Web.UI;
using BregVision;

namespace BregVision.ErrHandler
{
    public class VisionErrHandler : BregWcfService.VisionErrHandler
    {
        public static void RaiseError(Exception ext)
        {
            var appendHTML = string.Empty;
            try
            {
                var userErrorInfo = _GetUserErrorInfo();
                var userData = _GetUserData();

                var url = "UNKNOWN";
                try
                {
                    url = HttpContext.Current.Request.Url.ToString();
                }
                catch { /* leave url as unkown if error retrieving */ }

                appendHTML =
                    string.Format(@"<pre>
{0}
{1}
{2}
</pre>",
                        "URL = " + url,
                        (userErrorInfo != null) ? userErrorInfo.ToString() : string.Empty,
                        (userData != null) ? userData.ToString() : string.Empty);
            }
            catch
            {
                appendHTML = string.Empty;
            }

            try
            {

                BregWcfService.VisionErrHandler.Current.RaiseError(ext, appendHTML);
            }
            catch { }
        }

        private class UserErrorInfo
        {
            public string userName { get; set; }
            public int eUserID { get; set; }
            public int ePracticeID { get; set; }
            public string ePracticeName { get; set; }
            public int practiceLocationID { get; set; }
            public string practiceLocationName { get; set; }

            public string errString
            {
                get
                {
                    return
                        string.Format(@"User = {1} : {0}
Practice = {3} : {2}
PracticeLocation = {5} : {4}",
                             eUserID.ToString(), userName,
                             ePracticeID.ToString(), ePracticeName,
                             practiceLocationID.ToString(), practiceLocationName);
                }
            }

            public override string ToString()
            {
                return errString;
            }
        }

        private static UserErrorInfo _GetUserErrorInfo()
        {
            try
            {
                var errorInfo =
                    new UserErrorInfo
                    {
                        ePracticeID = HttpContext.Current.Session.GetInt32("PracticeID"),
                        ePracticeName = HttpContext.Current.Session.GetString("PracticeName"),
                        eUserID = HttpContext.Current.Session.GetInt32("UserID"),
                        practiceLocationID = HttpContext.Current.Session.GetInt32("PracticeLocationID"),
                        practiceLocationName = HttpContext.Current.Session.GetString("PracticeLocationName"),
                        userName = HttpContext.Current.User.Identity.Name
                    };

                return errorInfo;
            }
            catch
            {
                return null;
            }
        }

        private class UserData
        {
            public string BrowserType { get; set; }
            public string Platform { get; set; }
            public bool IsCookiesSupported { get; set; }
            public System.Version JavaScriptVersion { get; set; }
            public bool IsMobileBrowser { get; set; }
            public string UserIPAddress { get; set; }

            public override string ToString()
            {
                return
                    string.Format(@"Browser Type = {0}
Platform = {1}
Supports Cookies = {2}
Supports JavaScript = {3}
IsMobileBrowser = {4}
User's IP Address = {5}",
                        BrowserType,
                        Platform,
                        IsCookiesSupported,
                        JavaScriptVersion,
                        IsMobileBrowser,
                        UserIPAddress);
            }
        }

        private static UserData _GetUserData()
        {
            try
            {
                var browser = HttpContext.Current.Request.Browser;
                var userdata =
                    new UserData
                    {
                        BrowserType = browser.Type,
                        IsCookiesSupported = browser.Cookies,
                        JavaScriptVersion = browser.EcmaScriptVersion,
                        IsMobileBrowser = browser.IsMobileDevice,
                        Platform = browser.Platform,
                        UserIPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString()
                    };
                return userdata;
            }
            catch
            {
                return null;
            }
        }

        public string ShowAlert(string myMessage, bool useHtml, bool goBack)
        {
            string message = "alert('" + myMessage + "');";
            string back = "";
            if (goBack)
            {
                back = "window.history.go(-1);";
            }
            string output = "<script>";
            output += message;
            output += back;
            output += "</script>";
            if (useHtml)
            {
                output = "<html><head></head><body>" + output + "</body></html>";
            }
            return output;
        }
    }
}
