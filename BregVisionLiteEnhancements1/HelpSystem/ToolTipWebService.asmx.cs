﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ClassLibrary.BLL.HelpSystem;

namespace BregVision.HelpSystem
{
    /// <summary>
    /// Summary description for ToolTipWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ToolTipWebService : System.Web.Services.WebService
    {

        [WebMethod]
        public string GetToolTip(DataContext context)
        {
            HelpArticleResponse ret = new HelpArticleResponse();
            try
            {
                ret.HelpArticle = HelpArticle.GetHelpArticle(int.Parse(context.Value));

                // create javascript links to related content from related articles and media
                if (ret.HelpArticle.Description == null)
                {
                    return "<div align=\"left\">Not Found: " + context.Value + "<br/>(" + ret.HelpArticle.Id + ")</div>";
                }
                else
                {
                    //ID for debugging
                    //return "<div align=\"left\">" + ret.HelpArticle.Description + "<br/>(" + ret.HelpArticle.Id + ")</div>"; ;

                    return "<div align=\"left\">" + ret.HelpArticle.Description + "</div>"; ;
                }
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
                return ret.Exception.Message;
            }
        }

        [Serializable]
        public class DataContext
        {
            public string TargetControlID;
            public string Value;
        }
        [WebMethod]
        public string Test()
        {
            HelpArticleResponse ret = new HelpArticleResponse();
            try
            {
                ret.HelpArticle = HelpArticle.GetHelpArticle(60);
                return ret.HelpArticle.Description;
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
                return ret.Exception.Message;
            }

            
        }
    }
}
