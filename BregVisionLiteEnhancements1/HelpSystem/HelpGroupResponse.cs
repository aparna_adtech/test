﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using ClassLibrary.BLL.HelpSystem;

namespace BregVision
{
    [DataContract]
    public class HelpGroupResponse
    {
        HelpGroup _HelpGroup;
        [DataMember]
        public HelpGroup HelpGroup
        {
            get { return _HelpGroup; }
            set { _HelpGroup = value; }
        }

        Exception _Exception;
        [DataMember]
        public Exception Exception
        {
            get { return _Exception; }
            set { _Exception = value; }
        }
    }
}
