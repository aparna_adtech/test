﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using ClassLibrary.BLL.HelpSystem;

namespace BregVision
{
    [DataContract]
    public class HelpArticleResponse
    {
        HelpArticle _HelpArticle;
        [DataMember]
        public HelpArticle HelpArticle
        {
            get { return _HelpArticle; }
            set { _HelpArticle = value; }
        }

        Exception _Exception;
        [DataMember]
        public Exception Exception
        {
            get { return _Exception; }
            set { _Exception = value; }
        }
    }
}
