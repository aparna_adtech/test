﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using ClassLibrary.BLL.HelpSystem;

namespace BregVision.HelpSystem
{
    [DataContract]
    public class HelpEntityResponse
    {
        HelpEntity _HelpEntity;
        [DataMember]
        public HelpEntity HelpEntity
        {
            get { return _HelpEntity; }
            set { _HelpEntity = value; }
        }

        Exception _Exception;
        [DataMember]
        public Exception Exception
        {
            get { return _Exception; }
            set { _Exception = value; }
        }
    }
}
