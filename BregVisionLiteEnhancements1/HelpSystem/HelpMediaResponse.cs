﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using ClassLibrary.BLL.HelpSystem;

namespace BregVision
{
    [DataContract]
    public class HelpMediaResponse
    {
        HelpMedia _HelpMedia;
        [DataMember]
        public HelpMedia HelpMedia
        {
            get { return _HelpMedia; }
            set { _HelpMedia = value; }
        }

        Exception _Exception;
        [DataMember]
        public Exception Exception
        {
            get { return _Exception; }
            set { _Exception = value; }
        }
    }
}
