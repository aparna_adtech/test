﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ClassLibrary.BLL.HelpSystem;
using BregVision.HelpSystem;

namespace BregVision
{
    // NOTE: If you change the interface name "IHelpSystemService" here, you must also update the reference to "IHelpSystemService" in Web.config.
    [ServiceContract]
    public interface IHelpSystemService
    {
        
        [OperationContract]
        HelpGroupResponse GetHelpGroup(int id, int depth);

        [OperationContract]
        HelpArticleResponse GetHelpArticle(int id);

        [OperationContract]
        HelpGroupResponse SearchForHelpArticlesAndHelpMeida(string searchText);

        [OperationContract]
        HelpGroupResponse InsertHelpGroup(HelpGroup entity);

        [OperationContract]
        HelpArticleResponse InsertHelpArticle(HelpArticle entity);

        [OperationContract]
        HelpMediaResponse InsertHelpMedia(HelpMedia entity);

        [OperationContract]
        HelpEntityResponse InsertHelpEntityMap(HelpEntity entity);

        [OperationContract]
        HelpGroupResponse UpdateHelpGroup(HelpGroup entity);

        [OperationContract]
        HelpArticleResponse UpdateHelpArticle(HelpArticle entity);

        [OperationContract]
        HelpMediaResponse UpdateHelpMedia(HelpMedia entity);

        [OperationContract]
        HelpEntityResponse UpdateHelpEntityMap(HelpEntity entity);

        [OperationContract]
        HelpEntityResponse DeleteHelpEntity(HelpEntity entity, bool removeEntity);

        [OperationContract]
        bool Test();
    }
}
