﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ClassLibrary.BLL.HelpSystem;
using BregVision.HelpSystem;

namespace BregVision
{
    // NOTE: If you change the class name "HelpSystemService" here, you must also update the reference to "HelpSystemService" in Web.config.
    public class HelpSystemService : IHelpSystemService
    {
        public HelpGroupResponse GetHelpGroup(int id, int depth)
        {
            HelpGroupResponse ret = new HelpGroupResponse();

            try
            {
                ret.HelpGroup = HelpGroup.GetHelpGroup(id, depth);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public HelpArticleResponse GetHelpArticle(int id)
        {
            HelpArticleResponse ret = new HelpArticleResponse();

            try
            {
                ret.HelpArticle = HelpArticle.GetHelpArticle(id);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }


        public HelpGroupResponse SearchForHelpArticlesAndHelpMeida(string searchText)
        {
            HelpGroupResponse ret = new HelpGroupResponse();

            try
            {
                ret.HelpGroup = HelpGroup.SearchForHelpArticlesAndHelpMeida(searchText);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public HelpGroupResponse InsertHelpGroup(HelpGroup entity)
        {
            HelpGroupResponse ret = new HelpGroupResponse();
            try
            {
                ret.HelpGroup = HelpGroup.Insert(entity);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public HelpArticleResponse InsertHelpArticle(HelpArticle entity)
        {
            HelpArticleResponse ret = new HelpArticleResponse();
            try
            {
                ret.HelpArticle = HelpArticle.Insert(entity);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public HelpMediaResponse InsertHelpMedia(HelpMedia entity)
        {
            HelpMediaResponse ret = new HelpMediaResponse();
            try
            {
                ret.HelpMedia = HelpMedia.Insert(entity);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public HelpEntityResponse InsertHelpEntityMap(HelpEntity entity)
        {
            HelpEntityResponse ret = new HelpEntityResponse();
            try
            {
                ret.HelpEntity = HelpEntity.InsertHelpEntityMap(entity);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public HelpEntityResponse DeleteHelpEntity(HelpEntity entity, bool removeEntity)
        {
            HelpEntityResponse ret = new HelpEntityResponse();
            ret.HelpEntity = entity; //if exception is null then success as the proc can thow user errors
            
            try
            {
                HelpEntity.DeleteHelpEntity(entity, removeEntity);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public HelpGroupResponse UpdateHelpGroup(HelpGroup entity)
        {
            HelpGroupResponse ret = new HelpGroupResponse();
            try
            {
                ret.HelpGroup = HelpGroup.Update(entity);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public HelpArticleResponse UpdateHelpArticle(HelpArticle entity)
        {
            HelpArticleResponse ret = new HelpArticleResponse();
            try
            {
                ret.HelpArticle = HelpArticle.Update(entity);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public HelpMediaResponse UpdateHelpMedia(HelpMedia entity)
        {
            HelpMediaResponse ret = new HelpMediaResponse();
            try
            {
                ret.HelpMedia = HelpMedia.Update(entity);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }

        public HelpEntityResponse UpdateHelpEntityMap(HelpEntity entity)
        {
            HelpEntityResponse ret = new HelpEntityResponse();
            try
            {
                ret.HelpEntity = HelpEntity.UpdateHelpEntityMap(entity);
            }
            catch (Exception ex)
            {
                ret.Exception = ex;
            }

            return ret;
        }


        public bool Test()
        {
            return true;
        }

    }
}
