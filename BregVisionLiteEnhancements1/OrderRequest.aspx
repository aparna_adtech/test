<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/SiteMaster.Master" CodeBehind="OrderRequest.aspx.cs" Inherits="BregVision.OrderRequest" %>

 <asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">


<table width="96%" border="0">
    <tr>
        <td colspan=3>
        <b>Order Confirmation</b>
        </td>
     </tr>
  <tr>
    <td width="21">&nbsp;</td>
    <td>
    
    A request for a purchase order has been created and emailed to the practice administrator for approval.<br /><br />
    You will receive an email shortly with with an approval or denial of the purchase order.<br /><br />
    Please contact your practice administrator if you have any questions. 
  
    </td>
    <td width="21">&nbsp;</td>

    
  
  </tr>
  </table>

 </asp:Content>

