<%@ Page Language="C#" MasterPageFile="~/MasterPages/SiteMaster.Master" AutoEventWireup="true"
  ValidateRequest="true" CodeBehind="Dispense.aspx.cs" Inherits="BregVision.Dispense"
  Title="Breg Vision - Dispense" %>

<%@ MasterType VirtualPath="~/MasterPages/SiteMaster.Master" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/Admin/UserControls/PracticeLocation/PracticeLocationDispensementModifications.ascx"
  TagName="PracticeLocationDispensementModifications" TagPrefix="bvl" %>
<%@ Register Src="~/UserControls/DispensePage/DispenseFavorites.ascx" TagName="DispenseFavorites"
  TagPrefix="bvu" %>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
  <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    <AjaxSettings>
      <telerik:AjaxSetting AjaxControlID="rmpMain">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="rmpMain" />
          <telerik:AjaxUpdatedControl ControlID="pvDispense" LoadingPanelID="LoadingPanel1" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="btnAddBrowseToDispensement">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="lblBrowseDispense" />
          <telerik:AjaxUpdatedControl ControlID="grdInventory" LoadingPanelID="LoadingPanel1" />
          <telerik:AjaxUpdatedControl ControlID="lblSummary" UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="lblErrorMsg"
            UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="grdSummary2"
            UpdatePanelRenderMode="Inline" LoadingPanelID="LoadingPanel1" />
          <telerik:AjaxUpdatedControl ControlID="lblSummary2"
            UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="lblErrorMsg2"
            UpdatePanelRenderMode="Inline" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="RadToolbar1">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="grdInventory" LoadingPanelID="LoadingPanel1" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="grdInventory">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="grdInventory"
            LoadingPanelID="LoadingPanel1" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="btnAddCustomBraceToDispensement">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="lblBrowseDispenseCustomBrace" />
          <telerik:AjaxUpdatedControl ControlID="grdCustomBrace"
            LoadingPanelID="LoadingPanel1" />
          <telerik:AjaxUpdatedControl ControlID="lblSummary"
            UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="lblErrorMsg"
            UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="grdSummary2"
            LoadingPanelID="LoadingPanel1" UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="lblSummary2"
            UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="lblErrorMsg2"
            UpdatePanelRenderMode="Inline" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="grdCustomBrace">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="grdCustomBrace"
            LoadingPanelID="LoadingPanel1" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="btnPrintReceiptHistory"></telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="btnDownloadReceipt">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="btnDownloadReceipt" LoadingPanelID="LoadingPanel1" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="btnResendPrintDispensement">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="lblReceiptHistory" />
          <telerik:AjaxUpdatedControl ControlID="grdReceiptHistory"
            LoadingPanelID="LoadingPanel1" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="grdReceiptHistory">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="lblReceiptHistory" />
          <telerik:AjaxUpdatedControl ControlID="grdReceiptHistory"
            LoadingPanelID="LoadingPanel1" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="btnSaveDispenseQueue">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="lblSummary" UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="lblErrorMsg"
            UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="grdSummary2"
            UpdatePanelRenderMode="Inline" LoadingPanelID="LoadingPanel1" />
          <telerik:AjaxUpdatedControl ControlID="lblSummary2"
            UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="lblErrorMsg2"
            UpdatePanelRenderMode="Inline" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="btnDispenseProducts">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="rmpMain" />
          <telerik:AjaxUpdatedControl ControlID="pvDispense"
            LoadingPanelID="LoadingPanel1" />
          <telerik:AjaxUpdatedControl ControlID="lblSummary"
            UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="lblErrorMsg"
            UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="grdSummary2" LoadingPanelID="LoadingPanel1"
            UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="lblSummary2"
            UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="lblErrorMsg2"
            UpdatePanelRenderMode="Inline" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="grdSummary2">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="lblSummary" UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="lblErrorMsg"
            UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="grdSummary2"
            UpdatePanelRenderMode="Inline" LoadingPanelID="LoadingPanel1" />
          <telerik:AjaxUpdatedControl ControlID="lblSummary2"
            UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="lblErrorMsg2"
            UpdatePanelRenderMode="Inline" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="btnSaveDispenseQueue2">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="lblSummary"
            UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="lblErrorMsg"
            UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="grdSummary2"
            LoadingPanelID="LoadingPanel1" UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="lblSummary2"
            UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="lblErrorMsg2"
            UpdatePanelRenderMode="Inline" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="btnDispenseProducts2">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="rmpMain" />
          <telerik:AjaxUpdatedControl ControlID="pvDispense"
            LoadingPanelID="LoadingPanel1" />
          <telerik:AjaxUpdatedControl ControlID="lblSummary"
            UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="lblErrorMsg"
            UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="grdSummary2" LoadingPanelID="LoadingPanel1"
            UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="lblSummary2"
            UpdatePanelRenderMode="Inline" />
          <telerik:AjaxUpdatedControl ControlID="lblErrorMsg2"
            UpdatePanelRenderMode="Inline" />
        </UpdatedControls>
      </telerik:AjaxSetting>
    </AjaxSettings>
  </telerik:RadAjaxManager>
  <telerik:RadWindowManager runat="server" RestrictionZoneID="offsetElement" ID="RadWindowManager1">
    <Windows>
      <telerik:RadWindow ID="scheduledPatients" NavigateUrl="PatientSchedulePopup.aspx"
        Modal="true" runat="server" ReloadOnShow="true" VisibleOnPageLoad="false" Top="60"
        Left="560" Behaviors="Reload,Close" VisibleStatusbar="False" OnClientClose="patientWindowClose" 
        Skin="Breg" EnableEmbeddedSkins="False">
      </telerik:RadWindow>
    </Windows>
  </telerik:RadWindowManager>
  <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
      

      function showDispenseQueue() {
        var splitter = $find("<%= splitterDispense.ClientID %>");
        var pane = splitter.GetPaneById("<%= paneDispenseQueue.ClientID %>");
        if (pane && pane.get_collapsed()==true) {
          pane.expand(2);
        }
      }

      function hideDispenseQueue() {
        var splitter = $find("<%= splitterDispense.ClientID %>");
        var pane = splitter.GetPaneById("<%= paneDispenseQueue.ClientID %>");
        if (pane && pane.get_collapsed()==false) {
          pane.collapse(1);
        }
      }

      function stopProp(e) {
          if (!e)
              e = window.event;

          //IE9 & Other Browsers
          if (e.stopPropagation) {
              e.stopPropagation();
          }
              //IE8 and Lower
          else {
              e.cancelBubble = true;
          }
      }

      function GetRadWindow() {
        var oWindow = null;
        if (window.radWindow)
          oWindow = window.radWindow;
        else if (window.frameElement.radWindow)
          oWindow = window.frameElement.radWindow;
        return oWindow;
      }

      function PopupArguments() {
        this.DispenseQueueIndex = -1;
        this.CboICD9ClientId = "";
      }

      function PatientSchedule() {
        this.PatientName = "";
        this.PatientFirstName = "";
        this.PatientLastName = "";
        this.PatientId = "";
        this.PhysicianName = "";
        this.PatientEmail = "";
        this.ScheduledVisit = new Date("1/1/1900");
        this.ICD9 = "";
        this.PopupArguments = new PopupArguments();
      }

      function OpenWindow(gridItemIndex, cboICD9ClientId) {
        //alert(cboICD9ClientId);
        var wnd = $find("<%= scheduledPatients.ClientID %>");
        var popupArguments = new PopupArguments();
        popupArguments.DispenseQueueIndex = gridItemIndex;
        popupArguments.CboICD9ClientId = cboICD9ClientId;
        wnd.argument = popupArguments;
        wnd.setSize(1000, 600);
        wnd.show();
        return false;
      }

      function patientWindowClose(sender, args) {
        if (args.get_argument() != null) {
          var patientSchedule = args.get_argument();
          var grid = $find("<%= grdSummary2.ClientID %>");
          var MasterTable = grid.get_masterTableView();
          var parent = MasterTable.get_dataItems()[patientSchedule.PopupArguments.DispenseQueueIndex];
            var row = parent.get_nestedViews()[0].get_dataItems()[0];
          var txtPatientCode = row.findElement("txtPatientCode");
          txtPatientCode.value = patientSchedule.PatientId;
          var cboPhysicians = row.findElement("cboPhysicians");
          for (var i = 0; i < cboPhysicians.options.length; i++) {
            if (cboPhysicians.options[i].text == patientSchedule.PhysicianName) {
              cboPhysicians.selectedIndex = i;
              break;
            }
          }
          var cboICD9 = $find(patientSchedule.PopupArguments.CboICD9ClientId);
          var item = cboICD9.findItemByText(patientSchedule.ICD9);
          if (item) {
            item.select();
          } else {
              cboICD9.set_text(patientSchedule.ICD9);
          }
          var txtPatientEmail = row.findElement("txtPatientEmail");
          txtPatientEmail.value = patientSchedule.PatientEmail;

          var txtPatientFirstName = row.findElement("txtPatientFirstName");
          txtPatientFirstName.value = patientSchedule.PatientFirstName;
          var txtPatientLastName = row.findElement("txtPatientLastName");
          txtPatientLastName.value = patientSchedule.PatientLastName;
        }
      }

      function ChangeRowColor(rowID) {
        var color = document.getElementById(rowID).style.backgroundColor;

        if (color != '#f6d28d') {
          oldColor = color;
        }
        if (color == '#f6d28d') {
          document.getElementById(rowID).style.backgroundColor = oldColor;
        } else {
          document.getElementById(rowID).style.backgroundColor = '#f6d28d';
        }
      }

      function scrollTop() {
        window.document.body.scrollTop = 0;
        window.document.documentElement.scrollTop = 0;
      }

      function CheckQuantityToDispense(ddlSideID, txtQuantity) {
          var side = document.getElementById(ddlSideID);
          var quantity = document.getElementById(txtQuantity);
          var quantityValue = document.getElementById(txtQuantity + '_Value');
          var quantityText = document.getElementById(txtQuantity + '_text');
          var spanMsg = document.getElementById('reqAlert');
          if(side.value == 'BI'){
              if (parseInt(quantity.value) % 2 != 0) {
                  quantity.value = '';
                  quantityValue.value = '';
                  quantityText.value = '';
                  spanMsg.style.display = 'block';
              }
              else
                  spanMsg.style.display = 'none';
          }
      }
      function CalculateQuantityForSide(ddlSideID, ddlSideOldID, txtQuantity) {
          try {
              var side = document.getElementById(ddlSideID);
              var sideOld = document.getElementById(ddlSideOldID);
              var quantity = document.getElementById(txtQuantity + '_Value');
              var quantityText = document.getElementById(txtQuantity + '_text');
              if (side.value !== sideOld.value) {
                  if (side.value == 'BI') {
                      var quantityBeforeChange = parseInt(quantity.value);
                      var calculatedQuantity = 2 * quantityBeforeChange;
                      quantity.value = calculatedQuantity;
                      quantityText.value = calculatedQuantity;
                  }else if (sideOld.value == 'BI') {
                      var quantityBeforeChange = parseInt(quantity.value);
                      var calculatedQuantity = quantityBeforeChange / 2;
                      quantity.value = calculatedQuantity;
                      quantityText.value = calculatedQuantity;
                  }

                  sideOld.value = side.value;
              }
          } catch (ex) {
          }
      }

      /*function ChangeChecked(isMedicareID, abnFormID, controlChangedID) {
        ChangeChecked(isMedicareID, abnFormID, 'empty', controlChangedID, 'empty', false, 'empty', 0, 'empty', 'empty', false, 'empty', 'empty', 'empty');//todo cleanup
      }*/

      function _GetQueueIconImageUrl(doesProductRequirePreAuth, isCustomFitEnabledForPractice, isProductASplitCodeProduct, isProductSetAsAlwaysOffTheShelf, isProductACustomBrace, isItemFlaggedAsMedicare, isItemSelectedAsOffTheShelf) {
        var _isPreAuth = doesProductRequirePreAuth && !isItemFlaggedAsMedicare;
        var _isCustomFit = isCustomFitEnabledForPractice && isProductASplitCodeProduct && !(isProductSetAsAlwaysOffTheShelf || isItemSelectedAsOffTheShelf);
        var _isCustomFab = isProductACustomBrace;

        var url = '';

        if (_isCustomFab == true)
        {
          url =
            _isPreAuth
              ? 'App_Themes/Breg/images/Common/pre-auth-custom-fab.png'
              : 'App_Themes/Breg/images/Common/custom-fab.png';
        }
        else if (_isCustomFit == true) {
          url =
            _isPreAuth
              ? 'App_Themes/Breg/images/Common/pre-auth-custom-fit.png'
              : 'App_Themes/Breg/images/Common/custom-fit.png';
        }
        else {
          url =
            _isPreAuth
              ? 'App_Themes/Breg/images/Common/Pre-Auth.png'
              : '';
        }

        return url;
      }

        function ChangeChecked(isMedicareID, abnFormID, controlChangedID, blnABNNeeded, dmeDepositID, actualDMEDeposit, cashOverrideID, queueIconID, preAuthorization, preAuthorizationID, billingChargeID, actualBillingCharge, isCustomFitEnabledForPractice, isProductASplitCodeProduct, isProductSetAsAlwaysOffTheShelf, isProductACustomBrace, cboSplitCodeID) {
        try {
          var isMedicare = document.getElementById(isMedicareID);
          var abnForm = document.getElementById(abnFormID);
          var controlChanged = document.getElementById(controlChangedID);
          var lblPreAuthorization = document.getElementById(preAuthorizationID);
          var lblBillingCharge = document.getElementById(billingChargeID);
          var cboSplitCode = document.getElementById(cboSplitCodeID);
          var queueIcon;
          var txtDMEDeposit;
          var cashOverride;

          if (dmeDepositID != 'empty') {
              txtDMEDeposit = $find(dmeDepositID); 
          }
          if (cashOverrideID != 'empty') {
              cashOverride = document.getElementById(cashOverrideID);
          }
          if (queueIconID != 'empty') {
              queueIcon = document.getElementById(queueIconID);
          }

          if (isMedicare.checked == false) {
            if (abnForm.value == 'Medicare') {
              if (isMedicare == controlChanged) {
                abnForm.value = '';
              } else {
                  isMedicare.checked = true;
              }
            }
          } else {
            if (abnForm.value == 'Commercial')
              isMedicare.checked = false;
            if (blnABNNeeded == true) {
              abnForm.checked = blnABNNeeded;
            }
          }
          //alert(isMedicare.checked);
          if (dmeDepositID != 'empty' && cashOverrideID != 'empty') {
             if (cashOverride.checked == true) {
                  txtDMEDeposit.set_value(0);
                  lblBillingCharge.innerHTML = "$0.00";
             } else if (isMedicare.checked == true && cashOverride.checked == false) {
                 txtDMEDeposit.set_value(0);
             }
              else {
                  txtDMEDeposit.set_value(actualDMEDeposit);
                  lblBillingCharge.innerHTML = actualBillingCharge;
            }
          }

          // pre-auth label visibility
          if (isMedicare.checked == true) {
              lblPreAuthorization.setAttribute("class", "preAuthorization display-none");
          } else {
              lblPreAuthorization.setAttribute("class", "preAuthorization");
          }

          // update queue icon
          var url = _GetQueueIconImageUrl(preAuthorization == 'True', isCustomFitEnabledForPractice == 'True', isProductASplitCodeProduct == 'True', isProductSetAsAlwaysOffTheShelf == 'True', isProductACustomBrace == 'True', isMedicare.checked, cboSplitCode.value == 'Off the Shelf');
          if (url == '') {
              queueIcon.setAttribute("class", "queueIcon display-none");
          } else {
              queueIcon.setAttribute("src", url);
              queueIcon.setAttribute("class", "queueIcon");
          }
        } catch (ex) {
            console.log(ex)
        }
      }

    </script>
  </telerik:RadCodeBlock>
  <telerik:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server" InitialDelayTime="500" EnableAjaxSkinRendering="True" BackgroundPosition="Center" Skin="Breg" EnableEmbeddedSkins="False">
    <img src="App_Themes/Breg/images/Common/loading.gif" />
  </telerik:RadAjaxLoadingPanel>
  <div class="flex-row subWelcome-bar">
    <h1>Dispense</h1>
    <asp:CheckBox ID="chkABNCostIsCash" runat="server" Visible="false" Text="SET ABN COST TO CASH"
      AutoPostBack="true" OnCheckedChanged="chkABNCostIsCash_OnCheckChanged" />
    <telerik:RadTabStrip ID="rmpMain" runat="server" AutoPostBack="True" MultiPageID="pvDispense"
      OnTabClick="rmpMain_TabClick" Width="100%">
      <Tabs>
        <telerik:RadTab ID="Tab1" runat="server" PageViewID="PageView1" Text="Browse/Search"></telerik:RadTab>
        <telerik:RadTab ID="Tab2" runat="server" PageViewID="PageView8" Text="Browse/Search Custom Brace"></telerik:RadTab>
        <telerik:RadTab ID="Tab7" runat="server" PageViewID="PageView7" Text="Receipt History"></telerik:RadTab>
        <telerik:RadTab ID="tabDispenseMod" runat="server" Enabled="true" PageViewID="pagDispenseMod" Text="Dispense Modification"></telerik:RadTab>
      </Tabs>
    </telerik:RadTabStrip>
  </div>
  <div class="subTabContent-container">
    <div class="flex-row align-start">
      <telerik:RadSplitter runat="server" ID="splitterDispense" RenderMode="Lightweight" Height="100%" Width="100%" Skin="Breg" EnableEmbeddedSkins="False">
        <telerik:RadPane ID="paneDispenseTabContent" runat="server">
          <div class="flex-column flex-auto">
            <telerik:RadMultiPage ID="pvDispense" runat="server" RenderSelectedPageOnly="True">
              <%-- SUB-TAB : BROWSE / SEARCH --%>
              <telerik:RadPageView ID="PageView1" runat="server">
                <div class="content">
                  <div class="flex-row actions-bar">
                    <div class="flex-row search-container">
                      <telerik:RadToolBar ID="RadToolbar1" runat="server" AutoPostBack="True"
                        EnableViewState="True" OnButtonClick="RadToolbar1_ButtonClick" UseFadeEffect="True" Skin="Breg" EnableEmbeddedSkins="False" style="z-index: 3000;">
                        <Items>
                          <telerik:RadToolBarButton ID="rttbFilter" runat="server">
                            <ItemTemplate>
                              <asp:TextBox ID="tbFilter" runat="server" placeholder="Search for Products"></asp:TextBox>
                            </ItemTemplate>
                          </telerik:RadToolBarButton>
                          <telerik:RadToolBarButton ID="rttbSearchFilter" runat="server">
                            <ItemTemplate>
                              <telerik:RadComboBox ID="cboSearchFilter" runat="server" ExpandEffect="fade" Width="100" Skin="Breg" EnableEmbeddedSkins="False">
                                <Items>
                                  <telerik:RadComboBoxItem ID="RadComboBoxItem3" runat="server" Text="Code" Value="Code" />
                                </Items>
                                <Items>
                                  <telerik:RadComboBoxItem ID="RadComboBoxItem5" runat="server" Text="Product Name"
                                    Value="Name" />
                                </Items>
                                <Items>
                                  <telerik:RadComboBoxItem ID="RadComboBoxItem2" runat="server" Text="Manufacturer"
                                    Value="Brand" />
                                </Items>
                                <Items>
                                  <telerik:RadComboBoxItem ID="RadComboBoxItem1" runat="server" Text="HCPCs" Value="HCPCs" Selected="True" />
                                </Items>
                                <Items>
                                  <telerik:RadComboBoxItem ID="rcbiUPCCode" runat="server" Text="UPC Code" Value="UPCCODE" />
                                </Items>
                              </telerik:RadComboBox>
                            </ItemTemplate>
                          </telerik:RadToolBarButton>
                          <telerik:RadToolBarButton ID="rtbFilter" runat="server" CausesValidation="true" CommandName="Search"
                            Hidden="False" ImageUrl="~/App_Themes/Breg/images/Common/search.png"
                            ToolTip="Search" />
                          <telerik:RadToolBarButton ID="rtbBrowse" runat="server" CausesValidation="true" CommandName="Browse"
                            Hidden="False" ImageUrl="~/App_Themes/Breg/images/Common/browse.png"
                            ToolTip="Browse all products" />
                        </Items>
                      </telerik:RadToolBar>
                      <bvu:Help ID="Help2" runat="server" HelpID="78" />
                      <asp:CheckBox ID="chkScanMode" runat="server" Checked="true" CssClass="flex-row" Text="Put in Dispense Queue<br>if Exact Match" />
                    </div>
                    <div class="no-wrap">
                      <bvu:Help ID="Help1" runat="server" HelpID="77" />
                      <asp:Button ID="btnAddBrowseToDispensement" runat="server" OnClick="btnAddBrowseToDispensement_Click"
                        Text="Add to Dispensement" ToolTip="Add all products selected to dispensement on Summary/Dispense tab." CssClass="button-primary" />
                    </div>
                  </div>
                  <asp:Label ID="lblBrowseDispense" runat="server" CssClass="WarningMsg" Visible="False"></asp:Label>
                  <div class="section">
                    <telerik:RadGrid ID="grdInventory" runat="server" AllowMultiRowSelection="True" AllowPaging="True"
                      AllowSorting="True" EnableViewState="True" GridLines="None" OnDetailTableDataBind="grdInventory_DetailTableDataBind"
                      OnItemCommand="grdInventory_ItemCommand" OnItemDataBound="grdInventory_ItemDataBound"
                      OnNeedDataSource="grdInventory_NeedDataSource" OnPageIndexChanged="grdInventory_PageIndexChanged"
                      OnPreRender="grdInventory_PreRender" ShowGroupPanel="True" Width="98%">
                      <PagerStyle AlwaysVisible="True" Mode="NumericPages" />
                      <MasterTableView AutoGenerateColumns="False" DataKeyNames="SupplierID" EnableTheming="true"
                        HierarchyLoadMode="ServerOnDemand" Name="mtvSupplier">
                        <ExpandCollapseColumn Resizable="false" Visible="true">
                          <HeaderStyle Width="20px" />
                        </ExpandCollapseColumn>
                        <RowIndicatorColumn Visible="true">
                          <HeaderStyle Width="20px" />
                        </RowIndicatorColumn>
                        <Columns>
                          <telerik:GridBoundColumn DataField="SupplierID" HeaderStyle-BorderColor="#A2B6CB"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" UniqueName="SupplierID"
                            Visible="False">
                          </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="Brand" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                            HeaderStyle-BorderWidth="1px" HeaderText="Brand" UniqueName="Brand">
                          </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="Name" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                            HeaderStyle-BorderWidth="1px" HeaderText="Name" UniqueName="Name">
                          </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="PhoneWork" HeaderStyle-BorderColor="#A2B6CB"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderText="Phone"
                            UniqueName="PhoneWork">
                          </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="Address" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                            HeaderStyle-BorderWidth="1px" HeaderText="Address" UniqueName="Address">
                          </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="City" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                            HeaderStyle-BorderWidth="1px" HeaderText="City" UniqueName="City">
                          </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="ZipCode" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                            HeaderStyle-BorderWidth="1px" HeaderText="Zip Code" UniqueName="ZipCode">
                          </telerik:GridBoundColumn>
                        </Columns>
                        <DetailTables>
                          <telerik:GridTableView runat="server" AutoGenerateColumns="False" DataKeyNames="SupplierName"
                            Name="gvwSupplier" Width="98%">
                            <ExpandCollapseColumn Visible="False">
                              <HeaderStyle Width="19px" />
                            </ExpandCollapseColumn>
                            <RowIndicatorColumn Visible="False">
                              <HeaderStyle Width="20px" />
                            </RowIndicatorColumn>
                            <Columns>
                              <telerik:GridClientSelectColumn HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                                HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="35px"
                                ItemStyle-HorizontalAlign="Center" UniqueName="ClientSelectColumn" />
                              <telerik:GridBoundColumn DataField="SupplierID" HeaderStyle-BorderColor="#A2B6CB"
                                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" UniqueName="SupplierID"
                                Visible="False">
                              </telerik:GridBoundColumn>
                              <telerik:GridBoundColumn DataField="ProductInventoryID" HeaderStyle-BorderColor="#A2B6CB"
                                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" UniqueName="ProductInventoryID"
                                Visible="false">
                              </telerik:GridBoundColumn>
                              <telerik:GridBoundColumn DataField="DMEDeposit" HeaderStyle-BorderColor="#A2B6CB"
                                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" UniqueName="DMEDeposit"
                                Visible="False">
                              </telerik:GridBoundColumn>
                              <telerik:GridBoundColumn DataField="ABN_Needed" HeaderStyle-BorderColor="#A2B6CB"
                                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderText="ABN_Needed"
                                UniqueName="ABN_Needed" Visible="false">
                              </telerik:GridBoundColumn>
                              <telerik:GridBoundColumn DataField="SupplierName" HeaderStyle-BorderColor="#A2B6CB"
                                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderText="Supplier"
                                UniqueName="Category" Visible="false">
                              </telerik:GridBoundColumn>
                              <telerik:GridBoundColumn DataField="BrandName" HeaderStyle-BorderColor="#A2B6CB"
                                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderText="Brand"
                                UniqueName="SubCategory">
                              </telerik:GridBoundColumn>
                              <telerik:GridBoundColumn DataField="ProductName" HeaderStyle-BorderColor="#A2B6CB"
                                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderText="Product"
                                UniqueName="Product">
                              </telerik:GridBoundColumn>
                              <telerik:GridBoundColumn DataField="Code" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                                HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50"
                                HeaderStyle-Wrap="true" HeaderText="Code" ItemStyle-HorizontalAlign="center"
                                ItemStyle-Width="50" UniqueName="Code" />
                              <telerik:GridBoundColumn DataField="Side" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                                HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50"
                                HeaderStyle-Wrap="true" HeaderText="Side" ItemStyle-HorizontalAlign="center"
                                ItemStyle-Width="50" UniqueName="Side" />
                              <telerik:GridBoundColumn DataField="Size" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                                HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50"
                                HeaderStyle-Wrap="true" HeaderText="Size" ItemStyle-HorizontalAlign="center"
                                ItemStyle-Width="50" UniqueName="Size" />
                              <telerik:GridBoundColumn DataField="Gender" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                                HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50"
                                HeaderStyle-Wrap="true" HeaderText="Gender" ItemStyle-HorizontalAlign="center"
                                ItemStyle-Width="50" UniqueName="Gender" />
                              <telerik:GridBoundColumn DataField="QuantityOnHand" HeaderStyle-BorderColor="#A2B6CB"
                                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50" HeaderStyle-Wrap="true" HeaderText="On Hand" ItemStyle-HorizontalAlign="center"
                                ItemStyle-Width="50" UniqueName="QuantityOnHand" />
                              <telerik:GridBoundColumn DataField="IsSplitCode" HeaderStyle-BorderColor="#A2B6CB"
                                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50" HeaderStyle-Wrap="true" HeaderText="Split Code" ItemStyle-HorizontalAlign="center"
                                ItemStyle-Width="50" UniqueName="SplitCode" Display="False" />
                              <telerik:GridBoundColumn DataField="IsAlwaysOffTheShelf" HeaderStyle-BorderColor="#A2B6CB"
                                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50" HeaderStyle-Wrap="true" HeaderText="Always OTS" ItemStyle-HorizontalAlign="center"
                                ItemStyle-Width="50" UniqueName="AlwaysOTS" Display="False" />
                                <telerik:GridBoundColumn DataField="Mod1" Display="False" UniqueName="Mod1"/>
                                <telerik:GridBoundColumn DataField="Mod2" Display="False" UniqueName="Mod2"/>
                                <telerik:GridBoundColumn DataField="Mod3" Display="False" UniqueName="Mod3"/>
                            </Columns>
                          </telerik:GridTableView>
                        </DetailTables>
                      </MasterTableView>
                      <GroupPanel Visible="True"></GroupPanel>
                      <ClientSettings AllowColumnsReorder="false" AllowDragToGroup="false" ReorderColumnsOnClient="false">
                        <Selecting AllowRowSelect="True" />
                      </ClientSettings>
                    </telerik:RadGrid>
                  </div>
                </div>
              </telerik:RadPageView>
              <%-- SUB-TAB : BROWSE / SEARCH CUSTOM BRACE --%>
              <telerik:RadPageView ID="PageView8" runat="server">
                <div class="content">
                  <div class="flex-row actions-bar">
                    <telerik:RadToolBar ID="rtbCustomBrace" runat="server" AutoPostBack="True"
                      EnableViewState="True" OnButtonClick="rtbCustomBrace_ButtonClick" style="z-index: 3000;">
                      <Items>
                        <telerik:RadToolBarButton ID="rttbFilterCustomBrace" runat="server">
                          <ItemTemplate>
                            <asp:TextBox ID="tbFilterCustomBrace" runat="server" placeholder="Search for Custom Brace" CssClass="customBraceSearch"></asp:TextBox>
                          </ItemTemplate>
                        </telerik:RadToolBarButton>
                        <telerik:RadToolBarButton ID="rttbSearchFilterCustomBrace" runat="server">
                          <ItemTemplate>
                            <telerik:RadComboBox ID="cboSearchFilterCustomBrace" runat="server" ExpandEffect="fade"
                              Width="100">
                              <Items>
                                <telerik:RadComboBoxItem ID="RadComboBoxItem2" runat="server" Text="Patient Code"
                                  Value="PatientID" />
                              </Items>
                              <Items>
                                <telerik:RadComboBoxItem ID="RadComboBoxItem1" runat="server" Text="PO Number" Value="PONumber" />
                              </Items>
                            </telerik:RadComboBox>
                          </ItemTemplate>
                        </telerik:RadToolBarButton>
                        <telerik:RadToolBarButton ID="rttbSearchCustomBrace" runat="server" CausesValidation="true"
                          CommandName="Search" Hidden="False" ImageUrl="~/App_Themes/Breg/images/Common/search.png"
                          ToolTip="Search" />
                        <telerik:RadToolBarButton ID="rttbBrowseCustomBrace" runat="server" CausesValidation="true"
                          CommandName="Browse" Hidden="False" ImageUrl="~/App_Themes/Breg/images/Common/browse.png"
                          ToolTip="Browse all products" />
                      </Items>
                    </telerik:RadToolBar>
                    <div>
                      <bvu:Help ID="Help7" runat="server" HelpID="77" />
                      <asp:Button ID="btnAddCustomBraceToDispensement" runat="server" OnClick="btnAddCustomBraceToDispensement_Click"
                        Text="Add to Dispensement" ToolTip="Add all products selected to dispensement on Summary/Dispense tab." CssClass="button-primary" />
                    </div>
                  </div>
                  <asp:Label ID="lblBrowseDispenseCustomBrace" runat="server" CssClass="WarningMsg"></asp:Label>
                  <div class="section">
                    <telerik:RadGrid ID="grdCustomBrace" runat="server" AllowMultiRowSelection="False"
                      AllowPaging="True" AllowSorting="True" EnableViewState="True" GridLines="None"
                      OnDetailTableDataBind="grdCustomBrace_DetailTableDataBind" OnItemDataBound="grdCustomBrace_ItemDataBound"
                      OnNeedDataSource="grdCustomBrace_NeedDataSource" OnPageIndexChanged="grdCustomBrace_PageIndexChanged"
                      ShowGroupPanel="True" Width="100%" ClientSettings-EnablePostBackOnRowClick="True" OnItemCommand="grdCustomBrace_OnItemCommand">
                      <PagerStyle AlwaysVisible="true" Mode="NumericPages" />
                      <MasterTableView AutoGenerateColumns="False" DataKeyNames="BregVisionOrderID" EnableTheming="True"
                        HierarchyLoadMode="ServerOnDemand" Name="mtvCustomBraceOutstanding_ParentLevel"
                        RetrieveAllDataFields="true">
                        <ExpandCollapseColumn Resizable="False">
                          <HeaderStyle Width="20px" />
                        </ExpandCollapseColumn>
                        <RowIndicatorColumn Visible="true">
                          <HeaderStyle Width="20px" />
                        </RowIndicatorColumn>
                        <Columns>
                          <telerik:GridClientSelectColumn HeaderStyle-Width="20" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                            HeaderStyle-BorderWidth="1px" UniqueName="ClientSelectColumn"/>
                          <telerik:GridBoundColumn DataField="BregVisionOrderID" HeaderStyle-BorderColor="#A2B6CB"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" UniqueName="BregVisionOrderID"
                            Visible="False">
                          </telerik:GridBoundColumn>
                          <telerik:GridTemplateColumn HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                            HeaderStyle-BorderWidth="1px" HeaderStyle-Width="150px" HeaderText="Provider"
                            UniqueName="Physician">
                            <ItemTemplate>
                              <telerik:RadComboBox ID="cboPhysicians" runat="server" ExpandEffect="fade"></telerik:RadComboBox>
                            </ItemTemplate>
                          </telerik:GridTemplateColumn>
                          <telerik:GridBoundColumn DataField="PurchaseOrder" HeaderStyle-BorderColor="#A2B6CB"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderText="Purchase Order"
                            ItemStyle-HorizontalAlign="Left" UniqueName="PurchaseOrder">
                          </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="CustomPurchaseOrderCode" HeaderText="Custom PO Number"
                            ItemStyle-Width="115px" UniqueName="CustomPurchaseOrderCode">
                          </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="CreatedUserID" HeaderStyle-BorderColor="#A2B6CB"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderText="Created by"
                            UniqueName="CreatedUserID" Visible="false">
                          </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="Status" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                            HeaderStyle-BorderWidth="1px" UniqueName="Status" Visible="false">
                          </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="ShippingType" HeaderStyle-BorderColor="#A2B6CB"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderText="Shipping Type"
                            UniqueName="ShippingType" Visible="false">
                          </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="Total" DataFormatString="{0:C}" HeaderStyle-BorderColor="#A2B6CB"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderText="Total"
                            UniqueName="Total" Visible="false">
                          </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="CreatedDate" HeaderStyle-BorderColor="#A2B6CB"
                            HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderStyle-Width="120px"
                            HeaderText="Date Created" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="120px"
                            UniqueName="CreatedDate">
                          </telerik:GridBoundColumn>
                        </Columns>
                        <DetailTables>
                          <telerik:GridTableView runat="server" AutoGenerateColumns="false" DataKeyNames="ProductInventoryID"
                            HierarchyLoadMode="ServerOnDemand" Name="mtvCustomBraceOutstanding_Detail1Level"
                            RetrieveAllDataFields="true" Width="100%">
                            <ExpandCollapseColumn Resizable="False" Visible="false">
                              <HeaderStyle Width="20px" />
                            </ExpandCollapseColumn>
                            <RowIndicatorColumn Visible="False">
                              <HeaderStyle Width="20px" />
                            </RowIndicatorColumn>
                            <Columns>
                              <telerik:GridBoundColumn DataField="BregVisionOrderID" HeaderStyle-BorderColor="#A2B6CB"
                                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" UniqueName="BregVisionOrderID"
                                Visible="False">
                              </telerik:GridBoundColumn>
                              <telerik:GridBoundColumn DataField="ProductInventoryID" HeaderStyle-BorderColor="#A2B6CB"
                                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" UniqueName="ProductInventoryID"
                                Visible="False">
                              </telerik:GridBoundColumn>
                              <telerik:GridBoundColumn DataField="ProductName" HeaderStyle-BorderColor="#A2B6CB"
                                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderStyle-Width="220px"
                                HeaderText="Product" UniqueName="Product">
                              </telerik:GridBoundColumn>
                              <telerik:GridBoundColumn DataField="Code" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                                HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50"
                                HeaderStyle-Wrap="true" HeaderText="Code" ItemStyle-HorizontalAlign="center"
                                ItemStyle-Width="50" UniqueName="Code" />
                              <telerik:GridTemplateColumn HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                                HeaderStyle-BorderWidth="1px" HeaderStyle-Width="30px" HeaderText="RT / LT Modifier"
                                UniqueName="Side">
                                <ItemTemplate>
                                  <telerik:RadComboBox ID="cboSide" runat="server" ExpandEffect="fade" Width="45px">
                                    <Items>
                                      <telerik:RadComboBoxItem Text="Please Select a Side" Value="" />
                                      <telerik:RadComboBoxItem Text="U" Value="U" />
                                      <telerik:RadComboBoxItem Text="LT" Value="L" />
                                      <telerik:RadComboBoxItem Text="RT" Value="R" />
                                    </Items>
                                  </telerik:RadComboBox>
                                </ItemTemplate>
                              </telerik:GridTemplateColumn>
                              <telerik:GridBoundColumn DataField="Size" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                                HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50"
                                HeaderStyle-Wrap="true" HeaderText="Size" ItemStyle-HorizontalAlign="center"
                                ItemStyle-Width="50" UniqueName="Size" />
                              <telerik:GridBoundColumn DataField="Gender" HeaderStyle-BorderColor="#A2B6CB" HeaderStyle-BorderStyle="Solid"
                                HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50"
                                HeaderStyle-Wrap="true" HeaderText="Gender" ItemStyle-HorizontalAlign="center"
                                ItemStyle-Width="50" UniqueName="Gender" />
                              <telerik:GridBoundColumn DataField="QuantityOnHand" HeaderStyle-BorderColor="#A2B6CB"
                                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50" HeaderStyle-Wrap="true" HeaderText="Quantity" ItemStyle-HorizontalAlign="center"
                                ItemStyle-Width="50" UniqueName="QuantityOnHand" />
                              <telerik:GridBoundColumn DataField="IsSplitCode" HeaderStyle-BorderColor="#A2B6CB"
                                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50" HeaderStyle-Wrap="true" HeaderText="Split Code" ItemStyle-HorizontalAlign="center"
                                ItemStyle-Width="50" UniqueName="SplitCode" Display="False" />
                              <telerik:GridBoundColumn DataField="IsAlwaysOffTheShelf" HeaderStyle-BorderColor="#A2B6CB"
                                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50" HeaderStyle-Wrap="true" HeaderText="Always OTS" ItemStyle-HorizontalAlign="center"
                                ItemStyle-Width="50" UniqueName="AlwaysOTS" Display="False" />
                              <telerik:GridBoundColumn DataField="IsCustomBrace" HeaderStyle-BorderColor="#A2B6CB"
                                HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50" HeaderStyle-Wrap="true" HeaderText="Custom Brace" ItemStyle-HorizontalAlign="center"
                                ItemStyle-Width="50" UniqueName="CustomBrace" Display="False" />
                            </Columns>
                          </telerik:GridTableView>
                        </DetailTables>
                      </MasterTableView>
                      <GroupPanel Visible="True"></GroupPanel>
                      <ClientSettings AllowColumnsReorder="false" AllowDragToGroup="false" ReorderColumnsOnClient="false">
                        <Selecting AllowRowSelect="True" />
                      </ClientSettings>
                    </telerik:RadGrid>
                  </div>
                </div>
              </telerik:RadPageView>
              <%-- SUB-TAB : RECEIPT HISTORY --%>
              <telerik:RadPageView ID="PageView7" runat="server">
                <div class="content">
                  <div class="flex-row actions-bar">
                    <telerik:RadToolBar ID="rtbReceiptHistory" runat="server" AutoPostBack="True"
                      EnableViewState="True" OnButtonClick="rtbReceiptHistory_ButtonClick" style="z-index: 3000;">
                      <Items>
                        <telerik:RadToolBarButton ID="rttbFilterReceiptHistory" runat="server">
                          <ItemTemplate>
                            <asp:TextBox ID="tbFilterReceiptHistory" runat="server" placeholder="Search for Receipt"></asp:TextBox>
                          </ItemTemplate>
                        </telerik:RadToolBarButton>
                        <telerik:RadToolBarButton ID="rttbSearchFilterReceiptHistory" runat="server">
                          <ItemTemplate>
                            <telerik:RadComboBox ID="cboSearchFilterReceiptHistory" runat="server" ExpandEffect="fade"
                              Width="100">
                              <Items>
                                <telerik:RadComboBoxItem ID="RadComboBoxItem3" runat="server" Text="Patient Code"
                                  Value="PatientCode" />
                                <telerik:RadComboBoxItem ID="RadComboBoxItem6" runat="server" Text="Patient Name"
                                  Value="PatientName" />
                                <telerik:RadComboBoxItem ID="RadComboBoxItem4" runat="server" Text="Provider Name"
                                  Value="PhysicianName" />
                                <telerik:RadComboBoxItem ID="RadComboBoxItem7" runat="server" Text="Claim Number"
                                  Value="BCSClaimID" />
                              </Items>
                            </telerik:RadComboBox>
                          </ItemTemplate>
                        </telerik:RadToolBarButton>
                        <telerik:RadToolBarButton ID="rttbSearchReceiptHistory" runat="server" CausesValidation="true"
                          CommandName="Search" Hidden="False" ImageUrl="~/App_Themes/Breg/images/Common/search.png"
                          ToolTip="Search" />
                        <telerik:RadToolBarButton ID="rttbBrowseReceiptHistory" runat="server" CausesValidation="true"
                          CommandName="Browse" Hidden="False" ImageUrl="~/App_Themes/Breg/images/Common/browse.png"
                          ToolTip="Browse all products" />
                      </Items>
                    </telerik:RadToolBar>
                    <div>
                      <asp:Button ID="btnDownloadReceipt" Text="Download Receipt" runat="server" OnClick="btnDownloadReceipt_OnClick" CssClass="button-accent" />
                      <%--<asp:Button ID="btnPrintReceiptHistory" runat="server" OnClick="btnPrintReceiptHistory_Click"
                        Text="Print Preview" CssClass="button-accent" />--%>
                      <asp:Button ID="btnResendPrintDispensement" runat="server" OnClick="btnResendPrintDispensement_Click"
                        Text="Resend Fax Dispensement Receipt" CssClass="button-primary" />
                      <%-- <br /><asp:CheckBox ID="chkIncludePatient" runat="server" CssClass="FieldLabel" Text="Include Patient" /> --%>
                    </div>
                  </div>
                   <asp:Label ID="lblReceiptHistory" runat="server" CssClass="WarningMsg"></asp:Label>
                  <div class="section">
                    <telerik:RadGrid ID="grdReceiptHistory" runat="server" AllowMultiRowSelection="true"
                      AllowPaging="True"  AllowSorting="True" EnableViewState="True" GridLines="None"
                      OnDetailTableDataBind="grdReceiptHistory_DetailTableBind" OnNeedDataSource="grdReceiptHistory_NeedDataSource"
                      ShowGroupPanel="True" Width="100%">
                      <PagerStyle AlwaysVisible="true" Mode="NumericPages" />
                      <MasterTableView AutoGenerateColumns="False" DataKeyNames="DispenseID" EnableTheming="true"
                        HierarchyLoadMode="ServerOnDemand">
                        <SortExpressions>
                          <telerik:GridSortExpression FieldName="DateDispensed" SortOrder="Descending" />
                        </SortExpressions>
                        <Columns>
                          <telerik:GridClientSelectColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                            UniqueName="ClientSelectColumn" />
                          <telerik:GridBoundColumn DataField="DispenseID" UniqueName="DispenseID" Visible="False" />
                          <telerik:GridBoundColumn DataField="DispenseBatchID" UniqueName="DispenseBatchID"
                            Visible="False" />
                          <telerik:GridCheckBoxColumn DataField="HasABNForm" DataType="System.Boolean" HeaderStyle-HorizontalAlign="Center"
                            HeaderStyle-Width="100px" HeaderText="Has ABN Form(s)" ItemStyle-HorizontalAlign="Center"
                            UniqueName="HasABNForm" />
                          <telerik:GridBoundColumn DataField="BCSClaimID" HeaderStyle-HorizontalAlign="Left"
                            HeaderText="Claim Number" ItemStyle-HorizontalAlign="Left" UniqueName="BCSClaimID" />  
                          <telerik:GridBoundColumn DataField="CreatedWithHandheld" HeaderStyle-HorizontalAlign="Center"
                            HeaderText="Created w/<br>Handheld" ItemStyle-HorizontalAlign="Center" UniqueName="Handheld" />
                          <telerik:GridBoundColumn DataField="PatientCode" HeaderStyle-HorizontalAlign="Left"
                            HeaderText="Patient Code" ItemStyle-HorizontalAlign="Left" UniqueName="PatientCode" />
                          <telerik:GridBoundColumn DataField="PatientName" HeaderStyle-HorizontalAlign="Left"
                            HeaderText="Patient Name" ItemStyle-HorizontalAlign="Left" UniqueName="PatientName" />
                          <telerik:GridBoundColumn DataField="Total" DataFormatString="{0:C}" HeaderStyle-HorizontalAlign="Right"
                            HeaderText="Total" ItemStyle-HorizontalAlign="Right" UniqueName="Total" />
                          <telerik:GridBoundColumn DataField="DateDispensed" HeaderStyle-HorizontalAlign="Right"
                            HeaderText="Date Dispensed" ItemStyle-HorizontalAlign="Right" UniqueName="DateDispensed" />
                        </Columns>
                        <DetailTables>
                          <telerik:GridTableView runat="server" AutoGenerateColumns="False" DataKeyNames="DispenseID">
                            <Columns>
                              <telerik:GridBoundColumn DataField="DispenseID" UniqueName="DispenseID" Visible="False" />
                              <telerik:GridBoundColumn DataField="ABNType" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="ABN Type" ItemStyle-HorizontalAlign="Center" UniqueName="ABNType" />
                              <telerik:GridBoundColumn DataField="PhysicianName" HeaderText="Provider" UniqueName="PhysicianName" />
                              <telerik:GridBoundColumn DataField="SupplierShortName" HeaderText="Brand" UniqueName="SupplierShortName" />
                              <telerik:GridBoundColumn DataField="Name" HeaderText="Product" UniqueName="Product" />
                              <telerik:GridBoundColumn DataField="Code" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px"
                                HeaderStyle-Wrap="true" HeaderText="Code" ItemStyle-HorizontalAlign="Center"
                                UniqueName="Code" />
                              <telerik:GridBoundColumn DataField="Side" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px"
                                HeaderStyle-Wrap="true" HeaderText="RT / LT Modifier" ItemStyle-HorizontalAlign="Center"
                                UniqueName="Side" />
                              <telerik:GridBoundColumn DataField="Mod1" HeaderStyle-HorizontalAlign="Center" UniqueName="Mod1" HeaderText="Mod"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Wrap="true" HeaderStyle-Width="50px" />
                              <telerik:GridBoundColumn DataField="Mod2" HeaderStyle-HorizontalAlign="Center" UniqueName="Mod2" HeaderText="Mod"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Wrap="true" HeaderStyle-Width="50px" />
                              <telerik:GridBoundColumn DataField="Mod3" HeaderStyle-HorizontalAlign="Center" UniqueName="Mod3" HeaderText="Mod"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Wrap="true" HeaderStyle-Width="50px" />
                              <telerik:GridBoundColumn DataField="Size" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px"
                                HeaderStyle-Wrap="true" HeaderText="Size" ItemStyle-HorizontalAlign="Center"
                                UniqueName="Size" />
                              <telerik:GridBoundColumn DataField="DMEDeposit" DataFormatString="{0:C}" HeaderText="DME Deposit"
                                UniqueName="DMEDeposit" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                              <telerik:GridBoundColumn DataField="Quantity" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50px" HeaderStyle-Wrap="true" HeaderText="Qty" ItemStyle-HorizontalAlign="Center"
                                UniqueName="Quantity" />
                              <telerik:GridBoundColumn DataField="LineTotal" DataFormatString="{0:C}" HeaderStyle-HorizontalAlign="Right"
                                HeaderText="Billing Charge" ItemStyle-HorizontalAlign="Right" UniqueName="LineTotal" />                         
                            </Columns>
                          </telerik:GridTableView>
                        </DetailTables>
                      </MasterTableView><GroupPanel Visible="True"></GroupPanel><ClientSettings AllowColumnsReorder="false" AllowDragToGroup="false" ReorderColumnsOnClient="false">
                        <Selecting AllowRowSelect="True" />
                      </ClientSettings>
                    </telerik:RadGrid>
                  </div>
                </div>
              </telerik:RadPageView>
              <%-- SUB-TAB : DISPENSE MODIFICATION --%>
              <telerik:RadPageView ID="pagDispenseMod" runat="server">
                <div class="content">
                  <bvl:PracticeLocationDispensementModifications ID="ctlPracticeLocationDispensementModifications" runat="server" />
                </div>
              </telerik:RadPageView>
            </telerik:RadMultiPage>
          </div>
        </telerik:RadPane>
        <telerik:RadSplitBar ID="splitBar" runat="server" CollapseMode="Backward">
        </telerik:RadSplitBar>
        <telerik:RadPane runat="server" ID="paneDispenseQueue" Width="510px" Scrolling="X">
          <asp:Panel ID="pnlDispense" runat="server" Width="100%">
            <div class="content">
              <div class="flex-row actions-bar">
                <h2 class="no-margin no-padding">Dispense Queue&nbsp;<bvu:Help ID="Help5" runat="server" HelpID="83" />
                </h2>
                <div>
                  <asp:Button ID="btnSaveDispenseQueue" runat="server" OnClick="btnSaveDispenseQueue_Click"
                    Text="Save Changes" ToolTip="Save Dispense Queue" ValidationGroup="vgGroup" CssClass="button-accent" />
                  <bvu:Help ID="Help6" runat="server" HelpID="84" />
                  <asp:Button
                    ID="btnDispenseProducts" runat="server" OnClick="btnDispenseProducts_Click" Text="Dispense Product(s)"
                    ToolTip="Dispense product(s)" ValidationGroup="vgGroup" CssClass="button-primary" />
                </div>
              </div>
               <div>
                  <asp:Label ID="lblSummary" runat="server" CssClass="WarningMsg" OnPreRender="LabelPreRenderHideShow"></asp:Label>
                  <asp:Label ID="lblErrorMsg" runat="server" CssClass="WarningMsg" OnPreRender="LabelPreRenderHideShow"></asp:Label>
                </div>
              <telerik:RadGrid ID="grdSummary2" runat="server" AllowMultiRowSelection="true" AllowPaging="true"
                AutoGenerateColumns="false" ItemStyle-HorizontalAlign="Left" OnDeleteCommand="grdSummary2_DeleteCommand"
                OnItemDataBound="grdSummary2_ItemDataBound" OnNeedDataSource="grdSummary2_NeedDataSource" OnDetailTableDataBind="grdSummary2_OnDetailTableDataBind">
                <PagerStyle Mode="NumericPages" AlwaysVisible="True" CssClass="auto"/>
                <ClientSettings>
                  <Selecting AllowRowSelect="true" />
                  <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True"></Scrolling>
                </ClientSettings>
                <MasterTableView DataKeyNames="DispenseQueueID" NoMasterRecordsText="Dispense queue is empty." GroupsDefaultExpanded="True">
                <Columns>
                    <telerik:GridClientSelectColumn UniqueName="chkRowSelect" HeaderStyle-Width="40px" ItemStyle-Width="30px">
                    </telerik:GridClientSelectColumn>
                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" ConfirmText="Are you sure you want to delete this?"
                      ImageUrl="~/App_Themes/Breg/images/Common/delete.png" HeaderStyle-Width="40px" ItemStyle-Width="30px" ItemStyle-VerticalAlign="Middle">
                    </telerik:GridButtonColumn>
                    <telerik:GridTemplateColumn HeaderText="Dispense Queue Item" ItemStyle-HorizontalAlign="Left">
                      <ItemTemplate>
                        <span class="light pull-right textRight">Size:&nbsp;<strong><%# Eval("Size") %></strong><br />Code:&nbsp;<strong><%# Eval("Code") %></strong></span><h4 class="no-margin-top no-margin-bottom">
                           <%# Eval("BrandShortName") %>&nbsp;<%# Eval("ShortName") %>
                           <asp:Image ID="QueueIcon" runat="server" CssClass="queueIcon"/>
                             </h4>
                        <asp:Label ID="lblPreAuthorization" runat="server" Text="Pre-Authorization Required" CssClass="preAuthorization"></asp:Label>

                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn UniqueName="PreAuthorization" DataField="IsPreAuthorization" Visible="False"/>
                    <telerik:GridBoundColumn UniqueName="DispenseQueueID" DataField="DispenseQueueID" Visible="False"/>
                </Columns>
                <DetailTables>
                <telerik:GridTableView runat="server" Name="ChildGrid">
                    <ParentTableRelation>
                  <telerik:GridRelationFields DetailKeyField="DispenseQueueID" MasterKeyField="DispenseQueueID"/>
                </ParentTableRelation>
                  <Columns>
                    <telerik:GridTemplateColumn  ItemStyle-HorizontalAlign="Left" UniqueName="QueueDetail">
                      <ItemTemplate>
                        <table class="DispenseQueueTable">
                          <tr>
                            <td>Medicare</td>
                            <td>
                              <asp:CheckBox ID="chkIsMedicare" runat="server" Checked='<%# Eval("IsMedicare") %>' />
                            </td>
                          </tr>
                          <tr>
                              <td>Pre-Authorized</td>
                              <td>
                                  <asp:CheckBox ID="chkIsPreAuthorization" runat="server" Checked='<%# Eval("PreAuthorized") %>'/>
                              </td>
                          </tr>
                          <tr>
                            <td>Dispense Date</td>
                            <td>
                              <telerik:RadDatePicker ID="txtDateDispensed" runat="server">
                                <DateInput ID="DateInput2" runat="server" DisplayDateFormat="MM/dd/yy" MaxLength="20" ToolTip="Dates can be entered in the following formats: 01/01/2016, January 1,2016, 01012016"></DateInput>
                                <Calendar ID="Calendar1" runat="server" ShowRowHeaders="false" ImagesPath="App_Themes/Breg/images/Common/" Skin="Breg" EnableEmbeddedSkins="False"></Calendar>
                              </telerik:RadDatePicker>
                            </td>
                          </tr>
                          <tr>
                            <td>Provider</td>
                            <td>
                              <div class="Dropdown-Container">
                                <asp:DropDownList ID="cboPhysicians" runat="server" DataSource="<%# _dsPhysicians %>" DataTextField="PhysicianName" DataValueField="PhysicianId" CssClass="RadComboBox_Breg">
                                </asp:DropDownList>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td>Patient Name</td>
                            <td>
                              <div class="half">
                                <asp:TextBox ID="txtPatientFirstName" runat="server" MaxLength="50" Text='<%# Bind("PatientFirstName") %>' CssClass="RadInput_Breg" Placeholder="First Name"></asp:TextBox>
                              </div>
                              <div class="half">
                                <asp:TextBox ID="txtPatientLastName" runat="server" MaxLength="50" Text='<%# Bind("PatientLastName") %>' CssClass="RadInput_Breg" Placeholder="Last Name"></asp:TextBox>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td>Patient Code/MRN</td>
                            <td>
                              <asp:TextBox ID="txtPatientCode" runat="server" MaxLength="255" Text='<%# Bind("PatientCode") %>'></asp:TextBox>
                              <asp:Button ID="btnSelectPatient" runat="server" CssClass="button-moreInfo" />
                            </td>
                          </tr>
                          <tr>
                            <td>Fitter</td>
                            <td>
                             <asp:Panel ID="cboFittersPanel" runat="server" data-splitcode='<%# Eval("IsSplitCode") %>'>
                                <div class="Dropdown-Container">
                                  <asp:DropDownList ID="cboFitters" runat="server" DataSource="<%# _dsFitters %>" DataTextField="ClinicianName" DataValueField="ClinicianId">
                                  </asp:DropDownList>
                                </div>
                             </asp:Panel>
                            </td>
                          </tr>
                          <tr>
                            <td>Side</td>
                            <td>
                              <div class="Dropdown-Container">
                                <asp:DropDownList ID="cboSide" Enabled='<%# Bind("EnableSide") %>' runat="server" SelectedValue='<%# Bind("Side") %>' >
                                  <asp:ListItem Text="Please Select a Side" Value=""></asp:ListItem>
                                  <asp:ListItem Text="U" Value="U"></asp:ListItem>
                                  <asp:ListItem Text="LT" Value="L"></asp:ListItem>
                                  <asp:ListItem Text="RT" Value="R"></asp:ListItem>
                                  <asp:ListItem Text="BI" Value="BI"></asp:ListItem>
                                </asp:DropDownList>

                                <asp:TextBox ID="cboSideOldValue" runat="server" Text='<%# Bind("Side") %>' CssClass="hide"/>
                                    
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td>Cash &amp; Carry&nbsp;<bvu:Help ID="Help3" runat="server" HelpID="128" />
                            </td>
                            <td>
                              <div>
                                <asp:CheckBox ID="chkDMEDeposit" runat="server" Checked='<%# Eval("IsCashCollection") %>'/>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td>DME Deposit&nbsp;<bvu:Help ID="Help4" runat="server" HelpID="31" />
                            </td>
                            <td>
                              <telerik:RadNumericTextBox ID="txtDMEDeposit" runat="server" DbValue='<%# Eval("DMEDeposit") %>' MaxValue="9999" MinValue="0" ShowSpinButtons="false" Type="Currency">
                              </telerik:RadNumericTextBox>
                              <%--<asp:TextBox ID="txtDMEDeposit" runat="server" Text='<%# Eval("DMEDeposit") %>'></asp:TextBox>--%>
                              <asp:RequiredFieldValidator ID="rfvDMEDeposit" runat="server" ControlToValidate="txtDMEDeposit" CssClass="WarningMsgSml" Display="Dynamic" Enabled="false" ErrorMessage="*" ValidationGroup="vgGroup" Visible="false"></asp:RequiredFieldValidator>
                            </td>
                          </tr>
                          <tr>
                            <td>Payer</td>
                            <td>
                              <div class="Dropdown-Container">
                                <asp:DropDownList ID="cboPayers" runat="server" DataSource="<%# _dsPayers %>" DataTextField="Name" DataValueField="PayerId" />
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td>Billing Charge</td>
                            <td>
                              <asp:Label ID="lblBillingCharge" data-billingCharge='<%# Eval("BillingCharge") %>' data-billingChargeOTS='<%# Eval("BillingChargeOffTheShelf") %>' runat="server" Text='<%# Eval("BillingCharge", "{0:C}") %>'></asp:Label>
                            </td>
                          </tr>
                          <tr>
                            <td>Cash & Carry</td>
                            <td>
                               <asp:Label Text='<%# Eval("BillingChargeCash", "{0:C}") %>' runat="server" ID="lblCashAndCarryValue"></asp:Label>
                            </td>
                          </tr>
                          <tr>
                            <td>Queued Reason</td>
                            <td>
                              <div class="Dropdown-Container">
                                <asp:DropDownList ID="ddlQueuedReason" runat="server" SelectedValue='<%# Eval("QueuedReason") %>'>
                                  <asp:ListItem Value="" Text="" />
                                  <asp:ListItem Value="Surgery" Text="Surgery" />
                                  <asp:ListItem Value="Finish Later" Text="Finish Later" />
                                  <asp:ListItem Value="Pre-Authorization" Text="Pre-Authorization" />
                                </asp:DropDownList>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td>ABN Type</td>
                            <td>
                              <div class="Dropdown-Container">
                                <asp:DropDownList ID="ddlAbnType" SelectedValue='<%# Bind("ABNType") %>' runat="server">
                                  <asp:ListItem Value=""></asp:ListItem>
                                  <asp:ListItem Value="Medicare" Text="Medicare" />
                                  <asp:ListItem Value="Commercial" Text="Commercial" />
                                </asp:DropDownList>
                                <asp:TextBox ID="tbdispenseQueueID" runat="server" Visible="False" Text='<%# Bind("DispenseQueueID") %>' />
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td>Modifier(s)</td>
                            <td>
                              <div class="third">
                                <div class="Dropdown-Container">
                                  <asp:DropDownList ID="ddlMod1" runat="server" SelectedValue='<%# Eval("Mod1") %>'>
                                    <asp:ListItem Text="" Value=""></asp:ListItem>
                                    <asp:ListItem Text="CG" Value="CG"></asp:ListItem>
                                    <asp:ListItem Text="EY" Value="EY"></asp:ListItem>
                                    <asp:ListItem Text="GA" Value="GA"></asp:ListItem>
                                    <asp:ListItem Text="GX" Value="GX"></asp:ListItem>
                                    <asp:ListItem Text="GY" Value="GY"></asp:ListItem>
                                    <asp:ListItem Text="GZ" Value="GZ"></asp:ListItem>
                                    <asp:ListItem Text="KV" Value="KV"></asp:ListItem>
                                    <asp:ListItem Text="KX" Value="KX"></asp:ListItem>
                                    <asp:ListItem Text="NU" Value="NU"></asp:ListItem>
                                    <asp:ListItem Text="RA" Value="RA"></asp:ListItem>
                                    <asp:ListItem Text="RB" Value="RB"></asp:ListItem>
                                    <asp:ListItem Text="RR" Value="RR"></asp:ListItem>
                                  </asp:DropDownList>
                                </div>
                              </div>
                              <div class="third">
                                <div class="Dropdown-Container">
                                  <asp:DropDownList ID="ddlMod2" runat="server" SelectedValue='<%# Eval("Mod2") %>'>
                                    <asp:ListItem Text="" Value=""></asp:ListItem>
                                    <asp:ListItem Text="CG" Value="CG"></asp:ListItem>
                                    <asp:ListItem Text="EY" Value="EY"></asp:ListItem>
                                    <asp:ListItem Text="GA" Value="GA"></asp:ListItem>
                                    <asp:ListItem Text="GX" Value="GX"></asp:ListItem>
                                    <asp:ListItem Text="GY" Value="GY"></asp:ListItem>
                                    <asp:ListItem Text="GZ" Value="GZ"></asp:ListItem>
                                    <asp:ListItem Text="KV" Value="KV"></asp:ListItem>
                                    <asp:ListItem Text="KX" Value="KX"></asp:ListItem>
                                    <asp:ListItem Text="NU" Value="NU"></asp:ListItem>
                                    <asp:ListItem Text="RA" Value="RA"></asp:ListItem>
                                    <asp:ListItem Text="RB" Value="RB"></asp:ListItem>
                                    <asp:ListItem Text="RR" Value="RR"></asp:ListItem>
                                  </asp:DropDownList>
                                </div>
                              </div>
                              <div class="third">
                                <div class="Dropdown-Container">
                                  <asp:DropDownList ID="ddlMod3" runat="server" SelectedValue='<%# Eval("Mod3") %>'>
                                    <asp:ListItem Text="" Value=""></asp:ListItem>
                                    <asp:ListItem Text="CG" Value="CG"></asp:ListItem>
                                    <asp:ListItem Text="EY" Value="EY"></asp:ListItem>
                                    <asp:ListItem Text="GA" Value="GA"></asp:ListItem>
                                    <asp:ListItem Text="GX" Value="GX"></asp:ListItem>
                                    <asp:ListItem Text="GY" Value="GY"></asp:ListItem>
                                    <asp:ListItem Text="GZ" Value="GZ"></asp:ListItem>
                                    <asp:ListItem Text="KV" Value="KV"></asp:ListItem>
                                    <asp:ListItem Text="KX" Value="KX"></asp:ListItem>
                                    <asp:ListItem Text="NU" Value="NU"></asp:ListItem>
                                    <asp:ListItem Text="RA" Value="RA"></asp:ListItem>
                                    <asp:ListItem Text="RB" Value="RB"></asp:ListItem>
                                    <asp:ListItem Text="RR" Value="RR"></asp:ListItem>
                                  </asp:DropDownList>
                                </div>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td>On Hand</td>
                            <td>
                              <div>
                                <%# Eval("QuantityOnHandPerSystem") %>
                              </div>
                            </td>
                          </tr>
                        <tr>
                              
                            <td>Qty. to Dispense</td>
                            <td>
                              <telerik:RadNumericTextBox ID="txtQuantityDispensed" runat="server" DbValue='<%# Eval("QuantityToDispense") %>' MaxValue="99" MinValue="0" NumberFormat-DecimalDigits="0" ShowSpinButtons="false" Type="Number">
                              </telerik:RadNumericTextBox> 
                              <asp:RequiredFieldValidator ID="rfvQuantityDispensed" runat="server" ControlToValidate="txtQuantityDispensed" CssClass="WarningMsgSml" Display="Dynamic" Enabled="false" ErrorMessage="*" ValidationGroup="vgGroup" Visible="false"></asp:RequiredFieldValidator>
                              <span id="reqAlert" class="alertMsg">For a bilateral setting, the quantity to dispense must be a multiple of 2</span>
                            </td>
                          </tr>
                          <tr>
                            <td>Dx Code</td>
                            <td>
                              <telerik:RadComboBox ID="cboICD9" runat="server" AllowCustomText="true" DataTextField="ICD9Code" DataValueField="ICD9Code" CssClass="icd9code dropdown-input" onclick="stopProp(event || window.event);">
                              </telerik:RadComboBox>
                              <asp:RegularExpressionValidator ID="revICD9" runat="server" ControlToValidate="cboICD9" Display="Dynamic" Enabled="false" ErrorMessage="*Invalid Characters" SetFocusOnError="true" Text="*Invalid Characters" ValidationExpression="^()*([\s?])*([\w?\$?\.?\s?]{1,4})+([\s?])*([,]([\s?])*([\w?\$?\.?\s?]{1,4})+([\s?])*)*$" Visible="false">
                              </asp:RegularExpressionValidator>
                            </td>
                          </tr>
                          <tr>
                            <td>Note</td>
                            <td>
                              <asp:TextBox ID="txtNote" TextMode="MultiLine" runat="server" MaxLength="250" Text='<%# Eval("Note") %>' CssClass="RadInputMultiline_Breg RadInputMgr_Breg"></asp:TextBox>
                            </td>
                          </tr>
                          <tr style="display: none">
                            <td>Patient Email</td>
                            <td>
                              <asp:TextBox ID="txtPatientEmail" runat="server" MaxLength="50" Style="visibility: hidden; display: none" Text='<%# Bind("PatientEmail") %>'></asp:TextBox>
                            </td>
                          </tr>
                        </table>
                        <asp:Table ID="splitCodeTable" runat="server" data-splitcode='<%# Eval("IsSplitCode") %>' data-alwaysofftheshelf='<%# Eval("IsAlwaysOffTheShelf") %>' CssClass="DispenseQueueTable">
                          <asp:TableRow ID="splitCodeRow" runat="server" data-splitcode='<%# Eval("IsSplitCode") %>' data-alwaysofftheshelf='<%# Eval("IsAlwaysOffTheShelf") %>'>
                            <asp:TableCell runat="server">
                              <asp:CheckBox ID="chkCustomFabricated" runat="server" MaxLength="50" Visible="false" Enabled="false"></asp:CheckBox>
                            </asp:TableCell>
                          </asp:TableRow>
                          <asp:TableRow ID="splitCodeRow2" runat="server" data-splitcode='<%# Eval("IsSplitCode") %>' data-alwaysofftheshelf='<%# Eval("IsAlwaysOffTheShelf") %>'>
                            <asp:TableCell runat="server">Split Code Options</asp:TableCell>
                            <asp:TableCell runat="server">
                              <div class="Dropdown-Container">
                                <asp:DropDownList ID="cboSplitCode" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cboSplitCode_OnSelectedIndexChanged" data-splitcode='<%# Eval("IsSplitCode") %>' data-alwaysofftheshelf='<%# Eval("IsAlwaysOffTheShelf") %>'>
                                  <asp:ListItem Value=""></asp:ListItem>
                                  <asp:ListItem Value="Custom Fit">Custom Fit</asp:ListItem>
                                  <asp:ListItem Value="Off the Shelf">Off the Shelf</asp:ListItem>
                                </asp:DropDownList>
                              </div>
                            </asp:TableCell>
                          </asp:TableRow>                       
                          <asp:TableRow ID="splitCodeRow4" runat="server" data-splitcode='<%# Eval("IsSplitCode") %>' data-alwaysofftheshelf='<%# Eval("IsAlwaysOffTheShelf") %>'>
                            <asp:TableCell runat="server">Custom Fit Procedures</asp:TableCell>
                            <asp:TableCell runat="server">
                              <div>
                                <asp:Panel ID="CustomFitProcedurePanel" runat="server" data-splitcode='<%# Eval("IsSplitCode") %>'>
                                  <ol class="CheckboxList">
                                    <!--Inject the list of Custom Fit Procedures -->
                                    <asp:Repeater runat="server" ID="rpCustomFitProcedureList" DataSource='<%# Eval("CustomFitProcedures") %>'>
                                      <ItemTemplate>
                                        <li>
                                          <!-- Some of these controls are not displayed as they're only used by the code behind to facilitate updates to the database -->
                                          <asp:Label ID="lblCustomFitProcedureID" runat="server" Visible="false" Text='<%# ((ClassLibrary.DAL.DQCustomFitProcedure) (Container.DataItem)).CustomFitProcedureID %>'></asp:Label>
                                          <asp:CheckBox ID="cbIsActive" runat="server" Checked='<%# ((ClassLibrary.DAL.DQCustomFitProcedure) (Container.DataItem)).IsActive %>' />&nbsp;
                            <asp:CheckBox ID="cbIsUserEntered" runat="server" Visible="false" Checked='<%# ((ClassLibrary.DAL.DQCustomFitProcedure) (Container.DataItem)).IsUserEntered %>' />&nbsp;
                            <asp:Label runat="server" Visible='<%# ((ClassLibrary.DAL.DQCustomFitProcedure) (Container.DataItem)).IsUserEntered %>' Text="EXPLANATION: "></asp:Label>
                                          <asp:TextBox ID="txtDescription" runat="server" MaxLength="500" Width="80%" Visible='<%# ((ClassLibrary.DAL.DQCustomFitProcedure) (Container.DataItem)).IsUserEntered %>' Text='<%# ((ClassLibrary.DAL.DQCustomFitProcedure) (Container.DataItem)).IsUserEntered ? ((ClassLibrary.DAL.DQCustomFitProcedure) (Container.DataItem)).Description : "" %>'>
                                          </asp:TextBox>
                                          <asp:Label runat="server" Visible='<%# !((ClassLibrary.DAL.DQCustomFitProcedure) (Container.DataItem)).IsUserEntered %>' Text='<%# !((ClassLibrary.DAL.DQCustomFitProcedure) (Container.DataItem)).IsUserEntered ? ((ClassLibrary.DAL.DQCustomFitProcedure) (Container.DataItem)).Description : "" %>'>
                                          </asp:Label>
                                        </li>
                                      </ItemTemplate>
                                    </asp:Repeater>
                                  </ol>
                                </asp:Panel>
                              </div>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>
                      </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="ABN_Needed" Visible="false" />
                    <telerik:GridBoundColumn DataField="DispenseQueueID" Visible="false" />
                    <telerik:GridBoundColumn DataField="PracticeLocationID" Visible="false" />
                    <telerik:GridBoundColumn DataField="BregVisionOrderID" Visible="false" />
                    <telerik:GridBoundColumn DataField="Code" Visible="false" />
                    <telerik:GridBoundColumn DataField="HCPCsString" UniqueName="HCPCs" Visible="false" />
                    <telerik:GridBoundColumn DataField="WholesaleCost" DataFormatString="{0:C}" Visible="false" />
                    <telerik:GridBoundColumn DataField="QuantityOnHandPerSystem" Visible="false" />
                    <telerik:GridBoundColumn DataField="BillingCharge" Visible="false" />
                    <telerik:GridBoundColumn DataField="BillingChargeOffTheShelf" Visible="false" />
                    <telerik:GridBoundColumn DataField="BillingChargeCash" Visible="false" />
                    <telerik:GridBoundColumn DataField="ProductInventoryID" Visible="false" />
                    <telerik:GridBoundColumn DataField="PracticeCatalogProductID" Visible="false" />
                  </Columns>
                       </telerik:GridTableView>
                    </DetailTables>
                </MasterTableView>
              </telerik:RadGrid>

              <div class="section" style="display: none">
                <div class="flex-row">
                  <div>
                    <asp:Label ID="lblSummary2" runat="server" CssClass="WarningMsg" OnPreRender="LabelPreRenderHideShow"></asp:Label>
                    <asp:Label ID="lblErrorMsg2" runat="server" CssClass="WarningMsg" OnPreRender="LabelPreRenderHideShow"></asp:Label>
                  </div>
                  <div>
                    <div>
                      <asp:Button ID="btnSaveDispenseQueue2" runat="server" OnClick="btnSaveDispenseQueue_Click"
                        Text="Save Changes" ToolTip="Save Dispense Queue" ValidationGroup="vgGroup" />
                      <bvu:Help ID="Help8" runat="server" HelpID="84" />
                      <asp:Button
                        ID="btnDispenseProducts2" runat="server" OnClick="btnDispenseProducts_Click" Text="Dispense Product(s)"
                        ToolTip="Dispense product(s)" ValidationGroup="vgGroup" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </asp:Panel>
          <div id="Supplemental" runat="server" style="visibility: hidden">
            Your products have been dispensed. Please click the receipt tab to view and print this dispensement.
          </div>
          <asp:Label ID="lblTurnOffDispenseSummarySafeguard" Visible="false" Text="SetThisTextToEmptyStringToTurnOnSafeguard"
            runat="server" />
          <div style="display: none;">
            <%--        <telerik:RadDatePicker ID="RadDatePicker1" Width="100" runat="server">
          <DateInput ID="DateInput1" runat="server" MaxLength="20" DisplayDateFormat="MM/dd/yy" ToolTip="Dates can be entered in the following formats(01012007,01/01/2007,January 1,2007)"></DateInput>
          <Calendar ID="Calendar1" runat="server" ShowRowHeaders="false" BorderStyle="Solid" CalendarTableStyle-BorderStyle="Solid" CalendarTableStyle-BorderColor="Blue"
                                CalendarTableStyle-BorderWidth="1px" TitleStyle-BorderColor="Blue" TitleStyle-BorderStyle="Solid"
            TitleStyle-BorderWidth="1px" TitleStyle-Font-Bold="true"></Calendar>
        </telerik:RadDatePicker>--%>
            <telerik:RadComboBox ID="RadComboBox1" runat="server" AllowCustomText="true" ExpandEffect="fade"></telerik:RadComboBox>
          </div>
        </telerik:RadPane>
      </telerik:RadSplitter>
    </div>
  </div>
</asp:Content>
