﻿using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.WebControls;
using System.Web;

namespace BregVision
{
    public class UIHelper
    {

        public StateBag CurrentViewState { get; set; }

        public RadGrid CurrentGrid { get; set; }

        public int NumChildRecs { get; set; }

        public bool IsSearch { get; set; }

        private bool _hasExpanded = true;

        public bool NeedsRebind { get; set; }


        private Hashtable _ordersExpandedState;
        private Hashtable _selectedState;
      

        public UIHelper(StateBag currentViewState, RadGrid currentGrid, bool hasExpanded)
        {
            this.CurrentGrid = currentGrid;
            this._hasExpanded = hasExpanded;
            this.CurrentViewState = currentViewState;
            this.NumChildRecs = 0;
            this.SearchCriteria = "Browse";
        }

        public UIHelper(StateBag currentViewState, RadGrid currentGrid, bool hasExpanded, bool MaintainGridStateOnRebind)
        {
            //if we instantiate using this constructor, it will maintain state in the grid across rebind calls
            this.CurrentGrid = currentGrid;
            this.CurrentGrid.ItemCommand +=new GridCommandEventHandler(CurrentGrid_ItemCommand);
            this.CurrentGrid.DataBound +=new EventHandler(CurrentGrid_DataBound);
            this._hasExpanded = hasExpanded;
            this.CurrentViewState = currentViewState;
            this.NumChildRecs = 0;
            this.SearchCriteria = "Browse";
        }

        public static void RemoveVisionLiteSearchCriteria(RadToolbar toolbar, string toolbarButtonName, string comboBoxName)
        {
            if (BLL.Website.IsVisionExpress())
            {
                UIHelper.RemoveSearchComboItem(toolbar, "Brand", toolbarButtonName, comboBoxName);
                UIHelper.RemoveSearchComboItem(toolbar, "HCPCs", toolbarButtonName, comboBoxName);
                UIHelper.RemoveSearchComboItem(toolbar, "Supplier", toolbarButtonName, comboBoxName);
                UIHelper.RemoveSearchComboItem(toolbar, "Category", toolbarButtonName, comboBoxName);
            }
        }

        public static void RemoveSearchComboItem(RadToolbar toolbar, string cboItemValue, string toolbarButtonName, string comboBoxName)
        {


            RadToolbarTemplateButton templateButton = toolbar.FindControl(toolbarButtonName) as RadToolbarTemplateButton;
            if (templateButton != null)
            {
                RadComboBox cboSearchFilter = templateButton.FindControl(comboBoxName) as RadComboBox;
                RemoveItem(cboSearchFilter, cboItemValue);
                toolbar.EnableViewState = false;
            }
        }

        public static void RemoveItem(RadComboBox cboSearchFilter, string cboItemValue)
        {
            if (cboSearchFilter != null)
            {
               
                int itemIndex = cboSearchFilter.FindItemIndexByValue(cboItemValue);
                if (itemIndex > -1)
                {
                    cboSearchFilter.Items.Remove(itemIndex);
                    
                }
            }
        }

        public void SetGridLabel(string SearchCriteria, bool IsPostBack, Label lblBrowseDispense)
        {
            SetGridLabel(SearchCriteria, IsPostBack, lblBrowseDispense, false);
        }
        public void SetGridLabel(string SearchCriteria, bool IsPostBack, Label lblBrowseDispense, bool ForceDefaultText)
        {

            lblBrowseDispense.Visible = true;
            //if (SearchCriteria != "All" && SearchCriteria != "Browse" && IsPostBack && SearchCriteria != "" && ForceDefaultText == false)
            if (SearchCriteria != "All" && SearchCriteria != "Browse" && IsSearch && SearchCriteria != "" && ForceDefaultText == false)
            {
                lblBrowseDispense.Text = NumChildRecs.ToString() + " search result(s) found.";
            }
            else
            {
                lblBrowseDispense.Text = "";
            }
        }

        public void SetRecordCount(DataSet ds, string FieldName)
        {
            var query = ds.Tables[0].AsEnumerable()
                .Count(products => products.Field<string>(FieldName) != null); //

            this.NumChildRecs = query;

        }

        public bool HasExpanded 
        { 
            get
            {
                if (CurrentViewState[CurrentGrid.ID + "hasExpanded"] != null)
                {
                    _hasExpanded = (Boolean)CurrentViewState[CurrentGrid.ID + "hasExpanded"];
                    return _hasExpanded;
                }
                return true;
            }
            set
            {
                this.CurrentViewState[CurrentGrid.ID + "hasExpanded"] = value;
                _hasExpanded = value;
            }
        }

        public string SearchCriteria
        {
            get
            {
                return GetViewStateVariable(CurrentGrid.ID + "_SearchCriteria");
            }
            set
            {
                this.CurrentViewState[CurrentGrid.ID + "_SearchCriteria"] = value;
            }
        }

        public string SearchText
        {
            get
            {
                return GetViewStateVariable(CurrentGrid.ID + "_SearchText");
            }
            set
            {
                this.CurrentViewState[CurrentGrid.ID + "_SearchText"] = value;
            }
        }

        public string GetViewStateVariable(string searchCriteriaName)
        {
            if (CurrentViewState[searchCriteriaName] != null)
            {
                return (string)CurrentViewState[searchCriteriaName];
            }
            return string.Empty;
        }



        public void SetSearchCriteria(string commandName)
        {
            if (commandName == "Browse")
            {
                this.SearchCriteria = "Browse";
                this.SearchText = "";
                try
                {
                    CurrentGrid.Rebind();
                }
                catch (HttpException hex)
                {
                    //This can happen if there are no records yet
                }
                this.HasExpanded = true; //mws
            }

            if (commandName == "Search")
            {
                this.IsSearch = true;
                try
                {
                    CurrentGrid.Rebind();
                }
                catch (HttpException hex)
                {
                    //This can happen if there are no records yet
                }
                
                this.HasExpanded = false; //mws
            }
        }


        public void CauseRebindOnDelete(GridDataItem Parent)
        {
            if (Parent.HasChildItems)
            {
                if (Parent.ChildItem.NestedTableViews.Length > 0)
                {
                    if (Parent.ChildItem.NestedTableViews[0].Items.Count < 2) //MWS <2 because grid item hasn't actually been removed yet
                    {
                        //grdInventory.MasterTableView.Rebind();
                        this.NeedsRebind = true;
                    }
                }
            }
        }
        
        public void PageIndexChanged()
        {
            if (this.SearchCriteria == null || this.SearchCriteria.ToString() == "Browse" || this.SearchCriteria.ToString() == "")
            {
                this.HasExpanded = true; //mws
            }

            else
            {
                this.HasExpanded = false; //mws
            }
        }

        public void ExpandGrid()
        {

            if (this.HasExpanded == false) //mws
            {

                UIHelper.ExpandFormatGrid(CurrentGrid);

                this.HasExpanded = true; //mws

            }
        }


        public void ExpandGrid(bool ExpandOnly)
        {

            if (this.HasExpanded == false) //mws
            {

                UIHelper.ExpandFormatGrid(CurrentGrid, ExpandOnly);

                this.HasExpanded = true; //mws

            }
        }


        public void ExpandAndFormatGrid(RadGrid Grid)
        {
            ExpandFormatGrid(Grid);
        }


        public static void ExpandFormatGrid(RadGrid Grid)
        {
            foreach (GridDataItem item in Grid.MasterTableView.Items)
            {
                if (item.HasChildItems)
                {

                    item.Expanded = true;

                    if (item.ChildItem.NestedTableViews.Length > 0)
                    {
                        if (item.ChildItem.NestedTableViews[0].Items.Count < 1)
                        {
                            item.Expanded = false;
                            item.ChildItem.Enabled = false;
                            item.Enabled = false;

                        }
                    }
                }
                else
                {
                    item.Visible = false;
                }
            }
        }

        public static void ExpandFormatGrid(RadGrid Grid, bool ExpandOnly)
        {
            foreach (GridDataItem item in Grid.MasterTableView.Items)
            {
                if (item.HasChildItems)
                {
                    item.Expanded = true;
                }
            }
        }

        /// <summary>
        /// This sets the onkeydown javascript on a control to 'click' another control when enter button is pressed
        /// </summary>
        /// <param name="keyControl">a textbox, in which 'enter' will be pressed</param>
        /// <param name="targetControl">the control, usually a button, that will have its 'click' fired on the 'enter' </param>
        public static void SetEnterPressTarget(Telerik.Web.UI.RadTextBox keyControl, Control targetControl)
        {
            //TextBox tbFilter = (TextBox)rttbFilter.FindControl("tbFilter");
            keyControl.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + targetControl.ClientID + "').click();return false;}} else {return true}; ");

        }


        public static RadToolbarToggleButton FindToggleButtonRecursive(object root)
        {
            if (root is RadToolbarToggleButton)
            {
                RadToolbarToggleButton foundBtn = (RadToolbarToggleButton)root;
                if (foundBtn.CommandName.ToLower() == "search")
                {
                    return foundBtn;
                }
            }

            foreach (object c in ((Control)root).Controls)
            {
                RadToolbarToggleButton t = FindToggleButtonRecursive(c);
                if (t != null)
                {
                    return t;
                }
            }

            return null;
        } 

        public static Control FindControlRecursive(Control root, string id)
        {
            if (root.ID == id)
            {
                return root;
            }

            foreach (Control c in root.Controls)
            {
                Control t = FindControlRecursive(c, id);
                if (t != null)
                {
                    return t;
                }
            }

            return null;
        }


        #region //functions to maintain grid state across rebinds
        //Save/load expanded states Hash from the session
        //this can also be implemented in the ViewState
        private Hashtable ExpandedStates
        {
            get
            {
                if (this._ordersExpandedState == null)
                {
                    _ordersExpandedState = this.CurrentViewState[CurrentGrid.ID + "_ordersExpandedState"] as Hashtable;
                    if (_ordersExpandedState == null)
                    {
                        _ordersExpandedState = new Hashtable();
                        this.CurrentViewState[CurrentGrid.ID + "_ordersExpandedState"] = _ordersExpandedState;
                    }
                }

                return this._ordersExpandedState;
            }
        }

  

        //Clear the state for all expanded children if a parent item is collapsed
        private void ClearExpandedChildren(string parentHierarchicalIndex)
        {
            string[] indexes = new string[this.ExpandedStates.Keys.Count];
            this.ExpandedStates.Keys.CopyTo(indexes, 0);
            foreach (string index in indexes)
            {
                //all indexes of child items
                if (index.StartsWith(parentHierarchicalIndex + "_") ||
                    index.StartsWith(parentHierarchicalIndex + ":"))
                {
                    this.ExpandedStates.Remove(index);
                }
            }
        }


        //Save/load selected states Hash from the session
        //this can also be implemented in the ViewState
        private Hashtable SelectedStates
        {
            get
            {
                if (this._selectedState == null)
                {
                    _selectedState = this.CurrentViewState[CurrentGrid.ID + "_selectedState"] as Hashtable;
                    if (_selectedState == null)
                    {
                        _selectedState = new Hashtable();
                        this.CurrentViewState[CurrentGrid.ID + "_selectedState"] = _selectedState;
                    }
                }

                return this._selectedState;
            }
        }
        protected void CurrentGrid_ItemCommand(object source, GridCommandEventArgs e)
        {
            
            //save the expanded/selected state in the session
            if (e.CommandName == RadGrid.ExpandCollapseCommandName)
            {
                //Is the item about to be expanded or collapsed
                if (!e.Item.Expanded)
                {
                    //Save its unique index among all the items in the hierarchy
                    this.ExpandedStates[e.Item.ItemIndexHierarchical] = true;
                }
                else //collapsed
                {
                    this.ExpandedStates.Remove(e.Item.ItemIndexHierarchical);
                    this.ClearExpandedChildren(e.Item.ItemIndexHierarchical);
                }
            }
            //Is the item about to be selected 
            else if (e.CommandName == RadGrid.SelectCommandName)
            {
                //Save its unique index among all the items in the hierarchy
                this.SelectedStates[e.Item.ItemIndexHierarchical] = true;
            }
            //Is the item about to be deselected 
            else if (e.CommandName == RadGrid.DeselectCommandName)
            {
                this.SelectedStates.Remove(e.Item.ItemIndexHierarchical);
            }
        }
        protected void CurrentGrid_DataBound(object sender, EventArgs e)
        {
            
            //Expand all items using our custom storage
            string[] indexes = new string[this.ExpandedStates.Keys.Count];
            this.ExpandedStates.Keys.CopyTo(indexes, 0);

            ArrayList arr = new ArrayList(indexes);
            //Sort so we can guarantee that a parent item is expanded before any of 
            //its children
            arr.Sort();

            foreach (string key in arr)
            {
                bool value = (bool)this.ExpandedStates[key];
                if (value)
                {
                    if (CurrentGrid.Items.Count > 0)
                    {
                        CurrentGrid.Items[key].Expanded = true;
                    }
                }
            }

            //Select all items using our custom storage
            indexes = new string[this.SelectedStates.Keys.Count];
            this.SelectedStates.Keys.CopyTo(indexes, 0);

            arr = new ArrayList(indexes);
            //Sort to ensure that a parent item is selected before any of its children
            arr.Sort();

            foreach (string key in arr)
            {
                bool value = (bool)this.SelectedStates[key];
                if (value)
                {
                    CurrentGrid.Items[key].Selected = true;
                }
            }
        }

        public static void WriteJavascriptToLabel(RadGrid grd, Label lblToUpdate, string UpdateText)
        {

            //Outside control - for updating a label outside of Telerik RadGrid with AJAX enabled              
            LiteralControl ctlScript = new LiteralControl(String.Format("<script type='text/javascript'>document.getElementById('{0}').innerHTML = unescape('{1}');</script>", lblToUpdate.ClientID, Microsoft.JScript.GlobalObject.escape(UpdateText)));

            //add the LiteralControl to the Controls collection of RadGrid in order to make it work with Telerik RadGrid AJAX
            grd.Controls.Add(ctlScript);
        }
        #endregion


        #region common page events
        public static void LabelPreRenderHideShow(object sender, EventArgs e)
        {
            var l = sender as Label;
            if (l != null)
                l.Visible = !string.IsNullOrEmpty(l.Text.Trim());
        }
        #endregion
    }


    public class UIHelper2
    {

        public StateBag CurrentViewState { get; set; }

        public Telerik.Web.UI.RadGrid CurrentGrid { get; set; }

        public int NumChildRecs { get; set; }

        public bool IsSearch { get; set; }

        private bool _hasExpanded = true;

        public bool NeedsRebind { get; set; }


        private Hashtable _ordersExpandedState;
        private Hashtable _selectedState;


        public UIHelper2(StateBag currentViewState, Telerik.Web.UI.RadGrid currentGrid, bool hasExpanded)
        {
            this.CurrentGrid = currentGrid;
            this._hasExpanded = hasExpanded;
            this.CurrentViewState = currentViewState;
            this.NumChildRecs = 0;
            this.SearchCriteria = "Browse";
        }

        public UIHelper2(StateBag currentViewState, Telerik.Web.UI.RadGrid currentGrid, bool hasExpanded, bool MaintainGridStateOnRebind)
        {
            //if we instantiate using this constructor, it will maintain state in the grid across rebind calls
            this.CurrentGrid = currentGrid;
            this.CurrentGrid.ItemCommand += new Telerik.Web.UI.GridCommandEventHandler(CurrentGrid_ItemCommand);
            this.CurrentGrid.DataBound += new EventHandler(CurrentGrid_DataBound);
            this._hasExpanded = hasExpanded;
            this.CurrentViewState = currentViewState;
            this.NumChildRecs = 0;
            this.SearchCriteria = "Browse";
        }

        public static void RemoveVisionLiteSearchCriteria(Telerik.Web.UI.RadToolBar toolbar, string toolbarButtonName, string comboBoxName)
        {
            if (BLL.Website.IsVisionExpress())
            {
                UIHelper2.RemoveSearchComboItem(toolbar, "Brand", toolbarButtonName, comboBoxName);
                UIHelper2.RemoveSearchComboItem(toolbar, "HCPCs", toolbarButtonName, comboBoxName);
                UIHelper2.RemoveSearchComboItem(toolbar, "Supplier", toolbarButtonName, comboBoxName);
                UIHelper2.RemoveSearchComboItem(toolbar, "Category", toolbarButtonName, comboBoxName);
            }
        }

        public static void RemoveSearchComboItem(Telerik.Web.UI.RadToolBar toolbar, string cboItemValue, string toolbarButtonName, string comboBoxName)
        {


            var templateButton = toolbar.FindControl(toolbarButtonName) as Telerik.Web.UI.RadToolBarButton;
            if (templateButton != null)
            {
                var cboSearchFilter = templateButton.FindControl(comboBoxName) as Telerik.Web.UI.RadComboBox;
                RemoveItem(cboSearchFilter, cboItemValue);
                toolbar.EnableViewState = false;
            }
        }

        public static void RemoveItem(Telerik.Web.UI.RadComboBox cboSearchFilter, string cboItemValue)
        {
            if (cboSearchFilter != null)
            {
                var itemIndex = cboSearchFilter.FindItemIndexByValue(cboItemValue);
                if (itemIndex > -1)
                    cboSearchFilter.Items.Remove(itemIndex);
            }
        }

        public void SetGridLabel(string SearchCriteria, bool IsPostBack, Label lblBrowseDispense)
        {
            SetGridLabel(SearchCriteria, IsPostBack, lblBrowseDispense, false);
        }
        public void SetGridLabel(string SearchCriteria, bool IsPostBack, Label lblBrowseDispense, bool ForceDefaultText)
        {
            lblBrowseDispense.Visible = true;
            //if (SearchCriteria != "All" && SearchCriteria != "Browse" && IsPostBack && SearchCriteria != "" && ForceDefaultText == false)
            if (SearchCriteria != "All" && SearchCriteria != "Browse" && IsSearch && SearchCriteria != "" && ForceDefaultText == false)
            {
                lblBrowseDispense.Text = NumChildRecs.ToString() + " search result(s) found.";
            }
            else
            {
                lblBrowseDispense.Text = "";
            }
        }

        public void SetRecordCount(DataSet ds, string FieldName)
        {
            var query = ds.Tables[0].AsEnumerable()
                .Count(products => products.Field<string>(FieldName) != null); //

            this.NumChildRecs = query;

        }

        public bool HasExpanded
        {
            get
            {
                if (CurrentViewState[CurrentGrid.ID + "hasExpanded"] != null)
                {
                    _hasExpanded = (Boolean)CurrentViewState[CurrentGrid.ID + "hasExpanded"];
                    return _hasExpanded;
                }
                return true;
            }
            set
            {
                this.CurrentViewState[CurrentGrid.ID + "hasExpanded"] = value;
                _hasExpanded = value;
            }
        }

        public string SearchCriteria
        {
            get
            {
                return GetViewStateVariable(CurrentGrid.ID + "_SearchCriteria");
            }
            set
            {
                this.CurrentViewState[CurrentGrid.ID + "_SearchCriteria"] = value;
            }
        }

        public string SearchText
        {
            get
            {
                return GetViewStateVariable(CurrentGrid.ID + "_SearchText");
            }
            set
            {
                this.CurrentViewState[CurrentGrid.ID + "_SearchText"] = value;
            }
        }

        public string GetViewStateVariable(string searchCriteriaName)
        {
            if (CurrentViewState[searchCriteriaName] != null)
            {
                return (string)CurrentViewState[searchCriteriaName];
            }
            return string.Empty;
        }



        public void SetSearchCriteria(string commandName)
        {
            if (commandName == "Browse")
            {
                this.SearchCriteria = "Browse";
                this.SearchText = "";
                try
                {
                    CurrentGrid.Rebind();
                }
                catch (HttpException hex)
                {
                    //This can happen if there are no records yet
                }
                this.HasExpanded = true; //mws
            }

            if (commandName == "Search")
            {
                this.IsSearch = true;
                try
                {
                    CurrentGrid.Rebind();
                }
                catch (HttpException hex)
                {
                    //This can happen if there are no records yet
                }

                this.HasExpanded = false; //mws
            }
        }


        public void CauseRebindOnDelete(Telerik.Web.UI.GridDataItem Parent)
        {
            if (Parent.HasChildItems)
            {
                if (Parent.ChildItem.NestedTableViews.Length > 0)
                {
                    if (Parent.ChildItem.NestedTableViews[0].Items.Count < 2) //MWS <2 because grid item hasn't actually been removed yet
                    {
                        //grdInventory.MasterTableView.Rebind();
                        this.NeedsRebind = true;
                    }
                }
            }
        }

        public void PageIndexChanged()
        {
            if (this.SearchCriteria == null || this.SearchCriteria.ToString() == "Browse" || this.SearchCriteria.ToString() == "")
            {
                this.HasExpanded = true; //mws
            }

            else
            {
                this.HasExpanded = false; //mws
            }
        }

        public void ExpandGrid()
        {

            if (this.HasExpanded == false) //mws
            {

                UIHelper2.ExpandFormatGrid(CurrentGrid);

                this.HasExpanded = true; //mws

            }
        }


        public void ExpandGrid(bool ExpandOnly)
        {

            if (this.HasExpanded == false) //mws
            {

                UIHelper2.ExpandFormatGrid(CurrentGrid, ExpandOnly);

                this.HasExpanded = true; //mws

            }
        }


        public void ExpandAndFormatGrid(Telerik.Web.UI.RadGrid Grid)
        {
            ExpandFormatGrid(Grid);
        }


        public static void ExpandFormatGrid(Telerik.Web.UI.RadGrid Grid)
        {
            foreach (Telerik.Web.UI.GridDataItem item in Grid.MasterTableView.Items)
            {
                if (item.HasChildItems)
                {

                    item.Expanded = true;

                    if (item.ChildItem.NestedTableViews.Length > 0)
                    {
                        if (item.ChildItem.NestedTableViews[0].Items.Count < 1)
                        {
                            item.Expanded = false;
                            item.ChildItem.Enabled = false;
                            item.Enabled = false;

                        }
                    }
                }
                else
                {
                    item.Visible = false;
                }
            }
        }

        public static void ExpandFormatGrid(Telerik.Web.UI.RadGrid Grid, bool ExpandOnly)
        {
            foreach (Telerik.Web.UI.GridDataItem item in Grid.MasterTableView.Items)
            {
                if (item.HasChildItems)
                {
                    item.Expanded = true;
                }
            }
        }

        /// <summary>
        /// This sets the onkeydown javascript on a control to 'click' another control when enter button is pressed
        /// </summary>
        /// <param name="KeyControl">a textbox, in which 'enter' will be pressed</param>
        /// <param name="TargetControl">the control, usually a button, that will have its 'click' fired on the 'enter' </param>
        public static void SetEnterPressTarget(TextBox KeyControl, Control TargetControl)
        {
            //TextBox tbFilter = (TextBox)rttbFilter.FindControl("tbFilter");
            //KeyControl.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + TargetControl.ClientID + "').click();return false;}} else {return true}; ");
            SetEnterPressTarget(KeyControl, TargetControl.ClientID);
        }

        public static void SetEnterPressTarget(TextBox KeyControl, string TargetControlClientID)
        {
            KeyControl.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + TargetControlClientID + "').click();return false;}} else {return true}; ");
        }


        public static Telerik.Web.UI.RadToolBarButton FindToggleButtonRecursive(object root)
        {
            if (root is Telerik.Web.UI.RadToolBarButton)
            {
                var foundBtn = (Telerik.Web.UI.RadToolBarButton)root;
                if (foundBtn.CommandName.ToLower() == "search")
                {
                    return foundBtn;
                }
            }

            foreach (object c in ((Control)root).Controls)
            {
                var t = FindToggleButtonRecursive(c);
                if (t != null)
                {
                    return t;
                }
            }

            return null;
        }

        public static Control FindControlRecursive(Control root, string id)
        {
            if (root.ID == id)
            {
                return root;
            }

            foreach (Control c in root.Controls)
            {
                Control t = FindControlRecursive(c, id);
                if (t != null)
                {
                    return t;
                }
            }

            return null;
        }


        #region //functions to maintain grid state across rebinds
        //Save/load expanded states Hash from the session
        //this can also be implemented in the ViewState
        private Hashtable ExpandedStates
        {
            get
            {
                if (this._ordersExpandedState == null)
                {
                    _ordersExpandedState = this.CurrentViewState[CurrentGrid.ID + "_ordersExpandedState"] as Hashtable;
                    if (_ordersExpandedState == null)
                    {
                        _ordersExpandedState = new Hashtable();
                        this.CurrentViewState[CurrentGrid.ID + "_ordersExpandedState"] = _ordersExpandedState;
                    }
                }

                return this._ordersExpandedState;
            }
        }



        //Clear the state for all expanded children if a parent item is collapsed
        private void ClearExpandedChildren(string parentHierarchicalIndex)
        {
            string[] indexes = new string[this.ExpandedStates.Keys.Count];
            this.ExpandedStates.Keys.CopyTo(indexes, 0);
            foreach (string index in indexes)
            {
                //all indexes of child items
                if (index.StartsWith(parentHierarchicalIndex + "_") ||
                    index.StartsWith(parentHierarchicalIndex + ":"))
                {
                    this.ExpandedStates.Remove(index);
                }
            }
        }


        //Save/load selected states Hash from the session
        //this can also be implemented in the ViewState
        private Hashtable SelectedStates
        {
            get
            {
                if (this._selectedState == null)
                {
                    _selectedState = this.CurrentViewState[CurrentGrid.ID + "_selectedState"] as Hashtable;
                    if (_selectedState == null)
                    {
                        _selectedState = new Hashtable();
                        this.CurrentViewState[CurrentGrid.ID + "_selectedState"] = _selectedState;
                    }
                }

                return this._selectedState;
            }
        }
        protected void CurrentGrid_ItemCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {

            //save the expanded/selected state in the session
            if (e.CommandName == Telerik.Web.UI.RadGrid.ExpandCollapseCommandName)
            {
                //Is the item about to be expanded or collapsed
                if (!e.Item.Expanded)
                {
                    //Save its unique index among all the items in the hierarchy
                    this.ExpandedStates[e.Item.ItemIndexHierarchical] = true;
                }
                else //collapsed
                {
                    this.ExpandedStates.Remove(e.Item.ItemIndexHierarchical);
                    this.ClearExpandedChildren(e.Item.ItemIndexHierarchical);
                }
            }
            //Is the item about to be selected 
            else if (e.CommandName == Telerik.Web.UI.RadGrid.SelectCommandName)
            {
                //Save its unique index among all the items in the hierarchy
                this.SelectedStates[e.Item.ItemIndexHierarchical] = true;
            }
            //Is the item about to be deselected 
            else if (e.CommandName == Telerik.Web.UI.RadGrid.DeselectCommandName)
            {
                this.SelectedStates.Remove(e.Item.ItemIndexHierarchical);
            }
        }
        protected void CurrentGrid_DataBound(object sender, EventArgs e)
        {

            //Expand all items using our custom storage
            string[] indexes = new string[this.ExpandedStates.Keys.Count];
            this.ExpandedStates.Keys.CopyTo(indexes, 0);

            ArrayList arr = new ArrayList(indexes);
            //Sort so we can guarantee that a parent item is expanded before any of 
            //its children
            arr.Sort();

            foreach (string key in arr)
            {
                bool value = (bool)this.ExpandedStates[key];
                if (value)
                {
                    if (CurrentGrid.Items.Count > 0)
                    {
                        CurrentGrid.Items[key].Expanded = true;
                    }
                }
            }

            //Select all items using our custom storage
            indexes = new string[this.SelectedStates.Keys.Count];
            this.SelectedStates.Keys.CopyTo(indexes, 0);

            arr = new ArrayList(indexes);
            //Sort to ensure that a parent item is selected before any of its children
            arr.Sort();

            foreach (string key in arr)
            {
                bool value = (bool)this.SelectedStates[key];
                if (value)
                {
                    CurrentGrid.Items[key].Selected = true;
                }
            }
        }

        public static void WriteJavascriptToLabel(Telerik.Web.UI.RadGrid grd, Label lblToUpdate, string UpdateText)
        {

            //Outside control - for updating a label outside of Telerik RadGrid with AJAX enabled              
            var ctlScript = new LiteralControl(String.Format("<script type='text/javascript'>document.getElementById('{0}').innerHTML = unescape('{1}');</script>", lblToUpdate.ClientID, Microsoft.JScript.GlobalObject.escape(UpdateText)));

            //add the LiteralControl to the Controls collection of RadGrid in order to make it work with Telerik RadGrid AJAX
            grd.Controls.Add(ctlScript);
        }
        #endregion


        #region common page events
        public static void LabelPreRenderHideShow(object sender, EventArgs e)
        {
            var l = sender as Label;
            if (l != null)
                l.Visible = !string.IsNullOrEmpty(l.Text.Trim());
        }
        #endregion
    }


    public static class SessionExtensions
    {
        public static Int32 GetInt32(this System.Web.SessionState.HttpSessionState session, string FieldName)
        {
            return (session != null) ? ((session[FieldName] != null) ? Convert.ToInt32(session[FieldName].ToString()) : -1) : -1;
        }

        public static string GetString(this System.Web.SessionState.HttpSessionState session, string FieldName)
        {
            return (session != null) ? ((session[FieldName] != null) ? session[FieldName].ToString() : "") : ""; 
        }
    }


    public static class GridColumnExtensions
    {
        public static Int32 GetColumnInt32(this GridDataItem gridDataItem, string FieldName)
        {
            var numericValueFound = false;
            var numericValue = 0;
            var isActive = gridDataItem.OwnerTableView.GetColumnSafe(FieldName);
            if (isActive != null)
            {
                var dataField = gridDataItem[FieldName].Text;
                numericValueFound = Int32.TryParse(dataField, out numericValue);
                if (numericValueFound == true)
                {
                    return numericValue;
                }
            }
            return -1;
        }
        public static Int32 GetColumnInt32(this Telerik.Web.UI.GridDataItem gridDataItem, string FieldName)
        {
            var numericValueFound = false;
            var numericValue = 0;
            var isActive = gridDataItem.OwnerTableView.GetColumnSafe(FieldName);
            if (isActive != null)
            {
                var dataField = gridDataItem[FieldName].Text;
                numericValueFound = Int32.TryParse(dataField, out numericValue);
                if (numericValueFound == true)
                {
                    return numericValue;
                }
            }
            return -1;
        }

        public static Decimal GetColumnDecimal(this GridDataItem gridDataItem, string FieldName)
        {
            var numericValueFound = false;
            var numericValue = 0m;
            var isActive = gridDataItem.OwnerTableView.GetColumnSafe(FieldName);
            if (isActive != null)
            {
                var dataField = gridDataItem[FieldName].Text;
                dataField = dataField.Replace("$", "");
                numericValueFound = Decimal.TryParse(dataField, out numericValue);
                if (numericValueFound == true)
                {
                    return numericValue;
                }
            }
            return -1;
        }
        public static Decimal GetColumnDecimal(this Telerik.Web.UI.GridDataItem gridDataItem, string FieldName)
        {
            var numericValueFound = false;
            var numericValue = 0m;
            var isActive = gridDataItem.OwnerTableView.GetColumnSafe(FieldName);
            if (isActive != null)
            {
                var dataField = gridDataItem[FieldName].Text;
                dataField = dataField.Replace("$", "");
                numericValueFound = Decimal.TryParse(dataField, out numericValue);
                if (numericValueFound == true)
                {
                    return numericValue;
                }
            }
            return -1;
        }

        public static string GetColumnString(this GridDataItem gridDataItem, string FieldName)
        {
            var isActive = gridDataItem.OwnerTableView.GetColumnSafe(FieldName);
            if (isActive != null)
            {
                return gridDataItem[FieldName].Text;
            }
            return "";
        }
        public static string GetColumnString(this Telerik.Web.UI.GridDataItem gridDataItem, string FieldName)
        {
            var isActive = gridDataItem.OwnerTableView.GetColumnSafe(FieldName);
            if (isActive != null)
            {
                return gridDataItem[FieldName].Text;
            }
            return "";
        }

        public static bool GetColumnBoolean(this GridDataItem gridDataItem, string FieldName)
        {
            var booleanValueFound = false;
            var booleanValue = false;
            var isActive = gridDataItem.OwnerTableView.GetColumnSafe(FieldName);
            if (isActive != null)
            {
                var dataField = gridDataItem[FieldName].Text;
                booleanValueFound = bool.TryParse(dataField, out booleanValue);
                if (booleanValueFound == true)
                {
                    return booleanValue;
                }
            }
            return false;
        }
        public static bool GetColumnBoolean(this Telerik.Web.UI.GridDataItem gridDataItem, string FieldName)
        {
            var booleanValueFound = false;
            var booleanValue = false;
            var isActive = gridDataItem.OwnerTableView.GetColumnSafe(FieldName);
            if (isActive != null)
            {
                var dataField = gridDataItem[FieldName].Text;
                booleanValueFound = bool.TryParse(dataField, out booleanValue);
                if (booleanValueFound == true)
                {
                    return booleanValue;
                }
            }
            return false;
        }

        public static T GetColumnText<T>(this GridDataItem gridDataItem, string ControlName, string PropertyName)
        {
            T text = default(T);
            var control = gridDataItem.FindControl(ControlName);
            if (control != null)
            {
                try
                {
                    text = (T)System.ComponentModel.TypeDescriptor.GetProperties(control)[PropertyName].GetValue(control);
                }
                catch { }
            }
            return text;
        }
        public static T GetColumnText<T>(this Telerik.Web.UI.GridDataItem gridDataItem, string ControlName, string PropertyName)
        {
            T text = default(T);
            var control = gridDataItem.FindControl(ControlName);
            if (control != null)
            {
                try
                {
                    text = (T)System.ComponentModel.TypeDescriptor.GetProperties(control)[PropertyName].GetValue(control);
                }
                catch { }
            }
            return text;
        }

        public static string GetColumnText(this GridDataItem gridDataItem, string ControlName)
        {
            return GetColumnText<string>(gridDataItem, ControlName, "Text");
        }
        public static string GetColumnText(this Telerik.Web.UI.GridDataItem gridDataItem, string ControlName)
        {
            return GetColumnText<string>(gridDataItem, ControlName, "Text");
        }

        public static Int32 GetColumnTextInt32(this GridDataItem gridDataItem, string ControlName)
        {
            var retVal = 0;
            var txt = GetColumnText(gridDataItem, ControlName);
            Int32.TryParse(txt, out retVal);
            return retVal;
        }
        public static Int32 GetColumnTextInt32(this Telerik.Web.UI.GridDataItem gridDataItem, string ControlName)
        {
            var retVal = 0;
            var txt = GetColumnText(gridDataItem, ControlName);
            Int32.TryParse(txt, out retVal);
            return retVal;
        }

        public static Decimal GetColumnTextDecimal(this GridDataItem gridDataItem, string ControlName)
        {
            Decimal retVal = 0;
            string txt = GetColumnText(gridDataItem, ControlName);
            Decimal.TryParse(txt, out retVal);
            return retVal;
        }
        public static Decimal GetColumnTextDecimal(this Telerik.Web.UI.GridDataItem gridDataItem, string ControlName)
        {
            var retVal = 0m;
            var txt = GetColumnText(gridDataItem, ControlName);
            Decimal.TryParse(txt, out retVal);
            return retVal;
        }

        public static String GetColumnSelectedValue(this GridDataItem gridDataItem, string ControlName)
        {
            return GetColumnText<string>(gridDataItem, ControlName, "SelectedValue");
        }
        public static String GetColumnSelectedValue(this Telerik.Web.UI.GridDataItem gridDataItem, string ControlName)
        {
            return GetColumnText<string>(gridDataItem, ControlName, "SelectedValue");
        }

        public static Int32 GetColumnSelectedValueInt32(this GridDataItem gridDataItem, string ControlName)
        {
            var retVal = 0;
            var sv = GetColumnText<string>(gridDataItem, ControlName, "SelectedValue");
            Int32.TryParse(sv, out retVal);
            return retVal;
        }
        public static Int32 GetColumnSelectedValueInt32(this Telerik.Web.UI.GridDataItem gridDataItem, string ControlName)
        {
            var retVal = 0;
            var sv = GetColumnText<string>(gridDataItem, ControlName, "SelectedValue");
            Int32.TryParse(sv, out retVal);
            return retVal;
        }

        public static bool GetColumnChecked(this GridDataItem gridDataItem, string ControlName)
        {
            return GetColumnText<bool>(gridDataItem, ControlName, "Checked");
        }
        public static bool GetColumnChecked(this Telerik.Web.UI.GridDataItem gridDataItem, string ControlName)
        {
            return GetColumnText<bool>(gridDataItem, ControlName, "Checked");
        }

        public static DateTime? GetColumnSelectedDate(this GridDataItem gridDataItem, string ControlName)
        {
            return GetColumnText<DateTime?>(gridDataItem, ControlName, "SelectedDate");
        }
        public static DateTime? GetColumnSelectedDate(this Telerik.Web.UI.GridDataItem gridDataItem, string ControlName)
        {
            return GetColumnText<DateTime?>(gridDataItem, ControlName, "SelectedDate");
        }

        /// <summary>
        /// Get the underlying data item property.  This works really good at getting Linq anonymous types that are databound.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="gridDataItem"></param>
        /// <param name="PropertyName"></param>
        /// <returns></returns>
        public static T GetDataProperty<T>(this GridDataItem gridDataItem, string PropertyName)
        {
            try
            {
                var gridData = gridDataItem.DataItem;
                return (T)System.ComponentModel.TypeDescriptor.GetProperties(gridData)[PropertyName].GetValue(gridData);
            }
            catch
            {
                T retVal = default(T);
                return retVal;
            }

        }
        public static T GetDataProperty<T>(this Telerik.Web.UI.GridDataItem gridDataItem, string PropertyName)
        {
            try
            {
                var gridData = gridDataItem.DataItem;
                return (T)System.ComponentModel.TypeDescriptor.GetProperties(gridData)[PropertyName].GetValue(gridData);
            }
            catch
            {
                T retVal = default(T);
                return retVal;
            }

        }
    }
}
