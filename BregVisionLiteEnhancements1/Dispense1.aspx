<%@ Page Language="C#" MasterPageFile="~/BregVision.Master" AutoEventWireup="true" CodeBehind="Dispense1.aspx.cs" Inherits="BregVision.Dispense1" Title="Breg Vision - Dispense" %>


<%@ Register Assembly="RadWindow.Net2" Namespace="Telerik.WebControls" TagPrefix="radW" %>
<%@ Register Assembly="RadAjax.Net2" Namespace="Telerik.WebControls" TagPrefix="radA" %>

<%@ Register Assembly="RadInput.Net2" Namespace="Telerik.WebControls" TagPrefix="radI" %>

<%@ Register Assembly="RadComboBox.Net2" Namespace="Telerik.WebControls" TagPrefix="radC" %>

<%@ Register Assembly="RadToolbar.Net2" Namespace="Telerik.WebControls" TagPrefix="radTlb" %>
<%@ Register Assembly="RadGrid.Net2" Namespace="Telerik.WebControls" TagPrefix="radG" %>
<%@ Register Assembly="RadTabStrip.Net2" Namespace="Telerik.WebControls" TagPrefix="radTS" %>
<%@ Register Assembly="RadCalendar.Net2" Namespace="Telerik.WebControls" TagPrefix="radCal" %>
 <asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
 
 
 <script language=javascript>
 
 function setHourglass()
  {
    document.body.style.cursor = 'wait';
  }

</script>
 <radW:RadWindowManager ID="RadWindowManager1" runat="server">
             </radW:RadWindowManager>
             <radA:RadAjaxPanel ID="RadAjaxPanel2" runat="server" Width="100%" EnableViewState=true EnableAJAX=false>
<table width="96%" border="0">
    <tr>
        <td colspan=2>
        <b>Dispense</b>
        </td>
        <td colspan=1  align=right style="font-size:xx-small">Number of records displayed per page:
        </td><td align=left valign=bottom>
            <radI:RadNumericTextBox  ID="txtRecordsDisplayed" Text='' runat="server" ShowSpinButtons="false" Culture="English (United States)" MaxValue="100" MinValue="1" Skin="Office2007" Width="25px" NumberFormat-DecimalDigits="0" Font-Size=X-Small OnTextChanged="txtRecordsDisplayed_TextChanged" AutoPostBack=true/>
            

        </td>
        

        <td></td>
        

     </tr>
    

    <tr>
        

        <td>
            
 

        </td>
        

        <td colspan="3">
            

            <table width="100%">
                

                <tr>
                    

                    <td colspan="1" style="font-size:small">
                        

                        <asp:Label ID="lblSteps" runat="server" Font-Size="Small" />
                        

                    </td>
                    

                    <td align="right" colspan="2">
                        

                        <asp:Button ID="btnBack" runat="server" onclick="btnBack_Click" Text="Back" />
                        

                        <asp:Button ID="btnNext" runat="server" onclick="btnNext_Click" Text="Next" />
 
                        

                    </td>
                    

                </tr>
            </table>
            

        </td>
        

        <td>
            

        </td>
        

    </tr>
    

    <tr>
        

        <td>
            
 

        </td>
        

        <td colspan="3">
            


            <radTS:RadTabStrip ID="rmpMain" runat="server" AutoPostBack="True" 
                MultiPageID="pvDispense" OnTabClick="rmpMain_TabClick" Skin="Telerik" 
                SkinID="Web20" Width="695px">
        <tabs>
            <radTS:Tab ID="Tab1" runat="server" PageViewID="PageView1" 
                Text="Browse/Search">
            </radTS:Tab>
            
            <radTS:Tab ID="Tab2" runat="server" PageViewID="PageView8" 
                Text="Browse/Search Custom Brace">
            </radTS:Tab>

            <radTS:Tab ID="Tab3" runat="server" PageViewID="PageView3" 
                Text="Physician/Patient">
            </radTS:Tab>
            <radTS:Tab ID="Tab5" runat="server" PageViewID="PageView5" 
                Text="Summary/Dispense">
                   

            </radTS:Tab>
            <radTS:Tab runat="server" Enabled="False" PageViewID="PageView6" 
                Text="Receipt">
            </radTS:Tab>
            <radTS:Tab ID="Tab7" runat="server" Enabled="true" PageViewID="PageView7" 
                Text="Receipt History">
            </radTS:Tab>

        </tabs>
    </radTS:RadTabStrip>
            

            <radTS:RadMultiPage ID="pvDispense" runat="server">
        <radTS:PageView ID="PageView1" runat="server" BackColor="WhiteSmoke">
             
            <table width="100%">
 
                <tr>
                    <td width="5"></td>
                    <td><asp:Label ID="lblBrowseDispense" runat="server" Font-Size="Small" 
                            ForeColor="red"></asp:Label>&#160;</td>

                    <td align="right"> 
                        <asp:Button ID="btnAddBrowseToDispensement" runat="server" 
                            OnClick="btnAddBrowseToDispensement_Click" Text="Add to Dispensement" 
                            ToolTip="Add all products selected to dispensement on Summary/Dispense tab." 
                            Width="135px" /> 
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="4">&#160;
                           <radTlb:RadToolbar ID="RadToolbar1" runat="server" 
                            AutoPostBack="True" EnableViewState="False" OnOnClick="RadToolbar1_OnClick" 
                            Skin="inox_32x32" UseFadeEffect="True"><Items><radTlb:RadToolbarTemplateButton 
                                ID="rttbFilter" runat="server" Width="175px"><buttontemplate>Search <asp:TextBox 
                                ID="tbFilter" runat="server" Width="115"></asp:TextBox>
                                </buttontemplate>
                                </radTlb:RadToolbarTemplateButton><radTlb:RadToolbarTemplateButton 
                                ID="rttbSearchFilter" runat="server" Width="110px"><buttontemplate><radC:RadComboBox 
                                ID="cboSearchFilter" runat="server" ExpandEffect="fade" Skin="WebBlue" 
                                Width="100">
                                <Items>
                                    <radC:RadComboBoxItem ID="RadComboBoxItem3" runat="server" 
                                        Text="Product Code" Value="Code" />
                                </Items>

                                <Items><radC:RadComboBoxItem ID="RadComboBoxItem5" 
                                    runat="server" Text="Product Name" Value="Name" />
                                </Items>
                            
                            
                                <Items>
                                    <radC:RadComboBoxItem ID="RadComboBoxItem2" runat="server" 
                                        Text="Manufacturer" Value="Brand" />
                                </Items>
                                                
                                <Items>
                                    <radC:RadComboBoxItem ID="RadComboBoxItem1" runat="server" Text="HCPCs" 
                                        Value="HCPCs" />
                                </Items>
                                                


                            </radC:RadComboBox>
                            </buttontemplate>
                          
                            </radTlb:RadToolbarTemplateButton>

                            <radTlb:RadToolbarToggleButton ID="rtbFilter" 
                                runat="server" ButtonImage="Find.gif" ButtonText="Search" 
                                CausesValidation="true" CommandName="Search" Hidden="False" ToolTip="Search" 
                                Width="200px" />
                            
                            <radTlb:RadToolbarToggleButton ID="rtbBrowse" runat="server" 
                                ButtonImage="NewProject.gif" ButtonText="Browse" CausesValidation="true" 
                                CommandName="Browse" Hidden="False" ToolTip="Browse all products" 
                                Width="200px" />
                            </Items>
                        
                        </radTlb:RadToolbar>
                    </td>
                </tr>
                <tr>
                     <td align="center" colspan="4">
                    
                       <radG:RadGrid ID="grdInventory" runat="server" 
                             AllowMultiRowSelection="True" AllowPaging="True" AllowSorting="True" 
                             EnableAJAX="false" EnableAJAXLoadingTemplate="True" EnableViewState="True" 
                             GridLines="None" LoadingTemplateTransparency="10" 
                             OnDetailTableDataBind="grdInventory_DetailTableDataBind" 
                             OnItemCommand="grdInventory_ItemCommand" 
                             OnNeedDataSource="grdInventory_NeedDataSource" 
                             OnPageIndexChanged="grdInventory_PageIndexChanged" 
                             OnPreRender="grdInventory_PreRender" ShowGroupPanel="True" Skin="Office2007" 
                             Width="98%">
                        <PagerStyle Mode="NextPrevAndNumeric" />
                        <mastertableview autogeneratecolumns="False" datakeynames="SupplierID" 
                             enabletheming="true" hierarchyloadmode="ServerBind">
                            <expandcollapsecolumn>
                                <HeaderStyle Width="19px" />
                            </expandcollapsecolumn>
                            <detailtables>
                                <radG:GridTableView runat="server" AutoGenerateColumns="False" 
                             DataKeyNames="SupplierName" Width="100%">
                                    <parenttablerelation>
                                      <radG:GridRelationFields DetailKeyField="SupplierID" 
                             MasterKeyField="SupplierID" />
                                    </parenttablerelation>
                                    <expandcollapsecolumn visible="False">
                                        <HeaderStyle Width="19px" />
                                    </expandcollapsecolumn>
                                    <rowindicatorcolumn visible="False">
                                        <HeaderStyle Width="20px" />
                                    </rowindicatorcolumn>
                                    <Columns>
                                        <radG:GridClientSelectColumn UniqueName="ClientSelectColumn" />
                                        <radG:GridBoundColumn DataField="SupplierID" UniqueName="SupplierID" 
                                            Visible="False">
                                        </radG:GridBoundColumn>
                                        <radG:GridBoundColumn DataField="ProductInventoryID" 
                                            UniqueName="ProductInventoryID" Visible="False">
                                        </radG:GridBoundColumn>
                                        <radG:GridBoundColumn DataField="SupplierName" HeaderText="Supplier" 
                                            UniqueName="Category">
                                        </radG:GridBoundColumn>
                                        <radG:GridBoundColumn DataField="BrandName" HeaderText="Brand" 
                                            UniqueName="SubCategory">
                                        </radG:GridBoundColumn>
                                        <radG:GridBoundColumn DataField="ProductName" HeaderText="Product" 
                                            UniqueName="Product">
                                        </radG:GridBoundColumn>
                                        
                                         <radG:GridBoundColumn DataField="Code" 
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50" 
                                            HeaderStyle-Wrap="true" HeaderText="Product Code" 
                                            ItemStyle-HorizontalAlign="center" ItemStyle-Width="50" UniqueName="Code" />

                                        <radG:GridBoundColumn DataField="Side" 
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50" 
                                            HeaderStyle-Wrap="true" HeaderText="Side" ItemStyle-HorizontalAlign="center" 
                                            ItemStyle-Width="50" UniqueName="Side" />

                                        <radG:GridBoundColumn DataField="Size" 
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50" 
                                            HeaderStyle-Wrap="true" HeaderText="Size" ItemStyle-HorizontalAlign="center" 
                                            ItemStyle-Width="50" UniqueName="Size" />

                                        <radG:GridBoundColumn DataField="Gender" 
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50" 
                                            HeaderStyle-Wrap="true" HeaderText="Gender" ItemStyle-HorizontalAlign="center" 
                                            ItemStyle-Width="50" UniqueName="Gender" />
                            
                                        <radG:GridBoundColumn DataField="QuantityOnHand" 
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50" 
                                            HeaderStyle-Wrap="true" HeaderText="Qty. On Hand" 
                                            ItemStyle-HorizontalAlign="center" ItemStyle-Width="50" 
                                            UniqueName="QuantityOnHand" />
                            
                                                                               
                                    </Columns>
                         


                                </radG:GridTableView>
                            </detailtables>
                            <rowindicatorcolumn visible="False">
                                <HeaderStyle Width="20px" />
                            </rowindicatorcolumn>
                            <Columns>
                                <radG:GridBoundColumn DataField="SupplierID" UniqueName="SupplierID" 
                                    Visible="False">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="Brand" HeaderText="Brand" 
                                    UniqueName="Brand">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="Name" HeaderText="Name" 
                                    UniqueName="Name">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="LastName" UniqueName="LastName">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="PhoneWork" HeaderText="Phone" 
                                    UniqueName="PhoneWork">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="Address" HeaderText="Address" 
                                    UniqueName="Address">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="City" HeaderText="City" 
                                    UniqueName="City">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="ZipCode" HeaderText="Zip Code" 
                                    UniqueName="ZipCode">
                                </radG:GridBoundColumn>
                            </Columns>
                         
                        </mastertableview>
                        <grouppanel visible="True">
                        </grouppanel>
                        <clientsettings allowcolumnsreorder="True" allowdragtogroup="True" 
                             reordercolumnsonclient="True">
                            <selecting allowrowselect="True" />
                             
                        </clientsettings>
                                              
                        </radG:RadGrid>
                     </td>
                    </tr>
                </table>

        </radTS:PageView>
        
        <radTS:PageView ID="PageView8" runat="server" BackColor="WhiteSmoke">
             
            <table width="100%">
 
                <tr>
                    <td width="5"></td>
                    <td><asp:Label ID="lblBrowseDispenseCustomBrace" runat="server" Font-Size="Small" 
                            ForeColor="red"></asp:Label>&#160;</td>

                    <td align="right"> 
                        <asp:Button ID="btnAddCustomBraceToDispensement" runat="server" 
                            OnClick="btnAddCustomBraceToDispensement_Click" Text="Add to Dispensement" 
                            ToolTip="Add all products selected to dispensement on Summary/Dispense tab." 
                            Width="135px" /> 
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="4">&#160;
                           <radTlb:RadToolbar ID="rtbCustomBrace" runat="server" 
                            AutoPostBack="True" EnableViewState="False" OnOnClick="rtbCustomBrace_OnClick" 
                            Skin="inox_32x32" UseFadeEffect="True">
                            
                                <Items>
                                    <radTlb:RadToolbarTemplateButton ID="rttbFilterCustomBrace" runat="server" Width="175px">
                                        <buttontemplate>
                                            Search <asp:TextBox ID="tbFilterCustomBrace" runat="server" Width="115"></asp:TextBox>
                                        </buttontemplate>
                                    </radTlb:RadToolbarTemplateButton>
                                    <radTlb:RadToolbarTemplateButton ID="rttbSearchFilterCustomBrace" runat="server" Width="110px">
                                        <buttontemplate>
                                            <radC:RadComboBox ID="cboSearchFilterCustomBrace" runat="server" ExpandEffect="fade" Skin="WebBlue" Width="100">
                                                <Items>
                                                    <radC:RadComboBoxItem ID="RadComboBoxItem2" runat="server" 
                                                        Text="PatientID" Value="PatientID" />
                                                </Items>
                                                <Items>
                                                    <radC:RadComboBoxItem ID="RadComboBoxItem1" runat="server" Text="PONumber" 
                                                        Value="PONumber" />
                                                </Items>
                                            </radC:RadComboBox> 
                                        </buttontemplate>
                                    </radTlb:RadToolbarTemplateButton>

                                    <radTlb:RadToolbarToggleButton ID="rttbSearchCustomBrace" 
                                        runat="server" ButtonImage="Find.gif" ButtonText="Search" 
                                        CausesValidation="true" CommandName="Search" Hidden="False" ToolTip="Search" 
                                        Width="200px" />
                                    
                                    <radTlb:RadToolbarToggleButton ID="rttbBrowseCustomBrace" runat="server" 
                                        ButtonImage="NewProject.gif" ButtonText="Browse" CausesValidation="true" 
                                        CommandName="Browse" Hidden="False" ToolTip="Browse all products" 
                                        Width="200px" />
                                </Items>
                        
                        </radTlb:RadToolbar>
                    </td>
                </tr>
                <tr>
                     <td align="center" colspan="4">
                    
                       <radG:RadGrid ID="grdCustomBrace" runat="server" 
                             AllowMultiRowSelection="False" AllowPaging="True" AllowSorting="True" 
                             EnableAJAX="false" EnableAJAXLoadingTemplate="True" EnableViewState="True" 
                             GridLines="None" LoadingTemplateTransparency="10"     
                             OnNeedDataSource="grdCustomBrace_NeedDataSource" 
                             OnPageIndexChanged="grdCustomBrace_PageIndexChanged" 
                             OnPreRender="grdCustomBrace_PreRender" ShowGroupPanel="True" Skin="Office2007" 
                             Width="98%">
                        <PagerStyle Mode="NextPrevAndNumeric" />
                                                
                        <MasterTableView Name="mtvCustomBraceOutstanding_ParentLevel" DataKeyNames="BregVisionOrderID" AutoGenerateColumns="False"  EnableTheming="True" RetrieveAllDataFields="true" HierarchyLoadMode="ServerBind" >
                            <ExpandCollapseColumn Resizable="False">
                                <HeaderStyle Width="20px" />
                            </ExpandCollapseColumn>
                            <RowIndicatorColumn Visible="true">
                                <HeaderStyle Width="20px" />
                            </RowIndicatorColumn>
                            <Columns>
                                <radG:GridClientSelectColumn UniqueName="ClientSelectColumn" />
                                <radG:GridBoundColumn DataField="BregVisionOrderID" UniqueName="BregVisionOrderID"   visible="False">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="PhysicianID" UniqueName="PhysicianID"   visible="False">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="PurchaseOrder" HeaderText="Purchase Order" UniqueName="PurchaseOrder"  ItemStyle-HorizontalAlign="Left">
                                </radG:GridBoundColumn>
                                 <radG:GridBoundColumn DataField="CustomPurchaseOrderCode" HeaderText="Custom PO Number" UniqueName="CustomPurchaseOrderCode" ItemStyle-Width=120px >
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="PatientName" HeaderText="Patient ID" UniqueName="PatientName">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="CreatedUserID" HeaderText="Created by" UniqueName="CreatedUserID" Visible="false">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="Status" UniqueName="Status" Visible="false">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="ShippingType" HeaderText="Shipping Type" UniqueName="ShippingType" Visible="false">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="Total" HeaderText="Total" UniqueName="Total"  DataFormatString="{0:C}" Visible="false">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="CreatedDate" HeaderText="Date Created" UniqueName="CreatedDate"  ItemStyle-HorizontalAlign="Left" >
                                </radG:GridBoundColumn>
                                
                            </Columns>
                            
                            <detailtables>
                                <radG:GridTableView runat="server"  Name="mtvCustomBraceOutstanding_Detail1Level" AutoGenerateColumns="false" DataKeyNames="ProductInventoryID" Width="100%" RetrieveAllDataFields="true" HierarchyLoadMode="ServerBind">
                                    
                                    <parenttablerelation>
                                      <radG:GridRelationFields DetailKeyField="BregVisionOrderID" MasterKeyField="BregVisionOrderID" />
                                    </parenttablerelation>
                                    
                                    <expandcollapsecolumn visible="false" Resizable="False">
                                        <HeaderStyle Width="20px" />
                                    </expandcollapsecolumn>
                                    
                                    <rowindicatorcolumn visible="False">
                                        <HeaderStyle Width="20px" />
                                    </rowindicatorcolumn>
                                    
                                    <Columns>
                                        
                                        
                                        <radG:GridBoundColumn DataField="BregVisionOrderID" 
                                            UniqueName="BregVisionOrderID" Visible="False">
                                        </radG:GridBoundColumn>

                                        <radG:GridBoundColumn DataField="ProductInventoryID" 
                                            UniqueName="ProductInventoryID" Visible="False">
                                        </radG:GridBoundColumn>

                                        <radG:GridBoundColumn DataField="ProductName" HeaderText="Product" 
                                            UniqueName="Product">
                                        </radG:GridBoundColumn>
                                        
                                         <radG:GridBoundColumn DataField="Code" 
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50" 
                                            HeaderStyle-Wrap="true" HeaderText="Product Code" 
                                            ItemStyle-HorizontalAlign="center" ItemStyle-Width="50" UniqueName="Code" />

                                        <radG:GridBoundColumn DataField="Side" 
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50" 
                                            HeaderStyle-Wrap="true" HeaderText="Side" ItemStyle-HorizontalAlign="center" 
                                            ItemStyle-Width="50" UniqueName="Side" />

                                        <radG:GridBoundColumn DataField="Size" 
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50" 
                                            HeaderStyle-Wrap="true" HeaderText="Size" ItemStyle-HorizontalAlign="center" 
                                            ItemStyle-Width="50" UniqueName="Size" />

                                        <radG:GridBoundColumn DataField="Gender" 
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50" 
                                            HeaderStyle-Wrap="true" HeaderText="Gender" ItemStyle-HorizontalAlign="center" 
                                            ItemStyle-Width="50" UniqueName="Gender" />
                            
                                        <radG:GridBoundColumn DataField="QuantityOnHand" 
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50" 
                                            HeaderStyle-Wrap="true" HeaderText="Qty. On Hand" 
                                            ItemStyle-HorizontalAlign="center" ItemStyle-Width="50" 
                                            UniqueName="QuantityOnHand" />
                            
                                                                               
                                    </Columns>
                         


                                </radG:GridTableView>
                            </detailtables>
                        </MasterTableView>
                        <grouppanel visible="True">
                        </grouppanel>
                        <clientsettings allowcolumnsreorder="True" allowdragtogroup="True" 
                             reordercolumnsonclient="True">
                            <selecting allowrowselect="True" />
                             
                        </clientsettings>
                                              
                        </radG:RadGrid>
                     </td>
                    </tr>
                </table>

        </radTS:PageView>

 
        <radTS:PageView ID="PageView3" runat="server" BackColor="WhiteSmoke">
            <table>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblPatientPhysician" runat="server" Font-Size="Small" ForeColor="red"></asp:Label>&#160;
                    </td>
                </tr>
                
                <tr>
                    <td align="right">
                       Physician: 
                    </td>
                    <td>
                        <radC:RadComboBox ID="cboPhysicians" runat="server" ExpandEffect="fade" Skin="WebBlue" SkinsPath="~/RadControls/ComboBox/Skins"></radC:RadComboBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">Patient Code: </td>
                    <td>
                        <radI:RadTextBox  ID="txtPatientCode" runat="server" MaxLength=50 Width="315px"></radI:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right"></td>
                    <td>
                        <asp:HiddenField ID="hdnBVOID" runat="server" />
                    </td>
                </tr>
            </table>

            </radTS:PageView>
            
         <radTS:PageView ID="PageView4" runat="server">&#160;</radTS:PageView>
         <radTS:PageView 
                ID="PageView5" runat="server" BackColor="WhiteSmoke">
                <table width="100%"><tr><td><asp:Label  
                    ID="lblSummary" runat="server" 
                            Font-Size="Small" ForeColor="red"> &nbsp</asp:Label></td><td align=right><asp:Button 
                        ID="btnDispenseProducts" runat="server" OnClick="btnDispenseProducts_Click" 
                        Text="Dispense Product(s)" ToolTip="Dispense product(s)" Width="130px" />
                    </td>
                </tr>
                <tr>
                   <td colspan="2">
                   <table width="100%">
                    <tr>
                        <td>
                            <label style="font-size:small">Physician:   </label><asp:Label 
                                ID="lblSummaryPhysician" runat="server" Font-Bold="true" Font-Size="Small" />&#160;
                              <asp:Label ID="lblSummaryPatientIDLegend" runat="server" Font-Size="Small">Patient Code: </asp:Label><asp:Label 
                                ID="lblSummaryPatientID" runat="server" Font-Bold="true" Font-Size="Small" />
                        </td>
                        <td align="right">
                            <label style="font-size:small">Date Dispensed:</label>
                              </td><td align="left"><radCal:RadDatePicker ID="dtDispensed" runat="server" 
                                DateInput-DisplayDateFormat="MM/dd/yy" DateInput-MinDate="1/1/1900" 
                                Font-Size="Medium" 
                                ToolTip="Dates can be entered in the following formats(01012007,01/01/2007,January 1,2007)" 
                                Width="75px"><calendar id="Calendar1" runat="server" 
                                rangemaxdate="06/06/2079" rangemindate="01/01/1900" skin="Outlook"></calendar>
                                 </radCal:RadDatePicker></td></tr></table></td></tr><tr><td 
                        colspan="2"><radG:RadGrid ID="grdSummary" runat="server" 
                        AllowSorting="True" AutoGenerateColumns="False" EnableAJAX="True" 
                        GridLines="None" Height="100%" OnDeleteCommand="grdSummary_DeleteCommand" 
                        OnItemDataBound="grdSummary_ItemDataBound" OnLoad="grdSummary_OnLoad" 
                        Skin="Office2007" Width="100%"><PagerStyle Mode="NextPrevAndNumeric" />
                        <mastertableview autogeneratecolumns="False">
                            <expandcollapsecolumn visible="False">
                                <HeaderStyle Width="19px" />
                            </expandcollapsecolumn>
                            <rowindicatorcolumn visible="False">
                                <HeaderStyle Width="20px" />
                            </rowindicatorcolumn>
                            <Columns>
                                 <radG:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" 
                                    ConfirmText="Delete this product?" 
                                    ImageUrl="RadControls\Grid\Skins\Office2007\delete.gif" Text="Delete" 
                                    UniqueName="Delete"></radG:GridButtonColumn>
                                <radG:GridBoundColumn DataField="PracticeLocationID" Display="False" 
                                    UniqueName="PracticeLocationID">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="ProductInventoryID" Display="False" 
                                    UniqueName="ProductInventoryID">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="PracticeCatalogProductID" 
                                    Display="False" UniqueName="PracticeCatalogProductID">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="SupplierShortName" 
                                    HeaderText="Supplier" UniqueName="SupplierShortName">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="BrandShortName" HeaderText="Brand" 
                                    UniqueName="BrandShortName">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="ShortName" HeaderText="Name" 
                                    UniqueName="ShortName">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="Code" HeaderText="Code" 
                                    ItemStyle-HorizontalAlign="Center" UniqueName="ProductName">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="Side" HeaderText="Side" 
                                    ItemStyle-HorizontalAlign="Center" UniqueName="Side">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="Size" HeaderText="Size" 
                                    ItemStyle-HorizontalAlign="Center" UniqueName="Size">
                                </radG:GridBoundColumn>                                
                                <radG:GridBoundColumn DataField="HCPCsString" HeaderText="HCPCs" 
                                    UniqueName="HCPCs">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="WholesaleCost" DataFormatString="{0:C}" 
                                    Display="False" HeaderText="Cost" UniqueName="WholesaleCost">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="BillingCharge" DataFormatString="{0:C}" 
                                    HeaderStyle-Width="20" HeaderStyle-Wrap="true" HeaderText="Billing Charge" 
                                    UniqueName="BillingCharge">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="BillingChargeCash" 
                                    DataFormatString="{0:C}" HeaderStyle-Width="20" HeaderStyle-Wrap="true" 
                                    HeaderText="Billing Charge Cash" UniqueName="BillingChargeCash">
                                </radG:GridBoundColumn>
                                
                                <radG:GridTemplateColumn HeaderStyle-Width="20" HeaderStyle-Wrap="true" 
                                    HeaderText="Cash Override" ItemStyle-HorizontalAlign="Center" 
                                    UniqueName="DMEDeposit">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkDMEDeposit" runat="server" />
                                    </ItemTemplate>
                                
                                </radG:GridTemplateColumn>
                                <radG:GridTemplateColumn DataField="DMEDeposit" HeaderText="DME Deposit" 
                                    UniqueName="DMEDeposit2">
                                <ItemTemplate>
                                        <radI:RadNumericTextBox ID="txtDMEDeposit" runat="server" 
                                        Culture="English (United States)" Font-Size="x-small" 
                                        LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" 
                                        NumberFormat-DecimalDigits="2" ShowSpinButtons="True" Skin="Office2007" 
                                        SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" 
                                        Text='<%# bind("DMEDeposit") %>' Type="Currency" Width="20px">
                                    </radI:RadNumericTextBox>
                                    </ItemTemplate>
                                
                                </radG:GridTemplateColumn>
                                                     
                                <radG:GridBoundColumn DataField="QuantityOnHandPerSystem" 
                                    HeaderStyle-Width="20" HeaderStyle-Wrap="true" HeaderText="Qty. On Hand" 
                                    ItemStyle-HorizontalAlign="Center" UniqueName="QuantityOnHandPerSystem">
                                </radG:GridBoundColumn>
                             <radG:GridTemplateColumn HeaderStyle-Width="20" HeaderStyle-Wrap="true" 
                                    HeaderText="Qty. to Dispense" UniqueName="TemplateColumn3">
                                <ItemTemplate>
                                    &#160;<radI:RadNumericTextBox ID="txtQuantityDispensed" runat="server" 
                                        Culture="English (United States)" Font-Size="x-small" 
                                        LabelCssClass="radLabelCss_Default" MaxValue="999" MinValue="0" 
                                        NumberFormat-DecimalDigits="0" ShowSpinButtons="True" Skin="Office2007" 
                                        SpinDownCssClass="Office2007" SpinUpCssClass="Office2007" Text="1" Width="41px">
                                    </radI:RadNumericTextBox>
                                </ItemTemplate>
                                
                            </radG:GridTemplateColumn>
                            </Columns>
                    
                        </mastertableview>
              </radG:RadGrid>
              </td>
            </tr>
            </table>
            <div id="Supplemental" runat="server" style="visibility:hidden">
            <table>
                <tr>
                    <td style="color:Aqua">
                        Your products have been dispensed. Please click the receipt tab to view and print this dispensement. 
                          </td></tr></table></div>
        </radTS:PageView>
        <radTS:PageView ID="PageView6" runat="server" 
                BackColor="WhiteSmoke"><!--Receipt--><table border="0" width="96%"><tr><td 
                    align="right"><asp:Button ID="btnPrintReceipt" runat="server" 
                    OnClick="btnPrintReceipt_Click" Text="Printer Friendly Format" />
        
                </td>
               </tr>
               <tr>
                 <td align="center">
                    <div id="Receipt" runat="server">
                    </div>
 
                   </td>
                 </tr>
             </table>

    
        </radTS:PageView>
        <radTS:PageView ID="PageView7" runat="server" 
                BackColor="WhiteSmoke"><!--Receipt-->
                <table border="0" width="96%">
                    <tr>
                    <td ><asp:Label id="lblReceiptHistory" runat=server Font-Size="Small">Please select a receipt and click the 'Print Preview' button.</asp:Label></td>
                    <td align="right">
                    
                        <asp:Button ID="btnPrintReceiptHistory" runat="server" 
                        OnClick="btnPrintReceiptHistory_Click" Text="Print Preview" />
                </td>
               </tr>
               <tr>
                 <td colspan=2 align="center">
                    
                    <div id="Div1" runat="server">
                        <radG:RadGrid ID="grdReceiptHistory" runat="server" 
                             AllowMultiRowSelection="false" AllowPaging="True" AllowSorting="True" 
                             EnableAJAX="true" EnableAJAXLoadingTemplate="True" EnableViewState="True" 
                             GridLines="None" LoadingTemplateTransparency="10" 
                             OnDetailTableDataBind="grdReceiptHistory_DetailTableDataBind" 
                             OnNeedDataSource="grdReceiptHistory_NeedDataSource" 
                             ShowGroupPanel="True" Skin="Office2007" 
                             Width="98%">
                        <PagerStyle Mode="NextPrevAndNumeric" />
                        <mastertableview autogeneratecolumns="False" datakeynames="DispenseID" 
                             enabletheming="true" hierarchyloadmode="ServerBind">
                            <expandcollapsecolumn>
                                <HeaderStyle Width="19px" />
                            </expandcollapsecolumn>
                            <detailtables>
                                <radG:GridTableView runat="server" AutoGenerateColumns="False" 
                             DataKeyNames="DispenseID" Width="100%">
                                    <parenttablerelation>
                                      <radG:GridRelationFields DetailKeyField="DispenseID" 
                             MasterKeyField="DispenseID" />
                                    </parenttablerelation>
                                    <expandcollapsecolumn visible="False">
                                        <HeaderStyle Width="19px" />
                                    </expandcollapsecolumn>
                                    <rowindicatorcolumn visible="False">
                                        <HeaderStyle Width="20px" />
                                    </rowindicatorcolumn>
                                    <Columns>
                                        
                                        <radG:GridBoundColumn DataField="DispenseID" UniqueName="DispenseID" 
                                            Visible="False">
                                        </radG:GridBoundColumn>
                                        <radG:GridBoundColumn DataField="PhysicianName"  HeaderText="Physician" 
                                            UniqueName="PhysicianName" Visible="true">
                                        </radG:GridBoundColumn>
                                        <radG:GridBoundColumn DataField="SupplierShortName" HeaderText="Brand" 
                                            UniqueName="SupplierShortName">
                                        </radG:GridBoundColumn>
                                        <radG:GridBoundColumn DataField="Name" HeaderText="Product" 
                                            UniqueName="Product">
                                        </radG:GridBoundColumn>
                                        
                                         <radG:GridBoundColumn DataField="Code" 
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50" 
                                            HeaderStyle-Wrap="true" HeaderText="Product Code" 
                                            ItemStyle-HorizontalAlign="center" ItemStyle-Width="50" UniqueName="Code" />

                                        <radG:GridBoundColumn DataField="Side" 
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50" 
                                            HeaderStyle-Wrap="true" HeaderText="Side" ItemStyle-HorizontalAlign="center" 
                                            ItemStyle-Width="50" UniqueName="Side" />

                                        <radG:GridBoundColumn DataField="Size" 
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50" 
                                            HeaderStyle-Wrap="true" HeaderText="Size" ItemStyle-HorizontalAlign="center" 
                                            ItemStyle-Width="50" UniqueName="Size" />
                                        <radG:GridBoundColumn DataField="DMEDeposit" HeaderText="DME Deposit" 
                                            DataFormatString="{0:C}" UniqueName="DMEDeposit">
                                        </radG:GridBoundColumn>

                                        <radG:GridBoundColumn DataField="Quantity" 
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50" 
                                            HeaderStyle-Wrap="true" HeaderText="Qty" 
                                            ItemStyle-HorizontalAlign="center" ItemStyle-Width="50" 
                                            UniqueName="Quantity" />
                                        <radG:GridBoundColumn DataField="LineTotal" HeaderText="Billing Charge" 
                                            DataFormatString="{0:C}" UniqueName="LineTotal">
                                        </radG:GridBoundColumn>
                            
                                                                               
                                    </Columns>
                         


                                </radG:GridTableView>
                            </detailtables>
                            <rowindicatorcolumn visible="False">
                                <HeaderStyle Width="20px" />
                            </rowindicatorcolumn>
                            <Columns>
                                <radG:GridClientSelectColumn UniqueName="ClientSelectColumn" />
                                <radG:GridBoundColumn DataField="DispenseID" UniqueName="DispenseID" 
                                    Visible="False">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="CreatedWithHandheld" HeaderText="Created w/ Handheld" 
                                    UniqueName="Handheld" ItemStyle-HorizontalAlign="center" ItemStyle-Width="120" HeaderStyle-Width="120" >
                                </radG:GridBoundColumn>
                                
                                <radG:GridBoundColumn DataField="PatientCode" HeaderText="Patient Code" 
                                    UniqueName="PatientCode">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="Total" HeaderText="Total" 
                                   DataFormatString="{0:C}"  UniqueName="Total">
                                </radG:GridBoundColumn>
                                <radG:GridBoundColumn DataField="DateDispensed" HeaderText="Date Dispensed" UniqueName="DateDispensed">
                                </radG:GridBoundColumn>
                            </Columns>
                         
                        </mastertableview>
                        <grouppanel visible="True">
                        </grouppanel>
                        <clientsettings allowcolumnsreorder="True" allowdragtogroup="True" 
                             reordercolumnsonclient="True">
                            <selecting allowrowselect="True" />
                             
                        </clientsettings>
                                              
                        </radG:RadGrid>
                    
                    
                    </div>
 
                   </td>
                 </tr>
             </table>

    
        </radTS:PageView>


        &#160;&#160;
    </radTS:RadMultiPage>
            
 
            

        </td>
        
 
 

    </tr>
    
 
 


  
  

  </table>
                 

  <asp:Label ID="lblTurnOffDispenseSummarySafeguard" Visible="false" Text="SetThisTextToEmptyStringToTurnOnSafeguard" runat="server" />
                 

 </radA:RadAjaxPanel>
 </asp:Content>

