<%@ Page Language="C#" MasterPageFile="~/MasterPages/SiteMaster.Master" AutoEventWireup="true"
CodeBehind="Inventory.aspx.cs" Inherits="BregVision.Inventory" Title="Breg Vision - Inventory" %>

<%@ MasterType VirtualPath="~/MasterPages/SiteMaster.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
  <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
      $(function() {
        $telerik.isIE7 = false;
        $telerik.isIE6 = false;
      });
    </script>
  </telerik:RadScriptBlock>
  <telerik:RadCodeBlock runat="server">
    <script type="text/javascript">
      Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(startAjaxRequest);
      Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endAjaxRequest);

      var btnAddtoCartDisabled = false;

      function startAjaxRequest(sender, args) {
        var b;

        b = document.getElementById('<%= btnAddtoCart.ClientID %>');
        btnAddtoCartDisabled = b.disabled;
        b.disabled = true;
      }

      function endAjaxRequest(sender, args) {
        document.getElementById('<%= btnAddtoCart.ClientID %>').disabled = btnAddtoCartDisabled;
      }
    </script>
  </telerik:RadCodeBlock>
  <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    <AjaxSettings>
      <telerik:AjaxSetting AjaxControlID="ctlSearchToolbar">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="lblStatusReplacedText" />
          <telerik:AjaxUpdatedControl ControlID="lblStatusReplaced" />
          <telerik:AjaxUpdatedControl ControlID="lblStatus"/>
          <telerik:AjaxUpdatedControl ControlID="lblBrowseDispense"/>
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="btnAddtoCart">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="lblStatusReplacedText" />
          <telerik:AjaxUpdatedControl ControlID="lblStatusReplaced" />
          <telerik:AjaxUpdatedControl ControlID="lblStatus"/>
          <telerik:AjaxUpdatedControl ControlID="lblBrowseDispense"/>
          <telerik:AjaxUpdatedControl ControlID="grdInventory" LoadingPanelID="RadAjaxLoadingPanel1"/>
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="grdInventory">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="lblStatusReplacedText" />
          <telerik:AjaxUpdatedControl ControlID="lblStatusReplaced" />
          <telerik:AjaxUpdatedControl ControlID="lblStatus"/>
          <telerik:AjaxUpdatedControl ControlID="lblBrowseDispense"/>
          <telerik:AjaxUpdatedControl ControlID="grdInventory" LoadingPanelID="RadAjaxLoadingPanel1"/>
        </UpdatedControls>
      </telerik:AjaxSetting>
    </AjaxSettings>
  </telerik:RadAjaxManager>
  <telerik:RadToolTipManager ID="RadToolTipManager1" Width="300px" Height="100px" RelativeTo="Element"
                             Position="BottomRight" runat="server" AnimationDuration="200">
    <WebServiceSettings Path="HelpSystem/ToolTipWebService.asmx" Method="GetToolTip"/>
  </telerik:RadToolTipManager>
  <telerik:RadWindowManager ID="RadWindowManager1" runat="server"/>
  <div class="flex-row subWelcome-bar">
    <h1 class="flex-grow">Inventory</h1>

    <bvu:RecordsPerPage ID="ctlRecordsPerPage" runat="server"/>
    <div class="flex-grow">&nbsp;</div>
  </div>
  <div class="subTabContent-container">
    <div class="content">
      <div class="section">
        <div class="flex-row actions-bar">
          <div class="search-container flex-row">
            <bvu:SearchToolbar ID="ctlSearchToolbar" runat="server"/>
            <div class="flex-row">
              <asp:CheckBox ID="chkAllLocations" runat="server" Text="Search All Locations" Visible="False"/>
              <bvu:Help ID="Help2" runat="server" HelpID="93"/>
            </div>
          </div>
            <div class="legend">
            <div class="priority-1">
              <span class="priorityIcon"></span>
              <span>Critical</span>
            </div>
            <div class="priority-2">
              <span class="priorityIcon"></span>
              <span>Reorder</span>
            </div>
            <div class="shoppingCart">
              <span class="priorityIcon"></span>
              <span>In Cart</span>
            </div>
            <div class="priority-3">
              <span class="priorityIcon"></span>
              <span>On Order</span>
            </div>
          </div>
          <div class="flex-row">
              
            <bvu:Help ID="Help16" runat="server" HelpID="95"/>
            <asp:Button ID="btnAddtoCart" runat="server" Text="Add to Cart" OnClick="btnAddtoCart_Click" CssClass="button-primary"/>
          </div>
        </div>
		<div>
			<table>
				<tr>
					<td>
						<asp:Label CssClass="WarningMsgReplacement" ID="lblStatusReplaced" OnPreRender="LabelPreRenderHideShow" runat="server" Text=" "></asp:Label>
					</td>
					<td>
						<asp:Label CssClass="WarningMsgReplacementText" ID="lblStatusReplacedText" OnPreRender="LabelPreRenderHideShow" runat="server"></asp:Label>
					</td>
				</tr>
			</table>
            <asp:Label ID="lblStatus" runat="server" CssClass="WarningMsg" OnPreRender="LabelPreRenderHideShow"></asp:Label>
            <asp:Label ID="lblBrowseDispense" runat="server" CssClass="WarningMsg" OnPreRender="LabelPreRenderHideShow"></asp:Label>
		</div>
      </div>
      <div class="section">
        <bvu:VisionRadGridAjax ID="grdInventory" runat="server" EnableLinqExpressions="false"
                               OnPreRender="grdInventory_PreRender" OnNeedDataSource="grdInventory_NeedDataSource"
                               OnDetailTableDataBind="grdInventory_DetailTableDataBind" OnPageIndexChanged="grdInventory_PageIndexChanged"
                               AllowMultiRowSelection="True" Width="100%" Skin="Breg" EnableEmbeddedSkins="False" AddNonBreakingSpace="False"             OnItemDataBound="grdInventory_OnItemDataBound">

          <MasterTableView DataKeyNames="SupplierID" Width="100%">
            <DetailTables>
              <telerik:GridTableView runat="server" DataKeyNames="SupplierID" HierarchyLoadMode="ServerOnDemand"
                                     AutoGenerateColumns="False" Width="100%" AddNonBreakingSpace="False" Name="ChildGrid">
                <ParentTableRelation>
                  <telerik:GridRelationFields DetailKeyField="SupplierID" MasterKeyField="SupplierID"/>
                </ParentTableRelation>
                <Columns>
                  <telerik:GridClientSelectColumn UniqueName=   "ClientSelectColumn" HeaderStyle-Width="35px"
                                                  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-BorderColor="#A2B6CB"
                                                  HeaderStyle-BorderWidth="1px" HeaderStyle-BorderStyle="Solid" ItemStyle-Wrap="false"/>
                <telerik:GridBoundColumn DataField="IsShoppingCart" UniqueName="IsShoppingCart" Visible="False" />
				<telerik:GridBoundColumn DataField="NewPracticeCatalogProductID" Visible="False" />
				<telerik:GridBoundColumn DataField="NewProductName" Visible="False" />
				<telerik:GridBoundColumn DataField="NewProductCode" Visible="False" />
                <telerik:GridBoundColumn DataField="Priority" UniqueName="Priority" visible="False"/>
                  <telerik:GridTemplateColumn HeaderStyle-Width="30px" >
                      
                      <ItemTemplate>
                        <asp:Label runat="server" ID="lblPriority">
                            <span class="priorityIcon">
                                <asp:Label ID="prioritySplit" runat="server" Visible='<%# Boolean.Parse(Eval("IsShoppingCart").ToString()) %>' >
                                    <div class="priority-split"></div>
                                </asp:Label> 
                            </span>
                        </asp:Label>
                      </ItemTemplate>
                  </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="SupplierID" UniqueName="SupplierID" Visible="False">
                  </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="Product" HeaderText="Product" UniqueName="Product">
                  </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="Code" HeaderText="Code" UniqueName="Code">
                  </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="Packaging" HeaderText="Pkg." UniqueName="Packaging">
                  </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="LeftRightSide" HeaderText="Side" UniqueName="LeftRightSide">
                  </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="Size" HeaderText="Size" UniqueName="Size">
                  </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="Gender" HeaderText="Gender" UniqueName="Gender">
                  </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="Color" HeaderText="Color" UniqueName="color">
                  </telerik:GridBoundColumn>
                  <telerik:GridTemplateColumn UniqueName="ParLevel" DataField="ParLevel" HeaderStyle-Wrap="true"
                                              HeaderStyle-Width="50">
                    <HeaderTemplate>
                      <asp:Label ID="Label2" runat="server" Text="Par"/>
                      <bvu:Help ID="help96" runat="server" HelpID="96"/>
                    </HeaderTemplate>
                    <ItemTemplate>
                      <asp:Label ID="labParLevel" runat="server" Text='<%# Eval("ParLevel") %>'/>
                    </ItemTemplate>
                  </telerik:GridTemplateColumn>
                  <telerik:GridTemplateColumn UniqueName="QOH" DataField="QuantityOnHand" HeaderStyle-Wrap="true"
                                              HeaderStyle-Width="50">
                    <HeaderTemplate>
                      <asp:Label runat="server" Text="On Hand"/>
                      <bvu:Help ID="help97" runat="server" HelpID="97"/>
                    </HeaderTemplate>
                    <ItemTemplate>
                      <asp:Label ID="labQuantityOnHand" runat="server" Text='<%# Eval("QuantityOnHand") %>'/>
                    </ItemTemplate>
                  </telerik:GridTemplateColumn>
                  <telerik:GridTemplateColumn UniqueName="QOO" DataField="QuantityOnOrder" HeaderStyle-Wrap="true"
                                              HeaderStyle-Width="50">
                    <HeaderTemplate>
                      <asp:Label ID="Label3" runat="server" Text="On Order"/>
                      <bvu:Help ID="help98" runat="server" HelpID="98"/>
                    </HeaderTemplate>
                    <ItemTemplate>
                      <asp:Label ID="labQuantityOnOrder" runat="server" Text='<%# Eval("QuantityOnOrder") %>'/>
                    </ItemTemplate>
                  </telerik:GridTemplateColumn>
                  <telerik:GridTemplateColumn UniqueName="SuggestedReorderLevel" DataField="SuggestedReorderLevel"
                                              HeaderText="Sugg. Order #" HeaderStyle-Wrap="true" HeaderStyle-Width="70">
                    <ItemTemplate>
                      <telerik:RadNumericTextBox ID="txtSuggReorderLevel" Text='<%# Eval("SuggestedReorderLevel") %>'
                                                    CssClass="suggestedOrderTextBox"   runat="server"  ShowSpinButtons="True" Culture="English (United States)" MaxValue="999"
                                                       MinValue="0" Width="70" NumberFormat-DecimalDigits="0">
                        <NumberFormat DecimalDigits="0"/>
                      </telerik:RadNumericTextBox>
                    </ItemTemplate>
                  </telerik:GridTemplateColumn>
                  <telerik:GridBoundColumn DataField="IsNotFlaggedForReorder" UniqueName="IsNotFlaggedForReorder" Visible="False"/>
                  <telerik:GridBoundColumn DataField="CriticalLevel" UniqueName="CriticalLevel" Visible="False"/>
                  <telerik:GridBoundColumn DataField="PracticeLocationName" HeaderText="Location" UniqueName="PracticeLocationName"

                                           Visible="false">
                  </telerik:GridBoundColumn>
                  <telerik:GridBoundColumn DataField="practicecatalogproductid" UniqueName="practicecatalogproductid"
                                           Display="False">
                  </telerik:GridBoundColumn>
                </Columns>
              </telerik:GridTableView>
            </DetailTables>
            <Columns>
              <telerik:GridBoundColumn DataField="SupplierID" UniqueName="SupplierID" Visible="False"></telerik:GridBoundColumn>
              <telerik:GridBoundColumn DataField="Brand" HeaderText="Brand" UniqueName="Brand">
              </telerik:GridBoundColumn>
              <telerik:GridBoundColumn DataField="Name" HeaderText="Name" UniqueName="Name">
              </telerik:GridBoundColumn>
              <telerik:GridBoundColumn DataField="PhoneWork" HeaderText="Phone" UniqueName="PhoneWork">
              </telerik:GridBoundColumn>
              <telerik:GridBoundColumn DataField="Address" HeaderText="Address" UniqueName="Address">
              </telerik:GridBoundColumn>
              <telerik:GridBoundColumn DataField="City" HeaderText="City" UniqueName="City">
              </telerik:GridBoundColumn>
              <telerik:GridBoundColumn DataField="ZipCode" HeaderText="Zip Code" UniqueName="ZipCode">
              </telerik:GridBoundColumn>
            </Columns>
          </MasterTableView>
        </bvu:VisionRadGridAjax>
      </div>
    </div>
  </div>
  <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" InitialDelayTime="500" EnableAjaxSkinRendering="True" BackgroundPosition="Center" Skin="Breg" EnableEmbeddedSkins="False">
    <img src="App_Themes/Breg/images/Common/loading.gif" />
  </telerik:RadAjaxLoadingPanel>
</asp:Content>
