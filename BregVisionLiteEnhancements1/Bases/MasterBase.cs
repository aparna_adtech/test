﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BregVision.Bases
{
    public class MasterBase : System.Web.UI.MasterPage
    {
        #region public properties

        public int userID
        {
            get
            {
                return GetSessionInt32("UserID");
            }
            set
            {
                SetSessionString<int>("UserID", value);
            }
        }

        public int practiceID
        {
            get
            {
                return GetSessionInt32("PracticeID");
            }
            set
            {
                SetSessionString<int>("PracticeID", value);
            }
        }
        public int practiceLocationID
        {
            get
            {
                return GetSessionInt32("PracticeLocationID");
            }
            set
            {
                SetSessionString<int>("PracticeLocationID", value);
            }
        }
        public string practiceName
        {
            get
            {
                return GetSessionString("PracticeName");
            }
            set
            {
                SetSessionString<string>("PracticeName", value);
            }
        }
        public string practiceLocationName
        {
            get
            {
                return GetSessionString("PracticeLocationName");
            }
            set
            {
                SetSessionString<string>("PracticeLocationName", value);
            }
        }

        public int recordsDisplayedPerPage
        {
            get
            {
                int recordsDisplayed = GetSessionInt32("RecordsDisplayed");
                if (recordsDisplayed < 1)
                {
                    recordsDisplayed = 10;
                }
                if (recordsDisplayed > 100)
                {
                    recordsDisplayed = 100;
                }
                return recordsDisplayed;
            }
            set
            {
                var v = value;
                if (v < 1) v = 10;
                if (v > 100) v = 100;
                SetSessionString<int>("RecordsDisplayed", v);
            }
        }

        public bool ScrollEntirePage
        {
            get
            {
                return GetSessionBoolean("ScrollEntirePage");
            }
            set
            {
                SetSessionString<bool>("ScrollEntirePage", value);
            }
        }

        public bool DispenseQueueSortDescending
        {
            get
            {
                return GetSessionBoolean("DispenseQueueSortDescending");
            }
            set
            {
                SetSessionString<bool>("DispenseQueueSortDescending", value);
            }
        }

        public bool AllowPracticeUserOrderWithoutApproval
        {
            get
            {
                return GetSessionBoolean("AllowPracticeUserOrderWithoutApproval");
            }
            set
            {
                SetSessionString<bool>("AllowPracticeUserOrderWithoutApproval", value);
            }
        }

        protected bool GetSessionBoolean(string variableName)
        {
            string value = GetSessionString(variableName);
            bool retVal = false;
            bool.TryParse(value, out retVal);
            return retVal;
        }

        protected Int32 GetSessionInt32(string variableName)
        {
            string value = GetSessionString(variableName);
            Int32 retVal = -1;
            Int32.TryParse(value, out retVal);
            return retVal;
        }

        protected string GetSessionString(string variableName)
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session[variableName] != null)
            {
                return HttpContext.Current.Session[variableName].ToString();
            }
            return "";
        }

        protected void SetSessionString<T>(string variableName, T value)
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
            {
                HttpContext.Current.Session[variableName] = value;
            }
        }


        #endregion

    }
}
