﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ClassLibrary.DAL;

namespace BregVision.Bases
{
    public class PageBase : System.Web.UI.Page
    {
        #region public properties

        public int userID
        {
            get
            {
                return GetSessionInt32("UserID");
            }
            set
            {
                SetSessionString<int>("UserID", value);
            }
        }

        public int practiceID
        {
            get
            {
                return GetSessionInt32("PracticeID");
            }
            set
            {
                SetSessionString<int>("PracticeID", value);
            }
        }
        public int practiceLocationID
        {
            get
            {
                return GetSessionInt32("PracticeLocationID");
            }
            set
            {
                SetSessionString<int>("PracticeLocationID", value);
            }
        }
        public string practiceName
        {
            get
            {
                return GetSessionString("PracticeName");
            }
            set
            {
                SetSessionString<string>("PracticeName", value);
            }
        }
        public string practiceLocationName
        {
            get
            {
                return GetSessionString("PracticeLocationName");
            }
            set
            {
                SetSessionString<string>("PracticeLocationName", value);
            }
        }
        public bool practiceIsCustomFitEnabled
        {
            get
            {
                return GetSessionBool("PracticeIsCustomFitEnabled");
            }
            set
            {
                SetSessionString<bool>("PracticeIsCustomFitEnabled", value);
            }
        }

        public int recordsDisplayedPerPage
        {
            get
            {
                int recordsDisplayed = GetSessionInt32("RecordsDisplayed");
                if (recordsDisplayed < 1)
                {
                    recordsDisplayed = 10;
                }
                if (recordsDisplayed > 100)
                {
                    recordsDisplayed = 100;
                }
                return recordsDisplayed;
            }
            set
            {
                var v = value;
                if (v < 1) v = 10;
                if (v > 100) v = 100;
                SetSessionString<int>("RecordsDisplayed", value);
            }
        }

        public bool recordsDisplayedPerPageFromTextBox(System.Web.UI.WebControls.TextBox txtRecordsPerPage)
        {
            if (txtRecordsPerPage != null)
            {
                if (!string.IsNullOrEmpty(txtRecordsPerPage.Text))
                {
                    int recordsPerPage = 0;
                    bool converted = Int32.TryParse(txtRecordsPerPage.Text, out recordsPerPage);
                    if (recordsPerPage > 0)
                    {
                        recordsDisplayedPerPage = recordsPerPage;
                        return true;
                    }
                }
            }
            return false;
        }

        #endregion

        #region constructor
        public PageBase()
            : base()
        {
            this.visionSessionProvider = new _VisionSessionProvider(this);
        }
        #endregion

        #region IVisionSessionProvider
        private class _VisionSessionProvider : BLL.IVisionSessionProvider
        {
            private PageBase _p;

            public _VisionSessionProvider(PageBase p)
            {
                _p = p;
            }

            public int GetCurrentPracticeID()
            {
                return _p.practiceID;
            }

            public int GetCurrentPracticeLocationID()
            {
                return _p.practiceLocationID;
            }
        }
        public BLL.IVisionSessionProvider visionSessionProvider = null;
        #endregion

        #region fax dispensement sending
        private bool? _is_fax_dispensement_enabled = null;
        public bool IsPracticeFaxDispensementEnabled
        {
            get
            {
                if (!_is_fax_dispensement_enabled.HasValue)
                {
                    using (var db = VisionDataContext.GetVisionDataContext())
                    {
                        var practice =
                            db.Practices
                            .Where(p => p.PracticeID == this.practiceID)
                            .Select(p => new { p.FaxDispenseReceiptEnabled })
                            .FirstOrDefault();
                        
                        _is_fax_dispensement_enabled =
                            (practice == null)
                            ? false
                            : practice.FaxDispenseReceiptEnabled;
                    }
                }
                return _is_fax_dispensement_enabled.Value;
            }
        }
        #endregion

        #region Protected Methods

        protected void GetSessionVariables(out int userID, out int practiceID, out string practiceName,
                            out int practiceLocationID, out string practiceLocationName)
        {
            GetSessionVariables(out userID, out practiceID, out practiceName);

            practiceLocationID = (Session["PracticeLocationID"] != null) ? Convert.ToInt32(Session["PracticeLocationID"].ToString()) : -1;
            practiceLocationName = (Session["PracticeLocationName"] != null) ? Session["PracticeLocationName"].ToString() : "";
        }

        protected void GetSessionVariables(out int userID, out int practiceID, out string practiceName)
        {
            userID = (Session["UserID"] != null) ? Convert.ToInt32(Session["UserID"].ToString()) : -1;
            practiceID = (Session["PracticeID"] != null) ? Convert.ToInt32(Session["PracticeID"].ToString()) : -1;
            practiceName = (Session["PracticeName"] != null) ? Session["PracticeName"].ToString() : "";
        }

        protected Int32 GetSessionInt32(string variableName)
        {
            string value = GetSessionString(variableName);
            Int32 retVal = -1;
            Int32.TryParse(value, out retVal);
            return retVal;
        }

        protected bool GetSessionBool(string variableName)
        {
            string value = GetSessionString(variableName);
            bool retVal = false;
            bool.TryParse(value, out retVal);
            return retVal;
        }

        protected string GetSessionString(string variableName)
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session[variableName] != null)
            {
                return HttpContext.Current.Session[variableName].ToString();
            }
            return "";
        }

        protected void SetSessionString<T>(string variableName, T value)
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
            {
                HttpContext.Current.Session[variableName] = value;
            }
        }
        #endregion

        #region UI Helper Methods
        protected void LabelPreRenderHideShow(object sender, EventArgs e)
        {
            // just a wrapper around the static method
            BregVision.UIHelper.LabelPreRenderHideShow(sender, e);
        }
        #endregion
    }
}
