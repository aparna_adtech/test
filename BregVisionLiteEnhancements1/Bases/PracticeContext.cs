﻿using System;
using System.Web;

namespace BregVision.Bases
{
    public class VisionPracticeContext
    {
        private static VisionPracticeContext _current;
        public static VisionPracticeContext Current
        {
            get
            {
                if(_current == null)
                {
                    _current = new VisionPracticeContext();
                }

                return _current;
            }
        }

        public int UserID 
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                    return (HttpContext.Current.Session["UserID"] != null) ? Convert.ToInt32(HttpContext.Current.Session["UserID"].ToString()) : -1;
                return -1;
            }
        }
        public int PracticeID 
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                    return (HttpContext.Current.Session["PracticeID"] != null) ? Convert.ToInt32(HttpContext.Current.Session["PracticeID"].ToString()) : -1;
                return -1;
            }
        }
        public int PracticeLocationID 
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                    return (HttpContext.Current.Session["PracticeLocationID"] != null) ? Convert.ToInt32(HttpContext.Current.Session["PracticeLocationID"].ToString()) : -1;
                return -1;
            }
        }
        public string PracticeName 
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                    return (HttpContext.Current.Session["PracticeName"] != null) ? HttpContext.Current.Session["PracticeName"].ToString() : "";
                return "";
            }        
        }
        public string PracticeLocationName 
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                    return (HttpContext.Current.Session["PracticeLocationName"] != null) ? HttpContext.Current.Session["PracticeLocationName"].ToString() : ""; 
                return "";
            } 
        }

    }
}
