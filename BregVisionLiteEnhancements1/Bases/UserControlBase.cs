﻿using System;
using System.Web;

namespace BregVision.Bases
{
    public class UserControlBase : System.Web.UI.UserControl
    {
        public UserControlBase()
        {
            _PracticeIDatPageLoad = this.practiceID;
            _PracticeLocationIDatPageLoad = this.practiceLocationID;
        }
        #region Private Variables

        int _PracticeIDatPageLoad = -1;
        int _PracticeLocationIDatPageLoad = -1;

        #endregion

        #region public properties

        public int userID
        {
            get
            {
                return GetSessionInt32("UserID");
            }
            set
            {
                SetSessionString<int>("UserID", value); 
            }
        }

        public int practiceID
        {
            get
            {
                return GetSessionInt32("PracticeID");
            }
            set
            {
                SetSessionString<int>("PracticeID", value);
            }
        }
        public int practiceLocationID
        {
            get
            {
                return GetSessionInt32("PracticeLocationID");
            }
            set
            {
                SetSessionString<int>("PracticeLocationID", value);
            }
        }
        public string practiceName
        {
            get
            {
                return GetSessionString("PracticeName");
            }
            set
            {
                SetSessionString<string>("PracticeName", value);
            }
        }
        public string practiceLocationName
        {
            get
            {
                return GetSessionString("PracticeLocationName");
            }
            set
            {
                SetSessionString<string>("PracticeLocationName", value);
            }
        }
        public bool practiceIsCustomFitEnabled
        {
            get
            {
                return GetSessionBool("PracticeIsCustomFitEnabled");
            }
            set
            {
                SetSessionString<bool>("PracticeIsCustomFitEnabled", value);
            }
        }

        public int recordsDisplayedPerPage
        {
            get
            {
                int recordsDisplayed = GetSessionInt32("RecordsDisplayed");
                if (recordsDisplayed < 1)
                {
                    recordsDisplayed = 10;
                }
                if (recordsDisplayed > 100)
                {
                    recordsDisplayed = 100;
                }
                return recordsDisplayed;
            }
            set
            {
                var v = value;
                if (v < 1) v = 10;
                if (v > 100) v = 100;
                SetSessionString<int>("RecordsDisplayed", value);
            }
        }

        public bool PracticeIDChanged
        {
            get
            {
                return _PracticeIDatPageLoad == this.practiceID ? false : true;
            }
        }

        public bool PracticeLocationIDChanged
        {
            get
            {
                if (_PracticeLocationIDatPageLoad > 0 && _PracticeLocationIDatPageLoad != this.practiceLocationID)
                    return true;
                else
                    return false;
            }
        }

#endregion

#region Protected Methods

        protected void GetSessionVariables(out int userID, out int practiceID, out string practiceName,
                            out int practiceLocationID, out string practiceLocationName)
        {
            GetSessionVariables(out userID, out practiceID, out practiceName);

            practiceLocationID = (Session["PracticeLocationID"] != null) ? Convert.ToInt32(Session["PracticeLocationID"].ToString()) : -1;
            practiceLocationName = (Session["PracticeLocationName"] != null) ? Session["PracticeLocationName"].ToString() : "";
        }

        protected void GetSessionVariables(out int userID, out int practiceID, out string practiceName)
        {
            userID = (Session["UserID"] != null) ? Convert.ToInt32(Session["UserID"].ToString()) : -1;
            practiceID = (Session["PracticeID"] != null) ? Convert.ToInt32(Session["PracticeID"].ToString()) : -1;
            practiceName = (Session["PracticeName"] != null) ? Session["PracticeName"].ToString() : "";
        }

        protected Int32 GetSessionInt32(string variableName)
        {
            string value = GetSessionString(variableName);
            Int32 retVal = -1;
            Int32.TryParse(value, out retVal);
            return retVal;
        }

        protected bool GetSessionBool(string variableName)
        {
            string value = GetSessionString(variableName);
            bool retVal = false;
            bool.TryParse(value, out retVal);
            return retVal;
        }

        protected string GetSessionString(string variableName)
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session[variableName] != null)
            {
                return HttpContext.Current.Session[variableName].ToString();
            }
            return "";
        }

        protected void SetSessionString<T>(string variableName, T value)
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
            {
                HttpContext.Current.Session[variableName] = value;
            }
        }

        

#endregion

        public string GetAbsPath(string imagePath)
        {
            return BLL.Website.GetAbsPath(imagePath);
        }

#region Page Events

        protected void Page_Init(object sender, EventArgs e)
        {

        }

#endregion


    }
}
