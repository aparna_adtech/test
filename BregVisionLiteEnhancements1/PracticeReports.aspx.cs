using System;
using System.Data;

public partial class PracticeReports : BregVision.Bases.PageBase
{



    protected void Page_Load(object sender, EventArgs e)
    {


        //((BregVision.MasterPages.SiteMaster)Master).SetLocationDropdownVisibility(true);


        //RadTab adminTab;
        //adminTab = ctlMainMenu.FindToolbarTab("Admin");
        //adminTab.NavigateUrl = ctlMainMenu.GetAdminPath();
        //adminTab.Selected = false;

        //((BregVision.MasterPages.PracticeAdmin)Page.Master).SelectCurrentTab("TabReports");
        ctlMainMenu.SelectCurrentTab("Reports");

        PopulateClinicAndLocation();

    }

    public string GetAbsPath(string imagePath)
    {
        return BLL.Website.GetAbsPath(imagePath);
    }

    //protected void cbxChangeLocation_SelectedIndexChanged(object o, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    //{

    //    lblLocation.Text = cbxChangeLocation.Text.ToString();
    //    practiceLocationID = Convert.ToInt32(cbxChangeLocation.SelectedValue);
    //    practiceLocationName = cbxChangeLocation.SelectedItem.Text;
    //}

    private void PopulateClinicAndLocation()
    {

        using (IDataReader reader = DAL.Practice.Practice.GetPracticeLocationInfo(Context.User.Identity.Name))
        {
            if (reader.Read())
            {

                //Check to see if session variables have been set.
                if (Session["PracticeName"] == null || Session["PracticeName"].ToString().Length == 0)
                {

                    practiceName = reader["PracticeName"].ToString();

                    practiceID = Convert.ToInt32(reader["PracticeID"].ToString());
                }
                // Clear location drop down
                //cbxChangeLocation.ClearSelection();

                do
                {
                    //Fill location drop down box
                    if (reader["AllowAccess"].ToString() == "True")
                    {
                        Telerik.Web.UI.RadComboBoxItem item = new Telerik.Web.UI.RadComboBoxItem();

                        item.Text = reader["Name"].ToString();
                        item.Value = reader["PracticeLocationID"].ToString();
                        item.Font.Size = System.Web.UI.WebControls.FontUnit.XXSmall;

                        //set the value in the drop down to selected if it is the primary location
                        //if (reader["isPrimaryLocation"].ToString() == "True" && Session["PracticeLocationID"] == null)
                        if ((reader["UserDefaultLocation"].ToString() == "True" && Session["PracticeLocationID"] == null) || (Convert.ToInt32(reader["PracticeLocationID"]) == Convert.ToInt32(Session["PracticeLocationID"])))
                        {
                            item.Selected = true;
                            lblLocation.Text = reader["Name"].ToString();
                            practiceLocationName = reader["Name"].ToString();
                            practiceLocationID = Convert.ToInt32(reader["PracticeLocationID"].ToString());
                        }

                        //cbxChangeLocation.Items.Add(item);
                    }
                }
                while (reader.Read());

                //cbxChangeLocation.SelectedItem.Font.Size = System.Web.UI.WebControls.FontUnit.XXSmall;

                lblClinic.Font.Bold = true;
                lblClinic.Text = practiceName;
                lblLocation.Font.Bold = true;
                lblLocation.Text = practiceLocationName;
            }

        }
    }
}