﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using API.Web.Models;

namespace API.Web
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EMRIntegration" in code, svc and config file together.
    public class EMRIntegration : IEMRIntegration
    {
        // TODO: add authentication to this method
        // TODO: figure out if HL7 dates/times are in UTC or specific to the time zone & update query
        public schedule_item[] GetPatients(int practiceId, int practiceLocationId, DateTime utcStartDate, DateTime? utcEndDate)
        {
            using (var db = new BregVisionEMREntities())
            {
                // convert our utcStartDate and utcEndDate to practice time zone
                // NOTE: data received from practice EMR is in local time zone

                Func<string, TimeZoneInfo> get_local_timezone =
                    (x) =>
                    {
                        try
                        {
                            return TimeZoneInfo.FindSystemTimeZoneById(x);
                        }
                        catch
                        {
                            return TimeZoneInfo.Local;
                        }
                    };

                var entries =
                    db.schedule_item
                    .Where(item => item.practice_id == practiceId && item.practice_location_id == practiceLocationId)
                    .AsEnumerable()
                    .Where(
                        item =>
                        {
                            var localTimeZoneInfo = get_local_timezone(item.scheduled_visit_datetime_timezoneid);
                            DateTime localStartDate = TimeZoneInfo.ConvertTimeFromUtc(utcStartDate, localTimeZoneInfo);
                            DateTime? localEndDate = localStartDate.Date.Add(new TimeSpan(23, 59, 59)); // force window to end at 23:59:59 for the date
                            return
                                (item.scheduled_visit_datetime.Date >= localStartDate.Date)
                                && (!localEndDate.HasValue || (localEndDate.HasValue && item.scheduled_visit_datetime.Date <= localEndDate.Value.Date));
                        }
                    );

                var ordered_entries = entries.OrderBy(x => x.scheduled_visit_datetime).ToArray();

                // apply custom string formatting
                {
                    var format_string_obj = db.patientcode_format_strings.Where(x => x.practice_id == practiceId).FirstOrDefault();
                    var expand_shortcodes =
                        (Func<schedule_item, string, string>)
                        (
                            (i, f) =>
                            {
                                var patient_id = i.patient_id;
                                var patient_name = i.patient_name;
                                var physician_name = i.physician_name;
                                var patient_dob = i.patient_dob;
                                var schedule_item_id = i.schedule_item_id;
                                var remote_schedule_item_id = i.remote_schedule_item_id;

                                var result = String.Copy(f);

                                return
                                    result
                                        .Replace("{%patient_name%}", patient_name)
                                        .Replace("{%physician_name%}", physician_name)
                                        .Replace("{%patient_id%}", patient_id)
                                        .Replace("{%patient_dob%}", (patient_dob.HasValue ? patient_dob.Value.ToShortDateString() : string.Empty))
                                        .Replace("{%schedule_item_id%}", schedule_item_id.ToString("N"))
                                        .Replace("{%remote_schedule_item_id%}", remote_schedule_item_id);
                            }
                        );
                    if (format_string_obj != null)
                    {
                        foreach (var entry in ordered_entries)
                            entry.patient_id = expand_shortcodes(entry, format_string_obj.format_string);
                    }
                }

                return ordered_entries;
            }
        }

        public void QueueOutboundFile(int practiceId, string fileName, byte[] fileContents, string vccScheduleId)
        {
            // generate unique id for file
            var unique_fileid = Guid.NewGuid();

            // store file bytes into filesystem
            var unique_filename = unique_fileid + ".dat";
            {
                // calculate full path of file storage
                var p = _GetOutboundFileStorageFullPath(unique_filename);

                // write file contents
                try
                {
                    var f =
                        System.IO.File.Open(
                            p,
                            System.IO.FileMode.CreateNew,
                            System.IO.FileAccess.Write,
                            System.IO.FileShare.None);
                    f.Write(fileContents, 0, fileContents.Length);
                    f.Close();
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Error saving file to datastore: " + p, ex);
                }
            }

            // add record to database
            try
            {
                using (var db = new BregVisionEMREntities())
                {
                    // look up config
                    var config =
                        db.outbound_file_config
                        .Where(x => x.practice_id == practiceId)
                        .FirstOrDefault();

                    // add mdm record
                    if (config != null && config.is_autoqueue_hl7_mdm)
                        _QueueOutboundHL7MDMRequest(practiceId, vccScheduleId, unique_fileid, db);

                    // add file record
                    {
                        var has_filename = string.IsNullOrEmpty(fileName) ? false : true;
                        var is_config_guid_override = (config != null && config.is_guid_filename_override);
                        var filename_suffix = (System.IO.Path.GetExtension(fileName) ?? string.Empty);
                        var adjusted_file_name =
                            ((has_filename == false) || (is_config_guid_override == true))
                            ? (unique_fileid.ToString("N") + filename_suffix)
                            : fileName;

                        db.outbound_file_item_queue.AddObject(
                            new outbound_file_item_queue
                            {
                                creation_date = DateTime.Now,
                                filename = adjusted_file_name,
                                filestore_path = unique_filename,
                                outbound_file_item_queue_id = unique_fileid,
                                practice_id = practiceId,
                                send_success_date = null,
                                remote_schedule_item_id = vccScheduleId
                            });
                    }

                    // submit database changes
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error saving queue entry to database.", ex);
            }
        }

        private static void _QueueOutboundHL7MDMRequest(int practiceId, string vccScheduleId, Guid unique_fileid, BregVisionEMREntities db)
        {
            db.outbound_hl7_item_queue.AddObject(
                new outbound_hl7_item_queue
                {
                    associated_outbound_file_item_queue_id = unique_fileid,
                    creation_date = DateTime.Now,
                    hcpcs_transform_enabled = false,
                    message_type = "MDM",
                    outbound_hl7_item_queue_id = Guid.NewGuid(),
                    practice_id = practiceId,
                    remote_schedule_item_id = vccScheduleId,
                    send_success_date = null
                });
        }

        private static string _GetOutboundFileStorageFullPath(string filename)
        {
            return _GetOutboundFileStoragePathRoot() + filename;
        }

        private static string _GetOutboundFileStoragePathRoot()
        {
            var p = System.Configuration.ConfigurationManager.AppSettings["OutboundQueueFileStoragePath"];
            if (p.Substring(p.Length - 1, 1) != "/")
                p += "/";
            return p;
        }

        public Models.OutboundQueueItem[] GetOutboundQueueItems()
        {
            try
            {
                using (var db = new BregVisionEMREntities())
                {
                    return
                        db.outbound_file_item_queue
                            .Where(x => x.send_success_date.HasValue == false)
                            .Select(
                                x =>
                                    new Models.OutboundQueueItem
                                    {
                                        CreationDate = x.creation_date,
                                        Filename = x.filename,
                                        ID = x.outbound_file_item_queue_id,
                                        PracticeID = x.practice_id
                                    })
                            .ToArray();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error executing query for queued items.", ex);
            }
        }

        public byte[] GetOutboundFileContentsByOutboundQueueID(Guid outboundQueueId)
        {
            using (var db = new BregVisionEMREntities())
            {
                // retrieve record
                var f =
                    db.outbound_file_item_queue
                        .Where(x => x.outbound_file_item_queue_id == outboundQueueId)
                        .Single();

                // calculate full path to file data
                var p = _GetOutboundFileStorageFullPath(f.filestore_path);

                // read and return file data
                try
                {
                    var data = System.IO.File.ReadAllBytes(p);
                    return data;
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Error reading datastore for file: " + p, ex);
                }
            }
        }

        public void SetOutboundQueueItemProcessed(Guid outboundQueueId)
        {
            using (var db = new BregVisionEMREntities())
            {
                // look for matching record
                var f =
                    db.outbound_file_item_queue
                        .Where(x => x.outbound_file_item_queue_id == outboundQueueId)
                        .Single();

                // set processed
                f.send_success_date = DateTime.Now;

                // save record
                db.SaveChanges();
            }
        }

        public string GetQueueProcessorStoragePathForPracticeId(int practiceId)
        {
            using (var db = new BregVisionEMREntities())
            {
                return
                    db.outbound_file_config
                        .Where(x => x.practice_id == practiceId)
                        .First()
                        .file_dropoff_path;
            }
        }

        public void AddQueueProcessorErrorMessageForQueueItem(Guid outboundQueueId, string message)
        {
            try
            {
                using (var db = new BregVisionEMREntities())
                {
                    db.outbound_file_item_queue_log.AddObject(
                        new outbound_file_item_queue_log
                        {
                            log_datetime = DateTime.Now,
                            log_message = message,
                            outbound_file_item_queue_id = outboundQueueId,
                            outbound_file_item_queue_log_id = Guid.NewGuid()
                        });
                    db.SaveChanges();
                }
            }
            catch { /* ignore errors when reporting/logging errors.. should log to event log or something here */ }
        }

        public void QueueOutboundHL7Request(int practiceID, string scheduleIdentifier, OutboundHL7DispenseItem[] dispenseItems, DiagnosisCode[] diagnosisCodes, int dispenseId)
        {
            var message_type = "DFT";

            using (var db = new BregVisionEMREntities())
            {
                // do not add a new record if it's a duplicate
                if (db.outbound_hl7_item_queue.Any(x => (x.practice_id == practiceID) && (x.remote_schedule_item_id == scheduleIdentifier) && (x.message_type.ToUpper() == message_type.ToUpper())))
                    return;

                // generate new unique identifier for the queue entry
                var new_queue_id = Guid.NewGuid();

                // get message type or default it to DFT
                var is_hcpcs_transform_enabled = false; // TODO: implement this feature at some point...
                {
                    var config_data =
                        db.outbound_hl7_config
                        .Where(x => x.practice_id == practiceID)
                        .FirstOrDefault();
                    if (config_data != null)
                    {
                        message_type = config_data.hl7_message_type ?? "DFT";
                    }
                }

                // add queue entry
                db.AddTooutbound_hl7_item_queue(
                    new outbound_hl7_item_queue
                    {
                        practice_id = practiceID,
                        creation_date = DateTime.Now,
                        hcpcs_transform_enabled = is_hcpcs_transform_enabled,
                        message_type = message_type,
                        outbound_hl7_item_queue_id = new_queue_id,
                        remote_schedule_item_id = scheduleIdentifier,
                        send_success_date = null,
                        source_item_id = dispenseId
                    });

                // add queue items
                foreach (var item in dispenseItems)
                {
                    db.AddTooutbound_hl7_dispensement_item(
                        new outbound_hl7_dispensement_item
                        {
                            creation_date = DateTime.Now,
                            dispense_date = item.dispense_date,
                            dispense_quantity = item.quantity,
                            hcpcs_code = item.hcpcs_code,
                            item_code = item.item_code,
                            modifier1 = item.modifier1,
                            modifier2 = item.modifier2,
                            modifier3 = item.modifier3,
                            modifier4 = item.modifier4,
                            outbound_hl7_dispensement_item_id = Guid.NewGuid(),
                            outbound_hl7_item_queue_id = new_queue_id,
                            patient_signature_date = item.patient_signature_date,
                            physician_signature_date = item.physician_signature_date,
                            unit_price = item.unit_price,
                            abn_reason = item.abn_reason,
                            dme_deposit = item.dme_deposit,
                            is_abn = item.is_abn,
                            is_medicare = item.is_medicare,
                            item_name = item.item_name,
                            item_supplier_name = item.supplier_name,
                        });
                }

                // add diagnosis codes
                foreach (var item in diagnosisCodes)
                {
                    // skip over if item's diag code is not valid/empty
                    if (string.IsNullOrEmpty(item.Code))
                        continue;
                    
                    db.AddTooutbound_hl7_diagnosis(
                        new outbound_hl7_diagnosis
                        {
                            creation_date = DateTime.Now,
                            coding_method = item.Type.ToString(),
                            diagnosis_code = item.Code,
                            outbound_hl7_diagnosis_id = Guid.NewGuid(),
                            outbound_hl7_item_queue_id = new_queue_id
                        });
                }

                // save changes
                db.SaveChanges();
            }
        }
    }
}
