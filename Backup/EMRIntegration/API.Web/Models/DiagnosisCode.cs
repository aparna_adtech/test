﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace API.Web.Models
{
    [DataContract(Name = "DiagnosisCodeType")]
    public enum DiagnosisCodeType
    {
        [EnumMember(Value = "ICD9")]
        ICD9,
        [EnumMember(Value = "ICD10")]
        ICD10
    }

    [DataContract]
    public class DiagnosisCode
    {
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public DiagnosisCodeType Type { get; set; }
    }
}