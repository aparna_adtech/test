﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.Logger
{
    public class FileLogger : Logger
    {
        string ApplicationFilePath = ConfigurationManager.AppSettings["ApplicationFileLog"];

        public StreamWriter Applicationpointer;

        public FileLogger()
        {
            
        }

        public void WriteLog(string Message, Exception exception, String source,Guid CorelationID)
        {
            using (Applicationpointer = new StreamWriter(ApplicationFilePath, true))
            {
                if (exception != null)
                    this.Applicationpointer.WriteLine($"{CorelationID}: <{source}> {exception}");
                else
                    this.Applicationpointer.WriteLine($"{CorelationID}: <{source}> {Message}");

                this.Applicationpointer.Flush();
            }
        }

   
        public void NonAsyncTraceLogger(string Message, Exception exception, String source)
        {
            this.Applicationpointer.WriteLine(Message);
            this.Applicationpointer.Flush();
        }
    }
}
