﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace com.breg.vision.Logger
{
    public class SQLLogger : Logger 
    {
        string FilePath = ConfigurationManager.AppSettings["FilePath"];

        public StreamWriter SQLpointer;

        public SQLLogger()
        {
            SQLpointer = new StreamWriter(FilePath, true);
        }

        public void WriteLog(string Message, Exception exception, String source,Guid CorelationID)
        {
            Tasks.Add(CreateTaskForLog(Message, exception, source, CorelationID));
        }

        private async Task<bool> CreateTaskForLog(string Message,Exception Exception, String source,Guid CorelationID)
        {
            try
            {
                return await Task.Run(() =>
                {
                    this.SQLpointer.WriteLine(Message);
                    this.SQLpointer.Flush();
                    return true;
                });
            }
            catch (Exception ex)
            {
                // to do fallback Logger for Exceptions
                return false;
            }
        }

        public void NonAsyncTraceLogger(string Message, Exception exception, String source)
        {
            this.SQLpointer.WriteLine(Message);
            this.SQLpointer.Flush();
        }
    }
}
