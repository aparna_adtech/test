﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.Logger.iLogger
{
    public interface ILogger
    {
        void WriteLog(string Message, Exception ex, String source,Guid CorelationID);
    }
}
