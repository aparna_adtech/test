﻿using com.breg.vision.Logger.iLogger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.Logger
{
    public abstract class Logger : ILogger
    {
        public List<Task<bool>> Tasks;

        public Logger()
        {
            Tasks = new List<Task<bool>>();
        }

        public void AwaitAllTasks(List<Task<bool>> Tasks)
        {
            try
            {
                Task.WaitAll(Tasks.ToArray());
            }
            catch (Exception ex)
            {
                // to do fallback Logger for Exceptions
            }
        }

        public void WriteLog(string Message, Exception ex, string source,Guid CorelationID)
        {
            throw new NotImplementedException();
        }
    }
}
