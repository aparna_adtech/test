﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Forums.ForumsService;
using System.Windows.Data;
using Telerik.Windows.Controls;

namespace Forums
{
    public partial class UserView : UserControl
    {
        UserViewModel _UserViewModel;

        public event RoutedEventHandler ForumArticleSelected; //local the this view for loading the listbox. processing happens in the viewmodel
        public event RoutedEventHandler ForumArticlesLoaded; //raise event to forumView to show the users articles in the general article ListView

        public UserView(UserViewModel userViewModel)
        {
            _UserViewModel = userViewModel;
            _UserViewModel.ForumArticlesLoaded += new RoutedEventHandler(_UserViewModel_ForumArticleLoaded);
            _UserViewModel.ForumArticleSelected += new RoutedEventHandler(_UserViewModel_ForumArticleSelected);
            _UserViewModel.GetPracticeInfoCompleted += new RoutedEventHandler(_UserViewModel_GetPracticeInfoCompleted);

            this.DataContext = _UserViewModel;
            InitializeComponent();

            this.Loaded += new RoutedEventHandler(UserView_Loaded);

            _UserViewModel.GetForumArticlesByUserId(_UserViewModel.UserPublicInfo.UserId,1,true); //load the artices for this selected user
            _UserViewModel.GetUserPublicInfo(_UserViewModel.UserPublicInfo.UserId);
            _UserViewModel.GetPracticePublicInfo(_UserViewModel.UserPublicInfo.UserId);

            if (App.Current.Resources["LoggedInUser"] != null && App.Current.Resources["LoggedInUser"].ToString() != "")
            {
                int userId = int.Parse((string)App.Current.Resources["LoggedInUser"]);
                if (userId != _UserViewModel.UserPublicInfo.UserId)
                {
                    btnEditPublicInfo.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
            
        }

        void _UserViewModel_GetPracticeInfoCompleted(object sender, RoutedEventArgs e)
        {
            //Thins happens when the user is a BregAdmin whith no Practice associated with the account.
            //Don't show the practice fields as they will be null anyways.
            if (((PracticePublicInfo)sender) == null)
            {
                spPracticeInfo.Visibility = Visibility.Collapsed;
            }
        }
               
        void UserView_Loaded(object sender, RoutedEventArgs e)
        {
            //happens when a user navigates to the user tab when it has been previously loaded.
            if (_UserViewModel.ForumArticles != null)
            {
                if (ForumArticlesLoaded != null)
                    ForumArticlesLoaded(_UserViewModel.ForumArticles, e);
            }
        }

        void _UserViewModel_ForumArticleSelected(object sender, RoutedEventArgs e)
        {
            if (ForumArticleSelected != null)
                ForumArticleSelected(sender, e);
        }

        void _UserViewModel_ForumArticleLoaded(object sender, RoutedEventArgs e)
        {
            
            if (ForumArticlesLoaded != null)
                ForumArticlesLoaded(sender, e);

            //load the list box with articles
            //lstBox.ItemsSource = _UserViewModel.ForumArticles;
        }

        private void lstBox_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (_UserViewModel.ForumArticles != null)
            {
                lstBox.ItemsSource = null;
                lstBox.ItemsSource = _UserViewModel.ForumArticles;
            }
        }

        private void lstBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            _UserViewModel.ForumArticle = null;

            ForumArticle forumArticle = lstBox.SelectedItem as ForumArticle;

            if (forumArticle != null)
            {
                _UserViewModel.GetForumArticleTreeByTopArticle(forumArticle.Id, 1000,true);
            }

        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            Grid g = sender as Grid;

            Binding b = new Binding("ActualWidth");
            b.Source = lstBox;
            b.Mode = BindingMode.OneWay;

            g.SetBinding(Grid.WidthProperty, b);
        }

       
        private void editPublicInfo_Click(object sender, RoutedEventArgs e)
        {
            //If this is this user allow edit
            if (App.Current.Resources["LoggedInUser"] != null && App.Current.Resources["LoggedInUser"].ToString() != "")
            {
                int userId = int.Parse((string)App.Current.Resources["LoggedInUser"]);
                if (userId == _UserViewModel.UserPublicInfo.UserId)
                {
                    RadWindow radWindow = new RadWindow();
                    UserInfoViewModel userInfoViewModel = new UserInfoViewModel();

                    userInfoViewModel.PracticePublicInfo = _UserViewModel.PracticePublicInfo;
                    userInfoViewModel.UserPublicInfo = _UserViewModel.UserPublicInfo;

                    radWindow.Content = new UserInfoView(userInfoViewModel);
                    radWindow.Width = 550;
                    radWindow.Height = 445;
                    radWindow.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                    radWindow.Header = "Edit Public Profile";

                    radWindow.Closed += (se, ea) =>
                    {
                        if (radWindow.DialogResult == true)
                        {
                            //apply changes to DB here
                            ForumsServiceClient client = new ForumsServiceClient();
                            client.UpdateUserPublicInfoAsync(userInfoViewModel.UserPublicInfo);
                            client.UpdateUserPublicInfoCompleted += (se1, ea1) =>
                            {
                                if (ea1.Error != null)
                                {
                                    //raise error to UI
                                }
                                else
                                {
                                    if (ea1.Result.Exception != null)
                                    {
                                        //raise error to UI
                                    }
                                    else
                                    {
                                        _UserViewModel.UserPublicInfo = userInfoViewModel.UserPublicInfo;
                                    }
                                }
                            };
                            //BregAdmin account do not have practices associated with them
                            if (userInfoViewModel.PracticePublicInfo != null)
                            {
                                client.UpdatePracticePublicInfoAsync(userInfoViewModel.PracticePublicInfo);
                                client.UpdatePracticePublicInfoCompleted += (se2, ea2) =>
                                {
                                    if (ea2.Error != null)
                                    {
                                        //raise error to UI
                                    }
                                    else
                                    {
                                        if (ea2.Result.Exception != null)
                                        {
                                            //raise error to UI
                                        }
                                        else
                                        {
                                            _UserViewModel.PracticePublicInfo = userInfoViewModel.PracticePublicInfo;
                                        }
                                    }
                                };
                            }
                        }
                    };

                    radWindow.ShowDialog();
                }
            }
        }
    }
}
