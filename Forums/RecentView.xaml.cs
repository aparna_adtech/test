﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using Forums.ForumsService;

namespace Forums
{
    public partial class RecentView : UserControl
    {
        RecentViewModel _RecentViewModel;

        public RecentView(RecentViewModel recentViewModel)
        {
            _RecentViewModel = recentViewModel;
            _RecentViewModel.SearchCompleted += new RoutedEventHandler(_RecentViewModel_SearchCompleted);
            this.DataContext = _RecentViewModel;
            InitializeComponent();

            this.Loaded += new RoutedEventHandler(RecentView_Loaded);
        }

        void _RecentViewModel_SearchCompleted(object sender, RoutedEventArgs e)
        {
            waitIndicator.IsActive = false;
        }

        void RecentView_Loaded(object sender, RoutedEventArgs e)
        {
            waitIndicator.IsActive = true;
            _RecentViewModel.GetRecentArticles(50, 1,true);
        }

        private void lstBox_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (_RecentViewModel.ForumArticles != null)
            {
                lstBox.ItemsSource = null;
                lstBox.ItemsSource = _RecentViewModel.ForumArticles;
            }
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            Grid g = sender as Grid;

            Binding b = new Binding("ActualWidth");
            b.Source = lstBox;
            b.Mode = BindingMode.OneWay;

            g.SetBinding(Grid.WidthProperty, b);
        }

        private void lst_User_Click(object sender, MouseButtonEventArgs e)
        {
            ForumArticle forumArticle = null; ;
            if (sender.GetType().Equals(typeof(TextBlock)))
            {
                TextBlock tb = (TextBlock)sender;
                forumArticle = (ForumArticle)tb.DataContext;
            }
            else if (sender.GetType().Equals(typeof(Image)))
            {
                Image img = (Image)sender;
                forumArticle = (ForumArticle)img.DataContext;
            }

            if (forumArticle != null)
            {
                System.Windows.Browser.HtmlPage.Window.Navigate(new Uri("forum.aspx?UserId=" + forumArticle.UserId.ToString() + "&UserName=" + forumArticle.PublicUserName.ToString(), UriKind.Relative), "blank"); 
            }
        }

        private void lst_Subject_Click(object sender, MouseButtonEventArgs e)
        {
            TextBlock tb = (TextBlock)sender;
            ForumArticle forumArticle = (ForumArticle)tb.DataContext;

            if (forumArticle != null)
            {
                System.Windows.Browser.HtmlPage.Window.Navigate(new Uri("forum.aspx?ArticleId=" + forumArticle.Id.ToString(), UriKind.Relative), "blank"); 
            }
        }

        private void toolbarButton_Click(object sender, RoutedEventArgs e)
        {
            switch ((string)((Button)sender).Tag)
            {
                case "Home":
                    waitIndicator.IsActive = true;
                    _RecentViewModel.GetRecentArticles(50, 1,true);  
                    break;
                case "Search":
                    DoSearch(true);
                    break;
                default:
                    break;
            }
        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                DoSearch(true);
        }

        private void DoSearch(bool isActive)
        {
            waitIndicator.IsActive = true;
            _RecentViewModel.SearchArticles(txtSearch.Text, 250, 1, isActive);  
        }

    }
}
