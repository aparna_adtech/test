﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Forums.ForumsService;

namespace Forums
{
    public class ArticleViewModel : EntityBase
    {
        public event RoutedEventHandler ForumArticleLoaded;

        ForumArticle _ForumArticle;

        public ForumArticle ForumArticle
        {
            get { return _ForumArticle; }
            set { _ForumArticle = value; OnPropertyChanged("ForumArticle"); }
        }

        public ArticleViewModel(ForumArticle forumArticle)
        {
            _ForumArticle = forumArticle;
        }

        public void GetForumArticleTreeByTopArticle(int id, int depth, bool isActive)
        {
            ForumArticle = null;

            ForumsServiceClient client = new ForumsServiceClient();

            client.Endpoint.Address = EndPointHelper.GetServiceURI("Forums/ForumsService.svc");

            client.GetForumArticleTreeByTopArticleAsync(id, depth, isActive);
            client.GetForumArticleTreeByTopArticleCompleted += (se, ea) =>
            {
                if (ea.Error != null)
                {
                    //raise error to UI
                }
                else
                {
                    if (ea.Result.Exception != null)
                    {
                        //raise error to UI
                    }
                    else
                    {
                        ForumArticle = ea.Result.ForumArticle;

                        if (ForumArticleLoaded != null)
                            ForumArticleLoaded(ea.Result.ForumArticle, null);
                    }
                }
            };
        }
    }
}
