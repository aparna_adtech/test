﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text;

namespace Forums
{
    public class EndPointHelper
    {
        public static System.ServiceModel.EndpointAddress GetServiceURI(string serviceAddress)
        {
            System.ServiceModel.EndpointAddress newAddress;
            Uri baseUriString = GetBaseUri();
            string newUriString = baseUriString + serviceAddress;
            Uri newUri = new Uri(newUriString);
            newAddress = new System.ServiceModel.EndpointAddress(newUri);
            return newAddress;
        }

        private static Uri GetBaseUri()
        {

            Uri currentUri = new Uri(Application.Current.Host.Source.AbsoluteUri);
            string baseUrl = getBaseUrl(currentUri);

            return new Uri(baseUrl);
        }
        private static string getBaseUrl(Uri currentUri)
        {
            StringBuilder url = new StringBuilder();
            url.Append(currentUri.Scheme);
            url.Append("://");
            url.Append(currentUri.Host);
            if (currentUri.Port != 80)
            {
                url.Append(":");
                url.Append(currentUri.Port);
            }
            if (currentUri.Host.ToLower() == "localhost")
            {
                url.Append(getFirstSegment(currentUri.AbsolutePath));
            }
            url.Append("/");
            return url.ToString();
        }
        //when we develop locally, we use a virtual directory, which always has vision in it's name. ??? I hope...
        private static string getFirstSegment(string absolutePath)
        {
            string[] segments = absolutePath.Split("/".ToCharArray());
            foreach (string segment in segments)
            {
                if (segment != "")
                {
                    if (segment.ToLower().Contains("vision"))
                    {
                        return string.Format("/{0}", segment);
                    }
                }
            }
            return "";
        }
    }
}
