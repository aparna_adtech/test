﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Forums.ForumsService;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Forums
{
    public class ForumViewModel : EntityBase
    {
        public event RoutedEventHandler ForumGroupsLoaded;
        public event RoutedEventHandler ForumGroupsArticlesLoaded;
        public event RoutedEventHandler ForumArticleLoaded;
        public event RoutedEventHandler DeleteForumArticleCompleted;

        string _ArticlesHeader = "Articles";

        public string ArticlesHeader
        {
            get { return _ArticlesHeader; }
            set { _ArticlesHeader = value; OnPropertyChanged("ArticlesHeader"); }
        }

        ForumArticle _ForumArticle;

        public ForumArticle ForumArticle
        {
            get { return _ForumArticle; }
            set { _ForumArticle = value; OnPropertyChanged("ForumArticle"); }
        }

        ObservableCollection<ForumGroup> _ForumGroups;

        public ObservableCollection<ForumGroup> ForumGroups
        {
            get { return _ForumGroups; }
            set { _ForumGroups = value; OnPropertyChanged("ForumGroups"); }
        }


        ForumGroup _SelectedForumGroup;

        public ForumGroup SelectedForumGroup
        {
            get { return _SelectedForumGroup; }
            set { _SelectedForumGroup = value; OnPropertyChanged("SelectedForumGroup"); }
        }

        ObservableCollection<ForumArticle> _ForumArticles;

        public ObservableCollection<ForumArticle> ForumArticles
        {
            get { return _ForumArticles; }
            set { _ForumArticles = value; OnPropertyChanged("ForumArticles"); }
        }

        public void DeleteForumArticle(int articleId)
        {
            ForumsServiceClient client = new ForumsServiceClient();
            client.DeleteForumArticleAsync(articleId);
            client.DeleteForumArticleCompleted += (se, ea) =>
            {
                if (ea.Error != null)
                {
                    //raise error to UI
                    if (DeleteForumArticleCompleted != null)
                        DeleteForumArticleCompleted(false, null);
                }
                else
                {
                    if (DeleteForumArticleCompleted != null)
                        DeleteForumArticleCompleted(ea.Result, null);
                }
            };

        }

        public void SetForumArticleIsActive(int articleId, bool isActive)
        {
            ForumsServiceClient client = new ForumsServiceClient();
            client.SetForumArticleIsActiveAsync(articleId, isActive);
            client.SetForumArticleIsActiveCompleted += (se, ea) =>
            {
                if (ea.Error != null)
                {
                    //raise error to UI
                    //if (DeleteForumArticleCompleted != null)
                    //    DeleteForumArticleCompleted(false, null);
                }
                else
                {
                    //if (DeleteForumArticleCompleted != null)
                    //    DeleteForumArticleCompleted(ea.Result, null);
                }
            };

        }

        public void GetForumGroups(bool includeArticles, int depth)
        {
            ForumsServiceClient client = new ForumsServiceClient();
            client.GetForumGroupsAsync(includeArticles, depth);
            client.GetForumGroupsCompleted += (se, ea) =>
                {
                    if(ea.Error != null)
                    {
                        //raise error to UI
                    }
                    else
                    {
                        if (ea.Result.Exception != null)
                        {
                            //raise error to UI
                        }
                        else
                        {
                            ForumGroups = ea.Result.ForumGroups;

                            if (ForumGroupsLoaded != null)
                                ForumGroupsLoaded(null, null);

                            ///testing the function
                            //foreach (ForumGroup fg in ForumGroups)
                            //{
                            //    GetForumArticlesByForumGroup(fg, 1000);
                            //}
                        }
                    }
                };

            //testing the function
            //GetRecientArticles(100, 100);

        }

        public void GetForumArticlesByForumGroup(ForumGroup forumGroup, int depth, bool isActive)
        {
            ForumsServiceClient client = new ForumsServiceClient();
            client.GetForumArticlesByForumGroupAsync(forumGroup.Id,depth, isActive);
            client.GetForumArticlesByForumGroupCompleted += (se, ea) =>
            {
                if (ea.Error != null)
                {
                    //raise error to UI
                }
                else
                {
                    if (ea.Result.Exception != null)
                    {
                        //raise error to UI
                    }
                    else
                    {
                        forumGroup.ForumArticles = ea.Result.ForumArticles;

                        if (ForumGroupsArticlesLoaded != null)
                            ForumGroupsArticlesLoaded(null, null);
                    }
                }
            };
        }

        public void GetRecentArticlesByGroupId(int groupId, int count, int depth, bool isActive)
        {
            ForumArticles = null;

            ForumsServiceClient client = new ForumsServiceClient();

            client.Endpoint.Address = EndPointHelper.GetServiceURI("Forums/ForumsService.svc");

            client.GetRecentArticlesByGroupIdAsync(groupId, count, depth, isActive);
            client.GetRecentArticlesByGroupIdCompleted += (se, ea) =>
            {
                if (ea.Error != null)
                {
                    //raise error to UI
                }
                else
                {
                    if (ea.Result.Exception != null)
                    {
                        //raise error to UI
                    }
                    else
                    {
                        ForumArticles = ea.Result.ForumArticles;

                        if (ForumGroupsArticlesLoaded != null)
                            ForumGroupsArticlesLoaded(null, null);
                    }
                }
            };
        }

        public void GetForumArticleTreeByTopArticle(int id, int depth, bool isActiveOnly)
        {
            ForumArticles = null;

            ForumsServiceClient client = new ForumsServiceClient();
            client.GetForumArticleTreeByTopArticleAsync(id, depth, isActiveOnly);
            client.GetForumArticleTreeByTopArticleCompleted += (se, ea) =>
            {
                if (ea.Error != null)
                {
                    //raise error to UI
                }
                else
                {
                    if (ea.Result.Exception != null)
                    {
                        //raise error to UI
                    }
                    else
                    {
                        //we conditionally set this in the view

                        if (ForumArticleLoaded != null)
                            ForumArticleLoaded(ea.Result.ForumArticle, null);
                    }
                }
            };
        }

        public void GetForumArticlesBySearchText(string searchText, int depth, bool isActive)
        {
            ForumArticles = null;

            ForumsServiceClient client = new ForumsServiceClient();
            client.GetForumArticlesBySearchTextAsync(searchText, depth, isActive);
            client.GetForumArticlesBySearchTextCompleted += (se, ea) =>
            {
                if (ea.Error != null)
                {
                    //raise error to UI
                }
                else
                {
                    if (ea.Result.Exception != null)
                    {
                        //raise error to UI
                    }
                    else
                    {
                        this.ForumArticles = ea.Result.ForumArticles;

                        //TODO may need a search result event here
                        if (ForumGroupsArticlesLoaded != null)
                            ForumGroupsArticlesLoaded(null, null);
                    }
                }
            };
        }

        public void GetRecentArticles(int count, int depth, bool isActive)
        {
            ForumsServiceClient client = new ForumsServiceClient();
            client.GetRecentArticlesAsync(count, depth, isActive);
            client.GetRecentArticlesCompleted += (se, ea) =>
            {
                if (ea.Error != null)
                {
                    //raise error to UI
                }
                else
                {
                    if (ea.Result.Exception != null)
                    {
                        //raise error to UI
                    }
                    else
                    {
                        ForumArticles = ea.Result.ForumArticles;
                    }
                }

                if (ForumGroupsArticlesLoaded != null)
                    ForumGroupsArticlesLoaded(null, null);
            };
        }
    }
}
