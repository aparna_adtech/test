﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Forums.ForumsService;

namespace Forums
{
    public class AddForumArticleViewModel : EntityBase
    {
        ForumArticle _ParentForumArticle;
        public event RoutedEventHandler InsertForumArticleCompleted;
        

        public ForumArticle ParentForumArticle
        {
            get { return _ParentForumArticle; }
            set { _ParentForumArticle = value; OnPropertyChanged("ParentForumArticle"); }
        }

        ForumArticle _ForumArticle;

        public ForumArticle ForumArticle
        {
            get { return _ForumArticle; }
            set { _ForumArticle = value; OnPropertyChanged("ForumArticle"); }
        }


        ForumGroup _ForumGroup;

        public ForumGroup ForumGroup
        {
            get { return _ForumGroup; }
            set { _ForumGroup = value; OnPropertyChanged("ForumGroup"); }
        }

        public AddForumArticleViewModel(ForumArticle parentForumArticle, ForumArticle forumArticle)
        {
            ParentForumArticle = parentForumArticle;
            ForumArticle = forumArticle;
        }

        public AddForumArticleViewModel(ForumGroup forumGroup, ForumArticle forumArticle)
        {
            ForumGroup = forumGroup;
            ForumArticle = forumArticle;
        }

        public void InsertForumArticle()
        {

            ForumsServiceClient client = new ForumsServiceClient();

            client.Endpoint.Address = EndPointHelper.GetServiceURI("Forums/ForumsService.svc"); 

            client.InsertForumArticleAsync(_ForumArticle);
            client.InsertForumArticleCompleted += (se, ea) =>
            {
                if (ea.Error != null)
                {
                    //raise error to UI
                }
                else
                {
                    if (ea.Result.Exception != null)
                    {
                        //raise error to UI
                    }
                    else
                    {
                        ForumArticle = ea.Result.ForumArticle;

                        if (InsertForumArticleCompleted != null)
                            InsertForumArticleCompleted(null, null);
                    }
                }
            };
        }

    }
}
