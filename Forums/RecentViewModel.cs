﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Forums.ForumsService;

namespace Forums
{
    
    public class RecentViewModel : EntityBase
    {
        public event RoutedEventHandler SearchCompleted;

        ObservableCollection<ForumArticle> _ForumArticles;

        public ObservableCollection<ForumArticle> ForumArticles
        {
            get { return _ForumArticles; }
            set { _ForumArticles = value; OnPropertyChanged("ForumArticles"); }
        }

        public void GetRecentArticles(int count, int depth, bool isActive)
        {
            ForumsServiceClient client = new ForumsServiceClient();
            client.GetRecentArticlesAsync(count, depth, isActive);
            client.GetRecentArticlesCompleted += (se, ea) =>
            {
                if (ea.Error != null)
                {
                    //raise error to UI
                }
                else
                {
                    if (ea.Result.Exception != null)
                    {
                        //raise error to UI
                    }
                    else
                    {
                        ForumArticles = ea.Result.ForumArticles;
                    }
                }

                if(SearchCompleted!=null)
                    SearchCompleted(null,null);
            };
        }

        public void SearchArticles(string searchText, int count, int depth, bool isActive)
        {
            ForumsServiceClient client = new ForumsServiceClient();
            client.SearchArticlesAsync(searchText, count, depth, isActive);
            client.SearchArticlesCompleted += (se, ea) =>
            {
                if (ea.Error != null)
                {
                    //raise error to UI
                }
                else
                {
                    if (ea.Result.Exception != null)
                    {
                        //raise error to UI
                    }
                    else
                    {
                        ForumArticles = ea.Result.ForumArticles;
                    }
                }

                if (SearchCompleted != null)
                    SearchCompleted(null, null);
            };
        }
    }
}
