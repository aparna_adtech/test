﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace Forums
{
    public partial class AddForumArticleView : UserControl
    {
        
        AddForumArticleViewModel _AddForumArticleViewModel;

        public AddForumArticleView(AddForumArticleViewModel addForumArticleViewModel)
        {
            _AddForumArticleViewModel = addForumArticleViewModel;
            _AddForumArticleViewModel.InsertForumArticleCompleted += new RoutedEventHandler(_AddForumArticleViewModel_InsertForumArticleCompleted);
            this.DataContext = _AddForumArticleViewModel;
            InitializeComponent();
            
            if (_AddForumArticleViewModel.ParentForumArticle != null)
            {
                txtSubject.Text = "RE: " + _AddForumArticleViewModel.ParentForumArticle.Subject;
            }

            
        }
        
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            RadWindow parent = (RadWindow)this.Parent;
            parent.DialogResult = false;
            parent.Close();
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            if (txtSubject.Text == null || txtSubject.Text.Length == 0)
            {
                labSubject.Foreground = new SolidColorBrush(Colors.Red);
                return;
            }
            else
            {
                labSubject.Foreground = new SolidColorBrush(Colors.Black);
            }

            if (txtArticleText.Text == null || txtArticleText.Text.Length == 0)
            {
                labBody.Foreground = new SolidColorBrush(Colors.Red);
                return;
            }
            else
            {
                labBody.Foreground = new SolidColorBrush(Colors.Black);
            }
            
            int userId = int.Parse((string)App.Current.Resources["LoggedInUser"]);
            _AddForumArticleViewModel.ForumArticle.UserId = userId; 

            _AddForumArticleViewModel.ForumArticle.IsActive = false; //this is the default setting to prevent articles from being instantly published

            _AddForumArticleViewModel.ForumArticle.Body = txtArticleText.Text.Replace("\r", "<br/>");
            _AddForumArticleViewModel.ForumArticle.Subject = txtSubject.Text;

            if (_AddForumArticleViewModel.ParentForumArticle != null)
            {
                //this is a reply to an existing article
                _AddForumArticleViewModel.ForumArticle.ParentId = _AddForumArticleViewModel.ParentForumArticle.Id;
                _AddForumArticleViewModel.ForumArticle.MasterCatalogProductId = _AddForumArticleViewModel.ParentForumArticle.MasterCatalogProductId;
                _AddForumArticleViewModel.ForumArticle.ForumGroupId = _AddForumArticleViewModel.ParentForumArticle.ForumGroupId;
            }
            else if (_AddForumArticleViewModel.ForumGroup != null)
            {
                //this is a new post in a ForumGroup
                _AddForumArticleViewModel.ForumArticle.ForumGroupId = _AddForumArticleViewModel.ForumGroup.Id;
            }
            
            _AddForumArticleViewModel.InsertForumArticle();

        }

        void _AddForumArticleViewModel_InsertForumArticleCompleted(object sender, RoutedEventArgs e)
        {
            RadWindow parent = (RadWindow)this.Parent;
            parent.DialogResult = true;
            parent.Close();
        }

        private void editor_HtmlChanged(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {

        }

    }
}
