﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Forums.ForumsService;
using Telerik.Windows.Controls;

namespace Forums
{
    public partial class UserInfoView : UserControl
    {
        UserInfoViewModel _UserInfoViewModel;
        
        public UserInfoView(UserInfoViewModel userInfoViewModel)
        {
            _UserInfoViewModel = userInfoViewModel;
           
            this.DataContext = _UserInfoViewModel;
            InitializeComponent();

            this.Loaded += new RoutedEventHandler(UserInfoView_Loaded);
        }
       
        void UserInfoView_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            RadWindow parent = (RadWindow)this.Parent;
            parent.DialogResult = false;
            parent.Close();
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            RadWindow parent = (RadWindow)this.Parent;
            parent.DialogResult = true;
            parent.Close();
        }

       
    }
}
