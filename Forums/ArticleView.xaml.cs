﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Forums.ForumsService;

namespace Forums
{
    public partial class ArticleView : UserControl
    {
        ArticleViewModel _ArticleViewModel;

        public event RoutedEventHandler ReplySelected;
        public event RoutedEventHandler UserSelected;

        public ArticleView(ArticleViewModel articleViewModel)
        {
            
            _ArticleViewModel = articleViewModel;
            _ArticleViewModel.ForumArticleLoaded += new RoutedEventHandler(_ArticleViewModel_ForumArticleLoaded);
            this.DataContext = _ArticleViewModel;
            
            InitializeComponent();
            

            if(!_ArticleViewModel.ForumArticle.IsActive && (App.Current.Resources["BregAdmin"].ToString() == "True"))
            {
                btnNotActive.Visibility = Visibility.Visible;
            }

            LoadArticles();
            this.Loaded += new RoutedEventHandler(ArticleView_Loaded);
        }

        void _ArticleViewModel_ForumArticleLoaded(object sender, RoutedEventArgs e)
        {
            LoadArticles();
        }

        public void RefreshTree()
        {
            _ArticleViewModel.GetForumArticleTreeByTopArticle(_ArticleViewModel.ForumArticle.Id, 1000,true);
        }

        void ArticleView_Loaded(object sender, RoutedEventArgs e)
        {
            layoutRoot.IsExpanded = true;
        }

        private void LoadArticles()
        {
            this.childArticles.Children.Clear();

            foreach (ForumArticle forumArticle in _ArticleViewModel.ForumArticle.ForumArticles)
            {
                ArticleView articleView = new ArticleView(new ArticleViewModel(forumArticle));
                articleView.ReplySelected += new RoutedEventHandler(articleView_ReplySelected);
                articleView.UserSelected += new RoutedEventHandler(articleView_UserSelected);
                childArticles.Children.Add(articleView);
            }
        }

        void articleView_UserSelected(object sender, RoutedEventArgs e)
        {
            if(UserSelected!=null)
                UserSelected(sender,e);
        }

        void articleView_ReplySelected(object sender, RoutedEventArgs e)
        {
            if (ReplySelected != null)
                ReplySelected(sender, e);
        }

        private void Reply_Click(object sender, RoutedEventArgs e)
        {
            if (ReplySelected != null)
                ReplySelected(_ArticleViewModel.ForumArticle, null);
        }

        private void User_Click(object sender, RoutedEventArgs e)
        {
            if (UserSelected != null)
                UserSelected(_ArticleViewModel.ForumArticle, null);
        }

        private void btnNotActive_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
