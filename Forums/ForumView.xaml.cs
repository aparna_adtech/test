﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Forums.ForumsService;
using Telerik.Windows.Controls;
using System.Windows.Data;
using System.Windows.Threading;
using System.Collections.ObjectModel;

namespace Forums
{
    public partial class ForumView : UserControl
    {
        ForumViewModel _ForumViewModel;
        RadTreeViewItem _TopNode = new RadTreeViewItem();
        RadTreeViewItem _ActiveTreeViewItem;
        DispatcherTimer inactiveArticleTimer;

        public ForumView(ForumViewModel forumViewModel)
        {
            _ForumViewModel = forumViewModel;
            _ForumViewModel.ForumGroupsLoaded += new RoutedEventHandler(_ForumViewModel_ForumGroupsLoaded);
            _ForumViewModel.ForumGroupsArticlesLoaded += new RoutedEventHandler(_ForumViewModel_ForumGroupsArticlesLoaded);
            _ForumViewModel.ForumArticleLoaded += new RoutedEventHandler(_ForumViewModel_ForumArticleLoaded);
            _ForumViewModel.DeleteForumArticleCompleted += new RoutedEventHandler(_ForumViewModel_DeleteForumArticleCompleted);
            this.DataContext = _ForumViewModel;
            
            InitializeComponent();

            this.treeView.AddHandler(RadTreeViewItem.MouseLeftButtonDownEvent, new MouseButtonEventHandler(treeView_MouseLeftButtonDown), true);

            this.Loaded += new RoutedEventHandler(ForumView_Loaded);

            if (App.Current.Resources.Contains("BregAdmin"))
            {
                if (App.Current.Resources["BregAdmin"].ToString() != "True")
                {
                    adminToolbar.Visibility = System.Windows.Visibility.Collapsed;
                    ContextMenu.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }

        void _ForumViewModel_DeleteForumArticleCompleted(object sender, RoutedEventArgs e)
        {
            if((bool)sender)
            {
                MessageBox.Show("Delete succeeded");
            }
            else
            {
                MessageBox.Show("Delete failed.  Make sure you are not trying to delete an article that has children.  If so please delete all the children 1st.");
            }
        }

        void _ForumViewModel_ForumArticleLoaded(object sender, RoutedEventArgs e)
        {
            if (!ActivatePane(sender as ForumArticle))
            {
                _ForumViewModel.ForumArticle = sender as ForumArticle;

                RadDocumentPane radDocumentPane = new RadDocumentPane();
                radDocumentPane.DataContext = _ForumViewModel.ForumArticle;
                ArticleView articleView = new ArticleView(new ArticleViewModel(_ForumViewModel.ForumArticle));
                articleView.ReplySelected += new RoutedEventHandler(articleView_ReplySelected);
                articleView.UserSelected += new RoutedEventHandler(articleView_UserSelected);

                ScrollViewer scrollViewer = new ScrollViewer();
                scrollViewer.Content = articleView;

                radDocumentPane.Content = scrollViewer;
                radDocumentPane.CanFloat = true;

                radDocumentPane.Header = _ForumViewModel.ForumArticle.Subject;

                radDocumentPane.HeaderTemplate = Resources["HeaderTemplateArticle"] as DataTemplate;
                panelGroupMain.Items.Add(radDocumentPane);
            }
        }

        void articleView_UserSelected(object sender, RoutedEventArgs e)
        {
            ForumArticle forumArticle = sender as ForumArticle;

            DisplayUser(forumArticle.UserId, forumArticle.PublicUserName);

        }

        private void DisplayUser(int userId, string userName)
        {
            UserViewModel userViewModel = new UserViewModel(userId, userName);

            if (!ActivatePane(userViewModel))
            {
                UserView userView = new UserView(userViewModel);
                userView.ForumArticlesLoaded += new RoutedEventHandler(userView_ForumArticlesLoaded);
                userView.ForumArticleSelected += new RoutedEventHandler(userView_ForumArticleSelected);

                RadDocumentPane radDocumentPane = new RadDocumentPane();
                radDocumentPane.DataContext = userViewModel;
                radDocumentPane.Content = userView;
                radDocumentPane.CanFloat = true;

                radDocumentPane.Header = userName;

                radDocumentPane.HeaderTemplate = Resources["HeaderTemplatePerson"] as DataTemplate;
                panelGroupMain.Items.Add(radDocumentPane);
            }
        }

        void userView_ForumArticlesLoaded(object sender, RoutedEventArgs e)
        {
            ObservableCollection<ForumArticle> thisArticles = sender as ObservableCollection<ForumArticle>;

            if (thisArticles != null)
            {
                //show the selected users articles.
                _ForumViewModel.ArticlesHeader = "Selected Users Posts";
                btnAddArticle.Visibility = System.Windows.Visibility.Collapsed;
                lstBox.ItemsSource = thisArticles;

            }
        }

        void userView_ForumArticleSelected(object sender, RoutedEventArgs e)
        {
            ForumArticle thisArticle = sender as ForumArticle;
          
            if (thisArticle != null)
            {
                _ForumViewModel.ForumArticle = null;
                _ForumViewModel.GetForumArticleTreeByTopArticle(thisArticle.Id, 1000, true);
            }
        }

        void articleView_ReplySelected(object sender, RoutedEventArgs e)
        {
            //display add new article form in new document window or modal window
            ForumArticle parentForumArticle = sender as ForumArticle;

            //hack to hide the RadhtmlPlaceholder HTML which wants to sit above the modal dialog!  we will set again on close of the modal.
            foreach (var itm in panelGroupMain.Items)
            {
                if (itm.GetType().ToString().Equals(typeof(RadDocumentPane).ToString()))
                {
                    RadDocumentPane rdp = (RadDocumentPane)itm;

                    if (rdp.DataContext.GetType().ToString().Equals(typeof(ForumArticle).ToString()))
                    {
                        ScrollViewer scrollViewer = rdp.Content as ScrollViewer;
                        ArticleView articleView = scrollViewer.Content as ArticleView;
                        articleView.Visibility = Visibility.Collapsed;
                    }
                }
            }

            AddForumArticleViewModel addForumArticleViewModel = new AddForumArticleViewModel(parentForumArticle, new ForumArticle());

            AddForumArticleView addForumArticleView = new AddForumArticleView(addForumArticleViewModel);

            RadWindow radWindow = new RadWindow();

            radWindow.Content = addForumArticleView;
            radWindow.Width = 800;
            radWindow.Height = 600;
            radWindow.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            radWindow.Header = "Reply to: " + parentForumArticle.Subject;

            radWindow.Closed += (se, ea) =>
            {
                if (radWindow.DialogResult == true)
                {
                    //refresh the active rdp
                    foreach (var itm in panelGroupMain.Items)
                    {
                        if (itm.GetType().ToString().Equals(typeof(RadDocumentPane).ToString()))
                        {
                            RadDocumentPane rdp = (RadDocumentPane)itm;
                            if (rdp.DataContext.GetType().ToString().Equals(typeof(ForumArticle).ToString()))
                            {
                                ScrollViewer scrollViewer = rdp.Content as ScrollViewer;
                                ArticleView articleView = scrollViewer.Content as ArticleView;
                                articleView.RefreshTree();
                            }
                        }
                    }
                }

                foreach (var itm in panelGroupMain.Items)
                {
                    if (itm.GetType().ToString().Equals(typeof(RadDocumentPane).ToString()))
                    {
                        RadDocumentPane rdp = (RadDocumentPane)itm;
                        if (rdp.DataContext.GetType().ToString().Equals(typeof(ForumArticle).ToString()))
                        {
                            ScrollViewer scrollViewer = rdp.Content as ScrollViewer;
                            ArticleView articleView = scrollViewer.Content as ArticleView;
                            articleView.Visibility = Visibility.Visible;
                        }
                    }
                }
            };

            radWindow.ShowDialog();
        }

        void _ForumViewModel_ForumGroupsArticlesLoaded(object sender, RoutedEventArgs e)
        {
            waitIndicator.IsActive = false;
            lstBox.ItemsSource = _ForumViewModel.ForumArticles;
        }

        void _ForumViewModel_ForumGroupsLoaded(object sender, RoutedEventArgs e)
        {
            treeView.Items.Clear();
            _TopNode.Items.Clear();

            foreach (ForumGroup forumGroup in _ForumViewModel.ForumGroups)
            {
                RadTreeViewItem thisNode = new RadTreeViewItem();
                thisNode.DataContext = forumGroup;
                thisNode.Header = forumGroup.Name;
                thisNode.DefaultImageSrc = "Images/folder16.png";
                _TopNode.Items.Add(thisNode);
            }

            //allows for adding fake nodes into the tree if need be
            List<RadTreeViewItem> items = new List<RadTreeViewItem>();

            foreach (RadTreeViewItem tvi in _TopNode.Items)
            {
                items.Add(tvi);
            }

            _TopNode.Items.Clear(); //because a node can only have one parent

            foreach (RadTreeViewItem tvi in items)
            {
                treeView.Items.Add(tvi);
            }

            waitIndicator.IsActive = false;
        }

        void ForumView_Loaded(object sender, RoutedEventArgs e)
        {
            waitIndicator.IsActive = true;
            _ForumViewModel.GetForumGroups(false, 0);

            if (App.Current.Resources["ArticleId"] != null && App.Current.Resources["ArticleId"].ToString() != "")
            {
                _ForumViewModel.GetForumArticleTreeByTopArticle(int.Parse(App.Current.Resources["ArticleId"].ToString()), 1000, true);
            }
            else if (App.Current.Resources["UserId"] != null && App.Current.Resources["UserId"].ToString() != "")
            {
                DisplayUser(int.Parse(App.Current.Resources["UserId"].ToString()), App.Current.Resources["UserName"].ToString());
            }
        }

        private void treeView_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            RadTreeViewItem treeViewItem = treeView.SelectedItem as RadTreeViewItem;
            if (treeViewItem != null)
            {
                lstBox.ItemsSource = null;

                _ActiveTreeViewItem = treeViewItem;
                ForumGroup forumGroup = treeViewItem.DataContext as ForumGroup;
                _ForumViewModel.SelectedForumGroup = forumGroup;
                btnAddArticle.Visibility = System.Windows.Visibility.Visible;

                _ForumViewModel.ArticlesHeader = forumGroup.Name + " articles";

                _ForumViewModel.GetRecentArticlesByGroupId(forumGroup.Id, 200, 1, true);
            }
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            Grid g = sender as Grid;

            Binding b = new Binding("ActualWidth");
            b.Source = lstBox;
            b.Mode = BindingMode.OneWay;

            g.SetBinding(Grid.WidthProperty, b);
        }

        private void lstBox_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (_ForumViewModel.ForumArticles != null)
            {
                lstBox.ItemsSource = null;
                lstBox.ItemsSource = _ForumViewModel.ForumArticles;
            }
        }

        private void lstBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            _ForumViewModel.ForumArticle = null;

            ForumArticle forumArticle = lstBox.SelectedItem as ForumArticle;

            if (forumArticle != null)
            {
                if (App.Current.Resources["BregAdmin"].ToString() == "True")
                {
                    //a breg admin will see all articles in an article thread active or not
                    _ForumViewModel.GetForumArticleTreeByTopArticle(forumArticle.Id, 1000, false);
                }
                else
                {
                    _ForumViewModel.GetForumArticleTreeByTopArticle(forumArticle.Id, 1000, true);
                }
            }
        }

        private bool ActivatePane(object sender)
        {
            foreach (var itm in panelGroupMain.Items)
            {
                if (itm.GetType().ToString().Equals(typeof(RadDocumentPane).ToString()))
                {
                    RadDocumentPane rdp = (RadDocumentPane)itm;
                    if (rdp.DataContext != null)
                    {
                        if (sender.GetType().ToString().Equals(typeof(ForumArticle).ToString()))
                        {
                            if (rdp.DataContext.GetType().ToString().Equals(typeof(ForumArticle).ToString()))
                            {
                                if (((ForumArticle)rdp.DataContext).Id == ((ForumArticle)sender).Id)
                                {
                                    rdp.IsSelected = true;
                                    return true;
                                }
                            }
                        }
                        else if (sender.GetType().ToString().Equals(typeof(UserViewModel).ToString()))
                        {
                            if (rdp.DataContext.GetType().ToString().Equals(typeof(UserViewModel).ToString()))
                            {
                                if (((UserViewModel)rdp.DataContext).UserPublicInfo.UserId == ((UserViewModel)sender).UserPublicInfo.UserId)
                                {
                                    rdp.IsSelected = true;
                                    return true;
                                }
                            }
                        }
                    }
                }
            }

            return false;

        }

        private void addArticle_Click(object sender, RoutedEventArgs e)
        {
            if (_ForumViewModel.SelectedForumGroup == null)
                return;

            //hack to hide the RadhtmlPlaceholder HTML which wants to sit above the modal dialog!  we will set again on close of the modal.
            foreach (var itm in panelGroupMain.Items)
            {
                if (itm.GetType().ToString().Equals(typeof(RadDocumentPane).ToString()))
                {
                    RadDocumentPane rdp = (RadDocumentPane)itm;

                    if (rdp.DataContext.GetType().ToString().Equals(typeof(ForumArticle).ToString()))
                    {
                        ScrollViewer viewer = rdp.Content as ScrollViewer;
                        ArticleView articleView = viewer.Content as ArticleView;
                        articleView.Visibility = Visibility.Collapsed;
                    }
                }
            }

            AddForumArticleViewModel addForumArticleViewModel = new AddForumArticleViewModel(_ForumViewModel.SelectedForumGroup, new ForumArticle());

            AddForumArticleView addForumArticleView = new AddForumArticleView(addForumArticleViewModel);

            RadWindow radWindow = new RadWindow();

            radWindow.Content = addForumArticleView;
            radWindow.Width = 800;
            radWindow.Height = 600;
            radWindow.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            radWindow.Header = "New Article in " + _ForumViewModel.SelectedForumGroup.Name;

            radWindow.Closed += (se, ea) =>
            {
                if (radWindow.DialogResult == true)
                {
                }

                foreach (var itm in panelGroupMain.Items)
                {
                    if (itm.GetType().ToString().Equals(typeof(RadDocumentPane).ToString()))
                    {
                        RadDocumentPane rdp = (RadDocumentPane)itm;
                        if (rdp.DataContext.GetType().ToString().Equals(typeof(ForumArticle).ToString()))
                        {
                            ScrollViewer viewer = rdp.Content as ScrollViewer;
                            ArticleView articleView = viewer.Content as ArticleView;
                            articleView.Visibility = Visibility.Visible;
                        }
                    }
                }
            };

            radWindow.ShowDialog();
        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DoSearch(true);
            }
        }

        private void DoSearch(bool isActive)
        {
            if (txtSearch.Text.Length > 2)
            {
                _ForumViewModel.ArticlesHeader = "Search Result";
                btnAddArticle.Visibility = System.Windows.Visibility.Collapsed;
                waitIndicator.IsActive = true;
                _ForumViewModel.GetForumArticlesBySearchText(txtSearch.Text, 1, isActive);


            }
        }
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            DoSearch(true);
        }

        private void ContextMenu_Click(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            
            ForumArticle forumArticle = lstBox.SelectedItem as ForumArticle;

            if (forumArticle != null)
            {
                string name = (e.OriginalSource as RadMenuItem).Name as string;

                switch (name)
                {
                    case "mnuSetArticleIsActive":
                        _ForumViewModel.SetForumArticleIsActive(forumArticle.Id, !forumArticle.IsActive);
                        break;
                    case "mnuDeleteArticle":
                        _ForumViewModel.DeleteForumArticle(forumArticle.Id);
                        break;
                    default:
                        break;
                }
            }
        }

        private void ContextMenu_Opened(object sender, RoutedEventArgs e)
        {
            //TODO show or hide context menu if user is an admin or not
        }

        private void chkInactiveArticles_Click(object sender, RoutedEventArgs e)
        {
            CheckBox chkBox = (CheckBox)e.OriginalSource;

            if ((bool)chkBox.IsChecked)
            {
                waitIndicator.IsActive = true;
                _ForumViewModel.GetRecentArticles(100, 1, false);
                _ForumViewModel.ArticlesHeader = "All Inactive Articles";
                StartInactiveTimer();
            }
            else
            {
                StopInactiveTimer();
            }
        }


        private void StartInactiveTimer()
        {
            
            if(inactiveArticleTimer == null)
            {
                int counter = 120;
                inactiveArticleTimer = new DispatcherTimer();

                inactiveArticleTimer.Tick +=
                    delegate(object s, EventArgs args)
                    {
                        tbInactiveTimerLabel.Text = " " + counter--.ToString();

                        if (counter < 0)
                        {
                            counter = 120;
                            waitIndicator.IsActive = true;
                            _ForumViewModel.GetRecentArticles(100, 1, false);
                            _ForumViewModel.ArticlesHeader = "All Inactive Articles";
                        }
                    };
            }

            inactiveArticleTimer.Interval = new TimeSpan(0, 0, 1); // one second
            inactiveArticleTimer.Start();

        }

       

        private void StopInactiveTimer()
        {
            if (inactiveArticleTimer.IsEnabled)
            {
                inactiveArticleTimer.Stop();
                tbInactiveTimerLabel.Text = "";
            }
        }

    }
}
