﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Forums.ForumsService;

namespace Forums
{
    public class UserViewModel : EntityBase
    {
        public event RoutedEventHandler ForumArticlesLoaded; //local the this view for loading the listbox
        public event RoutedEventHandler ForumArticleSelected; //tells formView to load the new Article thread in a new window
        public event RoutedEventHandler GetPracticeInfoCompleted; //tells formView to load the new Article thread in a new window

        ForumArticle _ForumArticle;
        public ForumArticle ForumArticle
        {
            get { return _ForumArticle; }
            set { _ForumArticle = value; OnPropertyChanged("ForumArticle"); }
        }

        ObservableCollection<ForumArticle> _ForumArticles;
        public ObservableCollection<ForumArticle> ForumArticles
        {
            get { return _ForumArticles; }
            set { _ForumArticles = value; OnPropertyChanged("ForumArticles"); }
        }

        PracticePublicInfo _PracticePublicInfo = new PracticePublicInfo();

        public PracticePublicInfo PracticePublicInfo
        {
            get { return _PracticePublicInfo; }
            set { _PracticePublicInfo = value; OnPropertyChanged("PracticePublicInfo"); }
        }

        UserPublicInfo _UserPublicInfo = new UserPublicInfo();

        public UserPublicInfo UserPublicInfo
        {
            get { return _UserPublicInfo; }
            set { _UserPublicInfo = value; OnPropertyChanged("UserPublicInfo"); }
        }

        
        public UserViewModel(int userId, string publicUserName)
        {
            UserPublicInfo.UserId = userId;
            UserPublicInfo.PublicUserName = publicUserName;
        }

        public void GetForumArticleTreeByTopArticle(int id, int depth, bool isActive)
        {
            ForumArticles = null;

            ForumsServiceClient client = new ForumsServiceClient();

            client.Endpoint.Address = EndPointHelper.GetServiceURI("Forums/ForumsService.svc");

            client.GetForumArticleTreeByTopArticleAsync(id, depth, isActive);
            client.GetForumArticleTreeByTopArticleCompleted += (se, ea) =>
            {
                if (ea.Error != null)
                {
                    //raise error to UI
                }
                else
                {
                    if (ea.Result.Exception != null)
                    {
                        //raise error to UI
                    }
                    else
                    {
                        //we conditionally set this in the view

                        if (ForumArticleSelected != null)
                            ForumArticleSelected(ea.Result.ForumArticle, null);
                        
                    }
                }
            };
        }

        public void GetForumArticlesByUserId(int userId, int depth, bool isActive)
        {
            ForumArticles = null;

            ForumsServiceClient client = new ForumsServiceClient();
            client.GetForumArticlesByUserIdAsync(userId, depth,isActive);
            client.GetForumArticlesByUserIdCompleted += (se, ea) =>
            {
                if (ea.Error != null)
                {
                    //raise error to UI
                }
                else
                {
                    if (ea.Result.Exception != null)
                    {
                        //raise error to UI
                    }
                    else
                    {
                        this.ForumArticles = ea.Result.ForumArticles;

                        if (ForumArticlesLoaded != null)
                            ForumArticlesLoaded(ea.Result.ForumArticles, null); //notifies the view to reload the listbox
                    }
                }
            };
        }

        public void GetUserPublicInfo(int userId)
        {
            
            ForumsServiceClient client = new ForumsServiceClient();
            client.GetUserPublicInfoAsync(userId);
            client.GetUserPublicInfoCompleted += (se, ea) =>
            {
                if (ea.Error != null)
                {
                    //raise error to UI
                }
                else
                {
                    if (ea.Result.Exception != null)
                    {
                        //raise error to UI
                    }
                    else
                    {
                        this.UserPublicInfo = ea.Result.UserPublicInfo; //should raise binding events
                    }
                }
            };
        }

        public void GetPracticePublicInfo(int userId)
        {
            ForumsServiceClient client = new ForumsServiceClient();
            client.GetPracticePublicInfoAsync(userId);
            client.GetPracticePublicInfoCompleted += (se, ea) =>
            {
                if (ea.Error != null)
                {
                    //raise error to UI
                }
                else
                {
                    if (ea.Result.Exception != null)
                    {
                        //raise error to UI
                    }
                    else
                    {
                        this.PracticePublicInfo = ea.Result.PracticePublicInfo; //raise binding events
                        
                    }

                    if (GetPracticeInfoCompleted != null)
                        GetPracticeInfoCompleted(ea.Result.PracticePublicInfo, null);
                }
            };
        }
    }
}
