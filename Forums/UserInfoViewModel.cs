﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Forums.ForumsService;
using System.Collections.ObjectModel;

namespace Forums
{
    public class UserInfoViewModel : EntityBase
    {


        PracticePublicInfo _PracticePublicInfo = new PracticePublicInfo();

        public PracticePublicInfo PracticePublicInfo
        {
            get { return _PracticePublicInfo; }
            set { _PracticePublicInfo = value; OnPropertyChanged("PracticePublicInfo"); }
        }

        UserPublicInfo _UserPublicInfo = new UserPublicInfo();

        public UserPublicInfo UserPublicInfo
        {
            get { return _UserPublicInfo; }
            set { _UserPublicInfo = value; OnPropertyChanged("UserPublicInfo"); }
        }

        
    }
}