﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;

namespace Forums
{
    public class DescriptionToHtmlConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string description = (string)value;
            return string.Format("<div style=\"height:auto;width:100%;padding:0;margin:0;font-family:Tahoma,Arial,Helvetica;font-size:12px;text-align:left\">{0}</div>", description);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class DiffFromTimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            DateTime datetime = (DateTime)value;
            string ret = string.Empty;

            //TimeZoneInfo localZone = TimeZoneInfo.Local;
            
            //TimeSpan tsOffset = localZone.GetUtcOffset(DateTime.Now);

            //DateTime datetimeLocal = datetime.Add(localZone.BaseUtcOffset);

            //TimeSpan ts = DateTime.Now - datetimeLocal;

            //if (ts.TotalDays >= 1)
            //{
            //    ret = ts.TotalDays.ToString("{0}") + " days old";
            //}
            //else if (ts.TotalHours >= 1)
            //{
            //    ret = ts.TotalHours.ToString("{0}") + " hours old";
            //}
            //else if (ts.TotalMinutes >= 1)
            //{
            //    ret = ts.TotalMinutes.ToString("{0}") + " minutes old";
            //}
            //else
            //{
            //    ret = ts.TotalSeconds.ToString("{0}") + " seconds old";
            //}

            ret = datetime.ToShortDateString() + "   ";

            return ret;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
