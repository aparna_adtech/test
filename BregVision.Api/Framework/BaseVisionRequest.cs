namespace BregVision.Api.Framework
{
    public class BaseVisionRequest
    {
        public string Username { get; set; }
        public string Password { get; set; } 
        public string DeviceId { get; set;  }
        public string LastLocation { get; set; }
    }
}