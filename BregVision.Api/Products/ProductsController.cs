﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BregVision.Api.Products.Models;
using BregWcfService;

namespace BregVision.Api.Products
{
    [Route(ControllerRoot)]
    public class ProductsController : ApiController
    {
        public const string ControllerRoot = "api/products";

        [HttpGet]
        public GetProductsForPracticeRequest Get()
        {
            return new GetProductsForPracticeRequest();
        }

        // GET api/<controller>
        [HttpPost]
        public IEnumerable<InventoryOrderStatusResult> GetProductsForPracticeLocation(GetProductsForPracticeRequest request)
        {
            UserContext.ValidateLogin(request.Username, request.Password, request.DeviceId, request.LastLocation);
            bool noSkipTake = request.Skip < 1 && request.Take < 1;
            int count;
            var data = LandingData.GetInventoryOrderStatus(out count, request.PracticeId, request.PracticeLocationID, request.Skip, request.Take, noSkipTake);
            return data;
        }
    }
}