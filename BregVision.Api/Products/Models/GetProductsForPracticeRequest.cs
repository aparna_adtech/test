using BregVision.Api.Framework;

namespace BregVision.Api.Products.Models
{
    public class GetProductsForPracticeRequest : BaseVisionRequest
    {
        public int PracticeId { get; set; }
        public int PracticeLocationID { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
    }
}