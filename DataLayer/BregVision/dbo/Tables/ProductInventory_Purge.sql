﻿CREATE TABLE [dbo].[ProductInventory_Purge] (
    [ProductInventory_PurgeID] INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ProductInventoryID]       INT           NOT NULL,
    [ActualWholesaleCost]      SMALLMONEY    NULL,
    [QuantityPurged]           INT           NOT NULL,
    [DatePurged]               DATETIME      NOT NULL,
    [Comments]                 VARCHAR (200) NULL,
    [CreatedUserID]            INT           NOT NULL,
    [CreatedDate]              DATETIME      NOT NULL,
    [ModifiedUserID]           INT           NULL,
    [ModifiedDate]             DATETIME      NULL,
    [IsActive]                 BIT           NOT NULL,
    CONSTRAINT [ProductInventory_Purge_PK] PRIMARY KEY CLUSTERED ([ProductInventory_PurgeID] ASC),
    CONSTRAINT [ProductInventory_ProductInventory_Purge_FK1] FOREIGN KEY ([ProductInventoryID]) REFERENCES [dbo].[ProductInventory] ([ProductInventoryID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the Product Inventory Purge table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_Purge', @level2type = N'COLUMN', @level2name = N'ProductInventory_PurgeID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the price the clinic paid for the product is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_Purge', @level2type = N'COLUMN', @level2name = N'ActualWholesaleCost';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the quantity of a specific purged product is enterd into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_Purge', @level2type = N'COLUMN', @level2name = N'QuantityPurged';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the system automatically enters the date of the purged product.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_Purge', @level2type = N'COLUMN', @level2name = N'DatePurged';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the user can enter in comments about the purged product.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_Purge', @level2type = N'COLUMN', @level2name = N'Comments';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in the Product Inventory Purge is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_Purge', @level2type = N'COLUMN', @level2name = N'IsActive';

