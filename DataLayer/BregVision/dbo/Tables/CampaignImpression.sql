﻿CREATE TABLE [dbo].[CampaignImpression] (
    [CampaignId]      INT      NOT NULL,
    [UserId]          INT      NOT NULL,
    [CreatedDateTime] DATETIME CONSTRAINT [DF_CampaignImpression_CreatedDateTime] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CampaignImpression_1] PRIMARY KEY CLUSTERED ([CampaignId] ASC, [UserId] ASC, [CreatedDateTime] ASC),
    CONSTRAINT [FK_CampaignImpression_Campaign] FOREIGN KEY ([CampaignId]) REFERENCES [dbo].[Campaign] ([Id]),
    CONSTRAINT [FK_CampaignImpression_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserID])
);

