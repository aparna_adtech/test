﻿CREATE TABLE [dbo].[ShoppingCartStatus] (
    [ShoppingCartStatusID] TINYINT      NOT NULL,
    [Status]               VARCHAR (50) NOT NULL,
    CONSTRAINT [ShoppingCartStatus_PK] PRIMARY KEY CLUSTERED ([ShoppingCartStatusID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the primary key for the shopping cart status table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ShoppingCartStatus', @level2type = N'COLUMN', @level2name = N'ShoppingCartStatusID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the status of the order will show pending or denied.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ShoppingCartStatus', @level2type = N'COLUMN', @level2name = N'Status';

