﻿CREATE TABLE [dbo].[BregVisionPracticeCode] (
    [BregVisionPracticeCodeID] INT          IDENTITY (1, 1) NOT NULL,
    [PracticeID]               INT          NOT NULL,
    [PracticeCode]             VARCHAR (50) NULL,
    [BregVisionAccountingCode] VARCHAR (50) NULL
);

