﻿CREATE TABLE [dbo].[ShoppingCartItem] (
    [ShoppingCartItemID]       INT        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ShoppingCartID]           INT        NOT NULL,
    [PracticeCatalogProductID] INT        NULL,
    [ShoppingCartItemStatusID] TINYINT    NULL,
    [Quantity]                 INT        NOT NULL,
    [ActualWholesaleCost]      SMALLMONEY NOT NULL,
    [CreatedUserID]            INT        NOT NULL,
    [CreatedDate]              DATETIME   CONSTRAINT [DF__ShoppingC__Creat__3D5E1FD2] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]           INT        NULL,
    [ModifiedDate]             DATETIME   NULL,
    [IsActive]                 BIT        NOT NULL,
    [IsLogoPart]               BIT        DEFAULT ((0)) NOT NULL,
    CONSTRAINT [ShoppingCartItem_PK] PRIMARY KEY CLUSTERED ([ShoppingCartItemID] ASC),
    CONSTRAINT [PracticeCatalogProduct_ShoppingCartItem_FK1] FOREIGN KEY ([PracticeCatalogProductID]) REFERENCES [dbo].[PracticeCatalogProduct] ([PracticeCatalogProductID]),
    CONSTRAINT [ShoppingCart_ShoppingCartItem_FK1] FOREIGN KEY ([ShoppingCartID]) REFERENCES [dbo].[ShoppingCart] ([ShoppingCartID]),
    CONSTRAINT [ShoppingCartItemStatus_ShoppingCartItem_FK1] FOREIGN KEY ([ShoppingCartItemStatusID]) REFERENCES [dbo].[ShoppingCartItemStatus] ([ShoppingCartItemStatusID])
);


GO



CREATE TRIGGER [dbo].[UpdateProductInventoryChangeLogForShoppingCartItem]
ON [dbo].[ShoppingCartItem] FOR INSERT, UPDATE, DELETE
AS
	SET NOCOUNT ON
	
	IF EXISTS (SELECT * FROM INSERTED)
	BEGIN
		INSERT INTO ProductChangeLog (PracticeCatalogProductID, PracticeId)
			SELECT a.PracticeCatalogProductID, a.PracticeID
			FROM PracticeCatalogProduct a
			INNER JOIN INSERTED b ON a.PracticeCatalogProductID=b.PracticeCatalogProductID
	END

	IF EXISTS (SELECT * FROM DELETED)
	BEGIN
		INSERT INTO ProductChangeLog (PracticeCatalogProductID, PracticeId)
			SELECT a.PracticeCatalogProductID, a.PracticeID
			FROM PracticeCatalogProduct a
			INNER JOIN DELETED b ON a.PracticeCatalogProductID=b.PracticeCatalogProductID
	END

GO
CREATE trigger dbo.trg_ShoppingCartItem_Deleted 
ON dbo.ShoppingCartItem 
FOR DELETE

AS
begin
    insert into dbo.ShoppingCartItem_Deleted
		(
			  ShoppingCartItemID 
			, ShoppingCartID 
			, PracticeCatalogProductID
			, ShoppingCartItemStatusID
			, Quantity 
			, ActualWholesaleCost 
			, CreatedUserID 
			, CreatedDate
			, ModifiedUserID 
			, ModifiedDate 
			, IsActive 
		)
    select 
		      ShoppingCartItemID 
			, ShoppingCartID 
			, PracticeCatalogProductID
			, ShoppingCartItemStatusID
			, Quantity 
			, ActualWholesaleCost 
			, CreatedUserID 
			, CreatedDate
			, ModifiedUserID 
			, ModifiedDate 
			, IsActive 
    from deleted

end


GO
CREATE trigger [dbo].[trg_ShoppingCartItem_Updated]
ON [dbo].[ShoppingCartItem] 
FOR UPDATE

AS
begin
    insert into dbo.ShoppingCartItem_Updated
		(
			  ShoppingCartItemID 
			, [Action]
			, ShoppingCartID 
			, PracticeCatalogProductID
			, ShoppingCartItemStatusID
			, Quantity 
			, ActualWholesaleCost 
			, CreatedUserID 
			, CreatedDate
			, ModifiedUserID 
			, ModifiedDate 
			, IsActive 
		)
    select 
		      ShoppingCartItemID 
			, 'Update OLD'
			, ShoppingCartID 
			, PracticeCatalogProductID
			, ShoppingCartItemStatusID
			, Quantity 
			, ActualWholesaleCost 
			, CreatedUserID 
			, CreatedDate
			, ModifiedUserID 
			, ModifiedDate 
			, IsActive 
    from Deleted


    insert into dbo.ShoppingCartItem_Updated
		(
			  ShoppingCartItemID
			, [Action] 
			, ShoppingCartID 
			, PracticeCatalogProductID
			, ShoppingCartItemStatusID
			, Quantity 
			, ActualWholesaleCost 
			, CreatedUserID 
			, CreatedDate
			, ModifiedUserID 
			, ModifiedDate 
			, IsActive 
		)
    select 
		      ShoppingCartItemID 
			, 'Update New' 
			, ShoppingCartID 
			, PracticeCatalogProductID
			, ShoppingCartItemStatusID
			, Quantity 
			, ActualWholesaleCost 
			, CreatedUserID 
			, CreatedDate
			, ModifiedUserID 
			, ModifiedDate 
			, IsActive 
    from Inserted



end


GO
CREATE trigger dbo.trg_ShoppingCartItem_Inserted 
ON dbo.ShoppingCartItem 
FOR INSERT

AS
begin
    insert into dbo.ShoppingCartItem_Inserted
		(
			  ShoppingCartItemID 
			, ShoppingCartID 
			, PracticeCatalogProductID
			, ShoppingCartItemStatusID
			, Quantity 
			, ActualWholesaleCost 
			, CreatedUserID 
			, CreatedDate
			, ModifiedUserID 
			, ModifiedDate 
			, IsActive 
		)
    select 
		      ShoppingCartItemID 
			, ShoppingCartID 
			, PracticeCatalogProductID
			, ShoppingCartItemStatusID
			, Quantity 
			, ActualWholesaleCost 
			, CreatedUserID 
			, CreatedDate
			, ModifiedUserID 
			, ModifiedDate 
			, IsActive 
    from inserted

end


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the primary key for the Shopping Cart Item table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ShoppingCartItem', @level2type = N'COLUMN', @level2name = N'ShoppingCartItemID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the desired quantity is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ShoppingCartItem', @level2type = N'COLUMN', @level2name = N'Quantity';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the cost, per unit, is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ShoppingCartItem', @level2type = N'COLUMN', @level2name = N'ActualWholesaleCost';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in a shopping cart item is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ShoppingCartItem', @level2type = N'COLUMN', @level2name = N'IsActive';

