﻿CREATE TABLE [dbo].[HelpArticles] (
    [Id]               INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [HelpEntityTypeId] INT            CONSTRAINT [DF_HelpArticles_HelpEntityTypeId] DEFAULT ((2)) NOT NULL,
    [Title]            NVARCHAR (200) NOT NULL,
    [Description]      NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_HelpArticles] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_HelpArticles_HelpEntityTypes] FOREIGN KEY ([HelpEntityTypeId]) REFERENCES [dbo].[HelpEntityTypes] ([Id])
);

