﻿CREATE TABLE [dbo].[tmpOracleExport] (
    [ops flag]               NVARCHAR (50)  NULL,
    [customer no]            INT            NULL,
    [customer nm]            NVARCHAR (100) NULL,
    [bill to site number]    INT            NULL,
    [bill to company nm]     NVARCHAR (100) NULL,
    [bill to address line 1] NVARCHAR (100) NULL,
    [bill to address line 2] NVARCHAR (100) NULL,
    [bill to city]           NVARCHAR (50)  NULL,
    [bill to state]          NVARCHAR (50)  NULL,
    [bill to zip]            NVARCHAR (50)  NULL,
    [ship to site number]    INT            NULL,
    [ship to company nm]     NVARCHAR (100) NULL,
    [ship to address line 1] NVARCHAR (100) NULL,
    [ship to address line 2] NVARCHAR (100) NULL,
    [ship to city]           NVARCHAR (50)  NULL,
    [ship to state]          NVARCHAR (50)  NULL,
    [ship to zip]            NVARCHAR (50)  NULL
);

