﻿CREATE TABLE [dbo].[xxxBregBledsoePracticeCatalogProductLookup_01202016_1405] (
    [PracticeID]                  INT           NOT NULL,
    [oldPracticeCatalogProductID] INT           NOT NULL,
    [oldMasterCatalogProductID]   INT           NOT NULL,
    [newMasterCatalogProductID]   INT           NULL,
    [newPracticeCatalogProductID] INT           NULL,
    [oldCode]                     VARCHAR (50)  NOT NULL,
    [oldName]                     VARCHAR (100) NOT NULL
);

