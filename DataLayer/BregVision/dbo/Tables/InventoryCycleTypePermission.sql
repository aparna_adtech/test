﻿CREATE TABLE [dbo].[InventoryCycleTypePermission] (
    [InventoryCycleTypeID] INT           NOT NULL,
    [Role]                 NVARCHAR (50) NOT NULL,
    [CreatedUserID]        INT           NOT NULL,
    [CreatedDate]          DATETIME      CONSTRAINT [DF_InventoryCycleTypePermission_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]       INT           NULL,
    [ModifiedDate]         DATETIME      NULL,
    [IsActive]             BIT           CONSTRAINT [DF_InventoryCycleTypePermission_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_InventoryCycleTypePermission] PRIMARY KEY CLUSTERED ([InventoryCycleTypeID] ASC, [Role] ASC),
    CONSTRAINT [FK_InventoryCycleTypePermission_InventoryCycleType] FOREIGN KEY ([InventoryCycleTypeID]) REFERENCES [dbo].[InventoryCycleType] ([InventoryCycleTypeID])
);

