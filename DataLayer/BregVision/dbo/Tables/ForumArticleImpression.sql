﻿CREATE TABLE [dbo].[ForumArticleImpression] (
    [ArticleId] INT      NOT NULL,
    [UserId]    INT      NOT NULL,
    [Created]   DATETIME CONSTRAINT [DF_ForumArticleImpression_Created] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ForumArticleImpression] PRIMARY KEY CLUSTERED ([ArticleId] ASC, [UserId] ASC),
    CONSTRAINT [FK_ForumArticleImpression_ForumArticle] FOREIGN KEY ([ArticleId]) REFERENCES [dbo].[ForumArticle] ([Id]),
    CONSTRAINT [FK_ForumArticleImpression_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserID])
);

