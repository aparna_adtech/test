﻿CREATE TABLE [dbo].[UPCCode] (
    [UPCCodeID]              INT          IDENTITY (1, 1) NOT NULL,
    [Code]                   VARCHAR (50) NOT NULL,
    [MasterCatalogProductID] INT          NULL,
    [ThirdPartyProductID]    INT          NULL,
    [CreatedUserID]          INT          DEFAULT ((1)) NOT NULL,
    [CreatedDate]            DATETIME     DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]         INT          NULL,
    [ModifiedDate]           DATETIME     NULL,
    [IsActive]               BIT          DEFAULT ((1)) NOT NULL,
    [timestamp]              ROWVERSION   NOT NULL,
    CONSTRAINT [PK_UPCCode] PRIMARY KEY CLUSTERED ([UPCCodeID] ASC),
    CONSTRAINT [FK_UPCCode_MasterCatalogProduct] FOREIGN KEY ([MasterCatalogProductID]) REFERENCES [dbo].[MasterCatalogProduct] ([MasterCatalogProductID]),
    CONSTRAINT [FK_UPCCode_ThirdPartyProduct] FOREIGN KEY ([ThirdPartyProductID]) REFERENCES [dbo].[ThirdPartyProduct] ([ThirdPartyProductID])
);


GO
CREATE NONCLUSTERED INDEX [FK_MCP_ID]
    ON [dbo].[UPCCode]([MasterCatalogProductID] ASC);


GO
CREATE NONCLUSTERED INDEX [FK_TPP_ID]
    ON [dbo].[UPCCode]([ThirdPartyProductID] ASC);


GO



CREATE TRIGGER [dbo].[UpdateProductInventoryChangeLogForUPCCode]
ON [dbo].[UPCCode] FOR INSERT, UPDATE, Delete

AS 
	SET NOCOUNT ON

	DECLARE @MasterCatalogProductId_insert as int
	DECLARE @ThirdPartyProductId_insert as int
	
	DECLARE @MasterCatalogProductId_deleted as int
	DECLARE @ThirdPartyProductId_deleted as int

	SELECT
		@MasterCatalogProductId_insert = INSERTED.MasterCatalogProductID,
		@ThirdPartyProductId_insert = INSERTED.ThirdPartyProductID
	FROM INSERTED

	SELECT
		@MasterCatalogProductId_deleted = DELETED.MasterCatalogProductID,
		@ThirdPartyProductId_deleted = DELETED.ThirdPartyProductID
	FROM DELETED

	INSERT INTO ProductChangeLog (MasterCatalogProductId, PracticeId, PracticeLocationId)
		SELECT
			mcp.[MasterCatalogProductID], -1,-1
		FROM
			[dbo].[MasterCatalogProduct] mcp
		WHERE
			mcp.[IsActive] = 1
			AND (mcp.[MasterCatalogProductID] = @MasterCatalogProductId_deleted OR mcp.MasterCatalogProductID = @MasterCatalogProductId_insert)

	INSERT INTO ProductChangeLog (ProductInventoryId, PracticeCatalogProductId, PracticeId, PracticeLocationId) 
		SELECT
			pinv.ProductInventoryID, pcp.PracticeCatalogProductID, pcp.PracticeID, pinv.PracticeLocationID
		FROM
			ThirdPartyProduct tpp
			INNER JOIN PracticeCatalogProduct pcp ON pcp.ThirdPartyProductID = tpp.ThirdPartyProductID
			INNER JOIN ProductInventory pinv ON pinv.PracticeCatalogProductID = pcp.PracticeCatalogProductID
		WHERE
			tpp.ThirdPartyProductID = @ThirdPartyProductId_deleted OR tpp.ThirdPartyProductID = @ThirdPartyProductId_insert
			AND tpp.[IsActive] = 1
			AND pcp.[IsActive] = 1
			AND pinv.[IsActive] = 1
