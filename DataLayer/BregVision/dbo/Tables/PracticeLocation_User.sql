﻿CREATE TABLE [dbo].[PracticeLocation_User] (
    [PracticeLocationID] INT      NOT NULL,
    [UserID]             INT      NOT NULL,
    [CreatedUserID]      INT      NOT NULL,
    [CreatedDate]        DATETIME NOT NULL,
    [ModifiedUserID]     INT      NULL,
    [ModifiedDate]       DATETIME NULL,
    [IsActive]           BIT      NOT NULL,
    CONSTRAINT [PracticeLocation_User_PK] PRIMARY KEY CLUSTERED ([PracticeLocationID] ASC, [UserID] ASC),
    CONSTRAINT [PracticeLocation_PracticeLocation_User_FK1] FOREIGN KEY ([PracticeLocationID]) REFERENCES [dbo].[PracticeLocation] ([PracticeLocationID]),
    CONSTRAINT [User_PracticeLocation_User_FK1] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID])
);

