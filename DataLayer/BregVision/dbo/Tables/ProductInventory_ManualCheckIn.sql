﻿CREATE TABLE [dbo].[ProductInventory_ManualCheckIn] (
    [ProductInventory_ManualCheckInID] INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ProductInventoryID]               INT           NOT NULL,
    [PracticeCatalogProductID]         INT           NULL,
    [ActualWholesaleCost]              SMALLMONEY    NOT NULL,
    [Quantity]                         INT           NOT NULL,
    [DateCheckedIn]                    DATETIME      NOT NULL,
    [IsAcquiredOutsideOfSystem]        BIT           NOT NULL,
    [IsReturn]                         BIT           NOT NULL,
    [DispenseDetailID]                 INT           NULL,
    [Comments]                         VARCHAR (200) NULL,
    [CreatedUserID]                    INT           NOT NULL,
    [CreatedDate]                      DATETIME      NOT NULL,
    [ModifiedUserID]                   INT           NULL,
    [ModifiedDate]                     DATETIME      NULL,
    [IsActive]                         BIT           NOT NULL,
    CONSTRAINT [ProductInventory_ManualCheckIn_PK] PRIMARY KEY CLUSTERED ([ProductInventory_ManualCheckInID] ASC),
    CONSTRAINT [PracticeCatalogProduct_ProductInventory_ManualCheckIn_FK1] FOREIGN KEY ([PracticeCatalogProductID]) REFERENCES [dbo].[PracticeCatalogProduct] ([PracticeCatalogProductID]),
    CONSTRAINT [ProductInventory_ProductInventory_ManualCheckIn_FK1] FOREIGN KEY ([ProductInventoryID]) REFERENCES [dbo].[ProductInventory] ([ProductInventoryID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the primary key for the Product Inventory Manual Check-in Table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_ManualCheckIn', @level2type = N'COLUMN', @level2name = N'ProductInventory_ManualCheckInID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the actual price, per unit, that was paid by the clinic, is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_ManualCheckIn', @level2type = N'COLUMN', @level2name = N'ActualWholesaleCost';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the quantity of a specific product being checked in is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_ManualCheckIn', @level2type = N'COLUMN', @level2name = N'Quantity';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the system will automatically enter a time stamp for the product that was checked in.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_ManualCheckIn', @level2type = N'COLUMN', @level2name = N'DateCheckedIn';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that verifies if the product was acquired outside of the system.  For example, there are clinic locations shutting down within a practice and other locations absorb the leftover inventory.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_ManualCheckIn', @level2type = N'COLUMN', @level2name = N'IsAcquiredOutsideOfSystem';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that verifies if the product was a return.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_ManualCheckIn', @level2type = N'COLUMN', @level2name = N'IsReturn';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which comments about the product can be entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_ManualCheckIn', @level2type = N'COLUMN', @level2name = N'Comments';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in the product inventory manual check-in  is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_ManualCheckIn', @level2type = N'COLUMN', @level2name = N'IsActive';

