﻿CREATE TABLE [dbo].[HelpEntityTree] (
    [Id]           INT IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ParentId]     INT NULL,
    [EntityId]     INT NOT NULL,
    [EntityTypeId] INT NOT NULL,
    [SortOrder]    INT CONSTRAINT [DF_HelpEntityTree_SortOrder] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_HelpEntityTree] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_HelpEntityTree_HelpEntityTree] FOREIGN KEY ([ParentId]) REFERENCES [dbo].[HelpEntityTree] ([Id]),
    CONSTRAINT [FK_HelpEntityTree_HelpEntityTypes] FOREIGN KEY ([EntityTypeId]) REFERENCES [dbo].[HelpEntityTypes] ([Id])
);

