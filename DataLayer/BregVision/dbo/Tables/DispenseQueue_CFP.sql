﻿CREATE TABLE [dbo].[DispenseQueue_CFP] (
    [DispensQueueCfpID]    INT           IDENTITY (1, 1) NOT NULL,
    [DispenseQueueID]      INT           NOT NULL,
    [CustomFitProcedureID] INT           NOT NULL,
    [UserInput]            VARCHAR (140) NULL,
    CONSTRAINT [PK_DispenseQueue_CFP] PRIMARY KEY CLUSTERED ([DispensQueueCfpID] ASC),
    CONSTRAINT [FK_DispenseQueue_CFP_CustomFitProcedure] FOREIGN KEY ([CustomFitProcedureID]) REFERENCES [dbo].[CustomFitProcedure] ([CustomFitProcedureID]),
    CONSTRAINT [FK_DispenseQueue_CFP_DispenseQueue] FOREIGN KEY ([DispenseQueueID]) REFERENCES [dbo].[DispenseQueue] ([DispenseQueueID])
);

