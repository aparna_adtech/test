﻿CREATE TABLE [dbo].[ShippingType] (
    [ShippingTypeID]   TINYINT       NOT NULL,
    [ShippingTypeName] VARCHAR (50)  NULL,
    [BregServiceLevel] VARCHAR (100) NULL,
    CONSTRAINT [ShippingType_PK] PRIMARY KEY CLUSTERED ([ShippingTypeID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the shipping type table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ShippingType', @level2type = N'COLUMN', @level2name = N'ShippingTypeID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which shipping can be selected: Next Day, Second Day, Third Day or Ground (3-5 days).', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ShippingType', @level2type = N'COLUMN', @level2name = N'ShippingTypeName';

