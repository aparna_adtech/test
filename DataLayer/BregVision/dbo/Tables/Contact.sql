﻿CREATE TABLE [dbo].[Contact] (
    [ContactID]          INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Title]              VARCHAR (50)  NULL,
    [Salutation]         VARCHAR (10)  NULL,
    [FirstName]          VARCHAR (50)  NOT NULL,
    [MiddleName]         VARCHAR (50)  NULL,
    [LastName]           VARCHAR (50)  NOT NULL,
    [Suffix]             VARCHAR (10)  NULL,
    [EmailAddress]       VARCHAR (100) NULL,
    [PhoneWork]          VARCHAR (25)  NULL,
    [PhoneWorkExtension] VARCHAR (10)  CONSTRAINT [DF_Contact_PhoneWorkExtension] DEFAULT ('') NULL,
    [PhoneCell]          VARCHAR (25)  NULL,
    [PhoneHome]          VARCHAR (25)  NULL,
    [Fax]                VARCHAR (25)  NULL,
    [IsSpecificContact]  BIT           CONSTRAINT [DF_Contact_IsSpecificContact] DEFAULT ((0)) NOT NULL,
    [CreatedUserID]      INT           NOT NULL,
    [CreatedDate]        DATETIME      CONSTRAINT [DF_Contact_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]     INT           NULL,
    [ModifiedDate]       DATETIME      NULL,
    [IsActive]           BIT           NOT NULL,
    CONSTRAINT [Contact_PK] PRIMARY KEY CLUSTERED ([ContactID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [_dta_index_Contact_12_85575343__K18_K1_4_6]
    ON [dbo].[Contact]([IsActive] ASC, [ContactID] ASC)
    INCLUDE([FirstName], [LastName]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_Contact_12_85575343__K1_4_6]
    ON [dbo].[Contact]([ContactID] ASC)
    INCLUDE([FirstName], [LastName]);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the contact table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contact', @level2type = N'COLUMN', @level2name = N'ContactID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the user''s title is entered into. ( Clinic Administrator, etc.)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contact', @level2type = N'COLUMN', @level2name = N'Title';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the user''s salutation is entered into (Mr., Mrs., Miss, Ms. Dr., etc.)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contact', @level2type = N'COLUMN', @level2name = N'Salutation';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the user''s first name is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contact', @level2type = N'COLUMN', @level2name = N'FirstName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the user''s middle name is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contact', @level2type = N'COLUMN', @level2name = N'MiddleName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the user''s last name is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contact', @level2type = N'COLUMN', @level2name = N'LastName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the user''s suffix is entered into (Jr., Sr., II, III, etc.)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contact', @level2type = N'COLUMN', @level2name = N'Suffix';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the user''s e-mail address is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contact', @level2type = N'COLUMN', @level2name = N'EmailAddress';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the user''s work phone number is entered into.  This will contain 10 characters with no formatting.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contact', @level2type = N'COLUMN', @level2name = N'PhoneWork';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the user''s cell phone number is entered into.  This will contain 10 characters with no formatting.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contact', @level2type = N'COLUMN', @level2name = N'PhoneCell';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the user''s home phone number is entered into.  This will contain 10 characters with no formatting.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contact', @level2type = N'COLUMN', @level2name = N'PhoneHome';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the user''s fax number is entered into.  This will contain 10 characters with no formatting.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contact', @level2type = N'COLUMN', @level2name = N'Fax';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in contacts is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Contact', @level2type = N'COLUMN', @level2name = N'IsActive';

