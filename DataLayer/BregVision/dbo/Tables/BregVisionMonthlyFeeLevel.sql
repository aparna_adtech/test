﻿CREATE TABLE [dbo].[BregVisionMonthlyFeeLevel] (
    [BregVisionMonthlyFeeLevelID] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [LevelName]                   VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_BregVisionMonthlyFeeLevel] PRIMARY KEY CLUSTERED ([BregVisionMonthlyFeeLevelID] ASC)
);

