﻿CREATE TABLE [dbo].[PracticeLocationBillingAddress] (
    [PracticeLocationID]  INT          NOT NULL,
    [AddressID]           INT          NOT NULL,
    [AttentionOf]         VARCHAR (50) NULL,
    [CreatedUserID]       INT          NOT NULL,
    [CreatedDate]         DATETIME     NOT NULL,
    [ModifiedUserID]      INT          NULL,
    [ModifiedDate]        DATETIME     NULL,
    [IsActive]            BIT          NOT NULL,
    [BregOracleID]        VARCHAR (50) NULL,
    [BregOracleIDInvalid] BIT          DEFAULT ((0)) NULL,
    CONSTRAINT [PracticeLocationBillingAddress_PK] PRIMARY KEY CLUSTERED ([PracticeLocationID] ASC, [AddressID] ASC),
    CONSTRAINT [Address_PracticeLocationBillingAddress_FK1] FOREIGN KEY ([AddressID]) REFERENCES [dbo].[Address] ([AddressID]),
    CONSTRAINT [PracticeLocation_PracticeLocationBillingAddress_FK1] FOREIGN KEY ([PracticeLocationID]) REFERENCES [dbo].[PracticeLocation] ([PracticeLocationID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which users at locations can enter in who will recieve the bill (accountants, financial department, accounting, etc.)  ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeLocationBillingAddress', @level2type = N'COLUMN', @level2name = N'AttentionOf';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in the practice location billing address is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeLocationBillingAddress', @level2type = N'COLUMN', @level2name = N'IsActive';

