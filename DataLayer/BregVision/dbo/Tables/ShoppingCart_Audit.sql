﻿CREATE TABLE [dbo].[ShoppingCart_Audit] (
    [ShoppingCart_AuditID] INT           IDENTITY (1, 1) NOT NULL,
    [ShoppingCartID]       INT           NOT NULL,
    [PracticeLocationID]   INT           NOT NULL,
    [ShoppingCartStatusID] TINYINT       NULL,
    [ShippingTypeID]       TINYINT       NULL,
    [DeniedComments]       VARCHAR (200) NOT NULL,
    [CreatedUserID]        INT           NOT NULL,
    [CreatedDate]          DATETIME      NOT NULL,
    [ModifiedUserID]       INT           NULL,
    [ModifiedDate]         DATETIME      NULL,
    [IsActive]             BIT           NOT NULL,
    [AuditAction]          VARCHAR (50)  NULL,
    [AuditCreatedDate]     DATETIME      CONSTRAINT [DF_ShoppingCart_Audit_AuditCreatedDate] DEFAULT (getdate()) NOT NULL
);

