﻿CREATE TABLE [dbo].[ThirdPartySupplier] (
    [ThirdPartySupplierID]  INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PracticeID]            INT           NOT NULL,
    [ContactID]             INT           NULL,
    [AddressID]             INT           NULL,
    [SupplierName]          VARCHAR (50)  NOT NULL,
    [SupplierShortName]     VARCHAR (50)  NOT NULL,
    [FaxOrderPlacement]     VARCHAR (50)  NULL,
    [EmailOrderPlacement]   VARCHAR (100) NULL,
    [IsEmailOrderPlacement] BIT           NOT NULL,
    [IsVendor]              BIT           NOT NULL,
    [Sequence]              INT           NULL,
    [CreatedUserID]         INT           NOT NULL,
    [CreatedDate]           DATETIME      CONSTRAINT [DF__ThirdPart__Creat__239E4DCF] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]        INT           NULL,
    [ModifiedDate]          DATETIME      NULL,
    [IsActive]              BIT           NOT NULL,
    [Source]                VARCHAR (50)  NULL,
    [IsFaxVerified]         BIT           DEFAULT ((0)) NOT NULL,
    CONSTRAINT [ThirdPartySupplier_PK] PRIMARY KEY CLUSTERED ([ThirdPartySupplierID] ASC),
    CONSTRAINT [Address_ThirdPartySupplier_FK1] FOREIGN KEY ([AddressID]) REFERENCES [dbo].[Address] ([AddressID]),
    CONSTRAINT [FK_ThirdPartySupplier_Contact] FOREIGN KEY ([ContactID]) REFERENCES [dbo].[Contact] ([ContactID]),
    CONSTRAINT [Practice_ThirdPartySupplier_FK1] FOREIGN KEY ([PracticeID]) REFERENCES [dbo].[Practice] ([PracticeID])
);


GO
CREATE STATISTICS [_dta_stat_1909581841_1_10]
    ON [dbo].[ThirdPartySupplier]([ThirdPartySupplierID], [IsVendor]);


GO
CREATE STATISTICS [_dta_stat_1909581841_10_16_1]
    ON [dbo].[ThirdPartySupplier]([IsVendor], [IsActive], [ThirdPartySupplierID]);


GO
CREATE STATISTICS [_dta_stat_1909581841_1_4_3]
    ON [dbo].[ThirdPartySupplier]([ThirdPartySupplierID], [AddressID], [ContactID]);


GO
CREATE STATISTICS [_dta_stat_1909581841_9_8_7]
    ON [dbo].[ThirdPartySupplier]([IsEmailOrderPlacement], [EmailOrderPlacement], [FaxOrderPlacement]);


GO
CREATE STATISTICS [_dta_stat_1909581841_16_1_4_3]
    ON [dbo].[ThirdPartySupplier]([IsActive], [ThirdPartySupplierID], [AddressID], [ContactID]);


GO
CREATE STATISTICS [_dta_stat_1909581841_1_9_8_7]
    ON [dbo].[ThirdPartySupplier]([ThirdPartySupplierID], [IsEmailOrderPlacement], [EmailOrderPlacement], [FaxOrderPlacement]);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the Third Party Supplier table.  It should be noted that third party suppliers must be manually entered in by the clinics because  these suppliers live on the outside of the master catalog.  Third party suppliers are different for each clinic.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartySupplier', @level2type = N'COLUMN', @level2name = N'ThirdPartySupplierID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the supplier''s name is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartySupplier', @level2type = N'COLUMN', @level2name = N'SupplierName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the abbreviated version of the supplier''s name is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartySupplier', @level2type = N'COLUMN', @level2name = N'SupplierShortName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field for the supplier to indicate they take the order by fax.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartySupplier', @level2type = N'COLUMN', @level2name = N'FaxOrderPlacement';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field for the supplier to indicate they take the order by e-mail.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartySupplier', @level2type = N'COLUMN', @level2name = N'EmailOrderPlacement';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag to verify that the supplier takes the order by email.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartySupplier', @level2type = N'COLUMN', @level2name = N'IsEmailOrderPlacement';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag to verify that the supplier is a reseller.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartySupplier', @level2type = N'COLUMN', @level2name = N'IsVendor';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field to define the order in which the suppliers will be displayed. ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartySupplier', @level2type = N'COLUMN', @level2name = N'Sequence';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in third party supplier is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartySupplier', @level2type = N'COLUMN', @level2name = N'IsActive';

