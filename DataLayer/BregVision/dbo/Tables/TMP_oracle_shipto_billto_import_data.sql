﻿CREATE TABLE [dbo].[TMP_oracle_shipto_billto_import_data] (
    [OPS Flag]               NVARCHAR (50)  NULL,
    [Customer No]            NVARCHAR (50)  NULL,
    [Customer Nm]            NVARCHAR (200) NULL,
    [Bill To Site Number]    NVARCHAR (50)  NULL,
    [Bill To Company Nm]     NVARCHAR (200) NULL,
    [Bill To Address Line 1] NVARCHAR (200) NULL,
    [Bill To Address Line 2] NVARCHAR (200) NULL,
    [Bill to City]           NVARCHAR (50)  NULL,
    [Bill To State]          NVARCHAR (10)  NULL,
    [Bill To Zip]            NVARCHAR (10)  NULL,
    [Ship To Site Number]    NVARCHAR (50)  NULL,
    [Ship To Company Nm]     NVARCHAR (200) NULL,
    [Ship To Address Line 1] NVARCHAR (200) NULL,
    [Ship To Address Line 2] NVARCHAR (200) NULL,
    [Ship To City]           NVARCHAR (50)  NULL,
    [Ship To State]          NVARCHAR (10)  NULL,
    [Ship To Zip]            NVARCHAR (10)  NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_customer_number]
    ON [dbo].[TMP_oracle_shipto_billto_import_data]([Customer No] ASC);

