﻿CREATE TABLE [dbo].[PracticeApplicationAgreementTerms] (
    [PracticeAppAgreementTermsID] INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Terms]                       NVARCHAR (MAX) NULL,
    [DateTime]                    DATETIME       NOT NULL,
    CONSTRAINT [PK_PracticeApplicationAgreementTerms] PRIMARY KEY CLUSTERED ([PracticeAppAgreementTermsID] ASC)
);

