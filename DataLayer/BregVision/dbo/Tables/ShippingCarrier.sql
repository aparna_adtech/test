﻿CREATE TABLE [dbo].[ShippingCarrier] (
    [ShippingCarrierID]   INT          NOT NULL,
    [ShippingCarrierName] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ShippingCarrier] PRIMARY KEY CLUSTERED ([ShippingCarrierID] ASC)
);

