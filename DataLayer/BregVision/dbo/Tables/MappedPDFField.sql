﻿CREATE TABLE [dbo].[MappedPDFField] (
    [MappedPDFFieldID] INT           IDENTITY (1, 1) NOT NULL,
    [MappedPDFID]      INT           NOT NULL,
    [DBFieldNameGet]   VARCHAR (100) NULL,
    [DBFieldNameSet]   VARCHAR (100) NULL,
    [DBFieldCode]      VARCHAR (50)  NULL,
    [DBFieldValue]     VARCHAR (200) NULL,
    [DBFieldDefault]   VARCHAR (200) NULL,
    [PDFFieldPath]     VARCHAR (200) NULL,
    [DBFieldRequired]  BIT           NOT NULL,
    [HTMLFormLabel]    VARCHAR (100) NULL,
    [HTMLFormType]     VARCHAR (100) NULL,
    [HTMLFormOrder]    VARCHAR (50)  NULL,
    [HTMLFormGroup]    VARCHAR (50)  NULL,
    [HTMLFormWidth]    INT           NULL,
    [IsActive]         BIT           NOT NULL,
    CONSTRAINT [PK_MappedPDFField] PRIMARY KEY CLUSTERED ([MappedPDFFieldID] ASC)
);

