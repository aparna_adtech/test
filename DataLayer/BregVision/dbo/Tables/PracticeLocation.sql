﻿CREATE TABLE [dbo].[PracticeLocation] (
    [PracticeLocationID]             INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PracticeID]                     INT           NOT NULL,
    [AddressID]                      INT           NULL,
    [ContactID]                      INT           NULL,
    [TimeZoneID]                     INT           NULL,
    [Name]                           VARCHAR (50)  NOT NULL,
    [IsPrimaryLocation]              BIT           NULL,
    [EmailForOrderApproval]          VARCHAR (200) NULL,
    [EmailForOrderConfirmation]      VARCHAR (200) NULL,
    [EmailForFaxConfirmation]        VARCHAR (200) NULL,
    [EmailForLowInventoryAlerts]     VARCHAR (200) NULL,
    [IsEmailForLowInventoryAlertsOn] BIT           CONSTRAINT [DF__PracticeL__IsEma__4F7CD00D] DEFAULT ((0)) NULL,
    [CreatedUserID]                  INT           NOT NULL,
    [CreatedDate]                    DATETIME      CONSTRAINT [DF__PracticeL__Creat__5070F446] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]                 INT           NULL,
    [ModifiedDate]                   DATETIME      NULL,
    [IsActive]                       BIT           NOT NULL,
    [EmailForBillingDispense]        VARCHAR (200) NULL,
    [EmailForPrintDispense]          VARCHAR (200) NULL,
    [FaxForPrintDispense]            VARCHAR (100) NULL,
    [GeneralLedgerNumber]            VARCHAR (50)  NULL,
    [IsHcpSignatureRequired]         BIT           CONSTRAINT [DF_PracticeLocation_IsHcpSignatureRequired] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PracticeLocation_PK] PRIMARY KEY CLUSTERED ([PracticeLocationID] ASC),
    CONSTRAINT [Address_PracticeLocation_FK1] FOREIGN KEY ([AddressID]) REFERENCES [dbo].[Address] ([AddressID]),
    CONSTRAINT [Contact_PracticeLocation_FK1] FOREIGN KEY ([ContactID]) REFERENCES [dbo].[Contact] ([ContactID]),
    CONSTRAINT [Practice_PracticeLocation_FK1] FOREIGN KEY ([PracticeID]) REFERENCES [dbo].[Practice] ([PracticeID])
);


GO
CREATE NONCLUSTERED INDEX [_dta_index_PracticeLocation_8_885578193__K2_K17_K1_6_7]
    ON [dbo].[PracticeLocation]([PracticeID] ASC, [IsActive] ASC, [PracticeLocationID] ASC)
    INCLUDE([Name], [IsPrimaryLocation]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_PracticeLocation_8_885578193__K2]
    ON [dbo].[PracticeLocation]([PracticeID] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key of the Practice Location table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeLocation', @level2type = N'COLUMN', @level2name = N'PracticeLocationID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the name of the clinic is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeLocation', @level2type = N'COLUMN', @level2name = N'Name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag to verify if the clinic named is the primary location.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeLocation', @level2type = N'COLUMN', @level2name = N'IsPrimaryLocation';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the e-mail for order approval is automatically generated and sent out', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeLocation', @level2type = N'COLUMN', @level2name = N'EmailForOrderApproval';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the e-mail for low inventory alert would automatically be generated and sent out', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeLocation', @level2type = N'COLUMN', @level2name = N'EmailForLowInventoryAlerts';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag to verify if that the e-mail being automatically sent out is for low inventory.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeLocation', @level2type = N'COLUMN', @level2name = N'IsEmailForLowInventoryAlertsOn';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in the practice location is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeLocation', @level2type = N'COLUMN', @level2name = N'IsActive';

