﻿CREATE TABLE [dbo].[z_Legacy Product Eligible 4 Logo] (
    [Base]                   NVARCHAR (255) NULL,
    [LogoPN]                 NVARCHAR (255) NULL,
    [BasePN]                 NVARCHAR (255) NULL,
    [BaseProductDescription] NVARCHAR (255) NULL,
    [ProductDescription]     NVARCHAR (255) NULL,
    [LogoPerProduct]         FLOAT (53)     NULL
);

