﻿CREATE TABLE [dbo].[SupplierOrderLineItemStatus] (
    [SupplierOrderLineItemStatusID] TINYINT      NOT NULL,
    [Status]                        VARCHAR (50) NOT NULL,
    CONSTRAINT [SupplierOrderLineItemStatus_PK] PRIMARY KEY CLUSTERED ([SupplierOrderLineItemStatusID] ASC)
);

