﻿CREATE TABLE [dbo].[PracticeCatalogProduct] (
    [PracticeCatalogProductID]       INT         IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PracticeID]                     INT         NOT NULL,
    [PracticeCatalogSupplierBrandID] INT         NOT NULL,
    [MasterCatalogProductID]         INT         NULL,
    [ThirdPartyProductID]            INT         NULL,
    [IsThirdPartyProduct]            BIT         NOT NULL,
    [WholesaleCost]                  SMALLMONEY  CONSTRAINT [DF_PracticeCatalogProduct_WholesaleCost] DEFAULT ((0)) NOT NULL,
    [BillingCharge]                  SMALLMONEY  CONSTRAINT [DF_PracticeCatalogProduct_BillingCharge] DEFAULT ((0)) NOT NULL,
    [DMEDeposit]                     SMALLMONEY  CONSTRAINT [DF_PracticeCatalogProduct_DMEDeposit] DEFAULT ((0)) NOT NULL,
    [BillingChargeCash]              SMALLMONEY  CONSTRAINT [DF_PracticeCatalogProduct_BillingChargeCash] DEFAULT ((0)) NOT NULL,
    [StockingUnits]                  INT         CONSTRAINT [DF_PracticeCatalogProduct_StockingUnits] DEFAULT ((1)) NOT NULL,
    [BuyingUnits]                    INT         CONSTRAINT [DF_PracticeCatalogProduct_BuyingUnits] DEFAULT ((1)) NOT NULL,
    [Sequence]                       INT         NULL,
    [CreatedUserID]                  INT         NOT NULL,
    [CreatedDate]                    DATETIME    CONSTRAINT [DF__PracticeC__Creat__6E01572D] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]                 INT         NULL,
    [ModifiedDate]                   DATETIME    NULL,
    [IsActive]                       BIT         CONSTRAINT [DF_PracticeCatalogProduct_IsActive] DEFAULT ((0)) NULL,
    [IsLogoAblePart]                 BIT         DEFAULT ((0)) NOT NULL,
    [IsSplitCode]                    BIT         CONSTRAINT [DF_PracticeCatalogProduct_IsSplitCode] DEFAULT ((0)) NOT NULL,
    [IsAlwaysOffTheShelf]            BIT         CONSTRAINT [DF_PracticeCatalogProduct_IsAlwaysOffTheShelf] DEFAULT ((0)) NOT NULL,
    [IsPreAuthorization]             BIT         CONSTRAINT [DF_PracticeCatalogProduct_IsPreAuthorization] DEFAULT ((0)) NOT NULL,
    [IsLateralityExempt]             BIT         CONSTRAINT [DF_PracticeCatalogProduct_IsLateralityExempt] DEFAULT ((0)) NULL,
	[Mod1]                           VARCHAR (2) NULL,
    [Mod2]                           VARCHAR (2) NULL,
    [Mod3]                           VARCHAR (2) NULL,
    [UsePCPMods]                     BIT         NULL,
    CONSTRAINT [PracticeCatalogProduct_PK] PRIMARY KEY CLUSTERED ([PracticeCatalogProductID] ASC),
    CONSTRAINT [MasterCatalogProduct_PracticeCatalogProduct_FK1] FOREIGN KEY ([MasterCatalogProductID]) REFERENCES [dbo].[MasterCatalogProduct] ([MasterCatalogProductID]),
    CONSTRAINT [Practice_PracticeCatalogProduct_FK1] FOREIGN KEY ([PracticeID]) REFERENCES [dbo].[Practice] ([PracticeID]),
    CONSTRAINT [PracticeCatalogSupplierBrand_PracticeCatalogProduct_FK1] FOREIGN KEY ([PracticeCatalogSupplierBrandID]) REFERENCES [dbo].[PracticeCatalogSupplierBrand] ([PracticeCatalogSupplierBrandID]),
    CONSTRAINT [ThirdPartyProduct_PracticeCatalogProduct_FK1] FOREIGN KEY ([ThirdPartyProductID]) REFERENCES [dbo].[ThirdPartyProduct] ([ThirdPartyProductID])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_973]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogSupplierBrandID] ASC, [IsActive] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_553]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogSupplierBrandID] ASC, [IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [WholesaleCost]);


GO
CREATE NONCLUSTERED INDEX [missing_index_908]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogSupplierBrandID] ASC, [IsActive] ASC)
    INCLUDE([PracticeCatalogProductID]);


GO
CREATE NONCLUSTERED INDEX [missing_index_503]
    ON [dbo].[PracticeCatalogProduct]([ThirdPartyProductID] ASC, [IsThirdPartyProduct] ASC, [IsActive] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_555]
    ON [dbo].[PracticeCatalogProduct]([IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [WholesaleCost]);


GO
CREATE NONCLUSTERED INDEX [missing_index_770]
    ON [dbo].[PracticeCatalogProduct]([IsThirdPartyProduct] ASC, [IsActive] ASC)
    INCLUDE([ThirdPartyProductID]);


GO
CREATE NONCLUSTERED INDEX [missing_index_691]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogSupplierBrandID] ASC, [IsThirdPartyProduct] ASC)
    INCLUDE([PracticeCatalogProductID], [MasterCatalogProductID], [WholesaleCost]);


GO
CREATE NONCLUSTERED INDEX [missing_index_425]
    ON [dbo].[PracticeCatalogProduct]([IsActive] ASC)
    INCLUDE([PracticeCatalogProductID]);


GO
CREATE NONCLUSTERED INDEX [missing_index_694]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogSupplierBrandID] ASC, [IsThirdPartyProduct] ASC)
    INCLUDE([PracticeCatalogProductID], [ThirdPartyProductID], [WholesaleCost]);


GO
CREATE NONCLUSTERED INDEX [missing_index_884]
    ON [dbo].[PracticeCatalogProduct]([PracticeID] ASC, [PracticeCatalogSupplierBrandID] ASC, [IsThirdPartyProduct] ASC, [IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [MasterCatalogProductID], [WholesaleCost], [BillingCharge], [DMEDeposit], [BillingChargeCash]);


GO
CREATE NONCLUSTERED INDEX [missing_index_887]
    ON [dbo].[PracticeCatalogProduct]([PracticeID] ASC, [PracticeCatalogSupplierBrandID] ASC, [IsThirdPartyProduct] ASC, [IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [ThirdPartyProductID], [WholesaleCost], [BillingCharge], [DMEDeposit], [BillingChargeCash]);


GO
CREATE NONCLUSTERED INDEX [missing_index_567]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogSupplierBrandID] ASC)
    INCLUDE([PracticeCatalogProductID], [MasterCatalogProductID], [WholesaleCost]);


GO
CREATE NONCLUSTERED INDEX [missing_index_569]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogSupplierBrandID] ASC)
    INCLUDE([PracticeCatalogProductID], [ThirdPartyProductID], [WholesaleCost]);


GO
CREATE NONCLUSTERED INDEX [missing_index_869]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogSupplierBrandID] ASC, [IsThirdPartyProduct] ASC, [IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [MasterCatalogProductID], [WholesaleCost]);


GO
CREATE NONCLUSTERED INDEX [missing_index_493]
    ON [dbo].[PracticeCatalogProduct]([MasterCatalogProductID] ASC, [IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID]);


GO
CREATE NONCLUSTERED INDEX [missing_index_467]
    ON [dbo].[PracticeCatalogProduct]([MasterCatalogProductID] ASC, [IsThirdPartyProduct] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [WholesaleCost]);


GO
CREATE NONCLUSTERED INDEX [missing_index_443]
    ON [dbo].[PracticeCatalogProduct]([IsThirdPartyProduct] ASC, [IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [ThirdPartyProductID]);


GO
CREATE NONCLUSTERED INDEX [missing_index_498]
    ON [dbo].[PracticeCatalogProduct]([ThirdPartyProductID] ASC, [IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID]);


GO
CREATE NONCLUSTERED INDEX [missing_index_571]
    ON [dbo].[PracticeCatalogProduct]([MasterCatalogProductID] ASC, [IsThirdPartyProduct] ASC, [IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [WholesaleCost]);


GO
CREATE NONCLUSTERED INDEX [missing_index_891]
    ON [dbo].[PracticeCatalogProduct]([ThirdPartyProductID] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [WholesaleCost]);


GO
CREATE NONCLUSTERED INDEX [missing_index_471]
    ON [dbo].[PracticeCatalogProduct]([ThirdPartyProductID] ASC, [IsThirdPartyProduct] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [WholesaleCost]);


GO
CREATE NONCLUSTERED INDEX [missing_index_563]
    ON [dbo].[PracticeCatalogProduct]([PracticeID] ASC, [ThirdPartyProductID] ASC, [IsThirdPartyProduct] ASC, [IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [WholesaleCost], [BillingCharge], [DMEDeposit], [BillingChargeCash]);


GO
CREATE NONCLUSTERED INDEX [missing_index_559]
    ON [dbo].[PracticeCatalogProduct]([PracticeID] ASC, [MasterCatalogProductID] ASC, [IsThirdPartyProduct] ASC, [IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [WholesaleCost], [BillingCharge], [DMEDeposit], [BillingChargeCash]);


GO
CREATE NONCLUSTERED INDEX [missing_index_683]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogSupplierBrandID] ASC, [IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [MasterCatalogProductID]);


GO
CREATE NONCLUSTERED INDEX [missing_index_878]
    ON [dbo].[PracticeCatalogProduct]([ThirdPartyProductID] ASC, [IsThirdPartyProduct] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID]);


GO
CREATE NONCLUSTERED INDEX [missing_index_579]
    ON [dbo].[PracticeCatalogProduct]([MasterCatalogProductID] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [WholesaleCost]);


GO
CREATE NONCLUSTERED INDEX [missing_index_648]
    ON [dbo].[PracticeCatalogProduct]([MasterCatalogProductID] ASC, [IsThirdPartyProduct] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID]);


GO
CREATE NONCLUSTERED INDEX [missing_index_574]
    ON [dbo].[PracticeCatalogProduct]([ThirdPartyProductID] ASC, [IsThirdPartyProduct] ASC, [IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [WholesaleCost]);


GO
CREATE NONCLUSTERED INDEX [missing_index_906]
    ON [dbo].[PracticeCatalogProduct]([IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID]);


GO
CREATE NONCLUSTERED INDEX [missing_index_686]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogSupplierBrandID] ASC, [IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [ThirdPartyProductID]);


GO
CREATE NONCLUSTERED INDEX [missing_index_450]
    ON [dbo].[PracticeCatalogProduct]([PracticeID] ASC, [IsThirdPartyProduct] ASC, [IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [MasterCatalogProductID], [WholesaleCost], [BillingCharge], [DMEDeposit], [BillingChargeCash]);


GO
CREATE NONCLUSTERED INDEX [missing_index_452]
    ON [dbo].[PracticeCatalogProduct]([PracticeID] ASC, [IsThirdPartyProduct] ASC, [IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [ThirdPartyProductID], [WholesaleCost], [BillingCharge], [DMEDeposit], [BillingChargeCash]);


GO
CREATE NONCLUSTERED INDEX [missing_index_478]
    ON [dbo].[PracticeCatalogProduct]([IsThirdPartyProduct] ASC, [IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [MasterCatalogProductID], [WholesaleCost]);


GO
CREATE NONCLUSTERED INDEX [missing_index_480]
    ON [dbo].[PracticeCatalogProduct]([IsThirdPartyProduct] ASC, [IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [ThirdPartyProductID], [WholesaleCost]);


GO
CREATE NONCLUSTERED INDEX [missing_index_460]
    ON [dbo].[PracticeCatalogProduct]([IsThirdPartyProduct] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [ThirdPartyProductID], [WholesaleCost]);


GO
CREATE NONCLUSTERED INDEX [missing_index_458]
    ON [dbo].[PracticeCatalogProduct]([IsThirdPartyProduct] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [MasterCatalogProductID], [WholesaleCost]);


GO
CREATE NONCLUSTERED INDEX [missing_index_441]
    ON [dbo].[PracticeCatalogProduct]([IsThirdPartyProduct] ASC, [IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [MasterCatalogProductID]);


GO
CREATE NONCLUSTERED INDEX [missing_index_496]
    ON [dbo].[PracticeCatalogProduct]([IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [MasterCatalogProductID]);


GO
CREATE NONCLUSTERED INDEX [missing_index_500]
    ON [dbo].[PracticeCatalogProduct]([IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [ThirdPartyProductID]);


GO
CREATE NONCLUSTERED INDEX [missing_index_651]
    ON [dbo].[PracticeCatalogProduct]([IsThirdPartyProduct] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [MasterCatalogProductID]);


GO
CREATE NONCLUSTERED INDEX [missing_index_653]
    ON [dbo].[PracticeCatalogProduct]([IsThirdPartyProduct] ASC)
    INCLUDE([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [ThirdPartyProductID]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_PracticeCatalogProduct_8_392388467__K2_K6_K18_K5_K1_K3_K7_K8_K9_K10_K11_K12_4]
    ON [dbo].[PracticeCatalogProduct]([PracticeID] ASC, [IsThirdPartyProduct] ASC, [IsActive] ASC, [ThirdPartyProductID] ASC, [PracticeCatalogProductID] ASC, [PracticeCatalogSupplierBrandID] ASC, [WholesaleCost] ASC, [BillingCharge] ASC, [DMEDeposit] ASC, [BillingChargeCash] ASC, [StockingUnits] ASC, [BuyingUnits] ASC)
    INCLUDE([MasterCatalogProductID]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_PracticeCatalogProduct_8_392388467__K18_K3_K4_K1_K5_7_11]
    ON [dbo].[PracticeCatalogProduct]([IsActive] ASC, [PracticeCatalogSupplierBrandID] ASC, [MasterCatalogProductID] ASC, [PracticeCatalogProductID] ASC, [ThirdPartyProductID] ASC)
    INCLUDE([WholesaleCost], [StockingUnits]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_PracticeCatalogProduct_12_392388467__K6_K18_K3_K1_K5_K7_K8_K10]
    ON [dbo].[PracticeCatalogProduct]([IsThirdPartyProduct] ASC, [IsActive] ASC, [PracticeCatalogSupplierBrandID] ASC, [PracticeCatalogProductID] ASC, [ThirdPartyProductID] ASC, [WholesaleCost] ASC, [BillingCharge] ASC, [BillingChargeCash] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_PracticeCatalogProduct_12_392388467__K6_K18_K3_K4_K1_K7_K8_K10]
    ON [dbo].[PracticeCatalogProduct]([IsThirdPartyProduct] ASC, [IsActive] ASC, [PracticeCatalogSupplierBrandID] ASC, [MasterCatalogProductID] ASC, [PracticeCatalogProductID] ASC, [WholesaleCost] ASC, [BillingCharge] ASC, [BillingChargeCash] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_PracticeCatalogProduct_12_392388467__K18_K4_K1_5_6_7_11]
    ON [dbo].[PracticeCatalogProduct]([IsActive] ASC, [MasterCatalogProductID] ASC, [PracticeCatalogProductID] ASC)
    INCLUDE([ThirdPartyProductID], [IsThirdPartyProduct], [WholesaleCost], [StockingUnits]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_PracticeCatalogProduct_12_392388467__K18_K5_K4_K3_K1_9]
    ON [dbo].[PracticeCatalogProduct]([IsActive] ASC, [ThirdPartyProductID] ASC, [MasterCatalogProductID] ASC, [PracticeCatalogSupplierBrandID] ASC, [PracticeCatalogProductID] ASC)
    INCLUDE([DMEDeposit]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_PracticeCatalogProduct_12_392388467__K18_K1_K3_K5_7_11]
    ON [dbo].[PracticeCatalogProduct]([IsActive] ASC, [PracticeCatalogProductID] ASC, [PracticeCatalogSupplierBrandID] ASC, [ThirdPartyProductID] ASC)
    INCLUDE([WholesaleCost], [StockingUnits]);


GO
CREATE STATISTICS [_dta_stat_392388467_1_5_4]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogProductID], [ThirdPartyProductID], [MasterCatalogProductID]);


GO
CREATE STATISTICS [_dta_stat_392388467_1_2_6]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogProductID], [PracticeID], [IsThirdPartyProduct]);


GO
CREATE STATISTICS [_dta_stat_392388467_4_3_2]
    ON [dbo].[PracticeCatalogProduct]([MasterCatalogProductID], [PracticeCatalogSupplierBrandID], [PracticeID]);


GO
CREATE STATISTICS [_dta_stat_392388467_6_3_4]
    ON [dbo].[PracticeCatalogProduct]([IsThirdPartyProduct], [PracticeCatalogSupplierBrandID], [MasterCatalogProductID]);


GO
CREATE STATISTICS [_dta_stat_392388467_1_3_2]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [PracticeID]);


GO
CREATE STATISTICS [_dta_stat_392388467_1_7_6_3]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogProductID], [WholesaleCost], [IsThirdPartyProduct], [PracticeCatalogSupplierBrandID]);


GO
CREATE STATISTICS [_dta_stat_392388467_18_5_3_4]
    ON [dbo].[PracticeCatalogProduct]([IsActive], [ThirdPartyProductID], [PracticeCatalogSupplierBrandID], [MasterCatalogProductID]);


GO
CREATE STATISTICS [_dta_stat_392388467_5_6_4_1]
    ON [dbo].[PracticeCatalogProduct]([ThirdPartyProductID], [IsThirdPartyProduct], [MasterCatalogProductID], [PracticeCatalogProductID]);


GO
CREATE STATISTICS [_dta_stat_392388467_5_18_4_1]
    ON [dbo].[PracticeCatalogProduct]([ThirdPartyProductID], [IsActive], [MasterCatalogProductID], [PracticeCatalogProductID]);


GO
CREATE STATISTICS [_dta_stat_392388467_1_7_18_6]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogProductID], [WholesaleCost], [IsActive], [IsThirdPartyProduct]);


GO
CREATE STATISTICS [_dta_stat_392388467_4_5_2_6]
    ON [dbo].[PracticeCatalogProduct]([MasterCatalogProductID], [ThirdPartyProductID], [PracticeID], [IsThirdPartyProduct]);


GO
CREATE STATISTICS [_dta_stat_392388467_3_4_1_5_6]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogSupplierBrandID], [MasterCatalogProductID], [PracticeCatalogProductID], [ThirdPartyProductID], [IsThirdPartyProduct]);


GO
CREATE STATISTICS [_dta_stat_392388467_3_4_1_2_6]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogSupplierBrandID], [MasterCatalogProductID], [PracticeCatalogProductID], [PracticeID], [IsThirdPartyProduct]);


GO
CREATE STATISTICS [_dta_stat_392388467_7_8_10_9_1]
    ON [dbo].[PracticeCatalogProduct]([WholesaleCost], [BillingCharge], [BillingChargeCash], [DMEDeposit], [PracticeCatalogProductID]);


GO
CREATE STATISTICS [_dta_stat_392388467_5_18_6_3_4]
    ON [dbo].[PracticeCatalogProduct]([ThirdPartyProductID], [IsActive], [IsThirdPartyProduct], [PracticeCatalogSupplierBrandID], [MasterCatalogProductID]);


GO
CREATE STATISTICS [_dta_stat_392388467_6_4_3_2_18]
    ON [dbo].[PracticeCatalogProduct]([IsThirdPartyProduct], [MasterCatalogProductID], [PracticeCatalogSupplierBrandID], [PracticeID], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_392388467_4_3_5_2_6]
    ON [dbo].[PracticeCatalogProduct]([MasterCatalogProductID], [PracticeCatalogSupplierBrandID], [ThirdPartyProductID], [PracticeID], [IsThirdPartyProduct]);


GO
CREATE STATISTICS [_dta_stat_392388467_4_18_3_1_5]
    ON [dbo].[PracticeCatalogProduct]([MasterCatalogProductID], [IsActive], [PracticeCatalogSupplierBrandID], [PracticeCatalogProductID], [ThirdPartyProductID]);


GO
CREATE STATISTICS [_dta_stat_392388467_3_1_5_2_6]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogSupplierBrandID], [PracticeCatalogProductID], [ThirdPartyProductID], [PracticeID], [IsThirdPartyProduct]);


GO
CREATE STATISTICS [_dta_stat_392388467_5_18_6_1_4]
    ON [dbo].[PracticeCatalogProduct]([ThirdPartyProductID], [IsActive], [IsThirdPartyProduct], [PracticeCatalogProductID], [MasterCatalogProductID]);


GO
CREATE STATISTICS [_dta_stat_392388467_3_5_2_6_18]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogSupplierBrandID], [ThirdPartyProductID], [PracticeID], [IsThirdPartyProduct], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_392388467_3_4_1_5_2_6]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogSupplierBrandID], [MasterCatalogProductID], [PracticeCatalogProductID], [ThirdPartyProductID], [PracticeID], [IsThirdPartyProduct]);


GO
CREATE STATISTICS [_dta_stat_392388467_1_6_4_3_5_7]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogProductID], [IsThirdPartyProduct], [MasterCatalogProductID], [PracticeCatalogSupplierBrandID], [ThirdPartyProductID], [WholesaleCost]);


GO
CREATE STATISTICS [_dta_stat_392388467_4_18_6_3_1_5]
    ON [dbo].[PracticeCatalogProduct]([MasterCatalogProductID], [IsActive], [IsThirdPartyProduct], [PracticeCatalogSupplierBrandID], [PracticeCatalogProductID], [ThirdPartyProductID]);


GO
CREATE STATISTICS [_dta_stat_392388467_3_18_6_4_2_5]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogSupplierBrandID], [IsActive], [IsThirdPartyProduct], [MasterCatalogProductID], [PracticeID], [ThirdPartyProductID]);


GO
CREATE STATISTICS [_dta_stat_392388467_1_3_5_7_8_10_9]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [ThirdPartyProductID], [WholesaleCost], [BillingCharge], [BillingChargeCash], [DMEDeposit]);


GO
CREATE STATISTICS [_dta_stat_392388467_18_6_4_1_3_5_7]
    ON [dbo].[PracticeCatalogProduct]([IsActive], [IsThirdPartyProduct], [MasterCatalogProductID], [PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [ThirdPartyProductID], [WholesaleCost]);


GO
CREATE STATISTICS [_dta_stat_392388467_1_3_4_7_8_10_9]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [MasterCatalogProductID], [WholesaleCost], [BillingCharge], [BillingChargeCash], [DMEDeposit]);


GO
CREATE STATISTICS [_dta_stat_392388467_4_1_3_5_7_8_10_9]
    ON [dbo].[PracticeCatalogProduct]([MasterCatalogProductID], [PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [ThirdPartyProductID], [WholesaleCost], [BillingCharge], [BillingChargeCash], [DMEDeposit]);


GO
CREATE STATISTICS [_dta_stat_392388467_2_1_5_3_7_8_9_10_18]
    ON [dbo].[PracticeCatalogProduct]([PracticeID], [PracticeCatalogProductID], [ThirdPartyProductID], [PracticeCatalogSupplierBrandID], [WholesaleCost], [BillingCharge], [DMEDeposit], [BillingChargeCash], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_392388467_2_1_4_3_7_8_9_10_18]
    ON [dbo].[PracticeCatalogProduct]([PracticeID], [PracticeCatalogProductID], [MasterCatalogProductID], [PracticeCatalogSupplierBrandID], [WholesaleCost], [BillingCharge], [DMEDeposit], [BillingChargeCash], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_392388467_1_11_12_3_2_6_18_5_7_8_9]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogProductID], [StockingUnits], [BuyingUnits], [PracticeCatalogSupplierBrandID], [PracticeID], [IsThirdPartyProduct], [IsActive], [ThirdPartyProductID], [WholesaleCost], [BillingCharge], [DMEDeposit]);


GO
CREATE STATISTICS [_dta_stat_392388467_2_1_4_3_7_8_9_10_11_12_18]
    ON [dbo].[PracticeCatalogProduct]([PracticeID], [PracticeCatalogProductID], [MasterCatalogProductID], [PracticeCatalogSupplierBrandID], [WholesaleCost], [BillingCharge], [DMEDeposit], [BillingChargeCash], [StockingUnits], [BuyingUnits], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_392388467_2_1_5_3_7_8_9_10_11_12_18]
    ON [dbo].[PracticeCatalogProduct]([PracticeID], [PracticeCatalogProductID], [ThirdPartyProductID], [PracticeCatalogSupplierBrandID], [WholesaleCost], [BillingCharge], [DMEDeposit], [BillingChargeCash], [StockingUnits], [BuyingUnits], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_392388467_18_6_5_4_2_1_3_7_8_9_10_11]
    ON [dbo].[PracticeCatalogProduct]([IsActive], [IsThirdPartyProduct], [ThirdPartyProductID], [MasterCatalogProductID], [PracticeID], [PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [WholesaleCost], [BillingCharge], [DMEDeposit], [BillingChargeCash], [StockingUnits]);


GO
CREATE STATISTICS [_dta_stat_392388467_5_2_6_18_1_3_7_8_9_10_11_12]
    ON [dbo].[PracticeCatalogProduct]([ThirdPartyProductID], [PracticeID], [IsThirdPartyProduct], [IsActive], [PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [WholesaleCost], [BillingCharge], [DMEDeposit], [BillingChargeCash], [StockingUnits], [BuyingUnits]);


GO
CREATE STATISTICS [_dta_stat_392388467_2_1_4_5_3_7_8_9_10_11_12_18]
    ON [dbo].[PracticeCatalogProduct]([PracticeID], [PracticeCatalogProductID], [MasterCatalogProductID], [ThirdPartyProductID], [PracticeCatalogSupplierBrandID], [WholesaleCost], [BillingCharge], [DMEDeposit], [BillingChargeCash], [StockingUnits], [BuyingUnits], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_392388467_2_18_6_1_4_3_7_8_9_10_11_12_5]
    ON [dbo].[PracticeCatalogProduct]([PracticeID], [IsActive], [IsThirdPartyProduct], [PracticeCatalogProductID], [MasterCatalogProductID], [PracticeCatalogSupplierBrandID], [WholesaleCost], [BillingCharge], [DMEDeposit], [BillingChargeCash], [StockingUnits], [BuyingUnits], [ThirdPartyProductID]);


GO
CREATE STATISTICS [_dta_stat_392388467_10_18]
    ON [dbo].[PracticeCatalogProduct]([BillingChargeCash], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_392388467_2_1_18]
    ON [dbo].[PracticeCatalogProduct]([PracticeID], [PracticeCatalogProductID], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_392388467_3_2_18]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogSupplierBrandID], [PracticeID], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_392388467_1_10_18]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogProductID], [BillingChargeCash], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_392388467_5_2_18]
    ON [dbo].[PracticeCatalogProduct]([ThirdPartyProductID], [PracticeID], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_392388467_4_2_18]
    ON [dbo].[PracticeCatalogProduct]([MasterCatalogProductID], [PracticeID], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_392388467_1_11_3]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogProductID], [StockingUnits], [PracticeCatalogSupplierBrandID]);


GO
CREATE STATISTICS [_dta_stat_392388467_2_1_4_18]
    ON [dbo].[PracticeCatalogProduct]([PracticeID], [PracticeCatalogProductID], [MasterCatalogProductID], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_392388467_7_8_10_18]
    ON [dbo].[PracticeCatalogProduct]([WholesaleCost], [BillingCharge], [BillingChargeCash], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_392388467_1_10_3_18]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogProductID], [BillingChargeCash], [PracticeCatalogSupplierBrandID], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_392388467_6_5_3_4]
    ON [dbo].[PracticeCatalogProduct]([IsThirdPartyProduct], [ThirdPartyProductID], [PracticeCatalogSupplierBrandID], [MasterCatalogProductID]);


GO
CREATE STATISTICS [_dta_stat_392388467_18_4_3_2]
    ON [dbo].[PracticeCatalogProduct]([IsActive], [MasterCatalogProductID], [PracticeCatalogSupplierBrandID], [PracticeID]);


GO
CREATE STATISTICS [_dta_stat_392388467_18_4_3_1_2]
    ON [dbo].[PracticeCatalogProduct]([IsActive], [MasterCatalogProductID], [PracticeCatalogSupplierBrandID], [PracticeCatalogProductID], [PracticeID]);


GO
CREATE STATISTICS [_dta_stat_392388467_18_1_3_5_10]
    ON [dbo].[PracticeCatalogProduct]([IsActive], [PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [ThirdPartyProductID], [BillingChargeCash]);


GO
CREATE STATISTICS [_dta_stat_392388467_1_5_9_18_3]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogProductID], [ThirdPartyProductID], [DMEDeposit], [IsActive], [PracticeCatalogSupplierBrandID]);


GO
CREATE STATISTICS [_dta_stat_392388467_1_3_4_5_9]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [MasterCatalogProductID], [ThirdPartyProductID], [DMEDeposit]);


GO
CREATE STATISTICS [_dta_stat_392388467_1_3_5_6_18_11]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [ThirdPartyProductID], [IsThirdPartyProduct], [IsActive], [StockingUnits]);


GO
CREATE STATISTICS [_dta_stat_392388467_3_18_1_4_5_9]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogSupplierBrandID], [IsActive], [PracticeCatalogProductID], [MasterCatalogProductID], [ThirdPartyProductID], [DMEDeposit]);


GO
CREATE STATISTICS [_dta_stat_392388467_1_3_6_11_18_4]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogProductID], [PracticeCatalogSupplierBrandID], [IsThirdPartyProduct], [StockingUnits], [IsActive], [MasterCatalogProductID]);


GO
CREATE STATISTICS [_dta_stat_392388467_1_4_3_18_10_5]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogProductID], [MasterCatalogProductID], [PracticeCatalogSupplierBrandID], [IsActive], [BillingChargeCash], [ThirdPartyProductID]);


GO
CREATE STATISTICS [_dta_stat_392388467_7_8_10_6_18_1]
    ON [dbo].[PracticeCatalogProduct]([WholesaleCost], [BillingCharge], [BillingChargeCash], [IsThirdPartyProduct], [IsActive], [PracticeCatalogProductID]);


GO
CREATE STATISTICS [_dta_stat_392388467_7_18_6_8_10_5]
    ON [dbo].[PracticeCatalogProduct]([WholesaleCost], [IsActive], [IsThirdPartyProduct], [BillingCharge], [BillingChargeCash], [ThirdPartyProductID]);


GO
CREATE STATISTICS [_dta_stat_392388467_18_6_1_5_3_4_11]
    ON [dbo].[PracticeCatalogProduct]([IsActive], [IsThirdPartyProduct], [PracticeCatalogProductID], [ThirdPartyProductID], [PracticeCatalogSupplierBrandID], [MasterCatalogProductID], [StockingUnits]);


GO
CREATE STATISTICS [_dta_stat_392388467_8_10_3_6_18_5_7]
    ON [dbo].[PracticeCatalogProduct]([BillingCharge], [BillingChargeCash], [PracticeCatalogSupplierBrandID], [IsThirdPartyProduct], [IsActive], [ThirdPartyProductID], [WholesaleCost]);


GO
CREATE STATISTICS [_dta_stat_392388467_3_1_5_6_18_7_8]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogSupplierBrandID], [PracticeCatalogProductID], [ThirdPartyProductID], [IsThirdPartyProduct], [IsActive], [WholesaleCost], [BillingCharge]);


GO
CREATE STATISTICS [_dta_stat_392388467_1_8_10_3_6_18_5_7]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogProductID], [BillingCharge], [BillingChargeCash], [PracticeCatalogSupplierBrandID], [IsThirdPartyProduct], [IsActive], [ThirdPartyProductID], [WholesaleCost]);


GO
CREATE STATISTICS [_dta_stat_392388467_1_18_6_3_4_5_7_8_10]
    ON [dbo].[PracticeCatalogProduct]([PracticeCatalogProductID], [IsActive], [IsThirdPartyProduct], [PracticeCatalogSupplierBrandID], [MasterCatalogProductID], [ThirdPartyProductID], [WholesaleCost], [BillingCharge], [BillingChargeCash]);


GO
CREATE TRIGGER [dbo].[trg_PracticeCatalogProduct_Inserted] ON [dbo].[PracticeCatalogProduct]
    FOR INSERT
AS
    begin

        INSERT INTO
            dbo.PracticeCatalogProduct_Audit
            (    
                 PracticeCatalogProductID
			   , PracticeID
			   , PracticeCatalogSupplierBrandID
			   , MasterCatalogProductID
			   , ThirdPartyProductID
			   , IsThirdPartyProduct
			   , WholesaleCost
			   , BillingCharge
			   , DMEDeposit
			   , BillingChargeCash
			   , StockingUnits
			   , BuyingUnits
			   , Sequence
			   , CreatedUserID
			   , CreatedDate
			   , ModifiedUserID
			   , ModifiedDate
			   , IsActive
			   , AuditAction			   
            )
            SELECT
				 PracticeCatalogProductID
			   , PracticeID
			   , PracticeCatalogSupplierBrandID
			   , MasterCatalogProductID
			   , ThirdPartyProductID
			   , IsThirdPartyProduct
			   , WholesaleCost
			   , BillingCharge
			   , DMEDeposit
			   , BillingChargeCash
			   , StockingUnits
			   , BuyingUnits
			   , Sequence
			   , CreatedUserID
			   , CreatedDate
			   , ModifiedUserID
			   , ModifiedDate
			   , IsActive
               , 'Inserted'
            FROM
                inserted
  
    END

GO
/*
	--  dbo.[trg_PracticeCatalogProduct_Deleted] ON dbo.[PracticeCatalogProduct]
	-- Trigger
	John Bongiorni  
	2008.02.25
	
	Trigger fires on Delete from PracticeCatalogProduct
	and inserts the deleted record into
	PracticeCatalogProduct_Audit table.
*/
CREATE TRIGGER [dbo].[trg_PracticeCatalogProduct_Deleted] ON [dbo].[PracticeCatalogProduct]
    FOR DELETE
AS
    begin

        INSERT INTO
            dbo.PracticeCatalogProduct_Audit
            (    
                 PracticeCatalogProductID
			   , PracticeID
			   , PracticeCatalogSupplierBrandID
			   , MasterCatalogProductID
			   , ThirdPartyProductID
			   , IsThirdPartyProduct
			   , WholesaleCost
			   , BillingCharge
			   , DMEDeposit
			   , BillingChargeCash
			   , StockingUnits
			   , BuyingUnits
			   , Sequence
			   , CreatedUserID
			   , CreatedDate
			   , ModifiedUserID
			   , ModifiedDate
			   , IsActive
			   , AuditAction			   
            )
            SELECT
				 PracticeCatalogProductID
			   , PracticeID
			   , PracticeCatalogSupplierBrandID
			   , MasterCatalogProductID
			   , ThirdPartyProductID
			   , IsThirdPartyProduct
			   , WholesaleCost
			   , BillingCharge
			   , DMEDeposit
			   , BillingChargeCash
			   , StockingUnits
			   , BuyingUnits
			   , Sequence
			   , CreatedUserID
			   , CreatedDate
			   , ModifiedUserID
			   , ModifiedDate
			   , IsActive
               , 'Deleted'
            FROM
                deleted
  
    END

GO
CREATE TRIGGER [dbo].[trg_PracticeCatalogProduct_Updated] ON [dbo].[PracticeCatalogProduct]
    FOR UPDATE
AS
    begin

        INSERT INTO
            dbo.PracticeCatalogProduct_Audit
            (    
                 PracticeCatalogProductID
			   , PracticeID
			   , PracticeCatalogSupplierBrandID
			   , MasterCatalogProductID
			   , ThirdPartyProductID
			   , IsThirdPartyProduct
			   , WholesaleCost
			   , BillingCharge
			   , DMEDeposit
			   , BillingChargeCash
			   , StockingUnits
			   , BuyingUnits
			   , Sequence
			   , CreatedUserID
			   , CreatedDate
			   , ModifiedUserID
			   , ModifiedDate
			   , IsActive
			   , AuditAction			   
            )
            SELECT
				 PracticeCatalogProductID
			   , PracticeID
			   , PracticeCatalogSupplierBrandID
			   , MasterCatalogProductID
			   , ThirdPartyProductID
			   , IsThirdPartyProduct
			   , WholesaleCost
			   , BillingCharge
			   , DMEDeposit
			   , BillingChargeCash
			   , StockingUnits
			   , BuyingUnits
			   , Sequence
			   , CreatedUserID
			   , CreatedDate
			   , ModifiedUserID
			   , ModifiedDate
			   , IsActive
               , 'Update(OLD)'
            FROM
                deleted;
                
        INSERT INTO
            dbo.PracticeCatalogProduct_Audit
            (    
                 PracticeCatalogProductID
			   , PracticeID
			   , PracticeCatalogSupplierBrandID
			   , MasterCatalogProductID
			   , ThirdPartyProductID
			   , IsThirdPartyProduct
			   , WholesaleCost
			   , BillingCharge
			   , DMEDeposit
			   , BillingChargeCash
			   , StockingUnits
			   , BuyingUnits
			   , Sequence
			   , CreatedUserID
			   , CreatedDate
			   , ModifiedUserID
			   , ModifiedDate
			   , IsActive
			   , AuditAction			   
            )
            SELECT
				 PracticeCatalogProductID
			   , PracticeID
			   , PracticeCatalogSupplierBrandID
			   , MasterCatalogProductID
			   , ThirdPartyProductID
			   , IsThirdPartyProduct
			   , WholesaleCost
			   , BillingCharge
			   , DMEDeposit
			   , BillingChargeCash
			   , StockingUnits
			   , BuyingUnits
			   , Sequence
			   , CreatedUserID
			   , CreatedDate
			   , ModifiedUserID
			   , ModifiedDate
			   , IsActive
               , 'Update(NEW)'
            FROM
                inserted;
  
    END

GO

CREATE TRIGGER [dbo].[trg_SyncModifiers_PracticeCatalogProduct]
ON [dbo].[PracticeCatalogProduct] FOR UPDATE

AS 
	SET NOCOUNT ON 
	DECLARE @ThirdPartyProductId int 
	DECLARE @Mod1 as VARCHAR(2) 
	DECLARE @Mod2 as VARCHAR(2)
	DECLARE @Mod3 as VARCHAR(2)
	DECLARE @tppMod1 as VARCHAR(2) 
	DECLARE @tppMod2 as VARCHAR(2)
	DECLARE @tppMod3 as VARCHAR(2)
	
	Select
		@ThirdPartyProductId = i.ThirdPartyProductID,
		@Mod1 = i.Mod1,
		@Mod2 = i.Mod2,
		@Mod3 = i.Mod3,
		@tppMod1 = dbo.ThirdPartyProduct.Mod1,
		@tppMod2 = dbo.ThirdPartyProduct.Mod2,
		@tppMod3 = dbo.ThirdPartyProduct.Mod3
	FROM inserted as i
	inner join ThirdPartyProduct on i.ThirdPartyProductID = ThirdPartyProduct.ThirdPartyProductID

	If @Mod1 <> @tppMod1 or @Mod2 <> @tppMod2 or @Mod3 <> @tppMod3
	BEGIN
		update dbo.ThirdPartyProduct 
		set Mod1 = @Mod1, Mod2 = @Mod2, Mod3 = @Mod3
		WHERE ThirdPartyProductID = @ThirdPartyProductId
	END

GO



CREATE TRIGGER [dbo].[UpdateProductInventoryChangeLogForPracticeCatalog]
ON [dbo].[PracticeCatalogProduct] FOR INSERT, UPDATE

AS 
	SET NOCOUNT ON
	DECLARE @PracticeCatalogProductId as int
	DECLARE @PracticeId as int
	
	Select
		@PracticeId = INSERTED.PracticeID,
		@PracticeCatalogProductId = INSERTED.PracticeCatalogProductId
	FROM inserted

	insert ProductChangeLog (PracticeId, PracticeCatalogProductId)
	values(@PracticeId, @PracticeCatalogProductId)

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the Practice Catalog Product table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeCatalogProduct', @level2type = N'COLUMN', @level2name = N'PracticeCatalogProductID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if the product comes from a third-party.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeCatalogProduct', @level2type = N'COLUMN', @level2name = N'IsThirdPartyProduct';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the expected pay price is entered into.  ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeCatalogProduct', @level2type = N'COLUMN', @level2name = N'WholesaleCost';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the product price charged to insurance companies is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeCatalogProduct', @level2type = N'COLUMN', @level2name = N'BillingCharge';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the product price charged to the non-insured consumer is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeCatalogProduct', @level2type = N'COLUMN', @level2name = N'BillingChargeCash';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field to define the order in which the practice''s catalog of products will be displayed. ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeCatalogProduct', @level2type = N'COLUMN', @level2name = N'Sequence';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record of the practice''s product catalog is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeCatalogProduct', @level2type = N'COLUMN', @level2name = N'IsActive';

