﻿CREATE TABLE [dbo].[TMPcart_transferlog] (
    [TMPcart_transferlog_id]   BIGINT           IDENTITY (1, 1) NOT NULL,
    [transferID]               UNIQUEIDENTIFIER NULL,
    [practiceID]               INT              NULL,
    [fromPracticeLocationID]   INT              NULL,
    [toPracticeLocationID]     INT              NULL,
    [userID]                   INT              NULL,
    [counter]                  INT              NULL,
    [grandTotalQuantity]       INT              NULL,
    [totalQuantity]            INT              NULL,
    [practiceCatalogProductID] BIGINT           NULL,
    [shoppingCartItemID]       BIGINT           NULL,
    [quantity]                 INT              NULL,
    [actualWholesaleCost]      DECIMAL (18, 2)  NULL,
    [EntryAddDate]             DATETIME         CONSTRAINT [DF_TMPcart_transferlog_EntryAddDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_TMPcart_transferlog] PRIMARY KEY CLUSTERED ([TMPcart_transferlog_id] ASC)
);

