﻿CREATE TABLE [dbo].[SupplierOrderLineItem] (
    [SupplierOrderLineItemID]       INT        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [SupplierOrderID]               INT        NOT NULL,
    [PracticeCatalogProductID]      INT        NOT NULL,
    [SupplierOrderLineItemStatusID] TINYINT    NULL,
    [ActualWholesaleCost]           SMALLMONEY NOT NULL,
    [QuantityOrdered]               INT        NOT NULL,
    [LineTotal]                     SMALLMONEY NOT NULL,
    [CreatedUserID]                 INT        NOT NULL,
    [CreatedDate]                   DATETIME   CONSTRAINT [DF__SupplierO__Creat__628FA481] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]                INT        NULL,
    [ModifiedDate]                  DATETIME   NULL,
    [IsActive]                      BIT        NOT NULL,
    [IsLogoPart]                    BIT        DEFAULT ((0)) NOT NULL,
    CONSTRAINT [SupplierOrderLineItem_PK] PRIMARY KEY CLUSTERED ([SupplierOrderLineItemID] ASC),
    CONSTRAINT [PracticeCatalogProduct_SupplierOrderLineItem_FK1] FOREIGN KEY ([PracticeCatalogProductID]) REFERENCES [dbo].[PracticeCatalogProduct] ([PracticeCatalogProductID]),
    CONSTRAINT [SupplierOrder_SupplierOrderLineItem_FK1] FOREIGN KEY ([SupplierOrderID]) REFERENCES [dbo].[SupplierOrder] ([SupplierOrderID]),
    CONSTRAINT [SupplierOrderLineItemStatus_SupplierOrderLineItem_FK1] FOREIGN KEY ([SupplierOrderLineItemStatusID]) REFERENCES [dbo].[SupplierOrderLineItemStatus] ([SupplierOrderLineItemStatusID])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_490]
    ON [dbo].[SupplierOrderLineItem]([SupplierOrderID] ASC, [SupplierOrderLineItemStatusID] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_821]
    ON [dbo].[SupplierOrderLineItem]([PracticeCatalogProductID] ASC, [IsActive] ASC, [SupplierOrderLineItemStatusID] ASC)
    INCLUDE([SupplierOrderID]);


GO
CREATE NONCLUSTERED INDEX [missing_index_637]
    ON [dbo].[SupplierOrderLineItem]([PracticeCatalogProductID] ASC, [IsActive] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_632]
    ON [dbo].[SupplierOrderLineItem]([PracticeCatalogProductID] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_534]
    ON [dbo].[SupplierOrderLineItem]([SupplierOrderID] ASC, [SupplierOrderLineItemStatusID] ASC, [IsActive] ASC)
    INCLUDE([LineTotal]);


GO
CREATE NONCLUSTERED INDEX [missing_index_823]
    ON [dbo].[SupplierOrderLineItem]([IsActive] ASC, [SupplierOrderLineItemStatusID] ASC)
    INCLUDE([SupplierOrderID], [PracticeCatalogProductID]);


GO
CREATE NONCLUSTERED INDEX [missing_index_536]
    ON [dbo].[SupplierOrderLineItem]([SupplierOrderLineItemStatusID] ASC, [IsActive] ASC)
    INCLUDE([SupplierOrderID], [LineTotal]);


GO
CREATE NONCLUSTERED INDEX [missing_index_740]
    ON [dbo].[SupplierOrderLineItem]([SupplierOrderID] ASC, [SupplierOrderLineItemStatusID] ASC, [IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [QuantityOrdered]);


GO
CREATE NONCLUSTERED INDEX [missing_index_977]
    ON [dbo].[SupplierOrderLineItem]([SupplierOrderID] ASC, [SupplierOrderLineItemStatusID] ASC, [IsActive] ASC)
    INCLUDE([SupplierOrderLineItemID], [PracticeCatalogProductID], [QuantityOrdered]);


GO
CREATE NONCLUSTERED INDEX [missing_index_538]
    ON [dbo].[SupplierOrderLineItem]([SupplierOrderID] ASC, [IsActive] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_482]
    ON [dbo].[SupplierOrderLineItem]([SupplierOrderID] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_757]
    ON [dbo].[SupplierOrderLineItem]([SupplierOrderID] ASC, [IsActive] ASC)
    INCLUDE([SupplierOrderLineItemID], [PracticeCatalogProductID], [SupplierOrderLineItemStatusID], [ActualWholesaleCost], [QuantityOrdered]);


GO
CREATE NONCLUSTERED INDEX [missing_index_434]
    ON [dbo].[SupplierOrderLineItem]([SupplierOrderLineItemStatusID] ASC, [IsActive] ASC)
    INCLUDE([SupplierOrderLineItemID], [SupplierOrderID], [PracticeCatalogProductID], [QuantityOrdered]);


GO
CREATE NONCLUSTERED INDEX [missing_index_541]
    ON [dbo].[SupplierOrderLineItem]([IsActive] ASC)
    INCLUDE([SupplierOrderID], [PracticeCatalogProductID], [ActualWholesaleCost], [QuantityOrdered], [LineTotal]);


GO
CREATE NONCLUSTERED INDEX [missing_index_742]
    ON [dbo].[SupplierOrderLineItem]([SupplierOrderLineItemStatusID] ASC, [IsActive] ASC)
    INCLUDE([SupplierOrderID], [PracticeCatalogProductID], [QuantityOrdered]);


GO
CREATE STATISTICS [_dta_stat_1717581157_12_1]
    ON [dbo].[SupplierOrderLineItem]([IsActive], [SupplierOrderLineItemID]);


GO
CREATE STATISTICS [_dta_stat_1717581157_4_1]
    ON [dbo].[SupplierOrderLineItem]([SupplierOrderLineItemStatusID], [SupplierOrderLineItemID]);


GO
CREATE STATISTICS [_dta_stat_1717581157_2_3]
    ON [dbo].[SupplierOrderLineItem]([SupplierOrderID], [PracticeCatalogProductID]);


GO
CREATE STATISTICS [_dta_stat_1717581157_1_3_4]
    ON [dbo].[SupplierOrderLineItem]([SupplierOrderLineItemID], [PracticeCatalogProductID], [SupplierOrderLineItemStatusID]);


GO
CREATE STATISTICS [_dta_stat_1717581157_3_4_2]
    ON [dbo].[SupplierOrderLineItem]([PracticeCatalogProductID], [SupplierOrderLineItemStatusID], [SupplierOrderID]);


GO
CREATE STATISTICS [_dta_stat_1717581157_5_6_7_2]
    ON [dbo].[SupplierOrderLineItem]([ActualWholesaleCost], [QuantityOrdered], [LineTotal], [SupplierOrderID]);


GO
CREATE STATISTICS [_dta_stat_1717581157_1_3_5_6_2]
    ON [dbo].[SupplierOrderLineItem]([SupplierOrderLineItemID], [PracticeCatalogProductID], [ActualWholesaleCost], [QuantityOrdered], [SupplierOrderID]);


GO
CREATE STATISTICS [_dta_stat_1717581157_5_6_2_12_7]
    ON [dbo].[SupplierOrderLineItem]([ActualWholesaleCost], [QuantityOrdered], [SupplierOrderID], [IsActive], [LineTotal]);


GO
CREATE STATISTICS [_dta_stat_1717581157_2_5_6_7_1]
    ON [dbo].[SupplierOrderLineItem]([SupplierOrderID], [ActualWholesaleCost], [QuantityOrdered], [LineTotal], [SupplierOrderLineItemID]);


GO
CREATE STATISTICS [_dta_stat_1717581157_5_6_1_2_4_7]
    ON [dbo].[SupplierOrderLineItem]([ActualWholesaleCost], [QuantityOrdered], [SupplierOrderLineItemID], [SupplierOrderID], [SupplierOrderLineItemStatusID], [LineTotal]);


GO
CREATE STATISTICS [_dta_stat_1717581157_1_7_2_12_3_5]
    ON [dbo].[SupplierOrderLineItem]([SupplierOrderLineItemID], [LineTotal], [SupplierOrderID], [IsActive], [PracticeCatalogProductID], [ActualWholesaleCost]);


GO
CREATE STATISTICS [_dta_stat_1717581157_2_1_4_7_3_5]
    ON [dbo].[SupplierOrderLineItem]([SupplierOrderID], [SupplierOrderLineItemID], [SupplierOrderLineItemStatusID], [LineTotal], [PracticeCatalogProductID], [ActualWholesaleCost]);


GO
CREATE STATISTICS [_dta_stat_1717581157_2_1_3_4_5_6_7]
    ON [dbo].[SupplierOrderLineItem]([SupplierOrderID], [SupplierOrderLineItemID], [PracticeCatalogProductID], [SupplierOrderLineItemStatusID], [ActualWholesaleCost], [QuantityOrdered], [LineTotal]);


GO
CREATE STATISTICS [_dta_stat_1717581157_1_3_2_12_5_6_7]
    ON [dbo].[SupplierOrderLineItem]([SupplierOrderLineItemID], [PracticeCatalogProductID], [SupplierOrderID], [IsActive], [ActualWholesaleCost], [QuantityOrdered], [LineTotal]);


GO

CREATE TRIGGER trg_SupplierOrderLineItem_Deleted 
ON [dbo].[SupplierOrderLineItem] 
FOR DELETE 

AS
BEGIN

	INSERT INTO SupplierOrderLineItem_Audit
	(
	[SupplierOrderLineItemID],
    [SupplierOrderID],
	[PracticeCatalogProductID],
	[SupplierOrderLineItemStatusID],
	[ActualWholesaleCost],
	[QuantityOrdered],
	[LineTotal],
	[CreatedUserID],
	[CreatedDate],
	[ModifiedUserID],
	[ModifiedDate],
	[IsActive],
	TriggerName
	)
	SELECT
		[SupplierOrderLineItemID],
		[SupplierOrderID],
		[PracticeCatalogProductID],
		[SupplierOrderLineItemStatusID],
		[ActualWholesaleCost],
		[QuantityOrdered],
		[LineTotal],
		[CreatedUserID],
		[CreatedDate],
		[ModifiedUserID],
		[ModifiedDate],
		[IsActive],
		'Deleted'
	FROM Deleted

END
GO

CREATE TRIGGER trg_SupplierOrderLineItem_Updated 
ON [dbo].[SupplierOrderLineItem] 
FOR UPDATE 

AS
BEGIN

	INSERT INTO SupplierOrderLineItem_Audit
	(
	[SupplierOrderLineItemID],
    [SupplierOrderID],
	[PracticeCatalogProductID],
	[SupplierOrderLineItemStatusID],
	[ActualWholesaleCost],
	[QuantityOrdered],
	[LineTotal],
	[CreatedUserID],
	[CreatedDate],
	[ModifiedUserID],
	[ModifiedDate],
	[IsActive],
	TriggerName
	)
	SELECT
		[SupplierOrderLineItemID],
		[SupplierOrderID],
		[PracticeCatalogProductID],
		[SupplierOrderLineItemStatusID],
		[ActualWholesaleCost],
		[QuantityOrdered],
		[LineTotal],
		[CreatedUserID],
		[CreatedDate],
		[ModifiedUserID],
		[ModifiedDate],
		[IsActive],
		'Updated OLD'
	FROM Deleted


	INSERT INTO SupplierOrderLineItem_Audit
	(
	[SupplierOrderLineItemID],
    [SupplierOrderID],
	[PracticeCatalogProductID],
	[SupplierOrderLineItemStatusID],
	[ActualWholesaleCost],
	[QuantityOrdered],
	[LineTotal],
	[CreatedUserID],
	[CreatedDate],
	[ModifiedUserID],
	[ModifiedDate],
	[IsActive],
	TriggerName
	)
	SELECT
		[SupplierOrderLineItemID],
		[SupplierOrderID],
		[PracticeCatalogProductID],
		[SupplierOrderLineItemStatusID],
		[ActualWholesaleCost],
		[QuantityOrdered],
		[LineTotal],
		[CreatedUserID],
		[CreatedDate],
		[ModifiedUserID],
		[ModifiedDate],
		[IsActive],
		'Updated NEW'
	FROM Inserted


END
GO

CREATE TRIGGER [dbo].[trg_SupplierOrderLineItem_Inserted] 
ON [dbo].[SupplierOrderLineItem] 
FOR INSERT 

AS
BEGIN

	INSERT INTO SupplierOrderLineItem_Audit
	(
	[SupplierOrderLineItemID],
    [SupplierOrderID],
	[PracticeCatalogProductID],
	[SupplierOrderLineItemStatusID],
	[ActualWholesaleCost],
	[QuantityOrdered],
	[LineTotal],
	[CreatedUserID],
	[CreatedDate],
	[ModifiedUserID],
	[ModifiedDate],
	[IsActive],
	TriggerName
	)
	SELECT
		[SupplierOrderLineItemID],
		[SupplierOrderID],
		[PracticeCatalogProductID],
		[SupplierOrderLineItemStatusID],
		[ActualWholesaleCost],
		[QuantityOrdered],
		[LineTotal],
		[CreatedUserID],
		[CreatedDate],
		[ModifiedUserID],
		[ModifiedDate],
		[IsActive],
		'Inserted'
	FROM Inserted

END
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the Supplier Order Line Item table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SupplierOrderLineItem', @level2type = N'COLUMN', @level2name = N'SupplierOrderLineItemID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the clinic purchase price, per product unit, is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SupplierOrderLineItem', @level2type = N'COLUMN', @level2name = N'ActualWholesaleCost';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the quantity of a product being ordered is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SupplierOrderLineItem', @level2type = N'COLUMN', @level2name = N'QuantityOrdered';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the total purchase price of a quantity of a product (line) is entered into.  This line total is equal to wholesale cost X quantity ordered.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SupplierOrderLineItem', @level2type = N'COLUMN', @level2name = N'LineTotal';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in the Supplier Order Line Item is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SupplierOrderLineItem', @level2type = N'COLUMN', @level2name = N'IsActive';

