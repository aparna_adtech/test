﻿CREATE TABLE [dbo].[xxxModifierLoadData07312016] (
    [Foot and Ankle] NVARCHAR (255)   NULL,
    [HCPC]           NVARCHAR (255)   NULL,
    [Modifier]       NVARCHAR (255)   NULL,
    [Product #]      NVARCHAR (255)   NULL,
    [Mod1]           NVARCHAR (255)   NULL,
    [Mod2]           NVARCHAR (255)   NULL,
    [ID]             UNIQUEIDENTIFIER CONSTRAINT [DF_xxxModifierLoadData07312016_ID] DEFAULT (newid()) NOT NULL,
    [Mod3]           NVARCHAR (255)   NULL
);

