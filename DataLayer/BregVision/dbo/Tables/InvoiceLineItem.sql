﻿CREATE TABLE [dbo].[InvoiceLineItem] (
    [InvoiceLineItemID]       INT           IDENTITY (1, 1) NOT NULL,
    [InvoiceID]               INT           NOT NULL,
    [SupplierOrderLineItemID] INT           NOT NULL,
    [Quantity]                INT           NOT NULL,
    [WholesaleCost]           SMALLMONEY    NOT NULL,
    [ProductName]             VARCHAR (100) NOT NULL,
    [ProductCode]             VARCHAR (50)  NOT NULL,
    [IsActive]                BIT           CONSTRAINT [DF_InvoiceLineItem_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedDate]             DATETIME      CONSTRAINT [DF_InvoiceLineItem_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [CreatedUserID]           INT           NOT NULL,
    CONSTRAINT [PK_InvoiceLineItem] PRIMARY KEY CLUSTERED ([InvoiceLineItemID] ASC),
    CONSTRAINT [FK_InvoiceLineItem_Invoice] FOREIGN KEY ([InvoiceID]) REFERENCES [dbo].[Invoice] ([InvoiceID]),
    CONSTRAINT [FK_InvoiceLineItem_SupplierOrderLineItem] FOREIGN KEY ([SupplierOrderLineItemID]) REFERENCES [dbo].[SupplierOrderLineItem] ([SupplierOrderLineItemID])
);


GO
CREATE NONCLUSTERED INDEX [FK_SuppOrderLineItem]
    ON [dbo].[InvoiceLineItem]([SupplierOrderLineItemID] ASC)
    INCLUDE([InvoiceID], [Quantity]);

