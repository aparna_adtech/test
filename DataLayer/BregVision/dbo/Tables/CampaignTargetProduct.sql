﻿CREATE TABLE [dbo].[CampaignTargetProduct] (
    [CampaignId]             INT NOT NULL,
    [MasterCatalogProductId] INT NOT NULL,
    CONSTRAINT [PK_CampaignTargetProduct_1] PRIMARY KEY CLUSTERED ([CampaignId] ASC, [MasterCatalogProductId] ASC),
    CONSTRAINT [FK_CampaignTargetProduct_Campaign] FOREIGN KEY ([CampaignId]) REFERENCES [dbo].[Campaign] ([Id]),
    CONSTRAINT [FK_CampaignTargetProduct_MasterCatalogProduct] FOREIGN KEY ([MasterCatalogProductId]) REFERENCES [dbo].[MasterCatalogProduct] ([MasterCatalogProductID])
);

