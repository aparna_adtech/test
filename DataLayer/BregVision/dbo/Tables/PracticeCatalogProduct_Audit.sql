﻿CREATE TABLE [dbo].[PracticeCatalogProduct_Audit] (
    [PracticeCatalogProduct_AuditID] INT          IDENTITY (1, 1) NOT NULL,
    [PracticeCatalogProductID]       INT          NOT NULL,
    [PracticeID]                     INT          NOT NULL,
    [PracticeCatalogSupplierBrandID] INT          NOT NULL,
    [MasterCatalogProductID]         INT          NULL,
    [ThirdPartyProductID]            INT          NULL,
    [IsThirdPartyProduct]            BIT          NULL,
    [WholesaleCost]                  SMALLMONEY   NULL,
    [BillingCharge]                  SMALLMONEY   NULL,
    [DMEDeposit]                     SMALLMONEY   NULL,
    [BillingChargeCash]              SMALLMONEY   NULL,
    [StockingUnits]                  INT          NULL,
    [BuyingUnits]                    INT          NULL,
    [Sequence]                       INT          NULL,
    [CreatedUserID]                  INT          NULL,
    [CreatedDate]                    DATETIME     NULL,
    [ModifiedUserID]                 INT          NULL,
    [ModifiedDate]                   DATETIME     NULL,
    [IsActive]                       BIT          NULL,
    [AuditAction]                    VARCHAR (50) NULL,
    [AuditCreatedDate]               DATETIME     CONSTRAINT [DF__PracticeC__Audit__2E7BCEF5] DEFAULT (getdate()) NULL
);

