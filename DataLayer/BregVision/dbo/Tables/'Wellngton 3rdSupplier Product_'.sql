﻿CREATE TABLE [dbo].['Wellngton 3rdSupplier Product$'] (
    [Practice]                       NVARCHAR (255) NULL,
    [Supplier]                       NVARCHAR (255) NULL,
    [IsVendor]                       NVARCHAR (255) NULL,
    [PracticeCatalogSupplierBrandID] INT            NULL,
    [BrandName]                      NVARCHAR (255) NULL,
    [BrandDisplayName]               NVARCHAR (255) NULL,
    [ProductCode]                    NVARCHAR (255) NULL,
    [ProductName]                    NVARCHAR (255) NULL,
    [ProductShortName]               NVARCHAR (255) NULL,
    [Packaging]                      NVARCHAR (255) NULL,
    [LeftRightSide]                  NVARCHAR (255) NULL,
    [Size]                           NVARCHAR (255) NULL,
    [Color]                          NVARCHAR (255) NULL,
    [Gender]                         NVARCHAR (255) NULL,
    [WholesaleListCost]              SMALLMONEY     NULL,
    [Description]                    NVARCHAR (255) NULL
);

