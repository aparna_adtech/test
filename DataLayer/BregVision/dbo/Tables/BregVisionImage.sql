﻿CREATE TABLE [dbo].[BregVisionImage] (
    [ImageID]   INT             IDENTITY (1, 1) NOT NULL,
    [Image]     VARBINARY (MAX) NULL,
    [ImagePath] VARCHAR (200)   NULL,
    [ImageName] VARCHAR (50)    NULL
);

