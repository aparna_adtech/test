﻿CREATE TABLE [dbo].[ProductReview] (
    [Id]                     INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MasterCatalogProductID] INT            NOT NULL,
    [Pros]                   NVARCHAR (MAX) NULL,
    [Cons]                   NVARCHAR (MAX) NULL,
    [Thoughts]               NVARCHAR (MAX) NULL,
    [StarRating]             INT            CONSTRAINT [DF_ProductReview_Rating] DEFAULT ((0)) NOT NULL,
    [UserId]                 INT            NOT NULL,
    [Created]                DATETIME       CONSTRAINT [DF_ProductReview_Created] DEFAULT (getdate()) NOT NULL,
    [SortOrder]              INT            CONSTRAINT [DF_ProductReview_SortOrder] DEFAULT ((0)) NOT NULL,
    [IsActive]               BIT            CONSTRAINT [DF_ProductReview_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ProductReview] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ProductReview_MasterCatalogProduct] FOREIGN KEY ([MasterCatalogProductID]) REFERENCES [dbo].[MasterCatalogProduct] ([MasterCatalogProductID]),
    CONSTRAINT [FK_ProductReview_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserID])
);

