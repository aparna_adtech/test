﻿CREATE TABLE [dbo].[PracticeCatalogCategory] (
    [PracticeCatalogCategoryID] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PracticeCatalogProductID]  INT          NULL,
    [PracticeID]                INT          NOT NULL,
    [Name]                      VARCHAR (50) NOT NULL,
    [Sequence]                  INT          NULL,
    [CreatedUserID]             INT          NOT NULL,
    [CreatedDate]               DATETIME     CONSTRAINT [DF__PracticeC__Creat__6B24EA82] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]            INT          NULL,
    [ModifiedDate]              DATETIME     NULL,
    [IsActive]                  BIT          NOT NULL,
    CONSTRAINT [PracticeCatalogCategory_PK] PRIMARY KEY CLUSTERED ([PracticeCatalogCategoryID] ASC),
    CONSTRAINT [Practice_PracticeCatalogCategory_FK1] FOREIGN KEY ([PracticeID]) REFERENCES [dbo].[Practice] ([PracticeID]),
    CONSTRAINT [PracticeCatalogProduct_PracticeCatalogCategory_FK1] FOREIGN KEY ([PracticeCatalogProductID]) REFERENCES [dbo].[PracticeCatalogProduct] ([PracticeCatalogProductID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the Practice Catalog Category table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeCatalogCategory', @level2type = N'COLUMN', @level2name = N'PracticeCatalogCategoryID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the product category name is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeCatalogCategory', @level2type = N'COLUMN', @level2name = N'Name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field to define the order in which the practice''s catalog of categories will be displayed. ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeCatalogCategory', @level2type = N'COLUMN', @level2name = N'Sequence';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record of the practice''s category catalog is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeCatalogCategory', @level2type = N'COLUMN', @level2name = N'IsActive';

