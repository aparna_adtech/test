﻿CREATE TABLE [dbo].[SuperBillCategory] (
    [SuperBillCategoryID] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PracticeLocationID]  INT          NOT NULL,
    [Name]                VARCHAR (50) NOT NULL,
    [Sequence]            INT          NULL,
    [ColumnNumber]        INT          NULL,
    [CreatedUserID]       INT          NOT NULL,
    [CreatedDate]         DATETIME     NOT NULL,
    [ModifiedUserID]      INT          NULL,
    [ModifiedDate]        DATETIME     NULL,
    [IsActive]            BIT          CONSTRAINT [DF_SuperBillCategory_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_SuperBillCategory] PRIMARY KEY CLUSTERED ([SuperBillCategoryID] ASC),
    CONSTRAINT [FK_SuperBillCategory_PracticeLocation] FOREIGN KEY ([PracticeLocationID]) REFERENCES [dbo].[PracticeLocation] ([PracticeLocationID])
);

