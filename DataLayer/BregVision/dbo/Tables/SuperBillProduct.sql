﻿CREATE TABLE [dbo].[SuperBillProduct] (
    [SuperBillProductID]       INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PracticeCatalogProductID] INT          NOT NULL,
    [PracticeLocationID]       INT          NOT NULL,
    [SuperBillCategoryID]      INT          NOT NULL,
    [Name]                     VARCHAR (50) NULL,
    [HCPCs]                    VARCHAR (10) NULL,
    [Deposit]                  SMALLMONEY   NULL,
    [Cost]                     SMALLMONEY   NULL,
    [Charge]                   SMALLMONEY   NULL,
    [FlowsheetOnly]            BIT          CONSTRAINT [DF_SuperBillProduct_FlowsheetOnly] DEFAULT ((0)) NOT NULL,
    [Sequence]                 INT          NULL,
    [CreateUserID]             INT          NOT NULL,
    [CreatedDate]              DATETIME     NOT NULL,
    [ModifiedUserID]           INT          NULL,
    [ModifiedDate]             DATETIME     NULL,
    [IsActive]                 BIT          NULL,
    [IsAddOn]                  BIT          CONSTRAINT [DF_SuperBillProduct_IsAddOn] DEFAULT ((0)) NOT NULL,
    [SuperBillProductID_FK]    INT          NULL,
    CONSTRAINT [PK_SuperBillProduct] PRIMARY KEY CLUSTERED ([SuperBillProductID] ASC),
    CONSTRAINT [FK_SuperBillProduct_PracticeCatalogProduct] FOREIGN KEY ([PracticeCatalogProductID]) REFERENCES [dbo].[PracticeCatalogProduct] ([PracticeCatalogProductID]),
    CONSTRAINT [FK_SuperBillProduct_PracticeLocation] FOREIGN KEY ([PracticeLocationID]) REFERENCES [dbo].[PracticeLocation] ([PracticeLocationID]),
    CONSTRAINT [FK_SuperBillProduct_SuperBillCategory] FOREIGN KEY ([SuperBillCategoryID]) REFERENCES [dbo].[SuperBillCategory] ([SuperBillCategoryID]),
    CONSTRAINT [FK_SuperBillProduct_SuperBillProduct] FOREIGN KEY ([SuperBillProductID_FK]) REFERENCES [dbo].[SuperBillProduct] ([SuperBillProductID])
);


GO
CREATE NONCLUSTERED INDEX [IX_SuperBillProduct]
    ON [dbo].[SuperBillProduct]([PracticeCatalogProductID] ASC, [PracticeLocationID] ASC);

