﻿CREATE TABLE [dbo].[ShoppingCartItem_Updated] (
    [ShoppingCartItem_UpdatedID] INT          IDENTITY (1, 1) NOT NULL,
    [Action]                     VARCHAR (50) NULL,
    [ShoppingCartItemID]         INT          NULL,
    [ShoppingCartID]             INT          NULL,
    [PracticeCatalogProductID]   INT          NULL,
    [ShoppingCartItemStatusID]   TINYINT      NULL,
    [Quantity]                   INT          NULL,
    [ActualWholesaleCost]        SMALLMONEY   NULL,
    [CreatedUserID]              INT          NULL,
    [CreatedDate]                DATETIME     NULL,
    [ModifiedUserID]             INT          NULL,
    [ModifiedDate]               DATETIME     NULL,
    [IsActive]                   BIT          NULL,
    [RecordCreatedDate]          DATETIME     CONSTRAINT [DF_ShoppingCartItem_Updated_RecordCreatedDate] DEFAULT (getdate()) NULL
);

