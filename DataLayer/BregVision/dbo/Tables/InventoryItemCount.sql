﻿CREATE TABLE [dbo].[InventoryItemCount] (
    [InventoryItemCountID] INT              IDENTITY (1, 1) NOT NULL,
    [InventoryCycleID]     INT              NOT NULL,
    [ProductInventoryID]   INT              NOT NULL,
    [Count]                INT              NOT NULL,
    [CountDate]            DATETIME         NOT NULL,
    [DeviceIdentifier]     VARCHAR (255)    NOT NULL,
    [UserID]               INT              NOT NULL,
    [CreatedUserID]        INT              NOT NULL,
    [CreatedDate]          DATETIME         CONSTRAINT [DF_InventoryItemCount_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]       INT              NULL,
    [ModifiedDate]         DATETIME         NULL,
    [IsActive]             BIT              CONSTRAINT [DF_InventoryItemCount_IsActive] DEFAULT ((1)) NOT NULL,
    [MembershipUserID]     UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_InventoryItemCount] PRIMARY KEY CLUSTERED ([InventoryItemCountID] ASC),
    CONSTRAINT [FK_InventoryItemCount_InventoryItemCount] FOREIGN KEY ([InventoryCycleID]) REFERENCES [dbo].[InventoryCycle] ([InventoryCycleID]),
    CONSTRAINT [FK_InventoryItemCount_ProductInventory] FOREIGN KEY ([ProductInventoryID]) REFERENCES [dbo].[ProductInventory] ([ProductInventoryID])
);

