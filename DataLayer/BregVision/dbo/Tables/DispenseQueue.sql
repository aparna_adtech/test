﻿CREATE TABLE [dbo].[DispenseQueue] (
    [DispenseQueueID]     INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PracticeLocationID]  INT           NOT NULL,
    [ProductInventoryID]  INT           NOT NULL,
    [PatientCode]         VARCHAR (255) NULL,
    [PhysicianID]         INT           NULL,
    [ICD9_Code]           VARCHAR (50)  NULL,
    [IsMedicare]          BIT           CONSTRAINT [DF_DispenseQueue_IsMedicare] DEFAULT ((0)) NOT NULL,
    [Side]                VARCHAR (2)   NULL,
    [DMEDeposit]          MONEY         CONSTRAINT [DF_DispenseQueue_DMEDeposit] DEFAULT ((0)) NOT NULL,
    [QuantityToDispense]  INT           CONSTRAINT [DF_DispenseQueue_QuantityToDispense] DEFAULT ((1)) NOT NULL,
    [DispenseDate]        DATETIME      NULL,
    [IsCashCollection]    BIT           CONSTRAINT [DF_DispenseQueue_IsCashCollection] DEFAULT ((0)) NOT NULL,
    [IsDispensed]         BIT           CONSTRAINT [DF_DispenseQueue_IsDispensed] DEFAULT ((0)) NOT NULL,
    [BregVisionOrderID]   INT           NULL,
    [CreatedUser]         INT           CONSTRAINT [DF_DispenseQueue_CreatedUser] DEFAULT ((1)) NOT NULL,
    [CreatedDate]         DATETIME      CONSTRAINT [DF_DispenseQueue_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [ModifiedUser]        INT           CONSTRAINT [DF_DispenseQueue_ModifiedUser] DEFAULT ((1)) NULL,
    [ModifiedDate]        DATETIME      CONSTRAINT [DF_DispenseQueue_ModifiedDate] DEFAULT (getdate()) NULL,
    [IsActive]            BIT           CONSTRAINT [DF_DispenseQueue_IsActive] DEFAULT ((1)) NOT NULL,
    [ABNReason]           VARCHAR (500) NULL,
    [DispenseSignatureID] INT           NULL,
    [PatientEmail]        VARCHAR (50)  NULL,
    [IsRental]            BIT           DEFAULT ((0)) NOT NULL,
    [DeclineReason]       VARCHAR (50)  NULL,
    [MedicareOption]      INT           DEFAULT ((0)) NOT NULL,
    [PatientFirstName]    VARCHAR (50)  NULL,
    [PatientLastName]     VARCHAR (50)  NULL,
    [Note]                VARCHAR (500) NULL,
    [PracticePayerID]     INT           NULL,
    [MedicalRecordNumber] NCHAR (10)    NULL,
    [IsCustomFit]         BIT           NULL,
    [FitterID]            INT           NULL,
    [IsCustomFabricated]  BIT           NULL,
    [HCPCs]               VARCHAR (50)  NULL,
    [DeviceIdentifier]    VARCHAR (255) NULL,
    [DispenseIdentifier]  VARCHAR (255) NULL,
    [ABNType]             VARCHAR (50)  NULL,
    [Mod1]                VARCHAR (2)   NULL,
    [Mod2]                VARCHAR (2)   NULL,
    [Mod3]                VARCHAR (2)   NULL,
    [QueuedReason]        VARCHAR (50)  NULL,
    [ABNForm]             BIT           DEFAULT ((0)) NOT NULL,
    [DispenseGroupingId]  NVARCHAR (36) CONSTRAINT [DF_DispenseQueue_DispenseGroupingId] DEFAULT (NULL) NULL,
    [PreAuthorized]       BIT           DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_DispenseQueue] PRIMARY KEY CLUSTERED ([DispenseQueueID] ASC),
    CONSTRAINT [FK_DispenseQueue_PracticePayer] FOREIGN KEY ([PracticePayerID]) REFERENCES [dbo].[PracticePayer] ([PracticePayerID])
);


GO
CREATE NONCLUSTERED INDEX [IX_DispenseQueue_GetDispenseQueue]
    ON [dbo].[DispenseQueue]([PracticeLocationID] ASC, [IsActive] ASC, [IsDispensed] ASC)
    INCLUDE([DispenseQueueID], [ProductInventoryID], [PatientCode], [PhysicianID], [ICD9_Code], [IsMedicare], [ABNForm], [Side], [DMEDeposit], [QuantityToDispense], [DispenseDate], [CreatedDate], [DispenseSignatureID], [PatientEmail], [IsRental]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_DispenseQueue_12_1316915763__K2_K3]
    ON [dbo].[DispenseQueue]([PracticeLocationID] ASC, [ProductInventoryID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_DispenseQueue_BregVisionOrderID]
    ON [dbo].[DispenseQueue]([BregVisionOrderID] ASC)
    INCLUDE([ProductInventoryID]);


GO
CREATE NONCLUSTERED INDEX [IX_DispenseQueue_DispenseSignatureID]
    ON [dbo].[DispenseQueue]([DispenseSignatureID] ASC)
    INCLUDE([DispenseQueueID], [PracticeLocationID]);


GO
CREATE NONCLUSTERED INDEX [IX_DispenseQueue_PracticeLocationID_DeclineReason]
    ON [dbo].[DispenseQueue]([PracticeLocationID] ASC, [DeclineReason] ASC)
    INCLUDE([DispenseQueueID], [PatientCode], [PhysicianID], [DispenseDate], [DispenseSignatureID], [PatientFirstName], [PatientLastName], [DispenseIdentifier], [DispenseGroupingId]);


GO
CREATE NONCLUSTERED INDEX [IX_DispenseQueue_DeviceIdentifier_DispenseIdentifier]
    ON [dbo].[DispenseQueue]([DeviceIdentifier] ASC, [DispenseIdentifier] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_DispenseQueue_DispenseGroupingId]
    ON [dbo].[DispenseQueue]([DispenseGroupingId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_DispenseQueue_IsActive_CreatedDate]
    ON [dbo].[DispenseQueue]([IsActive] ASC, [CreatedDate] ASC)
    INCLUDE([ProductInventoryID], [DispenseGroupingId]);


GO




CREATE TRIGGER [dbo].[trg_DispenseQueue_Deleted] 
ON [dbo].[DispenseQueue] 
FOR DELETE 
AS
BEGIN

	INSERT INTO [DispenseQueue_Audit]
(      
	   [DispenseQueueID]
      ,[PracticeLocationID]
      ,[ProductInventoryID]
      ,[PatientCode]
      ,[PhysicianID]
      ,[ICD9_Code]
      ,[IsMedicare]
      ,[ABNForm]
      ,[Side]
      ,[DMEDeposit]
      ,[QuantityToDispense]
      ,[DispenseDate]
      ,[IsCashCollection]
      ,[IsDispensed]
      ,[BregVisionOrderID]
      ,[CreatedUser]
      ,[CreatedDate]
      ,[ModifiedUser]
      ,[ModifiedDate]
      ,[IsActive]
      ,[ABNReason]
      ,[DispenseSignatureID]
      ,[PatientEmail]
      ,[IsRental]
      ,[DeclineReason]
      ,[MedicareOption]
      ,[PatientFirstName]
      ,[PatientLastName]
      ,[Note]
      ,[PracticePayerID]
      ,[MedicalRecordNumber]
	  ,[AuditAction]
	  )
	select
	   [DispenseQueueID]
      ,[PracticeLocationID]
      ,[ProductInventoryID]
      ,[PatientCode]
      ,[PhysicianID]
      ,[ICD9_Code]
      ,[IsMedicare]
      ,[ABNForm]
      ,[Side]
      ,[DMEDeposit]
      ,[QuantityToDispense]
      ,[DispenseDate]
      ,[IsCashCollection]
      ,[IsDispensed]
      ,[BregVisionOrderID]
      ,[CreatedUser]
      ,[CreatedDate]
      ,[ModifiedUser]
      ,[ModifiedDate]
      ,[IsActive]
      ,[ABNReason]
      ,[DispenseSignatureID]
      ,[PatientEmail]
      ,[IsRental]
      ,[DeclineReason]
      ,[MedicareOption]
      ,[PatientFirstName]
      ,[PatientLastName]
      ,[Note]
      ,[PracticePayerID]
      ,[MedicalRecordNumber]
	  ,'Deleted'
  FROM deleted
END

GO


CREATE TRIGGER [dbo].[trg_DispenseQueue_Inserted] 
ON [dbo].[DispenseQueue] 
FOR INSERT 
AS
BEGIN

	INSERT INTO [DispenseQueue_Audit]
(      
	   [DispenseQueueID]
      ,[PracticeLocationID]
      ,[ProductInventoryID]
      ,[PatientCode]
      ,[PhysicianID]
      ,[ICD9_Code]
      ,[IsMedicare]
      ,[ABNForm]
      ,[Side]
      ,[DMEDeposit]
      ,[QuantityToDispense]
      ,[DispenseDate]
      ,[IsCashCollection]
      ,[IsDispensed]
      ,[BregVisionOrderID]
      ,[CreatedUser]
      ,[CreatedDate]
      ,[ModifiedUser]
      ,[ModifiedDate]
      ,[IsActive]
      ,[ABNReason]
      ,[DispenseSignatureID]
      ,[PatientEmail]
      ,[IsRental]
      ,[DeclineReason]
      ,[MedicareOption]
      ,[PatientFirstName]
      ,[PatientLastName]
      ,[Note]
      ,[PracticePayerID]
      ,[MedicalRecordNumber]
      ,[IsCustomFit]
      ,[FitterID]
      ,[IsCustomFabricated]
      ,[HCPCs]
      ,[DeviceIdentifier]
      ,[DispenseIdentifier]

	  ,[AuditAction]
	  )
	select
		[DispenseQueueID]
      ,[PracticeLocationID]
      ,[ProductInventoryID]
      ,[PatientCode]
      ,[PhysicianID]
      ,[ICD9_Code]
      ,[IsMedicare]
      ,[ABNForm]
      ,[Side]
      ,[DMEDeposit]
      ,[QuantityToDispense]
      ,[DispenseDate]
      ,[IsCashCollection]
      ,[IsDispensed]
      ,[BregVisionOrderID]
      ,[CreatedUser]
      ,[CreatedDate]
      ,[ModifiedUser]
      ,[ModifiedDate]
      ,[IsActive]
      ,[ABNReason]
      ,[DispenseSignatureID]
      ,[PatientEmail]
      ,[IsRental]
      ,[DeclineReason]
      ,[MedicareOption]
      ,[PatientFirstName]
      ,[PatientLastName]
      ,[Note]
      ,[PracticePayerID]
      ,[MedicalRecordNumber]
      ,[IsCustomFit]
      ,[FitterID]
      ,[IsCustomFabricated]
      ,[HCPCs]
      ,[DeviceIdentifier]
      ,[DispenseIdentifier]
	  ,'Inserted'
  FROM inserted
END

GO



CREATE TRIGGER [dbo].[trg_DispenseQueue_Updated] 
ON [dbo].[DispenseQueue] 
FOR UPDATE 
AS
BEGIN

	INSERT INTO [DispenseQueue_Audit]
(      
	   [DispenseQueueID]
      ,[PracticeLocationID]
      ,[ProductInventoryID]
      ,[PatientCode]
      ,[PhysicianID]
      ,[ICD9_Code]
      ,[IsMedicare]
      ,[ABNForm]
      ,[Side]
      ,[DMEDeposit]
      ,[QuantityToDispense]
      ,[DispenseDate]
      ,[IsCashCollection]
      ,[IsDispensed]
      ,[BregVisionOrderID]
      ,[CreatedUser]
      ,[CreatedDate]
      ,[ModifiedUser]
      ,[ModifiedDate]
      ,[IsActive]
      ,[ABNReason]
      ,[DispenseSignatureID]
      ,[PatientEmail]
      ,[IsRental]
      ,[DeclineReason]
      ,[MedicareOption]
      ,[PatientFirstName]
      ,[PatientLastName]
      ,[Note]
      ,[PracticePayerID]
      ,[MedicalRecordNumber]
      ,[IsCustomFit]
      ,[FitterID]
      ,[IsCustomFabricated]
      ,[HCPCs]
      ,[DeviceIdentifier]
      ,[DispenseIdentifier]

	  ,[AuditAction]
	  )
	select
		[DispenseQueueID]
      ,[PracticeLocationID]
      ,[ProductInventoryID]
      ,[PatientCode]
      ,[PhysicianID]
      ,[ICD9_Code]
      ,[IsMedicare]
      ,[ABNForm]
      ,[Side]
      ,[DMEDeposit]
      ,[QuantityToDispense]
      ,[DispenseDate]
      ,[IsCashCollection]
      ,[IsDispensed]
      ,[BregVisionOrderID]
      ,[CreatedUser]
      ,[CreatedDate]
      ,[ModifiedUser]
      ,[ModifiedDate]
      ,[IsActive]
      ,[ABNReason]
      ,[DispenseSignatureID]
      ,[PatientEmail]
      ,[IsRental]
      ,[DeclineReason]
      ,[MedicareOption]
      ,[PatientFirstName]
      ,[PatientLastName]
      ,[Note]
      ,[PracticePayerID]
      ,[MedicalRecordNumber]
      ,[IsCustomFit]
      ,[FitterID]
      ,[IsCustomFabricated]
      ,[HCPCs]
      ,[DeviceIdentifier]
      ,[DispenseIdentifier]
	  ,'Update(OLD)'
  FROM deleted;

	INSERT INTO [DispenseQueue_Audit]
(      
	   [DispenseQueueID]
      ,[PracticeLocationID]
      ,[ProductInventoryID]
      ,[PatientCode]
      ,[PhysicianID]
      ,[ICD9_Code]
      ,[IsMedicare]
      ,[ABNForm]
      ,[Side]
      ,[DMEDeposit]
      ,[QuantityToDispense]
      ,[DispenseDate]
      ,[IsCashCollection]
      ,[IsDispensed]
      ,[BregVisionOrderID]
      ,[CreatedUser]
      ,[CreatedDate]
      ,[ModifiedUser]
      ,[ModifiedDate]
      ,[IsActive]
      ,[ABNReason]
      ,[DispenseSignatureID]
      ,[PatientEmail]
      ,[IsRental]
      ,[DeclineReason]
      ,[MedicareOption]
      ,[PatientFirstName]
      ,[PatientLastName]
      ,[Note]
      ,[PracticePayerID]
      ,[MedicalRecordNumber]
      ,[IsCustomFit]
      ,[FitterID]
      ,[IsCustomFabricated]
      ,[HCPCs]
      ,[DeviceIdentifier]
      ,[DispenseIdentifier]
	  ,[AuditAction]
	  )
	select
		[DispenseQueueID]
      ,[PracticeLocationID]
      ,[ProductInventoryID]
      ,[PatientCode]
      ,[PhysicianID]
      ,[ICD9_Code]
      ,[IsMedicare]
      ,[ABNForm]
      ,[Side]
      ,[DMEDeposit]
      ,[QuantityToDispense]
      ,[DispenseDate]
      ,[IsCashCollection]
      ,[IsDispensed]
      ,[BregVisionOrderID]
      ,[CreatedUser]
      ,[CreatedDate]
      ,[ModifiedUser]
      ,[ModifiedDate]
      ,[IsActive]
      ,[ABNReason]
      ,[DispenseSignatureID]
      ,[PatientEmail]
      ,[IsRental]
      ,[DeclineReason]
      ,[MedicareOption]
      ,[PatientFirstName]
      ,[PatientLastName]
      ,[Note]
      ,[PracticePayerID]
      ,[MedicalRecordNumber]
      ,[IsCustomFit]
      ,[FitterID]
      ,[IsCustomFabricated]
      ,[HCPCs]
      ,[DeviceIdentifier]
      ,[DispenseIdentifier]
	  ,'Update(NEW)'
  FROM inserted;
  END
