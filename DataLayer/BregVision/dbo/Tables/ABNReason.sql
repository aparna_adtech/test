﻿CREATE TABLE [dbo].[ABNReason] (
    [ABNReasonID]    INT           IDENTITY (1, 1) NOT NULL,
    [ABNReasonText]  VARCHAR (MAX) NOT NULL,
    [SortOrder]      INT           NOT NULL,
    [CreatedUserID]  INT           DEFAULT ((1)) NOT NULL,
    [CreatedDate]    DATETIME      DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID] INT           NULL,
    [ModifiedDate]   DATETIME      NULL,
    [IsActive]       BIT           DEFAULT ((1)) NOT NULL,
    [timestamp]      ROWVERSION    NOT NULL,
    CONSTRAINT [PK_ABNReason] PRIMARY KEY CLUSTERED ([ABNReasonID] ASC)
);

