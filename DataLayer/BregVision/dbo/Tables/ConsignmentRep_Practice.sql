﻿CREATE TABLE [dbo].[ConsignmentRep_Practice] (
    [UserID]     UNIQUEIDENTIFIER NOT NULL,
    [PracticeID] INT              NOT NULL,
    [IsActive]   BIT              CONSTRAINT [DF_ConsignmentRep_Practice_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ConsignmentRep_Practice] PRIMARY KEY CLUSTERED ([UserID] ASC, [PracticeID] ASC),
    CONSTRAINT [FK_ConsignmentRep_Practice_Practice] FOREIGN KEY ([PracticeID]) REFERENCES [dbo].[Practice] ([PracticeID])
);

