﻿CREATE TABLE [dbo].[FaxMessage] (
    [FaxMessageID] INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FaxNumber]    VARCHAR (50)  NOT NULL,
    [FaxFileName]  VARCHAR (250) NOT NULL,
    [Sent]         BIT           CONSTRAINT [DF_FaxMessage_Sent] DEFAULT ((0)) NOT NULL,
    [Skip]         BIT           CONSTRAINT [DF_FaxMessage_Skip] DEFAULT ((0)) NOT NULL,
    [SentTime]     DATETIME      NULL,
    [CreatedDate]  DATETIME      CONSTRAINT [DF_FaxMessage_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [SendAttempts] INT           DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_FaxMessage] PRIMARY KEY CLUSTERED ([FaxMessageID] ASC)
);

