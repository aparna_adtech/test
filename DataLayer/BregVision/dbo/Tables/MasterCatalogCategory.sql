﻿CREATE TABLE [dbo].[MasterCatalogCategory] (
    [MasterCatalogCategoryID] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MasterCatalogSupplierID] INT          NOT NULL,
    [Name]                    VARCHAR (50) NOT NULL,
    [Sequence]                INT          NULL,
    [CreatedUserID]           INT          NOT NULL,
    [CreatedDate]             DATETIME     CONSTRAINT [DF__MasterCat__Creat__59FA5E80] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]          INT          NULL,
    [ModifiedDate]            DATETIME     NULL,
    [IsActive]                BIT          NOT NULL,
    CONSTRAINT [MasterCatalogCategory_PK] PRIMARY KEY CLUSTERED ([MasterCatalogCategoryID] ASC),
    CONSTRAINT [MasterCatalogSupplier_MasterCatalogCategory_FK1] FOREIGN KEY ([MasterCatalogSupplierID]) REFERENCES [dbo].[MasterCatalogSupplier] ([MasterCatalogSupplierID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the Master Catalog Category table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogCategory', @level2type = N'COLUMN', @level2name = N'MasterCatalogCategoryID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the product category is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogCategory', @level2type = N'COLUMN', @level2name = N'Name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the order of the categories will be displayed.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogCategory', @level2type = N'COLUMN', @level2name = N'Sequence';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record of category is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogCategory', @level2type = N'COLUMN', @level2name = N'IsActive';

