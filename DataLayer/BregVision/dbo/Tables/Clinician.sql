﻿CREATE TABLE [dbo].[Clinician] (
    [ClinicianID]    INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PracticeID]     INT          NOT NULL,
    [ContactID]      INT          NULL,
    [CreatedUserID]  INT          NOT NULL,
    [CreatedDate]    DATETIME     CONSTRAINT [DF__Physician__Creat__49C3F6B7] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID] INT          NULL,
    [ModifiedDate]   DATETIME     NULL,
    [IsActive]       BIT          NOT NULL,
    [SortOrder]      INT          DEFAULT ((0)) NOT NULL,
    [PIN]            VARCHAR (10) NULL,
    [IsProvider]     BIT          CONSTRAINT [DF_Physician_IsProvider] DEFAULT ((1)) NOT NULL,
    [IsFitter]       BIT          CONSTRAINT [DF_Physician_IsFitter] DEFAULT ((0)) NOT NULL,
    [CloudConnectId] VARCHAR (50) NULL,
    [NPI]            VARCHAR (10) NULL,
    CONSTRAINT [Clinician_PK] PRIMARY KEY CLUSTERED ([ClinicianID] ASC),
    CONSTRAINT [Contact_Clinician_FK1] FOREIGN KEY ([ContactID]) REFERENCES [dbo].[Contact] ([ContactID]),
    CONSTRAINT [Practice_Clinician_FK1] FOREIGN KEY ([PracticeID]) REFERENCES [dbo].[Practice] ([PracticeID])
);


GO
CREATE NONCLUSTERED INDEX [_dta_index_Physician_12_565577053__K8_K1_K3]
    ON [dbo].[Clinician]([IsActive] ASC, [ClinicianID] ASC, [ContactID] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_Physician_12_565577053__K1_3]
    ON [dbo].[Clinician]([ClinicianID] ASC)
    INCLUDE([ContactID]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_Physician_12_565577053__K3_K1]
    ON [dbo].[Clinician]([ContactID] ASC, [ClinicianID] ASC);


GO
CREATE STATISTICS [_dta_stat_565577053_1_8]
    ON [dbo].[Clinician]([ClinicianID], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_565577053_3_8]
    ON [dbo].[Clinician]([ContactID], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_565577053_1_3_8]
    ON [dbo].[Clinician]([ClinicianID], [ContactID], [IsActive]);

