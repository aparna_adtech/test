﻿CREATE TABLE [dbo].[PracticeCatalogSupplierBrand] (
    [PracticeCatalogSupplierBrandID] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PracticeID]                     INT          NOT NULL,
    [MasterCatalogSupplierID]        INT          NULL,
    [ThirdPartySupplierID]           INT          NULL,
    [SupplierName]                   VARCHAR (50) NOT NULL,
    [SupplierShortName]              VARCHAR (50) NOT NULL,
    [BrandName]                      VARCHAR (50) NOT NULL,
    [BrandShortName]                 VARCHAR (50) NOT NULL,
    [IsThirdPartySupplier]           BIT          NOT NULL,
    [Sequence]                       INT          CONSTRAINT [DF_PracticeCatalogSupplierBrand_Sequence] DEFAULT ((999)) NULL,
    [CreatedUserID]                  INT          NOT NULL,
    [CreatedDate]                    DATETIME     CONSTRAINT [DF_PracticeCatalogSupplierBrand_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]                 INT          NULL,
    [ModifiedDate]                   DATETIME     NULL,
    [IsActive]                       BIT          NOT NULL,
    [SupplierWarrantyID]             INT          NULL,
    CONSTRAINT [PracticeCatalogSupplierBrand_PK] PRIMARY KEY CLUSTERED ([PracticeCatalogSupplierBrandID] ASC),
    CONSTRAINT [FK_PracticeCatalogSupplierBrand_SupplierWarranty] FOREIGN KEY ([SupplierWarrantyID]) REFERENCES [dbo].[SupplierWarranty] ([SupplierWarrantyID]),
    CONSTRAINT [MasterCatalogSupplier_PracticeCatalogSupplierBrand_FK1] FOREIGN KEY ([MasterCatalogSupplierID]) REFERENCES [dbo].[MasterCatalogSupplier] ([MasterCatalogSupplierID]),
    CONSTRAINT [Practice_PracticeCatalogSupplierBrand_FK1] FOREIGN KEY ([PracticeID]) REFERENCES [dbo].[Practice] ([PracticeID]),
    CONSTRAINT [ThirdPartySupplier_PracticeCatalogSupplierBrand_FK1] FOREIGN KEY ([ThirdPartySupplierID]) REFERENCES [dbo].[ThirdPartySupplier] ([ThirdPartySupplierID])
);


GO
ALTER TABLE [dbo].[PracticeCatalogSupplierBrand] NOCHECK CONSTRAINT [FK_PracticeCatalogSupplierBrand_SupplierWarranty];


GO
CREATE NONCLUSTERED INDEX [_dta_index_PracticeCatalogSupplierBrand_8_837578022__K15_K8_K5_K7_K1_K6]
    ON [dbo].[PracticeCatalogSupplierBrand]([IsActive] ASC, [BrandShortName] ASC, [SupplierName] ASC, [BrandName] ASC, [PracticeCatalogSupplierBrandID] ASC, [SupplierShortName] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_PracticeCatalogSupplierBrand_8_837578022__K15_K1_K6_K8_2_3_4_9_10]
    ON [dbo].[PracticeCatalogSupplierBrand]([IsActive] ASC, [PracticeCatalogSupplierBrandID] ASC, [SupplierShortName] ASC, [BrandShortName] ASC)
    INCLUDE([PracticeID], [MasterCatalogSupplierID], [ThirdPartySupplierID], [IsThirdPartySupplier], [Sequence]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_PracticeCatalogSupplierBrand_8_837578022__K15_K2_K1_K9_3_4_6_8_10]
    ON [dbo].[PracticeCatalogSupplierBrand]([IsActive] ASC, [PracticeID] ASC, [PracticeCatalogSupplierBrandID] ASC, [IsThirdPartySupplier] ASC)
    INCLUDE([MasterCatalogSupplierID], [ThirdPartySupplierID], [SupplierShortName], [BrandShortName], [Sequence]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_PracticeCatalogSupplierBrand_8_837578022__K15_K7_K8_K5_K1]
    ON [dbo].[PracticeCatalogSupplierBrand]([IsActive] ASC, [BrandName] ASC, [BrandShortName] ASC, [SupplierName] ASC, [PracticeCatalogSupplierBrandID] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_PracticeCatalogSupplierBrand_8_837578022__K15_K9_K1_K4_K10_K8_6]
    ON [dbo].[PracticeCatalogSupplierBrand]([IsActive] ASC, [IsThirdPartySupplier] ASC, [PracticeCatalogSupplierBrandID] ASC, [ThirdPartySupplierID] ASC, [Sequence] ASC, [BrandShortName] ASC)
    INCLUDE([SupplierShortName]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_PracticeCatalogSupplierBrand_8_837578022__K1_K15_K9_K6_K8_10]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeCatalogSupplierBrandID] ASC, [IsActive] ASC, [IsThirdPartySupplier] ASC, [SupplierShortName] ASC, [BrandShortName] ASC)
    INCLUDE([Sequence]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_PracticeCatalogSupplierBrand_8_837578022__K15_K1_K5_K7]
    ON [dbo].[PracticeCatalogSupplierBrand]([IsActive] ASC, [PracticeCatalogSupplierBrandID] ASC, [SupplierName] ASC, [BrandName] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_PracticeCatalogSupplierBrand_8_837578022__K15_K9_K3_K1_K10_K8]
    ON [dbo].[PracticeCatalogSupplierBrand]([IsActive] ASC, [IsThirdPartySupplier] ASC, [MasterCatalogSupplierID] ASC, [PracticeCatalogSupplierBrandID] ASC, [Sequence] ASC, [BrandShortName] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_PracticeCatalogSupplierBrand_12_837578022__K1_K15_K5_K7_6_8]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeCatalogSupplierBrandID] ASC, [IsActive] ASC, [SupplierName] ASC, [BrandName] ASC)
    INCLUDE([SupplierShortName], [BrandShortName]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_PracticeCatalogSupplierBrand_12_837578022__K15_K5_K6_K1]
    ON [dbo].[PracticeCatalogSupplierBrand]([IsActive] ASC, [SupplierName] ASC, [SupplierShortName] ASC, [PracticeCatalogSupplierBrandID] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_PracticeCatalogSupplierBrand_12_837578022__K15_K1_K5]
    ON [dbo].[PracticeCatalogSupplierBrand]([IsActive] ASC, [PracticeCatalogSupplierBrandID] ASC, [SupplierName] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_PracticeCatalogSupplierBrand_12_837578022__K15_K8_K1]
    ON [dbo].[PracticeCatalogSupplierBrand]([IsActive] ASC, [BrandShortName] ASC, [PracticeCatalogSupplierBrandID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PracticeCatalogSupplierBrand_IsThirdPartySupplier_MasterCatalogSupplierID]
    ON [dbo].[PracticeCatalogSupplierBrand]([IsThirdPartySupplier] ASC, [MasterCatalogSupplierID] ASC)
    INCLUDE([PracticeCatalogSupplierBrandID]);


GO
CREATE STATISTICS [_dta_stat_837578022_7_15]
    ON [dbo].[PracticeCatalogSupplierBrand]([BrandName], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_837578022_8_7]
    ON [dbo].[PracticeCatalogSupplierBrand]([BrandShortName], [BrandName]);


GO
CREATE STATISTICS [_dta_stat_837578022_15_5]
    ON [dbo].[PracticeCatalogSupplierBrand]([IsActive], [SupplierName]);


GO
CREATE STATISTICS [_dta_stat_837578022_15_4]
    ON [dbo].[PracticeCatalogSupplierBrand]([IsActive], [ThirdPartySupplierID]);


GO
CREATE STATISTICS [_dta_stat_837578022_2_1]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeID], [PracticeCatalogSupplierBrandID]);


GO
CREATE STATISTICS [_dta_stat_837578022_5_15_9]
    ON [dbo].[PracticeCatalogSupplierBrand]([SupplierName], [IsActive], [IsThirdPartySupplier]);


GO
CREATE STATISTICS [_dta_stat_837578022_3_4_8]
    ON [dbo].[PracticeCatalogSupplierBrand]([MasterCatalogSupplierID], [ThirdPartySupplierID], [BrandShortName]);


GO
CREATE STATISTICS [_dta_stat_837578022_1_8_5]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeCatalogSupplierBrandID], [BrandShortName], [SupplierName]);


GO
CREATE STATISTICS [_dta_stat_837578022_1_8_9]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeCatalogSupplierBrandID], [BrandShortName], [IsThirdPartySupplier]);


GO
CREATE STATISTICS [_dta_stat_837578022_2_4_9]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeID], [ThirdPartySupplierID], [IsThirdPartySupplier]);


GO
CREATE STATISTICS [_dta_stat_837578022_2_4_15]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeID], [ThirdPartySupplierID], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_837578022_1_5_7]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeCatalogSupplierBrandID], [SupplierName], [BrandName]);


GO
CREATE STATISTICS [_dta_stat_837578022_4_8_6]
    ON [dbo].[PracticeCatalogSupplierBrand]([ThirdPartySupplierID], [BrandShortName], [SupplierShortName]);


GO
CREATE STATISTICS [_dta_stat_837578022_1_15_7]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeCatalogSupplierBrandID], [IsActive], [BrandName]);


GO
CREATE STATISTICS [_dta_stat_837578022_6_8_1_4]
    ON [dbo].[PracticeCatalogSupplierBrand]([SupplierShortName], [BrandShortName], [PracticeCatalogSupplierBrandID], [ThirdPartySupplierID]);


GO
CREATE STATISTICS [_dta_stat_837578022_2_15_3_4]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeID], [IsActive], [MasterCatalogSupplierID], [ThirdPartySupplierID]);


GO
CREATE STATISTICS [_dta_stat_837578022_1_15_8_5]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeCatalogSupplierBrandID], [IsActive], [BrandShortName], [SupplierName]);


GO
CREATE STATISTICS [_dta_stat_837578022_5_4_1_15]
    ON [dbo].[PracticeCatalogSupplierBrand]([SupplierName], [ThirdPartySupplierID], [PracticeCatalogSupplierBrandID], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_837578022_6_1_7_8]
    ON [dbo].[PracticeCatalogSupplierBrand]([SupplierShortName], [PracticeCatalogSupplierBrandID], [BrandName], [BrandShortName]);


GO
CREATE STATISTICS [_dta_stat_837578022_2_15_9_4]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeID], [IsActive], [IsThirdPartySupplier], [ThirdPartySupplierID]);


GO
CREATE STATISTICS [_dta_stat_837578022_1_15_9_8]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeCatalogSupplierBrandID], [IsActive], [IsThirdPartySupplier], [BrandShortName]);


GO
CREATE STATISTICS [_dta_stat_837578022_6_8_15_5]
    ON [dbo].[PracticeCatalogSupplierBrand]([SupplierShortName], [BrandShortName], [IsActive], [SupplierName]);


GO
CREATE STATISTICS [_dta_stat_837578022_1_15_4_2]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeCatalogSupplierBrandID], [IsActive], [ThirdPartySupplierID], [PracticeID]);


GO
CREATE STATISTICS [_dta_stat_837578022_2_15_9_3]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeID], [IsActive], [IsThirdPartySupplier], [MasterCatalogSupplierID]);


GO
CREATE STATISTICS [_dta_stat_837578022_3_1_4_15]
    ON [dbo].[PracticeCatalogSupplierBrand]([MasterCatalogSupplierID], [PracticeCatalogSupplierBrandID], [ThirdPartySupplierID], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_837578022_3_8_6_4]
    ON [dbo].[PracticeCatalogSupplierBrand]([MasterCatalogSupplierID], [BrandShortName], [SupplierShortName], [ThirdPartySupplierID]);


GO
CREATE STATISTICS [_dta_stat_837578022_5_15_1_9]
    ON [dbo].[PracticeCatalogSupplierBrand]([SupplierName], [IsActive], [PracticeCatalogSupplierBrandID], [IsThirdPartySupplier]);


GO
CREATE STATISTICS [_dta_stat_837578022_7_1_8_5]
    ON [dbo].[PracticeCatalogSupplierBrand]([BrandName], [PracticeCatalogSupplierBrandID], [BrandShortName], [SupplierName]);


GO
CREATE STATISTICS [_dta_stat_837578022_8_15_7_1]
    ON [dbo].[PracticeCatalogSupplierBrand]([BrandShortName], [IsActive], [BrandName], [PracticeCatalogSupplierBrandID]);


GO
CREATE STATISTICS [_dta_stat_837578022_5_7_15_1]
    ON [dbo].[PracticeCatalogSupplierBrand]([SupplierName], [BrandName], [IsActive], [PracticeCatalogSupplierBrandID]);


GO
CREATE STATISTICS [_dta_stat_837578022_9_1_6_8]
    ON [dbo].[PracticeCatalogSupplierBrand]([IsThirdPartySupplier], [PracticeCatalogSupplierBrandID], [SupplierShortName], [BrandShortName]);


GO
CREATE STATISTICS [_dta_stat_837578022_1_3_4_8]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeCatalogSupplierBrandID], [MasterCatalogSupplierID], [ThirdPartySupplierID], [BrandShortName]);


GO
CREATE STATISTICS [_dta_stat_837578022_1_15_4_5_7]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeCatalogSupplierBrandID], [IsActive], [ThirdPartySupplierID], [SupplierName], [BrandName]);


GO
CREATE STATISTICS [_dta_stat_837578022_6_8_1_3_4]
    ON [dbo].[PracticeCatalogSupplierBrand]([SupplierShortName], [BrandShortName], [PracticeCatalogSupplierBrandID], [MasterCatalogSupplierID], [ThirdPartySupplierID]);


GO
CREATE STATISTICS [_dta_stat_837578022_6_15_7_8_1]
    ON [dbo].[PracticeCatalogSupplierBrand]([SupplierShortName], [IsActive], [BrandName], [BrandShortName], [PracticeCatalogSupplierBrandID]);


GO
CREATE STATISTICS [_dta_stat_837578022_9_6_8_2_15]
    ON [dbo].[PracticeCatalogSupplierBrand]([IsThirdPartySupplier], [SupplierShortName], [BrandShortName], [PracticeID], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_837578022_10_1_8_9_15]
    ON [dbo].[PracticeCatalogSupplierBrand]([Sequence], [PracticeCatalogSupplierBrandID], [BrandShortName], [IsThirdPartySupplier], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_837578022_9_6_8_1_2]
    ON [dbo].[PracticeCatalogSupplierBrand]([IsThirdPartySupplier], [SupplierShortName], [BrandShortName], [PracticeCatalogSupplierBrandID], [PracticeID]);


GO
CREATE STATISTICS [_dta_stat_837578022_8_5_7_6_1]
    ON [dbo].[PracticeCatalogSupplierBrand]([BrandShortName], [SupplierName], [BrandName], [SupplierShortName], [PracticeCatalogSupplierBrandID]);


GO
CREATE STATISTICS [_dta_stat_837578022_15_6_8_1_10]
    ON [dbo].[PracticeCatalogSupplierBrand]([IsActive], [SupplierShortName], [BrandShortName], [PracticeCatalogSupplierBrandID], [Sequence]);


GO
CREATE STATISTICS [_dta_stat_837578022_5_7_15_8_6]
    ON [dbo].[PracticeCatalogSupplierBrand]([SupplierName], [BrandName], [IsActive], [BrandShortName], [SupplierShortName]);


GO
CREATE STATISTICS [_dta_stat_837578022_2_3_4_9_15]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeID], [MasterCatalogSupplierID], [ThirdPartySupplierID], [IsThirdPartySupplier], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_837578022_1_2_15_5_4]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeCatalogSupplierBrandID], [PracticeID], [IsActive], [SupplierName], [ThirdPartySupplierID]);


GO
CREATE STATISTICS [_dta_stat_837578022_1_15_6_8_4]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeCatalogSupplierBrandID], [IsActive], [SupplierShortName], [BrandShortName], [ThirdPartySupplierID]);


GO
CREATE STATISTICS [_dta_stat_837578022_6_8_1_15_5]
    ON [dbo].[PracticeCatalogSupplierBrand]([SupplierShortName], [BrandShortName], [PracticeCatalogSupplierBrandID], [IsActive], [SupplierName]);


GO
CREATE STATISTICS [_dta_stat_837578022_6_8_10_15_7_5]
    ON [dbo].[PracticeCatalogSupplierBrand]([SupplierShortName], [BrandShortName], [Sequence], [IsActive], [BrandName], [SupplierName]);


GO
CREATE STATISTICS [_dta_stat_837578022_1_4_15_9_10_8]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeCatalogSupplierBrandID], [ThirdPartySupplierID], [IsActive], [IsThirdPartySupplier], [Sequence], [BrandShortName]);


GO
CREATE STATISTICS [_dta_stat_837578022_1_15_2_9_6_8]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeCatalogSupplierBrandID], [IsActive], [PracticeID], [IsThirdPartySupplier], [SupplierShortName], [BrandShortName]);


GO
CREATE STATISTICS [_dta_stat_837578022_15_6_8_1_3_4]
    ON [dbo].[PracticeCatalogSupplierBrand]([IsActive], [SupplierShortName], [BrandShortName], [PracticeCatalogSupplierBrandID], [MasterCatalogSupplierID], [ThirdPartySupplierID]);


GO
CREATE STATISTICS [_dta_stat_837578022_1_15_3_9_4_10_8]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeCatalogSupplierBrandID], [IsActive], [MasterCatalogSupplierID], [IsThirdPartySupplier], [ThirdPartySupplierID], [Sequence], [BrandShortName]);


GO
CREATE STATISTICS [_dta_stat_837578022_8_1_15_7_5_6_10]
    ON [dbo].[PracticeCatalogSupplierBrand]([BrandShortName], [PracticeCatalogSupplierBrandID], [IsActive], [BrandName], [SupplierName], [SupplierShortName], [Sequence]);


GO
CREATE STATISTICS [_dta_stat_837578022_6_7]
    ON [dbo].[PracticeCatalogSupplierBrand]([SupplierShortName], [BrandName]);


GO
CREATE STATISTICS [_dta_stat_837578022_5_6]
    ON [dbo].[PracticeCatalogSupplierBrand]([SupplierName], [SupplierShortName]);


GO
CREATE STATISTICS [_dta_stat_837578022_7_8_6]
    ON [dbo].[PracticeCatalogSupplierBrand]([BrandName], [BrandShortName], [SupplierShortName]);


GO
CREATE STATISTICS [_dta_stat_837578022_1_15_5_6]
    ON [dbo].[PracticeCatalogSupplierBrand]([PracticeCatalogSupplierBrandID], [IsActive], [SupplierName], [SupplierShortName]);


GO
CREATE trigger [dbo].[trg_PracticeCatalogSupplierBrand_Updated] ON [dbo].[PracticeCatalogSupplierBrand]
    FOR UPDATE
AS
    begin

		INSERT INTO [dbo].[PracticeCatalogSupplierBrand_Audit]
				   ([PracticeCatalogSupplierBrandID]
				   ,[PracticeID]
				   ,[MasterCatalogSupplierID]
				   ,[ThirdPartySupplierID]
				   ,[SupplierName]
				   ,[SupplierShortName]
				   ,[BrandName]
				   ,[BrandShortName]
				   ,[IsThirdPartySupplier]
				   ,[Sequence]
				   ,[CreatedUserID]
				   ,[CreatedDate]
				   ,[ModifiedUserID]
				   ,[ModifiedDate]
				   ,[IsActive]
				   ,[SupplierWarrantyID]
				   ,[AuditAction]
				   ,[AuditCreatedDate])
		SELECT
					[PracticeCatalogSupplierBrandID]
				   ,[PracticeID]
				   ,[MasterCatalogSupplierID]
				   ,[ThirdPartySupplierID]
				   ,[SupplierName]
				   ,[SupplierShortName]
				   ,[BrandName]
				   ,[BrandShortName]
				   ,[IsThirdPartySupplier]
				   ,[Sequence]
				   ,[CreatedUserID]
				   ,[CreatedDate]
				   ,[ModifiedUserID]
				   ,[ModifiedDate]
				   ,[IsActive]
				   ,[SupplierWarrantyID]
				   ,'Update(OLD)'
				   ,GETDATE()
		FROM
					deleted;
                
		INSERT INTO [dbo].[PracticeCatalogSupplierBrand_Audit]
				   ([PracticeCatalogSupplierBrandID]
				   ,[PracticeID]
				   ,[MasterCatalogSupplierID]
				   ,[ThirdPartySupplierID]
				   ,[SupplierName]
				   ,[SupplierShortName]
				   ,[BrandName]
				   ,[BrandShortName]
				   ,[IsThirdPartySupplier]
				   ,[Sequence]
				   ,[CreatedUserID]
				   ,[CreatedDate]
				   ,[ModifiedUserID]
				   ,[ModifiedDate]
				   ,[IsActive]
				   ,[SupplierWarrantyID]
				   ,[AuditAction]
				   ,[AuditCreatedDate])
		SELECT
					[PracticeCatalogSupplierBrandID]
				   ,[PracticeID]
				   ,[MasterCatalogSupplierID]
				   ,[ThirdPartySupplierID]
				   ,[SupplierName]
				   ,[SupplierShortName]
				   ,[BrandName]
				   ,[BrandShortName]
				   ,[IsThirdPartySupplier]
				   ,[Sequence]
				   ,[CreatedUserID]
				   ,[CreatedDate]
				   ,[ModifiedUserID]
				   ,[ModifiedDate]
				   ,[IsActive]
				   ,[SupplierWarrantyID]
				   ,'Update(NEW)'
				   ,GETDATE()
		FROM
					inserted;
  
    END

GO
CREATE trigger [dbo].[trg_PracticeCatalogSupplierBrand_Inserted] ON [dbo].[PracticeCatalogSupplierBrand]
    FOR INSERT
AS
    begin

		INSERT INTO [dbo].[PracticeCatalogSupplierBrand_Audit]
				   ([PracticeCatalogSupplierBrandID]
				   ,[PracticeID]
				   ,[MasterCatalogSupplierID]
				   ,[ThirdPartySupplierID]
				   ,[SupplierName]
				   ,[SupplierShortName]
				   ,[BrandName]
				   ,[BrandShortName]
				   ,[IsThirdPartySupplier]
				   ,[Sequence]
				   ,[CreatedUserID]
				   ,[CreatedDate]
				   ,[ModifiedUserID]
				   ,[ModifiedDate]
				   ,[IsActive]
				   ,[SupplierWarrantyID]
				   ,[AuditAction]
				   ,[AuditCreatedDate])
		SELECT
					[PracticeCatalogSupplierBrandID]
				   ,[PracticeID]
				   ,[MasterCatalogSupplierID]
				   ,[ThirdPartySupplierID]
				   ,[SupplierName]
				   ,[SupplierShortName]
				   ,[BrandName]
				   ,[BrandShortName]
				   ,[IsThirdPartySupplier]
				   ,[Sequence]
				   ,[CreatedUserID]
				   ,[CreatedDate]
				   ,[ModifiedUserID]
				   ,[ModifiedDate]
				   ,[IsActive]
				   ,[SupplierWarrantyID]
				   ,'Inserted'
				   ,GETDATE()
		FROM
					inserted;
  
    END

GO
CREATE trigger [dbo].[trg_PracticeCatalogSupplierBrand_Deleted] ON [dbo].[PracticeCatalogSupplierBrand]
    FOR DELETE
AS
    begin

		INSERT INTO [dbo].[PracticeCatalogSupplierBrand_Audit]
				   ([PracticeCatalogSupplierBrandID]
				   ,[PracticeID]
				   ,[MasterCatalogSupplierID]
				   ,[ThirdPartySupplierID]
				   ,[SupplierName]
				   ,[SupplierShortName]
				   ,[BrandName]
				   ,[BrandShortName]
				   ,[IsThirdPartySupplier]
				   ,[Sequence]
				   ,[CreatedUserID]
				   ,[CreatedDate]
				   ,[ModifiedUserID]
				   ,[ModifiedDate]
				   ,[IsActive]
				   ,[SupplierWarrantyID]
				   ,[AuditAction]
				   ,[AuditCreatedDate])
		SELECT
					[PracticeCatalogSupplierBrandID]
				   ,[PracticeID]
				   ,[MasterCatalogSupplierID]
				   ,[ThirdPartySupplierID]
				   ,[SupplierName]
				   ,[SupplierShortName]
				   ,[BrandName]
				   ,[BrandShortName]
				   ,[IsThirdPartySupplier]
				   ,[Sequence]
				   ,[CreatedUserID]
				   ,[CreatedDate]
				   ,[ModifiedUserID]
				   ,[ModifiedDate]
				   ,[IsActive]
				   ,[SupplierWarrantyID]
				   ,'Deleted'
				   ,GETDATE()
		FROM
					deleted;
  
    END

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the Practice Catalog Supplier table.  This table is linked to the Practice ID table, the Master Catalog Supplier table and the Third Party Supplier table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeCatalogSupplierBrand', @level2type = N'COLUMN', @level2name = N'PracticeCatalogSupplierBrandID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the supplier''s name is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeCatalogSupplierBrand', @level2type = N'COLUMN', @level2name = N'SupplierName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the supplier''s abbreviated name is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeCatalogSupplierBrand', @level2type = N'COLUMN', @level2name = N'SupplierShortName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the brand name is entered into.  In some cases, it is the same name as the supplier.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeCatalogSupplierBrand', @level2type = N'COLUMN', @level2name = N'BrandName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the abbreviated brand name is entered into.  In some cases, it is the same as the supplier''s abbreviated name.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeCatalogSupplierBrand', @level2type = N'COLUMN', @level2name = N'BrandShortName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag to verify if the supplier is a third party.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeCatalogSupplierBrand', @level2type = N'COLUMN', @level2name = N'IsThirdPartySupplier';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field to define the order in which the practice''s suppliers will be displayed. ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeCatalogSupplierBrand', @level2type = N'COLUMN', @level2name = N'Sequence';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in the practice supplier catalog is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeCatalogSupplierBrand', @level2type = N'COLUMN', @level2name = N'IsActive';

