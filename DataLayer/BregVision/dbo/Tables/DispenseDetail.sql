﻿CREATE TABLE [dbo].[DispenseDetail] (
    [DispenseDetailID]         INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [DispenseID]               INT           NOT NULL,
    [PracticeCatalogProductID] INT           NOT NULL,
    [PhysicianID]              INT           NULL,
    [ActualChargeBilled]       SMALLMONEY    NULL,
    [DMEDeposit]               SMALLMONEY    NULL,
    [Quantity]                 INT           NULL,
    [LineTotal]                SMALLMONEY    NULL,
    [IsCashCollection]         BIT           NULL,
    [CreatedUserID]            INT           NOT NULL,
    [CreatedDate]              DATETIME      CONSTRAINT [DF__DispenseD__Creat__1BFD2C07] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]           INT           NULL,
    [ModifiedDate]             DATETIME      NULL,
    [IsActive]                 BIT           NOT NULL,
    [ICD9Code]                 VARCHAR (50)  NULL,
    [DispenseQueueID]          INT           NULL,
    [LRModifier]               VARCHAR (2)   NULL,
    [IsMedicare]               BIT           DEFAULT ((0)) NOT NULL,
    [IsCustomFit]              BIT           NULL,
    [FitterID]                 INT           NULL,
    [ABNType]                  VARCHAR (50)  NULL,
    [Mod1]                     VARCHAR (2)   NULL,
    [Mod2]                     VARCHAR (2)   NULL,
    [Mod3]                     VARCHAR (2)   NULL,
    [DeleteReason]             VARCHAR (250) NULL,
    [ABNForm]                  BIT           DEFAULT ((0)) NOT NULL,
    CONSTRAINT [DispenseDetail_PK] PRIMARY KEY CLUSTERED ([DispenseDetailID] ASC),
    CONSTRAINT [Clinician_DispenseDetail_FK1] FOREIGN KEY ([PhysicianID]) REFERENCES [dbo].[Clinician] ([ClinicianID]),
    CONSTRAINT [Dispense_DispenseDetail_FK1] FOREIGN KEY ([DispenseID]) REFERENCES [dbo].[Dispense] ([DispenseID]),
    CONSTRAINT [FK_DispenseDetail_Clinician_Fitter] FOREIGN KEY ([FitterID]) REFERENCES [dbo].[Clinician] ([ClinicianID]),
    CONSTRAINT [PracticeCatalogProduct_DispenseDetail_FK1] FOREIGN KEY ([PracticeCatalogProductID]) REFERENCES [dbo].[PracticeCatalogProduct] ([PracticeCatalogProductID])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_462]
    ON [dbo].[DispenseDetail]([DispenseID] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_819]
    ON [dbo].[DispenseDetail]([DispenseID] ASC)
    INCLUDE([DispenseDetailID], [PracticeCatalogProductID], [PhysicianID], [ActualChargeBilled], [DMEDeposit], [Quantity], [LineTotal], [IsCashCollection], [CreatedUserID], [CreatedDate], [ModifiedUserID], [ModifiedDate], [IsActive]);


GO
CREATE NONCLUSTERED INDEX [missing_index_826]
    ON [dbo].[DispenseDetail]([PhysicianID] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_464]
    ON [dbo].[DispenseDetail]([DispenseID] ASC, [IsActive] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_616]
    ON [dbo].[DispenseDetail]([DispenseID] ASC, [IsActive] ASC)
    INCLUDE([DispenseDetailID], [PracticeCatalogProductID], [PhysicianID], [ActualChargeBilled], [Quantity], [LineTotal]);


GO
CREATE NONCLUSTERED INDEX [missing_index_815]
    ON [dbo].[DispenseDetail]([DispenseID] ASC, [IsActive] ASC)
    INCLUDE([DispenseDetailID], [PracticeCatalogProductID], [PhysicianID], [ActualChargeBilled], [DMEDeposit], [Quantity]);


GO
CREATE NONCLUSTERED INDEX [missing_index_789]
    ON [dbo].[DispenseDetail]([DispenseID] ASC)
    INCLUDE([PracticeCatalogProductID]);


GO
CREATE NONCLUSTERED INDEX [missing_index_987]
    ON [dbo].[DispenseDetail]([IsActive] ASC)
    INCLUDE([DispenseDetailID], [DispenseID], [PracticeCatalogProductID], [PhysicianID], [ActualChargeBilled], [Quantity], [LineTotal]);


GO
CREATE NONCLUSTERED INDEX [IX_DispenseDetail_DispenseQueueID]
    ON [dbo].[DispenseDetail]([DispenseQueueID] ASC)
    INCLUDE([DispenseDetailID], [DispenseID], [PracticeCatalogProductID], [PhysicianID], [ActualChargeBilled], [DMEDeposit], [Quantity], [LineTotal], [IsCashCollection], [CreatedUserID], [CreatedDate], [ModifiedUserID], [ModifiedDate], [IsActive], [ICD9Code], [LRModifier], [IsMedicare], [ABNForm]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_DispenseDetail_12_197575742__K4_K14_K1_K2_K3_K5_K7_K8_K6_K17_K19]
    ON [dbo].[DispenseDetail]([PhysicianID] ASC, [IsActive] ASC, [DispenseDetailID] ASC, [DispenseID] ASC, [PracticeCatalogProductID] ASC, [ActualChargeBilled] ASC, [Quantity] ASC, [LineTotal] ASC, [DMEDeposit] ASC, [LRModifier] ASC, [ABNForm] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_DispenseDetail_12_197575742__K2_K14_K1_K4_K3_K5_K7_K8_K6_K17_K19]
    ON [dbo].[DispenseDetail]([DispenseID] ASC, [IsActive] ASC, [DispenseDetailID] ASC, [PhysicianID] ASC, [PracticeCatalogProductID] ASC, [ActualChargeBilled] ASC, [Quantity] ASC, [LineTotal] ASC, [DMEDeposit] ASC, [LRModifier] ASC, [ABNForm] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_DispenseDetail_12_197575742__K14_K4_K2_K1_K3_K5_K7_K8_K6_K17_K19]
    ON [dbo].[DispenseDetail]([IsActive] ASC, [PhysicianID] ASC, [DispenseID] ASC, [DispenseDetailID] ASC, [PracticeCatalogProductID] ASC, [ActualChargeBilled] ASC, [Quantity] ASC, [LineTotal] ASC, [DMEDeposit] ASC, [LRModifier] ASC, [ABNForm] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_DispenseDetail_12_197575742__K14_K2_K1_K3_K5_K7_K8_K6_K17_K19_K4]
    ON [dbo].[DispenseDetail]([IsActive] ASC, [DispenseID] ASC, [DispenseDetailID] ASC, [PracticeCatalogProductID] ASC, [ActualChargeBilled] ASC, [Quantity] ASC, [LineTotal] ASC, [DMEDeposit] ASC, [LRModifier] ASC, [ABNForm] ASC, [PhysicianID] ASC);


GO
CREATE STATISTICS [_dta_stat_197575742_2_4]
    ON [dbo].[DispenseDetail]([DispenseID], [PhysicianID]);


GO
CREATE STATISTICS [_dta_stat_197575742_4_14_2]
    ON [dbo].[DispenseDetail]([PhysicianID], [IsActive], [DispenseID]);


GO
CREATE STATISTICS [_dta_stat_197575742_14_3_2]
    ON [dbo].[DispenseDetail]([IsActive], [PracticeCatalogProductID], [DispenseID]);


GO
CREATE STATISTICS [_dta_stat_197575742_3_2_4]
    ON [dbo].[DispenseDetail]([PracticeCatalogProductID], [DispenseID], [PhysicianID]);


GO
CREATE STATISTICS [_dta_stat_197575742_1_14_7_5_8]
    ON [dbo].[DispenseDetail]([DispenseDetailID], [IsActive], [Quantity], [ActualChargeBilled], [LineTotal]);


GO
CREATE STATISTICS [_dta_stat_197575742_5_6_7_8_2_14]
    ON [dbo].[DispenseDetail]([ActualChargeBilled], [DMEDeposit], [Quantity], [LineTotal], [DispenseID], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_197575742_1_7_5_8_14_2]
    ON [dbo].[DispenseDetail]([DispenseDetailID], [Quantity], [ActualChargeBilled], [LineTotal], [IsActive], [DispenseID]);


GO
CREATE STATISTICS [_dta_stat_197575742_1_5_6_7_8_14_2]
    ON [dbo].[DispenseDetail]([DispenseDetailID], [ActualChargeBilled], [DMEDeposit], [Quantity], [LineTotal], [IsActive], [DispenseID]);


GO
CREATE STATISTICS [_dta_stat_197575742_3_2_14_4_5_6_7_8]
    ON [dbo].[DispenseDetail]([PracticeCatalogProductID], [DispenseID], [IsActive], [PhysicianID], [ActualChargeBilled], [DMEDeposit], [Quantity], [LineTotal]);


GO
CREATE STATISTICS [_dta_stat_197575742_14_4_3_2_1_5_6_7_8]
    ON [dbo].[DispenseDetail]([IsActive], [PhysicianID], [PracticeCatalogProductID], [DispenseID], [DispenseDetailID], [ActualChargeBilled], [DMEDeposit], [Quantity], [LineTotal]);


GO
CREATE STATISTICS [_dta_stat_197575742_1_4]
    ON [dbo].[DispenseDetail]([DispenseDetailID], [PhysicianID]);


GO
CREATE STATISTICS [_dta_stat_197575742_3_14_1]
    ON [dbo].[DispenseDetail]([PracticeCatalogProductID], [IsActive], [DispenseDetailID]);


GO
CREATE STATISTICS [_dta_stat_197575742_14_1_3_4]
    ON [dbo].[DispenseDetail]([IsActive], [DispenseDetailID], [PracticeCatalogProductID], [PhysicianID]);


GO
CREATE STATISTICS [_dta_stat_197575742_1_3_14_7_5_8]
    ON [dbo].[DispenseDetail]([DispenseDetailID], [PracticeCatalogProductID], [IsActive], [Quantity], [ActualChargeBilled], [LineTotal]);


GO
CREATE STATISTICS [_dta_stat_197575742_2_1_5_7_8_3_4]
    ON [dbo].[DispenseDetail]([DispenseID], [DispenseDetailID], [ActualChargeBilled], [Quantity], [LineTotal], [PracticeCatalogProductID], [PhysicianID]);


GO
CREATE STATISTICS [_dta_stat_197575742_1_5_6_7_8_14_4]
    ON [dbo].[DispenseDetail]([DispenseDetailID], [ActualChargeBilled], [DMEDeposit], [Quantity], [LineTotal], [IsActive], [PhysicianID]);


GO
CREATE STATISTICS [_dta_stat_197575742_4_3_14_1_5_6_7_8]
    ON [dbo].[DispenseDetail]([PhysicianID], [PracticeCatalogProductID], [IsActive], [DispenseDetailID], [ActualChargeBilled], [DMEDeposit], [Quantity], [LineTotal]);


GO
CREATE STATISTICS [_dta_stat_197575742_1_2_5_7_8_6_17_14]
    ON [dbo].[DispenseDetail]([DispenseDetailID], [DispenseID], [ActualChargeBilled], [Quantity], [LineTotal], [DMEDeposit], [LRModifier], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_197575742_1_6_17_2_14_4_3_5_7]
    ON [dbo].[DispenseDetail]([DispenseDetailID], [DMEDeposit], [LRModifier], [DispenseID], [IsActive], [PhysicianID], [PracticeCatalogProductID], [ActualChargeBilled], [Quantity]);


GO



CREATE TRIGGER [dbo].[trg_DispenseDetail_Deleted] 
ON [dbo].[DispenseDetail] 
FOR DELETE 
AS
BEGIN

	INSERT [DispenseDetail_Audit]
	(
[DispenseDetailID]
      ,[DispenseID]
      ,[PracticeCatalogProductID]
      ,[PhysicianID]
      ,[ActualChargeBilled]
      ,[DMEDeposit]
      ,[Quantity]
      ,[LineTotal]
      ,[IsCashCollection]
      ,[CreatedUserID]
      ,[CreatedDate]
      ,[ModifiedUserID]
      ,[ModifiedDate]
      ,[IsActive]
      ,[ICD9Code]
      ,[DispenseQueueID]
      ,[LRModifier]
      ,[IsMedicare]
      ,[ABNForm]
	  ,[AuditAction]
  )
  select 
  	
[DispenseDetailID]
      ,[DispenseID]
      ,[PracticeCatalogProductID]
      ,[PhysicianID]
      ,[ActualChargeBilled]
      ,[DMEDeposit]
      ,[Quantity]
      ,[LineTotal]
      ,[IsCashCollection]
      ,[CreatedUserID]
      ,[CreatedDate]
      ,[ModifiedUserID]
      ,[ModifiedDate]
      ,[IsActive]
      ,[ICD9Code]
      ,[DispenseQueueID]
      ,[LRModifier]
      ,[IsMedicare]
      ,[ABNForm]
	  ,'Deleted'
  
from deleted
end

GO


CREATE TRIGGER [dbo].[trg_DispenseDetail_Inserted] 
ON [dbo].[DispenseDetail] 
FOR INSERT 
AS
BEGIN

	INSERT INTO [DispenseDetail_Audit]
	(
[DispenseDetailID]
      ,[DispenseID]
      ,[PracticeCatalogProductID]
      ,[PhysicianID]
      ,[ActualChargeBilled]
      ,[DMEDeposit]
      ,[Quantity]
      ,[LineTotal]
      ,[IsCashCollection]
      ,[CreatedUserID]
      ,[CreatedDate]
      ,[ModifiedUserID]
      ,[ModifiedDate]
      ,[IsActive]
      ,[ICD9Code]
      ,[DispenseQueueID]
      ,[LRModifier]
      ,[IsMedicare]
      ,[ABNForm]
      ,[IsCustomFit]
	  ,[FitterID]
	  ,[AuditAction]
  )
  select 
  	
[DispenseDetailID]
      ,[DispenseID]
      ,[PracticeCatalogProductID]
      ,[PhysicianID]
      ,[ActualChargeBilled]
      ,[DMEDeposit]
      ,[Quantity]
      ,[LineTotal]
      ,[IsCashCollection]
      ,[CreatedUserID]
      ,[CreatedDate]
      ,[ModifiedUserID]
      ,[ModifiedDate]
      ,[IsActive]
      ,[ICD9Code]
      ,[DispenseQueueID]
      ,[LRModifier]
      ,[IsMedicare]
      ,[ABNForm]
      ,[IsCustomFit]
	  ,[FitterID]
	  ,'Inserted'
  
from inserted
end

GO




CREATE TRIGGER [dbo].[trg_DispenseDetail_Updated] 
ON [dbo].[DispenseDetail] 
FOR DELETE 
AS
BEGIN

	INSERT [DispenseDetail_Audit]
	(
[DispenseDetailID]
      ,[DispenseID]
      ,[PracticeCatalogProductID]
      ,[PhysicianID]
      ,[ActualChargeBilled]
      ,[DMEDeposit]
      ,[Quantity]
      ,[LineTotal]
      ,[IsCashCollection]
      ,[CreatedUserID]
      ,[CreatedDate]
      ,[ModifiedUserID]
      ,[ModifiedDate]
      ,[IsActive]
      ,[ICD9Code]
      ,[DispenseQueueID]
      ,[LRModifier]
      ,[IsMedicare]
      ,[ABNForm]
      ,[IsCustomFit]
	  ,[FitterID]
	  ,[AuditAction]
  )
  select 
  	
[DispenseDetailID]
      ,[DispenseID]
      ,[PracticeCatalogProductID]
      ,[PhysicianID]
      ,[ActualChargeBilled]
      ,[DMEDeposit]
      ,[Quantity]
      ,[LineTotal]
      ,[IsCashCollection]
      ,[CreatedUserID]
      ,[CreatedDate]
      ,[ModifiedUserID]
      ,[ModifiedDate]
      ,[IsActive]
      ,[ICD9Code]
      ,[DispenseQueueID]
      ,[LRModifier]
      ,[IsMedicare]
      ,[ABNForm]
      ,[IsCustomFit]
	  ,[FitterID]
	  ,'Update(OLD)'
  
from deleted;

	INSERT [DispenseDetail_Audit]
	(
[DispenseDetailID]
      ,[DispenseID]
      ,[PracticeCatalogProductID]
      ,[PhysicianID]
      ,[ActualChargeBilled]
      ,[DMEDeposit]
      ,[Quantity]
      ,[LineTotal]
      ,[IsCashCollection]
      ,[CreatedUserID]
      ,[CreatedDate]
      ,[ModifiedUserID]
      ,[ModifiedDate]
      ,[IsActive]
      ,[ICD9Code]
      ,[DispenseQueueID]
      ,[LRModifier]
      ,[IsMedicare]
      ,[ABNForm]
      ,[IsCustomFit]
	  ,[FitterID]
	  ,[AuditAction]
  )
  select 
  	
[DispenseDetailID]
      ,[DispenseID]
      ,[PracticeCatalogProductID]
      ,[PhysicianID]
      ,[ActualChargeBilled]
      ,[DMEDeposit]
      ,[Quantity]
      ,[LineTotal]
      ,[IsCashCollection]
      ,[CreatedUserID]
      ,[CreatedDate]
      ,[ModifiedUserID]
      ,[ModifiedDate]
      ,[IsActive]
      ,[ICD9Code]
      ,[DispenseQueueID]
      ,[LRModifier]
      ,[IsMedicare]
      ,[ABNForm]
      ,[IsCustomFit]
	  ,[FitterID]
	  ,'Update(NEW)'
  
from inserted;
end

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the Dispense Detail table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DispenseDetail', @level2type = N'COLUMN', @level2name = N'DispenseDetailID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the actual price of product to dispense is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DispenseDetail', @level2type = N'COLUMN', @level2name = N'ActualChargeBilled';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the quantity of a product to dispense is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DispenseDetail', @level2type = N'COLUMN', @level2name = N'Quantity';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the total price of one particular product (line) is entered into. (This is the quantity X price per unit.)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DispenseDetail', @level2type = N'COLUMN', @level2name = N'LineTotal';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag to verify that the dispensed product is being sold to a non-insured customer customer who is paying cash.  This can also include a variety of other situations, including overrides and other circumstances.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DispenseDetail', @level2type = N'COLUMN', @level2name = N'IsCashCollection';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in the Dispense Detail is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DispenseDetail', @level2type = N'COLUMN', @level2name = N'IsActive';

