﻿CREATE TABLE [dbo].[MasterCatalogSupplier] (
    [MasterCatalogSupplierID]       INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MasterCatalogSupplierParentID] INT           NULL,
    [AddressID]                     INT           NULL,
    [ContactID]                     INT           NULL,
    [SupplierName]                  VARCHAR (50)  NOT NULL,
    [SupplierShortName]             VARCHAR (50)  NOT NULL,
    [FaxOrderPlacement]             VARCHAR (25)  NULL,
    [EmailOrderPlacement]           VARCHAR (100) NULL,
    [IsEmailOrderPlacement]         BIT           NOT NULL,
    [Sequence]                      INT           NULL,
    [CreatedUserID]                 INT           NOT NULL,
    [CreatedDate]                   DATETIME      CONSTRAINT [DF__MasterCat__Creat__571DF1D5] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]                INT           NULL,
    [ModifiedDate]                  DATETIME      NULL,
    [IsActive]                      BIT           NOT NULL,
    [IsBCSSupplier]                 BIT           NULL,
    CONSTRAINT [MasterCatalogSupplier_PK] PRIMARY KEY CLUSTERED ([MasterCatalogSupplierID] ASC),
    CONSTRAINT [Address_MasterCatalogSupplier_FK1] FOREIGN KEY ([AddressID]) REFERENCES [dbo].[Address] ([AddressID]),
    CONSTRAINT [Contact_MasterCatalogSupplier_FK1] FOREIGN KEY ([ContactID]) REFERENCES [dbo].[Contact] ([ContactID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the Master Catalog Supplier table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogSupplier', @level2type = N'COLUMN', @level2name = N'MasterCatalogSupplierID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field for the name of the of the company selling the product is entered into.  It can be the manufacturer or the reseller.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogSupplier', @level2type = N'COLUMN', @level2name = N'SupplierName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the abbreviated version of the supplier''s name is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogSupplier', @level2type = N'COLUMN', @level2name = N'SupplierShortName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field for the supplier to indicate they take the order by fax.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogSupplier', @level2type = N'COLUMN', @level2name = N'FaxOrderPlacement';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field for the supplier to indicate they take the order by e-mail.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogSupplier', @level2type = N'COLUMN', @level2name = N'EmailOrderPlacement';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag to verify that the supplier takes the order by email.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogSupplier', @level2type = N'COLUMN', @level2name = N'IsEmailOrderPlacement';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field to define the order in which the suppliers will be displayed.  In this case, Breg will be at the top.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogSupplier', @level2type = N'COLUMN', @level2name = N'Sequence';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in master catalog is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogSupplier', @level2type = N'COLUMN', @level2name = N'IsActive';

