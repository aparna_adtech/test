﻿CREATE TABLE [dbo].[CampaignClickThrough] (
    [CampaignId]      INT      NOT NULL,
    [UserId]          INT      NOT NULL,
    [CreatedDateTime] DATETIME CONSTRAINT [DF_CampaignClickThrough_CreatedDateTime] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CampaignClickThrough_1] PRIMARY KEY CLUSTERED ([CampaignId] ASC, [UserId] ASC, [CreatedDateTime] ASC),
    CONSTRAINT [FK_CampaignClickThrough_Campaign] FOREIGN KEY ([CampaignId]) REFERENCES [dbo].[Campaign] ([Id]),
    CONSTRAINT [FK_CampaignClickThrough_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserID])
);

