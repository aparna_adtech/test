﻿CREATE TABLE [dbo].[FaxServer] (
    [FaxServerID] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FaxEnabled]  BIT          CONSTRAINT [DF_FaxServer_FaxEnabled] DEFAULT ((1)) NOT NULL,
    [Password]    VARCHAR (50) CONSTRAINT [DF_FaxServer_Password] DEFAULT ('#1limes') NOT NULL,
    CONSTRAINT [PK_FaxServer] PRIMARY KEY CLUSTERED ([FaxServerID] ASC)
);

