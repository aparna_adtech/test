﻿CREATE TABLE [dbo].[zzzSalesReportExclusions] (
    [ExclusionID]                    INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PracticeCatalogSupplierBrandID] INT          NOT NULL,
    [SupplierShortName]              VARCHAR (50) NULL,
    [BrandShortName]                 VARCHAR (50) NULL,
    [IsExcluded]                     BIT          CONSTRAINT [DF_zzzSalesReportExclusions_IsExcluded] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_zzzSalesReportExclusions] PRIMARY KEY CLUSTERED ([ExclusionID] ASC)
);

