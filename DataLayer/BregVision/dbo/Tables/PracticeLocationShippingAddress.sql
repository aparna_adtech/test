﻿CREATE TABLE [dbo].[PracticeLocationShippingAddress] (
    [PracticeLocationID]  INT          NOT NULL,
    [AddressID]           INT          NOT NULL,
    [AttentionOf]         VARCHAR (50) NULL,
    [CreatedUserID]       INT          NOT NULL,
    [CreatedDate]         DATETIME     NOT NULL,
    [ModifiedUserID]      INT          NULL,
    [ModifiedDate]        DATETIME     NULL,
    [IsActive]            BIT          NOT NULL,
    [BregOracleID]        VARCHAR (50) NULL,
    [BregOracleIDInvalid] BIT          DEFAULT ((0)) NULL,
    CONSTRAINT [ShippingAddress_PK] PRIMARY KEY CLUSTERED ([AddressID] ASC, [PracticeLocationID] ASC),
    CONSTRAINT [Address_ShippingAddress_FK1] FOREIGN KEY ([AddressID]) REFERENCES [dbo].[Address] ([AddressID]),
    CONSTRAINT [PracticeLocation_ShippingAddress_FK1] FOREIGN KEY ([PracticeLocationID]) REFERENCES [dbo].[PracticeLocation] ([PracticeLocationID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the Practice Location Shipping Address table.  This table links directly to the Practice ID table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeLocationShippingAddress', @level2type = N'COLUMN', @level2name = N'PracticeLocationID';

