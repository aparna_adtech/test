﻿CREATE TABLE [dbo].[DispenseSignature] (
    [DispenseSignatureID]            INT        IDENTITY (1, 1) NOT NULL,
    [PhysicianSigned]                BIT        NULL,
    [PhysicianSignedDate]            DATETIME   NULL,
    [PatientSignature]               IMAGE      NULL,
    [CreatedUserID]                  INT        DEFAULT ((1)) NOT NULL,
    [CreatedDate]                    DATETIME   DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]                 INT        NULL,
    [ModifiedDate]                   DATETIME   NULL,
    [IsActive]                       BIT        DEFAULT ((1)) NOT NULL,
    [timestamp]                      ROWVERSION NOT NULL,
    [PatientSignedDate]              DATETIME   NULL,
    [PatientSignatureRelationshipId] INT        NULL,
    CONSTRAINT [PK_DispenseSignature] PRIMARY KEY CLUSTERED ([DispenseSignatureID] ASC),
    FOREIGN KEY ([PatientSignatureRelationshipId]) REFERENCES [dbo].[PatientSignatureRelationship] ([Id])
);

