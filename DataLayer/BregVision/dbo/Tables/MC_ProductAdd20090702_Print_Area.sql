﻿CREATE TABLE [dbo].[MC_ProductAdd20090702$Print_Area] (
    [Master Catalog Supplier ID]    FLOAT (53)     NULL,
    [Supplier Name]                 NVARCHAR (255) NULL,
    [Master Catalog Category ID]    FLOAT (53)     NULL,
    [Category Name]                 NVARCHAR (255) NULL,
    [Master Catalog Subcategory ID] FLOAT (53)     NULL,
    [Sub Category Name]             NVARCHAR (255) NULL,
    [ProductNumber]                 NVARCHAR (255) NULL,
    [ProductName]                   NVARCHAR (255) NULL,
    [ProductShortName]              NVARCHAR (255) NULL,
    [Packaging]                     NVARCHAR (255) NULL,
    [LeftRightSide]                 NVARCHAR (255) NULL,
    [Size]                          NVARCHAR (255) NULL,
    [Color]                         NVARCHAR (255) NULL,
    [Gender]                        NVARCHAR (255) NULL,
    [ProductCost]                   MONEY          NULL,
    [Source]                        NVARCHAR (255) NULL
);

