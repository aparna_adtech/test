﻿CREATE TABLE [dbo].[PayPalPostLog] (
    [PayPalPostLogID]  INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Request]          VARCHAR (MAX) NOT NULL,
    [Item_Number]      VARCHAR (10)  NULL,
    [Transaction_Type] VARCHAR (30)  NULL,
    [CreatedUserID]    INT           DEFAULT ((1)) NOT NULL,
    [CreatedDate]      DATETIME      DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]   INT           NULL,
    [ModifiedDate]     DATETIME      NULL,
    [IsActive]         BIT           DEFAULT ((1)) NOT NULL,
    [timestamp]        ROWVERSION    NOT NULL,
    CONSTRAINT [PK_PayPalPostLog] PRIMARY KEY CLUSTERED ([PayPalPostLogID] ASC)
);

