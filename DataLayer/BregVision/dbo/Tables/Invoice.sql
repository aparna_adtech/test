﻿CREATE TABLE [dbo].[Invoice] (
    [InvoiceID]               INT          IDENTITY (1, 1) NOT NULL,
    [PracticeID]              INT          NOT NULL,
    [MasterCatalogSupplierID] INT          NULL,
    [ThirdPartySupplierID]    INT          NULL,
    [Number]                  VARCHAR (50) NOT NULL,
    [SalesTax]                SMALLMONEY   NULL,
    [ShippingCost]            SMALLMONEY   NULL,
    [Total]                   SMALLMONEY   NULL,
    [Date]                    DATETIME     NULL,
    [IsPaid]                  BIT          CONSTRAINT [DF_Invoice_IsPaid] DEFAULT ((0)) NOT NULL,
    [IsActive]                BIT          CONSTRAINT [DF_Invoice_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedDate]             DATETIME     CONSTRAINT [DF_Invoice_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [CreatedUserID]           INT          NOT NULL,
    [ModifiedDate]            DATETIME     NULL,
    [ModifiedUserID]          INT          NULL,
    [IsExported]              BIT          CONSTRAINT [DF_Invoice_IsExported] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Invoice] PRIMARY KEY CLUSTERED ([InvoiceID] ASC),
    CONSTRAINT [FK_Invoice_MasterCatSupplier] FOREIGN KEY ([MasterCatalogSupplierID]) REFERENCES [dbo].[MasterCatalogSupplier] ([MasterCatalogSupplierID]),
    CONSTRAINT [FK_Invoice_Practice] FOREIGN KEY ([PracticeID]) REFERENCES [dbo].[Practice] ([PracticeID]),
    CONSTRAINT [FK_Invoice_ThirdPartySupplier] FOREIGN KEY ([ThirdPartySupplierID]) REFERENCES [dbo].[ThirdPartySupplier] ([ThirdPartySupplierID])
);

