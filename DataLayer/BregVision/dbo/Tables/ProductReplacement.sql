﻿CREATE TABLE [dbo].[ProductReplacement] (
    [ProductReplacementID]      INT      IDENTITY (1, 1) NOT NULL,
    [OldMasterCatalogProductID] INT      NULL,
    [OldThirdPartyProductID]    INT      NULL,
    [NewMasterCatalogProductID] INT      NULL,
    [NewThirdPartyProductID]    INT      NULL,
    [PracticeID]                INT      NULL,
    [StartDate]                 DATETIME NULL,
    [EndDate]                   DATETIME NULL,
    [CreatedUserID]             INT      NOT NULL,
    [CreatedDate]               DATETIME NOT NULL,
    [ModifiedUserID]            INT      NULL,
    [ModifiedDate]              DATETIME NULL,
    [IsActive]                  BIT      NOT NULL,
    CONSTRAINT [PK_ProductReplacement] PRIMARY KEY CLUSTERED ([ProductReplacementID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_ProductReplacement_NewMasterCatalogProductID]
    ON [dbo].[ProductReplacement]([NewMasterCatalogProductID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ProductReplacement_NewThirdPartyProductID]
    ON [dbo].[ProductReplacement]([NewThirdPartyProductID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ProductReplacement_OldMasterCatalogProductID]
    ON [dbo].[ProductReplacement]([OldMasterCatalogProductID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ProductReplacement_OldThirdPartyProductID]
    ON [dbo].[ProductReplacement]([OldThirdPartyProductID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ProductReplacement_PracticeID]
    ON [dbo].[ProductReplacement]([PracticeID] ASC);

