﻿CREATE TABLE [dbo].[BregVisionMonthlyFee] (
    [MinTransactions]             INT          NOT NULL,
    [MaxTransactions]             INT          NOT NULL,
    [MonthlyFee]                  SMALLMONEY   NOT NULL,
    [ServiceCode]                 VARCHAR (50) NULL,
    [BregVisionMonthlyFeeID]      INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [BregVisionMonthlyFeeLevelID] INT          NOT NULL,
    CONSTRAINT [PK_BregVisionMonthlyFee] PRIMARY KEY CLUSTERED ([BregVisionMonthlyFeeID] ASC),
    CONSTRAINT [FK_BregVisionMonthlyFee_BregVisionMonthlyFeeLevel] FOREIGN KEY ([BregVisionMonthlyFeeLevelID]) REFERENCES [dbo].[BregVisionMonthlyFeeLevel] ([BregVisionMonthlyFeeLevelID])
);

