﻿CREATE TABLE [dbo].[MasterCatalogProduct] (
    [MasterCatalogProductID]     INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MasterCatalogSupplierID]    INT           NULL,
    [MasterCatalogSubCategoryID] INT           NOT NULL,
    [Code]                       VARCHAR (50)  NOT NULL,
    [Name]                       VARCHAR (100) NOT NULL,
    [ShortName]                  VARCHAR (50)  NOT NULL,
    [Packaging]                  VARCHAR (50)  NULL,
    [LeftRightSide]              VARCHAR (10)  NULL,
    [Size]                       VARCHAR (50)  NULL,
    [Color]                      VARCHAR (50)  NULL,
    [Gender]                     VARCHAR (10)  NULL,
    [WholesaleListCost]          SMALLMONEY    NULL,
    [Description]                VARCHAR (200) NULL,
    [IsDiscontinued]             BIT           CONSTRAINT [DF_MasterCatalogProduct_IsDiscontinued] DEFAULT ((0)) NOT NULL,
    [DateDiscontinued]           DATETIME      NULL,
    [CreatedUserID]              INT           NOT NULL,
    [CreatedDate]                DATETIME      CONSTRAINT [DF_MasterCatalogProduct_CreatedDate_1] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]             INT           NULL,
    [ModifiedDate]               DATETIME      NULL,
    [IsActive]                   BIT           NOT NULL,
    [IsCustomBrace]              BIT           DEFAULT ((0)) NOT NULL,
    [UPCCode]                    VARCHAR (50)  NULL,
    [isCustomBraceAccessory]     BIT           DEFAULT ((0)) NOT NULL,
    [IsLogoAblePart]             BIT           DEFAULT ((0)) NOT NULL,
    [Mod1]                       VARCHAR (2)   NULL,
    [Mod2]                       VARCHAR (2)   NULL,
    [Mod3]                       VARCHAR (2)   NULL,
    [BCSInventoryItemID]         INT           NULL,
    CONSTRAINT [MasterCatalogProduct_PK] PRIMARY KEY CLUSTERED ([MasterCatalogProductID] ASC),
    CONSTRAINT [FK_MasterCatalogProduct_MasterCatalogSubCategory] FOREIGN KEY ([MasterCatalogSubCategoryID]) REFERENCES [dbo].[MasterCatalogSubCategory] ([MasterCatalogSubCategoryID])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_117]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogSubCategoryID] ASC, [IsActive] ASC)
    INCLUDE([MasterCatalogProductID], [Code], [ShortName], [Packaging], [LeftRightSide], [Size], [Color], [Gender], [WholesaleListCost]);


GO
CREATE NONCLUSTERED INDEX [missing_index_65]
    ON [dbo].[MasterCatalogProduct]([Code] ASC)
    INCLUDE([MasterCatalogProductID], [Name], [Packaging], [LeftRightSide], [Size], [Gender]);


GO
CREATE NONCLUSTERED INDEX [missing_index_106]
    ON [dbo].[MasterCatalogProduct]([IsActive] ASC, [Code] ASC)
    INCLUDE([MasterCatalogProductID], [ShortName]);


GO
CREATE NONCLUSTERED INDEX [missing_index_42]
    ON [dbo].[MasterCatalogProduct]([Code] ASC)
    INCLUDE([MasterCatalogProductID], [MasterCatalogSubCategoryID], [Name], [Packaging], [LeftRightSide], [Size], [Gender]);


GO
CREATE NONCLUSTERED INDEX [missing_index_350]
    ON [dbo].[MasterCatalogProduct]([IsActive] ASC)
    INCLUDE([MasterCatalogProductID], [Code], [ShortName]);


GO
CREATE NONCLUSTERED INDEX [missing_index_325]
    ON [dbo].[MasterCatalogProduct]([IsActive] ASC, [Code] ASC)
    INCLUDE([MasterCatalogProductID], [ShortName], [LeftRightSide], [Size], [Gender]);


GO
CREATE NONCLUSTERED INDEX [missing_index_359]
    ON [dbo].[MasterCatalogProduct]([IsActive] ASC, [Code] ASC)
    INCLUDE([MasterCatalogProductID], [Name], [LeftRightSide], [Size]);


GO
CREATE NONCLUSTERED INDEX [missing_index_114]
    ON [dbo].[MasterCatalogProduct]([IsActive] ASC)
    INCLUDE([MasterCatalogProductID], [Code], [ShortName], [LeftRightSide], [Size]);


GO
CREATE NONCLUSTERED INDEX [missing_index_17]
    ON [dbo].[MasterCatalogProduct]([IsActive] ASC)
    INCLUDE([MasterCatalogProductID], [Code], [Name], [LeftRightSide], [Size]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MasterCatalogProduct_8_1730821228__K20_K5_K6_K1_K4_K8_K9_K11]
    ON [dbo].[MasterCatalogProduct]([IsActive] ASC, [Name] ASC, [ShortName] ASC, [MasterCatalogProductID] ASC, [Code] ASC, [LeftRightSide] ASC, [Size] ASC, [Gender] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MasterCatalogProduct_8_1730821228__K20_K1_K5_K4_K7_K8_K9_K11_3]
    ON [dbo].[MasterCatalogProduct]([IsActive] ASC, [MasterCatalogProductID] ASC, [Name] ASC, [Code] ASC, [Packaging] ASC, [LeftRightSide] ASC, [Size] ASC, [Gender] ASC)
    INCLUDE([MasterCatalogSubCategoryID]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MasterCatalogProduct_8_1730821228__K20_K1_K5_K3_4]
    ON [dbo].[MasterCatalogProduct]([IsActive] ASC, [MasterCatalogProductID] ASC, [Name] ASC, [MasterCatalogSubCategoryID] ASC)
    INCLUDE([Code]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MasterCatalogProduct_8_1730821228__K20_K6_K5_K1]
    ON [dbo].[MasterCatalogProduct]([IsActive] ASC, [ShortName] ASC, [Name] ASC, [MasterCatalogProductID] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MasterCatalogProduct_12_1730821228__K1_4_5_6_8_9_11_20]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogProductID] ASC)
    INCLUDE([Code], [Name], [ShortName], [LeftRightSide], [Size], [Gender], [IsActive]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MasterCatalogProduct_12_1730821228__K20_K1_K5_K6_K4_K7_K9_K10_K11]
    ON [dbo].[MasterCatalogProduct]([IsActive] ASC, [MasterCatalogProductID] ASC, [Name] ASC, [ShortName] ASC, [Code] ASC, [Packaging] ASC, [Size] ASC, [Color] ASC, [Gender] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MasterCatalogProduct_12_1730821228__K1_K3_K5_K4_K8_K9]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogProductID] ASC, [MasterCatalogSubCategoryID] ASC, [Name] ASC, [Code] ASC, [LeftRightSide] ASC, [Size] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_MasterCatalogProduct_12_1730821228__K3_K1_K5_K4_K8_K9]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogSubCategoryID] ASC, [MasterCatalogProductID] ASC, [Name] ASC, [Code] ASC, [LeftRightSide] ASC, [Size] ASC);


GO
CREATE NONCLUSTERED INDEX [BCSInventoryItemID_Index]
    ON [dbo].[MasterCatalogProduct]([BCSInventoryItemID] ASC);


GO
CREATE STATISTICS [_dta_stat_1730821228_3_4]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogSubCategoryID], [Code]);


GO
CREATE STATISTICS [_dta_stat_1730821228_7_11]
    ON [dbo].[MasterCatalogProduct]([Packaging], [Gender]);


GO
CREATE STATISTICS [_dta_stat_1730821228_1_10]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogProductID], [Color]);


GO
CREATE STATISTICS [_dta_stat_1730821228_3_6]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogSubCategoryID], [ShortName]);


GO
CREATE STATISTICS [_dta_stat_1730821228_5_6]
    ON [dbo].[MasterCatalogProduct]([Name], [ShortName]);


GO
CREATE STATISTICS [_dta_stat_1730821228_1_11]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogProductID], [Gender]);


GO
CREATE STATISTICS [_dta_stat_1730821228_5_4_20]
    ON [dbo].[MasterCatalogProduct]([Name], [Code], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_1730821228_1_3_5]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogProductID], [MasterCatalogSubCategoryID], [Name]);


GO
CREATE STATISTICS [_dta_stat_1730821228_6_4_20]
    ON [dbo].[MasterCatalogProduct]([ShortName], [Code], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_1730821228_1_7_11]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogProductID], [Packaging], [Gender]);


GO
CREATE STATISTICS [_dta_stat_1730821228_5_4_6_3]
    ON [dbo].[MasterCatalogProduct]([Name], [Code], [ShortName], [MasterCatalogSubCategoryID]);


GO
CREATE STATISTICS [_dta_stat_1730821228_20_6_5_4]
    ON [dbo].[MasterCatalogProduct]([IsActive], [ShortName], [Name], [Code]);


GO
CREATE STATISTICS [_dta_stat_1730821228_3_20_6_5]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogSubCategoryID], [IsActive], [ShortName], [Name]);


GO
CREATE STATISTICS [_dta_stat_1730821228_1_3_20_5]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogProductID], [MasterCatalogSubCategoryID], [IsActive], [Name]);


GO
CREATE STATISTICS [_dta_stat_1730821228_20_6_4_8_9]
    ON [dbo].[MasterCatalogProduct]([IsActive], [ShortName], [Code], [LeftRightSide], [Size]);


GO
CREATE STATISTICS [_dta_stat_1730821228_5_4_8_9_1]
    ON [dbo].[MasterCatalogProduct]([Name], [Code], [LeftRightSide], [Size], [MasterCatalogProductID]);


GO
CREATE STATISTICS [_dta_stat_1730821228_1_6_4_8_9]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogProductID], [ShortName], [Code], [LeftRightSide], [Size]);


GO
CREATE STATISTICS [_dta_stat_1730821228_6_4_8_9_11_20]
    ON [dbo].[MasterCatalogProduct]([ShortName], [Code], [LeftRightSide], [Size], [Gender], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_1730821228_5_4_8_9_20_7]
    ON [dbo].[MasterCatalogProduct]([Name], [Code], [LeftRightSide], [Size], [IsActive], [Packaging]);


GO
CREATE STATISTICS [_dta_stat_1730821228_1_20_4_8_9_11]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogProductID], [IsActive], [Code], [LeftRightSide], [Size], [Gender]);


GO
CREATE STATISTICS [_dta_stat_1730821228_5_4_3_1_8_9]
    ON [dbo].[MasterCatalogProduct]([Name], [Code], [MasterCatalogSubCategoryID], [MasterCatalogProductID], [LeftRightSide], [Size]);


GO
CREATE STATISTICS [_dta_stat_1730821228_5_4_7_8_9_11_6]
    ON [dbo].[MasterCatalogProduct]([Name], [Code], [Packaging], [LeftRightSide], [Size], [Gender], [ShortName]);


GO
CREATE STATISTICS [_dta_stat_1730821228_5_4_8_9_11_20_6]
    ON [dbo].[MasterCatalogProduct]([Name], [Code], [LeftRightSide], [Size], [Gender], [IsActive], [ShortName]);


GO
CREATE STATISTICS [_dta_stat_1730821228_20_5_4_1_8_9_11]
    ON [dbo].[MasterCatalogProduct]([IsActive], [Name], [Code], [MasterCatalogProductID], [LeftRightSide], [Size], [Gender]);


GO
CREATE STATISTICS [_dta_stat_1730821228_6_4_7_8_9_11_20]
    ON [dbo].[MasterCatalogProduct]([ShortName], [Code], [Packaging], [LeftRightSide], [Size], [Gender], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_1730821228_1_20_6_4_7_8_9_11]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogProductID], [IsActive], [ShortName], [Code], [Packaging], [LeftRightSide], [Size], [Gender]);


GO
CREATE STATISTICS [_dta_stat_1730821228_5_4_7_8_9_11_10_1]
    ON [dbo].[MasterCatalogProduct]([Name], [Code], [Packaging], [LeftRightSide], [Size], [Gender], [Color], [MasterCatalogProductID]);


GO
CREATE STATISTICS [_dta_stat_1730821228_4_8_9_11_20_3_5_7]
    ON [dbo].[MasterCatalogProduct]([Code], [LeftRightSide], [Size], [Gender], [IsActive], [MasterCatalogSubCategoryID], [Name], [Packaging]);


GO
CREATE STATISTICS [_dta_stat_1730821228_20_5_4_1_7_8_9_11]
    ON [dbo].[MasterCatalogProduct]([IsActive], [Name], [Code], [MasterCatalogProductID], [Packaging], [LeftRightSide], [Size], [Gender]);


GO
CREATE STATISTICS [_dta_stat_1730821228_1_5_6_4_7_8_9_11]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogProductID], [Name], [ShortName], [Code], [Packaging], [LeftRightSide], [Size], [Gender]);


GO
CREATE STATISTICS [_dta_stat_1730821228_5_4_7_8_9_11_20_6]
    ON [dbo].[MasterCatalogProduct]([Name], [Code], [Packaging], [LeftRightSide], [Size], [Gender], [IsActive], [ShortName]);


GO
CREATE STATISTICS [_dta_stat_1730821228_20_4_1_8_9_11_3_5]
    ON [dbo].[MasterCatalogProduct]([IsActive], [Code], [MasterCatalogProductID], [LeftRightSide], [Size], [Gender], [MasterCatalogSubCategoryID], [Name]);


GO
CREATE STATISTICS [_dta_stat_1730821228_5_4_7_8_9_11_10_6]
    ON [dbo].[MasterCatalogProduct]([Name], [Code], [Packaging], [LeftRightSide], [Size], [Gender], [Color], [ShortName]);


GO
CREATE STATISTICS [_dta_stat_1730821228_4_6_7_8_9_10_11_20]
    ON [dbo].[MasterCatalogProduct]([Code], [ShortName], [Packaging], [LeftRightSide], [Size], [Color], [Gender], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_1730821228_5_4_7_8_9_11_10_20]
    ON [dbo].[MasterCatalogProduct]([Name], [Code], [Packaging], [LeftRightSide], [Size], [Gender], [Color], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_1730821228_1_20_3_5_4_7_8_9_11]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogProductID], [IsActive], [MasterCatalogSubCategoryID], [Name], [Code], [Packaging], [LeftRightSide], [Size], [Gender]);


GO
CREATE STATISTICS [_dta_stat_1730821228_1_20_5_4_7_8_9_11_10]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogProductID], [IsActive], [Name], [Code], [Packaging], [LeftRightSide], [Size], [Gender], [Color]);


GO
CREATE STATISTICS [_dta_stat_1730821228_3_1_5_4_7_8_9_11_10]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogSubCategoryID], [MasterCatalogProductID], [Name], [Code], [Packaging], [LeftRightSide], [Size], [Gender], [Color]);


GO
CREATE STATISTICS [_dta_stat_1730821228_4_6_20_1_7_8_9_10_11]
    ON [dbo].[MasterCatalogProduct]([Code], [ShortName], [IsActive], [MasterCatalogProductID], [Packaging], [LeftRightSide], [Size], [Color], [Gender]);


GO
CREATE STATISTICS [_dta_stat_1730821228_6_5_3_1_4_7_8_9_11_10]
    ON [dbo].[MasterCatalogProduct]([ShortName], [Name], [MasterCatalogSubCategoryID], [MasterCatalogProductID], [Code], [Packaging], [LeftRightSide], [Size], [Gender], [Color]);


GO
CREATE STATISTICS [_dta_stat_1730821228_20_6_5_1_3_4_7_8_9_11]
    ON [dbo].[MasterCatalogProduct]([IsActive], [ShortName], [Name], [MasterCatalogProductID], [MasterCatalogSubCategoryID], [Code], [Packaging], [LeftRightSide], [Size], [Gender]);


GO
CREATE STATISTICS [_dta_stat_1730821228_1_21]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogProductID], [IsCustomBrace]);


GO
CREATE STATISTICS [_dta_stat_1730821228_3_20_4]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogSubCategoryID], [IsActive], [Code]);


GO
CREATE STATISTICS [_dta_stat_1730821228_3_1_21]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogSubCategoryID], [MasterCatalogProductID], [IsCustomBrace]);


GO
CREATE STATISTICS [_dta_stat_1730821228_5_7_10_11]
    ON [dbo].[MasterCatalogProduct]([Name], [Packaging], [Color], [Gender]);


GO
CREATE STATISTICS [_dta_stat_1730821228_1_5_7_10_11]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogProductID], [Name], [Packaging], [Color], [Gender]);


GO
CREATE STATISTICS [_dta_stat_1730821228_1_20_6_4_9_5]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogProductID], [IsActive], [ShortName], [Code], [Size], [Name]);


GO
CREATE STATISTICS [_dta_stat_1730821228_6_4_9_20_5_7_10]
    ON [dbo].[MasterCatalogProduct]([ShortName], [Code], [Size], [IsActive], [Name], [Packaging], [Color]);


GO
CREATE STATISTICS [_dta_stat_1730821228_4_7_8_9_11_10_20]
    ON [dbo].[MasterCatalogProduct]([Code], [Packaging], [LeftRightSide], [Size], [Gender], [Color], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_1730821228_1_3_4_7_8_9_11_10]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogProductID], [MasterCatalogSubCategoryID], [Code], [Packaging], [LeftRightSide], [Size], [Gender], [Color]);


GO
CREATE STATISTICS [_dta_stat_1730821228_5_6_4_7_9_10_11_20]
    ON [dbo].[MasterCatalogProduct]([Name], [ShortName], [Code], [Packaging], [Size], [Color], [Gender], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_1730821228_20_5_6_4_1_7_9_10_11]
    ON [dbo].[MasterCatalogProduct]([IsActive], [Name], [ShortName], [Code], [MasterCatalogProductID], [Packaging], [Size], [Color], [Gender]);


GO
CREATE STATISTICS [_dta_stat_1730821228_5_4_7_8_9_11_10_20_3]
    ON [dbo].[MasterCatalogProduct]([Name], [Code], [Packaging], [LeftRightSide], [Size], [Gender], [Color], [IsActive], [MasterCatalogSubCategoryID]);


GO
CREATE STATISTICS [_dta_stat_1730821228_20_3_1_4_7_8_9_11_10]
    ON [dbo].[MasterCatalogProduct]([IsActive], [MasterCatalogSubCategoryID], [MasterCatalogProductID], [Code], [Packaging], [LeftRightSide], [Size], [Gender], [Color]);


GO
CREATE STATISTICS [_dta_stat_1730821228_3_1_20_5_4_7_8_9_11_10]
    ON [dbo].[MasterCatalogProduct]([MasterCatalogSubCategoryID], [MasterCatalogProductID], [IsActive], [Name], [Code], [Packaging], [LeftRightSide], [Size], [Gender], [Color]);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the Master Catalog Product table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogProduct', @level2type = N'COLUMN', @level2name = N'MasterCatalogProductID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which a unique number is entered into for each product in the sub-category.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogProduct', @level2type = N'COLUMN', @level2name = N'Code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the name of the product is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogProduct', @level2type = N'COLUMN', @level2name = N'Name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the abbreviated name of the product is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogProduct', @level2type = N'COLUMN', @level2name = N'ShortName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the quantity of the package is described (ea., qty., gal.)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogProduct', @level2type = N'COLUMN', @level2name = N'Packaging';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the side the product is meant for is entered into ( L,R,U)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogProduct', @level2type = N'COLUMN', @level2name = N'LeftRightSide';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the size of the product is entered into (XXS - XXXXS).', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogProduct', @level2type = N'COLUMN', @level2name = N'Size';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the gender the product is meant for is entered into (F,M,U).', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogProduct', @level2type = N'COLUMN', @level2name = N'Gender';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the supplier''s wholesale price is entered into (as shown in the master catalog).', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogProduct', @level2type = N'COLUMN', @level2name = N'WholesaleListCost';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the description of the product is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogProduct', @level2type = N'COLUMN', @level2name = N'Description';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in master product catalog is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogProduct', @level2type = N'COLUMN', @level2name = N'IsActive';

