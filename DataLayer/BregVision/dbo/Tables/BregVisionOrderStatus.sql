﻿CREATE TABLE [dbo].[BregVisionOrderStatus] (
    [BregVisionOrderStatusID] TINYINT      NOT NULL,
    [Status]                  VARCHAR (50) NOT NULL,
    CONSTRAINT [BregVisionOrderStatus_PK] PRIMARY KEY CLUSTERED ([BregVisionOrderStatusID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the Breg Vision Order Status', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BregVisionOrderStatus', @level2type = N'COLUMN', @level2name = N'BregVisionOrderStatusID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the status of the Breg Vision order will show ordered, checked-in or partially checked-in.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BregVisionOrderStatus', @level2type = N'COLUMN', @level2name = N'Status';

