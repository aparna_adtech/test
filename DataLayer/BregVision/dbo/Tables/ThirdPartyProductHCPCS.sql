﻿CREATE TABLE [dbo].[ThirdPartyProductHCPCS] (
    [ThirdPartyProductHCPCSID] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ThirdPartyProductID]      INT          NOT NULL,
    [HCPCS]                    VARCHAR (20) NOT NULL,
    [CreatedUserID]            INT          NOT NULL,
    [CreatedDate]              DATETIME     NOT NULL,
    [ModifiedUserID]           INT          NULL,
    [ModifiedDate]             DATETIME     NULL,
    [IsActive]                 BIT          NOT NULL,
    CONSTRAINT [ThirdPartyProductHCPCS_PK] PRIMARY KEY CLUSTERED ([ThirdPartyProductHCPCSID] ASC),
    CONSTRAINT [ThirdPartyProduct_ThirdPartyProductHCPCS_FK1] FOREIGN KEY ([ThirdPartyProductID]) REFERENCES [dbo].[ThirdPartyProduct] ([ThirdPartyProductID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for theThird-Party Product Healthcare Common Procedure Coding System (HCPCS) table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartyProductHCPCS', @level2type = N'COLUMN', @level2name = N'ThirdPartyProductHCPCSID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the HCPCS code is entered for third-party products that have one.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartyProductHCPCS', @level2type = N'COLUMN', @level2name = N'HCPCS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in the Third-Party Prodcut HCPCS is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartyProductHCPCS', @level2type = N'COLUMN', @level2name = N'IsActive';

