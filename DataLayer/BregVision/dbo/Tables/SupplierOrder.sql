﻿CREATE TABLE [dbo].[SupplierOrder] (
    [SupplierOrderID]                INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [BregVisionOrderID]              INT          NOT NULL,
    [PracticeCatalogSupplierBrandID] INT          NOT NULL,
    [SupplierOrderStatusID]          TINYINT      NULL,
    [PurchaseOrder]                  INT          NOT NULL,
    [CustomPurchaseOrderCode]        VARCHAR (75) NULL,
    [Total]                          SMALLMONEY   NULL,
    [CreatedUserID]                  INT          NOT NULL,
    [CreatedDate]                    DATETIME     CONSTRAINT [DF__SupplierO__Creat__656C112C] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]                 INT          NULL,
    [ModifiedDate]                   DATETIME     CONSTRAINT [DF_SupplierOrder_ModifiedDate] DEFAULT (getdate()) NULL,
    [IsActive]                       BIT          NOT NULL,
    [InvoicePaid]                    BIT          DEFAULT ((0)) NOT NULL,
    [InvoiceNumber]                  VARCHAR (50) NULL,
    CONSTRAINT [SupplierOrder_PK] PRIMARY KEY CLUSTERED ([SupplierOrderID] ASC),
    CONSTRAINT [BregVisionOrder_SupplierOrder_FK1] FOREIGN KEY ([BregVisionOrderID]) REFERENCES [dbo].[BregVisionOrder] ([BregVisionOrderID]),
    CONSTRAINT [PracticeCatalogSupplierBrand_SupplierOrder_FK1] FOREIGN KEY ([PracticeCatalogSupplierBrandID]) REFERENCES [dbo].[PracticeCatalogSupplierBrand] ([PracticeCatalogSupplierBrandID]),
    CONSTRAINT [SupplierOrderStatus_SupplierOrder_FK1] FOREIGN KEY ([SupplierOrderStatusID]) REFERENCES [dbo].[SupplierOrderStatus] ([SupplierOrderStatusID])
);


GO
CREATE NONCLUSTERED INDEX [_dta_index_SupplierOrder_8_1275867612__K3_K2_K12_K1_7]
    ON [dbo].[SupplierOrder]([PracticeCatalogSupplierBrandID] ASC, [BregVisionOrderID] ASC, [IsActive] ASC, [SupplierOrderID] ASC)
    INCLUDE([Total]);


GO
CREATE STATISTICS [_dta_stat_1275867612_2_4]
    ON [dbo].[SupplierOrder]([BregVisionOrderID], [SupplierOrderStatusID]);


GO
CREATE STATISTICS [_dta_stat_1275867612_4_1]
    ON [dbo].[SupplierOrder]([SupplierOrderStatusID], [SupplierOrderID]);


GO
CREATE STATISTICS [_dta_stat_1275867612_12_1]
    ON [dbo].[SupplierOrder]([IsActive], [SupplierOrderID]);


GO
CREATE STATISTICS [_dta_stat_1275867612_4_3_2]
    ON [dbo].[SupplierOrder]([SupplierOrderStatusID], [PracticeCatalogSupplierBrandID], [BregVisionOrderID]);


GO
CREATE STATISTICS [_dta_stat_1275867612_2_12_3]
    ON [dbo].[SupplierOrder]([BregVisionOrderID], [IsActive], [PracticeCatalogSupplierBrandID]);


GO
CREATE STATISTICS [_dta_stat_1275867612_12_2_4]
    ON [dbo].[SupplierOrder]([IsActive], [BregVisionOrderID], [SupplierOrderStatusID]);


GO
CREATE STATISTICS [_dta_stat_1275867612_2_12_4_3]
    ON [dbo].[SupplierOrder]([BregVisionOrderID], [IsActive], [SupplierOrderStatusID], [PracticeCatalogSupplierBrandID]);


GO
CREATE STATISTICS [_dta_stat_1275867612_1_3_2_12]
    ON [dbo].[SupplierOrder]([SupplierOrderID], [PracticeCatalogSupplierBrandID], [BregVisionOrderID], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_1275867612_1_2_12_4]
    ON [dbo].[SupplierOrder]([SupplierOrderID], [BregVisionOrderID], [IsActive], [SupplierOrderStatusID]);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the Supplier Order table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SupplierOrder', @level2type = N'COLUMN', @level2name = N'SupplierOrderID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the six-digit generated number (sequential, starting with 100001) is entered into.  It is unique to each purchase order.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SupplierOrder', @level2type = N'COLUMN', @level2name = N'PurchaseOrder';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the user can enter in a custom customer code.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SupplierOrder', @level2type = N'COLUMN', @level2name = N'CustomPurchaseOrderCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the total amount spent in the purchase order is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SupplierOrder', @level2type = N'COLUMN', @level2name = N'Total';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in the Supplier Order is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SupplierOrder', @level2type = N'COLUMN', @level2name = N'IsActive';

