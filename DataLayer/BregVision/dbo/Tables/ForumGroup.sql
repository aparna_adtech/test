﻿CREATE TABLE [dbo].[ForumGroup] (
    [Id]        INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]      NVARCHAR (100) NOT NULL,
    [ParentId]  INT            NULL,
    [Created]   DATETIME       CONSTRAINT [DF_ForumGroup_Created] DEFAULT (getdate()) NOT NULL,
    [SortOrder] INT            CONSTRAINT [DF_ForumGroup_SortOrder] DEFAULT ((0)) NOT NULL,
    [IsActive]  BIT            CONSTRAINT [DF_ForumGroup_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ForumGroup] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ForumGroup_ForumGroup] FOREIGN KEY ([ParentId]) REFERENCES [dbo].[ForumGroup] ([Id])
);

