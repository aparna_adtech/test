﻿CREATE TABLE [dbo].[ExceptionLog] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [Date]      DATETIME       NOT NULL,
    [Thread]    VARCHAR (255)  NOT NULL,
    [Level]     VARCHAR (50)   NOT NULL,
    [Logger]    VARCHAR (255)  NOT NULL,
    [Message]   VARCHAR (4000) NOT NULL,
    [Exception] VARCHAR (2000) NULL,
    CONSTRAINT [PK_ExceptionLog] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_ExceptionLog_Date]
    ON [dbo].[ExceptionLog]([Date] ASC)
    INCLUDE([Id], [Thread], [Level], [Logger], [Message], [Exception]);


GO



CREATE TRIGGER [dbo].[ClassExceptionEmail]
ON [dbo].[ExceptionLog]
FOR INSERT
AS
BEGIN
    SET NOCOUNT ON;

	DECLARE @body NVARCHAR(MAX) = N'';

	SELECT @body += [message] from inserted

	if exists (select 1 from inserted where [message] like '%.cs%')
	begin
        EXEC msdb.dbo.sp_send_dbmail
          @recipients = 'smarks@breg.com;hchan@breg.com',
          @profile_name = 'OrderConfirmation',
          @subject = 'Application error!', 
          @body = @body,
		  @body_format = 'HTML';
	end
END



