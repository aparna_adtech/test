﻿CREATE TABLE [dbo].[PaymentType] (
    [PaymentTypeID]   INT          NOT NULL,
    [PaymentTypeName] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_PaymentType] PRIMARY KEY CLUSTERED ([PaymentTypeID] ASC)
);

