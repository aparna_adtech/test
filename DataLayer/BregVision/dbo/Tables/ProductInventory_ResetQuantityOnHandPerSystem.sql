﻿CREATE TABLE [dbo].[ProductInventory_ResetQuantityOnHandPerSystem] (
    [ProductInventory_ResetQuantityOnHandPerSystemID] INT        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ProductInventoryID]                              INT        NOT NULL,
    [OldQuantityOnHandPerSystem]                      INT        NOT NULL,
    [NewQuantityOnHandPerSystem]                      INT        NOT NULL,
    [QuantityOfShrinkage]                             INT        NOT NULL,
    [ActualWholesaleCost]                             SMALLMONEY NULL,
    [DateReset]                                       DATETIME   NOT NULL,
    [CreatedUserID]                                   INT        NOT NULL,
    [CreatedDate]                                     DATETIME   NOT NULL,
    [ModifiedUserID]                                  INT        NULL,
    [ModifiedDate]                                    DATETIME   NULL,
    [IsActive]                                        BIT        NOT NULL,
    [InventoryCycleID]                                INT        NOT NULL,
    CONSTRAINT [ProductInventory_ResetQuantityOnHandPerSystem_PK] PRIMARY KEY CLUSTERED ([ProductInventory_ResetQuantityOnHandPerSystemID] ASC),
    CONSTRAINT [FK_ProductInventory_ResetQuantityOnHandPerSystem_InventoryCycle] FOREIGN KEY ([InventoryCycleID]) REFERENCES [dbo].[InventoryCycle] ([InventoryCycleID]),
    CONSTRAINT [ProductInventory_ProductInventory_ResetQuantityOnHandPerSystem_FK1] FOREIGN KEY ([ProductInventoryID]) REFERENCES [dbo].[ProductInventory] ([ProductInventoryID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the Product Inventory Reset Quantity On Hand Per System table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_ResetQuantityOnHandPerSystem', @level2type = N'COLUMN', @level2name = N'ProductInventory_ResetQuantityOnHandPerSystemID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the previous count of product is entered into.  This previous count is the number that was in the system, prior to the new physical count.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_ResetQuantityOnHandPerSystem', @level2type = N'COLUMN', @level2name = N'OldQuantityOnHandPerSystem';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the new count of product is entered into.  This new count is due to a physical count of product.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_ResetQuantityOnHandPerSystem', @level2type = N'COLUMN', @level2name = N'NewQuantityOnHandPerSystem';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the difference between the old count and the new count is  entered.  The shrinkage can be due to broken or missing product.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_ResetQuantityOnHandPerSystem', @level2type = N'COLUMN', @level2name = N'QuantityOfShrinkage';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the clinic purchase price, per product unit, is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_ResetQuantityOnHandPerSystem', @level2type = N'COLUMN', @level2name = N'ActualWholesaleCost';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the system automatically enters the date in which the product count was reset.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_ResetQuantityOnHandPerSystem', @level2type = N'COLUMN', @level2name = N'DateReset';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in the Product Inventory Reset Quantity On-hand per System is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_ResetQuantityOnHandPerSystem', @level2type = N'COLUMN', @level2name = N'IsActive';

