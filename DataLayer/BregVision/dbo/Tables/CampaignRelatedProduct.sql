﻿CREATE TABLE [dbo].[CampaignRelatedProduct] (
    [CampaignId]             INT NOT NULL,
    [MasterCatalogProductId] INT NOT NULL,
    CONSTRAINT [PK_CampaignRelatedProduct] PRIMARY KEY CLUSTERED ([CampaignId] ASC, [MasterCatalogProductId] ASC),
    CONSTRAINT [FK_CampaignRelatedProduct_Campaign] FOREIGN KEY ([CampaignId]) REFERENCES [dbo].[Campaign] ([Id]),
    CONSTRAINT [FK_CampaignRelatedProduct_MasterCatalogProduct] FOREIGN KEY ([MasterCatalogProductId]) REFERENCES [dbo].[MasterCatalogProduct] ([MasterCatalogProductID])
);

