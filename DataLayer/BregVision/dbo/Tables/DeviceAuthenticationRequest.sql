﻿CREATE TABLE [dbo].[DeviceAuthenticationRequest] (
    [RequestID]          UNIQUEIDENTIFIER CONSTRAINT [DF_DeviceAuthenticationRequest_RequestID] DEFAULT (newid()) NOT NULL,
    [DeviceIdentifier]   VARCHAR (255)    NOT NULL,
    [PracticeID]         INT              NULL,
    [PracticeLocationID] INT              NULL,
    [CreatedDate]        DATETIME         CONSTRAINT [DF_DeviceAuthenticationRequest_CreatedDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_DeviceAuthenticationRequest] PRIMARY KEY CLUSTERED ([RequestID] ASC)
);

