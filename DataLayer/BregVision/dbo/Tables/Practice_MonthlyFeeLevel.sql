﻿CREATE TABLE [dbo].[Practice_MonthlyFeeLevel] (
    [PracticeID]                  INT             NOT NULL,
    [BregVisionMonthlyFeeLevelID] INT             NOT NULL,
    [StartDate]                   DATETIME        NOT NULL,
    [Discount]                    DECIMAL (10, 4) DEFAULT ((0)) NOT NULL,
    [CreatedUserID]               INT             NOT NULL,
    [CreatedDate]                 DATETIME        NOT NULL,
    [ModifiedUserID]              INT             NULL,
    [ModifiedDate]                DATETIME        NULL,
    [IsActive]                    BIT             NOT NULL,
    CONSTRAINT [PK_Practice_MonthlyFeeLevel] PRIMARY KEY CLUSTERED ([PracticeID] ASC, [BregVisionMonthlyFeeLevelID] ASC, [StartDate] ASC),
    CONSTRAINT [FK_Practice_MonthlyFeeLevel_BregVisionMonthlyFeeLevel] FOREIGN KEY ([BregVisionMonthlyFeeLevelID]) REFERENCES [dbo].[BregVisionMonthlyFeeLevel] ([BregVisionMonthlyFeeLevelID]),
    CONSTRAINT [FK_Practice_MonthlyFeeLevel_Practice] FOREIGN KEY ([PracticeID]) REFERENCES [dbo].[Practice] ([PracticeID])
);

