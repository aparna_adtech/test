﻿CREATE TABLE [dbo].[ShoppingCartItemStatus] (
    [ShoppingCartItemStatusID] TINYINT      NOT NULL,
    [Status]                   VARCHAR (50) NOT NULL,
    CONSTRAINT [ShoppingCartItemStatus_PK] PRIMARY KEY CLUSTERED ([ShoppingCartItemStatusID] ASC)
);

