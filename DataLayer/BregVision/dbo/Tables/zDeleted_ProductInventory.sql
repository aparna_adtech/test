﻿CREATE TABLE [dbo].[zDeleted_ProductInventory] (
    [ProductInventoryID]       INT      IDENTITY (1, 1) NOT NULL,
    [PracticeLocationID]       INT      NOT NULL,
    [PracticeCatalogProductID] INT      NOT NULL,
    [QuantityOnHandPerSystem]  INT      NOT NULL,
    [ParLevel]                 INT      NULL,
    [ReorderLevel]             INT      NULL,
    [CriticalLevel]            INT      NULL,
    [CreatedUserID]            INT      NOT NULL,
    [CreatedDate]              DATETIME NOT NULL,
    [ModifiedUserID]           INT      NULL,
    [ModifiedDate]             DATETIME NULL,
    [IsActive]                 BIT      NOT NULL,
    [DateDeleted]              DATETIME CONSTRAINT [DF_zDeleted_ProductInventory_DataDeleted] DEFAULT (getdate()) NULL
);

