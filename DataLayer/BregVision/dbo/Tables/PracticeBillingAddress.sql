﻿CREATE TABLE [dbo].[PracticeBillingAddress] (
    [PracticeID]     INT          NOT NULL,
    [AddressID]      INT          NOT NULL,
    [AttentionOf]    VARCHAR (50) NULL,
    [CreatedUserID]  INT          NOT NULL,
    [CreatedDate]    DATETIME     CONSTRAINT [DF_PracticeBillingAddress_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID] INT          NULL,
    [ModifiedDate]   DATETIME     NULL,
    [IsActive]       BIT          NOT NULL,
    CONSTRAINT [PracticeBillingAddress_PK] PRIMARY KEY CLUSTERED ([PracticeID] ASC, [AddressID] ASC),
    CONSTRAINT [Address_PracticeBillingAddress_FK1] FOREIGN KEY ([AddressID]) REFERENCES [dbo].[Address] ([AddressID]),
    CONSTRAINT [Practice_PracticeBillingAddress_FK1] FOREIGN KEY ([PracticeID]) REFERENCES [dbo].[Practice] ([PracticeID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which users can enter in who will recieve the bill (accountants, financial department, accounting, etc.)  ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeBillingAddress', @level2type = N'COLUMN', @level2name = N'AttentionOf';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in the practice billing address is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeBillingAddress', @level2type = N'COLUMN', @level2name = N'IsActive';

