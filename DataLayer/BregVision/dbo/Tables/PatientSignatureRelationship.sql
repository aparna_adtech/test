﻿CREATE TABLE [dbo].[PatientSignatureRelationship] (
    [Id]                           INT          NOT NULL,
    [PatientSignatureRelationship] VARCHAR (50) NULL,
    [IsActive]                     BIT          NULL,
    CONSTRAINT [PK_PatientSignatureRelationship] PRIMARY KEY CLUSTERED ([Id] ASC)
);

