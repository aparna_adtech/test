﻿CREATE TABLE [dbo].[Dispense] (
    [DispenseID]          INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PracticeLocationID]  INT           NOT NULL,
    [PatientCode]         VARCHAR (255) NULL,
    [ReceiptCode]         VARCHAR (50)  NULL,
    [Total]               SMALLMONEY    NOT NULL,
    [DateDispensed]       DATETIME      NOT NULL,
    [Comment]             VARCHAR (200) NULL,
    [CreatedUserID]       INT           NOT NULL,
    [CreatedDate]         DATETIME      CONSTRAINT [DF__Dispense__Create__1ED998B2] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]      INT           NULL,
    [ModifiedDate]        DATETIME      NULL,
    [CreatedWithHandheld] BIT           NULL,
    [IsActive]            BIT           NOT NULL,
    [DispenseBatchID]     INT           NULL,
    [PatientEmail]        VARCHAR (50)  NULL,
    [PatientFirstName]    VARCHAR (50)  NULL,
    [PatientLastName]     VARCHAR (50)  NULL,
    [Note]                VARCHAR (250) NULL,
    [PracticePayerID]     INT           NULL,
    [Notes]               AS            ([Note]),
	[DispenseDocumentVersion] TINYINT NULL,
	[DispenseDocumentFeaturesFlags] XML NULL,
    CONSTRAINT [Dispense_PK] PRIMARY KEY CLUSTERED ([DispenseID] ASC),
    CONSTRAINT [FK_Dispense_PracticePayer] FOREIGN KEY ([PracticePayerID]) REFERENCES [dbo].[PracticePayer] ([PracticePayerID]),
    CONSTRAINT [PracticeLocation_Dispense_FK1] FOREIGN KEY ([PracticeLocationID]) REFERENCES [dbo].[PracticeLocation] ([PracticeLocationID])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_397]
    ON [dbo].[Dispense]([PracticeLocationID] ASC, [IsActive] ASC)
    INCLUDE([DispenseID], [DateDispensed]);


GO
CREATE NONCLUSTERED INDEX [missing_index_507]
    ON [dbo].[Dispense]([PracticeLocationID] ASC)
    INCLUDE([DispenseID], [DateDispensed]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_Dispense_8_1000390633__K2_K13_K1_K3_K5_K6_9]
    ON [dbo].[Dispense]([PracticeLocationID] ASC, [IsActive] ASC, [DispenseID] ASC, [PatientCode] ASC, [Total] ASC, [DateDispensed] ASC)
    INCLUDE([CreatedDate]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_Dispense_8_1000390633__K2_K13_K1_K9_K3_K6]
    ON [dbo].[Dispense]([PracticeLocationID] ASC, [IsActive] ASC, [DispenseID] ASC, [CreatedDate] ASC, [PatientCode] ASC, [DateDispensed] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_Dispense_12_1000390633__K13_K2_K1_3]
    ON [dbo].[Dispense]([IsActive] ASC, [PracticeLocationID] ASC, [DispenseID] ASC)
    INCLUDE([PatientCode]);


GO
CREATE NONCLUSTERED INDEX [ix_Dispense_DispenseBatchID]
    ON [dbo].[Dispense]([DispenseBatchID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Dispense_PracticeLocationID]
    ON [dbo].[Dispense]([PracticeLocationID] ASC)
    INCLUDE([DispenseID], [PatientCode], [ReceiptCode], [Total], [DateDispensed], [Comment], [CreatedUserID], [CreatedDate], [ModifiedUserID], [ModifiedDate], [CreatedWithHandheld], [IsActive], [DispenseBatchID], [PatientEmail]);


GO
CREATE NONCLUSTERED INDEX [IX_Dispense_PracticeLocationID_IsActive]
    ON [dbo].[Dispense]([PracticeLocationID] ASC, [IsActive] ASC)
    INCLUDE([DispenseID], [PatientCode], [DateDispensed], [CreatedDate], [PatientFirstName], [PatientLastName], [PracticePayerID]);


GO
CREATE STATISTICS [_dta_stat_1000390633_13_1]
    ON [dbo].[Dispense]([IsActive], [DispenseID]);


GO
CREATE STATISTICS [_dta_stat_1000390633_9_1_2]
    ON [dbo].[Dispense]([CreatedDate], [DispenseID], [PracticeLocationID]);


GO
CREATE STATISTICS [_dta_stat_1000390633_1_9_3_2]
    ON [dbo].[Dispense]([DispenseID], [CreatedDate], [PatientCode], [PracticeLocationID]);


GO
CREATE STATISTICS [_dta_stat_1000390633_1_13_3_5]
    ON [dbo].[Dispense]([DispenseID], [IsActive], [PatientCode], [Total]);


GO
CREATE STATISTICS [_dta_stat_1000390633_1_6_2_13_9]
    ON [dbo].[Dispense]([DispenseID], [DateDispensed], [PracticeLocationID], [IsActive], [CreatedDate]);


GO
CREATE STATISTICS [_dta_stat_1000390633_9_1_3_6_2]
    ON [dbo].[Dispense]([CreatedDate], [DispenseID], [PatientCode], [DateDispensed], [PracticeLocationID]);


GO
CREATE STATISTICS [_dta_stat_1000390633_1_3_5_6_2]
    ON [dbo].[Dispense]([DispenseID], [PatientCode], [Total], [DateDispensed], [PracticeLocationID]);


GO
CREATE STATISTICS [_dta_stat_1000390633_1_2_13_9_3_6]
    ON [dbo].[Dispense]([DispenseID], [PracticeLocationID], [IsActive], [CreatedDate], [PatientCode], [DateDispensed]);


GO
CREATE STATISTICS [_dta_stat_1000390633_1_2_13_3_5_6]
    ON [dbo].[Dispense]([DispenseID], [PracticeLocationID], [IsActive], [PatientCode], [Total], [DateDispensed]);


GO
CREATE STATISTICS [_dta_stat_1000390633_1_14]
    ON [dbo].[Dispense]([DispenseID], [DispenseBatchID]);


GO
CREATE STATISTICS [_dta_stat_1000390633_6_13]
    ON [dbo].[Dispense]([DateDispensed], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_1000390633_2_6_13]
    ON [dbo].[Dispense]([PracticeLocationID], [DateDispensed], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_1000390633_6_2_14]
    ON [dbo].[Dispense]([DateDispensed], [PracticeLocationID], [DispenseBatchID]);


GO
CREATE STATISTICS [_dta_stat_1000390633_2_14_1_6]
    ON [dbo].[Dispense]([PracticeLocationID], [DispenseBatchID], [DispenseID], [DateDispensed]);


GO
CREATE STATISTICS [_dta_stat_1000390633_3_6_13_2]
    ON [dbo].[Dispense]([PatientCode], [DateDispensed], [IsActive], [PracticeLocationID]);


GO
CREATE STATISTICS [_dta_stat_1000390633_3_1_6_13]
    ON [dbo].[Dispense]([PatientCode], [DispenseID], [DateDispensed], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_1000390633_1_6_13_2_3]
    ON [dbo].[Dispense]([DispenseID], [DateDispensed], [IsActive], [PracticeLocationID], [PatientCode]);


GO




CREATE TRIGGER [dbo].[trg_Dispense_Deleted] 
ON [dbo].[Dispense] 
FOR delete 
AS
BEGIN

	INSERT INTO [dbo].[Dispense_Audit]
	( 
	  [DispenseID]
      ,[PracticeLocationID]
      ,[PatientCode]
      ,[ReceiptCode]
      ,[Total]
      ,[DateDispensed]
      ,[Comment]
      ,[CreatedUserID]
      ,[CreatedDate]
      ,[ModifiedUserID]
      ,[ModifiedDate]
      ,[CreatedWithHandheld]
      ,[IsActive]
      ,[DispenseBatchID]
      ,[PatientEmail]
      ,[PatientFirstName]
      ,[PatientLastName]
      ,[Note]
      ,[PracticePayerID]
      ,[AuditAction]
  )
  select 
	  [DispenseID]
      ,[PracticeLocationID]
      ,[PatientCode]
      ,[ReceiptCode]
      ,[Total]
      ,[DateDispensed]
      ,[Comment]
      ,[CreatedUserID]
      ,[CreatedDate]
      ,[ModifiedUserID]
      ,[ModifiedDate]
      ,[CreatedWithHandheld]
      ,[IsActive]
      ,[DispenseBatchID]
      ,[PatientEmail]
      ,[PatientFirstName]
      ,[PatientLastName]
      ,[Note]
      ,[PracticePayerID]
	  ,'Deleted'
from deleted
end



GO



CREATE TRIGGER [dbo].[trg_Dispense_Inserted] 
ON [dbo].[Dispense] 
FOR INSERT 
AS
BEGIN

	INSERT INTO [dbo].[Dispense_Audit]
	( 
	  [DispenseID]
      ,[PracticeLocationID]
      ,[PatientCode]
      ,[ReceiptCode]
      ,[Total]
      ,[DateDispensed]
      ,[Comment]
      ,[CreatedUserID]
      ,[CreatedDate]
      ,[ModifiedUserID]
      ,[ModifiedDate]
      ,[CreatedWithHandheld]
      ,[IsActive]
      ,[DispenseBatchID]
      ,[PatientEmail]
      ,[PatientFirstName]
      ,[PatientLastName]
      ,[Note]
      ,[PracticePayerID]
      ,[AuditAction]
  )
  select 
	  [DispenseID]
      ,[PracticeLocationID]
      ,[PatientCode]
      ,[ReceiptCode]
      ,[Total]
      ,[DateDispensed]
      ,[Comment]
      ,[CreatedUserID]
      ,[CreatedDate]
      ,[ModifiedUserID]
      ,[ModifiedDate]
      ,[CreatedWithHandheld]
      ,[IsActive]
      ,[DispenseBatchID]
      ,[PatientEmail]
      ,[PatientFirstName]
      ,[PatientLastName]
      ,[Note]
      ,[PracticePayerID]
	  ,'Inserted'
from inserted
end


GO




CREATE TRIGGER [dbo].[trg_Dispense_Updated] 
ON [dbo].[Dispense] 
FOR UPDATE 
AS
BEGIN

	INSERT INTO [dbo].[Dispense_Audit]
	( 
	  [DispenseID]
      ,[PracticeLocationID]
      ,[PatientCode]
      ,[ReceiptCode]
      ,[Total]
      ,[DateDispensed]
      ,[Comment]
      ,[CreatedUserID]
      ,[CreatedDate]
      ,[ModifiedUserID]
      ,[ModifiedDate]
      ,[CreatedWithHandheld]
      ,[IsActive]
      ,[DispenseBatchID]
      ,[PatientEmail]
      ,[PatientFirstName]
      ,[PatientLastName]
      ,[Note]
      ,[PracticePayerID]
      ,[AuditAction]
  )
  select 
	  [DispenseID]
      ,[PracticeLocationID]
      ,[PatientCode]
      ,[ReceiptCode]
      ,[Total]
      ,[DateDispensed]
      ,[Comment]
      ,[CreatedUserID]
      ,[CreatedDate]
      ,[ModifiedUserID]
      ,[ModifiedDate]
      ,[CreatedWithHandheld]
      ,[IsActive]
      ,[DispenseBatchID]
      ,[PatientEmail]
      ,[PatientFirstName]
      ,[PatientLastName]
      ,[Note]
      ,[PracticePayerID]
	  ,'Update(OLD)'
from deleted;

	INSERT INTO [dbo].[Dispense_Audit]
	( 
	  [DispenseID]
      ,[PracticeLocationID]
      ,[PatientCode]
      ,[ReceiptCode]
      ,[Total]
      ,[DateDispensed]
      ,[Comment]
      ,[CreatedUserID]
      ,[CreatedDate]
      ,[ModifiedUserID]
      ,[ModifiedDate]
      ,[CreatedWithHandheld]
      ,[IsActive]
      ,[DispenseBatchID]
      ,[PatientEmail]
      ,[PatientFirstName]
      ,[PatientLastName]
      ,[Note]
      ,[PracticePayerID]
      ,[AuditAction]
  )
  select 
	  [DispenseID]
      ,[PracticeLocationID]
      ,[PatientCode]
      ,[ReceiptCode]
      ,[Total]
      ,[DateDispensed]
      ,[Comment]
      ,[CreatedUserID]
      ,[CreatedDate]
      ,[ModifiedUserID]
      ,[ModifiedDate]
      ,[CreatedWithHandheld]
      ,[IsActive]
      ,[DispenseBatchID]
      ,[PatientEmail]
      ,[PatientFirstName]
      ,[PatientLastName]
      ,[Note]
      ,[PracticePayerID]
	  ,'Update(NEW)'
from inserted;
end



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the dispense table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Dispense', @level2type = N'COLUMN', @level2name = N'DispenseID';

