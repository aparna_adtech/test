﻿CREATE TABLE [dbo].[ProductInventory_OrderCheckIn] (
    [ProductInventory_OrderCheckInID] INT        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ProductInventoryID]              INT        NOT NULL,
    [SupplierOrderLineItemID]         INT        NOT NULL,
    [ActualWholesaleCost]             SMALLMONEY NOT NULL,
    [Quantity]                        INT        NOT NULL,
    [IsEntireLineItemCheckedIn]       BIT        NOT NULL,
    [DateCheckedIn]                   DATETIME   NOT NULL,
    [CreatedUserID]                   INT        NOT NULL,
    [CreatedDate]                     DATETIME   NOT NULL,
    [ModifiedUserID]                  INT        NULL,
    [ModifiedDate]                    DATETIME   NULL,
    [IsActive]                        BIT        NOT NULL,
    CONSTRAINT [ProductInventory_OrderCheckIn_PK] PRIMARY KEY CLUSTERED ([ProductInventory_OrderCheckInID] ASC),
    CONSTRAINT [ProductInventory_ProductInventory_OrderCheckIn_FK1] FOREIGN KEY ([ProductInventoryID]) REFERENCES [dbo].[ProductInventory] ([ProductInventoryID]),
    CONSTRAINT [SupplierOrderLineItem_ProductInventory_OrderCheckIn_FK1] FOREIGN KEY ([SupplierOrderLineItemID]) REFERENCES [dbo].[SupplierOrderLineItem] ([SupplierOrderLineItemID])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_945]
    ON [dbo].[ProductInventory_OrderCheckIn]([SupplierOrderLineItemID] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_410]
    ON [dbo].[ProductInventory_OrderCheckIn]([ProductInventoryID] ASC, [CreatedDate] ASC)
    INCLUDE([ActualWholesaleCost]);


GO
CREATE NONCLUSTERED INDEX [missing_index_703]
    ON [dbo].[ProductInventory_OrderCheckIn]([ProductInventoryID] ASC, [IsEntireLineItemCheckedIn] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_718]
    ON [dbo].[ProductInventory_OrderCheckIn]([ProductInventoryID] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_412]
    ON [dbo].[ProductInventory_OrderCheckIn]([CreatedDate] ASC)
    INCLUDE([ProductInventoryID], [ActualWholesaleCost]);


GO
CREATE NONCLUSTERED INDEX [missing_index_975]
    ON [dbo].[ProductInventory_OrderCheckIn]([SupplierOrderLineItemID] ASC, [IsActive] ASC)
    INCLUDE([ProductInventoryID], [Quantity]);


GO
CREATE NONCLUSTERED INDEX [missing_index_372]
    ON [dbo].[ProductInventory_OrderCheckIn]([SupplierOrderLineItemID] ASC, [IsActive] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_436]
    ON [dbo].[ProductInventory_OrderCheckIn]([IsActive] ASC)
    INCLUDE([ProductInventoryID], [SupplierOrderLineItemID], [Quantity]);


GO
CREATE NONCLUSTERED INDEX [ProdInv_OC_SupplierOrderLineItem]
    ON [dbo].[ProductInventory_OrderCheckIn]([SupplierOrderLineItemID] ASC)
    INCLUDE([Quantity], [DateCheckedIn]);


GO
CREATE STATISTICS [_dta_stat_1285579618_1_3]
    ON [dbo].[ProductInventory_OrderCheckIn]([ProductInventory_OrderCheckInID], [SupplierOrderLineItemID]);


GO
CREATE STATISTICS [_dta_stat_1285579618_3_2]
    ON [dbo].[ProductInventory_OrderCheckIn]([SupplierOrderLineItemID], [ProductInventoryID]);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary Key for the Product Inventory Order Check-in table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_OrderCheckIn', @level2type = N'COLUMN', @level2name = N'ProductInventory_OrderCheckInID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the amount paid, by the clinic, for the product is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_OrderCheckIn', @level2type = N'COLUMN', @level2name = N'ActualWholesaleCost';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the amount of a checked-in product is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_OrderCheckIn', @level2type = N'COLUMN', @level2name = N'Quantity';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag to verify if the entire order of a specific product (line item) has been checked in. ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_OrderCheckIn', @level2type = N'COLUMN', @level2name = N'IsEntireLineItemCheckedIn';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the system automatically enters in the date of the checked-in product.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_OrderCheckIn', @level2type = N'COLUMN', @level2name = N'DateCheckedIn';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in the Product Inventory Order Check-in  is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_OrderCheckIn', @level2type = N'COLUMN', @level2name = N'IsActive';

