﻿CREATE TABLE [dbo].[Practice] (
    [PracticeID]                               INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ContactID]                                INT            NULL,
    [PracticeName]                             VARCHAR (50)   NOT NULL,
    [IsBillingCentralized]                     BIT            CONSTRAINT [DF_Practice_IsBillingCentralized] DEFAULT ((0)) NOT NULL,
    [IsShoppingCentralized]                    BIT            CONSTRAINT [DF__Practice__IsShop__534D60F1] DEFAULT ((0)) NOT NULL,
    [BillingStartDate]                         DATETIME       CONSTRAINT [DF_Practice_BillingStartDate] DEFAULT ('12/31/2019 12:00:00 AM') NOT NULL,
    [CreatedUserID]                            INT            NOT NULL,
    [CreatedDate]                              DATETIME       CONSTRAINT [DF__Practice__Create__5441852A] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]                           INT            NULL,
    [ModifiedDate]                             DATETIME       NULL,
    [IsActive]                                 BIT            NOT NULL,
    [IsOrthoSelect]                            BIT            DEFAULT ((0)) NOT NULL,
    [ABNCostIsCash]                            BIT            DEFAULT ((0)) NOT NULL,
    [IsVisionLite]                             BIT            DEFAULT ((0)) NOT NULL,
    [timestamp]                                ROWVERSION     NOT NULL,
    [PaymentTypeID]                            INT            DEFAULT ((1)) NOT NULL,
    [PublicDescription]                        NVARCHAR (500) NULL,
    [LogoPartNumber]                           VARCHAR (255)  NULL,
    [CameraScanEnabled]                        BIT            DEFAULT ((0)) NOT NULL,
    [IsBregBilling]                            BIT            NULL,
    [EMRIntegrationPullEnabled]                BIT            CONSTRAINT [DF_Practice_EMRIntegrationPullEnabled] DEFAULT ((0)) NOT NULL,
    [FaxDispenseReceiptEnabled]                BIT            DEFAULT ((0)) NOT NULL,
    [EMRIntegrationDispenseReceiptPushEnabled] BIT            DEFAULT ((0)) NOT NULL,
    [EMRIntegrationHL7PushEnabled]             BIT            CONSTRAINT [DF_Practice_EMRIntegrationHL7PushEnabled] DEFAULT ((0)) NOT NULL,
    [IsHandheldPrintEnabled]                   BIT            CONSTRAINT [DF_Practice_IsHandheldPrintEnabled] DEFAULT ((0)) NOT NULL,
    [IsSecureMessagingInternalEnabled]         BIT            CONSTRAINT [DF_Practice_IsSecureMessagingInternalEnabled] DEFAULT ((0)) NOT NULL,
    [IsSecureMessagingExternalEnabled]         BIT            CONSTRAINT [DF_Practice_IsSecureMessagingExternalEnabled] DEFAULT ((0)) NOT NULL,
    [IsCustomFitEnabled]                       BIT            CONSTRAINT [DF_Practice_IsCustomFitEnabled] DEFAULT ((0)) NOT NULL,
    [IsDisplayBillChargeOnReceipt]             BIT            CONSTRAINT [DF_Practice_IsDisplayBillChargeOnReceipt] DEFAULT ((1)) NOT NULL,
    [IsBcsWorkflow]                            BIT            CONSTRAINT [DF_Practice_IsBcsWorkflow] DEFAULT ((0)) NOT NULL,
    [GeneralLedgerNumber]                      VARCHAR (50)   NULL,
	[BCSSubInventoryID]						   VARCHAR (10)   NULL,
    [IsHcpSignatureRequired]                   BIT            CONSTRAINT [DF_Practice_IsHcpSignatureRequired] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [Practice_PK] PRIMARY KEY CLUSTERED ([PracticeID] ASC),
    CONSTRAINT [Contact_Practice_FK1] FOREIGN KEY ([ContactID]) REFERENCES [dbo].[Contact] ([ContactID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the primary key for the Practice table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Practice', @level2type = N'COLUMN', @level2name = N'PracticeID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the practice name is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Practice', @level2type = N'COLUMN', @level2name = N'PracticeName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag to verify if the billing is centralized.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Practice', @level2type = N'COLUMN', @level2name = N'IsBillingCentralized';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag to verify if the shopping is centralized.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Practice', @level2type = N'COLUMN', @level2name = N'IsShoppingCentralized';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in the practice records is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Practice', @level2type = N'COLUMN', @level2name = N'IsActive';

