﻿CREATE TABLE [dbo].[PracticeCatalogSupplierBrand_Audit] (
    [PracticeCatalogSupplierBrand_AuditID] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PracticeCatalogSupplierBrandID]       INT          NOT NULL,
    [PracticeID]                           INT          NOT NULL,
    [MasterCatalogSupplierID]              INT          NULL,
    [ThirdPartySupplierID]                 INT          NULL,
    [SupplierName]                         VARCHAR (50) NOT NULL,
    [SupplierShortName]                    VARCHAR (50) NOT NULL,
    [BrandName]                            VARCHAR (50) NOT NULL,
    [BrandShortName]                       VARCHAR (50) NOT NULL,
    [IsThirdPartySupplier]                 BIT          NOT NULL,
    [Sequence]                             INT          NULL,
    [CreatedUserID]                        INT          NOT NULL,
    [CreatedDate]                          DATETIME     NOT NULL,
    [ModifiedUserID]                       INT          NULL,
    [ModifiedDate]                         DATETIME     NULL,
    [IsActive]                             BIT          NOT NULL,
    [SupplierWarrantyID]                   INT          NULL,
    [AuditAction]                          VARCHAR (50) NULL,
    [AuditCreatedDate]                     DATETIME     NULL,
    CONSTRAINT [PracticeCatalogSupplierBrand_Audit_PK] PRIMARY KEY CLUSTERED ([PracticeCatalogSupplierBrand_AuditID] ASC)
);

