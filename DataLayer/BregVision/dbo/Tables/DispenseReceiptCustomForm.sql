﻿CREATE TABLE [dbo].[DispenseReceiptCustomForm] (
    [Id]         INT           IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (250) NOT NULL,
    [BlobOrder]  INT           NOT NULL,
    [PracticeId] INT           NOT NULL,
    [IsActive]   BIT           CONSTRAINT [DF_DispenseReceiptCustomForm_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_DispenseReceiptCustomForm] PRIMARY KEY CLUSTERED ([Id] ASC)
);

