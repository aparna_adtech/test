﻿CREATE TABLE [dbo].[OffTheShelfBillingCharge] (
    [OffTheShelfBillingChargeID] INT        IDENTITY (1, 1) NOT NULL,
    [PracticeCatalogProductID]   INT        NOT NULL,
    [BillingCharge]              SMALLMONEY NOT NULL,
    [BillingChargeCash]          SMALLMONEY NULL,
    CONSTRAINT [PK_OffTheShelfBillingCharge] PRIMARY KEY CLUSTERED ([OffTheShelfBillingChargeID] ASC),
    CONSTRAINT [FK_OffTheShelfBillingCharge_PracticeCatalogProduct] FOREIGN KEY ([PracticeCatalogProductID]) REFERENCES [dbo].[PracticeCatalogProduct] ([PracticeCatalogProductID])
);

