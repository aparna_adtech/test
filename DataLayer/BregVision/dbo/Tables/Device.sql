﻿CREATE TABLE [dbo].[Device] (
    [DeviceID]           INT           IDENTITY (1, 1) NOT NULL,
    [DeviceIdentifier]   VARCHAR (255) NULL,
    [LastLocation]       VARCHAR (255) NULL,
    [PracticeID]         INT           NULL,
    [PracticeLocationID] INT           NULL,
    [CreatedUserID]      INT           DEFAULT ((1)) NOT NULL,
    [CreatedDate]        DATETIME      DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]     INT           NULL,
    [ModifiedDate]       DATETIME      NULL,
    [IsActive]           BIT           DEFAULT ((1)) NOT NULL,
    [timestamp]          ROWVERSION    NOT NULL,
    CONSTRAINT [PK_Device] PRIMARY KEY CLUSTERED ([DeviceID] ASC),
    CONSTRAINT [FK_Device_Practice] FOREIGN KEY ([PracticeID]) REFERENCES [dbo].[Practice] ([PracticeID]),
    CONSTRAINT [FK_Device_PracticeLocation] FOREIGN KEY ([PracticeLocationID]) REFERENCES [dbo].[PracticeLocation] ([PracticeLocationID])
);

