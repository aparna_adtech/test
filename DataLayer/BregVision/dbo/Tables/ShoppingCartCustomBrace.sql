﻿CREATE TABLE [dbo].[ShoppingCartCustomBrace] (
    [ShoppingCartID]                         INT            NOT NULL,
    [BregVisionOrderID]                      INT            NULL,
    [DispenseID]                             INT            NULL,
    [MappedPDFID]                            INT            NULL,
    [PONumber]                               VARCHAR (50)   NULL,
    [Billing_CustomerNumber]                 VARCHAR (50)   NULL,
    [Billing_Phone]                          VARCHAR (50)   NULL,
    [Billing_Attention]                      VARCHAR (50)   NULL,
    [Billing_Address1]                       VARCHAR (50)   NULL,
    [Billing_Address2]                       VARCHAR (50)   NULL,
    [Billing_City]                           VARCHAR (50)   NULL,
    [Billing_State]                          VARCHAR (50)   NULL,
    [Billing_Zip]                            VARCHAR (50)   NULL,
    [Shipping_Attention]                     VARCHAR (50)   NULL,
    [Shipping_Address1]                      VARCHAR (50)   NULL,
    [Shipping_Address2]                      VARCHAR (50)   NULL,
    [Shipping_City]                          VARCHAR (50)   NULL,
    [Shipping_State]                         VARCHAR (50)   NULL,
    [Shipping_Zip]                           VARCHAR (50)   NULL,
    [PatientName]                            VARCHAR (50)   NULL,
    [PhysicianID]                            INT            NULL,
    [Patient_Age]                            INT            NULL,
    [Patient_Weight]                         INT            NULL,
    [Patient_Height]                         VARCHAR (50)   NULL,
    [Patient_Sex]                            VARCHAR (8)    NULL,
    [BraceFor]                               VARCHAR (50)   NULL,
    [Instability]                            VARCHAR (50)   NULL,
    [BilatCastReform]                        VARCHAR (50)   NULL,
    [ThighCircumference]                     VARCHAR (50)   NULL,
    [CalfCircumference]                      VARCHAR (50)   NULL,
    [KneeOffset]                             VARCHAR (50)   NULL,
    [KneeWidth]                              VARCHAR (50)   NULL,
    [Extension]                              VARCHAR (50)   NULL,
    [Flexion]                                VARCHAR (50)   NULL,
    [MeasurementsTakenBy]                    VARCHAR (150)  NULL,
    [X2K_Counterforce]                       VARCHAR (50)   NULL,
    [X2K_Counterforce_Degrees]               VARCHAR (50)   NULL,
    [X2K_FramePadColor1]                     VARCHAR (50)   NULL,
    [X2K_FramePadColor2]                     VARCHAR (50)   NULL,
    [Fusion_Enhancement]                     VARCHAR (50)   NULL,
    [Fusion_Notes]                           VARCHAR (1000) NULL,
    [Fusion_Color]                           VARCHAR (50)   NULL,
    [Fusion_Pantone]                         VARCHAR (50)   NULL,
    [Fusion_Pattern]                         VARCHAR (50)   NULL,
    [Fusion_Pattern_Notes]                   VARCHAR (250)  NULL,
    [ShippingTypeID]                         INT            NULL,
    [ShippingCarrierID]                      INT            NULL,
    [Instability_BilatRightLeg]              VARCHAR (50)   NULL,
    [X2K_Counterforce_BilatRightLeg]         VARCHAR (50)   NULL,
    [X2K_Counterforce_Degrees_BilatRightLeg] VARCHAR (50)   NULL,
    [ThighCircumference_BilatRightLeg]       VARCHAR (50)   NULL,
    [CalfCircumference_BilatRightLeg]        VARCHAR (50)   NULL,
    [KneeOffset_BilatRightLeg]               VARCHAR (50)   NULL,
    [KneeWidth_BilatRightLeg]                VARCHAR (50)   NULL,
    [Extension_BilatRightLeg]                VARCHAR (50)   NULL,
    [Flexion_BilatRightLeg]                  VARCHAR (50)   NULL,
    [FDF]                                    VARCHAR (MAX)  NULL,
    CONSTRAINT [PK_ShoppingCartCustomBrace] PRIMARY KEY CLUSTERED ([ShoppingCartID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_ShoppingCartCustomBrace]
    ON [dbo].[ShoppingCartCustomBrace]([BregVisionOrderID] ASC);


GO
CREATE TRIGGER [dbo].[trg_ShoppingCartCustomBrace_Updated] 
ON dbo.ShoppingCartCustomBrace 
FOR UPDATE 
AS
BEGIN

	INSERT INTO [ShoppingCartCustomBrace_Audit]
	(
		[ShoppingCartID]
		,[BregVisionOrderID]
		,[DispenseID]
		,[Billing_CustomerNumber]
		,[Billing_Phone]
		,[Billing_Attention]
		,[Billing_Address1]
		,[Billing_Address2]
		,[Billing_City]
		,[Billing_State]
		,[Billing_Zip]
		,[Shipping_Attention]
		,[Shipping_Address1]
		,[Shipping_Address2]
		,[Shipping_City]
		,[Shipping_State]
		,[Shipping_Zip]
		,[PatientName]
		,[PhysicianID]
		,[Patient_Age]
		,[Patient_Weight]
		,[Patient_Height]
		,[Patient_Sex]
		,[BraceFor]
		,[Instability]
		,[BilatCastReform]
		,[ThighCircumference]
		,[CalfCircumference]
		,[KneeOffset]
		,[KneeWidth]
		,[Extension]
		,[Flexion]
		,[MeasurementsTakenBy]
		,[X2K_Counterforce]
		,[X2K_Counterforce_Degrees]
		,[X2K_FramePadColor1]
		,[X2K_FramePadColor2]
		,[Fusion_Enhancement]
		,[Fusion_Notes]
		,[Fusion_Color]
		,[Fusion_Pantone]
		,[Fusion_Pattern]
		,[Fusion_Pattern_Notes]
		,[ShippingTypeID]
		,[ShippingCarrierID]
		,[Instability_BilatRightLeg]
		,[X2K_Counterforce_BilatRightLeg]
		,[X2K_Counterforce_Degrees_BilatRightLeg]
		,[ThighCircumference_BilatRightLeg]
		,[CalfCircumference_BilatRightLeg]
		,[KneeOffset_BilatRightLeg]
		,[KneeWidth_BilatRightLeg]
		,[Extension_BilatRightLeg]
		,[Flexion_BilatRightLeg]
		,[AuditAction]
	)
	SELECT
		[ShoppingCartID]
		,[BregVisionOrderID]
		,[DispenseID]
		,[Billing_CustomerNumber]
		,[Billing_Phone]
		,[Billing_Attention]
		,[Billing_Address1]
		,[Billing_Address2]
		,[Billing_City]
		,[Billing_State]
		,[Billing_Zip]
		,[Shipping_Attention]
		,[Shipping_Address1]
		,[Shipping_Address2]
		,[Shipping_City]
		,[Shipping_State]
		,[Shipping_Zip]
		,[PatientName]
		,[PhysicianID]
		,[Patient_Age]
		,[Patient_Weight]
		,[Patient_Height]
		,[Patient_Sex]
		,[BraceFor]
		,[Instability]
		,[BilatCastReform]
		,[ThighCircumference]
		,[CalfCircumference]
		,[KneeOffset]
		,[KneeWidth]
		,[Extension]
		,[Flexion]
		,[MeasurementsTakenBy]
		,[X2K_Counterforce]
		,[X2K_Counterforce_Degrees]
		,[X2K_FramePadColor1]
		,[X2K_FramePadColor2]
		,[Fusion_Enhancement]
		,[Fusion_Notes]
		,[Fusion_Color]
		,[Fusion_Pantone]
		,[Fusion_Pattern]
		,[Fusion_Pattern_Notes]
		,[ShippingTypeID]
		,[ShippingCarrierID]
		,[Instability_BilatRightLeg]
		,[X2K_Counterforce_BilatRightLeg]
		,[X2K_Counterforce_Degrees_BilatRightLeg]
		,[ThighCircumference_BilatRightLeg]
		,[CalfCircumference_BilatRightLeg]
		,[KneeOffset_BilatRightLeg]
		,[KneeWidth_BilatRightLeg]
		,[Extension_BilatRightLeg]
		,[Flexion_BilatRightLeg]
		,'Updated (OLD)'
	FROM deleted

	INSERT INTO [ShoppingCartCustomBrace_Audit]
	(
		[ShoppingCartID]
		,[BregVisionOrderID]
		,[DispenseID]
		,[Billing_CustomerNumber]
		,[Billing_Phone]
		,[Billing_Attention]
		,[Billing_Address1]
		,[Billing_Address2]
		,[Billing_City]
		,[Billing_State]
		,[Billing_Zip]
		,[Shipping_Attention]
		,[Shipping_Address1]
		,[Shipping_Address2]
		,[Shipping_City]
		,[Shipping_State]
		,[Shipping_Zip]
		,[PatientName]
		,[PhysicianID]
		,[Patient_Age]
		,[Patient_Weight]
		,[Patient_Height]
		,[Patient_Sex]
		,[BraceFor]
		,[Instability]
		,[BilatCastReform]
		,[ThighCircumference]
		,[CalfCircumference]
		,[KneeOffset]
		,[KneeWidth]
		,[Extension]
		,[Flexion]
		,[MeasurementsTakenBy]
		,[X2K_Counterforce]
		,[X2K_Counterforce_Degrees]
		,[X2K_FramePadColor1]
		,[X2K_FramePadColor2]
		,[Fusion_Enhancement]
		,[Fusion_Notes]
		,[Fusion_Color]
		,[Fusion_Pantone]
		,[Fusion_Pattern]
		,[Fusion_Pattern_Notes]
		,[ShippingTypeID]
		,[ShippingCarrierID]
		,[Instability_BilatRightLeg]
		,[X2K_Counterforce_BilatRightLeg]
		,[X2K_Counterforce_Degrees_BilatRightLeg]
		,[ThighCircumference_BilatRightLeg]
		,[CalfCircumference_BilatRightLeg]
		,[KneeOffset_BilatRightLeg]
		,[KneeWidth_BilatRightLeg]
		,[Extension_BilatRightLeg]
		,[Flexion_BilatRightLeg]
		,[AuditAction]
	)
	SELECT
		[ShoppingCartID]
		,[BregVisionOrderID]
		,[DispenseID]
		,[Billing_CustomerNumber]
		,[Billing_Phone]
		,[Billing_Attention]
		,[Billing_Address1]
		,[Billing_Address2]
		,[Billing_City]
		,[Billing_State]
		,[Billing_Zip]
		,[Shipping_Attention]
		,[Shipping_Address1]
		,[Shipping_Address2]
		,[Shipping_City]
		,[Shipping_State]
		,[Shipping_Zip]
		,[PatientName]
		,[PhysicianID]
		,[Patient_Age]
		,[Patient_Weight]
		,[Patient_Height]
		,[Patient_Sex]
		,[BraceFor]
		,[Instability]
		,[BilatCastReform]
		,[ThighCircumference]
		,[CalfCircumference]
		,[KneeOffset]
		,[KneeWidth]
		,[Extension]
		,[Flexion]
		,[MeasurementsTakenBy]
		,[X2K_Counterforce]
		,[X2K_Counterforce_Degrees]
		,[X2K_FramePadColor1]
		,[X2K_FramePadColor2]
		,[Fusion_Enhancement]
		,[Fusion_Notes]
		,[Fusion_Color]
		,[Fusion_Pantone]
		,[Fusion_Pattern]
		,[Fusion_Pattern_Notes]
		,[ShippingTypeID]
		,[ShippingCarrierID]
		,[Instability_BilatRightLeg]
		,[X2K_Counterforce_BilatRightLeg]
		,[X2K_Counterforce_Degrees_BilatRightLeg]
		,[ThighCircumference_BilatRightLeg]
		,[CalfCircumference_BilatRightLeg]
		,[KneeOffset_BilatRightLeg]
		,[KneeWidth_BilatRightLeg]
		,[Extension_BilatRightLeg]
		,[Flexion_BilatRightLeg]
		,'Updated (NEW)'
	FROM inserted

END

GO
CREATE TRIGGER [dbo].[trg_ShoppingCartCustomBrace_Deleted] 
ON dbo.ShoppingCartCustomBrace 
FOR DELETE 
AS
BEGIN

	INSERT INTO [ShoppingCartCustomBrace_Audit]
	(
		[ShoppingCartID]
		,[BregVisionOrderID]
		,[DispenseID]
		,[Billing_CustomerNumber]
		,[Billing_Phone]
		,[Billing_Attention]
		,[Billing_Address1]
		,[Billing_Address2]
		,[Billing_City]
		,[Billing_State]
		,[Billing_Zip]
		,[Shipping_Attention]
		,[Shipping_Address1]
		,[Shipping_Address2]
		,[Shipping_City]
		,[Shipping_State]
		,[Shipping_Zip]
		,[PatientName]
		,[PhysicianID]
		,[Patient_Age]
		,[Patient_Weight]
		,[Patient_Height]
		,[Patient_Sex]
		,[BraceFor]
		,[Instability]
		,[BilatCastReform]
		,[ThighCircumference]
		,[CalfCircumference]
		,[KneeOffset]
		,[KneeWidth]
		,[Extension]
		,[Flexion]
		,[MeasurementsTakenBy]
		,[X2K_Counterforce]
		,[X2K_Counterforce_Degrees]
		,[X2K_FramePadColor1]
		,[X2K_FramePadColor2]
		,[Fusion_Enhancement]
		,[Fusion_Notes]
		,[Fusion_Color]
		,[Fusion_Pantone]
		,[Fusion_Pattern]
		,[Fusion_Pattern_Notes]
		,[ShippingTypeID]
		,[ShippingCarrierID]
		,[Instability_BilatRightLeg]
		,[X2K_Counterforce_BilatRightLeg]
		,[X2K_Counterforce_Degrees_BilatRightLeg]
		,[ThighCircumference_BilatRightLeg]
		,[CalfCircumference_BilatRightLeg]
		,[KneeOffset_BilatRightLeg]
		,[KneeWidth_BilatRightLeg]
		,[Extension_BilatRightLeg]
		,[Flexion_BilatRightLeg]
		,[AuditAction]
	)
	SELECT
		[ShoppingCartID]
		,[BregVisionOrderID]
		,[DispenseID]
		,[Billing_CustomerNumber]
		,[Billing_Phone]
		,[Billing_Attention]
		,[Billing_Address1]
		,[Billing_Address2]
		,[Billing_City]
		,[Billing_State]
		,[Billing_Zip]
		,[Shipping_Attention]
		,[Shipping_Address1]
		,[Shipping_Address2]
		,[Shipping_City]
		,[Shipping_State]
		,[Shipping_Zip]
		,[PatientName]
		,[PhysicianID]
		,[Patient_Age]
		,[Patient_Weight]
		,[Patient_Height]
		,[Patient_Sex]
		,[BraceFor]
		,[Instability]
		,[BilatCastReform]
		,[ThighCircumference]
		,[CalfCircumference]
		,[KneeOffset]
		,[KneeWidth]
		,[Extension]
		,[Flexion]
		,[MeasurementsTakenBy]
		,[X2K_Counterforce]
		,[X2K_Counterforce_Degrees]
		,[X2K_FramePadColor1]
		,[X2K_FramePadColor2]
		,[Fusion_Enhancement]
		,[Fusion_Notes]
		,[Fusion_Color]
		,[Fusion_Pantone]
		,[Fusion_Pattern]
		,[Fusion_Pattern_Notes]
		,[ShippingTypeID]
		,[ShippingCarrierID]
		,[Instability_BilatRightLeg]
		,[X2K_Counterforce_BilatRightLeg]
		,[X2K_Counterforce_Degrees_BilatRightLeg]
		,[ThighCircumference_BilatRightLeg]
		,[CalfCircumference_BilatRightLeg]
		,[KneeOffset_BilatRightLeg]
		,[KneeWidth_BilatRightLeg]
		,[Extension_BilatRightLeg]
		,[Flexion_BilatRightLeg]
		,'Deleted'
	FROM deleted

END

GO
CREATE TRIGGER [dbo].[trg_ShoppingCartCustomBrace_Inserted] 
ON dbo.ShoppingCartCustomBrace 
FOR INSERT 
AS
BEGIN

	INSERT INTO [ShoppingCartCustomBrace_Audit]
	(
		[ShoppingCartID]
		,[BregVisionOrderID]
		,[DispenseID]
		,[Billing_CustomerNumber]
		,[Billing_Phone]
		,[Billing_Attention]
		,[Billing_Address1]
		,[Billing_Address2]
		,[Billing_City]
		,[Billing_State]
		,[Billing_Zip]
		,[Shipping_Attention]
		,[Shipping_Address1]
		,[Shipping_Address2]
		,[Shipping_City]
		,[Shipping_State]
		,[Shipping_Zip]
		,[PatientName]
		,[PhysicianID]
		,[Patient_Age]
		,[Patient_Weight]
		,[Patient_Height]
		,[Patient_Sex]
		,[BraceFor]
		,[Instability]
		,[BilatCastReform]
		,[ThighCircumference]
		,[CalfCircumference]
		,[KneeOffset]
		,[KneeWidth]
		,[Extension]
		,[Flexion]
		,[MeasurementsTakenBy]
		,[X2K_Counterforce]
		,[X2K_Counterforce_Degrees]
		,[X2K_FramePadColor1]
		,[X2K_FramePadColor2]
		,[Fusion_Enhancement]
		,[Fusion_Notes]
		,[Fusion_Color]
		,[Fusion_Pantone]
		,[Fusion_Pattern]
		,[Fusion_Pattern_Notes]
		,[ShippingTypeID]
		,[ShippingCarrierID]
		,[Instability_BilatRightLeg]
		,[X2K_Counterforce_BilatRightLeg]
		,[X2K_Counterforce_Degrees_BilatRightLeg]
		,[ThighCircumference_BilatRightLeg]
		,[CalfCircumference_BilatRightLeg]
		,[KneeOffset_BilatRightLeg]
		,[KneeWidth_BilatRightLeg]
		,[Extension_BilatRightLeg]
		,[Flexion_BilatRightLeg]
		,[AuditAction]
	)
	SELECT
		[ShoppingCartID]
		,[BregVisionOrderID]
		,[DispenseID]
		,[Billing_CustomerNumber]
		,[Billing_Phone]
		,[Billing_Attention]
		,[Billing_Address1]
		,[Billing_Address2]
		,[Billing_City]
		,[Billing_State]
		,[Billing_Zip]
		,[Shipping_Attention]
		,[Shipping_Address1]
		,[Shipping_Address2]
		,[Shipping_City]
		,[Shipping_State]
		,[Shipping_Zip]
		,[PatientName]
		,[PhysicianID]
		,[Patient_Age]
		,[Patient_Weight]
		,[Patient_Height]
		,[Patient_Sex]
		,[BraceFor]
		,[Instability]
		,[BilatCastReform]
		,[ThighCircumference]
		,[CalfCircumference]
		,[KneeOffset]
		,[KneeWidth]
		,[Extension]
		,[Flexion]
		,[MeasurementsTakenBy]
		,[X2K_Counterforce]
		,[X2K_Counterforce_Degrees]
		,[X2K_FramePadColor1]
		,[X2K_FramePadColor2]
		,[Fusion_Enhancement]
		,[Fusion_Notes]
		,[Fusion_Color]
		,[Fusion_Pantone]
		,[Fusion_Pattern]
		,[Fusion_Pattern_Notes]
		,[ShippingTypeID]
		,[ShippingCarrierID]
		,[Instability_BilatRightLeg]
		,[X2K_Counterforce_BilatRightLeg]
		,[X2K_Counterforce_Degrees_BilatRightLeg]
		,[ThighCircumference_BilatRightLeg]
		,[CalfCircumference_BilatRightLeg]
		,[KneeOffset_BilatRightLeg]
		,[KneeWidth_BilatRightLeg]
		,[Extension_BilatRightLeg]
		,[Flexion_BilatRightLeg]
		,'Inserted'
	FROM inserted

END
