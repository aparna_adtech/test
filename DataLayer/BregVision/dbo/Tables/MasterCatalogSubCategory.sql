﻿CREATE TABLE [dbo].[MasterCatalogSubCategory] (
    [MasterCatalogSubCategoryID] INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MasterCatalogCategoryID]    INT          NOT NULL,
    [Name]                       VARCHAR (50) NOT NULL,
    [Sequence]                   INT          NULL,
    [CreatedUserID]              INT          NOT NULL,
    [CreatedDate]                DATETIME     CONSTRAINT [DF__MasterCat__Creat__5CD6CB2B] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]             INT          NULL,
    [ModifiedDate]               DATETIME     NULL,
    [IsActive]                   BIT          NOT NULL,
    [IsBCSSubCategory]           BIT          NULL,
    CONSTRAINT [MasterCatalogSubCategory_PK] PRIMARY KEY CLUSTERED ([MasterCatalogSubCategoryID] ASC),
    CONSTRAINT [MasterCatalogCategory_MasterCatalogSubCategory_FK1] FOREIGN KEY ([MasterCatalogCategoryID]) REFERENCES [dbo].[MasterCatalogCategory] ([MasterCatalogCategoryID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the Master Catalog Sub-Category table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogSubCategory', @level2type = N'COLUMN', @level2name = N'MasterCatalogSubCategoryID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the 20 different sub-categories will be entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogSubCategory', @level2type = N'COLUMN', @level2name = N'Name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the order of the 20 different sub-categories will be displayed.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogSubCategory', @level2type = N'COLUMN', @level2name = N'Sequence';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record of sub-category is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogSubCategory', @level2type = N'COLUMN', @level2name = N'IsActive';

