﻿CREATE TABLE [dbo].[ProductInventory_QuantityOnHandPhysical] (
    [ProductInventory_QuantityOnHandPhysicalID] INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ProductInventoryID]                        INT      NOT NULL,
    [Quantity]                                  INT      NOT NULL,
    [DateOfPhysicalCount]                       DATETIME NOT NULL,
    [CreatedUserID]                             INT      NOT NULL,
    [CreatedDate]                               DATETIME NOT NULL,
    [ModifiedUserID]                            INT      NULL,
    [ModifiedDate]                              DATETIME NULL,
    [IsActive]                                  BIT      NOT NULL,
    [InventoryCycleID]                          INT      NOT NULL,
    CONSTRAINT [ProductInventory_QuantityOnHandPhysical_PK] PRIMARY KEY CLUSTERED ([ProductInventory_QuantityOnHandPhysicalID] ASC),
    CONSTRAINT [FK_ProductInventory_QuantityOnHandPhysical_InventoryCycle] FOREIGN KEY ([InventoryCycleID]) REFERENCES [dbo].[InventoryCycle] ([InventoryCycleID]),
    CONSTRAINT [ProductInventory_ProductInventory_QuantityOnHandPhysical_FK1] FOREIGN KEY ([ProductInventoryID]) REFERENCES [dbo].[ProductInventory] ([ProductInventoryID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the Product Inventory Quantity on Hand table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_QuantityOnHandPhysical', @level2type = N'COLUMN', @level2name = N'ProductInventory_QuantityOnHandPhysicalID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the quantity the clinic has on hand is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_QuantityOnHandPhysical', @level2type = N'COLUMN', @level2name = N'Quantity';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the system automatically enters the date in which physical count was completed.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_QuantityOnHandPhysical', @level2type = N'COLUMN', @level2name = N'DateOfPhysicalCount';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in the Physical  Product Inventory Quantity On-hand is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_QuantityOnHandPhysical', @level2type = N'COLUMN', @level2name = N'IsActive';

