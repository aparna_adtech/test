﻿CREATE TABLE [dbo].[TimeZone] (
    [TimeZoneID]      INT          CONSTRAINT [DF__TimeZone__TimeZo__023D5A04] DEFAULT ((1)) NOT NULL,
    [TimeZoneName]    VARCHAR (50) NOT NULL,
    [HoursAddedToPST] INT          NOT NULL,
    [IsDSTObserved]   BIT          NOT NULL,
    CONSTRAINT [TimeZone_PK] PRIMARY KEY CLUSTERED ([TimeZoneID] ASC)
);

