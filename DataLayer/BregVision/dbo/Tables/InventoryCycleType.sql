﻿CREATE TABLE [dbo].[InventoryCycleType] (
    [InventoryCycleTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [CycleName]            NVARCHAR (50) NOT NULL,
    [IsConsignment]        BIT           CONSTRAINT [DF_InventoryCycleType_IsConsignment] DEFAULT ((0)) NOT NULL,
    [CreatedUserID]        INT           NOT NULL,
    [CreatedDate]          DATETIME      CONSTRAINT [DF_InventoryCycleType_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]       INT           NULL,
    [ModifiedDate]         DATETIME      NULL,
    [IsActive]             BIT           CONSTRAINT [DF_InventoryCycleType_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_InventoryCycleType] PRIMARY KEY CLUSTERED ([InventoryCycleTypeID] ASC)
);

