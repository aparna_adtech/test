﻿CREATE TABLE [dbo].[InventoryCycle] (
    [InventoryCycleID]     INT           IDENTITY (1, 1) NOT NULL,
    [StartDate]            DATETIME      NOT NULL,
    [EndDate]              DATETIME      NULL,
    [PracticeLocationID]   INT           NOT NULL,
    [InventoryCycleTypeID] INT           NOT NULL,
    [CreatedUserID]        INT           NOT NULL,
    [CreatedDate]          DATETIME      CONSTRAINT [DF_InventoryCycle_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]       INT           NULL,
    [ModifiedDate]         DATETIME      NOT NULL,
    [IsActive]             BIT           CONSTRAINT [DF_InventoryCycle_IsActive] DEFAULT ((1)) NOT NULL,
    [Title]                VARCHAR (255) NULL,
    CONSTRAINT [PK_InventoryCycle] PRIMARY KEY CLUSTERED ([InventoryCycleID] ASC),
    CONSTRAINT [FK_InventoryCycle_InventoryCycleType] FOREIGN KEY ([InventoryCycleTypeID]) REFERENCES [dbo].[InventoryCycleType] ([InventoryCycleTypeID]),
    CONSTRAINT [FK_InventoryCycle_PracticeLocation] FOREIGN KEY ([PracticeLocationID]) REFERENCES [dbo].[PracticeLocation] ([PracticeLocationID])
);

