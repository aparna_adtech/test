﻿CREATE TABLE [dbo].[xxxx_Wholesale_Price_List] (
    [PLL Item No]       NVARCHAR (255) NULL,
    [PLL Item Desc]     NVARCHAR (255) NULL,
    [PLL Line Price]    FLOAT (53)     NULL,
    [PLL UOM Cd]        NVARCHAR (255) NULL,
    [PLL Start Dt]      FLOAT (53)     NULL,
    [PLL End Dt]        NVARCHAR (255) NULL,
    [Price List Header] NVARCHAR (255) NULL,
    [P/N - UOM]         NVARCHAR (255) NULL
);

