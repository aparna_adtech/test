﻿CREATE TABLE [dbo].[BregVisionOrder] (
    [BregVisionOrderID]       INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PracticeLocationID]      INT          NOT NULL,
    [BregVisionOrderStatusID] TINYINT      NULL,
    [ShippingTypeID]          TINYINT      NULL,
    [PurchaseOrder]           INT          NULL,
    [CustomPurchaseOrderCode] VARCHAR (75) NOT NULL,
    [Total]                   SMALLMONEY   NULL,
    [CreatedUserID]           INT          NOT NULL,
    [CreatedDate]             DATETIME     CONSTRAINT [DF__BregVisio__Creat__68487DD7] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]          INT          NULL,
    [ModifiedDate]            DATETIME     CONSTRAINT [DF_BregVisionOrder_ModifiedDate] DEFAULT (getdate()) NULL,
    [IsActive]                BIT          NOT NULL,
    [IsConsignment]           BIT          DEFAULT ((0)) NOT NULL,
    CONSTRAINT [BregVisionOrder_PK] PRIMARY KEY CLUSTERED ([BregVisionOrderID] ASC),
    CONSTRAINT [BregVisionOrderStatus_BregVisionOrder_FK1] FOREIGN KEY ([BregVisionOrderStatusID]) REFERENCES [dbo].[BregVisionOrderStatus] ([BregVisionOrderStatusID]),
    CONSTRAINT [PracticeLocation_BregVisionOrder_FK1] FOREIGN KEY ([PracticeLocationID]) REFERENCES [dbo].[PracticeLocation] ([PracticeLocationID]),
    CONSTRAINT [ShippingType_BregVisionOrder_FK1] FOREIGN KEY ([ShippingTypeID]) REFERENCES [dbo].[ShippingType] ([ShippingTypeID])
);


GO
CREATE NONCLUSTERED INDEX [_dta_index_BregVisionOrder_8_1147867156__K2_K3_K12_1_4_7]
    ON [dbo].[BregVisionOrder]([PracticeLocationID] ASC, [BregVisionOrderStatusID] ASC, [IsActive] ASC)
    INCLUDE([BregVisionOrderID], [ShippingTypeID], [Total]);


GO
CREATE NONCLUSTERED INDEX [IX_BregVisionOrder_CreatedDate]
    ON [dbo].[BregVisionOrder]([CreatedDate] ASC)
    INCLUDE([BregVisionOrderID], [PracticeLocationID], [PurchaseOrder], [CustomPurchaseOrderCode]);


GO
CREATE STATISTICS [_dta_stat_1147867156_3_1]
    ON [dbo].[BregVisionOrder]([BregVisionOrderStatusID], [BregVisionOrderID]);


GO
CREATE STATISTICS [_dta_stat_1147867156_2_12_3]
    ON [dbo].[BregVisionOrder]([PracticeLocationID], [IsActive], [BregVisionOrderStatusID]);


GO
CREATE STATISTICS [_dta_stat_1147867156_1_2_12]
    ON [dbo].[BregVisionOrder]([BregVisionOrderID], [PracticeLocationID], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_1147867156_1_12_4]
    ON [dbo].[BregVisionOrder]([BregVisionOrderID], [IsActive], [ShippingTypeID]);


GO
CREATE STATISTICS [_dta_stat_1147867156_9_2_3_12]
    ON [dbo].[BregVisionOrder]([CreatedDate], [PracticeLocationID], [BregVisionOrderStatusID], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_1147867156_5_6_1_12]
    ON [dbo].[BregVisionOrder]([PurchaseOrder], [CustomPurchaseOrderCode], [BregVisionOrderID], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_1147867156_3_4_2_12_9]
    ON [dbo].[BregVisionOrder]([BregVisionOrderStatusID], [ShippingTypeID], [PracticeLocationID], [IsActive], [CreatedDate]);


GO
CREATE STATISTICS [_dta_stat_1147867156_4_2_1_12_5_6]
    ON [dbo].[BregVisionOrder]([ShippingTypeID], [PracticeLocationID], [BregVisionOrderID], [IsActive], [PurchaseOrder], [CustomPurchaseOrderCode]);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary Key for the Breg Vision Order table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BregVisionOrder', @level2type = N'COLUMN', @level2name = N'BregVisionOrderID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the six-digit generated number (sequential, starting with 100001) is entered into.  It is unique to each purchase order.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BregVisionOrder', @level2type = N'COLUMN', @level2name = N'PurchaseOrder';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the user can enter in a custom customer code.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BregVisionOrder', @level2type = N'COLUMN', @level2name = N'CustomPurchaseOrderCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the total amount spent in the purchase order is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BregVisionOrder', @level2type = N'COLUMN', @level2name = N'Total';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in the Breg Vision Order is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BregVisionOrder', @level2type = N'COLUMN', @level2name = N'IsActive';

