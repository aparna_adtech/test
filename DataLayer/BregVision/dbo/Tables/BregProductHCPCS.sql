﻿CREATE TABLE [dbo].[BregProductHCPCS] (
    [BregProductHCPCSID]     INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MasterCatalogProductID] INT          NOT NULL,
    [HCPCS]                  VARCHAR (20) NOT NULL,
    [CreatedUserID]          INT          NOT NULL,
    [CreatedDate]            DATETIME     NOT NULL,
    [ModifiedUserID]         INT          NOT NULL,
    [ModifiedDate]           DATETIME     NOT NULL,
    [IsActive]               BIT          NOT NULL,
    [IsOffTheShelf]          BIT          CONSTRAINT [DF_BregProductHCPCS_IsSplitCode] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [BregProductHCPCS_PK] PRIMARY KEY CLUSTERED ([BregProductHCPCSID] ASC),
    CONSTRAINT [MasterCatalogProduct_BregProductHCPCS_FK1] FOREIGN KEY ([MasterCatalogProductID]) REFERENCES [dbo].[MasterCatalogProduct] ([MasterCatalogProductID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the Breg Product Healthcare Common Procedure Coding System (HCPCS) table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BregProductHCPCS', @level2type = N'COLUMN', @level2name = N'BregProductHCPCSID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the HCPCS code is entered into (for products that have one).', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BregProductHCPCS', @level2type = N'COLUMN', @level2name = N'HCPCS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in the Breg Product HCPCS is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'BregProductHCPCS', @level2type = N'COLUMN', @level2name = N'IsActive';

