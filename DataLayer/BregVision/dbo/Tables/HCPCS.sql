﻿CREATE TABLE [dbo].[HCPCS] (
    [HCPCSID]            INT          IDENTITY (1, 1) NOT NULL,
    [Code]               VARCHAR (5)  NOT NULL,
    [Description]        VARCHAR (50) NULL,
    [IsCustomFabricated] BIT          CONSTRAINT [DF_HCPCS_IsCustomFabricated] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_HCPCS] PRIMARY KEY CLUSTERED ([HCPCSID] ASC),
    CONSTRAINT [IX_HCPCS_Code_Unique] UNIQUE NONCLUSTERED ([Code] ASC)
);

