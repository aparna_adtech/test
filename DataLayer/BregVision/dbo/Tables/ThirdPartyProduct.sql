﻿CREATE TABLE [dbo].[ThirdPartyProduct] (
    [ThirdPartyProductID]            INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PracticeCatalogSupplierBrandID] INT           NOT NULL,
    [Code]                           VARCHAR (50)  NOT NULL,
    [Name]                           VARCHAR (100) NOT NULL,
    [ShortName]                      VARCHAR (50)  NOT NULL,
    [Packaging]                      VARCHAR (50)  NOT NULL,
    [LeftRightSide]                  VARCHAR (10)  NULL,
    [Size]                           VARCHAR (50)  NULL,
    [Color]                          VARCHAR (50)  NULL,
    [Gender]                         VARCHAR (10)  NULL,
    [WholesaleListCost]              SMALLMONEY    NULL,
    [Description]                    VARCHAR (200) NULL,
    [IsDiscontinued]                 BIT           NOT NULL,
    [DateDiscontinued]               DATETIME      NULL,
    [OldMasterCatalogProductID]      INT           NULL,
    [CreatedUserID]                  INT           NOT NULL,
    [CreatedDate]                    DATETIME      CONSTRAINT [DF__ThirdPart__Creat__70DDC3D8] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]                 INT           NULL,
    [ModifiedDate]                   DATETIME      NULL,
    [IsActive]                       BIT           NOT NULL,
    [Source]                         VARCHAR (50)  NULL,
    [UPCCode]                        VARCHAR (50)  NULL,
    [IsLogoAblePart]                 BIT           DEFAULT ((0)) NOT NULL,
    [Mod1]                           VARCHAR (2)   NULL,
    [Mod2]                           VARCHAR (2)   NULL,
    [Mod3]                           VARCHAR (2)   NULL,
    CONSTRAINT [ThirdPartyProduct_PK] PRIMARY KEY CLUSTERED ([ThirdPartyProductID] ASC),
    CONSTRAINT [FK_ThirdPartyProduct_PracticeCatalogSupplierBrand] FOREIGN KEY ([PracticeCatalogSupplierBrandID]) REFERENCES [dbo].[PracticeCatalogSupplierBrand] ([PracticeCatalogSupplierBrandID])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_598]
    ON [dbo].[ThirdPartyProduct]([PracticeCatalogSupplierBrandID] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_699]
    ON [dbo].[ThirdPartyProduct]([PracticeCatalogSupplierBrandID] ASC, [IsActive] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_596]
    ON [dbo].[ThirdPartyProduct]([PracticeCatalogSupplierBrandID] ASC)
    INCLUDE([ThirdPartyProductID], [Code], [Name], [ShortName], [Packaging], [LeftRightSide], [Size], [Color], [Gender], [WholesaleListCost], [Description], [IsDiscontinued], [DateDiscontinued], [OldMasterCatalogProductID], [CreatedUserID], [CreatedDate], [ModifiedUserID], [ModifiedDate], [IsActive], [Source]);


GO
CREATE NONCLUSTERED INDEX [missing_index_701]
    ON [dbo].[ThirdPartyProduct]([IsActive] ASC)
    INCLUDE([PracticeCatalogSupplierBrandID]);


GO
CREATE NONCLUSTERED INDEX [missing_index_279]
    ON [dbo].[ThirdPartyProduct]([IsActive] ASC, [Source] ASC)
    INCLUDE([ThirdPartyProductID], [PracticeCatalogSupplierBrandID], [WholesaleListCost]);


GO
CREATE NONCLUSTERED INDEX [missing_index_329]
    ON [dbo].[ThirdPartyProduct]([IsActive] ASC, [Code] ASC)
    INCLUDE([ThirdPartyProductID], [ShortName], [LeftRightSide], [Size], [Gender]);


GO
CREATE NONCLUSTERED INDEX [missing_index_576]
    ON [dbo].[ThirdPartyProduct]([IsActive] ASC, [Code] ASC)
    INCLUDE([ThirdPartyProductID], [Name], [Packaging], [LeftRightSide], [Size], [Gender]);


GO
CREATE NONCLUSTERED INDEX [missing_index_235]
    ON [dbo].[ThirdPartyProduct]([IsActive] ASC)
    INCLUDE([ThirdPartyProductID], [Code], [Name], [LeftRightSide], [Size]);


GO
CREATE NONCLUSTERED INDEX [missing_index_110]
    ON [dbo].[ThirdPartyProduct]([IsActive] ASC, [Code] ASC)
    INCLUDE([ThirdPartyProductID], [ShortName]);


GO
CREATE NONCLUSTERED INDEX [missing_index_781]
    ON [dbo].[ThirdPartyProduct]([IsActive] ASC, [Code] ASC)
    INCLUDE([ThirdPartyProductID], [Name], [LeftRightSide], [Size]);


GO
CREATE NONCLUSTERED INDEX [missing_index_1023]
    ON [dbo].[ThirdPartyProduct]([IsActive] ASC)
    INCLUDE([ThirdPartyProductID], [Code], [ShortName], [LeftRightSide], [Size]);


GO
CREATE NONCLUSTERED INDEX [missing_index_803]
    ON [dbo].[ThirdPartyProduct]([IsActive] ASC)
    INCLUDE([ThirdPartyProductID], [Code], [Name], [ShortName], [Packaging], [LeftRightSide], [Size], [Gender]);


GO
CREATE NONCLUSTERED INDEX [missing_index_291]
    ON [dbo].[ThirdPartyProduct]([Code] ASC)
    INCLUDE([ThirdPartyProductID], [Name], [Packaging], [LeftRightSide], [Size], [Gender]);


GO
CREATE NONCLUSTERED INDEX [missing_index_333]
    ON [dbo].[ThirdPartyProduct]([IsActive] ASC)
    INCLUDE([ThirdPartyProductID], [Code], [Name], [ShortName], [LeftRightSide], [Size], [Gender]);


GO
CREATE NONCLUSTERED INDEX [missing_index_95]
    ON [dbo].[ThirdPartyProduct]([IsActive] ASC)
    INCLUDE([ThirdPartyProductID], [Code], [Name], [ShortName], [LeftRightSide], [Size]);


GO
CREATE NONCLUSTERED INDEX [missing_index_893]
    ON [dbo].[ThirdPartyProduct]([IsActive] ASC)
    INCLUDE([ThirdPartyProductID], [Code], [Name], [ShortName], [Packaging], [LeftRightSide], [Size], [Color], [Gender]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_ThirdPartyProduct_8_79339347__K5_K20_K4_K1_K3_K7_K8_K10]
    ON [dbo].[ThirdPartyProduct]([ShortName] ASC, [IsActive] ASC, [Name] ASC, [ThirdPartyProductID] ASC, [Code] ASC, [LeftRightSide] ASC, [Size] ASC, [Gender] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_ThirdPartyProduct_8_79339347__K20_K4_K5_K3_K1_K7_K8_K10]
    ON [dbo].[ThirdPartyProduct]([IsActive] ASC, [Name] ASC, [ShortName] ASC, [Code] ASC, [ThirdPartyProductID] ASC, [LeftRightSide] ASC, [Size] ASC, [Gender] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_ThirdPartyProduct_12_79339347__K1_3_4_5_7_8_10_20]
    ON [dbo].[ThirdPartyProduct]([ThirdPartyProductID] ASC)
    INCLUDE([Code], [Name], [ShortName], [LeftRightSide], [Size], [Gender], [IsActive]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_ThirdPartyProduct_12_79339347__K20_K4_K5_K3_K1_K6_K8_K9_K10]
    ON [dbo].[ThirdPartyProduct]([IsActive] ASC, [Name] ASC, [ShortName] ASC, [Code] ASC, [ThirdPartyProductID] ASC, [Packaging] ASC, [Size] ASC, [Color] ASC, [Gender] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_ThirdPartyProduct_12_79339347__K1_K20_K4_K5_K3_K6_K8_K9_K10]
    ON [dbo].[ThirdPartyProduct]([ThirdPartyProductID] ASC, [IsActive] ASC, [Name] ASC, [ShortName] ASC, [Code] ASC, [Packaging] ASC, [Size] ASC, [Color] ASC, [Gender] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_ThirdPartyProduct_12_79339347__K1_K4_K3_K7_K8]
    ON [dbo].[ThirdPartyProduct]([ThirdPartyProductID] ASC, [Name] ASC, [Code] ASC, [LeftRightSide] ASC, [Size] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_ThirdPartyProduct_12_79339347__K1_20]
    ON [dbo].[ThirdPartyProduct]([ThirdPartyProductID] ASC)
    INCLUDE([IsActive]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_ThirdPartyProduct_12_79339347__K20_K1]
    ON [dbo].[ThirdPartyProduct]([IsActive] ASC, [ThirdPartyProductID] ASC);


GO
CREATE STATISTICS [_dta_stat_79339347_1_10]
    ON [dbo].[ThirdPartyProduct]([ThirdPartyProductID], [Gender]);


GO
CREATE STATISTICS [_dta_stat_79339347_1_9]
    ON [dbo].[ThirdPartyProduct]([ThirdPartyProductID], [Color]);


GO
CREATE STATISTICS [_dta_stat_79339347_4_5]
    ON [dbo].[ThirdPartyProduct]([Name], [ShortName]);


GO
CREATE STATISTICS [_dta_stat_79339347_6_10_9]
    ON [dbo].[ThirdPartyProduct]([Packaging], [Gender], [Color]);


GO
CREATE STATISTICS [_dta_stat_79339347_5_1_20]
    ON [dbo].[ThirdPartyProduct]([ShortName], [ThirdPartyProductID], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_79339347_20_4_3]
    ON [dbo].[ThirdPartyProduct]([IsActive], [Name], [Code]);


GO
CREATE STATISTICS [_dta_stat_79339347_1_6_10_9]
    ON [dbo].[ThirdPartyProduct]([ThirdPartyProductID], [Packaging], [Gender], [Color]);


GO
CREATE STATISTICS [_dta_stat_79339347_20_4_5_1]
    ON [dbo].[ThirdPartyProduct]([IsActive], [Name], [ShortName], [ThirdPartyProductID]);


GO
CREATE STATISTICS [_dta_stat_79339347_4_3_20_5]
    ON [dbo].[ThirdPartyProduct]([Name], [Code], [IsActive], [ShortName]);


GO
CREATE STATISTICS [_dta_stat_79339347_1_4_3_7_8]
    ON [dbo].[ThirdPartyProduct]([ThirdPartyProductID], [Name], [Code], [LeftRightSide], [Size]);


GO
CREATE STATISTICS [_dta_stat_79339347_4_3_7_8_20]
    ON [dbo].[ThirdPartyProduct]([Name], [Code], [LeftRightSide], [Size], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_79339347_5_3_7_8_10]
    ON [dbo].[ThirdPartyProduct]([ShortName], [Code], [LeftRightSide], [Size], [Gender]);


GO
CREATE STATISTICS [_dta_stat_79339347_1_5_3_7_8]
    ON [dbo].[ThirdPartyProduct]([ThirdPartyProductID], [ShortName], [Code], [LeftRightSide], [Size]);


GO
CREATE STATISTICS [_dta_stat_79339347_1_4_5_3_7_8]
    ON [dbo].[ThirdPartyProduct]([ThirdPartyProductID], [Name], [ShortName], [Code], [LeftRightSide], [Size]);


GO
CREATE STATISTICS [_dta_stat_79339347_4_3_7_8_10_20]
    ON [dbo].[ThirdPartyProduct]([Name], [Code], [LeftRightSide], [Size], [Gender], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_79339347_20_5_3_7_8_10]
    ON [dbo].[ThirdPartyProduct]([IsActive], [ShortName], [Code], [LeftRightSide], [Size], [Gender]);


GO
CREATE STATISTICS [_dta_stat_79339347_4_3_6_7_8_10_20]
    ON [dbo].[ThirdPartyProduct]([Name], [Code], [Packaging], [LeftRightSide], [Size], [Gender], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_79339347_1_20_4_3_7_8_10]
    ON [dbo].[ThirdPartyProduct]([ThirdPartyProductID], [IsActive], [Name], [Code], [LeftRightSide], [Size], [Gender]);


GO
CREATE STATISTICS [_dta_stat_79339347_4_3_6_7_8_10_5]
    ON [dbo].[ThirdPartyProduct]([Name], [Code], [Packaging], [LeftRightSide], [Size], [Gender], [ShortName]);


GO
CREATE STATISTICS [_dta_stat_79339347_20_1_5_3_6_7_8]
    ON [dbo].[ThirdPartyProduct]([IsActive], [ThirdPartyProductID], [ShortName], [Code], [Packaging], [LeftRightSide], [Size]);


GO
CREATE STATISTICS [_dta_stat_79339347_5_3_6_7_8_10_20_1]
    ON [dbo].[ThirdPartyProduct]([ShortName], [Code], [Packaging], [LeftRightSide], [Size], [Gender], [IsActive], [ThirdPartyProductID]);


GO
CREATE STATISTICS [_dta_stat_79339347_3_5_6_7_8_9_10_20]
    ON [dbo].[ThirdPartyProduct]([Code], [ShortName], [Packaging], [LeftRightSide], [Size], [Color], [Gender], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_79339347_4_3_5_1_6_7_8_10]
    ON [dbo].[ThirdPartyProduct]([Name], [Code], [ShortName], [ThirdPartyProductID], [Packaging], [LeftRightSide], [Size], [Gender]);


GO
CREATE STATISTICS [_dta_stat_79339347_1_4_3_6_7_8_10_9]
    ON [dbo].[ThirdPartyProduct]([ThirdPartyProductID], [Name], [Code], [Packaging], [LeftRightSide], [Size], [Gender], [Color]);


GO
CREATE STATISTICS [_dta_stat_79339347_4_3_6_7_8_10_9_20_1]
    ON [dbo].[ThirdPartyProduct]([Name], [Code], [Packaging], [LeftRightSide], [Size], [Gender], [Color], [IsActive], [ThirdPartyProductID]);


GO
CREATE STATISTICS [_dta_stat_79339347_20_3_5_1_6_7_8_9_10]
    ON [dbo].[ThirdPartyProduct]([IsActive], [Code], [ShortName], [ThirdPartyProductID], [Packaging], [LeftRightSide], [Size], [Color], [Gender]);


GO
CREATE STATISTICS [_dta_stat_79339347_4_1_5_3_6_7_8_10_9]
    ON [dbo].[ThirdPartyProduct]([Name], [ThirdPartyProductID], [ShortName], [Code], [Packaging], [LeftRightSide], [Size], [Gender], [Color]);


GO
CREATE STATISTICS [_dta_stat_79339347_20_4_3_6_7_8]
    ON [dbo].[ThirdPartyProduct]([IsActive], [Name], [Code], [Packaging], [LeftRightSide], [Size]);


GO
CREATE STATISTICS [_dta_stat_79339347_4_3_6_7_8_10_9_5]
    ON [dbo].[ThirdPartyProduct]([Name], [Code], [Packaging], [LeftRightSide], [Size], [Gender], [Color], [ShortName]);


GO
CREATE STATISTICS [_dta_stat_79339347_4_5_3_6_8_9_10_20]
    ON [dbo].[ThirdPartyProduct]([Name], [ShortName], [Code], [Packaging], [Size], [Color], [Gender], [IsActive]);


GO
/*
	--  [dbo].[trg_ThirdPartyProduct_Deleted] ON [dbo].[ThirdPartyProduct]
	-- Trigger
	John Bongiorni  
	2008.04.10
	
	Trigger fires on Delete from ThirdPartyProduct
	and inserts the deleted record into
	ThirdPartyProduct_Audit table.
*/
CREATE TRIGGER [dbo].[trg_ThirdPartyProduct_Deleted] ON [dbo].[ThirdPartyProduct]
    FOR DELETE
AS
    begin

        INSERT INTO
            dbo.ThirdPartyProduct_Audit
            (    
                 ThirdPartyProductID
               , PracticeCatalogSupplierBrandID
			   , Code
			   , Name
			   , ShortName
			   , Packaging
			   , LeftRightSide
			   , Size
			   , Color
			   , Gender
			   , WholesaleListCost
			   , Description
			   , IsDiscontinued
			   , DateDiscontinued
			   , OldMasterCatalogProductID
			   , CreatedUserID
			   , CreatedDate
			   , ModifiedUserID
			   , ModifiedDate
			   , IsActive
			   , Source	 
			   , AuditAction  
            )
            SELECT
				 ThirdPartyProductID
               , PracticeCatalogSupplierBrandID
			   , Code
			   , Name
			   , ShortName
			   , Packaging
			   , LeftRightSide
			   , Size
			   , Color
			   , Gender
			   , WholesaleListCost
			   , Description
			   , IsDiscontinued
			   , DateDiscontinued
			   , OldMasterCatalogProductID
			   , CreatedUserID
			   , CreatedDate
			   , ModifiedUserID
			   , ModifiedDate
			   , IsActive
			   , Source	
               , 'Deleted'
            FROM
                deleted
    END

GO
/*
	--  [dbo].[trg_ThirdPartyProduct_Updated] ON [dbo].[ThirdPartyProduct]
	-- Trigger
	John Bongiorni  
	2008.04.10
	
	Trigger fires on Update from ThirdPartyProduct table
	and inserts the deleted record into
	ThirdPartyProduct_Audit table.
*/
CREATE TRIGGER [dbo].[trg_ThirdPartyProduct_Updated] ON [dbo].[ThirdPartyProduct]
    FOR UPDATE
AS
    begin

        INSERT INTO
            dbo.ThirdPartyProduct_Audit
            (    
                 ThirdPartyProductID
               , PracticeCatalogSupplierBrandID
			   , Code
			   , Name
			   , ShortName
			   , Packaging
			   , LeftRightSide
			   , Size
			   , Color
			   , Gender
			   , WholesaleListCost
			   , Description
			   , IsDiscontinued
			   , DateDiscontinued
			   , OldMasterCatalogProductID
			   , CreatedUserID
			   , CreatedDate
			   , ModifiedUserID
			   , ModifiedDate
			   , IsActive
			   , Source	 
			   , AuditAction  
            )
            SELECT
				 ThirdPartyProductID
               , PracticeCatalogSupplierBrandID
			   , Code
			   , Name
			   , ShortName
			   , Packaging
			   , LeftRightSide
			   , Size
			   , Color
			   , Gender
			   , WholesaleListCost
			   , Description
			   , IsDiscontinued
			   , DateDiscontinued
			   , OldMasterCatalogProductID
			   , CreatedUserID
			   , CreatedDate
			   , ModifiedUserID
			   , ModifiedDate
			   , IsActive
			   , Source	
               , 'Updated Old Record'
            FROM
                deleted
  
        INSERT INTO
            dbo.ThirdPartyProduct_Audit
            (    
                 ThirdPartyProductID
               , PracticeCatalogSupplierBrandID
			   , Code
			   , Name
			   , ShortName
			   , Packaging
			   , LeftRightSide
			   , Size
			   , Color
			   , Gender
			   , WholesaleListCost
			   , Description
			   , IsDiscontinued
			   , DateDiscontinued
			   , OldMasterCatalogProductID
			   , CreatedUserID
			   , CreatedDate
			   , ModifiedUserID
			   , ModifiedDate
			   , IsActive
			   , Source	 
			   , AuditAction  
            )
            SELECT
				 ThirdPartyProductID
               , PracticeCatalogSupplierBrandID
			   , Code
			   , Name
			   , ShortName
			   , Packaging
			   , LeftRightSide
			   , Size
			   , Color
			   , Gender
			   , WholesaleListCost
			   , Description
			   , IsDiscontinued
			   , DateDiscontinued
			   , OldMasterCatalogProductID
			   , CreatedUserID
			   , CreatedDate
			   , ModifiedUserID
			   , ModifiedDate
			   , IsActive
			   , Source	
               , 'Updated New Record'
            FROM
                inserted
    END

GO

CREATE TRIGGER [dbo].[trg_SyncModifiers_ThirdPartyProduct]
ON [dbo].[ThirdPartyProduct] FOR UPDATE, Insert

AS 
	SET NOCOUNT ON 
	DECLARE @ThirdPartyProductId as int  
	DECLARE @Mod1 as VARCHAR(2) 
	DECLARE @Mod2 as VARCHAR(2)
	DECLARE @Mod3 as VARCHAR(2)
	DECLARE @pcpMod1 as VARCHAR(2) 
	DECLARE @pcpMod2 as VARCHAR(2)
	DECLARE @pcpMod3 as VARCHAR(2)
	
	Select
		@ThirdPartyProductId = i.ThirdPartyProductID,
		@Mod1 = i.Mod1,
		@Mod2 = i.Mod2,
		@Mod3 = i.Mod3,
		@pcpMod1 = dbo.PracticeCatalogProduct.Mod1,
		@pcpMod2 = dbo.PracticeCatalogProduct.Mod2,
		@pcpMod3 = dbo.PracticeCatalogProduct.Mod3
	FROM inserted as i
	inner join PracticeCatalogProduct on i.ThirdPartyProductID = PracticeCatalogProduct.ThirdPartyProductID 

	If @Mod1 <> @pcpMod1 or @Mod2 <> @pcpMod1 or @Mod3 <> @pcpMod1
	BEGIN
		update dbo.PracticeCatalogProduct 
		set Mod1 = @Mod1, Mod2 = @Mod2, Mod3 = @Mod3
		WHERE ThirdPartyProductID = @ThirdPartyProductId
	END

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the Third Party Product table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartyProduct', @level2type = N'COLUMN', @level2name = N'ThirdPartyProductID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which a unique number is entered for each third-party product.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartyProduct', @level2type = N'COLUMN', @level2name = N'Code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the name of the third party product is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartyProduct', @level2type = N'COLUMN', @level2name = N'Name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the abbreviated name of the third party product is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartyProduct', @level2type = N'COLUMN', @level2name = N'ShortName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the quantity of the third party package is described (ea., qty., gal.)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartyProduct', @level2type = N'COLUMN', @level2name = N'Packaging';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the side the third-party product is meant for is entered into (L,R,U)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartyProduct', @level2type = N'COLUMN', @level2name = N'LeftRightSide';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the size of the third-party product is entered into (XXS - XXXXS)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartyProduct', @level2type = N'COLUMN', @level2name = N'Size';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the color of the third-party product is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartyProduct', @level2type = N'COLUMN', @level2name = N'Color';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the gender the third-party product is meant for is entered into (F,M,U)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartyProduct', @level2type = N'COLUMN', @level2name = N'Gender';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the third-party''s wholesale price is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartyProduct', @level2type = N'COLUMN', @level2name = N'WholesaleListCost';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the description of the third-party product is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartyProduct', @level2type = N'COLUMN', @level2name = N'Description';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a third-party product has been discontinued.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartyProduct', @level2type = N'COLUMN', @level2name = N'IsDiscontinued';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the date of the discountined product is automatically entered.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartyProduct', @level2type = N'COLUMN', @level2name = N'DateDiscontinued';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in third party products is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThirdPartyProduct', @level2type = N'COLUMN', @level2name = N'IsActive';

