﻿CREATE TABLE [dbo].[MasterCatalogProductTypeImage] (
    [MasterCatalogProductTypeImageID] INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ProductTypeImage]                IMAGE         NULL,
    [ImagePath]                       VARCHAR (100) NULL,
    [ImageFileName]                   VARCHAR (50)  NULL,
    CONSTRAINT [MasterCatalogProductTypeImage_PK] PRIMARY KEY CLUSTERED ([MasterCatalogProductTypeImageID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the Master Catalog Product Type Image table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogProductTypeImage', @level2type = N'COLUMN', @level2name = N'MasterCatalogProductTypeImageID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the product image is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogProductTypeImage', @level2type = N'COLUMN', @level2name = N'ProductTypeImage';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field  in which the product image path is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogProductTypeImage', @level2type = N'COLUMN', @level2name = N'ImagePath';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the name of the product image is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MasterCatalogProductTypeImage', @level2type = N'COLUMN', @level2name = N'ImageFileName';

