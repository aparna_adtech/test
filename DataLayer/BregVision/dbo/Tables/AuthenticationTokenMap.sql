﻿CREATE TABLE [dbo].[AuthenticationTokenMap] (
    [AuthenticationToken] UNIQUEIDENTIFIER NOT NULL,
    [BregVisionUserID]    INT              NOT NULL,
    [DeviceID]            INT              NOT NULL,
    [AuthDate]            DATETIME         CONSTRAINT [DF_AuthenticationTokenMap_AuthDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_AuthenticationTokenMap] PRIMARY KEY CLUSTERED ([AuthenticationToken] ASC),
    CONSTRAINT [FK_AuthenticationTokenMap_Device] FOREIGN KEY ([DeviceID]) REFERENCES [dbo].[Device] ([DeviceID]),
    CONSTRAINT [FK_AuthenticationTokenMap_User] FOREIGN KEY ([BregVisionUserID]) REFERENCES [dbo].[User] ([UserID])
);

