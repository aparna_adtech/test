﻿CREATE TABLE [dbo].[Medicare] (
    [HCPCs]          NVARCHAR (8)   NULL,
    [State]          NVARCHAR (2)   NULL,
    [MedicareCharge] SMALLMONEY     NULL,
    [Description]    NVARCHAR (255) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_Medicare]
    ON [dbo].[Medicare]([HCPCs] ASC, [State] ASC);

