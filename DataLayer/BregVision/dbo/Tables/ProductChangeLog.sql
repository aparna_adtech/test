﻿CREATE TABLE [dbo].[ProductChangeLog] (
    [Id]                       INT IDENTITY (1, 1) NOT NULL,
    [PracticeCatalogProductId] INT NULL,
    [ProductInventoryId]       INT NULL,
    [PracticeId]               INT NULL,
    [PracticeLocationId]       INT NULL,
    [MasterCatalogProductId]   INT NULL,
    CONSTRAINT [PK_ProductInventoryChangeLog] PRIMARY KEY CLUSTERED ([Id] ASC)
);

