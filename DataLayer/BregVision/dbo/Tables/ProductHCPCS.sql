﻿CREATE TABLE [dbo].[ProductHCPCS] (
    [ProductHCPCSID]           INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PracticeCatalogProductID] INT          NULL,
    [HCPCS]                    VARCHAR (20) NOT NULL,
    [CreatedUserID]            INT          NOT NULL,
    [CreatedDate]              DATETIME     NOT NULL,
    [ModifiedUserID]           INT          NULL,
    [ModifiedDate]             DATETIME     NULL,
    [IsActive]                 BIT          NOT NULL,
    [IsOffTheShelf]            BIT          CONSTRAINT [DF_ProductHCPCS_IsSplitCode] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [ProductHCPCS_PK] PRIMARY KEY CLUSTERED ([ProductHCPCSID] ASC),
    CONSTRAINT [PracticeCatalogProduct_ProductHCPCS_FK1] FOREIGN KEY ([PracticeCatalogProductID]) REFERENCES [dbo].[PracticeCatalogProduct] ([PracticeCatalogProductID])
);


GO
CREATE NONCLUSTERED INDEX [IX_ProductHCPCS]
    ON [dbo].[ProductHCPCS]([HCPCS] ASC);


GO
CREATE NONCLUSTERED INDEX [_dta_index_ProductHCPCS_12_1125579048__K2_3]
    ON [dbo].[ProductHCPCS]([PracticeCatalogProductID] ASC)
    INCLUDE([HCPCS]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_ProductHCPCS_12_1125579048__K3_K2]
    ON [dbo].[ProductHCPCS]([HCPCS] ASC, [PracticeCatalogProductID] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the Product Healthcare Common Procedure Coding System (HCPCS) table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductHCPCS', @level2type = N'COLUMN', @level2name = N'ProductHCPCSID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the HCPCS code is entered for products that have one.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductHCPCS', @level2type = N'COLUMN', @level2name = N'HCPCS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in the Product HCPCS is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductHCPCS', @level2type = N'COLUMN', @level2name = N'IsActive';

