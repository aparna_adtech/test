﻿CREATE TABLE [dbo].[ShippingCost] (
    [ShippingCostID]   INT        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [SupplierOrderID]  INT        NOT NULL,
    [DeliverySequence] TINYINT    NOT NULL,
    [ShippingCost]     SMALLMONEY NOT NULL,
    [CreatedUserID]    INT        NOT NULL,
    [CreatedDate]      DATETIME   NOT NULL,
    [ModifiedUserID]   INT        NULL,
    [ModifiedDate]     DATETIME   NULL,
    [IsActive]         BIT        NOT NULL,
    CONSTRAINT [ShippingCost_PK] PRIMARY KEY CLUSTERED ([ShippingCostID] ASC),
    CONSTRAINT [SupplierOrder_ShippingCost_FK1] FOREIGN KEY ([SupplierOrderID]) REFERENCES [dbo].[SupplierOrder] ([SupplierOrderID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for shipping cost table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ShippingCost', @level2type = N'COLUMN', @level2name = N'ShippingCostID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the sequence of delivery of the products will be displayed.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ShippingCost', @level2type = N'COLUMN', @level2name = N'DeliverySequence';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the cost of shipment will be displayed.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ShippingCost', @level2type = N'COLUMN', @level2name = N'ShippingCost';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in the Shipping Cost is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ShippingCost', @level2type = N'COLUMN', @level2name = N'IsActive';

