﻿CREATE TABLE [dbo].[ApiKey] (
    [ApiKey]      NVARCHAR (50) NOT NULL,
    [IsActive]    BIT           CONSTRAINT [DF_ApiKey_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedDate] DATETIME      NOT NULL,
    CONSTRAINT [PK_ApiKey] PRIMARY KEY CLUSTERED ([ApiKey] ASC)
);

