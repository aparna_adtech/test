﻿CREATE TABLE [dbo].[HelpMedia] (
    [Id]               INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [HelpEntityTypeId] INT            CONSTRAINT [DF_HelpMedia_HelpEntityTypeId] DEFAULT ((4)) NOT NULL,
    [Title]            NVARCHAR (200) NOT NULL,
    [Description]      NVARCHAR (500) NULL,
    [Url]              NVARCHAR (200) NULL,
    CONSTRAINT [PK_HelpMedia] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_HelpMedia_HelpEntityTypes] FOREIGN KEY ([HelpEntityTypeId]) REFERENCES [dbo].[HelpEntityTypes] ([Id])
);

