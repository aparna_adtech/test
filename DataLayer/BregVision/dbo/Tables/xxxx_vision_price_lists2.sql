﻿CREATE TABLE [dbo].[xxxx_vision_price_lists2] (
    [CUSTOMER]         NVARCHAR (255) NULL,
    [ACCT_NUM]         NVARCHAR (255) NULL,
    [ITEM_NUM]         NVARCHAR (255) NULL,
    [ITEM_DESCRIPTION] NVARCHAR (255) NULL,
    [UOM]              NVARCHAR (255) NULL,
    [LIST_PRICE]       FLOAT (53)     NULL,
    [PRICE]            FLOAT (53)     NULL
);

