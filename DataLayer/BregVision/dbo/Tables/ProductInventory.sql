﻿CREATE TABLE [dbo].[ProductInventory] (
    [ProductInventoryID]        INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PracticeLocationID]        INT      NOT NULL,
    [PracticeCatalogProductID]  INT      NOT NULL,
    [QuantityOnHandPerSystem]   INT      NOT NULL,
    [ParLevel]                  INT      NULL,
    [ReorderLevel]              INT      NULL,
    [CriticalLevel]             INT      NULL,
    [CreatedUserID]             INT      NOT NULL,
    [CreatedDate]               DATETIME CONSTRAINT [DF__ProductIn__Creat__3A81B327] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]            INT      NULL,
    [ModifiedDate]              DATETIME NULL,
    [IsNotFlaggedForReorder]    BIT      CONSTRAINT [DF_ProductInventory_IsNotFlaggedForReorder] DEFAULT ((0)) NOT NULL,
    [IsSetToActive]             BIT      CONSTRAINT [DF_ProductInventory_IsSetToActive] DEFAULT ((0)) NULL,
    [IsActive]                  BIT      NOT NULL,
    [ConsignmentQuantity]       INT      DEFAULT ((0)) NOT NULL,
    [IsConsignment]             BIT      DEFAULT ((0)) NOT NULL,
    [ConsignmentLockParLevel]   BIT      DEFAULT ((0)) NOT NULL,
    [IsBuyout]                  BIT      DEFAULT ((0)) NOT NULL,
    [IsRMA]                     BIT      DEFAULT ((0)) NOT NULL,
    [BuyoutRMAInventoryCycleID] INT      NULL,
    [IsLogoAblePart]            BIT      DEFAULT ((0)) NOT NULL,
    CONSTRAINT [ProductInventory_PK] PRIMARY KEY CLUSTERED ([ProductInventoryID] ASC),
    CONSTRAINT [PracticeCatalogProduct_ProductInventory_FK1] FOREIGN KEY ([PracticeCatalogProductID]) REFERENCES [dbo].[PracticeCatalogProduct] ([PracticeCatalogProductID]),
    CONSTRAINT [PracticeLocation_ProductInventory_FK1] FOREIGN KEY ([PracticeLocationID]) REFERENCES [dbo].[PracticeLocation] ([PracticeLocationID]),
    CONSTRAINT [ProductInventory_UniqueIndex_ClinicLocationID_ClinicCatalogProductID_UniqueConstraint] UNIQUE NONCLUSTERED ([PracticeLocationID] ASC, [PracticeCatalogProductID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [missing_index_487]
    ON [dbo].[ProductInventory]([PracticeCatalogProductID] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_939]
    ON [dbo].[ProductInventory]([ModifiedUserID] ASC, [IsActive] ASC)
    INCLUDE([ProductInventoryID], [PracticeLocationID], [PracticeCatalogProductID], [QuantityOnHandPerSystem], [ParLevel], [ReorderLevel], [CriticalLevel], [CreatedUserID], [CreatedDate], [ModifiedDate], [IsNotFlaggedForReorder], [IsSetToActive]);


GO
CREATE NONCLUSTERED INDEX [missing_index_430]
    ON [dbo].[ProductInventory]([PracticeLocationID] ASC, [IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [IsNotFlaggedForReorder]);


GO
CREATE NONCLUSTERED INDEX [missing_index_932]
    ON [dbo].[ProductInventory]([IsSetToActive] ASC, [IsActive] ASC)
    INCLUDE([ProductInventoryID], [PracticeLocationID], [PracticeCatalogProductID], [QuantityOnHandPerSystem], [ParLevel], [ReorderLevel], [CriticalLevel], [CreatedUserID], [CreatedDate], [ModifiedUserID], [ModifiedDate], [IsNotFlaggedForReorder]);


GO
CREATE NONCLUSTERED INDEX [missing_index_427]
    ON [dbo].[ProductInventory]([PracticeLocationID] ASC, [IsActive] ASC)
    INCLUDE([PracticeCatalogProductID]);


GO
CREATE NONCLUSTERED INDEX [missing_index_438]
    ON [dbo].[ProductInventory]([PracticeLocationID] ASC, [IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [QuantityOnHandPerSystem], [ParLevel], [ReorderLevel], [CriticalLevel]);


GO
CREATE NONCLUSTERED INDEX [missing_index_936]
    ON [dbo].[ProductInventory]([IsSetToActive] ASC, [IsActive] ASC)
    INCLUDE([ProductInventoryID]);


GO
CREATE NONCLUSTERED INDEX [missing_index_787]
    ON [dbo].[ProductInventory]([PracticeCatalogProductID] ASC, [IsActive] ASC)
    INCLUDE([QuantityOnHandPerSystem], [ParLevel], [ReorderLevel], [CriticalLevel]);


GO
CREATE NONCLUSTERED INDEX [missing_index_456]
    ON [dbo].[ProductInventory]([PracticeLocationID] ASC, [IsActive] ASC)
    INCLUDE([ProductInventoryID], [PracticeCatalogProductID], [QuantityOnHandPerSystem], [ParLevel], [ReorderLevel], [CriticalLevel]);


GO
CREATE NONCLUSTERED INDEX [missing_index_1032]
    ON [dbo].[ProductInventory]([PracticeLocationID] ASC, [IsSetToActive] ASC, [IsActive] ASC)
    INCLUDE([ProductInventoryID], [PracticeCatalogProductID], [QuantityOnHandPerSystem], [ParLevel], [ReorderLevel], [CriticalLevel], [IsNotFlaggedForReorder]);


GO
CREATE NONCLUSTERED INDEX [missing_index_589]
    ON [dbo].[ProductInventory]([PracticeLocationID] ASC, [IsActive] ASC)
    INCLUDE([ProductInventoryID], [PracticeCatalogProductID], [QuantityOnHandPerSystem]);


GO
CREATE NONCLUSTERED INDEX [missing_index_681]
    ON [dbo].[ProductInventory]([IsActive] ASC)
    INCLUDE([PracticeCatalogProductID], [QuantityOnHandPerSystem], [ParLevel], [ReorderLevel], [CriticalLevel]);


GO
CREATE NONCLUSTERED INDEX [_dta_index_ProductInventory_8_1615344819__K2_K14_K3_K1_K4]
    ON [dbo].[ProductInventory]([PracticeLocationID] ASC, [IsActive] ASC, [PracticeCatalogProductID] ASC, [ProductInventoryID] ASC, [QuantityOnHandPerSystem] ASC);


GO
CREATE NONCLUSTERED INDEX [FK_PCP]
    ON [dbo].[ProductInventory]([PracticeCatalogProductID] ASC);


GO
CREATE STATISTICS [_dta_stat_1615344819_2_1]
    ON [dbo].[ProductInventory]([PracticeLocationID], [ProductInventoryID]);


GO
CREATE STATISTICS [_dta_stat_1615344819_13_3]
    ON [dbo].[ProductInventory]([IsSetToActive], [PracticeCatalogProductID]);


GO
CREATE STATISTICS [_dta_stat_1615344819_13_1_2]
    ON [dbo].[ProductInventory]([IsSetToActive], [ProductInventoryID], [PracticeLocationID]);


GO
CREATE STATISTICS [_dta_stat_1615344819_3_4_2]
    ON [dbo].[ProductInventory]([PracticeCatalogProductID], [QuantityOnHandPerSystem], [PracticeLocationID]);


GO
CREATE STATISTICS [_dta_stat_1615344819_3_1_2]
    ON [dbo].[ProductInventory]([PracticeCatalogProductID], [ProductInventoryID], [PracticeLocationID]);


GO
CREATE STATISTICS [_dta_stat_1615344819_4_2_14]
    ON [dbo].[ProductInventory]([QuantityOnHandPerSystem], [PracticeLocationID], [IsActive]);


GO
CREATE STATISTICS [_dta_stat_1615344819_14_1_4_3]
    ON [dbo].[ProductInventory]([IsActive], [ProductInventoryID], [QuantityOnHandPerSystem], [PracticeCatalogProductID]);


GO
CREATE STATISTICS [_dta_stat_1615344819_2_14_13_4]
    ON [dbo].[ProductInventory]([PracticeLocationID], [IsActive], [IsSetToActive], [QuantityOnHandPerSystem]);


GO
CREATE STATISTICS [_dta_stat_1615344819_2_3_1_13]
    ON [dbo].[ProductInventory]([PracticeLocationID], [PracticeCatalogProductID], [ProductInventoryID], [IsSetToActive]);


GO
CREATE STATISTICS [_dta_stat_1615344819_4_2_3_1]
    ON [dbo].[ProductInventory]([QuantityOnHandPerSystem], [PracticeLocationID], [PracticeCatalogProductID], [ProductInventoryID]);


GO
CREATE STATISTICS [_dta_stat_1615344819_1_14_4_2]
    ON [dbo].[ProductInventory]([ProductInventoryID], [IsActive], [QuantityOnHandPerSystem], [PracticeLocationID]);


GO
CREATE STATISTICS [_dta_stat_1615344819_4_5_2_14_13]
    ON [dbo].[ProductInventory]([QuantityOnHandPerSystem], [ParLevel], [PracticeLocationID], [IsActive], [IsSetToActive]);


GO
CREATE STATISTICS [_dta_stat_1615344819_1_4_2_14_13]
    ON [dbo].[ProductInventory]([ProductInventoryID], [QuantityOnHandPerSystem], [PracticeLocationID], [IsActive], [IsSetToActive]);


GO
CREATE STATISTICS [_dta_stat_1615344819_3_2_13_14_4_5]
    ON [dbo].[ProductInventory]([PracticeCatalogProductID], [PracticeLocationID], [IsSetToActive], [IsActive], [QuantityOnHandPerSystem], [ParLevel]);


GO
CREATE STATISTICS [_dta_stat_1615344819_4_5_6_7_1_14_2]
    ON [dbo].[ProductInventory]([QuantityOnHandPerSystem], [ParLevel], [ReorderLevel], [CriticalLevel], [ProductInventoryID], [IsActive], [PracticeLocationID]);


GO
CREATE STATISTICS [_dta_stat_1615344819_1_4_5_6_7_12_14_13_2]
    ON [dbo].[ProductInventory]([ProductInventoryID], [QuantityOnHandPerSystem], [ParLevel], [ReorderLevel], [CriticalLevel], [IsNotFlaggedForReorder], [IsActive], [IsSetToActive], [PracticeLocationID]);


GO
CREATE STATISTICS [_dta_stat_1615344819_3_14_13]
    ON [dbo].[ProductInventory]([PracticeCatalogProductID], [IsActive], [IsSetToActive]);


GO
CREATE STATISTICS [_dta_stat_1615344819_14_1_3]
    ON [dbo].[ProductInventory]([IsActive], [ProductInventoryID], [PracticeCatalogProductID]);


GO
CREATE STATISTICS [_dta_stat_1615344819_4_5_6_7_14_13]
    ON [dbo].[ProductInventory]([QuantityOnHandPerSystem], [ParLevel], [ReorderLevel], [CriticalLevel], [IsActive], [IsSetToActive]);


GO
CREATE STATISTICS [_dta_stat_1615344819_13_14_2_3_4_5_6_7]
    ON [dbo].[ProductInventory]([IsSetToActive], [IsActive], [PracticeLocationID], [PracticeCatalogProductID], [QuantityOnHandPerSystem], [ParLevel], [ReorderLevel], [CriticalLevel]);


GO


/*
	--  dbo.trg_ProductInventory_Inserted ON dbo.ProductInventory
	-- Trigger
	Henry Chan  
	2011.06.30
	
	Trigger fires on Insert from ProductInventory
	and inserts the inserted record into
	ProductInventory_Audit table.
*/
CREATE TRIGGER [dbo].[trg_ProductInventory_Inserted] ON [dbo].[ProductInventory]
    FOR INSERT
AS
    BEGIN

        INSERT  INTO dbo.ProductInventory_Audit
                ( ProductInventoryID ,
                  PracticeLocationID ,
                  PracticeCatalogProductID ,
                  QuantityOnHandPerSystem ,
                  ParLevel ,
                  ReorderLevel ,
                  CriticalLevel ,
                  CreatedUserID ,
                  CreatedDate ,
                  ModifiedUserID ,
                  ModifiedDate ,
                  IsNotFlaggedForReorder ,
                  IsSetToActive ,
                  IsActive ,
                  ConsignmentQuantity ,
                  IsConsignment ,
                  ConsignmentLockParLevel ,
                  IsBuyout ,
                  IsRMA ,
                  BuyoutRMAInventoryCycleID ,
                  IsLogoAblePart ,
                  AuditAction			   
                )
                SELECT  ProductInventoryID ,
                        PracticeLocationID ,
                        PracticeCatalogProductID ,
                        QuantityOnHandPerSystem ,
                        ParLevel ,
                        ReorderLevel ,
                        CriticalLevel ,
                        CreatedUserID ,
                        CreatedDate ,
                        ModifiedUserID ,
                        ModifiedDate ,
                        IsNotFlaggedForReorder ,
                        IsSetToActive ,
                        IsActive ,
                        ConsignmentQuantity ,
                        IsConsignment ,
                        ConsignmentLockParLevel ,
                        IsBuyout ,
                        IsRMA ,
                        BuyoutRMAInventoryCycleID ,
                        IsLogoAblePart ,
                        'Inserted'
                FROM    inserted
  
    END

GO



/*
	--  dbo.trg_ProductInventory_Updated ON dbo.ProductInventory
	-- Trigger
	Henry Chan  
	2011.06.30
	
	Trigger fires on Update from ProductInventory
	and inserts the old and new records into
	ProductInventory_Audit table.
*/
CREATE TRIGGER [dbo].[trg_ProductInventory_Updated] ON [dbo].[ProductInventory]
    FOR UPDATE
AS
    BEGIN

        INSERT  INTO dbo.ProductInventory_Audit
                ( ProductInventoryID ,
                  PracticeLocationID ,
                  PracticeCatalogProductID ,
                  QuantityOnHandPerSystem ,
                  ParLevel ,
                  ReorderLevel ,
                  CriticalLevel ,
                  CreatedUserID ,
                  CreatedDate ,
                  ModifiedUserID ,
                  ModifiedDate ,
                  IsNotFlaggedForReorder ,
                  IsSetToActive ,
                  IsActive ,
                  ConsignmentQuantity ,
                  IsConsignment ,
                  ConsignmentLockParLevel ,
                  IsBuyout ,
                  IsRMA ,
                  BuyoutRMAInventoryCycleID ,
                  IsLogoAblePart ,
                  AuditAction			   
                )
                SELECT  ProductInventoryID ,
                        PracticeLocationID ,
                        PracticeCatalogProductID ,
                        QuantityOnHandPerSystem ,
                        ParLevel ,
                        ReorderLevel ,
                        CriticalLevel ,
                        CreatedUserID ,
                        CreatedDate ,
                        ModifiedUserID ,
                        ModifiedDate ,
                        IsNotFlaggedForReorder ,
                        IsSetToActive ,
                        IsActive ,
                        ConsignmentQuantity ,
                        IsConsignment ,
                        ConsignmentLockParLevel ,
                        IsBuyout ,
                        IsRMA ,
                        BuyoutRMAInventoryCycleID ,
                        IsLogoAblePart ,
                        'Update (old)'
                FROM    deleted;
                
        INSERT  INTO dbo.ProductInventory_Audit
                ( ProductInventoryID ,
                  PracticeLocationID ,
                  PracticeCatalogProductID ,
                  QuantityOnHandPerSystem ,
                  ParLevel ,
                  ReorderLevel ,
                  CriticalLevel ,
                  CreatedUserID ,
                  CreatedDate ,
                  ModifiedUserID ,
                  ModifiedDate ,
                  IsNotFlaggedForReorder ,
                  IsSetToActive ,
                  IsActive ,
                  ConsignmentQuantity ,
                  IsConsignment ,
                  ConsignmentLockParLevel ,
                  IsBuyout ,
                  IsRMA ,
                  BuyoutRMAInventoryCycleID ,
                  IsLogoAblePart ,
                  AuditAction			   
                )
                SELECT  ProductInventoryID ,
                        PracticeLocationID ,
                        PracticeCatalogProductID ,
                        QuantityOnHandPerSystem ,
                        ParLevel ,
                        ReorderLevel ,
                        CriticalLevel ,
                        CreatedUserID ,
                        CreatedDate ,
                        ModifiedUserID ,
                        ModifiedDate ,
                        IsNotFlaggedForReorder ,
                        IsSetToActive ,
                        IsActive ,
                        ConsignmentQuantity ,
                        IsConsignment ,
                        ConsignmentLockParLevel ,
                        IsBuyout ,
                        IsRMA ,
                        BuyoutRMAInventoryCycleID ,
                        IsLogoAblePart ,
                        'Update (new)'
                FROM    inserted;
  
    END

GO

/*
	--  dbo.trg_ProductInventory_Deleted ON dbo.ProductInventory
	-- Trigger
	John Bongiorni  
	2008.01.30
	
	Trigger fires on Delete from ProductInventory
	and inserts the deleted record into
	ProductInventory_Audit table.
*/
CREATE TRIGGER [dbo].[trg_ProductInventory_Deleted] ON [dbo].[ProductInventory]
    FOR DELETE
AS
    BEGIN

        INSERT  INTO dbo.ProductInventory_Audit
                ( ProductInventoryID ,
                  PracticeLocationID ,
                  PracticeCatalogProductID ,
                  QuantityOnHandPerSystem ,
                  ParLevel ,
                  ReorderLevel ,
                  CriticalLevel ,
                  CreatedUserID ,
                  CreatedDate ,
                  ModifiedUserID ,
                  ModifiedDate ,
                  IsNotFlaggedForReorder ,
                  IsSetToActive ,
                  IsActive ,
                  ConsignmentQuantity ,
                  IsConsignment ,
                  ConsignmentLockParLevel ,
                  IsBuyout ,
                  IsRMA ,
                  BuyoutRMAInventoryCycleID ,
                  IsLogoAblePart ,
                  AuditAction			   
                )
                SELECT  ProductInventoryID ,
                        PracticeLocationID ,
                        PracticeCatalogProductID ,
                        QuantityOnHandPerSystem ,
                        ParLevel ,
                        ReorderLevel ,
                        CriticalLevel ,
                        CreatedUserID ,
                        CreatedDate ,
                        ModifiedUserID ,
                        ModifiedDate ,
                        IsNotFlaggedForReorder ,
                        IsSetToActive ,
                        IsActive ,
                        ConsignmentQuantity ,
                        IsConsignment ,
                        ConsignmentLockParLevel ,
                        IsBuyout ,
                        IsRMA ,
                        BuyoutRMAInventoryCycleID ,
                        IsLogoAblePart ,
                        'Deleted'
                FROM    deleted
  
    END

GO


CREATE TRIGGER [dbo].[UpdateProductInventoryChangeLog]
ON [dbo].[ProductInventory] FOR INSERT, UPDATE

AS 
	SET NOCOUNT ON
	DECLARE @PracticeLocationId as int
	DECLARE @ProductInventoryId as int
	
	Select
		@PracticeLocationId = INSERTED.PracticeLocationId,
		@ProductInventoryId = INSERTED.ProductInventoryId
	FROM inserted

	insert ProductChangeLog (PracticeLocationId, ProductInventoryId)
	values(@PracticeLocationId, @ProductInventoryId)

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the primary key for the Product Inventory table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory', @level2type = N'COLUMN', @level2name = N'ProductInventoryID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the quantity of a product on-hand, is indicated in the system.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory', @level2type = N'COLUMN', @level2name = N'QuantityOnHandPerSystem';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the quantity the clinic would prefer to have on hand is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory', @level2type = N'COLUMN', @level2name = N'ParLevel';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the quantity the clinic location would prefer to have on hand to prevent running out of the product, is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory', @level2type = N'COLUMN', @level2name = N'ReorderLevel';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the quantity the clinic location would prefer to not let the inventory drop to is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory', @level2type = N'COLUMN', @level2name = N'CriticalLevel';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in the Product Inventory is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory', @level2type = N'COLUMN', @level2name = N'IsActive';

