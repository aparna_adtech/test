﻿CREATE TABLE [dbo].[PracticeLocation_Supplier_AccountCode] (
    [PracticeLocationID]             INT          NOT NULL,
    [PracticeCatalogSupplierBrandID] INT          NOT NULL,
    [AccountCode]                    VARCHAR (50) NOT NULL,
    [CreatedUserID]                  INT          NOT NULL,
    [CreatedDate]                    DATETIME     NOT NULL,
    [ModifiedUserID]                 INT          NULL,
    [ModifiedDate]                   DATETIME     NULL,
    [IsActive]                       BIT          NOT NULL,
    CONSTRAINT [PracticeLocation_Supplier_AccountCode_PK] PRIMARY KEY CLUSTERED ([PracticeLocationID] ASC, [PracticeCatalogSupplierBrandID] ASC),
    CONSTRAINT [PracticeCatalogSupplierBrand_PracticeLocation_Supplier_AccountCode_FK1] FOREIGN KEY ([PracticeCatalogSupplierBrandID]) REFERENCES [dbo].[PracticeCatalogSupplierBrand] ([PracticeCatalogSupplierBrandID]),
    CONSTRAINT [PracticeLocation_PracticeLocation_Supplier_AccountCode_FK1] FOREIGN KEY ([PracticeLocationID]) REFERENCES [dbo].[PracticeLocation] ([PracticeLocationID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the practice location supplier account number would be entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeLocation_Supplier_AccountCode', @level2type = N'COLUMN', @level2name = N'AccountCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in the practice location supplier account code is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PracticeLocation_Supplier_AccountCode', @level2type = N'COLUMN', @level2name = N'IsActive';

