﻿CREATE TABLE [dbo].[CustomBraceIncompatibility] (
    [CustomBraceDependencyID]  INT          IDENTITY (1, 1) NOT NULL,
    [CustomBraceCode]          VARCHAR (50) NOT NULL,
    [CustomBraceAccessoryCode] VARCHAR (50) NOT NULL,
    [IsActive]                 BIT          NOT NULL,
    CONSTRAINT [PK_CustomBraceDependency] PRIMARY KEY CLUSTERED ([CustomBraceDependencyID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_CustomBraceDependency_Code]
    ON [dbo].[CustomBraceIncompatibility]([CustomBraceCode] ASC, [CustomBraceAccessoryCode] ASC);

