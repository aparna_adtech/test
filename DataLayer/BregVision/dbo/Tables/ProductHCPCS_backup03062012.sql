﻿CREATE TABLE [dbo].[ProductHCPCS_backup03062012] (
    [ProductHCPCSID]           INT          IDENTITY (1, 1) NOT NULL,
    [PracticeCatalogProductID] INT          NULL,
    [HCPCS]                    VARCHAR (20) NOT NULL,
    [CreatedUserID]            INT          NOT NULL,
    [CreatedDate]              DATETIME     NOT NULL,
    [ModifiedUserID]           INT          NULL,
    [ModifiedDate]             DATETIME     NULL,
    [IsActive]                 BIT          NOT NULL
);

