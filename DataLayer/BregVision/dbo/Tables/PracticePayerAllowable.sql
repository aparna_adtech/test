﻿CREATE TABLE [dbo].[PracticePayerAllowable] (
    [PracticePayerAllowableID] INT   IDENTITY (1, 1) NOT NULL,
    [PracticePayerID]          INT   NOT NULL,
    [HcpcsID]                  INT   NOT NULL,
    [AllowableAmount]          MONEY NULL,
    CONSTRAINT [PK_PracticePayerAllowable] PRIMARY KEY CLUSTERED ([PracticePayerAllowableID] ASC),
    CONSTRAINT [FK_PracticePayerAllowable_HCPCS] FOREIGN KEY ([HcpcsID]) REFERENCES [dbo].[HCPCS] ([HCPCSID]),
    CONSTRAINT [FK_PracticePayerAllowable_PracticePayer] FOREIGN KEY ([PracticePayerID]) REFERENCES [dbo].[PracticePayer] ([PracticePayerID])
);


GO
CREATE NONCLUSTERED INDEX [IX_Hcpcs]
    ON [dbo].[PracticePayerAllowable]([HcpcsID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PracticePayer]
    ON [dbo].[PracticePayerAllowable]([PracticePayerID] ASC);

