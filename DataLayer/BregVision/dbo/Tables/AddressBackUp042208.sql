﻿CREATE TABLE [dbo].[AddressBackUp042208] (
    [AddressID]      INT          NOT NULL,
    [AddressLine1]   VARCHAR (50) NOT NULL,
    [AddressLine2]   VARCHAR (50) NULL,
    [City]           VARCHAR (50) NOT NULL,
    [State]          CHAR (2)     NOT NULL,
    [ZipCode]        CHAR (5)     NOT NULL,
    [ZipCodePlus4]   CHAR (4)     NULL,
    [CreatedUserID]  INT          NOT NULL,
    [CreatedDate]    DATETIME     CONSTRAINT [DF__AddressBa__Creat__6F556E19] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID] INT          NULL,
    [ModifiedDate]   DATETIME     NULL,
    [IsActive]       BIT          NOT NULL
);

