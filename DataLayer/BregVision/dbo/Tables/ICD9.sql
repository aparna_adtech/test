﻿CREATE TABLE [dbo].[ICD9] (
    [ICD9ID]          INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [HCPCs]           VARCHAR (20)  NOT NULL,
    [ICD9Code]        VARCHAR (50)  NULL,
    [ICD9Description] VARCHAR (250) NULL,
    [PracticeID]      INT           NULL,
    [CreatedUserID]   INT           CONSTRAINT [DF__ICD9__CreatedUse__74F938D6] DEFAULT ((1)) NOT NULL,
    [CreatedDate]     DATETIME      CONSTRAINT [DF__ICD9__CreatedDat__75ED5D0F] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]  INT           NULL,
    [ModifiedDate]    DATETIME      NULL,
    [IsActive]        BIT           CONSTRAINT [DF__ICD9__IsActive__76E18148] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ICD9] PRIMARY KEY CLUSTERED ([ICD9ID] ASC)
);

