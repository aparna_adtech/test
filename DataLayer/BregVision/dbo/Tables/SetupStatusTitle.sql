﻿CREATE TABLE [dbo].[SetupStatusTitle] (
    [UserControlName]    VARCHAR (100) NOT NULL,
    [PageTitle]          VARCHAR (100) NULL,
    [DisplayOrder]       INT           CONSTRAINT [DF_SetupStatusTitle_DisplayOrder] DEFAULT ((0)) NOT NULL,
    [IsPracticeLocation] BIT           CONSTRAINT [DF_SetupStatusTitle_IsPracticeLocation] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_SetupStatusTitle_1] PRIMARY KEY CLUSTERED ([UserControlName] ASC)
);

