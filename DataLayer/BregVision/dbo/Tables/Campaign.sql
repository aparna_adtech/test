﻿CREATE TABLE [dbo].[Campaign] (
    [Id]                 INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]               NVARCHAR (100) NOT NULL,
    [ImpressionDuration] INT            NOT NULL,
    [AdMediaId]          INT            NOT NULL,
    [ContactEmail]       NVARCHAR (100) NOT NULL,
    [StartDateTime]      DATETIME       NOT NULL,
    [EndDateTime]        DATETIME       NOT NULL,
    [CreatedUserId]      INT            CONSTRAINT [DF_Campaign_UserId] DEFAULT ((0)) NOT NULL,
    [CreatedDateTime]    DATETIME       NOT NULL,
    [ModifiedDateTime]   DATETIME       CONSTRAINT [DF_Campaign_UpdatedDateTime] DEFAULT (getdate()) NULL,
    [ModifiedUserId]     INT            NULL,
    [IsActive]           BIT            NOT NULL,
    [timestamp]          ROWVERSION     NOT NULL,
    [Url]                NVARCHAR (100) NULL,
    CONSTRAINT [PK_Campaign] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Campaign_AdMedia] FOREIGN KEY ([AdMediaId]) REFERENCES [dbo].[AdMedia] ([Id]),
    CONSTRAINT [FK_Campaign_User] FOREIGN KEY ([CreatedUserId]) REFERENCES [dbo].[User] ([UserID]),
    CONSTRAINT [FK_Campaign_User1] FOREIGN KEY ([ModifiedUserId]) REFERENCES [dbo].[User] ([UserID])
);

