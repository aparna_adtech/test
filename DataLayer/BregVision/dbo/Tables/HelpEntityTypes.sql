﻿CREATE TABLE [dbo].[HelpEntityTypes] (
    [Id]   INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name] NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_HelpEntityTypes] PRIMARY KEY CLUSTERED ([Id] ASC)
);

