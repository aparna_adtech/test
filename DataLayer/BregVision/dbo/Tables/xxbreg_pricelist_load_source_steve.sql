﻿CREATE TABLE [dbo].[xxbreg_pricelist_load_source_steve] (
    [CUSTOMER]         NVARCHAR (255) NULL,
    [ACCT_NUM]         FLOAT (53)     NULL,
    [ITEM_NUM]         NVARCHAR (255) NULL,
    [ITEM_DESCRIPTION] NVARCHAR (255) NULL,
    [UOM]              NVARCHAR (255) NULL,
    [LIST_PRICE]       FLOAT (53)     NULL,
    [PRICE]            FLOAT (53)     NULL
);

