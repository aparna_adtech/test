﻿CREATE TABLE [dbo].[PracticeLocation_Physician] (
    [PracticeLocationID] INT      NOT NULL,
    [PhysicianID]        INT      NOT NULL,
    [CreatedUserID]      INT      NOT NULL,
    [CreatedDate]        DATETIME CONSTRAINT [DF_PracticeLocation_Physician_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]     INT      NULL,
    [ModifiedDate]       DATETIME NULL,
    [IsActive]           BIT      NOT NULL,
    CONSTRAINT [PracticeLocation_Physician_PK] PRIMARY KEY CLUSTERED ([PracticeLocationID] ASC, [PhysicianID] ASC),
    CONSTRAINT [Physician_PracticeLocation_Clinician_FK1] FOREIGN KEY ([PhysicianID]) REFERENCES [dbo].[Clinician] ([ClinicianID]),
    CONSTRAINT [PracticeLocation_PracticeLocation_Physician_FK1] FOREIGN KEY ([PracticeLocationID]) REFERENCES [dbo].[PracticeLocation] ([PracticeLocationID])
);

