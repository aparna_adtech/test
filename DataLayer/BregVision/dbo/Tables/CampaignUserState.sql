﻿CREATE TABLE [dbo].[CampaignUserState] (
    [CampaignId]       INT      NOT NULL,
    [UserId]           INT      NOT NULL,
    [AdLocationId]     INT      NOT NULL,
    [ModifiedDateTime] DATETIME NOT NULL,
    CONSTRAINT [PK_CampaignUserState_1] PRIMARY KEY CLUSTERED ([CampaignId] ASC, [UserId] ASC, [AdLocationId] ASC),
    CONSTRAINT [FK_CampaignUserState_AdLocation] FOREIGN KEY ([AdLocationId]) REFERENCES [dbo].[AdLocation] ([Id]),
    CONSTRAINT [FK_CampaignUserState_Campaign] FOREIGN KEY ([CampaignId]) REFERENCES [dbo].[Campaign] ([Id]),
    CONSTRAINT [FK_CampaignUserState_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserID])
);

