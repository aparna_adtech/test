﻿CREATE TABLE [dbo].[ProductInventory_ReturnedToSupplier] (
    [ProductInventory_ReturnedToSupplierID] INT        NOT NULL,
    [ProductInventoryID]                    INT        NOT NULL,
    [PracticeCatalogSupplierBrandID]        INT        NOT NULL,
    [ActualWholesaleCost]                   SMALLMONEY NOT NULL,
    [MoneyToBeReceivedForReturn]            SMALLMONEY NOT NULL,
    [Quantity]                              INT        NOT NULL,
    [DateReturned]                          DATETIME   NOT NULL,
    [Comments]                              CHAR (50)  NULL,
    [CreatedUserID]                         INT        NOT NULL,
    [CreatedDate]                           DATETIME   NOT NULL,
    [ModifiedUserID]                        INT        NULL,
    [ModifiedDate]                          DATETIME   NULL,
    [IsActive]                              BIT        NOT NULL,
    CONSTRAINT [ProductInventory_ReturnedToSupplier_PK] PRIMARY KEY CLUSTERED ([ProductInventory_ReturnedToSupplierID] ASC),
    CONSTRAINT [PracticeCatalogSupplierBrand_ProductInventory_ReturnedToSupplier_FK1] FOREIGN KEY ([PracticeCatalogSupplierBrandID]) REFERENCES [dbo].[PracticeCatalogSupplierBrand] ([PracticeCatalogSupplierBrandID]),
    CONSTRAINT [ProductInventory_ProductInventory_ReturnedToSupplier_FK1] FOREIGN KEY ([ProductInventoryID]) REFERENCES [dbo].[ProductInventory] ([ProductInventoryID])
);

