﻿CREATE TABLE [dbo].[SupplierWarranty] (
    [SupplierWarrantyID] INT            IDENTITY (1, 1) NOT NULL,
    [SupplierName]       NCHAR (50)     NOT NULL,
    [ProductWarranty]    VARCHAR (2000) NOT NULL,
    CONSTRAINT [PK_SupplierWarranty] PRIMARY KEY CLUSTERED ([SupplierWarrantyID] ASC)
);

