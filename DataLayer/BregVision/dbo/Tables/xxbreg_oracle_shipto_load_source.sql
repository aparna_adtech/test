﻿CREATE TABLE [dbo].[xxbreg_oracle_shipto_load_source] (
    [PracticeName]        NVARCHAR (255) NULL,
    [LocationName]        NVARCHAR (255) NULL,
    [AccountCode]         NVARCHAR (255) NULL,
    [AddressLine1]        NVARCHAR (255) NULL,
    [AddressLine2]        NVARCHAR (255) NULL,
    [City]                NVARCHAR (255) NULL,
    [State]               NVARCHAR (255) NULL,
    [ZipCode]             NVARCHAR (255) NULL,
    [BregOracleID]        NVARCHAR (255) NULL,
    [BregOracleIDInvalid] NVARCHAR (255) NULL,
    [F11]                 NVARCHAR (255) NULL
);

