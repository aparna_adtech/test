﻿CREATE TABLE [dbo].[HelpGroups] (
    [Id]               INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [HelpEntityTypeId] INT            CONSTRAINT [DF_HelpGroups_HelpEntityTypeId] DEFAULT ((1)) NOT NULL,
    [Title]            NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_HelpGroups] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_HelpGroups_HelpEntityTypes] FOREIGN KEY ([HelpEntityTypeId]) REFERENCES [dbo].[HelpEntityTypes] ([Id])
);

