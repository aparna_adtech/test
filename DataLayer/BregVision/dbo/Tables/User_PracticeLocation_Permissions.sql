﻿CREATE TABLE [dbo].[User_PracticeLocation_Permissions] (
    [UserID]             INT      NOT NULL,
    [PracticeLocationID] INT      NOT NULL,
    [AllowAccess]        BIT      CONSTRAINT [DF_User_PracticeLocation_Permissions_AllowAccess] DEFAULT ((1)) NOT NULL,
    [CreatedUserID]      INT      NOT NULL,
    [CreatedDate]        DATETIME NOT NULL,
    [ModifiedUserID]     INT      NULL,
    [ModifiedDate]       DATETIME NULL,
    [IsActive]           BIT      CONSTRAINT [DF_User_PracticeLocation_Permissions_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_User_PracticeLocation_Permissions] PRIMARY KEY CLUSTERED ([UserID] ASC, [PracticeLocationID] ASC)
);

