﻿CREATE TABLE [dbo].[PracticePayer] (
    [PracticePayerID] INT          IDENTITY (1, 1) NOT NULL,
    [PracticeID]      INT          NOT NULL,
    [Name]            VARCHAR (50) NOT NULL,
    [GroupNumber]     VARCHAR (50) NULL,
    [MemberNumber]    VARCHAR (50) NULL,
    [IsActive]        BIT          DEFAULT ((1)) NULL,
    CONSTRAINT [PK_PracticePayor] PRIMARY KEY CLUSTERED ([PracticePayerID] ASC),
    CONSTRAINT [FK_PracticePayer_Practice] FOREIGN KEY ([PracticeID]) REFERENCES [dbo].[Practice] ([PracticeID]),
    CONSTRAINT [IX_PracticePayer_PracticeID_Name] UNIQUE NONCLUSTERED ([PracticeID] ASC, [Name] ASC)
);

