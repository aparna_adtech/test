﻿CREATE TABLE [dbo].[MappedPDF] (
    [MappedPDFID]             INT           IDENTITY (1, 1) NOT NULL,
    [Name]                    VARCHAR (100) NOT NULL,
    [PDFURL]                  VARCHAR (200) NOT NULL,
    [JSURL]                   VARCHAR (200) NULL,
    [RenderDPI]               INT           NULL,
    [SubmitButtonText]        VARCHAR (50)  NULL,
    [SubmitButtonImageFile]   VARCHAR (200) NULL,
    [SubmitButtonPage]        INT           NULL,
    [SubmitButtonUpperLeftX]  FLOAT (53)    NULL,
    [SubmitButtonUpperLeftY]  FLOAT (53)    NULL,
    [SubmitButtonWidth]       FLOAT (53)    NULL,
    [SubmitButtonHeight]      FLOAT (53)    NULL,
    [IsNewOrdersVersion]      BIT           NOT NULL,
    [IsOriginalOrdersVersion] BIT           NOT NULL,
    CONSTRAINT [PK_MappedPDF] PRIMARY KEY CLUSTERED ([MappedPDFID] ASC)
);

