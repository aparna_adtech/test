﻿CREATE TABLE [dbo].[DispenseDetail_Audit] (
    [DispenseDetailID_Audit]   INT          IDENTITY (1, 1) NOT NULL,
    [DispenseDetailID]         INT          NOT NULL,
    [DispenseID]               INT          NOT NULL,
    [PracticeCatalogProductID] INT          NOT NULL,
    [PhysicianID]              INT          NULL,
    [ActualChargeBilled]       SMALLMONEY   NULL,
    [DMEDeposit]               SMALLMONEY   NULL,
    [Quantity]                 INT          NULL,
    [LineTotal]                SMALLMONEY   NULL,
    [IsCashCollection]         BIT          NULL,
    [CreatedUserID]            INT          NOT NULL,
    [CreatedDate]              DATETIME     NOT NULL,
    [ModifiedUserID]           INT          NULL,
    [ModifiedDate]             DATETIME     NULL,
    [IsActive]                 BIT          NOT NULL,
    [ICD9Code]                 VARCHAR (50) NULL,
    [DispenseQueueID]          INT          NULL,
    [LRModifier]               VARCHAR (2)  NULL,
    [IsMedicare]               BIT          NOT NULL,
    [ABNForm]                  BIT          NOT NULL,
    [IsCustomFit]              BIT          NULL,
    [FitterID]                 INT          NULL,
    [AuditAction]              VARCHAR (50) NOT NULL,
    [AuditCreatedDate]         DATETIME     CONSTRAINT [DF_DispenseDetail_Audit_AuditCreatedDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_DispenseDetail_Audit] PRIMARY KEY CLUSTERED ([DispenseDetailID_Audit] ASC)
);

