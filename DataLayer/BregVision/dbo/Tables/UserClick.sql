﻿CREATE TABLE [dbo].[UserClick] (
    [UserClickID]        INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [UserID]             INT            NOT NULL,
    [UserName]           NVARCHAR (100) NOT NULL,
    [PracticeID]         INT            NOT NULL,
    [PracticeLocationID] INT            NULL,
    [PageName]           NVARCHAR (50)  NOT NULL,
    [Area]               NVARCHAR (50)  NOT NULL,
    [Item]               NVARCHAR (50)  NOT NULL,
    [Action]             NVARCHAR (50)  NOT NULL,
    [Promotion]          NVARCHAR (50)  NULL,
    [UserDefined1]       NVARCHAR (50)  NULL,
    [UserDefined2]       NVARCHAR (50)  NULL,
    [UserDefined3]       NVARCHAR (50)  NULL,
    [UserDefined4]       NVARCHAR (50)  NULL,
    [CreatedDate]        DATETIME       CONSTRAINT [DF_UserClick_TimeStamp] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_UserClick] PRIMARY KEY CLUSTERED ([UserClickID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The area on the page that was clicked', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'UserClick', @level2type = N'COLUMN', @level2name = N'Area';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The item on the page that was clicked', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'UserClick', @level2type = N'COLUMN', @level2name = N'Item';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The specific action that caused the logging', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'UserClick', @level2type = N'COLUMN', @level2name = N'Action';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A Breg promotion or special to which the clicked item belongs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'UserClick', @level2type = N'COLUMN', @level2name = N'Promotion';

