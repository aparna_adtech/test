﻿CREATE TABLE [dbo].[Lookup_Salutation] (
    [Lookup_SalutationID] INT          IDENTITY (1, 1) NOT NULL,
    [Salutation]          VARCHAR (10) NOT NULL
);

