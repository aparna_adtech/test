﻿CREATE TABLE [dbo].[CustomFitProcedure] (
    [CustomFitProcedureID] INT           IDENTITY (1, 1) NOT NULL,
    [Description]          VARCHAR (250) NOT NULL,
    [SortPosition]         INT           NOT NULL,
    [IsActive]             BIT           CONSTRAINT [DF_CustomFitProcedure_IsActive] DEFAULT ((1)) NOT NULL,
    [IsUserEntered]        BIT           CONSTRAINT [DF_CustomFitProcedure_IsUserEntered] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_CustomFitProcedure] PRIMARY KEY CLUSTERED ([CustomFitProcedureID] ASC)
);

