﻿CREATE TABLE [dbo].[xxbreg_pricelist_customers_master] (
    [name]       NVARCHAR (MAX) NULL,
    [acct_num]   FLOAT (53)     NULL,
    [practiceId] INT            NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_xxbreg_pricelist_customers_master]
    ON [dbo].[xxbreg_pricelist_customers_master]([acct_num] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_xxbreg_pricelist_customers_master_1]
    ON [dbo].[xxbreg_pricelist_customers_master]([practiceId] ASC);

