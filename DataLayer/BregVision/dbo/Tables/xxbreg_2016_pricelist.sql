﻿CREATE TABLE [dbo].[xxbreg_2016_pricelist] (
    [Code]           NVARCHAR (255) NULL,
    [Desc]           NVARCHAR (255) NULL,
    [Unit]           NVARCHAR (255) NULL,
    [WholesalePrice] MONEY          NULL
);

