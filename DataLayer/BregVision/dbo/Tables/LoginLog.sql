﻿CREATE TABLE [dbo].[LoginLog] (
    [LoginLog_ID]       BIGINT         IDENTITY (1, 1) NOT NULL,
    [Username]          NVARCHAR (MAX) NOT NULL,
    [LoginDateTime]     DATETIME       CONSTRAINT [DF_LoginLog_LoginDateTime] DEFAULT (getdate()) NOT NULL,
    [ClientIPAddress]   NVARCHAR (250) NULL,
    [RecordCreatedDate] DATETIME       CONSTRAINT [DF_LoginLog_RecordCreatedDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_LoginLog] PRIMARY KEY CLUSTERED ([LoginLog_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_LoginLog_LoginDateTime]
    ON [dbo].[LoginLog]([LoginDateTime] ASC);

