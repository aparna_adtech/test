﻿CREATE TABLE [dbo].[Lookup_State] (
    [Lookup_StateID] INT          IDENTITY (1, 1) NOT NULL,
    [StateAbbr]      CHAR (2)     NOT NULL,
    [Name]           VARCHAR (50) NOT NULL
);

