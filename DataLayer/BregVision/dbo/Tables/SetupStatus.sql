﻿CREATE TABLE [dbo].[SetupStatus] (
    [PracticeID]         INT           NOT NULL,
    [PracticeLocationID] INT           NOT NULL,
    [UserControlName]    VARCHAR (100) NOT NULL,
    [PageStatus]         INT           CONSTRAINT [DF_Table_1_SetupStatus] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_SetupStatus_1] PRIMARY KEY CLUSTERED ([PracticeID] ASC, [PracticeLocationID] ASC, [UserControlName] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'0 Incomplete, 1 Complete', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SetupStatus', @level2type = N'COLUMN', @level2name = N'PageStatus';

