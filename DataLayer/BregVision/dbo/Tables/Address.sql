﻿CREATE TABLE [dbo].[Address] (
    [AddressID]      INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [AddressLine1]   VARCHAR (50) NOT NULL,
    [AddressLine2]   VARCHAR (50) NULL,
    [City]           VARCHAR (50) NOT NULL,
    [State]          CHAR (2)     NOT NULL,
    [ZipCode]        CHAR (5)     NOT NULL,
    [ZipCodePlus4]   CHAR (4)     NULL,
    [CreatedUserID]  INT          NOT NULL,
    [CreatedDate]    DATETIME     CONSTRAINT [DF__Address__CreateD__46E78A0C] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID] INT          NULL,
    [ModifiedDate]   DATETIME     NULL,
    [IsActive]       BIT          NOT NULL,
    CONSTRAINT [Address_PK] PRIMARY KEY CLUSTERED ([AddressID] ASC)
);


GO
CREATE STATISTICS [_dta_stat_2073058421_12_1]
    ON [dbo].[Address]([IsActive], [AddressID]);


GO
CREATE STATISTICS [_dta_stat_2073058421_2_4_6]
    ON [dbo].[Address]([AddressLine1], [City], [ZipCode]);


GO
CREATE STATISTICS [_dta_stat_2073058421_1_2_4_6]
    ON [dbo].[Address]([AddressID], [AddressLine1], [City], [ZipCode]);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary Key for the Address Table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Address', @level2type = N'COLUMN', @level2name = N'AddressID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the physical address is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Address', @level2type = N'COLUMN', @level2name = N'AddressLine1';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field 2 in which the physical address is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Address', @level2type = N'COLUMN', @level2name = N'AddressLine2';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the city of the  physical address exists, is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Address', @level2type = N'COLUMN', @level2name = N'City';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the numbered state (1-50) of the physical address exists, is entered into.  This will be displayed as a two-lettered abbreviation of the state name.  This also connects to the state ID table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Address', @level2type = N'COLUMN', @level2name = N'State';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the five-number zip code of the physical address, is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Address', @level2type = N'COLUMN', @level2name = N'ZipCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the four-number zip code extension of the physical address is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Address', @level2type = N'COLUMN', @level2name = N'ZipCodePlus4';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in addresses is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Address', @level2type = N'COLUMN', @level2name = N'IsActive';

