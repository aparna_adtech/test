﻿CREATE TABLE [dbo].[Practice_ICD9] (
    [ICD9ID]         INT      NOT NULL,
    [PracticeID]     INT      NOT NULL,
    [CreatedUserID]  INT      DEFAULT ((1)) NOT NULL,
    [CreatedDate]    DATETIME DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID] INT      NULL,
    [ModifiedDate]   DATETIME NULL,
    [IsActive]       BIT      DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Practice_ICD9] PRIMARY KEY CLUSTERED ([ICD9ID] ASC, [PracticeID] ASC),
    CONSTRAINT [FK_Practice_ICD9_ICD9] FOREIGN KEY ([ICD9ID]) REFERENCES [dbo].[ICD9] ([ICD9ID]),
    CONSTRAINT [FK_Practice_ICD9_Practice] FOREIGN KEY ([PracticeID]) REFERENCES [dbo].[Practice] ([PracticeID])
);

