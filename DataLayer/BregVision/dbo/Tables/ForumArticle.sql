﻿CREATE TABLE [dbo].[ForumArticle] (
    [Id]                     INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ParentId]               INT            NULL,
    [Subject]                NVARCHAR (100) NULL,
    [Body]                   NVARCHAR (MAX) NULL,
    [ForumGroupId]           INT            NOT NULL,
    [MasterCatalogProductID] INT            NULL,
    [UserId]                 INT            NOT NULL,
    [Created]                DATETIME       CONSTRAINT [DF_ForumArticle_Created] DEFAULT (getdate()) NOT NULL,
    [IsActive]               BIT            CONSTRAINT [DF_ForumArticle_IsActive] DEFAULT ((1)) NOT NULL,
    [IsSticky]               BIT            CONSTRAINT [DF_ForumArticle_IsSticky] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ForumArticle] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ForumArticle_ForumArticle] FOREIGN KEY ([ParentId]) REFERENCES [dbo].[ForumArticle] ([Id]),
    CONSTRAINT [FK_ForumArticle_ForumGroup] FOREIGN KEY ([ForumGroupId]) REFERENCES [dbo].[ForumGroup] ([Id]),
    CONSTRAINT [FK_ForumArticle_MasterCatalogProduct] FOREIGN KEY ([MasterCatalogProductID]) REFERENCES [dbo].[MasterCatalogProduct] ([MasterCatalogProductID]),
    CONSTRAINT [FK_ForumArticle_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserID])
);

