﻿CREATE TABLE [dbo].[DispenseBatch] (
    [DispenseBatchID]    INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PracticeLocationID] INT      NOT NULL,
    [DispenseDate]       DATETIME NOT NULL,
    [CreatedUser]        INT      NOT NULL,
    [CreatedDate]        DATETIME NOT NULL,
    [ModifiedUser]       INT      NOT NULL,
    [ModifiedDate]       DATETIME NOT NULL,
    [IsActive]           BIT      CONSTRAINT [DF_DispenseBatch_IsActive] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_DispenseBatch] PRIMARY KEY CLUSTERED ([DispenseBatchID] ASC),
    CONSTRAINT [FK_DispenseBatch_PracticeLocation] FOREIGN KEY ([PracticeLocationID]) REFERENCES [dbo].[PracticeLocation] ([PracticeLocationID])
);

