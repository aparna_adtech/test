﻿CREATE TABLE [dbo].[SupplierOrderLineItem_Audit] (
    [SupplierOrderLineItemAuditID]  INT          IDENTITY (1, 1) NOT NULL,
    [SupplierOrderLineItemID]       INT          NULL,
    [SupplierOrderID]               INT          NULL,
    [PracticeCatalogProductID]      INT          NULL,
    [SupplierOrderLineItemStatusID] TINYINT      NULL,
    [ActualWholesaleCost]           SMALLMONEY   NULL,
    [QuantityOrdered]               INT          NULL,
    [LineTotal]                     SMALLMONEY   NULL,
    [CreatedUserID]                 INT          NULL,
    [CreatedDate]                   DATETIME     NULL,
    [ModifiedUserID]                INT          NULL,
    [ModifiedDate]                  DATETIME     NULL,
    [IsActive]                      BIT          NULL,
    [TriggerName]                   VARCHAR (50) NULL,
    [RecordCreateDate]              DATETIME     CONSTRAINT [DF_SupplierOrderLineItem_Audit_RecordCreateDate] DEFAULT (getdate()) NULL
);

