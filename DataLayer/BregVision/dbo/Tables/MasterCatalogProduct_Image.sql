﻿CREATE TABLE [dbo].[MasterCatalogProduct_Image] (
    [MasterCatalogProductTypeImageID] INT NOT NULL,
    [MasterCatalogProductID]          INT NOT NULL,
    CONSTRAINT [MasterCatalogProduct_Image_PK] PRIMARY KEY CLUSTERED ([MasterCatalogProductTypeImageID] ASC, [MasterCatalogProductID] ASC),
    CONSTRAINT [MasterCatalogProduct_MasterCatalogProduct_Image_FK1] FOREIGN KEY ([MasterCatalogProductID]) REFERENCES [dbo].[MasterCatalogProduct] ([MasterCatalogProductID]),
    CONSTRAINT [MasterCatalogProductTypeImage_MasterCatalogProduct_Image_FK1] FOREIGN KEY ([MasterCatalogProductTypeImageID]) REFERENCES [dbo].[MasterCatalogProductTypeImage] ([MasterCatalogProductTypeImageID])
);

