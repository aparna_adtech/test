﻿CREATE TABLE [dbo].[SupplierOrderStatus] (
    [SupplierOrderStatusID] TINYINT      NOT NULL,
    [Status]                VARCHAR (50) NOT NULL,
    CONSTRAINT [SupplierOrderStatus_PK] PRIMARY KEY CLUSTERED ([SupplierOrderStatusID] ASC)
);

