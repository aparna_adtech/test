﻿CREATE TABLE [dbo].[AdMedia] (
    [Id]               INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]             NVARCHAR (100) NOT NULL,
    [AdMediaTypeId]    INT            NOT NULL,
    [Url]              NVARCHAR (200) NOT NULL,
    [AdLocationId]     INT            NOT NULL,
    [CreatedUserId]    INT            CONSTRAINT [DF_AdMedia_UserId] DEFAULT ((0)) NOT NULL,
    [CreatedDateTime]  DATETIME       CONSTRAINT [DF_AdMedia_CreatedDateTime] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserId]   INT            CONSTRAINT [DF_AdMedia_ModifiedUserId] DEFAULT ((0)) NULL,
    [ModifiedDateTime] DATETIME       CONSTRAINT [DF_AdMedia_ModifiedDateTime] DEFAULT (getdate()) NULL,
    [IsActive]         BIT            CONSTRAINT [DF_AdMedia_IsActive] DEFAULT ((1)) NOT NULL,
    [timestamp]        ROWVERSION     NOT NULL,
    CONSTRAINT [PK_AdImage1] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_AdMedia_AdLocation] FOREIGN KEY ([AdLocationId]) REFERENCES [dbo].[AdLocation] ([Id]),
    CONSTRAINT [FK_AdMedia_AdMediaType] FOREIGN KEY ([AdMediaTypeId]) REFERENCES [dbo].[AdMediaType] ([Id]),
    CONSTRAINT [FK_AdMedia_User] FOREIGN KEY ([CreatedUserId]) REFERENCES [dbo].[User] ([UserID]),
    CONSTRAINT [FK_AdMedia_User1] FOREIGN KEY ([ModifiedUserId]) REFERENCES [dbo].[User] ([UserID])
);

