﻿CREATE TABLE [dbo].[ProductInventory_DispenseDetail] (
    [ProductInventory_DispenseDetail] INT        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ProductInventoryID]              INT        NOT NULL,
    [DispenseDetailID]                INT        NOT NULL,
    [ActualWholesaleCost]             SMALLMONEY NOT NULL,
    [CreatedUserID]                   INT        NOT NULL,
    [CreatedDate]                     DATETIME   NOT NULL,
    [ModifiedUserID]                  INT        NULL,
    [ModifiedDate]                    DATETIME   NULL,
    [IsActive]                        BIT        NOT NULL,
    CONSTRAINT [ProductInventory_DispenseDetail_PK] PRIMARY KEY CLUSTERED ([ProductInventory_DispenseDetail] ASC),
    CONSTRAINT [ProductInventory_ProductInventory_DispenseDetail_FK1] FOREIGN KEY ([ProductInventoryID]) REFERENCES [dbo].[ProductInventory] ([ProductInventoryID])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_378]
    ON [dbo].[ProductInventory_DispenseDetail]([ProductInventoryID] ASC, [CreatedDate] ASC)
    INCLUDE([ActualWholesaleCost]);


GO
CREATE NONCLUSTERED INDEX [missing_index_239]
    ON [dbo].[ProductInventory_DispenseDetail]([CreatedDate] ASC)
    INCLUDE([ProductInventoryID], [ActualWholesaleCost]);


GO
CREATE NONCLUSTERED INDEX [missing_index_813]
    ON [dbo].[ProductInventory_DispenseDetail]([DispenseDetailID] ASC, [IsActive] ASC)
    INCLUDE([ActualWholesaleCost]);


GO
CREATE STATISTICS [_dta_stat_1221579390_9_2]
    ON [dbo].[ProductInventory_DispenseDetail]([IsActive], [ProductInventoryID]);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the Product Inventory Dispense Detail table.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_DispenseDetail', @level2type = N'COLUMN', @level2name = N'ProductInventory_DispenseDetail';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which the price per unit the practice paid for that particular product is entered into.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_DispenseDetail', @level2type = N'COLUMN', @level2name = N'ActualWholesaleCost';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in the Product Inventory Dispense Detail  is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductInventory_DispenseDetail', @level2type = N'COLUMN', @level2name = N'IsActive';

