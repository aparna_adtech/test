﻿CREATE TABLE [dbo].[ShoppingCart] (
    [ShoppingCartID]          INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PracticeLocationID]      INT           NOT NULL,
    [ShoppingCartStatusID]    TINYINT       NULL,
    [ShippingTypeID]          TINYINT       CONSTRAINT [DF_ShoppingCart_ShippingTypeID] DEFAULT ((4)) NULL,
    [DeniedComments]          VARCHAR (200) NOT NULL,
    [CreatedUserID]           INT           NOT NULL,
    [CreatedDate]             DATETIME      CONSTRAINT [DF__ShoppingC__Creat__4CA06362] DEFAULT (getdate()) NOT NULL,
    [ModifiedUserID]          INT           NULL,
    [ModifiedDate]            DATETIME      NULL,
    [IsActive]                BIT           NOT NULL,
    [CustomPurchaseOrderCode] VARCHAR (75)  NULL,
    [IsConsignment]           BIT           DEFAULT ((0)) NOT NULL,
    CONSTRAINT [ShoppingCart_PK] PRIMARY KEY CLUSTERED ([ShoppingCartID] ASC),
    CONSTRAINT [PracticeLocation_ShoppingCart_FK1] FOREIGN KEY ([PracticeLocationID]) REFERENCES [dbo].[PracticeLocation] ([PracticeLocationID]),
    CONSTRAINT [ShippingType_ShoppingCart_FK1] FOREIGN KEY ([ShippingTypeID]) REFERENCES [dbo].[ShippingType] ([ShippingTypeID]),
    CONSTRAINT [ShoppingCartStatus_ShoppingCart_FK1] FOREIGN KEY ([ShoppingCartStatusID]) REFERENCES [dbo].[ShoppingCartStatus] ([ShoppingCartStatusID])
);


GO
/*
	-- dbo.trg_ShoppingCart_Deleted ON dbo.ShoppingCart
	-- Trigger
	John Bongiorni  
	2008.01.30
	
	Trigger fires on Delete from ShoppingCart
	and inserts the deleted record into
	ShoppingCart_Audit table.
*/
CREATE trigger dbo.trg_ShoppingCart_Deleted ON dbo.ShoppingCart
    FOR DELETE
AS
    begin

        INSERT INTO
            dbo.ShoppingCart_Audit
            (
              ShoppingCartID
            , PracticeLocationID
            , ShoppingCartStatusID
            , ShippingTypeID
            , DeniedComments
            , CreatedUserID
            , CreatedDate
            , ModifiedUserID
            , ModifiedDate
            , IsActive
            , AuditAction
			   
            )
            SELECT
                ShoppingCartID
              , PracticeLocationID
              , ShoppingCartStatusID
              , ShippingTypeID
              , DeniedComments
              , CreatedUserID
              , CreatedDate
              , ModifiedUserID
              , ModifiedDate
              , IsActive
              , 'Deleted' AS AuditAction
            FROM
                deleted
    END
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key for the shopping cart table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ShoppingCart', @level2type = N'COLUMN', @level2name = N'ShoppingCartID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Field in which comments can be entered as to why the order was denied.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ShoppingCart', @level2type = N'COLUMN', @level2name = N'DeniedComments';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag that indicates if a record in the shopping cart is still active.  For example, if someone deletes the record, it will be flagged as inactive.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ShoppingCart', @level2type = N'COLUMN', @level2name = N'IsActive';

