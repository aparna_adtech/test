﻿-- =============================================
-- Author:		Mike Sneen
-- Create date: 4/8/2012
-- Description:	Determines if a part is logo-able by examining the master catalog (mcp.IsLogoAblePart)
--and Practice (practice.LogoPartNumber not null or empty)
/*
	select dbo.IsPiItemLogoAblePart(142501)
	select dbo.IsPiItemLogoAblePart(142502)
	select dbo.IsPiItemLogoAblePart(194500)
*/
-- =============================================
CREATE FUNCTION [dbo].[IsPIItemLogoAblePart]  
(
	@ProductInventoryID int
)
RETURNS bit
AS
BEGIN
	DECLARE @IsPcpItemLogoable bit
	
	Select @IsPcpItemLogoable = dbo.IsPCPItemLogoAblePart(PCP.PracticeCatalogProductID) & PCP.IsLogoAblePart 
	From
		ProductInventory PI
		Inner Join PracticeCatalogProduct PCP
			on PI.practiceCatalogProductID = PCP.PracticeCatalogProductID
		Where PI.ProductInventoryID = @ProductInventoryID

	-- Return the result of the function
	RETURN @IsPcpItemLogoable

END
