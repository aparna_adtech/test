﻿
/* ------------------------------------------------------------
   FUNCTION:    [dbo].[udf_QuantityCheckedIn_Given_SupplierLineItemOrderID]
   
   Description:  Gets SupplierLineItemOrderID, QuantityCheckedIn, IsEntireLineItemCheckedIn
					 Given the SupplierLineItemOrderID
		
   AUTHOR:       John Bongiorni 11/15/2007 3:23:17 PM

   Test Script:

	DECLARE @SupplierOrderLineItemID INT
	SET @SupplierOrderLineItemID = 378
	SELECT * FROM dbo.[udf_QuantityCheckedIn_Given_SupplierLineItemOrderID]( @SupplierOrderLineItemID) 
	SELECT 
		  SOLI.SupplierOrderLineItemID
		, udfQCI.SupplierOrderLineItemID
		, udfQCI.QuantityCheckedIn  
		, udfQCI.IsEntireLineItemCheckedIn 
	FROM
		dbo.udf_QuantityCheckedIn_Given_SupplierLineItemOrderID (@SupplierOrderLineItemID) AS udfQCI
	INNER JOIN SupplierOrderLineItem AS SOLI
		ON udfQCI.SupplierOrderLineItemID = SOLI.SupplierOrderLineItemID
	WHERE SOLI.SupplierOrderLineItemID = @SupplierOrderLineItemID  -- Not really neccessary.
	
	
	SELECT 
		  SOLI.SupplierOrderLineItemID
		, udfQCI.SupplierOrderLineItemID
		, udfQCI.QuantityCheckedIn  
		, udfQCI.IsEntireLineItemCheckedIn 
	FROM
		SupplierOrderLineItem AS SOLI		
	INNER JOIN 
		dbo.udf_QuantityCheckedIn_Given_SupplierLineItemOrderID (@SupplierOrderLineItemID) AS udfQCI
			ON  SOLI.SupplierOrderLineItemID = udfQCI.SupplierOrderLineItemID 
	WHERE SOLI.SupplierOrderLineItemID = @SupplierOrderLineItemID  -- Not really neccessary.
	
	-- End Test Script
   
   Modifications:  
   ------------------------------------------------------------ */ 

CREATE FUNCTION [dbo].[udf_QuantityCheckedIn_Given_SupplierLineItemOrderID]
(
	  @SupplierOrderLineItemID		INT	
)  
RETURNS @ReturnTable TABLE 
	( 
		  SupplierOrderLineItemID INT
		, QuantityCheckedIn INT DEFAULT 0
		, IsEntireLineItemCheckedIn INT DEFAULT 0  
	) 
AS
BEGIN	

	-- 1. Get SupplierLineItemOrderID, Total QuantityCheckedIn, IsEntireLineItemCheckedIn
	--					 Given the SupplierLineItemOrderID
	INSERT INTO @ReturnTable
		( 
				SupplierOrderLineItemID
			  , QuantityCheckedIn  
			  , IsEntireLineItemCheckedIn 
		)
	SELECT
		  MAX(SupplierOrderLineItemID) AS SupplierOrderLineItemID
		, SUM(Quantity)AS QuantityCheckedIn
		, CAST( MAX(CAST(IsEntireLineItemCheckedIn AS INT)) AS BIT) AS IsEntireLineItemCheckedIn
	FROM ProductInventory_OrderCheckIn WITH (NOLOCK)
	WHERE SupplierOrderLineItemID = @SupplierOrderLineItemID
		AND dbo.ProductInventory_OrderCheckIn.IsActive = 1


	RETURN	--This will return the table variable @ReturnTable
	
END            

