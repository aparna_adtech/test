﻿
/* ------------------------------------------------------------
   FUNCTION:    [dbo].[udf_QuantityCheckedIn_Given_PracticeLocationID]
   
   Description:  Gets PracticeLocationID, SupplierLineItemOrderID, QuantityCheckedIn, IsEntireLineItemCheckedIn
					for each SupplierOrderLineItemID	 
					given the PracticeLocationID
		
   AUTHOR:       John Bongiorni 11/15/2007 4:15:17 PM

   Test Script:

	DECLARE @PracticeLocationID INT
	SET @PracticeLocationID = 15
	
	SELECT * FROM dbo.[udf_QuantityCheckedIn_Given_PracticeLocationID]( @PracticeLocationID) 
	
	SELECT 
		  udfQCI.PracticeLocationID
		,  SOLI.SupplierOrderLineItemID
		, udfQCI.SupplierOrderLineItemID
		, udfQCI.QuantityCheckedIn  
		, udfQCI.IsEntireLineItemCheckedIn 
	FROM
		SupplierOrderLineItem AS SOLI		
	INNER JOIN 
		dbo.[udf_QuantityCheckedIn_Given_PracticeLocationID] (@PracticeLocationID) AS udfQCI
			ON  SOLI.SupplierOrderLineItemID = udfQCI.SupplierOrderLineItemID 
	WHERE udfQCI.PracticeLocationID = @PracticeLocationID  
	
	
	-- End Test Script
   
   Modifications:  
   ------------------------------------------------------------ */ 

CREATE FUNCTION [dbo].[udf_QuantityCheckedIn_Given_PracticeLocationID]
(
	  @PracticeLocationID		INT	
)  
RETURNS @ReturnTable TABLE 
	( 
		  PracticeLocationID		INT
		, SupplierOrderLineItemID	INT
		, QuantityCheckedIn			INT DEFAULT 0
		, IsEntireLineItemCheckedIn INT DEFAULT 0  
	) 
AS
BEGIN	

	-- 1. Get SupplierLineItemOrderID, Total QuantityCheckedIn, IsEntireLineItemCheckedIn
	--					 Given the SupplierLineItemOrderID
	INSERT INTO @ReturnTable
		( 
			    PracticeLocationID		
			  , SupplierOrderLineItemID
			  , QuantityCheckedIn  
			  , IsEntireLineItemCheckedIn 
		)
	SELECT
		  PL.PracticeLocationID			 AS PracticeLocationID
		, PI_OCI.SupplierOrderLineItemID AS SupplierOrderLineItemID
		, SUM(PI_OCI.Quantity)			 AS QuantityCheckedIn
		, CAST( MAX(CAST(PI_OCI.IsEntireLineItemCheckedIn AS INT)) AS BIT) AS IsEntireLineItemCheckedIn
	FROM ProductInventory_OrderCheckIn AS PI_OCI WITH (NOLOCK)
	INNER JOIN dbo.ProductInventory AS PI  WITH (NOLOCK)
		ON PI_OCI.ProductInventoryID = PI.ProductInventoryID
	INNER JOIN dbo.PracticeLocation AS PL  WITH (NOLOCK)
		ON PI.PracticeLocationID = PL.PracticeLocationID
	WHERE 
		PL.PracticeLocationID = @PracticeLocationID
		AND PI_OCI.IsActive = 1
		AND PL.IsActive = 1
		AND PI.IsActive = 1
	GROUP BY 
		  PL.PracticeLocationID
		, PI_OCI.SupplierOrderLineItemID


	RETURN	--This will return the table variable @ReturnTable
	
END            

