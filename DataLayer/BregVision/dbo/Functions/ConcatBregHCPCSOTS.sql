﻿

--  SELECT * FROM dbo.ProductHCPCS AS ph
--  SELECT [dbo].[ConcatHCPCS](1072) 

CREATE FUNCTION [dbo].[ConcatBregHCPCSOTS](@MasterCatalogProductID INT)
RETURNS VARCHAR(200)
AS
BEGIN
DECLARE @Output VARCHAR(200)
	SET @Output = ''

	SELECT @Output =	CASE @Output 
				WHEN '' THEN HCPCS 
				ELSE @Output + ', ' + HCPCS 
				END
	FROM BregProductHCPCS AS BPHCPCS WITH (NOLOCK)
	WHERE 
		BPHCPCS.MasterCatalogProductID = @MasterCatalogProductID
		AND BPHCPCS.IsOffTheShelf = 1
		AND BPHCPCS.IsActive = 1
	
	--ORDER BY ProductHCPCSID

	RETURN @Output
	--SELECT @Output AS output

END
