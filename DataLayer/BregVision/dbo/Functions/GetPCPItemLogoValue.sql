﻿-- =============================================
-- Author:		Mike Sneen
-- Create date: 4/8/12
-- Description:	Determines if a part is a logo part by pcp.IsLogoAblePart cross verifying by examining the master catalog (mcp.IsLogoAblePart)
--and Practice (practice.LogoPartNumber not null or empty)
-- =============================================
CREATE FUNCTION GetPCPItemLogoValue 
(
	@PracticeCatalogProductID int
)
RETURNS bit
AS
BEGIN
	Declare @PcpItemLogoValue bit
	
	Select @PcpItemLogoValue = case when pcp.IsLogoAblePart = 1  AND dbo.IsPCPItemLogoAblePart(@PracticeCatalogProductID) = 1 THEN		
		1		
		else 
		0
		end
		 
	from 
		PracticeCatalogProduct pcp
	where pcp.PracticeCatalogProductID = @PracticeCatalogProductID
	
	return @PcpItemLogoValue

END
