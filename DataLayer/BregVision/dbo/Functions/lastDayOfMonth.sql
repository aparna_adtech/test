﻿CREATE function lastDayOfMonth(@d datetime ) 
returns datetime
as
BEGIN
return dateadd(ms,-3,DATEADD(mm, DATEDIFF(m,0,@d)+1, 0))
END
