﻿
--  SELECT * FROM dbo.ProductHCPCS AS ph
--  SELECT [dbo].[ConcatHCPCS](1072) 

CREATE FUNCTION [dbo].[ConcatHCPCSOTS](@PracticeCatalogProductID INT)
RETURNS VARCHAR(200)
AS
BEGIN
DECLARE @Output VARCHAR(200)
	SET @Output = ''

	SELECT @Output =	CASE @Output 
				WHEN '' THEN HCPCS 
				ELSE @Output + ', ' + HCPCS 
				END
	FROM ProductHCPCS AS PHCPCS WITH (NOLOCK)
	WHERE 
		PHCPCS.PracticeCatalogProductID = @PracticeCatalogProductID
		AND PHCPCS.IsActive = 1
		AND PHCPCS.IsOffTheShelf = 1
	
	--ORDER BY ProductHCPCSID

	RETURN @Output
	--SELECT @Output AS output

END
