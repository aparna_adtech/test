﻿CREATE function [dbo].[firstDayOfMonth](@d datetime ) 
returns datetime
as
BEGIN
return dateadd(ms,+1,DATEADD(mm, DATEDIFF(m,0,@d)-0, 0))
END
