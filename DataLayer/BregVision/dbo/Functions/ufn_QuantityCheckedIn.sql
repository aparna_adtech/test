﻿-- =============================================
-- Author:		John Bongiorni
-- Create date: 2007.11.15
-- Description:	Get Quantity Checked In given the SupplierOrderLineItemID
-- =============================================
CReate FUNCTION [dbo].[ufn_QuantityCheckedIn] (@SupplierOrderLineItemID INT)
RETURNS INT
AS
BEGIN

	DECLARE @QuantityCheckedIn INT

	SELECT 
		@QuantityCheckedIn = SUM(Quantity)
	FROM ProductInventory_OrderCheckIn WITH (NOLOCK)
	WHERE SupplierOrderLineItemID = @SupplierOrderLineItemID

	RETURN @QuantityCheckedIn

END
