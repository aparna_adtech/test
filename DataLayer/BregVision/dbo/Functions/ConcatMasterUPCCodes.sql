﻿Create FUNCTION [dbo].[ConcatMasterUPCCodes]
(@MasterCatalogProductID INT)
RETURNS VARCHAR(200)
AS
BEGIN
DECLARE @Output VARCHAR(200)
	SET @Output = ''

	SELECT @Output =	CASE @Output 
				WHEN '' THEN Code 
				ELSE @Output + ', ' + Code 
				END
	FROM UPCCode AS UPC WITH (NOLOCK)
	WHERE 
		UPC.MasterCatalogProductID = @MasterCatalogProductID
		AND UPC.IsActive = 1

	RETURN @Output
	--SELECT @Output AS output

END
