﻿
--  Test Script
--	DECLARE @PracticeLocationID INT
--	SET @PracticeLocationID = 3
--	SELECT * FROM dbo.[udf_GetSuggestedReorder_Given_PracticeLocationID]( @PracticeLocationID) ORDER BY PracticeCatalogProductID
/* ------------------------------------------------------------
   FUNCTION:    [dbo].[udf_GetSuggestedReorder_Given_PracticeLocationID]
   
   Description:  Gets Qusntity on Hand, Quntity on Order, Inventory Counts
					, and Suggested Reorder Level 
					for all location inventory products given a PracticeLocationID.
		
	Dependencies:  	udf_GetSuggestedReorder_Given_PracticeLocationID	 
   
   AUTHOR:       John Bongiorni 10/18/2007 5:45:17 PM
   
   Modifications:  
				2008.02.05 JB  Add column PI.IsNotFlaggedForReorder 
   ------------------------------------------------------------ */ 

CREATE FUNCTION [dbo].[udf_GetSuggestedReorder_Given_PracticeLocationID]
(
	  @PracticeLocationID		INT
)
RETURNS @ReturnTable TABLE 
	( 
		  PracticeCatalogProductID INT
        , NewPracticeCatalogProductID INT DEFAULT 0

		, QuantityOnHand INT DEFAULT 0
		, NewQuantityOnHand INT DEFAULT 0
		, QuantityOnOrder INT DEFAULT 0    --  RemainingToBeReceivedfromOrder
		, NewQuantityOnOrder INT DEFAULT 0    --  NewRemainingToBeReceivedfromOrder

		, ParLevel INT DEFAULT 0					
		, ReorderLevel INT DEFAULT 0			
		, CriticalLevel INT DEFAULT 0
		, PriorityPARLevel  INT DEFAULT 0
		, PriorityReorderLevel INT DEFAULT 0
        , PriorityCriticalLevel  INT DEFAULT 0

		, IsNotFlaggedForReorder BIT DEFAULT 0

		, SuggestedReorderLevel INT DEFAULT 0
        , NewSuggestedReOrderLevel INT DEFAULT 0

		--  The following four columns work and may be uncommented to test or debug.
		--		, StatusOrdered_Quantity_Ordered INT DEFAULT 0
		--		, StatusPartiallyCheckedIn_Quantity_Ordered INT DEFAULT 0
		--		, StatusPartiallyCheckedIn_Quantity_CheckedintoInventory INT DEFAULT 0
		--		, StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory INT DEFAULT 0		
											
		--	The following column works may be uncommented to test or debug.
		--	     , QuantityOnHand_plus_ToBeReceieved INT DEFAULT 0   -- used for comparison with reorderLevel for suggestedReorderLevel
				
	) 
AS
BEGIN	


	--  Table Variable for work.
	DECLARE @ProductTable TABLE
		(
			  PracticeCatalogProductID INT
            , NewPracticeCatalogProductID INT DEFAULT 0

			, QuantityOnHand  INT DEFAULT 0
			, NewQuantityOnHand  INT DEFAULT 0

			, IsNotFlaggedForReorder BIT DEFAULT 0
		)

	DECLARE @Table TABLE
		(
			  PracticeCatalogProductID INT
            , NewPracticeCatalogProductID INT DEFAULT 0

			, StatusOrdered_Quantity_Ordered INT DEFAULT 0
			, NewStatusOrdered_Quantity_Ordered INT DEFAULT 0

			, StatusPartiallyCheckedIn_Quantity_Ordered INT DEFAULT 0
			, StatusPartiallyCheckedIn_Quantity_CheckedintoInventory INT DEFAULT 0
			, StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory INT DEFAULT 0
			, RemainingToBeReceivedfromOrder INT DEFAULT 0

			, NewStatusPartiallyCheckedIn_Quantity_Ordered INT DEFAULT 0
			, NewStatusPartiallyCheckedIn_Quantity_CheckedintoInventory INT DEFAULT 0
			, NewStatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory INT DEFAULT 0
			, NewRemainingToBeReceivedfromOrder INT DEFAULT 0

			, QuantityOnHand  INT DEFAULT 0
			, PARLevel  INT DEFAULT 0
			, ReorderLevel INT DEFAULT 0
            , CriticalLevel  INT DEFAULT 0

			, NewQuantityOnHand  INT DEFAULT 0
			, NewPARLevel  INT DEFAULT 0
			, NewReorderLevel INT DEFAULT 0
            , NewCriticalLevel  INT DEFAULT 0

			, IsNotFlaggedForReorder BIT DEFAULT 0
            
            , QuantityOnHand_plus_ToBeReceieved INT DEFAULT 0
            , NewQuantityOnHand_plus_ToBeReceieved INT DEFAULT 0
            
            , SuggestedReOrderLevel INT DEFAULT 0
            , NewSuggestedReOrderLevel INT DEFAULT 0
            
            , OldMasterCatalogProductID INT DEFAULT 0
            , OldThirdPartyProductID INT DEFAULT 0
            , NewMasterCatalogProductID INT DEFAULT 0
            , NewThirdPartyProductID INT DEFAULT 0
		)


	-- 1A. Get all PracticeCatalogProductID in inventory for the Practice Location
	INSERT INTO @ProductTable
		( 
			  PracticeCatalogProductID
			, NewPracticeCatalogProductID 
			, IsNotFlaggedForReorder
			, QuantityOnHand
		)
	SELECT  
		  PI.PracticeCatalogProductID
		, 0 AS NewPracticeCatalogProductID
        , PI.IsNotFlaggedForReorder
		, PI.QuantityOnHandPerSystem

    FROM ProductInventory AS PI WITH(NOLOCK)
    INNER JOIN [PracticeCatalogProduct] AS PCP WITH(NOLOCK) ON PCP.PracticeCatalogProductID = PI.PracticeCatalogProductID

	WHERE 
		PI.PracticeLocationID = @PracticeLocationID
		AND PI.IsActive = 1
		AND PCP.IsActive = 1



	-- 1B. Get all PracticeCatalogProductID in inventory for the Practice Location
	INSERT INTO @ProductTable
		( 
			  PracticeCatalogProductID
			, NewPracticeCatalogProductID 
			, IsNotFlaggedForReorder
			, QuantityOnHand
		)
	SELECT  
		  PI.PracticeCatalogProductID
		, MAX(CASE WHEN (NPI.PracticeLocationID IS NOT NULL) THEN NPCP.PracticeCatalogProductID ELSE 0 END) AS NewPracticeCatalogProductID
        , NPI.IsNotFlaggedForReorder
		, NPI.QuantityOnHandPerSystem

    FROM ProductInventory AS PI WITH(NOLOCK)
    INNER JOIN [PracticeCatalogProduct] AS PCP WITH(NOLOCK) ON PCP.PracticeCatalogProductID = PI.PracticeCatalogProductID
	LEFT JOIN MasterCatalogProduct OMCP ON OMCP.MasterCatalogProductID = PCP.MasterCatalogProductID
	LEFT JOIN ThirdPartyProduct OTPP ON OTPP.ThirdPartyProductID = PCP.ThirdPartyProductID
	LEFT JOIN ProductReplacement PR ON PR.IsActive = 1 AND
		(PR.OldMasterCatalogProductID = PCP.MasterCatalogProductID OR PR.OldThirdPartyProductID = PCP.ThirdPartyProductID) AND
		(PR.PracticeID IS NULL OR PR.PracticeID = PCP.PracticeID)
	LEFT JOIN MasterCatalogProduct NMCP ON NMCP.MasterCatalogProductID = PR.NewMasterCatalogProductID
	LEFT JOIN ThirdPartyProduct NTPP ON NTPP.ThirdPartyProductID = PR.NewThirdPartyProductID
	LEFT JOIN PracticeCatalogProduct NPCP ON NPCP.MasterCatalogProductID = PR.NewMasterCatalogProductID OR NPCP.ThirdPartyProductID = PR.NewThirdPartyProductID
	LEFT JOIN ProductInventory NPI ON NPI.PracticeCatalogProductID = NPCP.PracticeCatalogProductID AND NPI.PracticeLocationID = @PracticeLocationID

	WHERE 
		PI.PracticeLocationID = @PracticeLocationID
		AND PI.IsActive = 1
		AND PCP.IsActive = 1
		AND PR.IsActive = 1
		AND NPCP.IsActive = 1
		AND NPI.IsActive = 1
	GROUP BY
		  PI.PracticeCatalogProductID
        , NPI.IsNotFlaggedForReorder         
		, NPI.QuantityOnHandPerSystem



	-- 1C. Get all PracticeCatalogProductID in inventory for the Practice Location
	INSERT INTO @ProductTable
		( 
			  PracticeCatalogProductID
			, NewPracticeCatalogProductID 
			, IsNotFlaggedForReorder
			, QuantityOnHand
		)
	SELECT  
		  MAX(CASE WHEN (NPI.PracticeLocationID IS NOT NULL) THEN NPCP.PracticeCatalogProductID ELSE 0 END) AS NewPracticeCatalogProductID
		, MAX(CASE WHEN (NPI.PracticeLocationID IS NOT NULL) THEN NPCP.PracticeCatalogProductID ELSE 0 END) AS NewPracticeCatalogProductID
        , NPI.IsNotFlaggedForReorder
		, NPI.QuantityOnHandPerSystem

    FROM ProductInventory AS PI WITH(NOLOCK)
    INNER JOIN [PracticeCatalogProduct] AS PCP WITH(NOLOCK) ON PCP.PracticeCatalogProductID = PI.PracticeCatalogProductID
	LEFT JOIN MasterCatalogProduct OMCP ON OMCP.MasterCatalogProductID = PCP.MasterCatalogProductID
	LEFT JOIN ThirdPartyProduct OTPP ON OTPP.ThirdPartyProductID = PCP.ThirdPartyProductID
	LEFT JOIN ProductReplacement PR ON PR.IsActive = 1 AND
		(PR.OldMasterCatalogProductID = PCP.MasterCatalogProductID OR PR.OldThirdPartyProductID = PCP.ThirdPartyProductID) AND
		(PR.PracticeID IS NULL OR PR.PracticeID = PCP.PracticeID)
	LEFT JOIN MasterCatalogProduct NMCP ON NMCP.MasterCatalogProductID = PR.NewMasterCatalogProductID
	LEFT JOIN ThirdPartyProduct NTPP ON NTPP.ThirdPartyProductID = PR.NewThirdPartyProductID
	LEFT JOIN PracticeCatalogProduct NPCP ON NPCP.MasterCatalogProductID = PR.NewMasterCatalogProductID OR NPCP.ThirdPartyProductID = PR.NewThirdPartyProductID
	LEFT JOIN ProductInventory NPI ON NPI.PracticeCatalogProductID = NPCP.PracticeCatalogProductID AND NPI.PracticeLocationID = @PracticeLocationID

	WHERE 
		PI.PracticeLocationID = @PracticeLocationID
		AND PI.IsActive = 1
		AND PCP.IsActive = 1
		AND PR.IsActive = 1
		AND NPCP.IsActive = 1
		AND NPI.IsActive = 1
	GROUP BY
		  PI.PracticeCatalogProductID
        , NPI.IsNotFlaggedForReorder         
		, NPI.QuantityOnHandPerSystem


	-- 1D. Get all PracticeCatalogProductID in inventory for the Practice Location
	INSERT INTO @Table
		( 
			  PracticeCatalogProductID
			, NewPracticeCatalogProductID
			, IsNotFlaggedForReorder 
			, QuantityOnHand
			, NewQuantityOnHand
		)
		SELECT
			  PracticeCatalogProductID
			, MAX(NewPracticeCatalogProductID)
			, IsNotFlaggedForReorder
			, SUM(QuantityOnHand)
			, SUM(NewQuantityOnHand)

		FROM @ProductTable

		GROUP BY
			  PracticeCatalogProductID
			, IsNotFlaggedForReorder



	--  2A. Get the SupplierOrderLineItem.QuantityOrder with status of Ordered
	
		UPDATE  @Table
		SET     T.StatusOrdered_Quantity_Ordered = StatusOrdered.StatusOrdered_Quantity_Ordered
		FROM    @Table AS T
			
		INNER JOIN (
				SELECT SOLI.PracticeCatalogProductID, SUM(SOLI.QuantityOrdered) AS StatusOrdered_Quantity_Ordered

				FROM dbo.ProductInventory AS PI WITH(NOLOCK)
				INNER JOIN dbo.PracticeCatalogProduct AS PCP WITH(NOLOCK) ON PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
				INNER JOIN dbo.BregVisionOrder AS BVO WITH(NOLOCK) ON PI.PracticeLocationID = BVO.PracticeLocationID
				INNER JOIN dbo.SupplierOrder AS SO WITH(NOLOCK) ON BVO.BregVisionOrderID = SO.BregVisionOrderID      
				INNER JOIN dbo.SupplierOrderLineItem AS SOLI WITH(NOLOCK) ON SO.SupplierOrderID = SOLI.SupplierOrderID AND PCP.PracticeCatalogProductID = SOLI.PracticeCatalogProductID
				INNER JOIN dbo.SupplierOrderLineItemStatus AS SOLIStatus WITH(NOLOCK) ON SOLI.SupplierOrderLineItemStatusID = SOLIStatus.SupplierOrderLineItemStatusID   
					
			   WHERE 
					PI.PracticeLocationID = @PracticeLocationID  
					AND PI.IsActive = 1    
					AND PCP.IsActive = 1
					AND BVO.IsActive = 1
					AND SO.IsActive = 1
					AND SOLI.IsActive = 1
					AND SOLIStatus.SupplierOrderLineItemStatusID = 1      -- SOLineItemStatus is 'Ordered''
			        
				GROUP BY
					SOLI.PracticeCatalogProductID) AS StatusOrdered
						ON T.PracticeCatalogProductID = StatusOrdered.PracticeCatalogProductID
		

	--  2B. Get the SupplierOrderLineItem.QuantityOrder with status of Ordered
	
		UPDATE  @Table
		SET     T.NewStatusOrdered_Quantity_Ordered = StatusOrdered.StatusOrdered_Quantity_Ordered
		FROM    @Table AS T
			
		INNER JOIN (
				SELECT SOLI.PracticeCatalogProductID, SUM(SOLI.QuantityOrdered) AS StatusOrdered_Quantity_Ordered
		
				FROM dbo.ProductInventory AS PI WITH(NOLOCK)
				INNER JOIN dbo.PracticeCatalogProduct AS PCP WITH(NOLOCK) ON PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
				INNER JOIN dbo.BregVisionOrder AS BVO WITH(NOLOCK) ON PI.PracticeLocationID = BVO.PracticeLocationID
				INNER JOIN dbo.SupplierOrder AS SO WITH(NOLOCK) ON BVO.BregVisionOrderID = SO.BregVisionOrderID      
				INNER JOIN dbo.SupplierOrderLineItem AS SOLI WITH(NOLOCK) ON SO.SupplierOrderID = SOLI.SupplierOrderID  AND PCP.PracticeCatalogProductID = SOLI.PracticeCatalogProductID      
				INNER JOIN dbo.SupplierOrderLineItemStatus AS SOLIStatus WITH(NOLOCK) ON SOLI.SupplierOrderLineItemStatusID = SOLIStatus.SupplierOrderLineItemStatusID   
					
			   WHERE 
					PI.PracticeLocationID = @PracticeLocationID  
					AND PI.IsActive = 1    
					AND PCP.IsActive = 1
					AND BVO.IsActive = 1
					AND SO.IsActive = 1
					AND SOLI.IsActive = 1
					AND SOLIStatus.SupplierOrderLineItemStatusID = 1      -- SOLineItemStatus is 'Ordered''
			        
				GROUP BY
					SOLI.PracticeCatalogProductID) AS StatusOrdered
						ON T.NewPracticeCatalogProductID = StatusOrdered.PracticeCatalogProductID
		


			--  3A. Get StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory
			
			UPDATE  @Table
			SET     
				  T.StatusPartiallyCheckedIn_Quantity_Ordered = StatusPartiallyCheckedIn.StatusPartiallyCheckedIn_Quantity_Ordered
				, T.StatusPartiallyCheckedIn_Quantity_CheckedintoInventory = StatusPartiallyCheckedIn.StatusPartiallyCheckedIn_Quantity_CheckedintoInventory
				, T.StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory = StatusPartiallyCheckedIn.StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory

			FROM @Table AS T
				INNER JOIN (
					SELECT 
						  SOLI.PracticeCatalogProductID
						, MAX(SOLI.QuantityOrdered) AS StatusPartiallyCheckedIn_Quantity_Ordered          -- Checked-IN amount for Partially checked-in SOLineItems
						, SUM(PI_OCI.Quantity) AS StatusPartiallyCheckedIn_Quantity_CheckedintoInventory  -- QuantityOrdered amount for Partially checked-in SOLineItems
						, (MAX( SOLI.QuantityOrdered) - SUM(PI_OCI.Quantity)) AS StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory  -- NOT Checked-IN amount for Partially checked-in SOLineItems
																				
					FROM
						dbo.ProductInventory AS PI WITH(NOLOCK)
						INNER JOIN dbo.PracticeCatalogProduct AS PCP WITH(NOLOCK) ON PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
						INNER JOIN dbo.BregVisionOrder AS BVO WITH(NOLOCK) ON PI.PracticeLocationID = BVO.PracticeLocationID
						INNER JOIN dbo.SupplierOrder AS SO WITH(NOLOCK) ON BVO.BregVisionOrderID = SO.BregVisionOrderID      
						INNER JOIN dbo.SupplierOrderLineItem AS SOLI WITH(NOLOCK) ON SO.SupplierOrderID = SOLI.SupplierOrderID AND PCP.PracticeCatalogProductID = SOLI.PracticeCatalogProductID      
						INNER JOIN dbo.ProductInventory_OrderCheckIn AS PI_OCI ON PI.ProductInventoryID = PI_OCI.ProductInventoryID AND SOLI.SupplierOrderLineItemID = PI_OCI.SupplierOrderLineItemID
							
					   WHERE 
							PI.PracticeLocationID = @PracticeLocationID  
							AND PI.IsActive = 1   
							AND PCP.IsActive = 1
							AND BVO.IsActive = 1
							AND SO.IsActive = 1
							AND SOLI.IsActive = 1
							AND PI_OCI.IsActive = 1
							AND SOLI.SupplierOrderLineItemStatusID = 3      -- SOLineItemStatus is Partially checked-in
					        
						GROUP BY
							SOLI.PracticeCatalogProductID
							
				) AS StatusPartiallyCheckedIn ON T.PracticeCatalogProductID = StatusPartiallyCheckedIn.PracticeCatalogProductID


			--  3B. Get StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory
			
			UPDATE  @Table
			SET     
				  T.NewStatusPartiallyCheckedIn_Quantity_Ordered = StatusPartiallyCheckedIn.StatusPartiallyCheckedIn_Quantity_Ordered
				, T.NewStatusPartiallyCheckedIn_Quantity_CheckedintoInventory = StatusPartiallyCheckedIn.StatusPartiallyCheckedIn_Quantity_CheckedintoInventory
				, T.NewStatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory = StatusPartiallyCheckedIn.StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory

			FROM @Table AS T
				INNER JOIN (
					SELECT 
						  SOLI.PracticeCatalogProductID
						, MAX(SOLI.QuantityOrdered) AS StatusPartiallyCheckedIn_Quantity_Ordered          -- Checked-IN amount for Partially checked-in SOLineItems
						, SUM(PI_OCI.Quantity) AS StatusPartiallyCheckedIn_Quantity_CheckedintoInventory  -- QuantityOrdered amount for Partially checked-in SOLineItems
						, (MAX( SOLI.QuantityOrdered) - SUM(PI_OCI.Quantity)) AS StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory  -- NOT Checked-IN amount for Partially checked-in SOLineItems
																				
					FROM
						dbo.ProductInventory AS PI WITH(NOLOCK)
						INNER JOIN dbo.PracticeCatalogProduct AS PCP WITH(NOLOCK) ON PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
						INNER JOIN dbo.BregVisionOrder AS BVO WITH(NOLOCK) ON PI.PracticeLocationID = BVO.PracticeLocationID
						INNER JOIN dbo.SupplierOrder AS SO WITH(NOLOCK) ON BVO.BregVisionOrderID = SO.BregVisionOrderID      
						INNER JOIN dbo.SupplierOrderLineItem AS SOLI WITH(NOLOCK) ON SO.SupplierOrderID = SOLI.SupplierOrderID AND PCP.PracticeCatalogProductID = SOLI.PracticeCatalogProductID      
						INNER JOIN dbo.ProductInventory_OrderCheckIn AS PI_OCI ON PI.ProductInventoryID = PI_OCI.ProductInventoryID AND SOLI.SupplierOrderLineItemID = PI_OCI.SupplierOrderLineItemID
							
					   WHERE 
							PI.PracticeLocationID = @PracticeLocationID  
							AND PI.IsActive = 1   
							AND PCP.IsActive = 1
							AND BVO.IsActive = 1
							AND SO.IsActive = 1
							AND SOLI.IsActive = 1
							AND PI_OCI.IsActive = 1
							AND SOLI.SupplierOrderLineItemStatusID = 3      -- SOLineItemStatus is Partially checked-in
					        
						GROUP BY
							SOLI.PracticeCatalogProductID
							
				) AS StatusPartiallyCheckedIn ON T.NewPracticeCatalogProductID = StatusPartiallyCheckedIn.PracticeCatalogProductID
							
							
			-- 4A.  Update StatusOrderedorPartiallyCheckedIn_Quantity_RemainingToBeReceivedfromOrder
				
			UPDATE  @Table SET RemainingToBeReceivedfromOrder = StatusOrdered_Quantity_Ordered + StatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory
       

			-- 4B.  Update StatusOrderedorPartiallyCheckedIn_Quantity_RemainingToBeReceivedfromOrder
				
			UPDATE  @Table SET NewRemainingToBeReceivedfromOrder = NewStatusOrdered_Quantity_Ordered + NewStatusPartiallyCheckedIn_Quantity_NotCheckedintoInventory
       

			-- 5A. --  Update QuantityOnHand, Par Level, ReOrder Level, and Critical Levels
			
			UPDATE  @Table
			SET
					  T.QuantityOnHand = PILevels.QuantityOnHand
					, T.PARLevel = PILevels.PARLevel
					, T.ReorderLevel = PILevels.ReorderLevel
					, T.CriticalLevel = PILevels.CriticalLevel
                    
			FROM    @Table AS T
			INNER JOIN 
					( SELECT 
							  PI.PracticeCatalogProductID
                            , PI.QuantityOnHandPerSystem AS QuantityOnHand
                            , PI.ParLevel
                            , PI.ReorderLevel
                            , PI.CriticalLevel
                                                        
                     FROM  
                            ProductInventory AS PI
                            INNER JOIN PracticeCatalogProduct AS PCP ON PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
                     WHERE  
					        PI.PracticeLocationID = @PracticeLocationID                
                            AND  PI.IsActive = 1   
							AND PCP.IsActive = 1
                     
                   ) AS PILevels ON T.PracticeCatalogProductID = PILevels.PracticeCatalogProductID
       

			-- 5B. --  Update QuantityOnHand, Par Level, ReOrder Level, and Critical Levels
			
			UPDATE  @Table
			SET
					  T.NewQuantityOnHand = PILevels.QuantityOnHand
					, T.NewPARLevel = PILevels.PARLevel
					, T.NewReorderLevel = PILevels.ReorderLevel
					, T.NewCriticalLevel = PILevels.CriticalLevel
                    
			FROM @Table AS T
				INNER JOIN (
					SELECT 
							  PI.PracticeCatalogProductID
                            , PI.QuantityOnHandPerSystem AS QuantityOnHand
                            , PI.ParLevel
                            , PI.ReorderLevel
                            , PI.CriticalLevel
                                                        
                     FROM  
                            ProductInventory AS PI
                            INNER JOIN PracticeCatalogProduct AS PCP ON PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
                     WHERE  
					        PI.PracticeLocationID = @PracticeLocationID                
                            AND  PI.IsActive = 1   
							AND PCP.IsActive = 1
                     
                   ) AS PILevels ON T.NewPracticeCatalogProductID = PILevels.PracticeCatalogProductID



			--  6.  UPDATE  SuggestedReorderLevel
			
					UPDATE @Table SET
						QuantityOnHand_plus_ToBeReceieved = QuantityOnHand + RemainingToBeReceivedfromOrder,
						NewQuantityOnHand_plus_ToBeReceieved = NewQuantityOnHand + NewRemainingToBeReceivedfromOrder,
						SuggestedReOrderLevel = CASE WHEN((ParLevel - (QuantityOnHand + RemainingToBeReceivedfromOrder)) < 0 ) 
							THEN 0 ELSE ParLevel - (QuantityOnHand + RemainingToBeReceivedfromOrder) END,
						NewSuggestedReOrderLevel = CASE WHEN((NewParLevel - (NewQuantityOnHand + NewRemainingToBeReceivedfromOrder)) < 0 ) 
							THEN 0 ELSE NewParLevel - (NewQuantityOnHand + NewRemainingToBeReceivedfromOrder) END


            -- 7. Insert the contents of working table into the table variable to be returned.
            Insert INTO @ReturnTable 
				( 
					  PracticeCatalogProductID
					, NewPracticeCatalogProductID

					, QuantityOnHand
					, NewQuantityOnHand
					, QuantityOnOrder
					, NewQuantityOnOrder

					, ParLevel					
					, ReorderLevel			
					, CriticalLevel
					, PriorityPARLevel 
					, PriorityReorderLevel
					, PriorityCriticalLevel 

					, IsNotFlaggedForReorder

					, SuggestedReorderLevel
					, NewSuggestedReOrderLevel
				)
            SELECT 
					  PracticeCatalogProductID
					, NewPracticeCatalogProductID

					, QuantityOnHand
					, NewQuantityOnHand
					, RemainingToBeReceivedfromOrder AS QuantityOnOrder
					, NewRemainingToBeReceivedfromOrder AS NewQuantityOnOrder

					, PARLevel 
					, ReorderLevel
					, CriticalLevel 
					, CASE WHEN (NewPracticeCatalogProductID = 0) THEN ParLevel ELSE CASE WHEN (NewPracticeCatalogProductID = PracticeCatalogProductID) THEN NewParLevel ELSE 0 END END
					, CASE WHEN (NewPracticeCatalogProductID = 0) THEN ReorderLevel ELSE CASE WHEN (NewPracticeCatalogProductID = PracticeCatalogProductID) THEN NewReorderLevel ELSE 0 END END
					, CASE WHEN (NewPracticeCatalogProductID = 0) THEN CriticalLevel ELSE CASE WHEN (NewPracticeCatalogProductID = PracticeCatalogProductID) THEN NewCriticalLevel ELSE 0 END END

					, IsNotFlaggedForReorder

					, CASE WHEN (NewPracticeCatalogProductID = PracticeCatalogProductID) THEN
							CASE WHEN (NewPARLevel - (QuantityOnHand + RemainingToBeReceivedfromOrder + NewQuantityOnHand + NewRemainingToBeReceivedfromOrder) > 0)
							THEN (NewPARLevel - (QuantityOnHand + RemainingToBeReceivedfromOrder + NewQuantityOnHand + NewRemainingToBeReceivedfromOrder)) ELSE 0 END
						ELSE
							SuggestedReOrderLevel
						END
					, NewSuggestedReOrderLevel
					
			FROM @Table
			ORDER BY NewPracticeCatalogProductID DESC

	RETURN	--This will return the table variable @ReturnTable
END
