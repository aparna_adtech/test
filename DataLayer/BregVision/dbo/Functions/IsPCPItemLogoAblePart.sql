﻿-- =============================================
-- Author:		Mike Sneen
-- Create date: 4/8/2012
-- Description:	Determines if a part is logo-able by examining the master catalog (mcp.IsLogoAblePart)
--and Practice (practice.LogoPartNumber not null or empty)
-- =============================================
/*
select dbo.IsPCPItemLogoAblePart(25976)
select dbo.IsPCPItemLogoAblePart(25977)
 */
create FUNCTION IsPCPItemLogoAblePart 
(
	@PracticeCatalogProductID int
)
RETURNS bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @IsPcpItemLogoable bit = 0
	Set @IsPcpItemLogoable = 0
	
	select 
		@IsPcpItemLogoable = case when mcp.IsLogoAblePart = 1  AND p.LogoPartNumber IS Not Null AND p.LogoPartNumber <> '' THEN		
		1		
		else 
		0
		end
	from 
		PracticeCatalogProduct pcp 
		inner join MasterCatalogProduct mcp 
			 on pcp.MasterCatalogProductID = mcp.MasterCatalogProductID
		inner join Practice p
			on pcp.PracticeID = p.practiceId
		Where
			pcp.practiceCatalogProductID = @PracticeCatalogProductID

	-- Return the result of the function
	RETURN @IsPcpItemLogoable

END
