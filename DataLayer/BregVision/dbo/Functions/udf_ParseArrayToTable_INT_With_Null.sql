﻿
--  Test Script
--	DECLARE @Array VARCHAR(4000)
--	SET @Array = '151,34,56,67,78,NULL'
--	SELECT * FROM dbo.[udf_ParseArrayToTable_INT_With_Null]( @Array, ',') ORDER BY ID

CREATE FUNCTION [dbo].[udf_ParseArrayToTable_INT_With_Null]
(
	  @Array		VARCHAR(4000)	-- Array of values to be parsed.  (Values are delimited by a character.)
	, @Separator	CHAR(1)			-- Character that delimits the array, such as a comma.
)  
RETURNS @ReturnTable TABLE ( ID INT NULL) 
AS
BEGIN	

	DECLARE @SeparatorPosition INT		-- This is used to locate each separator character

    DECLARE @ArrayItemValue VARCHAR(10) -- Holds each array value as it is returned

	SET @Array = @Array + @Separator	-- add a seperator character to the end of the array.

    WHILE PATINDEX('%' + @Separator + '%', @Array) <> 0 
    
        BEGIN
        
			--  Get the Position of the separator in the Array.
            SET  @SeparatorPosition = PATINDEX('%' + @Separator + '%', @Array)
            
            --  Get the Array Item
			IF LEFT(@Array, @SeparatorPosition - 1) = 'NULL' 
				BEGIN 
					INSERT INTO @ReturnTable
					SELECT NULL
				END
			ELSE
				BEGIN
					SET  @ArrayItemValue = LEFT(@Array, @SeparatorPosition - 1)
					 -- Insert the Array Item into the table variable.
					Insert INTO @ReturnTable 
					SELECT @ArrayItemValue
				END

           
            
            --  Replace the array item in the array with an empty string.
            SET  @Array = STUFF( @Array, 1, @SeparatorPosition, '' )
            
        END        
        
	RETURN	--This will return the table variable @ReturnTable
END            

