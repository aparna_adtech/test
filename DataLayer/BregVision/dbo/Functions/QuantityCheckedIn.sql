﻿-- =============================================
-- Author:		John Bongiorni
-- Create date: 2007.11.15
-- Description:	Get Quantity Checked In given the SupplierOrderLineItemID
-- =============================================
CREATE FUNCTION [dbo].[QuantityCheckedIn] (@SupplierOrderLineItemID INT)
RETURNS INT
AS
BEGIN

	DECLARE @QuantityCheckedIn INT

	SELECT 
		@QuantityCheckedIn = SUM(Quantity)
	FROM ProductInventory_OrderCheckIn WITH (NOLOCK)
	WHERE SupplierOrderLineItemID = @SupplierOrderLineItemID

	RETURN @QuantityCheckedIn

END
