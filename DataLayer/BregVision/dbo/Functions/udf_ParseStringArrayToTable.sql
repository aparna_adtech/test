﻿

--  Test Script
--	DECLARE @Array VARCHAR(4000)
--	SET @Array = 'A,B,C,F,W'
--	SELECT TextValue FROM dbo.[udf_ParseStringArrayToTable]( @Array, ',') ORDER BY TextValue

--  Note no spaces after comma.

CREATE FUNCTION [dbo].[udf_ParseStringArrayToTable]
(
	  @Array		VARCHAR(4000)	-- Array of values to be parsed.  (Values are delimited by a character.)
	, @Separator	CHAR(1)			-- Character that delimits the array, such as a comma.
)  
RETURNS @ReturnTable TABLE ( TextValue VARCHAR(50) ) 
AS
BEGIN	

	DECLARE @SeparatorPosition INT		-- This is used to locate each separator character

    DECLARE @ArrayItemValue VARCHAR(50) -- Holds each array value as it is returned

	SET @Array = @Array + @Separator	-- add a seperator character to the end of the array.

    WHILE PATINDEX('%' + @Separator + '%', @Array) <> 0 
    
        BEGIN
        
			--  Get the Position of the separator in the Array.
            SET  @SeparatorPosition = PATINDEX('%' + @Separator + '%', @Array)
            
            --  Get the Array Item
            SET  @ArrayItemValue = LEFT(@Array, @SeparatorPosition - 1)
            
            -- Insert the Array Item into the table variable.
            Insert INTO @ReturnTable 
            SELECT @ArrayItemValue
            
            --  Replace the array item in the array with an empty string.
            SET  @Array = STUFF( @Array, 1, @SeparatorPosition, '' )
            
        END        
        
	RETURN	--This will return the table variable @ReturnTable
END            

