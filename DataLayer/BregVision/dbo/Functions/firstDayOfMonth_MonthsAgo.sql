﻿create function [dbo].[firstDayOfMonth_MonthsAgo](@d datetime, @m int ) 
returns datetime
as
BEGIN
return dateadd(ms,+1,DATEADD(mm, DATEDIFF(m,0,@d)-@m, 0))
END

