﻿/*

SELECT dbo.MonthNameAndYear('12/15/2006')

*/

CREATE function [dbo].[MonthNameAndYear](@d datetime) 
returns VARCHAR(50)

AS
BEGIN
	Declare @Output VARCHAR(50)

	SELECT @Output = DATENAME(month, @d) + ' ' + DATENAME(year, @d) 

	Return @Output
	

END

