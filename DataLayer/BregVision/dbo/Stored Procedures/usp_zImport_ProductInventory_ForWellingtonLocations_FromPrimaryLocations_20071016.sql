﻿
CREATE PROC usp_zImport_ProductInventory_ForWellingtonLocations_FromPrimaryLocations_20071016
AS
begin

DECLARE @PracticeLocationID INT
SET @PracticeLocationID = 23

BEGIN TRAN



--SELECT * FROM dbo.productinventory WHERE practicelocationid = 16
--SELECT * FROM practicelocation WHERE practiceID = 6  -- not 15 or 18
-- do for 13, 16,     17, 19, 20, 22, 23  -- 24 is already done.

SELECT COUNT(1) FROM dbo.productinventory WHERE practicelocationid = @PracticeLocationID

INSERT
	INTO dbo.ProductInventory
	(
          PracticeLocationID
        , PracticeCatalogProductID
        , QuantityOnHandPerSystem
        , ParLevel
        , ReorderLevel
        , CriticalLevel
        
        , CreatedUserID
        , CreatedDate
        , ModifiedUserID
        , ModifiedDate
        , IsActive
	)
	SELECT
	
          @PracticeLocationID  AS PracticeLocationID  --@PracticeLocationID
        , PracticeCatalogProductID
        , 0 AS QuantityOnHandPerSystem
        , 0 AS ParLevel 
        , 0 AS ReorderLevel
        , 0 AS CriticalLevel 
        
        , -13 AS CreatedUserID
        , GETDATE() AS CreatedDate
        , -13 AS ModifiedUserID
        , GETDATE() AS ModifiedDate
        , 0 AS IsActive       
  FROM ProductInventory 
  WHERE PracticelocationID = 18
	AND PracticeCatalogProductID NOT IN 
					(SELECT PI.PracticeCatalogProductID 
					 FROM dbo.ProductInventory AS PI 
					 WHERE PI.practicelocationID = @PracticeLocationID)	
	
	
	SELECT * FROM dbo.productinventory WHERE practicelocationid = @PracticeLocationID
	
	
--  COMMIT TRAN
ROLLBACK tran	


END