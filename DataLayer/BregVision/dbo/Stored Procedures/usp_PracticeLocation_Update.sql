﻿

/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocation_Update
   
   Description:  Updates a record in the table PracticeLocation
   
   AUTHOR:       John Bongiorni 7/23/2007 11:05:42 AM
   
   Modifications:  Do allow user to set the primary location to false,
						This is done by explicitly set the correct primary location to true.
   ------------------------------------------------------------ */

CREATE PROCEDURE [dbo].[usp_PracticeLocation_Update]
(
	  @PracticeLocationID				INT
	, @PracticeID						INT
	, @Name								VARCHAR(50)
	, @IsPrimaryLocation				BIT
--	, @EmailForOrderApproval			VARCHAR(100)
--	, @EmailForLowInventoryAlerts		VARCHAR(100)
--	, @IsEmailForLowInventoryAlertsOn	BIT
	, @ModifiedUserID					INT
	, @IsHCPSignatureRequired			BIT
)
AS
BEGIN
DECLARE    @Err             			INT        --  holds the @@ERROR code returned by SQL Server


--	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
--
--	SELECT @TransactionCountOnEntry = @@TRANCOUNT
--	SELECT @Err = @@ERROR
--	BEGIN TRANSACTION      

	
	DECLARE @RecordCount INT

	-- Is There already a location with the same name with a different practicelocationID?
	SELECT @RecordCount = COUNT(1)
	FROM dbo.PracticeLocation
	WHERE 
		NAME = @Name
		AND PracticeID = @PracticeID
		AND	IsActive = 1
		AND PracticeLocationID <> @PracticeLocationID
	
	
	IF @RecordCount = 0
	BEGIN

--		IF @Err = 0
--		BEGIN

		
			DECLARE @IsPrimaryLocationCount INT  -- not used.
			
			
			IF ( @IsPrimaryLocation = 1)
			BEGIN
			
				UPDATE dbo.PracticeLocation
				SET
					  PracticeID = @PracticeID		
					, Name = @Name
					, IsPrimaryLocation = @IsPrimaryLocation
	--				, EmailForOrderApproval = @EmailForOrderApproval
	--				, EmailForLowInventoryAlerts = @EmailForLowInventoryAlerts
	--				, IsEmailForLowInventoryAlertsOn = @IsEmailForLowInventoryAlertsOn
					, ModifiedUserID = @ModifiedUserID
					, ModifiedDate = GETDATE()
					, IsHcpSignatureRequired = @IsHCPSignatureRequired

				WHERE 
					PracticeLocationID = @PracticeLocationID

			END
			ELSE	--  @IsPrimaryLocation = 0 so do not update.
			BEGIN
				UPDATE dbo.PracticeLocation
				SET
					  PracticeID = @PracticeID		
					, Name = @Name
	--				, IsPrimaryLocation = @IsPrimaryLocation
	--				, EmailForOrderApproval = @EmailForOrderApproval
	--				, EmailForLowInventoryAlerts = @EmailForLowInventoryAlerts
	--				, IsEmailForLowInventoryAlertsOn = @IsEmailForLowInventoryAlertsOn
					, ModifiedUserID = @ModifiedUserID
					, ModifiedDate = GETDATE()
					, IsHcpSignatureRequired = @IsHCPSignatureRequired

				WHERE 
					PracticeLocationID = @PracticeLocationID
			END
--				SELECT @Err = @@ERROR
--
--		END
--		IF @Err = 0
--		BEGIN



			--  May need to use this.
			IF (@IsPrimaryLocation = 1)
			BEGIN
				EXEC usp_PracticeLocation_Set_Primary_Location @PracticeLocationID = @PracticeLocationID
			END	
--			SET @Err = @@ERROR	
--			
--		END
--
--		IF @@TranCount > @TransactionCountOnEntry
--		BEGIN

--			IF @Err = 0
--				BEGIN 
--					COMMIT TRANSACTION
--					RETURN @Err
--				END
--			ELSE
--				BEGIN
--					ROLLBACK TRANSACTION
--					RETURN @Err
--					--  Add any database logging here      
--				END
		END
	END	
	
--SELECT * FROM dbo.PracticeLocation 
--UPDATE dbo.PracticeLocation 
--  SET dbo.PracticeLocation.IsPrimaryLocation = 1
