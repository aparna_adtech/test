﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_IsEmailOrderPlacement]
	-- Add the parameters for the stored procedure here
	@BregVisionOrderID int,
	@SupplierOrderID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select 
	sub.IsemailOrderPlacement,
	sub.EmailOrderPlacement,
	sub.FaxOrderPlacement,	
	SUB.SupplierOrderID

from
(


   select 
MCS.IsemailOrderPlacement,
MCS.EmailOrderPlacement,
MCS.FaxOrderPlacement,	
	SO.SupplierOrderID,
0 AS IsThirdPartyProduct
 from BregVisionOrder BVO 
	inner join SupplierOrder SO 
		on BVO.BregVisionOrderID = SO.BregVisionOrderID
	inner join PracticeCatalogSupplierBrand PCSB
		on SO.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
	inner join MasterCatalogSupplier MCS
		on PCSB.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
where BVO.isactive=1
	and SO.isactive=1
	and PCSB.isactive=1
	and BVO.BregVisionOrderID=@BregVisionOrderID
	and SO.SupplierOrderID = @SupplierOrderID

union 

   select 
TPS.IsemailOrderPlacement,
TPS.EmailOrderPlacement,
TPS.FaxOrderPlacement,	
	SO.SupplierOrderID,
1 AS IsThirdPartyProduct
 from BregVisionOrder BVO 
	inner join SupplierOrder SO 
		on BVO.BregVisionOrderID = SO.BregVisionOrderID
	inner join PracticeCatalogSupplierBrand PCSB
		on SO.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
	inner join ThirdPartySupplier TPS
		on PCSB.thirdPartySupplierid = TPS.thirdPartySupplierid
where BVO.isactive=1
	and SO.isactive=1
	and PCSB.isactive=1
	and BVO.BregVisionOrderID=@BregVisionOrderID
	and SO.SupplierOrderID = @SupplierOrderID

) as SUB

ORDER BY
	Sub.IsThirdPartyProduct

END

