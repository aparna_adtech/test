﻿


CREATE PROCEDURE [dbo].[usp_ReceiptHistory]
	-- Add the parameters for the stored procedure here
	@PracticeLocationID int
	, @SearchCriteria varchar(50)
	, @SearchText  varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	/*These variables are used in conjunction with the coalesce statements in the where clause.
	each of these variables will be null except one is @SearchCriteria matches.*/
	Declare @PatientCode varchar(50)			--NULL   These default to null
	Declare @PatientName varchar(50)
	Declare @PhysicianName varchar(50)
	Declare @BCSClaimID varchar(50)
	Declare @DispenseBatchID int = 0
	
	if @SearchCriteria='PatientCode'
		Set @PatientCode = '%' +  @SearchText + '%'	
	if @SearchCriteria='PatientName'
		Set @PatientName = '%' +  @SearchText + '%'	
	if @SearchCriteria='PhysicianName'
		Set @PhysicianName = '%' +  @SearchText + '%'	
	if @SearchCriteria='BCSClaimID'
		Set @BCSClaimID = '%' +  @SearchText + '%'
	if @SearchCriteria='Batch'
		Set @DispenseBatchID = CAST(@SearchText as int)

  select d.DispenseID
		, CASE WHEN d.CreatedWithHandheld is Null or d.CreatedWithHandheld=0 THEN 'No'
		ELSE 'Yes'
		End as CreatedWithHandheld		

		,d.DispenseBatchID
		,d.PatientCode
		,d.Total
		,d.DateDispensed
		,ltrim(rtrim(isnull(d.PatientFirstName,'') + ' ' + isnull(d.PatientLastName,''))) as PatientName
		,HasABNForm = CAST( Coalesce(
		
		(CASE WHEN (select count(dd.ABNType) from DispenseDetail dd where dd.DispenseID = d.DispenseID AND dd.ABNType is not null AND dd.ABNType != '') > 0 THEN 1 ELSE 0 END)
		
		, 0) 
		
		as bit)
		,bcs.BCSClaimID
	from Dispense d left outer join [BregVisionBCSClaims].[dbo].[DispensementBCSClaimMap] bcs
	on d.DispenseID = bcs.DispenseID 
	where d.practicelocationid=@PracticeLocationID --PLID
	AND d.IsActive = 1
	AND COALESCE(d.PatientCode, '') like COALESCE(@PatientCode , d.PatientCode, '')
	AND COALESCE(bcs.BCSClaimID, '') like COALESCE(@BCSClaimID , bcs.BCSClaimID, '')
	AND (COALESCE(d.PatientFirstName, '') like COALESCE(@PatientName , d.PatientFirstName, '')
		or
		COALESCE(d.PatientLastName, '') like COALESCE(@PatientName , d.PatientLastName, ''))
	AND (
			(
				(@PhysicianName IS NOT NULL) AND (d.DispenseID in (Select distinct dd.DispenseID from DispenseDetail dd 
					join Physician subP on dd.PhysicianID = subP.PhysicianID
					join Contact subC on subP.ContactID = subC.ContactID
					where COALESCE(subC.LastName, '') like COALESCE(@PhysicianName , subC.LastName, '')))
			)
			OR (@PhysicianName IS NULL)
					
		)
	AND (
			((@DispenseBatchID > 0) AND (d.DispenseBatchID = @DispenseBatchID))
			OR
			(@DispenseBatchID = 0)
		)
	order by d.CreatedDate
	Print case when @PhysicianName is null then '1' else '2' end
END
