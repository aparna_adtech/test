﻿
/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocationBillingAddress_Insert
   
   Description:  Inserts a record into table Address
				  , Gets the new AddressID
				  , and Inserts a record into the PracticeLocationBillingAddress table.
   
   AUTHOR:       John Bongiorni 8/06/2007 8:00 PM
   
   Modifications:  
   ------------------------------------------------------------ */  
   
CREATE PROCEDURE [dbo].[usp_PracticeLocationBillingAddress_Insert]
(
	  @PracticeLocationID			 INT
    , @AddressID                     INT = NULL OUTPUT
	, @AttentionOf					 VARCHAR(50) = NULL
    , @AddressLine1                  VARCHAR(50)
    , @AddressLine2                  VARCHAR(50) = NULL
    , @City                          VARCHAR(50)
    , @State                         CHAR(2)
    , @ZipCode                       CHAR(5)
    , @ZipCodePlus4                  CHAR(4) = NULL
    , @CreatedUserID                 INT
)
AS
BEGIN

	DECLARE @TransactionCountOnEntry    INT,       -- Transaction Count before the transaction begins
			@Err						INT        --  holds the @@Error code returned by SQL Server

	SELECT @Err = @@ERROR
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @Err = 0
	BEGIN    
		SELECT @TransactionCountOnEntry = @@TRANCOUNT
		BEGIN TRANSACTION
	END        

--	IF @Err = 0
--	BEGIN
--
--		UPDATE dbo.Practice
--		SET IsBillingCentralized = @IsBillingCentralized
--		WHERE PracticeLocationID = @PracticeLocationID 
--		
--		SET @Err = @@ERROR
--
--	END

	IF @Err = 0
	BEGIN

		INSERT
		INTO dbo.Address
		(
			  AddressLine1
			, AddressLine2
			, City
			, State
			, ZipCode
			, ZipCodePlus4
			, CreatedUserID
			, IsActive
		)
		VALUES
		(
			  @AddressLine1
			, @AddressLine2
			, @City
			, @State
			, @ZipCode
			, @ZipCodePlus4
			, @CreatedUserID
			, 1
		)

		SET @Err = @@ERROR
	END

	IF @Err = 0
	BEGIN

		SELECT @AddressID = SCOPE_IDENTITY()

		SELECT @Err = @@ERROR
	END


	IF @Err = 0
	BEGIN

		INSERT
		INTO dbo.PracticeLocationBillingAddress
		(
			  PracticeLocationID
			, AddressID
			, AttentionOf
			, CreatedUserID
			, IsActive
		)
		VALUES
		(
			  @PracticeLocationID
			, @AddressID
			, @AttentionOf
			, @CreatedUserID
			, 1
		)

		SELECT @Err = @@ERROR
	END


	--May need to fix this
	IF @@TranCount > @TransactionCountOnEntry
	BEGIN

		IF @Err = 0
			COMMIT TRANSACTION
		ELSE
			ROLLBACK TRANSACTION
			--  Add any database logging here      
	END
			
		RETURN @Err
END


