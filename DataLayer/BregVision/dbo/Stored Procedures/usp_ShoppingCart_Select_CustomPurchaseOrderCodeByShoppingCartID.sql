﻿
CREATE PROC [dbo].[usp_ShoppingCart_Select_CustomPurchaseOrderCodeByShoppingCartID]
	  @ShoppingCartID INT
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT CustomPurchaseOrderCode
	FROM ShoppingCart
	WHERE ShoppingCartID = @ShoppingCartID;

END
