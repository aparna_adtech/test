﻿-- =============================================
--EXEC USP_User_SelectByUserName 'MikesTestUser2'
--EXEC USP_User_SelectByUserName 'adminortho'
-- Author:		<Author:Mike Sneen>
-- Create date: <Create Date:04/06/09>
-- Description:	<Description:Used to get the user permissions to menu items 
-- Will be used in the master pages >
-- Modification:  
-- =============================================
Create PROCEDURE [dbo].[USP_User_SelectByUserName] --''
	-- Add the parameters for the stored procedure here
	@UserName VARCHAR(50)
AS
BEGIN

	SELECT  distinct  
		U.*
					

	
	FROM aspnetdb.dbo.aspnet_Users AS AU 
	INNER JOIN dbo.[User] AS U
		ON AU.UserId = U.AspNet_UserID


	WHERE AU.LoweredUserName = LOWER(@UserName)
	AND U.IsActive = 1  --  JB  2008.01.11

END