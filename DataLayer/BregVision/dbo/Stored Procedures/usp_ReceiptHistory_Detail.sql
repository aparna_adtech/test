﻿-- =============================================
-- Author:		Greg Ross
-- Create date: 5/5/08
-- Description:	Used to get all information so that user can choose a historical dispensement to view or print
-- Modified Nov 12 to add third party products to the data - GR
--usp_ReceiptHistory_Detail 3, 'PhysicianName', 'Alleyne'
--usp_ReceiptHistory_Detail 3, 'PhysicianName', 'kane'
--usp_ReceiptHistory_Detail 3, 'PatientCode', 'Sneen'
-- =============================================
CREATE PROCEDURE [dbo].[usp_ReceiptHistory_Detail]
	-- Add the parameters for the stored procedure here
	@PracticeLocationID int
	, @SearchCriteria varchar(50)
	, @SearchText  varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		/*These variables are used in conjunction with the coalesce statements in the where clause.
	each of these variables will be null except one is @SearchCriteria matches.*/
	Declare @PatientCode varchar(50)			--NULL   These default to null
	Declare @PhysicianName varchar(50)

	if @SearchCriteria='PatientCode'
		Set @PatientCode = '%' +  @SearchText + '%'
	if @SearchCriteria='PhysicianName'
		Set @PhysicianName = '%' +  @SearchText + '%'

Select
	SUB.DispenseID
	,SUB.DispenseDetailID
	,SUB.DDDispenseID
	,SUB.ActualChargeBilled
	,SUB.DMEDeposit
	,SUB.Quantity
	,cast (SUB.ABNForm as bit) as ABNForm
	,SUB.ABNType
	,SUB.LineTotal
	,SUB.PhysicianName
	,SUB.WholesaleCost
	,SUB.BillingCharge
	,SUB.BillingChargeCash
	,SUB.SupplierName
	,SUB.SupplierShortName
	,SUB.Name
	,SUB.ShortName
	,SUB.Code
	,SUB.Packaging
	,SUB.Side
	,SUB.Size
	,SUB.Color
	,SUB.Gender
	,SUB.Mod1
	,SUB.Mod2
	,SUB.Mod3

FROM
	(
	 select D.DispenseID
		,DD.DispenseDetailID
		,DD.DispenseID as DDDispenseID
		,DD.ActualChargeBilled
		,DD.Quantity
		,DD.ABNForm
		,DD.ABNType
		,LineTotal
		,C.FirstName + ' ' + C.LastName as PhysicianName
		,PCP.WholesaleCost
		,PCP.BillingCharge
		--,PCP.DMEDeposit
		,DD.DMEDeposit
		,PCP.BillingChargeCash
		,PCSB.SupplierName
		,PCSB.SupplierShortName
		,MCP.Name
		,MCP.ShortName
		,MCP.Code
		,MCP.Packaging
		,DD.LRModifier as Side --,MCP.LeftRightSide as Side
		,MCP.Size
		,MCP.Color
		,MCP.Gender
		,DD.Mod1
		,DD.Mod2
		,DD.Mod3
	 from dispensedetail DD
	left outer join Physician P on DD.PhysicianID = P.PhysicianID
	left outer join Contact C on P.ContactID = C.ContactID
	inner join practicecatalogproduct PCP
		on DD.practicecatalogproductID = PCP.practicecatalogproductID
	inner join PracticeCatalogSupplierBrand PCSB
		on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
	inner join MasterCatalogProduct MCP
		on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
	inner join Dispense D
		on DD.DispenseID = D.DispenseID
	--left outer join DispenseQueue dq
	--	on DD.DispenseQueueID = dq.DispenseQueueID

	where D.PracticeLocationID=@PracticeLocationID
	and D.IsActive=1
	and DD.Isactive=1
	--and PCP.Isactive=1
	--and PCSB.Isactive=1
	--and MCP.ISACTIVE=1
	AND COALESCE(D.PatientCode, '') like COALESCE(@PatientCode , D.PatientCode, '')
	AND(((@PhysicianName Is Not Null) AND (DD.PhysicianID = (Select top 1 subP.PhysicianID from Physician subP join Contact subC on subP.ContactID = subC.ContactID
							AND COALESCE(subC.FirstName + ' ' + subC.LastName, '') like COALESCE(@PhysicianName , subC.FirstName + ' ' + subC.LastName, '')
						  ))) OR (@PhysicianName IS Null))
	and PCP.ISThirdPartyProduct=0

	UNION All

	 select D.DispenseID
		,DD.DispenseDetailID as DDDispenseID
		,DD.DispenseID
		,DD.ActualChargeBilled
		,DD.Quantity
		,DD.ABNForm
		,DD.ABNType
		,LineTotal
		,C.FirstName + ' ' + C.LastName as PhysicianName
		,PCP.WholesaleCost
		,PCP.BillingCharge
		--,PCP.DMEDeposit
		,DD.DMEDeposit
		,PCP.BillingChargeCash
		,PCSB.SupplierName
		,PCSB.SupplierShortName
		,TPP.Name
		,TPP.ShortName
		,TPP.Code
		,TPP.Packaging
		,DD.LRModifier as Side --,TPP.LeftRightSide as Side
		,TPP.Size
		,TPP.Color
		,TPP.Gender
		,DD.Mod1
		,DD.Mod2
		,DD.Mod3
	 from dispensedetail DD
	inner join Physician P on DD.PhysicianID = P.PhysicianID
	left outer join Contact C on P.ContactID = C.ContactID
	left outer join practicecatalogproduct PCP
		on DD.practicecatalogproductID = PCP.practicecatalogproductID
	inner join PracticeCatalogSupplierBrand PCSB
		on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
	inner join THirdPartyProduct TPP
		on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
	inner join Dispense D
		on DD.DispenseID = D.DispenseID
		--left outer join DispenseQueue dq
		--on DD.DispenseQueueID = dq.DispenseQueueID

	where D.PracticeLocationID=@PracticeLocationID
	and D.IsActive=1
	and DD.Isactive=1
	--and PCP.Isactive=1
	--and PCSB.Isactive=1
	--and TPP.ISACTIVE=1
	and PCP.ISThirdPartyProduct=1
	AND COALESCE(D.PatientCode, '') like COALESCE(@PatientCode , D.PatientCode, '')
	AND(((@PhysicianName Is Not Null) AND (DD.PhysicianID = (Select top 1 subP.PhysicianID from Physician subP join Contact subC on subP.ContactID = subC.ContactID
							AND COALESCE(subC.FirstName + ' ' + subC.LastName, '') like COALESCE(@PhysicianName , subC.FirstName + ' ' + subC.LastName, '')
						  ))) OR (@PhysicianName IS Null))
	)
	AS SUB

END
