﻿

CREATE PROCEDURE [dbo].[usp_PracticeLocationShippingAddress_UpdateInsert]
(
	  @PracticeLocationID			 INT
    , @AddressID                     INT = NULL OUTPUT
	, @AttentionOf					 VARCHAR(50) = NULL
    , @AddressLine1                  VARCHAR(50)
    , @AddressLine2                  VARCHAR(50) = NULL
    , @City                          VARCHAR(50)
    , @State                         CHAR(2)
    , @ZipCode                       CHAR(5)
    , @ZipCodePlus4                  CHAR(4) = NULL
    , @UserID						 INT
)
AS
BEGIN

--  DECLARE @AddressID INT
DECLARE @Err INT

			-- Check if PracticeLocationShippingAddress already has a record for the practiceLocation	
		SELECT @AddressID = AddressID 
		FROM PracticeLocationShippingAddress 
		WHERE PracticeLocationID = @PracticeLocationID	
		
		SELECT  @Err = @@Error
		
		IF ( @AddressID IS NOT NULL ) --so UPDATE
			BEGIN
				
				DECLARE	@return_value int

				EXEC	@Err = [dbo].[usp_PracticeLocationShippingAddress_Update]
						@PracticeLocationID = @PracticeLocationID
						, @AttentionOf = @AttentionOf
						, @AddressLine1 = @AddressLine1
						, @AddressLine2 = @AddressLine2
						, @City = @City
						, @State = @State
						, @ZipCode = @ZipCode
						, @ZipCodePlus4 = @ZipCodePlus4 
						 , @ModifiedUserID = @UserID
				
				SELECT  @Err = @@Error
				
			END
		
		IF ( @AddressID IS NULL ) --so INSERT
			BEGIN

				EXEC	@Err = [dbo].[usp_PracticeLocationShippingAddress_Insert]
							  @PracticeLocationID = @PracticeLocationID
							, @AddressID = @AddressID OUTPUT
							, @AttentionOf = @AttentionOf
							, @AddressLine1 = @AddressLine1
							, @AddressLine2 = @AddressLine2
							, @City = @City
							, @State = @State
							, @ZipCode = @ZipCode
							, @ZipCodePlus4 = @ZipCodePlus4 
							, @CreatedUserID = @UserID
							
				SELECT  @Err = @@Error

				SELECT	@AddressID = @AddressID
				
				SELECT  @Err = @@Error
				
			END   
		
		RETURN @Err
		
	END

