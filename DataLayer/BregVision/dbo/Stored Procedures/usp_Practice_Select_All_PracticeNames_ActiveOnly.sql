﻿/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Practice_Select_All_Names
   
   Description:  Selects all records from the table dbo.Practice
   				returns the PracticeID and the PracticeName.
   
   AUTHOR:       John Bongiorni 7/25/2007 9:45:21 PM
   
   Modifications:  
   ------------------------------------------------------------ */  -------------------SELECT ALL----------------

CREATE PROCEDURE [dbo].[usp_Practice_Select_All_PracticeNames_ActiveOnly]
AS
BEGIN
	DECLARE @Err INT
	SELECT
		  PracticeName
	FROM dbo.Practice
	WHERE IsActive = 1
	ORDER BY PracticeName
	
	SET @Err = @@ERROR

	RETURN @Err
END

