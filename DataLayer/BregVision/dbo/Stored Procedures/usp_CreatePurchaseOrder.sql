﻿/*
	John Bongiorni
	2008.01.15
	Create Order given ShoppingCartID

*/

--  usp_CreatePurchaseOrder  0, 414

CREATE PROC [dbo].[usp_CreatePurchaseOrder]
--  CREATE PROCEDURE dbo.usp_BregVisionOrder_Insert_From_ShoppingCartID
    @PracticeLocationID INT = 0,		--  USELESS, Not used for; for backward compatibility only. 
    @ShoppingCartID INT,
    @UserID INT = -99,
    @ShippingTypeID INT,			--  USELESS, Not used for; for backward compatibility only. 
    @PurchaseOrderID INT OUTPUT,
    @CustomPurchaseOrderCode VARCHAR(20) = ''		--  For future use; not yet implemented.
AS 
    BEGIN TRY
		BEGIN TRANSACTION    -- Start the transaction

        DECLARE @BVOID INT			--  BregVisionOrderID that is created.
        DECLARE @PurchaseOrder INT	--  Purchase Order.  (@BVOID + @PurchaseOrder)

        
	
	
		--  Step 1:  Create the BregVision Order given the ShoppingCartID, UserID, and CustomPurchaseOrderCode
        INSERT  INTO dbo.BregVisionOrder
                (
                  PracticeLocationID,
                  BregVisionOrderStatusID,
                  ShippingTypeID,				 
				--  PurchaseOrder,				-- to be gather later.
                  CustomPurchaseOrderCode,	-- from input.
				--  Total,						-- update later.
                  CreatedUserID,				-- from parameter.
                  ModifiedUserID,				-- from parameter.
                  IsActive					-- set as 1.
			
                )
                SELECT  
						  SC.PracticeLocationID
                        , 1 AS BregVisionOrderStatusID  --  Ordered
                        
                        , @ShippingTypeID
                        , @CustomPurchaseOrderCode AS CustomPurchaseOrderCode  --  Not Yet implemented.
                       
                        , @UserID AS CreatedUserID
                        , @UserID AS ModifiedUserID
                        , 1 AS IsActive
                FROM    dbo.ShoppingCart AS SC
                WHERE   SC.ShoppingCartID = @ShoppingCartID
                        AND SC.ShoppingCartStatusID = 0  -- Pending (as opposed to Denied.)
                        AND SC.IsActive = 1
		

	
		--  Step 2:  Get the BregVisionOrderID and Set the PO Number for the new order. 
        SELECT  @BVOID = SCOPE_IDENTITY()  --  New BregVisionOrderID that was just inserted.
        SET @PurchaseOrder = 100000 + @BVOID  -- Set the PO number
			
		--  Testing
		--		SELECT @BVOID AS BVOID, @PurchaseOrder AS PurchaseOrder
		
		--  Step 3:  Update the BregVisionOrder with the Purchase Order Number for the new order. 								
        UPDATE  dbo.BregVisionOrder
        SET     BregVisionOrder.PurchaseOrder = @PurchaseOrder
        WHERE   BregVisionOrder.BregVisionOrderID = @BVOID	
		
		

		--  Testing:  SELECT * FROM dbo.BregVisionOrder WHERE BregVisionOrderID = @BVOID   
		
		
		--  Step 4:  Populate the dbo.SupplierOrder for each Supplier / Brand combo in the Cart.  
        INSERT  INTO dbo.SupplierOrder
                (
                  BregVisionOrderID,
                  PracticeCatalogSupplierBrandID,  --  Supplier / Brand combo.
                  SupplierOrderStatusID,
                  PurchaseOrder,
                  CustomPurchaseOrderCode,
                  CreatedUserID,
                  ModifiedUserID,
                  IsActive
		    )
                SELECT DISTINCT
                        @BVOID AS BregVisionOrderID,
                        PCP.PracticeCatalogSupplierBrandID,	--  Supplier / Brand combo.
                        
                        1 AS SupplierOrderStatusID,			--  Ordered
                        
                        @PurchaseOrder AS PurchaseOrder,
                        @CustomPurchaseOrderCode AS CustomPurchaseOrderCode,
                        @UserID AS CreatedUserID,
                        @UserID AS ModifiedUserID,
                        1 AS IsActive
                FROM    dbo.ShoppingCart AS SC
                        INNER JOIN dbo.ShoppingCartItem AS SCI ON SC.ShoppingCartID = SCI.ShoppingCartID
                        INNER JOIN dbo.PracticeCatalogProduct AS PCP ON PCP.PracticeCatalogProductID = SCI.PracticeCatalogProductID
                WHERE   SC.ShoppingCartID = @ShoppingCartID
                        AND SC.IsActive = 1
                        AND SCI.IsActive = 1
                        AND PCP.IsActive = 1
                        AND SC.ShoppingCartStatusID = 0
                        AND SCI.ShoppingCartItemStatusID = 0
			
			--	For testing only.			
			--	SELECT * FROM dbo.SupplierOrder WHERE BregVisionOrderID = @BVOID  ORDER BY SUPPLIERORDERid DESC   


		--  Step 5:  Populate the Supplier Order Line Items based on the Shopping Cart Items.
        INSERT  INTO dbo.SupplierOrderLineItem
                (
                  SupplierOrderID,
                  PracticeCatalogProductID,
                  SupplierOrderLineItemStatusID,
                  ActualWholesaleCost,
                  QuantityOrdered,
                  LineTotal,
                  CreatedUserID,
                  CreatedDate,
                  ModifiedUserID,
                  ModifiedDate,
                  IsActive
		    )
            SELECT  
				SO.SupplierOrderID,
                PCP.PracticeCatalogProductID,
                1 AS SupplierOrderLineItemStatusID,  --  Ordered
		   --PCP.PracticeID,
		   --  PCP.PracticeCatalogSupplierBrandID,
				PCP.WholesaleCost AS ActualWholesaleCost,

		   --PL.PracticeLocationID,
		   --PL.PracticeID,
		   --PL.[Name],
		   
		   --SC.ShoppingCartID,
		   --SC.PracticeLocationID,
		   --SC.ShoppingCartStatusID,
		   --SC.ShippingTypeID,
		   --SC.DeniedComments,

--		   SCI.ShoppingCartID,
--		   SCI.ShoppingCartItemID,
--		   SCI.PracticeCatalogProductID,
--		   SCI.ShoppingCartItemStatusID,
                SCI.Quantity AS QuantityOrdered,
                PCP.WholesaleCost * SCI.Quantity AS LineTotal,
                @UserID AS CreatedUserID,
                GETDATE() AS CreatedDate,
                @UserID AS ModifiedUserID,
                GETDATE() AS ModifiedDate,
                1 AS IsActive
			
			FROM    dbo.ShoppingCart AS SC
                INNER JOIN dbo.ShoppingCartItem AS SCI 
					ON SC.ShoppingCartID = SCI.ShoppingCartID
                INNER JOIN dbo.PracticeLocation AS PL 
					ON SC.PracticeLocationID = PL.PracticeLocationID
                INNER JOIN dbo.PracticeCatalogProduct AS PCP 
					ON SCI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
                INNER JOIN dbo.SupplierOrder AS SO 
					ON SO.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
                    AND SO.BregVisionOrderID = @BVOID
			WHERE   SC.ShoppingCartID = @ShoppingCartID
                AND SC.IsActive = 1
                AND SCI.IsActive = 1
                AND PL.IsActive = 1
                AND PCP.IsActive = 1
                AND SC.ShoppingCartStatusID = 0
                AND SCI.ShoppingCartItemStatusID = 0
        ORDER BY SO.SupplierOrderID,
                PCP.PracticeCatalogProductID



		--  Step 6:  Calculate and update the Supplier Order Total. 
        UPDATE  dbo.SupplierOrder
        SET     Total = Derived.Total		--  SELECT Derived.Total AS Total
        FROM    ( SELECT    SOLI.SupplierOrderID AS SupplierOrderID,
                            SUM(SOLI.LineTotal) AS Total
                  FROM      dbo.SupplierOrder AS SO
                            INNER JOIN dbo.SupplierOrderLineItem AS SOLI 
								ON SO.SupplierOrderID = SOLI.SupplierOrderID
                  WHERE     SO.BregVisionOrderID = @BVOID
                            AND SO.IsActive = 1
                            AND SOLI.IsActive = 1
                            AND SO.SupplierOrderStatusID = 1		-- Ordered
                            AND SOLI.SupplierOrderLineItemStatusID = 1  -- Ordered
                  GROUP BY  SOLI.SupplierOrderID
                ) AS Derived
        WHERE   SupplierOrder.SupplierOrderID = Derived.SupplierOrderID



		--  Step 7:  Calculate and update the Breg Vision Order Total. 
        UPDATE  dbo.BregVisionOrder
        SET     Total = Derived.Total		--  SELECT Derived.Total AS Total
        FROM    ( SELECT    SO.BregVisionOrderID AS BregVisionOrderID,
                            SUM(SO.Total) AS Total
                  FROM      dbo.SupplierOrder AS SO
                  WHERE     SO.BregVisionOrderID = @BVOID
                            AND SO.IsActive = 1
                            AND SO.SupplierOrderStatusID = 1  --  Ordered
                  GROUP BY  SO.BregVisionOrderID
                ) AS Derived
        WHERE   BregVisionOrder.BregVisionOrderID = Derived.BregVisionOrderID


		-- For Testing Purposes
--        SELECT  'BVO', *
--        FROM    dbo.BregVisionOrder
--        WHERE   BregVisionOrder.BregVisionOrderID = @BVOID 
--	
--        SELECT  'SO', *
--        FROM    dbo.SupplierOrder
--        WHERE   SupplierOrder.BregVisionOrderID = @BVOID  
--		
--		
--        SELECT  'SOLI'
--				, SOLI.*
--        FROM    dbo.SupplierOrder AS SO
--        INNER JOIN dbo.SupplierOrderLineItem AS SOLI 
--			ON SO.SupplierOrderID = SOLI.SupplierOrderID
--        WHERE   SO.BregVisionOrderID = @BVOID
--        ORDER BY SO.SupplierOrderID,
--                SOLI.SupplierOrderLineItemID
				
		
		
--		Notify when duplicates are have been created in the BregVisionOrder.
		
--        SELECT  
--                  P.PracticeID
--                , P.PracticeName AS Practice
--                , PL.PracticeLocationID 
--                , PL.NAME AS PracticeLocation
--                , SO.BregVisionOrderID
--                , SO.SupplierOrderID
--                , SOLI.PracticeCatalogProductID
--                , BVO.CreatedDate   
--                     
--        FROM    BregVisionOrder AS BVO
--        INNER JOIN dbo.PracticeLocation AS PL
--			ON BVO.PracticeLocationID = PL.PracticeLocationID
--		INNER JOIN dbo.Practice AS P
--			ON PL.PracticeID = P.PracticeID
--        INNER JOIN dbo.SupplierOrder AS SO
--			ON BVO.BregVisionOrderID = SO.BregVisionOrderID
--        INNER JOIN dbo.SupplierOrderLineItem AS SOLI 
--			ON SO.SupplierOrderID = SOLI.SupplierOrderID
--		GROUP BY 
--				  P.PracticeID
--                , P.PracticeName
--                , PL.NAME
--                , PL.PracticeLocationID 
--                , SO.BregVisionOrderID
--                , SO.SupplierOrderID
--                , SOLI.PracticeCatalogProductID
--                , BVO.CreatedDate   
--                
--		HAVING COUNT(1) > 1
--		ORDER BY SO.BregVisionOrderID DESC
--			, P.PracticeName
--			, PL.NAME
--			, SO.SupplierOrderID
--			, SOLI.PracticeCatalogProductID
--			, BVO.CreatedDate   
--  

		--  STEP 9:  Clear out the Shopping Cart.
		DELETE FROM ShoppingCartItem
		WHERE ShoppingCartID = @ShoppingCartID
		
		DELETE FROM ShoppingCart
		WHERE ShoppingCartID = @ShoppingCartID
	 

		-- Successfully executed.
		SET @PurchaseOrderID = @PurchaseOrder
		
	   COMMIT TRANSACTION	
   
	END TRY

	BEGIN CATCH
	  -- There was an error, so rollback the entire transaction.
	  
	  IF @@TRANCOUNT > 0
		 ROLLBACK TRANSACTION
		 
	  EXECUTE [dbo].[uspLogError]
	  EXECUTE [dbo].[uspPrintError]	

	  SET @PurchaseOrderID = 0
	  
	END CATCH

		
