﻿
/*
Author Mike Sneen
Date   2/20/2010
	exec usp_GetPracticeLocationHCPCs '3,4,5'
	exec usp_GetPracticeLocationHCPCs '3'
	exec usp_GetPracticeLocationHCPCs @PracticeLocationIDString=N'26,14'
*/

CREATE PROC [dbo].[usp_GetPracticeLocationHCPCs]  
		  @PracticeLocationIDString			varchar(2000),
		  @ExtraHCPC						VARCHAR(2000) = NULL
AS	  
Begin

IF @ExtraHCPC IS NULL
BEGIN
	select distinct HCPCS from (
		select distinct dq.HCPCs
          from DispenseQueue dq
          where dq.PracticeLocationID in (select ID from dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))
            and nullif(dq.HCPCs, '') is not null

		union

		select 
			LTrim(RTrim(ph.HCPCS )) AS HCPCS
		from 
			ProductInventory pi
			inner join PracticeCatalogProduct pcp
				on pi.PracticeCatalogProductID = pcp.PracticeCatalogProductID
			inner join ProductHCPCS ph on
				pcp.PracticeCatalogProductID = ph.PracticeCatalogProductID
		where
			pi.PracticeLocationID in (SELECT ID 
					 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))
	) as T
	Group By T.HCPCS
	Order By T.HCPCS
END
ELSE
BEGIN
	SELECT HCPCS FROM
		(SELECT DISTINCT HCPCS FROM
			(
				select distinct dq.HCPCs
				  from DispenseQueue dq
				  where dq.PracticeLocationID in (select ID from dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))
				    and nullif(dq.HCPCs, '') is not null

				union

				select 
					LTrim(RTrim(ph.HCPCS )) AS HCPCS
				from 
					ProductInventory pi
					inner join PracticeCatalogProduct pcp
						on pi.PracticeCatalogProductID = pcp.PracticeCatalogProductID
					inner join ProductHCPCS ph on
						pcp.PracticeCatalogProductID = ph.PracticeCatalogProductID
				where
					pi.PracticeLocationID in (SELECT ID 
							 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))
			) a

		UNION

			(SELECT @ExtraHCPC AS HCPCS)
		) AS SUB
	Group By HCPCS
	Order By HCPCS
END

END