﻿
/*
Author Mike Sneen
Date   2/20/2010
	exec usp_GetPracticeLocationHCPCs_ByProduct '3,4,5'
	exec usp_GetPracticeLocationHCPCs_ByProduct '3'
	exec usp_GetPracticeLocationHCPCs_ByProduct @PracticeLocationIDString=N'26,14'
*/

CREATE PROC [dbo].[usp_GetPracticeLocationHCPCs_ByProduct]  
		  @PracticeLocationIDString			varchar(2000)
AS	  
Begin

SELECT DISTINCT
	ISNULL(NULLIF(
		LTRIM(RTrim(dbo.ConcatHCPCSDelimit(pcp.PracticeCatalogProductID, '; '))), ''),
	'No HCPC') AS HCPCS
from 
	ProductInventory pi
	inner join PracticeCatalogProduct pcp
		on pi.PracticeCatalogProductID = pcp.PracticeCatalogProductID
where
	pi.PracticeLocationID in (SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))

End