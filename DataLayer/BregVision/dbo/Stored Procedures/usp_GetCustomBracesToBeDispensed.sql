﻿
-- =============================================
-- Author:		<Author: Mike Sneen>
-- Create date: <Create Date: 06/16/09>
-- Description:	<Description: Used to populate the grdCustomBracesToBeDispensed on home page >
--  MODIFICATION:  

--EXEC [usp_GetCustomBracesToBeDispensed] 3
-- =============================================


CREATE PROCEDURE [dbo].[usp_GetCustomBracesToBeDispensed]   -- 1, 3   --6, 15  -- 1, 3
	-- Add the parameters for the stored procedure here
    @PracticeLocationID int
AS 
    BEGIN
select

	MCS.[SupplierName],
    MCP.[name] as ProductName,
	ISNULL(C.[FirstName], '') AS FirstName,
    ISNULL(C.[LastName], '') AS LastName,
	BVO.PurchaseOrder,
	BVO.CustomPurchaseOrderCode,
	SCCB.PatientName as PatientCode,
	BVO.Total as WholesaleCost,
	BillingCost = (select SUM(PCPsub.BillingCharge) from PracticeCatalogProduct PCPsub where PCPsub.PracticeCatalogProductID IN (Select SOLIsub.PracticeCatalogProductID from SupplierOrderLineItem SOLIsub where SOLIsub.SupplierOrderID=SO.SupplierOrderID)),
    
    MCP.[Code],
	MCP.LeftRightSide as Side,
	MCP.Size,
	PI_OCI.DateCheckedIn
    
	--SCCB.*
	
from
	ShoppingCartCustomBrace SCCB
	inner Join BregVisionOrder BVO on SCCB.BregVisionOrderID=BVO.BregVisionOrderID
	inner join SupplierOrder SO on BVO.BregVisionOrderID = SO.BregVisionOrderID
	inner join SupplierOrderLineItem SOLI on  SO.SupplierOrderID = SOLI.SupplierOrderID
	INNER JOIN [PracticeCatalogProduct] PCP ON SOLI.[PracticeCatalogProductID] = PCP.[PracticeCatalogProductID]
	Inner Join ProductInventory_OrderCheckIn PI_OCI on SOLI.SupplierOrderLineItemID = PI_OCI.SupplierOrderLineItemID
	LEFT JOIN [Physician] Phy 
		ON SCCB.[PhysicianID] = Phy.[PhysicianID]
		AND Phy.IsActive = 1
		
    LEFT  JOIN [Contact] 
		C ON Phy.[ContactID] = c.[ContactID]
		AND C.IsActive = 1
	INNER JOIN [MasterCatalogProduct] MCP 
		ON PCP.[MasterCatalogProductID] = MCP.[MasterCatalogProductID]
		
    INNER JOIN [MasterCatalogSubCategory] MCSC 
		ON MCP.[MasterCatalogSubCategoryID] = MCSC.[MasterCatalogSubCategoryID]
		
    INNER JOIN [MasterCatalogCategory] MCC 
		ON MCSC.[MasterCatalogCategoryID] = MCC.[MasterCatalogCategoryID]
		
    INNER JOIN [MasterCatalogSupplier] MCS 
		ON MCc.[MasterCatalogSupplierID] = MCS.[MasterCatalogSupplierID]	
	
where 
	MCP.IsCustomBrace = 1
	AND SCCB.DispenseID is null
	AND BVO.BregVisionOrderStatusID = 2
	AND BVO.PracticeLocationID = @PracticeLocationID
	
	AND BVO.[IsActive] = 1
	AND PCP.[IsActive] = 1
    AND MCSC.[IsActive] = 1
    AND MCC.[IsActive] = 1
    AND MCS.[IsActive] = 1
END	