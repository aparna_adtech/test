﻿/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Lookup_Suffix_Select_All
   
   Description:  Selects all records from the table dbo.Lookup_Suffix
   
   AUTHOR:       John Bongiorni 8/2/2007 5:41:07 PM
   
   Modifications:  
   ------------------------------------------------------------ */  

CREATE PROCEDURE [dbo].[usp_Lookup_Suffix_Select_All]
AS
BEGIN
	DECLARE @Err INT
	SELECT
		  Lookup_SuffixID
		, Suffix
	
	FROM dbo.Lookup_Suffix
	
	SET @Err = @@ERROR

	RETURN @Err
END


