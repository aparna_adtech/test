﻿
/* ------------------------------------------------------------
   PROCEDURE:    [dbo].[usp_ProductInventory_Delete]
   
   Description:  "Deletes" a record from table dbo.ProductInventory
   					This actual flags the IsActive column to false.
					
   Details:      MWS 1/3/2011  Refactored to Set all associated records to inactive and allow a delete
   
				There are three case when the user attempts to 
				 delete a product in the product inventory table.
				Denoted by the @RemovalType variable.
				0. Unable to be removed due to the product have QOH, of Inv. Levels other than 0
				1. Hard Delete, no dependecies in other tables.  Delete.  Should be in the ProductInventory audit table.
				2. Soft Delete, some dependencies, but still okay.  Set Inactive to true.  Product still viewable in reports.		 
				 
   Dependencies: 
   
   Pages used by:  DAL.PracticeLocation.Inventory.cs
   Accessed from web page:  Admin\PracticeLocation_ProductInventory.aspx
   
   AUTHOR:       John Bongiorni 02/05/2008 12:54:42 AM
   
   Modifications:
   MWS 1/3/2011  Removed hard delete and refactored to set all associated records to inactive and allow a delete
   If hard delete, then delete, if not the set to IsActive = False.
   Check for dependencies.

   ------------------------------------------------------------ */ 


CREATE PROCEDURE [dbo].[usp_ProductInventory_Admin_Delete]  
    (
        @ProductInventoryID		INT
      , @UserID			INT
      , @RemovalType			INT  OUTPUT
      , @DependencyMessage		VARCHAR(2000) OUTPUT  -- To application / user.
    )
AS 

    BEGIN TRY	
    
			DECLARE @PracticeLocationID INT
			DECLARE @PracticeCatalogProductID INT

						
			SET @DependencyMessage = ''
			
			--  Get the PracticeLocationID and the PracticeCatalogProductID given the @ProductInventoryID. 
			/*
			SELECT 
				  @PracticeLocationID   = Sub.PracticeLocationID
				, @PracticeCatalogProductID = Sub.PracticeCatalogProductID
			FROM 
				(
				 SELECT 
					PracticeLocationID,
					PracticeCatalogProductID
				 FROM [ProductInventory] AS PI	WITH (NOLOCK)
				 WHERE PI.[ProductInventoryID] = @ProductInventoryID
				 ) AS Sub
			*/
			

			--  Set QuantityOnHandPerSystem, PI.ParLevel, PI.ReorderLevel, PI.CriticalLevel to zero In ProductInventory.
			UPDATE ProductInventory
				SET IsActive = 0,
				QuantityOnHandPerSystem = 0,
				ParLevel = 0,
				ReorderLevel = 0,
				CriticalLevel = 0,
				ModifiedUserID = @UserID,
				ModifiedDate = GETDATE()
			where ProductInventoryID = @ProductInventoryID -- PracticeCatalogProductID = @PracticeCatalogProductID

			--  Update the Shopping Cart and set any items to inactive.  
			/*
			UPDATE ShoppingCartItem
				SET IsActive = 0,
				ModifiedUserID = @UserID,
				ModifiedDate = GETDATE()
			where PracticeCatalogProductID = @PracticeCatalogProductID
			*/
			/*
			--  Check that there are no outstanding order items for the product.  ie not fully checked in.  Set isActive = 0 and status = 2(Checked in)			
			UPDATE SupplierOrderLineItem 
			SET 
				IsActive = 0, 
				SupplierOrderLineItemStatusID = 2, 
				ModifiedUserID = @UserID, 
				ModifiedDate = GETDATE()
			WHERE 
				PracticeCatalogProductID = @PracticeCatalogProductID
				AND SupplierOrderLineItemStatusID <> 2
				AND IsActive = 1
			*/

			SET @RemovalType = 2 	--  Execute a soft delete(mws We only do soft deletes now as of 1/3/2011).
	
	END TRY

	BEGIN CATCH

	  EXECUTE [dbo].[uspLogError]
	  EXECUTE [dbo].[uspPrintError]
	  
	END CATCH	
    