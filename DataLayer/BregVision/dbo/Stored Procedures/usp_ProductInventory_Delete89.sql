﻿
/* ------------------------------------------------------------
   PROCEDURE:    [dbo].[usp_ProductInventory_Delete]
   
   Description:  "Deletes" a record from table dbo.PrroductInventory
   					This actual flags the IsActive column to false.
					
   AUTHOR:       John Bongiorni 02/05/2008 12:54:42 AM
   
   Modifications:
   TODO:  If hard delete, then delete, if not the set to IsActive = False.
   Check for dependencies.
   ------------------------------------------------------------ */ 
/*
             [dbo].[usp_ProductInventory_Delete_Count]  , -2000
*/

--SELECT * FROM ProductInventory WHERE PracticeLocationID = 3 AND IsActive = 1
--2
--3
--4
--5
--6
--7
--8
--9
--10
--11
--12
--13
--14
--15
--16
--17
--18
--19
--20
--64
--143
--553
--588
--6988
--6989
--6990
--6991
--6992
--6993
--6994
--6995
--6996
--6997
--6998
--6999
--7714
--7715
--7716
--7717
--7827
--7828
--7829
--7830
--7831
--7832
--7833
--7834
--7835
--7836
--14351
--14353
--

--  Dependency count = 105
--(1 row(s) affected)
--105
--Delete Soft
--
--(1 row(s) affected)
--  [dbo].[usp_ProductInventory_Delete89]  2, -999089


CREATE PROCEDURE [dbo].[usp_ProductInventory_Delete89]  --2, -999089
    (
        @ProductInventoryID INT
      , @ModifiedUserID	  INT
    )
AS 

    BEGIN TRY	
    
			DECLARE @PracticeLocationID INT
			DECLARE @PracticeCatalogProductID INT
			
    
			--  Given the PIID, Get the PracticeLocation and the PCPID
			SELECT 
				  @PracticeLocationID   = Sub.PracticeLocationID
				, @PracticeCatalogProductID = Sub.PracticeCatalogProductID
			FROM 
				(
				 SELECT 
					PracticeLocationID,
					PracticeCatalogProductID
				 FROM [ProductInventory] AS PI	WITH (NOLOCK)
				 WHERE PI.[ProductInventoryID] = @ProductInventoryID
				 ) AS Sub
			--  AND PI.IsActive = 1
			
			
			--  Following is for testing.  Tested successfully.
			--  SELECT @PracticeLocationID AS PL, @PracticeCatalogProductID AS pcp
    
    
			--  Check Shopping Cart.
			--  Check SupplierOrderLineItem.
			--  Check ProductInventory_OrderCheckIn.
			
			DECLARE @Count_Cart INT 			
			DECLARE @Count_SupplierOrderLineItemCount INT 
			DECLARE @Count_ProductInventory_DispenseDetail  INT
			DECLARE @Count_ProductInventory_OrderCheckIn INT 
			DECLARE @Count_ProductInventory_ManualCheckIn INT
			DECLARE @Count_ProductInventory_ReturnedToSupplier int
			DECLARE @Count_ProductInventory_Purge INT
			DECLARE @Count_ProductInventory_QuantityOnHandPhysical INT
			DECLARE @Count_ProductInventory_ResetQuantityOnHandPerSystem Int
			
			--  the following works and has been tested (unit)
			--  Check Shopping Cart.
			SELECT 
				@Count_Cart = SUB.Count_Cart
			FROM
				(
					SELECT  			
						COUNT(1) AS Count_Cart  --SC.[PracticeLocationID], SI.[PracticeCatalogProductID] 			
					FROM ShoppingCart AS SC WITH (NOLOCK)
					INNER JOIN ShoppingCartItem AS SI WITH (NOLOCK)
						ON SC.ShoppingCartID = SI.ShoppingCartID
					--ORDER BY SI.practicecatalogproductID
					WHERE 
						  SI.PracticeCatalogProductID = @PracticeCatalogProductID
					  AND SC.PracticeLocationID = @PracticeLocationID
				) AS SUB
				
			--  Following is for testing.  Tested successfully.
			--  cHANGE order of the tables.
			--  Check SupplierOrderLineItem.
				SELECT 
				@Count_SupplierOrderLineItemCount = SUB.Count_SupplierOrderLineItemCount
			FROM
				(
			
					SELECT COUNT(1) AS Count_SupplierOrderLineItemCount
					FROM [SupplierOrderLineItem] AS SOLI  WITH (NOLOCK)
					INNER JOIN [SupplierOrder] AS SO  WITH (NOLOCK)
						ON Soli.supplierOrderID = SO.supplierOrderID
					INNER JOIN [BregVisionOrder] AS BVO  WITH (NOLOCK)
						ON SO.BregVisionOrderID = BVO.BregVisionOrderID
					WHERE 
						BVO.[PracticeLocationID] = @PracticeLocationID
						AND SOLI.[PracticeCatalogProductID] = @PracticeCatalogProductID
						AND BVO.[IsActive] = 1
						AND SO.[IsActive] = 1
						AND SOLI.[IsActive] = 1	
				) AS SUB		
			
			
			
			
			
			
			
			
			--  Check ProductInventory_DispenseDetail
			
				SELECT 
					@Count_ProductInventory_DispenseDetail = SUB.Count_ProductInventory_DispenseDetail
				FROM
					(			
						SELECT COUNT(1) AS Count_ProductInventory_DispenseDetail
						FROM dbo.ProductInventory_DispenseDetail AS PIDD  WITH (NOLOCK)
						WHERE PIDD.ProductInventoryID = @ProductInventoryID
					) AS SUB
			
					
					
					
				SELECT 
					@Count_ProductInventory_OrderCheckIn = SUB.Count_ProductInventory_OrderCheckIn
				FROM
					(			
						SELECT COUNT(1) AS Count_ProductInventory_OrderCheckIn
						FROM dbo.ProductInventory_OrderCheckIn AS PI_OCI  WITH (NOLOCK)
						WHERE PI_OCI.ProductInventoryID = @ProductInventoryID
					) AS SUB
					

					
				SELECT 
					@Count_ProductInventory_ManualCheckIn = SUB.Count_ProductInventory_ManualCheckIn
				FROM
					(			
						SELECT COUNT(1) AS Count_ProductInventory_ManualCheckIn
						FROM dbo.ProductInventory_ManualCheckIn AS PI_OCI  WITH (NOLOCK)
						WHERE PI_OCI.ProductInventoryID = @ProductInventoryID
					) AS SUB
			
			
			----
				SELECT 
					@Count_ProductInventory_ReturnedToSupplier = SUB.Count_ProductInventory_ReturnedToSupplier
				FROM
					(			
						SELECT COUNT(1) AS Count_ProductInventory_ReturnedToSupplier
						FROM dbo.ProductInventory_ReturnedToSupplier AS PI_OCI  WITH (NOLOCK)
						WHERE PI_OCI.ProductInventoryID = @ProductInventoryID
					) AS SUB
					

					
				--------------------------	
				SELECT 
					@Count_ProductInventory_Purge = SUB.Count_ProductInventory_Purge
				FROM
					(			
						SELECT COUNT(1) AS Count_ProductInventory_Purge
						FROM dbo.ProductInventory_Purge AS PI_OCI  WITH (NOLOCK)
						WHERE PI_OCI.ProductInventoryID = @ProductInventoryID
					) AS SUB
					
					
					
				SELECT 
					@Count_ProductInventory_QuantityOnHandPhysical = SUB.Count_ProductInventory_QuantityOnHandPhysical
				FROM
					(			
						SELECT COUNT(1) AS Count_ProductInventory_QuantityOnHandPhysical
						FROM dbo.ProductInventory_QuantityOnHandPhysical AS PI_OCI  WITH (NOLOCK)
						WHERE PI_OCI.ProductInventoryID = @ProductInventoryID
					) AS SUB
			
			
			
					SELECT 
					@Count_ProductInventory_ResetQuantityOnHandPerSystem = SUB.Count_ProductInventory_ResetQuantityOnHandPerSystem
				FROM
					(			
						SELECT COUNT(1) AS Count_ProductInventory_ResetQuantityOnHandPerSystem
						FROM dbo.ProductInventory_ResetQuantityOnHandPerSystem AS PI_OCI  WITH (NOLOCK)
						WHERE PI_OCI.ProductInventoryID = @ProductInventoryID
					) AS SUB
					
					
					
		
				--  For testing only.
--				SELECT 
--					@Count_Cart AS Cart
--					, @Count_SupplierOrderLineItemCount AS soli
--					, @Count_ProductInventory_OrderCheckIn AS PI_OCI
--					, @Count_ProductInventory_DispenseDetail AS PIDD
--					, @Count_ProductInventory_ManualCheckIn AS PI_MCI
--					, @Count_ProductInventory_ReturnedToSupplier AS PI_RTS
--					, @Count_ProductInventory_Purge AS PI_P
--					, @Count_ProductInventory_QuantityOnHandPhysical AS PI_QOHP
--					, @Count_ProductInventory_ResetQuantityOnHandPerSystem AS PI_RQOHPS
			
			
				DECLARE @TotalCount INT
				DECLARE @IsHardDelete BIT
				SET @IsHardDelete = NULL
				SET @TotalCount = 9999  -- To avoid an inadvertent delete.
			
				SET
					@TotalCount =	
					  @Count_Cart 
					+ @Count_SupplierOrderLineItemCount 
					+ @Count_ProductInventory_OrderCheckIn 
					+ @Count_ProductInventory_DispenseDetail 
					+ @Count_ProductInventory_ManualCheckIn 
					+ @Count_ProductInventory_ReturnedToSupplier 
					+ @Count_ProductInventory_Purge 
					+ @Count_ProductInventory_QuantityOnHandPhysical 
					+ @Count_ProductInventory_ResetQuantityOnHandPerSystem 
			
				SELECT @TotalCount AS TotalCount
				PRINT @TotalCount 
				
				DECLARE @DependencyCount INT
				SET @DependencyCount = @TotalCount
		
				IF (@DependencyCount = 0)  
				BEGIN
					SET @IsHardDelete = 1  --  Execute a hard delete.
					PRINT 'Delete Hard'
				END
				ELSE IF (@DependencyCount > 0)  
				BEGIN
					SET @IsHardDelete = 0  --  Execute a soft delete.
					PRINT 'Delete Soft'
				END
		
				IF (@IsHardDelete = 1)
				  BEGIN
					DELETE dbo.ProductInventory		            	        
					WHERE ProductInventoryID = @ProductInventoryID
				  END
				ELSE IF (@IsHardDelete = 0) 
				  BEGIN
    				UPDATE dbo.ProductInventory		            
					SET IsActive = 0		        
					WHERE ProductInventoryID = @ProductInventoryID
				  END
				  
				--TODO: IF @TotalCount = 0 then HARD delete
				--  ELSE Soft Delete.
				
				--  TEST, TEST, TEST, 
				
				--  IF @TotalCount > 0 THEN Set IsActive = 0?????????
				--  find the effects of this through out the system.
				--	Cart?
				--  Order?
				--  Reports?
				--  Dispense?
				--  Check-In
				
				
				
--			SELECT * FROM [ProductInventory]
--			WHERE [PracticeCatalogProductID] = 983
			
			--SELECT * FROM practicelocation WHERE practicelocationid IN (41,42,43)
			
--    7828
--7858
--7861
--7864

--			--  IF their are dependencies.
--    		UPDATE
--				dbo.ProductInventory
--	            
--			SET IsActive = 0
--	        
--			WHERE
--				ProductInventoryID = @ProductInventoryID
	
			
	END TRY

	BEGIN CATCH

	  EXECUTE [dbo].[uspLogError]
	  EXECUTE [dbo].[uspPrintError]
	  
	END CATCH	
    


--SELECT *  --  12570
--FROM [ProductInventory]
--WHERE [PracticeLocationID] = 46
--AND [PracticeCatalogProductID] = 2544


