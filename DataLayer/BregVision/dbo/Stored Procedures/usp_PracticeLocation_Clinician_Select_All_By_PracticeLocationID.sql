﻿
CREATE PROCEDURE [dbo].[usp_PracticeLocation_Clinician_Select_All_By_PracticeLocationID]
		@PracticeLocationID INT
AS
BEGIN	
	SELECT
		CL.ClinicianID
	  , C.ContactID
	  , C.Salutation
	  , C.FirstName
	  , C.MiddleName
	  , C.LastName
	  , C.Suffix
	  , C.FirstName + ' ' + C.LastName AS ClinicianName
	
	FROM		
		dbo.Contact AS C

	INNER JOIN	
		dbo.Clinician AS CL
			ON C.ContactID = CL.ContactID
	
	INNER JOIN	
		dbo.PracticeLocation_Physician AS PLP		
			ON CL.ClinicianID = PLP.PhysicianID

	WHERE
		PLP.PracticeLocationID = @PracticeLocationID
		AND CL.IsProvider = 1
		AND CL.IsActive = 1
		AND C.IsActive = 1
END




