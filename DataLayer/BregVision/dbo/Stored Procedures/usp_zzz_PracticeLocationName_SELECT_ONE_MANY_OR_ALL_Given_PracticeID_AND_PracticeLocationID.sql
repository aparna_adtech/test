﻿/*
	Get all practice Names given the practiceID and one many or all PracticeIDs.
	Test:
	EXEC:
		EXEC usp_zzz_PracticeLocationName_SELECT_ONE_MANY_OR_ALL_Given_PracticeID_AND_PracticeLocationID 1, '0'
		EXEC usp_zzz_PracticeLocationName_SELECT_ONE_MANY_OR_ALL_Given_PracticeID_AND_PracticeLocationID 1, '3,25,41,43'
		EXEC usp_zzz_PracticeLocationName_SELECT_ONE_MANY_OR_ALL_Given_PracticeID_AND_PracticeLocationID 1, '3,25,40,41,42,43'
		EXEC usp_zzz_PracticeLocationName_SELECT_ONE_MANY_OR_ALL_Given_PracticeID_AND_PracticeLocationID 1, '3,5,7'

*/


CREATE PROC usp_zzz_PracticeLocationName_SELECT_ONE_MANY_OR_ALL_Given_PracticeID_AND_PracticeLocationID 
		  @PracticeID					INT
		, @PracticeLocationIDString   VARCHAR(2000)
AS 
BEGIN
		
----  Get the PracticeID
--DECLARE @PracticeID INT
--SET @PracticeID = 6

--DECLARE @PracticeLocationID INT
--SET @PracticeLocationID = 3

--  @PracticeLocationIDTable to stored the PracticeLocationIDs to search
DECLARE @PracticeLocationIDStringTable TABLE
	(PracticeLocationIDString VARCHAR(2000) )
	
-- Check if PracticeLocationID is 0 -- That is ALL for the practice.
--  If 0 then get ALL PracticeLocationID into the @PracticeLocationIDTable
IF @PracticeLocationIDString = '0'
	BEGIN
		--  Build String
		
		DECLARE @Output VARCHAR(2000)
		SET @Output = ''
		
		SELECT @Output =	CASE @Output 
					WHEN '' THEN CAST(PracticeLocationID AS VARCHAR(50)) 
					ELSE @Output + ', ' + CAST(PracticeLocationID AS VARCHAR(50)) 
					END
		FROM PracticeLocation
		WHERE PracticeID = @PracticeID
		AND IsActive = 1
	
		INSERT INTO @PracticeLocationIDStringTable 
		(PracticeLocationIDString) 
		SELECT @Output
		
		SELECT @PracticeLocationIDString = @Output
		
	END	
ELSE
	BEGIN
		INSERT INTO @PracticeLocationIDStringTable 
		(PracticeLocationIDString) 
		SELECT @PracticeLocationIDString
		
	END

--SELECT PracticeLocationIDString 
--		 FROM @PracticeLocationIDStringTable
--
--SELECT ID 
-- FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ',')

SELECT PracticeID, PracticeLocationID, NAME
FROM PracticeLocation
WHERE PracticeLocationID IN 
		(SELECT ID 
		 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))
AND PracticeID = @PracticeID

END	