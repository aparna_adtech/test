﻿
/* ------------------------------------------------------------
   PROCEDURE:    [dbo].[usp_ProductInventory_Delete]
   
   Description:  "Deletes" a record from table dbo.PrroductInventory
   					This actual flags the IsActive column to false.
					
   AUTHOR:       John Bongiorni 02/05/2008 12:54:42 AM
   
   Modifications:
   TODO:  If hard delete, then delete, if not the set to IsActive = False.
   Check for dependencies.
   ------------------------------------------------------------ */ 
   
CREATE PROCEDURE [dbo].[usp_ProductInventory_Delete98]  --26, -2000
    (
        @ProductInventoryID INT
      , @ModifiedUserID	  INT
    )
AS 

    BEGIN TRY	
    
			DECLARE @PracticeLocationID INT
			DECLARE @PracticeCatalogProductID INT
			
    
			--  Given the PIID, Get the PracticeLocation and the PCPID
			SELECT 
				  @PracticeLocationID   = Sub.PracticeLocationID
				, @PracticeCatalogProductID = Sub.PracticeCatalogProductID
			FROM 
				(
				 SELECT 
					PracticeLocationID,
					PracticeCatalogProductID
				 FROM [ProductInventory] AS PI	WITH (NOLOCK)
				 WHERE PI.[ProductInventoryID] = @ProductInventoryID
				 ) AS Sub
			--  AND PI.IsActive = 1
			
			
			--  Following is for testing.  Tested successfully.
			SELECT @PracticeLocationID AS PL, @PracticeCatalogProductID AS pcp
    
    
			--  Check Shopping Cart.
			--  Check SupplierOrderLineItem.
			--  Check ProductInventory_OrderCheckIn.
			
			DECLARE @Count_Cart INT 
			DECLARE @Count_SupplierOrderLineItemCount INT 
			DECLARE @Count_ProductInventory_OrderCheckIn INT 
			
			--  the following works and has been tested (unit)
			--  Check Shopping Cart.
			SELECT  
			
				COUNT(1) AS Count_Cart  --SC.[PracticeLocationID], SI.[PracticeCatalogProductID] 
			
			FROM ShoppingCart AS SC
			INNER JOIN ShoppingCartItem AS SI
				ON SC.ShoppingCartID = SI.ShoppingCartID
			--ORDER BY SI.practicecatalogproductID
			WHERE 
				  SI.PracticeCatalogProductID = @PracticeCatalogProductID
			  AND SC.PracticeLocationID = @PracticeLocationID
			
			
			--  Check SupplierOrderLineItem.
			SELECT COUNT(1) AS Count_SupplierOrderLineItemCount
			FROM [SupplierOrderLineItem] AS SOLI  WITH (NOLOCK)
			INNER JOIN [SupplierOrder] AS SO  WITH (NOLOCK)
				ON Soli.supplierOrderID = SO.supplierOrderID
			INNER JOIN [BregVisionOrder] AS BVO  WITH (NOLOCK)
				ON SO.BregVisionOrderID = BVO.BregVisionOrderID
			WHERE 
				BVO.[PracticeLocationID] = @PracticeLocationID
				AND SOLI.[PracticeCatalogProductID] = @PracticeCatalogProductID
				AND BVO.[IsActive] = 1
				AND SO.[IsActive] = 1
				AND SOLI.[IsActive] = 1	
			
			
SELECT * FROM [SupplierOrderLineItem]
			
			
			
			
			
			SELECT * FROM [ProductInventory]
			WHERE [PracticeCatalogProductID] = 983
			
			--SELECT * FROM practicelocation WHERE practicelocationid IN (41,42,43)
			
--    7828
--7858
--7861
--7864

--			--  IF their are dependencies.
--    		UPDATE
--				dbo.ProductInventory
--	            
--			SET IsActive = 0
--	        
--			WHERE
--				ProductInventoryID = @ProductInventoryID
	
			
	END TRY

	BEGIN CATCH

	  EXECUTE [dbo].[uspLogError]
	  EXECUTE [dbo].[uspPrintError]
	  
	END CATCH	
    


SELECT *  --  12570
FROM [ProductInventory]
WHERE [PracticeLocationID] = 46
AND [PracticeCatalogProductID] = 2544


