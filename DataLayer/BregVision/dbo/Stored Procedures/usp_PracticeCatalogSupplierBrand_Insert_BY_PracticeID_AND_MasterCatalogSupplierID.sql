﻿--   2. Get PracticeCatalogSupplierBrandID for the practice and supplier.
--    If this does not exist Insert record int ProductCatalogSupplierBrand	
--		This is a nested proc called by
--		dbo.usp_Insert_dbo.PracticeCatalogSupplierBrandID_From_MasterCatalogProductID	

-- TEST
--		SELECT * FROM dbo.PracticeCatalogSupplierBrand 
--		DECLARE	@return_value int,
--				@PracticeCatalogSupplierBrandID int
--
--		EXEC	@return_value = [dbo].[usp_PracticeCatalogSupplierBrand_Insert_BY_PracticeID_AND_MasterCatalogSupplierID]
--				@PracticeID = 2,
--				@MasterCatalogSupplierID = 5,
--				@UserID = 0,
--				@PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID OUTPUT
--
--		SELECT	@PracticeCatalogSupplierBrandID as N'@PracticeCatalogSupplierBrandID'
--
--		SELECT	'Return Value' = @return_value
--		SELECT * FROM dbo.PracticeCatalogSupplierBrand 

-- jb 20070914 Added where clause to line 64

CREATE PROC [dbo].[usp_PracticeCatalogSupplierBrand_Insert_BY_PracticeID_AND_MasterCatalogSupplierID]
		(
			  @PracticeID						INT				--  2
			, @MasterCatalogSupplierID			INT				--  5
			, @UserID							INT   			--  0 for testing			
			, @PracticeCatalogSupplierBrandID	INT OUTPUT		--  should return 9 on test
		)
AS
BEGIN	
	DECLARE		@RecordCount		INT   --  Number ofDECLARE
	DECLARE		@SupplierName		VARCHAR(50)
	DECLARE		@SupplierShortName	VARCHAR(50)
	
		

		SELECT @RecordCount = COUNT(1) 
		FROM dbo.PracticeCatalogSupplierBrand 
		WHERE PracticeID = @PracticeID
		  AND MasterCatalogSupplierID = @MasterCatalogSupplierID
		
		IF (@RecordCount > 0)
		  BEGIN
				Select @PracticeCatalogSupplierBrandID = PracticeCatalogSupplierBrandID
				FROM dbo.PracticeCatalogSupplierBrand 
				WHERE PracticeID = @PracticeID
				    AND MasterCatalogSupplierID = @MasterCatalogSupplierID 
		  END
		  
		ELSE
		
		  BEGIN
			SELECT 
					  @SupplierName = SupplierName
					, @SupplierShortName = SupplierShortName
			FROM	dbo.MasterCatalogSupplier  	
			WHERE MasterCatalogSupplierID = @MasterCatalogSupplierID			
				
				
				
			Insert INTO dbo.PracticeCatalogSupplierBrand
					  (		PracticeID
						  , MasterCatalogSupplierID
	--					  , ThirdPartySupplier
						  , SupplierName
						  , SupplierShortName
						  , BrandName
						  , BrandShortName
						  , IsThirdPartySupplier
						  , CreatedUserID
						  , IsActive
					  )
			  VALUES
						(
							  @PracticeID
							, @MasterCatalogSupplierID
	--						, NULL
							, @SupplierName
							, @SupplierShortName
							, @SupplierName
							, @SupplierShortName
							, 0
							, @UserID
							, 1
						)
					
			Select @PracticeCatalogSupplierBrandID = SCOPE_IDENTITY()
		
		  END			
END			

