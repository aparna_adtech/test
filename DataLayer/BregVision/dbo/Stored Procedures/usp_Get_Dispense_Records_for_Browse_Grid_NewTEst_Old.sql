﻿
-- =============================================
-- Author:		Greg Ross
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- Modifications:  20071012 JB  Switch supplierID to PCP.practicecatalogsupplierbrandID.
--  Modifications:  20080219 JB Adjusted as this is the slowest query in the entire application.
-- =============================================
CREATE PROCEDURE [dbo].[usp_Get_Dispense_Records_for_Browse_Grid_NewTEst_Old] -- 3
		@PracticeLocationID			INT
	  , @PracticeID					INT = 0			--  For backward compatibility with application code, @PracticeID is never used.
	  , @SearchCriteria varchar(50)
	  , @SearchText  varchar(50)
AS
BEGIN


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	set @SearchText = @SearchText + '%'	
	if (@SearchCriteria='Browse' or @SearchCriteria='All' or @SearchCriteria='')
		begin
		SELECT 
			
			SUB.ProductInventoryID,			
			SUB.SupplierID,
			SUB.SupplierName,
			SUB.BrandName,
			SUB.ProductName,
			SUB.Code,
			--  SUB.IsThirdPartySupplier
			
			ISNULL(SUB.Side, '') as Side,
			ISNULL(SUB.Size, '') AS Size, 
			ISNULL(SUB.Gender, '') AS Gender, 
			ISNULL(SUB.QuantityOnHandPerSystem, 0) AS QuantityOnHand
			
		FROM
		(  
			SELECT
	    
			PI.ProductInventoryID, 
			
			
			--  20071012 JB  Swithch out.
			PCP.practicecatalogsupplierbrandID as SupplierID,
			--PCSB.MasterCatalogSupplierID as SupplierID,
			PCSB.SupplierName,
			PCSB.BrandName,
			MCP.Name as ProductName,
			MCP.Code,

			MCP.LeftRightSide as Side,
			MCP.Size, 
			MCP.Gender,
			PI.QuantityOnHandPerSystem		
			
			--  PCSB.IsThirdPartySupplier
			 from productInventory PI			WITH(NOLOCK)
			inner join practicecatalogproduct PCP WITH(NOLOCK)
				on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID   
			inner join practicecatalogsupplierbrand PCSB    WITH (NOLOCK)
				on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID   
			inner join MasterCatalogProduct MCP		WITH (NOLOCK)
				on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID    
			where PI.isactive =1 
			and PCP.isactive = 1
			and PCSB.isactive = 1
			and MCP.isactive = 1
			and PI.PracticeLocationID = @PracticeLocationID
			group by PCSB.BrandName,PCSB.SupplierName,MCP.Name,Code, 
							MCP.LeftRightSide,
							MCP.Size,
							MCP.Gender,
							 PI.ProductInventoryID
							, PCP.practicecatalogsupplierbrandID --PCSB.MasterCatalogSupplierID
							, PI.QuantityOnHandPerSystem	
		UNION
		
			  select 
					
					PI.ProductInventoryID, 				
					
					--  20071012 JB  Swithch out supplierID.
					PCP.practicecatalogsupplierbrandID as SupplierID,
					--PCSB.ThirdPartySupplierID as SupplierID,
					
					PCSB.SupplierName,
					PCSB.BrandName,
					TPP.Name as ProductName,
					TPP.Code,
					--  PCSB.IsThirdPartySupplier
					TPP.LeftRightSide as Side,
					TPP.Size,
					TPP.Gender,
					PI.QuantityOnHandPerSystem

			from productInventory PI    WITH (NOLOCK)
			inner join practicecatalogproduct PCP   WITH (NOLOCK)
				on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID   
			inner join practicecatalogsupplierbrand PCSB WITH (NOLOCK)
				on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID   
			inner join ThirdPartyProduct TPP WITH (NOLOCK)
				on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID   
			where PI.isactive =1 
			and PCP.isactive = 1
			and PCSB.isactive = 1
			and TPP.isactive = 1
			and PI.PracticeLocationID = @PracticeLocationID
			group by PCSB.BrandName,PCSB.SupplierName,TPP.Name, TPP.Code, TPP.LeftRightSide,
					TPP.Size, TPP.Gender, PI.ProductInventoryID, PCP.practicecatalogsupplierbrandID, PI.QuantityOnHandPerSystem --PCSB.ThirdPartySupplierID

		) AS Sub
		GROUP BY
			--  SUB.IsThirdPartySupplier,
			SUB.SupplierName,
			SUB.BrandName,
			SUB.ProductName,
			SUB.Code,
			SUB.ProductInventoryID,			
			SUB.SupplierID,
			ISNULL(SUB.Side, ''),
			ISNULL(SUB.Size, ''), 
			ISNULL(SUB.Gender, ''), 
			ISNULL(SUB.QuantityOnHandPerSystem, 0) 
		ORDER BY
			SUB.SupplierName,
			SUB.BrandName,
			SUB.ProductName,
			SUB.Code
	end 
else if @SearchCriteria='Name'	-- ProductName
	begin
		-- When searching for a product name, search with wildcard on both sides.
		SET @SearchText = '%' + @SearchText
			SELECT 
			
			SUB.ProductInventoryID,			
			SUB.SupplierID,
			SUB.SupplierName,
			SUB.BrandName,
			SUB.ProductName,
			SUB.Code,
			--  SUB.IsThirdPartySupplier
			
			ISNULL(SUB.Side, '') as Side,
			ISNULL(SUB.Size, '') AS Size, 
			ISNULL(SUB.Gender, '') AS Gender, 
			ISNULL(SUB.QuantityOnHandPerSystem, 0) AS QuantityOnHand
			
		FROM
		(  
			SELECT
	    
			PI.ProductInventoryID, 
			
			
			--  20071012 JB  Swithch out.
			PCP.practicecatalogsupplierbrandID as SupplierID,
			--PCSB.MasterCatalogSupplierID as SupplierID,
			PCSB.SupplierName,
			PCSB.BrandName,
			MCP.Name as ProductName,
			MCP.Code,

			MCP.LeftRightSide as Side,
			MCP.Size, 
			MCP.Gender,
			PI.QuantityOnHandPerSystem		
			
			--  PCSB.IsThirdPartySupplier
			 from productInventory PI   WITH (NOLOCK)
			inner join practicecatalogproduct PCP    WITH (NOLOCK)
				on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
			inner join practicecatalogsupplierbrand PCSB    WITH (NOLOCK)
				 on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID   
			inner join MasterCatalogProduct MCP    WITH (NOLOCK)
				 on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID    
			where PI.isactive =1 
			and PCP.isactive = 1
			and PCSB.isactive = 1
			and MCP.isactive = 1
			and PI.PracticeLocationID = @PracticeLocationID
			and (MCP.Name like @SearchText or MCP.ShortName like  @SearchText)
			group by PCSB.BrandName,PCSB.SupplierName,MCP.Name,Code, 
							MCP.LeftRightSide,
							MCP.Size,
							MCP.Gender,
							 PI.ProductInventoryID
							, PCP.practicecatalogsupplierbrandID --PCSB.MasterCatalogSupplierID
							, PI.QuantityOnHandPerSystem	
		UNION
		
			  select 
					
					PI.ProductInventoryID, 				
					
					--  20071012 JB  Swithch out supplierID.
					PCP.practicecatalogsupplierbrandID as SupplierID,
					--PCSB.ThirdPartySupplierID as SupplierID,
					
					PCSB.SupplierName,
					PCSB.BrandName,
					TPP.Name as ProductName,
					TPP.Code,
					--  PCSB.IsThirdPartySupplier
					TPP.LeftRightSide as Side,
					TPP.Size,
					TPP.Gender,
					PI.QuantityOnHandPerSystem

			from productInventory PI     WITH (NOLOCK)
			inner join practicecatalogproduct PCP WITH (NOLOCK)	
				on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID     
			inner join practicecatalogsupplierbrand PCSB WITH (NOLOCK)	
				on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID    
			inner join ThirdPartyProduct TPP WITH (NOLOCK)
				on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID    
			where PI.isactive =1 
			and PCP.isactive = 1
			and PCSB.isactive = 1
			and TPP.isactive = 1
			and PI.PracticeLocationID = @PracticeLocationID
			and (TPP.Name like @SearchText or TPP.ShortName like  @SearchText)
			group by PCSB.BrandName,PCSB.SupplierName,TPP.Name, TPP.Code, TPP.LeftRightSide,
					TPP.Size, TPP.Gender, PI.ProductInventoryID, PCP.practicecatalogsupplierbrandID, PI.QuantityOnHandPerSystem --PCSB.ThirdPartySupplierID

		) AS Sub
		GROUP BY
			--  SUB.IsThirdPartySupplier,
			SUB.SupplierName,
			SUB.BrandName,
			SUB.ProductName,
			SUB.Code,
			SUB.ProductInventoryID,			
			SUB.SupplierID,
			ISNULL(SUB.Side, ''),
			ISNULL(SUB.Size, ''), 
			ISNULL(SUB.Gender, ''), 
			ISNULL(SUB.QuantityOnHandPerSystem, 0) 
		ORDER BY
			SUB.SupplierName,
			SUB.BrandName,
			SUB.ProductName,
			SUB.Code
end 
else if @SearchCriteria='Code'
begin
			SELECT 
			
			SUB.ProductInventoryID,			
			SUB.SupplierID,
			SUB.SupplierName,
			SUB.BrandName,
			SUB.ProductName,
			SUB.Code,
			--  SUB.IsThirdPartySupplier
			
			ISNULL(SUB.Side, '') as Side,
			ISNULL(SUB.Size, '') AS Size, 
			ISNULL(SUB.Gender, '') AS Gender, 
			ISNULL(SUB.QuantityOnHandPerSystem, 0) AS QuantityOnHand
			
		FROM
		(  
			SELECT
	    
			PI.ProductInventoryID, 
			
			
			--  20071012 JB  Swithch out.
			PCP.practicecatalogsupplierbrandID as SupplierID,
			--PCSB.MasterCatalogSupplierID as SupplierID,
			PCSB.SupplierName,
			PCSB.BrandName,
			MCP.Name as ProductName,
			MCP.Code,

			MCP.LeftRightSide as Side,
			MCP.Size, 
			MCP.Gender,
			PI.QuantityOnHandPerSystem		
			
			--  PCSB.IsThirdPartySupplier
			 from productInventory PI	WITH (NOLOCK)
			inner join practicecatalogproduct PCP     WITH (NOLOCK)
				on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID   
			inner join practicecatalogsupplierbrand PCSB   WITH (NOLOCK)
				on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID   
			inner join MasterCatalogProduct MCP    WITH (NOLOCK)
				on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID    
			where PI.isactive =1 
			and PCP.isactive = 1
			and PCSB.isactive = 1
			and MCP.isactive = 1
			and PI.PracticeLocationID = @PracticeLocationID
			and MCP.Code like @SearchText
			group by PCSB.BrandName,PCSB.SupplierName,MCP.Name,Code, 
							MCP.LeftRightSide,
							MCP.Size,
							MCP.Gender,
							 PI.ProductInventoryID
							, PCP.practicecatalogsupplierbrandID --PCSB.MasterCatalogSupplierID
							, PI.QuantityOnHandPerSystem	
		UNION
		
			  select 
					
					PI.ProductInventoryID, 				
					
					--  20071012 JB  Swithch out supplierID.
					PCP.practicecatalogsupplierbrandID as SupplierID,
					--PCSB.ThirdPartySupplierID as SupplierID,
					
					PCSB.SupplierName,
					PCSB.BrandName,
					TPP.Name as ProductName,
					TPP.Code,
					--  PCSB.IsThirdPartySupplier
					TPP.LeftRightSide as Side,
					TPP.Size,
					TPP.Gender,
					PI.QuantityOnHandPerSystem

			from productInventory PI WITH (NOLOCK)
			inner join practicecatalogproduct PCP    WITH (NOLOCK)
				on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID    
			inner join practicecatalogsupplierbrand PCSB    WITH (NOLOCK)
				on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID   
			inner join ThirdPartyProduct TPP    WITH (NOLOCK)
				on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID   
			where PI.isactive =1 
			and PCP.isactive = 1
			and PCSB.isactive = 1
			and TPP.isactive = 1
			and PI.PracticeLocationID = @PracticeLocationID
			and TPP.Code like @SearchText
			group by PCSB.BrandName,PCSB.SupplierName,TPP.Name, TPP.Code, TPP.LeftRightSide,
					TPP.Size, TPP.Gender, PI.ProductInventoryID, PCP.practicecatalogsupplierbrandID, PI.QuantityOnHandPerSystem --PCSB.ThirdPartySupplierID

		) AS Sub
		GROUP BY
			--  SUB.IsThirdPartySupplier,
			SUB.SupplierName,
			SUB.BrandName,
			SUB.ProductName,
			SUB.Code,
			SUB.ProductInventoryID,			
			SUB.SupplierID,
			ISNULL(SUB.Side, ''),
			ISNULL(SUB.Size, ''), 
			ISNULL(SUB.Gender, ''), 
			ISNULL(SUB.QuantityOnHandPerSystem, 0) 
		ORDER BY
			SUB.SupplierName,
			SUB.BrandName,
			SUB.ProductName,
			SUB.Code
end                
else if @SearchCriteria='Brand'
begin
			SELECT 
			
			SUB.ProductInventoryID,			
			SUB.SupplierID,
			SUB.SupplierName,
			SUB.BrandName,
			SUB.ProductName,
			SUB.Code,
			--  SUB.IsThirdPartySupplier
			
			ISNULL(SUB.Side, '') as Side,
			ISNULL(SUB.Size, '') AS Size, 
			ISNULL(SUB.Gender, '') AS Gender, 
			ISNULL(SUB.QuantityOnHandPerSystem, 0) AS QuantityOnHand
			
		FROM
		(  
			SELECT
	    
			PI.ProductInventoryID, 
			
			
			--  20071012 JB  Swithch out.
			PCP.practicecatalogsupplierbrandID as SupplierID,
			--PCSB.MasterCatalogSupplierID as SupplierID,
			PCSB.SupplierName,
			PCSB.BrandName,
			MCP.Name as ProductName,
			MCP.Code,

			MCP.LeftRightSide as Side,
			MCP.Size, 
			MCP.Gender,
			PI.QuantityOnHandPerSystem		
			
			--  PCSB.IsThirdPartySupplier
			 from productInventory PI WITH (NOLOCK)
			inner join practicecatalogproduct PCP   WITH (NOLOCK)
				on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
			inner join practicecatalogsupplierbrand PCSB   WITH (NOLOCK)
				on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID   
			inner join MasterCatalogProduct MCP     WITH (NOLOCK)
				on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
			where PI.isactive =1 
			and PCP.isactive = 1
			and PCSB.isactive = 1
			and MCP.isactive = 1
			and PI.PracticeLocationID = @PracticeLocationID
			AND (PCSB.BrandName LIKE @SearchText OR PCSB.BrandShortName LIKE @SearchText)
			group by PCSB.BrandName,PCSB.SupplierName,MCP.Name,Code, 
							MCP.LeftRightSide,
							MCP.Size, 
							MCP.Gender,
							PI.ProductInventoryID
							, PCP.practicecatalogsupplierbrandID --PCSB.MasterCatalogSupplierID
							, PI.QuantityOnHandPerSystem	
		UNION
		
			  select 
					
					PI.ProductInventoryID, 				
					
					--  20071012 JB  Swithch out supplierID.
					PCP.practicecatalogsupplierbrandID as SupplierID,
					--PCSB.ThirdPartySupplierID as SupplierID,
					
					PCSB.SupplierName,
					PCSB.BrandName,
					TPP.Name as ProductName,
					TPP.Code,
					--  PCSB.IsThirdPartySupplier
					TPP.LeftRightSide as Side,
					TPP.Size,
					TPP.Gender,
					PI.QuantityOnHandPerSystem

			from productInventory PI WITH (NOLOCK)
			inner join practicecatalogproduct PCP            WITH (NOLOCK)
				on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID   
			inner join practicecatalogsupplierbrand PCSB     WITH (NOLOCK)
				on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID  
			inner join ThirdPartyProduct TPP                 WITH (NOLOCK)
				on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID   
			where PI.isactive =1 
			and PCP.isactive = 1
			and PCSB.isactive = 1
			and TPP.isactive = 1
			and PI.PracticeLocationID = @PracticeLocationID
			AND ( 
					(PCSB.BrandName LIKE @SearchText OR PCSB.BrandShortName LIKE @SearchText)
				OR
					(PCSB.SupplierName LIKE @SearchText OR PCSB.SupplierShortName LIKE @SearchText)
				)			


			group by PCSB.BrandName,PCSB.SupplierName,TPP.Name, TPP.Code, TPP.LeftRightSide,
					TPP.Size, TPP.Gender, PI.ProductInventoryID, PCP.practicecatalogsupplierbrandID, PI.QuantityOnHandPerSystem --PCSB.ThirdPartySupplierID

		) AS Sub
		GROUP BY
			--  SUB.IsThirdPartySupplier,
			SUB.SupplierName,
			SUB.BrandName,
			SUB.ProductName,
			SUB.Code,
			SUB.ProductInventoryID,			
			SUB.SupplierID,
			ISNULL(SUB.Side, ''),
			ISNULL(SUB.Size, ''), 
			ISNULL(SUB.Gender, ''), 
			ISNULL(SUB.QuantityOnHandPerSystem, 0) 
		ORDER BY
			SUB.SupplierName,
			SUB.BrandName,
			SUB.ProductName,
			SUB.Code
end

else if @SearchCriteria='HCPCs'
begin
			SELECT 
			
			SUB.ProductInventoryID,			
			SUB.SupplierID,
			SUB.SupplierName,
			SUB.BrandName,
			SUB.ProductName,
			SUB.Code,
			--  SUB.IsThirdPartySupplier
			
			ISNULL(SUB.Side, '') as Side,
			ISNULL(SUB.Size, '') AS Size, 
			ISNULL(SUB.Gender, '') AS Gender, 
			ISNULL(SUB.QuantityOnHandPerSystem, 0) AS QuantityOnHand
			
		FROM
		(  
			SELECT
	    
			PI.ProductInventoryID, 
			PCP.practicecatalogsupplierbrandID as SupplierID,

			PCSB.SupplierName,
			PCSB.BrandName,
			MCP.Name as ProductName,
			MCP.Code,

			MCP.LeftRightSide as Side,
			MCP.Size, 
			MCP.Gender,
			PI.QuantityOnHandPerSystem
			--dbo.ConcatHCPCS(PI.PracticeCatalogProductID) AS HCPCSString 		
			
			--  PCSB.IsThirdPartySupplier
			 from productInventory PI      WITH (NOLOCK)
			inner join practicecatalogproduct PCP WITH (NOLOCK)
				on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
			inner join practicecatalogsupplierbrand PCSB WITH (NOLOCK)
				on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID
			inner join MasterCatalogProduct MCP WITH (NOLOCK)
				on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
			where PI.isactive =1 
			and PCP.isactive = 1
			and PCSB.isactive = 1
			and MCP.isactive = 1
			and PI.PracticeLocationID = @PracticeLocationID
			and dbo.ConcatHCPCS(PI.PracticeCatalogProductID) like @SearchText
			group by PCSB.BrandName,PCSB.SupplierName,MCP.Name,Code, 
							MCP.LeftRightSide,
							MCP.Size,
							MCP.Gender,
							 PI.ProductInventoryID
							, PCP.practicecatalogsupplierbrandID --PCSB.MasterCatalogSupplierID
							, PI.QuantityOnHandPerSystem
							,PI.PracticeCatalogProductID	

		UNION
		
			  select 
					
					PI.ProductInventoryID, 				
					
					--  20071012 JB  Swithch out supplierID.
					PCP.practicecatalogsupplierbrandID as SupplierID,
					--PCSB.ThirdPartySupplierID as SupplierID,
					
					PCSB.SupplierName,
					PCSB.BrandName,
					TPP.Name as ProductName,
					TPP.Code,
					--  PCSB.IsThirdPartySupplier
					TPP.LeftRightSide as Side,
					TPP.Size,
					TPP.Gender,
					PI.QuantityOnHandPerSystem

			from productInventory PI 
			inner join practicecatalogproduct PCP WITH (NOLOCK)
				on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
			inner join practicecatalogsupplierbrand PCSB WITH (NOLOCK)
				on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID
			inner join ThirdPartyProduct TPP WITH (NOLOCK)
				on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
			where PI.isactive =1 
			and PCP.isactive = 1
			and PCSB.isactive = 1
			and TPP.isactive = 1
			and PI.PracticeLocationID = @PracticeLocationID
			and dbo.ConcatHCPCS(PI.PracticeCatalogProductID) like @SearchText

			group by PCSB.BrandName,PCSB.SupplierName,TPP.Name, TPP.Code, TPP.LeftRightSide,
					TPP.Size, TPP.Gender, PI.ProductInventoryID, PCP.practicecatalogsupplierbrandID, PI.QuantityOnHandPerSystem,PI.PracticeCatalogProductID --PCSB.ThirdPartySupplierID

		) AS Sub
		GROUP BY
			--  SUB.IsThirdPartySupplier,
			SUB.SupplierName,
			SUB.BrandName,
			SUB.ProductName,
			SUB.Code,
			SUB.ProductInventoryID,			
			SUB.SupplierID,
			ISNULL(SUB.Side, ''),
			ISNULL(SUB.Size, ''), 
			ISNULL(SUB.Gender, ''), 
			ISNULL(SUB.QuantityOnHandPerSystem, 0) 
		ORDER BY
			SUB.SupplierName,
			SUB.BrandName,
			SUB.ProductName,
			SUB.Code
end 

END

