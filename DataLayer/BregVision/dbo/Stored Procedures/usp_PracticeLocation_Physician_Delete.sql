﻿CREATE PROCEDURE [dbo].[usp_PracticeLocation_Physician_Delete]
		  @PracticeLocationID INT
		, @PhysicianID		  INT
		, @ModifiedUserID	  INT
AS
BEGIN	
	
	UPDATE dbo.PracticeLocation_Physician 
	
	SET 
		  ModifiedUserID = @ModifiedUserID
		, ModifiedDate = GetDate()
		, IsActive = 0
	
	WHERE	
		  PracticeLocationID = @PracticeLocationID 
		  AND PhysicianID = @PhysicianID		
		
END



