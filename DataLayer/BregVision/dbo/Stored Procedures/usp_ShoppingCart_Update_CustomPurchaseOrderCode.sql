﻿
CREATE PROC [dbo].[usp_ShoppingCart_Update_CustomPurchaseOrderCode]
	  @ShoppingCartID INT
	, @CustomPurchaseOrderCode VARCHAR(75)
AS
BEGIN

	UPDATE ShoppingCart
	SET CustomPurchaseOrderCode = @CustomPurchaseOrderCode
	WHERE ShoppingCartID = @ShoppingCartID

END
