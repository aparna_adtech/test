﻿
/*
-- =============================================
-- Author:		Mike Sneen	
-- Create date: 06/14/10
-- Description:	Used to get the parent detail for a practice. Used in MCToPC.aspx
-- =============================================
--	MWS I started to put paging in here but didn't finish because I don't know the gains are worth it.

	EXEC [usp_GetPracticeCatalogSupplierBrandsPaged] 1,'browse','',15,1
*/
Create PROCEDURE [dbo].[usp_GetPracticeCatalogSupplierBrandsPaged] 
	-- Add the parameters for the stored procedure here
		@PracticeID int,
		@SearchCriteria varchar(50),
		@SearchText  varchar(50),
		@PageSize int,
		@PageNumber int, 
		@TotalRecords int OUTPUT
AS
BEGIN

		/*These variables are used in conjunction with the coalesce statements in the where clause.
		each of these variables will be null except one is @SearchCriteria matches.*/
		Declare @ProductName varchar(50)			--NULL   These default to null
		Declare @ProductCode varchar(50)			--NULL   These default to null
		Declare @ProductBrand varchar(50)			--NULL   These default to null
		Declare @ProductHCPCs varchar(50)			--NULL   These default to null

		if @SearchCriteria='Name'					-- ProductName
			Set @ProductName = '%' + @SearchText + '%'		--put the percent sign on both sides of the search text for product name
		else if @SearchCriteria='Code'
			Set @ProductCode = '%' +  @SearchText + '%'	
		else if @SearchCriteria='Brand'
			Set @ProductBrand = @SearchText + '%'	
		else if @SearchCriteria='HCPCs'
			Set @ProductHCPCs = @SearchText + '%'
		

		SELECT  
				  PCSB.PracticeCatalogSupplierBrandID
				, PCSB.SupplierShortName	AS SupplierName
				, PCSB.BrandShortName		AS BrandName
				, PCSB.IsThirdPartySupplier AS IsThirdPartySupplier
				, ISNULL(PCSB.Sequence, 999) AS DerivedSequence -- JB
				, ProductCount = 
				(
					SELECT COUNT( vwPC.PracticecatalogProductID ) -- Region Product Count
					FROM vwPracticeCatalog vwPC 
					WHERE
						vwPC.PracticeID = @PracticeID
						AND           
						vwPC.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
						AND
						(
							--ProductName || ShortName
							COALESCE(vwPC.ProductName, '') like COALESCE(@ProductName, vwPC.ProductName, '')
							OR
							COALESCE(vwPC.ShortName, '') like COALESCE(@ProductName, vwPC.ShortName, '')
						)
						AND
						(
							COALESCE(vwPC.Code, '') like COALESCE(@ProductCode , vwPC.Code, '')
						)
						AND
						(
							COALESCE(vwPC.BrandName, '') like COALESCE(@ProductBrand, vwPC.BrandName, '')
							OR
							COALESCE(vwPC.BrandShortName, '') like COALESCE(@ProductBrand, vwPC.BrandShortName, '')
						)
						AND (   /* use this type of statement for Child tables of the target to avoid duplicate rows.
									If we use a join for this, we would return duplicate rows, so we just check the existence
									of one record that matches the search parameters in the child table
									*/
									(
										 (@ProductHCPCs IS NULL)
										 OR EXISTS
										 (  
											Select 
												1 
											 from 
												ProductHCPCS PHCPCS 
											 where 
												PHCPCS.PracticeCatalogProductID = vwPC.PracticeCatalogProductID 
												and PHCPCS.HCPCS like @ProductHCPCs  
										  )
									 )
							)
				
				)
			
        FROM 
			PracticeCatalogSupplierBrand AS PCSB WITH (NOLOCK)
		WHERE 

			PCSB.PracticeID = @PracticeID
			AND PCSB.IsActive = 1	
					
			AND EXISTS
			(
				Select
				1
				from
				PracticeCatalogProduct PCP
				Left Outer join ThirdPartyProduct AS TPP  WITH(NOLOCK) 
					on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
				where
				PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
				AND PCP.IsActive = 1
				AND ISNULL(TPP.IsActive, 1) = 1
			)
			
		ORDER BY 
			  PCSB.IsThirdPartySupplier
			, ISNULL(PCSB.Sequence, 999)  -- JB
			, PCSB.SupplierShortName
			, PCSB.BrandShortName 		


END





