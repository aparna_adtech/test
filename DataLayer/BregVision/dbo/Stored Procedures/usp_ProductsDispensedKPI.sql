﻿-- =============================================
-- Author:		Greg Ross
-- Create date: 11/28/07
-- Description:	Gets last 7 and last 30 days of cost for items dispensed
-- =============================================
CREATE PROCEDURE [dbo].[usp_ProductsDispensedKPI]
	@PracticeLocationID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @ProductDispensed Table
    (
	  Last7Days decimal default 0,
	  Last30Days decimal default 0		
    )

INSERT  INTO @ProductDispensed
        (
          Last7Days
          --, QuantityNotFullyCheckin
        )
select 
	ISNULL(sum(ActualWHolesaleCost), 0) AS ActualWHolesaleCost
from productinventory_DispenseDetail PIDD
inner join productInventory PI
	on PIDD.ProductInventoryID = PI.ProductInventoryID
where 
	practicelocationid=@PracticeLocationID
	and PIDD.CreatedDate > dateadd(wk,-1,getdate())

	update @ProductDispensed 
	set Last30Days = Positive.Last30Days
	from
	(
		select ISNULL(sum(ActualWHolesaleCost), 0) as Last30Days
		from productinventory_DispenseDetail PIDD
		inner join productInventory PI
			on PIDD.ProductInventoryID = PI.ProductInventoryID
		where practicelocationid=@PracticeLocationID
			and PIDD.CreatedDate > dateadd(m,-1,getdate())
	) as Positive

select * from @ProductDispensed
 
END
