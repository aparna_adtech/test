﻿

/* ------------------------------------------------------------
   PROCEDURE:    [dbo].[usp_GetAllInventoryDataforPCToIventory_ByProductName_With_ThirdPartyProducts]
   
   Description:  Selects products (mc and 3rdParty) that are in inventory given a product name
					or partial product name to search, also given a practiceID
					and IsActice of 0 or 1 (false or true)
					This is called from the proc:
						usp_GetAllInventoryDataforPCToIventory						
   
   AUTHOR:       John Bongiorni 10/9/2007 2:36:40 PM
   
   Modifications:  2007.11.06  John Bongiorni
						Modify to work similar to ..._All_With_ThirdPartyProducts.
						20080123 JB Add SideSize Combo.
					2008.01.31 JB 
						--  User the new column IsSetToActive to correspond to the PIProduct Grid Search.
						--  change AND PI.IsActive = @IsActive PI.IsActive = to = 1 default PI.IsActive to 1.	
					2008.02.04 JB 	
					--  Add space if null: ISNULL(SUB.LeftRightSide, ' ') + ', ' + ISNULL(SUB.Size, ' ')
					--  Add column IsDeletable set to true when	the QOH, Par, Reorder, Critical levels are zero.
					--  Add column to pull the ProductInventoryID!	
					2008.02.05 JB
					--  Add column IsNotFlaggedForReorder for checkbox in PI grid in PL_InventoryLevels page.																									
   ------------------------------------------------------------ */   


CREATE PROCEDURE [dbo].[usp_GetAllInventoryDataforPCToIventory_ByProductName_With_ThirdPartyProducts]

	@PracticeLocationID int
	, @SearchText  varchar(50)
	, @IsActive bit

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--   NOCOUNT ON
	
			DECLARE @IsSetToActive BIT --  This is now being used in place to indicate a product that has been set
										--  in the system to be reordered.
			SET @IsSetToActive = @IsActive


			SELECT 
				  Sub.ProductInventoryID
				
				, Sub.SupplierShortName		--  2007.11.06  JB
				, Sub.BrandShortName		--  2007.11.06  JB
				
				, SUB.PracticeCatalogProductID				
				, SUB.SupplierID
				, SUB.Category
				, SUB.Product
				, SUB.Code
				, SUB.Packaging
				
				, ISNULL(SUB.LeftRightSide, ' ') + ', ' + ISNULL(SUB.Size, ' ') AS SideSize
				
				, SUB.LeftRightSide
				, SUB.Size
				, SUB.Gender
				, SUB.QOH
				, SUB.ParLevel
				, SUB.ReorderLevel
				, SUB.CriticalLevel
				
				, SUB.IsNotFlaggedForReorder
				
				--  2008.02.04   JB
				--  If the QOH, Par, Reorder, Critical quantities are zero then
				--    set isDeletable to true.
				, CASE (SUB.QOH + SUB.ParLevel + SUB.ReorderLevel + SUB.CriticalLevel)
					WHEN 0 THEN 1 ELSE 0 END AS IsDeletable				
				
				, SUB.WholesaleCost
				, SUB.IsActive
				, SUB.IsSetToActive			--  2008.01.31 JB Change to IsSetToActive
				, SUB.IsThirdPartyProduct
				, SUB.IsLogoAblePart
				, SUB.McpIsLogoAblePart
				, SUB.IsConsignment
				, SUB.ConsignmentQuantity
											
			FROM
			(
					SELECT	-- 249 active and inactive mastercatalogproducts in inventory WORKS!!!
 
							PI.ProductInventoryID,
							
							PCSB.SupplierShortName,		--  2007.11.06  JB
							PCSB.BrandShortName,		--  2007.11.06  JB
							
							PCSB.PracticeCatalogSupplierBrandID AS SupplierID,	--  2007.11.06  JB
							PCP.PracticeCatalogProductID,						--  2007.11.06  JB

                            --  PCSB.MasterCatalogSupplierID AS SupplierID,		--  2007.11.06  JB
                            
                            MCC.[NAME] AS Category,								--  2007.11.06  JB
                            
                            --  PCSB.BrandShortName AS Category,   --  This column should be unneccessary
																--  Only here to preserve the existing application code.
						    MCP.[Name]			AS Product,
                            
                            MCP.[Code]			AS Code,
                            MCP.[Packaging]		AS Packaging,
                            MCP.[LeftRightSide] AS LeftRightSide,
                            MCP.[Size]			AS Size,
                            MCP.[Gender]		AS Gender,
							
							PI.QuantityOnHandPerSystem AS QOH,
							PI.ParLevel,
							PI.ReorderLevel,
							PI.CriticalLevel,
						
							PI.IsNotFlaggedForReorder,
							
							PCP.WholesaleCost,
							PI.Isactive,
							PI.IsSetToActive,
							0 AS IsThirdPartyProduct,
							PCP.IsLogoAblePart,
							dbo.isPcpItemLogoAblePart(PCP.PracticeCatalogProductID) AS McpIsLogoAblePart,
							PI.IsConsignment,
							PI.ConsignmentQuantity
                     
                    FROM dbo.PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
							                    
                    INNER JOIN [PracticeCatalogProduct] AS PCP WITH(NOLOCK) 
						ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
							
                    INNER JOIN [ProductInventory] AS PI  WITH(NOLOCK)
							ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
             
                    INNER JOIN [MasterCatalogProduct] AS MCP  WITH(NOLOCK)
							ON PCP.[MasterCatalogProductID] = MCP.[MasterCatalogProductID]
                      
                    							
					-- the following two inner join can be removed later
					-- Needed now for the MCC.Name column.		
					INNER JOIN dbo.MasterCatalogsubCategory AS MCSC WITH(NOLOCK)
							ON MCP.MasterCatalogsubCategoryID = MCSC.MasterCatalogsubCategoryID

					INNER JOIN dbo.MasterCatalogCategory AS MCC WITH(NOLOCK)
							ON MCSC.MasterCatalogCategoryID = MCC.MasterCatalogCategoryID
							  
                                                 
                     WHERE  PI.[PracticeLocationID] = @PracticeLocationID
							AND PCP.IsThirdPartyProduct = 0  -- MasterCatalogProductsOnly
							AND PCSB.IsActive =	1
							AND PCP.IsActive = 1
							AND MCP.IsActive = 1
							
							AND PI.IsSetToActive = @IsSetToActive --  2008.01.31 JB Change to IsSetToActive
							AND PI.IsActive = 1			--  Later change this to = 1 default PI.IsActive to 1.

							AND (MCP.Name LIKE @SearchText OR MCP.ShortName LIKE  @SearchText)
					

				UNION

					--  Third Party Product Info
					SELECT		-- 249 active and inactive mastercatalogproducts in inventory WORKS!!!
 
							PI.ProductInventoryID,
							
							PCSB.SupplierShortName, 
							PCSB.BrandShortName, 
							
							PCSB.PracticeCatalogSupplierBrandID AS SupplierID, 
				
							PCP.PracticeCatalogProductID,		       
                            --  PCSB.ThirdPartySupplierID AS OldSupplierID,                        
                            'NA' AS Category,
                            --PCSB.BrandShortName AS Category,   --  This column should be unneccessary
																--  Only here to preserve the existing application code.
						    
 
					--
--							  PCP.PracticeCatalogProductID,		       
--                            PCSB.ThirdPartySupplierID AS SupplierID,                        
--                            PCSB.BrandShortName AS Category,   --  This column should be unneccessary
																--  Only here to preserve the existing application code.
						    TPP.[Name]			AS Product,                            
                            TPP.[Code]			AS Code,
                            TPP.[Packaging]		AS Packaging,
                            TPP.[LeftRightSide] AS LeftRightSide,
                            TPP.[Size]			AS Size,
                            TPP.[Gender]		AS Gender,
							
							PI.QuantityOnHandPerSystem AS QOH,
							PI.ParLevel,
							PI.ReorderLevel,
							PI.CriticalLevel,
						
							PI.IsNotFlaggedForReorder,
							
							PCP.WholesaleCost,						
							PI.Isactive,	
							PI.IsSetToActive,						
							1 AS IsThirdPartyProduct,
							0 AS IsLogoAblePart,
							0 AS McpIsLogoAblePart,
							PI.IsConsignment,
							PI.ConsignmentQuantity 
                     
                    FROM dbo.PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
                    
                    INNER JOIN [PracticeCatalogProduct] AS PCP WITH(NOLOCK) 
						ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
							
                    INNER JOIN [ProductInventory] AS PI  WITH(NOLOCK)
							ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
             
                    INNER JOIN [ThirdPartyProduct] AS TPP  WITH(NOLOCK)
							ON PCP.[ThirdPartyProductID] = TPP.[ThirdPartyProductID]
                                                 
                     WHERE  PI.[PracticeLocationID] = @PracticeLocationID
							AND PCP.IsThirdPartyProduct = 1		-- ThirdPartyProductsOnly 
							AND PCSB.IsActive =	1
							AND PCP.IsActive = 1
							AND TPP.IsActive = 1
							
							AND PI.IsSetToActive = @IsSetToActive --  2008.01.31 JB Change to IsSetToActive
							AND PI.IsActive = 1			--  Later change this to = 1 default PI.IsActive to 1.

							AND (TPP.Name LIKE @SearchText OR TPP.ShortName LIKE  @SearchText)
										
				) AS Sub
			
			ORDER BY Sub.IsThirdPartyProduct
			
END