﻿
--Create PROCEDURE [dbo].[usp_PracticeLocation_Physician_Select_Top10_Names_By_PracticeLocationID]
Create PROCEDURE [dbo].[usp_PracticeLocation_Physician_Select_Top10_Names_By_PracticeLocationID]
  @PracticeLocationID INT
AS
BEGIN 
Set NoCount on

SELECT IDENTITY(int,1,1) AS ID, Phy.PhysicianName
INTO #TEMP
FROM (
SELECT Top 10
	RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix) AS PhysicianName
	 
	 FROM  
	  dbo.Contact AS C
	 INNER JOIN 
	  dbo.Physician AS PH ON C.ContactID = PH.ContactID
	 INNER JOIN 
	  dbo.PracticeLocation_Physician AS PLP ON PH.PhysicianID = PLP.PhysicianID
	 WHERE
	  PLP.PracticeLocationID = @PracticeLocationID
	  AND PH.IsActive = 1
	  AND C.IsActive = 1
	  AND PLP.IsActive = 1
	  ) 
	  	  
	  Phy --WHERE Phy.ID % 2 = 1
			--ORDER BY Phy.ID
			--DROP TABLE #TEMP
	  
	 Select T1.PhysicianName, T2.PhysicianName as PhysicianName2 from #TEMP T1 
		LEFT OUTER JOIN #TEMP T2 ON (T1.ID+1 = T2.ID or T2.ID IS NULL)
		WHERE T1.ID % 2 = 1
			ORDER BY T1.ID
			
	 DROP TABLE #TEMP
END

--exec usp_PracticeLocation_Physician_Select_Top10_Names_By_PracticeLocationID 3

