﻿
-- =============================================
--EXEC usp_User_Delete 1, 23
-- Author:		<Author: Mike Sneen>
-- Create date: <Create Date: 04/02/09>
-- Description:	<Description: Used to update Default User Preferences>
-- modification:
--  [dbo].[usp_User_Delete]  

-- =============================================
Create PROCEDURE [dbo].[usp_User_Delete]  --3 --10  --  _With_Brands
	@PracticeID int,
	@UserID int
AS 
    BEGIN
    DECLARE @Err INT
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
--        SET NOCOUNT ON ;
     
     Update [User]
     SET
		IsActive = 0
	WHERE
		UserID = @UserID
		AND PracticeID = @PracticeID
		
     

     
     	SET @Err = @@ERROR

	RETURN @Err

    END

--Exec usp_SuperbillCategory_Update 3, 3