﻿-- =============================================
-- Author:		Greg Ross
-- Create date: 11/28/07
-- Description:	Gets last 7 and last 30 days of cost for items CheckedIn
-- =============================================
CREATE PROCEDURE [dbo].[usp_ProductsCheckedInKPI]
	@PracticeLocationID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @ProductDispensed Table
    (
	  Last7Days decimal default 0,
	  Last30Days decimal default 0		
    )

INSERT  INTO @ProductDispensed
        (
          Last7Days
          --, QuantityNotFullyCheckin
        )
select 
	ISNULL(sum(ActualWHolesaleCost), 0)  AS  ActualWHolesaleCost
from productinventory_OrderCheckIn PIOCI
inner join productInventory PI
	on PIOCI.ProductInventoryID = PI.ProductInventoryID
where practicelocationid=@PracticeLocationID
and PIOCI.CreatedDate > dateadd(wk,-1,getdate())

update @ProductDispensed 
	set Last30Days = Positive.Last30Days
	from
	(select ISNULL(sum(ActualWHolesaleCost), 0) as Last30Days
	from productinventory_OrderCheckIn PIOCI
	inner join productInventory PI
	on PIOCI.ProductInventoryID = PI.ProductInventoryID
	where practicelocationid=@PracticeLocationID
	and PIOCI.CreatedDate > dateadd(m,-1,getdate())
	) as Positive

select * from @ProductDispensed
 
END
