﻿
/* ------------------------------------------------------------
   PROCEDURE:    [dbo].[usp_PracticeLocation_Select_All_ListNames_By_UserID]
   
   Description:  Selects all PracticeLocation Names and IDs for a given userID.
				Select all names and ids from the table dbo.PracticeLocation
				given the UserID.
   
   AUTHOR:       John Bongiorni 10/21/2007 15:22:42 
   
   Modifications:  
SELECT * FROM PRACTICELOCATION_uSER INNER JOIN [uSER] ON [USER].USERid = PRACTICELOCATION_USER.USERid
ORDER BY [USER].USERID

   ------------------------------------------------------------ */  

CREATE PROCEDURE [dbo].[usp_PracticeLocation_Select_All_ListNames_By_UserID] --20 -- wellington / Fusion.01  -- OrthoSelect
			( @UserID INT )
AS 
    BEGIN
        DECLARE @Err INT
        SELECT
        
            PL.PracticeLocationID
          , PL.Name AS PracticeLocationName
          , PL.IsPrimaryLocation
        
        FROM
            dbo.PracticeLocation AS PL WITH(NOLOCK)
			INNER JOIN PracticeLocation_User AS PLU WITH(NOLOCK)
				ON PL.[PracticeLocationID] = PLU.[PracticeLocationID] 
        WHERE
            PL.IsActive = 1
			AND PL.IsActive = 1
			AND PLU.IsActive = 1
            AND PLU.UserID = @UserID
        
        ORDER BY
            PL.IsPrimaryLocation DESC
	      , PL.Name
        
        SET @Err = @@ERROR

        RETURN @Err
    END
