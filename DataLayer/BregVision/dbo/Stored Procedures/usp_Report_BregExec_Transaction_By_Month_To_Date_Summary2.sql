﻿CREATE PROC [dbo].[usp_Report_BregExec_Transaction_By_Month_To_Date_Summary2]
AS
BEGIN
	--SELECT 
	--	   PracticeID,
	--	   ContactID,
	--	   PracticeName,
	--	   IsBillingCentralized,
	--	   IsShoppingCentralized,
	--	   BillingStartDate,
	--	   CreatedUserID,
	--	   CreatedDate,
	--	   ModifiedUserID,
	--	   ModifiedDate,
	--	   IsActive
	--FROM practice 
	--
	--WHERE PracticeID NOT IN (1, 3, 9)
	--AND isActive = 1

	--  Create Table Variable @TransactionTable
		DECLARE @QuantityOrderedTable TABLE
			(
				  PracticeName		VARCHAR(50)
				, PracticeLocation  VARCHAR(50)
				, Year				INT
				, Month				INT
				, QuantityDispensed INT
				, QuantityOrdered   INT
				, TotalTransactions INT	
			)

		DECLARE @QuantityDispensedTable TABLE
			(
				  PracticeName		VARCHAR(50)
				, PracticeLocation  VARCHAR(50)
				, Year				INT
				, Month				INT
				, QuantityDispensed INT
				, QuantityOrdered   INT
				, TotalTransactions INT
			)


		DECLARE @QuantityOrderQuantityDispensedTable TABLE
			(
				  PracticeName		VARCHAR(50)
				, PracticeLocation  VARCHAR(50)
				, Year				INT
				, Month				INT
				, QuantityDispensed INT
				, QuantityOrdered   INT
				, TotalTransactions INT
			)
		

		INSERT INTO @QuantityOrderedTable
		(
				  PracticeName		
				, PracticeLocation  
				, Year				
				, Month				
				, QuantityDispensed
				, QuantityOrdered 
				, TotalTransactions
		)
		SELECT 
			P.PracticeName
			, PL.Name AS PracticeLocation
			, YEAR( SOLI.CreatedDate ) AS Year
			, MONTH( SOLI.CreatedDate )  AS Month
			, 0
			, SUM(SOLI.QuantityOrdered) AS QuantityOrdered
			, 0
		FROM
			Practice AS P
		INNER JOIN 
			PracticeLocation AS PL
				ON P.PracticeID = PL.PracticeID
		INNER JOIN 
			BregVisionOrder AS BVO
				ON PL.PracticeLocationID = BVO.PracticeLocationID
		INNER JOIN 
			SupplierOrder AS SO
				ON BVO.BregVisionOrderID = SO.BregVisionOrderID
		INNER JOIN
			SupplierOrderLineItem AS SOLI 
				ON SO.SupplierOrderID = SOLI.SupplierOrderID
		WHERE P.PracticeID NOT IN (1, 3, 9)
			AND P.IsActive = 1
		Group BY
			P.PracticeName
			, PL.Name
			, YEAR( SOLI.CreatedDate ) 
			, MONTH( SOLI.CreatedDate ) 
		ORDER BY
			P.PracticeName
			, PL.Name	
			, YEAR( SOLI.CreatedDate ) 
			, MONTH( SOLI.CreatedDate ) 


		--
		INSERT INTO @QuantityDispensedTable
		(
				  PracticeName		
				, PracticeLocation  
				, Year				
				, Month				
				, QuantityDispensed
				, QuantityOrdered 
				, TotalTransactions	
		)
		SELECT 
			P.PracticeName
			, PL.Name AS PracticeLocation
			, YEAR( D.DateDispensed ) AS Year
			, MONTH( D.DateDispensed )  AS Month
			, SUM(DD.Quantity) AS QuantityDispensed
			, 0
			, 0
		FROM
			Practice AS P
		INNER JOIN 
			PracticeLocation AS PL
				ON P.PracticeID = PL.PracticeID
		INNER JOIN 
			Dispense AS D
				ON PL.PracticeLocationID = D.PracticeLocationID
		INNER JOIN 
			DispenseDetail AS DD
				ON D.DispenseID = DD.DispenseID
		WHERE P.PracticeID NOT IN (1, 3, 9)
			AND P.IsActive = 1
		Group BY
			P.PracticeName
			, PL.Name
			, YEAR( D.DateDispensed ) 
			, MONTH( D.DateDispensed )
		ORDER BY
			P.PracticeName
			, PL.Name	
			, YEAR( D.DateDispensed ) 
			, MONTH( D.DateDispensed )  



		INSERT INTO @QuantityOrderQuantityDispensedTable 
			(
				  PracticeName
				, PracticeLocation  
				, Year				
				, Month			
				, QuantityDispensed
				, QuantityOrdered
				, TotalTransactions
			)
		SELECT 
				SUB.PracticeName
				, SUB.PracticeLocation  
				, SUB.Year				
				, SUB.Month			
				, SUM( ISNULL(SUB.QuantityDispensed	, 0)) AS QuantityDispensed
				, SUM( ISNULL(SUB.QuantityOrdered	, 0)) AS QuantityOrdered
				, SUM( ISNULL(SUB.QuantityDispensed	, 0) + ISNULL(SUB.QuantityOrdered	, 0) ) AS TotalTransactions
		FROM
		(
		SELECT * FROM @QuantityDispensedTable
		UNION
		SELECT * FROM @QuantityOrderedTable
		) AS SUB
		GROUP BY SUB.PracticeName
				, SUB.PracticeLocation  
				, SUB.Year				
				, SUB.Month				

	--SELECT *
	--FROM @QuantityOrderQuantityDispensedTable		
	--		
	--SELECT 
	--	SUM(QuantityDispensed) AS Dispensed
	--	, SUM(QuantityOrdered) AS Ordered
	--	, SUM(TotalTransactions) AS Transactions
	--FROM @QuantityOrderQuantityDispensedTable
	--WHERE [Month] = Month(GETDATE())
	--	AND [Year] = Year(GETDATE())
	DECLARE @MonthToDateTransactions AS NVARCHAR(200)
	DECLARE @MonthToDateTransactionsMessage AS NVARCHAR(200)
	DECLARE @Subject  VARCHAR(70)

SET @Subject = (
				SELECT 'BV MTD Trans: ' + CAST( SUM(TotalTransactions) AS VARCHAR(10) )
				+ ',  Ordered: ' + CAST( SUM(QuantityOrdered)  AS VARCHAR(10) )  
				+ ',  Dispensed: ' + CAST( SUM(QuantityDispensed) AS VARCHAR(10) ) 
				FROM @QuantityOrderQuantityDispensedTable
				WHERE [Month] = Month(GETDATE())
				AND [Year] = Year(GETDATE())
				)
	
	SELECT @MonthToDateTransactions =
		  'Transactions: ' + CAST( SUM(TotalTransactions) AS VARCHAR(10) ) + CHAR(13) + CHAR(10)
		+ 'Ordered: ' + CAST( SUM(QuantityOrdered)  AS VARCHAR(50) ) + CHAR(13) + CHAR(10) 
		+ 'Dispensed: ' + CAST( SUM(QuantityDispensed) AS VARCHAR(50) ) + CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10)
		+ CONVERT(VARCHAR(50), GETDATE(), 109) 
		+ CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10)
		+ CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10)
		+ CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10)
		+ 'P.S. Sorry about the bonehead email last night'
	FROM @QuantityOrderQuantityDispensedTable
	WHERE [Month] = Month(GETDATE())
		AND [Year] = Year(GETDATE())
		
	SELECT 	@MonthToDateTransactions  AS MonthToDateTransactions 
	SELECT @MonthToDateTransactionsMessage  = '''' + 'MonthToDate ' + CAST(@MonthToDateTransactions AS NVARCHAR(200))	+ ''''

	SELECT @MonthToDateTransactionsMessage  AS MonthToDateTransactionsMessage  

	SELECT @Subject

	EXEC msdb.dbo.sp_send_dbmail	
		  @profile_name =  'Breg Vision Reports'  --'OrderConfirmation'
		--, @recipients = 'sromeo@breg.com'
	
		, @blind_copy_recipients = 'jbongiorni@breg.com'
		--, @subject = @MonthToDateTransactionsMessage
		, @subject = @Subject   --'BregVision Transaction Month To Date'  --@MonthToDateTransactionsMessage
		, @body = @MonthToDateTransactions
		--, @body = CAST(@MonthToDateTransactions AS VARCHAR(255) )
	
END	