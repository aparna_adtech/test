﻿/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Practice_Physician_Update
   
   Description:  Updates a record in the table Contact,
				and updates a recored in the table Physician
				given a PracticeID
   
   AUTHOR:       John Bongiorni 7/20/2007 6:45:20 PM
   
   Modifications:  
   
   exec [dbo].[usp_Practice_Physician_Update] 14, 'Dr.', 'Richard', '', 'Muir', '', 1, 2, ''
   ------------------------------------------------------------ */  

CREATE PROCEDURE [dbo].[usp_Practice_Physician_Update]
(
  --  @PracticeID					 INT
	  @ContactID                     INT
	, @Salutation                    VARCHAR(10)
	, @FirstName                     VARCHAR(50)
	, @MiddleName                    VARCHAR(50)
	, @LastName                      VARCHAR(50)
	, @Suffix                        VARCHAR(10)
	, @ModifiedUserID                INT
	, @SortOrder					 INT = 0
	, @Pin							 VARCHAR(4) = null
	, @ClearPin						 Bit = 0
)
AS
DECLARE
    @TransactionCountOnEntry    INT,       -- Transaction Count before the transaction begins
    @Err             		INT        --  holds the @@Error code returned by SQL Server

SELECT @Err = @@ERROR
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF @Err = 0
BEGIN    
    SELECT @TransactionCountOnEntry = @@TRANCOUNT
    BEGIN TRANSACTION
END        


IF @Err = 0
BEGIN

	IF NULLIF(@Pin, '') IS NULL
		set @Pin = NULL
	PRINT 'Pin=' + IsNull(@Pin, 'null')
		

    UPDATE dbo.Contact
	SET
		  Salutation = @Salutation
		, FirstName = @FirstName
		, MiddleName = @MiddleName
		, LastName = @LastName
		, Suffix = @Suffix
		, ModifiedUserID = @ModifiedUserID
		, ModifiedDate = GETDATE()

	WHERE 
		ContactID = @ContactID

    SELECT @Err = @@ERROR

END
IF @Err = 0
BEGIN
--

    UPDATE dbo.Physician
	SET
		SortOrder = @SortOrder
	,	Pin = CASE
				WHEN @ClearPin = 0 THEN ISNULL(@Pin, PIN)
				ELSE NULL
			  END		  

	WHERE 
		PhysicianID in (Select PhysicianID from Physician p where p.ContactID = @ContactID)


--    SELECT @Err = @@ERROR
--
END

IF @@TranCount > @TransactionCountOnEntry
BEGIN

    IF @Err = 0
        COMMIT TRANSACTION
    ELSE
        ROLLBACK TRANSACTION
        --  Add any database logging here      

END



