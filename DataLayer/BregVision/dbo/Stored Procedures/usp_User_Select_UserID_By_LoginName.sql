﻿
/* ------------------------------------------------------------
   PROCEDURE:    [dbo].[usp_User_Select_UserID_By_LoginName]
   
   Description:  Selects the UserID from the table User
				   from a given aspnetdb username.
   
   AUTHOR:       John Bongiorni 9/29/2007 1:51:38 PM
   
   Modifications:  
   ------------------------------------------------------------ */   ----------------SELECT ----------------

CREATE PROCEDURE [dbo].[usp_User_Select_UserID_By_LoginName]
(
	  @UserLoginName	VARCHAR(256)
	, @UserID			INT OUTPUT
)
As
BEGIN
	DECLARE @Err INT

	SELECT
		  @UserID = U.UserID
	FROM 
		aspnetdb.dbo.aspnet_Users AS AU
	INNER JOIN 
		aspnetdb.dbo.aspnet_Applications AS AA
			ON AU.ApplicationId = AA.ApplicationId
	INNER JOIN 
		dbo.[User] AS U
			ON AU.UserID = U.AspNet_UserID		
	WHERE 
		AU.LoweredUserName = LOWER(@UserLoginName)
		AND AA.LoweredApplicationName = LOWER('/bregvision')
		AND U.IsActive = 1
		
	SET @Err = @@ERROR

	RETURN @Err
End