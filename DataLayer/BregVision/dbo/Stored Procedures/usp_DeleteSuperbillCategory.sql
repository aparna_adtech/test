﻿
-- =============================================
-- Author:		<Author: Mike Sneen>
-- Create date: <Create Date: 02/28/09>
-- Description:	<Description: Used to get all a superbill>
-- modification:
--  [dbo].[usp_DeleteSuperbillCategory]  

-- =============================================
Create PROCEDURE [dbo].[usp_DeleteSuperbillCategory]  --3 --10  --  _With_Brands
	@PracticeLocationID int,
	@SuperbillCategoryID int
AS 
    BEGIN
    DECLARE @Err INT
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
--        SET NOCOUNT ON ;
     
     Update SuperBillCategory 
     Set IsActive = 0
     Where
		SuperBillCategoryID = @SuperbillCategoryID
		AND
		PracticeLocationID = @PracticeLocationID
		
	SET @Err = @@ERROR

	RETURN @Err

    END

--Exec usp_DeleteSuperbillCategory 3, 5



