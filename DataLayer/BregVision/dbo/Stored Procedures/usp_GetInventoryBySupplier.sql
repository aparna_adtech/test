﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- Modifications:
-- 	2007.11.09 JB Remove * !!!  Replace with columns.

-- =============================================
CREATE PROCEDURE [dbo].[usp_GetInventoryBySupplier]
	-- Add the parameters for the stored procedure here
	@PracticeLocationID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 SELECT 
		ProductInventoryID,
		PracticeLocationID,
		PracticeCatalogProductID,
		QuantityOnHandPerSystem,
		ParLevel,
		ReorderLevel,
		CriticalLevel,
--		CreatedUserID,
--		CreatedDate,
--		ModifiedUserID,
--		ModifiedDate,
		IsActive
 FROM [ProductInventory]
WHERE [PracticeLocationID] = @PracticeLocationID

END

