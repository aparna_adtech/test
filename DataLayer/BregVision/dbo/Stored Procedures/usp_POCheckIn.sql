﻿

-- =============================================
-- Author:		Greg Ross
-- Create date: Aug. 24th 2007
-- Description:	Updates/Inserts data into all tables needed to Checkin products for a purchase order. 
--
--	Dependencies:  usp_SupplierOrderLineItem_Select_Quantities_Given_SupplierOrderLineItemID
--
--  Modifications:  
--				2007.11.14  John Bongiorni
--					1. Make sure the the QuantityToCheckIn is at least 1 and no more than the Maximum to CheckIn.
--					2. Only update the ProductInventory.QuantityOnHandPerSystem if the QuantityToCheckIn is valid.
--				2007.11.15 JB
--					Note: the dependency (nested proc was updated to corrected retrieve the quantity checked-in
--					was updated.)
--					Due to bad data in the database, items will not be checkin
--					when the quantity already checked in is equal to or greater than the 
--					quantity ordered.
-- =============================================
CREATE PROCEDURE [dbo].[usp_POCheckIn]
	
	@UserID INT,
	@ProductInventoryID INT,
	@SupplierOrderLineItemID INT,
	@SupplierOrderID INT, 
	@BregVisionOrderID INT,
	@PracticeCatalogProductID INT,
	@ActualWholesaleCost SMALLMONEY,
	@Quantity INT
AS
BEGIN
	
	SET NOCOUNT ON;
	
		DECLARE	@return_value INT
		DECLARE @Err INT 
        DECLARE @TransactionCountOnEntry INT 


		DECLARE @QuantityOrdered INT		-- Quantity Ordered.
		DECLARE @QuantityCheckedIn INT		-- Quantity Already Checked In
		DECLARE @MaxQuantityToCheckIn INT	--  Maximum Quantity that can be checked in.
		DECLARE @QuantityToCheckIn	INT		-- Quantity to checked In.
		
		
		DECLARE @TotalQuantityCheckedIn INT
		DECLARE @RecordCount INT
			
			
		--  Set the desired quantity to checkin
		SET @QuantityToCheckIn = @Quantity	
			
		--  Get the Quantity Ordered, QuantityCheckedIn, and MaxQuantityToCheckIn
		--		Given the SupplierOrderLineItemID

		EXEC	@return_value = [dbo].[usp_SupplierOrderLineItem_Select_Quantities_Given_SupplierOrderLineItemID]
					@SupplierOrderLineItemID = @SupplierOrderLineItemID,
					@QuantityOrdered = @QuantityOrdered OUTPUT,
					@QuantityCheckedIn = @QuantityCheckedIn OUTPUT,
					@MaxQuantityToCheckIn = @MaxQuantityToCheckIn OUTPUT


		--  Only insert if quantity is 1 or more and quantity is equal to or less than the maxQuantityToCheckIn
		IF ( (@QuantityToCheckIn >= 1) AND (@QuantityToCheckIn <= @MaxQuantityToCheckIn) )
			BEGIN
			
				INSERT INTO ProductInventory_orderCheckIn
					(
					ProductInventoryID,
					 SupplierOrderLineItemID,
					 ActualWholesaleCost,
					 Quantity,
					 IsEntireLineItemCheckedIn,
					 dateCheckedIn,
					 CreatedUserID,
					 CreatedDate,
					 ModifiedUserID,
					 ModifiedDate,
					 isActive
					 )
					VALUES
					(
					 @ProductInventoryID,
					 @SupplierOrderLineItemID,
					 @ActualWholesaleCost,
					 @Quantity,
					 0,
					 GETDATE(),
					 @UserID,
					 GETDATE(),
					 @UserID,
					 GETDATE(),
					 1
					 )
	 
				
				SET @TotalQuantityCheckedIn = ( SELECT SUM(Quantity) 
												FROM ProductInventory_OrderCheckIn WITH(NOLOCK)
												WHERE SupplierOrderLineItemID = @SupplierOrderLineItemID
												GROUP BY SupplierOrderLineItemID)
				--print 'Total Quantity Checked IN:'
				--print @TotalQuantityCheckedIn

				--Sets the Entire LineItem CheckIn Status to true in ProdInventory_OrdercheckIN
				-- and updates the Line Item Status in SupplierOrderLineItem to 'CheckedIN'
				
				
				
				--  If all item for the supplier order line item have been checked in 
				--		than no more item are remaining to be checked in
				IF @TotalQuantityCheckedIn >= @QuantityOrdered  -- SHOULD BE if @TotalQuantityCheckedIn = @QuantityOrdered
					BEGIN
						UPDATE ProductInventory_orderCheckIn 
							SET isEntireLineItemCheckedIn = 1,
								ModifiedUserID = @UserID,
								ModifiedDate = GETDATE()
							 WHERE SupplierOrderLineItemID = @SupplierOrderLineItemID
							
						UPDATE SupplierOrderLineItem
							 SET SupplierOrderLineItemStatusID = 2,
								ModifiedUserID = @UserID,
								ModifiedDate = GETDATE()
							 WHERE SupplierOrderLineItemID = @SupplierOrderLineItemID

							--print 'Set Entire Line Item to True'
				
							
					END 
				ELSE 

				--  If some items but not all item for the supplier order line item have been checked in 
				--		than there are more items are remaining to be checked in

					BEGIN
						IF @TotalQuantityCheckedIn > 0 --set Status to Partially Checked IN in SupplierOrderLineItem
							BEGIN
								UPDATE SupplierOrderLineItem
									SET SupplierOrderLineItemStatusID = 3,
										ModifiedUserID = @UserID,
										ModifiedDate = GETDATE()
									 WHERE SupplierOrderLineItemID = @SupplierOrderLineItemID
									--print 'Set Status to Partially'
								
								--Greg Ross - Added to set Supplier and Breg Vision order to partially ordered
								-- if any Supplier Line Items have been checked in 			
								--Set Status to Partially for Supplier
								UPDATE SupplierOrder
								SET SupplierOrderStatusID = 3 
								WHERE SupplierOrderID=@SupplierOrderID  
								
								--Set Statut to Partially for BVO
								UPDATE BregVisionOrder
								SET BregVisionOrderStatusID = 3 
								WHERE BregVisionOrderID=@BregVisionOrderID
							END 
					END 

 				--SupplierLineItem
				SELECT @RecordCount = COUNT(1)  FROM SupplierOrderLineItem 
					WHERE SupplierOrderID=@SupplierOrderID 
						AND SupplierOrderLineItemStatusID <>2 --Checked In
				IF 	@RecordCount = 0 --No more line Items, we need to close the SUpplier Order
					BEGIN
						UPDATE SupplierOrder
							SET SupplierOrderStatusID = 2 
								WHERE SupplierOrderID=@SupplierOrderID  
							--print 'Closed Supplier Order'
					
					END 
 				--SupplierOrder
				SELECT @RecordCount = COUNT(1)  FROM SupplierOrder 
					WHERE BregVisionOrderID = @BregVisionOrderID 
						AND SupplierOrderStatusID <>2 --Checked In
				IF 	@RecordCount = 0 --No more line Items, we need to close the BVO Order
					BEGIN
						UPDATE BregVisionOrder
							SET BregVisionOrderStatusID = 2 
								WHERE BregVisionOrderID=@BregVisionOrderID  
							--print 'Closed BV Order'
					END 
		


				--  Update product inventory Quantity on hand per system by adding the quantityToCheckIn.
				--  Modified 08/08/08 - GR - to account for stocking/buying units. 
				--  Multiply QOH by case size(Stocking Units) to reflect accurate quantity on hand

				
				Set @QuantityToCheckIn = @QuantityToCheckIn * 
					(select Stockingunits from practicecatalogproduct where practicecatalogproductID=@practicecatalogproductID)
				
				UPDATE ProductInventory 
				SET QuantityOnHandPerSystem = QuantityOnHandPerSystem + @QuantityToCheckIn
				WHERE ProductInventoryID = @ProductInventoryID
	END
	
END

