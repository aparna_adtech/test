﻿--usp_PracticeLocationBillingAddresses_Update_From_Decentralized_PracticeBillingAddress 1, 1

CREATE PROC [dbo].[usp_PracticeLocationBillingAddresses_Update_From_Decentralized_PracticeBillingAddress]
			  @PracticeID		INT
			, @UserID			INT
AS
	BEGIN

		DECLARE	@AttentionOf VARCHAR(50)
		DECLARE @AddressID INT
		
		
		DECLARE  @CreatedUserID INT
		SET @CreatedUserID = 0


		--	If PracticeLocationBillingAddress_AddressID exists update with AttentionOF
		--		Note the AddressID has already been been set previously.
		
		UPDATE dbo.PracticeLocationBillingAddress
		SET	   
		--	   PracticeLocationID = TPL.PracticeLocationID 
		--	   AddressID =  @AddressID
		--	 , AttentionOf = @AttentionOf
			  ModifiedUserID = @UserID
			 , ModifiedDate = GETDATE()
			 , IsActive = 0
		FROM dbo.Practice AS P
		INNER JOIN dbo.PracticeLocation AS PL
			ON P.PracticeID = PL.PracticeID	
		INNER JOIN dbo.PracticeLocationBillingAddress AS PLBA
			ON PL.PracticeLocationID = PLBA.PracticeLocationID		
		WHERE 
			  P.PracticeID =  @PracticeID 
			  AND PL.IsActive = 1 	
			  AND PLBA.IsActive = 1
			  

		--  jb 8/21/2007  DEACTIVATE ALL ASSOCIATED ADDRESSES OF PRACTICELOCATIONBILLINGADDRESS OF LOCATIONS IN THE PRACTICE.
		
		UPDATE dbo.Address
			SET 
			   ModifiedUserID = @UserID
			 , ModifiedDate = GETDATE()
			 , IsActive = 0
			
		FROM dbo.Practice AS P
		INNER JOIN dbo.PracticeLocation AS PL
			ON P.PracticeID = PL.PracticeID	
		INNER JOIN dbo.PracticeLocationBillingAddress AS PLBA
			ON PL.PracticeLocationID = PLBA.PracticeLocationID	
		INNER JOIN dbo.Address AS A
				ON PLBA.AddressID = A.AddressID
			WHERE 
			  P.PracticeID =  @PracticeID		
		
		
		--  SELECT * FROM dbo.PracticeLocationBillingAddress AS PLBA	Just for testing.

END					

