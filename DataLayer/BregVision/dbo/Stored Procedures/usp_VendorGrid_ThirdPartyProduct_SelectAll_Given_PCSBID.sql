﻿/*
This replaces usp_ThirdPartyProduct_SelectAll_Given_PCSBID
This needs to pull from the PracticeCatalogProduct table!!!! as much as possible, 
   since updates have been made to the pcp table but not the thirdpartysupplierbrand table.
   select * from practicecatalogproduct where thirdpartyProductID is not null and isactive = 1
*/


CREATE PROCEDURE [dbo].[usp_VendorGrid_ThirdPartyProduct_SelectAll_Given_PCSBID] --337
		@PracticeCatalogSupplierBrandID INT 
AS
BEGIN

	SELECT 
		   TPP.ThirdPartyProductID,
		   TPP.PracticeCatalogSupplierBrandID,
		   PCP.PracticeCatalogProductID,
--		   TPP.[Name],
		   TPP.ShortName,
		   TPP.Code,
		   ISNULL(TPP.Packaging, '') AS Packaging,
		   ISNULL(TPP.LeftRightSide, '')  AS Side,
		   ISNULL(TPP.Size, '') AS Size,
		   ISNULL(TPP.Color, '') AS Color,
		   ISNULL(TPP.Gender, '') AS Gender,
		   ISNULL(TPP.Mod1, '') AS Mod1,
		   ISNULL(TPP.Mod2, '') AS Mod2,
		   ISNULL(TPP.Mod3, '') AS Mod3,
		   CAST(ISNULL(PCP.WholesaleCost, 0) AS DECIMAL(8,2)) AS WholesaleCost   --  Get from PracticeCatalog
		   , dbo.ConcatHCPCS(PCP.PracticeCatalogProductID) AS HCPCSString -- JB 20071004 Added Column for UDF Concat of HCPCS
					
														
			, CASE (SELECT COUNT(1) 
			FROM productinventory AS PI WITH (NOLOCK) 
			WHERE PI.practiceCatalogProductID = PCP.practiceCatalogProductID
			) WHEN 0 THEN 1 ELSE 0 END AS IsDeletable   --  JB  20080225  Added Column for "Deletable" PCPs

		   , TPP.IsDiscontinued
		   , '' AS UserMessage
		   , 0 AS RowNumber
		   , UPCCode = (Select Top 1 upc.Code from UPCCode upc where upc.ThirdPartyProductID = TPP.ThirdPartyProductID )
		   , MAX(COALESCE(PR.IsActive,0)) AS IsReplaced

--		   TPP.Description,
--		   TPP.DateDiscontinued,
--		   TPP.OldMasterCatalogProductID,
--		   TPP.CreatedUserID,
--		   TPP.CreatedDate,
--		   TPP.ModifiedUserID,
--		   TPP.ModifiedDate,
--		   TPP.IsActive,
--		   TPP.Source 
	FROM dbo.ThirdPartyProduct AS TPP WITH (NOLOCK)
	INNER JOIN dbo.PracticeCatalogProduct AS PCP WITH (NOLOCK)
		ON TPP.ThirdPartyProductID = PCP.ThirdPartyProductID
		AND PCP.IsActive = 1
	LEFT OUTER JOIN ProductReplacement AS PR WITH(NOLOCK) ON PR.IsActive = 1 AND
		(PR.OldThirdPartyProductID = TPP.ThirdPartyProductID) AND
		(PR.PracticeID IS NULL OR PR.PracticeID = PCP.PracticeID)
	WHERE TPP.PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID
		AND TPP.IsActive = 1
		AND PCP.IsActive = 1
	GROUP BY
		   TPP.ThirdPartyProductID,
		   TPP.PracticeCatalogSupplierBrandID,
		   PCP.PracticeCatalogProductID,
--		   TPP.[Name],
		   TPP.ShortName,
		   TPP.Code,
		   TPP.Packaging,
		   TPP.LeftRightSide,
		   TPP.Size,
		   TPP.Color,
		   TPP.Gender,
		   TPP.Mod1,
		   TPP.Mod2,
		   TPP.Mod3,
		   PCP.WholesaleCost,
		   PCP.PracticeCatalogProductID,
		   TPP.IsDiscontinued
	ORDER BY
		   TPP.ShortName,
		   TPP.Code,
		   TPP.Packaging,
		   TPP.LeftRightSide,
		   TPP.Size,
		   TPP.Gender,
		   TPP.Color

END
