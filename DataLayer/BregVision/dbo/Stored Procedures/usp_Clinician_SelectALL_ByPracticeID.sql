﻿
CREATE PROCEDURE [dbo].[usp_Clinician_SelectALL_ByPracticeID]
		@PracticeID INT
AS
BEGIN	
	SELECT
		Pr.PracticeID 
	  , clin.clinicianID
	  , C.ContactID
	  , C.Salutation
	  , C.FirstName
	  , C.MiddleName
	  , C.LastName
	  , C.Suffix
	  , Pin = '' --this is a write only field so we don't need to retrieve it
	  , clin.SortOrder
	  , clin.IsProvider
	  , clin.IsFitter
	  , clin.CloudConnectId
	  , clin.NPI
	FROM
		dbo.Contact AS C
		INNER JOIN dbo.Clinician AS clin
			ON C.ContactID = clin.ContactID
		INNER JOIN dbo.Practice AS Pr
			ON clin.PracticeID = Pr.PracticeID
	WHERE
			Pr.PracticeID = @PracticeID
		AND clin.IsActive = 1
		AND C.IsActive = 1
END
