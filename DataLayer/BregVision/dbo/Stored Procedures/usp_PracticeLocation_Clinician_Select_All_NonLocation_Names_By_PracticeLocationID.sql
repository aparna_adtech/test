﻿

Create PROCEDURE [dbo].[usp_PracticeLocation_Clinician_Select_All_NonLocation_Names_By_PracticeLocationID]
					@PracticeLocationID INT
AS 
    BEGIN 
    
		DECLARE @PracticeID INT
    
    
		SELECT @PracticeID = PracticeID 
		FROM dbo.PracticeLocation WITH (NOLOCK)
		WHERE PracticeLocationID = @PracticeLocationID
    
    
        SELECT
            CL.ClinicianID
          , C.ContactID
	--	, C.Salutation
	--	, C.FirstName
	--	, C.MiddleName
	--	, C.LastName
	--	, C.Suffix
          , RTRIM(C.FirstName + ' ' + C.LastName + ' ' + C.Suffix) AS ClinicianName
        FROM
            dbo.Contact  AS C	WITH (NOLOCK)
            INNER JOIN dbo.Clinician  AS CL WITH (NOLOCK)
                ON C.ContactID = CL.ContactID
            INNER JOIN dbo.Practice AS P  WITH (NOLOCK)
				ON CL.PracticeID = P.PracticeID
            
        WHERE
            P.PracticeID = @PracticeID       
            AND CL.IsActive = 1
            AND C.IsActive = 1
            AND CL.ClinicianID NOT IN (	SELECT PhysicianID 
										FROM PracticeLocation_Physician WITH (NOLOCK)
										WHERE PracticeLocationID = @PracticeLocationID 
											AND IsActive = 1 
										) 
    END
