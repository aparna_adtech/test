﻿--  Get All PracticeCatalogSupplierBrand this is used for backouts given the PracticeID
--  Example EXEC [usp_Report_PracticeCatalogSupplierBrand_Select_All_Given_PracticeID] 6  


CREATE PROC [dbo].[usp_Report_PracticeCatalogSupplierBrand_Select_All_Given_PracticeID] 
				@PracticeID INT
AS
BEGIN

	
	SELECT
				  SUB.PracticeID
				, SUB.Practice
				, SUB.IsThirdPartySupplier
				, SUB.[SupplierID]
				, SUB.SupplierName
				, SUB.Supplier		
				, SUB.[SupplierType]
				, SUB.IsVendor

				, SUB.PracticeCatalogSupplierBrandID

				, SUB.BrandName
				, SUB.Brand

	FROM
		(SELECT
				  P.PracticeID AS [PracticeID]
				, P.PRacticeName AS Practice
				, PCSB.IsThirdPartySupplier AS IsThirdPartySupplier
				, PCSB.ThirdPartySupplierID AS [SupplierID]

				, PCSB.SupplierName
				, PCSB.SupplierShortName AS Supplier		
				, CASE IsVendor WHEN 1 THEN 'Vendor' WHEN 0 THEN 'Manufacturer' END AS [SupplierType]
				, IsVendor 	AS [IsVendor]	

				, PCSB.PracticeCatalogSupplierBrandID

				, PCSB.BrandName
				, PCSB.BrandShortName AS [Brand]
				
		--		, TPS.FaxOrderPlacement
			
			FROM dbo.ThirdPartySupplier AS TPS
			INNER JOIN Practice AS P
				ON P.PracticeID = TPS.PracticeID
			INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB 
				ON PCSB.ThirdPartySupplierID = TPS.ThirdPartySupplierID
				AND PCSB.PracticeID = P.PracticeID
				AND PCSB.IsActive = 1

			WHERE 		
				P.IsActive = 1 
				AND TPS.IsActive = 1
				AND PCSB.IsActive = 1
				AND PCSB.IsThirdPartySupplier = 1
				AND P.PracticeID = @PracticeID




		UNION


		SELECT
				  P.PracticeID AS [PracticeID]
				, P.PRacticeName AS Practice
				, PCSB.IsThirdPartySupplier AS IsThirdPartySupplier
				, PCSB.MasterCatalogSupplierID AS [SupplierID]

				, PCSB.SupplierName
				, PCSB.SupplierShortName AS Supplier		
				, 'MC Manufacturer' AS [SupplierType]
				, 0 AS [IsVendor]	

				, PCSB.PracticeCatalogSupplierBrandID

				, PCSB.BrandName
				, PCSB.BrandShortName AS [Brand]
				
		--		, TPS.FaxOrderPlacement
			
			FROM Practice AS P
				
			INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB 
				ON PCSB.PracticeID = PCSB.PracticeID
				AND PCSB.PracticeID = P.PracticeID
				AND PCSB.IsActive = 1

			WHERE 		
				P.IsActive = 1 
				AND PCSB.IsActive = 1
				AND P.PracticeID = @PracticeID
				AND PCSB.IsThirdPartySupplier = 0

		) AS SUB

	ORDER BY
		  SUB.Practice
		, SUB.IsThirdPartySupplier
		, SUB.IsVendor
		, SUB.Supplier  --SupplierShortName
		, SUB.Brand     --BrandShortName



END