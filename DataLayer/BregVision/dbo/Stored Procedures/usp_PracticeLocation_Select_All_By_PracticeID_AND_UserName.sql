﻿

/* ------------------------------------------------------------
--Exec usp_PracticeLocation_Select_All_By_PracticeID_AND_UserName 1, 'MikesTestUser2'
   PROCEDURE:    dbo.usp_PracticeLocation_Select_All_By_PracticeID_AND_UserName
   
   Description:  Selects all PracticeLocation records for a given Practice.
				Select all records from the table dbo.PracticeLocation
				given the PracticeID.
   
   AUTHOR:       John Bongiorni 7/23/2007 11:05:42 AM
   
   Modifications:  JB 9/30/2007 6:29PM  Add Column IsPrimaryLocationYesNo
   ------------------------------------------------------------ */  

CREATE PROCEDURE [dbo].[usp_PracticeLocation_Select_All_By_PracticeID_AND_UserName] 
			( @PracticeID INT,
			  @UserName varchar(30) )
AS 
    BEGIN
        DECLARE @Err INT
        
  --      declare @PracticeID INT
		--declare @UserName varchar(30) 
		--set @PracticeID=1
		--set @UserName='MikesTestUser2'
        SELECT
        
            PL.PracticeLocationID
          , PL.PracticeID
          , PL.Name
          , PL.IsPrimaryLocation
          , CASE (PL.IsPrimaryLocation) WHEN 1 THEN 'Yes' ELSE 'No' END AS IsPrimaryLocationYesNo
          , PL.EmailForOrderApproval
          , PL.EmailForLowInventoryAlerts
          , PL.IsEmailForLowInventoryAlertsOn
		  , PL.GeneralLedgerNumber AS 'GLNumber'
		  , PL.IsHcpSignatureRequired
        FROM
			User_PracticeLocation_Permissions UPP  WITH(NOLOCK)
inner join PracticeLocation AS PL WITH(NOLOCK)
	on UPP.PracticeLocationID = PL.PracticeLocationID
INNER JOIN dbo.[User] AS U WITH(NOLOCK)
	ON U.UserID = UPP.UserID
					
            INNER JOIN aspnetdb.dbo.aspnet_Users AS AU WITH(NOLOCK)
				ON AU.UserId = U.AspNet_UserID
 
			--INNER JOIN User_PracticeLocation_Permissions UPP  WITH(NOLOCK)
			--	ON UPP.UserID = U.UserID 
			--	AND UPP.PracticeLocationID=PL.PracticeLocationID
        
        WHERE
			UPP.IsActive = 1
        	and PL.isactive = 1
			AND U.IsActive = 1  --  JB  2008.01.11
			AND PL.PracticeID = @PracticeID
			AND UPP.AllowAccess = 1
			AND AU.UserName = @UserName
        
        ORDER BY
            PL.IsPrimaryLocation DESC
	      , PL.Name
        
        SET @Err = @@ERROR

        RETURN @Err
    END
