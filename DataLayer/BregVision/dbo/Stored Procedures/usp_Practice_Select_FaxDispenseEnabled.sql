﻿

/* ------------------------------------------------------------
   PROCEDURE:    dbo.[usp_Practice_Select_FaxDispenseEnabled]
   
   Description:  Selects the FaxDispenseReceiptEnabled from the table dbo.Practice
   				 given the PracticeID 
   				 and returns true or false.
   
   AUTHOR:       Yuiko Sneen-Itazawa 09/03/2013 4:18 PM
   
   Modifications:  
   ------------------------------------------------------------ */ 

CREATE PROCEDURE [dbo].[usp_Practice_Select_FaxDispenseEnabled]
		  @PracticeID		INT
		, @FaxDispenseReceiptEnabled		bit  output
AS
BEGIN
	DECLARE @Err INT
	
	SELECT
		  @FaxDispenseReceiptEnabled = FaxDispenseReceiptEnabled
	
	FROM dbo.Practice
	WHERE 
		PracticeID = @PracticeID
	
	SET @Err = @@ERROR

	RETURN @Err
END



