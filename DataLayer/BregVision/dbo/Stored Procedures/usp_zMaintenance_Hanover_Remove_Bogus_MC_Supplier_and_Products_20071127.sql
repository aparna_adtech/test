﻿CREATE PROC usp_zMaintenance_Hanover_Remove_Bogus_MC_Supplier_and_Products_20071127
AS 
BEGIN
	

--  Get practiceID for Hanover.
--  NOTE: BEFORE REUSING CHECK ALL CODE THIS IS FOR USE
--  ONLY WITH A ONE LOCATION Practice!!!!!!

SELECT * FROM PRACTICE WHERE PracticeID = 11

SELECT * 
FROM PracticeLocation AS PL
WHERE PL.PracticeLocationID = 39  --  Hanover has only one practice.
--WHERE PL.PracticeID = 11

--  Get PracticeCatalogSupplierBrandIDs for the bogus Suppliers.
SELECT *
FROM dbo.PracticeCatalogSupplierBrand AS pcsb
WHERE pcSB.PracticeID = 11
AND IsThirdPartySupplier = 0
AND MasterCatalogSupplierID IS NOT NULL

--  109	11	5	NULL	Bird and Cronin	B&C	Bird and Cronin	B&C	0	NULL	1	2007-11-07 21:40:41.873	NULL	NULL	1
--  111	11	9	NULL	Medical Specialties	MedSpec	Medical Specialties	MedSpec	0	NULL	1	2007-11-07 21:58:05.733	NULL	NULL	1

--  Select bogus PCproducts of the bogus suppliers.  --  83 records
SELECT PracticeCatalogProductID,
	   PracticeID,
	   PracticeCatalogSupplierBrandID,
	   MasterCatalogProductID,
	   ThirdPartyProductID,
	   IsThirdPartyProduct,
	   WholesaleCost,
	   BillingCharge,
	   DMEDeposit,
	   BillingChargeCash,
	   Sequence,
	   CreatedUserID,
	   CreatedDate,
	   ModifiedUserID,
	   ModifiedDate,
	   IsActive
FROM dbo.PracticeCatalogProduct AS PCP
WHERE PCP.PracticeID = 11
	AND PCP.PracticeCatalogSupplierBrandID IN (109, 111)
	AND PCP.IsThirdPartyProduct = 0
	--  AND PCP.IsActive = 1
ORDER BY
	PCP.PracticeCatalogSupplierBrandID
	, PCP.MasterCatalogProductID
	
	
/*
	Check the SupplierOrderLineItem table 
	and the ProductInventory table
*/

--  Check the SupplierOrderLineItem table for the bogus product inventory.
SELECT *
FROM dbo.SupplierOrderLineItem  -- zero records.
WHERE PracticeCatalogProductID IN
		(
			SELECT PracticeCatalogProductID
			FROM dbo.PracticeCatalogProduct AS PCP
			WHERE PCP.PracticeID = 11
				AND PCP.PracticeCatalogSupplierBrandID IN (109, 111)
				AND PCP.IsThirdPartyProduct = 0
				--AND PCP.IsActive = 1		
		)

--  Check Inventory Table for the bogus PCProducts.  --  83 records
SELECT * 
FROM dbo.ProductInventory AS PI
WHERE PI.PracticeCatalogProductID IN
		(
			SELECT PracticeCatalogProductID
			FROM dbo.PracticeCatalogProduct AS PCP
			WHERE PCP.PracticeID = 11
				AND PCP.PracticeCatalogSupplierBrandID IN (109, 111)
				AND PCP.IsThirdPartyProduct = 0
				--AND PCP.IsActive = 1		
		)
	
/*
	Check all ProductInventory tables for the bogus product inventory records.
	Create table variable, @BogusProductInventory, to hold the bogus
	 ProductInventoryIDs.
*/
	
DECLARE @BogusProductInventory TABLE 
	(ProductInventoryID INT)	
	
INSERT INTO @BogusProductInventory
	(ProductInventoryID)
SELECT ProductInventoryID
FROM dbo.ProductInventory AS PI			--  This will select inventory from ALL LOCATIONS!
WHERE PI.PracticeCatalogProductID IN
		(
			SELECT PracticeCatalogProductID
			FROM dbo.PracticeCatalogProduct AS PCP
			WHERE PCP.PracticeID = 11
				AND PCP.PracticeCatalogSupplierBrandID IN (109, 111)
				AND PCP.IsThirdPartyProduct = 0
				AND PCP.IsActive = 1		
		)
			
SELECT DISTINCT ProductInventoryID
FROM @BogusProductInventory
ORDER BY ProductInventoryID	


		
--  Check the ProductInventory_OrderCheckIn table for the bogus product inventory.

SELECT *
FROM dbo.ProductInventory_OrderCheckIn  
WHERE ProductInventoryID IN 
				(
					SELECT DISTINCT ProductInventoryID
					FROM @BogusProductInventory
				)		
		
--	   (				
--		SELECT ProductInventoryID
--		FROM dbo.ProductInventory AS PI
--		WHERE PI.PracticeCatalogProductID IN
--				(
--					SELECT PracticeCatalogProductID
--					FROM dbo.PracticeCatalogProduct AS PCP
--					WHERE PCP.PracticeID = 11
--						AND PCP.PracticeCatalogSupplierBrandID IN (109, 111)
--						AND PCP.IsThirdPartyProduct = 0
--						AND PCP.IsActive = 1		
--				)
--		)


--  Check the ProductInventory_DispenseDetail table for the bogus product inventory.

SELECT *
FROM dbo.ProductInventory_DispenseDetail  
WHERE ProductInventoryID IN 		
				(
					SELECT DISTINCT ProductInventoryID
					FROM @BogusProductInventory
				)		

		

--  Check the ProductInventory_QuantityOnHandPhysical table for the bogus product inventory.

SELECT *
FROM dbo.ProductInventory_QuantityOnHandPhysical
WHERE ProductInventoryID IN 		
				(
					SELECT DISTINCT ProductInventoryID
					FROM @BogusProductInventory
				)		
	
--  Check the ProductInventory_Purge table for the bogus product inventory.

SELECT *
FROM dbo.ProductInventory_Purge
WHERE ProductInventoryID IN 		
				(
					SELECT DISTINCT ProductInventoryID
					FROM @BogusProductInventory
				)		

--  Check the ProductInventory_ManualCheckIn table for the bogus product inventory.

SELECT *
FROM dbo.ProductInventory_ManualCheckIn
WHERE ProductInventoryID IN 		
				(
					SELECT DISTINCT ProductInventoryID
					FROM @BogusProductInventory
				)		
			
/*
	Summary:
	 2 bogus suppliers for PracticeCatalogSupplierBrandID
	 83 bogus records in PracticeCatalogProducts  
	 83 bogus records in PracticeCatalogSupplierBrand  
*/



SELECT *
FROM dbo.PracticeCatalogProduct AS PCP
WHERE IsActive = 0


-----------------
--SELECT * 
--INTO zDeleted_ProductInventory 
--FROM dbo.ProductInventory AS PI
--WHERE PI.PracticeCatalogProductID IN
--		(
--			SELECT PracticeCatalogProductID
--			FROM dbo.PracticeCatalogProduct AS PCP
--			WHERE PCP.PracticeID = 11
--				AND PCP.PracticeCatalogSupplierBrandID IN (109, 111)
--				AND PCP.IsThirdPartyProduct = 0
--				AND PCP.IsActive = 1		
--		)
---- Note: Add DateDeleted column update the 83 rows with GetDate for 
----			the DateDeleted.


-- Delete the 83 bogus records from ProductInventory.
--BEGIN TRAN	
--	
--DELETE
--FROM dbo.ProductInventory 
--WHERE PracticeCatalogProductID IN
--		(
--			SELECT PracticeCatalogProductID
--			FROM dbo.PracticeCatalogProduct AS PCP
--			WHERE PCP.PracticeID = 11
--				AND PCP.PracticeCatalogSupplierBrandID IN (109, 111)
--				AND PCP.IsThirdPartyProduct = 0
--				--AND PCP.IsActive = 1		
--		)
--		
--ROLLBACK TRAN
		
-- Note: Add DateDeleted column update the 83 rows with GetDate for 
--			the DateDeleted.


SELECT *
FROM PracticeCatalogProduct AS PCP
WHERE PCP.IsActive = 0



--  Update the 83 bogus PCP records by setting their isActive flag to false

--UPDATE PracticeCatalogProduct      -- 83 Rows Updated.
--SET IsActive = 0
--WHERE PracticeID = 11
--	AND PracticeCatalogSupplierBrandID IN (109, 111)
--	AND IsThirdPartyProduct = 0


--  Zero are Active, 83 are inactive.
SELECT *
FROM dbo.PracticeCatalogProduct AS PCP
WHERE PCP.PracticeID = 11
	AND PCP.PracticeCatalogSupplierBrandID IN (109, 111)
	AND PCP.IsThirdPartyProduct = 0
	  AND PCP.IsActive = 1

--  83 inactive
SELECT *
FROM dbo.PracticeCatalogProduct AS PCP
WHERE PCP.PracticeID = 11
	AND PCP.PracticeCatalogSupplierBrandID IN (109, 111)
	AND PCP.IsThirdPartyProduct = 0
	  AND PCP.IsActive = 0
ORDER BY
	PCP.PracticeCatalogSupplierBrandID
	, PCP.MasterCatalogProductID


SELECT *
FROM dbo.PracticeCatalogSupplierBrand AS pcsb
WHERE pcSB.PracticeID = 11
AND IsThirdPartySupplier = 0
AND MasterCatalogSupplierID IS NOT NULL
AND PracticeCatalogSupplierBrandID IN (109, 111)
AND IsActive = 1
--  109	11	5	NULL	Bird and Cronin	B&C	Bird and Cronin	B&C	0	NULL	1	2007-11-07 21:40:41.873	NULL	NULL	1
--  111	11	9	NULL	Medical Specialties	MedSpec	Medical Specialties	MedSpec	0	NULL	1	2007-11-07 21:58:05.733	NULL	NULL	1

--BEGIN TRAN
-- uPDATE BOGUS PCSuppplierBrand.IsActive = 0
--UPDATE dbo.PracticeCatalogSupplierBrand 
--SET IsActive = 0
--WHERE PracticeID = 11
--AND IsThirdPartySupplier = 0
--AND MasterCatalogSupplierID IS NOT NULL
--AND PracticeCatalogSupplierBrandID IN (109, 111)
--AND IsActive = 1
--
--ROLLBACK TRAN

SELECT *
FROM dbo.PracticeCatalogSupplierBrand AS pcsb
WHERE pcSB.PracticeID = 11
AND IsThirdPartySupplier = 0
AND MasterCatalogSupplierID IS NOT NULL
AND PracticeCatalogSupplierBrandID IN (109, 111)
AND IsActive = 0


END