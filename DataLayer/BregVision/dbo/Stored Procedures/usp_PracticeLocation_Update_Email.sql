﻿
/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocation_Update_Email
   
   Description:  Updates the email columns for a record in the table PracticeLocation
   
   AUTHOR:       John Bongiorni 9/24/2007 11:41:17 AM
   
   Modifications:  old proc is dbo.usp_PracticeLocation_Update_Email_OLDDONOTUSE
   
   Added FaxForPrintDispense				Ysi             09/07/2013
   ------------------------------------------------------------ */  ------------------ UPDATE -------------------

CREATE PROCEDURE [dbo].[usp_PracticeLocation_Update_Email]
(
	  @PracticeLocationID            INT
	, @EmailForOrderApproval         VARCHAR(200)
	, @EmailForOrderConfirmation     VARCHAR(200)
	, @EmailForFaxConfirmation       VARCHAR(200)
	, @EmailForBillingDispense       VARCHAR(200)
	, @EmailForLowInventoryAlerts    VARCHAR(200)
	, @IsEmailForLowInventoryAlertsOn BIT
	, @ModifiedUserID                 INT
	, @EmailForPrintDispense		 VARCHAR(200) = NULL
	, @FaxForPrintDispense			 VARCHAR(100) = NULL
)
AS
BEGIN
	DECLARE @Err INT

	UPDATE dbo.PracticeLocation
	SET
		  EmailForOrderApproval = @EmailForOrderApproval
		, EmailForOrderConfirmation = @EmailForOrderConfirmation
		, EmailForFaxConfirmation = @EmailForFaxConfirmation
		, EmailForBillingDispense = @EmailForBillingDispense
		, EmailForPrintDispense = @EmailForPrintDispense
		, FaxForPrintDispense = @FaxForPrintDispense
		, EmailForLowInventoryAlerts = @EmailForLowInventoryAlerts
		, IsEmailForLowInventoryAlertsOn = @IsEmailForLowInventoryAlertsOn
		, ModifiedUserID = @ModifiedUserID
		, ModifiedDate = GETDATE()

	WHERE 
		PracticeLocationID = @PracticeLocationID

	SET @Err = @@ERROR

	RETURN @Err
End
