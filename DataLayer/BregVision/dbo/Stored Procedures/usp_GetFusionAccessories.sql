﻿-- =============================================
-- Author:		Michael Sneen
-- Create date: May 20,2009
-- Description:	Used to get all Fusion Accessories for the Custom Brace form on POShipBillCustomBrace.aspx
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetFusionAccessories]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT 
	   [MasterCatalogProductID]
      ,[Code]
      ,[Name]
      ,[ShortName]
      ,[Size]
      ,[Color]
      ,[Gender]
      ,[WholesaleListCost]
      ,[Description]
      ,[IsActive]
  FROM [dbo].[MasterCatalogProduct]
  where (
		[Code] = '22000'
	OR	[Code] = '22001'
	OR	[Code] like '0985%'
	OR	[Code] like '0735%'
	OR	[Code] like '1008%')
	AND IsActive = 1
	
END
