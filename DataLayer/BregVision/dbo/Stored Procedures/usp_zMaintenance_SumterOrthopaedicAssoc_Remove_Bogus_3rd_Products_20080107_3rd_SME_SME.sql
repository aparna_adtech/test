﻿
/*
	Sumter OrthopaedicAssoc #95; Remove bogus 3rd products(SME/SME) Supplier/Brand and products (set to inactive).
	John Bongiorni   2008.01.03
*/

CREATE PROC [dbo].[usp_zMaintenance_SumterOrthopaedicAssoc_Remove_Bogus_3rd_Products_20080107_3rd_SME_SME]
AS 
BEGIN
	

--  Get practiceID for CenterForOthopediacs.
--  NOTE: BEFORE REUSING CHECK ALL CODE THIS IS FOR USE
--  ONLY WITH A ONE LOCATION Practice!!!!!!

SELECT * FROM PRACTICE WHERE PracticeID = 22
--  Above is Correct?  Yes.

SELECT * 
FROM PracticeLocation AS PL--  Center for Orthopaedics has only one location.
WHERE PL.PracticeID = 22  --  1500 Pleasant Valley Way
	AND PL.PracticeLocationID = 58


--  Get PracticeCatalogSupplierBrandIDs for the bogus Suppliers.
SELECT *
FROM dbo.PracticeCatalogSupplierBrand AS pcsb
WHERE pcSB.PracticeID = 22    --		Practice ID Set HERE!
AND IsThirdPartySupplier = 1
AND MasterCatalogSupplierID IS NULL
AND PracticeCatalogSupplierBrandID IN (259)  --  Set PCSBID HERE for bogus MC supplierbrands.

--  259	22	NULL	64	SME	SME	SME	SME	1	999	0	2007-12-19 09:17:48.373	0	2008-01-04 06:16:13.513	1

--  DIFFERENT  Select bogus PCproducts of the bogus supplier sme sme --  4 records
SELECT PracticeCatalogProductID,
	   PracticeID,
	   PCP.PracticeCatalogSupplierBrandID,
	   PCP.MasterCatalogProductID,
	   PCP.ThirdPartyProductID,
	   IsThirdPartyProduct,
	   WholesaleCost,
	   BillingCharge,
	   DMEDeposit,
	   BillingChargeCash,
	   Sequence,
	   PCP.CreatedUserID,
	   PCP.CreatedDate,
	   PCP.ModifiedUserID,
	   PCP.ModifiedDate,
	   PCP.IsActive
FROM dbo.PracticeCatalogProduct AS PCP
INNER JOIN dbo.ThirdPartyProduct AS TPP		--3rd
	ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID  --3rd
WHERE PCP.PracticeID = 22    --		Practice ID Set HERE!
	AND PCP.PracticeCatalogSupplierBrandID IN (259)  --  Set PCSBID HERE for bogus MC supplierbrands.
	AND PCP.IsThirdPartyProduct = 1  -- 3rd
	AND PCP.PracticeCatalogProductID IN				-- 3rd
		(
			  4864
			, 4865
			, 4866
			, 4867
		)

	--  AND PCP.IsActive = 1
ORDER BY
	PCP.PracticeCatalogSupplierBrandID
	, PCP.MasterCatalogProductID
	
--4864	22	259	NULL	1825	1	0.00	0.00	0.00	0.00	NULL	-13	2007-12-20 20:19:48.310	NULL	NULL	1
--4865	22	259	NULL	1826	1	0.00	0.00	0.00	0.00	NULL	-13	2007-12-20 20:19:48.310	NULL	NULL	1
--4866	22	259	NULL	1827	1	0.00	0.00	0.00	0.00	NULL	-13	2007-12-20 20:19:48.310	NULL	NULL	1
--4867	22	259	NULL	1828	1	0.00	0.00	0.00	0.00	NULL	-13	2007-12-20 20:19:48.310	NULL	NULL	1	

/*
	Check the SupplierOrderLineItem table 
	and the ProductInventory table
*/

--  Check the SupplierOrderLineItem table for the bogus product inventory.
SELECT *
FROM dbo.SupplierOrderLineItem  -- zero records.
WHERE PracticeCatalogProductID IN
		(
			  4864
			, 4865
			, 4866
			, 4867
		)

--  NONE from above.

--  Check Inventory Table for the bogus PCProducts.  --  83 records
SELECT * 
FROM dbo.ProductInventory AS PI
WHERE PI.PracticeCatalogProductID IN
		(
			  4864
			, 4865
			, 4866
			, 4867
		)
--  four FROM ABOVE.
	


select * 
from dbo.ProductInventory AS PI
WHERE  --  AND PI.PracticeCatalogProductID = 4722  --  This is not active. and has no dependencies below
	PI.PracticeCatalogProductID IN
		(
			  4864
			, 4865
			, 4866
			, 4867
		)

	AND PI.ProductInventoryID IN
		(
			 11736
			, 11735
			, 11734
			, 11737
		)

--11736	58	4864	0	0	0	0	1	2008-01-02 20:43:51.233	NULL	NULL	0
--11735	58	4865	0	0	0	0	1	2008-01-02 20:43:51.233	NULL	NULL	0
--11734	58	4866	0	0	0	0	1	2008-01-02 20:43:51.233	NULL	NULL	0
--11737	58	4867	0	0	0	0	1	2008-01-02 20:43:51.233	NULL	NULL	0

-- HARD DELETE due to time constaints!
begin tran

--DELETE from dbo.ProductInventory 
--WHERE PracticeCatalogProductID IN
--		(
--			  4864
--			, 4865
--			, 4866
--			, 4867
--		)  --  This is not active. and has no dependencies below
--AND ProductInventoryID IN
--		(
--			 11736
--			, 11735
--			, 11734
--			, 11737
--		)


select * 
from dbo.ProductInventory AS PI
WHERE  --  AND PI.PracticeCatalogProductID = 4722  --  This is not active. and has no dependencies below
	PI.PracticeCatalogProductID IN
		(
			  4864
			, 4865
			, 4866
			, 4867
		)

	AND PI.ProductInventoryID IN
		(
			 11736
			, 11735
			, 11734
			, 11737
		)

rollback tran
-- ABOVE FOUR record were DELETED.


begin tran

select * from dbo.PracticeCatalogProduct 
WHERE PracticeCatalogProductID IN
		(
			  4864
			, 4865
			, 4866
			, 4867
		)


--DELETE from dbo.PracticeCatalogProduct 
--WHERE PracticeCatalogProductID IN
--		(
--			  4864
--			, 4865
--			, 4866
--			, 4867
--		)

select * from dbo.PracticeCatalogProduct 
WHERE PracticeCatalogProductID IN
		(
			  4864
			, 4865
			, 4866
			, 4867
		)


rollback tran

-- ABOVE FOUR record were DELETED.

begin tran

SELECT * 
from practicecatalogsupplierbrand 
where practicecatalogsupplierbrandID = 259
--  259	22	NULL	64	SME	SME	SME	SME	1	999	0	2007-12-19 09:17:48.373	0	2008-01-04 06:16:13.513	1

--UPDATE practicecatalogsupplierbrand 
--SET isactive = 0
--where practicecatalogsupplierbrandID = 259

SELECT * 
from practicecatalogsupplierbrand 
where practicecatalogsupplierbrandID = 259

rollback tran
-- ABOVE one record was inactivated.

--  NONE OF THE FOLLOWING CODE HAS BEEN EXECUTED or used FOR THIS!!!!  2007.01.03

--------/*
--------	Check all ProductInventory tables for the bogus product inventory records.
--------	Create table variable, @BogusProductInventory, to hold the bogus
--------	 ProductInventoryIDs.
--------*/
--------	
--------DECLARE @BogusProductInventory TABLE 
--------	(ProductInventoryID INT)	
--------	
--------INSERT INTO @BogusProductInventory
--------	(ProductInventoryID)
--------SELECT ProductInventoryID
--------FROM dbo.ProductInventory AS PI			--  This will select inventory from ALL LOCATIONS!
--------WHERE PI.PracticeCatalogProductID IN
--------		(
--------			SELECT PracticeCatalogProductID
--------			FROM dbo.PracticeCatalogProduct AS PCP
--------			WHERE PCP.PracticeID = 23								 --	Practice ID Set HERE!
--------				AND PCP.PracticeCatalogSupplierBrandID IN (263, 264)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
--------				AND PCP.IsThirdPartyProduct = 0
--------				AND PCP.IsActive = 1		
--------		)
--------			
--------SELECT DISTINCT ProductInventoryID
--------FROM @BogusProductInventory
--------ORDER BY ProductInventoryID	
--------
----------  NONE ABOVE.
--------		
----------  Check the ProductInventory_OrderCheckIn table for the bogus product inventory.
--------
--------SELECT *
--------FROM dbo.ProductInventory_OrderCheckIn  
--------WHERE ProductInventoryID IN 
--------				(
--------					SELECT DISTINCT ProductInventoryID
--------					FROM @BogusProductInventory
--------				)		
--------		
----------	   (				
----------		SELECT ProductInventoryID
----------		FROM dbo.ProductInventory AS PI
----------		WHERE PI.PracticeCatalogProductID IN
----------				(
----------					SELECT PracticeCatalogProductID
----------					FROM dbo.PracticeCatalogProduct AS PCP
----------					WHERE PCP.PracticeID = 11
----------						AND PCP.PracticeCatalogSupplierBrandID IN (109, 111)
----------						AND PCP.IsThirdPartyProduct = 0
----------						AND PCP.IsActive = 1		
----------				)
----------		)
--------
--------
----------  Check the ProductInventory_DispenseDetail table for the bogus product inventory.
--------
--------SELECT *
--------FROM dbo.ProductInventory_DispenseDetail  
--------WHERE ProductInventoryID IN 		
--------				(
--------					SELECT DISTINCT ProductInventoryID
--------					FROM @BogusProductInventory
--------				)		
--------
--------		
--------
----------  Check the ProductInventory_QuantityOnHandPhysical table for the bogus product inventory.
--------
--------SELECT *
--------FROM dbo.ProductInventory_QuantityOnHandPhysical
--------WHERE ProductInventoryID IN 		
--------				(
--------					SELECT DISTINCT ProductInventoryID
--------					FROM @BogusProductInventory
--------				)		
--------	
----------  Check the ProductInventory_Purge table for the bogus product inventory.
--------
--------SELECT *
--------FROM dbo.ProductInventory_Purge
--------WHERE ProductInventoryID IN 		
--------				(
--------					SELECT DISTINCT ProductInventoryID
--------					FROM @BogusProductInventory
--------				)		
--------
----------  Check the ProductInventory_ManualCheckIn table for the bogus product inventory.
--------
--------SELECT *
--------FROM dbo.ProductInventory_ManualCheckIn
--------WHERE ProductInventoryID IN 		
--------				(
--------					SELECT DISTINCT ProductInventoryID
--------					FROM @BogusProductInventory
--------				)		
--------			
--------/*
--------	Summary:
--------	 2 bogus suppliers for PracticeCatalogSupplierBrandID
--------	 3 bogus records in PracticeCatalogProducts  
--------	 2 bogus records in PracticeCatalogSupplierBrand  
--------*/
--------
--------
--------
--------SELECT *
--------FROM dbo.PracticeCatalogProduct AS PCP
--------WHERE IsActive = 0
--------
--------
-------------------------
----------SELECT * 
----------INTO zDeleted_ProductInventory 
----------FROM dbo.ProductInventory AS PI
----------WHERE PI.PracticeCatalogProductID IN
----------		(
----------			SELECT PracticeCatalogProductID
----------			FROM dbo.PracticeCatalogProduct AS PCP
----------			WHERE PCP.PracticeID = 11
----------				AND PCP.PracticeCatalogSupplierBrandID IN (109, 111)
----------				AND PCP.IsThirdPartyProduct = 0
----------				AND PCP.IsActive = 1		
----------		)
------------ Note: Add DateDeleted column update the 83 rows with GetDate for 
------------			the DateDeleted.
--------
--------
---------- Delete the 83 bogus records from ProductInventory.
----------BEGIN TRAN	
----------	
----------DELETE
----------FROM dbo.ProductInventory 
----------WHERE PracticeCatalogProductID IN
----------		(
----------			SELECT PracticeCatalogProductID
----------			FROM dbo.PracticeCatalogProduct AS PCP
----------			WHERE PCP.PracticeID = 11
----------				AND PCP.PracticeCatalogSupplierBrandID IN (109, 111)
----------				AND PCP.IsThirdPartyProduct = 0
----------				--AND PCP.IsActive = 1		
----------		)
----------		
----------ROLLBACK TRAN
--------		
---------- Note: Add DateDeleted column update the 83 rows with GetDate for 
----------			the DateDeleted.
--------
--------
--------SELECT *
--------FROM PracticeCatalogProduct AS PCP
--------WHERE PCP.IsActive = 0
--------
--------
--------
----------  Update the 3 bogus PCP records by setting their isActive flag to false
--------
--------begIN TRAN
--------
--------		SELECT *
--------	FROM dbo.PracticeCatalogProduct AS PCP
--------	WHERE PCP.PracticeID = 23									--	Practice ID Set HERE!
--------		AND PCP.PracticeCatalogSupplierBrandID IN (263, 264)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
--------		AND PCP.IsThirdPartyProduct = 0
--------		  AND PCP.IsActive = 1
----------4722	23	263	2126	NULL	0	45.00	154.00	0.00	0.00	NULL	1	2007-12-20 11:08:21.077	NULL	NULL	1
----------4990	23	264	8948	NULL	0	23.95	0.00	NULL	0.00	NULL	1	2007-12-21 07:09:07.247	NULL	NULL	1
----------4991	23	264	8949	NULL	0	23.95	0.00	NULL	0.00	NULL	1	2007-12-21 07:09:07.247	NULL	NULL	1
--------
--------	--  21 inactive
--------	SELECT *
--------	FROM dbo.PracticeCatalogProduct AS PCP
--------	WHERE PCP.PracticeID = 23									--	Practice ID Set HERE!
--------		AND PCP.PracticeCatalogSupplierBrandID IN (263, 264)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
--------		AND PCP.IsThirdPartyProduct = 0
--------		  AND PCP.IsActive = 0
--------	ORDER BY
--------		PCP.PracticeCatalogSupplierBrandID
--------		, PCP.MasterCatalogProductID
--------
--------	-- Setting all 3 products to inactive.
----------	UPDATE PracticeCatalogProduct      -- 14 Rows Updated.
----------	SET IsActive = 0
----------	WHERE PracticeID = 23									--	Practice ID Set HERE!
----------		AND PracticeCatalogSupplierBrandID IN (263, 264)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
----------		AND IsThirdPartyProduct = 0
--------
--------
--------	--  Zero are Active, 21 are inactive.
--------	SELECT *
--------	FROM dbo.PracticeCatalogProduct AS PCP
--------	WHERE PCP.PracticeID = 23									--	Practice ID Set HERE!
--------		AND PCP.PracticeCatalogSupplierBrandID IN  (263, 264)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
--------		AND PCP.IsThirdPartyProduct = 0
--------		  AND PCP.IsActive = 1
--------
--------	--  21 inactive
--------	SELECT *
--------	FROM dbo.PracticeCatalogProduct AS PCP
--------	WHERE PCP.PracticeID = 23									--	Practice ID Set HERE!
--------		AND PCP.PracticeCatalogSupplierBrandID IN (263, 264)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
--------		AND PCP.IsThirdPartyProduct = 0
--------		  AND PCP.IsActive = 0
--------	ORDER BY
--------		PCP.PracticeCatalogSupplierBrandID
--------		, PCP.MasterCatalogProductID
--------
--------
--------
----------rOLLBACK TRAN  --COMMIT TRAN  --  
--------
--------
----------  After the PCProducts have been deactivated, deactivate the bogus supplierBrands.
--------
--------
--------SELECT *
--------FROM dbo.PracticeCatalogSupplierBrand AS pcsb
--------WHERE pcSB.PracticeID = 23							--	Practice ID Set HERE!
--------AND IsThirdPartySupplier = 0
--------AND MasterCatalogSupplierID IS NOT NULL
--------AND PracticeCatalogSupplierBrandID IN (263, 264)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
--------AND IsActive = 1
----------263	23	4	NULL	Bauerfeind	Bauerfeind	Bauerfeind	Bauerfeind	0	999	1	2007-12-20 11:08:21.077	NULL	NULL	1
----------264	23	8	NULL	Hely Weber	HW	Hely Weber	HW	0	999	1	2007-12-21 07:09:07.247	NULL	NULL	1
--------
---------- !!!!CANNOT REMOVE the bogus supplierbrands (MC Bauerfiend and MC HW) from THE APPLICATION; CenterForOrthopediacs, Admin, Supplier, Brand, since they are MC Suppliers
----------		Add to not have the ability to be removed.
--------
--------BEGIN TRAN
--------
--------	SELECT *
--------	FROM dbo.PracticeCatalogSupplierBrand AS pcsb
--------	WHERE pcSB.PracticeID = 23							--	Practice ID Set HERE!
--------	AND IsThirdPartySupplier = 0
--------	AND MasterCatalogSupplierID IS NOT NULL
--------	AND PracticeCatalogSupplierBrandID IN (263, 264)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
--------	AND IsActive = 0
--------
--------
--------	-- uPDATE BOGUS set the PCSuppplierBrand.IsActive to 0
----------	UPDATE dbo.PracticeCatalogSupplierBrand 
----------	SET IsActive = 0
----------	WHERE PracticeID = 23							--	Practice ID Set HERE!
----------	AND IsThirdPartySupplier = 0
----------	AND MasterCatalogSupplierID IS NOT NULL
----------	AND PracticeCatalogSupplierBrandID IN (263, 264)  --  Set PCSBID HERE for bogus MC supplierbrands.  
----------	AND IsActive = 1
--------
--------	SELECT *
--------	FROM dbo.PracticeCatalogSupplierBrand AS pcsb
--------	WHERE pcSB.PracticeID = 23							--	Practice ID Set HERE!
--------	AND IsThirdPartySupplier = 0
--------	AND MasterCatalogSupplierID IS NOT NULL
--------	AND PracticeCatalogSupplierBrandID IN (263, 264)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
--------	AND IsActive = 0
--------
--------ROLLBACK TRAN

END