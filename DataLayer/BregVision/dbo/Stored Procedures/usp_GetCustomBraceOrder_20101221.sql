﻿
-- =============================================
-- Author:		<Author: Mike Sneen>
-- Create date: <Create Date: 02/28/09>
-- Description:	<Description: Used to insert into ShoppingCartCustomBrace>
-- modification:
--  [dbo].[usp_GetCustomBraceOrder]  
--EXEC usp_GetCustomBraceOrder 8859
--Exec usp_GetCustomBraceOrder @ShoppingCartID = 9102
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCustomBraceOrder_20101221]  --3 --10  --  _With_Brands
   @ShoppingCartID int = null,
   @BregVisionOrderID int = null
AS 
    BEGIN
    DECLARE @Err INT
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	       SET NOCOUNT ON ;
	       
SELECT 
	   SCSB.[ShoppingCartID]
	  ,SCSB.[BregVisionOrderID]
      ,SCSB.[Billing_CustomerNumber]
      ,SCSB.[Billing_Phone]
      ,SCSB.[Billing_Attention]
      ,SCSB.[Billing_Address1]
      ,SCSB.[Billing_Address2]
      ,SCSB.[Billing_City]
      ,SCSB.[Billing_State]
      ,SCSB.[Billing_Zip]
      ,SCSB.[Shipping_Attention]
      ,SCSB.[Shipping_Address1]
      ,SCSB.[Shipping_Address2]
      ,SCSB.[Shipping_City]
      ,SCSB.[Shipping_State]
      ,SCSB.[Shipping_Zip]
      ,SCSB.[PatientName]
      ,SCSB.[PhysicianID]
      ,PhysicianName = (Select C.FirstName + ' ' + C.LastName from Physician P join Contact C on P.ContactID = C.ContactID Where P.PhysicianID = SCSB.[PhysicianID])
      ,SCSB.[Patient_Age]
      ,SCSB.[Patient_Weight]
      ,SCSB.[Patient_Height]
      ,SCSB.[Patient_Sex]
      ,SCSB.[BraceFor]
      ,SCSB.[Instability]
      ,SCSB.[BilatCastReform]
      ,SCSB.[ThighCircumference]
      ,SCSB.[CalfCircumference]
      ,SCSB.[KneeOffset]
      ,SCSB.[KneeWidth]
      ,SCSB.[Extension]
      ,SCSB.[Flexion]
      ,SCSB.[MeasurementsTakenBy]
      ,SCSB.[X2K_Counterforce]
      ,SCSB.[X2K_Counterforce_Degrees]
      ,SCSB.[X2K_FramePadColor1]
      ,SCSB.[X2K_FramePadColor2]
      ,SCSB.[Fusion_Enhancement]
      ,SCSB.[Fusion_Notes]
      ,SCSB.[Fusion_Color]
      ,SCSB.[Fusion_Pantone]
      ,SCSB.[Fusion_Pattern]
      ,SCSB.[Fusion_Pattern_Notes]
      ,SCSB.[ShippingTypeID]
      ,ShippingTypeName = (Select st.ShippingTypeName from ShippingType st where shippingTypeID = SCSB.[ShippingTypeID])
      ,SCSB.[ShippingCarrierID]
      ,ShippingCarrierName = (Select sc.ShippingCarrierName from ShippingCarrier sc where shippingCarrierID = SCSB.[ShippingCarrierID])
  FROM [ShoppingCartCustomBrace] SCSB
  Where 
	SCSB.[ShoppingCartID] = coalesce(@ShoppingCartID, SCSB.[ShoppingCartID])
	--AND (((@BregVisionOrderID IS NOT NULL) AND (SCSB.[BregVisionOrderID] = @BregVisionOrderID)) OR (@BregVisionOrderID IS NULL))
    AND COALESCE(SCSB.[BregVisionOrderID], '') = COALESCE(@BregVisionOrderID, SCSB.[BregVisionOrderID], '')
     	SET @Err = @@ERROR

	RETURN @Err

    END

--Exec usp_SuperbillCategory_Insert 3, 3