﻿

-- =============================================
-- Author:		<Author: Greg Ross>
-- Create date: <Create Date: 07/20/07>
-- Description:	<Description: Used to populate the last 10 items dispensed >
--  MODIFATION:  20071015 JB Left JOin Physician and Contact remove is Active for these columns.
--				20071015 JB Add Third Party Products
--				20080218 JB Format Date Dispensed
--				20080220 JB Order by full Created Date (with time), but only display the dispensed date.
-- =============================================


CREATE PROCEDURE [dbo].[usp_GetLast10ItemsDispensed1]     --1, 3   --6, 15  -- 1, 3
	-- Add the parameters for the stored procedure here
    @PracticeID INT = 0,
    @PracticeLocationID int
AS 
    BEGIN
SELECT TOP 20

	SUB.DISPENSEID,
	SUB.DispenseDetailID,
    SUB.SupplierName,
    SUB.name,
    SUB.Code,

    SUB.Quantity,
    
    SUB.FirstName,
    SUB.LastName,
   
    SUB.LineTotal,
    SUB.Total,			                
--    SUB.DateDispensed,
	SUB.CreatedDate,

	CONVERT(CHAR(10), DateDispensed,110) AS DateDispensed,  --  20080218 JB  

	SUB.WholesaleCost

--  select CONVERT(CHAR(10),GETDATE(),110) 

FROM
   ( SELECT TOP 10
				D.DISPENSEID,
				dd.DispenseDetailID,
                MCS.[SupplierName],
                MCP.name,
                MCP.[Code],

                DD.[Quantity],
                
                ISNULL(C.[FirstName], '') AS FirstName,
                ISNULL(C.[LastName], '') AS LastName,
               
                DD.ActualChargeBilled AS Total,
                DD.[LineTotal] AS LineTotal,			                
                [DateDispensed],
				
				DD.CreatedDate AS CreatedDate,  --  JB 20080220 
				
				PCP.WholesaleCost
                
        FROM    dispense D
        
                INNER JOIN DispenseDetail DD 
					ON D.[DispenseID] = DD.[DispenseID]
					
                LEFT JOIN [Physician] Phy 
					ON Dd.[PhysicianID] = Phy.[PhysicianID]
					AND Phy.IsActive = 1
					
                LEFT  JOIN [Contact] 
					C ON Phy.[ContactID] = c.[ContactID]
					AND C.IsActive = 1
					
                INNER JOIN [PracticeCatalogProduct] PCP ON 
					DD.[PracticeCatalogProductID] = PCP.[PracticeCatalogProductID]
					
--                INNER JOIN [Practice] P 
--					ON PCP.[PracticeID] = P.[PracticeID]

                INNER JOIN [PracticeLocation] PL 
					ON D.[PracticeLocationID] = PL.[PracticeLocationID]
					
                INNER JOIN [MasterCatalogProduct] MCP 
					ON PCP.[MasterCatalogProductID] = MCP.[MasterCatalogProductID]
					
                INNER JOIN [MasterCatalogSubCategory] MCSC 
					ON MCP.[MasterCatalogSubCategoryID] = MCSC.[MasterCatalogSubCategoryID]
					
                INNER JOIN [MasterCatalogCategory] MCC 
					ON MCSC.[MasterCatalogCategoryID] = MCC.[MasterCatalogCategoryID]
					
                INNER JOIN [MasterCatalogSupplier] MCS 
					ON MCc.[MasterCatalogSupplierID] = MCS.[MasterCatalogSupplierID]
        WHERE   
				--  p.[PracticeID] = @PracticeID
                --  AND 
                pl.[PracticeLocationID] = @PracticeLocationID
                AND D.[IsActive] = 1
                AND DD.[IsActive] = 1
                --  AND PHY.[IsActive] = 1
                --  AND C.[IsActive] = 1
                AND PCP.[IsActive] = 1
                --AND P.[IsActive] = 1
                AND PL.[IsActive] = 1
                AND MCSC.[IsActive] = 1
                AND MCC.[IsActive] = 1
                AND MCS.[IsActive] = 1
        ORDER BY [DateDispensed] DESC,
                suppliername,
                [MCP].name 
                
UNION


SELECT TOP 10
				D.DISPENSEID,
				dd.DispenseDetailID,
                PCSB.[SupplierName],
                TPP.name,
                TPP.[Code],

                DD.[Quantity],
                
                ISNULL(C.[FirstName], '') AS FirstName,
                ISNULL(C.[LastName], '') AS LastName,
               
                DD.ActualChargeBilled AS Total,
                DD.[LineTotal] AS LineTotal,			                
                [DateDispensed],
				
				DD.CreatedDate AS CreatedDate,  --  JB 20080220 
				
				PCP.WholesaleCost
                
        FROM    
				dispense D
				
                INNER JOIN DispenseDetail DD 
					ON D.[DispenseID] = DD.[DispenseID]
					
                LEFT JOIN [Physician] Phy 
					ON Dd.[PhysicianID] = Phy.[PhysicianID]
					AND Phy.IsActive = 1
					
                LEFT  JOIN [Contact] 
					C ON Phy.[ContactID] = c.[ContactID]
					AND C.IsActive = 1
					
                INNER JOIN [PracticeCatalogProduct] PCP ON 
					DD.[PracticeCatalogProductID] = PCP.[PracticeCatalogProductID]

				
				INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
					ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
					
--                INNER JOIN [Practice] P 
--					ON PCP.[PracticeID] = P.[PracticeID]
					
                INNER JOIN [PracticeLocation] PL 
					ON D.[PracticeLocationID] = PL.[PracticeLocationID]
					
					
                INNER JOIN [ThirdPartyProduct] TPP 
					ON PCP.[ThirdPartyProductID] = TPP.[ThirdPartyProductID]
					
        WHERE   
				--p.[PracticeID] = @PracticeID
                --AND 
                pl.[PracticeLocationID] = @PracticeLocationID
                AND D.[IsActive] = 1
                AND DD.[IsActive] = 1
                --  AND PHY.[IsActive] = 1
                --  AND C.[IsActive] = 1
                AND PCP.[IsActive] = 1
                AND PCSB.IsActive = 1
                -- AND P.[IsActive] = 1
                AND PL.[IsActive] = 1
                
        ORDER BY DD.CreatedDate DESC,
				[DateDispensed] DESC,
                suppliername,
                [TPP].name                 
                
      ) AS SUB
      ORDER BY
			SUB.CreatedDate DESC,
			SUB.DateDispensed DESC,
            SUB.suppliername,
            SUB.name    	
    END
    
--SELECT * FROM DISPENSE WHERE DISPENSEid = 57
--SELECT * FROM DISPENSEDETAIL WHERE DISPENSEid = 57




