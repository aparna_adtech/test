﻿/*
SELECT ID 
			 FROM dbo.udf_ParseArrayToTable('3,5', ',')

DECLARE @PracticeLocationIDString VARCHAR(2000)
SET @PracticeLocationIDString = '3,5'

select * from practicelocation as pl where
		PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))

*/



CREATE PROC [dbo].[usp_zzzProductsPurchased_By_ProductCodeAndCost_Given_PracticeID_PLID_Supplier2] 
		@PracticeID	INT
		, @PracticeLocationID VARCHAR(2000)
		, @SupplierShortName  VARCHAR(50)
AS
BEGIN

--	DECLARE @PracticeID INT
--	SET @PracticeID = 6

INSERT INTO zzzReportMessage (Message)
SELECT @PracticeLocationID



DECLARE @PracticeLocationIDString VARCHAR(2000)
SET @PracticeLocationIDString = @PracticeLocationID

INSERT INTO zzzReportMessage (Message)
SELECT @PracticeLocationIDString 

--  SELECT * FROM zzzReportMessage

SELECT 
		 SUB.PracticeName
		, SUB.PracticeLocation
--		, SUB.IsThirdPartySupplier
		, SUB.SupplierShortName
		, SUB.BrandShortName
		, SUB.Sequence

		, SUB.ProductName
		, SUB.Side
		, SUB.Size
		, SUB.Gender
		, SUB.Code
		
		, SUB.ActualWholesaleCost
		, SUM(SUB.QuantityOrdered) AS QuantityOrdered
		, SUM(SUB.LineTotal)	   AS TotalCost
		
--		, SUB.OrderDate
FROM
	(SELECT 
		  P.PracticeName
		, PL.NAME AS PracticeLocation
		, PCSB.IsThirdPartySupplier
		, PCSB.SupplierShortName
		, PCSB.BrandShortName
		, PCSB.Sequence

		, MCP.[Name] AS ProductName
		, MCP.LeftRightSide AS Side
		, MCP.Size AS Size
		, MCP.Gender AS Gender
		, MCP.Code AS Code
		
		, SOLI.ActualWholesaleCost
		, SOLI.QuantityOrdered
		, SOLI.LineTotal

		, SOLI.CreatedDate AS OrderDate		
		
	FROM dbo.SupplierOrderLineItem AS SOLI WITH (NOLOCK)
	INNER JOIN dbo.SupplierOrder AS SO	WITH (NOLOCK)
		ON SOLI.SupplierOrderID = SO.SupplierOrderID
	INNER JOIN dbo.BregVisionOrder AS BVO	WITH (NOLOCK)
		ON BVO.BregVisionOrderID = SO.BregVisionOrderID
	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON BVO.PracticeLocationID = PL.PracticeLocationID
	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID
	INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
		ON SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
	INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
		ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
	WHERE P.PracticeID = @PracticeID		
		
		AND 
		PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))

		--  AND PL.PracticeLocationID = @PracticeLocationID
		AND PCSB.SupplierShortName = @SupplierShortName
	
	UNION
	
		SELECT 
		  P.PracticeName
		, PL.NAME AS PracticeLocation
		, PCSB.IsThirdPartySupplier
		, PCSB.SupplierShortName
		, PCSB.BrandShortName
		, PCSB.Sequence
		
		, TPP.[Name] AS ProductName
		, TPP.LeftRightSide AS Side
		, TPP.Size AS Size
		, TPP.Gender AS Gender
		, TPP.Code AS Code
		
		, SOLI.ActualWholesaleCost
		, SOLI.QuantityOrdered
		, SOLI.LineTotal
		
		, SOLI.CreatedDate AS OrderDate
				
		
	FROM dbo.SupplierOrderLineItem AS SOLI	WITH (NOLOCK)
	INNER JOIN dbo.SupplierOrder AS SO	WITH (NOLOCK)
		ON SOLI.SupplierOrderID = SO.SupplierOrderID
	INNER JOIN dbo.BregVisionOrder AS BVO	WITH (NOLOCK)
		ON BVO.BregVisionOrderID = SO.BregVisionOrderID
	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON BVO.PracticeLocationID = PL.PracticeLocationID
	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID
	INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
		ON SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
	INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
		ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
	WHERE 
		P.PracticeID = @PracticeID	

		AND PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))

		AND PCSB.SupplierShortName = @SupplierShortName

	
	)		
		AS SUB	
		
	GROUP BY
		SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.IsThirdPartySupplier
		, SUB.Sequence
		, SUB.SupplierShortName
		, SUB.BrandShortName
		
		, SUB.ProductName
		, SUB.Side
		, SUB.Size
		, SUB.Gender
		, SUB.Code
		
		, SUB.ActualWholesaleCost
		
	ORDER BY
		SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.IsThirdPartySupplier
		, SUB.Sequence
		, SUB.SupplierShortName
		, SUB.BrandShortName
		, SUB.ProductName
		, SUB.Code
		
END