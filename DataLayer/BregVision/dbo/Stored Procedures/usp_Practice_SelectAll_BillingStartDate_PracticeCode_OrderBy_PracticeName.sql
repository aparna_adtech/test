﻿/*
	dbo.usp_Practice_SelectAll_BillingStartDate_PracticeCode
	
	Get all Practices (except test practices) with their billingstartdates
	and oracle practice codes (supplied by Accounting)
	Send this to Bobby Haywood monthly to update
	
	John Bongiorni
	2008.01.28
	

*/


CREATE PROC [dbo].[usp_Practice_SelectAll_BillingStartDate_PracticeCode_OrderBy_PracticeName]
AS 
BEGIN

	SELECT
		 P.PracticeID
		 , P.PracticeName
		 , CONVERT(VARCHAR(50), P.BillingStartDate, 101) AS BillingStartDate  
		, BVPC.PracticeCode
	FROM Practice AS P	WITH (NOLOCK)
	LEFT JOIN dbo.BregVisionPracticeCode AS BVPC	WITH (NOLOCK)
		ON P.PracticeID = BVPC.PracticeID
	WHERE P.IsActive = 1
		AND P.PracticeID NOT IN (1,3)  -- Test Practices
	ORDER BY 
--		  P.BillingStartDate
--		, 
		  P.PracticeName
	
END 	