﻿--  usp_PracticeCatalogProduct_Delete
/*
	Proc:		usp_PracticeCatalogProduct_Delete
	Author:		Mike Sneen
	Date:		8/26/2010
	
	Description:  Deactivate the PracticeCatalogProduct
				
	
	
*/
  CREATE PROC [dbo].[usp_PracticeCatalogProduct_Delete]  --  1000000, -20080225, 0, 'a' --1000000  -- 600, 2,  400
  		  @PracticeCatalogProductID	INT
		, @UserID					INT
		, @RemovalType				INT  OUTPUT
		, @DependencyMessage		VARCHAR(2000) OUTPUT  -- To application / user.
		, @IsAdmin					BIT = 0

AS 
BEGIN

	DECLARE @ProductInventoryCount			INT  --  Number of PI records for the practice and PCP.

	IF(@IsAdmin = 1)
		EXEC usp_PracticeCatalogProduct_Admin_Delete @PracticeCatalogProductID, @UserID, @RemovalType OUTPUT, @DependencyMessage OUTPUT
	ELSE
	BEGIN --else
		BEGIN TRY
		
		--  1. Check the ProductInventory table.
		SELECT
			@ProductInventoryCount = 
				COUNT(PI.ProductInventoryID)	--AS ProductInventoryCountAllLocations	--  Count of PI for the given PL and PCP
		FROM ProductInventory AS PI				WITH (NOLOCK)
		INNER JOIN PracticeLocation AS PL		WITH (NOLOCK)
			ON PI.PracticeLocationID = PL.PracticeLocationID
			AND PI.PracticeCatalogProductID = @PracticeCatalogProductID
		WHERE 
			PI.PracticeCatalogProductID = @PracticeCatalogProductID
			AND PL.IsActive = 1
		GROUP BY
			  PI.PracticeCatalogProductID		--  PracticeCatalogProductID given
		
		SELECT @ProductInventoryCount = ISNULL(@ProductInventoryCount, 0)  
		
		IF (@ProductInventoryCount = 0)   
		BEGIN

			BEGIN TRY

			BEGIN TRANSACTION

				--Set a Product to Inactive

				--In Shopping Cart
				UPDATE ShoppingCartItem
					SET IsActive = 0,
					ModifiedUserID = @UserID,
					ModifiedDate = GETDATE()
				where PracticeCatalogProductID = @PracticeCatalogProductID

				-- In ProductInventory
				UPDATE ProductInventory
					SET IsActive = 0,
					QuantityOnHandPerSystem = 0,
					ParLevel = 0,
					ReorderLevel = 0,
					CriticalLevel = 0,
					ModifiedUserID = @UserID,
					ModifiedDate = GETDATE()
				where PracticeCatalogProductID = @PracticeCatalogProductID

				--In ProductHCPCS
				UPDATE ProductHCPCS
					SET IsActive = 0,
					ModifiedUserID = @UserID,
					ModifiedDate = GETDATE()
				where PracticeCatalogProductID = @PracticeCatalogProductID

				--In ThirdPartyProduct
				UPDATE ThirdPartyProduct
					SET IsActive = 0,
					ModifiedUserID = @UserID,
					ModifiedDate = GETDATE()
				from
				ThirdPartyProduct TPP
				Inner Join PracticeCatalogProduct PCP
					on TPP.ThirdPartyProductID = PCP.ThirdPartyProductID
				Where PCP.PracticeCatalogProductID = @PracticeCatalogProductID

				--In PracticeCatalogProduct
				UPDATE dbo.PracticeCatalogProduct 
					SET IsActive = 0,
					ModifiedUserID = @UserID,
					ModifiedDate = GETDATE()		        
				WHERE PracticeCatalogProductID = @PracticeCatalogProductID

			COMMIT TRANSACTION
			
			SET @RemovalType = 1
			SET @DependencyMessage = ''
			
		END TRY
		BEGIN CATCH
			SELECT ISNULL(ERROR_NUMBER(), 0) as ErrorNumber
			IF (XACT_STATE() <> 0)
				ROLLBACK TRANSACTION
			SET @RemovalType = 0
			SET @DependencyMessage = 'Unable to deactivate this product'
		END CATCH
		END
		ELSE  -- @ProductInventoryCount is more than 0 --here
		  BEGIN
			-- SET the DependencyMessage. for PICount
			SET @RemovalType = 0
			PRINT 'Inventory' + CAST(@PracticeCatalogProductID AS VARCHAR(10))
			SET @DependencyMessage = @DependencyMessage + ', Inventory' 
		  END
		  
		END TRY

	BEGIN CATCH

	  EXECUTE [dbo].[uspLogError]
	  EXECUTE [dbo].[uspPrintError]
	  
	END CATCH	
	END --/else
END
  
  
  