﻿CREATE PROC usp_zGetInfo_PracticeLocation_EmailForOrderConfirmation
AS
BEGIN

	SELECT 
		  P.PracticeName
		, PL.NAME AS PracticeLocationName
		, EmailForOrderConfirmation 
	FROM dbo.PracticeLocation AS PL
	INNER JOIN dbo.Practice AS p
		ON PL.PracticeID = P.PracticeID
	WHERE PL.isActive = 1
	ORDER BY EmailForOrderConfirmation

END
