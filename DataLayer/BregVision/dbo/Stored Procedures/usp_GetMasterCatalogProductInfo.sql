﻿-- =============================================
-- Author:		Greg Ross
-- Create date: 09/05/2007
-- Description:	Gets all data from the masterCatalogProduct Tables
-- Modifications:  Order by MCSupplierID, ProductName, Code.
--   JB 2008.01.23  Not Done Should Combine LeftRightSide and Size as SideSize 
--   MWS 2009.01.30  Addedd Search by HCPC
-- 2016.01.19 Joe Hessburg from West Monroe Parnters added modifier fields
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetMasterCatalogProductInfo]

@SearchCriteria varchar(50),
@SearchText  varchar(50)

AS


BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
set @SearchText = '%' + @SearchText + '%'	
	if (@SearchCriteria='Browse' or @SearchCriteria='All' or @SearchCriteria='')
	begin
		select MCS.MasterCatalogSupplierID as SupplierID,
				MCP.MastercatalogProductID,
				MCP.MasterCatalogSubCategoryID,
				MCP.Code,
				MCP.ShortName,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				MCP.Mod1,
				MCP.Mod2,
				MCP.Mod3,
				MCP.WholesaleListCost,
				MCP.isLogoAblePart,
				MAX(COALESCE(PR.IsActive, 0)) AS IsReplaced
		 from mastercatalogSupplier MCS
					INNER JOIN [MasterCatalogCategory] MCC ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
					INNER JOIN [MasterCatalogSubCategory] MCSC ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
					INNER JOIN [MasterCatalogProduct] MCP ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
					LEFT OUTER JOIN [ProductReplacement] PR ON PR.IsActive = 1 AND PR.[OldMasterCatalogProductID] = MCP.[MasterCatalogProductID]
		where MCP.isactive=1
			and MCC.Isactive=1
			and MCSC.isactive=1
		GROUP BY
				MCS.MasterCatalogSupplierID,
				MCP.MastercatalogProductID,
				MCP.MasterCatalogSubCategoryID,
				MCP.Code,
				MCP.ShortName,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				MCP.Mod1,
				MCP.Mod2,
				MCP.Mod3,
				MCP.WholesaleListCost,
				MCP.isLogoAblePart
		ORDER BY 
			MCS.MasterCatalogSupplierID,
--			MCC.Sequence, 
--			MCS.Sequence,
			MCP.ShortName,
			MCP.Code
	end 

else if @SearchCriteria='Name'
	begin
		select MCS.MasterCatalogSupplierID as SupplierID,
				MCP.MastercatalogProductID,
				MCP.MasterCatalogSubCategoryID,
				MCP.Code,
				MCP.ShortName,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				MCP.Mod1,
				MCP.Mod2,
				MCP.Mod3,
				MCP.WholesaleListCost,
				MCP.isLogoAblePart,
				MAX(COALESCE(PR.IsActive, 0)) AS IsReplaced
		 from mastercatalogSupplier MCS
					INNER JOIN [MasterCatalogCategory] MCC ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
					INNER JOIN [MasterCatalogSubCategory] MCSC ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
					INNER JOIN [MasterCatalogProduct] MCP ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
					LEFT OUTER JOIN [ProductReplacement] PR ON PR.IsActive = 1 AND PR.[OldMasterCatalogProductID] = MCP.[MasterCatalogProductID]
		where MCP.isactive=1
			and MCC.Isactive=1
			and MCSC.isactive=1
			and (MCP.Name like @SearchText or MCP.ShortName like  @SearchText)
		GROUP BY
				MCS.MasterCatalogSupplierID,
				MCP.MastercatalogProductID,
				MCP.MasterCatalogSubCategoryID,
				MCP.Code,
				MCP.ShortName,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				MCP.Mod1,
				MCP.Mod2,
				MCP.Mod3,
				MCP.WholesaleListCost,
				MCP.isLogoAblePart
		ORDER BY 
			MCS.MasterCatalogSupplierID,
--			MCC.Sequence, 
--			MCS.Sequence,
			MCP.ShortName,
			MCP.Code

	end
else if @SearchCriteria='Code'
	begin
		select MCS.MasterCatalogSupplierID as SupplierID,
				MCP.MastercatalogProductID,
				MCP.MasterCatalogSubCategoryID,
				MCP.Code,
				MCP.ShortName,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				MCP.Mod1,
				MCP.Mod2,
				MCP.Mod3,
				MCP.WholesaleListCost,
				MCP.isLogoAblePart,
				MAX(COALESCE(PR.IsActive, 0)) AS IsReplaced
		 from mastercatalogSupplier MCS
					INNER JOIN [MasterCatalogCategory] MCC ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
					INNER JOIN [MasterCatalogSubCategory] MCSC ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
					INNER JOIN [MasterCatalogProduct] MCP ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
					LEFT OUTER JOIN [ProductReplacement] PR ON PR.IsActive = 1 AND PR.[OldMasterCatalogProductID] = MCP.[MasterCatalogProductID]
		where MCP.isactive=1
			and MCC.Isactive=1
			and MCSC.isactive=1
			and MCP.Code like @SearchText 
		GROUP BY 
				MCS.MasterCatalogSupplierID,
				MCP.MastercatalogProductID,
				MCP.MasterCatalogSubCategoryID,
				MCP.Code,
				MCP.ShortName,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				MCP.Mod1,
				MCP.Mod2,
				MCP.Mod3,
				MCP.WholesaleListCost,
				MCP.isLogoAblePart
		ORDER BY 
			MCS.MasterCatalogSupplierID,
--			MCC.Sequence, 
--			MCS.Sequence,
			MCP.ShortName,
			MCP.Code
	end
else if @SearchCriteria='Brand'
	begin
		select MCS.MasterCatalogSupplierID as SupplierID,
				MCP.MastercatalogProductID,
				MCP.MasterCatalogSubCategoryID,
				MCP.Code,
				MCP.ShortName,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				MCP.Mod1,
				MCP.Mod2,
				MCP.Mod3,
				MCP.WholesaleListCost,
				MCP.isLogoAblePart,
				MAX(COALESCE(PR.IsActive, 0)) AS IsReplaced
		 from mastercatalogSupplier MCS
					INNER JOIN [MasterCatalogCategory] MCC ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
					INNER JOIN [MasterCatalogSubCategory] MCSC ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
					INNER JOIN [MasterCatalogProduct] MCP ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
					LEFT OUTER JOIN [ProductReplacement] PR ON PR.IsActive = 1 AND PR.[OldMasterCatalogProductID] = MCP.[MasterCatalogProductID]
		where MCP.isactive=1
			and MCC.Isactive=1
			and MCSC.isactive=1
			and (MCS.SupplierName like @SearchText or MCS.SupplierShortName like @SearchText)
		GROUP BY
				MCS.MasterCatalogSupplierID,
				MCP.MastercatalogProductID,
				MCP.MasterCatalogSubCategoryID,
				MCP.Code,
				MCP.ShortName,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				MCP.Mod1,
				MCP.Mod2,
				MCP.Mod3,
				MCP.WholesaleListCost,
				MCP.isLogoAblePart
		ORDER BY 
			MCS.MasterCatalogSupplierID,
--			MCC.Sequence, 
--			MCS.Sequence,
			MCP.ShortName,
			MCP.Code
	end
else if @SearchCriteria='Category'
	begin
		select MCS.MasterCatalogSupplierID as SupplierID,
				MCP.MastercatalogProductID,
				MCP.MasterCatalogSubCategoryID,
				MCP.Code,
				MCP.ShortName,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				MCP.Mod1,
				MCP.Mod2,
				MCP.Mod3,
				MCP.WholesaleListCost,
				MCP.isLogoAblePart,
				MAX(COALESCE(PR.IsActive, 0)) AS IsReplaced
		 from mastercatalogSupplier MCS
					INNER JOIN [MasterCatalogCategory] MCC ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
					INNER JOIN [MasterCatalogSubCategory] MCSC ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
					INNER JOIN [MasterCatalogProduct] MCP ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
					LEFT OUTER JOIN [ProductReplacement] PR ON PR.IsActive = 1 AND PR.[OldMasterCatalogProductID] = MCP.[MasterCatalogProductID]
		where MCP.isactive=1
			and MCC.Isactive=1
			and MCSC.isactive=1
			and (MCC.Name like @SearchText or MCSC.Name like @SearchText)
		GROUP BY
				MCS.MasterCatalogSupplierID,
				MCP.MastercatalogProductID,
				MCP.MasterCatalogSubCategoryID,
				MCP.Code,
				MCP.ShortName,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				MCP.Mod1,
				MCP.Mod2,
				MCP.Mod3,
				MCP.WholesaleListCost,
				MCP.isLogoAblePart
		ORDER BY 
			MCS.MasterCatalogSupplierID,
--			MCC.Sequence, 
--			MCS.Sequence,
			MCP.ShortName,
			MCP.Code
	end
	else if @SearchCriteria='HCPCs'
	begin
		select MCS.MasterCatalogSupplierID as SupplierID,
				MCP.MastercatalogProductID,
				MCP.MasterCatalogSubCategoryID,
				MCP.Code,
				MCP.ShortName,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				MCP.Mod1,
				MCP.Mod2,
				MCP.Mod3,
				MCP.WholesaleListCost,
				MCP.isLogoAblePart,
				MAX(COALESCE(PR.IsActive, 0)) AS IsReplaced
		 from mastercatalogSupplier MCS
					INNER JOIN [MasterCatalogCategory] MCC ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
					INNER JOIN [MasterCatalogSubCategory] MCSC ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
					INNER JOIN [MasterCatalogProduct] MCP ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
					LEFT OUTER JOIN [ProductReplacement] PR ON PR.IsActive = 1 AND PR.[OldMasterCatalogProductID] = MCP.[MasterCatalogProductID]
		where MCP.isactive=1
			and MCC.Isactive=1
			and MCSC.isactive=1
			AND dbo.ConcatBregHCPCS(MCP.MastercatalogProductID) like @SearchText
		GROUP BY
				MCS.MasterCatalogSupplierID,
				MCP.MastercatalogProductID,
				MCP.MasterCatalogSubCategoryID,
				MCP.Code,
				MCP.ShortName,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				MCP.Mod1,
				MCP.Mod2,
				MCP.Mod3,
				MCP.WholesaleListCost,
				MCP.isLogoAblePart
		ORDER BY 
			MCS.MasterCatalogSupplierID,
--			MCC.Sequence, 
--			MCS.Sequence,
			MCP.ShortName,
			MCP.Code
	end
END
