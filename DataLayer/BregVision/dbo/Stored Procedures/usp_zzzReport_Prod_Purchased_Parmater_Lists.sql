﻿CREATE PROC usp_zzzReport_Prod_Purchased_Parmater_Lists
AS
BEGIN

SELECT * 
FROM Practice
WHERE PracticeID > 1
AND ISACTIVE = 1
ORDER BY PracticeName
 

SELECT * 
FROM PracticeLocation
WHERE PracticeID = 6
ORDER BY NAME

SELECT DISTINCT SupplierShortName, IsThirdPartySupplier
FROM dbo.PracticeCatalogSupplierBrand 
WHERE PracticeID = 6
AND IsActive = 1
ORDER BY 
	  IsThirdPartySupplier 
	, SupplierShortName

SELECT DISTINCT BrandShortName
FROM dbo.PracticeCatalogSupplierBrand 
WHERE PracticeID = 6
AND SupplierShortName = 'Tri-State Orthopedics'
AND IsActive = 1
ORDER BY 
	  BrandShortName

END