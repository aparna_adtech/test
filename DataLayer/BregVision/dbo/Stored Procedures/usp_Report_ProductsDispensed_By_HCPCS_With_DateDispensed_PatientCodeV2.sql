﻿CREATE PROC [dbo].[usp_Report_ProductsDispensed_By_HCPCS_With_DateDispensed_PatientCodeV2]
		  @PracticeID			INT
		, @PracticeLocationID	VARCHAR(2000)
		--, @PhysicianID			VARCHAR(2000)
		, @StartDate			DATETIME
		, @EndDate				DATETIME
		, @OrderBy				int
		, @OrderDirection		bit
		, @HCPCsList			Varchar(2000) = null
		, @IsMedicare           bit = null
		, @MedicareOrderDirection bit = null
		, @InternalPracticeLocationID VARCHAR(2000)
		, @InternalHCPCsList	VARCHAR(2000) = NULL
AS
BEGIN

DECLARE @PracticeLocationIDString VARCHAR(2000)

-- Parse Practice Location List
DECLARE @locations TABLE([PracticeLocationID] INT);

IF(@InternalPracticeLocationID = 'ALL')
BEGIN
	DECLARE @tempLocations TABLE (
			PracticeLocationID INT
			,PracticeLocationName VARCHAR(50)
			,IsPrimaryLocation BIT
			);

		INSERT @tempLocations
		EXEC dbo.usp_PracticeLocation_Select_All_ID_Name_Given_PracticeID @PracticeID = @PracticeID;

		INSERT @locations ([PracticeLocationID])
		SELECT PracticeLocationID
		FROM @tempLocations;
END;
ELSE
BEGIN
	INSERT @locations ([PracticeLocationID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable(@PracticeLocationID, ',');
END;

-- Get All HCPCs if called for
IF(@InternalHCPCsList = 'ALL')
BEGIN
	DECLARE @locationList NVARCHAR(MAX);
	IF(@InternalPracticeLocationID = 'ALL')
	BEGIN
		SELECT @locationList = COALESCE(@locationList + CAST(PracticeLocationID AS NVARCHAR(10)) + ',', '')
		FROM @locations;

		IF(LEN(@locationList) > 0)
			SET @locationList = LEFT(@locationList, LEN(@locationList) - 1);
	END;
	ELSE
	BEGIN
		SET @locationList = @PracticeLocationID;
	END;

	DECLARE @hcpcs TABLE (HCPCS VARCHAR(50));

	INSERT @hcpcs
	EXEC dbo.usp_GetPracticeLocationHCPCs @PracticeLocationIDString = @locationList;

	SET @HCPCsList = '';

	SELECT @HCPCsList = COALESCE(@HCPCsList + HCPCS + ',','')
	FROM @hcpcs;

	IF(LEN(@HCPCsList) > 0)
		SET @HCPCsList = LEFT(@HCPCsList, LEN(@HCPCsList) - 1);
END;

IF NOT EXISTS (
  select * from (
          select distinct dq.HCPCs
            from DispenseQueue dq
            where dq.PracticeLocationID in (select ID from dbo.udf_ParseArrayToTable(@PracticeLocationID, ','))
              and nullif(dq.HCPCs, '') is not null
          union
          select distinct ph.HCPCS
            from
              ProductInventory pi
              inner join PracticeCatalogProduct pcp on pi.PracticeCatalogProductID = pcp.PracticeCatalogProductID
              inner join ProductHCPCS ph on pcp.PracticeCatalogProductID = ph.PracticeCatalogProductID
            where
              pi.PracticeLocationID in (select ID from dbo.udf_ParseArrayToTable(@PracticeLocationID, ','))
        ) as T
        where ltrim(rtrim(t.HCPCS)) not in (select ltrim(rtrim(TextValue)) from dbo.udf_ParseStringArrayToTable(@HCPCsList, ','))
	) SET @HCPCsList = NULL

IF @HCPCsList = '' set @HCPCsList = NULL

SELECT @EndDate = DATEADD(DAY, 1, @EndDate);

WITH SUB AS
(
	SELECT
		ISNULL(NULLIF(
			ISNULL(DQ.HCPCs,
				IIF(DQ.IsCustomFit = NULL or (DQ.IsCustomFit = 0 AND PCP.IsSplitCode=1),
					dbo.ConcatHCPCSOTS(PCP.PracticeCatalogProductID),
					dbo.ConcatHCPCS(PCP.PracticeCatalogProductID)))
			,''), 'No HCPC') AS HCPCS

		, P.PracticeName
		, PL.Name AS PracticeLocation
			
		, CASE WHEN PCSB.SupplierShortName = PCSB.BrandShortName
			THEN PCSB.SupplierShortName
			ELSE PCSB.SupplierShortName + ' (' + PCSB.BrandShortName + ')'
			END AS SupplierBrand
		, PCSB.SupplierShortName AS Supplier
		, CASE WHEN PCSB.SupplierShortName = PCSB.BrandShortName
			THEN ''
			ELSE PCSB.BrandShortName
			END AS Brand

		, ISNULL(PCSB.Sequence, 999) + CASE WHEN PCP.IsThirdPartyProduct = 1 THEN 1000 ELSE 0 END AS Sequence

		, CASE WHEN PCP.IsThirdPartyProduct = 1
			THEN TPP.[Name]
			ELSE MCP.[Name]
			END AS ProductName

		, CAST(ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) ) + ', '
			+ CAST(ISNULL(CASE WHEN PCP.IsThirdPartyProduct = 1 THEN TPP.Size ELSE MCP.Size END, 'NA') AS VARCHAR(50) ) + ', '
			+ CAST(ISNULL(CASE WHEN PCP.IsThirdPartyProduct = 1 THEN TPP.Gender ELSE MCP.Gender END, 'NA') AS VARCHAR(50) ) AS SideSizeGender
			
		, ISNULL(CASE WHEN PCP.IsThirdPartyProduct = 1 THEN TPP.Code ELSE MCP.Code END, '') AS Code
			
		, CASE DD.IsCashCollection
			WHEN 1 THEN PCP.BillingChargeCash
			WHEN 0 THEN 0
			END AS BillingChargeCash

		, CONVERT(VARCHAR(10), D.DateDispensed, 101) AS DateDispensed
		, D.PatientCode

		, CASE DD.IsMedicare
			WHEN 1 THEN 'Y'
			WHEN 0 THEN 'N'
			END AS IsMedicare
		, CASE CAST(DD.ABNType AS varchar(50))
			WHEN 'Medicare' THEN 'M' + ',' +  CAST(DQ.MedicareOption AS varchar(50))
			WHEN 'Commercial' THEN 'C' + ',' + CAST(DQ.MedicareOption AS varchar(50))
			End AS ABNType
				
		, DD.ICD9Code
		, DD.Quantity
			
		, CAST( ((PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits) AS DECIMAL(15,2) ) AS ActualWholesaleCostLineTotal
		, CAST( DD.DMEDeposit AS DECIMAL(15,2) ) AS DMEDepositLineTotal
		, DD.IsCashCollection
		, CASE DD.IsCashCollection
			WHEN 1 THEN 0
			WHEN 0 THEN CAST( (DD.ActualChargeBilled * DD.Quantity) AS DECIMAL(15,2) )
			END AS ActualChargeBilledLineTotal

		, ISNULL(PP.Name, 'N/A') AS PayerName
		, D.Notes AS DispenseNote
		, DD.ActualChargeBilled AS OriginalActualChargeBilled
		, LTRIM(RTRIM(ISNULL(D.PatientFirstName,''))) AS PatientFirstName
		, LTRIM(RTRIM(ISNULL(D.PatientLastName,''))) AS PatientLastName
		, DD.Mod1
		, DD.Mod2
		, DD.Mod3

	FROM Dispense AS D WITH (NOLOCK)

	INNER JOIN PracticeLocation AS PL WITH (NOLOCK)
		ON D.PracticeLocationID = PL.PracticeLocationID

	INNER JOIN Practice AS P WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID

	INNER JOIN DispenseDetail AS DD WITH (NOLOCK)
		ON D.DispenseID = DD.DispenseID

	INNER JOIN ProductInventory_DispenseDetail AS PI_DD WITH (NOLOCK)
		ON DD.DispenseDetailID = PI_DD.DispenseDetailID

	INNER JOIN dbo.PracticeCatalogProduct AS PCP WITH (NOLOCK)
		ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
	LEFT JOIN dbo.MasterCatalogProduct AS MCP WITH (NOLOCK)
		ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
	LEFT JOIN dbo.ThirdPartyProduct AS TPP WITH (NOLOCK)
		ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

	INNER JOIN DispenseQueue DQ WITH (NOLOCK)
		ON DQ.DispenseQueueID = DD.DispenseQueueID

	LEFT JOIN PracticePayer PP WITH (NOLOCK)
		ON PP.PracticePayerID = D.PracticePayerID

	WHERE
		P.PracticeID = @PracticeID
		AND DD.IsMedicare = COALESCE(@IsMedicare, DD.IsMedicare)
		AND D.IsActive = 1
		AND DD.IsActive = 1
		AND PI_DD.IsActive = 1

		AND PL.PracticeLocationID IN (SELECT PracticeLocationID FROM @locations)

		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM
)
SELECT * FROM SUB
--WHERE (@HCPCsList IS NULL OR SUB.HCPCS = 'No HCPC' OR (dbo.CompareDelimitedStrings(SUB.HCPCS, @HCPCsList, ',') = 1))
ORDER BY
	CASE
		WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName DESC))
		WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName))

		WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName DESC))
		WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName))

		WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName DESC ))
		WHEN (@OrderBy = 1 OR @OrderBy = 3) and @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName ))


		WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed DESC))
		WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed))

		WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed DESC))
		WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed))

		WHEN @OrderBy = 2 and @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed DESC))
		WHEN @OrderBy = 2 and @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed))
	END

END
