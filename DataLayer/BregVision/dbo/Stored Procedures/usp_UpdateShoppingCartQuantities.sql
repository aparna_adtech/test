﻿
-- =============================================
-- Author:		Greg Ross
-- Create date: 11/02/07
-- Description:	Updates Shopping Cart Quantities
-- =============================================
CREATE PROCEDURE [dbo].[usp_UpdateShoppingCartQuantities]
	-- Add the parameters for the stored procedure here
	@ShoppingCartItemID int,
	@Quantity int,
	@IsLogoPart bit = 0

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if @Quantity <= 0 
		begin
			
			--Delete from shoppingcart
			delete from shoppingcartitem where shoppingcartitemid=@ShoppingCartItemID
			--set @Quantity = 0
		end 
	else 
		begin		
			update shoppingcartItem 
				set Quantity = @Quantity,
					IsLogoPart = @IsLogoPart 
			where ShoppingCartItemID=@ShoppingCartItemID
		end 
END

