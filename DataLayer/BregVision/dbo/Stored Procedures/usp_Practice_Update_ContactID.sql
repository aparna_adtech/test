﻿
/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Practice_Update_ContactID
   
   Description:  Updates a record in the table Practice
   
   AUTHOR:       John Bongiorni 7/14/2007 7:28 PM
   
   Modifications:  
   ------------------------------------------------------------ */  

CREATE PROCEDURE [dbo].[usp_Practice_Update_ContactID]
(
	  @PracticeID                    INT
	, @ContactID                     INT
	, @ModifiedUserID                INT
)
AS
BEGIN
	DECLARE @Err INT

	UPDATE dbo.Practice
	SET
		  ContactID = @ContactID
		, ModifiedUserID = @ModifiedUserID
		, ModifiedDate = GETDATE()

	WHERE 
		PracticeID = @PracticeID

	SET @Err = @@ERROR

	RETURN @Err
End


