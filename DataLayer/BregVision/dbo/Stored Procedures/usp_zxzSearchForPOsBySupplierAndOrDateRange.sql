﻿




-- =============================================
-- Author:		Greg Ross
-- Create date: 08/21/07
-- Description:	Search for all PO by supplier and/or date range for Check In
-- JB:  Correct Search To Date and Search From Date
--  exec [usp_zxzSearchForPOsBySupplierAndOrDateRange] 6, 1, 'Breg'
-- =============================================
CREATE PROCEDURE [dbo].[usp_zxzSearchForPOsBySupplierAndOrDateRange]
	@PracticeLocationID int,
	@SearchCriteria int,
	@SearchText varchar(50),
	@SearchFromDate datetime = null,
	@SearchToDate datetime = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
IF @SearchToDate IS NULL
	BEGIN
		-- Set to tommorrow at 12AM. 
		SET @SearchToDate = DATEADD(DAY, 1, GETDATE())
	END	
	
IF @SearchFromDate IS NULL
	BEGIN
		--  Set to two years back from today.
		SET @SearchFromDate = DATEADD(Year, -2, GETDATE())
	END	


if 	@SearchCriteria = 0 --Both
	begin
		EXEC	[dbo].usp_zxzSearchForPOsBySupplierAndDateRange_With_ThirdPartyProducts
		@PracticeLocationID = @PracticeLocationID,
		@SearchText = @SearchText,
		@SearchFromDate = @SearchFromDate,
		@SearchToDate = @SearchToDate
			
	end 	

if 	@SearchCriteria = 1 --Supplier
	begin
		EXEC	[dbo].usp_zxzSearchForPOsBySupplier_With_ThirdPartyProducts
		@PracticeLocationID = @PracticeLocationID,
		@SearchText = @SearchText

	end 

if 	@SearchCriteria = 2 --Date Range
	begin
		EXEC	[dbo].usp_zxzSearchForPOsByDateRange_With_ThirdPartyProducts
		@PracticeLocationID = @PracticeLocationID,
		@SearchFromDate = @SearchFromDate,
		@SearchToDate = @SearchToDate
	end 	
END





