﻿/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Lookup_Salutation_Select_All
   
   Description:  Selects all records from the table dbo.Lookup_Salutation
   
   AUTHOR:       John Bongiorni 8/2/2007 5:12:38 PM
   
   Modifications:  
   ------------------------------------------------------------ */ 

CREATE PROCEDURE [dbo].[usp_Lookup_Salutation_Select_All]
AS
BEGIN
	DECLARE @Err INT
	SELECT
		  Lookup_SalutationID
		, Salutation
	
	FROM dbo.Lookup_Salutation
	
	SET @Err = @@ERROR

	RETURN @Err
END


