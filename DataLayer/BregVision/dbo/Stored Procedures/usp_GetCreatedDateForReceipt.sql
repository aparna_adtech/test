﻿

-- =============================================
-- Author:		Greg Ross
-- Create date: Sept. 24, 2007
-- Description:	Gets Created Date for building Receipt


-- =============================================
create PROCEDURE [dbo].[usp_GetCreatedDateForReceipt]  -- 46
	@DispenseID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select DateDispensed from Dispense where DispenseID=@DispenseID
		
END


SET ANSI_NULLS ON


