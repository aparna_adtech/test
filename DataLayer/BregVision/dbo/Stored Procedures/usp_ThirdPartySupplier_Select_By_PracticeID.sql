﻿/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_ThirdPartySupplier_Select_By_PracticeID
   
   Description:  Selects records from the table dbo.ThirdPartySupplier
					for a given Practice
					
   AUTHOR:       John Bongiorni 8/13/2007 12:25:58 PM
   
   Modifications:  
   ------------------------------------------------------------ */  -------------------SELECT ALL----------------

CREATE PROCEDURE [dbo].[usp_ThirdPartySupplier_Select_By_PracticeID] 
		@PracticeID INT
AS
BEGIN
	DECLARE @Err INT
	SELECT
		  ThirdPartySupplierID
--		, AddressID
		, PracticeID	
		, SupplierName			AS Name
		, SupplierShortName		AS ShortName

		, IsVendor

		, IsEmailOrderPlacement	AS IsEmailOrder


		, FaxOrderPlacement		AS FaxOrder
		, EmailOrderPlacement	AS EmailOrder
		
		, 'Contact'				AS Contact
		
--		, IsEmailOrderPlacement

--		
--		, Sequence
--		, CreatedUserID
--		, CreatedDate
--		, ModifiedUserID
--		, ModifiedDate
--		, IsActive
	
				
	
	FROM dbo.ThirdPartySupplier
	WHERE PracticeID = @PracticeID
		AND IsActive = 1
	--ORDER BY
	
	SET @Err = @@ERROR

	RETURN @Err
END

