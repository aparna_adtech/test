﻿
/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_ThirdPartySupplier_Insert
   
   Description:  Inserts a record into table ThirdPartySupplier
   
   AUTHOR:       John Bongiorni 9/27/2007 5:27:27 PM
   
   Modifications:  JB 20071005 If Manufacturer then insert into the SupplierBrand table.
   ------------------------------------------------------------ */  ------------------ INSERT ------------------
   
CREATE PROCEDURE [dbo].[usp_ThirdPartySupplier_Insert]
(
      @ThirdPartySupplierID          INT = NULL OUTPUT
    , @PracticeID                    INT
--    , @ContactID                     INT = NULL
--    , @AddressID                     INT = NULL
    , @SupplierName                  VARCHAR(50)
    , @SupplierShortName             VARCHAR(50)
    , @FaxOrderPlacement             VARCHAR(50) = NULL
    , @EmailOrderPlacement           VARCHAR(100) = NULL
    , @IsEmailOrderPlacement         BIT
    , @IsVendor                      BIT
    , @Sequence                      INT = 999
    , @CreatedUserID                 INT
)
AS
BEGIN
	DECLARE @Err INT

	INSERT
	INTO dbo.ThirdPartySupplier
	(
          PracticeID
--        , ContactID
--        , AddressID
        , SupplierName
        , SupplierShortName
        , FaxOrderPlacement
        , EmailOrderPlacement
        , IsEmailOrderPlacement
        , IsVendor
        , Sequence
        , CreatedUserID
        , IsActive
	)
	VALUES
	(
          @PracticeID
--        , @ContactID
--        , @AddressID
        , @SupplierName
        , @SupplierShortName
        , @FaxOrderPlacement
        , @EmailOrderPlacement
        , @IsEmailOrderPlacement
        , @IsVendor
        , @Sequence
        , @CreatedUserID
        , 1
	)

	SET @Err = @@ERROR


	SELECT @ThirdPartySupplierID = SCOPE_IDENTITY()

	IF (@IsVendor = 0)
	begin		
	DECLARE	@PracticeCatalogSupplierBrandID int

	EXEC	@Err = [dbo].[usp_PracticeCatalogSupplierBrand_Insert_Mfg]
			@PracticeID = @PracticeID ,
			@ThirdPartySupplierID = @ThirdPartySupplierID,
			@UserID = @CreatedUserID,
			@PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID OUTPUT


	SELECT @PracticeCatalogSupplierBrandID  = SCOPE_IDENTITY()
	END 
	RETURN @Err
END