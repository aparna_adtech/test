﻿

/* ------------------------------------------------------------
   PROCEDURE:    [dbo].[usp_GetAllInventoryDataforPCToIventory_ByBrand_With_ThirdPartyProducts]
   
   Description:  Selects products (mc and 3rdParty) that are in inventory given a brandname
					or shortbrandname
					or partial brand name or partial shortbrandName 
						to search, also given a practiceID
					and IsActice of 0 or 1 (false or true)
					This is called from the proc:
						usp_GetAllInventoryDataforPCToIventory						
   
   AUTHOR:       John Bongiorni 10/9/2007 2:51:40 PM
   
   Modifications:   						20080123 JB Add SideSize Combo. 
					2008.01.31 JB 
						--  User the new column IsSetToActive to correspond to the PIProduct Grid Search.
						--  Later change AND PI.IsActive = @IsActive to = 1 default PI.IsActive to 1.	
					2008.02.04 JB 	
					--  Add space if null: ISNULL(SUB.LeftRightSide, ' ') + ', ' + ISNULL(SUB.Size, ' ')
					--  Add column IsDeletable set to true when	the QOH, Par, Reorder, Critical levels are zero.	
					--  Add column to pull the ProductInventoryID!			
					2008.02.05 JB
					--  Add column IsNotFlaggedForReorder for checkbox in PI grid in PL_InventoryLevels page.																						
   ------------------------------------------------------------ */   


CREATE PROCEDURE [dbo].[usp_GetAllInventoryDataforPCToIventory_ByBrand_With_ThirdPartyProducts]

	@PracticeLocationID int
	, @SearchText  varchar(50)
	, @IsActive bit

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;
	
			DECLARE @IsSetToActive BIT --  This is now being used in place to indicate a product that has been set
										--  in the system to be reordered.
			SET @IsSetToActive = @IsActive


			SELECT 
				  Sub.ProductInventoryID
				
				, Sub.SupplierShortName		--  2007.11.06  JB
				, Sub.BrandShortName		--  2007.11.06  JB
					
				, SUB.PracticeCatalogProductID				
				, SUB.SupplierID
				, SUB.Category
				, SUB.Product
				, SUB.Code
				, SUB.Packaging
				
					
				, ISNULL(SUB.LeftRightSide, ' ') + ', ' + ISNULL(SUB.Size, ' ') AS SideSize
	
				
				, SUB.LeftRightSide
				, SUB.Size
				, SUB.Gender
				, SUB.QOH
				, SUB.ParLevel
				, SUB.ReorderLevel
				, SUB.CriticalLevel
				
				
				, SUB.IsNotFlaggedForReorder
				
				--  2008.02.04   JB
				--  If the QOH, Par, Reorder, Critical quantities are zero then
				--    set isDeletable to true.
				, CASE (SUB.QOH + SUB.ParLevel + SUB.ReorderLevel + SUB.CriticalLevel)
					WHEN 0 THEN 1 ELSE 0 END AS IsDeletable
				
				
				, SUB.WholesaleCost
				, SUB.IsActive
				, SUB.IsSetToActive			--  2008.01.31 JB Change to IsSetToActive
				, SUB.IsThirdPartyProduct
				, SUB.IsConsignment
				, SUB.ConsignmentQuantity
							
			FROM
			(
					SELECT	-- 249 active and inactive mastercatalogproducts in inventory WORKS!!!
 
--							PCP.PracticeCatalogProductID,		
--
--                            PCSB.MasterCatalogSupplierID AS SupplierID, 
--                            PCSB.BrandShortName AS Category,   --  This column should be unneccessary
--																--  Only here to preserve the existing application code.
--						    MCP.[Name]			AS Product,
--                            
                            
							PI.ProductInventoryID,
							
							PCSB.SupplierShortName,		--  2007.11.06  JB
							PCSB.BrandShortName,		--  2007.11.06  JB
							
							PCSB.PracticeCatalogSupplierBrandID AS SupplierID,	--  2007.11.06  JB
							PCP.PracticeCatalogProductID,						--  2007.11.06  JB

                            --  PCSB.MasterCatalogSupplierID AS SupplierID,		--  2007.11.06  JB
                            
                            MCC.[NAME] AS Category,								--  2007.11.06  JBPCP.PracticeCatalogProductID,		

							--  above is Only here to preserve the existing application code.
						    MCP.[Name]			AS Product,
                            
                            MCP.[Code]			AS Code,
                            MCP.[Packaging]		AS Packaging,
                            MCP.[LeftRightSide] AS LeftRightSide,
                            MCP.[Size]			AS Size,
                            MCP.[Gender]		AS Gender,
							
							PI.QuantityOnHandPerSystem AS QOH,
							PI.ParLevel,
							PI.ReorderLevel,
							PI.CriticalLevel,
						
							PI.IsNotFlaggedForReorder,
							
							PCP.WholesaleCost,
							PI.Isactive,
							PI.IsSetToActive,
							0 AS IsThirdPartyProduct,
							PI.IsConsignment,
							PI.ConsignmentQuantity
                     
                    FROM dbo.PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
							                    
                    INNER JOIN [PracticeCatalogProduct] AS PCP WITH(NOLOCK) 
						ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
							
                    INNER JOIN [ProductInventory] AS PI  WITH(NOLOCK)
							ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
             
                    INNER JOIN [MasterCatalogProduct] AS MCP  WITH(NOLOCK)
							ON PCP.[MasterCatalogProductID] = MCP.[MasterCatalogProductID]
                                                 
                     
                     -- the following two inner join can be removed later
					-- Needed now for the MCC.Name column.		
					INNER JOIN dbo.MasterCatalogsubCategory AS MCSC WITH(NOLOCK)  --  2007.11.06  JB
							ON MCP.MasterCatalogsubCategoryID = MCSC.MasterCatalogsubCategoryID

					INNER JOIN dbo.MasterCatalogCategory AS MCC WITH(NOLOCK)      --  2007.11.06  JB
							ON MCSC.MasterCatalogCategoryID = MCC.MasterCatalogCategoryID   
							
							
                       
                       
                       WHERE  PI.[PracticeLocationID] = @PracticeLocationID
							AND PCP.IsThirdPartyProduct = 0  -- MasterCatalogProductsOnly
							
																					
							AND PCSB.IsActive =	1	--  2007.11.06  JB
							AND PCP.IsActive = 1	--  2007.11.06  JB
							AND MCP.IsActive = 1	--  2007.11.06  JB
							
							AND PI.IsSetToActive = @IsSetToActive --  2008.01.31 JB Change to IsSetToActive
							AND PI.IsActive = 1			--  Later change this to = 1 default PI.IsActive to 1.

							and (PCSB.BrandName like @SearchText or PCSB.BrandShortName like @SearchText)
					

				UNION

					--  Third Party Product Info
					SELECT		-- 249 active and inactive mastercatalogproducts in inventory WORKS!!!
 
--							PCP.PracticeCatalogProductID,		       
--                            PCSB.ThirdPartySupplierID AS SupplierID,                        
--                            PCSB.BrandShortName AS Category,   --  This column should be unneccessary
--																--  Only here to preserve the existing application code.
							PI.ProductInventoryID,
							
							PCSB.SupplierShortName, 
							PCSB.BrandShortName, 							
							PCSB.PracticeCatalogSupplierBrandID AS SupplierID, 				
							PCP.PracticeCatalogProductID,		                               
                            'NA'								AS Category,
                            
																--  Only here to preserve the existing application code.
						    
						   

						    TPP.[Name]			AS Product,                            
                            TPP.[Code]			AS Code,
                            TPP.[Packaging]		AS Packaging,
                            TPP.[LeftRightSide] AS LeftRightSide,
                            TPP.[Size]			AS Size,
                            TPP.[Gender]		AS Gender,
							
							PI.QuantityOnHandPerSystem AS QOH,
							PI.ParLevel,
							PI.ReorderLevel,
							PI.CriticalLevel,
							
							PI.IsNotFlaggedForReorder,
							
							PCP.WholesaleCost,						
							PI.Isactive,
							PI.IsSetToActive,							
							1 AS IsThirdPartyProduct,
							PI.IsConsignment,
							PI.ConsignmentQuantity
                     
                    FROM dbo.PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
                    
                    INNER JOIN [PracticeCatalogProduct] AS PCP WITH(NOLOCK) 
						ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
							
                    INNER JOIN [ProductInventory] AS PI  WITH(NOLOCK)
							ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
             
                    INNER JOIN [ThirdPartyProduct] AS TPP  WITH(NOLOCK)
							ON PCP.[ThirdPartyProductID] = TPP.[ThirdPartyProductID]
                                                 
                     WHERE  PI.[PracticeLocationID] = @PracticeLocationID
							AND PCP.IsThirdPartyProduct = 1		-- ThirdPartyProductsOnly 
							
																												
							AND PCSB.IsActive =	1	--  2007.11.06  JB
							AND PCP.IsActive = 1	--  2007.11.06  JB
							AND TPP.IsActive = 1	--  2007.11.06  JB							
							
							AND PI.IsSetToActive = @IsSetToActive --  2008.01.31 JB Change to IsSetToActive
							AND PI.IsActive = 1			--  Later change this to = 1 default PI.IsActive to 1.

							AND (
							
									(PCSB.BrandName like @SearchText or PCSB.BrandShortName like @SearchText)
									OR
									(PCSB.SupplierName like @SearchText or PCSB.SupplierShortName like @SearchText)
								)	
										
				) AS Sub
			
			ORDER BY Sub.IsThirdPartyProduct
			
END
