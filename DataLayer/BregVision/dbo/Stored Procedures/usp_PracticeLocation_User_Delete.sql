﻿
CREATE PROCEDURE [dbo].[usp_PracticeLocation_User_Delete]
		  @PracticeLocationID INT
		, @UserID		  INT
		, @ModifiedUserID	  INT
AS
BEGIN	
	
	UPDATE dbo.PracticeLocation_User
	SET 
		  ModifiedUserID = @ModifiedUserID
		, ModifiedDate = GetDate()
		, IsActive = 0	
	WHERE	
		  PracticeLocationID = @PracticeLocationID 
		  AND UserID = @UserID		
		
END

