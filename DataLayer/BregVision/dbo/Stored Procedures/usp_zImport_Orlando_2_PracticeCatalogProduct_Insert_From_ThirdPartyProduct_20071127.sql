﻿

CREATE PROC [dbo].[usp_zImport_Orlando_2_PracticeCatalogProduct_Insert_From_ThirdPartyProduct_20071127]  
	--  @PracticeID INT

as
begin

-- select * from practice  
DECLARE @PracticeID INT
SET @PracticeID = 14                        --  SET @PRACTICEID FOR ORLANDO to 14

INSERT INTO PracticeCatalogProduct
	( PracticeID
	, PracticeCatalogSupplierBrandID
	, ThirdPartyProductID
	, IsThirdPartyProduct
	, WholesaleCost
	, CreatedUserID
	, ModifiedUserID
	, ModifiedDate
	, IsActive
	)

--  Note: UNCOMMENT THE FOLLOWING TWO LINES AND RUN SELECT TO VERIFY BEFORE INSERTING!!!
--DECLARE @PracticeID INT
--SET @PracticeID = 14

SELECT
		PCSB.PracticeID
	, PCSB.PracticeCatalogSupplierBrandID
	, TPS.ThirdPartyProductID
	, 1 AS IsThirdPartyProduct
	, TPS.WholesaleListCost AS WholesaleCost
	, -13  as CreatedUserID    --TPS.CreatedUserID
	, -13  as ModifiedUserID 
	, GetDate() AS ModifiedDate
	, TPS.IsActive
FROM ThirdPartyProduct AS TPS
	INNER JOIN PracticeCatalogSupplierBrand AS PCSB
		ON TPS.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
	INNER JOIN Practice AS P
		ON P.PracticeID = PCSB.PracticeID
	WHERE P.PracticeID = @PracticeID			--  Enter ProductID of `11 for hanover 
	AND TPS.IsActive = 1	--  Avoid getting bogus third party products that have been backed out
	AND TPS.ThirdPartyProductID NOT IN
		(SELECT ThirdPartyProductID 
		 FROM PracticeCatalogProduct 
		 WHERE IsActive = 1 
			AND IsThirdPartyProduct = 1) 

end