﻿/* ------------------------------------------------------------
   PROCEDURE:    [dbo].[usp_PracticeCatalogSupplierBrand_Select_All_Brands_By_Vendor]
   
   Description:  Selects all records from the table dbo.PracticeCatalogSupplierBrand.
				Get the BrandNames and BrandDisplayNames for a given vendor (ThirdPartySupplier that is a vendor)
   
   AUTHOR:       John Bongiorni 10/4/2007 12:25:29 PM
   
   Modifications:  
   ------------------------------------------------------------ */  -------------------SELECT ALL----------------
/*
This replaces [usp_PracticeCatalogSupplierBrand_Select_All_VendorBrands_Or_NonVendorSuppliers_Given_Vendor]
   Note:  The LinkThirdPartySupplierID is used to link the Vendor Grid's top two hierarchies,
   The NonVendorSuppliers... holds the LinkThirdPartySupplierID in order to match is up to the 
   PCSBs with all PCSBs of the suppliers of LinkThirdPartySupplierID's practice.
*/


CREATE PROCEDURE [dbo].[usp_VendorGrid_PracticeCatalogSupplierBrand_SelectAll_Given_VendorB] --98 --103  31, 0
		@LinkThirdPartySupplierID INT

AS
BEGIN
	DECLARE @Err INT
	
--	--  Check Third Party Supplier ID to see if it is a vendor.
--	-- jb 20080328  DECLARE @IsVendor BIT
--	-- jb 20080328  SET @IsVendor = -1
--	-- jb 20080328    SELECT @IsVendor = IsVendor FROM ThirdPartySupplier WHERE ThirdPartySupplierID = @LinkThirdPartySupplierID 
--	
--	
--	-- jb 20080328  IF @IsVendor = 0
--	-- jb 20080328  	BEGIN
--		
--			SELECT 
--			 ISNULL(PCSB.BrandShortName, 'MissingBrand')	AS Brand
--			, TPS.IsVendor AS IsVendorBrand    -- jb 20080328  0 AS IsVendorBrand
--			, PCSB.PracticeCatalogSupplierBrandID
--			, PCSB.PracticeID
--			, (SELECT @LinkThirdPartySupplierID) AS LinkThirdPartySupplierID
--			, PCSB.ThirdPartySupplierID
--			--, PCSB.SupplierShortName 
--			, ISNULL(PCSB.Sequence, 9999) AS Sequence
--				
--		
--			FROM dbo.ThirdPartySupplier AS TPS	WITH (NOLOCK)
--		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB  WITH (NOLOCK)
--			ON TPS.ThirdPartySupplierID = PCSB.ThirdPartySupplierID
--			AND PCSB.IsActive = 1
--		WHERE 
--			-- jb 20080328  TPS.PracticeID = ( SELECT TOP 1 PracticeID FROM ThirdPartySupplier WITH (NOLOCK) WHERE ThirdPartySupplierID = @LinkThirdPartySupplierID )
--			-- jb 20080328  AND 
--			TPS.IsActive = 1
--			AND PCSB.IsActive = 1
--			AND IsVendor = 0
--		ORDER BY 
--			ISNULL(PCSB.Sequence, 9999)
--			, PCSB.BrandShortName
--		END
--	
	-- jb 20080328  ELSE
		-- jb 20080328  BEGIN
			SELECT
				 ISNULL(PCSB.BrandShortName, 'MissingBrand')	AS Brand
				, TPS.IsVendor AS IsVendorBrand  -- jb 20080328  1 AS IsVendorBrand  -- ( SELECT TOP 1 IsVendor FROM ThirdPartySupplier WHERE ThirdPartySupplierID = @LinkThirdPartySupplierID ) AS IsVendorBrand
				, PCSB.PracticeCatalogSupplierBrandID
				, PCSB.PracticeID
				, PCSB.ThirdPartySupplierID
				, PCSB.ThirdPartySupplierID AS LinkThirdPartySupplierID
				--, PCSB.SupplierShortName
				
				, ISNULL(PCSB.Sequence, 9999) AS Sequence
			
			FROM dbo.PracticeCatalogSupplierBrand AS PCSB WITH (NOLOCK)
			-- jb 20080328  
			INNER JOIN dbo.ThirdPartySupplier AS TPS WITH (NOLOCK) ON PCSB.ThirdPartySupplierID = TPS.ThirdPartySupplierID
			WHERE 
				PCSB.ThirdPartySupplierID = @LinkThirdPartySupplierID
				AND PCSB.MasterCatalogSupplierID IS NULL
				AND PCSB.IsThirdPartySupplier = 1
				AND PCSB.IsActive = 1
				
				--AND IsVendorBrand = 1
				--AND ( SELECT IsVendor FROM ThirdPartySupplier WHERE ThirdPartySupplierID = @LinkThirdPartySupplierID ) = 1
			ORDER BY 
				ISNULL(PCSB.Sequence, 9999)
				, PCSB.BrandShortName
		-- jb 20080328  END
		
	SET @Err = @@ERROR

	RETURN @Err
END


