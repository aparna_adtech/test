﻿


/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocation_Supplier_AccountCode_Insert
   
   Description:  Inserts a record into table PracticeLocation_Supplier_AccountCode
   
   AUTHOR:       John Bongiorni 9/26/2007 1:45:52 PM
   
   Modifications:  
   ------------------------------------------------------------ */  ------------------ INSERT ------------------
   
CREATE PROCEDURE dbo.usp_PracticeLocation_Supplier_AccountCode_Insert
(
      @PracticeLocationID             INT 
    , @PracticeCatalogSupplierBrandID INT 
    , @AccountCode                   VARCHAR(50)
    , @CreatedUserID                 INT
    
)
AS
BEGIN
	DECLARE @Err INT

	INSERT
	INTO dbo.PracticeLocation_Supplier_AccountCode
	(
          PracticeLocationID
        , PracticeCatalogSupplierBrandID
        , AccountCode
        , CreatedUserID
        , CreatedDate
        , IsActive
        
	)
	VALUES
	(
          @PracticeLocationID
        , @PracticeCatalogSupplierBrandID
        , @AccountCode
        , @CreatedUserID
        , GETDATE()
        , 1
	)

	SET @Err = @@ERROR

	RETURN @Err
END