﻿CREATE PROC usp_Report_BregExec_DateHelper4
AS
BEGIN

	DECLARE @StartDate	AS DateTime
	DECLARE @EndDate	AS DateTime
	DECLARE @StartYear	AS INT 
	DECLARE @EndYear	AS INT 

	--SET @StartYear = 2006 
	SET @StartDate = '1/6/2006' 
	SET @EndDate = '12/31/2006'
	--SET @EndDate = DATEADD(Day, 1, @EndDate) ;


	SET @StartYear	= Year(@StartDate)
	SET @EndYear	= Year(@EndDate);
	



	WITH                   Years
			  AS ( SELECT   YYYY = @StartYear
				   UNION ALL
				   SELECT   YYYY + 1
				   FROM     Years
				   Where    YYYY < @EndYear
				 ) ,
			Months
			  AS ( SELECT   MM = 1
				   UNION ALL
				   SELECT   MM + 1
				   FROM     Months
				   WHERE    MM < 12
				 ) 


	--SELECT * FROM Years
	--SELECT * FROM Months, Years

	SELECT 
		  P.PracticeName
		, PL.Name AS PracticeLocation
		, YYYY
		, MM
		, Cast(YYYY as char(4)) + RIGHT('0' + CAST (MM as varchar(2)), 2) AS YYYYMM 
		, Cast(YEAR(@StartDate) as char(4)) + RIGHT('0' + CAST (MONTH(@StartDate) as varchar(2)), 2) AS StartYYYYMM
		, Cast(YEAR(@EndDate) as char(4)) + RIGHT('0' + CAST (MONTH(@EndDate) as varchar(2)), 2) AS EndYYYYMM
	FROM Years, Months 
	CROSS JOIN Practice AS P
	INNER JOIN PracticeLocation AS PL
		ON P.PracticeID = PL.PracticeID
	where Cast(YYYY as char(4)) + RIGHT('0' + CAST (MM as varchar(2)), 2) >= Cast(YEAR(@StartDate) as char(4)) + RIGHT('0' + CAST (MONTH(@StartDate) as varchar(2)), 2) 
	AND Cast(YYYY as char(4)) + RIGHT('0' + CAST (MM as varchar(2)), 2) <= Cast(YEAR(@EndDate) as char(4)) + RIGHT('0' + CAST (MONTH(@EndDate) as varchar(2)), 2) 
	ORDER BY PracticeName, YYYYMM, PracticeLocation

--  usp_Report_BregExec_DateHelper4
  
--	SELECT PracticeName, YYYY, MM FROM PRactice Cross join Years, Months
--	ORDER BY PracticeName, YYYY, MM

END