﻿

-- =============================================
-- Author:		<Greg Ross>
-- Create date: <07/31/07>
-- Description:	<Used to get all data for the shopping cart>
--  
--  Modification:  20071011 JB  Added Third Party Supplier Information.
--    check this for Cost
-- exec usp_GetShoppingCart 3
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetShoppingCart]   --18
	@PracticeLocationID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		  SUB.Supplier
		, SUB.Category
		, SUB.Product
		, SUB.Code
		, SUB.Packaging
		, SUB.Side
		, SUB.Size
		, SUB.Gender
		, cast(SUB.IsCustomBrace as bit) as IsCustomBrace
		, cast(SUB.IsCustomBraceAccessory as bit) as IsCustomBraceAccessory
		, SUB.Quantity
		, SUB.Cost           -- check this
		, CAST( (SUB.Quantity * SUB.Cost) AS DECIMAL(9, 2) )  AS LineTotal
		--, (SUB.Quantity * SUB.Cost) AS TotalCost
		, SUB.PracticeCatalogProductID
		, SUB.ShoppingCartItemID
		, SUB.IsThirdPartyProduct
		, SUB.Sequence
		, SUB.ParLevel
		, SUB.QuantityOnHand
		, SUB.ShoppingCartID
		, SUB.MasterCatalogProductID
		, SUB.IsConsignment
		, SUB.IsLogoPart
		, Cast(SUB.McpIsLogoAblePart as bit) as McpIsLogoAblePart

	FROM
			(
				SELECT  MCS.[SupplierName] AS Supplier,
						MCC.[Name] AS Category,
						MCP.[Name] AS Product,
						MCP.[Code] AS Code,
						MCP.[Packaging] AS Packaging,
						MCP.[LeftRightSide] AS Side,
						MCP.[Size] AS Size,
						MCP.[Gender] AS Gender,
						MCP.[IsCustomBrace] AS IsCustomBrace,
						MCP.IsCustomBraceAccessory,
						SCI.[Quantity] AS Quantity,
						SCI.[ActualWholesaleCost] AS Cost,
						PCP.[PracticeCatalogproductid] AS PracticeCatalogProductID,
						SCI.[ShoppingCartItemID] AS ShoppingCartItemID,
						PCP.IsThirdPartyProduct,
						ISNULL(PCSB.Sequence, 0) AS Sequence,
						ISNULL(PRI.ParLevel, 0) AS ParLevel,
						ISNULL(PRI.QuantityOnHandPerSystem, 0) AS QuantityOnHand,
						SC.ShoppingCartID as ShoppingCartID, 
						MCP.MasterCatalogProductID as MasterCatalogProductID, 
						SC.IsConsignment,
						SCI.IsLogoPart,
						(PRI.IsLogoAblePart & PCP.IsLogoAblePart & MCP.IsLogoAblePart) as McpIsLogoAblePart
						
				FROM    
						[ShoppingCart] SC
				        
						INNER JOIN shoppingcartitem SCI 
							ON SC.[ShoppingCartID] = SCI.[ShoppingCartID]
						
						INNER JOIN [PracticeCatalogProduct] PCP 
							ON SCI.[PracticeCatalogProductID] = PCP.[PracticeCatalogProductID]
						
						INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
							ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID							

						INNER JOIN [MasterCatalogProduct] MCP 
							ON PCP.[MasterCatalogProductID] = MCP.[MasterCatalogProductID]

						INNER JOIN [MasterCatalogSubCategory] MCSC 
							ON MCP.[MasterCatalogSubCategoryID] = MCSC.[MasterCatalogSubCategoryID]
						
						INNER JOIN [MasterCatalogCategory] MCC 
							ON MCSC.[MasterCatalogCategoryID] = MCC.[MasterCatalogCategoryID]
						
						INNER JOIN [MasterCatalogSupplier] MCS 
							ON MCC.[MasterCatalogSupplierID] = MCS.[MasterCatalogSupplierID]
						Left Outer JOIN [ProductInventory] PRI 
							ON PRI.PracticeCatalogProductID = SCI.PracticeCatalogProductID AND PRI.PracticeLocationID = @practicelocationID

				WHERE   
						SC.[ShoppingCartStatusID] = 0
						AND SCI.[ShoppingCartItemStatusID] = 0
						AND SC.[IsActive] = 1
						AND SCI.[IsActive] = 1
						AND pcp.[IsActive] = 1
						AND mcsc.[IsActive] = 1
						AND mcc.[IsActive] = 1
						AND mcs.[IsActive] = 1
						AND PCSB.IsActive = 1
						AND SC.[PracticeLocationID] = @practicelocationID
						AND PCSB.IsThirdPartySupplier  = 0
						AND PRI.IsActive = 1

			UNION
			
				SELECT  
						PCSB.[SupplierName]AS Supplier,
						'NA' AS Category,
						TPP.[Name] AS Product,
						TPP.[Code]AS Code,
						TPP.[Packaging] AS Packaging,
						TPP.[LeftRightSide] AS Side,
						TPP.[Size] AS Size,
						TPP.[Gender] AS Gender,
						IsCustomBrace = 0,
						IsCustomBraceAccessory = 0,
						SCI.[Quantity] AS Quantity,
						SCI.[ActualWholesaleCost] AS Cost,
						PCP.[PracticeCatalogproductid] AS PracticeCatalogProductID,
						SCI.[ShoppingCartItemID] AS ShoppingCartItemID,
						PCP.IsThirdPartyProduct,
						ISNULL(PCSB.Sequence, 0) AS Sequence,
						ISNULL(PRI.ParLevel, 0) AS ParLevel,
						ISNULL(PRI.QuantityOnHandPerSystem, 0) AS QuantityOnHand,
						SC.ShoppingCartID as ShoppingCartID, 
						0 as MasterCatalogProductID,
						SC.IsConsignment,
						0 as IsLogoPart,
						0 AS McpIsLogoAblePart
				FROM    
						[ShoppingCart] SC
				        
						INNER JOIN shoppingcartitem SCI 
							ON SC.[ShoppingCartID] = SCI.[ShoppingCartID]
							
						INNER JOIN [PracticeCatalogProduct] PCP 
							ON SCI.[PracticeCatalogProductID] = PCP.[PracticeCatalogProductID]
							
						INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
							ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID							
							
						INNER JOIN [ThirdPartyProduct] TPP 
							ON PCP.[ThirdPartyProductID] = TPP.[ThirdPartyProductID]
						Left Outer JOIN [ProductInventory] PRI 
							ON PRI.PracticeCatalogProductID = SCI.PracticeCatalogProductID AND PRI.PracticeLocationID = @practicelocationID
				WHERE   
						SC.[ShoppingCartStatusID] = 0
						AND SCI.[ShoppingCartItemStatusID] = 0
						AND SC.[IsActive] = 1
						AND SCI.[IsActive] = 1
						
						AND pcp.IsActive = 1
						AND PCSB.IsActive = 1
						AND TPP.IsActive = 1
						AND PCSB.IsThirdPartySupplier  = 1
						
						AND SC.[PracticeLocationID] = @practicelocationID
						AND PRI.IsActive = 1
						
				) AS SUB
				
			ORDER BY 
				SUB.IsThirdPartyProduct
				, SUB.Sequence
				, SUB.Supplier


END
