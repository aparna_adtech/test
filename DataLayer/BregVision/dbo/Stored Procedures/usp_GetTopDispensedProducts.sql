﻿
-- =============================================
-- Author:		<Author: Mike Sneen>
-- Create date: <Create Date: 12/21/10>
-- Description:	<Description: Get the top 10 products dispensed for a PracticeLocation>
-- modification:
--  [dbo].[usp_GetTopDispensedProducts]  

-- =============================================
Create PROCEDURE [dbo].[usp_GetTopDispensedProducts] 
	@PracticeLocationID int
	
AS 
    BEGIN

Select Top 10
	 dd.PracticeCatalogProductID
	,Coalesce(mcp.Name, tpp.Name) as ProductName
	,pcp.WholesaleCost
	,SUM(dd.Quantity) as TotalQuantity
	,d.PracticeLocationID
from
	DispenseDetail dd
	inner join Dispense d on dd.DispenseID = d.DispenseID
	inner join PracticeCatalogProduct pcp on dd.PracticeCatalogProductID = pcp.PracticeCatalogProductID
	Left Outer Join MasterCatalogProduct mcp on pcp.MasterCatalogProductID = mcp.MasterCatalogProductID AND pcp.IsThirdPartyProduct = 0
	Left Outer Join ThirdPartyProduct tpp on pcp.ThirdPartyProductID = tpp.ThirdPartyProductID AND pcp.IsThirdPartyProduct = 1
where
	d.PracticeLocationID = 3--@PracticeLocationID
Group By d.PracticeLocationID, dd.PracticeCatalogProductID, Coalesce(mcp.Name, tpp.Name), pcp.WholesaleCost
order by TotalQuantity desc

End