﻿
-- =============================================
--EXEC  usp_PracticeLocation_Set_Permissions 3, 23, 1
-- Author:		<Author: Mike Sneen>
-- Create date: <Create Date: 04/02/09>
-- Description:	<Description: Used to update Default User Preferences>
-- modification:
--  [dbo].[usp_PracticeLocation_Set_Permissions]  

-- =============================================
Create /*ALTER*/ PROCEDURE [dbo].[usp_PracticeLocation_Set_Permissions]  --3 --10  --  _With_Brands
	@PracticeLocationID int,
	@UserID int,
	@AllowAccess bit
	
AS 
    BEGIN
    DECLARE @Err INT
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	       SET NOCOUNT ON 
	       
	declare @RecordCount int

	SELECT  @RecordCount = COUNT(1)
                FROM    [User_PracticeLocation_Permissions]
                WHERE   
					PracticeLocationID =@PracticeLocationID
					AND UserID = @UserId
     if @recordcount = 0 -- No record exists
	begin
		Insert into [User_PracticeLocation_Permissions]
		(PracticeLocationID,
		 UserID,
		 AllowAccess,
		 CreatedUserID,
		 CreatedDate,
		 IsActive)
		 Values(
		 @PracticeLocationID,
		 @UserID,
		 @AllowAccess,
		 1,
		 GETDATE(),
		 1
		 )
	end
	else
	
		 Update [User_PracticeLocation_Permissions]
		 SET
			AllowAccess = @AllowAccess,
			ModifiedUserID = 1,
			ModifiedDate = GETDATE(),
			IsActive = 1
		WHERE
			UserID = @UserID
			AND PracticeLocationID = @PracticeLocationID
	
     

     
     	SET @Err = @@ERROR

	RETURN @Err

    END

--Exec usp_SuperbillCategory_Update 3, 3