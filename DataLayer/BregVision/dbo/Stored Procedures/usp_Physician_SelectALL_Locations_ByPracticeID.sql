﻿




CREATE PROCEDURE [dbo].[usp_Physician_SelectALL_Locations_ByPracticeID]
		@PracticeID INT
AS
BEGIN	
	SELECT
		Pr.PracticeID 
	  , Ph.PhysicianID
	  , C.ContactID
	  , C.Salutation
	  , C.FirstName
	  , C.MiddleName
	  , C.LastName
	  , C.Suffix
	  , Pin = Ph.PIN
	  , Ph.SortOrder
	  , CASE when (Ph.IsActive = 1 AND C.IsActive = 1) THEN 1 ELSE 0 END AS IsActive
	  , plp.PracticeLocationID
	  , RTRIM( C.FirstName + ' ' + C.LastName + ' ' +	C.Suffix) AS PhysicianName
	FROM
		dbo.Contact AS C
		INNER JOIN dbo.Physician AS Ph
			ON C.ContactID = Ph.ContactID
		INNER JOIN dbo.Practice AS Pr
			ON Ph.PracticeID = Pr.PracticeID
		LEFT OUTER JOIN dbo.PracticeLocation_Physician plp
			ON plp.PhysicianID = Ph.PhysicianID AND plp.IsActive = 1
	WHERE
			Pr.PracticeID = @PracticeID
	ORDER BY Ph.PracticeID, Ph.PhysicianID, plp.PracticeLocationID
END
