﻿/* ------------------------------------------------------------

   PROCEDURE:    dbo.usp_ThirdPartySupplier_Delete
   
   Description:  "Deletes" a record from table dbo.ThirdPartySupplier
   					This actual flags the IsActive column to false.
					
   AUTHOR:       John Bongiorni 9/28/2007 6:39:00 PM
   
   Modifications:  
		  JB  2008.01.07  Set the PCSupplierBrand as Inactive after the thirdpartyproduct has been inactivated.
		  JB  2008.03.13  Check why the Midwest ortho 34, centec 508 will not delete.
						  This is not able to delete because the thirdpartyproduct is still active.
						  I believe this is due the bogus IsActive stuff from the alpha \ beta versions.

   ------------------------------------------------------------ */  ------------------ DELETE ----------------

CREATE PROCEDURE [dbo].[usp_ThirdPartySupplier_Delete_20080313_Backup]
(
	@ThirdPartySupplierID int
	, @ModifiedUserID INT
	
)
AS
BEGIN
	DECLARE @Err INT
	DECLARE @IsVendor BIT
	DECLARE @MFGProductCount INT
	DECLARE @VendorBrandCount INT
	
	SET @MFGProductCount = 0
	SET @VendorBrandCount = 0
	
	--  Check if the ThirdPartySupplier is a Manufacturer
		-- Check for product of the manufacturer.
		
	--   Check if the ThirdPartySupplier is a Vendor
		-- Check for brands of the vendor.

	SELECT @IsVendor = IsVendor
	FROM dbo.ThirdPartySupplier WITH(NOLOCK)
	WHERE ThirdPartySupplierID = @ThirdPartySupplierID
	
	IF (@IsVendor = 0)  --MFG
	BEGIN
		SELECT @MFGProductCount = COUNT(TPP.ThirdPartyProductID)
		FROM 
			dbo.ThirdPartyProduct AS TPP WITH(NOLOCK)
		
		INNER JOIN PracticeCatalogSupplierBrand AS PCSB
			ON TPP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
		
		INNER JOIN ThirdPartySupplier AS TPS
			ON PCSB.ThirdPartySupplierID = TPS.ThirdPartySupplierID
		
		WHERE TPS.ThirdPartySupplierID = @ThirdPartySupplierID
			AND TPP.IsActive = 1
			AND PCSB.IsActive = 1
			AND TPS.IsActive = 1
	END
	
	IF (@IsVendor = 1)  --  3rdPartyVendor
	BEGIN
	
		SELECT 
			@VendorBrandCount = COUNT(PCSB.PracticeCatalogSupplierBrandID)
		
		FROM 
			ThirdPartySupplier AS TPS
				
			INNER JOIN PracticeCatalogSupplierBrand AS PCSB
				ON PCSB.ThirdPartySupplierID = TPS.ThirdPartySupplierID
		
		WHERE TPS.ThirdPartySupplierID =  @ThirdPartySupplierID
			AND TPS.IsActive = 1
			AND PCSB.IsActive =1 
	END



	IF (  ( @MFGProductCount= 0) AND ( @VendorBrandCount= 0)  )
	BEGIN

		UPDATE
			  dbo.ThirdPartySupplier
		SET 
			  IsActive = 0	  
			, ModifiedUserID = @ModifiedUserID
			, ModifiedDate = GETDATE()
		WHERE 
			ThirdPartySupplierID = @ThirdPartySupplierID
		
		
		--  JB  2008.01.07  Set the PCSupplierBrand as Inactive.
		UPDATE
			dbo.PracticeCatalogSupplierBrand
		SET
			  IsActive = 0	  
			, ModifiedUserID = @ModifiedUserID
			, ModifiedDate = GETDATE()
		WHERE 
			ThirdPartySupplierID = @ThirdPartySupplierID

		SET @Err =   @@ERROR

	END
		
	-- Need to set the PCSB.Isactive to 0 and uncomment the above.

	RETURN @Err
End