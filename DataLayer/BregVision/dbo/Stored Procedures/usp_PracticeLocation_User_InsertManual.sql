﻿/*
Insert a given aspnetdb user name into 
the User table with given practiceID
and given PracticeLocationID
*/

--		SET @UserName = 'PracticeUser'
--		SELECT @PracticeID = 1
--		SELECT @PracticeLocationID = 3

CREATE PROCEDURE dbo.usp_PracticeLocation_User_InsertManual 
		@UserName VARCHAR(50)
		, @PracticeID INT
		, @PracticeLocationID INT
		
AS
BEGIN

		DECLARE @TheUserID INT
		DECLARE @return_value INT

		--  Add @UserName to User Table with practiceID
		--exec dbo.usp_User_Insert @PracticeID, @UserName, 0, @TheUserID

		EXEC	@return_value = [dbo].[usp_User_Insert]
				@PracticeID = 1,
				@UserName = @UserName,
				@CreatedUserID = 0,
				@UserID = @TheUserID OUTPUT

		SELECT	@TheUserID as N'@TheUserID'

		INSERT INTO dbo.PracticeLocation_User
			(PracticeLocationID, UserID, CreatedUserID, CreatedDate, IsActive)
		VALUES
			(@PracticeLocationID, @TheUserID, 0, GETDATE(), 1)
END