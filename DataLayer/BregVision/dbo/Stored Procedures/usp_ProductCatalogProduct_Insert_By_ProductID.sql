﻿


-- 3. usp_ProductCatagoryProuct_Insert_By_ProductID
--		Insert the @PracticeID 
--				, @ProductCatalogSupplierBrandID
--				, AND  @MasterCatalogProductID
--			Into the ProductCatalogSupplierBrand table.
--		This is a nested proc called by
--		dbo.usp_Insert_PracticeCatalogProductID_From_MasterCatalogProductID	

-- Test
--		DECLARE	@return_value int,
--				@PracticeCatalogProductID int
--
--		EXEC	@return_value = [dbo].[usp_ProductCatalogProduct_Insert_By_ProductID]
--				@PracticeID = 2,
--				@PracticeCatalogSupplierBrandID = 19,
--				@MasterCatalogProductID = 2,
--				@UserID = 0,
--				@PracticeCatalogProductID = @PracticeCatalogProductID OUTPUT 
--
--		SELECT	@PracticeCatalogProductID as N'@PracticeCatalogProductID' -- outputs 25
--
--		SELECT	'Return Value' = @return_value




CREATE PROC [dbo].[usp_ProductCatalogProduct_Insert_By_ProductID]
	(
		  @PracticeID						INT
		, @PracticeCatalogSupplierBrandID   INT
		, @MasterCatalogProductID			INT
		, @UserID							INT
		, @TransferModifiers				BIT
		, @PracticeCatalogProductID			INT OUTPUT
	)
AS
	BEGIN

		DECLARE @RecordCount INT  --  Check if item already exists in PracticeCatalog.
		DECLARE @TransactionCountOnEntry    INT       -- Transaction Count before the transaction begins
		DECLARE @Err             			INT        --  holds the @@ERROR code returned by SQL Server

		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

		SELECT @TransactionCountOnEntry = @@TRANCOUNT
		SELECT @Err = @@ERROR
		BEGIN TRANSACTION      

		IF @Err = 0
		BEGIN    
		
			SELECT @RecordCount = COUNT(1)
			FROM dbo.PracticeCatalogProduct 
			WHERE
				  PracticeID = @PracticeID 
				AND PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID
				AND MasterCatalogProductID = @MasterCatalogProductID 
				AND IsActive = 1

	
		SET @Err = @@ERROR

		END
		IF @Err = 0
		BEGIN
	
			IF ( @RecordCount = 0 )
				BEGIN   
					--  Insert Record
						DECLARE @WholesaleListCost SMALLMONEY
						DECLARE @IsLogoAblePart bit
						
						SELECT @WholesaleListCost = WholesaleListCost,
							   @IsLogoAblePart = IsLogoAblePart
						FROM dbo.MasterCatalogProduct
						WHERE MasterCatalogProductID = @MasterCatalogProductID
			

						--  Insert into ProductCatagoryProduct
						INSERT
							INTO dbo.PracticeCatalogProduct
							(
								  PracticeID
								, PracticeCatalogSupplierBrandID
								, MasterCatalogProductID
				--				, ThirdPartyProductID
								, IsThirdPartyProduct
								, WholesaleCost
								, BillingCharge
								, BillingChargeCash
								, IsLogoAblePart
				--				, Sequence
								, CreatedUserID
								, IsActive
								, UsePCPMods
							)
							VALUES
							(
								  @PracticeID
								, @PracticeCatalogSupplierBrandID
								, @MasterCatalogProductID
				--				, @ThirdPartyProductID
								, 0
								, @WholesaleListCost
								, 0			-- Default BillingCharge to 0 to indicate that the Practice Admin need to fill this out.
								, 0			-- Default BillingChargeCash to 0 to indicate that the Practice Admin need to fill this out.
								, @IsLogoAblePart
				--				, @Sequence		-- Not used yet.
								, @UserID
								, 1
								, ~@TransferModifiers
							)
					
							
							Select @PracticeCatalogProductID = SCOPE_IDENTITY()
	
						END
					
				ELSE
					BEGIN
						--  Record Already Exists so select the PracticeCatalogProductID
						SELECT @PracticeCatalogProductID = PracticeCatalogProductID
						FROM dbo.PracticeCatalogProduct
						WHERE 
							PracticeID = @PracticeID 
							AND PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID
							AND MasterCatalogProductID = @MasterCatalogProductID 
							AND IsActive = 1
					END	
	
			SET @Err = @@ERROR	
		
		END

		IF @@TranCount > @TransactionCountOnEntry
		BEGIN

			IF @Err = 0
				BEGIN 
					COMMIT TRANSACTION
					RETURN @Err
				END
			ELSE
				BEGIN
					ROLLBACK TRANSACTION
					RETURN @Err
					--  Add any database logging here      
				END
		END				
					
END
