﻿/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeCatalogSupplierBrand_Update_Brands_By_Vendor
   
   Description:  Updates the BrandName and BrandShortName
				 in the table PracticeCatalogSupplierBrand
				 given the PCSupplierBrandID for a vendor's brand.
   
   AUTHOR:       John Bongiorni 10/4/2007 11:07:12 PM
   
   Modifications:  
   ------------------------------------------------------------ */  ------------------ UPDATE -------------------

CREATE PROCEDURE dbo.usp_PracticeCatalogSupplierBrand_Update_Brands_By_Vendor
(
	  @PracticeCatalogSupplierBrandID INT
	, @BrandName                     VARCHAR(50)
	, @BrandShortName                VARCHAR(50)
	, @ModifiedUserID                INT
)
AS
BEGIN
	DECLARE @Err INT

	UPDATE dbo.PracticeCatalogSupplierBrand
	SET
		  BrandName = @BrandName
		, BrandShortName = @BrandShortName
		, ModifiedUserID = @ModifiedUserID
		, ModifiedDate = GETDATE()

	WHERE 
		PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID

	SET @Err = @@ERROR

	RETURN @Err
End