﻿
/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Practice_Clinician_Delete
   
   Description:  Deletes a Clinician from a Practice.
			This actually set the IsActive flag to false.
			The tables updated are Contact and Practice.
   
   AUTHOR:       Basil Juan 11/5/2014 5:20:17 PM
   
   Modifications:  
   ------------------------------------------------------------ */  

CREATE PROCEDURE [dbo].[usp_Practice_Clinician_Delete]
(
      @ClinicianID					 INT
	, @ModifiedUserID                INT
)
AS
DECLARE
    @TransactionCountOnEntry    INT,       -- Transaction Count before the transaction begins
    @Err             		INT        --  holds the @@Error code returned by SQL Server

SELECT @Err = @@ERROR
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF @Err = 0
BEGIN    
    SELECT @TransactionCountOnEntry = @@TRANCOUNT
    BEGIN TRANSACTION
END        


IF @Err = 0
BEGIN

    UPDATE dbo.Contact
	SET
		  ModifiedUserID = @ModifiedUserID
		, ModifiedDate = GETDATE()
		, IsActive = 0
	FROM dbo.Contact AS C 
	INNER JOIN dbo.Clinician AS Clin 
		ON C.ContactID = Clin.ContactID 
	WHERE 
		Clin.ClinicianID = @ClinicianID

    SELECT @Err = @@ERROR

END
IF @Err = 0
BEGIN

    UPDATE dbo.Clinician
	SET
		  ModifiedUserID = @ModifiedUserID
		, ModifiedDate = GETDATE()
		, IsActive = 0
	WHERE 
		ClinicianID = @ClinicianID 


    SELECT @Err = @@ERROR

END

IF @@TranCount > @TransactionCountOnEntry
BEGIN

    IF @Err = 0
        COMMIT TRANSACTION
    ELSE
        ROLLBACK TRANSACTION
        --  Add any database logging here      

END
