﻿

/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocationShippingAddress_Select_One_ByParams
   
   Description:  Selects a record from table dbo.PracticeLocationShippingAddress
   				   and from the related address table and puts values into parameters
   
   Unit Test:
		DECLARE	@return_value int,
				@AddressID int,
				@AttentionOf varchar(50),
				@AddressLine1 varchar(50),
				@AddressLine2 varchar(50),
				@City varchar(50),
				@State char(2),
				@ZipCode char(5),
				@ZipCodePlus4 char(4)

		EXEC	@return_value = [dbo].[usp_PracticeLocationShippingAddress_Select_One_ByParams]
				@PracticeLocationID = 1,		--  Test of PraciceLocation of 1.
				@AddressID = @AddressID OUTPUT,
				@AttentionOf = @AttentionOf OUTPUT,
				@AddressLine1 = @AddressLine1 OUTPUT,
				@AddressLine2 = @AddressLine2 OUTPUT,
				@City = @City OUTPUT,
				@State = @State OUTPUT,
				@ZipCode = @ZipCode OUTPUT,
				@ZipCodePlus4 = @ZipCodePlus4 OUTPUT

		SELECT	@AddressID as N'@AddressID',
				@AttentionOf as N'@AttentionOf',
				@AddressLine1 as N'@AddressLine1',
				@AddressLine2 as N'@AddressLine2',
				@City as N'@City',
				@State as N'@State',
				@ZipCode as N'@ZipCode',
				@ZipCodePlus4 as N'@ZipCodePlus4'

		SELECT	'Return Value' = @return_value

		-- End Unit Test
   
   AUTHOR:       John Bongiorni 8/20/2007 2:15:55 PM
   
   Modifications:  
   ------------------------------------------------------------ */  ---------------------SELECT PARAMS-----------

CREATE PROCEDURE [dbo].[usp_PracticeLocationShippingAddress_Select_One_ByParams]
(
      @PracticeLocationID            INT
    , @AddressID                     INT			OUTPUT
    , @AttentionOf                   VARCHAR(50)    OUTPUT
    , @AddressLine1                  VARCHAR(50)    OUTPUT
    , @AddressLine2                  VARCHAR(50)    OUTPUT
    , @City                          VARCHAR(50)    OUTPUT
    , @State                         CHAR(2)        OUTPUT
    , @ZipCode                       CHAR(5)        OUTPUT
    , @ZipCodePlus4                  CHAR(4)        OUTPUT
)
AS
BEGIN
	DECLARE @Err INT

	SELECT
		  @AddressID = A.AddressID
        , @AttentionOf = AttentionOf
        , @AddressLine1 = AddressLine1
        , @AddressLine2 = AddressLine2
        , @City = City
        , @State = State
        , @ZipCode = ZipCode
        , @ZipCodePlus4 = ZipCodePlus4
        
	FROM 
		dbo.PracticeLocationShippingAddress AS PLSA
	INNER JOIN 
		dbo.Address AS A
			ON PLSA.AddressID = A.AddressID
	
	WHERE 
		PracticeLocationID = @PracticeLocationID
		AND PLSA.IsActive = 1
		AND A.IsActive = 1
		
		--AND IsActive = 1

	SET @Err = @@ERROR

	RETURN @Err
End


