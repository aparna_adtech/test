﻿CREATE proc zzzzzzusp_REport_rec_PrepWork
as
begin

DECLARE @PracticeID INT
SET @PracticeID = 6
--  4211 with mcp only.  -- Works with inner join on union 4211--  Now 1809


select 
		   P.PracticeID
		, P.PracticeName
		, SO.PurchaseOrder		
    	, PL.PracticeLocationID	
		, PL.NAME AS PracticeLocation
		, '5' as a


	, PCSB.SupplierShortName
	, PCSB.BrandShortName
	, ISNULL(PCSB.Sequence, 999) AS Sequence
		, '8' as a

	, PCP.PracticeCatalogProductID
	, MCP.[ShortName] AS ProductShortName
	, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS Side
	, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )			AS Size
	, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender		
	, ISNULL(MCP.Code, '') AS Code
 		, '14' as A

	, SOLI.ActualWholesaleCost
	, SOLI.QuantityOrdered	
	, ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityCheckedIn
	, CASE WHEN ( SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) ) < 0 THEN 0
		ELSE  SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) 
		END									AS QuantityToCheckIn					--  FUDGED due to bad checkin data.
--	, SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0)  AS QuantityToCheckIn  --Correct
	, SOLI.LineTotal
		, '19' as A

	, SOLI.CreatedDate



	
from SupplierOrderLineItem SOLI  WITH (NOLOCK)

INNER JOIN SupplierOrder AS SO  WITH (NOLOCK)
	ON SOLI.SupplierOrderID = SO.SupplierOrderID

INNER JOIN BregVisionOrder AS BVO  WITH (NOLOCK)
	ON SO.BregVisionOrderID = BVO.BregVisionOrderID

INNER JOIN PracticeLocation AS PL  WITH (NOLOCK)
	ON BVO.PracticeLocationID = PL.PracticeLocationID

INNER JOIN Practice AS P  WITH (NOLOCK)
	ON P.PracticeID = PL.PracticeID

inner join PracticeCatalogPRoduct PCP  WITH (NOLOCK)
	on SOLI.PracticeCatalogProductID = 	PCP.PracticeCatalogPRoductID


inner join MasterCatalogProduct MCP  WITH (NOLOCK)
	on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID


INNER JOIN PracticeCatalogSupplierBrand AS PCSB  WITH (NOLOCK)
	ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

LEFT join ProductInventory PI  WITH (NOLOCK)
	on SOLI.PracticeCatalogProductID = PI.PracticeCatalogProductID
	AND PI.PracticeLocationID = PL.PracticeLocationID

LEFT JOIN (SELECT SupplierOrderLineItemID, SUM(Quantity) AS QuantityCheckedIn
			FROM dbo.ProductInventory_OrderCheckIn 
			WHERE IsActive = 1
			GROUP BY SupplierOrderLineItemID)AS SubPIOCI
				ON SOLI.SupplierOrderLineItemID = SubPIOCI.SupplierOrderLineItemID

where 
	P.PracticeID = @PracticeID
	--  AND PI.PracticeLocationID = @PracticeLocationID

	AND P.IsActive = 1
	AND PL.IsActive = 1
	AND BVO.IsActive = 1
	AND SO.IsActive = 1
	AND SOLI.IsActive = 1
	AND PCP.isactive = 1
	AND PCSB.isactive = 1
	AND PI.isactive = 1
	and MCP.isactive = 1

UNION


--DECLARE @PracticeID INT
--SET @PracticeID = 6
select 
		   P.PracticeID
		, P.PracticeName
		, SO.PurchaseOrder		
    	, PL.PracticeLocationID	
		, PL.NAME AS PracticeLocation
		, '5' as a


	, PCSB.SupplierShortName
	, PCSB.BrandShortName
	, ISNULL(PCSB.Sequence, 999) + 1000 AS Sequence
		, '8' as a

	, PCP.PracticeCatalogProductID
	, TPP.[ShortName] AS ProductShortName
	, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS Side
	, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )			AS Size
	, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender		
	, ISNULL(TPP.Code, '') AS Code
		, '14' as A

 
	, SOLI.ActualWholesaleCost
	, SOLI.QuantityOrdered	
	, ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityCheckedIn
	, CASE WHEN ( SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) ) < 0 THEN 0
		ELSE  SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) 
		END									AS QuantityToCheckIn
--	, SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityToCheckIn
	, SOLI.LineTotal
		, '19' as A
	, SOLI.CreatedDate



	
from SupplierOrderLineItem SOLI  WITH (NOLOCK)

INNER JOIN SupplierOrder AS SO  WITH (NOLOCK)
	ON SOLI.SupplierOrderID = SO.SupplierOrderID

INNER JOIN BregVisionOrder AS BVO  WITH (NOLOCK)
	ON SO.BregVisionOrderID = BVO.BregVisionOrderID

INNER JOIN PracticeLocation AS PL  WITH (NOLOCK)
	ON BVO.PracticeLocationID = PL.PracticeLocationID

INNER JOIN Practice AS P  WITH (NOLOCK)
	ON P.PracticeID = PL.PracticeID

inner join PracticeCatalogPRoduct PCP  WITH (NOLOCK)
	on SOLI.PracticeCatalogProductID = 	PCP.PracticeCatalogPRoductID


inner join ThirdPartyProduct TPP  WITH (NOLOCK)
	on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID


INNER JOIN PracticeCatalogSupplierBrand AS PCSB  WITH (NOLOCK)
	ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

LEFT join ProductInventory PI  WITH (NOLOCK)
	on SOLI.PracticeCatalogProductID = PI.PracticeCatalogProductID
	AND PI.PracticeLocationID = PL.PracticeLocationID

LEFT JOIN (SELECT SupplierOrderLineItemID, SUM(Quantity) AS QuantityCheckedIn
			FROM dbo.ProductInventory_OrderCheckIn 
			WHERE IsActive = 1
			GROUP BY SupplierOrderLineItemID)AS SubPIOCI
				ON SOLI.SupplierOrderLineItemID = SubPIOCI.SupplierOrderLineItemID





where 
	P.PracticeID = @PracticeID
	--  AND PI.PracticeLocationID = @PracticeLocationID

	AND P.IsActive = 1
	AND PL.IsActive = 1
	AND BVO.IsActive = 1
	AND SO.IsActive = 1
	AND SOLI.IsActive = 1
	AND PCP.isactive = 1
	AND PCSB.isactive = 1
	AND PI.isactive = 1
	and TPP.isactive = 1

end