﻿

CREATE proc [dbo].[usp_zImport_PCSPortsMed_1_ThirPartyProduct_From_Import_PracticeData__TPSProduct_20071127]
as
begin
--select TOP 4 *    -- 871, --870, --869, --868
--from ThirdPartyProduct 
--order by ThirdPartyProductID desc

		INSERT INTO ThirdPartyProduct
			( PracticeCatalogSupplierBrandID
			, Code
			, Name
			, ShortName

			, Packaging
			, LeftRightSide
			, Size
			, Color
			, Gender
			, IsDiscontinued
			, WholesaleListCost
			, Description

			, CreatedUserID
			, CreatedDate
			, ModifiedUserID
			, ModifiedDate
			, IsActive
			)

		Select 
			  PracticeCatalogSupplierBrandID
			, ProductCode
			, ProductName
			, ProductShortName

			, Packaging
			, LeftRightSide
			, Size
			, ISNULL(Color, '') as color
			, ISNULL(Gender, '') as Gender
			, 0 AS IsDiscontinued
			, WholesaleListCost
			, ISNULL(Description, '') AS Description

			, -13
			, GetDate()
			, -13
			, GetDate()
			, 1
		from Import_PracticeData.dbo.PCSM_3rdProducts_20071127  --SET THIS to IMportdata table.
		where practiceCatalogSupplierBrandID is not null  --  prevent import of extraneous null rows



end

select TOP 4 *    -- 871, --870, --869, --868
from ThirdPartyProduct 
order by ThirdPartyProductID desc
