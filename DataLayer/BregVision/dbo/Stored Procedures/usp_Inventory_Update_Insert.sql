﻿


-- =============================================
--EXEC usp_Inventory_Update_Insert 3, 8, 1, 1, 0
-- Exec usp_Inventory_Update_Insert @PracticeLocationID=3, @PracticeCatalogProductID=33553, @UserID=1, @Quantity=1, @Bilat=1, @ShoppingCartIDReturn=0
-- Author:		Mike Sneen
-- Create date: 04/17/09
-- NOTE: Rewritten by Mike Sneen to clean up logic, add structured try/catch, and raise an error if user tries to add a custom brace and other items to the same cart
-- Description:	Used to update and insert records from the Inventory UI
-- Modification: 02/06/2012 Mike Sneen
-- Created 'isCustomBraceAccessory' in MCP 
-- used list of items previously in this proc to set to true.  
-- Removed physical list of items and query where 'isCustomBraceAccessory' is 1
--  Modification:  
--				2007.11.14 JB
--					On update to shopping cart item, update the quantity rather than replace.

-- =============================================
CREATE PROCEDURE [dbo].[usp_Inventory_Update_Insert]
	-- Add the parameters for the stored procedure here
    @PracticeLocationID INT,
    @PracticeCatalogProductID INT,
    @UserID INT,
    @Quantity int,
    @Bilat int = 0,
	@ShoppingCartIDReturn int output
	
AS
BEGIN TRY
		SET NOCOUNT ON 
		
		declare @Msg varchar(4000)
        DECLARE @TransactionCountOnEntry INT 
        DECLARE @CartExistsRecordCount INT
        Declare @CurrentItemAlreadyInCartCount int
        DECLARE @ShoppingCartID INT
        DECLARE @ShoppingCartItemID INT
        

        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

        SELECT  @TransactionCountOnEntry = @@TRANCOUNT
        BEGIN TRANSACTION  
        --Body
			IF ((@Quantity = 0))
			BEGIN
				Set @Msg = 'No products added to cart (Custom Braces quantity must be 1)'
				RAISERROR(@Msg, 18, 1)	
			END
        
        
        
			SELECT  @CartExistsRecordCount = COUNT(1)
                FROM    dbo.[ShoppingCart]
                WHERE   PracticeLocationID = @PracticeLocationID
                        AND IsActive = 1
                        
                        
			IF ( @CartExistsRecordCount = 0 ) 
            BEGIN   --Insert New record into ShoppingCart
                INSERT  INTO dbo.ShoppingCart
                        (
                          [PracticeLocationID],
                          [ShoppingCartStatusID],
                          [ShippingTypeID],
                          [DeniedComments],
                          [CreatedUserID],
                          IsActive
					
                        )
                VALUES  (
                          @PracticeLocationID,
                          0,
                          4,
                          '',
                          @UserID,
                          1
					
                        )

                Select  @ShoppingCartID = SCOPE_IDENTITY()             
            END           
			ELSE 
            BEGIN --Get the existing shopping cart id
                SELECT  @ShoppingCartID = ShoppingCartID
                FROM    dbo.ShoppingCart
                WHERE   PracticeLocationID = @PracticeLocationID
                        AND IsActive = 1                      
            END   --End Else if @CartExistsRecordCount = 0             
				--the record count wasn't zero  if this item isCustomBrace then it can't be added.  If the item in the cart        isCustomBrace, then this item can't be added
				
				
				--Check if the item being added is a custom brace
				DECLARE @IsCustomBraceCount INT 
				Select 
					@IsCustomBraceCount = COUNT(1)
				from 
					PracticeCatalogProduct PCP
				Left Outer Join MasterCatalogProduct MCP
					on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
				Where 
					ISNULL(MCP.IsCustomBrace, 0) = 1
					AND PCP.PracticeCatalogProductID= @PracticeCatalogProductID
				--END Check if the item being added is a custom brace
				
				
				
				--Check to see if there is a CustomBrace item in the cart
				Declare @CustomBraceInCartCount int
				SELECT  @CustomBraceInCartCount = COUNT(1)
                    FROM    dbo.[ShoppingCartItem] SCI
					Inner Join PracticeCatalogProduct  PCP
						ON SCI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
					Left Outer Join MasterCatalogProduct MCP
						on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
                    WHERE   SCI.[ShoppingCartID] = @ShoppingCartID
                            --AND SCI.[PracticeCatalogProductID] = @PracticeCatalogProductID
                            AND ISNULL(MCP.IsCustomBrace, 0) = 1
                            AND SCI.IsActive = 1
				--END Check to see if there is a CustomBrace item in the cart
				
				--Check to see if the item being added is a custom Brace accessory
				Declare @IsCustomBraceAccessory int
				SELECT @IsCustomBraceAccessory = COUNT(1)
				FROM PracticeCatalogProduct PCPsub
					join MasterCatalogProduct MCPsub on PCPsub.MasterCatalogProductID = MCPsub.MasterCatalogProductID
					WHERE
					(
						MCPsub.isCustomBraceAccessory = 1
						AND PCPsub.PracticeCatalogProductID = @PracticeCatalogProductID
					) 
				
				
				--Check to see if there are any items in the cart
				 Declare @ItemsInCartCount int  
				 SELECT  @ItemsInCartCount = COUNT(1) 
					FROM    dbo.[ShoppingCartItem] SCI
					WHERE   SCI.[ShoppingCartID] = @ShoppingCartID
							AND SCI.IsActive = 1
							AND SCI.PracticeCatalogProductID NOT IN
							(
								Select PracticeCatalogProductID from PracticeCatalogProduct PCPsub1
								join MasterCatalogProduct MCPsub1 on PCPsub1.MasterCatalogProductID = MCPsub1.MasterCatalogProductID
								WHERE
								(
									MCPsub1.isCustomBraceAccessory = 1
								) 
							)



				--Check to see if there are any incompatible items being added to the cart
				Declare @IncompatibleCartItems int  
				SELECT  @IncompatibleCartItems = COUNT(1) 
				FROM    
						[ShoppingCart] SC
						INNER JOIN [ShoppingCartItem] SCIBrace ON SC.[ShoppingCartID] = SCIBrace.[ShoppingCartID]
						INNER JOIN [ShoppingCartItem] SCIAccessory ON SC.[ShoppingCartID] = SCIAccessory.[ShoppingCartID]
						INNER JOIN [PracticeCatalogProduct] PCPBrace ON SCIBrace.[PracticeCatalogProductID] = PCPBrace.[PracticeCatalogProductID] OR PCPBrace.[PracticeCatalogProductID] = @PracticeCatalogProductID
						INNER JOIN [PracticeCatalogProduct] PCPAccessory ON SCIAccessory.[PracticeCatalogProductID] = PCPAccessory.[PracticeCatalogProductID] OR PCPAccessory.[PracticeCatalogProductID] = @PracticeCatalogProductID
						INNER JOIN [MasterCatalogProduct] MCPBrace ON PCPBrace.[MasterCatalogProductID] = MCPBrace.[MasterCatalogProductID] AND MCPBrace.IsCustomBrace = 1
						INNER JOIN [MasterCatalogProduct] MCPAccessory ON PCPAccessory.[MasterCatalogProductID] = MCPAccessory.[MasterCatalogProductID] AND MCPAccessory.IsCustomBraceAccessory = 1
						INNER JOIN [CustomBraceIncompatibility] CBI ON MCPBrace.Code LIKE CBI.CustomBraceCode AND MCPAccessory.Code LIKE CBI.CustomBraceAccessoryCode

				WHERE   
						SC.[IsActive] = 1
						AND SC.[ShoppingCartStatusID] = 0
						AND SCIBrace.[ShoppingCartItemStatusID] = 0
						AND SCIAccessory.[ShoppingCartItemStatusID] = 0
						AND SCIBrace.[IsActive] = 1
						AND SCIAccessory.[IsActive] = 1
						AND PCPBrace.[IsActive] = 1
						AND PCPAccessory.[IsActive] = 1
						AND PCPBrace.[IsThirdPartyProduct] = 0
						AND PCPAccessory.[IsThirdPartyProduct] = 0
						AND SCIBrace.[ShoppingCartID] = @ShoppingCartID
						AND SCIAccessory.[ShoppingCartID] = @ShoppingCartID
				

		
				--Check to see if there are any items in the cart
				--If there is already a custom Brace in the Cart AND This is not a custom brace accessory
				--OR
				--If this is a custom brace and there are already items in the cart(that are not custom brace accessories)
				--then we can't add this item
				IF ((@CustomBraceInCartCount > 0 AND @IsCustomBraceAccessory < 1) OR (@IsCustomBraceCount > 0 and @ItemsInCartCount > 0))--then we can't add
				BEGIN
					--print '@CustomBraceInCartCount='  + cast(@CustomBraceInCartCount as varchar)
					--print '@IsCustomBraceAccessory='  + cast(@IsCustomBraceAccessory as varchar)
					--print '@IsCustomBraceCount='  + cast(@IsCustomBraceCount as varchar)
					--print '@ItemsInCartCount='  + cast(@ItemsInCartCount as varchar)
					IF(@Bilat = 0)
					BEGIN
						Set @Msg = 'No products added to cart (Custom Braces must be the only item in the cart)'
						RAISERROR(@Msg, 17, 1)	
					END
				END
				--if @CustomBraceInCartCount = 0 and @IsCustomBraceCount > 0 and @ItemsInCartCount > 0 --then we can't add
				--if @CustomBraceInCartCount > 0 and @IsCustomBraceCount = 0 --then ok to add add
				--if @CustomBraceInCartCount = 0 and @IsCustomBraceCount = 0 --then ok to add add
				
				IF(@IsCustomBraceCount > 0 and @Quantity > 1)
				BEGIN
					Set @Msg = 'No products added to cart (Custom Brace must have a quantity of 1)'
					RAISERROR(@Msg, 18, 1)	
				END				
				
				--Check to see if any of the items in the cart or items being added are inompatible with one another
				--If there is already a custom Brace in the Cart AND This is not a custom brace accessory
				--OR
				--If this is a custom brace and there are already items in the cart(that are not custom brace accessories)
				--then we can't add this item
				IF (@IncompatibleCartItems > 0)--then we can't add
				BEGIN
					Set @Msg = 'No products added to cart (Custom Braces and Accessories must be compatible)'
					RAISERROR(@Msg, 18, 2)	
				END
		
				--MWS Check to see if this item is in the cart
				--PRINT @ReOrderQuanity 	
                SELECT  @CurrentItemAlreadyInCartCount = COUNT(1)
                FROM    dbo.[ShoppingCartItem]
                WHERE   [ShoppingCartID] = @ShoppingCartID
                        AND [PracticeCatalogProductID] = @PracticeCatalogProductID
                        AND IsActive = 1
                
                
                IF ( @CurrentItemAlreadyInCartCount = 0 ) --MWS if this item is NOT in the cart
                BEGIN   --Insert New record into ShoppingCart
                    DECLARE @wlc SMALLMONEY
					SET @wlc = (select WholesaleCost from PracticeCatalogProduct 
						where PracticeCatalogProduct.PracticeCatalogProductID = @PracticeCatalogProductID)

					DECLARE @defaultLogoFlag BIT
					SET @defaultLogoFlag=0
					SELECT @defaultLogoFlag=(ISNULL(a.IsLogoAblePart, 0) & ISNULL(b.IsLogoAblePart, 0) & ISNULL(c.IsLogoAblePart, 0))
					FROM
						ProductInventory a
						LEFT JOIN PracticeCatalogProduct b ON a.PracticeCatalogProductID=b.PracticeCatalogProductID
						LEFT JOIN MasterCatalogProduct c ON b.MasterCatalogProductID=c.MasterCatalogProductID
					WHERE
						b.PracticeCatalogProductID=@PracticeCatalogProductID
						AND a.PracticeLocationID=@PracticeLocationID
						AND a.IsActive=1
						AND b.IsActive=1
						AND c.IsActive=1

					INSERT  INTO dbo.ShoppingCartItem
                        (
                          [ShoppingCartID],
                          [PracticeCatalogProductID],
                          [ShoppingCartItemStatusID],
                          [Quantity],
                          [ActualWholesaleCost],
                          [CreatedUserID],
                          IsActive,
						  IsLogoPart
                        )
                    VALUES  (
                          @ShoppingCartID,
                          @PracticeCatalogProductID,
                          0,
                          @Quantity,
						  @wlc,	
                          @UserID,
                          1,
						  @defaultLogoFlag
                        )

                    Select  @ShoppingCartItemID = SCOPE_IDENTITY()

                END
                ELSE 
                BEGIN --If the item being added is in the cart then Update Existing record								
					
					IF(@Quantity > 0)
					BEGIN
						UPDATE  [ShoppingCartItem]
						SET     Quantity = Quantity + @Quantity,  
								[ModifiedUserID] = @UserID,
								[ModifiedDate] = GETDATE()
						WHERE   ShoppingCartID = @ShoppingCartID
								AND [PracticeCatalogProductID] = @PracticeCatalogProductID
								AND (
										(@Bilat = 0)
									OR
										(	
											@Bilat = 1
											AND
											Quantity < 2
										)
									)
					END --IF(@Quantity > 0)
					ELSE
					BEGIN --If the quantity is 0, delete the item from the cart.
						DELETE FROM [ShoppingCartItem]
						WHERE ShoppingCartID = @ShoppingCartID
							  AND [PracticeCatalogProductID] = @PracticeCatalogProductID
					END
                END
	
                  
                        
                        
        --END Body 	
		IF @@TRANCOUNT > @TransactionCountOnEntry 
			COMMIT TRANSACTION
			set @ShoppingCartIDReturn = @ShoppingCartID				
   END TRY	
   BEGIN CATCH
		-- Whoops, there was an error
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION
			set @ShoppingCartIDReturn = 0
				-- Raise an error with the details of the exception
		DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
		SELECT @ErrMsg = ERROR_MESSAGE(),
			@ErrSeverity = ERROR_SEVERITY()	
		
	    EXECUTE [dbo].[uspLogError]
	    EXECUTE [dbo].[uspPrintError]
		RAISERROR(@ErrMsg, @ErrSeverity, 1)
   END CATCH
