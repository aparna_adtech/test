﻿CREATE PROC usp_zzMaintenance_FixSupplierBrandSequence
AS
BEGIN

	SELECT * FROM dbo.ThirdPartySupplier WHERE practiceid = 6

	SELECT * FROM dbo.PracticeCatalogSupplierBrand 
	WHERE practiceID = 6 
		AND  IsThirdPartySupplier = 1
		AND  Sequence >=6
	ORDER BY BrandShortName

	SELECT * FROM dbo.PracticeCatalogSupplierBrand 
	WHERE practiceID = 6 
		AND  IsThirdPartySupplier = 1
	--	AND  Sequence >=6
	ORDER BY BrandShortName

	--UPDATE dbo.PracticeCatalogSupplierBrand 
	--SET Sequence = Sequence + 1
	--WHERE practiceID = 6 
	--	AND  IsThirdPartySupplier = 1
	--	AND  Sequence >=6


	--UPDATE dbo.PracticeCatalogSupplierBrand 
	--SET Sequence = 6
	--WHERE 
	--		 practiceID = 6 
	--	AND  IsThirdPartySupplier = 1
	--	AND  Sequence = 1000


	-------------------------------------

	--UPDATE dbo.PracticeCatalogSupplierBrand 
	--SET Sequence = 3
	--WHERE 
	--		 practiceID = 6 
	--	AND  IsThirdPartySupplier = 0
	--	AND  Sequence IS NULL
		
		
	SELECT * FROM dbo.PracticeCatalogSupplierBrand 
	WHERE practiceID = 6 
		AND  IsThirdPartySupplier = 0
	ORDER BY Sequence


		
	--UPDATE dbo.PracticeCatalogSupplierBrand 
	--SET Sequence = 5
	--WHERE 
	--		 practiceID = 6 
	--	AND  IsThirdPartySupplier = 0
	--	AND  Sequence = 3
	--	AND BrandShortName = 'Procare'
	--	

END		