﻿CREATE PROC usp_zGetInfo_MasterCatalogHierarchy_Given_MCS_MCSC
AS
BEGIN

	SELECT 
		  MCS.SupplierName
		, MCS.MasterCatalogSupplierID
		, MCC.NAME AS CategoryName
		, MCC.MasterCatalogCategoryID
		, MCSC.MasterCatalogSubCategoryID
		, MCSC.NAME AS SubCategoryName
		
	FROM MasterCatalogSubCategory AS MCSC

	INNER JOIN dbo.MasterCatalogCategory AS MCC
		ON MCSC.MasterCatalogCategoryID = MCC.MasterCatalogCategoryID

	INNER JOIN dbo.MasterCatalogSupplier AS MCS
		ON MCC.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID

	WHERE MCS.SupplierName = 'Hely Weber'	
		AND (mcsc.[Name] = 'Soft Knee Bracing' 
			OR mcsc.[Name] = 'Wrist Bracing')

END		
	