﻿CREATE PROCEDURE [dbo].[usp_ProductsPurchased_Select_All_By_PracticeID] 
			@PracticeID INT
AS
BEGIN
SELECT		  
			  P.PracticeID
			, PL.PracticeLocationID
			, P.PracticeName AS Practice							
			, PL.Name AS Location
			, SOLI.CreatedDate AS Date
			, SOLI.LineTotal
			, SOLI.QuantityOrdered AS Quantity
			, SOLI.ActualWholesaleCost AS ActualCost
			, PCSB.SupplierShortName AS Supplier
			, PCSB.BrandShortName AS MFG
			, MCP.Code  AS ProductCode
			, MCP.ShortName AS ProductName
FROM         
	SupplierOrderLineItem AS SOLI
INNER JOIN
    PracticeCatalogProduct AS PCP
			ON SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID 
INNER JOIN
	MasterCatalogProduct AS MCP
			ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID 
INNER JOIN
	PracticeCatalogSupplierBrand AS PCSB
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
INNER JOIN
	dbo.SupplierOrder AS SO
			ON SOLI.SupplierOrderID = SO.SupplierOrderID
INNER JOIN
	dbo.BregVisionOrder AS BVO
			ON SO.BregVisionOrderID = BVO.BregVisionOrderID
INNER JOIN	
	dbo.PracticeLocation AS PL
		ON BVO.PracticeLocationID = PL.PracticeLocationID

INNER JOIN
	dbo.Practice AS P
		ON PL.PracticeID = P.PracticeID

WHERE P.PracticeID = @PracticeID

END



