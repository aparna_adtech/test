﻿
--	  4. Insert a Master Catalog Product Into the
--			Practice's Catalog
--      This called the 3 nested procs:
--		1. dbo.usp_MasterCatalogSupplier_Select_MCSupplierID_By_MCProductID	
--		2. dbo.usp_PracticeCatalogSupplierBrand_Insert_BY_PracticeID_AND_MasterCatalogSupplierID	
--		3. dbo.usp_ProductCatalogProduct_Insert_By_ProductID
--  TEST

--		DECLARE	@return_value int,
--				@PracticeCatalogProductID int
--
--		EXEC	@return_value = [dbo].[usp_PracticeCatalogProduct_Insert_By_MasterCatalogProductID]
--				@PracticeID = 2,
--				@MasterCatalogProductID = 2,
--				@UserID = 0,
--				@PracticeCatalogProductID = @PracticeCatalogProductID OUTPUT
--
--		SELECT	@PracticeCatalogProductID as N'@PracticeCatalogProductID'
--
--		SELECT	'Return Value' = @return_value

CREATE PROC [dbo].[usp_PracticeCatalogProduct_Insert_By_MasterCatalogProductID]
(
			  @PracticeID						INT
			, @MasterCatalogProductID			INT
			, @UserID							INT    -- for testing only.
			, @PracticeCatalogProductID			INT OUTPUT
			, @TransferModifiers				BIT
)
AS
BEGIN

DECLARE @MasterCatalogSupplierID   INT		--  MCSupplierID of the MCProductID 
DECLARE @PracticeCatalogSupplierBrandID INT --  ProductCatalogSupplierBrand Identifier for the given PracticeID and MCSupplierID
DECLARE
    @TransactionCountOnEntry    INT,       -- Transaction Count before the transaction begins
    @Err             			INT        --  holds the @@ERROR code returned by SQL Server

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT @TransactionCountOnEntry = @@TRANCOUNT
	SELECT @Err = @@ERROR
	BEGIN TRANSACTION      

	IF @Err = 0
	BEGIN


		--  Document
		EXEC dbo.usp_MasterCatalogSupplier_Select_MCSupplierID_By_MCProductID 
					@MasterCatalogProductID						-- 2
					, @MasterCatalogSupplierID OUTPUT			-- testing 5
					
		SET @Err = @@ERROR

	END
	IF @Err = 0
	BEGIN
					

		--  Document
		EXEC usp_PracticeCatalogSupplierBrand_Insert_BY_PracticeID_AND_MasterCatalogSupplierID
					  @PracticeID								--  2
					, @MasterCatalogSupplierID					--  5
					, @UserID    								--  0 for testing
					, @PracticeCatalogSupplierBrandID OUTPUT	--  19 for testing

		SET @Err = @@ERROR

	END
	IF @Err = 0
	BEGIN
							
		--  Document		
		EXEC dbo.usp_ProductCatalogProduct_Insert_By_ProductID			
		  @PracticeID											--   2
		, @PracticeCatalogSupplierBrandID						--  19
		, @MasterCatalogProductID								--   2
		, @UserID												--   0
		, @TransferModifiers				
		, @PracticeCatalogProductID			 OUTPUT				--   25 for testing.
			
						
		SET @Err = @@ERROR	
		
	END

	IF @@TranCount > @TransactionCountOnEntry
	BEGIN

		IF @Err = 0
			BEGIN 
				COMMIT TRANSACTION
				RETURN @Err
			END
		ELSE
			BEGIN
				ROLLBACK TRANSACTION
				RETURN @Err
				--  Add any database logging here      
			END
	END		
END
