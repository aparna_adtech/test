﻿-- =============================================
-- Author:		Greg Ross
-- Create date: 08/20/07
-- Description:	Used to get the parent table for the oustanding POs in the Outstanding POs section of the Check-in UI. 
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetOustandingPOsforCheckIn_ParentLevel]
	@PracticeLocationID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	SELECT 
	  BVO.BregVisionOrderID
	, BVO.CustomPurchaseOrderCode
	, BVO.PurchaseOrder
	, BVO.CreatedUserID
	, BVOS.Status
	, ST.ShippingTypeName as ShippingType
	, BVO.Total
	, BVO.CreatedDate 
	FROM BregVisionOrder AS BVO
	INNER JOIN BregVisionOrderStatus AS BVOS 
		ON BVO.BregVisionOrderStatusID = BVOS.BregVisionOrderStatusID
	INNER JOIN ShippingType AS ST 
		ON BVO.ShippingTypeID = ST.ShippingTypeID
	WHERE BVO.BregVisionOrderStatusID IN (1,3) 
		AND BVO.isactive = 1 
		AND BVO.PracticeLocationID = @PracticeLocationID 
	ORDER BY CreatedDate

END