﻿
CREATE PROC dbo.usp_PracticeLocation_Select_PrimaryLocation_Given_PracticeID
				  @PracticeID		  INT
				, @PracticeLocationID INT = -1 OUTPUT  
AS
BEGIN
	
	SELECT 	
		@PracticeLocationID = ISNULL(PL.PracticeLocationID, -1) 
	FROM 
		PracticeLocation AS PL
	WHERE 
		PL.PracticeID = @PracticeID
		AND PL.IsPrimaryLocation = 1
		AND PL.IsActive = 1
	
END
