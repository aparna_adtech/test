﻿
-- =============================================
-- Author:		Greg Ross	
-- Create date: 10/12/2007
-- Description:	Adds new HCPCs
-- =============================================
create PROCEDURE [dbo].[usp_AddHCPC]
	-- Add the parameters for the stored procedure here
	@PracticeCatalogProductID int,
	@HCPC varchar(20),
	@UserID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Insert into ProductHCPCs(
		PracticeCatalogProductID
		,HCPCs
		,CreatedUserID
		,CreatedDate
		,IsActive)
		Values
		(@PracticeCatalogProductID,
		 @HCPC,
		 @UserID,
		 getdate(),
		 1)
		 
		

END


GO
--GRANT EXECUTE
--    ON OBJECT::[dbo].[usp_AddHCPC] TO [BregVisionWebApp]
--    AS [dbo];

