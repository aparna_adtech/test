﻿CREATE PROC usp_Report_BregExec_Transaction_By_Month_QtyDispensed
AS
BEGIN

	SELECT 
		P.PracticeName
		, PL.Name AS PracticeLocation
		, YEAR( D.DateDispensed ) AS Year
		, MONTH( D.DateDispensed )  AS Month
		, SUM(DD.Quantity) AS QuantityDispensed
	FROM
		Practice AS P
	INNER JOIN 
		PracticeLocation AS PL
			ON P.PracticeID = PL.PracticeID
	INNER JOIN 
		Dispense AS D
			ON PL.PracticeLocationID = D.PracticeLocationID
	INNER JOIN 
		DispenseDetail AS DD
			ON D.DispenseID = DD.DispenseID
	Group BY
		P.PracticeName
		, PL.Name
		, YEAR( D.DateDispensed ) 
		, MONTH( D.DateDispensed )
	ORDER BY
		P.PracticeName
		, PL.Name	
		, YEAR( D.DateDispensed ) 
		, MONTH( D.DateDispensed )  

END