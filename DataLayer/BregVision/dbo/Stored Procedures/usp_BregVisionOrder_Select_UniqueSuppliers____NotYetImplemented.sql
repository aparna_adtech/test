﻿CREATE PROC dbo.usp_BregVisionOrder_Select_UniqueSuppliers____NotYetImplemented
AS
BEGIN


	--  Get all MC Suppliers, 3rd Party Suppliers, and 3rd Party Vendors.
	--  given the PCSBID.

	--WITH Supplier 
	--SELECT 

	SELECT 

		  PCSB.PracticeID
		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierName
		, PCSB.IsThirdPartySupplier
		, PCSB.MasterCatalogSupplierID
		, PCSB.ThirdPartySupplierID
		, TPS.IsVendor
		
	FROM PracticeCatalogSupplierBrand AS PCSB
	LEFT JOIN MasterCatalogSupplier AS MCS
		ON PCSB.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
	LEFT JOIN ThirdPartySupplier AS TPS
		ON PCSB.ThirdPartySupplierID = TPS.ThirdPartySupplierID
	ORDER BY PCSB.PracticeID



	SELECT 

		  PCSB.PracticeID
		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierName
		, PCSB.IsThirdPartySupplier
		, PCSB.MasterCatalogSupplierID AS SupplierID
	--	, PCSB.ThirdPartySupplierID
	--	, TPS.IsVendor
		, 0							   AS IsVendor
		
	FROM PracticeCatalogSupplierBrand AS PCSB
	INNER JOIN MasterCatalogSupplier AS MCS
		ON PCSB.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
		

	SELECT 

		  PCSB.PracticeID
		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierName
		, PCSB.IsThirdPartySupplier
	--	, PCSB.MasterCatalogSupplierID AS SupplierID
		, PCSB.ThirdPartySupplierID
		, TPS.IsVendor					AS IsVendor
		
	FROM PracticeCatalogSupplierBrand AS PCSB
	INNER JOIN ThirdPartySupplier AS TPS
		ON PCSB.ThirdPartySupplierID = TPS.ThirdPartySupplierID
		AND IsVendor = 0
		
	-- Get the first (top 1) pcSupplierBrand for the given vendor	
	SELECT 

		  PCSB.PracticeID
		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierName
		, PCSB.IsThirdPartySupplier
	--	, PCSB.MasterCatalogSupplierID AS SupplierID
		, PCSB.ThirdPartySupplierID
		, TPS.IsVendor					AS IsVendor
		
	FROM PracticeCatalogSupplierBrand AS PCSB
	INNER JOIN ThirdPartySupplier AS TPS
		ON PCSB.ThirdPartySupplierID = TPS.ThirdPartySupplierID
		AND TPS.IsVendor = 1
		AND PCSB.PracticeID = 6
		

	-- Get the first (top 1) pcSupplierBrand for the given vendor	


	SELECT DISTINCT

		  Vendor.PracticeID
		, MIN(PCSB2.PracticeCatalogSupplierBrandID)
	--	, MIN(PCSB2.SupplierName)
		, Vendor.IsThirdPartySupplier
	--	, PCSB.MasterCatalogSupplierID AS SupplierID
		, Vendor.ThirdPartySupplierID  AS SupplierID
		, Vendor.IsVendor					AS IsVendor
		
	FROM 
		(SELECT DISTINCT

		  PCSB.PracticeID
	--	, PCSB.PracticeCatalogSupplierBrandID
	--	, PCSB.SupplierName
		, PCSB.IsThirdPartySupplier
	--	, PCSB.MasterCatalogSupplierID AS SupplierID
		, PCSB.ThirdPartySupplierID
		, TPS.IsVendor					AS IsVendor
		
	FROM PracticeCatalogSupplierBrand AS PCSB
	INNER JOIN ThirdPartySupplier AS TPS
		ON PCSB.ThirdPartySupplierID = TPS.ThirdPartySupplierID
		AND TPS.IsVendor = 1
		AND PCSB.PracticeID = 6
		AND TPS.IsActive = 1
		AND PCSB.IsActive = 1
	) AS Vendor	
	INNER JOIN PracticeCatalogSupplierBrand AS PCSB2
		ON  Vendor.PracticeID = PCSB2.PracticeID
		AND Vendor.ThirdPartySupplierID = PCSB2.ThirdPartySupplierID
		AND PCSB2.IsActive = 1
	GROUP BY
		  Vendor.PracticeID
		, Vendor.ThirdPartySupplierID
		, Vendor.IsThirdPartySupplier
		, Vendor.IsVendor
		
		
END