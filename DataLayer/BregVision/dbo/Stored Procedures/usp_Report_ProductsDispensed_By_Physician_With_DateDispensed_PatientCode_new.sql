﻿
/*
DECLARE @PhysicianIDString VARCHAR(2000)
SET @PhysicianIDString = '34, 35, 36, 37, 38, 39,NulL'
			 SELECT ID 
			 FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianIDString, ',')

SELECT ISNULL(ID, -999)
FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianIDString, ',')

declare @isBit bit

-- works~~~
SELECT * from practice
where isactive = (SELECT CASE WHEN ISNULL(ID, -999) = -999 THEN 1 ELSE 0 END --AS IsNoPhysician 
		 FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianIDString, ',')
		 WHERE ISNULL(ID, -999) = -999)
			
SELECT @isbit = CASE WHEN ISNULL(ID, -999) = -999 THEN 1 ELSE 0 END --AS IsNoPhysician 
		 FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianIDString, ',')
		 WHERE ISNULL(ID, -999) = -999

select @isbit as isbit

WHERE ISNULL(ID, -999) = -999 --NULL 


SELECT PH.PhysicianID, * 
FROM DispenseDetail  AS DD
LEFT JOIN Physician AS PH
	ON DD.PhysicianID = PH.PhysicianID
WHERE DD.PhysicianID IN ( 2,3 )
	OR DD.PhysicianID IS NULL

SELECT COUNT(ID) WHERE ID IS NULL 

*/
--DECLARE @PhysicianIDString VARCHAR(2000)
--SET @PhysicianIDString =  '2,NULL'  --'34, 35, 36, 37, 38, 39,NULL'
--SELECT PH.PhysicianID, * FROM DispenseDetail AS DD
--LEFT JOIN Physician AS PH
--	ON DD.PhysicianID = PH.PhysicianID
--WHERE 	
--	PH.PhysicianID 
--		IN (
--			 SELECT ID 
--			 FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianIDString, ',')
--			)
--		OR 
--	( 
--		(PH.PhysicianID IS NULL) 
--			AND 
--		( 0 = (SELECT CASE WHEN ISNULL(ID, -999) = -999 THEN 1 ELSE 0 END --AS IsNoPhysician 
--		 FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianIDString, ',')
--		 WHERE ISNULL(ID, -999) = -999)
--		) 
--	)
--ORDER BY PH.PhysicianID
--
--
--*/


--		IN (
--			 SELECT ID 
--			 FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianIDString, ',')
--			)
			


--  [usp_Report_ProductsDispensed_By_Physician_With_DateDispensed_PatientCode]  6, '13, 15, 16, 17, 18, 19, 20, 22, 23, 24', 
--		'34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 58, 59, 60, 61, 62, 63, 64, 65, 66'  
--		,'1/1/2007', '12/31/2007'  -- 1110

----
--*/
--  [usp_Report_ProductsDispensed_By_Physician_With_DateDispensed_PatientCode]  18, '56,57', '1', '1/1/2007', '12/31/2007'
--    [usp_Report_ProductsDispensed_By_Physician_With_DateDispensed_PatientCode]  1, '3', '2,-999', '1/1/2007', '12/31/2007'  --94

--  Added Supplier Brand.
--  Added Sequence
--  Add IsActive = 1

-- Modification:  , D.DateDispensed AS DateDispensed  -- , DD.CreatedDate AS DateDispensed  --  2008.02.12  JB

create PROC [dbo].[usp_Report_ProductsDispensed_By_Physician_With_DateDispensed_PatientCode_new]  
		  @PracticeID			INT
		, @PracticeLocationID	VARCHAR(2000)
		, @PhysicianID			VARCHAR(2000)
		, @StartDate			DATETIME
		, @EndDate				DATETIME
		, @OrderBy				int
		, @OrderDirection		bit
AS
BEGIN


--SELECT * FROM PracticelOCATION WHERE PRACTICEID = 18
--select * from Practice WHERE PRACTICEID = 18


--DECLARE @Output VARCHAR(500)
--SET @Output = ''
--
--SELECT @Output =
--	CASE WHEN @Output = '' THEN CAST(PracticeLocationID AS VARCHAR(50))
--		 ELSE @Output + ', ' + CAST(PracticeLocationID AS VARCHAR(50))
--		 END
--FROM PracticeLocation
--WHERE PracticeID = 6

--
--SELECT @Output AS Output

--	DECLARE @PracticeID INT
--	SET @PracticeID = 6

--  SET @EndDate to 11:59:59 PM
SELECT @EndDate = DATEADD(DAY, 1, @EndDate)

DECLARE @PracticeLocationIDString VARCHAR(2000)
DECLARE @PhysicianIDString VARCHAR(2000)

SET @PracticeLocationIDString = @PracticeLocationID
SET @PhysicianIDString = @PhysicianID


--DECLARE @PhysicianIDString VARCHAR(2000)
--SET @PhysicianIDString = @PhysicianID
---  Insert select here ----

INSErt into dbo.zzzReportMessage
		(MESSAGE)
SELECT @PhysicianIDString 



SELECT 
		  
		  ISNULL(SUB.PhysicianID, -999) AS PhysicianID
		, SUB.PracticeName		AS Practice
		, SUB.PracticeLocation	AS Location

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
			ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
			END					AS SupplierBrand


		, SUB.SupplierShortName	AS Supplier

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
		ELSE SUB.BrandShortName
		END						AS Brand

--		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN 1
--		ELSE 0
--		END AS IsBrandSuppressed
--		, SUB.IsThirdPartySupplier
		, SUB.Sequence	

		, SUB.ProductName			AS Product

		, SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender

		, SUB.Code

		, dbo.ConcatHCPCS(SUB.PracticeCatalogProductID) AS HCPCS -- JB 20071004 Added Column for UDF Concat of HCPCS
	
		
		, CASE WHEN LEN(SUB.Physician) > 0 THEN SUB.Physician ELSE '<blank>' END AS Physician
		, CONVERT(VARCHAR(10), SUB.DateDispensed, 101) AS DateDispensed		
		, SUB.PatientCode			


		, SUB.Quantity 		--, SUM( SUB.Quantity )

--		, CAST( SUB.ActualWholesaleCost AS DECIMAL(15,2) )	AS ActualWholesaleCost
--		, CAST( SUB.ActualChargeBilled AS DECIMAL(15,2) )	AS ActualChargeBilled
--		, CAST( SUB.DMEDeposit AS DECIMAL(15,2) )			AS DMEDeposit

		, CAST(  SUB.ActualWholesaleCostLineTotal  AS DECIMAL(15,2) )	AS ActualWholesaleCostLineTotal
		, CAST(  SUB.DMEDepositLineTotal  AS DECIMAL(15,2) )			AS DMEDepositLineTotal
		, CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )    AS ActualChargeBilledLineTotal
		
--		, SUB.Comment  -- Not Displayed for Version 1.
FROM


		(
		SELECT 
			  Ph.PhysicianID
			, P.PracticeName
			, PL.Name AS PracticeLocation
			, IsNULL(RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix), '') AS Physician

			, D.PatientCode
			
			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) AS Sequence

			, PCP.PracticeCatalogProductID

			, MCP.[Name] AS ProductName

			, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
			
			, ISNULL(MCP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed  -- , DD.CreatedDate AS DateDispensed  --  2008.02.12  JB  --, DD.CreatedDate AS DateDispensed

			, DD.Quantity
			, PI_DD.ActualWholesaleCost 
			, DD.ActualChargeBilled
			, DD.DMEDeposit	

			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit * DD.Quantity AS DMEDepositLineTotal
			, PI_DD.ActualWholesaleCost * DD.Quantity AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.
	
		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOin DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID 

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
		INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
			ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID		    --  Can be inactive

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1

		WHERE 
			    P.PracticeID = @PracticeID		
			AND P.IsActive = 1
			AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			AND PCP.IsActive = 1
			AND PCSB.IsActive = 1
			AND MCP.IsActive = 1
		
		AND 
		PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))

--		AND 
--		Ph.PhysicianID IN 
--			(SELECT ID 
--			 FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianIDString, ','))
		
		AND
			(       
					PH.PhysicianID 
					IN (
						 SELECT ID 
						 FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianIDString, ',')
						)
					OR 
				( 
					(PH.PhysicianID IS NULL) 
						AND 
					( 1 = (SELECT CASE WHEN ID = -999 THEN 1 ELSE 0 END --AS IsNoPhysician 
					 FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianIDString, ',')
					 WHERE ID = -999)
					) 
				)
			)
	
		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM
			
		UNION

		SELECT 

			  Ph.PhysicianID
			, P.PracticeName
			, PL.Name AS PracticeLocation
			, IsNULL(RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix), '') AS Physician

			, D.PatientCode
			
			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) + 1000 AS Sequence

			, PCP.PracticeCatalogProductID

			, TPP.[Name] AS ProductName

			, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender


			, ISNULL(TPP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed  -- , DD.CreatedDate AS DateDispensed  --  2008.02.12  JB

			, DD.Quantity
			, PI_DD.ActualWholesaleCost 
			, DD.ActualChargeBilled
			, DD.DMEDeposit	

			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit * DD.Quantity AS DMEDepositLineTotal
			, PI_DD.ActualWholesaleCost * DD.Quantity AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.




		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOin DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID 

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
		INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
			ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID			--  Can be inactive

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1	
	
		WHERE 
			P.PracticeID = @PracticeID		
			AND P.IsActive = 1
			AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			AND PCP.IsActive = 1
			AND PCSB.IsActive = 1
			AND TPP.IsActive = 1

		AND 
		PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))
	
		AND
			(       
					PH.PhysicianID 
					IN (
						 SELECT ID 
						 FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianIDString, ',')
						)
					OR 

				( 
					(PH.PhysicianID IS NULL) 
						AND 
					( 1 = (SELECT CASE WHEN ID = -999 THEN 1 ELSE 0 END --AS IsNoPhysician 
					 FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianIDString, ',')
					 WHERE ID = -999)
					) 
				)

--				( 
--					(PH.PhysicianID IS NULL) 
--						AND 
--					( 1 = (SELECT CASE WHEN ISNULL(ID, -999) = -999 THEN 1 ELSE 0 END --AS IsNoPhysician 
--					 FROM dbo.udf_ParseArrayToTable_INT_With_Null(@PhysicianIDString, ',')
--					 WHERE ISNULL(ID, -999) = -999)
--					) 
--				)
			)


		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM	

		) AS Sub

----
------GROUP BY
------		SUB.PracticeName
------		, SUB.PracticeLocation
------		, SUB.Physician
------		, SUB.IsThirdPartySupplier
------		, SUB.Sequence
------		, SUB.SupplierShortName
------		, SUB.BrandShortName
------		
------		, SUB.ProductName
------		, SUB.Side
------		, SUB.Size
------		, SUB.Gender
------		, SUB.Code
------		
------		, SUB.PatientCode
------
------		, SUB.DateDispensed
------
------		, SUB.ActualWholesaleCost
------		, SUB.ActualChargeBilled
------		, Sub.DMEDeposit
------		, Sub.Comment
----
----		

--if @OrderBy	= 1 
--begin
	 order by

		CASE WHEN @OrderBy = 1 and @OrderDirection = 0  THEN SUB.ProductName End DESC,
		CASE WHEN @OrderBy = 1 and @OrderDirection = 1  THEN SUB.ProductName END,
		CASE WHEN @OrderBy = 2 and @OrderDirection = 0  THEN SUB.DateDispensed End DESC,
		CASE WHEN @OrderBy = 2 and @OrderDirection = 1  THEN SUB.DateDispensed END,
		CASE WHEN @OrderBy = 3 and @OrderDirection = 0  THEN SUB.ProductName End DESC,
		CASE WHEN @OrderBy = 3 and @OrderDirection = 1  THEN SUB.ProductName END
	

--	if @OrderDirection = 0 
--	begin
--		select ' desc'
--	end
--	CASE 
--		WHEN @OrderDirection = 0 THEN ' desc'
--	END 
--SUB.ProductName 
--end 
----	ORDER BY

----		SUB.PracticeName
----		, SUB.PracticeLocation
----
------		, SUB.Physician
----
----		, SUB.Sequence		--  Note 3rd Party has 100 added to it sequence for this query.
------		, SUB.IsThirdPartySupplier
----		, SUB.SupplierShortName
----		, SUB.BrandShortName
----
----		, SUB.ProductName
----		, SUB.Code
----
----		, SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender
----		, SUB.DateDispensed		--  JB  2008.01.09
----		, SUB.PatientCode		--  JB  2008.01.09

END
