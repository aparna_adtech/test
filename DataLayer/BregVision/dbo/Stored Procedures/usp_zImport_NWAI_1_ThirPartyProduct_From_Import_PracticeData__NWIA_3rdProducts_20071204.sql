﻿

create proc [dbo].[usp_zImport_NWAI_1_ThirPartyProduct_From_Import_PracticeData__NWIA_3rdProducts_20071204]
as
begin

INSERT INTO ThirdPartyProduct
	( PracticeCatalogSupplierBrandID
	, Code
	, Name
	, ShortName

	, Packaging
	, LeftRightSide
	, Size
	, Color
	, Gender
	, IsDiscontinued
	, WholesaleListCost
	, Description

	, CreatedUserID
	, CreatedDate
	, ModifiedUserID
	, ModifiedDate
	, IsActive
	, Source
	)

Select 
	  PracticeCatalogSupplierBrandID
	, ProductCode
	, ProductName
	, ProductShortName

	, Packaging
	, LeftRightSide
	, Size
	, ISNULL(Color, '')		AS Color
	, ISNULL(Gender, '')	as Gender
	, 0						AS IsDiscontinued
	, WholesaleListCost
	, ISNULL(Description, '') as Description

	, -13			as CreatedUserID
	, GetDate()		as CreatedDate
	, -13			as ModifiedUserID
	, GetDate()		as ModifiedDate
	, 1				as IsActive
	, Source
from Import_PracticeData.dbo.NWIA_3rdProducts_20071204

end