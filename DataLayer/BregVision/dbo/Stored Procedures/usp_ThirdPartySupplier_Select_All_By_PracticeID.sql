﻿
/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_ThirdPartySupplier_Select_All_By_PracticeID
   
   Description:  Selects all records from the table dbo.ThirdPartySupplier
					given a practiceID
   
   AUTHOR:       John Bongiorni 9/26/2007 3:46:45 PM
   
   Modifications:  JB 10/3/2007  3:45PM  Order by sequence if there is one, then by Supplier Short Name.
   ------------------------------------------------------------ */  -------------------SELECT ALL By PracticeID

CREATE PROCEDURE [dbo].[usp_ThirdPartySupplier_Select_All_By_PracticeID] 
		@PracticeID	INT
AS
BEGIN
	DECLARE @Err INT
	
	SELECT
		  ThirdPartySupplierID
		, PracticeID
--		, ContactID
--		, AddressID
		, SupplierName
		, SupplierShortName
		
		, IsVendor		
		, IsEmailOrderPlacement
		
		--  , CASE IsVendor WHEN 1 THEN 'Vendor' WHEN 0 THEN 'Manufacturer' END AS SupplierType
		, CASE IsVendor WHEN 1 THEN 'Vendor' WHEN 0 THEN 'Manufacturer' END AS SupplierType
		
		, CASE IsVendor WHEN 1 THEN 'Brands' WHEN 0 THEN '' END AS VendorLink
		
		, CASE IsEmailOrderPlacement WHEN 1 THEN 'Email' WHEN 0 THEN 'Fax' END AS OrderPlacementType
		
		, ISNULL(FaxOrderPlacement, '') AS FaxOrderPlacement
		, ISNULL(EmailOrderPlacement, '') AS EmailOrderPlacement
		, IsFaxVerified

--		, ISNULL(Sequence, 0)
--		, CreatedUserID
--		, CreatedDate
--		, ModifiedUserID
--		, ModifiedDate
--		, IsActive
	
	FROM dbo.ThirdPartySupplier
	WHERE 
		PracticeID = @PracticeID
		AND IsActive = 1
	ORDER BY
		ISNULL(Sequence, 999)
		, SupplierShortName
	
	SET @Err = @@ERROR

	RETURN @Err
END