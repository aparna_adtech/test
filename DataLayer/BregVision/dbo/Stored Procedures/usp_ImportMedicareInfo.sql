﻿
--exec [usp_ImportMedicareInfo]

Create PROCEDURE [dbo].[usp_ImportMedicareInfo]

AS
BEGIN

declare @status int,
	@Process varchar(40),
	@Msg varchar(300)
	--@RestoreTableCount int

set @status = 0
SET NoCount on
	
BEGIN TRANSACTION

--Step 1

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Medicare_Backup]') AND type in (N'U'))
DROP TABLE [dbo].Medicare_Backup

	if @status <> 0 
	begin
		SET @Msg='Error dropping Medicare_Backup'
		goto COMMIT_OR_ROLLBACK
	end
	
	
--Step 2
exec @status=sp_rename Medicare, Medicare_Backup

	if @status <> 0 
	begin
		SET @Msg='Error renaming Medicare'
		goto COMMIT_OR_ROLLBACK
	end
	
	
--Step 3
/****** Object:  Table [dbo].[MedicareStep1]    Script Date: 03/17/2009 00:46:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MedicareStep1]') AND type in (N'U'))
DROP TABLE [dbo].MedicareStep1

	if @status <> 0 
	begin
		SET @Msg='Error dropping MedicareStep1'
		goto COMMIT_OR_ROLLBACK
	end

--Step 4
Select  Distinct Cast(HCPCS as nvarchar(8)) AS HCPCS, Cast(Mod as nvarchar(8)) AS [Mod], Cast(Mod2 as nvarchar(8)) AS Mod2, Cast([State] as nvarchar(2)) AS [State], Cast(MedicareCharge as smallmoney) AS MedicareCharge, [Description] Into MedicareStep1 from MedicareImport
	UNPIVOT(MedicareCharge for [State] in(
[AL],
[AR],
[AZ],
[CA],
[CO],
[CT],
[DC],
[DE],
[FL],
[GA],
[IA],
[ID],
[IL],
[IN],
[KS],
[KY],
[LA],
[MA],
[MD],
[ME],
[MI],
[MN],
[MO],
[MS],
[MT],
[NC],
[ND],
[NE],
[NH],
[NJ],
[NM],
[NV],
[NY],
[OH],
[OK],
[OR],
[PA],
[RI],
[SC],
[SD],
[TN],
[TX],
[UT],
[VA],
[VT],
[WA],
[WI],
[WV],
[WY],
[AK],
[HI],
[PR],
[VI])) as U

	if @status <> 0 
	begin
		SET @Msg='Error inserting into MedicareStep1'
		goto COMMIT_OR_ROLLBACK
	end

--Step 5

/****** Object:  Table [dbo].[Medicare_InProgress]    Script Date: 03/17/2009 00:46:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Medicare_InProgress]') AND type in (N'U'))
DROP TABLE [dbo].Medicare_InProgress

	if @status <> 0 
	begin
		SET @Msg='Error dropping Medicare_InProgress'
		goto COMMIT_OR_ROLLBACK
	end


--Step 6 make a copy of the step1 table
Select 
	HCPCs,
	[State],
	MedicareCharge,
	[Description]
into Medicare_InProgress
from MedicareStep1
Where 1=2

	if @status <> 0 
	begin
		SET @Msg='Error creating Medicare_InProgress'
		goto COMMIT_OR_ROLLBACK
	end

--select COUNT(*)  from Medicare_InProgress where  MedicareCharge is null

--Step 7
Insert into Medicare_InProgress(HCPCS, [STATE])
Select  Distinct 
HCPCS,
[State]
from MedicareStep1

	if @status <> 0 
	begin
		SET @Msg='Error inserting distinct HCPCs, States into Medicare_InProgress'
		goto COMMIT_OR_ROLLBACK
	end

--select COUNT(*)  from Medicare_InProgress where  MedicareCharge is null


--Step 8
Update Medicare_InProgress
Set MedicareCharge = (
						Select MedicareCharge 
						from MedicareStep1 
						where 
							Medicare_InProgress.HCPCs = MedicareStep1.HCPCS
							AND Medicare_InProgress.[State] = MedicareStep1.[State]
							AND MedicareStep1.[Mod] = 'NU'
							AND MedicareStep1.Mod2 is null
						),
	 [Description] = (
						Select [Description] 
						from MedicareStep1 
						where 
							Medicare_InProgress.HCPCs = MedicareStep1.HCPCS
							AND Medicare_InProgress.[State] = MedicareStep1.[State]
							AND MedicareStep1.[Mod] = 'NU'
							AND MedicareStep1.Mod2 is null
						)					
						
--select COUNT(*)  from Medicare_InProgress where  MedicareCharge is null

	if @status <> 0 
	begin
		SET @Msg='Error inserting NU values into Medicare_InProgress'
		goto COMMIT_OR_ROLLBACK
	end

--Step 9
Update Medicare_InProgress
Set MedicareCharge = (
						Select TOP 1 MedicareCharge 
						from MedicareStep1 
						where 
							Medicare_InProgress.HCPCs = MedicareStep1.HCPCS
							AND Medicare_InProgress.[State] = MedicareStep1.[State]
							AND MedicareStep1.[Mod] <> 'NU'
							--AND Medicare_InProgress.MedicareCharge is null
							--AND MedicareStep1.Mod2 is null
						),
    	 [Description] = (
						Select TOP 1 [Description]
						from MedicareStep1 
						where 
							Medicare_InProgress.HCPCs = MedicareStep1.HCPCS
							AND Medicare_InProgress.[State] = MedicareStep1.[State]
							AND MedicareStep1.[Mod] <> 'NU'
							--AND Medicare_InProgress.MedicareCharge is null
							--AND MedicareStep1.Mod2 is null
						) 						
where Medicare_InProgress.MedicareCharge is null

--select COUNT(*)  from Medicare_InProgress where  MedicareCharge is null

	if @status <> 0 
	begin
		SET @Msg='Error inserting non-NU values into Medicare_InProgress'
		goto COMMIT_OR_ROLLBACK
	end
						
--Step 10
Update Medicare_InProgress
Set MedicareCharge = (
						Select TOP 1 MedicareCharge 
						from MedicareStep1 
						where 
							Medicare_InProgress.HCPCs = MedicareStep1.HCPCS
							AND Medicare_InProgress.[State] = MedicareStep1.[State]
						),
    	 [Description] = (
						Select TOP 1 [Description]
						from MedicareStep1 
						where 
							Medicare_InProgress.HCPCs = MedicareStep1.HCPCS
							AND Medicare_InProgress.[State] = MedicareStep1.[State]
						)
where Medicare_InProgress.MedicareCharge is null

--select COUNT(*)  from Medicare_InProgress where  MedicareCharge is null

	if @status <> 0 
	begin
		SET @Msg='Error inserting all other values into Medicare_InProgress'
		goto COMMIT_OR_ROLLBACK
	end
	
	
--Step 11

/****** Object:  Index [IX_Medicare]    Script Date: 03/19/2009 18:39:57 ******/
CREATE NONCLUSTERED INDEX [IX_Medicare] ON [dbo].Medicare_InProgress 
(
	[HCPCs] ASC,
	[State] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
if @status <> 0 
	begin
		SET @Msg='Error creating index on Medicare_InProgress'
		goto COMMIT_OR_ROLLBACK
	end

--Step 12

exec @status=sp_rename Medicare_InProgress, Medicare

	if @status <> 0 
	begin
		SET @Msg='Error renaming Medicare_InProgress to Medicare'
		goto COMMIT_OR_ROLLBACK
	end
COMMIT_OR_ROLLBACK:
IF @status<>0
	BEGIN
		ROLLBACK
		--INSERT INTO support..Log_SupportProcessing VALUES (default, @Process, @Msg, 1 )
		PRINT @Status + ' - ' + @Msg
	END
ELSE
	COMMIT
		
RETURN @status

END