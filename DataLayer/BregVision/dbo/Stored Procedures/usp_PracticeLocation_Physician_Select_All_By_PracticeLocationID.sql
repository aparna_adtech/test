﻿CREATE PROCEDURE [dbo].[usp_PracticeLocation_Physician_Select_All_By_PracticeLocationID]
		@PracticeLocationID INT
AS
BEGIN	
	SELECT
		PH.PhysicianID
	  , C.ContactID
	  , C.Salutation
	  , C.FirstName
	  , C.MiddleName
	  , C.LastName
	  , C.Suffix
	  , C.FirstName + ' ' + C.LastName as PhysicianName
	
	FROM		
		dbo.Contact AS C

	INNER JOIN	
		dbo.Physician AS PH
			ON C.ContactID = PH.ContactID
	
	INNER JOIN	
		dbo.PracticeLocation_Physician AS PLP		
			ON PH.PhysicianID = PLP.PhysicianID

	WHERE
		PLP.PracticeLocationID = @PracticeLocationID
		AND PH.IsActive = 1
		AND C.IsActive = 1
END



