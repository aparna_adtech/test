﻿
-- eRROR ON INSERT FROM EXIST INACTIVE ON INSERT.
CREATE PROC [dbo].[usp_PracticeLocationBillingAddresses_Update_From_Centralized_PracticeBillingAddress]  
			  @PracticeID		INT
			, @UserID	INT
AS
	BEGIN

		DECLARE	@AttentionOf VARCHAR(50)
		DECLARE @AddressID INT

		--  Get the Attentionof field and the addressID field from the PracticeBillingAddress.

			SELECT 
				  @AttentionOf = AttentionOf
				, @AddressID = AddressID
			FROM 
				dbo.PracticeBillingAddress WITH (NOLOCK)
			WHERE
				PracticeID = @PracticeID
				AND IsActive = 1
			
			
		--  Practice is Centralized so populate any practiceLocation Billing Address records to match the Practice's Billing Address.
			DECLARE @TablePracticeLocation TABLE 
				(
					  PracticeID INT
					, PracticeLocationID INT
					, PracticeLocationBillingAddress_AddressID INT
				)


		--  Get the Practice Location ID (active) for each Location of the Practice 
		--  Put into table variable @tablePLIDs with column ID

			INSERT INTO @TablePracticeLocation
				(   PracticeID
				  , PracticeLocationID
				)
			SELECT
				  PracticeID 
				, PracticeLocationID
			FROM dbo.PracticeLocation WITH (NOLOCK)
			WHERE PracticeID = @PracticeID
				AND IsActive = 1
					
					
		-- Update table variable with the PracticeLocationBillingAddressID (active) if it exists otherwise null			
				
		UPDATE @TablePracticeLocation
		SET PracticeLocationBillingAddress_AddressID = 
			PLBA.AddressID
			FROM dbo.PracticeLocationBillingAddress AS PLBA
			INNER JOIN @TablePracticeLocation AS TPL
				ON PLBA.PracticeLocationID = TPL.PracticeLocationID  
			--WHERE 
				--  PLBA.IsActive = 1
			
					
			---SELECT * FROM @TablePracticeLocation 
			
		--	if PracticeLocationBillingAddress_AddressID not exist insert into with AddressID and AttentionOF	
		INSERT INTO dbo.PracticeLocationBillingAddress
			( 
			   PracticeLocationID 
			 , AddressID
			 , AttentionOf
			 , CreatedUserID
			 , CreatedDate 
			 , IsActive )	
		SELECT 	
			   TPL.PracticeLocationID 
			 , @AddressID
			 , @AttentionOf
			 , @UserID
			 , GETDATE()
			 , 1 
		FROM @TablePracticeLocation AS TPL
		WHERE TPL.PracticeLocationBillingAddress_AddressID IS NULL
			 	 
		--  SELECT * FROM dbo.PracticeLocationBillingAddress AS PLBA	Just for testing.


		--	If PracticeLocationBillingAddress_AddressID exists update with AttentionOF
		--		Note the AddressID has already been been set previously.
		UPDATE dbo.PracticeLocationBillingAddress
		SET	   
		--	   PracticeLocationID = TPL.PracticeLocationID 
			   AddressID =  @AddressID
			 , AttentionOf = @AttentionOf
			 , ModifiedUserID = @UserID
			 , ModifiedDate = GETDATE()
			 , IsActive = 1
		FROM @TablePracticeLocation AS TPL
		INNER JOIN dbo.PracticeLocationBillingAddress AS PLBA
			ON TPL.PracticeLocationID = PLBA.PracticeLocationID	
			
		--  SELECT * FROM dbo.PracticeLocationBillingAddress AS PLBA	Just for testing.

END					
