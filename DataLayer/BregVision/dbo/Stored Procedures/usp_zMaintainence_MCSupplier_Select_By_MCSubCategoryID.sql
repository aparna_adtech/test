﻿CREATE PROC usp_zMaintainence_MCSupplier_Select_By_MCSubCategoryID
			@MasterCatalogSubcategoryID		INT
AS
BEGIN
	
	SELECT *
	FROM MasterCatalogSubcategory AS MCSC
	INNER JOIN MasterCatalogCategory AS MCC
		ON MCSC.MasterCatalogCategoryID = MCC.MasterCatalogCategoryID
	INNER JOIN MasterCatalogSupplier AS MCS
		ON MCS.MasterCatalogSupplierID = MCC.MasterCatalogSupplierID
	WHERE MCSC.MasterCatalogSubCategoryID IN
				(
					@MasterCatalogSubcategoryID
				)
		
END