﻿--  usp_PracticeCatalogProduct_Delete
/*
	Proc:		usp_PracticeCatalogProduct_Delete99
	Author:		John Bongiorni
	Date:		2008.02.22
	
	Description:  Delete the PracticeCatalogProduct
					if it is deletable.
					
    Steps:       
		Given the PCPID, Get the PracticeID.
		With the PracticeID, get the PracticeLocationIDs
		With the PLIDs and the PCPIDs check the count
		  of PIIDs for each location, if any exist,
		  do not delete, and retrieve the name of 
		  the location.					
		1.  Check the ProductInventory table for the PCPID>
		2.  Check the PCCatalog for the PCPID.
		3.  Check the other tables for the PCPID.

	Important:  In the future, each check to a table 
	should be made while joining to the practiceLocation table
	so that a InActive PL with PCPs used to not impede the
	delete if all active PLs have no associated PCPs
	
	SELECT 
	PL.PracticeLocationID
	FROM PracticeLocation AS PL
	WHERE pl.ISACTIVE = 0

	SELECT 
		  PL.PracticeLocationID
		, PCP.PracticeCatalogProductID 
	FROM PracticeLocation AS PL
	INNER JOIN PracticeCatalogProduct AS PCP
		ON PCP.PracticeID = PL.PracticeID
	WHERE pl.ISACTIVE = 0
	AND PL.practiceID = 10  note 326 pcps in the inactive location (37) with in practice 10.
	
	Add Deletes and test.
	
	Streamline the structure of this.
	
	Important Print this and make clear for greg and others.
	make sure performance works well.
	
	JB 2008.03.13  On Hard Delete make sure that if the pcpID relates to a thirdpartyproduct to set the thirdpartyproduct as inactive.
	--  Later as the application stablizes, create an audit table for the thirdpartyproducts and delete them.
	JB 2008.04.10  Delete related ThirdPartyProduct on hard 
	delete.  Deleted thirdpartyproduct will end up in the thirdPartyProduct_Audit table.
	
	
*/
  Create PROC [dbo].[usp_PracticeCatalogProduct_Delete_orig]  --  1000000, -20080225, 0, 'a' --1000000  -- 600, 2,  400
  		  @PracticeCatalogProductID	INT
		, @UserID					INT
		, @RemovalType				INT  OUTPUT
		, @DependencyMessage		VARCHAR(2000) OUTPUT  -- To application / user.

  AS 
  BEGIN

--	DECLARE @PracticeCatalogProductID		INT
--	SET @PracticeCatalogProductID	=		600  -- Wellington

	DECLARE @ProductInventoryCount			INT  --  Number of PI records for the practice and PCP.
	DECLARE @PracticeCatalogCategoryCount	INT   --  Count of PCCategories where the PCPid is contained.

	DECLARE @AllRemainingCount INT					
	 DECLARE @ShoppingCartItemCount			INT		--  Count of PCPID in the Cart.
	 DECLARE @SupplierOrderLineItemCount	INT		-- Count of PCPID that have been ordered.
	 DECLARE @DispensedDetailCount			INT

	
	SET @DependencyMessage = ''

BEGIN TRY
	
	--  1. Check the ProductInventory table.
	SELECT
		@ProductInventoryCount = 
			COUNT(PI.ProductInventoryID)	--AS ProductInventoryCountAllLocations	--  Count of PI for the given PL and PCP
	FROM ProductInventory AS PI				WITH (NOLOCK)
	INNER JOIN PracticeLocation AS PL		WITH (NOLOCK)
		ON PI.PracticeLocationID = PL.PracticeLocationID
		AND PI.PracticeCatalogProductID = @PracticeCatalogProductID
	WHERE 
		PI.PracticeCatalogProductID = @PracticeCatalogProductID
		AND PL.IsActive = 1
	GROUP BY
		  PI.PracticeCatalogProductID		--  PracticeCatalogProductID given
	
	SELECT @ProductInventoryCount = ISNULL(@ProductInventoryCount, 0)  
	
	IF (@ProductInventoryCount = 0) 
	BEGIN						-- Continue with test.  
	
			--  2. Check PCCategory for any dependencies on the PCPID.	
			SELECT @PracticeCatalogCategoryCount = COUNT(PracticeCatalogProductID)
			FROM PracticeCatalogCategory AS PCC		WITH (NOLOCK)
			WHERE PCC.PracticeCatalogProductID = @PracticeCatalogProductID
	
			SELECT @PracticeCatalogCategoryCount = ISNULL(@PracticeCatalogCategoryCount, 0)  	
	
			IF (@PracticeCatalogCategoryCount = 0 )
			BEGIN		
					
					--  3. Cart
					--    a. Check the Shopping Cart for the PCPID, if there then end
					SELECT @ShoppingCartItemCount = COUNT(@PracticeCatalogProductID)
					FROM dbo.ShoppingCartItem AS SCI		WITH (NOLOCK)
					WHERE PracticeCatalogProductID = @PracticeCatalogProductID
					--	AND IsActive = 1						
						SELECT @ShoppingCartItemCount = ISNULL(@ShoppingCartItemCount, 0)  	
											
					--  Ordered  
					--    b.  Check if the PCPID has ever been ordered.
					SELECT @SupplierOrderLineItemCount = COUNT(PracticeCatalogProductID) --AS  SupplierOrderLineItemCount
					FROM dbo.SupplierOrderLineItem AS SOLI  WITH (NOLOCK)
					WHERE PracticeCatalogProductID = @PracticeCatalogProductID
					--	AND IsActive = 1
						SELECT @SupplierOrderLineItemCount = ISNULL(@SupplierOrderLineItemCount, 0)  	

					--  CheckedIn  
					--    c.  To be Checked In or Check In manually, the product must be in one of the location's product inventory.
					--    therefore use the check on PI count to indirectly check the ProductInventory. 

					--  Dispensed   
					--    d.  Check the dispensed detail table for the PracticeCatalogProductID.
					--		might be able to check indirectly through the PI table, but this is sketchy.	
					SELECT @DispensedDetailCount = COUNT(PracticeCatalogProductID)		--  AS DispensedDetailCount
					FROM DispenseDetail AS	DD			WITH (NOLOCK) 
					WHERE PracticeCatalogProductID = @PracticeCatalogProductID
					--	AND IsActive = 1

						SELECT @DispensedDetailCount = ISNULL(@DispensedDetailCount, 0)  	
					
					SELECT @PracticeCatalogCategoryCount	AS PracticeCatalogCategoryCount
					SELECT @ShoppingCartItemCount			AS ShoppingCartItemCount	
					SELECT @SupplierOrderLineItemCount		AS SupplierOrderLineItemCount	-- Count of PCPID that have been ordered.
					SELECT @DispensedDetailCount			AS DispensedDetailCount			

					
					
					SELECT @AllRemainingCount = ( 
													   @ShoppingCartItemCount
													+  @SupplierOrderLineItemCount
													+  @DispensedDetailCount
												)
														
					SELECT @AllRemainingCount = ISNULL(@AllRemainingCount, 0)  	
			
					IF( @AllRemainingCount = 0 )
					BEGIN --  all other tests.
						PRINT 'Delete'
						--  Create audit table for deletes.
						--  DELECT HCPCs for this from pcp.
						
							SET @RemovalType = 1 	--  hard delete.
							PRINT 'Delete Hard' + CAST(@PracticeCatalogProductID AS VARCHAR(10))
						
							--  Set the ModifiedUserID prior to deleting, so the user is recorded in the audit table.
							UPDATE dbo.PracticeCatalogProduct 
							SET 
								  ModifiedUserID = @UserID
								, ModifiedDate = GETDATE()		        
							WHERE PracticeCatalogProductID = @PracticeCatalogProductID
						
							--  JB 20080313 Not tested should work for 3rdProducts.							
							--  Update the corresponding thirdpartyProductID if there is a related thirdparty product id.
							-- If the PCPID is to be deleted then set the corresponding ThirdPartyProductID to inactive.
							-- 
							-- If the PCPID is to be deleted then set the corresponding ThirdPartyProductID to inactive.
							DECLARE @ThirdPartyProductID INT


							SELECT @ThirdPartyProductID = ThirdPartyProductID
							FROM PracticeCatalogProduct
							WHERE PracticeCatalogProductID = @PracticeCatalogProductID
							AND PracticeCatalogProduct.IsThirdPartyProduct = 1

							IF (@ThirdPartyProductID IS NOT NULL)
							BEGIN
								UPDATE ThirdPartyProduct 
									SET IsActive = 0
									, ModifiedDate = GETDATE()
									, ModifiedUserID = -4512
								WHERE ThirdPartyProductID = @ThirdPartyProductID
								
								DELETE FROM ThirdPartyProduct
								WHERE ThirdPartyProductID = @ThirdPartyProductID
							END

							--  DELETE the PCP.
							DELETE FROM dbo.ProductHCPCS
							WHERE PracticeCatalogProductID = @PracticeCatalogProductID
							
							DELETE FROM dbo.PracticeCatalogProduct		            	        
						    WHERE PracticeCatalogProductID = @PracticeCatalogProductID
							--  End JB 20080313
							
					END
					ELSE						
					  BEGIN 
						-- SET the DependencyMessage. for all other tests.
						   PRINT 'Other Test'  + CAST(@PracticeCatalogProductID AS VARCHAR(10))		--SELECT 1
						   SET @RemovalType = 0 	
						   SET @DependencyMessage = @DependencyMessage + ', Cart, Ordered, Dispensed' 
					  END
					--END
					
			END		
			ELSE  -- @PracticeCatalogCategoryCount is more than 0
			  BEGIN
				-- SET the DependencyMessage. for PC Catagory
				PRINT 'Catalog'  + CAST(@PracticeCatalogProductID AS VARCHAR(10))
				SET @RemovalType = 0
				SET @DependencyMessage = @DependencyMessage + ', Catalog' 
			  END
			END
				
	--END
	ELSE  -- @ProductInventoryCount is more than 0
	  BEGIN
		-- SET the DependencyMessage. for PICount
		SET @RemovalType = 0
		PRINT 'Inventory' + CAST(@PracticeCatalogProductID AS VARCHAR(10))
		SET @DependencyMessage = @DependencyMessage + ', Inventory' 
	  END
	--end


END TRY

BEGIN CATCH

  EXECUTE [dbo].[uspLogError]
  EXECUTE [dbo].[uspPrintError]
  
END CATCH	


  END
  
  
  