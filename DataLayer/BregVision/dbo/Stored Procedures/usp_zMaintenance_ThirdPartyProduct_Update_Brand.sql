﻿/*
	John Bongiorni
	2007.12.11
*/
CREATE PROC [dbo].[usp_zMaintenance_ThirdPartyProduct_Update_Brand]
AS
BEGIN

begin tran

SELECT * FROM ThirdPartyProduct 
where practicecatalogsupplierbrandID IN ( 224, 225 )
and Name Like 'Sully%'

UPDATE ThirdPartyProduct 
SET practicecatalogsupplierbrandID = 225
where practicecatalogsupplierbrandID IN ( 224 )
and Name Like 'Sully%'


 
UPDATE PRACTiceCatalogProduct
SET practicecatalogsupplierbrandID = 225 
where PracticeID = 16
and practicecatalogsupplierbrandID = 224
and ThirdPartyProductID IN
	(
		1536
		, 1537
		, 1538
		, 1539
		, 1556
		, 1557
		, 1558
		, 1559
		, 1560
		, 1561
	)

SELECT * 
FROM PracticeCatalogProduct
where PracticeID = 16
and practicecatalogsupplierbrandID in (224, 225)


SELECT * FROM ThirdPartyProduct 
where practicecatalogsupplierbrandID IN ( 224, 225 )
and Name Like 'Sully%'



select *
from practiceCatalogSupplierBrand
where practicecatalogsupplierbrandID = 224

rollback tran

END