﻿/* ------------------------------------------------------------
   PROCEDURE:    [dbo].[usp_PracticeCatalogSupplierBrand_Select_All_Brands_By_Vendor]
   
   Description:  Selects all records from the table dbo.PracticeCatalogSupplierBrand.
				Get the BrandNames and BrandDisplayNames for a given vendor (ThirdPartySupplier that is a vendor)
   
   AUTHOR:       John Bongiorni 10/4/2007 12:25:29 PM
   
   Modifications:  
   ------------------------------------------------------------ */  -------------------SELECT ALL----------------

CREATE PROCEDURE [dbo].[usp_PracticeCatalogSupplierBrand_Select_All_Brands_By_Vendor]
		@ThirdPartySupplierID INT

AS
BEGIN
	DECLARE @Err INT
	
	SELECT
		  PCSB.PracticeCatalogSupplierBrandID
		, PCSB.PracticeID
--		, PCSB.MasterCatalogSupplierID
		, PCSB.ThirdPartySupplierID
		, PCSB.SupplierName
		, PCSB.SupplierShortName
		, ISNULL(PCSB.BrandName, '')			AS BrandName
		, ISNULL(PCSB.BrandShortName, '')	AS BrandShortName
--		, PCSB.IsThirdPartySupplier
		, PCSB.Sequence
	
	FROM dbo.PracticeCatalogSupplierBrand AS PCSB
	WHERE 
		PCSB.ThirdPartySupplierID = @ThirdPartySupplierID
		AND PCSB.MasterCatalogSupplierID IS NULL
		AND PCSB.IsThirdPartySupplier = 1
		AND PCSB.IsActive = 1
	ORDER BY PCSB.Sequence
	
	SET @Err = @@ERROR

	RETURN @Err
END
