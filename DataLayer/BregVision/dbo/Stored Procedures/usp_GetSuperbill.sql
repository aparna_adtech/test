﻿
-- =============================================
-- Author:		<Author: Mike Sneen>
-- Create date: <Create Date: 02/28/09>
-- Description:	<Description: Used to get all a superbill>
-- modification:
--  [dbo].[usp_GetSuperbill]  

-- =============================================
Create PROCEDURE [dbo].[usp_GetSuperbill]  --3 --10  --  _With_Brands
	@PracticeLocationID int
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
--        SET NOCOUNT ON ;
     
     SELECT 
			sbp.SuperBillProductID,
			sbp.PracticeCatalogProductID,
			ProductName = Coalesce(SBP.Name,  MCP1.Name, TPT1.Name, 'Name Not Found'),--sbp.[Name],
			sbp.HCPCs,
			sbp.FlowsheetOnly,
			sbp.Sequence,
			sbp.IsActive,
			sbc.SuperBillCategoryID as CategoryID,
			sbc.[Name] as CategoryName ,
			sbc.ColumnNumber
		from SuperBillProduct sbp
			join SuperBillCategory sbc 
				on sbp.SuperBillCategoryID = sbc.SuperBillCategoryID 
			join PracticeCatalogProduct PCP1 
				on SBP.PracticeCatalogProductID = PCP1.PracticeCatalogProductID
			left outer join MasterCatalogProduct MCP1 
				on PCP1.MasterCatalogProductID = MCP1.MasterCatalogProductID AND PCP1.IsThirdPartyProduct = 0
			left outer join ThirdPartyProduct TPT1 
				on PCP1.ThirdPartyProductID = TPT1.ThirdPartyProductID AND PCP1.IsThirdPartyProduct = 1
		where 
			sbp.PracticeLocationID = @PracticeLocationID
			AND sbp.IsAddOn = 0
			AND sbp.SuperBillProductID_FK IS NULL
			AND sbp.IsActive = 1 
			AND sbc.IsActive = 1
		
		

    END

--Exec usp_GetSuperbill 3



