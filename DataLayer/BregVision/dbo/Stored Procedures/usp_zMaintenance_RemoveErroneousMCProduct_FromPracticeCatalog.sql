﻿CREATE PROC usp_zMaintenance_RemoveErroneousMCProduct_FromPracticeCatalog
AS
BEGIN

	--  Get PracticeCatalogSupplierBrandID given the PracticeID, if the supplier is third party, and the brandname.
	SELECT * FROM dbo.PracticeCatalogSupplierBrand AS pcsb   --  PracticeCatalogSupplierBrandID = 107
	WHERE practiceid = 6
	AND IsThirdPartySupplier = 0
	AND brandName = 'Hely Weber'

	--  Get the PracticeCatalogProducts for the bogus PracticeCatalogSupplierBrandID
	SELECT * FROM dbo.PracticeCatalogProduct 
	WHERE practiceCatalogSupplierBrandID = 107

	--  Get the bogus inventory for the bogus PCProduct for the bogus PracticeCatalogSupplierBrandID
	SELECT * FROM dbo.ProductInventory 
	WHERE PracticeCatalogProductID IN ( 

	--2103
	--2104
										SELECT PracticeCatalogProductID 
										FROM dbo.PracticeCatalogProduct 
										WHERE practiceCatalogSupplierBrandID = 107
									  )
	-- None are product inventory for this case.
	--  If in productInventory then go through all other tables.
		
--	--  Delete from PracticeCatalogProduct
--		DELETE FROM dbo.PracticeCatalogProduct 
--		WHERE practiceCatalogSupplierBrandID = 107
	
--	--  "Delete the PCSB", by setting the PCSB to isActive = 0 
--	UPDATE dbo.PracticeCatalogSupplierBrand --  PracticeCatalogSupplierBrandID = 107
--	SET IsActive = 0
--	WHERE practiceid = 6
--	AND IsThirdPartySupplier = 0
--	AND BrandName = 'Hely Weber'
--	AND PracticeCatalogSupplierBrandID = 107

END




