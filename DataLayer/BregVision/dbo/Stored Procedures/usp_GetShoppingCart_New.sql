﻿

-- =============================================
-- Author:		<Greg Ross>
-- Create date: <07/31/07>
-- Description:	<Used to get all data for the shopping cart>
--  
--  Modification:  20071011 JB  Added Third Party Supplier Information.
--    check this for Cost
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetShoppingCart_New] --  18
	@PracticeLocationID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		  SUB.Supplier
		, SUB.Category
		, SUB.Product
		, SUB.Code
		, SUB.Packaging
		, SUB.Side
		, SUB.Size
		, SUB.Gender
		, SUB.Quantity
		, SUB.Cost           -- check this
		, (SUB.Quantity * SUB.Cost) AS LineTotal
		, SUB.PracticeCatalogProductID
		, SUB.ShoppingCartItemID
		, SUB.IsThirdPartyProduct
		, SUB.Sequence

	FROM
			(
				SELECT  MCS.[SupplierName] AS Supplier,
						MCC.[Name] AS Category,
						MCP.[Name] AS Product,
						MCP.[Code] AS Code,
						MCP.[Packaging] AS Packaging,
						MCP.[LeftRightSide] AS Side,
						MCP.[Size] AS Size,
						MCP.[Gender] AS Gender,
						SCI.[Quantity] AS Quantity,
						SCI.[ActualWholesaleCost] AS Cost,
						PCP.[PracticeCatalogproductid] AS PracticeCatalogProductID,
						SCI.[ShoppingCartItemID] AS ShoppingCartItemID,
						PCP.IsThirdPartyProduct,
						ISNULL(PCSB.Sequence, 0) AS Sequence 

--SELECT * FROM shoppingcartitem
						
				FROM    
						[ShoppingCart] SC
				        
						INNER JOIN shoppingcartitem SCI 
							ON SC.[ShoppingCartID] = SCI.[ShoppingCartID]
						
						INNER JOIN [PracticeCatalogProduct] PCP 
							ON SCI.[PracticeCatalogProductID] = PCP.[PracticeCatalogProductID]
						
						INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
							ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID							

						INNER JOIN [MasterCatalogProduct] MCP 
							ON PCP.[MasterCatalogProductID] = MCP.[MasterCatalogProductID]

						INNER JOIN [MasterCatalogSubCategory] MCSC 
							ON MCP.[MasterCatalogSubCategoryID] = MCSC.[MasterCatalogSubCategoryID]
						
						INNER JOIN [MasterCatalogCategory] MCC 
							ON MCSC.[MasterCatalogCategoryID] = MCC.[MasterCatalogCategoryID]
						
						INNER JOIN [MasterCatalogSupplier] MCS 
							ON MCC.[MasterCatalogSupplierID] = MCS.[MasterCatalogSupplierID]

				WHERE   
						SC.[ShoppingCartStatusID] = 0
						AND SCI.[ShoppingCartItemStatusID] = 0
						AND SC.[IsActive] = 1
						AND SCI.[IsActive] = 1
						AND pcp.[IsActive] = 1
						AND mcsc.[IsActive] = 1
						AND mcc.[IsActive] = 1
						AND mcs.[IsActive] = 1
						AND PCSB.IsActive = 1
						AND SC.[PracticeLocationID] = @practicelocationID
						AND PCSB.IsThirdPartySupplier  = 0

			UNION
			
				SELECT  
						PCSB.[SupplierName]AS Supplier,
						'NA' AS Category,
						TPP.[Name] AS Product,
						TPP.[Code]AS Code,
						TPP.[Packaging] AS Packaging,
						TPP.[LeftRightSide] AS Side,
						TPP.[Size] AS Size,
						TPP.[Gender] AS Gender,
						SCI.[Quantity] AS Quantity,
						SCI.[ActualWholesaleCost] AS Cost,
						PCP.[PracticeCatalogproductid] AS PracticeCatalogProductID,
						SCI.[ShoppingCartItemID] AS ShoppingCartItemID,
						PCP.IsThirdPartyProduct,
						ISNULL(PCSB.Sequence, 0) AS Sequence
						
				FROM    
						[ShoppingCart] SC
				        
						INNER JOIN shoppingcartitem SCI 
							ON SC.[ShoppingCartID] = SCI.[ShoppingCartID]
							
						INNER JOIN [PracticeCatalogProduct] PCP 
							ON SCI.[PracticeCatalogProductID] = PCP.[PracticeCatalogProductID]
							
						INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
							ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID							
							
						INNER JOIN [ThirdPartyProduct] TPP 
							ON PCP.[ThirdPartyProductID] = TPP.[ThirdPartyProductID]

				WHERE   
						SC.[ShoppingCartStatusID] = 0
						AND SCI.[ShoppingCartItemStatusID] = 0
						AND SC.[IsActive] = 1
						AND SCI.[IsActive] = 1
						
						AND pcp.IsActive = 1
						AND PCSB.IsActive = 1
						AND TPP.IsActive = 1
						AND PCSB.IsThirdPartySupplier  = 1
						
						AND SC.[PracticeLocationID] = @practicelocationID
	
				) AS SUB
				
			ORDER BY 
				SUB.IsThirdPartyProduct
				, SUB.Sequence
				, SUB.Supplier


END



