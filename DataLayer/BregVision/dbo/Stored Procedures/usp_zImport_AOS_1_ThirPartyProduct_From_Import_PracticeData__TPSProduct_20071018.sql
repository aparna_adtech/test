﻿
CREATE proc [dbo].[usp_zImport_AOS_1_ThirPartyProduct_From_Import_PracticeData__TPSProduct_20071018]
as
begin
--select * from ThirdPartyProduct  should be 712-715

INSERT INTO ThirdPartyProduct
	( PracticeCatalogSupplierBrandID
	, Code
	, Name
	, ShortName

	, Packaging
	, LeftRightSide
	, Size
	, Color
	, Gender
	, IsDiscontinued
	, WholesaleListCost
	, Description
	, CreatedUserID
	, CreatedDate
	, IsActive
	)

Select 
	  PracticeCatalogSupplierBrandID
	, ProductCode
	, ProductName
	, ProductShortName

	, Packaging
	, LeftRightSide
	, Size
	, Color
	, Gender
	, 0 AS IsDiscontinued
	, WholesaleListCost
	, Description
	, 0
	, GetDate()
	, 1
from Import_PracticeData.dbo.['AOS 3rd Party Products 20071018$'Print_Area]

end