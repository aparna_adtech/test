﻿

--SELECT * FROM dbo.SupplierOrder AS so INNER JOIN dbo.BregVisionOrder  AS bvo --po 100139  --soid 222
--	ON so.BregVisionOrderID = bvo.BregVisionOrderID 
--	WHERE bvo.PracticeLocationID = 3




-- =============================================
-- Author:		Greg Ross
-- Create date: 08/20/2007
-- Description:	Used in the Oustanding Purchase Order section of the Check-in UI. 
-- This is a query for the Detail table

--  Modifications:  2007.11.13 JB  Comment out correct code for quantities.
--				Add BOGUS code for quantities for compatibility for current version.
--
--				JB 2007.11.15 fix query to put all quantity checked-in for the supplier order line item
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetOutstandingPOsforCheckIn_Detail2Level] --162, 15  -- Prod 222, 3  -- BimsDev 169, 3   -- BimsDev 162, 15

		
	@SupplierOrderID int,
	--@BregVisionOrderID int,
	@PracticeLocationID int

AS
BEGIN

SET NOCOUNT ON;
select  SUB.SupplierOrderID, 
		SUB.ActualWholesaleCost,
		SUB.QuantityOrdered,
		
		--  Correct for next release		
		ISNULL(SUB.QuantityCheckedIn, 0) AS QuantityCheckedIn,   --  Correct for next release
		ISNULL(SUB.QuantityToCheckIn, 0) AS QuantityToCheckIn,  --   Correct for next release

		--  Wrong!!! Correct for next release		
--		ISNULL(SUB.QuantityCheckedIn, 0) AS QuantityAlreadyCheckedIn,   --  Wrong Correct for next release
--		ISNULL(SUB.QuantityToCheckIn, 0) AS QuantityCheckedIn,  --  Wrong Correct for next release
		
		
--		ISNULL(SUB.IsEntireLineItemCheckedIn, 0) AS IsEntireLineItemCheckedIn,

		SUB.LineTotal,
		SUB.Code,
		SUB.ShortName,
		SUB.Packaging,
		SUB.LeftRightSide,
		SUB.Size,
		SUB.Color,
		SUB.Gender,
		SUB.PracticeCatalogProductID,
		SUB.SupplierOrderLineItemID,
		SUB.SupplierOrderLineItemStatusID,
		SUB.ProductInventoryID,
		SUB.Status,
		SUB.IsThirdPartyProduct

from (

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	select SOLI.SupplierOrderID, 
		SOLI.ActualWholesaleCost,
		SOLI.QuantityOrdered,
		
		ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityCheckedIn,
		
		CASE
		WHEN ( SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) ) < 0 THEN 0
		ELSE  SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) 
		END									AS QuantityToCheckIn,

--		ISNULL(PIOCI.IsEntireLineItemCheckedIn, 0) AS IsEntireLineItemCheckedIn,
		
		SOLI.LineTotal,
		MCP.Code,
		MCP.ShortName,
		MCP.Packaging,
		MCP.LeftRightSide,
		MCP.Size,
		MCP.Color,
		MCP.Gender,
		PCP.PracticeCatalogProductID,
		SOLI.SupplierOrderLineItemID,
		SOLI.SupplierOrderLineItemStatusID,
		PI.ProductInventoryID,
		SOLIS.Status,
		0 AS IsThirdPartyProduct	
	
from SupplierOrderLineItem SOLI
--inner join SupplierOrder SO on SOLI.SupplierOrderID = SO.SupplierOrderID
--inner join BregVisionOrder BVO on SO.BregVisionOrderID= BVO.BregVisionOrderID
inner join PracticeCatalogPRoduct PCP 
	on SOLI.PracticeCatalogProductID = 	PCP.PracticeCatalogPRoductID
inner join MasterCatalogProduct MCP
	on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
inner join ProductInventory PI
	on SOLI.PracticeCatalogProductID = PI.PracticeCatalogProductID
inner join SupplierOrderLineItemStatus SOLIS 
	on SOLI.SupplierOrderLineItemStatusID = SOLIS.SupplierOrderLineItemStatusID

-- Dummy Programming below.
--LEFT JOIN 
--	dbo.ProductInventory_OrderCheckIn AS PIOCI
--		ON SOLI.SupplierOrderLineItemID = PIOCI.SupplierOrderLineItemID
--			AND PI.ProductInventoryID = PIOCI.ProductInventoryID
--			AND PIOCI.IsActive = 1

	-- JB 2007.11.15 fix query to put all quantity checked-in for the supplier order line item				
	LEFT JOIN (SELECT SupplierOrderLineItemID, SUM(Quantity) AS QuantityCheckedIn
							FROM dbo.ProductInventory_OrderCheckIn 
							WHERE IsActive = 1
							GROUP BY SupplierOrderLineItemID)AS SubPIOCI
								ON SOLI.SupplierOrderLineItemID = SubPIOCI.SupplierOrderLineItemID

where 
--SOLI.SupplierOrderLineItemStatusID in (1,3) 
PI.PracticeLocationID = @PracticeLocationID

and SOLI.SupplierOrderID = @SupplierOrderID

			AND PCP.IsThirdPartyProduct = 0		-- MasterCatalogProduct
			--and PCP.isactive = 1
			--and MCP.isactive = 1
			and SOLI.IsActive = 1

union


	select SOLI.SupplierOrderID, 
		SOLI.ActualWholesaleCost,
		SOLI.QuantityOrdered,
				

		ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityCheckedIn,
		
		CASE
		WHEN ( SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) ) < 0 THEN 0
		ELSE  SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) 
		END									AS QuantityToCheckIn,				

--		2007.11.15  Bad Code Below		
--		ISNULL(PIOCI.Quantity, 0) AS QuantityCheckedIn,		
--		SOLI.QuantityOrdered - ISNULL(PIOCI.Quantity, 0) AS QuantityToCheckIn,
--		ISNULL(PIOCI.IsEntireLineItemCheckedIn, 0) AS IsEntireLineItemCheckedIn,		
		
		SOLI.LineTotal,
		TPP.Code,
		TPP.ShortName,
		TPP.Packaging,
		TPP.LeftRightSide,
		TPP.Size,
		TPP.Color,
		TPP.Gender,
		PCP.PracticeCatalogProductID,
		SOLI.SupplierOrderLineItemID,
		SOLI.SupplierOrderLineItemStatusID,
		PI.ProductInventoryID,
		SOLIS.Status,
		1 AS IsThirdPartyProduct
	
from SupplierOrderLineItem SOLI
--inner join SupplierOrder SO on SOLI.SupplierOrderID = SO.SupplierOrderID
--inner join BregVisionOrder BVO on SO.BregVisionOrderID= BVO.BregVisionOrderID
inner join PracticeCatalogPRoduct PCP 
	on SOLI.PracticeCatalogProductID = 	PCP.PracticeCatalogPRoductID
inner join ThirdPartyProduct AS TPP  WITH(NOLOCK) 
	on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID 
--inner join MasterCatalogProduct MCP
--	on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
inner join ProductInventory PI
	on SOLI.PracticeCatalogProductID = PI.PracticeCatalogProductID
inner join SupplierOrderLineItemStatus SOLIS 
	on SOLI.SupplierOrderLineItemStatusID = SOLIS.SupplierOrderLineItemStatusID

-- JB 2007.11.15 erroneous code below.
--LEFT JOIN 
--	dbo.ProductInventory_OrderCheckIn AS PIOCI
--		ON SOLI.SupplierOrderLineItemID = PIOCI.SupplierOrderLineItemID
--			AND PI.ProductInventoryID = PIOCI.ProductInventoryID
--			AND PIOCI.IsActive = 1

	-- JB 2007.11.15 fix query to put all quantity checked-in for the supplier order line item				
	LEFT JOIN (SELECT SupplierOrderLineItemID, SUM(Quantity) AS QuantityCheckedIn
							FROM dbo.ProductInventory_OrderCheckIn 
							WHERE IsActive = 1
							GROUP BY SupplierOrderLineItemID)AS SubPIOCI
								ON SOLI.SupplierOrderLineItemID = SubPIOCI.SupplierOrderLineItemID


where 
--SOLI.SupplierOrderLineItemStatusID in (1,3) 
PI.PracticeLocationID = @PracticeLocationID

and SOLI.SupplierOrderID = @SupplierOrderID

			AND PCP.IsThirdPartyProduct = 1		-- ThirdPartyProduct
			--and PCP.isactive = 1
			--and TPP.isactive = 1
			and SOLI.IsActive = 1

) AS Sub
ORDER BY
	Sub.IsThirdPartyProduct,
	SUB.ShortName,
	SUB.Code,
	SUB.LeftRightSide
	
--and BVO.BregVisionOrderID = @BregVisionOrderID
END







