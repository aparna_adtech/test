﻿
create proc usp_zImport_ThirPartyProduct_From_Import_PracticeData_TOC
as
begin

INSERT INTO ThirdPartyProduct
	( PracticeCatalogSupplierBrandID
	, Code
	, Name
	, ShortName

	, Packaging
	, LeftRightSide
	, Size
	, Color
	, Gender
	, IsDiscontinued
	, WholesaleListCost
	, Description
	, CreatedUserID
	, CreatedDate
	, IsActive
	)

Select 
	PracticeCatalogSupplierBrand as PracticeCatalogSupplierBrandID
	, ProductCode as Code
	, Name
	, ShortName

	, Packaging
	, LeftRightSide
	, Size
	, Color
	, Gender
	, 0 AS IsDiscontinued
	, WholesaleListCost
	, Description
	, CreatedUserID
	, GetDate()
	, IsActive
from Import_PracticeData.dbo.TOC_TPSProduct_Import$

end