﻿/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Physician_Select_One_ByParams
   
   Description:  Selects a physician 
		(actually Selects a record from table dbo.Contact)
   				   and puts values into parameters
   
   AUTHOR:       John Bongiorni 7/20/2007 7:31:00 PM
   
   Modifications:  
   ------------------------------------------------------------ */


CREATE PROCEDURE [dbo].[usp_Physician_Select_One_By_Params]
(
	  @PhysicianID					 INT
    , @ContactID                     INT
    , @Salutation                    VARCHAR(10)    OUTPUT
    , @FirstName                     VARCHAR(50)    OUTPUT
    , @MiddleName                    VARCHAR(50)    OUTPUT
    , @LastName                      VARCHAR(50)    OUTPUT
    , @Suffix                        VARCHAR(10)    OUTPUT
)
AS
BEGIN
	DECLARE @Err INT

	SELECT
		  @ContactID = C.ContactID
        , @Salutation = C.Salutation
        , @FirstName = C.FirstName
        , @MiddleName = C.MiddleName
        , @LastName = C.LastName
        , @Suffix = C.Suffix

	FROM dbo.Contact AS C 

	INNER JOIN dbo.Physician AS Ph 
		ON C.ContactID = Ph.ContactID
 
	WHERE 
		Ph.PhysicianID = @PhysicianID
		AND C.ContactID = @ContactID
		AND Ph.IsActive = 1
		AND C.IsActive = 1

	SET @Err = @@ERROR

	RETURN @Err
End


