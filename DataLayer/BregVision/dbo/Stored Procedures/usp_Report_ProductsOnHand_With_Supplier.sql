﻿/*

	EXEC [usp_Report_ProductsOnHand] 6, '13, 15, 16, 17, 18, 19, 20, 22, 23, 24', '34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46'
	EXEC  [usp_Report_ProductsOnHand] 62, '187, 186, 188 ,190 ,189', '857'

*/


CREATE PROC [dbo].[usp_Report_ProductsOnHand_With_Supplier]
		  @PracticeID	INT
		, @PracticeLocationID VARCHAR(2000)
		, @SupplierBrandID  VARCHAR(2000)   --  ID String.
		, @InternalPracticeLocationID VARCHAR(2000)
		, @InternalSupplierBrandID VARCHAR(2000)

AS
BEGIN


-- Parse the Practice Location
DECLARE @locations TABLE ([PracticeLocationID] INT);
DECLARE @PracticeLocationIDString VARCHAR(2000);

-- If the Internal parameter is "ALL", we select all of the Practice Locations
-- for the passed PracticeID.
-- Otherwise, split as we were doing before.
IF(@InternalPracticeLocationID = 'ALL')
BEGIN
	DECLARE @tempLocations TABLE (
		PracticeLocationID INT
		,PracticeLocationName VARCHAR(50)
		,IsPrimaryLocation BIT
		);

	INSERT @tempLocations
	EXEC dbo.usp_PracticeLocation_Select_All_ID_Name_Given_PracticeID @PracticeID = @PracticeID;

	INSERT @locations ([PracticeLocationID])
	SELECT PracticeLocationID
	FROM @tempLocations;

	SELECT @PracticeLocationIDString = COALESCE(@PracticeLocationIDString + CAST(PracticeLocationID AS NVARCHAR(10)) + ',', '')
			FROM @locations;

	IF(LEN(@PracticeLocationIDString) > 0)
		SET @PracticeLocationIDString = LEFT(@PracticeLocationIDString, LEN(@PracticeLocationIDString) - 1);
END;
ELSE
BEGIN
	INSERT @locations ([PracticeLocationID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable(@PracticeLocationID, ',');

	SET @PracticeLocationIDString = @PracticeLocationID;
END;

-- Parse the Supplier Brand
DECLARE @supplierBrands TABLE([SupplierBrandID] INT);

-- If the Internal parameter is "ALL", we select all of the Supplier Brands
-- for the passed PracticeID.
-- Otherwise, split as we were doing before.
IF(@InternalSupplierBrandID = 'ALL')
BEGIN
	DECLARE @tempSupplierBrands TABLE (PracticeCatelogSupplierBrandID INT, SupplierBrand VARCHAR(1000), SupplierName VARCHAR(1000), BrandName VARCHAR(1000), IsThirdPartySupplier BIT, DerivedSequence INT);

	INSERT @tempSupplierBrands
	EXEC dbo.usp_Report_GetPracticeCatalogSupplierBrands_Formatted @PracticeID = @PracticeID;

	INSERT @supplierBrands ([SupplierBrandID])
	SELECT PracticeCatelogSupplierBrandID
	FROM @tempSupplierBrands;
END;
ELSE
BEGIN
	INSERT @supplierBrands ([SupplierBrandID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable(@SupplierBrandID, ',');
END;

SELECT
		  SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.PracticeCatalogSupplierBrandID
		, SUB.SupplierShortName	AS Supplier
		, SUB.BrandShortName AS Brand
		, SUB.Sequence
		, SUB.ProductName			AS Product
		, SUB.Side + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender
		, SUB.Code


		, SUM(SUB.QuantityOnHandPerSystem)					AS QuantityOnHandPerSystem
		, SUB.ConsignmentQuantity							AS ConsignmentQuantity
		, SUB.ParLevel										AS ParLevel
		, SUB.ReorderLevel									AS ReorderLevel
		, SUB.CriticalLevel									AS CriticalLevel
		, CAST(SUB.WholesaleCost AS DECIMAL(15,2) )			AS ActualWholesaleCost
		, SUB.SuggestedReorderLevel
		, CAST( SUM(SUB.LineTotal)  AS DECIMAL(15,2) )		AS TotalCost
		, dbo.ConcatHCPCS(Sub.PracticeCatalogProductID) +
		 CASE WHEN dbo.ConcatHCPCSOTS(Sub.PracticeCatalogProductID) <> '' THEN
			'/ ' + dbo.ConcatHCPCSOTS(Sub.PracticeCatalogProductID)
			ELSE ''
			END

			AS HCPCSString
		, (SUB.ParLevel - SUM(SUB.QuantityOnHandPerSystem))	AS SuggestedOrderQuantity
		, SUB.IsConsignment

FROM
	(SELECT
		  P.PracticeName
		, PL.NAME													AS PracticeLocation

		, PCSB.IsThirdPartySupplier

		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierShortName
		, PCSB.BrandShortName

		, ISNULL(PCSB.Sequence, 999)								AS Sequence
		, PCP.PracticeCatalogProductID

		, MCP.[Name] AS ProductName
		, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) )		AS Side
		, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )				AS Size
		, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
		, ISNULL(MCP.Code, '')										AS Code

		, (PCP.WholesaleCost / PCP.StockingUnits) as WholesaleCost
		, PI.QuantityOnHandPerSystem
		, PI.ConsignmentQuantity
		, PI.ParLevel
		, PI.ReorderLevel
		, PI.CriticalLevel
		, Levels.SuggestedReorderLevel
		, (Convert(money,PCP.WholesaleCost) / PCP.StockingUnits) * PI.QuantityOnHandPerSystem as LineTotal
		, PI.IsConsignment


	From dbo.ProductInventory as PI with (NOLOCK)

	INNER JOIN dbo.PracticeCatalogProduct AS PCP	WITH (NOLOCK)
		on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID

	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON PI.PracticeLocationID = PL.PracticeLocationID

	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID

	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

	INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
		ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

	Left Outer JOIN dbo.udf_GetSuggestedReorder_Given_PracticeLocationID_String( @PracticeLocationIDString )	AS Levels
				ON PCP.PracticeCatalogProductID = Levels.PracticeCatalogProductID

	WHERE
		P.PracticeID = @PracticeID
    --AND P.IsActive = 1
		--AND PL.IsActive = 1
		--AND PCP.IsActive = 1
		--AND PCSB.IsActive = 1
		--AND MCP.IsActive = 1
		and PI.IsSetToActive=1
		and PI.IsActive=1

		AND PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			 FROM @locations)

		AND PCSB.PracticeCatalogSupplierBrandID IN
			(SELECT SupplierBrandID
			 FROM @supplierBrands )


	UNION

		SELECT
		  P.PracticeName
		, PL.NAME													AS PracticeLocation

		, PCSB.IsThirdPartySupplier

		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierShortName
		, PCSB.BrandShortName

		, ISNULL(PCSB.Sequence, 999) + 1000							AS Sequence
		, PCP.PracticeCatalogProductID

		, TPP.[Name]												AS ProductName
		, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) )		AS Side
		, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )				AS Size
		, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
		, ISNULL(TPP.Code, '')										AS Code

		, (PCP.WholesaleCost / PCP.StockingUnits) as WholesaleCost
		, PI.QuantityOnHandPerSystem
		, PI.ConsignmentQuantity
		, PI.ParLevel
		, PI.ReorderLevel
		, PI.CriticalLevel
		, Levels.SuggestedReorderLevel
		, (Convert(money,PCP.WholesaleCost) / PCP.StockingUnits) * PI.QuantityOnHandPerSystem			as LineTotal
		, PI.IsConsignment


	From dbo.ProductInventory as PI with (NOLOCK)

	INNER JOIN dbo.PracticeCatalogProduct AS PCP	WITH (NOLOCK)
		on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID

	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON PI.PracticeLocationID = PL.PracticeLocationID

	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID

	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

	INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
		ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

	Left Outer JOIN dbo.udf_GetSuggestedReorder_Given_PracticeLocationID_String( @PracticeLocationIDString )	AS Levels
		ON PCP.PracticeCatalogProductID = Levels.PracticeCatalogProductID

	WHERE
		P.PracticeID = @PracticeID
    --AND P.IsActive = 1
		--AND PL.IsActive = 1
		--AND PCP.IsActive = 1
		--AND PCSB.IsActive = 1
		--AND TPP.IsActive = 1
		and PI.IsSetToActive=1
		and PI.IsActive=1

		AND PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			 FROM @locations)

		AND PCSB.PracticeCatalogSupplierBrandID IN
			(SELECT SupplierBrandID
			 FROM @supplierBrands )
	)
		AS SUB

	GROUP BY
		  SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.PracticeCatalogSupplierBrandID
		, SUB.IsThirdPartySupplier
		, SUB.Sequence
		, SUB.PracticeCatalogProductID
		, SUB.SupplierShortName
		, SUB.BrandShortName

		, SUB.ProductName
		, SUB.Side
		, SUB.Size
		, SUB.Gender
		, SUB.Code

		, SUB.WholesaleCost
		, SUB.ConsignmentQuantity
		, SUB.ParLevel
		, SUB.ReorderLevel
		, SUB.CriticalLevel
		, SUB.SuggestedReorderLevel
		, SUB.IsConsignment

	ORDER BY
		  SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.Sequence
		, SUB.SupplierShortName
		, SUB.BrandShortName
		, SUB.ProductName
		, SUB.Code
		, SUB.Side + ', ' + SUB.Size  + ', ' + SUB.Gender

END
