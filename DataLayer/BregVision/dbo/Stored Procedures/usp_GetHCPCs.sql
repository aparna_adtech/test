﻿
-- =============================================
-- Author:		Greg Ross
-- Create date: Oct 11,2007
-- Description:	Used to get all HCPCs in administration pages
-- =============================================
create PROCEDURE [dbo].[usp_GetHCPCs]
	@PracticeCatalogProductID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   select ProductHCPCSID,
		  HCPCS
		 from ProductHCPCs
		 where PracticeCatalogProductID = @PracticeCatalogProductID
END

