﻿CREATE PROC usp_zzz3rdIncomplete
AS
BEGIN

	-- 3rdPartySupplier_MoveProduct_To_FromMCSupplier.sql

	BEGIN TRAN

	--  Input variables.
	DECLARE @PracticeCatalogProductID		INT  --(of the given product)
	DECLARE @NewPracticeCatalogSupplierBrandID 	INT  --(of the given supplier brand to move to)

	--  Working variables
	DECLARE @NewThirdPartySupplierID INT			-- (of the given 3rd party supplier to move to)

	--SELECT * 
	--FROM PracticeCatalogProduct 
	--WHERE PracticeCatalogSupplierBrandID = 109  -- products to be updated.

	SET @PracticeCatalogProductID = 2148				-- PCProductID to be updated.
	SET @NewPracticeCatalogSupplierBrandID = 112	--  SupplierBrandID to be moved to.


	SELECT @NewThirdPartySupplierID = ThirdPartySupplierID
	FROM PracticeCatalogSupplierBrand
	WHERE PracticeCatalogSupplierBrandID = @NewPracticeCatalogSupplierBrandID

	-- Test here...  works
	SELECT @NewThirdPartySupplierID  AS variableNewThirdPartySupplierID 

	--  CHECK IF THE OLDSUPPLIER IS MC OR NOT!!!!!
	--  THE FOLLOWING IS FOR MCSupplier ONLY!!!!!


	--  Insert the product into the ThirdPartyProduct table with the MCP info given the PCPID.
	INSERT INTO ThirdPartyProduct
			(
				 PracticeCatalogSupplierBrandID,
				 
				 Code,
				 [Name],
				 ShortName,
				 Packaging,
				 LeftRightSide,
			
				 Size,
				 Color,
				 Gender,
				 WholesaleListCost,
				 Description,
			
				 IsDiscontinued,
				 DateDiscontinued,
			
				 OldMasterCatalogProductID,

				 CreatedUserID,
				 CreatedDate,
				 ModifiedUserID,
				 ModifiedDate,
				 IsActive 
			)

	SELECT  

				 @NewPracticeCatalogSupplierBrandID AS PracticeCatalogSupplierBrandID,  --  New PracticeCatalogSupplierBrandID is 112

				 Code,
				 [Name],
				 ShortName,
				 Packaging,
				 LeftRightSide,
			
				 ISNULL(Size, '')			AS Size,
				 ISNULL(Color, '')			AS Color,
				 ISNULL(Gender, '')			AS Gender,
				 0.00						AS WholesaleListCost,		--  Should not bring over costs.
				 ISNULL(Description, '')	AS Description,
			
				 0							AS IsDiscontinued,			--  Should set as not discontinued.
				 NULL						AS DateDiscontinued,							--  AS DateDiscountinued

				 MasterCatalogProductID		AS OldMasterCatalogProductID,
			
				 -13						AS CreatedUserID,		--  CreatedUserID
				 GETDATE()					AS CreatedDate,			--  CreatedDate
				 -13						AS ModifiedUserID,		--  ModifiedUserID
				 GETDATE()					AS ModifiedDate,		--  ModifiedDate
				 1							AS IsActive				--  IsActive

			FROM MasterCatalogProduct 
			WHERE MasterCatalogProductID IN --  Should be = here
						(
							--  Get the Old MCProductID for PracticeCatalogProductID
							Select MasterCatalogProductID 
							from PracticeCatalogPRoduct 
							where PracticeCatalogProductID = @PracticeCatalogProductID 
						)

	DECLARE @NewThirdPartyProductID INT

	-- iDENTITY SCOPE()
	SELECT @NewThirdPartyProductID = SCOPE_IDENTITY()
	SELECT @NewThirdPartyProductID AS variableNewThirdPartyProductID



--	UPDATE PracticeCatalogProduct
--	  SET  
--		  PracticeCatalogSupplierBrandID = @NewPracticeCatalogSupplierBrandID
--		, ThirdPartyProductID = SUB.ThirdPartyProductID
--		, IsThirdPartyProduct = 1
--		, MasterCatalogProductID = NULL
--		, WholesaleCost = 0
--		, CreatedUser = -13
--		, IsActive = 1
--	FROM
--	( 
--		SELECT
--		--	  PCSB.PracticeID
--			  @NewPracticeCatalogSupplierBrandID AS PracticeCatalogSupplierBrandID
--			, TPP.ThirdPartyProductID
--			, 1 AS IsThirdPartyProduct
--			, TPP.WholesaleListCost
--			, -13  --TPS.CreatedUserID
--			, TPP.IsActive
--		FROM dbo.ThirdPartyProduct AS tpp
--		WHERE PracticeCatalogSupplierBrandID = @NewPracticeCatalogSupplierBrandID
--		AND OldMasterCatalogProductID = 	
--					(
--						Select MasterCatalogProductID 
--						from PracticeCatalogPRoduct 
--						where PracticeCatalogProductID = @PracticeCatalogProductID 
--					)
--	) AS Sub		
--
	SELECT * 
	FROM dbo.PracticeCatalogProduct AS pcp	
	WHERE practiceCatalogProductID = @PracticeCatalogProductID
				
	ROLLBACK TRAN



END