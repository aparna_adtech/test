﻿
/* ------------------------------------------------------------
   PROCEDURE:    [dbo].[usp_PracticeCatalogSupplierBrand_Insert_Mfg]
   
   Description:  Inserts a Practice's (ThirdPartySupplier) Manufacturer 
				into table PracticeCatalogSupplierBrand

				This is a nested proc called from dbo.usp_ThirdPartySupplier_Insert
					when 3rd party supplier is a vendor.

   AUTHOR:       John Bongiorni 10/4/2007 3:26:30 PM
   
   Modifications:  SELECT SUBQUERY TO ISACTIVE = 1
				20071017 JB add sequence with case for null and to 999 for sequence
   ------------------------------------------------------------ */  ------------------ INSERT ------------------
 
	
CREATE PROC [dbo].[usp_PracticeCatalogSupplierBrand_Insert_Mfg]
			  @PracticeID				INT
			, @ThirdPartySupplierID		INT
			, @UserID					INT
			, @PracticeCatalogSupplierBrandID INT OUTPUT
AS
BEGIN


	INSERT
	INTO dbo.PracticeCatalogSupplierBrand
	(
          PracticeID
        , MasterCatalogSupplierID
        , ThirdPartySupplierID
        
        , SupplierName
        , SupplierShortName
        , BrandName
        , BrandShortName
        
        , IsThirdPartySupplier
        , Sequence
        , CreatedUserID
        , IsActive
	)


	SELECT 
		  @PracticeID							AS PracticeID
		, NULL									AS MasterCatalogProductID
		, TPS.ThirdPartySupplierID				AS ThirdPartySupplierID
		
		, TPS.SupplierName						AS SupplierName
		, TPS.SupplierShortName					AS SupplierShortName
		, TPS.SupplierName						AS BrandName
		, TPS.SupplierShortName					AS BrandShortName
		
		, 1										AS IsThirdPartySupplier
		, CASE TPS.Sequence WHEN NULL THEN 999 WHEN 0 THEN 999 ELSE TPS.Sequence END AS Sequence
		, 0 AS CreateUserID
		, 1 AS IsActive 
	FROM	
		dbo.ThirdPartySupplier AS TPS
	INNER JOIN Practice AS P
		ON P.PracticeID = TPS.PracticeID
	WHERE P.PracticeID = @PracticeID
		AND P.IsActive = 1
		AND TPS.IsVendor = 0
		AND TPS.IsActive = 1
		AND TPS.ThirdPartySupplierID = @ThirdPartySupplierID	
		AND TPS.ThirdPartySupplierID 
			NOT IN (SELECT ThirdPartySupplierID 
			FROM PracticeCatalogSupplierBrand
			WHERE MasterCatalogSupplierID IS NULL
				AND ThirdPartySupplierID IS NOT NULL
				AND PracticeCatalogSupplierBrand.IsActive = 1 ) -- Prevent duplicate from going in
					 -- If isactive is 0 then update, if 1 then update instead of insert


	SELECT @PracticeCatalogSupplierBrandID = SCOPE_IDENTITY()

END

--16
--15
--25
--25
--25
--28
--29