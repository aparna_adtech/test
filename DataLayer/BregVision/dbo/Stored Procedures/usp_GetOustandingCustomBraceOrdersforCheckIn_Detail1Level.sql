﻿-- =============================================
-- Author:		Greg Ross
-- Create date: 08/20/2007
-- Description:	Used in the Oustanding Purchase Order section of the Check-in UI. 
-- This is a query for the Detail table
-- Modification:  2007-10-03 JB Add Column  SO.SupplierOrderID
-- =============================================
Create PROCEDURE [dbo].[usp_GetOustandingCustomBraceOrdersforCheckIn_Detail1Level]
		
	@PracticeLocationID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select  SO.BregVisionOrderID,
			SO.SupplierOrderID,		--  JB Added this column 20071003
			PCSB.SupplierShortName, 
			PCSB.BrandShortName,
			SO.PurchaseOrder,	
			SOS.Status,
			SO.Total
			
	 from SupplierOrder SO
	inner join PracticeCatalogSupplierBrand PCSB 
		on SO.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
	inner join SupplierOrderStatus SOS 
		on SO.SupplierOrderStatusID = SOS.SupplierOrderStatusID
	inner join BregVisionOrder AS BVO
		on SO.BregVisionOrderID = BVO.BregVisionOrderID

	where 
		BVO.BregVisionOrderStatusID IN (1,3) 
		AND BVO.isactive = 1 
		AND BVO.PracticeLocationID = @PracticeLocationID 
		and SO.SupplierOrderStatusID in (1,3)
		and SO.isactive = 1
	order by SupplierShortName
END
