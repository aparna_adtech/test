﻿



/* ------------------------------------------------------------
   
   
   
   AUTHOR:       Greg Ross 10/31/07
   
   Modifications:  
   ------------------------------------------------------------ */   


CREATE PROCEDURE [dbo].[usp_SearchForItemsInInventory_ByProductCode_With_ThirdPartyProducts]

	@PracticeLocationID int
	, @SearchText  varchar(50)
	, @SearchCriteria varchar(50) = 'Code'


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--   NOCOUNT ON


			SELECT 
				  SUB.ProductInventoryID	
				, SUB.PracticeCatalogProductID	
				, SUB.BrandName
				, SUB.SupplierName
				, SUB.SupplierID
--				, SUB.Category
				, SUB.Product as ProductName
				, SUB.Code

				, ISNULL(SUB.Packaging, '') AS Packaging
				, ISNULL(SUB.LeftRightSide, '') AS Side
				, ISNULL(SUB.Size, '') AS Size
				, ISNULL(SUB.Gender, '') AS Gender
				
				, SUB.QOH
				, SUB.ParLevel
				, SUB.ReorderLevel
				, SUB.CriticalLevel
				, SUB.WholesaleCost
				, SUB.IsActive
				, SUB.IsThirdPartyProduct
				, SUB.OldMasterCatalogProductID
				, SUB.OldThirdPartyProductID
				, SUB.NewMasterCatalogProductID
				, SUB.NewThirdPartyProductID
				, SUB.NewPracticeCatalogProductID
				, SUB.Name AS NewProductName
				, SUB.NewProductCode
				, SUB.NewPracticeID
			INTO #WorkingTable
			FROM
			(
					SELECT	-- 249 active and inactive mastercatalogproducts in inventory WORKS!!!
 
							PCP.PracticeCatalogProductID,

                            PCSB.MasterCatalogSupplierID AS SupplierID, 
                            PCSB.BrandShortName AS BrandName,   --  This column should be unneccessary
							PCSB.SupplierShortName AS SupplierName,	
								--  Only here to preserve the existing application code.
						    MCP.[Name]			AS Product,
                            
                            MCP.[Code]			AS Code,
                            MCP.[Packaging]		AS Packaging,
                            MCP.[LeftRightSide] AS LeftRightSide,
                            MCP.[Size]			AS Size,
                            MCP.[Gender]		AS Gender,
                            MCP.UPCCode			AS UPCCode,
							
							PI.ProductInventoryID,
							PI.QuantityOnHandPerSystem AS QOH,
							PI.ParLevel,
							PI.ReorderLevel,
							PI.CriticalLevel,
						
							PCP.WholesaleCost,
							PI.Isactive,
							0 AS IsThirdPartyProduct,

							PR.OldMasterCatalogProductID,
							PR.OldThirdPartyProductID,
							PR.NewMasterCatalogProductID,
							PR.NewThirdPartyProductID,
							MAX(CASE WHEN (NPI.PracticeLocationID IS NOT NULL) THEN NPCP.PracticeCatalogProductID ELSE 0 END) AS NewPracticeCatalogProductID,
							NMCP.Name,
							NMCP.Code AS NewProductCode,
							PR.PracticeID AS NewPracticeID
                     
                    FROM dbo.PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
                    INNER JOIN [PracticeCatalogProduct] AS PCP WITH(NOLOCK) ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
                    INNER JOIN [ProductInventory] AS PI WITH(NOLOCK) ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]             
                    INNER JOIN [MasterCatalogProduct] AS MCP WITH(NOLOCK) ON PCP.[MasterCatalogProductID] = MCP.[MasterCatalogProductID]
					LEFT OUTER JOIN ProductReplacement AS PR WITH(NOLOCK) ON PR.IsActive = 1 AND
						(PR.OldMasterCatalogProductID = PCP.MasterCatalogProductID) AND
						(PR.PracticeID IS NULL OR PR.PracticeID = PCP.PracticeID)
					LEFT JOIN MasterCatalogProduct NMCP ON NMCP.MasterCatalogProductID = PR.NewMasterCatalogProductID
					LEFT JOIN PracticeCatalogProduct NPCP ON NPCP.MasterCatalogProductID = PR.NewMasterCatalogProductID
					LEFT JOIN ProductInventory NPI ON NPI.PracticeCatalogProductID = NPCP.PracticeCatalogProductID AND NPI.PracticeLocationID = @PracticeLocationID
                                                 
                     WHERE  PI.[PracticeLocationID] = @PracticeLocationID
							AND PCP.IsThirdPartyProduct = 0  -- MasterCatalogProductsOnly
							AND MCP.IsActive = 1
							AND MCP.IsDiscontinued = 0
							AND PCP.IsActive = 1
							AND PI.IsActive = 1
							AND (
									(@SearchCriteria = 'Code' and MCP.Code LIKE @SearchText)
									OR
									(
										@SearchCriteria = 'UPCCode' --AND MCP.UPCCode like @SearchText)
										AND
										(
											 (@SearchText IS NULL)
											 OR EXISTS
											 (  
												Select 
													1 
												 from 
													UPCCode upc 
												 where 
													(
														upc.MasterCatalogProductID = MCP.MasterCatalogProductID 
														and upc.Code like @SearchText 
													) 
											  )
										 )		
									)
								)

					GROUP BY
							PCP.PracticeCatalogProductID,
                            PCSB.MasterCatalogSupplierID, 
                            PCSB.BrandShortName,
							PCSB.SupplierShortName,	
						    MCP.[Name],
                            MCP.[Code],
                            MCP.[Packaging],
                            MCP.[LeftRightSide],
                            MCP.[Size],
                            MCP.[Gender],
							MCP.UPCCode,
							PI.ProductInventoryID,
							PI.QuantityOnHandPerSystem,
							PI.ParLevel,
							PI.ReorderLevel,
							PI.CriticalLevel,
							PCP.WholesaleCost,
							PI.Isactive,
							PR.OldMasterCatalogProductID,
							PR.OldThirdPartyProductID,
							PR.NewMasterCatalogProductID,
							PR.NewThirdPartyProductID,
							NMCP.Name,
							NMCP.Code,
							PR.PracticeID

				UNION

					--  Third Party Product Info
					SELECT		-- 249 active and inactive mastercatalogproducts in inventory WORKS!!!
 
							PCP.PracticeCatalogProductID,		       
                            PCSB.ThirdPartySupplierID AS SupplierID,                        
                            PCSB.BrandShortName AS BrandName,   --  This column should be unneccessary
							PCSB.SupplierShortName AS SupplierName,
									--  Only here to preserve the existing application code.
						    TPP.[Name]			AS Product,                            
                            TPP.[Code]			AS Code,
                            TPP.[Packaging]		AS Packaging,
                            TPP.[LeftRightSide] AS LeftRightSide,
                            TPP.[Size]			AS Size,
                            TPP.[Gender]		AS Gender,
                            TPP.UPCCode			AS UPCCode,
							
							PI.ProductInventoryID,
							PI.QuantityOnHandPerSystem AS QOH,
							PI.ParLevel,
							PI.ReorderLevel,
							PI.CriticalLevel,
						
							PCP.WholesaleCost,		
							PI.Isactive,							
							1 AS IsThirdPartyProduct,

							PR.OldMasterCatalogProductID,
							PR.OldThirdPartyProductID,
							PR.NewMasterCatalogProductID,
							PR.NewThirdPartyProductID,
							MAX(CASE WHEN (NPI.PracticeLocationID IS NOT NULL) THEN NPCP.PracticeCatalogProductID ELSE 0 END) AS NewPracticeCatalogProductID,
							NTPP.Name,
							NTPP.Code AS NewProductCode,
							PR.PracticeID AS NewPracticeID
                     
                    FROM dbo.PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
                    INNER JOIN [PracticeCatalogProduct] AS PCP WITH(NOLOCK) ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
                    INNER JOIN [ProductInventory] AS PI WITH(NOLOCK) ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
                    INNER JOIN [ThirdPartyProduct] AS TPP WITH(NOLOCK) ON PCP.[ThirdPartyProductID] = TPP.[ThirdPartyProductID]
					LEFT OUTER JOIN ProductReplacement AS PR WITH(NOLOCK) ON PR.IsActive = 1 AND
						(PR.OldThirdPartyProductID = TPP.ThirdPartyProductID) AND
						(PR.PracticeID IS NULL OR PR.PracticeID = PCP.PracticeID)
					LEFT JOIN ThirdPartyProduct NTPP ON NTPP.ThirdPartyProductID = PR.NewThirdPartyProductID
					LEFT JOIN PracticeCatalogProduct NPCP ON NPCP.ThirdPartyProductID = PR.NewThirdPartyProductID
					LEFT JOIN ProductInventory NPI ON NPI.PracticeCatalogProductID = NPCP.PracticeCatalogProductID AND NPI.PracticeLocationID = @PracticeLocationID
                                                 
                     WHERE  PI.[PracticeLocationID] = @PracticeLocationID
							AND PCP.IsThirdPartyProduct = 1		-- ThirdPartyProductsOnly 
							AND TPP.IsActive = 1
							AND TPP.IsDiscontinued = 0
							AND PCP.IsActive = 1
							AND PI.IsActive = 1
							AND (
									(@SearchCriteria = 'Code' AND TPP.Code LIKE @SearchText)
									OR
									(
										@SearchCriteria = 'UPCCode' --AND TPP.UPCCode like @SearchText)									
										AND
										(
											 (@SearchText IS NULL)
											 OR EXISTS
											 (  
												Select 
													1 
												 from 
													UPCCode upc 
												 where 
													(
														upc.ThirdPartyProductID = TPP.ThirdPartyProductID 
														and upc.Code like @SearchText 
													) 
											  )
										 )	
									)
								)

						GROUP BY
							PCP.PracticeCatalogProductID,
                            PCSB.ThirdPartySupplierID,
                            PCSB.BrandShortName,
							PCSB.SupplierShortName,
							TPP.[Name],
                            TPP.[Code],
                            TPP.[Packaging],
							TPP.[LeftRightSide],
                            TPP.[Size],
                            TPP.[Gender],
                            TPP.UPCCode,
							PI.ProductInventoryID,
							PI.QuantityOnHandPerSystem,
							PI.ParLevel,
							PI.ReorderLevel,
							PI.CriticalLevel,
							PCP.WholesaleCost,		
							PI.Isactive,							
							PR.OldMasterCatalogProductID,
							PR.OldThirdPartyProductID,
							PR.NewMasterCatalogProductID,
							PR.NewThirdPartyProductID,
							NTPP.Name,
							NTPP.Code,
							PR.PracticeID
				) AS Sub

			SELECT
				OldMasterCatalogProductID,
				OldThirdPartyProductID
			INTO #WorkingTablePracticeSpecific
			FROM #WorkingTable
			WHERE NewPracticeID IS NOT NULL

			SELECT * FROM #WorkingTable WT
			WHERE NOT EXISTS (
				SELECT * FROM #WorkingTablePracticeSpecific WTPS
				WHERE
					COALESCE(WT.OldMasterCatalogProductID, 0) = COALESCE(WTPS.OldMasterCatalogProductID, 0) AND
					COALESCE(WT.OldThirdPartyProductID, 0) = COALESCE(WTPS.OldThirdPartyProductID, 0) AND
					WT.NewPracticeID IS NULL
					)
			ORDER BY IsThirdPartyProduct
			
END
