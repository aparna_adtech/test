﻿



-- =============================================
-- Author:		Greg Ross
-- Create date: 09/27/07
-- Description:	Used to get the detail for a Practice. Used in the MCToPC.aspx page
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetPracticeCatalogProductInfoMCToPC_WithSearch_Without3rdParty]

	@PracticeID int,
	@SearchCriteria varchar(50),
	@SearchText  varchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
SET @SearchText = @SearchText + '%'  -- Adds wild card to the end.	

	IF (@SearchCriteria='Browse' OR @SearchCriteria='All' OR @SearchCriteria='')  -- Get All
	BEGIN
	   select
		PCP.PracticeID, 
		PCP.PracticecatalogProductID,
		PCP.MasterCatalogProductID,	
		PCP.PracticeCatalogSupplierBrandID,
		MCP.ShortName,
		MCP.Code,
		PCP.WholesaleCost,
		PCP.BillingCharge,
		PCP.DMEDeposit,
		PCP.BillingChargeCash
	 from PracticeCatalogProduct PCP
	--inner join PracticeCatalogSupplierBrand PCSB 
	--	on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
	inner join MasterCatalogProduct MCP 
		on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
	where PCP.practiceID = @PracticeID
		and PCP.isactive = 1
		--and PCSB.isactive = 1
		and MCP.isactive = 1
	end

else if @SearchCriteria='Name'  --  Get only product names
	begin
	   select
		PCP.PracticeID, 
		PCP.PracticecatalogProductID,
		PCP.MasterCatalogProductID,	
		PCP.PracticeCatalogSupplierBrandID,
		MCP.ShortName,
		MCP.Code,
		PCP.WholesaleCost,
		PCP.BillingCharge,
		PCP.DMEDeposit,
		PCP.BillingChargeCash
	 from PracticeCatalogProduct PCP
	--inner join PracticeCatalogSupplierBrand PCSB 
	--	on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
	inner join MasterCatalogProduct MCP 
		on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
	where PCP.practiceID = @PracticeID
		and PCP.isactive = 1
		--and PCSB.isactive = 1
		and MCP.isactive = 1
		and (MCP.Name LIKE @SearchText 
			OR MCP.ShortName LIKE  @SearchText)
	end

else if @SearchCriteria='Code'
	begin
	   select
		PCP.PracticeID, 
		PCP.PracticecatalogProductID,
		PCP.MasterCatalogProductID,	
		PCP.PracticeCatalogSupplierBrandID,
		MCP.ShortName,
		MCP.Code,
		PCP.WholesaleCost,
		PCP.BillingCharge,
		PCP.DMEDeposit,
		PCP.BillingChargeCash
	 from PracticeCatalogProduct PCP
	--inner join PracticeCatalogSupplierBrand PCSB 
	--	on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
	inner join MasterCatalogProduct MCP 
		on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
	where PCP.practiceID = @PracticeID
		and PCP.isactive = 1
		--and PCSB.isactive = 1
		and MCP.isactive = 1
		and MCP.Code LIKE @SearchText 
	end

ELSE IF @SearchCriteria='Brand'
	BEGIN
	   SELECT
		PCP.PracticeID, 
		PCP.PracticecatalogProductID,
		PCP.MasterCatalogProductID,	
		PCP.PracticeCatalogSupplierBrandID,
		MCP.ShortName,
		MCP.Code,
		PCP.WholesaleCost,
		PCP.BillingCharge,
		PCP.DMEDeposit,
		PCP.BillingChargeCash
	 FROM PracticeCatalogProduct PCP
	INNER JOIN PracticeCatalogSupplierBrand PCSB 
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
	INNER JOIN MasterCatalogProduct MCP 
		ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
	where PCP.practiceID = @PracticeID
		AND PCP.isactive = 1
		--and PCSB.isactive = 1
		AND MCP.isactive = 1
		AND (PCSB.SupplierName LIKE @SearchText 
			OR PCSB.SupplierShortName LIKE @SearchText
			)
	end

END




