﻿-- =============================================
-- Author:		Greg Ross
-- Create date: 10/06/08
-- Description:	Used to get all products by supplier and practice id
-- =============================================
CREATE PROCEDURE dbo.usp_GetProductsBySupplierBrand
	-- Add the parameters for the stored procedure here
	@PracticeID int,
	@PracticeCatalogSupplierBrandID int		

AS
BEGIN
	SET NOCOUNT ON;

    select PracticeCatalogProductID from practicecatalogproduct 
		where  practicecatalogsupplierbrandid=@PracticeCatalogSupplierBrandID and practiceid=@PracticeID

END
