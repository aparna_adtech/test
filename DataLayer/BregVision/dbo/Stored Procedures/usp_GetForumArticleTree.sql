﻿-- =============================================
-- Author:		<Author: Tom Thomas>
-- Create date: <Create Date: 06/17/10>
-- Description:	<Description: Get's article thread
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetForumArticleTree]  
	@Id int
	,@Depth int = 1000
	
AS 
    BEGIN
	
	SET NOCOUNT ON;
    
    WITH Tree ([Id],[ParentId],[Subject],[Body],[ForumGroupId],[MasterCatalogProductID],[UserId],[Created],[IsActive], [IsSticky], [Level], [TopParentId]) AS
	(      
		SELECT
			[Id]
			,[ParentId]
			,[Subject]
			,[Body]
			,[ForumGroupId]
			,[MasterCatalogProductID]
			,[UserId]
			,[Created]
			,[IsActive]
			,[IsSticky]
			,0 as 'Level',
			Id as 'TopParentId'
		FROM dbo.ForumArticle   
		UNION ALL       
		SELECT 
			t.[Id]
			,t.[ParentId]
			,t.[Subject]
			,t.[Body]
			,t.[ForumGroupId]
			,t.[MasterCatalogProductID]
			,t.[UserId]
			,t.[Created]
			,t.[IsActive]
			,t.[IsSticky]
			,[Level] + 1 as 'Level'
			,[TopParentId]
			FROM dbo.ForumArticle t          
				INNER JOIN Tree ON t.ParentId = Tree.Id
	)

	SELECT 
		t.*
		, u.PublicUserName as 'PublicUserName'
		, u.PublicRole as 'PublicUserRole'
		, u.PublicDescription as 'PublicUserDescription'
		, p.PracticeName as 'PracticeName'
		, p.PublicDescription as 'PracticeDescription'
		, g.Name as 'ForumGroupName'
	FROM Tree t
	LEFT OUTER JOIN dbo.[User] u ON t.UserId = u.UserId
	LEFT OUTER JOIN dbo.[Practice] p ON u.PracticeID = p.PracticeID
	LEFT OUTER JOIN dbo.[ForumGroup] g ON t.ForumGroupId = g.Id
	WHERE [TopParentId] = @Id 
	And [Level] < @Depth
	ORDER BY [Level], ParentId, IsSticky, Created desc

    END

-- exec [usp_GetForumArticleTree] 1
