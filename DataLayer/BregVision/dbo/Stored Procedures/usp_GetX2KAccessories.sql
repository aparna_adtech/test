﻿-- =============================================
-- Author:		Michael Sneen
-- Create date: May 20,2009
-- Description:	Used to get all Fusion Accessories for the Custom Brace form on POShipBillCustomBrace.aspx
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetX2KAccessories]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT 
	   [MasterCatalogProductID]
      ,[Code]
      ,[Name]
      ,[ShortName]
      ,[Size]
      ,[Color]
      ,[Gender]
      ,[WholesaleListCost]
      ,[Description]
      ,[IsActive]
  FROM [dbo].[MasterCatalogProduct]
  where (
		[Code] = '70057'
	OR	[Code] = '70058'
	OR	[Code] like '0985%'
	OR	[Code] like '0735%'
	OR	[Code] like '1099%'
	OR	[Code] like '1931%'
	OR	[Code] like '1008%')
	AND IsActive = 1
	
END
