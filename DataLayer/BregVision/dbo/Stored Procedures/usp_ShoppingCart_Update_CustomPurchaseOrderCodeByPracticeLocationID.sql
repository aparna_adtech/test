﻿

CREATE PROC [dbo].[usp_ShoppingCart_Update_CustomPurchaseOrderCodeByPracticeLocationID]
	  @PracticeLocationID INT
	, @CustomPurchaseOrderCode VARCHAR(75)
AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE ShoppingCart
	SET CustomPurchaseOrderCode = @CustomPurchaseOrderCode
	WHERE PracticeLocationID = @PracticeLocationID AND IsActive = 1

END

