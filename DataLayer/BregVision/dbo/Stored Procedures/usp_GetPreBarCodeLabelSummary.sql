﻿





/* ------------------------------------------------------------
   PROCEDURE:    dbo.[usp_GetPreBarCodeLabelSummary]
   
   Description:  Gets Pre Dispensement Summary 
				based on an array of ProductInventoryIDs
   
   
   Testing:
		--  DECLARE @Array VARCHAR(4000)
		--  SET @Array = '1, 5, 3, 2, 4'

		--  EXEC dbo.usp_GetPreDispensementSummary @ProductInventoryIDArray = @Array 
			
		
	AUTHOR:       Greg Ross 10/06/08
   


   ------------------------------------------------------------ */  

--SELECT * FROM productinventory AS PI INNER JOIN dbo.PracticeCatalogProduct AS PCP ON pcp.PracticeCatalogProductID = PI.practicecatalogproductid WHERE PI.PracticeLocationID = 10

CREATE PROCEDURE [dbo].[usp_GetPreBarCodeLabelSummary] --'33, 34, 35, 164, 165'
		@ProductInventoryIDArray		VARCHAR(4000)
AS
BEGIN


SELECT

		SUB.PracticeID
	  --, SUB.ProductInventoryID
	  , SUB.PracticeCatalogProductID
	  
	  , SUB.SupplierShortName
	  , SUB.BrandShortName

	  , SUB.ShortName
	  , SUB.Code
	  , ISNULL(SUB.Side, '') AS Side
	  , ISNULL(SUB.Size, '') AS Size	
	  , ISNULL(SUB.Gender, '') AS Gender
	  
--	  , SUB.WholesaleCost
--	  , ISNULL(SUB.BillingCharge, 0) AS BillingCharge
--	  , ISNULL(SUB.BillingChargeCash, 0) AS BillingChargeCash
--	  , ISNULL(SUB.DMEDeposit, 0) AS DMEDeposit
--	  , SUB.QuantityOnHandPerSystem
--	  , ISNULL(SUB.QuantityOnHandPerSystem, 0)  AS MaxValueDispensable
--	  , SUB.HCPCSString -- JB 20071004 Added Column for UDF Concat of HCPCS

FROM
		(
		
		SELECT distinct
			

			PCP.PracticeID
		  --, PI.ProductInventoryID
		  , PCP.PracticeCatalogProductID
		  
		  , PCSB.SupplierShortName
		  , PCSB.BrandShortName

		  , MCP.ShortName
		  , MCP.Code
	      , MCP.LeftRightSide as Side
		  , MCP.Size		
		  , MCP.Gender	  
		  
--		  , PCP.WholesaleCost
--		  , PCP.BillingCharge
--		  , PCP.BillingChargeCash
--		  , PCP.DMEDeposit	
--		  , PI.QuantityOnHandPerSystem
--		  , dbo.ConcatHCPCS(PI.PracticeCatalogProductID) AS HCPCSString -- JB 20071004 Added Column for UDF Concat of HCPCS

	--	  , SELECT HCPCS FROM ProductHCPCS WHERE PracticeCatalogProductID = PI.PracticeCatalogProductID

	--	  , SELECT HCPCSString FROM dbo.udf_GetHCPCSString ( PI.PracticeCatalogProductID, ',') AS HCPCSString  -- Pass in the PI.PracticeCatalogProductID and spit out Hcpcs string.

		FROM
			dbo.PracticeCatalogSupplierBrand AS PCSB WITH (NOLOCK)
		 
			INNER JOIN dbo.PracticeCatalogProduct AS PCP WITH (NOLOCK)
				ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
		 
			INNER JOIN dbo.MasterCatalogProduct AS MCP WITH (NOLOCK)
				ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
		 
			--INNER JOIN dbo.ProductInventory AS PI WITH (NOLOCK)
			--	ON PCP.PracticeCatalogProductID = PI.PracticeCatalogProductID
		 
			INNER JOIN 	( SELECT ID FROM dbo.udf_ParseArrayToTable( @ProductInventoryIDArray, ',') ) AS PIArray
				--ON PI.ProductInventoryID = PIArray.ID

				  On PCP.PracticeCatalogProductID = PIArray.ID	
		 
			WHERE PCP.IsActive = 1
			
--			ORDER BY
--				   PCSB.SupplierShortName
--				 , PCSB.BrandShortName
--				 , MCP.ShortName

	UNION
	
		SELECT distinct
			PCP.PracticeID
		  --, PI.ProductInventoryID
		  , PCP.PracticeCatalogProductID
		  
		  , PCSB.SupplierShortName
		  , PCSB.BrandShortName

		  , TPP.ShortName
		  , TPP.Code
		  , TPP.LeftRightSide as Side
		  , TPP.Size	
		  , TPP.Gender
		  
--		  , PCP.WholesaleCost
--		  , PCP.BillingCharge
--		  , PCP.BillingChargeCash
--		  , PCP.DMEDeposit	
--		  , PI.QuantityOnHandPerSystem
--		  , dbo.ConcatHCPCS(PI.PracticeCatalogProductID) AS HCPCSString -- JB 20071004 Added Column for UDF Concat of HCPCS
		
	--	  , SELECT HCPCS FROM ProductHCPCS WHERE PracticeCatalogProductID = PI.PracticeCatalogProductID

	--	  , SELECT HCPCSString FROM dbo.udf_GetHCPCSString ( PI.PracticeCatalogProductID, ',') AS HCPCSString  -- Pass in the PI.PracticeCatalogProductID and spit out Hcpcs string.

		FROM
			dbo.PracticeCatalogSupplierBrand AS PCSB WITH (NOLOCK)
		 
			INNER JOIN dbo.PracticeCatalogProduct AS PCP WITH (NOLOCK)
				ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
		 
			INNER JOIN dbo.ThirdPartyProduct AS TPP WITH (NOLOCK)
				ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
		 
			--INNER JOIN dbo.ProductInventory AS PI WITH (NOLOCK)
			--	ON PCP.PracticeCatalogProductID = PI.PracticeCatalogProductID
		 
			INNER JOIN 	( SELECT ID FROM dbo.udf_ParseArrayToTable( @ProductInventoryIDArray, ',') ) AS PIArray
				--ON PI.ProductInventoryID = PIArray.ID
				  On PCP.PracticeCatalogProductID = PIArray.ID	
		 
			WHERE PCP.IsActive = 1
			
--			ORDER BY
--				   PCSB.SupplierShortName
--				 , PCSB.BrandShortName
--				 , TPP.ShortName
				 
		) AS SUB

	ORDER BY
		  SUB.SupplierShortName
		, SUB.BrandShortName
		, SUB.ShortName
		, SUB.Code
	
END






