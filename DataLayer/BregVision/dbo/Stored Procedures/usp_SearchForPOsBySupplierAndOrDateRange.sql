﻿




-- =============================================
-- Author:		Greg Ross
-- Create date: 08/21/07
-- Description:	Search for all PO by supplier and/or date range for Check In
-- =============================================
CREATE PROCEDURE [dbo].[usp_SearchForPOsBySupplierAndOrDateRange]
	@PracticeLocationID int,
	@SearchCriteria int,
	@SearchText varchar(50),
	@SearchFromDate datetime = null,
	@SearchToDate datetime = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	

if 	@SearchCriteria = 0 --Both
	begin
		EXEC	[dbo].usp_SearchForPOsBySupplierAndDateRange_With_ThirdPartyProducts
		@PracticeLocationID = @PracticeLocationID,
		@SearchText = @SearchText,
		@SearchFromDate = @SearchFromDate,
		@SearchToDate = @SearchToDate
			
	end 	

if 	@SearchCriteria = 1 --Supplier
	begin
		EXEC	[dbo].usp_SearchForPOsBySupplier_With_ThirdPartyProducts
		@PracticeLocationID = @PracticeLocationID,
		@SearchText = @SearchText

	end 

if 	@SearchCriteria = 2 --Date Range
	begin
		EXEC	[dbo].usp_SearchForPOsByDateRange_With_ThirdPartyProducts
		@PracticeLocationID = @PracticeLocationID,
		@SearchFromDate = @SearchFromDate,
		@SearchToDate = @SearchToDate
	end 	
END





