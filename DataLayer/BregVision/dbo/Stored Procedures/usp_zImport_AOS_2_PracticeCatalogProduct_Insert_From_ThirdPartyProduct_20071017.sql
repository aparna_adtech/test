﻿
--  Step 2 for AOS
--  AOS's PracticeID is 7

CREATE PROC [dbo].[usp_zImport_AOS_2_PracticeCatalogProduct_Insert_From_ThirdPartyProduct_20071017] 
	@PracticeID INT

as
begin


INSERT INTO PracticeCatalogProduct
	( PracticeID
	, PracticeCatalogSupplierBrandID
	, ThirdPartyProductID
	, IsThirdPartyProduct
	, WholesaleCost
	, CreatedUserID
	, IsActive
	)

SELECT
		PCSB.PracticeID
	, PCSB.PracticeCatalogSupplierBrandID
	, TPS.ThirdPartyProductID
	, 1 AS IsThirdPartyProduct
	, TPS.WholesaleListCost AS WholesaleCost
	, TPS.CreatedUserID
	, TPS.IsActive
FROM ThirdPartyProduct AS TPS
	INNER JOIN PracticeCatalogSupplierBrand AS PCSB
		ON TPS.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
	INNER JOIN Practice AS P
		ON P.PracticeID = PCSB.PracticeID
	WHERE P.PracticeID = @PracticeID
	AND TPS.ThirdPartyProductID NOT IN
		(SELECT ThirdPartyProductID 
		 FROM PracticeCatalogProduct 
		 WHERE IsActive = 1 
			AND IsThirdPartyProduct = 1) 

end