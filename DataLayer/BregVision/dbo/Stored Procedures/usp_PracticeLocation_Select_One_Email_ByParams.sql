﻿
/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocation_Select_One_Email_ByParams
   
   Description:  Selects the email columns of a record from table dbo.PracticeLocation
   				   and puts values into parameters
   
   AUTHOR:       John Bongiorni 9/24/2007 11:41:17 AM
   
   Modifications:  
  
   Added FaxForPrintDispense				Ysi             09/07/2013
   
   ------------------------------------------------------------ */  ---------------------SELECT PARAMS-----------

CREATE PROCEDURE [dbo].[usp_PracticeLocation_Select_One_Email_ByParams]
(
      @PracticeLocationID            INT
    , @EmailForOrderApproval         VARCHAR(200)   OUTPUT
    , @EmailForOrderConfirmation     VARCHAR(200)   OUTPUT
    , @EmailForFaxConfirmation       VARCHAR(200)   OUTPUT
    , @EmailForBillingDispense		 VARCHAR(200)   OUTPUT
    , @EmailForLowInventoryAlerts    VARCHAR(200)   OUTPUT
    , @IsEmailForLowInventoryAlertsOn BIT           OUTPUT
    , @EmailForPrintDispense		 VARCHAR(200)=NULL   OUTPUT
    , @FaxForPrintDispense			 VARCHAR(100)=NULL  OUTPUT
)
AS
BEGIN
	DECLARE @Err INT

	SELECT
		  @EmailForOrderApproval = ISNULL(EmailForOrderApproval, '')
        , @EmailForOrderConfirmation = ISNULL(EmailForOrderConfirmation, '')
        , @EmailForFaxConfirmation = ISNULL(EmailForFaxConfirmation, '')
        , @EmailForBillingDispense = ISNULL(EmailForBillingDispense, '')
        , @EmailForLowInventoryAlerts = ISNULL(EmailForLowInventoryAlerts, '')
        , @IsEmailForLowInventoryAlertsOn = ISNULL(IsEmailForLowInventoryAlertsOn, 0)
        , @EmailForPrintDispense = ISNULL(EmailForPrintDispense, '')
        , @FaxForPrintDispense = ISNULL(FaxForPrintDispense, '')

	FROM dbo.PracticeLocation
	WHERE 
		PracticeLocationID = @PracticeLocationID
		AND IsActive = 1

	SET @Err = @@ERROR

	RETURN @Err
End
