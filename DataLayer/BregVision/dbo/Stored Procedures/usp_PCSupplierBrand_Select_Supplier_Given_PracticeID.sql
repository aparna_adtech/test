﻿
/*
	[dbo].[usp_PCSupplierBrand_Select_Supplier_Given_PracticeID] 
	John Bongiorni
	2007.11.30
	Get All Distinct Suppliers Given a PracticeID
	Order by correct sequence, breg, MCSupplier alpha, ThirdPartySuppliers alpha.

	EXEC [usp_PCSupplierBrand_Select_Supplier_Given_PracticeID]  1

*/

CREATE PROC [dbo].[usp_PCSupplierBrand_Select_Supplier_Given_PracticeID] 
			@PracticeID INT
AS 
    BEGIN

        SELECT DISTINCT
            SupplierShortName
          , IsThirdPartySupplier
          , CASE IsThirdPartySupplier WHEN 0 THEN MIN(Sequence) 
			WHEN 1 THEN MIN(Sequence) + 100 
			ELSE MIN(Sequence) + 200 
			END			AS DerivedSequence
        FROM
            PracticeCatalogSupplierBrand WITH (NOLOCK)
        WHERE
            PracticeID = @PracticeID
            AND 
            IsActive = 1
        GROUP BY
            SupplierShortName
          , IsThirdPartySupplier
        ORDER BY
           DerivedSequence 
          , IsThirdPartySupplier
          , SupplierShortName

		
    END

