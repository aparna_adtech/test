﻿/*
	usp_Repoort_ProductsDispensed
	AND PCP, MCP, TPP, and HCPCS
	

*/



CREATE PROC [dbo].[usp_Repoort_ProductsDispensed] 
		  @PracticeID	INT
		, @PracticeLocationID VARCHAR(2000)
		, @SupplierShortName  VARCHAR(2000)
		, @StartDate		DATETIME
		, @EndDate			DATETIME
AS
BEGIN

--	DECLARE @PracticeID INT
--	SET @PracticeID = 6


--  SET @EndDate to 11:59:59 PM
SELECT @EndDate = DATEADD(DAY, 1, @EndDate)

DECLARE @PracticeLocationIDString VARCHAR(2000)
SET @PracticeLocationIDString = @PracticeLocationID

/*

SELECT * FROM zzzReportMessage

*/



SELECT 
	  D.DispenseID
 	, D.PracticeLocationID
	, D.PatientCode
	, D.Comment
	, DD.PhysicianID

	, DD.ActualChargeBilled
	, DD.DMEDeposit
	, DD.Quantity
	
	, PI_DD.ActualWholesaleCost

	, DD.ActualChargeBilled * DD.Quantity AS BillingTotal
	, DD.ActualChargeBilled * DD.Quantity AS DDTotal

	, *
FROM Dispense AS D
INNER JOin DispenseDetail AS DD
	ON D.DispenseID = DD.DispenseID
INNER JOIN ProductInventory_DispenseDetail AS PI_DD
	ON DD.DispenseDetailID = PI_DD.DispenseDetailID 





SELECT 
		 SUB.PracticeName
		, SUB.PracticeLocation
--		, SUB.IsThirdPartySupplier
		, SUB.SupplierShortName
		--, SUB.BrandShortName
		
		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN NULL
		ELSE SUB.BrandShortName
		END AS BrandShortName
		
		
		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN 1
		ELSE 0
		END AS IsBrandSuppressed
		
		, SUB.Sequence

		, SUB.ProductName
		, SUB.Side
		, SUB.Size
		, SUB.Gender
		, SUB.Code
		
		, CAST(SUB.ActualWholesaleCost AS DECIMAL(15,2) )	AS ActualWholesaleCost
		, SUM(SUB.QuantityOrdered)							AS QuantityOrdered
		, CAST( SUM(SUB.LineTotal)  AS DECIMAL(15,2) )		AS TotalCost
		
--		, SUB.OrderDate
FROM
	(SELECT 
		  P.PracticeName
		, PL.NAME AS PracticeLocation
		, PCSB.IsThirdPartySupplier
		, PCSB.SupplierShortName
		, PCSB.BrandShortName
		, PCSB.Sequence

		, MCP.[Name] AS ProductName
		, MCP.LeftRightSide AS Side
		, MCP.Size AS Size
		, MCP.Gender AS Gender
		, MCP.Code AS Code
		
		, SOLI.ActualWholesaleCost
		, SOLI.QuantityOrdered
		, SOLI.LineTotal

		, SOLI.CreatedDate AS OrderDate		
		
	FROM dbo.SupplierOrderLineItem AS SOLI WITH (NOLOCK)
	INNER JOIN dbo.SupplierOrder AS SO	WITH (NOLOCK)
		ON SOLI.SupplierOrderID = SO.SupplierOrderID
	INNER JOIN dbo.BregVisionOrder AS BVO	WITH (NOLOCK)
		ON BVO.BregVisionOrderID = SO.BregVisionOrderID
	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON BVO.PracticeLocationID = PL.PracticeLocationID
	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID
	INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
		ON SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
	INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
		ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
	WHERE P.PracticeID = @PracticeID		
		
		AND 
		PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))

		AND PCSB.SupplierShortName IN 			
			(SELECT TextValue 
			 FROM dbo.udf_ParseStringArrayToTable( @SupplierShortName, ',') )
	
		AND SOLI.CreatedDate >= @StartDate
		AND SOLI.CreatedDate < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM
		
	UNION
	
		SELECT 
		  P.PracticeName
		, PL.NAME AS PracticeLocation
		, PCSB.IsThirdPartySupplier
		, PCSB.SupplierShortName
		, PCSB.BrandShortName
		, PCSB.Sequence
		
		, TPP.[Name] AS ProductName
		, TPP.LeftRightSide AS Side
		, TPP.Size AS Size
		, TPP.Gender AS Gender
		, TPP.Code AS Code
		
		, SOLI.ActualWholesaleCost
		, SOLI.QuantityOrdered
		, SOLI.LineTotal
		
		, SOLI.CreatedDate AS OrderDate
				
		
	FROM dbo.SupplierOrderLineItem AS SOLI	WITH (NOLOCK)
	INNER JOIN dbo.SupplierOrder AS SO	WITH (NOLOCK)
		ON SOLI.SupplierOrderID = SO.SupplierOrderID
	INNER JOIN dbo.BregVisionOrder AS BVO	WITH (NOLOCK)
		ON BVO.BregVisionOrderID = SO.BregVisionOrderID
	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON BVO.PracticeLocationID = PL.PracticeLocationID
	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID
	INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
		ON SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
	INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
		ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
	WHERE 
		P.PracticeID = @PracticeID	

		AND PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))

		AND PCSB.SupplierShortName IN 			
			(SELECT TextValue 
		 FROM dbo.udf_ParseStringArrayToTable( @SupplierShortName, ',') )

		AND SOLI.CreatedDate >= @StartDate
		AND SOLI.CreatedDate < @EndDate --  Less than since @EndDate has one day added to it since the time is 12AM
	
	)		
		AS SUB	
		
	GROUP BY
		SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.IsThirdPartySupplier
		, SUB.Sequence
		, SUB.SupplierShortName
		, SUB.BrandShortName
		
		, SUB.ProductName
		, SUB.Side
		, SUB.Size
		, SUB.Gender
		, SUB.Code
		
		, SUB.ActualWholesaleCost
		
	ORDER BY
		SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.IsThirdPartySupplier
		, SUB.Sequence
		, SUB.SupplierShortName
		, SUB.BrandShortName
		, SUB.ProductName
		, SUB.Code
		
END