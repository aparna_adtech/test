﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetConsignmentRepsByPractice]
@PracticeID int
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	 SELECT U.[UserId]
      ,U.[UserName]
  FROM [aspnetdb].[dbo].[aspnet_Users] U
left outer join 
  [dbo].[ConsignmentRep_Practice] P 
  on U.UserId = P.UserID 
  where P.PracticeID = @PracticeID 
END
