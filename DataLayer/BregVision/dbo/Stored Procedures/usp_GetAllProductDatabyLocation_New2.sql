﻿


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>

--  TEST:  [dbo].[usp_GetAllProductDatabyLocation] 11, 5

--  modification 2007 10 03 change Product to varchar 100 instead of varchar 50
--  2007 10 11  ADD third party products and the isThirdPartyProduct column.
--  20071012 JB  Switch the SupplierID to the PCP.PracticeCatalogSupplierBrandID
--                              -- JB 20071012 1527 Swith out
--                            PCP.PracticeCatalogSupplierBrandID AS SupplierID,
--                            --PCSB.[ThirdPartySupplierID] AS SupplierID,

-- =============================================
CREATE PROCEDURE [dbo].[usp_GetAllProductDatabyLocation_New2] --11, 5  --18, 6
	-- Add the parameters for the stored procedure here
	@PracticeLocationID INT,
	@PracticeID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- Table Variable to be built
DECLARE @QuantityInTransit Table
    (
      Category varchar(50),
      SubCategory varchar(50),
      Product varchar(100),
      Code varchar(50),
      Packaging varchar(50),
      LeftRightSide varchar(50),
      Size VARCHAR(50),
      Gender VARCHAR(50),
      SupplierID INT,
      practicecatalogproductid INT,
      QuantityNotFullyCheckin INT DEFAULT 0,
      QuantityCheckIn INT DEFAULT 0,
      QuantityBaseLine INT DEFAULT 0,
      QuantityOnHand INT DEFAULT 0,
      ReorderLevel INT DEFAULT 0,
      PARLevel INT DEFAULT 0,
      SuggestedReorderLevel INT DEFAULT 0,
      IsThirdPartyProduct BIT
    )

-- Insert the practicecatalogID for product that are in inventory and active.
INSERT INTO @QuantityInTransit
( practicecatalogproductid)
SELECT  productinventory.practicecatalogproductid
                
        FROM productinventory
             INNER JOIN [PracticeCatalogProduct] ON PracticeCatalogProduct.PracticeCatalogProductID = productinventory.PracticeCatalogProductID
				where ProductInventory.PracticeLocationID = @PracticeLocationID
                AND PracticeCatalogProduct.PracticeID = @PracticeID
				and productinventory.isactive=1
				and PracticeCatalogProduct.isactive=1


UPDATE  @QuantityInTransit
SET     QIT.QuantityNotFullyCheckin = CheckIn.QuantityNotFullyCheckin
FROM    @QuantityInTransit AS QIT
        INNER JOIN ( SELECT [PracticeCatalogProduct].[PracticeCatalogProductID],
                            SUM(quantityordered) AS QuantityNotFullyCheckin
                     FROM   SUPPLIERORDERLINEITEM
                            INNER join SUPPLIERORDER ON SUPPLIERORDERLINEITEM.SUPPLIERORDERID = supplierorder.SUPPLIERORDERID
                            INNER JOIN productinventory ON SUPPLIERORDERLINEITEM.PracticeCatalogProductID = productinventory.PracticeCatalogProductID
                            INNER JOIN [PracticeCatalogProduct] ON PracticeCatalogProduct.PracticeCatalogProductID = productinventory.PracticeCatalogProductID
                     WHERE  supplierOrderStatusId IN ( 1, 3 )
                            AND SUPPLIERORDERLINEITEM.SUPPLIERORDERLINEITEMStatusID IN (
                            1, 3 )
                            AND ProductInventory.PracticeLocationID = @PracticeLocationID
                            AND PracticeCatalogProduct.PracticeID = @PracticeID
                            AND SUPPLIERORDERLINEITEM.[IsActive] = 1
                            AND SUPPLIERORDER.[IsActive] = 1
                            AND [ProductInventory].[IsActive] = 1
                            AND [PracticeCatalogProduct].[IsActive] = 1
                     GROUP BY [PracticeCatalogProduct].PracticeCatalogProductid,
                            supplierOrder.supplierOrderid,
                            productinventory.practicecatalogproductid
                   ) AS CheckIn ON QIT.practicecatalogproductid = CheckIn.practicecatalogproductid
                   
                   

UPDATE  @QuantityInTransit
SET     QIT.Category = CheckIn.Category,
        QIT.SubCategory = CheckIn.SubCategory,
        QIT.Product = CheckIn.Product,
        QIT.Code = CheckIn.Code,
        QIT.Packaging = CheckIn.Packaging,
        QIT.LeftRightSide = CheckIn.LeftRightSide,
        QIT.Size = CheckIn.Size,
        QIT.Gender = CheckIn.Gender,
        QIT.SupplierID = CheckIn.SupplierID,
        QIT.IsThirdPartyProduct = CheckIn.IsThirdPartyProduct
        
FROM    @QuantityInTransit AS QIT
        INNER JOIN 
       
				( SELECT 
		
		
					Sub.PracticeCatalogProductID,
                    Sub.CATEGORY,
                    Sub.SUBCATEGORY,
                    Sub.PRODUCT,
                    Sub.Code,
                    Sub.Packaging,
                    Sub.LeftRightSide,
                    Sub.Size,
                    Sub.Gender,
                    Sub.SupplierID,
                    Sub.IsThirdPartyProduct        
        	
--					CheckIn.PracticeCatalogProductID,
--                    CheckIn.CATEGORY,
--                    CheckIn.SUBCATEGORY,
--                    CheckIn.PRODUCT,
--                    CheckIn.Code,
--                    CheckIn.Packaging,
--                    CheckIn.LeftRightSide,
--                    CheckIn.Size,
--                    CheckIn.Gender,
--                    CheckIn.SupplierID,
--                    CheckIn.IsThirdPartyProduct        
--        
					FROM
						( SELECT 
							PCP.[PracticeCatalogProductID],
                            mCC.NAME AS cATEGORY,
                            mcsc.[Name] AS sUBcATEGORY,
                            MCP.[Name] AS pRODUCT,
                            MCP.[Code] AS Code,
                            MCP.[Packaging] AS Packaging,
                            MCP.[LeftRightSide] AS LeftRightSide,
                            mcp.[Size] AS Size,
                            mcp.[Gender] AS Gender,
                            
                            -- JB 20071012 1527 Swith out
                            PCP.PracticeCatalogSupplierBrandID AS SupplierID,
                            --mcs.[MasterCatalogSupplierID] AS SupplierID,
                            PCP.IsThirdPartyProduct 
                            
						 FROM   
								[MasterCatalogSupplier] AS MCS   WITH(NOLOCK)
								
								INNER JOIN [MasterCatalogCategory] AS MCC   WITH(NOLOCK)
									ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
								
								INNER JOIN [MasterCatalogSubCategory] AS MCSC   WITH(NOLOCK)
									ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]

								
								INNER JOIN [MasterCatalogProduct] AS MCP  WITH(NOLOCK)  ---ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
									ON MCP.MasterCatalogSubCategoryID = MCSC.MasterCatalogSubCategoryID
						        
								INNER JOIN [PracticeCatalogProduct] AS PCP WITH(NOLOCK)
									ON MCP.[MasterCatalogProductID] = PCP.[MasterCatalogProductID]
															
								INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB  WITH(NOLOCK)
									ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
									
								INNER JOIN [ProductInventory] AS PI WITH(NOLOCK)
									ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
									
						 WHERE  PI.[PracticeLocationID] = @PracticeLocationID
								AND PCP.IsThirdPartyProduct = 0
	                   
                   --  20071011 JB Union the thirdpartyproducts.
                   UNION
                   
					SELECT 
							
							PCP.[PracticeCatalogProductID],
                            'NA' AS cATEGORY,
                            'NA' AS sUBcATEGORY,
                            TPP.[Name] AS pRODUCT,
                            TPP.[Code] AS Code,
                            TPP.[Packaging] AS Packaging,
                            TPP.[LeftRightSide] AS LeftRightSide,
                            TPP.[Size] AS Size,
                            TPP.[Gender] AS Gender,
                            
                            -- JB 20071012 1527 Swith out
                            PCP.PracticeCatalogSupplierBrandID AS SupplierID,
                            --PCSB.[ThirdPartySupplierID] AS SupplierID,
                            
                            PCP.IsThirdPartyProduct
                     FROM   
							--[MasterCatalogSupplier] MCS 
                            --  INNER JOIN [MasterCatalogCategory] MCC ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
                            --  INNER JOIN [MasterCatalogSubCategory] MCSC ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
                            --INNER JOIN [MasterCatalogProduct] MCP ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
                            dbo.ThirdPartyProduct AS TPP				  WITH(NOLOCK)
                            INNER JOIN [PracticeCatalogProduct] PCP       WITH(NOLOCK)
								ON PCP.[ThirdPartyProductID] = TPP.[ThirdPartyProductID]
								
							INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB  WITH(NOLOCK)
								ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
								
                            INNER JOIN [ProductInventory] PI				  WITH(NOLOCK)
								ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
                     WHERE  PI.[PracticeLocationID] = @PracticeLocationID
							AND PCP.IsThirdPartyProduct = 1
					
					
                   
                   ) AS Sub 
				)  AS CheckIn
                ON QIT.practicecatalogproductid = CheckIn.practicecatalogproductid
--------------------------------------
--  Update QuantityOnHand, Par Level
UPDATE  @QuantityInTransit
SET     --QIT.QuantityCheckIn = CheckIn.QuantityCheckIn,
        QIT.[QuantityOnHand] = CheckIn.[QuantityOnHand],
        QIT.[PARLevel] = CheckIn.[PARLevel]
FROM    @QuantityInTransit AS QIT
        INNER JOIN ( select 
							productinventory.practicecatalogproductid,
							
                       --     sum(productinventory_orderCheckin.Quantity) as QuantityCheckIn,
                            
                            productinventory.[QuantityOnHandPerSystem] AS QuantityOnHand,
                            [ProductInventory].[ParLevel] AS ParLevel
                     FROM  --   productinventory_orderCheckin,
                            productinventory,
                            PracticeCatalogProduct
                     where  
						--	productinventory_orderCheckin.ProductInventoryID = ProductInventory.productInventoryid
                            ProductInventory.PracticeLocationID = @PracticeLocationID
                            and ProductInventory.PracticeCatalogProductID = PracticeCatalogProduct.PracticeCatalogProductID
                            and PracticeCatalogProduct.PracticeID = @PracticeID
                            --AND productinventory_orderCheckin.[IsActive] = 1
                            AND [ProductInventory].[IsActive] = 1
                            AND [PracticeCatalogProduct].[IsActive] = 1
                     group by 
							PracticeCatalogProduct.practicecatalogproductid,
                            productinventory.practicecatalogproductid,
                            productinventory.[QuantityOnHandPerSystem],
                            [ProductInventory].[ParLevel] 
--order by supplierOrderlineitemid
                     
                   ) AS CheckIn ON QIT.practicecatalogproductid = CheckIn.practicecatalogproductid


--  UPDATE QuantityCheckedIn 

UPDATE  @QuantityInTransit
SET     QIT.QuantityCheckIn = CheckIn.QuantityCheckIn
        --QIT.[QuantityOnHand] = CheckIn.[QuantityOnHand],
        --QIT.[PARLevel] = CheckIn.[PARLevel]
FROM    @QuantityInTransit AS QIT
        INNER JOIN ( select 
							productinventory.practicecatalogproductid,
							
                            sum(productinventory_orderCheckin.Quantity) as QuantityCheckIn
                            
                           -- productinventory.[QuantityOnHandPerSystem] AS QuantityOnHand,
                           -- [ProductInventory].[ParLevel] AS ParLevel
                     from   productinventory_orderCheckin,
                            productinventory,
                            PracticeCatalogProduct
                     where  
							productinventory_orderCheckin.ProductInventoryID = ProductInventory.productInventoryid
                            and ProductInventory.PracticeLocationID = @PracticeLocationID
                            and ProductInventory.PracticeCatalogProductID = PracticeCatalogProduct.PracticeCatalogProductID
                            and PracticeCatalogProduct.PracticeID = @PracticeID
                            AND productinventory_orderCheckin.[IsActive] = 1
                            AND [ProductInventory].[IsActive] = 1
                            AND [PracticeCatalogProduct].[IsActive] = 1
                     group by 
							PracticeCatalogProduct.practicecatalogproductid,
                            productinventory.practicecatalogproductid
                            --productinventory.[QuantityOnHandPerSystem],
                            --[ProductInventory].[ParLevel] 
--order by supplierOrderlineitemid
                     
                   ) AS CheckIn ON QIT.practicecatalogproductid = CheckIn.practicecatalogproductid

UPDATE  @QuantityInTransit
SET     QuantityBaseLine = ( QuantityCheckIn + QuantityNotFullyCheckin )





UPDATE  @QuantityInTransit
--SET     SuggestedReorderLevel  = ParLevel -( QuantityonHand - QuantityBaseline )
--SET     SuggestedReorderLevel  =  ParLevel -( QuantityonHand + QuantityNotFullyCheckin )
SET     SuggestedReorderLevel = CASE --(ParLevel -( QuantityonHand + QuantityNotFullyCheckin ))
                                     WHEN ( ( ParLevel - ( QuantityonHand
                                                           + QuantityNotFullyCheckin ) ) < 0 )
                                     THEN 0
                                     WHEN ( ( ParLevel - ( QuantityonHand
                                                           + QuantityNotFullyCheckin ) ) >= 0 )
                                     THEN ( ParLevel - ( QuantityonHand
                                                         + QuantityNotFullyCheckin ) )
                                END	

									
--SELECT  *
--FROM    @QuantityInTransit

SELECT
      Category,
      SubCategory,
      Product,
      Code,
      Packaging,
      LeftRightSide,
      Size,
      Gender,
      SupplierID,
      practicecatalogproductid,
      QuantityNotFullyCheckin,
      QuantityCheckIn,
      QuantityBaseLine,
      QuantityOnHand,
      ReorderLevel,
      PARLevel,
      SuggestedReorderLevel,
      IsThirdPartyProduct
FROM    @QuantityInTransit
ORDER BY
	Category,
	Subcategory,
	Product,
	Code


END





