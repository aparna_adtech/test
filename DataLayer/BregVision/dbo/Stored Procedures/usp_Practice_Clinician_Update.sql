﻿
/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Practice_Clinician_Update
   
   Description:  Updates a record in the table Contact,
				and updates a record in the table Clinician
				given a PracticeID
   
   AUTHOR:       Basil Juan 11/05/2014 2:55:20 PM
   
   Modifications:  
   
   exec [dbo].[usp_Practice_Clinician_Update] 14, 'Dr.', 'Richard', '', 'Muir', '', 1, 2, ''
   ------------------------------------------------------------ */  

CREATE PROCEDURE [dbo].[usp_Practice_Clinician_Update]
(
	  @ContactID                     INT
	, @Salutation                    VARCHAR(10)
	, @FirstName                     VARCHAR(50)
	, @MiddleName                    VARCHAR(50)
	, @LastName                      VARCHAR(50)
	, @Suffix                        VARCHAR(10)
	, @IsProvider					 Bit = 0
	, @IsFitter						 Bit = 0
	, @CloudConnectID                VARCHAR(50)=NULL
	, @ModifiedUserID                INT
	, @SortOrder					 INT = 0
	, @Pin							 VARCHAR(4) = null
	, @ClearPin						 Bit = 0
	, @NPI							 VARCHAR(10) = NULL
)
AS
DECLARE
    @TransactionCountOnEntry    INT,       -- Transaction Count before the transaction begins
    @Err             		INT        --  holds the @@Error code returned by SQL Server

SELECT @Err = @@ERROR
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF @Err = 0
BEGIN    
    SELECT @TransactionCountOnEntry = @@TRANCOUNT
    BEGIN TRANSACTION
END        


IF @Err = 0
BEGIN

	IF NULLIF(@Pin, '') IS NULL
		set @Pin = NULL
	PRINT 'Pin=' + IsNull(@Pin, 'null')
		

    UPDATE dbo.Contact
	SET
		  Salutation = @Salutation
		, FirstName = @FirstName
		, MiddleName = @MiddleName
		, LastName = @LastName
		, Suffix = @Suffix
		, ModifiedUserID = @ModifiedUserID
		, ModifiedDate = GETDATE()

	WHERE 
		ContactID = @ContactID

    SELECT @Err = @@ERROR

END
IF @Err = 0
BEGIN
--

    UPDATE dbo.Clinician
	SET
		SortOrder = @SortOrder
	,	Pin = CASE
				WHEN @ClearPin = 0 THEN ISNULL(@Pin, PIN)
				ELSE NULL
			  END		  
	,	IsProvider = @IsProvider
	,   IsFitter = @IsFitter	
	,	CloudConnectId = COALESCE(@CloudConnectID, CloudConnectId)
	,	NPI = COALESCE(@NPI,NPI)

	WHERE 
		ClinicianID in (Select ClinicianID from Clinician c where c.ContactID = @ContactID)


--    SELECT @Err = @@ERROR
--
END

IF @@TranCount > @TransactionCountOnEntry
BEGIN

    IF @Err = 0
        COMMIT TRANSACTION
    ELSE
        ROLLBACK TRANSACTION
        --  Add any database logging here      

END
