﻿

-- =============================================
-- Author:		Greg Ross
-- Create date: 10/31/07
-- Description:	Gets email of the practice location contact
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetEmailForPracticeLocation]
	-- Add the parameters for the stored procedure here
	@PracticeLocationID int
AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select emailfororderapproval as email
	from PracticeLocation PL
	where PL.PracticeLocationID = @PracticeLocationID

END


