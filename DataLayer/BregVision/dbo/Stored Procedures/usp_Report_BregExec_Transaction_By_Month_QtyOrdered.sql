﻿CREATE PROC usp_Report_BregExec_Transaction_By_Month_QtyOrdered
AS
BEGIN

	SELECT 
		P.PracticeName
		, PL.Name AS PracticeLocation
		, YEAR( SOLI.CreatedDate ) AS Year
		, MONTH( SOLI.CreatedDate )  AS Month
		, SUM(SOLI.QuantityOrdered) AS QuantityOrdered
	FROM
		Practice AS P
	INNER JOIN 
		PracticeLocation AS PL
			ON P.PracticeID = PL.PracticeID
	INNER JOIN 
		BregVisionOrder AS BVO
			ON PL.PracticeLocationID = BVO.PracticeLocationID
	INNER JOIN 
		SupplierOrder AS SO
			ON BVO.BregVisionOrderID = SO.BregVisionOrderID
	INNER JOIN
		SupplierOrderLineItem AS SOLI 
			ON SO.SupplierOrderID = SOLI.SupplierOrderID
	Group BY
		P.PracticeName
		, PL.Name
		, YEAR( SOLI.CreatedDate ) 
		, MONTH( SOLI.CreatedDate ) 
	ORDER BY
		P.PracticeName
		, PL.Name	
		, YEAR( SOLI.CreatedDate ) 
		, MONTH( SOLI.CreatedDate ) 

END