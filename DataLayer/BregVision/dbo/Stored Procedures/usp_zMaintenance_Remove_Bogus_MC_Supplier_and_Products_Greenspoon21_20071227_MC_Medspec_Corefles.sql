﻿/*
	Greenspoon #79; Remove MC supplier (Medspec and Coreflex) Supplier/Brand and products (set to inactive).
	John Bongiorni   2007.12.27
*/

CREATE PROC [dbo].[usp_zMaintenance_Remove_Bogus_MC_Supplier_and_Products_Greenspoon21_20071227_MC_Medspec_Corefles]
AS 
BEGIN
	

--  Get practiceID for Greenspoon.
--  NOTE: BEFORE REUSING CHECK ALL CODE THIS IS FOR USE
--  ONLY WITH A ONE LOCATION Practice!!!!!!

SELECT * FROM PRACTICE WHERE PracticeID = 21
--  Above is Correct?  Yes.

SELECT * 
FROM PracticeLocation AS PL
--WHERE PL.PracticeLocationID = 55  --  Greenspoon has only one practice.
WHERE PL.PracticeID = 21

--  Get PracticeCatalogSupplierBrandIDs for the bogus Suppliers.
SELECT *
FROM dbo.PracticeCatalogSupplierBrand AS pcsb
WHERE pcSB.PracticeID = 21    --		Practice ID Set HERE!
AND IsThirdPartySupplier = 0
AND MasterCatalogSupplierID IS NOT NULL
AND PracticeCatalogSupplierBrandID IN (228, 232)  --  Set PCSBID HERE for bogus MC supplierbrands.

--   228	21	9	NULL	Medical Specialties	MedSpec	Medical Specialties	MedSpec	0	NULL	1	2007-12-04 07:53:45.653	NULL	NULL	1
--  232	21	6	NULL	Corflex	Corflex	Corflex	Corflex	0	NULL	1	2007-12-04 09:08:52.793	NULL	NULL	1

--  Select bogus PCproducts of the bogus suppliers.  --  83 records
SELECT PracticeCatalogProductID,
	   PracticeID,
	   PracticeCatalogSupplierBrandID,
	   MasterCatalogProductID,
	   ThirdPartyProductID,
	   IsThirdPartyProduct,
	   WholesaleCost,
	   BillingCharge,
	   DMEDeposit,
	   BillingChargeCash,
	   Sequence,
	   CreatedUserID,
	   CreatedDate,
	   ModifiedUserID,
	   ModifiedDate,
	   IsActive
FROM dbo.PracticeCatalogProduct AS PCP
WHERE PCP.PracticeID = 21    --		Practice ID Set HERE!
	AND PCP.PracticeCatalogSupplierBrandID IN (228, 232)  --  Set PCSBID HERE for bogus MC supplierbrands.
	AND PCP.IsThirdPartyProduct = 0
	--  AND PCP.IsActive = 1
ORDER BY
	PCP.PracticeCatalogSupplierBrandID
	, PCP.MasterCatalogProductID
	
--3979	21	228	9102	NULL	0	22.95	0.00	0.00	0.00	NULL	1	2007-12-04 07:53:45.653	NULL	NULL	1
--3980	21	228	9103	NULL	0	22.95	0.00	0.00	0.00	NULL	1	2007-12-04 07:53:45.667	NULL	NULL	1
--3981	21	228	9104	NULL	0	22.95	0.00	0.00	0.00	NULL	1	2007-12-04 07:53:45.667	NULL	NULL	1
--3982	21	228	9105	NULL	0	22.95	0.00	0.00	0.00	NULL	1	2007-12-04 07:53:45.667	NULL	NULL	1
--3983	21	228	9106	NULL	0	22.95	0.00	0.00	0.00	NULL	1	2007-12-04 07:53:45.667	NULL	NULL	1
--3984	21	228	9107	NULL	0	22.95	0.00	0.00	0.00	NULL	1	2007-12-04 07:53:45.667	NULL	NULL	1
--3985	21	228	9108	NULL	0	27.95	0.00	0.00	0.00	NULL	1	2007-12-04 07:53:45.667	NULL	NULL	1
--4075	21	232	5806	NULL	0	0.00	0.00	0.00	0.00	NULL	1	2007-12-04 09:08:52.793	NULL	NULL	1
--4076	21	232	5807	NULL	0	0.00	0.00	0.00	0.00	NULL	1	2007-12-04 09:08:52.793	NULL	NULL	1
--4077	21	232	5808	NULL	0	0.00	0.00	0.00	0.00	NULL	1	2007-12-04 09:08:52.793	NULL	NULL	1
--4078	21	232	5809	NULL	0	0.00	0.00	0.00	0.00	NULL	1	2007-12-04 09:08:52.793	NULL	NULL	1
--4079	21	232	5810	NULL	0	0.00	0.00	0.00	0.00	NULL	1	2007-12-04 09:08:52.810	NULL	NULL	1
--4080	21	232	5811	NULL	0	0.00	0.00	0.00	0.00	NULL	1	2007-12-04 09:08:52.810	NULL	NULL	1
--4098	21	232	6252	NULL	0	0.00	0.00	0.00	0.00	NULL	1	2007-12-04 09:14:21.560	NULL	NULL	1


	
/*
	Check the SupplierOrderLineItem table 
	and the ProductInventory table
*/

--  Check the SupplierOrderLineItem table for the bogus product inventory.
SELECT *
FROM dbo.SupplierOrderLineItem  -- zero records.
WHERE PracticeCatalogProductID IN
		(
			SELECT PracticeCatalogProductID
			FROM dbo.PracticeCatalogProduct AS PCP
			WHERE PCP.PracticeID = 21    --		Practice ID Set HERE!
				AND PCP.PracticeCatalogSupplierBrandID IN (228, 232)  --  Set PCSBID HERE for bogus MC supplierbrands.
				AND PCP.IsThirdPartyProduct = 0
				--AND PCP.IsActive = 1		
		)

--  NONE from above.

--  Check Inventory Table for the bogus PCProducts.  --  83 records
SELECT * 
FROM dbo.ProductInventory AS PI
WHERE PI.PracticeCatalogProductID IN
		(
			SELECT PracticeCatalogProductID
			FROM dbo.PracticeCatalogProduct AS PCP
			WHERE PCP.PracticeID =  21								 --	Practice ID Set HERE!
				AND PCP.PracticeCatalogSupplierBrandID IN (228, 232)  --  Set PCSBID HERE for bogus MC supplierbrands.
				AND PCP.IsThirdPartyProduct = 0
				--AND PCP.IsActive = 1		
		)

--  none FROM ABOVE.
	
/*
	Check all ProductInventory tables for the bogus product inventory records.
	Create table variable, @BogusProductInventory, to hold the bogus
	 ProductInventoryIDs.
*/
	
DECLARE @BogusProductInventory TABLE 
	(ProductInventoryID INT)	
	
INSERT INTO @BogusProductInventory
	(ProductInventoryID)
SELECT ProductInventoryID
FROM dbo.ProductInventory AS PI			--  This will select inventory from ALL LOCATIONS!
WHERE PI.PracticeCatalogProductID IN
		(
			SELECT PracticeCatalogProductID
			FROM dbo.PracticeCatalogProduct AS PCP
			WHERE PCP.PracticeID = 21								 --	Practice ID Set HERE!
				AND PCP.PracticeCatalogSupplierBrandID IN (228, 232)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
				AND PCP.IsThirdPartyProduct = 0
				AND PCP.IsActive = 1		
		)
			
SELECT DISTINCT ProductInventoryID
FROM @BogusProductInventory
ORDER BY ProductInventoryID	

--  NONE ABOVE.
		
--  Check the ProductInventory_OrderCheckIn table for the bogus product inventory.

SELECT *
FROM dbo.ProductInventory_OrderCheckIn  
WHERE ProductInventoryID IN 
				(
					SELECT DISTINCT ProductInventoryID
					FROM @BogusProductInventory
				)		
		
--	   (				
--		SELECT ProductInventoryID
--		FROM dbo.ProductInventory AS PI
--		WHERE PI.PracticeCatalogProductID IN
--				(
--					SELECT PracticeCatalogProductID
--					FROM dbo.PracticeCatalogProduct AS PCP
--					WHERE PCP.PracticeID = 11
--						AND PCP.PracticeCatalogSupplierBrandID IN (109, 111)
--						AND PCP.IsThirdPartyProduct = 0
--						AND PCP.IsActive = 1		
--				)
--		)


--  Check the ProductInventory_DispenseDetail table for the bogus product inventory.

SELECT *
FROM dbo.ProductInventory_DispenseDetail  
WHERE ProductInventoryID IN 		
				(
					SELECT DISTINCT ProductInventoryID
					FROM @BogusProductInventory
				)		

		

--  Check the ProductInventory_QuantityOnHandPhysical table for the bogus product inventory.

SELECT *
FROM dbo.ProductInventory_QuantityOnHandPhysical
WHERE ProductInventoryID IN 		
				(
					SELECT DISTINCT ProductInventoryID
					FROM @BogusProductInventory
				)		
	
--  Check the ProductInventory_Purge table for the bogus product inventory.

SELECT *
FROM dbo.ProductInventory_Purge
WHERE ProductInventoryID IN 		
				(
					SELECT DISTINCT ProductInventoryID
					FROM @BogusProductInventory
				)		

--  Check the ProductInventory_ManualCheckIn table for the bogus product inventory.

SELECT *
FROM dbo.ProductInventory_ManualCheckIn
WHERE ProductInventoryID IN 		
				(
					SELECT DISTINCT ProductInventoryID
					FROM @BogusProductInventory
				)		
			
/*
	Summary:
	 2 bogus suppliers for PracticeCatalogSupplierBrandID
	 14 bogus records in PracticeCatalogProducts  
	 2 bogus records in PracticeCatalogSupplierBrand  
*/



SELECT *
FROM dbo.PracticeCatalogProduct AS PCP
WHERE IsActive = 0


-----------------
--SELECT * 
--INTO zDeleted_ProductInventory 
--FROM dbo.ProductInventory AS PI
--WHERE PI.PracticeCatalogProductID IN
--		(
--			SELECT PracticeCatalogProductID
--			FROM dbo.PracticeCatalogProduct AS PCP
--			WHERE PCP.PracticeID = 11
--				AND PCP.PracticeCatalogSupplierBrandID IN (109, 111)
--				AND PCP.IsThirdPartyProduct = 0
--				AND PCP.IsActive = 1		
--		)
---- Note: Add DateDeleted column update the 83 rows with GetDate for 
----			the DateDeleted.


-- Delete the 83 bogus records from ProductInventory.
--BEGIN TRAN	
--	
--DELETE
--FROM dbo.ProductInventory 
--WHERE PracticeCatalogProductID IN
--		(
--			SELECT PracticeCatalogProductID
--			FROM dbo.PracticeCatalogProduct AS PCP
--			WHERE PCP.PracticeID = 11
--				AND PCP.PracticeCatalogSupplierBrandID IN (109, 111)
--				AND PCP.IsThirdPartyProduct = 0
--				--AND PCP.IsActive = 1		
--		)
--		
--ROLLBACK TRAN
		
-- Note: Add DateDeleted column update the 83 rows with GetDate for 
--			the DateDeleted.


SELECT *
FROM PracticeCatalogProduct AS PCP
WHERE PCP.IsActive = 0



--  Update the 14 bogus PCP records by setting their isActive flag to false

begIN TRAN

		SELECT *
	FROM dbo.PracticeCatalogProduct AS PCP
	WHERE PCP.PracticeID = 21									--	Practice ID Set HERE!
		AND PCP.PracticeCatalogSupplierBrandID IN (228, 232)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
		AND PCP.IsThirdPartyProduct = 0
		  AND PCP.IsActive = 1

	--  21 inactive
	SELECT *
	FROM dbo.PracticeCatalogProduct AS PCP
	WHERE PCP.PracticeID = 21									--	Practice ID Set HERE!
		AND PCP.PracticeCatalogSupplierBrandID IN (228, 232)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
		AND PCP.IsThirdPartyProduct = 0
		  AND PCP.IsActive = 0
	ORDER BY
		PCP.PracticeCatalogSupplierBrandID
		, PCP.MasterCatalogProductID

--	UPDATE PracticeCatalogProduct      -- 14 Rows Updated.
--	SET IsActive = 0
--	WHERE PracticeID = 21									--	Practice ID Set HERE!
--		AND PracticeCatalogSupplierBrandID IN (228, 232)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
--		AND IsThirdPartyProduct = 0


	--  Zero are Active, 21 are inactive.
	SELECT *
	FROM dbo.PracticeCatalogProduct AS PCP
	WHERE PCP.PracticeID = 21									--	Practice ID Set HERE!
		AND PCP.PracticeCatalogSupplierBrandID IN (228, 232)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
		AND PCP.IsThirdPartyProduct = 0
		  AND PCP.IsActive = 1

	--  21 inactive
	SELECT *
	FROM dbo.PracticeCatalogProduct AS PCP
	WHERE PCP.PracticeID = 21									--	Practice ID Set HERE!
		AND PCP.PracticeCatalogSupplierBrandID IN (228, 232)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
		AND PCP.IsThirdPartyProduct = 0
		  AND PCP.IsActive = 0
	ORDER BY
		PCP.PracticeCatalogSupplierBrandID
		, PCP.MasterCatalogProductID

	--	3979	21	228	9102	NULL	0	22.95	0.00	0.00	0.00	NULL	1	2007-12-04 07:53:45.653	NULL	NULL	0
	--	3980	21	228	9103	NULL	0	22.95	0.00	0.00	0.00	NULL	1	2007-12-04 07:53:45.667	NULL	NULL	0
	--	3981	21	228	9104	NULL	0	22.95	0.00	0.00	0.00	NULL	1	2007-12-04 07:53:45.667	NULL	NULL	0
	--	3982	21	228	9105	NULL	0	22.95	0.00	0.00	0.00	NULL	1	2007-12-04 07:53:45.667	NULL	NULL	0
	--	3983	21	228	9106	NULL	0	22.95	0.00	0.00	0.00	NULL	1	2007-12-04 07:53:45.667	NULL	NULL	0
	--	3984	21	228	9107	NULL	0	22.95	0.00	0.00	0.00	NULL	1	2007-12-04 07:53:45.667	NULL	NULL	0
	--	3985	21	228	9108	NULL	0	27.95	0.00	0.00	0.00	NULL	1	2007-12-04 07:53:45.667	NULL	NULL	0
	--	4075	21	232	5806	NULL	0	0.00	0.00	0.00	0.00	NULL	1	2007-12-04 09:08:52.793	NULL	NULL	0
	--	4076	21	232	5807	NULL	0	0.00	0.00	0.00	0.00	NULL	1	2007-12-04 09:08:52.793	NULL	NULL	0
	--	4077	21	232	5808	NULL	0	0.00	0.00	0.00	0.00	NULL	1	2007-12-04 09:08:52.793	NULL	NULL	0
	--	4078	21	232	5809	NULL	0	0.00	0.00	0.00	0.00	NULL	1	2007-12-04 09:08:52.793	NULL	NULL	0
	--	4079	21	232	5810	NULL	0	0.00	0.00	0.00	0.00	NULL	1	2007-12-04 09:08:52.810	NULL	NULL	0
	--	4080	21	232	5811	NULL	0	0.00	0.00	0.00	0.00	NULL	1	2007-12-04 09:08:52.810	NULL	NULL	0
	--	4098	21	232	6252	NULL	0	0.00	0.00	0.00	0.00	NULL	1	2007-12-04 09:14:21.560	NULL	NULL	0

rOLLBACK TRAN  --COMMIT TRAN  --  


--  After the PCProducts have been deactivated, deactivate the bogus supplierBrands.


SELECT *
FROM dbo.PracticeCatalogSupplierBrand AS pcsb
WHERE pcSB.PracticeID = 21							--	Practice ID Set HERE!
AND IsThirdPartySupplier = 0
AND MasterCatalogSupplierID IS NOT NULL
AND PracticeCatalogSupplierBrandID IN (228, 232)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
AND IsActive = 1
--228	21	9	NULL	Medical Specialties	MedSpec	Medical Specialties	MedSpec	0	NULL	1	2007-12-04 07:53:45.653	NULL	NULL	1
--232	21	6	NULL	Corflex	Corflex	Corflex	Corflex	0	NULL	1	2007-12-04 09:08:52.793	NULL	NULL	1

-- !!!!CANNOT REMOVE the bogus supplierbrands (MC Medspec and MC Coreflex) from THE APPLICATION; Greenspoon, Admin, Supplier, Brand, since they are MC Suppliers
--		Add to not have the ability to be removed.

BEGIN TRAN
-- uPDATE BOGUS PCSuppplierBrand.IsActive = 0
--UPDATE dbo.PracticeCatalogSupplierBrand 
--SET IsActive = 0
--WHERE PracticeID = 21							--	Practice ID Set HERE!
--AND IsThirdPartySupplier = 0
--AND MasterCatalogSupplierID IS NOT NULL
--AND PracticeCatalogSupplierBrandID IN (228, 232)  --  Set PCSBID HERE for bogus MC supplierbrands.  
--AND IsActive = 1
--
ROLLBACK TRAN


SELECT *
FROM dbo.PracticeCatalogSupplierBrand AS pcsb
WHERE pcSB.PracticeID = 21							--	Practice ID Set HERE!
AND IsThirdPartySupplier = 0
AND MasterCatalogSupplierID IS NOT NULL
AND PracticeCatalogSupplierBrandID IN (228, 232)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
AND IsActive = 0


END