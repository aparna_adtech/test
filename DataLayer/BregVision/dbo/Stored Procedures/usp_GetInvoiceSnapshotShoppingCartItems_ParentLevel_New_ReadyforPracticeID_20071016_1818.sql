﻿-- =============================================
-- Author:		Greg Ross
-- Create date: August 28th, 2007
-- Description:	Gets Practice Catalog Supplier Information for Invoice Snapshot (SP 2 of 2) 
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetInvoiceSnapshotShoppingCartItems_ParentLevel_New_ReadyforPracticeID_20071016_1818]   -- 21  --4, 10, 11, 18, 21-- Ortho admin san diego 4
	-- Add the parameters for the stored procedure here
	@PracticeLocationID int


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


SELECT  
	  
	   Parent.BrandShortName 
	 , Parent.PracticeCatalogSupplierBrandID AS PracticeCatalogSupplierBrandID
	 , Parent.TotalCost

FROM
(

SELECT

	Detail.PracticeCatalogSupplierBrandID,
	Detail.SupplierShortName AS BrandShortName,					--  SupplierName
--	Detail.BrandShortName,
	Detail.IsThirdPartySupplier,
	Detail.Sequence,
	SUM( Detail.ActualWholesaleCost ) AS TotalCost		


FROM
	(
	
	SELECT
	
		SUB.PracticeCatalogProductID,
		SUB.PracticeCatalogSupplierBrandID,
		SUB.SupplierShortName,					--  SupplierName
		SUB.BrandShortName,
		SUB.ShortName,
		SUB.Code,
		SUB.Packaging,
		SUB.LeftRightSide,
		SUB.Size,
		SUB.Color,
		SUB.Gender,
		SUB.ActualWholesaleCost,
		SUB.Quantity,
		SUB.IsThirdPartySupplier,
		SUB.Sequence

	FROM
		(
   			select	
   					PCP.PracticeCatalogProductID,
					PCSB.PracticeCatalogSupplierBrandID,
					PCSB.SupplierShortName,					--  SupplierName
					PCSB.BrandShortName,
					MCP.ShortName,
					MCP.Code,
					MCP.Packaging,
					MCP.LeftRightSide,
					MCP.Size,
					MCP.Color,
					MCP.Gender,
					SCI.ActualWholesaleCost,
					SCI.Quantity,
					PCSB.IsThirdPartySupplier,
					ISNULL( PCSB.Sequence, 999 ) AS Sequence
			
			from ShoppingCart SC
			
			inner join ShoppingCartItem SCI
				on SC.ShoppingCartID = SCI.ShoppingCartID
			
			inner join PracticeCatalogProduct PCP 
				on SCI.PracticeCatalogProductID =PCP.PracticeCatalogProductID 
			
			inner join PracticeCatalogSupplierBrand PCSB
				on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
			
			inner join MasterCatalogProduct MCP
				on PCP.MasterCatalogPRoductID = MCP.MasterCatalogPRoductID
			
			where SC.[ShoppingCartStatusID] = 0
					AND SCI.[ShoppingCartItemStatusID] = 0
					AND SC.[IsActive] = 1
					AND SCI.[IsActive] = 1
					AND PCP.IsActive = 1
					AND PCSB.IsActive = 1
					AND PCSB.IsThirdPartySupplier = 0
					AND MCP.IsActive = 1
					and SC.PracticeLocationID = @PracticeLocationID


			UNION
			
			
			   	select	
					PCP.PracticeCatalogProductID,
					PCSB.PracticeCatalogSupplierBrandID,
					--  PCSB.SupplierShortName,					--  SupplierName
					
					CASE WHEN (PCSB.IsThirdPartySupplier = 1 )AND (PCSB.SupplierShortName <> PCSB.BrandShortName) 		
							THEN PCSB.SupplierShortName + ' - ' + PCSB.BrandShortName 
							ELSE PCSB.SupplierShortName
						END AS SupplierShortName,
					
					PCSB.BrandShortName,
					TPP.ShortName,
					TPP.Code,
					TPP.Packaging,
					TPP.LeftRightSide,
					TPP.Size,
					TPP.Color,
					TPP.Gender,
					SCI.ActualWholesaleCost,
					SCI.Quantity,
					PCSB.IsThirdPartySupplier,
					ISNULL( PCSB.Sequence, 999 ) AS Sequence
				
				from ShoppingCart SC
				
				inner join ShoppingCartItem SCI
					on SC.ShoppingCartID = SCI.ShoppingCartID
				
				inner join PracticeCatalogProduct PCP 
					on SCI.PracticeCatalogProductID =PCP.PracticeCatalogProductID 
				
				inner join PracticeCatalogSupplierBrand PCSB
					on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
				
				inner join ThirdPartyProduct TPP
					on PCP.ThirdPartyPRoductID = TPP.ThirdPartyPRoductID
				
				where SC.[ShoppingCartStatusID] = 0
						AND SCI.[ShoppingCartItemStatusID] = 0
						AND SC.[IsActive] = 1
						AND SCI.[IsActive] = 1
						AND PCP.IsActive = 1
						AND PCSB.IsActive = 1
						AND PCSB.IsThirdPartySupplier = 1
						AND TPP.IsActive = 1
						and SC.PracticeLocationID = @PracticeLocationID
			) AS SUB

--		ORDER BY
--			SUB.IsThirdPartySupplier
--			, SUB.Sequence
--			, SUB.BrandShortName
--			, SUB.ShortName

	) AS Detail
	GROUP BY 
			Detail.PracticeCatalogSupplierBrandID,
			Detail.SupplierShortName,
--			Detail.BrandShortName,
			Detail.IsThirdPartySupplier,
			Detail.Sequence
--	ORDER BY 
----			--Detail.PracticeCatalogSupplierBrandID,
--			Detail.IsThirdPartySupplier,
--			Detail.Sequence,
--			Detail.SupplierShortName
----			Detail.BrandShortName
) AS Parent			

	
	ORDER BY 	 
	  Parent.IsThirdPartySupplier
	 , Parent.Sequence
	 , Parent.BrandShortName
	
END

