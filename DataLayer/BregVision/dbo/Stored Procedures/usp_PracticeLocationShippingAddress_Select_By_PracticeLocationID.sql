﻿

/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocationShippingAddress_Select_By_Params
   
   Description:  Input: PracticeLocationID
				 Selects a record from table dbo.Address, and also selects the
					PracticeLocationShippingAddress.AttentionOf column
   				     and puts values into parameters
   
   AUTHOR:       John Bongiorni 8/06/2007 7:15PM
   
   Modifications:  
   ------------------------------------------------------------ */  

Create PROCEDURE [dbo].[usp_PracticeLocationShippingAddress_Select_By_PracticeLocationID]
(
	  @PracticeLocationID			 INT
)
AS

DECLARE	@return_value int,
		@AttentionOf varchar(50),
		@AddressID int,
		@AddressLine1 varchar(50),
		@AddressLine2 varchar(50),
		@City varchar(50),
		@State char(2),
		@ZipCode char(5),
		@ZipCodePlus4 char(4)

EXEC	@return_value = [dbo].[usp_PracticeLocationShippingAddress_Select_By_Params]
		@PracticeLocationID = @PracticeLocationID,
		@AttentionOf = @AttentionOf OUTPUT,
		@AddressID = @AddressID OUTPUT,
		@AddressLine1 = @AddressLine1 OUTPUT,
		@AddressLine2 = @AddressLine2 OUTPUT,
		@City = @City OUTPUT,
		@State = @State OUTPUT,
		@ZipCode = @ZipCode OUTPUT,
		@ZipCodePlus4 = @ZipCodePlus4 OUTPUT

SELECT	@AttentionOf as N'AttentionOf',
		@AddressID as N'AddressID',
		@AddressLine1 as N'AddressLine1',
		@AddressLine2 as N'AddressLine2',
		@City as N'City',
		@State as N'State',
		@ZipCode as N'ZipCode',
		@ZipCodePlus4 as N'ZipCodePlus4'

SELECT	'Return Value' = @return_value

--usp_PracticeLocationShippingAddress_Select_By_PracticeLocationID 3

