﻿


CREATE PROCEDURE dbo.usp_PracticeLocation_User_Insert
		  @PracticeLocationID	INT
		, @UserID				INT
		, @CreatedUserID		INT
AS
BEGIN	

		DECLARE @RecordCount INT
		DECLARE @IsActive	BIT

		SELECT @RecordCount = COUNT(1) 
		FROM dbo.PracticeLocation_User
		WHERE  PracticeLocationID = @PracticeLocationID
			AND UserID = @UserID


		SELECT @IsActive	 = IsActive
		FROM dbo.PracticeLocation_User
		WHERE  PracticeLocationID = @PracticeLocationID
			AND UserID = @UserID


		IF ( @RecordCount = 0 )
		BEGIN
			INSERT INTO dbo.PracticeLocation_User
				(
					  PracticeLocationID 
					, UserID		
					, CreatedUserID
					, CreatedDate
					, IsActive
				)		
			VALUES
				(
					  @PracticeLocationID 
					, @UserID		
					, @CreatedUserID
					, GetDate()
					, 1
				)
		END


		IF ( @RecordCount = 1 AND @IsActive = 0 )
		BEGIN

			UPDATE dbo.PracticeLocation_User
			SET IsActive = 1
			WHERE  PracticeLocationID = @PracticeLocationID
				AND UserID = @UserID

		END			
				
END

