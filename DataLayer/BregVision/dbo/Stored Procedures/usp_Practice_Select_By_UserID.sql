﻿/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Practice_Select_By_UserID
   
   Description:  Selects the PracticeID and PracticeName from table Practice
				   from a given UserID
   
   AUTHOR:       John Bongiorni 9/29/2007 1:51:38 PM
   
   Modifications:  
   ------------------------------------------------------------ */   ----------------SELECT ----------------

CREATE PROCEDURE [dbo].[usp_Practice_Select_By_UserID]
(
	  @UserID INT
    , @PracticeID   INT OUTPUT
    , @PracticeName INT OUTPUT
)
As
BEGIN
	DECLARE @Err INT

	SELECT
		  @PracticeID = P.PracticeID
		, @PracticeName = P.PracticeName

	FROM dbo.[User] AS U
	INNER JOIN dbo.Practice AS P
		ON U.PracticeID = P.PracticeID
	WHERE 
		U.UserID = @UserID
		AND U.IsActive = 1
		AND P.IsActive = 1

	SET @Err = @@ERROR

	RETURN @Err
End