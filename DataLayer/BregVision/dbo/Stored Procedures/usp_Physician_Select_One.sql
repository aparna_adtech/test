﻿/* ------------------------------------------------------------
   PROCEDURE:    usp_Physician_Select_One
   
   Description:  Selects a physician 
		(actually Selects a record from table dbo.Contact)   				  
   
   AUTHOR:       John Bongiorni 7/20/2007 7:31:00 PM
   
   Modifications:  
   ------------------------------------------------------------ */


CREATE PROCEDURE [dbo].[usp_Physician_Select_One]
(
	  @PhysicianID					 INT
)
AS
BEGIN
	DECLARE @Err INT

	SELECT
		Pr.PracticeID 
	  , Ph.PhysicianID
	  , C.ContactID
	  , C.Salutation
	  , C.FirstName
	  , C.MiddleName
	  , C.LastName
	  , C.Suffix
	FROM
		dbo.Contact AS C
		INNER JOIN dbo.Physician AS Ph
			ON C.ContactID = Ph.ContactID
		INNER JOIN dbo.Practice AS Pr
			ON Ph.PracticeID = Pr.PracticeID
	WHERE
			Ph.PhysicianID = @PhysicianID
		AND Ph.IsActive = 1
		AND C.IsActive = 1
		AND Pr.IsActive = 1

	SET @Err = @@ERROR

	RETURN @Err
End


