﻿

-- ==============================================================
-- Author:		Mike Sneen
-- Create date: 7/20/2011
-- Description:	Search with multiple comma separated criteria
--Calls usp_SearchforItemsInInventory

--EXEC usp_SearchforItemsInInventory_With_ProductCode_Array 'Code', '60127,60122,60125,10085', 3 

Create PROCEDURE [dbo].[usp_SearchforItemsInInventory_With_ProductCode_Array]
	-- Add the parameters for the stored procedure here
	@SearchCriteria varchar(50),
	@SearchText varchar(Max),
	@PracticeLocationID int


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	if Exists(Select 1 from PracticeLocation PL where PL.PracticeLocationID=@PracticeLocationID)
	BEGIN
	
		DECLARE @BregProductsFromProc TABLE
		(
			ProductInventoryID int,
			PracticeCatalogProductID int,
			BrandName varchar(50),
			SupplierName varchar(50),			
			SupplierID int,
			ProductName	Varchar(50),
			Code varchar(50),
			Packaging varchar(50),
			Side varchar(50),
			Size varchar(50),
			Gender varchar(50),
			QOH int,
			ParLevel int,
			ReorderLevel int,
			CriticalLevel int,
			WholesaleCost smallMoney,
			IsActive bit,
			IsThirdPartyProduct bit
		)

		Declare @BregProducts TABLE
		(
			ProductInventoryID int,
			fUsed bit default 0
		)
		
		Insert into @BregProducts(ProductInventoryID, fUsed) (select ID, 0 from dbo.udf_ParseArrayToTable(@SearchText, ','))
		
		Declare @ProductInventoryID int

		Select TOP 1 @ProductInventoryID = ProductInventoryID from @BregProducts where fUsed = 0

		WHILE @@ROWCOUNT <> 0 AND @ProductInventoryID IS NOT NULL
		BEGIN
			Insert into @BregProductsFromProc
				EXEC usp_SearchforItemsInInventory 'Code', @ProductInventoryID, @PracticeLocationID
				
			Update @BregProducts SET fUsed = 1 where ProductInventoryID = @ProductInventoryID
				
			Select TOP 1 @ProductInventoryID = ProductInventoryID from @BregProducts where fUsed = 0
			
		END
		
		Select * from @BregProductsFromProc
	END
END