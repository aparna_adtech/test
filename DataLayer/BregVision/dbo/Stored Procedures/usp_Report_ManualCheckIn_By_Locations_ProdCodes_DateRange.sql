﻿CREATE procedure [dbo].[usp_Report_ManualCheckIn_By_Locations_ProdCodes_DateRange]
	@PracticeLocationID nvarchar(max),
	@Code nvarchar(max),
	@StartDate datetime,
	@EndDate datetime,
	@PracticeId int,
	@InternalCode nvarchar(1000),
	@InternalPracticeLocationID nvarchar(1000)
as
begin

	set nocount on;

	--- begin multi-value @CodeID parsing
	declare @codes as table ( [seq] int, [code] varchar(50) );
	if(@InternalCode = 'ALL')
	begin
		declare @tempCodes table ([code] varchar(50));
		insert @tempCodes
		exec dbo.usp_GetProductCodes_ByPractice @PracticeId;

		insert @codes ([code], [seq])
		select code
				,row_number() over(order by code) as [seq]
		from @tempCodes;
	end;
	else
	begin
		with pieces([seq], [start], [stop])
		as
		(
			select 1, 1, charindex(',', @Code)
			union all
			select cast([seq] + 1 as int), cast([stop] + 1 as int), charindex(',', @Code, [stop] + 1)
			from pieces
			where [stop] > 0
		)
		insert into @codes
		select
			[seq],
			cast(substring(@Code, [start], case when [stop] > 0 then [stop] - start else 512 end) as varchar(50))
		from
			pieces
		option
			(maxrecursion 10000);
	end;
	--- end multi-value @CodeID parsing

	--- begin multi-value @PracticeLocationID parsing
	declare @locations as table ( [seq] int, [location] int );
	if(@InternalPracticeLocationId = 'ALL')
	begin
		declare @tempLocations table (PracticeLocationID int, PracticeLocationName varchar(50), IsPrimaryLocation bit);

		insert @tempLocations
		exec dbo.usp_PracticeLocation_Select_All_ID_Name_Given_PracticeID @PracticeId;

		insert @locations ([location], [seq])
		select PracticeLocationID, Row_Number() over (order by PracticeLocationID)
		from @tempLocations;
	end;
	else
	begin
		with pieces([seq], [start], [stop])
		as
		(
			select 1, 1, charindex(',', @PracticeLocationID)
			union all
			select cast([seq] + 1 as int), cast([stop] + 1 as int), charindex(',', @PracticeLocationID, [stop] + 1)
			from pieces
			where [stop] > 0
		)
		insert into @locations
		select
			[seq],
			cast(substring(@PracticeLocationID, [start], case when [stop] > 0 then [stop] - start else 512 end) as int)
		from
			pieces;
	end;
	--- end multi-value @PracticeLocationID parsing

	set nocount off;

	select
		SUB.PracticeLocation,
		SUB.PracticeCatalogProductID,
		SUB.SupplierID,
		SUB.ProductInventoryID,
		SUB.BrandShortname,
		SUB.ShortName,
		SUB.Code,
		SUB.Packaging,
		SUB.LeftRightSide,
		SUB.Size,
		SUB.Color,
		SUB.Gender,
		SUB.QuantityOnHandPerSystem,
		SUB.WholesaleCost,
		SUB.IsThirdPartyProduct,
		SUB.DateCheckedIn,
		SUB.Comments
	from
		(
			select
				PL.Name as PracticeLocation,
				PCP.PracticeCatalogProductID,
				PCSB.MasterCatalogSupplierID as SupplierID,
				PI.ProductInventoryID,
				PCSB.BrandShortName,
				MCP.ShortName,
				MCP.Code,
				MCP.Packaging,
				MCP.LeftRightSide,
				MCP.Size,
				MCP.Color,
				MCP.Gender,
				PI.QuantityOnHandPerSystem,
				PCP.WholesaleCost,
				0 as IsThirdPartyProduct,
				PIMCI.DateCheckedIn,
				PIMCI.Comments
			from
				ProductInventory_ManualCheckIn PIMCI
					inner join ProductInventory PI
						on PIMCI.ProductInventoryID = PI.ProductInventoryID
					inner join PracticeLocation PL
						on PL.PracticeLocationID = PI.PracticeLocationID
					inner join PracticeCatalogProduct PCP
						on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
					inner join MasterCatalogProduct MCP
						on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
					inner join PracticeCatalogSupplierBrand PCSB
						on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
			where
				PI.IsActive = 1
				and PIMCI.DateCheckedIn between @StartDate and @EndDate
				--and MCP.IsActive = 1
				--and PCSB.IsActive = 1
				and
				(
					@Code is null
					or
					MCP.Code in (select code from @codes)
				)
				and PI.PracticeLocationID in (select location from @locations)

			union

			select
				PL.Name as PracticeLocation,
				PCP.PracticeCatalogProductID,
				PCSB.MasterCatalogSupplierID as SupplierID,
				PI.ProductInventoryID,
				PCSB.BrandShortName,
				TPP.ShortName,
				TPP.Code,
				TPP.Packaging,
				TPP.LeftRightSide,
				TPP.Size,
				TPP.Color,
				TPP.Gender,
				PI.QuantityOnHandPerSystem,
				PCP.WholesaleCost,
				1 as IsThirdPartyProduct,
				PIMCI.DateCheckedIn,
				PIMCI.Comments
			from
				ProductInventory_ManualCheckIn PIMCI
					inner join ProductInventory PI
						on PIMCI.ProductInventoryID = PI.ProductInventoryID
					inner join PracticeLocation PL
						on PL.PracticeLocationID = PI.PracticeLocationID
					inner join PracticeCatalogProduct PCP
						on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
					inner join ThirdPartyProduct as TPP with(nolock)
						on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
					inner join PracticeCatalogSupplierBrand PCSB
						on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
			where
				PI.IsActive = 1
				and PIMCI.DateCheckedIn between @StartDate and @EndDate
				--and TPP.IsActive = 1
				--and PCSB.IsActive = 1
				and
				(
					@Code is null
					or
					TPP.Code in (select code from @codes)
				)
				and PI.PracticeLocationID in (select location from @locations)
		) as SUB
	order by
		SUB.IsThirdPartyProduct asc,
		SUB.DateCheckedIn asc

end
