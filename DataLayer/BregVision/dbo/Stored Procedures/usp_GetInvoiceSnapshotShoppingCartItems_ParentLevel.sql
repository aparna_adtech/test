﻿-- =============================================
-- Author:		Greg Ross
-- Create date: August 28th, 2007
-- Description:	Gets Practice Catalog Supplier Information for Invoice Snapshot (SP 1 of 2) 
-- Modification: 20071016 JB  where is active = 1
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetInvoiceSnapshotShoppingCartItems_ParentLevel]
	-- Add the parameters for the stored procedure here
	@PracticeID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   	Select distinct 
   		PCSB.BrandShortName,
		PCSB.PracticeCatalogSupplierBrandID
	from
		PracticeCatalogSupplierBrand PCSB
		inner join PracticeCatalogProduct PCP
			on PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
		where PCSB.PracticeID = @PracticeID
		-- jb 20071016 added isactive below.
		AND PCP.isactive = 1
		AND PCSB.isactive = 1
END
