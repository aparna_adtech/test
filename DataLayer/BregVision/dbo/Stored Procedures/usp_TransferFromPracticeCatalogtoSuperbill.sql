﻿-- =============================================
-- Author:		Greg Ross
-- Create date: 10/01/07
-- Description:	Transfers products from the practice catalog to the product inventory
-- Modifications:  
--      2008.01.31  John Bongiorni
--		Set PI.IsSetToActive to 0, PI.IsActive to 1 on insert.
--      2008.02.15  John Bongiorni 
--		Set PI.IsSetToActive to 1, PI.IsActive to 1 on insert.
-- =============================================
Create PROCEDURE [dbo].[usp_TransferFromPracticeCatalogtoSuperbill]
	@PracticeLocationID int,
	@PracticeCatalogProductID int,
	@UserID int,
	@SuperbillCategoryID int,
	@HCPCs varchar(8) = null,
	@FlowsheetOnly bit = null,
	@ProductName varchar(50) = null,
	@SuperBillproductID int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @RecordCount int
declare @RecordCountInActive int

SELECT  @RecordCount = COUNT(1)
                FROM    dbo.SuperBillProduct
                WHERE   PracticeLocationID =@PracticeLocationID
                       AND practicecatalogproductid=@PracticeCatalogProductID	
                       AND IsAddOn = 0
                       AND SuperBillProductID = coalesce(@SuperBillproductID, SuperbillProductID)
--print @recordcount

if @recordcount = 0 -- No record exists
begin
	--Insert new record
	insert into SuperBillProduct
		(PracticeLocationID,
		PracticeCatalogProductID,
		SuperBillCategoryID,
		HCPCs,
		Name,
		FlowsheetOnly,
		CreateUserID,
		CreatedDate,
		isactive)
		Values
		(@PracticeLocationID,
		@PracticeCatalogProductID,
		@SuperbillCategoryID,
		@HCPCs, 
		@ProductName,
		@FlowsheetOnly,
		@UserID,
		getdate(),
		1)
	
end
ELSE
	--  Check if there is one and only one record that is InActive, 
	--		if so, then update to Active.
	SELECT  @RecordCountInActive = COUNT(1)
    FROM    dbo.SuperBillProduct
    WHERE   PracticeLocationID =@PracticeLocationID
            AND practicecatalogproductid=@PracticeCatalogProductID	
			AND IsActive = 0
			AND IsAddOn = 0
			AND SuperBillProductID = coalesce(@SuperBillproductID, SuperbillProductID)
	
	if @recordcount = 1 -- One InActive record exists, so set it to Active.
	BEGIN
		
		UPDATE dbo.SuperBillProduct
		SET  
			IsActive = 1, 
			SuperBillCategoryID = @SuperbillCategoryID, 
			HCPCs = Coalesce(@HCPCs,HCPCs), 
			FlowsheetOnly = Coalesce(@FlowsheetOnly, FlowsheetOnly),
			Name = Coalesce(@ProductName, Name)
	    WHERE   PracticeLocationID = @PracticeLocationID
            AND practicecatalogproductid = @PracticeCatalogProductID	
            AND IsAddOn = 0
            AND SuperBillProductID = coalesce(@SuperBillproductID, SuperbillProductID)
			--AND IsActive = 0
	
	END
	
END
