﻿/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_State_Select_All
   
   Description:  Selects all records from the table dbo.State
		Columns: StateID and ShortName (StateAbbr)

   AUTHOR:       John Bongiorni 7/19/2007 8:38:33 PM
   
   Modifications:  
   ------------------------------------------------------------ */ 

CREATE PROCEDURE [dbo].[usp_State_Select_All]
AS
BEGIN
	DECLARE @Err INT
	SELECT
		  Lookup_StateID
		, StateAbbr
	
	FROM dbo.Lookup_State
	ORDER BY Name
	
	SET @Err = @@ERROR

	RETURN @Err
END



