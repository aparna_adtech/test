﻿

CREATE PROCEDURE [dbo].[usp_User_Insert]
(
    @PracticeID AS INT
    , @UserName AS NVARCHAR (256)
    , @CreatedUserID AS INT
    , @UserID INT OUTPUT
) 
AS
BEGIN

	DECLARE @AspNet_UserID UNIQUEIDENTIFIER

	--  Get the aspnet_userID GUID given the UserName
	SELECT @AspNet_UserID = AU.UserID
	FROM aspnetdb.dbo.aspnet_Users AS AU
	INNER JOIN 
		aspnetdb.dbo.aspnet_Applications AS AA
			ON AU.ApplicationId = AA.ApplicationId
	WHERE LOWER(@UserName) = AU.LoweredUserName
		AND AA.LoweredApplicationName = LOWER('/bregvision')

	--  Insert the GUID and the PracticeID into the User table.
	INSERT INTO
		dbo.[User]
		(
		  AspNet_UserID
		, PracticeID
		, CreatedUserID
		, CreatedDate
		, IsActive
		)
	VALUES
		(
		  @AspNet_UserID
		, @PracticeID
		, @CreatedUserID
		, GETDATE()
		, 1
		)

	SELECT @UserID = @@IDENTITY 
	
	
END


