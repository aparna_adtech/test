﻿
/*
	Center for Orthopediacs #79; Remove bogus MC products(Breg) Supplier/Brand and products (set to inactive).
	John Bongiorni   2008.01.03
*/

CREATE PROC [dbo].[usp_zMaintenance_CenterForOrthopediacs_Remove_Bogus_MC_Products_20080103_MC_Breg]
AS 
BEGIN
	

--  Get practiceID for CenterForOthopediacs.
--  NOTE: BEFORE REUSING CHECK ALL CODE THIS IS FOR USE
--  ONLY WITH A ONE LOCATION Practice!!!!!!

SELECT * FROM PRACTICE WHERE PracticeID = 23
--  Above is Correct?  Yes.

SELECT * 
FROM PracticeLocation AS PL--  Center for Orthopaedics has only one location.
WHERE PL.PracticeID = 23  --  1500 Pleasant Valley Way
	AND PL.PracticeLocationID = 59


--  Get PracticeCatalogSupplierBrandIDs for the bogus Suppliers.
SELECT *
FROM dbo.PracticeCatalogSupplierBrand AS pcsb
WHERE pcSB.PracticeID = 23    --		Practice ID Set HERE!
AND IsThirdPartySupplier = 0
AND MasterCatalogSupplierID IS NOT NULL
AND PracticeCatalogSupplierBrandID IN (239)  --  Set PCSBID HERE for bogus MC supplierbrands.

--  239	23	1	NULL	BREG	BREG	BREG	BREG	0	999	1	2007-12-12 15:54:12.247	NULL	NULL	1

--  DIFFERENT  Select bogus PCproducts of the bogus suppliers using the MCProduct.Code  --  4 records
SELECT PracticeCatalogProductID,
	   PracticeID,
	   PracticeCatalogSupplierBrandID,
	   PCP.MasterCatalogProductID,
	   ThirdPartyProductID,
	   IsThirdPartyProduct,
	   WholesaleCost,
	   BillingCharge,
	   DMEDeposit,
	   BillingChargeCash,
	   Sequence,
	   PCP.CreatedUserID,
	   PCP.CreatedDate,
	   PCP.ModifiedUserID,
	   PCP.ModifiedDate,
	   PCP.IsActive
FROM dbo.PracticeCatalogProduct AS PCP
INNER JOIN dbo.MasterCatalogProduct AS MCP
	ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
WHERE PCP.PracticeID = 23    --		Practice ID Set HERE!
	AND PCP.PracticeCatalogSupplierBrandID IN (239)  --  Set PCSBID HERE for bogus MC supplierbrands.
	AND PCP.IsThirdPartyProduct = 0
	AND MCP.CODE IN 
		('11000', '10112', '10113')

	--  AND PCP.IsActive = 1
ORDER BY
	PCP.PracticeCatalogSupplierBrandID
	, PCP.MasterCatalogProductID
	
--4390	23	239	1407	NULL	0	9.50	23.00	0.00	0.00	NULL	1	2007-12-14 09:22:49.700	NULL	NULL	1
--4391	23	239	1408	NULL	0	9.50	23.00	0.00	0.00	NULL	1	2007-12-14 09:22:49.700	NULL	NULL	1
--4307	23	239	1784	NULL	0	0.00	0.00	0.00	0.00	NULL	1	2007-12-12 15:56:07.670	NULL	NULL	1
	
/*
	Check the SupplierOrderLineItem table 
	and the ProductInventory table
*/

--  Check the SupplierOrderLineItem table for the bogus product inventory.
SELECT *
FROM dbo.SupplierOrderLineItem  -- zero records.
WHERE PracticeCatalogProductID IN
		(
			  4390
			, 4391
			, 4307	
		)

--  NONE from above.

--  Check Inventory Table for the bogus PCProducts.  --  83 records
SELECT * 
FROM dbo.ProductInventory AS PI
WHERE PI.PracticeCatalogProductID IN
		(
			  4390
			, 4391
			, 4307	
		)
--  three FROM ABOVE.
	
select * 
from dbo.ProductInventory AS PI
WHERE  --  AND PI.PracticeCatalogProductID = 4722  --  This is not active. and has no dependencies below
	PI.PracticeCatalogProductID IN
		(
			  4390
			, 4391
			, 4307	
		)

	AND PI.ProductInventoryID IN
		(
			  11331
			, 11364
			, 11365	
		)
--11331	59	4307	1	0	0	0	1	2007-12-17 09:26:27.873	1	2007-12-28 10:06:32.640	1
--11364	59	4390	1	0	0	0	1	2007-12-17 09:27:05.873	1	2007-12-28 10:08:26.577	1
--11365	59	4391	1	0	0	0	1	2007-12-17 09:27:14.373	1	2007-12-28 10:08:26.577	1

-- HARD DELETE due to time constaints!
begin tran

--DELETE from dbo.ProductInventory 
--WHERE PracticeCatalogProductID IN
--		(
--			  4390
--			, 4391
--			, 4307	
--		)  --  This is not active. and has no dependencies below
--AND ProductInventoryID IN
--		(
--			  11331
--			, 11364
--			, 11365	
--		)


select * from dbo.ProductInventory AS PI
WHERE PI.PracticeCatalogProductID IN
		(
			  4390
			, 4391
			, 4307	
		)   --  This is not active. and has no dependencies below
AND PI.ProductInventoryID IN
		(
			  11331
			, 11364
			, 11365	
		)

rollback tran
-- ABOVE WAS DELETED.

--  NONE OF THE FOLLOWING CODE HAS BEEN EXECUTED or used FOR THIS!!!!  2007.01.03

--------/*
--------	Check all ProductInventory tables for the bogus product inventory records.
--------	Create table variable, @BogusProductInventory, to hold the bogus
--------	 ProductInventoryIDs.
--------*/
--------	
--------DECLARE @BogusProductInventory TABLE 
--------	(ProductInventoryID INT)	
--------	
--------INSERT INTO @BogusProductInventory
--------	(ProductInventoryID)
--------SELECT ProductInventoryID
--------FROM dbo.ProductInventory AS PI			--  This will select inventory from ALL LOCATIONS!
--------WHERE PI.PracticeCatalogProductID IN
--------		(
--------			SELECT PracticeCatalogProductID
--------			FROM dbo.PracticeCatalogProduct AS PCP
--------			WHERE PCP.PracticeID = 23								 --	Practice ID Set HERE!
--------				AND PCP.PracticeCatalogSupplierBrandID IN (263, 264)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
--------				AND PCP.IsThirdPartyProduct = 0
--------				AND PCP.IsActive = 1		
--------		)
--------			
--------SELECT DISTINCT ProductInventoryID
--------FROM @BogusProductInventory
--------ORDER BY ProductInventoryID	
--------
----------  NONE ABOVE.
--------		
----------  Check the ProductInventory_OrderCheckIn table for the bogus product inventory.
--------
--------SELECT *
--------FROM dbo.ProductInventory_OrderCheckIn  
--------WHERE ProductInventoryID IN 
--------				(
--------					SELECT DISTINCT ProductInventoryID
--------					FROM @BogusProductInventory
--------				)		
--------		
----------	   (				
----------		SELECT ProductInventoryID
----------		FROM dbo.ProductInventory AS PI
----------		WHERE PI.PracticeCatalogProductID IN
----------				(
----------					SELECT PracticeCatalogProductID
----------					FROM dbo.PracticeCatalogProduct AS PCP
----------					WHERE PCP.PracticeID = 11
----------						AND PCP.PracticeCatalogSupplierBrandID IN (109, 111)
----------						AND PCP.IsThirdPartyProduct = 0
----------						AND PCP.IsActive = 1		
----------				)
----------		)
--------
--------
----------  Check the ProductInventory_DispenseDetail table for the bogus product inventory.
--------
--------SELECT *
--------FROM dbo.ProductInventory_DispenseDetail  
--------WHERE ProductInventoryID IN 		
--------				(
--------					SELECT DISTINCT ProductInventoryID
--------					FROM @BogusProductInventory
--------				)		
--------
--------		
--------
----------  Check the ProductInventory_QuantityOnHandPhysical table for the bogus product inventory.
--------
--------SELECT *
--------FROM dbo.ProductInventory_QuantityOnHandPhysical
--------WHERE ProductInventoryID IN 		
--------				(
--------					SELECT DISTINCT ProductInventoryID
--------					FROM @BogusProductInventory
--------				)		
--------	
----------  Check the ProductInventory_Purge table for the bogus product inventory.
--------
--------SELECT *
--------FROM dbo.ProductInventory_Purge
--------WHERE ProductInventoryID IN 		
--------				(
--------					SELECT DISTINCT ProductInventoryID
--------					FROM @BogusProductInventory
--------				)		
--------
----------  Check the ProductInventory_ManualCheckIn table for the bogus product inventory.
--------
--------SELECT *
--------FROM dbo.ProductInventory_ManualCheckIn
--------WHERE ProductInventoryID IN 		
--------				(
--------					SELECT DISTINCT ProductInventoryID
--------					FROM @BogusProductInventory
--------				)		
--------			
--------/*
--------	Summary:
--------	 2 bogus suppliers for PracticeCatalogSupplierBrandID
--------	 3 bogus records in PracticeCatalogProducts  
--------	 2 bogus records in PracticeCatalogSupplierBrand  
--------*/
--------
--------
--------
--------SELECT *
--------FROM dbo.PracticeCatalogProduct AS PCP
--------WHERE IsActive = 0
--------
--------
-------------------------
----------SELECT * 
----------INTO zDeleted_ProductInventory 
----------FROM dbo.ProductInventory AS PI
----------WHERE PI.PracticeCatalogProductID IN
----------		(
----------			SELECT PracticeCatalogProductID
----------			FROM dbo.PracticeCatalogProduct AS PCP
----------			WHERE PCP.PracticeID = 11
----------				AND PCP.PracticeCatalogSupplierBrandID IN (109, 111)
----------				AND PCP.IsThirdPartyProduct = 0
----------				AND PCP.IsActive = 1		
----------		)
------------ Note: Add DateDeleted column update the 83 rows with GetDate for 
------------			the DateDeleted.
--------
--------
---------- Delete the 83 bogus records from ProductInventory.
----------BEGIN TRAN	
----------	
----------DELETE
----------FROM dbo.ProductInventory 
----------WHERE PracticeCatalogProductID IN
----------		(
----------			SELECT PracticeCatalogProductID
----------			FROM dbo.PracticeCatalogProduct AS PCP
----------			WHERE PCP.PracticeID = 11
----------				AND PCP.PracticeCatalogSupplierBrandID IN (109, 111)
----------				AND PCP.IsThirdPartyProduct = 0
----------				--AND PCP.IsActive = 1		
----------		)
----------		
----------ROLLBACK TRAN
--------		
---------- Note: Add DateDeleted column update the 83 rows with GetDate for 
----------			the DateDeleted.
--------
--------
--------SELECT *
--------FROM PracticeCatalogProduct AS PCP
--------WHERE PCP.IsActive = 0
--------
--------
--------
----------  Update the 3 bogus PCP records by setting their isActive flag to false
--------
--------begIN TRAN
--------
--------		SELECT *
--------	FROM dbo.PracticeCatalogProduct AS PCP
--------	WHERE PCP.PracticeID = 23									--	Practice ID Set HERE!
--------		AND PCP.PracticeCatalogSupplierBrandID IN (263, 264)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
--------		AND PCP.IsThirdPartyProduct = 0
--------		  AND PCP.IsActive = 1
----------4722	23	263	2126	NULL	0	45.00	154.00	0.00	0.00	NULL	1	2007-12-20 11:08:21.077	NULL	NULL	1
----------4990	23	264	8948	NULL	0	23.95	0.00	NULL	0.00	NULL	1	2007-12-21 07:09:07.247	NULL	NULL	1
----------4991	23	264	8949	NULL	0	23.95	0.00	NULL	0.00	NULL	1	2007-12-21 07:09:07.247	NULL	NULL	1
--------
--------	--  21 inactive
--------	SELECT *
--------	FROM dbo.PracticeCatalogProduct AS PCP
--------	WHERE PCP.PracticeID = 23									--	Practice ID Set HERE!
--------		AND PCP.PracticeCatalogSupplierBrandID IN (263, 264)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
--------		AND PCP.IsThirdPartyProduct = 0
--------		  AND PCP.IsActive = 0
--------	ORDER BY
--------		PCP.PracticeCatalogSupplierBrandID
--------		, PCP.MasterCatalogProductID
--------
--------	-- Setting all 3 products to inactive.
----------	UPDATE PracticeCatalogProduct      -- 14 Rows Updated.
----------	SET IsActive = 0
----------	WHERE PracticeID = 23									--	Practice ID Set HERE!
----------		AND PracticeCatalogSupplierBrandID IN (263, 264)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
----------		AND IsThirdPartyProduct = 0
--------
--------
--------	--  Zero are Active, 21 are inactive.
--------	SELECT *
--------	FROM dbo.PracticeCatalogProduct AS PCP
--------	WHERE PCP.PracticeID = 23									--	Practice ID Set HERE!
--------		AND PCP.PracticeCatalogSupplierBrandID IN  (263, 264)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
--------		AND PCP.IsThirdPartyProduct = 0
--------		  AND PCP.IsActive = 1
--------
--------	--  21 inactive
--------	SELECT *
--------	FROM dbo.PracticeCatalogProduct AS PCP
--------	WHERE PCP.PracticeID = 23									--	Practice ID Set HERE!
--------		AND PCP.PracticeCatalogSupplierBrandID IN (263, 264)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
--------		AND PCP.IsThirdPartyProduct = 0
--------		  AND PCP.IsActive = 0
--------	ORDER BY
--------		PCP.PracticeCatalogSupplierBrandID
--------		, PCP.MasterCatalogProductID
--------
--------
--------
----------rOLLBACK TRAN  --COMMIT TRAN  --  
--------
--------
----------  After the PCProducts have been deactivated, deactivate the bogus supplierBrands.
--------
--------
--------SELECT *
--------FROM dbo.PracticeCatalogSupplierBrand AS pcsb
--------WHERE pcSB.PracticeID = 23							--	Practice ID Set HERE!
--------AND IsThirdPartySupplier = 0
--------AND MasterCatalogSupplierID IS NOT NULL
--------AND PracticeCatalogSupplierBrandID IN (263, 264)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
--------AND IsActive = 1
----------263	23	4	NULL	Bauerfeind	Bauerfeind	Bauerfeind	Bauerfeind	0	999	1	2007-12-20 11:08:21.077	NULL	NULL	1
----------264	23	8	NULL	Hely Weber	HW	Hely Weber	HW	0	999	1	2007-12-21 07:09:07.247	NULL	NULL	1
--------
---------- !!!!CANNOT REMOVE the bogus supplierbrands (MC Bauerfiend and MC HW) from THE APPLICATION; CenterForOrthopediacs, Admin, Supplier, Brand, since they are MC Suppliers
----------		Add to not have the ability to be removed.
--------
--------BEGIN TRAN
--------
--------	SELECT *
--------	FROM dbo.PracticeCatalogSupplierBrand AS pcsb
--------	WHERE pcSB.PracticeID = 23							--	Practice ID Set HERE!
--------	AND IsThirdPartySupplier = 0
--------	AND MasterCatalogSupplierID IS NOT NULL
--------	AND PracticeCatalogSupplierBrandID IN (263, 264)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
--------	AND IsActive = 0
--------
--------
--------	-- uPDATE BOGUS set the PCSuppplierBrand.IsActive to 0
----------	UPDATE dbo.PracticeCatalogSupplierBrand 
----------	SET IsActive = 0
----------	WHERE PracticeID = 23							--	Practice ID Set HERE!
----------	AND IsThirdPartySupplier = 0
----------	AND MasterCatalogSupplierID IS NOT NULL
----------	AND PracticeCatalogSupplierBrandID IN (263, 264)  --  Set PCSBID HERE for bogus MC supplierbrands.  
----------	AND IsActive = 1
--------
--------	SELECT *
--------	FROM dbo.PracticeCatalogSupplierBrand AS pcsb
--------	WHERE pcSB.PracticeID = 23							--	Practice ID Set HERE!
--------	AND IsThirdPartySupplier = 0
--------	AND MasterCatalogSupplierID IS NOT NULL
--------	AND PracticeCatalogSupplierBrandID IN (263, 264)  --  Set PCSBID HERE for bogus MC supplierbrands.  --	Practice ID Set HERE!
--------	AND IsActive = 0
--------
--------ROLLBACK TRAN

END