﻿/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Practice_GeneralInfo_Update
   
   Description:  Updates a record in the table Practice
					Updates the PracticeName and the IsBillingCentralized

   AUTHOR:       John Bongiorni 7/13/2007 3:31 PM
   
   Modifications:  
   ------------------------------------------------------------ */

CREATE PROCEDURE [dbo].[usp_Practice_GeneralInfo_Update]
(
	  @PracticeID                    INT
	, @PracticeName                  VARCHAR(50)
	, @IsBillingCentralized          BIT
--	, @IsShoppingCentralized         BIT
	, @ModifiedUserID                INT
)
AS
BEGIN
	DECLARE @Err INT

	UPDATE dbo.Practice
	SET
		  PracticeName = @PracticeName
		, IsBillingCentralized = @IsBillingCentralized
--		, IsShoppingCentralized = @IsShoppingCentralized
		, ModifiedUserID = @ModifiedUserID
		, ModifiedDate = GETDATE()

	WHERE 
		PracticeID = @PracticeID

	SET @Err = @@ERROR

	RETURN @Err
End


