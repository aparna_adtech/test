﻿-- =============================================
-- Author:		Greg Ross
-- Create date: 09/05/2007
-- Description:	Gets all data from the masterCatalogProduct Tables
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetPracticeCatalogProductInfo]

	@PracticeID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   select 
	PCP.PracticecatalogProductID,
	PCP.MasterCatalogProductID,	
	PCSB.BrandShortName,
	MCP.ShortName,
	MCP.Code,
	PCP.WholesaleCost,
	PCP.BillingCharge,
	PCP.DMEDeposit,
	PCP.BillingChargeCash,
	PCP.StockingUnits,
	PCP.BuyingUnits

 from PracticeCatalogProduct PCP
inner join PracticeCatalogSupplierBrand PCSB 
	on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
inner join MasterCatalogProduct MCP 
	on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
where PCP.practiceID=@PracticeID
	and PCP.isactive=1
	and PCSB.isactive = 1
	and MCP.isactive=1
	
END

