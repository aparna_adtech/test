﻿
-- =============================================
-- Author:		<Author: Mike Sneen>
-- Create date: <Create Date: 02/28/09>
-- Description:	<Description: Used to insert into ShoppingCartCustomBrace>
-- modification:
--  [dbo].[usp_CreateCustomBraceOrderNew]  

-- =============================================
Create PROCEDURE [dbo].[usp_CreateCustomBraceOrderNew_20101221]  --3 --10  --  _With_Brands
   @ShoppingCartID int,
   @Billing_CustomerNumber varchar(50),
   @Billing_Phone varchar(50),
   @Billing_Attention varchar(50),
   @Billing_Address1 varchar(50),
   @Billing_Address2 varchar(50),
   @Billing_City varchar(50),
   @Billing_State varchar(50),
   @Billing_Zip varchar(50),
   @Shipping_Attention varchar(50),
   @Shipping_Address1 varchar(50),
   @Shipping_Address2 varchar(50),
   @Shipping_City varchar(50),
   @Shipping_State varchar(50),
   @Shipping_Zip varchar(50),
   @PatientName varchar(50),
   @PhysicianID int,
   @Patient_Age int,
   @Patient_Weight int,
   @Patient_Height varchar(50),
   @Patient_Sex varchar(8),
   @BraceFor varchar(50),
   @Instability varchar(50),
   @BilatCastReform varchar(50),
   @ThighCircumference varchar(50),
   @CalfCircumference varchar(50),
   @KneeOffset varchar(50),
   @KneeWidth varchar(50),
   @Extension varchar(50),
   @Flexion varchar(50),
   @MeasurementsTakenBy varchar(150),
   @X2K_Counterforce varchar(50),
   @X2K_Counterforce_degrees varchar(50),
   @X2K_FramePadColor1 varchar(50),
   @X2K_FramePadColor2 varchar(50),
   @Fusion_Enhancement varchar(50),
   @Fusion_Notes varchar(1000),
   @Fusion_Color varchar(50),
   @Fusion_Pantone varchar(50),
   @Fusion_Pattern varchar(50),
   @Fusion_Pattern_Notes varchar(250),
   @ShippingTypeID int,
   @ShippingCarrierID int
AS 
    BEGIN
    DECLARE @Err INT
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	       SET NOCOUNT ON ;
	       
declare @RecordCount int

SELECT  @RecordCount = COUNT(1)
                FROM    dbo.[ShoppingCartCustomBrace]
                WHERE   [ShoppingCartID] = @ShoppingCartID

if @RecordCount = 0 
Begin    
INSERT INTO [ShoppingCartCustomBrace]
           ([ShoppingCartID]
           ,[Billing_CustomerNumber]
           ,[Billing_Phone]
           ,[Billing_Attention]
           ,[Billing_Address1]
           ,[Billing_Address2]
           ,[Billing_City]
           ,[Billing_State]
           ,[Billing_Zip]
           ,[Shipping_Attention]
           ,[Shipping_Address1]
           ,[Shipping_Address2]
           ,[Shipping_City]
           ,[Shipping_State]
           ,[Shipping_Zip]
           ,[PatientName]
           ,[PhysicianID]
           ,[Patient_Age]
           ,[Patient_Weight]
           ,[Patient_Height]
           ,[Patient_Sex]
           ,[BraceFor]
           ,[Instability]
           ,[BilatCastReform]
           ,[ThighCircumference]
           ,[CalfCircumference]
           ,[KneeOffset]
           ,[KneeWidth]
           ,[Extension]
           ,[Flexion]
           ,[MeasurementsTakenBy]
           ,[X2K_Counterforce]
           ,[X2K_Counterforce_degrees]
           ,[X2K_FramePadColor1]
           ,[X2K_FramePadColor2]
           ,[Fusion_Enhancement]
           ,[Fusion_Notes]
           ,[Fusion_Color]
           ,[Fusion_Pantone]
           ,[Fusion_Pattern]
           ,[Fusion_Pattern_Notes]
           ,[ShippingTypeID]
           ,[ShippingCarrierID])
     VALUES
           (
           @ShoppingCartID,
		   @Billing_CustomerNumber,
		   @Billing_Phone,
		   @Billing_Attention,
		   @Billing_Address1,
		   @Billing_Address2,
		   @Billing_City,
		   @Billing_State,
		   @Billing_Zip,
		   @Shipping_Attention,
		   @Shipping_Address1,
		   @Shipping_Address2,
		   @Shipping_City,
		   @Shipping_State,
		   @Shipping_Zip,
		   @PatientName,
		   @PhysicianID,
		   @Patient_Age,
		   @Patient_Weight,
		   @Patient_Height,
		   @Patient_Sex,
		   @BraceFor,
		   @Instability,
		   @BilatCastReform,
		   @ThighCircumference,
		   @CalfCircumference,
		   @KneeOffset,
		   @KneeWidth,
		   @Extension,
		   @Flexion,
		   @MeasurementsTakenBy,
		   @X2K_Counterforce,
		   @X2K_Counterforce_degrees,
		   @X2K_FramePadColor1,
		   @X2K_FramePadColor2,
		   @Fusion_Enhancement,
		   @Fusion_Notes,
		   @Fusion_Color,
		   @Fusion_Pantone,
		   @Fusion_Pattern,
		   @Fusion_Pattern_Notes,
		   @ShippingTypeID,
		   @ShippingCarrierID
		   )
END     
ELSE
Begin
	Update [ShoppingCartCustomBrace]
	SET
		
       [Billing_CustomerNumber] = @Billing_CustomerNumber
       ,[Billing_Phone] = @Billing_Phone
       ,[Billing_Attention] = @Billing_Attention
       ,[Billing_Address1] = @Billing_Address1
       ,[Billing_Address2] = @Billing_Address2
       ,[Billing_City] = @Billing_City
       ,[Billing_State] = @Billing_State
       ,[Billing_Zip] = @Billing_Zip
       ,[Shipping_Attention] = @Shipping_Attention
       ,[Shipping_Address1]  = @Shipping_Address1
       ,[Shipping_Address2]  = @Shipping_Address2
       ,[Shipping_City]  = @Shipping_City
       ,[Shipping_State]  = @Shipping_State
       ,[Shipping_Zip]  = @Shipping_Zip
       ,[PatientName]  = @PatientName
       ,[PhysicianID]  = @PhysicianID
       ,[Patient_Age]  = @Patient_Age
       ,[Patient_Weight]  = @Patient_Weight
       ,[Patient_Height]  = @Patient_Height
       ,[Patient_Sex]  = @Patient_Sex
       ,[BraceFor]  = @BraceFor
       ,[Instability]  = @Instability
       ,[BilatCastReform]  = @BilatCastReform
       ,[ThighCircumference]  = @ThighCircumference
       ,[CalfCircumference]  = @CalfCircumference
       ,[KneeOffset]  = @KneeOffset
       ,[KneeWidth]  = @KneeWidth
       ,[Extension]  = @Extension
       ,[Flexion]  = @Flexion
       ,[MeasurementsTakenBy] = @MeasurementsTakenBy
       ,[X2K_Counterforce]  = @X2K_Counterforce
       ,[X2K_Counterforce_degrees] = @X2K_Counterforce_degrees
       ,[X2K_FramePadColor1]  = @X2K_FramePadColor1
       ,[X2K_FramePadColor2]  = @X2K_FramePadColor2
       ,[Fusion_Enhancement]  = @Fusion_Enhancement
       ,[Fusion_Notes]  = @Fusion_Notes
       ,[Fusion_Color]  = @Fusion_Color
       ,[Fusion_Pantone] = @Fusion_pantone
       ,[Fusion_Pattern]  = @Fusion_Pattern
       ,[Fusion_Pattern_Notes]  = @Fusion_Pattern_Notes
       ,[ShippingTypeID]  = @ShippingTypeID
       ,[ShippingCarrierID] = @ShippingCarrierID
	WHERE
		[ShoppingCartID] = @ShoppingCartID
End
     	SET @Err = @@ERROR

	RETURN @Err

    END

--Exec usp_SuperbillCategory_Insert 3, 3