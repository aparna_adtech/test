﻿
CREATE PROCEDURE [dbo].[usp_Report_ProductsDispensed_Physician_Select_All_Names_By_PracticeLocationIDString] 
			@PracticeLocationID VARCHAR(2000)
AS
BEGIN 

	DECLARE @PracticeLocationIDString VARCHAR(2000)
	SET @PracticeLocationIDString = @PracticeLocationID

--SELECT * FROM PracticelOCATION WHERE PRACTICEID = 18
--select * from Practice WHERE PRACTICEID = 18


--DECLARE @Output VARCHAR(500)
--SET @Output = ''
--
--SELECT @Output =
--	CASE WHEN @Output = '' THEN CAST(PH.PhysicianID AS VARCHAR(50))
--		 ELSE @Output + ', ' + CAST(PH.PhysicianID AS VARCHAR(50))
--		 END
-- FROM  
--  dbo.Contact AS C
-- INNER JOIN 
--  dbo.Physician AS PH
--   ON C.ContactID = PH.ContactID
-- 
-- INNER JOIN 
--  dbo.PracticeLocation_Physician AS PLP  
--   ON PH.PhysicianID = PLP.PhysicianID
-- WHERE
--	PLP.PracticeLocationID IN 
--			(SELECT ID 
--			 FROM dbo.udf_ParseArrayToTable('13, 15, 16, 17, 18, 19, 20, 22, 23, 24', ',')
--			)   --(@PracticeLocationIDString, ','))
--	AND PH.IsActive = 1
--	AND C.IsActive = 1
---- ORDER BY
----	RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix)
----WHERE PracticeID = 6
--
--SELeCT @OUTPUT as oUTPUT

-- '34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 58, 59, 60, 61, 62, 63, 64, 65, 66'  
--, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 58, 59, 60, 61, 62, 63, 64, 65, 66, 
--  34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 58, 59, 60, 61, 62, 63, 64, 65, 66, 
--  34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 58, 59, 60, 61, 62, 63, 



--	DECLARE @PracticeLocationIDString VARCHAR(2000)
--	SET @PracticeLocationIDString = '13, 15, 16, 17, 18, 19, 20, 22, 23, 24'

	SELECT 
		SUB.PhysicianID
		, SUB.ContactID
		, SUB.PhysicianName
	FROM
		(
			SELECT DISTINCT 
				PH.PhysicianID
				, C.ContactID
				, RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix) AS PhysicianName
			FROM  
				dbo.Contact AS C
				INNER JOIN 
				dbo.Physician AS PH
					ON C.ContactID = PH.ContactID 
			INNER JOIN 
				dbo.PracticeLocation_Physician AS PLP  
					ON PH.PhysicianID = PLP.PhysicianID
			WHERE
				PLP.PracticeLocationID IN 
					(SELECT ID 
					 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))
				AND PH.IsActive = 1
				AND C.IsActive = 1
		--	 ORDER BY
		--		RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix)

--		UNION
--			SELECT 
--				  -999					AS PhysicianID
--				, -999					AS ContactID
--				, '<blank>'				AS PhysicianName

		) AS SUB
	ORDER By
		SUB.PhysicianName


END

