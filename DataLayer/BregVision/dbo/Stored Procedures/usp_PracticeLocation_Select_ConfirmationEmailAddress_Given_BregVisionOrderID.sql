﻿--  [dbo].[usp_PracticeLocation_Select_ConfirmationEmailAddress_Given_BregVisionOrderID]
--  John Bongiorni
--  2007.11.13

--  Get the PracticeLocation's ConfirmationEmailAddress Given the BregVisionOrderID

CREATE PROC [dbo].[usp_PracticeLocation_Select_ConfirmationEmailAddress_Given_BregVisionOrderID]
		  @BregVisionOrderID INT
		, @EmailForOrderConfirmation VARCHAR(200) OUTPUT
AS
BEGIN

	SELECT @EmailForOrderConfirmation = EmailForOrderConfirmation
	FROM dbo.BregVisionOrder AS BVO WITH(NOLOCK)
	INNER JOIN dbo.PracticeLocation AS PL WITH(NOLOCK)
		ON BVO.PracticeLocationID = PL.PracticeLocationID
	WHERE BVO.BregVisionOrderID = @BregVisionOrderID
	
END
