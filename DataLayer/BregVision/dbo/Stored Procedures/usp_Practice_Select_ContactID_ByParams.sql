﻿/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Practice_Select_ContactID_ByParams
   
   Description:  Selects a ContactID from table dbo.Practice
   				   and puts value into a parameter.
   
   AUTHOR:       John Bongiorni 7/14/2007 7:56:05 PM
   
   Modifications:  
   ------------------------------------------------------------ */  ---------------------select params-----------

CREATE PROCEDURE [dbo].[usp_Practice_Select_ContactID_ByParams]
				
(
      @PracticeID                    INT
    , @ContactID                     INT            OUTPUT
)
AS
BEGIN
	DECLARE @Err INT

	SELECT
          @PracticeID = PracticeID
        , @ContactID = ContactID
	FROM dbo.Practice
	WHERE 
		PracticeID = @PracticeID
		AND IsActive = 1

	SET @Err = @@ERROR

	RETURN @Err
End

