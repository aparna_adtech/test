﻿

CREATE PROCEDURE dbo.usp_zImport_PracticeCatalogSupplierBrand_From_3rdPartySupplier_MillerOrtho
(
      @PracticeCatalogSupplierBrandID INT = NULL OUTPUT
    , @PracticeID                    INT
    , @MasterCatalogSupplierID       INT = NULL
    , @ThirdPartySupplierID          INT = NULL
    , @SupplierName                  VARCHAR(50)
    , @SupplierShortName             VARCHAR(50)
    , @BrandName                     VARCHAR(50)
    , @BrandShortName                VARCHAR(50)
    , @IsThirdPartySupplier          BIT
    , @Sequence                      INT = NULL
    , @CreatedUserID                 INT
)
AS
BEGIN
	DECLARE @Err INT

	INSERT
	INTO dbo.PracticeCatalogSupplierBrand
	(
          PracticeID
        , MasterCatalogSupplierID
        , ThirdPartySupplierID
        , SupplierName
        , SupplierShortName
        , BrandName
        , BrandShortName
        , IsThirdPartySupplier
        , Sequence
        , CreatedUserID
        , IsActive
	)
	VALUES
	(
          @PracticeID
        , @MasterCatalogSupplierID
        , @ThirdPartySupplierID
        , @SupplierName
        , @SupplierShortName
        , @BrandName
        , @BrandShortName
        , @IsThirdPartySupplier
        , @Sequence
        , @CreatedUserID
        , 1
	)

	SET @Err = @@ERROR
	SELECT @PracticeCatalogSupplierBrandID = SCOPE_IDENTITY()

	RETURN @Err
END
