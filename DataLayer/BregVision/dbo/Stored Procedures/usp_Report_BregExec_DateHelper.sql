﻿CREATE PROC usp_Report_BregExec_DateHelper
AS
BEGIN

	DECLARE @StartYear AS INT 
	DECLARE @EndYear AS INT 

	SET @StartYear = 2006 
	SET @EndYear = 2015 ;


	WITH                   Years
			  AS ( SELECT   YYYY = @StartYear
				   UNION ALL
				   SELECT   YYYY + 1
				   FROM     Years
				   Where    YYYY < @EndYear
				 ) ,
			Months
			  AS ( SELECT   MM = 1
				   UNION ALL
				   SELECT   MM + 1
				   FROM     Months
				   WHERE    MM < 12
				 ) 


	--SELECT * FROM Years
	--SELECT * FROM Months, Years
	SELECT PracticeName, YYYY, MM FROM PRactice Cross join Years, Months
	ORDER BY PracticeName, YYYY, MM

END