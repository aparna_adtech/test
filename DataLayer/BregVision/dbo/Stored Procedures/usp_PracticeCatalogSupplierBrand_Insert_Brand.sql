﻿

/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeCatalogSupplierBrand_Insert_Brand
   
   Description:  Inserts a new brand into the table PracticeCatalogSupplierBrand
   
   
   AUTHOR:       John Bongiorni 10/5/2007 1:10:02 AM
   
   Modifications:  20071017 John Bongiorni
					Use the ThirdPartySupplierID to pull the 
					SupplierName and the SupplierShortName.
   ------------------------------------------------------------ */  ------------------ INSERT ------------------
   
CREATE PROCEDURE [dbo].[usp_PracticeCatalogSupplierBrand_Insert_Brand]
(
      @PracticeCatalogSupplierBrandID INT = NULL OUTPUT
    , @PracticeID                    INT
--    , @MasterCatalogSupplierID       INT = NULL
    , @ThirdPartySupplierID          INT 
    , @SupplierName                  VARCHAR(50)
    , @SupplierShortName             VARCHAR(50)
    , @BrandName                     VARCHAR(50)
    , @BrandShortName                VARCHAR(50)
--    , @IsThirdPartySupplier          BIT
    , @Sequence                      INT = 999
    , @CreatedUserID                 INT
    
    

)
AS
BEGIN
	DECLARE @Err INT
	
	
	    -- Disregard the SupplierName and the SupplierShortName coming in as this
    --	may be erroneous due to updates of the supplier name and short name.
    
    SELECT 
		  @SupplierName =       SupplierName                  
		, @SupplierShortName =  SupplierShortName             
    FROM ThirdPartySupplier WITH( NOLOCK )
    WHERE ThirdPartySupplierID = @ThirdPartySupplierID
    
    

	INSERT
	INTO dbo.PracticeCatalogSupplierBrand
	(
          PracticeID
        , MasterCatalogSupplierID
        , ThirdPartySupplierID
        , SupplierName
        , SupplierShortName
        , BrandName
        , BrandShortName
        , IsThirdPartySupplier
        , Sequence
        , CreatedUserID
        , IsActive
	)
	VALUES
	(
          @PracticeID
        , NULL
        , @ThirdPartySupplierID
        , @SupplierName
        , @SupplierShortName
        , @BrandName
        , @BrandShortName
        , 1
        , @Sequence
        , @CreatedUserID
        , 1
	)

	SET @Err = @@ERROR
	SELECT @PracticeCatalogSupplierBrandID = SCOPE_IDENTITY()

	RETURN @Err
END