﻿
/*

Get all active User Full Names 
from the Contact tabel via the User table given a practiceID
Note the "user name" is the contact first name plus 
the contact last name plus the contact suffix.

John Bongiorni 09/24/2007 15:21:12 *****


*/


CREATE PROCEDURE [dbo].[usp_User_Select_All_FullNames_ByPracticeID]
		@PracticeID INT
AS
BEGIN	
	SELECT
		Pr.PracticeID 
	  , U.UserID
	  , C.ContactID
--	  , C.Salutation
--	  , C.FirstName
--	  , C.MiddleName
--	  , C.LastName
--	  , C.Suffix
	  , RTRIM( C.FirstName + ' ' + C.LastName + ' ' +	C.Suffix) AS UserFullName

	FROM
		dbo.Contact AS C
		INNER JOIN dbo.[User] AS U
			ON C.ContactID = U.ContactID
		INNER JOIN dbo.Practice AS Pr
			ON U.PracticeID = Pr.PracticeID
	WHERE
			Pr.PracticeID = @PracticeID
		AND U.IsActive = 1
		AND C.IsActive = 1
END

