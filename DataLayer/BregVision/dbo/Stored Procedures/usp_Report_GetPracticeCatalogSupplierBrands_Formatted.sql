﻿
/*
-- =============================================
-- Author:		Greg Ross	
-- Create date: 09/27/07
-- Description:	Used to get the parent detail for a practice. Used in MCToPC.aspx
-- =============================================
--  Modification:  2007.11.27  JB  
--		Proc below does not make any sense and 
--		is being commented out.
--      Add PCSB.IsActive = 1
--		Add SupplierName
--		--  2007.12.12 JB Order by IsThirdPartySupplier, SupplierName, BrandName.
--		2007.12.15  JB  Formatted for Report DropDown
*/
CREATE PROCEDURE [dbo].[usp_Report_GetPracticeCatalogSupplierBrands_Formatted] 
	@PracticeID int
AS
BEGIN
-- select * from PracticeCatalogSupplierBrand order by practiceID
		SELECT DISTINCT 

				  PCSB.PracticeCatalogSupplierBrandID

				, CASE WHEN PCSB.SupplierShortName = PCSB.BrandShortName THEN PCSB.SupplierShortName
					ELSE PCSB.SupplierShortName + ' (' + PCSB.BrandShortName + ')'
					END										AS SupplierBrand

				, PCSB.SupplierShortName					AS SupplierName
				, PCSB.BrandShortName						AS BrandName
				, PCSB.IsThirdPartySupplier					AS IsThirdPartySupplier
				, CASE WHEN (PCSB.IsThirdPartySupplier = 0) 
						THEN ISNULL(PCSB.Sequence, 999) 
						ELSE (ISNULL(PCSB.Sequence, 999) + 1000) 
						END									AS DerivedSequence -- JB
        FROM 
			PracticeCatalogSupplierBrand AS PCSB WITH (NOLOCK)
		WHERE 
			PCSB.PracticeID = @PracticeID
			--AND PCSB.IsActive = 1				
		ORDER BY 
			  CASE WHEN (PCSB.IsThirdPartySupplier = 0) 
						THEN ISNULL(PCSB.Sequence, 999) 
						ELSE ISNULL(PCSB.Sequence, 999) + 1000
						END  
			, PCSB.SupplierShortName
			, PCSB.BrandShortName 

--  OLD PROC CODE IS BELOW  -- JB commented out on 2007.11.27 
--	-- SET NOCOUNT ON added to prevent extra result sets from
--	-- interfering with SELECT statements.
--	SET NOCOUNT ON;
--
--
--     SELECT     DISTINCT 
--				PCSB.PracticeCatalogSupplierBrandID,
--				PCSB.BrandShortName as BrandName
--				--MCS.[SupplierName] AS Brand,
--                --MCS.[MasterCatalogSupplierID] AS SupplierID,
--				--c.[FirstName] AS Name,
--                --C.[LastName],
--                --c.[PhoneWork],
--                --A.[AddressLine1] AS Address,
--                --A.City AS City,
--                --A.[ZipCode] AS Zipcode
--				--MCC.[Name] AS Category
--        FROM   PracticeCatalogProduct PCP
--                --INNER JOIN address A ON MCS.[AddressID] = A.[AddressID]
--                --INNER JOIN [Contact] C ON mcs.[ContactID] = c.[ContactID]
--				--INNER JOIN [MasterCatalogCategory] MCC ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
--				--INNER JOIN [MasterCatalogSubCategory] MCSC ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
--				--INNER JOIN [MasterCatalogProduct] MCP ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
--				--INNER Join PracticeCatalogSupplierBrand PCSB on MCS.MasterCatalogSupplierID = PCSB.MasterCatalogSupplierID
--				inner join PracticeCatalogSupplierBrand PCSB on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
--				where PCP.PracticeID = @PracticeID
--				order by PCSB.BrandShortName 

END





