﻿CREATE PROC usp_zMaintenance_MCProducts_Modify_LeftRightSide
AS
BEGIN

BEGIN TRANSACTION

SELECT * 
FROM MasterCatalogProduct 
WHERE Code LIKE '450%'
	AND Code = '450LT'
	AND LeftRightSide = 'RT'
	
SELECT * 
FROM MasterCatalogProduct 
WHERE Code LIKE '450%'
	AND Code = '450RT'
	AND LeftRightSide = 'LT'

	
UPDATE MasterCatalogProduct 
SET LeftRightSide = 'LT'
WHERE Code LIKE '450%'
	AND Code = '450LT'
	AND LeftRightSide = 'RT'
	
	
UPDATE MasterCatalogProduct 
SET LeftRightSide = 'RT'
WHERE Code LIKE '450%'
	AND Code = '450RT'
	AND LeftRightSide = 'LT'

SELECT * 
FROM MasterCatalogProduct 
WHERE Code LIKE '450%'
--	AND Code = '450LT'
--	AND LeftRightSide = 'RT'
--	
--SELECT * 
--FROM MasterCatalogProduct 
--WHERE Code LIKE '450%'
--	AND Code = '450RT'
--	AND LeftRightSide = 'LT'


ROLLBACK TRANSACTION	
-- COMMIT TRANSACTION	
END