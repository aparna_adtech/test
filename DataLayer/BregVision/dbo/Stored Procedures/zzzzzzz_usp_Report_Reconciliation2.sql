﻿
CREATE PROC [dbo].[zzzzzzz_usp_Report_Reconciliation2]
AS
BEGIN

	DECLARE @PracticeID INT
	SET @PracticeID = 6

	SELECT 
			 SUB.PracticeName
			, SUB.PurchaseOrder
			, SUB.PracticeLocation

			, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
				ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
				END					AS SupplierBrand

	--		, SUB.SupplierShortName	AS Supplier
	--
	--		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
	--		ELSE SUB.BrandShortName
	--		END						AS Brand
	--		
	--		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN NULL
	--		ELSE SUB.BrandShortName
	--		END AS BrandShortName
	--		
	--		
	--		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN 1
	--		ELSE 0
	--		END AS IsBrandSuppressed


			, SUB.ProductShortName			AS Product
			, SUB.Side + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender
			, SUB.Code

			, SUB.QuantityOrdered		
			, SUB.ActualWholesaleCost
			, SUB.LineTotal										AS QuantityOrderedCost	

			, SUB.QuantityCheckedIn		
			, (SUB.QuantityCheckedIn * SUB.ActualWholesaleCost) AS QuantityCheckInCost

			, SUB.QuantityToCheckIn		
			, (SUB.QuantityToCheckIn * SUB.ActualWholesaleCost) AS QuantityToCheckInCost
			
			, SUB.OrderedDate
			, SUB.Sequence	

	FROM

		(SELECT 
			   P.PracticeID
			, P.PracticeName
			, SO.PurchaseOrder		
    		, PL.PracticeLocationID	
			, PL.NAME AS PracticeLocation

	--		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
	--			ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
	--			END					AS SupplierBrand

			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) AS Sequence

			, PCP.PracticeCatalogProductID
			, MCP.[ShortName] AS ProductShortName
			, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS Side
			, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender		
			, ISNULL(MCP.Code, '') AS Code

			, SOLI.ActualWholesaleCost
			, SOLI.QuantityOrdered
			, SOLI.LineTotal	

			, ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityCheckedIn
			, CASE WHEN ( SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) ) < 0 THEN 0
				ELSE  SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) 
				END									AS QuantityToCheckIn					--  FUDGED due to bad checkin data.
		--	, SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0)  AS QuantityToCheckIn  --Correct


			, CONVERT(VARCHAR(10), SOLI.CreatedDate, 101)  AS OrderedDate

		FROM SupplierOrderLineItem SOLI  WITH (NOLOCK)

		INNER JOIN SupplierOrder AS SO  WITH (NOLOCK)
			ON SOLI.SupplierOrderID = SO.SupplierOrderID

		INNER JOIN BregVisionOrder AS BVO  WITH (NOLOCK)
			ON SO.BregVisionOrderID = BVO.BregVisionOrderID

		INNER JOIN PracticeLocation AS PL  WITH (NOLOCK)
			ON BVO.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P  WITH (NOLOCK)
			ON P.PracticeID = PL.PracticeID

		INNER JOIN PracticeCatalogPRoduct PCP  WITH (NOLOCK)
			on SOLI.PracticeCatalogProductID = 	PCP.PracticeCatalogPRoductID


		INNER JOIN MasterCatalogProduct MCP  WITH (NOLOCK)
			on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID


		INNER JOIN PracticeCatalogSupplierBrand AS PCSB  WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

		LEFT join ProductInventory PI  WITH (NOLOCK)
			on SOLI.PracticeCatalogProductID = PI.PracticeCatalogProductID
			AND PI.PracticeLocationID = PL.PracticeLocationID

		LEFT JOIN (SELECT SupplierOrderLineItemID, SUM(Quantity) AS QuantityCheckedIn
					FROM dbo.ProductInventory_OrderCheckIn 
					WHERE IsActive = 1
					GROUP BY SupplierOrderLineItemID)AS SubPIOCI
						ON SOLI.SupplierOrderLineItemID = SubPIOCI.SupplierOrderLineItemID

		WHERE 
			P.PracticeID = @PracticeID
			--  AND PI.PracticeLocationID = @PracticeLocationID

			AND P.IsActive = 1
			AND PL.IsActive = 1
			AND BVO.IsActive = 1
			AND SO.IsActive = 1
			AND SOLI.IsActive = 1
			AND PCP.isactive = 1
			AND PCSB.isactive = 1
			AND PI.isactive = 1
			AND MCP.isactive = 1

	UNION


	--DECLARE @PracticeID INT
	--SET @PracticeID = 6
		SELECT 
				   P.PracticeID
				, P.PracticeName
				, SO.PurchaseOrder		
    			, PL.PracticeLocationID	
				, PL.NAME AS PracticeLocation


	--			, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
	--				ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
	--				END					AS SupplierBrand

				, PCSB.SupplierShortName
				, PCSB.BrandShortName
				, ISNULL(PCSB.Sequence, 999) + 1000 AS Sequence

				, PCP.PracticeCatalogProductID
				, TPP.[ShortName] AS ProductShortName
				, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS Side
				, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )			AS Size
				, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender		
				, ISNULL(TPP.Code, '') AS Code
			 
				, SOLI.ActualWholesaleCost
				, SOLI.QuantityOrdered
				, SOLI.LineTotal	

				, ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityCheckedIn
				, CASE WHEN ( SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) ) < 0 THEN 0
					ELSE  SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) 
					END									AS QuantityToCheckIn
			--	, SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityToCheckIn


			, CONVERT(VARCHAR(10), SOLI.CreatedDate, 101)  AS OrderedDate



		
		FROM SupplierOrderLineItem SOLI  WITH (NOLOCK)

		INNER JOIN SupplierOrder AS SO  WITH (NOLOCK)
			ON SOLI.SupplierOrderID = SO.SupplierOrderID

		INNER JOIN BregVisionOrder AS BVO  WITH (NOLOCK)
			ON SO.BregVisionOrderID = BVO.BregVisionOrderID

		INNER JOIN PracticeLocation AS PL  WITH (NOLOCK)
			ON BVO.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P  WITH (NOLOCK)
			ON P.PracticeID = PL.PracticeID

		INNER JOIN PracticeCatalogPRoduct PCP  WITH (NOLOCK)
			on SOLI.PracticeCatalogProductID = 	PCP.PracticeCatalogPRoductID


		INNER JOIN ThirdPartyProduct TPP  WITH (NOLOCK)
			on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID


		INNER JOIN PracticeCatalogSupplierBrand AS PCSB  WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

		LEFT JOIN ProductInventory PI  WITH (NOLOCK)
			on SOLI.PracticeCatalogProductID = PI.PracticeCatalogProductID
			AND PI.PracticeLocationID = PL.PracticeLocationID

		LEFT JOIN (SELECT SupplierOrderLineItemID, SUM(Quantity) AS QuantityCheckedIn
					FROM dbo.ProductInventory_OrderCheckIn 
					WHERE IsActive = 1
					GROUP BY SupplierOrderLineItemID)AS SubPIOCI
						ON SOLI.SupplierOrderLineItemID = SubPIOCI.SupplierOrderLineItemID





		WHERE 
			P.PracticeID = @PracticeID
			--  AND PI.PracticeLocationID = @PracticeLocationID

			AND P.IsActive = 1
			AND PL.IsActive = 1
			AND BVO.IsActive = 1
			AND SO.IsActive = 1
			AND SOLI.IsActive = 1
			AND PCP.isactive = 1
			AND PCSB.isactive = 1
			AND PI.isactive = 1
			and TPP.isactive = 1

		) AS SUB


	ORDER BY
			 SUB.PracticeName
			, SUB.PurchaseOrder
			, SUB.PracticeLocation
			, SUB.Sequence	

			, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
				ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
				END					--AS SupplierBrand


			, SUB.ProductShortName
			, SUB.Code							 
			, SUB.Side + ', ' + SUB.Size  + ', ' + SUB.Gender --AS SideSizeGender


END