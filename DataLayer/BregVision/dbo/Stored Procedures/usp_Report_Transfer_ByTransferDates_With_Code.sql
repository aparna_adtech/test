﻿/*
	Created for ad hoc report 01/29/2015 - RLD
		May be useful to satisfy VOC request
*/


/*
declare @startdate datetime
declare @enddate datetime
set @startdate = dateadd(m, -1, GetDate())
set @enddate = GetDate()
exec usp_Report_Transfer_ByTransferDates @startdate, @enddate
*/
CREATE PROCEDURE [dbo].[usp_Report_Transfer_ByTransferDates_With_Code]
      @PracticeID INT
    , @PracticeLocationID	VARCHAR(2000)
	, @StartDate DATETIME
	, @EndDate DATETIME

AS
BEGIN



DECLARE @PracticeLocationIDString VARCHAR(2000)

SET @PracticeLocationIDString = @PracticeLocationID
	/****** Script for SelectTopNRows command from SSMS  ******/
	SELECT 
		   P.PracticeName
		  ,PIT.[TransferDate]
		  ,PL_From.Name AS PracticeLocationFromName
		  ,PL_To.Name AS PracticeLocationToName
		  ,COALESCE(MCP_From.Code, TPP_From.Code)  AS ProductCode
		  ,COALESCE(MCP_From.Name, TPP_From.Name, 'Name not found')  AS ProductName
		  ,COALESCE(MCP_From.Size, TPP_From.Size, 'Size not found') AS Size
		  ,COALESCE(MCP_From.LeftRightSide, TPP_From.LeftRightSide, 'Side not found') AS Side
		  ,COALESCE(MCP_From.Gender, TPP_From.Gender, 'Gender not found') AS Gender
		  ,PIT.[Quantity]      
		  ,PIT.[ActualWholesaleCost]
		  ,PIT.[Comments]
		  ,PIT.[CreatedUserID]
		  ,PIT.[CreatedDate]

	  --Product Joins    
	  FROM [dbo].[ProductInventory_Transfer] PIT 
	  INNER JOIN [ProductInventory] AS PI_From  WITH(NOLOCK)
								ON PIT.[ProductInventoryID_TransferFrom] = PI_From.[ProductInventoryID]
	  LEFT OUTER JOIN [PracticeCatalogProduct] AS PCP_From WITH(NOLOCK) 
							ON PI_From.PracticeCatalogProductID = PCP_From.PracticeCatalogProductID	
	  LEFT OUTER JOIN [MasterCatalogProduct] AS MCP_From  WITH(NOLOCK)
								ON PCP_From.[MasterCatalogProductID] = MCP_From.[MasterCatalogProductID] AND PCP_From.IsThirdPartyProduct=0
	  LEFT OUTER JOIN [ThirdPartyProduct] AS TPP_From  WITH(NOLOCK)
								ON PCP_From.[ThirdPartyProductID] = TPP_From.[ThirdPartyProductID] AND PCP_From.IsThirdPartyProduct=1							
	--END Product Joins

	--From Practice Location
	  LEFT OUTER JOIN PracticeLocation PL_From 
		ON PIT.PracticeLocationID_TransferFrom = PL_From.PracticeLocationID
	--END From Practice Location

	--To Practice Location
	  LEFT OUTER JOIN PracticeLocation PL_To
		ON PIT.PracticeLocationID_TransferTo = PL_To.PracticeLocationID
	--END To Practice Location
	
	--Practice
	  LEFT OUTER JOIN Practice p 
		ON p.PracticeID = PL_To.PracticeID
	--END Practice	

	WHERE
	PIT.IsActive = 1
	AND PIT.practiceID = @PracticeID
	AND PIT.TransferDate >= @StartDate
	-- ditch the time component of PIT.TransferDate when comparing against @EndDate -hchan 11/24/2010
	AND CAST(FLOOR(CAST(PIT.TransferDate AS FLOAT)) AS DATETIME) <= @EndDate

	AND 
	PIT.PracticeLocationID_TransferFrom IN 
	(SELECT ID 
	 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))
	
	ORDER BY 
	PIT.TransferDate,
	PL_From.Name,
	PL_To.Name,
	ProductName	

END
