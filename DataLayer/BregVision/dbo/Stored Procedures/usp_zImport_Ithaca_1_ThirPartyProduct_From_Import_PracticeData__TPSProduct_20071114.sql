﻿
CREATE proc [dbo].[usp_zImport_Ithaca_1_ThirPartyProduct_From_Import_PracticeData__TPSProduct_20071114]
as
begin

INSERT INTO ThirdPartyProduct
	( PracticeCatalogSupplierBrandID
	, Code
	, Name
	, ShortName

	, Packaging
	, LeftRightSide
	, Size
	, Color
	, Gender

	, IsDiscontinued
	, WholesaleListCost
	, Description

	, CreatedUserID
	, CreatedDate
	, ModifiedUserID
	, ModifiedDate
	, IsActive
	)

Select 
	  PracticeCatalogSupplierBrandID
	, ProductCode
	, ProductName
	, ProductShortName

	, Packaging
	, LeftRightSide
	, Size
	, ISNULL(Color, '') AS Color
	, ISNULL(Gender, '') as Gender

	, 0 AS IsDiscontinued
	, WholesaleListCost
	, ISNULL(Description, '') as Description

	, -13			AS CreatedUserID
	, GetDate()		AS CreatedDate
	, -13			AS ModifiedUserID
	, GetDate()		AS ModifiedDate
	, 1				AS IsActive

from Import_PracticeData.dbo.Ithaca_3rdProducts_20071114

end