﻿--  1. Get the MasterCatalogSupplierID given a MasterCatalogProductID
--		This is a nested proc called by
--			dbo.usp_MasterCatalogSupplier_Select_MCSupplierID_By_MCProductID	
-- 
--	TEST: 
--		DECLARE	@return_value int,
--				@MasterCatalogSupplierID int
--				
--		EXEC	@return_value = [dbo].[usp_MasterCatalogSupplier_Select_MCSupplierID_By_MCProductID]
--				@MasterCatalogProductID = 24,
--				@MasterCatalogSupplierID = @MasterCatalogSupplierID OUTPUT
--		SELECT	@MasterCatalogSupplierID as N'@MasterCatalogSupplierID'
--		SELECT	'Return Value' = @return_value



CREATE PROC [dbo].[usp_MasterCatalogSupplier_Select_MCSupplierID_By_MCProductID]
		(
			  @MasterCatalogProductID	INT				--	24
			, @MasterCatalogSupplierID   INT	OUTPUT		--  1
		)			
AS
BEGIN
		SELECT @MasterCatalogSupplierID = mcs.MasterCatalogSupplierID
		FROM dbo.MasterCatalogSupplier AS mcs 
		INNER JOIN dbo.MasterCatalogCategory AS mcc 
			ON mcs.MasterCatalogSupplierID = mcc.MasterCatalogSupplierID
		INNER JOIN dbo.MasterCatalogSubCategory AS mcsc
			ON mcc.MasterCatalogCategoryID = mcsc.MasterCatalogCategoryID 
		INNER JOIN dbo.MasterCatalogProduct AS mcp 
			ON mcsc.MasterCatalogSubCategoryID = mcp.MasterCatalogSubCategoryID 	
		WHERE MasterCatalogProductID = @MasterCatalogProductID	
END



--SELECT *
--FROM MasterCatalogProduct
