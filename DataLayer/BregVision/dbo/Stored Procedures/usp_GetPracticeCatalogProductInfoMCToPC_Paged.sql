﻿


-- =============================================
-- Author:		Greg Ross
-- Create date: 09/27/07
-- Description:	Used to get the detail for a Practice. Used in the MCToPC.aspx page
-- Modification: 20071008 2033 JB Add Third Party and code to make this generic
--  (FOR MCProducts and ThirdPartyProducts)
--				20071009 1203 JB Add WITH(NOLOCK) cLAUSES to Tables.
--			2007.12.12 JB Order By ProductShortName, Code
--			2008.01.23 JB Add LeftRightSide, Size and SideSize

-- Exec usp_GetPracticeCatalogProductInfoMCToPC_Paged 1, 1, 'Browse', '', 15, 1
-- =============================================
Create PROCEDURE [dbo].[usp_GetPracticeCatalogProductInfoMCToPC_Paged] 

	@PracticeID int,
	@PracticeCatalogSupplierBrandID int,
	@SearchCriteria varchar(50),
	@SearchText  varchar(50),
	@PageSize int,
	@PageNumber int


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
		
	DECLARE @Ignore int
	DECLARE @LastID int
	Set @LastID = 0
	Set @Ignore = 0


	/*These variables are used in conjunction with the coalesce statements in the where clause.
	each of these variables will be null except one is @SearchCriteria matches.*/
	Declare @ProductName varchar(50)			--NULL   These default to null
	Declare @ProductCode varchar(50)			--NULL   These default to null
	Declare @ProductBrand varchar(50)			--NULL   These default to null
	Declare @ProductHCPCs varchar(50)			--NULL   These default to null

	if @SearchCriteria='Name'					-- ProductName
		Set @ProductName = '%' + @SearchText + '%'		--put the percent sign on both sides of the search text for product name
	else if @SearchCriteria='Code'
		Set @ProductCode = '%' +  @SearchText + '%'	
	else if @SearchCriteria='Brand'
		Set @ProductBrand = @SearchText + '%'	
	else if @SearchCriteria='HCPCs'
		Set @ProductHCPCs = @SearchText + '%'


IF @PageNumber > 1
  BEGIN 
        /* For pages > 1 compute how many records to ignore, 
        set ROWCOUNT and SELECT ID into @LastID */
        SET @Ignore = @PageSize * (@PageNumber - 1) --pn2 ign 15
        SET ROWCOUNT @Ignore 
        SELECT @LastID = vwPC.PracticecatalogProductID 
               FROM vwPracticeCatalog vwPC 
               WHERE
    vwPC.PracticeID = @PracticeID
    AND           
	vwPC.PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID
	AND
	(
		--ProductName || ShortName
		COALESCE(vwPC.ProductName, '') like COALESCE(@ProductName, vwPC.ProductName, '')
		OR
		COALESCE(vwPC.ShortName, '') like COALESCE(@ProductName, vwPC.ShortName, '')
	)
	AND
	(
		COALESCE(vwPC.Code, '') like COALESCE(@ProductCode , vwPC.Code, '')
	)
	AND
	(
		COALESCE(vwPC.BrandName, '') like COALESCE(@ProductBrand, vwPC.BrandName, '')
		OR
		COALESCE(vwPC.BrandShortName, '') like COALESCE(@ProductBrand, vwPC.BrandShortName, '')
	)
	AND (   /* use this type of statement for Child tables of the target to avoid duplicate rows.
				If we use a join for this, we would return duplicate rows, so we just check the existence
				of one record that matches the search parameters in the child table
				*/
				(
					 (@ProductHCPCs IS NULL)
					 OR EXISTS
					 (  
						Select 
							1 
						 from 
							ProductHCPCS PHCPCS 
						 where 
							PHCPCS.PracticeCatalogProductID = vwPC.PracticeCatalogProductID 
						    and PHCPCS.HCPCS like @ProductHCPCs  
					  )
				 )
			)
               ORDER BY vwPC.PracticecatalogProductID DESC
  END
ELSE
  BEGIN
        /* For page #1 just set rowcount to pagesize */ 
        SET ROWCOUNT @PageSize
        Set @LastID = 9999999
  END
 
/* Set rowcount to @PageSize and 
SELECT page for output (note the WHERE clause) */
SET ROWCOUNT @PageSize 


select
	PracticeID, 
	PracticecatalogProductID,
	MasterCatalogProductID,  -- Needs to be called ProductID (to handle MC and ThirdParty)
	PracticeCatalogSupplierBrandID,
	ProductName,
	ShortName,
	BrandName,
	BrandShortName,
	Code,
	SideSize,
	LeftRightSide,
	Size,
	Gender,
	WholesaleCost,
	BillingCharge,
	DMEDeposit,
	BillingChargeCash,
	StockingUnits,
	BuyingUnits,
	IsThirdPartyProduct,
	HCPCSString, -- JB 20071004 Added Column for UDF Concat of HCPCS
	IsDeletable   --  JB  20080225  Added Column for "Deletable" PCPs
												
	
FROM vwPracticeCatalog vwRetPC

WHERE
	vwRetPC.PracticeID = @PracticeID
	AND
	vwRetPC.PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID
	AND
	(
		--ProductName || ShortName
		COALESCE(vwRetPC.ProductName, '') like COALESCE(@ProductName, vwRetPC.ProductName, '')
		OR
		COALESCE(vwRetPC.ShortName, '') like COALESCE(@ProductName, vwRetPC.ShortName, '')
	)
	AND
	(
		COALESCE(vwRetPC.Code, '') like COALESCE(@ProductCode , vwRetPC.Code, '')
	)
	AND
	(
		COALESCE(vwRetPC.BrandName, '') like COALESCE(@ProductBrand, vwRetPC.BrandName, '')
		OR
		COALESCE(vwRetPC.BrandShortName, '') like COALESCE(@ProductBrand, vwRetPC.BrandShortName, '')
	)
	AND (   /* use this type of statement for Child tables of the target to avoid duplicate rows.
				If we use a join for this, we would return duplicate rows, so we just check the existence
				of one record that matches the search parameters in the child table
				*/
				(
					 (@ProductHCPCs IS NULL)
					 OR EXISTS
					 (  
						Select 
							1 
						 from 
							ProductHCPCS PHCPCS 
						 where 
							PHCPCS.PracticeCatalogProductID = vwRetPC.PracticeCatalogProductID 
						    and PHCPCS.HCPCS like @ProductHCPCs  
					  )
				 )
			)
	AND
		(
		vwRetPC.PracticecatalogProductID  < @LastID
             
             )

ORDER BY
	vwRetPC.PracticecatalogProductID DESC 
	--UPPER(RTRIM(LTRIM(ShortName))) -- jb 
	--, UPPER(RTRIM(LTRIM(Code)))		-- jb


SET ROWCOUNT 0


END





