﻿-- =============================================
--EXEC USP_GetPracticeAndPracticeLocation2 'MikesTestUser2'
--EXEC USP_GetPracticeAndPracticeLocation2 'adminortho'
-- Author:		<Author:Greg Ross>
-- Create date: <Create Date:07/18/07>
-- Description:	<Description:Used to get the practice and practice location information. 
-- Will be used in the master pages >
-- Modification:  JB 20070829 use aspnetdb.dbo.aspnet_Users
--                JB 20080111 Add IsActive clauses in WHERE clause.
--				  MWS 20090406 Added UserDefaultLocation AND AllowAccess
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetPracticeAndPracticeLocation2] --'adminortho'
	-- Add the parameters for the stored procedure here
	@UserName VARCHAR(50)
AS
BEGIN

	SELECT  distinct  
		  P.PracticeID
		, P.PracticeName
		, PL.Name
		, PL.IsPrimaryLocation
		, PL.PracticeLocationID
		, UserDefaultLocation = Cast((Case When U.DefaultLocationID <> 0 Then
									CASE When (PL.PracticeLocationID = U.DefaultLocationID AND PL.IsActive= 1) THEN 1 ELSE 0 END
								ELSE	
									PL.IsPrimaryLocation END) as Bit)
									
		  , AllowAccess= Cast((ISNULL((Select AllowAccess 
						 from User_PracticeLocation_Permissions UPP  WITH(NOLOCK)
						 where UPP.UserID = U.UserID 
							   AND UPP.PracticeLocationID=PL.PracticeLocationID 
							   AND UPP.IsActive = 1 )
						, 1)) as Bit)
		  , P.IsCustomFitEnabled AS PracticeIsCustomFitEnabled
	
	FROM aspnetdb.dbo.aspnet_Users AS AU 
	INNER JOIN dbo.[User] AS U
		ON AU.UserId = U.AspNet_UserID
	INNER JOIN Practice AS P
		ON U.PracticeID = P.PracticeID	
	INNER JOIN PracticeLocation AS PL
		ON P.PracticeID = PL.PracticeID

	WHERE AU.LoweredUserName = LOWER(@UserName)
	and PL.isactive = 1
	AND P.IsActive = 1  --  JB  2008.01.11
	AND U.IsActive = 1  --  JB  2008.01.11

END
