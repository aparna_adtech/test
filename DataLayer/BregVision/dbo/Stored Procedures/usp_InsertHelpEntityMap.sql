﻿
-- =============================================
-- Author:		<Author: Tom Thomas>
-- Create date: <Create Date: 03/27/10>
-- Description:	<Description: Insert HelpEntityMapping for existing entity
-- =============================================
CREATE PROCEDURE [dbo].[usp_InsertHelpEntityMap]  
	@Id int OUT
	,@EntityId int
	,@ParentId int
	,@EntityTypeId int
	,@SortOrder int = 0
		
AS 
    BEGIN
	
	SET NOCOUNT ON;
	
		
	insert into HelpEntityTree ( ParentId,EntityId,EntityTypeId,SortOrder )  values (@ParentId,@EntityId,@EntityTypeId,@SortOrder)
	Set @Id = @@IDENTITY
	
	END
    
    






