﻿
--  Step 2 for AOS
--  AOS's PracticeID is 7

CREATE PROC [dbo].[usp_zImport_PCSportMed_2_PracticeCatalogProduct_Insert_From_ThirdPartyProduct_20071112] --10
	@PracticeID INT

as
begin


INSERT INTO PracticeCatalogProduct
	( PracticeID
	, PracticeCatalogSupplierBrandID
	, ThirdPartyProductID
	, IsThirdPartyProduct
	, WholesaleCost
	, CreatedUserID
	, IsActive
	)

SELECT
		PCSB.PracticeID
	, PCSB.PracticeCatalogSupplierBrandID
	, TPS.ThirdPartyProductID
	, 1 AS IsThirdPartyProduct
	, TPS.WholesaleListCost AS WholesaleCost
	, TPS.CreatedUserID
	, TPS.IsActive  -- Should be true to work.
FROM ThirdPartyProduct AS TPS
	INNER JOIN PracticeCatalogSupplierBrand AS PCSB
		ON TPS.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
	INNER JOIN Practice AS P
		ON P.PracticeID = PCSB.PracticeID
	WHERE P.PracticeID = 10 --@PracticeID
	AND TPS.ThirdPartyProductID NOT IN
		(SELECT ThirdPartyProductID 
		 FROM PracticeCatalogProduct 
		 WHERE IsActive = 1 
			AND IsThirdPartyProduct = 1) 

end

--select * from practice
--
--select * from ThirdPartyProduct as tpp
--	--inner join PracticeCatalogSupplierBrand AS PCSB
--	--	ON TPP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
--where tpp.ThirdPartyProductID in ( 871, 870, 869, 868)
--
--
--select * from PracticeCatalogSupplierBrand where practiceID = 10 and isactive = 1 
--
--select * 
--FROM ThirdPartyProduct AS TPS
--	INNER JOIN PracticeCatalogSupplierBrand AS PCSB
--		ON TPS.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
--	INNER JOIN Practice AS P
--		ON P.PracticeID = PCSB.PracticeID
--where TPS. ThirdPartyProductID in ( 871, 870, 869, 868)
--


