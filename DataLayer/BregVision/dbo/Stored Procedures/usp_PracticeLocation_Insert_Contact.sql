﻿
/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocation_Insert_Contact
   
   Description:  Inserts a record into table Contact
			and the ContactID into the table PracticeLocation.
   
   AUTHOR:       John Bongiorni 8/03/2007 10:42 PM
   
   Modifications:  
   ------------------------------------------------------------ */  
   
CREATE PROCEDURE [dbo].[usp_PracticeLocation_Insert_Contact]
(
	  @PracticeLocationID			 INT
    , @ContactID                     INT = NULL OUTPUT
    , @Title                         VARCHAR(50) = NULL
    , @Salutation                    VARCHAR(10) = NULL
    , @FirstName                     VARCHAR(50)
    , @MiddleName                    VARCHAR(50) = NULL
    , @LastName                      VARCHAR(50)
    , @Suffix                        VARCHAR(10) = NULL
    , @EmailAddress                  VARCHAR(100) = NULL
    , @PhoneWork                     VARCHAR(25) = NULL
    , @PhoneWorkExtension            VARCHAR(10) = NULL
    , @PhoneCell                     VARCHAR(25) = NULL
    , @PhoneHome                     VARCHAR(25) = NULL
    , @Fax                           VARCHAR(25) = NULL
    , @CreatedUserID                 INT
    , @IsSpecificContact             BIT		    OUTPUT
)
AS
BEGIN
	DECLARE @Err INT

	INSERT
	INTO dbo.Contact
	(
          Title
        , Salutation
        , FirstName
        , MiddleName
        , LastName
        , Suffix
        , EmailAddress
        , PhoneWork
        , PhoneWorkExtension
        , PhoneCell
        , PhoneHome
        , Fax
		, IsSpecificContact
        , CreatedUserID
        , IsActive
	)
	VALUES
	(
          @Title
        , @Salutation
        , @FirstName
        , @MiddleName
        , @LastName
        , @Suffix
        , @EmailAddress
        , @PhoneWork
        , @PhoneWorkExtension
        , @PhoneCell
        , @PhoneHome
        , @Fax
		, @IsSpecificContact
        , @CreatedUserID
        , 1
	)
--  Add Error Handling
--	SET @Err = @@ERROR
	SELECT @ContactID = SCOPE_IDENTITY()

--  Add Error Handling

	UPDATE 
		dbo.PracticeLocation
	SET 
		ContactID = @ContactID 
	WHERE 
		PracticeLocationID = @PracticeLocationID


	RETURN @Err
END

