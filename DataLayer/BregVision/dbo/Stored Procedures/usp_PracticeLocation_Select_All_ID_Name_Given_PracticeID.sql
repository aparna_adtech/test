﻿CREATE PROC [dbo].[usp_PracticeLocation_Select_All_ID_Name_Given_PracticeID]
		@PracticeID	INT
AS
BEGIN

	SELECT 
	    PL.PracticeLocationID
	  , PL.NAME AS PracticeLocationName
	  , PL.IsPrimaryLocation 
	FROM dbo.PracticeLocation AS PL
	WHERE 
		PL.PracticeID = @PracticeID	
		AND PL.IsActive = 1
	ORDER BY
		PL.NAME
	
		
END