﻿

--  Adhoc query to include IsHandheld to product dispensed report data or Marcy.  Arizona bone and joint.

CREATE PROC [dbo].[usp_Report_ProductsDispensed_With_IsHandheld_Adhoc_RLD]  
		  @PracticeID	INT
		, @PracticeLocationID	VARCHAR(2000)
		, @StartDate		DATETIME
		, @EndDate			DATETIME
AS
BEGIN


--DECLARE @Output VARCHAR(50)
--SET @Output = ''
--
--SELECT @Output =
--	CASE WHEN @Output = '' THEN CAST(PracticeLocationID AS VARCHAR(50))
--		 ELSE @Output + ', ' + CAST(PracticeLocationID AS VARCHAR(50))
--		 END
--FROM PracticeLocation
--WHERE PracticeID = 6
--
--SELECT @Output AS Output



--	DECLARE @PracticeID INT
--	SET @PracticeID = 6

DECLARE @PracticeLocationIDString VARCHAR(2000)


--  SET @EndDate to 11:59:59 PM
SELECT @EndDate = DATEADD(DAY, 1, @EndDate)


SET @PracticeLocationIDString = @PracticeLocationID

--DECLARE @PhysicianIDString VARCHAR(2000)
--SET @PhysicianIDString = @PhysicianID
---  Insert select here ----

SELECT 
		  SUB.PracticeName		AS Practice
		, SUB.PracticeLocation	AS Location

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
			ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
			END					AS SupplierBrand


		, SUB.SupplierShortName	AS Supplier

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
		ELSE SUB.BrandShortName
		END						AS Brand

--		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN 1
--		ELSE 0
--		END AS IsBrandSuppressed
--		, SUB.IsThirdPartySupplier
		, SUB.Sequence	

		, SUB.ProductName			AS Product

		, SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender

		, SUB.Code

		, dbo.ConcatHCPCS(SUB.PracticeCatalogProductID) AS HCPCS -- JB 20071004 Added Column for UDF Concat of HCPCS
	
		
		, SUB.Physician
		, SUB.PatientCode			
		, CONVERT(VARCHAR(10), SUB.DateDispensed, 101) AS DateDispensed		

		, SUB.Quantity 		--, SUM( SUB.Quantity )

--		, CAST( SUB.ActualWholesaleCost AS DECIMAL(15,2) )	AS ActualWholesaleCost
--		, CAST( SUB.ActualChargeBilled AS DECIMAL(15,2) )	AS ActualChargeBilled
--		, CAST( SUB.DMEDeposit AS DECIMAL(15,2) )			AS DMEDeposit

		, CAST(  SUB.ActualWholesaleCostLineTotal  AS DECIMAL(15,2) )	AS ActualWholesaleCostLineTotal
		, CAST(  SUB.DMEDepositLineTotal  AS DECIMAL(15,2) )			AS DMEDepositLineTotal
		, CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )    AS ActualChargeBilledLineTotal
		
--		, SUB.Comment  -- Not Displayed for Version 1.
		, SUB.CreatedWithHandheld
FROM


		(
		SELECT 

			  P.PracticeName
			, PL.Name AS PracticeLocation
			, ISNULL(RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix), '') AS Physician

			, D.PatientCode
			
			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) AS Sequence

			, PCP.PracticeCatalogProductID

			, MCP.[Name] AS ProductName

			, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
			
			, ISNULL(MCP.Code, '') AS Code

			, DD.CreatedDate AS DateDispensed

			, DD.Quantity
			, PI_DD.ActualWholesaleCost 
			, DD.ActualChargeBilled
			, DD.DMEDeposit	

			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit * DD.Quantity AS DMEDepositLineTotal
			, PI_DD.ActualWholesaleCost * DD.Quantity AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.
			, D.CreatedWithHandheld
		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOIN DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID 

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
		INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
			ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID		    --  Can be inactive

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1

		WHERE 
			    P.PracticeID = @PracticeID		
			AND P.IsActive = 1
			AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			AND PCP.IsActive = 1
			AND PCSB.IsActive = 1
			AND MCP.IsActive = 1
		
		AND 
		PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))
		


--		AND 
--		Ph.PhysicianID IN 
--			(SELECT ID 
--			 FROM dbo.udf_ParseArrayToTable(@PhysicianIDString, ','))
	
		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM
			
		UNION

		SELECT 

			 P.PracticeName
			, PL.Name AS PracticeLocation
			, C.FirstName + ' ' + C.LastName AS Physician  --  Get proper line for this!!

			, D.PatientCode
			
			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) + 1000 AS Sequence

			, PCP.PracticeCatalogProductID

			, TPP.[Name] AS ProductName

			, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender


			, ISNULL(TPP.Code, '') AS Code

			, DD.CreatedDate AS DateDispensed

			, DD.Quantity
			, PI_DD.ActualWholesaleCost 
			, DD.ActualChargeBilled
			, DD.DMEDeposit	

			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit * DD.Quantity AS DMEDepositLineTotal
			, PI_DD.ActualWholesaleCost * DD.Quantity AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.
			, D.CreatedWithHandheld
		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOIN DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID 

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
		INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
			ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID			--  Can be inactive

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1	
	
		WHERE 
			P.PracticeID = @PracticeID		
			AND P.IsActive = 1
			AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			AND PCP.IsActive = 1
			AND PCSB.IsActive = 1
			AND TPP.IsActive = 1

		AND 
		PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))
	
		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM	

		) AS Sub

----
------GROUP BY
------		SUB.PracticeName
------		, SUB.PracticeLocation
------		, SUB.Physician
------		, SUB.IsThirdPartySupplier
------		, SUB.Sequence
------		, SUB.SupplierShortName
------		, SUB.BrandShortName
------		
------		, SUB.ProductName
------		, SUB.Side
------		, SUB.Size
------		, SUB.Gender
------		, SUB.Code
------		
------		, SUB.PatientCode
------
------		, SUB.DateDispensed
------
------		, SUB.ActualWholesaleCost
------		, SUB.ActualChargeBilled
------		, Sub.DMEDeposit
------		, Sub.Comment
----
----		
	ORDER BY
		SUB.PracticeName
		, SUB.PracticeLocation

--		, SUB.Physician

		, SUB.Sequence		--  Note 3rd Party has 100 added to it sequence for this query.
--		, SUB.IsThirdPartySupplier
		, SUB.SupplierShortName
		, SUB.BrandShortName

		, SUB.ProductName
		, SUB.Code

		, SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender

END

