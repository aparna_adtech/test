﻿CREATE PROC usp_zzz_PracticeLocationName_SELECT_ONE_OR_ALL_Given_PracticeID_AND_PracticeLocationID
		@PracticeID		    INT
		, @PracticeLocationID INT
AS 
BEGIN
		
----  Get the PracticeID
--DECLARE @PracticeID INT
--SET @PracticeID = 6

--DECLARE @PracticeLocationID INT
--SET @PracticeLocationID = 3

--  @PracticeLocationIDTable to stored the PracticeLocationIDs to search
DECLARE @PracticeLocationIDTable TABLE
	(PracticeLocationID INT)
	
-- Check if PracticeLocationID is 0 -- That is ALL for the practice.
--  If 0 then get ALL PracticeLocationID into the @PracticeLocationIDTable
IF @PracticeLocationID = 0
	BEGIN
		
		INSERT INTO @PracticeLocationIDTable 
			(PracticeLocationID) 
		SELECT 
			PracticeLocationID
		FROM dbo.PracticeLocation 
		WHERE PracticeID = @PracticeID
		AND IsActive = 1	
	END	
ELSE
	BEGIN
		INSERT INTO @PracticeLocationIDTable 
		(PracticeLocationID) 
		SELECT @PracticeLocationID
		
	END

SELECT PracticeLocationID 
		 FROM @PracticeLocationIDTable

SELECT NAME
FROM PracticeLocation
WHERE PracticeLocationID IN 
		(SELECT PracticeLocationID 
		 FROM @PracticeLocationIDTable)


END		