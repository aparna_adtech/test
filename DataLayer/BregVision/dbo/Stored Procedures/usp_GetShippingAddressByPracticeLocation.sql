﻿-- =============================================
-- Author:		Greg Ross
-- Create date: 08/27/07
-- Description:	Gets Shipping Address Info for Practice Location

-- Exec usp_GetShippingAddressByPracticeLocation 3
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetShippingAddressByPracticeLocation]
	-- Add the parameters for the stored procedure here
	@PracticeLocationID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
select 
	 COALESCE(SCCB.Shipping_Attention, SASupp.AttentionOf, '') as ShipAttentionOf, 
	 COALESCE(SCCB.Shipping_Address1, SA.AddressLine1, '') as ShipAddress1,
	 COALESCE(SCCB.Shipping_Address2, SA.AddressLine2, '') as ShipAddress2,
	 COALESCE(SCCB.Shipping_City, SA.City, '') as ShipCity,
	 COALESCE(SCCB.Shipping_State, SA.[State], '') as ShipState,
	 COALESCE(SCCB.Shipping_Zip, SA.ZipCode, '') as ShipZipCode,
	 SASupp.BregOracleID,
	 SASupp.BregOracleIDInvalid
 from PracticeLocationShippingAddress SASupp
inner join Address SA on SASupp.AddressID = SA.AddressID
Left Outer Join ShoppingCart SC
		On SASupp.PracticeLocationID = SC.PracticeLocationID
			AND SC.IsActive = 1
			AND SC.[ShoppingCartStatusID] = 0 --pending
	Left Outer Join ShoppingCartCustomBrace SCCB 
		ON SCCB.ShoppingCartID=SC.ShoppingCartID
		
where SASUPP.PracticeLocationID = @PracticeLocationID
and SASUPP.isactive=1
and SA.isactive=1
END
