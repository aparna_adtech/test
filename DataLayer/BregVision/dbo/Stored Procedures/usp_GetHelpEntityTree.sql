﻿
-- =============================================
-- Author:		<Author: Tom Thomas>
-- Create date: <Create Date: 03/05/10>
-- Description:	<Description: Obtain the HelpEntityTree for a given Id and the level to recurse
-- =============================================
Create PROCEDURE [dbo].[usp_GetHelpEntityTree]  
	@Id int
	,@Depth int = 1000
	
AS 
    BEGIN
	
	SET NOCOUNT ON;
    
    WITH Tree (Id, ParentId, EntityId, EntityTypeId, SortOrder, [Level], [TopParentId]) AS
	(      
		SELECT
			Id,
			ParentId,
			EntityId,
			EntityTypeId,
			SortOrder,
			0 as 'Level',
			Id as 'TopParentId'
		FROM dbo.HelpEntityTree   
		UNION ALL       
		SELECT 
			t.Id,
			t.ParentId,
			t.EntityId,
			t.EntityTypeId,
			t.SortOrder,
			[Level] + 1 as 'Level',
			[TopParentId]
			FROM dbo.HelpEntityTree t          
				INNER JOIN Tree ON t.ParentId = Tree.Id
	)

	SELECT 
		t.*
		, g.Title as 'GroupTitle'
		, a.Title as 'ArticleTitle'
		, a.Description as 'ArticleDescription'
		, m.Title as 'MediaTitle'
		, m.Description as 'MediaDescription'
		, m.Url as 'MediaUrl'
		, ra.Title as 'RelatedArticleTitle'
		, ra.Description as 'RelatedArticleDescription'
		, rm.Title as 'RelatedMediaTitle'
		, rm.Description as 'RelatedMediaDescription'
		, rm.Url as 'RelatedMediaUrl'
	FROM Tree t
	LEFT OUTER JOIN dbo.HelpGroups g ON t.EntityId = g.Id and t.EntityTypeId = g.HelpEntityTypeId
	LEFT OUTER JOIN dbo.HelpArticles a ON t.EntityId = a.Id and t.EntityTypeId = a.HelpEntityTypeId
	LEFT OUTER JOIN dbo.HelpMedia m ON t.EntityId = m.Id and t.EntityTypeId = m.HelpEntityTypeId
	LEFT OUTER JOIN dbo.HelpArticles ra ON t.EntityId = ra.Id and t.EntityTypeId = 3
	LEFT OUTER JOIN dbo.HelpMedia rm ON t.EntityId = rm.Id and t.EntityTypeId = 5
	WHERE [TopParentId] = @Id 
	And [Level] < @Depth
	ORDER BY [Level], ParentId, EntityTypeId, SortOrder

    END

-- exec [usp_GetHelpEntityTree] 1




