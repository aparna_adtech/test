﻿CREATE PROC usp_zzzProductsPurchased_By_SOLineItem
AS
BEGIN

	--  SELECT * FROM Practice
	--  SELECT * FROM Products ordered 
	--  for given practice
	--	  location
	--    supplier
	--    brand

	--    by date

	DECLARE @PracticeID INT
	SET @PracticeID = 6

SELECT 
		 SUB.PracticeName
		, SUB.PracticeLocation
--		, SUB.IsThirdPartySupplier
		, SUB.SupplierShortName
		, SUB.BrandShortName
		, SUB.Sequence

		, SUB.ProductName
		, SUB.Side
		, SUB.Size
		, SUB.Gender
		, SUB.Code
		
		, SUB.ActualWholesaleCost
		, SUB.QuantityOrdered
		, SUB.LineTotal
		
		, SUB.OrderDate
FROM
	(SELECT 
		  P.PracticeName
		, PL.NAME AS PracticeLocation
		, PCSB.IsThirdPartySupplier
		, PCSB.SupplierShortName
		, PCSB.BrandShortName
		, PCSB.Sequence

		, MCP.[Name] AS ProductName
		, MCP.LeftRightSide AS Side
		, MCP.Size AS Size
		, MCP.Gender AS Gender
		, MCP.Code AS Code
		
		, SOLI.ActualWholesaleCost
		, SOLI.QuantityOrdered
		, SOLI.LineTotal

		, SOLI.CreatedDate AS OrderDate		
		
	FROM dbo.SupplierOrderLineItem AS SOLI
	INNER JOIN dbo.SupplierOrder AS SO
		ON SOLI.SupplierOrderID = SO.SupplierOrderID
	INNER JOIN dbo.BregVisionOrder AS BVO
		ON BVO.BregVisionOrderID = SO.BregVisionOrderID
	INNER JOIN dbo.PracticeLocation AS PL
		ON BVO.PracticeLocationID = PL.PracticeLocationID
	INNER JOIN dbo.Practice AS P
		ON PL.PracticeID = P.PracticeID
	INNER JOIN dbo.PracticeCatalogProduct AS pcp
		ON SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
	INNER JOIN dbo.MasterCatalogProduct AS MCP
		ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
		
		
	UNION
	
		SELECT 
		  P.PracticeName
		, PL.NAME AS PracticeLocation
		, PCSB.IsThirdPartySupplier
		, PCSB.SupplierShortName
		, PCSB.BrandShortName
		, PCSB.Sequence
		
		, TPP.[Name] AS ProductName
		, TPP.LeftRightSide AS Side
		, TPP.Size AS Size
		, TPP.Gender AS Gender
		, TPP.Code AS Code
		
		, SOLI.ActualWholesaleCost
		, SOLI.QuantityOrdered
		, SOLI.LineTotal
		
		, SOLI.CreatedDate AS OrderDate
				
		
	FROM dbo.SupplierOrderLineItem AS SOLI
	INNER JOIN dbo.SupplierOrder AS SO
		ON SOLI.SupplierOrderID = SO.SupplierOrderID
	INNER JOIN dbo.BregVisionOrder AS BVO
		ON BVO.BregVisionOrderID = SO.BregVisionOrderID
	INNER JOIN dbo.PracticeLocation AS PL
		ON BVO.PracticeLocationID = PL.PracticeLocationID
	INNER JOIN dbo.Practice AS P
		ON PL.PracticeID = P.PracticeID
	INNER JOIN dbo.PracticeCatalogProduct AS pcp
		ON SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
	INNER JOIN dbo.ThirdPartyProduct AS TPP
		ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
	WHERE P.PracticeID = @PracticeID	
	)		
		AS SUB	
		
		
	
	ORDER BY
		SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.IsThirdPartySupplier
		, SUB.Sequence
		, SUB.SupplierShortName
		, SUB.BrandShortName
		, SUB.ProductName
		, SUB.Code
	
END	

