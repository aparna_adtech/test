﻿

CREATE PROCEDURE [dbo].[usp_PracticeLocation_Fitter_Select_All_Names_By_PracticeLocationID]
  @PracticeLocationID INT
AS
BEGIN 

SELECT
	  CL.ClinicianID
	   , C.ContactID

	-- , C.Salutation
	-- , C.FirstName
	-- , C.MiddleName
	-- , C.LastName
	-- , C.Suffix

	, RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix) AS ClinicianName
	, CL.SortOrder
	, CL.Pin
	 
	 FROM  
	  dbo.Contact AS C
	 INNER JOIN 
	  dbo.Clinician AS CL
	   ON C.ContactID = CL.ContactID
	 
	 INNER JOIN 
	  dbo.PracticeLocation_Physician AS PLP  
	   ON CL.ClinicianID = PLP.PhysicianID
	 WHERE
	  PLP.PracticeLocationID = @PracticeLocationID
	  AND CL.IsActive = 1
	  AND C.IsActive = 1
	  AND PLP.IsActive = 1
	  AND CL.IsFitter = 1 

	  
	  Order by CL.SortOrder
END
