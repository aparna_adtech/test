﻿CREATE PROC usp_Reports_ImportPrep_Select_MC_Subcategories_for_New_Products
				
AS 
BEGIN

	SELECT 
		  MCS.MasterCatalogSupplierID
		, MCS.SupplierName
		, MCC.MasterCatalogCategoryID
		, MCC.NAME AS CategoryName
		, MCSC.MasterCatalogSubcategoryID
		, MCSC.NAME AS SubCategoryName
	FROM 
		dbo.MasterCatalogSupplier AS MCS WITH (NOLOCK)
	INNER JOIN 
		dbo.MasterCatalogCategory AS MCC WITH (NOLOCK)
			ON MCS.MasterCatalogSupplierID = MCC.MasterCatalogSupplierID
	INNER JOIN 
		dbo.MasterCatalogSubCategory AS MCSC WITH (NOLOCK)
			ON MCC.MasterCatalogCategoryID = MCSC.MasterCatalogCategoryID

	ORDER BY
		  MCS.SupplierName
		, MCC.NAME 
		, MCSC.NAME 	

END