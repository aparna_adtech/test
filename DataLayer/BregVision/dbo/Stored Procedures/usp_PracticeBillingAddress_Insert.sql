﻿/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeBillingAddress_Insert
   
   Description:  Inserts a record into table Address
				  , Gets the new AddressID
				  , and Inserts a record into the PracticeBillingAddress table.
   
   AUTHOR:       John Bongiorni 7/16/2007 5:07:54 PM
   
   Modifications:  
   ------------------------------------------------------------ */  
   
CREATE PROCEDURE [dbo].[usp_PracticeBillingAddress_Insert]
(
	  @PracticeID					 INT
	, @IsBillingCentralized			 BIT 
    , @AddressID                     INT OUTPUT
	, @AttentionOf					 VARCHAR(50) = NULL
    , @AddressLine1                  VARCHAR(50)
    , @AddressLine2                  VARCHAR(50) = NULL
    , @City                          VARCHAR(50)
    , @State                         CHAR(2)
    , @ZipCode                       CHAR(5)
    , @ZipCodePlus4                  CHAR(4) = NULL
    , @CreatedUserID                 INT
)
AS
BEGIN

	DECLARE @TransactionCountOnEntry    INT,       -- Transaction Count before the transaction begins
			@Err						INT        --  holds the @@Error code returned by SQL Server

	SELECT @Err = @@ERROR
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @Err = 0
	BEGIN    
		SELECT @TransactionCountOnEntry = @@TRANCOUNT
		BEGIN TRANSACTION
	END        

	IF @Err = 0
	BEGIN

		UPDATE dbo.Practice
		SET IsBillingCentralized = @IsBillingCentralized
		WHERE PracticeID = @PracticeID 

	END

	IF @Err = 0
	BEGIN

		INSERT
		INTO dbo.Address
		(
			  AddressLine1
			, AddressLine2
			, City
			, State
			, ZipCode
			, ZipCodePlus4
			, CreatedUserID
			, IsActive
		)
		VALUES
		(
			  @AddressLine1
			, @AddressLine2
			, @City
			, @State
			, @ZipCode
			, @ZipCodePlus4
			, @CreatedUserID
			, 1
		)


		SELECT @AddressID = SCOPE_IDENTITY()


		SET @Err = @@ERROR
	END

	IF @Err = 0
	BEGIN

		INSERT
		INTO dbo.PracticeBillingAddress
		(
			  PracticeID
			, AddressID
			, AttentionOf
			, CreatedUserID
			, IsActive
		)
		VALUES
		(
			  @PracticeID
			, @AddressID
			, @AttentionOf
			, @CreatedUserID
			, 1
		)

		SELECT @Err = @@ERROR
	END


	
	-- JB Modified 20070818  to update all practice location billing addresses when a practice billing address is inserted.	
	--  uPDATE ALL Practice Location if Centralized.
	--DECLARE @IsBillingCentralized BIT	
	SELECT @IsBillingCentralized = IsBillingCentralized FROM dbo.Practice WHERE PracticeID = @PracticeID
	
	--	SELECT TOP 1 * FROM dbo.Practice 
	
	
	IF ( @IsBillingCentralized = 1 )
		begin
			EXEC dbo.usp_PracticeLocationBillingAddresses_Update_From_Centralized_PracticeBillingAddress  
					@PracticeID	= @PracticeID
					, @UserID	= @CreatedUserID
		end
	ELSE
		begin
			EXEC dbo.usp_PracticeLocationBillingAddresses_Update_From_Decentralized_PracticeBillingAddress
			  @PracticeID = @PracticeID
			, @UserID	= @CreatedUserID

		end	

	--May need to fix this
	IF @@TranCount > @TransactionCountOnEntry
	BEGIN

		IF @Err = 0
			COMMIT TRANSACTION
		ELSE
			ROLLBACK TRANSACTION
			--  Add any database logging here      
	END
			
		RETURN @Err
END
