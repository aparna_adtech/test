﻿-- =============================================
-- Author:		Mike Sneen
-- Create date: 02/06/09
-- Description:	Gets last 7 and last 30 days of cost for items dispensed
-- =============================================
create PROCEDURE [dbo].[usp_ProductCostAverageKPI]
	@PracticeLocationID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @ProductDispensed Table
    (
	  Last7DaysCost decimal default 0,
	  Last7DaysBillingCharge decimal default 0,
	  Last7DaysDMEDeposit decimal default 0,
	  Last30DaysCost decimal default 0,
	  Last30DaysBillingCharge decimal default 0,
	  Last30DaysDMEDeposit decimal default 0
    )

INSERT  INTO @ProductDispensed
        (
          Last7DaysCost,
          Last7DaysBillingCharge,
          Last7DaysDMEDeposit
          --, QuantityNotFullyCheckin
        )
select 
	ISNULL(AVG(ActualWHolesaleCost), 0) AS ActualWHolesaleCost,
	ISNULL(AVG(PCP.BillingCharge), 0) AS ActualBillingCharge,
	ISNULL(AVG(PCP.DMEDeposit), 0) AS DMEDeposit
from productinventory_DispenseDetail PIDD
inner join productInventory PI
	on PIDD.ProductInventoryID = PI.ProductInventoryID
--left outer join DispenseDetail DD
	--on PIDD.DispenseDetailID = DD.DispenseDetailID
Left Outer Join PracticeCatalogProduct PCP
	on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
where 
	practicelocationid=@PracticeLocationID
	and PIDD.CreatedDate > dateadd(wk,-1,getdate())

	update @ProductDispensed 
	set Last30DaysCost = Positive.Last30DaysCost,
	Last30DaysBillingCharge = Positive.Last30DaysBillingCharge,
	Last30DaysDMEDeposit = Positive.Last30DaysDMEDeposit
	from
	(
		select 
			ISNULL(AVG(ActualWHolesaleCost), 0) as Last30DaysCost,
			ISNULL(AVG(PCP.BillingCharge), 0) as Last30DaysBillingCharge,
			ISNULL(AVG(PCP.DMEDeposit), 0) as Last30DaysDMEDeposit
		from productinventory_DispenseDetail PIDD
		inner join productInventory PI
			on PIDD.ProductInventoryID = PI.ProductInventoryID
		--left outer join DispenseDetail DD
			--on PIDD.DispenseDetailID = DD.DispenseDetailID
		Left Outer Join PracticeCatalogProduct PCP
			on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
		where practicelocationid=@PracticeLocationID
			and PIDD.CreatedDate > dateadd(m,-1,getdate())
	) as Positive

select * from @ProductDispensed
 
END
