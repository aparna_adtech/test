﻿CREATE PROC usp_zPCSupplierBrand_SetSequence_20071129
AS 
BEGIN 

BEGIN TRAN

UPDATE PracticeCatalogSupplierBrand
	SET Sequence = IPD.Sequence
FROM Import_PracticeData.dbo.zPCSBID_Sequence_20071129 AS IPD
INNER JOIN PracticeCatalogSupplierBrand AS PCSB
	ON IPD.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 

SELECT 
	PCSB.PracticeCatalogSupplierBrandID
	, PCSB.Sequence
FROM PracticeCatalogSupplierBrand AS PCSB

ROLLBACK TRAN

END