﻿

-- =============================================
-- Author:		Greg Ross
-- Create date: 08/23/07
-- Description:	Used to insert and update table when performing a Manual CheckIn
-- =============================================
CREATE PROCEDURE [dbo].[usp_ManualCheckIn_InsertUpdate]
	
 @PracticeLocationID INT,
 @PracticeCatalogProductID INT,
 @ProductInventoryID int,
 @UserID INT,
 @Quantity int,
 @ActualWholesaleCost smallmoney,
 @Comments varchar(200)

AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON ;

        DECLARE @Err INT 
        DECLARE @TransactionCountOnEntry INT 

        IF @Quantity = 0 
			--PRINT 'zero' 	
            return 0	
        ELSE 
            SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

        SELECT  @TransactionCountOnEntry = @@TRANCOUNT
        SELECT  @Err = @@ERROR
        BEGIN TRANSACTION      	


        IF @Err = 0 
            BEGIN
			    
				--Update ProductInventory
				declare @QOHTemp int
				set @QOHTemp = (select QuantityOnHandPerSystem from ProductInventory where ProductInventoryID = @ProductInventoryID	)
--				print 'QOHTemp'				
--				print @QOHTemp
				set @QOHTemp = @QOHTemp + @Quantity

				update ProductInventory	
					set QuantityOnHandPerSystem = @QOHTemp,
						ModifiedUserID = @UserID,
						ModifiedDate = getdate()
					WHERE   ProductInventoryID = @ProductInventoryID
                        AND IsActive = 1

        
                SET @Err = @@ERROR	
			end 
			--Insert record into ProductInventory_ManualCheckIn
                iF @Err = 0 
                    BEGIN
   
			        --PRINT @ReOrderQuanity 	
						insert into ProductInventory_ManualCheckIn  
							(ProductInventoryID,
							 PracticeCatalogProductID,
							 ActualWholesaleCost,
							 Quantity,
							 DateCheckedIn,
							 IsAcquiredOutsideofSystem,
							 IsReturn,
							 Comments,
							 CreatedUserID,
							 CreatedDate,
							 IsActive)
							Values
							 (@ProductInventoryID,
							  @PracticeCatalogProductID,
							  @ActualWholesaleCost,
							  @Quantity,
							  getdate(),
							  1,
							  1,
							  @Comments,
							  @UserID,
							  getdate(),
							  1)
							 
	


                        SET @Err = @@ERROR

                    END
  
                        IF @@TranCount > @TransactionCountOnEntry 
                            BEGIN

                                IF @Err = 0 
                                    BEGIN 
                                        COMMIT TRANSACTION
                                        RETURN @Err
                               -- PRINT @err
                                    END
                                ELSE 
                                    BEGIN
                                        ROLLBACK TRANSACTION
                                        RETURN @Err
                                -- PRINT @err
					--  Add any database logging here      
                                    END
								END				
   end	


