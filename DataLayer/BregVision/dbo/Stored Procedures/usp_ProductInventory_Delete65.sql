﻿
--  ProductInventory_OrderCheckIn


/* ------------------------------------------------------------
   PROCEDURE:    [dbo].[usp_ProductInventory_Delete]
   
   Description:  "Deletes" a record from table dbo.PrroductInventory
   					This actual flags the IsActive column to false.
					
   AUTHOR:       John Bongiorni 02/05/2008 12:54:42 AM
   
   Modifications:
   TODO:  If hard delete, then delete, if not the set to IsActive = False.
   Check for dependencies.
   
   IMPORTANT:  ProductInventory_OrderCheckIn NEEDS SUM!!!
   
   ------------------------------------------------------------ */ 
/*
             [dbo].[usp_ProductInventory_Delete_Count]  , -2000
*/

--  SELECT * FROM ProductInventory WHERE PracticeLocationID = 3 AND IsActive = 0
--2


--  Dependency count = 105
--(1 row(s) affected)
--105
--Delete Soft
--
--(1 row(s) affected)
--  [dbo].[usp_ProductInventory_Delete89]  2, -999089

--  Set back.

--
--UPDATE productinventory
--SET IsActive= 1
--WHERE productinventoryID = 2

--SELECT * FROM productinventory WHERE productinventoryID = 2
--UPDATE productinventory
--SET QuantityOnHandPerSystem = 1
--WHERE productinventoryID = 2  

--[usp_ProductInventory_Delete61]  20, -999061   --14353 Test.


CREATE PROCEDURE [dbo].[usp_ProductInventory_Delete65]  --14353, -999061
    (
        @ProductInventoryID INT
      , @ModifiedUserID	  INT
    )
AS 

    BEGIN TRY	
    
			DECLARE @PracticeLocationID INT
			DECLARE @PracticeCatalogProductID INT
			
			DECLARE @IsRemovable BIT  --  Test to see if the productinventoryID is removalable by hard or soft delete.
			DECLARE @RemovalType INT  --  RemovalType is either 0 - none, 1 - Hard Delete, or 2 - Soft Delete.

			DECLARE @SumOfAllLevels		INT
			DECLARE @Count_Cart INT 			-- Prerequisite for removal.  No items may be in the shopping cart.
			DECLARE @Count_SOLI_NotFullyCheckedIn int  -- Prerequisite for removal.  No ordered items may outstanding (not fully checked-in).

			DECLARE @DependencyMessages VARCHAR(2000)
			DECLARE @IsAllLevelSetToZero INT  --  Check on all levels and Quantity on Hand.  Required to continue.
			
			
			
			
				
			DECLARE @Count_SupplierOrderLineItemCount INT 
			DECLARE @Count_ProductInventory_DispenseDetail  INT
			DECLARE @Count_ProductInventory_OrderCheckIn INT 
			DECLARE @Count_ProductInventory_DispenseDetail_PartiallyCheckedIn INT  --  Misnomer fix this!!!  -- Partially Checked in.  Cannot delete is more than 0.
			DECLARE @Count_ProductInventory_SOLI_OnOrder INT  --  On Order in SupplierOrderLineItem table, none checked in.

			DECLARE @Count_ProductInventory_ManualCheckIn INT
			DECLARE @Count_ProductInventory_ReturnedToSupplier int
			DECLARE @Count_ProductInventory_Purge INT
			DECLARE @Count_ProductInventory_QuantityOnHandPhysical INT
			DECLARE @Count_ProductInventory_ResetQuantityOnHandPerSystem Int
		
			
			SET @IsAllLevelSetToZero = 1  --  Default to false.
			SET @DependencyMessages = ''
			
			
			--  Given the PIID, Get the PracticeLocation and the PCPID (to be used below.)
			SELECT 
				  @PracticeLocationID   = Sub.PracticeLocationID
				, @PracticeCatalogProductID = Sub.PracticeCatalogProductID
			FROM 
				(
				 SELECT 
					PracticeLocationID,
					PracticeCatalogProductID
				 FROM [ProductInventory] AS PI	WITH (NOLOCK)
				 WHERE PI.[ProductInventoryID] = @ProductInventoryID
				 ) AS Sub
			--  AND PI.IsActive = 1
			
			
			--  Test Prerequisite that QuantityOnHandPerSystem, PI.ParLevel, PI.ReorderLevel, PI.CriticalLevel are all zero.
			SELECT @SumOfAllLevels = SUB.SumOfAllLevels
			FROM
				(
					SELECT 
						PI.QuantityOnHandPerSystem
						+ PI.ParLevel
						+ PI.ReorderLevel
						+ PI.CriticalLevel AS SumOfAllLevels						
					FROM ProductInventory AS PI	WITH (NOLOCK)
					WHERE PI.ProductInventoryID = @ProductInventoryID	
				) AS SUB
			
			SELECT @SumOfAllLevels AS SumOfAllLevels
			PRINT 'SumofAllLevels: ' + CAST(@SumOfAllLevels AS VARCHAR(10))
			SET @DependencyMessages = @DependencyMessages + ', Levels' 
    
    
    
    
			--  Check Shopping Cart.  
			--  If any items are found, then do not remove.
			--  Tested: the following works and has been tested (unit)
			SELECT 
				@Count_Cart = SUB.Count_Cart
			FROM
				(
					SELECT  			
						COUNT(1) AS Count_Cart  --SC.[PracticeLocationID], SI.[PracticeCatalogProductID] 			
					FROM ShoppingCart AS SC WITH (NOLOCK)
					INNER JOIN ShoppingCartItem AS SI WITH (NOLOCK)
						ON SC.ShoppingCartID = SI.ShoppingCartID
					--ORDER BY SI.practicecatalogproductID
					WHERE 
						  SI.PracticeCatalogProductID = @PracticeCatalogProductID
					  AND SC.PracticeLocationID = @PracticeLocationID
				) AS SUB
						
			SELECT @Count_Cart AS Count_Cart
			PRINT 'Count in Cart: ' + CAST(@Count_Cart AS VARCHAR(10))
    		SET @DependencyMessages = @DependencyMessages + ', Cart' 
			
			--  Check that there are no outstanding order items for the product.  ie not fully checked in.	
			--  Get count of SOLineItems that are not fully checked in for the productinventoryID.
				
				SELECT 
					@Count_SOLI_NotFullyCheckedIn = SUB.Count_SOLI_NotFullyCheckedIn								
				FROM
				(
					SELECT 
						COUNT(1) AS Count_SOLI_NotFullyCheckedIn
						--SUM(SOLI.QuantityOrdered) AS SOLI_Ordered_NotFullyCheckedIn
								
					FROM dbo.ProductInventory AS PI WITH(NOLOCK)
				    
					INNER JOIN dbo.PracticeCatalogProduct AS PCP WITH(NOLOCK) 
						ON PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
						AND PI.ProductInventoryID = @ProductInventoryID
						
					INNER JOIN dbo.BregVisionOrder AS BVO WITH(NOLOCK)
						ON PI.PracticeLocationID = BVO.PracticeLocationID
				        
					INNER JOIN dbo.SupplierOrder AS SO WITH(NOLOCK)
						ON BVO.BregVisionOrderID = SO.BregVisionOrderID      
				        
					INNER JOIN dbo.SupplierOrderLineItem AS SOLI WITH(NOLOCK)
						ON SO.SupplierOrderID = SOLI.SupplierOrderID 
						  AND PCP.PracticeCatalogProductID = SOLI.PracticeCatalogProductID      
				   
					INNER JOIN dbo.SupplierOrderLineItemStatus AS SOLIStatus WITH(NOLOCK)
						ON SOLI.SupplierOrderLineItemStatusID = SOLIStatus.SupplierOrderLineItemStatusID   
						
				   WHERE 
						PI.PracticeLocationID = @PracticeLocationID  
						AND PI.ProductInventoryID = @ProductInventoryID 
						AND PCP.IsActive = 1
						AND BVO.IsActive = 1
						AND SO.IsActive = 1
						AND SOLI.IsActive = 1
						--AND PI.IsActive = 1    
						AND SOLIStatus.SupplierOrderLineItemStatusID IN (1, 3)      -- SOLineItemStatus is 'On Order or Partially Checked In'  (has outstanding orders.)
				        
					GROUP BY
						PI.ProductInventoryID
				) AS SUB
				
			SELECT 	@Count_SOLI_NotFullyCheckedIn = ISNULL(@Count_SOLI_NotFullyCheckedIn, 0)	
			SELECT @Count_SOLI_NotFullyCheckedIn  AS Count_SOLI_NotFullyCheckedIn
			PRINT 'Count on Order: ' + CAST(@Count_SOLI_NotFullyCheckedIn AS VARCHAR(10))
    		SET @DependencyMessages = @DependencyMessages + ', On Order' 
			
			
			
			----------------------------
			DECLARE @Count_Removable INT
			SET @Count_Removable = @SumOfAllLevels + @Count_Cart + @Count_SOLI_NotFullyCheckedIn 
			IF ( @Count_Removable  > 0 )
			BEGIN
				--  Cannot be removed since the prerequisites are not met.
				SET @RemovalType = 0	--  item cannot be removed.
				PRINT 'Cannot be removed! Requisites are NOT meet.'
			END
			ELSE	
			BEGIN
				PRINT 'May be removed!  Prerequisites are meet.'  
					--  Check SupplierOrderLineItem to see if product has ever been order for the location.
			--  Following is for testing.  Tested successfully.
			--  TODO:  cHANGE order of the tables.
				SELECT 
					@Count_SupplierOrderLineItemCount = SUB.Count_SupplierOrderLineItemCount
				FROM
				(
			
					SELECT COUNT(1) AS Count_SupplierOrderLineItemCount
					FROM [SupplierOrderLineItem] AS SOLI  WITH (NOLOCK)
					INNER JOIN [SupplierOrder] AS SO  WITH (NOLOCK)
						ON Soli.supplierOrderID = SO.supplierOrderID
					INNER JOIN [BregVisionOrder] AS BVO  WITH (NOLOCK)
						ON SO.BregVisionOrderID = BVO.BregVisionOrderID
					WHERE 
						BVO.[PracticeLocationID] = @PracticeLocationID
						AND SOLI.[PracticeCatalogProductID] = @PracticeCatalogProductID
						AND BVO.[IsActive] = 1
						AND SO.[IsActive] = 1
						AND SOLI.[IsActive] = 1	
				) AS SUB		
			
			
			--  Check if ever dispensed ProductInventory_DispenseDetail
			
				SELECT 
					@Count_ProductInventory_DispenseDetail = SUB.Count_ProductInventory_DispenseDetail
				FROM
					(			
						SELECT COUNT(1) AS Count_ProductInventory_DispenseDetail
						FROM dbo.ProductInventory_DispenseDetail AS PIDD  WITH (NOLOCK)
						WHERE PIDD.ProductInventoryID = @ProductInventoryID
					) AS SUB
			
					
					
				--  Check if product was ever checked in.
				SELECT 
					@Count_ProductInventory_OrderCheckIn = SUB.Count_ProductInventory_OrderCheckIn
				FROM
					(			
						SELECT COUNT(1) AS Count_ProductInventory_OrderCheckIn
						FROM dbo.ProductInventory_OrderCheckIn AS PI_OCI  WITH (NOLOCK)
						WHERE PI_OCI.ProductInventoryID = @ProductInventoryID
					) AS SUB
										
				
				--  Check if product was ever manually checked in.	
				SELECT 
					@Count_ProductInventory_ManualCheckIn = SUB.Count_ProductInventory_ManualCheckIn
				FROM
					(			
						SELECT COUNT(1) AS Count_ProductInventory_ManualCheckIn
						FROM dbo.ProductInventory_ManualCheckIn AS PI_OCI  WITH (NOLOCK)
						WHERE PI_OCI.ProductInventoryID = @ProductInventoryID
					) AS SUB
			

			--  Check all for future compatibility.
			----  Not Yet Implemented ---------  For future compatibility. --------
				SELECT 
					@Count_ProductInventory_ReturnedToSupplier = SUB.Count_ProductInventory_ReturnedToSupplier
				FROM
					(			
						SELECT COUNT(1) AS Count_ProductInventory_ReturnedToSupplier
						FROM dbo.ProductInventory_ReturnedToSupplier AS PI_OCI  WITH (NOLOCK)
						WHERE PI_OCI.ProductInventoryID = @ProductInventoryID
					) AS SUB
					

					
				--------------------------	
				SELECT 
					@Count_ProductInventory_Purge = SUB.Count_ProductInventory_Purge
				FROM
					(			
						SELECT COUNT(1) AS Count_ProductInventory_Purge
						FROM dbo.ProductInventory_Purge AS PI_OCI  WITH (NOLOCK)
						WHERE PI_OCI.ProductInventoryID = @ProductInventoryID
					) AS SUB
					
					
					
				SELECT 
					@Count_ProductInventory_QuantityOnHandPhysical = SUB.Count_ProductInventory_QuantityOnHandPhysical
				FROM
					(			
						SELECT COUNT(1) AS Count_ProductInventory_QuantityOnHandPhysical
						FROM dbo.ProductInventory_QuantityOnHandPhysical AS PI_OCI  WITH (NOLOCK)
						WHERE PI_OCI.ProductInventoryID = @ProductInventoryID
					) AS SUB
			
			
			
					SELECT 
					@Count_ProductInventory_ResetQuantityOnHandPerSystem = SUB.Count_ProductInventory_ResetQuantityOnHandPerSystem
				FROM
					(			
						SELECT COUNT(1) AS Count_ProductInventory_ResetQuantityOnHandPerSystem
						FROM dbo.ProductInventory_ResetQuantityOnHandPerSystem AS PI_OCI  WITH (NOLOCK)
						WHERE PI_OCI.ProductInventoryID = @ProductInventoryID
					) AS SUB
		

						DECLARE @DependencyCount INT
				DECLARE @IsHardDelete BIT
				SET @IsHardDelete = NULL
				SET @DependencyCount = 9999  -- To avoid an inadvertent delete.
			
				SET
					@DependencyCount =	
					  @Count_Cart 
					+ @Count_SupplierOrderLineItemCount 
					+ @Count_ProductInventory_OrderCheckIn 
					+ @Count_ProductInventory_DispenseDetail 
					+ @Count_ProductInventory_ManualCheckIn 
					+ @Count_ProductInventory_ReturnedToSupplier 
					+ @Count_ProductInventory_Purge 
					+ @Count_ProductInventory_QuantityOnHandPhysical 
					+ @Count_ProductInventory_ResetQuantityOnHandPerSystem 
			
				SELECT @DependencyCount AS DependencyCount
				PRINT 'DependencyCount: ' + CAST(@DependencyCount AS VARCHAR(10))
				
				
		
				IF (@DependencyCount = 0)  
				BEGIN
					SET @IsHardDelete = 1  --  Execute a hard delete.
					PRINT 'Delete Hard'
				END
				ELSE IF (@DependencyCount > 0)  
				BEGIN
					
					PRINT 'DependencyMessages: ' + CAST(@DependencyMessages AS VARCHAR(500))
					
					SET @IsHardDelete = 0  --  Execute a soft delete.
					PRINT 'Delete Soft'
				END
		
			
			END
			
			
			
			
			
			
			
			
			
			
	
    
					
					
					
		
				--  For testing only.
--				SELECT 
--					@Count_Cart AS Cart
--					, @Count_SupplierOrderLineItemCount AS soli
--					, @Count_ProductInventory_OrderCheckIn AS PI_OCI
--					, @Count_ProductInventory_DispenseDetail AS PIDD
--					, @Count_ProductInventory_ManualCheckIn AS PI_MCI
--					, @Count_ProductInventory_ReturnedToSupplier AS PI_RTS
--					, @Count_ProductInventory_Purge AS PI_P
--					, @Count_ProductInventory_QuantityOnHandPhysical AS PI_QOHP
--					, @Count_ProductInventory_ResetQuantityOnHandPerSystem AS PI_RQOHPS
			
			
		
				
--				--  EXECUTE
--				IF (@IsHardDelete = 1)
--				  BEGIN
--					DELETE dbo.ProductInventory		            	        
--					WHERE ProductInventoryID = @ProductInventoryID
--				  END
--				ELSE IF (@IsHardDelete = 0) 
--				  BEGIN
--    				UPDATE dbo.ProductInventory		            
--					SET IsActive = 0		        
--					WHERE ProductInventoryID = @ProductInventoryID
--				  END
				  
				--TODO: IF @DependencyCount = 0 then HARD delete
				--  ELSE Soft Delete.
				
				--  TEST, TEST, TEST, 
				
				--  IF @DependencyCount > 0 THEN Set IsActive = 0?????????
				--  find the effects of this through out the system.
				--	Cart?
				--  Order?
				--  Reports?
				--  Dispense?
				--  Check-In
				
				
				
--			SELECT * FROM [ProductInventory]
--			WHERE [PracticeCatalogProductID] = 983
			
			--SELECT * FROM practicelocation WHERE practicelocationid IN (41,42,43)
			
--    7828
--7858
--7861
--7864

--			--  IF their are dependencies.
--    		UPDATE
--				dbo.ProductInventory
--	            
--			SET IsActive = 0
--	        
--			WHERE
--				ProductInventoryID = @ProductInventoryID
	
			
	END TRY

	BEGIN CATCH

	  EXECUTE [dbo].[uspLogError]
	  EXECUTE [dbo].[uspPrintError]
	  
	END CATCH	
    


--SELECT *  --  12570
--FROM [ProductInventory]
--WHERE [PracticeLocationID] = 46
--AND [PracticeCatalogProductID] = 2544


-----------------------------------------------------------------------------

--  SELECT * FROM ProductInventory WHERE PracticeLocationID = 3 AND IsActive = 0
--2
--3
--4
--5
--6
--7
--8
--9
--10
--11
--12
--13
--14
--15
--16
--17
--18
--19
--20
--64
--143
--553
--588
--6988
--6989
--6990
--6991
--6992
--6993
--6994
--6995
--6996
--6997
--6998
--6999
--7714
--7715
--7716
--7717
--7827
--7828
--7829
--7830
--7831
--7832
--7833
--7834
--7835
--7836
--14351
--14353
--
