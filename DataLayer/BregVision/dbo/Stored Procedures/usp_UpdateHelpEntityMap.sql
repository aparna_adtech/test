﻿
-- =============================================
-- Author:		<Author: Tom Thomas>
-- Create date: <Create Date: 03/12/10>
-- Description:	<Description: Update HelpEntityMapping
-- =============================================
Create PROCEDURE [dbo].[usp_UpdateHelpEntityMap]
	@Id int 
	,@ParentId int
	,@SortOrder int = 0
		
AS 
    BEGIN
	
	SET NOCOUNT ON;
	
	
	
	Update HelpEntityTree Set ParentId=@ParentId,SortOrder=@SortOrder
	Where Id=@Id
	
	END



