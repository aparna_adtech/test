﻿


/* ------------------------------------------------------------
   PROCEDURE:    [dbo].[usp_GetAllProductDatabyLocation]
   
   Description:  Gets all Child data for Inventory on Inventory.aspx page.
		
	Dependencies:  	udf_GetSuggestedReorder_Given_PracticeLocationID	 
   
   AUTHOR:       John Bongiorni 10/18/2007 5:45:17 PM
   
   Modifications:  Mike Sneen 6/15/2010 reworked for global search
   Exec [usp_GetAllProductDatabyPracticeOrLocation] 3, 1, 'Code', '10085'
   Exec [usp_GetAllProductDatabyPracticeOrLocation] 3, 0, 'Browse', ''
   ------------------------------------------------------------ */ 


CREATE PROCEDURE [dbo].[usp_GetAllProductDatabyPracticeOrLocation]   
		@PracticeLocationID			INT
	  , @PracticeID					INT = 0			--  For backward compatibility with application code, @PracticeID is never used.
	  , @SearchCriteria varchar(50)
	  , @SearchText  varchar(50)
AS 
    BEGIN
    
    	/*These variables are used in conjunction with the coalesce statements in the where clause.
	each of these variables will be null except one is @SearchCriteria matches.*/
	Declare @ProductName varchar(50)			--NULL   These default to null
	Declare @ProductCode varchar(50)			--NULL   These default to null
	Declare @ProductBrand varchar(50)			--NULL   These default to null
	Declare @ProductHCPCs varchar(50)			--NULL   These default to null

	if @SearchCriteria='Name'					-- ProductName
		Set @ProductName = '%' + @SearchText + '%'		--put the percent sign on both sides of the search text for product name
	else if @SearchCriteria='Code'
		Set @ProductCode =  '%' + @SearchText + '%'	
	else if @SearchCriteria='Brand'
		Set @ProductBrand = @SearchText + '%'	
	else if @SearchCriteria='HCPCs'
		Set @ProductHCPCs = @SearchText + '%'
    
    
	DECLARE @PracticeLocationPracticeID AS INTEGER
	SELECT @PracticeLocationPracticeID = PracticeID
	FROM PracticeLocation
	WHERE PracticeLocationID = @PracticeLocationID
    

	-- Table Variable used as a working table
        DECLARE @WorkingTable TABLE
            (
              Category VARCHAR(50)
            , SubCategory VARCHAR(50)
            , Product VARCHAR(100)
            , Code VARCHAR(50)
            , Packaging VARCHAR(50)
            , LeftRightSide VARCHAR(50)
            , Size VARCHAR(50)
            , Gender VARCHAR(50)
            , Color VARCHAR(50)
            , SupplierID INT
            , PracticeCatalogProductID INT

            , QuantityOnHand INT DEFAULT 0
            , QuantityOnOrder INT DEFAULT 0
            , PARLevel INT DEFAULT 0
            , ReorderLevel INT DEFAULT 0
            , CriticalLevel INT DEFAULT 0
            , PriorityPARLevel INT DEFAULT 0
            , PriorityReorderLevel INT DEFAULT 0
            , PriorityCriticalLevel INT DEFAULT 0
            , SuggestedReorderLevel INT DEFAULT 0
            , IsThirdPartyProduct BIT
			, StockingUnits int default 1
			, PracticeLocationName varchar(50)
			, Priority int default 4
			, IsNotFlaggedForReorder BIT DEFAULT 0
			, IsShoppingCart bit default 0
	  
			, OldMasterCatalogProductID		INT NULL DEFAULT NULL
			, OldThirdPartyProductID		INT NULL DEFAULT NULL
			, NewMasterCatalogProductID		INT NULL DEFAULT NULL
			, NewThirdPartyProductID		INT NULL DEFAULT NULL
			, NewPracticeCatalogProductID	INT NULL DEFAULT NULL
			, NewPracticeID					INT NULL DEFAULT NULL
			, NewProductCode				VARCHAR(50) NULL DEFAULT NULL
			, NewProductName				VARCHAR(100) NULL DEFAULT NULL
            )

         --     2.a  Create a list of all locations to loop through
         Declare @ProductLocations TABLE
         (
			PracticeLocationID int,
			PracticeLocationName varchar(50),
			fUsed bit default 0
         )
         insert into @ProductLocations(PracticeLocationID, PracticeLocationName, fUsed)
         (
			select pl.PracticeLocationID, pl.Name, 0 from PracticeLocation pl where (@PracticeID > 0 AND pl.PracticeID = @PracticeID) OR (@PracticeID < 1 AND pl.PracticeLocationID=@PracticeLocationID)
         )
         
         --Select * from @ProductLocations -- For debug
         
         Declare @CurrentPracticeLocationID int
         Declare @CurrentPracticeLocationName Varchar(50)
         
         Select TOP 1 @CurrentPracticeLocationID = PracticeLocationID from @ProductLocations where fUsed = 0
         Select TOP 1 @CurrentPracticeLocationName = PracticeLocationName from @ProductLocations where fUsed = 0
         
         WHILE @@ROWCOUNT <> 0 AND @CurrentPracticeLocationID IS NOT NULL
         BEGIN
         
         		-- 1.  Insert the PracticeCatalogProductID for the location's products that are in inventory and active.

		

			--  3.  Get Product Info for PracticeCatalogProduct in inventory. 
		--Select * from @WorkingTable -- for debug
	
		INSERT INTO
            @WorkingTable
            (
				  PracticeCatalogProductID 
				, StockingUnits 
        
				, Category 
				, SubCategory 
				, Product
				, Code
				, Packaging
				, LeftRightSide
				, Size
				, Gender
				, Color
				, SupplierID
				, IsThirdPartyProduct
		  		  
  				, QuantityOnHand 
				, QuantityOnOrder

				, ParLevel
				, ReorderLevel
				, CriticalLevel
				, PriorityParLevel
				, PriorityReorderLevel
				, priorityCriticalLevel
				, PracticeLocationName
				, SuggestedReorderLevel
				, IsNotFlaggedForReorder
				, IsShoppingCart

				, OldMasterCatalogProductID
				, OldThirdPartyProductID
				, NewMasterCatalogProductID
				, NewThirdPartyProductID
				, NewPracticeCatalogProductID
				, NewPracticeID
				, NewProductName
				, NewProductCode
            )
        Select 
			CheckIn.PracticeCatalogProductID
		  , CheckIn.StockingUnits
        
          , CheckIn.Category
          , CheckIn.SubCategory
          , CheckIn.Product
          , CheckIn.Code
          , CheckIn.Packaging
          , CheckIn.LeftRightSide
          , CheckIn.Size
          , CheckIn.Gender
          , CheckIn.Color
          , CheckIn.SupplierID
          , CheckIn.IsThirdPartyProduct
  		  
  		  , Levels.QuantityOnHand
		  , Levels.QuantityOnOrder

		  , Levels.ParLevel
		  , Levels.ReorderLevel
		  , Levels.CriticalLevel	
		  , Levels.PriorityParLevel
		  , Levels.PriorityReorderLevel
		  , Levels.PriorityCriticalLevel	
		  , @CurrentPracticeLocationName
		  , (Levels.SuggestedReorderLevel	/ CheckIn.StockingUnits)
		  , Levels.IsNotFlaggedForReorder 
		  , CheckIn.IsShoppingCart
		  , CheckIn.OldMasterCatalogProductID
		  , CheckIn.OldThirdPartyProductID
		  , CheckIn.NewMasterCatalogProductID
		  , CheckIn.NewThirdPartyProductID
		  , CheckIn.NewPracticeCatalogProductID
		  , CheckIn.NewPracticeID
		  , CheckIn.Name
		  , CheckIn.NewProductCode
        FROM
            (
                         SELECT
                            Sub.PracticeCatalogProductID
                          , Sub.CATEGORY
                          , Sub.SUBCATEGORY
                          , Sub.PRODUCT
                          , Sub.Code
                          , Sub.Packaging
                          , Sub.LeftRightSide
                          , Sub.Size
                          , Sub.Gender
						  , Sub.Color
                          , Sub.SupplierID
                          , Sub.IsThirdPartyProduct
                          , Sub.StockingUnits
						  , Sub.IsShoppingCart
						  , Sub.OldMasterCatalogProductID
						  , Sub.OldThirdPartyProductID
						  , Sub.NewMasterCatalogProductID
						  , Sub.NewThirdPartyProductID
						  , Sub.NewPracticeCatalogProductID
						  , Sub.NewPracticeID
						  , Sub.Name
						  , Sub.NewProductCode

                         FROM
                            (
                              SELECT
                                PCP.[PracticeCatalogProductID]
                              , mCC.NAME AS cATEGORY
                              , mcsc.[Name] AS sUBcATEGORY
                              , MCP.[Name] AS pRODUCT
                              , MCP.[Code] AS Code
                              , MCP.[Packaging] AS Packaging
                              , MCP.[LeftRightSide] AS LeftRightSide
                              , mcp.[Size] AS Size
                              , mcp.[Gender] AS Gender
                              , mcp.[Color] AS Color
                              , PCP.PracticeCatalogSupplierBrandID AS SupplierID
                              , PCP.IsThirdPartyProduct
                              , PCP.StockingUnits
							  , CASE WHEN (SCI.ShoppingCartItemID is null) THEN 0 ELSE 1 END as IsShoppingCart
							  , PR.OldMasterCatalogProductID
							  , PR.OldThirdPartyProductID
							  , PR.NewMasterCatalogProductID
							  , PR.NewThirdPartyProductID
							  , MAX(CASE WHEN (NPI.PracticeLocationID IS NOT NULL) THEN NPCP.PracticeCatalogProductID ELSE 0 END) AS NewPracticeCatalogProductID
							  , PR.PracticeID AS NewPracticeID
							  , NMCP.Name
							  , NMCP.Code AS NewProductCode

                              FROM
                                [MasterCatalogSupplier] AS MCS WITH (NOLOCK)
                                INNER JOIN [MasterCatalogCategory] AS MCC WITH (NOLOCK) ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
                                INNER JOIN [MasterCatalogSubCategory] AS MCSC WITH ( NOLOCK ) ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
                                INNER JOIN [MasterCatalogProduct] AS MCP WITH (NOLOCK) ON MCP.MasterCatalogSubCategoryID = MCSC.MasterCatalogSubCategoryID
                                INNER JOIN [PracticeCatalogProduct] AS PCP WITH (NOLOCK) ON MCP.[MasterCatalogProductID] = PCP.[MasterCatalogProductID]
                                INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB WITH (NOLOCK) ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
                                INNER JOIN [ProductInventory] AS PI WITH (NOLOCK) ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
								left join ShoppingCart SC on SC.[PracticeLocationID] = @CurrentPracticeLocationID
								left join shoppingcartitem SCI ON SC.[ShoppingCartID] = SCI.[ShoppingCartID] AND PCP.PracticeCatalogProductID = SCI.PracticeCatalogProductID
								LEFT JOIN MasterCatalogProduct OMCP ON OMCP.MasterCatalogProductID = PCP.MasterCatalogProductID
								LEFT JOIN ProductReplacement PR ON PR.IsActive = 1 AND
									(PR.OldMasterCatalogProductID = PCP.MasterCatalogProductID) AND
									(PR.PracticeID IS NULL OR PR.PracticeID = PCP.PracticeID)
								LEFT JOIN MasterCatalogProduct NMCP ON NMCP.MasterCatalogProductID = PR.NewMasterCatalogProductID
								LEFT JOIN PracticeCatalogProduct NPCP ON NPCP.MasterCatalogProductID = PR.NewMasterCatalogProductID
								LEFT JOIN ProductInventory NPI ON NPI.PracticeCatalogProductID = NPCP.PracticeCatalogProductID AND NPI.PracticeLocationID = @PracticeLocationID

                              WHERE
								MCP.IsActive = 1
								AND PCP.IsActive = 1
								AND PI.IsActive = 1
                                AND PI.[PracticeLocationID] = @CurrentPracticeLocationID --@PracticeLocationID
								AND (PR.PracticeID = @PracticeLocationPracticeID OR PR.PracticeID IS NULL)
                                AND PCP.IsThirdPartyProduct = 0
								--and (MCP.Name like @SearchText or MCP.ShortName like  @SearchText)
								AND
								(
									--ProductName || ShortName
									COALESCE(MCP.Name, '') like COALESCE(@ProductName, MCP.Name, '')
									OR
									COALESCE(MCP.ShortName, '') like COALESCE(@ProductName, MCP.ShortName, '')
								)
	                   			AND
								(
									COALESCE(MCP.Code, '') like COALESCE(@ProductCode , MCP.Code, '')
								)
								AND
								(
									COALESCE(PCSB.BrandName, '') like COALESCE(@ProductBrand, PCSB.BrandName, '')
									OR
									COALESCE(PCSB.BrandShortName, '') like COALESCE(@ProductBrand, PCSB.BrandShortName, '')
								)
								AND 
								(   /* use this type of statement for Child tables of the target to avoid duplicate rows.
										If we use a join for this, we would return duplicate rows, so we just check the existence
										of one record that matches the search parameters in the child table
										*/
									(
										 (@ProductHCPCs IS NULL)
										 OR EXISTS
										 (  
											Select 
												1 
											 from 
												ProductHCPCS PHCPCS 
											 where 
												PHCPCS.PracticeCatalogProductID = PI.PracticeCatalogProductID 
												and PHCPCS.HCPCS like @ProductHCPCs  
										  )
									 )
								)

								GROUP BY
									PCP.[PracticeCatalogProductID]
								  , mCC.NAME
								  , mcsc.[Name]
								  , MCP.[Name]
								  , MCP.[Code]
								  , MCP.[Packaging]
								  , MCP.[LeftRightSide]
								  , mcp.[Size]
								  , mcp.[Gender]
								  , mcp.[Color]
								  , PCP.PracticeCatalogSupplierBrandID
								  , PCP.IsThirdPartyProduct
								  , PCP.StockingUnits
								  , CASE WHEN (SCI.ShoppingCartItemID is null) THEN 0 ELSE 1 END
								  , PR.OldMasterCatalogProductID
								  , PR.OldThirdPartyProductID
								  , PR.NewMasterCatalogProductID
								  , PR.NewThirdPartyProductID
								  , PR.PracticeID
								  , NMCP.Name
								  , NMCP.Code
								
								
                   --  20071011 JB Union the thirdpartyproducts.
                              UNION
                              SELECT
                                PCP.[PracticeCatalogProductID]
                              , 'NA' AS cATEGORY
                              , 'NA' AS sUBcATEGORY
                              , TPP.[Name] AS pRODUCT
                              , TPP.[Code] AS Code
                              , TPP.[Packaging] AS Packaging
                              , TPP.[LeftRightSide] AS LeftRightSide
                              , TPP.[Size] AS Size
                              , TPP.[Gender] AS Gender
							  , TPP.[Color] AS Color
                              , PCP.PracticeCatalogSupplierBrandID AS SupplierID
                              , PCP.IsThirdPartyProduct
                              , PCP.StockingUnits
							  , CASE WHEN (SCI.ShoppingCartItemID is null) THEN 0 ELSE 1 END as IsShoppingCart
							  , PR.OldMasterCatalogProductID
							  , PR.OldThirdPartyProductID
							  , PR.NewMasterCatalogProductID
							  , PR.NewThirdPartyProductID
							  , MAX(CASE WHEN (NPI.PracticeLocationID IS NOT NULL) THEN NPCP.PracticeCatalogProductID ELSE 0 END) AS NewPracticeCatalogProductID
							  , PR.PracticeID AS NewPracticeID
							  , NTPP.Name
							  , NTPP.Code AS NewProductCode

                              FROM                              
                                dbo.ThirdPartyProduct AS TPP WITH (NOLOCK)
                                INNER JOIN [PracticeCatalogProduct] PCP WITH (NOLOCK) ON PCP.[ThirdPartyProductID] = TPP.[ThirdPartyProductID]
                                INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB WITH (NOLOCK) ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
                                INNER JOIN [ProductInventory] PI WITH (NOLOCK) ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
								left join ShoppingCart SC on SC.[PracticeLocationID] = @CurrentPracticeLocationID
								left join shoppingcartitem SCI ON SC.[ShoppingCartID] = SCI.[ShoppingCartID] AND PCP.PracticeCatalogProductID = SCI.PracticeCatalogProductID
								LEFT JOIN ProductReplacement PR ON PR.IsActive = 1 AND
									(PR.OldThirdPartyProductID = TPP.ThirdPartyProductID) AND
									(PR.PracticeID IS NULL OR PR.PracticeID = PCP.PracticeID)
								LEFT JOIN ThirdPartyProduct NTPP ON NTPP.ThirdPartyProductID = PR.NewThirdPartyProductID
								LEFT JOIN PracticeCatalogProduct NPCP ON NPCP.ThirdPartyProductID = PR.NewThirdPartyProductID
								LEFT JOIN ProductInventory NPI ON NPI.PracticeCatalogProductID = NPCP.PracticeCatalogProductID AND NPI.PracticeLocationID = @PracticeLocationID

                              WHERE
                                TPP.IsActive = 1
								AND PCP.IsActive = 1
								AND PI.IsActive = 1
                                AND PI.[PracticeLocationID] = @CurrentPracticeLocationID --@PracticeLocationID
                                AND PCP.IsThirdPartyProduct = 1
								--and (TPP.Name like @SearchText or TPP.ShortName like  @SearchText)
								AND
								(
									--ProductName || ShortName
									COALESCE(TPP.Name, '') like COALESCE(@ProductName, TPP.Name, '')
									OR
									COALESCE(TPP.ShortName, '') like COALESCE(@ProductName, TPP.ShortName, '')								
								)
	                   			AND
								(
									COALESCE(TPP.Code, '') like COALESCE(@ProductCode , TPP.Code, '')
								)	
								AND
								(
									COALESCE(PCSB.BrandName, '') like COALESCE(@ProductBrand, PCSB.BrandName, '')
									OR
									COALESCE(PCSB.BrandShortName, '') like COALESCE(@ProductBrand, PCSB.BrandShortName, '')
								)
								AND 
								(   /* use this type of statement for Child tables of the target to avoid duplicate rows.
										If we use a join for this, we would return duplicate rows, so we just check the existence
										of one record that matches the search parameters in the child table
										*/
									(
										 (@ProductHCPCs IS NULL)
										 OR EXISTS
										 (  
											Select 
												1 
											 from 
												ProductHCPCS PHCPCS 
											 where 
												PHCPCS.PracticeCatalogProductID = PI.PracticeCatalogProductID 
												and PHCPCS.HCPCS like @ProductHCPCs  
										  )
									 )
								)

							GROUP BY
                                PCP.[PracticeCatalogProductID]
                              , TPP.[Name]
                              , TPP.[Code]
                              , TPP.[Packaging]
                              , TPP.[LeftRightSide]
                              , TPP.[Size]
                              , TPP.[Gender]
							  , TPP.[Color]
                              , PCP.PracticeCatalogSupplierBrandID
                              , PCP.IsThirdPartyProduct
                              , PCP.StockingUnits
							  , CASE WHEN (SCI.ShoppingCartItemID is null) THEN 0 ELSE 1 END
							  , PR.OldMasterCatalogProductID
							  , PR.OldThirdPartyProductID
							  , PR.NewMasterCatalogProductID
							  , PR.NewThirdPartyProductID
							  , PR.PracticeID
							  , NTPP.Name
							  , NTPP.Code

                            ) AS Sub
                       ) AS CheckIn
                    INNER JOIN dbo.udf_GetSuggestedReorder_Given_PracticeLocationID( @CurrentPracticeLocationID )	AS Levels
						ON CheckIn.PracticeCatalogProductID = Levels.PracticeCatalogProductID

			-- end of While Loop
			Update @ProductLocations SET fUsed = 1 where PracticeLocationID = @CurrentPracticeLocationID
			
			Select TOP 1 @CurrentPracticeLocationID = PracticeLocationID from @ProductLocations where fUsed = 0
			Select TOP 1 @CurrentPracticeLocationName = PracticeLocationName from @ProductLocations where fUsed = 0
		END     -- end of While Loop                           
--  Below get counts

		SELECT
			OldMasterCatalogProductID,
			OldThirdPartyProductID
		INTO #WorkingTablePracticeSpecific
		FROM @WorkingTable
		WHERE NewPracticeID IS NOT NULL

        SELECT
				Category
			  , SubCategory
			  , Product
			  , Code
			  , Packaging
			  , LeftRightSide
			  , Size
			  , Gender
			  , Color
	          
			  , SupplierID
			  , PracticeCatalogProductID

			  , QuantityOnHand
			  , QuantityOnOrder
	                    
			  , PARLevel
			  , ReorderLevel
			  , CriticalLevel
			  , PriorityPARLevel
			  , PriorityReorderLevel
			  , PriorityCriticalLevel
	          
			  , COALESCE(SuggestedReorderLevel, 0) as SuggestedReorderLevel
	          , PracticeLocationName
			  , IsNotFlaggedForReorder
			  , IsShoppingCart
			  --, IsThirdPartyProduct
			  , OldMasterCatalogProductID
			  , OldThirdPartyProductID
			  , NewMasterCatalogProductID
			  , NewThirdPartyProductID
			  , NewPracticeCatalogProductID
			  , NewProductCode
			  , NewProductName
        
        FROM
            @WorkingTable WT
		WHERE NOT EXISTS (
			SELECT * FROM #WorkingTablePracticeSpecific WTPS
			WHERE
				COALESCE(WT.OldMasterCatalogProductID, 0) = COALESCE(WTPS.OldMasterCatalogProductID, 0) AND
				COALESCE(WT.OldThirdPartyProductID, 0) = COALESCE(WTPS.OldThirdPartyProductID, 0) AND
                WT.NewPracticeID IS NULL
				)

        ORDER BY
			PracticeLocationName
          , Category
          , Subcategory
          , Product
          , Code


    END
