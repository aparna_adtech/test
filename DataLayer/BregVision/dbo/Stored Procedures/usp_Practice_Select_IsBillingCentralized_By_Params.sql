﻿/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Practice_Select_IsBillingCentralized_By_Params
   
   Description:  Selects a IsBillingCentralized from table dbo.Practice
   				   and puts value into a parameter.
   
   AUTHOR:       John Bongiorni 8/06/2007 9:24 PM
   
   Modifications:  
   ------------------------------------------------------------ */  

CREATE PROCEDURE [dbo].[usp_Practice_Select_IsBillingCentralized_By_Params]
				
(
      @PracticeID                    INT
    , @IsBillingCentralized          BIT            OUTPUT
)
AS
BEGIN

	DECLARE @Err INT

	SELECT
          @PracticeID = PracticeID
        , @IsBillingCentralized = IsBillingCentralized
        
	FROM dbo.Practice
	
	WHERE 
		PracticeID = @PracticeID
		AND IsActive = 1

	SET @Err = @@ERROR

	RETURN @Err

End


