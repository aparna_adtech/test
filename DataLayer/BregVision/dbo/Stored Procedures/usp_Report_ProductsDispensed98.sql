﻿CREATE PROC dbo.usp_Report_ProductsDispensed98 
		  @PracticeID	INT
		, @PracticeLocationID	VARCHAR(2000)
		, @StartDate		DATETIME
		, @EndDate			DATETIME
AS
BEGIN

--	DECLARE @PracticeID INT
--	SET @PracticeID = 6

DECLARE @PracticeLocationIDString VARCHAR(2000)


--  SET @EndDate to 11:59:59 PM
SELECT @EndDate = DATEADD(DAY, 1, @EndDate)


SET @PracticeLocationIDString = @PracticeLocationID

--DECLARE @PhysicianIDString VARCHAR(2000)
--SET @PhysicianIDString = @PhysicianID
---  Insert select here ----

SELECT 
		  SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.Physician

		, SUB.PatientCode

		, SUB.IsThirdPartySupplier
		, SUB.SupplierShortName

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
		ELSE SUB.BrandShortName
		END AS BrandShortName	
		
		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN 1
		ELSE 0
		END AS IsBrandSuppressed

		, SUB.Sequence	

		, SUB.ProductName
		, SUB.Side
		, SUB.Size
		, SUB.Gender
		, SUB.Code

		, SUB.DateDispensed
		, SUB.Quantity 		--, SUM( SUB.Quantity )

		, CAST( SUB.ActualWholesaleCost AS DECIMAL(15,2) )	AS ActualWholesaleCost
		, CAST( SUB.ActualChargeBilled AS DECIMAL(15,2) )	AS ActualChargeBilled
		, CAST( SUB.DMEDeposit AS DECIMAL(15,2) )			AS DMEDeposit

		, CAST(  SUB.ActualWholesaleCostLineTotal  AS DECIMAL(15,2) )	AS ActualWholesaleCostLineTotal
		, CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )   AS ActualChargeBilledLineTotal
		, CAST(  SUB.DMEDepositLineTotal  AS DECIMAL(15,2) )			AS DMEDepositLineTotal
		
		, SUB.Comment  -- Not Displayed for Version 1.
FROM


		(
		SELECT 

			  P.PracticeName
			, PL.Name AS PracticeLocation
			, IsNULL(RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix), '') AS Physician

			, D.PatientCode
			
			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, PCSB.Sequence

			, MCP.[Name] AS ProductName
			, ISNULL(MCP.LeftRightSide, '') AS Side
			, ISNULL(MCP.Size, '') AS Size
			, ISNULL(MCP.Gender, '') AS Gender
			, ISNULL(MCP.Code, '') AS Code

			, DD.CreatedDate AS DateDispensed

			, DD.Quantity
			, PI_DD.ActualWholesaleCost 
			, DD.ActualChargeBilled
			, DD.DMEDeposit	

			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit * DD.Quantity AS DMEDepositLineTotal
			, PI_DD.ActualWholesaleCost * DD.Quantity AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.

		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOin DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID 

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
		INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
			ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1

		WHERE P.PracticeID = @PracticeID		
		
		AND 
		PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))
		
--		AND 
--		Ph.PhysicianID IN 
--			(SELECT ID 
--			 FROM dbo.udf_ParseArrayToTable(@PhysicianIDString, ','))
	
		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM
			
		UNION

		SELECT 

			 P.PracticeName
			, PL.Name AS PracticeLocation
			, C.FirstName + ' ' + C.LastName AS Physician  --  Get proper line for this!!

			, D.PatientCode
			
			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, PCSB.Sequence + 100 AS Sequence

			, TPP.[Name] AS ProductName
			, ISNULL(TPP.LeftRightSide, '') AS Side
			, ISNULL(TPP.Size, '') AS Size
			, ISNULL(TPP.Gender, '') AS Gender
			, ISNULL(TPP.Code, '') AS Code

			, DD.CreatedDate AS DateDispensed

			, DD.Quantity
			, PI_DD.ActualWholesaleCost 
			, DD.ActualChargeBilled
			, DD.DMEDeposit	

			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit * DD.Quantity AS DMEDepositLineTotal
			, PI_DD.ActualWholesaleCost * DD.Quantity AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.

		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOin DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID 

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
		INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
			ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1	
	
		WHERE P.PracticeID = @PracticeID		
		
		AND 
		PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))
	
		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM	

		) AS Sub


















----
------GROUP BY
------		SUB.PracticeName
------		, SUB.PracticeLocation
------		, SUB.Physician
------		, SUB.IsThirdPartySupplier
------		, SUB.Sequence
------		, SUB.SupplierShortName
------		, SUB.BrandShortName
------		
------		, SUB.ProductName
------		, SUB.Side
------		, SUB.Size
------		, SUB.Gender
------		, SUB.Code
------		
------		, SUB.PatientCode
------
------		, SUB.DateDispensed
------
------		, SUB.ActualWholesaleCost
------		, SUB.ActualChargeBilled
------		, Sub.DMEDeposit
------		, Sub.Comment
----
----		
--	ORDER BY
--		SUB.PracticeName
--		, SUB.PracticeLocation
--
--		, SUB.Physician
--
--		, SUB.Sequence		--  Note 3rd Party has 100 added to it sequence for this query.
--		, SUB.IsThirdPartySupplier
--		, SUB.SupplierShortName
--		, SUB.BrandShortName
--
--		, SUB.ProductName
--		, SUB.Code
--		, SUB.Gender
--		, SUB.Side
--		, SUB.Size

END