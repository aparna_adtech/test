﻿create procedure forwardEmail(
	@orig_mailitem_id int,
	@new_recipients varchar(max)
)
as
begin
	set nocount on;

	declare @orig_subject nvarchar(255);
	declare @orig_body nvarchar(max);
	declare @orig_body_format varchar(20);
	declare @orig_file_attachments nvarchar(max);
	declare @new_mailitem_id int;

	if @orig_mailitem_id is null
	begin
		raiserror('No orig_mailitem_id specified', 18, -1);
		return;
	end

	if @new_recipients is null
	begin
		raiserror('No recipient specified', 18, -1);
		return;
	end

	select
		@orig_subject=[subject]
		, @orig_body=[body]
		, @orig_body_format=[body_format]
		, @orig_file_attachments=[file_attachments]
	from
		[msdb].[dbo].[sysmail_allitems]
	where
		[mailitem_id]=@orig_mailitem_id;

	exec msdb.dbo.sp_send_dbmail
		@profile_name=N'OrderConfirmation'
		, @recipients=@new_recipients
		, @subject=@orig_subject
		, @body=@orig_body
		, @body_format=@orig_body_format
		, @file_attachments=@orig_file_attachments
		, @mailitem_id=@new_mailitem_id OUTPUT
		;

	select @new_mailitem_id as mailitem_id;
end