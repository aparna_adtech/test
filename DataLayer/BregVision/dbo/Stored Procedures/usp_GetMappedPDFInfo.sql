﻿
CREATE PROCEDURE [dbo].[usp_GetMappedPDFInfo]   --18
	@IsNewOrdersVersion BIT = NULL,
	@BregVisionOrderID INT = NULL,
	@MappedPDFID INT = NULL,
	@MappedPDFName VARCHAR(50) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF (@IsNewOrdersVersion = 1)
	BEGIN
		SELECT @MappedPDFID = p.MappedPDFID 
		  FROM MappedPDF p
		 WHERE p.IsNewOrdersVersion = @IsNewOrdersVersion
	END
	ELSE IF (@BregVisionOrderID IS NOT NULL)
	BEGIN
		SELECT @MappedPDFID = sccb.MappedPDFID 
		  FROM dbo.ShoppingCartCustomBrace sccb
		 WHERE sccb.BregVisionOrderID = @BregVisionOrderID
	END
	ELSE IF (@MappedPDFName IS NOT NULL)
	BEGIN
		SELECT @MappedPDFID = p.MappedPDFID 
		  FROM MappedPDF p
		 WHERE p.Name = @MappedPDFName
	END
	
	IF (@MappedPDFID IS NULL)
	BEGIN
		SELECT @MappedPDFID = p.MappedPDFID 
		  FROM MappedPDF p
		 WHERE p.IsOriginalOrdersVersion = 1
	END


	SELECT * FROM dbo.MappedPDF p
	WHERE p.MappedPDFID = @MappedPDFID

	SELECT * FROM dbo.MappedPDFField f
	WHERE f.MappedPDFID = @MappedPDFID AND f.IsActive = 1
	ORDER BY f.HTMLFormOrder

END
