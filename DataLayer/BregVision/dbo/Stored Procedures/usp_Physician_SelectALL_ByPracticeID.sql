﻿CREATE PROCEDURE [dbo].[usp_Physician_SelectALL_ByPracticeID]
		@PracticeID INT
AS
BEGIN	
	SELECT
		Pr.PracticeID 
	  , Ph.PhysicianID
	  , C.ContactID
	  , C.Salutation
	  , C.FirstName
	  , C.MiddleName
	  , C.LastName
	  , C.Suffix
	  , Pin = '' --this is a write only field so we don't need to retrieve it
	  , Ph.SortOrder
	FROM
		dbo.Contact AS C
		INNER JOIN dbo.Physician AS Ph
			ON C.ContactID = Ph.ContactID
		INNER JOIN dbo.Practice AS Pr
			ON Ph.PracticeID = Pr.PracticeID
	WHERE
			Pr.PracticeID = @PracticeID
		AND Ph.IsActive = 1
		AND C.IsActive = 1
END



