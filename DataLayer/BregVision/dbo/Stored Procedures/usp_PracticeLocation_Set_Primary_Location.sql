﻿
/*
	Stored Procedure:  dbo.usp_PracticeLocation_Set_Primary_Location
	
	Set all other PracticeLocations to NOT be the 
	Primary Location.
	
	Input:  PrimaryLocationID
	Updates all other PrimaryLocation records in the
	  table PrimaryLocation table and sets there
	  column IsPrimaryLocation to false
	  
	 Author: John Bongiorni  Create Date: 07/23/2007  12:25 PM
	 
	 Note:  This is called from the procs:
					usp_PracticeLocation_Update
					usp_PracticeLocation_Insert
*/

CREATE PROCEDURE [dbo].[usp_PracticeLocation_Set_Primary_Location] 
		(@PracticeLocationID INT) 
AS
	BEGIN

		UPDATE dbo.PracticeLocation 
		SET dbo.PracticeLocation.IsPrimaryLocation = 0
		WHERE dbo.PracticeLocation.PracticeLocationID IN 
				(	
					SELECT PL.PracticeLocationID 
					FROM dbo.PracticeLocation AS PL
					WHERE
						PL.PracticeID = (
											SELECT PL2.PracticeID 
											FROM dbo.PracticeLocation AS PL2
											WHERE PL2.PracticeLocationID = @PracticeLocationID
										 ) 
						AND PL.IsPrimaryLocation = 1
						AND PL.IsActive = 1
						AND PL.PracticeLocationID <> @PracticeLocationID
				)
	END


