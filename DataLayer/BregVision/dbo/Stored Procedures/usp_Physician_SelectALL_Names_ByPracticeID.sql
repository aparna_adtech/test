﻿
CREATE PROCEDURE [dbo].[usp_Physician_SelectALL_Names_ByPracticeID]
		@PracticeID INT
AS
BEGIN	
	SELECT
		Pr.PracticeID 
	  , Ph.PhysicianID
	  , C.ContactID
--	  , C.Salutation
--	  , C.FirstName
--	  , C.MiddleName
--	  , C.LastName
--	  , C.Suffix
	  , RTRIM( C.FirstName + ' ' + C.LastName + ' ' +	C.Suffix) AS PhysicianName

	FROM
		dbo.Contact AS C
		INNER JOIN dbo.Physician AS Ph
			ON C.ContactID = Ph.ContactID
		INNER JOIN dbo.Practice AS Pr
			ON Ph.PracticeID = Pr.PracticeID
	WHERE
			Pr.PracticeID = @PracticeID
		AND Ph.IsActive = 1
		AND C.IsActive = 1
END



