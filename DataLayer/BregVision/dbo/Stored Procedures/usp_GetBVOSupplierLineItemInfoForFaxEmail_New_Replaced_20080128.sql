﻿--DECLARE @SupplierOrderID INT
--SET @SupplierOrderID = 201  --198   --204  --  198


--			string ShortName;
--            string Code;
--            string Packaging;
--            string Side;
--            string Color;
--            string Gender;
--            decimal ActualWholesaleCost;
--            Int32 QuantityOrdered;
--            decimal LineTotal;
--            string SupplierName;

/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_GetBVOSupplierLineItemInfoForFaxEmail_2
   
   Description:  Select the SupplierOrder Line Items for a given SupplierOrderID.
				Used to Get SupplierLineItemdetails for fax and emailed POS.
				Called by class:  FaxEmailPOs   					
					
   AUTHOR:       Greg Ross 08/29/07
   
   Modifications:  John Bongiorni 2007.11.07  13:55
			Make sure that only one record per product order is selected.
   
   Note:  The commented fields will work for testing and debugging.
			There are commented as they are not  neccessary for the query.
			If a product info field is null then pass back empty string.
			
   For Vendors the SupplierOrderID is sent in for the first brand of the Vendor.
   However, all	Supplier Order info should be pulled for the Vendor.
   TODO: Make this work.		
   ------------------------------------------------------------ */  

CREATE PROCEDURE [dbo].[usp_GetBVOSupplierLineItemInfoForFaxEmail_New_Replaced_20080128]
		@SupplierOrderID int
AS
BEGIN

	SELECT 
						
--				  SUB.SupplierOrderID			
--				, SUB.SupplierOrderLineItemID		
--				, SUB.ProductID
--
				SUB.PracticeCatalogSupplierBrandID AS SupplierID

				, SUB.SupplierShortName							 --  This is the Supplier
				, SUB.BrandShortName 			AS SupplierName  --  This is the Brand		

				, SUB.Product					AS ShortName
				, SUB.Code
				, ISNULL(SUB.Packaging, '')		AS Packaging
				, ISNULL(SUB.LeftRightSide, '')		AS Side
				, ISNULL(SUB.Size, '')			AS Size
				, ISNULL(SUB.Gender, '')		AS Gender
				, ISNULL(SUB.Color, '')			AS Color

				, SUB.ActualWholesaleCost
				, SUB.QuantityOrdered
				, SUB.LineTotal

			FROM

				(
					SELECT 
--							SOLI.SupplierOrderID,
--							SOLI.SupplierOrderLineItemID,
--							MCP.MasterCatalogProductID AS ProductID,
--							

							PCSB.PracticeCatalogSupplierBrandID,

							PCSB.SupplierShortName, 
				
							PCSB.BrandShortName, 
							MCP.[Name]			AS Product,      
							MCP.[Code]			AS Code,
							MCP.[Packaging]		AS Packaging,
							MCP.[LeftRightSide] AS LeftRightSide,
							MCP.[Size]			AS Size,
							MCP.[Gender]		AS Gender,
							MCP.[Color]			AS Color,
							
							SOLI.ActualWholesaleCost,
							SOLI.QuantityOrdered,
							SOLI.LineTotal

					FROM 
						dbo.SupplierOrderLineItem AS SOLI		WITH(NOLOCK)
						
					INNER JOIN PracticeCatalogProduct AS PCP		WITH(NOLOCK)
						ON SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
					
					INNER JOIN dbo.MasterCatalogProduct AS MCP		WITH(NOLOCK)
						ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
					
					INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK) 
						ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
					
					WHERE SOLI.SupplierOrderID = @SupplierOrderID
						AND PCP.IsThirdPartyProduct = 0	
						AND SOLI.IsActive = 1
						AND PCP.IsActive = 1
						AND MCP.IsActive = 1
						AND PCSB.IsActive = 1
						
				UNION

						SELECT 
							
--							SOLI.SupplierOrderID,
--							SOLI.SupplierOrderLineItemID,
--							TPP.ThirdPartyProductID AS ProductID,
--														

							PCSB.PracticeCatalogSupplierBrandID,
							PCSB.SupplierShortName, 
							PCSB.BrandShortName, 

							TPP.[Name]			AS Product,      
							TPP.[Code]			AS Code,
							TPP.[Packaging]		AS Packaging,
							TPP.[LeftRightSide] AS LeftRightSide,
							TPP.[Size]			AS Size,
							TPP.[Gender]		AS Gender,
							TPP.[Color]			AS Color,
							
							SOLI.ActualWholesaleCost,
							SOLI.QuantityOrdered,
							SOLI.LineTotal

					FROM 
						dbo.SupplierOrderLineItem AS SOLI	WITH(NOLOCK)
					
					INNER JOIN PracticeCatalogProduct AS PCP   WITH(NOLOCK)
						ON SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
				
					INNER JOIN dbo.ThirdPartyProduct AS TPP    WITH(NOLOCK)
						ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
						
					INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK) 
						ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
							
					WHERE SOLI.SupplierOrderID = @SupplierOrderID
						AND PCP.IsThirdPartyProduct = 1
						AND TPP.IsActive = 1
						AND SOLI.IsActive = 1
						AND PCSB.IsActive = 1

				) AS Sub
			
			ORDER BY 
				  SUB.Product	
				, SUB.Code


END