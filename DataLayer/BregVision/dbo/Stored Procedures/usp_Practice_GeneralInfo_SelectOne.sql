﻿/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Practice_GeneralInfo_SelectOne
   
   Description:  Selects one record from the table dbo.Practice 
					Input: PracticeID.
					Columns returned: PracticeName, IsBillingCentralized
					
   AUTHOR:       John Bongiorni 7/1/2007 12:45 PM
   
   Modifications:  
   ------------------------------------------------------------ */

CREATE PROCEDURE [dbo].[usp_Practice_GeneralInfo_SelectOne]
			  @PracticeID				INT		
			, @PracticeName				VARCHAR(50)	OUTPUT
			, @IsBillingCentralized		BIT			OUTPUT

AS
BEGIN
	DECLARE @Err INT
	
	SELECT
		  @PracticeName = PracticeName
		, @IsBillingCentralized = IsBillingCentralized
		
	FROM dbo.Practice
	
	WHERE IsActive = 1
		AND @PracticeID = PracticeID
	
	SET @Err = @@ERROR

	RETURN @Err
END

