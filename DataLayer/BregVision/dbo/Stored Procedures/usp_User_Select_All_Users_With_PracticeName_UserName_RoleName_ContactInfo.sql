﻿CREATE PROC [dbo].[usp_User_Select_All_Users_With_PracticeName_UserName_RoleName_ContactInfo]
AS
BEGIN

	SELECT 
			  P.PracticeID
			, P.PracticeName
			, U.UserID
     		, AU.UserName
     		, R.RoleName 
     		, c.ContactID
     		, C.FirstName
     		, C.LastName
     		, C.EmailAddress
     		
	FROM Practice AS P
	INNER JOIN [User] AS U
		ON P.PracticeID = U.PracticeID 
		
	INNER JOIN aspnetdb.dbo.aspnet_Users AS AU
		ON U.AspNet_UserID = AU.UserID	
		
	INNER JOIN aspnetdb.dbo.aspnet_UsersInRoles AS UR
		ON AU.UserID = UR.UserID
		
	INNER JOIN aspnetdb.dbo.aspnet_Roles AS R
		ON UR.RoleId = R.RoleId 
		
	INNER JOIN 
		aspnetdb.dbo.aspnet_Applications AS AA
			ON AU.ApplicationId = AA.ApplicationId
			
	LEFT JOIN
		Contact AS C
			ON U.ContactID = C.ContactID
			AND C.IsActive = 1		
			
	WHERE  AA.LoweredApplicationName = LOWER('/bregvision')
	--		AND LOWER(@UserName) = AU.LoweredUserName
		AND P.IsActive = 1
		AND U.IsActive = 1
		
--	ORDER BY
--		  P.PracticeName
--		, R.RoleName
--		, AU.UserName
--		
	ORDER BY
		  R.RoleName
		, P.PracticeName
		, AU.UserName
	
END