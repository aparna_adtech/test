﻿--  Get All ThirdPartyProduct Prep info for importing given the PracticeName
--  Example EXEC usp_zThirdPartyProduct_Select_PracticeID_TPSID_PCSBID_Given_PracticeName  


CREATE PROC [dbo].[usp_zThirdPartyProduct_Select_PracticeID_TPSID_PCSBID_Given_PracticeName_OLD_20071126]  --'Celebration Orthopedics'
				@PracticeName VARCHAR(50)
AS
BEGIN

DECLARE @SearchPracticeName VARCHAR(50)
SET @SearchPracticeName = '%' + @PracticeName + '%'


SELECT
		  P.PracticeID
		, P.PRacticeName AS Practice
		
		, CASE IsVendor WHEN 1 THEN 'Vendor' WHEN 0 THEN 'Manufacturer' END AS SupplierType
		, TPS.ThirdPartySupplierID
		, TPS.SupplierName AS Supplier

		, IsVendor		

		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierName
		, PCSB.SupplierShortName		
		, PCSB.BrandName
		, PCSB.BrandShortName
		
		, TPS.FaxOrderPlacement
	
	FROM dbo.ThirdPartySupplier AS TPS
	INNER JOIN Practice AS P
		ON P.PracticeID = TPS.PracticeID
	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB 
		ON PCSB.ThirdPartySupplierID = TPS.ThirdPartySupplierID
		AND PCSB.IsActive = 1

	WHERE 		
		P.IsActive = 1 
		AND TPS.IsActive = 1
		AND PCSB.IsActive = 1
		AND P.PracticeName LIKE @SearchPracticeName

	ORDER BY
		  P.PracticeName
		, TPS.IsVendor
		, TPS.SupplierName
		, PCSB.BrandName



END