﻿
/* ------------------------------------------------------------
   PROCEDURE:    usp_Practice_Insert
   
   Description:  Inserts a record into table 'Practice'
   
   AUTHOR:       John Bongiorni 7/10/2007 10:31 PM
   ------------------------------------------------------------ */

CREATE PROCEDURE [dbo].[usp_Practice_Insert_Original]
    @PracticeID int = null output,
    @PracticeName varchar(50),
    @IsBillingCentralized bit,
    @CreatedUserID int = 0

As
BEGIN
	DECLARE @Err Int

	INSERT
	INTO Practice
	(
          PracticeName
        , IsBillingCentralized
        , CreatedUserID   
	)
	VALUES
	(
          @PracticeName
        , @IsBillingCentralized
        , @CreatedUserID
	)

	Set @Err = @@Error
	Select @PracticeID = scope_identity()

	RETURN @Err
End


