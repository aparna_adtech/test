﻿CREATE PROC [dbo].[usp_Practice_Select_Five_ID_Name]
AS
BEGIN

	SELECT 
	    P.PracticeID
	  , P.PracticeName
	FROM dbo.Practice AS P
	WHERE P.IsActive = 1
		AND billingstartdate < '1/1/2008'
		AND PracticeID > 1
	ORDER BY
		P.PracticeName
	
		
END