﻿/*
	John Bongiorni
	2007.12.10

	TODO:  This should have a parameter to choose to display all practices or only the billable practices.

	Modifications:
		2007.12.11 JB Add: BVPC Product Code
		2008.01.04 JB Created from [usp_Report_BregExec_Transaction_By_Month5]
					derived column for IsBillable, Billing Start Date, Service Code  
			
					Remove restriction on all practice that are billable.
					Add ServiceCode Column from BregVisionMonthlyFee	
	
	LEFT JOIN dbo.BregVisionPracticeCode AS BVPC 
	ON P.PracticeID = BVPC.PracticeID

	EXEC usp_Report_BREGVision_Billing_Summary '2,3,4,5,10,6,7,8,9,11,12,13,14,15,16,17,18,19,20', 1, '1/9/2007', '12/31/2007'
	EXEC usp_Report_BREGVision_Billing_Summary '2,3,4,5,10,6,7,8,9,11,12,13,14,15,16,17,18,19,20', 0, '1/9/2007', '12/31/2007'
	
EXEC usp_Report_BREGVision_Billing_Summary @PracticeID, @IsNonBillableHidden, @StartDate, @EndDate    

update practiceTransactions and PracticeMonthlyFee
*/


CREATE PROC [dbo].[usp_Report_BREGVision_Billing_Summary_OLD_20100109_MWS]
		  @PracticeID AS VARCHAR(2000)
		, @IsNonBillableHidden AS BIT = 0
		, @StartDate  AS DateTime 
		, @EndDate    AS DateTime 

AS
BEGIN

	DECLARE @PracticeIDString AS VARCHAR(2000)
	SET @PracticeIDString = @PracticeID


	DECLARE @IsEmptyRowsWithinSuppressed AS BIT
	SET @IsEmptyRowsWithinSuppressed = 0

	DECLARE @MinimumTransactions AS INT
	SELECT @MinimumTransactions = 	
				(SELECT CASE WHEN @IsEmptyRowsWithinSuppressed = 0 THEN -1 
				WHEN @IsEmptyRowsWithinSuppressed = 1 THEN 0  
				ELSE -1
				END	AS MinimumTransactions)    


--	DECLARE @StartDate	AS DateTime
--	DECLARE @EndDate	AS DateTime
	DECLARE @StartYear	AS INT 
	DECLARE @EndYear	AS INT 

 
--	SET @StartDate = '9/6/2007' 
--	SET @EndDate = '12/31/2007'


	SET @StartYear	= Year(@StartDate)
	SET @EndYear	= Year(@EndDate);
	

	DECLARE @PracticesToBeBilledTable TABLE
		(
			  PracticeID		INT
			, PracticeName		VARCHAR(50)
			, BillingStartDate  DATETIME
			, IsPrimaryLocation BIT
			, PracticeLocation  VARCHAR(50)
			, Year				INT
			, Month				INT
			, YYYYMM			CHAR(6)	
			, QuantityDispensed INT
			, QuantityOrdered   INT
			, TotalTransactions INT	
			, IsMonthBillable	BIT
		);
	


	WITH                   Years
			  AS ( SELECT   YYYY = @StartYear
				   UNION ALL
				   SELECT   YYYY + 1
				   FROM     Years
				   Where    YYYY < @EndYear
				 ) ,
			Months
			  AS ( SELECT   MM = 1
				   UNION ALL
				   SELECT   MM + 1
				   FROM     Months
				   WHERE    MM < 12
				 ) 


	--SELECT * FROM Years
	--SELECT * FROM Months, Years

	INSERT INTO @PracticesToBeBilledTable
		(	
			  PracticeID		
			, PracticeName		
			, BillingStartDate

			, IsPrimaryLocation
			, PracticeLocation  
			, Year				
			, Month				

			, YYYYMM			
			, QuantityDispensed 
			, QuantityOrdered   
			, TotalTransactions 	
		
			, IsMonthBillable

		)
	SELECT 
		  P.PracticeID		
		, P.PracticeName
		, P.BillingStartDate

		, PL.IsPrimaryLocation
		, PL.Name AS PracticeLocation
		, YYYY
		, MM

		, Cast(YYYY as char(4)) + RIGHT('0' + CAST (MM as varchar(2)), 2) AS YYYYMM 
		, 0 AS QuantityDispensed 
		, 0 AS QuantityOrdered   
		, 0 AS TotalTransactions 
--		, Cast(YEAR(@StartDate) as char(4)) + RIGHT('0' + CAST (MONTH(@StartDate) as varchar(2)), 2) AS StartYYYYMM
--		, Cast(YEAR(@EndDate) as char(4)) + RIGHT('0' + CAST (MONTH(@EndDate) as varchar(2)), 2) AS EndYYYYMM
	
	, CASE WHEN 
		( 
			Cast(YYYY as char(4)) + RIGHT('0' + CAST (MM as varchar(2)), 2) 
				>= Cast(YEAR(P.BillingStartDate) as char(4)) + RIGHT('0' + CAST (MONTH(P.BillingStartDate) as varchar(2)), 2) 
		) THEN 1 ELSE 0
		END AS IsMonthBillable

	FROM Years, Months 
	CROSS JOIN Practice AS P
	INNER JOIN PracticeLocation AS PL
		ON P.PracticeID = PL.PracticeID
	WHERE P.PracticeID > 1  -- GET RID OF TEST PRACTICE!!!
	AND P.PracticeID <> 3   --  Do Not use OthroSelect  
      


    AND Cast(YYYY as char(4)) + RIGHT('0' + CAST (MM as varchar(2)), 2) >= Cast(YEAR(@StartDate) as char(4)) + RIGHT('0' + CAST (MONTH(@StartDate) as varchar(2)), 2) 
	AND Cast(YYYY as char(4)) + RIGHT('0' + CAST (MM as varchar(2)), 2) <= Cast(YEAR(@EndDate) as char(4)) + RIGHT('0' + CAST (MONTH(@EndDate) as varchar(2)), 2) 

	AND 
		PL.PracticeID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeIDString, ','))

	AND P.IsActive = 1
	AND PL.IsActive = 1

--  NOW DISPLAY ALL Practices regardless of if they are billable or not.  JB  2008.01.04	
--	AND Cast(YYYY as char(4)) + RIGHT('0' + CAST (MM as varchar(2)), 2) >= Cast(YEAR(P.BillingStartDate) as char(4)) + RIGHT('0' + CAST (MONTH(P.BillingStartDate) as varchar(2)), 2) 
	

	ORDER BY PracticeName, YYYYMM, IsPrimaryLocation, PracticeLocation
	

	--  If NonBillable Records are not to be displayed then delete them from the table variable.
	IF (@IsNonBillableHidden = 1)
	BEGIN
		DELETE
		FROM @PracticesToBeBilledTable
		WHERE YYYYMM < Cast(YEAR(BillingStartDate) as char(4)) + RIGHT('0' + CAST (MONTH(BillingStartDate) as varchar(2)), 2) 
		OR BillingStartDate IS NULL
	END
--	SELECT *
--	FROM @PracticesToBeBilledTable
--	WHERE YYYYMM >= Cast(YEAR(BillingStartDate) as char(4)) + RIGHT('0' + CAST (MONTH(BillingStartDate) as varchar(2)), 2) 
	
	

	--  Create Table Variable @TransactionTable
	DECLARE @QuantityOrderedTable TABLE
		(
			  PracticeID		INT
			, PracticeName		VARCHAR(50)
			, PracticeLocation  VARCHAR(50)
			, Year				INT
			, Month				INT
			, QuantityDispensed INT
			, QuantityOrdered   INT
			, TotalTransactions INT	
		)

	DECLARE @QuantityDispensedTable TABLE
		(
			  PracticeID		INT
			, PracticeName		VARCHAR(50)
			, PracticeLocation  VARCHAR(50)
			, Year				INT
			, Month				INT
			, QuantityDispensed INT
			, QuantityOrdered   INT
			, TotalTransactions INT
		)


	DECLARE @QuantityOrderQuantityDispensedTable TABLE
		(
			  PracticeID		INT
			, PracticeName		VARCHAR(50)
			, PracticeLocation  VARCHAR(50)
			, Year				INT
			, Month				INT
			, QuantityDispensed INT
			, QuantityOrdered   INT
			, TotalTransactions INT
		)
	

	INSERT INTO @QuantityOrderedTable
	(
		  PracticeID		
		, PracticeName		
		, PracticeLocation  
		, Year				
		, Month				
		, QuantityDispensed
		, QuantityOrdered 
		, TotalTransactions
	)
	SELECT 
		  P.PracticeID
		, P.PracticeName
		, PL.Name AS PracticeLocation
		, YEAR( SOLI.CreatedDate ) AS Year
		, MONTH( SOLI.CreatedDate )  AS Month
		, 0
		, SUM(SOLI.QuantityOrdered) AS QuantityOrdered
		, 0
	FROM
		Practice AS P
	INNER JOIN 
		PracticeLocation AS PL
			ON P.PracticeID = PL.PracticeID
	INNER JOIN 
		BregVisionOrder AS BVO
			ON PL.PracticeLocationID = BVO.PracticeLocationID
	INNER JOIN 
		SupplierOrder AS SO
			ON BVO.BregVisionOrderID = SO.BregVisionOrderID
	INNER JOIN
		SupplierOrderLineItem AS SOLI 
			ON SO.SupplierOrderID = SOLI.SupplierOrderID
	Group BY
		P.PracticeName
		, P.PracticeID
		, PL.Name
		, YEAR( SOLI.CreatedDate ) 
		, MONTH( SOLI.CreatedDate ) 
		--, P.PracticeID			--  JB Changed this.
	ORDER BY
		P.PracticeName
		, PL.Name	
		, YEAR( SOLI.CreatedDate ) 
		, MONTH( SOLI.CreatedDate ) 


	--
	INSERT INTO @QuantityDispensedTable
	(
			  PracticeID		
			, PracticeName		
			, PracticeLocation  
			, Year				
			, Month				
			, QuantityDispensed
			, QuantityOrdered 
			, TotalTransactions	
	)
	SELECT 
		  P.PracticeID		
		, P.PracticeName
		, PL.Name AS PracticeLocation
		, YEAR( D.CreatedDate ) AS Year
		, MONTH( D.CreatedDate )  AS Month
		, SUM(DD.Quantity) AS QuantityDispensed
		, 0
		, 0
	FROM
		Practice AS P
	INNER JOIN 
		PracticeLocation AS PL
			ON P.PracticeID = PL.PracticeID
	INNER JOIN 
		Dispense AS D
			ON PL.PracticeLocationID = D.PracticeLocationID
	INNER JOIN 
		DispenseDetail AS DD
			ON D.DispenseID = DD.DispenseID
	Group BY
		P.PracticeName
		, P.PracticeID
		, PL.Name
		, YEAR( D.CreatedDate ) 
		, MONTH( D.CreatedDate )
	ORDER BY
		P.PracticeName
		, PL.Name	
		, YEAR( D.CreatedDate ) 
		, MONTH( D.CreatedDate )  



	INSERT INTO @QuantityOrderQuantityDispensedTable 
		(
			  PracticeID		
			, PracticeName
			, PracticeLocation  
			, Year				
			, Month			
			, QuantityDispensed
			, QuantityOrdered
			, TotalTransactions
		)
	SELECT 
			  SUB.PracticeID		
			, SUB.PracticeName
			, SUB.PracticeLocation  
			, SUB.Year				
			, SUB.Month			
			, SUM( ISNULL(SUB.QuantityDispensed	, 0)) AS QuantityDispensed
			, SUM( ISNULL(SUB.QuantityOrdered	, 0)) AS QuantityOrdered
			, SUM( ISNULL(SUB.QuantityDispensed	, 0) + ISNULL(SUB.QuantityOrdered	, 0) ) AS TotalTransactions
	FROM
	(
	SELECT * FROM @QuantityDispensedTable
	UNION
	SELECT * FROM @QuantityOrderedTable
	) AS SUB
	GROUP BY SUB.PracticeName
			, SUB.PracticeID
			, SUB.PracticeLocation  
			, SUB.Year				
			, SUB.Month				
		


	--  INSERT INTO

--	SELECT * 
--	FROM @QuantityOrderedTable AS QOT
--	FULL OUTER JOIN @QuantityDispensedTable AS QDT
--		ON QOT.PracticeName = QDT.PracticeName 
--		AND QOT.PracticeLocation = QDT.PracticeLocation
--		AND QOT.Year = QDT.Year
--		AND QOT.Month = QDT.Month
--		
--	--  usp_Report_BregExec_Transaction_By_Month

--	SELECT * 
--FROM @PracticesToBeBilledTable AS PTBBT
--inner join @QuantityDispensedTable as QDT
--	ON PTBBT. = QDT. 
--	AND PTBBT. = Q
--	(
--			  PracticeName		
--			, PracticeLocation  
--			, Year				
--			, Month				
--			, QuantityDispensed
--			, QuantityOrdered 
--			, TotalTransactions	

--  select * from @QuantityOrderedTable
--  select * from @QuantityDispensedTable


	DECLARE @ReturnTable TABLE
		(	
			  PracticeID		INT
			, PracticeName		VARCHAR(50)
			, BillingStartDate	DATETIME
			, IsPrimaryPractice BIT
			, YYYYMM			VARCHAR(50)
			, MonthYearText		VARCHAR(50)
			, PracticeLocation	VARCHAR(50)
			, QuantityDispensed	INT
			, QuantityOrdered	INT
			, TotalTransactions	INT
			, MonthlyFee		Decimal(15,2)
			, IsMonthBillable	BIT
		)

	INSERT INTO @ReturnTable
		(
			  PracticeID		
			, PracticeName		
			, BillingStartDate	
			, IsPrimaryPractice
			, YYYYMM			
			, MonthYearText		
			, PracticeLocation	
			, QuantityDispensed	
			, QuantityOrdered	
			, TotalTransactions	
			, IsMonthBillable
		)
	SELECT 
		  SUB.PracticeID		
		, SUB.PracticeName
		, SUB.BillingStartDate

		, SUB.IsPrimaryLocation
		, SUB.YYYYMM
		, DATENAME(month, Cast((SUB.Month) as varchar(2)) + '/01/' +  Cast((SUB.Year) as varchar(4)) ) + ' '+ CAST(YEAR AS CHAR(4)) as MonthYearText
		--, DATENAME(month, ( (Cast(SUB.Month as varchar(2)) + '/01/' +  Cast(SUB.Year as varchar(4))) as monthText
		--, DATENAME(month, ((Cast(SUB.Month as varchar(2)) + '/01/' +  Cast(SUB.Year as varchar(4))) ) + ' '+ CAST(YEAR AS CHAR(4)) as MonthYearName
--		, SUB.Year
--		, SUB.Month
		
		, SUB.PracticeLocation
		, SUB.QuantityDispensed
		, SUB.QuantityOrdered
		, SUB.TotalTransactions
		, SUB.IsMonthBillable
	FROM
	(
	SELECT 
		  PTBBT.PracticeID
		, PTBBT.PracticeName
		, PTBBT.BillingStartDate
		, PTBBT.IsPrimaryLocation
		, PTBBT.YYYYMM
		, PTBBT.PracticeLocation
		, PTBBT.Year
		, PTBBT.Month
		, ISNULL( QOQDT.QuantityDispensed, 0) AS QuantityDispensed
		, ISNULL( QOQDT.QuantityOrdered, 0)   AS QuantityOrdered
		, ISNULL( QOQDT.TotalTransactions, 0) AS TotalTransactions
		, PTBBT.IsMonthBillable
	FROM @PracticesToBeBilledTable				AS PTBBT
	LEFT JOIN @QuantityOrderQuantityDispensedTable   AS QOQDT
		ON PTBBT.PracticeName = QOQDT.PracticeName
		AND PTBBT.PracticeLocation = QOQDT.PracticeLocation 
		AND PTBBT.Year = QOQDT.Year
		AND PTBBT.Month = QOQDT.Month
	) AS SUB
--	INNER JOIN dbo.BregVisionMonthlyFee AS BVMF
--		ON SUB.TotalTransactions >= BVMF.MinTransactions
--		AND SUB.TotalTransactions <= BVMF.MaxTransactions
	WHERE SUB.TotalTransactions > @MinimumTransactions
	ORDER BY
		  SUB.PracticeName
		, SUB.YYYYMM
		, SUB.IsPrimaryLocation
		, SUB.PracticeLocation


		
--  select * from @QuantityOrderQuantityDispensedTable 

--  select * from @PracticesToBeBilledTable



--       usp_Report_BregExec_Transaction_By_Month___NO_Parameters
DECLARE @MonthlyFee Table
	(
			  PracticeName		VARCHAR(50)
			, YYYYMM			VARCHAR(50)
			, MonthlyTransactions	INT
			, MonthlyFee		Decimal(15,2)
			, ServiceCode		VARCHAR(50)
	)
INSERT INTO @MonthlyFee
	(
			  PracticeName		
			, YYYYMM			
			, MonthlyTransactions	
	)
SELECT 
	PracticeName
	, YYYYMM	
	, SUM(TotalTransactions) AS MonthlyTransactions
FROM @ReturnTable
GROUP BY
	PracticeName
	, YYYYMM
ORDER BY
	PracticeName
	, YYYYMM

--
--
--SELECt 'a', BVMF.MonthlyFee
--FROM @MonthlyFee AS MF
--JOIN dbo.BregVisionMonthlyFee AS BVMF
--	ON MF.MonthlyTransactions >= BVMF.MinTransactions
--    AND MF.MonthlyTransactions <= BVMF.MaxTransactions

--select * from BregVisionMonthlyFee
--) AS SUB
--WHERE
--	 PracticeName = SUB.PracticeName		
--	, YYYYMM = sub.YYYYMM	
--	, MonthlyTransactions = sub.MonthlyTransactions




UPDATE @MonthlyFee
SET MonthlyFee = (Round(BVMF.MonthlyFee - Round((BVMF.MonthlyFee * P_MFL.Discount), 2), 2))
	, ServiceCode = BVMF.ServiceCode
--FROM
--(SELECT 
----		  MF.PracticeName		
----		, MF.YYYYMM	
----		, MF.MonthlyTransactions
--		 BVMF.MonthlyFee	
FROM @MonthlyFee AS MF
JOIN dbo.BregVisionMonthlyFee AS BVMF
	ON MF.MonthlyTransactions >= BVMF.MinTransactions
    AND MF.MonthlyTransactions <= BVMF.MaxTransactions
Inner Join dbo.BregVisionMonthlyFeeLevel BVMFL
	ON BVMFL.BregVisionMonthlyFeeLevelID = BVMF.BregVisionMonthlyFeeLevelID
Inner Join Practice_MonthlyFeeLevel P_MFL
	ON P_MFL.BregVisionMonthlyFeeLevelID = BVMFL.BregVisionMonthlyFeeLevelID
	AND P_MFL.PracticeID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeIDString, ','))
WHERE P_MFL.StartDate = (Select Max(pMFL.StartDate) from Practice_MonthlyFeeLevel pMFL where pMFL.PracticeID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeIDString, ',')) 
			 AND pMFL.StartDate <= Convert(DateTime ,(Substring(MF.YYYYMM,5,2) + '/01/'  +  Left(MF.YYYYMM,4))))

--  SELECT * from @MonthlyFee

--select * from BregVisionMonthlyFee
--) AS SUB
--WHERE
--	 PracticeName = SUB.PracticeName		
--	, YYYYMM = sub.YYYYMM	
--	, MonthlyTransactions = sub.MonthlyTransactions

--SELECT * FROM @MonthlyFee

SELECT 

		  RT.PracticeName		
		, RT.YYYYMM			
		, RT.MonthYearText		
		, RT.PracticeLocation	
		, RT.QuantityDispensed	
		, RT.QuantityOrdered	
		, RT.TotalTransactions	
		, CASE WHEN RT.IsPrimaryPractice = 1 THEN MF.MonthlyTransactions else 0 END AS MonthlyTransactions


		, CASE WHEN RT.IsPrimaryPractice = 1 THEN MF.MonthlyFee else 0 END AS MonthlyFee	--  MonthlyFeeDisplay

		, CASE WHEN (RT.IsPrimaryPractice = 1 AND RT.IsMonthBillable = 1) THEN MF.MonthlyFee else 0 END AS MonthlyFeeBillable	--  MonthlyFeeBillable
		, CASE WHEN (RT.IsPrimaryPractice = 1 AND RT.IsMonthBillable = 0) THEN MF.MonthlyFee else 0 END AS MonthlyFeeNonBillable	--  MonthlyFeeNonBillable


	
		, CASE WHEN RT.IsPrimaryPractice = 1 THEN BVPC.PracticeCode ELSE '' END AS PracticeCode
		, CASE WHEN RT.IsPrimaryPractice = 1 THEN MF.ServiceCode ELSE 'N/A' END AS ServiceCode    

		, CASE	WHEN (RT.IsPrimaryPractice = 1 AND RT.IsMonthBillable = 1)  THEN MF.ServiceCode 
				WHEN (RT.IsPrimaryPractice = 1 AND RT.IsMonthBillable = 0) THEN 'Not Billable'
				ELSE '' 	
		  END AS ServiceCodeDisplay    

		, CASE WHEN RT.IsPrimaryPractice = 1 THEN CONVERT(VARCHAR(20), RT.BillingStartDate, 101) ELSE '' END AS BillingStartDate
		
		, CASE WHEN RT.IsPrimaryPractice = 1 THEN RT.IsMonthBillable ELSE -1 END AS IsMonthBillablePrimaryPractice
		
		-- Display the billable amount.
		, CASE WHEN (RT.IsPrimaryPractice = 1 AND RT.IsMonthBillable = 1) THEN 1 ELSE 0 END AS IsMonthBillableAndDisplayed
		--  Display the non billable amount thaty would be billed.
		, CASE WHEN (RT.IsPrimaryPractice = 1 AND RT.IsMonthBillable = 0) THEN 1 ELSE 0 END AS IsMonthNotBillableAndDisplayed
		
		, RT.IsPrimaryPractice		AS IsPrimaryPractice
		, RT.IsMonthBillable		AS IsMonthBillable
		

FROM @ReturnTable		AS RT
INNER JOIN @MonthlyFee	AS MF
	ON RT.PracticeName = MF.PracticeName
	AND RT.YYYYMM = MF.YYYYMM
--	AND RT.IsPrimaryPractice = 1
LEFT JOIN dbo.BregVisionPracticeCode AS BVPC 
	ON RT.PracticeID = BVPC.PracticeID

ORDER BY
	  RT.PracticeName		
	, RT.YYYYMM	
	, RT.IsPrimaryPractice DESC
	, RT.PracticeLocation





--SELECT 
--		  RT.PracticeName		
----		, RT.YYYYMM			
----		, RT.MonthYearText		
----		, RT.PracticeLocation	
----		, RT.QuantityDispensed	
----		, RT.QuantityOrdered	
----		, RT.TotalTransactions	
--		, SUM(MF.MonthlyTransactions)	
--		, SUM(MF.MonthlyFee)		
--FROM @ReturnTable		AS RT
--INNER JOIN @MonthlyFee	AS MF
--	ON RT.PracticeName = MF.PracticeName
--	AND RT.YYYYMM = MF.YYYYMM
--GROUP By
--	RT.PracticeName


END	