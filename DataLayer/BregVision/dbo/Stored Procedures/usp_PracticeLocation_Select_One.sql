﻿

/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocation_Select_One
   
   Description:  Selects a PracticeLocation record for a given PracticeLocationID.
				Select a record from the table dbo.PracticeLocation
				given the PracticeLocationID.
   
   AUTHOR:       John Bongiorni 7/23/2007 11:05:42 AM
   
   Modifications:  
   ------------------------------------------------------------ */  

CREATE PROCEDURE [dbo].[usp_PracticeLocation_Select_One]
			( @PracticeLocationID INT )
AS 
    BEGIN
        DECLARE @Err INT
        SELECT
        
            PL.PracticeLocationID
          , PL.PracticeID
          , PL.Name
          , PL.IsPrimaryLocation
          , PL.EmailForOrderApproval
          , PL.EmailForLowInventoryAlerts
          , PL.IsEmailForLowInventoryAlertsOn
        
        FROM
            dbo.PracticeLocation AS PL
        
        WHERE
            PL.IsActive = 1
            AND PL.PracticeLocationID = @PracticeLocationID
                
        SET @Err = @@ERROR

        RETURN @Err
    END


