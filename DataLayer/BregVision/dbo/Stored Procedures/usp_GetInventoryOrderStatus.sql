﻿
-- =============================================
-- Author:		<Author,,Greg Ross>
-- Create date: <07/19/07>
-- Description:	<Proc to get order, reorder critical status on items to display on the Breg Vision dashboard>
--  Modification:  
--          20071016 John Bongiorni 
--					Rewritten to correct faulty logic.
--					Add ThirdPartyProducts.
--			SELECT * FROM pROductinventory where practicelocationid = 3 and isactive = 1
--			2007.11.14 John Bongiorni
--				Add LeftRightSide and Size columns.

--	2008.01.28  JB  Test No Flagging for home page when the ParLevel is set to zero.
--  Remove if this does not work.
--  2008.02.05  John Bongiorni
--	Use the column ProductInventory.isNotFlaggedForReorder to determine if the product is highlighted on the home InvOrderStatus grid.
--  as critical or reorder.  ProductInventory.isNotFlaggedForReorder is derived from the udf.
--  2008.02.15  John Bongiorni  update:
--				(
--					IsNotFlaggedForReorder = 0		--  2008.02.05 JB
--					OR
--					(ParLevel > 0)	--  2008.01.28  JB  Test No Flagging for home page  -- Can Remove This after release on 2008.02.06!!!
--				)
-- Exec [usp_GetInventoryOrderStatus] 6, 296
-- 12/31/2011  Removed UPCCode Column and put in a separate Table
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetInventoryOrderStatus]  --  1,3   --6, 18  --1,3 
	-- Add the parameters for the stored procedure here
	@PracticeID INT = 0 
	,@PracticeLocationID INT
	
--SELECT * FROM dbo.PracticeLocation AS pl  --6;  24 18

AS 
BEGIN


--DECLARE @PracticeID INT
--DECLARE @PracticeLocationID INT

--SET @PracticeID = 1 ;
--SET @PracticeLocationID = 3 ;

DECLARE @WorkingTable TABLE
    (
      PracticeCatalogProductID INT,
      ProductInventoryID INT,
      MCName				VARCHAR(100),
      Code					VARCHAR(50),
      UPCCode				VARCHAR(50),
      LeftRightSide			VARCHAR(10),
	  Size					VARCHAR(50),
      SupplierName			VARCHAR(110),

      IsThirdPartySupplier	BIT,
      SupplierSequence		INT,

      --QuantityNotFullyCheckin INT DEFAULT 0,
      --QuantityCheckIn INT DEFAULT 0,
      --QuantityBaseLine INT DEFAULT 0,

      QuantityOnHand		INT DEFAULT 0,
      QuantityOnOrder		INT DEFAULT 0,
      QuantityOnHandPlusOnOrder INT DEFAULT 0,

      ParLevel				INT DEFAULT 0,
      ReorderLevel			INT DEFAULT 0,
      CriticalLevel			INT DEFAULT 0,
      IsNotFlaggedForReorder BIT DEFAULT 0,

      SuggestedReorderLevel	INT DEFAULT 0,

      Priority				INT DEFAULT 4,
	  PriorityWithCart		INT DEFAULT 5,
	  StockingUnits         int default 1,
	  IsShoppingCart        BIT NOT NULL DEFAULT 0,
	  
	  OldMasterCatalogProductID		INT NULL DEFAULT NULL,
	  OldThirdPartyProductID		INT NULL DEFAULT NULL,
	  NewMasterCatalogProductID		INT NULL DEFAULT NULL,
	  NewThirdPartyProductID		INT NULL DEFAULT NULL,
	  NewPracticeCatalogProductID	INT NULL DEFAULT NULL,
	  NewProductCode				VARCHAR(50) NULL DEFAULT NULL,
	  NewProductName				VARCHAR(100) NULL DEFAULT NULL,
	  NewPracticeID			INT NULL DEFAULT NULL,

	  IsConsignment			BIT NOT NULL DEFAULT 0,
	  ConsignmentQuantity	INT NOT NULL DEFAULT 0,
	  
	  DMEDeposit			SMALLMONEY NOT NULL DEFAULT 0,
	  MasterCatalogProductID	INT,
	  ThirdPartyProductID	INT,
	  BillingCharge			SMALLMONEY NOT NULL DEFAULT 0,
	  BillingChargeCash		SMALLMONEY NOT NULL DEFAULT 0,
	  Gender				VARCHAR(10),
	  Color					VARCHAR(50),
	  Packaging				VARCHAR(50),
	  WholesaleCost			SMALLMONEY NOT NULL DEFAULT 0,
      IsSplitCode			BIT NOT NULL DEFAULT 0,
	  IsCustomBrace			BIT NOT NULL DEFAULT 0,
	  IsAlwaysOffTheShelf	BIT NOT NULL DEFAULT 0,
	  Hcpcs					VARCHAR(50),
	  HcpcsOffTheShelf		VARCHAR(50),
	  IsPreAuthorization    BIT NOT NULL DEFAULT 0,
	  IsLateralityExempt    BIT NULL DEFAULT 0,
	  Mod1					VARCHAR(2),
	  Mod2					VARCHAR(2),
	  Mod3					VARCHAR(2),
	  IsActive				BIT
    )
	

	DECLARE @PracticeLocationPracticeID AS INTEGER
	SELECT @PracticeLocationPracticeID = PracticeID
	FROM PracticeLocation
	WHERE PracticeLocationID = @PracticeLocationID

/*Get items in PCProductIDs that are in inventory*/


		-- 1.  Insert the PracticeCatalogProductID for the location's products that are in inventory and active.
		
		INSERT  INTO @WorkingTable
        (
				PracticeCatalogProductID,
				ProductInventoryID,
				StockingUnits,
				IsSplitCode,
				DMEDeposit,
				MasterCatalogProductID,
				ThirdPartyProductID,
				BillingCharge,
				BillingChargeCash,
				IsAlwaysOffTheShelf,
				IsPreAuthorization,
				IsLateralityExempt,
				IsShoppingCart,
				IsConsignment,
				ConsignmentQuantity,
				Hcpcs,
				HcpcsOffTheShelf,
				Mod1,
				Mod2,
				Mod3,
				IsActive,
				OldMasterCatalogProductID,
				OldThirdPartyProductID,
				NewMasterCatalogProductID,
				NewThirdPartyProductID,
				NewPracticeCatalogProductID,
				NewProductCode,
				NewProductName,
				NewPracticeID
        )
        SELECT  
				PI.PracticeCatalogProductID,
				PI.ProductInventoryID,
				PCP.StockingUnits,
				PCP.IsSplitCode,
				PCP.DMEDeposit,
				PCP.MasterCatalogProductID,
				PCP.ThirdPartyProductID,
				PCP.BillingCharge,
				PCP.BillingChargeCash,
				PCP.IsAlwaysOffTheShelf,
				PCP.IsPreAuthorization,
				PCP.IsLateralityExempt,
				CASE WHEN (SCI.ShoppingCartItemID is null) THEN 0 ELSE 1 END,
				PI.IsConsignment,
				PI.ConsignmentQuantity,
				dbo.ConcatHCPCS(PI.PracticeCatalogProductID),
				dbo.ConcatHCPCSOTS(PI.PracticeCatalogProductID),
				PCP.Mod1,
				PCP.Mod2,
				PCP.Mod3,
				PCP.IsActive,
				PRP.OldMasterCatalogProductID,
				PRP.OldThirdPartyProductID,
				PRP.NewMasterCatalogProductID,
				PRP.NewThirdPartyProductID,
				MAX(CASE WHEN (NPI.PracticeLocationID IS NOT NULL) THEN PRP.NewPracticeCatalogProductID ELSE 0 END),
				PRP.NewProductCode,
				PRP.NewProductName,
				PRP.PracticeID

        FROM    
                dbo.ProductInventory AS PI  WITH(NOLOCK)
                INNER JOIN dbo.PracticeCatalogProduct AS PCP  WITH(NOLOCK) ON PCP.PracticeCatalogProductID = PI.PracticeCatalogProductID
				INNER JOIN PracticeCatalogSupplierBrand PCSB ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
				LEFT JOIN ShoppingCart SC ON SC.[PracticeLocationID] = @PracticeLocationID
				LEFT JOIN ShoppingCartItem SCI ON SC.[ShoppingCartID] = SCI.[ShoppingCartID] AND PCP.PracticeCatalogProductID = SCI.PracticeCatalogProductID
				LEFT JOIN ProductReplacementProduct PRP ON PRP.PracticeCatalogProductID = PCP.PracticeCatalogProductID AND PRP.IsActive = 1
				LEFT JOIN ProductInventory NPI ON NPI.PracticeCatalogProductID = PRP.NewPracticeCatalogProductID AND NPI.PracticeLocationID = @PracticeLocationID
				
        WHERE   
                PI.PracticeLocationID = @PracticeLocationID	
--				AND (PRP.PracticeID = @PracticeLocationPracticeID OR PRP.PracticeID IS NULL)
                AND PI.IsActive = 1
                AND PCP.IsActive = 1
				AND PCSB.isactive=1
         GROUP BY 
				PI.ProductInventoryID,
                PI.PracticeCatalogProductID,
				PCP.StockingUNits,
				PCP.IsSplitCode,
				PCP.DMEDeposit,
				PCP.MasterCatalogProductID,
				PCP.ThirdPartyProductID,
				PCP.BillingCharge,
				PCP.BillingChargeCash,
				PCP.IsAlwaysOffTheShelf,
				PCP.IsPreAuthorization,
				PCP.IsLateralityExempt,
				SCI.ShoppingCartItemID,
				PI.IsConsignment,
				PI.ConsignmentQuantity,
				PCP.Mod1,
				PCP.Mod2,
				PCP.Mod3,
				PCP.IsActive,
				PRP.OldMasterCatalogProductID,
				PRP.OldThirdPartyProductID,
				PRP.NewMasterCatalogProductID,
				PRP.NewThirdPartyProductID,
				CASE WHEN (NPI.PracticeLocationID IS NOT NULL) THEN PRP.NewPracticeCatalogProductID ELSE NULL END,
				PRP.NewProductCode,
				PRP.NewProductName,
				PRP.PracticeID


		--  2. GetS QuAntity on Hand, Quntity on Order, Inventory Counts, and Suggested Reorder Level        
         UPDATE @WorkingTable		
		 SET   
			  WT.QuantityOnHand = Levels.QuantityOnHand
			, WT.QuantityOnOrder = Levels.QuantityOnOrder
			, WT.QuantityOnHandPlusOnOrder = Levels.QuantityOnHand + (Levels.QuantityOnOrder * WT.StockingUnits)

			, WT.Priority = Levels.Priority
			, WT.PriorityWithCart = Levels.PriorityWithCart
			, WT.ParLevel = Levels.ParLevel
			, WT.ReorderLevel = Levels.ReorderLevel
			, WT.CriticalLevel = Levels.CriticalLevel	
			
			, WT.IsNotFlaggedForReorder = Levels.IsNotFlaggedForReorder 			--  2008.02.05 JB
			
			, WT.SuggestedReorderLevel = Levels.SuggestedReorderLevel
			
		 FROM @WorkingTable AS WT 
		 INNER JOIN dbo.udf_GetSuggestedReorder_Given_PracticeLocationID(@PracticeLocationID)	AS Levels
				ON WT.PracticeCatalogProductID = Levels.PracticeCatalogProductID


		Update @WorkingTable
		SET PriorityWithCart = 3
		Where IsShoppingCart = 1

		
				
--------------------------------------------------------------------------------------------------------------------------
		-- Update ProductName, Product Code, Side, Size, and Supplier Name for Master Catalog Products.

		UPDATE  @WorkingTable
		SET     WT.MCName = Positive.MCName,
				WT.Code = Positive.Code,
				WT.LeftRightSide = Positive.LeftRightSide,
				WT.Size  = Positive.Size,
				WT.SupplierName = Positive.SupplierName,
				WT.IsThirdPartySupplier = Positive.IsThirdPartySupplier,
				WT.SupplierSequence = Positive.SupplierSequence,
				WT.UPCCode = Positive.UPCCode,
				WT.Gender = Positive.Gender,
				WT.Color = Positive.Color,
				WT.Packaging = Positive.Packaging,
				WT.WholesaleCost = Positive.WholesaleCost,
				WT.IsCustomBrace = Positive.IsCustomBrace

				  -- Sets this so this will be ordered third in the grid
		FROM    @WorkingTable AS WT
        INNER JOIN (SELECT PCP.PracticeCatalogProductid AS PracticeCatalogProductid,
                            MCP.Name			AS MCNAME,
                            MCP.Code			AS Code,
                            UPCCode = (Select Top 1 upc.Code from UPCCode upc where upc.MasterCatalogProductID = MCP.MasterCatalogProductID),
                            MCP.LeftRightSide	AS LeftRightSide,
                            MCP.Size			AS Size,
                            MCS.SupplierName	AS SupplierName,
                            0 AS IsThirdPartySupplier,
							ISNULL(PCSB.[Sequence], 999) AS SupplierSequence,
                            MCP.Gender			AS Gender,
                            MCP.Color			AS Color,
                            MCP.Packaging		AS Packaging,
                            MCP.WholesaleListCost	AS WholesaleCost,
							MCP. IsCustomBrace AS IsCustomBrace
                            
                     FROM   dbo.PracticeCatalogProduct AS PCP
                            
                            INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
								ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
                            
                            INNER JOIN dbo.MasterCatalogProduct AS MCP 
								ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
                            
                            INNER JOIN dbo.MasterCatalogSubCategory AS MCSC 
								ON MCP.MasterCatalogSubCategoryID = MCSC.MasterCatalogSubCategoryID
                            
                            INNER JOIN dbo.MasterCatalogCategory AS MCC 
								ON MCSC.MasterCatalogCategoryID = MCC.MasterCatalogCategoryID
                            
                            INNER JOIN dbo.MasterCatalogSupplier AS MCS 
								ON MCC.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
                     
                     WHERE  
								PCP.IsActive = 1
                            AND PCP.IsThirdPartyProduct = 0  -- Master Catalog Product
                            AND MCP.IsActive = 1
                            AND MCC.IsActive = 1
                            AND MCSC.IsActive = 1
                            AND MCS.IsActive = 1

							And PCSB.Isactive=1
                   
                   ) AS Positive 
						ON WT.PracticeCatalogProductid = Positive.PracticeCatalogProductid
			

		-- Update ProductName, Product Code, and Supplier Name for Third Party Products.

		UPDATE  @WorkingTable
		SET     WT.MCName = Positive.MCName,
				WT.Code = Positive.Code,
				WT.UPCCode = Positive.UPCCode,
				WT.LeftRightSide = Positive.LeftRightSide,
				WT.Size  = Positive.Size,
				WT.SupplierName = Positive.SupplierName,
				WT.IsThirdPartySupplier = Positive.IsThirdPartySupplier,
				WT.SupplierSequence = Positive.SupplierSequence
				  -- Sets this so this will be ordered third in the grid
		FROM    @WorkingTable AS WT
        INNER JOIN (SELECT PCP.PracticeCatalogProductid AS PracticeCatalogProductid,
                            TPP.Name AS MCNAME,
                            TPP.Code AS Code,
                            TPP.UPCCode AS UPCCode,
                            TPP.LeftRightSide	AS LeftRightSide,
                            TPP.Size			AS Size,
                            CASE WHEN (PCSB.IsThirdPartySupplier = 1) AND (PCSB.SupplierShortName <> PCSB.BrandShortName) 		
							THEN PCSB.SupplierShortName + ' - ' + PCSB.BrandShortName 
							ELSE PCSB.SupplierShortName END AS SupplierName,
							1 AS IsThirdPartySupplier,
							ISNULL(PCSB.[Sequence], 999) AS SupplierSequence

				
                            --PCSB.SupplierName AS SupplierName
                     FROM   
							dbo.PracticeCatalogProduct AS PCP
                            
                            INNER JOIN dbo.ThirdPartyProduct AS TPP 
								ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
                            
                            INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
								ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
                     
                     WHERE  
								PCP.IsActive = 1
                            AND TPP.IsActive = 1
							AND PCSB.Isactive=1
                            AND PCP.IsThirdPartyProduct = 1  -- Third Party Product
                   
                   ) AS Positive 
						ON WT.PracticeCatalogProductid = Positive.PracticeCatalogProductid
			
---------------------------------------------------------------------------------------------------------------			

SELECT
	OldMasterCatalogProductID,
	OldThirdPartyProductID
INTO #WorkingTablePracticeSpecific
FROM @WorkingTable
WHERE NewPracticeID IS NOT NULL


SELECT
    WT.PracticeCatalogProductID
  , WT.ProductInventoryID
  , WT.MCName
  , WT.Code
  , WT.UPCCode
  , WT.LeftRightSide
  , WT.Size
  , WT.SupplierName
  , WT.IsThirdPartySupplier
  , WT.SupplierSequence
  , WT.QuantityOnHand
  , WT.QuantityOnOrder
  , WT.QuantityOnHandPlusOnOrder
  , WT.ParLevel AS Parlevel
  , WT.ReorderLevel AS ReorderLevel
  , WT.CriticalLevel AS CriticalLevel
  , WT.SuggestedReorderLevel
  , WT.Priority
  , WT.PriorityWithCart
  , WT.StockingUnits
  , WT.IsConsignment
  , WT.ConsignmentQuantity
  , WT.DMEDeposit
  , WT.MasterCatalogProductID
  , WT.ThirdPartyProductID
  , WT.BillingCharge
  , WT.BillingChargeCash
  , WT.Gender
  , WT.Color
  , WT.Packaging
  , WT.WholesaleCost
  , WT.IsSplitCode
  , WT.IsShoppingCart
  , WT.IsCustomBrace
  , WT.IsAlwaysOffTheShelf
  , WT.Hcpcs
  , WT.HcpcsOffTheShelf
  , WT.IsPreAuthorization
  , WT.IsLateralityExempt
  , WT.Mod1
  , WT.Mod2
  , WT.Mod3
  , WT.IsActive
  , WT.IsNotFlaggedForReorder
  , WT.OldMasterCatalogProductID
  , WT.OldThirdPartyProductID
  , WT.NewMasterCatalogProductID
  , WT.NewThirdPartyProductID
  , WT.NewPracticeCatalogProductID
  , WT.NewProductCode
  , WT.NewProductName
  , WT.NewPracticeID
FROM
    @WorkingTable WT
WHERE NOT EXISTS (
	SELECT * FROM #WorkingTablePracticeSpecific WTPS
	WHERE
		COALESCE(WT.OldMasterCatalogProductID, 0) = COALESCE(WTPS.OldMasterCatalogProductID, 0) AND
		COALESCE(WT.OldThirdPartyProductID, 0) = COALESCE(WTPS.OldThirdPartyProductID, 0) AND
        WT.NewPracticeID IS NULL
		)
ORDER BY
	PriorityWithCart
  , Priority
  , SupplierName
  , MCName
  , Code
  , LeftRightSide
  , Size
  , IsNotFlaggedForReorder
  , IsThirdPartySupplier
  , SupplierSequence
  

        
END
