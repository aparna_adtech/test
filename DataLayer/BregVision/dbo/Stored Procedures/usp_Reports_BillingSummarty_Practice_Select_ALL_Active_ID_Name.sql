﻿CREATE PROC [dbo].[usp_Reports_BillingSummarty_Practice_Select_ALL_Active_ID_Name]
AS
BEGIN

	SELECT 
	    P.PracticeID
	  , P.PracticeName
	FROM dbo.Practice AS P
	WHERE P.IsActive = 1
	AND P.PracticeID NOT IN (1, 3, 9)
	ORDER BY
		P.PracticeName
	
		
END