﻿-- =============================================
-- Author:		<Author Greg Ross>
-- Create date: <Create 10/28/08>
-- Description:	<Description Used to show Breg Vision orders for a specified date range>
-- =============================================


CREATE PROCEDURE dbo.usp_Report_POs 
	-- Add the parameters for the stored procedure here
	@StartDate datetime,
	@EndDate datetime
	
AS
BEGIN
	SET NOCOUNT ON;

	select BVO.CreatedDate, 
	BVO.PurchaseOrder,
	P.PracticeName,
	PL.Name as LocationName,
	PC.PracticeCode,
	BVO.CustomPurchaseOrderCode as CustomPurchaseOrder
	 from bregvisionorder BVO
	inner join practicelocation PL on 
	BVO.PracticelocationID = PL.PracticeLocationID
	inner join Practice P on
	PL.PracticeID = P.PracticeID
	inner join BregVisionPracticeCode PC on
	P.PracticeID = PC.PracticeID
	inner join SupplierOrder SO on
	BVO.BregVisionOrderID = SO.BregVisionOrderID
	inner join PracticeCatalogSupplierBrand PCSB on
	SO.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID


	where BVO.createddate >= @StartDate
	and BVO.createddate <= @EndDate
	and PCSB.IsThirdPartySupplier=0 
	and Upper(PCSB.SupplierName) = 'BREG'

END
