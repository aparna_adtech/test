﻿-- =============================================
-- Author:		Greg Ross
-- Create date: Nov. 19th 2007
-- Description:	Gets shipping type description via a shipping type id
-- =============================================
CREATE PROCEDURE dbo.usp_GetShippingType 

	@ShippingTypeID int
AS
BEGIN

	SET NOCOUNT ON;
	
	Select ShippingTypeName from ShippingType
	where ShippingTypeID = @ShippingTypeID

END
