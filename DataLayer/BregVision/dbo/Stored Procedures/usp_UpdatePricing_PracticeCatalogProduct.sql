﻿

-- =============================================
-- Author:		Greg Ross
-- Create date: 10/01/07
-- Description:	Updates all pricing informationin the Practice Catalog for a given practice
-- =============================================
CREATE PROCEDURE [dbo].[usp_UpdatePricing_PracticeCatalogProduct]
	-- Add the parameters for the stored procedure here
	@practiceCatalogProductID int,
	@WholesaleCost smallmoney,
	@BillingCharge smallmoney,
	@BillingChargeCash smallmoney,
	@DMEDeposit smallmoney,
	@Mod1 Varchar(2),
	@Mod2 Varchar(2),
	@Mod3 Varchar(2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

update practiceCatalogproduct
set WholesaleCost = @WholesaleCost,
	BillingCharge = @BillingCharge,
	BillingChargeCash = @BillingChargeCash,
	DMEDeposit = @DMEDeposit,
	Mod1 = @Mod1,
	Mod2 = @Mod2,
	Mod3 = @Mod3,
	UsePCPMods = 1

where practiceCatalogProductID = @practiceCatalogProductID

END
