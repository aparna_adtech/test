﻿

/* ------------------------------------------------------------
   PROCEDURE:    [dbo].[usp_GetAllInventoryDataforPCToIventory_ByBrand_With_ThirdPartyProducts]
   
   Description:  Selects products (mc and 3rdParty) that are in inventory given a Suppliername
					or shortSuppliername
					or partial Supplier name or partial shortSupplierName 
						to search, also given a practiceID
					and IsActice of 0 or 1 (false or true)
					
					Difference between this and Brand search is that this
						only search the suppliers for third party products 
						and not the brand.
					
					This is called from the proc:
						usp_GetAllInventoryDataforPCToIventory						
   
   AUTHOR:       John Bongiorni 10/9/2007 3:12:40 PM
   
   Modifications:  
   ------------------------------------------------------------ */   


CREATE PROCEDURE [dbo].[usp_GetAllInventoryDataforPCToIventory_BySupplier_With_ThirdPartyProducts]

	@PracticeLocationID int
	, @SearchText  varchar(50)
	, @IsActive bit

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


			SELECT 
				  SUB.PracticeCatalogProductID				
				, SUB.SupplierID
				, SUB.Category
				, SUB.Product
				, SUB.Code
				, SUB.Packaging
				, SUB.LeftRightSide
				, SUB.Size
				, SUB.Gender
				, SUB.QOH
				, SUB.ParLevel
				, SUB.ReorderLevel
				, SUB.CriticalLevel
				, SUB.WholesaleCost
				, SUB.IsActive
				, SUB.IsThirdPartyProduct
				, SUB.IsConsignment
				, SUB.ConsignmentQuantity
											
			FROM
			(
					SELECT	-- 249 active and inactive mastercatalogproducts in inventory WORKS!!!
 
							PCP.PracticeCatalogProductID,		

                            PCSB.MasterCatalogSupplierID AS SupplierID, 
                            PCSB.BrandShortName AS Category,   --  This column should be unneccessary
																--  Only here to preserve the existing application code.
						    MCP.[Name]			AS Product,
                            
                            MCP.[Code]			AS Code,
                            MCP.[Packaging]		AS Packaging,
                            MCP.[LeftRightSide] AS LeftRightSide,
                            MCP.[Size]			AS Size,
                            MCP.[Gender]		AS Gender,
							
							PI.QuantityOnHandPerSystem AS QOH,
							PI.ParLevel,
							PI.ReorderLevel,
							PI.CriticalLevel,
						
							PCP.WholesaleCost,
							PI.Isactive,
							0 AS IsThirdPartyProduct,
							PI.IsConsignment,
							PI.ConsignmentQuantity
                     
                    FROM dbo.PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
							                    
                    INNER JOIN [PracticeCatalogProduct] AS PCP WITH(NOLOCK) 
						ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
							
                    INNER JOIN [ProductInventory] AS PI  WITH(NOLOCK)
							ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
             
                    INNER JOIN [MasterCatalogProduct] AS MCP  WITH(NOLOCK)
							ON PCP.[MasterCatalogProductID] = MCP.[MasterCatalogProductID]
                                                 
                     WHERE  PI.[PracticeLocationID] = @PracticeLocationID
							AND PCP.IsThirdPartyProduct = 0  -- MasterCatalogProductsOnly
							AND PI.IsActive = @IsActive
							and (PCSB.SupplierName like @SearchText or PCSB.SupplierShortName like @SearchText)
					

				UNION

					--  Third Party Product Info
					SELECT		-- 249 active and inactive mastercatalogproducts in inventory WORKS!!!
 
							PCP.PracticeCatalogProductID,		       
                            PCSB.ThirdPartySupplierID AS SupplierID,                        
                            PCSB.BrandShortName AS Category,   --  This column should be unneccessary
																--  Only here to preserve the existing application code.
						    TPP.[Name]			AS Product,                            
                            TPP.[Code]			AS Code,
                            TPP.[Packaging]		AS Packaging,
                            TPP.[LeftRightSide] AS LeftRightSide,
                            TPP.[Size]			AS Size,
                            TPP.[Gender]		AS Gender,
							
							PI.QuantityOnHandPerSystem AS QOH,
							PI.ParLevel,
							PI.ReorderLevel,
							PI.CriticalLevel,
						
							PCP.WholesaleCost,						
							PI.Isactive,							
							1 AS IsThirdPartyProduct,
							PI.IsConsignment,
							PI.ConsignmentQuantity 
                     
                    FROM dbo.PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
                    
                    INNER JOIN [PracticeCatalogProduct] AS PCP WITH(NOLOCK) 
						ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
							
                    INNER JOIN [ProductInventory] AS PI  WITH(NOLOCK)
							ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
             
                    INNER JOIN [ThirdPartyProduct] AS TPP  WITH(NOLOCK)
							ON PCP.[ThirdPartyProductID] = TPP.[ThirdPartyProductID]
                                                 
                     WHERE  PI.[PracticeLocationID] = @PracticeLocationID
							AND PCP.IsThirdPartyProduct = 1		-- ThirdPartyProductsOnly 
							AND PI.IsActive = @IsActive
							AND 
								(PCSB.SupplierName like @SearchText or PCSB.SupplierShortName like @SearchText)
									
										
				) AS Sub
			
			ORDER BY Sub.IsThirdPartyProduct
			
END