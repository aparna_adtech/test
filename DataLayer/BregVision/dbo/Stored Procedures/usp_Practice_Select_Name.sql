﻿
/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Practice_Select_Name
   
   Description:  Selects the PracticeName from the table dbo.Practice
   				given the PracticeID 
   				and returns the PracticeName.
   
   AUTHOR:       John Bongiorni 08/08/2007 2:18 PM
   
   Modifications:  
   ------------------------------------------------------------ */ 

CREATE PROCEDURE [dbo].[usp_Practice_Select_Name]
		  @PracticeID		INT
		, @PracticeName		VARCHAR(50)		OUTPUT
AS
BEGIN
	DECLARE @Err INT
	
	SELECT
		  @PracticeName = PracticeName
	
	FROM dbo.Practice
	WHERE 
		PracticeID = @PracticeID
		AND IsActive = 1
	
	SET @Err = @@ERROR

	RETURN @Err
END


