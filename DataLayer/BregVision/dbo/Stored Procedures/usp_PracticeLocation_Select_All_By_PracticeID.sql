﻿

/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocation_Select_All_By_PracticeID
   
   Description:  Selects all PracticeLocation records for a given Practice.
				Select all records from the table dbo.PracticeLocation
				given the PracticeID.
   
   AUTHOR:       John Bongiorni 7/23/2007 11:05:42 AM
   
   Modifications:  JB 9/30/2007 6:29PM  Add Column IsPrimaryLocationYesNo
   ------------------------------------------------------------ */  

CREATE PROCEDURE [dbo].[usp_PracticeLocation_Select_All_By_PracticeID] 
			( @PracticeID INT )
AS 
    BEGIN
        DECLARE @Err INT
        SELECT
        
            PL.PracticeLocationID
          , PL.PracticeID
          , PL.Name
          , PL.IsPrimaryLocation
          , CASE (PL.IsPrimaryLocation) WHEN 1 THEN 'Yes' ELSE 'No' END AS IsPrimaryLocationYesNo
          , PL.EmailForOrderApproval
          , PL.EmailForLowInventoryAlerts
          , PL.IsEmailForLowInventoryAlertsOn
		  , PL.GeneralLedgerNumber AS GLNumber
		  , PL.IsHcpSignatureRequired
        
        FROM
            dbo.PracticeLocation AS PL WITH(NOLOCK)
        
        WHERE
            PL.IsActive = 1
            AND PL.PracticeID = @PracticeID
        
        ORDER BY
            PL.IsPrimaryLocation DESC
	      , PL.Name
        
        SET @Err = @@ERROR

        RETURN @Err
    END
