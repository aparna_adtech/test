﻿
CREATE PROC [dbo].[usp_ShoppingCart_Select_CustomPurchaseOrderCodeByPracticeLocationID]
	  @PracticeLocationID INT
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT TOP 1
		CustomPurchaseOrderCode
	FROM
		ShoppingCart
	WHERE
		PracticeLocationID = @PracticeLocationID
		AND
		IsActive = 1
	ORDER BY
		CreatedDate DESC;

END
