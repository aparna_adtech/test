﻿/* ------------------------------------------------------------
   PROCEDURE:    [dbo].[usp_PracticeCatalogSupplierBrand_Insert_ThirdPartySupplier_And_Brand_By_PracticeID]
   
   Description:  Inserts a Practice's ThirdPartySupplier 
				with its Brand (if Vendor) into table PracticeCatalogSupplierBrand

   AUTHOR:       John Bongiorni 10/3/2007 6:32:30 PM
   
   Modifications:  
   ------------------------------------------------------------ */  ------------------ INSERT ------------------
 
--		--  TEST
--		--  End Test Script
--		
--		BEGIN TRAN
--		
--		DECLARE	@return_value int
--			, @PracticeCatalogSupplierBrandID INT 
--
--		EXEc [dbo].[usp_PracticeCatalogSupplierBrand_Insert_ThirdPartySupplier_And_Brand_By_PracticeID]
--		
--				  @PracticeID = 4
--				, @ThirdPartySupplierID = 5
----				, @BrandName = 'Bledsoe'
----				, @BrandShortName = 'Bledsoe'
--				, @UserID = 0
--				, @PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID OUTPUT
--		
--		SELECT	@PracticeCatalogSupplierBrandID as N'@PracticeCatalogSupplierBrandID'
--		  
--		SELECT * FROM PracticeCatalogSupplierBrand 
--		WHERE PracticeID = 4
--		AND ThirdPartySupplierID = 3
--			
--		ROLLBACK TRAN
--		
--		  -- End Test Script
	
CREATE PROC [dbo].[usp_PracticeCatalogSupplierBrand_Insert_ThirdPartySupplier_And_Brand_By_PracticeID]
			  @PracticeID				INT
			, @ThirdPartySupplierID		INT
			, @BrandName				VARCHAR(50) = NULL 
			, @BrandShortName			VARCHAR(50) = NULL
			, @UserID					INT
			, @PracticeCatalogSupplierBrandID INT OUTPUT
AS
BEGIN


	INSERT
	INTO dbo.PracticeCatalogSupplierBrand
	(
          PracticeID
        , MasterCatalogSupplierID
        , ThirdPartySupplierID
        , SupplierName
        , SupplierShortName
        , BrandName
        , BrandShortName
        , IsThirdPartySupplier
        , Sequence
        , CreatedUserID
        , IsActive
	)


	SELECT 
		  @PracticeID							AS PracticeID
		, NULL									AS MasterCatalogProductID
		, TPS.ThirdPartySupplierID				AS ThirdPartySupplierID
		, TPS.SupplierName						AS SupplierName
		, TPS.SupplierShortName					AS SupplierShortName
		, CASE TPS.IsVendor WHEN 1 THEN @BrandName ELSE TPS.SupplierName END		AS BrandName
		, CASE TPS.IsVendor WHEN 1 THEN @BrandShortName ELSE TPS.SupplierShortName END  AS BrandShortName
		, 1										AS IsThirdPartySupplier
		, CASE TPS.Sequence WHEN NULL THEN 999 WHEN 0 THEN 999 ELSE TPS.Sequence END AS Sequence
		, 0 AS CreateUserID
		, 1 AS IsActive 
	FROM	
		dbo.ThirdPartySupplier AS TPS
	INNER JOIN Practice AS P
		ON P.PracticeID = TPS.PracticeID
	WHERE P.PracticeID = @PracticeID
		AND P.IsActive = 1
		AND TPS.IsActive = 1
		AND TPS.ThirdPartySupplierID = @ThirdPartySupplierID	
		AND TPS.ThirdPartySupplierID 
			NOT IN (SELECT ThirdPartySupplierID 
			FROM PracticeCatalogSupplierBrand
			WHERE MasterCatalogSupplierID IS NULL
				AND ThirdPartySupplierID IS NOT NULL) -- Prevent duplicate from going in
					 -- If isactive is 0 then update, if 1 then update instead of insert


	SELECT @PracticeCatalogSupplierBrandID = SCOPE_IDENTITY()

END

