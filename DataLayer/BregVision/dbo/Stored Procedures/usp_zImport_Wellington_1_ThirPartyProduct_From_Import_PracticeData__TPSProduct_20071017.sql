﻿
create proc [dbo].[usp_zImport_Wellington_1_ThirPartyProduct_From_Import_PracticeData__TPSProduct_20071017]
as
begin

INSERT INTO ThirdPartyProduct
	( PracticeCatalogSupplierBrandID
	, Code
	, Name
	, ShortName

	, Packaging
	, LeftRightSide
	, Size
	, Color
	, Gender
	, IsDiscontinued
	, WholesaleListCost
	, Description
	, CreatedUserID
	, CreatedDate
	, IsActive
	)

Select 
	  PracticeCatalogSupplierBrandID
	, ProductCode
	, ProductName
	, ProductShortName

	, Packaging
	, LeftRightSide
	, Size
	, Color
	, Gender
	, 0 AS IsDiscontinued
	, WholesaleListCost
	, Description
	, 0
	, GetDate()
	, 1
from Import_PracticeData.dbo.['Wellngton 3rdSupplier Product_20071017$']

end