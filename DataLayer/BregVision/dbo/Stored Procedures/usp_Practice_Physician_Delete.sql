﻿/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Practice_Physician_Delete
   
   Description:  Deletes a Physician from a Practice.
			This actually set the IsActive flag to false.
			The tables updated are Contact and Practice.
   
   AUTHOR:       John Bongiorni 7/21/2007 5:15:20 PM
   
   Modifications:  
   ------------------------------------------------------------ */  

CREATE PROCEDURE [dbo].[usp_Practice_Physician_Delete]
(
      @PhysicianID					 INT
	, @ModifiedUserID                INT
)
AS
DECLARE
    @TransactionCountOnEntry    INT,       -- Transaction Count before the transaction begins
    @Err             		INT        --  holds the @@Error code returned by SQL Server

SELECT @Err = @@ERROR
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF @Err = 0
BEGIN    
    SELECT @TransactionCountOnEntry = @@TRANCOUNT
    BEGIN TRANSACTION
END        


IF @Err = 0
BEGIN

    UPDATE dbo.Contact
	SET
		  ModifiedUserID = @ModifiedUserID
		, ModifiedDate = GETDATE()
		, IsActive = 0
	FROM dbo.Contact AS C 
	INNER JOIN dbo.Physician AS P 
		ON C.ContactID = P.ContactID 
	WHERE 
		P.PhysicianID = @PhysicianID 

    SELECT @Err = @@ERROR

END
IF @Err = 0
BEGIN

    UPDATE dbo.Physician
	SET
		  ModifiedUserID = @ModifiedUserID
		, ModifiedDate = GETDATE()
		, IsActive = 0
	WHERE 
		PhysicianID = @PhysicianID 


    SELECT @Err = @@ERROR

END

IF @@TranCount > @TransactionCountOnEntry
BEGIN

    IF @Err = 0
        COMMIT TRANSACTION
    ELSE
        ROLLBACK TRANSACTION
        --  Add any database logging here      

END



