﻿--  [usp_Report_ProductsDispensed]  6, '13, 15, 16, 17, 18, 19, 20, 22, 23, 24', '1/1/2007', '12/31/2007'
--  Added Supplier Brand.
--  Added Sequence
--  Add IsActive = 1


CREATE PROC [dbo].[usp_Report_ProductsDispensed_By_Practice]
		  @PracticeID	INT
		, @StartDate		DATETIME
		, @EndDate			DATETIME
		, @OrderBy				INT
		, @OrderDirection		BIT
		, @IsMedicare           BIT = NULL
		, @MedicareOrderDirection BIT = NULL
AS
BEGIN


--  SET @EndDate to 11:59:59 PM
SELECT @EndDate = DATEADD(DAY, 1, @EndDate)




SELECT
		  SUB.PracticeName		AS Practice
		, SUB.PracticeLocation	AS Location

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
			ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
			END					AS SupplierBrand


		, SUB.SupplierShortName	AS Supplier

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
		ELSE SUB.BrandShortName
		END						AS Brand


		, SUB.Sequence

		, SUB.ProductName			AS Product

		, SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender

		, SUB.Code

		, COALESCE(SUB.HCPCs, dbo.ConcatHCPCS(SUB.PracticeCatalogProductID)) AS HCPCS -- JB 20071004 Added Column for UDF Concat of HCPCS
		, CASE SUB.IsCashCollection
			WHEN 1 THEN Sub.BillingChargeCash
			WHEN 0 THEN 0
			END
			 AS BillingChargeCash

		, SUB.Physician
		, SUB.PatientCode
		, CONVERT(VARCHAR(10), SUB.DateDispensed, 101) AS DateDispensed

		, CASE SUB.IsMedicare
			WHEN 1 THEN 'Y'
			WHEN 0 THEN 'N'
		  END
		  AS IsMedicare
		, CASE CAST(SUB.ABNType AS varchar(50))
			When 'Medicare' then 'M' + ',' + CAST(SUB.MedicareOption AS varchar(50))
			When 'Commercial' then 'C' + ',' + CAST(SUB.MedicareOption AS varchar(50))
		  End
		  as ABNType
		, SUB.ICD9Code
		, SUB.Quantity 		--, SUM( SUB.Quantity )
        ,SUB.FitterName

		, CAST(  SUB.ActualWholesaleCostLineTotal  AS DECIMAL(15,2) )	AS ActualWholesaleCostLineTotal
		, CAST(  SUB.DMEDepositLineTotal  AS DECIMAL(15,2) )			AS DMEDepositLineTotal
		, SUB.IsCashCollection
		--, CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )    AS ActualChargeBilledLineTotal
		,  CASE SUB.IsCashCollection
			WHEN 1 THEN 0
			WHEN 0 THEN CAST(  SUB.ActualChargeBilledLineTotal  AS DECIMAL(15,2) )
			END
			AS ActualChargeBilledLineTotal

--		, SUB.Comment  -- Not Displayed for Version 1.

		, ISNULL(SUB.PayerName, 'N/A') AS PayerName
		, SUB.DispenseNote
		, SUB.OriginalActualChargeBilled
		, LTRIM(RTRIM(ISNULL(SUB.PatientFirstName,''))) AS PatientFirstName
		, LTRIM(RTRIM(ISNULL(SUB.PatientLastName,''))) AS PatientLastName
		, SUB.Mod1
		, SUB.Mod2
		, SUB.Mod3
FROM


		(
		SELECT

			  P.PracticeName
			, PL.Name AS PracticeLocation
			, ISNULL(RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix), '') AS Physician

			, D.PatientCode

			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) AS Sequence

			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash

			, MCP.[Name] AS ProductName

			--, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender

			, ISNULL(MCP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed

			, DD.IsMedicare
			, DD.ABNType
			, DD.ICD9Code
			, DD.Quantity
            , FC.FirstName + ' ' + FC.LastName as FitterName
			, PI_DD.ActualWholesaleCost
			, DD.ActualChargeBilled AS ActualChargeBilled
			, DD.DMEDeposit
			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit AS DMEDepositLineTotal
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.

			, PP.Name AS PayerName
			, D.Notes AS DispenseNote
			, DQ.HCPCs
			, DD.ActualChargeBilled AS OriginalActualChargeBilled
			, D.PatientFirstName AS PatientFirstName
			, D.PatientLastName AS PatientLastName
			, DD.Mod1
			, DD.Mod2
			, DD.Mod3
			, DQ.MedicareOption

		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOIN DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

        INNER JOIN Clinician AS CP
            ON CP.ClinicianID = DD.FitterID
        INNER JOIN Contact AS FC
            ON CP.ContactID = FC.ContactID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
			ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID		    --  Can be inactive

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1

		LEFT JOIN PracticePayer PP
			ON PP.PracticePayerID = D.PracticePayerID

		LEFT JOIN DispenseQueue AS DQ
			ON DQ.DispenseQueueID = DD.DispenseQueueID

		WHERE
			    P.PracticeID = @PracticeID
			AND DD.IsMedicare = COALESCE(@IsMedicare, DD.IsMedicare)
			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			--AND PCP.IsActive = 1
			--AND PCSB.IsActive = 1
			--AND MCP.IsActive = 1


		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM

		UNION ALL

		SELECT

			 P.PracticeName
			, PL.Name AS PracticeLocation
			, C.FirstName + ' ' + C.LastName AS Physician  --  Get proper line for this!!

			, D.PatientCode

			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) + 1000 AS Sequence

			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash

			, TPP.[Name] AS ProductName

			--, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) ) AS LeftRightSide
			, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )			AS Size
			, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender


			, ISNULL(TPP.Code, '') AS Code

			, D.DateDispensed AS DateDispensed

			, DD.IsMedicare
			, DD.ABNType
			, DD.ICD9Code
			, DD.Quantity
            , FC.FirstName + ' ' + FC.LastName as FitterName
			, PI_DD.ActualWholesaleCost
			, DD.ActualChargeBilled AS ActualChargeBilled
			, DD.DMEDeposit
			, DD.ActualChargeBilled * DD.Quantity AS ActualChargeBilledLineTotal
			, DD.DMEDeposit AS DMEDepositLineTotal
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits AS ActualWholesaleCostLineTotal

			, ISNULL(D.Comment, '') AS Comment -- Not Displayed for Version 1.

			, PP.Name AS PayerName
			, D.Notes AS DispenseNote
			, DQ.HCPCs
			, DD.ActualChargeBilled AS OriginalActualChargeBilled
			, D.PatientFirstName AS PatientFirstName
			, D.PatientLastName AS PatientLastName
			, DD.Mod1
			, DD.Mod2
			, DD.Mod3
			, DQ.MedicareOption

		FROM Dispense AS D  WITH (NOLOCK)

		INNER JOIN PracticeLocation AS PL   WITH (NOLOCK)
			ON D.PracticeLocationID = PL.PracticeLocationID

		INNER JOIN Practice AS P   WITH (NOLOCK)
			ON PL.PracticeID = P.PracticeID

		INNER JOIN DispenseDetail AS DD  WITH (NOLOCK)
			ON D.DispenseID = DD.DispenseID

        INNER JOIN Clinician AS CP
            ON CP.ClinicianID = DD.FitterID
        INNER JOIN Contact AS FC
            ON CP.ContactID = FC.ContactID

		INNER JOIN ProductInventory_DispenseDetail AS PI_DD  WITH (NOLOCK)
			ON DD.DispenseDetailID = PI_DD.DispenseDetailID

		INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
			ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
			ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

		LEFT JOIN Physician AS Ph
			ON DD.PhysicianID = Ph.PhysicianID			--  Can be inactive

		LEFT JOIN Contact AS C
			ON Ph.ContactID = C.ContactID
			AND C.IsActive = 1

		LEFT JOIN PracticePayer PP
			ON PP.PracticePayerID = D.PracticePayerID

		LEFT JOIN DispenseQueue AS DQ
			ON DQ.DispenseQueueID = DD.DispenseQueueID
		WHERE
			P.PracticeID = @PracticeID
			AND DD.IsMedicare = COALESCE(@IsMedicare, DD.IsMedicare)
			--AND P.IsActive = 1
			--AND PL.IsActive = 1
			AND D.IsActive = 1
			AND DD.IsActive = 1
			AND PI_DD.IsActive = 1
			--AND PCP.IsActive = 1
			--AND PCSB.IsActive = 1
			--AND TPP.IsActive = 1


		AND D.DateDispensed >= @StartDate
		AND D.DateDispensed < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM

		) AS Sub


	ORDER BY

		CASE
			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.ProductName DESC,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.ProductName     ,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))

			WHEN (@OrderBy = 2				  ) AND @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.DateDispensed DESC,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))
			WHEN (@OrderBy = 2				  ) AND @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.DateDispensed     ,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))

			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.IsMedicare DESC,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.ProductName DESC,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.IsMedicare DESC,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.ProductName     ,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))

			WHEN (@OrderBy = 2				  ) AND @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.IsMedicare DESC,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.DateDispensed DESC,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))
			WHEN (@OrderBy = 2				  ) AND @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.IsMedicare DESC,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.DateDispensed     ,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))

			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.IsMedicare,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.ProductName DESC,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))
			WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.IsMedicare,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.ProductName     ,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))

			WHEN (@OrderBy = 2				  ) AND @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.IsMedicare,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.DateDispensed DESC,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))
			WHEN (@OrderBy = 2				  ) AND @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.PracticeName,SUB.PracticeLocation,SUB.IsMedicare,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName,SUB.DateDispensed     ,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender))


		END
		  --SUB.PracticeName,SUB.PracticeLocation,SUB.Sequence,SUB.SupplierShortName,SUB.BrandShortName

		--,SUB.ProductName
		--,SUB.Code,SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender

END
