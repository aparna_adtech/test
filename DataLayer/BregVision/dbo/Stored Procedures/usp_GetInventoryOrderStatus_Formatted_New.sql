﻿-- =============================================
-- Author:		<Author,,Greg Ross>
-- Create date: <07/19/07>
-- Description:	<Proc to get order, reorder critical status on items to display on the Breg Vision dashboard>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetInventoryOrderStatus_Formatted_New] --5, 11
	-- Add the parameters for the stored procedure here
	@PracticeID INT
	,@PracticeLocationID INT

AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;

   --use bregvision

--DECLARE @PracticeID INT
--DECLARE @PracticeLocationID INT

--SET @PracticeID = 1 ;
--SET @PracticeLocationID = 3 ;

DECLARE @QuantityInTransit Table
    (
      practicecatalogproductid INT,
      MCName varchar(100),
      Code varchar(50),
      SupplierName varchar(50),
      QuantityNotFullyCheckin INT DEFAULT 0,
      QuantityCheckIn INT DEFAULT 0,
      QuantityBaseLine INT DEFAULT 0,
      QuantityOnHand INT DEFAULT 0,
      ReorderLevel INT DEFAULT 0,
      PARLevel INT,
      CriticalLevel INT,
      Priority INT DEFAULT 4
    )
	

/*Get items in PCProductIDs that are in inventory*/


INSERT  INTO @QuantityInTransit
        (
          practicecatalogproductid
          --, QuantityNotFullyCheckin
        )
        SELECT  
				productinventory.practicecatalogproductid
                -- ,SUM(quantityordered) AS QuantityNotFullyCheckin
        
        FROM    
				--SUPPLIERORDERLINEITEM
                --INNER join SUPPLIERORDER 
				--	ON SUPPLIERORDERLINEITEM.SUPPLIERORDERID = supplierorder.SUPPLIERORDERID
                --iNNER JOIN 
                productinventory 
					--ON SUPPLIERORDERLINEITEM.PracticeCatalogProductID = productinventory.PracticeCatalogProductID
                INNER JOIN [PracticeCatalogProduct] 
					ON PracticeCatalogProduct.PracticeCatalogProductID = productinventory.PracticeCatalogProductID
        
        WHERE   
				--supplierOrderStatusId IN ( 1, 3 )
                --AND SUPPLIERORDERLINEITEM.SUPPLIERORDERLINEITEMStatusID IN ( 1, 3 )
                --AND 
                
                ProductInventory.PracticeLocationID = @PracticeLocationID
                AND PracticeCatalogProduct.PracticeID = @PracticeID
                --AND SUPPLIERORDERLINEITEM.[IsActive] = 1
                --AND SUPPLIERORDER.[IsActive] = 1
                AND [ProductInventory].[IsActive] = 1
                AND [PracticeCatalogProduct].[IsActive] = 1
        GROUP BY 
				--supplierorderlineitem.PracticeCatalogProductid,
                --supplierOrder.supplierOrderid,
                productinventory.practicecatalogproductid

		--order by supplierOrderlineitemid



--  SELECT * FROM @QuantityInTransit


/*Get items reordered*/

UPDATE @QuantityInTransit

    SET QuantityNotFullyCheckin = SUB.QuantityNotFullyCheckin
    FROM
        (SELECT  productinventory.practicecatalogproductid AS PCPID,
                SUM(quantityordered) AS QuantityNotFullyCheckin
                
        FROM    SUPPLIERORDERLINEITEM
                INNER join SUPPLIERORDER ON SUPPLIERORDERLINEITEM.SUPPLIERORDERID = supplierorder.SUPPLIERORDERID
                INNER JOIN productinventory ON SUPPLIERORDERLINEITEM.PracticeCatalogProductID = productinventory.PracticeCatalogProductID
                INNER JOIN [PracticeCatalogProduct] ON PracticeCatalogProduct.PracticeCatalogProductID = productinventory.PracticeCatalogProductID
        
        WHERE   supplierOrderStatusId IN ( 1, 3 )
                AND SUPPLIERORDERLINEITEM.SUPPLIERORDERLINEITEMStatusID IN ( 1, 3 )
                AND ProductInventory.PracticeLocationID = @PracticeLocationID
                AND PracticeCatalogProduct.PracticeID = @PracticeID
                AND SUPPLIERORDERLINEITEM.[IsActive] = 1
                AND SUPPLIERORDER.[IsActive] = 1
                AND [ProductInventory].[IsActive] = 1
                AND [PracticeCatalogProduct].[IsActive] = 1
        GROUP BY supplierorderlineitem.PracticeCatalogProductid,
                supplierOrder.supplierOrderid,
                productinventory.practicecatalogproductid
		) AS SUB
	WHERE practicecatalogproductid = SUB.PCPID



--  SELECT * FROM @QuantityInTransit


/*Get Checked in item */
UPDATE  @QuantityInTransit
SET     QIT.QuantityCheckIn = CheckIn.QuantityCheckIn
FROM    @QuantityInTransit AS QIT
        INNER JOIN ( select productinventory.practicecatalogproductid,
                            sum(Quantity) as QuantityCheckIn
                     from   productinventory_orderCheckin,
                            productinventory,
                            PracticeCatalogProduct
                     where  productinventory_orderCheckin.ProductInventoryID = ProductInventory.productInventoryid
                            and ProductInventory.PracticeLocationID = @PracticeLocationID
                            and ProductInventory.PracticeCatalogProductID = PracticeCatalogProduct.PracticeCatalogProductID
                            and PracticeCatalogProduct.PracticeID = @PracticeID
                            AND productinventory_orderCheckin.[IsActive] = 1
                            AND [ProductInventory].[IsActive] = 1
                            AND [PracticeCatalogProduct].[IsActive] = 1
                     group by PracticeCatalogProduct.practicecatalogproductid,
                            productinventory.practicecatalogproductid
--order by supplierOrderlineitemid
                     
                   ) AS CheckIn ON QIT.practicecatalogproductid = CheckIn.practicecatalogproductid
--select  *
--FROM    @QuantityInTransit

--select * FROM @QuantityInTransit

UPDATE  @QuantityInTransit
SET     QuantityBaseLine = ( QuantityNotFullyCheckin - QuantityCheckIn )


--  select * FROM @QuantityInTransit

update  @QuantityInTransit
set     QIT.MCNAME = QIT.MCNAME 
--, @QuantityInTransit.Code = ProductInfo.Code
--, @QuantityInTransit.SupplierName = ProductInfo.SupplierName
from    @QuantityInTransit AS QIT
        inner join ( SELECT PCP.PracticeCatalogProductid as PracticeCatalogProductid,
                            MCP.Name as MCNAME
--		, MCP.Code as Code
--		, MCS.SupplierName as SupplierName
                     from   PracticeCatalogProduct AS PCP
                            inner join MasterCatalogProduct AS MCP
                            inner join MasterCatalogSubCategory AS MCSC
                            inner join MasterCatalogCategory AS MCC
                            inner join MasterCatalogSupplier AS MCS on MCC.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID on MCSC.MastercatalogcategoryID = MCC.MastercatalogcategoryID on MCP.MastercatalogSubcategoryID = MCSC.MastercatalogSubcategoryID on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
                     WHERE  PCP.[IsActive] = 1
                            AND MCP.[IsActive] = 1
                            AND MCC.[IsActive] = 1
                            AND MCSC.[IsActive] = 1
                            AND MCS.[IsActive] = 1
                   ) as ProductInfo --where PCP.PracticeCatalogProductid = practicecatalogproductid) as ProductInfo
        on QIT.PracticeCatalogProductid = ProductInfo.PracticeCatalogProductid


UPDATE  @QuantityInTransit
SET     QIT.MCName = Positive.MCName,
        QIT.Code = Positive.Code,
        QIT.SupplierName = Positive.SupplierName
		  -- Sets this so this will be ordered third in the grid
FROM    @QuantityInTransit AS QIT
        INNER JOIN ( SELECT PCP.PracticeCatalogProductid as PracticeCatalogProductid,
                            MCP.Name as MCNAME,
                            MCP.Code as Code,
                            MCS.SupplierName AS SupplierName
                     FROM   PracticeCatalogProduct AS PCP
                            INNER JOIN MasterCatalogProduct AS MCP ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
                            INNER JOIN MasterCatalogSubCategory AS MCSC ON MCP.MasterCatalogSubCategoryID = MCSC.MasterCatalogSubCategoryID
                            inner join MasterCatalogCategory AS MCC ON MCSC.MasterCatalogCategoryID = MCC.MasterCatalogCategoryID
                            inner join MasterCatalogSupplier AS MCS ON MCC.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
                     WHERE  PCP.[IsActive] = 1
                            AND MCP.[IsActive] = 1
                            AND MCC.[IsActive] = 1
                            AND MCSC.[IsActive] = 1
                            AND MCS.[IsActive] = 1
                   ) AS Positive ON QIT.PracticeCatalogProductid = Positive.PracticeCatalogProductid
			

UPDATE  @QuantityInTransit
SET     priority = 3
WHERE   [QuantityBaseLine] > 0

--Reorder Level
UPDATE  @QuantityInTransit
SET     ABC.Priority = 2
FROM    @QuantityInTransit AS ABC
WHERE   [PracticeCatalogProductID] in (
        select  [PracticeCatalogProduct].[PracticeCatalogProductID] as PracticeCatalogProductid
        from    productinventory,
                PracticeCatalogProduct
        where   ProductInventory.PracticeLocationID = @PracticeLocationID
                and ProductInventory.PracticeCatalogProductID = PracticeCatalogProduct.PracticeCatalogProductID
                and PracticeCatalogProduct.PracticeID = @PracticeID
                AND [QuantityOnHandPerSystem] < [ReorderLevel]
                AND ProductInventory.[IsActive] = 1
                AND PracticeCatalogProduct.[IsActive] = 1 )
  
UPDATE  @QuantityInTransit
SET     QIT.QuantityOnHand = Positive.QuantityOnHand,
        QIT.ReorderLevel = Positive.ReorderLevel,
        QIT.PARLevel = Positive.PARLevel,
        QIT.CriticalLevel = Positive.CriticalLevel
FROM    @QuantityInTransit AS QIT
        INNER JOIN ( SELECT [PracticeCatalogProduct].[PracticeCatalogProductID] as PracticeCatalogProductid,
                            [ProductInventory].[QuantityOnHandPerSystem] AS QuantityOnHand,
                            [ProductInventory].ReorderLevel AS ReorderLevel,
                            [ProductInventory].PARLevel AS PARLevel,
                            [ProductInventory].CriticalLevel AS CriticalLevel
                     from   productinventory,
                            PracticeCatalogProduct
                     where  ProductInventory.PracticeLocationID = @PracticeLocationID
                            and ProductInventory.PracticeCatalogProductID = PracticeCatalogProduct.PracticeCatalogProductID
                            and PracticeCatalogProduct.PracticeID = @PracticeID
                            AND ProductInventory.[IsActive] = 1
                            AND PracticeCatalogProduct.[IsActive] = 1
                   ) AS Positive ON QIT.PracticeCatalogProductid = Positive.PracticeCatalogProductid
--End reorder Level

-- Critical Level
UPDATE  @QuantityInTransit
SET     ABC.Priority = 1
FROM    @QuantityInTransit AS ABC
WHERE   [PracticeCatalogProductID] in (
        select  [PracticeCatalogProduct].[PracticeCatalogProductID] as PracticeCatalogProductid
        from    productinventory,
                PracticeCatalogProduct
        where   ProductInventory.PracticeLocationID = @PracticeLocationID
                and ProductInventory.PracticeCatalogProductID = PracticeCatalogProduct.PracticeCatalogProductID
                and PracticeCatalogProduct.PracticeID = @PracticeID
                AND ProductInventory.[IsActive] = 1
                AND PracticeCatalogProduct.[IsActive] = 1
                AND [QuantityOnHandPerSystem] < [CriticalLevel] )
-- End Critical Level
select  *
FROM    @QuantityInTransit
ORDER BY [Priority],
        SupplierName,
        MCName
END

