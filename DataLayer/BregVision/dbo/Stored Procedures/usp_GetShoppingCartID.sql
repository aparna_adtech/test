﻿-- =============================================
-- Author:		Greg Ross
-- Create date: 08/29/07
-- Description:	Gets ShoppingCart ID 
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetShoppingCartID]
	-- Add the parameters for the stored procedure here
	@PracticeLocationID int	

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
select ShoppingCartID from ShoppingCart where PracticeLocationID=@PracticeLocationID and isactive=1

END
