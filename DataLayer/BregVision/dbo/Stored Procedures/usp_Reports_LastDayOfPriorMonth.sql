﻿/*

  John Bongiorni
  2007.12.11

EXEC dbo.usp_Reports_LastDayOfMonth

*/


CREATE proc [dbo].[usp_Reports_LastDayOfPriorMonth]
AS 
BEGIN

select CAST( CONVERT(VARCHAR(10), [dbo].[lastDayOfMonth](DATEADD(Month, -1, getdate())), 101) as DateTime) As LastDayOfPriorMonth

END



