﻿/*
-- Note due to corrupt data in the database the MaxQuantityToCheckIn will be negative is some cases.
--  Not allowing the user to check in data.
--  Example:
 
	DECLARE	@return_value int,
			@QuantityOrdered int,
			@QuantityCheckedIn int,
			@MaxQuantityToCheckIn int

	EXEC	@return_value = [dbo].[usp_SupplierOrderLineItem_Select_Quantities_Given_SupplierOrderLineItemID]
			@SupplierOrderLineItemID = 868,
			@QuantityOrdered = @QuantityOrdered OUTPUT,
			@QuantityCheckedIn = @QuantityCheckedIn OUTPUT,
			@MaxQuantityToCheckIn = @MaxQuantityToCheckIn OUTPUT

	SELECT	@QuantityOrdered as N'@QuantityOrdered',
			@QuantityCheckedIn as N'@QuantityCheckedIn',
			@MaxQuantityToCheckIn as N'@MaxQuantityToCheckIn'

	SELECT	'Return Value' = @return_value


-- Modifications:  
-- JB 2007.11.15 fix query to put all quantity checked-in for the supplier order line item

*/

CREATE PROC [dbo].[usp_SupplierOrderLineItem_Select_Quantities_Given_SupplierOrderLineItemID]
				  @SupplierOrderLineItemID	INT
				, @QuantityOrdered			INT		OUTPUT
				, @QuantityCheckedIn		INT		OUTPUT
				, @MaxQuantityToCheckIn		INT		OUTPUT
				
AS
BEGIN

	SELECT 
		  @QuantityOrdered = SOLI.QuantityOrdered
		, @QuantityCheckedIn = ISNULL(SubPIOCI.QuantityCheckedIn, 0)	
		, @MaxQuantityToCheckIn = 
			CASE 
			WHEN ( SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) ) < 0 THEN 0
			ELSE SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0)
			END 	
			
	FROM SupplierOrderLineItem AS SOLI
	
	INNER JOIN PracticeCatalogPRoduct AS PCP 
		ON SOLI.PracticeCatalogProductID = 	PCP.PracticeCatalogPRoductID
	
	INNER JOIN ProductInventory AS PI
		ON SOLI.PracticeCatalogProductID = PI.PracticeCatalogProductID
	
	INNER JOIN SupplierOrderLineItemStatus AS SOLIS 
		ON SOLI.SupplierOrderLineItemStatusID = SOLIS.SupplierOrderLineItemStatusID
	
-- --  Dummy programming below	
--	LEFT JOIN 
--		dbo.ProductInventory_OrderCheckIn AS PIOCI
--			ON SOLI.SupplierOrderLineItemID = PIOCI.SupplierOrderLineItemID
--				AND PI.ProductInventoryID = PIOCI.ProductInventoryID
--				AND PIOCI.IsActive = 1
				
	-- JB 2007.11.15 fix query to put all quantity checked-in for the supplier order line item				
	LEFT JOIN (SELECT SupplierOrderLineItemID, SUM(Quantity) AS QuantityCheckedIn
							FROM dbo.ProductInventory_OrderCheckIn 
							WHERE IsActive = 1
							GROUP BY SupplierOrderLineItemID)AS SubPIOCI
								ON SOLI.SupplierOrderLineItemID = SubPIOCI.SupplierOrderLineItemID
	
	WHERE SOLI.SupplierOrderLineItemID = @SupplierOrderLineItemID

END