﻿/* ------------------------------------------------------------
   PROCEDURE:    dbo.zusp_PracticeLocation_Supplier_AccountCode_Delete
   
   Description:  "Deletes" a record from table dbo.PracticeLocation_Supplier_AccountCode
   					This actual flags the IsActive column to false.
					
   AUTHOR:       John Bongiorni 9/28/2007 6:22:39 PM
   
   Modifications:  
   ------------------------------------------------------------ */  ------------------ DELETE ----------------

CREATE PROCEDURE dbo.usp_PracticeLocation_Supplier_AccountCode_Delete
(
	@PracticeLocationID INT,
	@PracticeCatalogSupplierBrandID INT,
	@ModifiedUserID INT
)
AS
BEGIN
	DECLARE @Err INT

	UPDATE
		  dbo.PracticeLocation_Supplier_AccountCode
	SET 
		  IsActive = 0	  
		, ModifiedUserID = @ModifiedUserID
		, ModifiedDate = GETDATE()
	WHERE 
		PracticeLocationID = @PracticeLocationID AND
		PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID
	
	SET @Err = @@ERROR

	RETURN @Err
End