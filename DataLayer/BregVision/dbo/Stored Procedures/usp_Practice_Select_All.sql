﻿/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Practice_Select_All
   
   Description:  Selects all records from the table dbo.Practice
   
   AUTHOR:       John Bongiorni 7/12/2007 12:17 PM
   
   Modifications:  
   ------------------------------------------------------------ */

CREATE PROCEDURE [dbo].[usp_Practice_Select_All]
AS
BEGIN
	DECLARE @Err INT
	SELECT
		  PracticeID
		, PracticeName
	
	FROM dbo.Practice
	WHERE IsActive = 1
	ORDER BY PracticeName
	
	SET @Err = @@ERROR

	RETURN @Err
END


