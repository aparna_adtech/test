﻿
Create PROCEDURE [dbo].[usp_PracticeLocation_Physician_Select_All_NonLocation_Names_By_PracticeLocationID]
					@PracticeLocationID INT
AS 
    BEGIN 
    
		DECLARE @PracticeID INT
    
    
		SELECT @PracticeID = PracticeID 
		FROM dbo.PracticeLocation WITH (NOLOCK)
		WHERE PracticeLocationID = @PracticeLocationID
    
    
        SELECT
            PH.PhysicianID
          , C.ContactID
	--	, C.Salutation
	--	, C.FirstName
	--	, C.MiddleName
	--	, C.LastName
	--	, C.Suffix
          , RTRIM(C.FirstName + ' ' + C.LastName + ' ' + C.Suffix) AS PhysicianName
        FROM
            dbo.Contact  AS C	WITH (NOLOCK)
            INNER JOIN dbo.Physician  AS PH WITH (NOLOCK)
                ON C.ContactID = PH.ContactID
            INNER JOIN dbo.Practice AS P  WITH (NOLOCK)
				ON PH.PracticeID = P.PracticeID
            
        WHERE
            P.PracticeID = @PracticeID       
            AND PH.IsActive = 1
            AND C.IsActive = 1
            AND PH.PhysicianID NOT IN (	SELECT PhysicianID 
										FROM PracticeLocation_Physician WITH (NOLOCK)
										WHERE PracticeLocationID = @PracticeLocationID 
											AND IsActive = 1 
										) 
    END
    

