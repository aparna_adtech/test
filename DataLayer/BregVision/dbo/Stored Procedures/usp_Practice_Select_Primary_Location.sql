﻿

/*
	Stored Procedure:  dbo.usp_Practice_Select_Primary_Location
*/

CREATE PROCEDURE [dbo].[usp_Practice_Select_Primary_Location] 
		(@PracticeID INT
		, @PracticeLocationID INT OUTPUT) 
AS
	BEGIN
					SELECT @PracticeLocationID = PL.PracticeLocationID 
					FROM dbo.PracticeLocation AS PL
					INNER JOIN Practice AS P
						ON PL.PracticeID = P.PracticeID
					WHERE
						P.PracticeID = @PracticeID
						AND PL.IsPrimaryLocation = 1
						AND PL.IsActive = 1										
	END


