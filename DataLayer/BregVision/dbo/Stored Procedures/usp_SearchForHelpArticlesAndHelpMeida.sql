﻿
-- =============================================
-- Author:		<Author: Tom Thomas>
-- Create date: <Create Date: 03/11/10>
-- Description:	<Description: Get HelpArticles and HelpMeida matching searchText
-- =============================================
Create PROCEDURE [dbo].[usp_SearchForHelpArticlesAndHelpMeida]  
	@searchText nvarchar(100)
	
AS 
    BEGIN
	
	SET NOCOUNT ON;
    
SELECT DISTINCT 
                      HelpEntityTree.Id, HelpEntityTree.ParentId, HelpArticles.Title AS ArticleTitle, HelpArticles.Description AS ArticleDescription, - 1 AS TopParentId, 2 AS [Level], 
                      HelpEntityTree.EntityTypeId, HelpEntityTree.SortOrder, HelpEntityTree.EntityId
FROM         HelpArticles INNER JOIN
                      HelpEntityTree ON HelpArticles.Id = HelpEntityTree.EntityId AND HelpArticles.HelpEntityTypeId = HelpEntityTree.EntityTypeId
WHERE     (HelpArticles.Title LIKE '%' + @searchText + '%')

SELECT DISTINCT 
                      HelpEntityTree.Id, HelpEntityTree.ParentId, HelpMedia.Title AS MediaTitle, HelpMedia.Description AS MediaDescription, HelpMedia.Url AS MediaUrl, 
                      - 1 AS TopParentId, 2 AS [Level], HelpEntityTree.EntityTypeId, HelpEntityTree.EntityId, HelpEntityTree.SortOrder
FROM         HelpMedia INNER JOIN
                      HelpEntityTree ON HelpMedia.Id = HelpEntityTree.EntityId AND HelpMedia.HelpEntityTypeId = HelpEntityTree.EntityTypeId
WHERE     (HelpMedia.Title LIKE '%' + @searchText + '%')

END

-- exec [usp_SearchForHelpArticlesAndHelpMeida] 'i'




