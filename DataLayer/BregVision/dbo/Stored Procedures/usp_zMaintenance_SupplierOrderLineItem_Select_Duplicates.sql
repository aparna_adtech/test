﻿
CREATE PROC usp_zMaintenance_SupplierOrderLineItem_Select_Duplicates
AS 
BEGIN

	SELECT 
		  Dups.PracticeName AS Practice
--		, Dups.PracticeLocationID 
		, Dups.PracticeLocation
		, Dups.PurchaseOrder
		, Dups.SupplierShortName
		, Dups.BrandShortName
		, CASE DUPS.IsThirdPartyProduct WHEN 0 THEN MCP.Code WHEN 1 THEN TPP.Code END AS ProductCode
		, DUPS.CreatedDate AS OrderDate

	FROM
	( 
		SELECT  TOP 10000
				  P.PracticeID
				, P.PracticeName 
				, PL.PracticeLocationID 
				, PL.NAME AS PracticeLocation
				, BVO.PurchaseOrder
				, SO.BregVisionOrderID
				, SO.SupplierOrderID
				, PCP.IsThirdPartyProduct
				, PCSB.SupplierShortName
				, PCSB.BrandShortName
				, SOLI.PracticeCatalogProductID
				, BVO.CreatedDate   
	                     
			FROM    BregVisionOrder AS BVO
			INNER JOIN dbo.PracticeLocation AS PL
				ON BVO.PracticeLocationID = PL.PracticeLocationID
			INNER JOIN dbo.Practice AS P
				ON PL.PracticeID = P.PracticeID
			INNER JOIN dbo.SupplierOrder AS SO
				ON BVO.BregVisionOrderID = SO.BregVisionOrderID
			INNER JOIN dbo.SupplierOrderLineItem AS SOLI 
				ON SO.SupplierOrderID = SOLI.SupplierOrderID
			INNER JOIN PracticeCatalogProduct AS PCP
				ON PCP.PracticeCatalogProductID = SOLI.PracticeCatalogProductID
			INNER JOIN PracticeCatalogSupplierBrand AS PCSB
				ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
			
			
			GROUP BY 
					  P.PracticeID
					, P.PracticeName
					, PL.NAME
					, PL.PracticeLocationID 
					, BVO.PurchaseOrder
					, SO.BregVisionOrderID
					, SO.SupplierOrderID
					, PCSB.SupplierShortName
					, PCSB.BrandShortName
					, PCP.IsThirdPartyProduct
					, SOLI.PracticeCatalogProductID
					, BVO.CreatedDate   
	                
			HAVING COUNT(1) > 1
			ORDER BY SO.BregVisionOrderID DESC
				, P.PracticeName
				, PL.NAME
				, SO.SupplierOrderID
				, SOLI.PracticeCatalogProductID
				, BVO.CreatedDate  
	)
		AS Dups

	INNER JOIN PracticeCatalogProduct AS PCP2
		ON Dups.PracticeCatalogProductID = PCP2.PracticeCatalogProductID
		
	LEFT JOIN dbo.MasterCatalogProduct AS MCP
		ON MCP.MasterCatalogProductID = PCP2.MasterCatalogProductID

	LEFT JOIN dbo.ThirdPartyProduct AS TPP
		ON TPP.ThirdPartyProductID = PCP2.ThirdPartyProductID

END