﻿
--  Select * from Practice  -- To Get the PracticeID
--  Modification:  
--      2008.01.31  John Bongiorni
--		Pull Role from aspnet database.

CREATE PROC [dbo].[usp_z_AspNet_UserName_AssignToRole_AndPractice]  
		  @PracticeID INT
		, @UserName  VARCHAR(50)
		, @RoleName	 VARCHAR(50)  --  PracticeAdmin or PracticeLocationUser

AS
BEGIN

	DECLARE @AppName AS VARCHAR(50)
	DECLARE @UTCDate DATETIME
	DECLARE @PracticeName AS VARCHAR(50)
	DECLARE @aspnetdbRoleName AS VARCHAR(50)

	SET @AppName = '/BregVision'
	SELECT @UTCDate = GETUTCDATE()



	DECLARE	@return_value int

	EXEC	@return_value = [aspnetdb].[dbo].[aspnet_UsersInRoles_AddUsersToRoles]
			@ApplicationName = N'/BregVision'
			, @UserNames = @UserName		-- Set UserName
			, @RoleNames = @RoleName		-- Set RoleName
			, @CurrentTimeUtc = @UTCDate

	SELECT	'Return Value' = @return_value

	DECLARE	@UserID int

	EXEC	@return_value = [dbo].[usp_User_Insert]
			@PracticeID = @PracticeID				--  Set PracticeID
			, @UserName = @UserName				-- Set UserName
			, @CreatedUserID = -13				-- -13 indicates Team Breg Vision
			, @UserID = @UserID OUTPUT

	SELECT	@UserID as N'@UserID'   -- The userID generated is: 26
	SELECT  @PracticeName = PracticeName FROM Practice WHERE PracticeID = @PracticeID
	SELECT  @aspnetdbRoleName = (SELECT 
									  R.RoleName
								FROM aspnetdb.dbo.aspnet_Roles AS R  WITH (NOLOCK)
								INNER JOIN aspnetdb.dbo.aspnet_UsersInRoles AS UR  WITH (NOLOCK)
									ON R.RoleId = UR.RoleId
								INNER JOIN aspnetdb.dbo.aspnet_Users AS U  WITH (NOLOCK)
									ON UR.UserID = U.UserID
								WHERE U.UserName = @UserName
								)
								
	SELECT  @aspnetdbRoleName + ': ' + @UserName + ' add to: ' + @PracticeName + ' with UserID: ' + CAST( @UserID AS VARCHAR)
								
	--SELECT  'User: ' + @UserName + ' of Practice: ' + @PracticeName + chr(13) + chr(10) + ' as Role:   ' + @aspnetdbRoleName + '  with UserID:    ' + CAST( @UserID AS VARCHAR)

END

