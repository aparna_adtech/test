﻿-- =============================================
-- Author:		Greg Ross
-- Create date: 10/12/07
-- Description:	Deletes from ProductHCPCs
-- =============================================
CREATE PROCEDURE dbo.usp_DeleteItemFromHCPCs
	-- Add the parameters for the stored procedure here
	@HCPCSID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

delete from ProductHCPCS where ProductHCPCSID=@HCPCSID 

END