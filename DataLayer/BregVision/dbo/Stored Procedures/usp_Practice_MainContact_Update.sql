﻿/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Practice_MainContact_Update
   
   Description:  Updates a record in the table Practice
					Updates the ContactID, which is the main contact person 
					for the practice.

   AUTHOR:       John Bongiorni 7/14/2007 6:31 PM
   
   Modifications:  
   ------------------------------------------------------------ */


CREATE PROCEDURE [dbo].[usp_Practice_MainContact_Update]
(
	  @PracticeID                    INT
	, @ContactID					 INT
	, @ModifiedUserID                INT
)
AS
BEGIN
	DECLARE @Err INT

	UPDATE dbo.Practice
	SET
		  ContactID = @ContactID
		, ModifiedUserID = @ModifiedUserID
		, ModifiedDate = GETDATE()

	WHERE 
		PracticeID = @PracticeID

	SET @Err = @@ERROR

	RETURN @Err
End


