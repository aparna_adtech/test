﻿
-- =============================================
-- Author:		Greg Ross
-- Create date: 08/27/07
-- Description:	Gets Billing Address Info for Practice Location

--  Modifications: 
--		2007.11.06  John Bongiorni
--			Modify to LEFT join on Contact as one may not exist.
--			Move C.IsActive to the LEFT JOIN clause instead of the WHERE clause.
--
--  EXEC usp_GetBillingAddressByPracticeLocation 3
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetBillingAddressByPracticeLocation]
	-- Add the parameters for the stored procedure here
	@PracticeLocationID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;


SELECT 
	COALESCE(SCCB.Billing_Attention, PLBA.AttentionOf, '') as BillAttentionOf	 --  ISNULL(PLBA.AttentionOf, '') as BillAttentionOf
	,COALESCE(SCCB.Billing_Address1, A.AddressLine1, '') as BillAddress1 --, ISNULL(A.AddressLine1, '') as BillAddress1
	,COALESCE(SCCB.Billing_Address2, A.AddressLine2, '') as BillAddress2 --, ISNULL(A.AddressLine2, '') as BillAddress2
	,COALESCE(SCCB.Billing_City, A.City, '') as BillCity  --, ISNULL(A.City, '') as BillCity
	,COALESCE(SCCB.Billing_State, A.State, '') as BillState  --, ISNULL(A.State, '') as BillState
	,COALESCE(SCCB.Billing_Zip, A.ZipCode, '') as BillZipCode  --, ISNULL(A.ZipCode, '') as BillZipCode
	, ISNULL(C.FAX, '') AS Fax
	, ISNULL(C.PhoneWork, '') AS WorkPhone
	, PLBA.BregOracleID
	, PLBA.BregOracleIDInvalid
FROM practicelocationbillingaddress PLBA
INNER JOIN PracticeLocation AS PL
	ON PL.PracticeLocationID = PLBA.PracticeLocationID
INNER JOIN dbo.Address AS A
	ON PLBA.AddressID = A.AddressID
LEFT JOIN dbo.Contact AS C			--  2007.11.06 JB Changed to LEFT
	ON PL.ContactID = C.ContactID
		AND C.IsActive = 1			--  2007.11.06 JB Moved this line from WHERE clause to here in the LEFT JOIN
	Left Outer Join ShoppingCart SC
		On PLBA.PracticeLocationID = SC.PracticeLocationID
			AND SC.IsActive = 1
			AND SC.[ShoppingCartStatusID] = 0 --pending
	Left Outer Join ShoppingCartCustomBrace SCCB 
		ON SCCB.ShoppingCartID=SC.ShoppingCartID
			
WHERE PLBA.PracticeLocationID = @PracticeLocationID
AND PLBA.IsActive = 1
AND PL.IsActive = 1
AND A.IsActive = 1


END
