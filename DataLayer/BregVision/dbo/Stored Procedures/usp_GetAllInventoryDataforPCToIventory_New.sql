﻿

-- =============================================
-- Author:		Michael Sneen
-- Create date: 2012.01.29
-- Description:	Rewritten to Consolidate procs
--				This proc will return all rows without a filter unless the Filter(@SearchCriteria)
--matches one of the items in the if statement below, in which case the result set will be narrowed
--by that criteria.

-- Modifications:  
--  exec usp_GetAllInventoryDataforPCToIventory_New 3, '', '', 1

-- =============================================
CREATE PROCEDURE [dbo].[usp_GetAllInventoryDataforPCToIventory_New]
	@PracticeLocationID int,
	@SearchCriteria varchar(50),
	@SearchText  varchar(50),
	@IsActive bit
AS
BEGIN

	SET NOCOUNT ON;
	
	/*These variables are used in conjunction with the coalesce statements in the where clause.
	each of these variables will be null except one is @SearchCriteria matches.*/
	Declare @ProductName varchar(50)			--NULL   These default to null
	Declare @ProductCode varchar(50)			--NULL   These default to null
	Declare @ProductBrand varchar(50)			--NULL   These default to null
	Declare @ProductCategory varchar(50)
	Declare @ProductHCPCs varchar(50)			--NULL   These default to null


	if @SearchCriteria='Name'					-- ProductName
		Set @ProductName = '%' + @SearchText + '%'		--put the percent sign on both sides of the search text for all
	else if @SearchCriteria='Code'
		Set @ProductCode = '%' +  @SearchText + '%'	
	else if @SearchCriteria='Brand'
		Set @ProductBrand = '%' +  @SearchText + '%'
	else if @SearchCriteria='Category'
		Set @ProductCategory = '%' +  @SearchText				
	else if @SearchCriteria='HCPCs'
		Set @ProductHCPCs = '%' +  @SearchText + '%'	


SELECT
 
	PI.ProductInventoryID,
	PI.QuantityOnHandPerSystem AS QOH,
	PI.ParLevel,
	PI.ReorderLevel,
	PI.CriticalLevel,
	PI.IsNotFlaggedForReorder,
	PI.Isactive,
	PI.IsSetToActive,
	PI.IsConsignment,
	PI.ConsignmentQuantity,
	PI.ConsignmentLockParLevel,
	
	IsDeletable = (
					CASE (PI.QuantityOnHandPerSystem + PI.ParLevel + PI.ReorderLevel + PI.CriticalLevel)
						WHEN 0 
							THEN 1 
							ELSE 0 
					END
				   ),

	PCSB.SupplierShortName,		
	PCSB.BrandShortName,		
	PCSB.PracticeCatalogSupplierBrandID AS SupplierID,
	
	PCP.PracticeCatalogProductID,						
	PCP.WholesaleCost,							
	PCP.IsThirdPartyProduct,

	Category = COALESCE(MCC.[NAME], 'NA'),								

	Product = COALESCE(MCP.[Name], TPP.[Name]),
	Code = COALESCE(MCP.[Code], TPP.[Code]),
	SideSize = COALESCE(MCP.LeftRightSide, TPP.LeftRightSide, ' ') + ', ' + COALESCE(MCP.Size, TPP.Size, ' '),
	Packaging = COALESCE(MCP.[Packaging], TPP.[Packaging]),
	LeftRightSide = COALESCE(MCP.LeftRightSide, TPP.LeftRightSide),
	Size = COALESCE(MCP.[Size], TPP.[Size]),
	Gender = COALESCE(MCP.[Gender], TPP.[Gender]),
	PI.IsLogoAblePart,
	dbo.IsPIItemLogoAblePart(PI.ProductInventoryID) AS McpIsLogoAblePart
		
	FROM 
		[ProductInventory] AS PI
			
		INNER JOIN [PracticeCatalogProduct] AS PCP	
			ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
			
		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB 
			ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID 
		              


		LEFT OUTER JOIN [MasterCatalogProduct] AS MCP  
			ON PCP.[MasterCatalogProductID] = MCP.[MasterCatalogProductID]
			
			Left Outer JOIN dbo.MasterCatalogsubCategory AS MCSC
				ON MCP.MasterCatalogsubCategoryID = MCSC.MasterCatalogsubCategoryID

			left outer JOIN dbo.MasterCatalogCategory AS MCC
				ON MCSC.MasterCatalogCategoryID = MCC.MasterCatalogCategoryID 
				
			
		LEFT OUTER JOIN [ThirdPartyProduct] AS TPP
			ON PCP.[ThirdPartyProductID] = TPP.[ThirdPartyProductID]



	WHERE  
		PI.[PracticeLocationID] = @PracticeLocationID
		AND PI.IsSetToActive = @IsActive 
		AND PI.IsActive = 1
		AND PCSB.IsActive =	1
		AND PCP.IsActive = 1		
		AND COALESCE(MCP.IsActive, TPP.IsActive) = 1
		AND 
		(   /*
			This part is similar to an Optional Parameter Search.
			if the Parameter is not Null, then narrow the search by it, otherwise
			return all rows.  If all parameters are null, then return all rows.
			Use these lines for target or parent tables. 
			This has the effect of saying "if @variable is not null then try to match it to the " 
			left hand expression.  If @variable is null then match the left hand field to itself
			if the left hand field is null, then match '' = ''(This last part is because NULL = NULL) 
			does not return a match. */
			COALESCE(MCP.Name, '') like COALESCE(@ProductName, MCP.Name, '')
			OR
			COALESCE(TPP.Name, '') like COALESCE(@ProductName, TPP.Name, '')
			OR
			COALESCE(MCP.ShortName, '') like COALESCE(@ProductName, MCP.ShortName, '')
			OR
			COALESCE(TPP.ShortName, '') like COALESCE(@ProductName, TPP.ShortName, '')
		)
		AND 
		(
			COALESCE(MCP.Code, '') like COALESCE(@ProductCode , MCP.Code, '')
			OR
			COALESCE(TPP.Code, '') like COALESCE(@ProductCode , TPP.Code, '')			
		)	
		AND 
		(
			COALESCE(PCSB.BrandName, '') like COALESCE(@ProductBrand, PCSB.BrandName, '')
			OR
			COALESCE(PCSB.BrandShortName, '') like COALESCE(@ProductBrand, PCSB.BrandShortName, '')
			OR
			COALESCE(PCSB.SupplierName, '') like COALESCE(@ProductBrand, PCSB.SupplierName, '')
			OR
			COALESCE(PCSB.SupplierShortName, '') like COALESCE(@ProductBrand, PCSB.SupplierShortName, '')			
		)
		AND
		(
			COALESCE(MCC.[Name], '' ) Like COALESCE(@ProductCategory, MCC.[Name], '')
		)
		AND (   /* use this type of statement for Child tables of the target to avoid duplicate rows.
		If we use a join for this, we would return duplicate rows, so we just check the existence
		of one record that matches the search parameters in the child table
		*/
		(
			 (@ProductHCPCs IS NULL)
				 OR EXISTS
				 (  
					Select 
						1 
					 from 
						ProductHCPCS PHCPCS 
					 where 
						PHCPCS.PracticeCatalogProductID = PCP.PracticeCatalogProductID
						AND PHCPCS.IsActive = 1 
						and PHCPCS.HCPCS like @ProductHCPCs  
				  )
			  )			 
		 )	
		 
		 
		 
		 
		 ORDER BY PCP.IsThirdPartyProduct			

END