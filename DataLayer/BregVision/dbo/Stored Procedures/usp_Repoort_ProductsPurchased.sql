﻿/*
SELECT ID 
			 FROM dbo.udf_ParseArrayToTable('3,5', ',')

DECLARE @PracticeLocationIDString VARCHAR(2000)
SET @PracticeLocationIDString = '3,5'

select * from practicelocation as pl where
		PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))

exec usp_zzzProductsPurchased_By_ProductCodeAndCost_Given_PracticeID_PLID_Supplier3
	6, '15', 'BREG,Aircast,DJO,Procare,Tri-State Orthopedics'
SELECT * FROM zzzReportMessage

exec usp_zzzProductsPurchased_By_ProductCodeAndCost_Given_PracticeID_PLID_Supplier2
	6, '15', 'Tri-State Orthopedics'

*/



CREATE PROC [dbo].[usp_Repoort_ProductsPurchased] 
		  @PracticeID	INT
		, @PracticeLocationID VARCHAR(2000)
		, @SupplierShortName  VARCHAR(2000)
		, @StartDate		DATETIME
		, @EndDate			DATETIME
AS
BEGIN

--	DECLARE @PracticeID INT
--	SET @PracticeID = 6

--INSERT INTO zzzReportMessage (Message)
--SELECT @PracticeLocationID

--  SET @EndDate to 11:59:59 PM

SELECT @EndDate = DATEADD(DAY, 1, @EndDate)

DECLARE @PracticeLocationIDString VARCHAR(2000)
SET @PracticeLocationIDString = @PracticeLocationID

INSERT INTO zzzReportMessage (Message)
SELECT @StartDate


INSERT INTO zzzReportMessage (Message)
SELECT @EndDate

/*

SELECT * FROM zzzReportMessage

*/

--INSERT INTO zzzReportMessage (Message)
--SELECT TextValue 
--		 FROM dbo.udf_ParseStringArrayToTable( @SupplierShortName, ',')

--  SELECT * FROM zzzReportMessage
--BREG,Aircast,DJO,Procare,Tri-State Orthopedics

--SELECT * FROM dbo.PracticeCatalogSupplierBrand AS PCSB ORDER BY  IsThirdPartySupplier DESC



SELECT 
		 SUB.PracticeName
		, SUB.PracticeLocation
--		, SUB.IsThirdPartySupplier
		, SUB.SupplierShortName
		--, SUB.BrandShortName
		
		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN NULL
		ELSE SUB.BrandShortName
		END AS BrandShortName
		
		
		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN 1
		ELSE 0
		END AS IsBrandSuppressed
		
		, SUB.Sequence

		, SUB.ProductName
		, SUB.Side
		, SUB.Size
		, SUB.Gender
		, SUB.Code
		
		, CAST(SUB.ActualWholesaleCost AS DECIMAL(15,2) )	AS ActualWholesaleCost
		, SUM(SUB.QuantityOrdered)							AS QuantityOrdered
		, CAST( SUM(SUB.LineTotal)  AS DECIMAL(15,2) )		AS TotalCost
		
--		, SUB.OrderDate
FROM
	(SELECT 
		  P.PracticeName
		, PL.NAME AS PracticeLocation
		, PCSB.IsThirdPartySupplier
		, PCSB.SupplierShortName
		, PCSB.BrandShortName
		, PCSB.Sequence

		, MCP.[Name] AS ProductName
		, MCP.LeftRightSide AS Side
		, MCP.Size AS Size
		, MCP.Gender AS Gender
		, MCP.Code AS Code
		
		, SOLI.ActualWholesaleCost
		, SOLI.QuantityOrdered
		, SOLI.LineTotal

		, SOLI.CreatedDate AS OrderDate		
		
	FROM dbo.SupplierOrderLineItem AS SOLI WITH (NOLOCK)
	INNER JOIN dbo.SupplierOrder AS SO	WITH (NOLOCK)
		ON SOLI.SupplierOrderID = SO.SupplierOrderID
	INNER JOIN dbo.BregVisionOrder AS BVO	WITH (NOLOCK)
		ON BVO.BregVisionOrderID = SO.BregVisionOrderID
	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON BVO.PracticeLocationID = PL.PracticeLocationID
	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID
	INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
		ON SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
	INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
		ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
	WHERE P.PracticeID = @PracticeID		
		
		AND 
		PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))

		AND PCSB.SupplierShortName IN 			
			(SELECT TextValue 
			 FROM dbo.udf_ParseStringArrayToTable( @SupplierShortName, ',') )
	
		AND SOLI.CreatedDate >= @StartDate
		AND SOLI.CreatedDate < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM
		
	UNION
	
		SELECT 
		  P.PracticeName
		, PL.NAME AS PracticeLocation
		, PCSB.IsThirdPartySupplier
		, PCSB.SupplierShortName
		, PCSB.BrandShortName
		, PCSB.Sequence
		
		, TPP.[Name] AS ProductName
		, TPP.LeftRightSide AS Side
		, TPP.Size AS Size
		, TPP.Gender AS Gender
		, TPP.Code AS Code
		
		, SOLI.ActualWholesaleCost
		, SOLI.QuantityOrdered
		, SOLI.LineTotal
		
		, SOLI.CreatedDate AS OrderDate
				
		
	FROM dbo.SupplierOrderLineItem AS SOLI	WITH (NOLOCK)
	INNER JOIN dbo.SupplierOrder AS SO	WITH (NOLOCK)
		ON SOLI.SupplierOrderID = SO.SupplierOrderID
	INNER JOIN dbo.BregVisionOrder AS BVO	WITH (NOLOCK)
		ON BVO.BregVisionOrderID = SO.BregVisionOrderID
	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON BVO.PracticeLocationID = PL.PracticeLocationID
	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID
	INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
		ON SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
	INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
		ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
	WHERE 
		P.PracticeID = @PracticeID	

		AND PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))

		AND PCSB.SupplierShortName IN 			
			(SELECT TextValue 
		 FROM dbo.udf_ParseStringArrayToTable( @SupplierShortName, ',') )

		AND SOLI.CreatedDate >= @StartDate
		AND SOLI.CreatedDate < @EndDate --  Less than since @EndDate has one day added to it since the time is 12AM
	
	)		
		AS SUB	
		
	GROUP BY
		SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.IsThirdPartySupplier
		, SUB.Sequence
		, SUB.SupplierShortName
		, SUB.BrandShortName
		
		, SUB.ProductName
		, SUB.Side
		, SUB.Size
		, SUB.Gender
		, SUB.Code
		
		, SUB.ActualWholesaleCost
		
	ORDER BY
		SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.IsThirdPartySupplier
		, SUB.Sequence
		, SUB.SupplierShortName
		, SUB.BrandShortName
		, SUB.ProductName
		, SUB.Code
		
END