﻿

-- =============================================
--EXEC usp_User_Update_DefaultPreferences 1, 23, 1, 1,1, 0, 1, 42, 10, 'Code'
-- Author:		<Author: Mike Sneen>
-- Create date: <Create Date: 04/02/09>
-- Description:	<Description: Used to update Default User Preferences>
-- modification:
--  [dbo].[usp_User_Update_DefaultPreferences]  

-- =============================================
CREATE PROCEDURE [dbo].[usp_User_Update_DefaultPreferences]  --3 --10  --  _With_Brands
	@PracticeID int,
	@UserID int,
	@ShowProductInfo bit,
	@AllowDispense bit,
	@AllowInventory bit,
	@AllowCheckIn bit,
	@AllowReports bit,
	@AllowCart bit,
	@AllowDispenseModification bit,
	@DefaultLocationID int,
	@DefaultRecordsPerPage int,
	@DefaultDispenseSearchCriteria varchar(20),
	@AllowZeroQtyDispense bit,
	@ScrollEntirePage bit = 0,
	@DispenseQueueSortDescending bit = 0,
	@AllowPracticeUserOrderWithoutApproval bit = 0,
	@AllowInventoryClose bit = 0
AS 
    BEGIN
    DECLARE @Err INT
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
--        SET NOCOUNT ON ;
     
     Update [User]
     SET
		ShowProductInfo = @ShowProductInfo,
		AllowDispense = @AllowDispense,
		AllowInventory = @AllowInventory,
		AllowCheckIn = @AllowCheckIn,
		AllowReports = @AllowReports,
		AllowCart = @AllowCart,
		AllowDispenseModification = @AllowDispenseModification,
		AllowPracticeUserOrderWithoutApproval = @AllowPracticeUserOrderWithoutApproval,
		DefaultLocationID = @DefaultLocationID,
		DefaultRecordsPerPage = @DefaultRecordsPerPage,
		DefaultDispenseSearchCriteria = @DefaultDispenseSearchCriteria,
		AllowZeroQtyDispense = @AllowZeroQtyDispense,
		ScrollEntirePage = @ScrollEntirePage,
		DispenseQueueSortDescending = @DispenseQueueSortDescending,
		AllowInventoryClose = @AllowInventoryClose,
		ModifiedUserID = 1,
		ModifiedDate = GETDATE(),
		IsActive = 1
	WHERE
		UserID = @UserID
		AND PracticeID = @PracticeID
		
     

     
     	SET @Err = @@ERROR

	RETURN @Err

    END

--Exec usp_SuperbillCategory_Update 3, 3
