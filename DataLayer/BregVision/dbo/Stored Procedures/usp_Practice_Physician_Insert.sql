﻿/* ------------------------------------------------------------
   PROCEDURE:    usp_Practice_Physician_Insert
   
   Description:   Inserts a Physician for a Practice. 
				Inserts a record into table Contact
				and then insert a record into the table Physician
				given the PracticeID
				
				Returns: the PhysicianID and the ContactID   

   AUTHOR:       John Bongiorni 7/13/2007 5:20 PM
   
   Modifications:  
   ------------------------------------------------------------ */  
   
CREATE PROCEDURE [dbo].[usp_Practice_Physician_Insert]
(
	  @PracticeID					 INT
    , @PhysicianID					 INT	OUTPUT
    , @ContactID                     INT	OUTPUT
    , @Salutation                         VARCHAR(10) = NULL
    , @FirstName                     VARCHAR(50)
    , @MiddleName                    VARCHAR(50) = NULL
    , @LastName                      VARCHAR(50)
    , @Suffix                        VARCHAR(10) = NULL
    , @CreatedUserID                 INT
    , @SortOrder					 INT = 0
    , @Pin							 VARCHAR(4) = null

)
AS
BEGIN

	DECLARE @TransactionCountOnEntry    INT,       -- Transaction Count before the transaction begins
			@Err						INT        --  holds the @@Error code returned by SQL Server

			
	IF NULLIF(@Pin, '') IS NULL
		set @Pin = NULL
		
			
	SELECT @Err = @@ERROR
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @Err = 0
	BEGIN    
		SELECT @TransactionCountOnEntry = @@TRANCOUNT
		BEGIN TRANSACTION
	END        

	IF @Err = 0
	BEGIN

		INSERT
		INTO dbo.Contact
		(
			  Salutation
			, FirstName
			, MiddleName
			, LastName
			, Suffix
			, CreatedUserID
			, IsActive
		)
		VALUES
		(
			  @Salutation
			, @FirstName
			, @MiddleName
			, @LastName
			, @Suffix
			, @CreatedUserID
			, 1
		)

		SET @Err = @@ERROR

	END
	IF @Err = 0
	BEGIN

		SELECT @ContactID = SCOPE_IDENTITY()

	END
	IF @Err = 0
	BEGIN

		INSERT
		INTO dbo.Physician
		(
			  PracticeID
			, ContactID
			, CreatedUserID
			, IsActive
			, SortOrder
			, Pin
		)
		VALUES
		(
			  @PracticeID
			, @ContactID
			, @CreatedUserID
			, 1
			, @SortOrder
			, @Pin 
		)

		SET @Err = @@ERROR
		SELECT @PhysicianID = SCOPE_IDENTITY()

	END

	IF @@TranCount > @TransactionCountOnEntry
	BEGIN

		IF @Err = 0
			COMMIT TRANSACTION
		ELSE
			ROLLBACK TRANSACTION
			--  Add any database logging here      
	END
			
	RETURN @Err

END
















