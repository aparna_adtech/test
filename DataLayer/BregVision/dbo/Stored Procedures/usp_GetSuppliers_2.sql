﻿/*
-- =============================================
-- Author:		<Author: Greg Ross>
-- Create date: <Create Date: 07/23/07>
-- Description:	<Description: Used to get all suppliers/Brands in database>
-- modification:  jb 10072007 fix query to be effient and add thirdpartysuppliers.
--  [dbo].[usp_GetSuppliers_2]  10

--  Modification:  11/5/2007 JB Add line to 3rd party union piece
--			--  AND PCSB.isActive = 1
--  Modification:  11/27/2007 JB Add line to MC union piece
--			--  AND PCSB.isActive = 1
-- =============================================
*/

CREATE PROCEDURE [dbo].[usp_GetSuppliers_2]  
	@PracticeLocationID int
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON ;
     SELECT     DISTINCT PCSB.[SupplierName] AS Brand,
                
                
                PCSB.[MasterCatalogSupplierID] AS SupplierID,
                
				ISNULL(c.[FirstName], '') AS Name,
                ISNULL(C.[LastName], '')  AS LastName,
                ISNULL(c.[PhoneWork], '')  AS PhoneWork,
                
                ISNULL(A.[AddressLine1], '')   AS Address,
                ISNULL(A.City, '')  AS City,  
                ISNULL(A.[ZipCode], '')   AS Zipcode
				--MCC.[Name] AS Category


        FROM    [MasterCatalogSupplier] AS MCS
                INNER JOIN address A 
						ON MCS.[AddressID] = A.[AddressID]
                INNER JOIN [Contact] C 
						ON mcs.[ContactID] = c.[ContactID]

				INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
					ON MCS.MasterCatalogSupplierID = pcsb.MasterCatalogSupplierID

				INNER JOIN [PracticeCatalogProduct] PCP 
					ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID				
										
				INNER JOIN [ProductInventory] PI 
					ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]

				WHERE PI.[PracticeLocationID] = @PracticeLocationID
					AND    MCS.ISACTIVE = 1
					AND A.ISACTIVE = 1
					AND C.ISACTIVE = 1
					AND PCSB.isActive = 1	-- JB 11/05/2007
					AND PCP.ISACTIVE = 1
					AND PI.ISACTIVE = 1
					AND IsThirdPartySupplier = 0 -- MasterCatalogSuppliers.
					
UNION

     SELECT     DISTINCT PCSB.[SupplierName] AS Brand,
                
                
                TPS.[ThirdPartySupplierID] AS SupplierID,
                
				ISNULL(c.[FirstName], '') AS Name,
                ISNULL(C.[LastName], '')  AS LastName,
                ISNULL(c.[PhoneWork], '')  AS PhoneWork,
                
                ISNULL(A.[AddressLine1], '')   AS Address,
                ISNULL(A.City, '')  AS City,  
                ISNULL(A.[ZipCode], '')   AS Zipcode
				--MCC.[Name] AS Category


        FROM    [dbo].[ThirdPartySupplier] AS TPS
                LEFT JOIN address A 
						ON TPS.[AddressID] = A.[AddressID]
                lefT JOIN [Contact] C 
						ON TPS.[ContactID] = c.[ContactID]

				INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
					ON TPS.ThirdPartySupplierID = pcsb.ThirdPartySupplierID

				INNER JOIN [PracticeCatalogProduct] PCP 
					ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID				
										
				INNER JOIN [ProductInventory] PI 
					ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]

				WHERE PI.[PracticeLocationID] = @PracticeLocationID
					AND    tps.ISACTIVE = 1
					AND A.ISACTIVE = 1
					AND C.ISACTIVE = 1
					AND PCP.ISACTIVE = 1
					
					AND PCSB.isActive = 1  -- JB 11/05/2007
					
					AND PI.ISACTIVE = 1
					AND IsThirdPartySupplier = 1 -- ThirdPartySuppliers.
					
					

    END





