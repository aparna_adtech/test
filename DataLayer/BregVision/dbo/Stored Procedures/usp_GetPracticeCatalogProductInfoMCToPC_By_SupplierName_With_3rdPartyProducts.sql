﻿



-- =============================================
-- Author:		John Bongiorni
-- Create date: 2007 10 09
-- Description:	Gets ProductCatalogProduct Info for MC and 3rdParty Products,
--				Given the PSCB.SupplierName or PSCB.SupplierShortName or portion thereof.
--				Note: The only difference to _BrandName query is that 
--					for third party suppliers, the search is on the supplierName
--					vs. the BrandName.
--								
--
--			Used to get the detail for a Practice. Used in the MCToPC.aspx page
-- Modification: 20071008 2033 JB Add Third Party and code to make this generic
--  (FOR MCProducts and ThirdPartyProducts)
--				20071009 1203 JB Add WITH(NOLOCK) cLAUSES to Tables.
--			2007.12.12 JB Order By ProductShortName, Code
--	    2008.02.25 JB Added Column for "Deletable" PCPs
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetPracticeCatalogProductInfoMCToPC_By_SupplierName_With_3rdPartyProducts]

	@PracticeID int
	, @SearchText  varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


select
	Sub.PracticeID, 
	Sub.PracticecatalogProductID,
	Sub.MasterCatalogProductID	AS MasterCatalogProductID,  -- Needs to be called ProductID (to handle MC and ThirdParty)
	Sub.PracticeCatalogSupplierBrandID,
	
	Sub.SupplierShortName,
	Sub.BrandShortName,
	SUB.Sequence,  -- jb
	
	Sub.ShortName,
	Sub.Code
	
	, ISNULL( Sub.WholesaleCost, 0 )	AS WholesaleCost
	, ISNULL( Sub.BillingCharge, 0 )	AS BillingCharge
	, ISNULL( Sub.DMEDeposit, 0 )		AS DMEDeposit
	, ISNULL( Sub.BillingChargeCash, 0 ) AS BillingChargeCash
	, Sub.StockingUnits
	, Sub.BuyingUnits
		
	, Sub.IsThirdPartyProduct
	, dbo.ConcatHCPCS(Sub.PracticeCatalogProductID) AS HCPCSString -- JB 20071004 Added Column for UDF Concat of HCPCS						-- Need to be put in app code and grid to distinguish 
	, Sub.IsPreAuthorization											--  Master Catalog Product and Third Party Product.
	, Sub.IsReplaced
	, CASE (SELECT COUNT(1) 
			FROM productinventory AS PI WITH (NOLOCK) 
			WHERE PI.practiceCatalogProductID = sub.practiceCatalogProductID
		    ) WHEN 0 THEN 1 ELSE 0 END AS IsDeletable   --  JB  20080225  Added Column for "Deletable" PCPs
	
FROM
(
		select
			PCP.PracticeID, 
			PCP.PracticecatalogProductID,
			PCP.MasterCatalogProductID	AS MasterCatalogProductID,  -- Needs to be ProductID
			PCP.PracticeCatalogSupplierBrandID,
	
			PCSB.SupplierShortName,
			PCSB.BrandShortName,
			ISNULL(PCSB.Sequence, 999) AS Sequence,  -- jb
	
			MCP.ShortName,
			MCP.Code,
			PCP.WholesaleCost,
			PCP.BillingCharge,
			PCP.DMEDeposit,
			PCP.BillingChargeCash,
			PCP.StockingUnits,
			PCP.BuyingUnits,
	
			0 AS IsThirdPartyProduct,					-- Needs to know if this is a Third Party Product.
			PCP.IsPreAuthorization,
			MAX(COALESCE(PR.IsActive, 0)) AS IsReplaced

		FROM PracticeCatalogProduct AS PCP WITH(NOLOCK) 
		INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK) ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		INNER JOIN MasterCatalogProduct AS MCP WITH(NOLOCK) ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
		LEFT OUTER JOIN ProductReplacement AS PR WITH(NOLOCK) ON PR.IsActive = 1 AND
			(PR.OldMasterCatalogProductID = PCP.MasterCatalogProductID) AND
			(PR.PracticeID IS NULL OR PR.PracticeID = PCP.PracticeID)
		WHERE PCP.practiceID = @PracticeID
			AND PCP.IsThirdPartyProduct = 0		-- MasterCatalogProduct
			AND PCP.isactive = 1
			AND MCP.isactive = 1
			AND PCSB.IsActive = 1
			AND (PCSB.SupplierName LIKE @SearchText OR PCSB.SupplierShortName LIKE @SearchText)
		GROUP BY
			PCP.PracticeID, 
			PCP.PracticecatalogProductID,
			PCP.MasterCatalogProductID,
			PCP.PracticeCatalogSupplierBrandID,
			PCSB.SupplierShortName,
			PCSB.BrandShortName,
			PCSB.Sequence,
			MCP.ShortName,
			MCP.Code,
			PCP.WholesaleCost,
			PCP.BillingCharge,
			PCP.DMEDeposit,
			PCP.BillingChargeCash,
			PCP.StockingUnits,
			PCP.BuyingUnits,
			PCP.IsPreAuthorization
			

		UNION

		select
			PCP.PracticeID, 
			PCP.PracticecatalogProductID,
			PCP.ThirdPartyProductID		 AS MasterCatalogProductID,  -- Needs to be ProductID
			PCP.PracticeCatalogSupplierBrandID,
	
			PCSB.SupplierShortName,
			PCSB.BrandShortName,
			ISNULL(PCSB.Sequence, 999) AS Sequence,  -- jb
	
			TPP.ShortName,
			TPP.Code,
			PCP.WholesaleCost,
			PCP.BillingCharge,
			PCP.DMEDeposit,
			PCP.BillingChargeCash,
			PCP.StockingUnits,
			PCP.BuyingUnits,

			1 AS IsThirdPartyProduct,					-- Needs to know if this is a Third Party Product.
			PCP.IsPreAuthorization,
			MAX(COALESCE(PR.IsActive, 0)) AS IsReplaced

		FROM PracticeCatalogProduct AS PCP WITH(NOLOCK) 
		INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK) ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		INNER JOIN ThirdPartyProduct AS TPP WITH(NOLOCK) on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID 
		LEFT OUTER JOIN ProductReplacement AS PR WITH(NOLOCK) ON PR.IsActive = 1 AND
			(PR.OldThirdPartyProductID = TPP.ThirdPartyProductID) AND
			(PR.PracticeID IS NULL OR PR.PracticeID = PCP.PracticeID)
		WHERE PCP.practiceID = @PracticeID
			AND PCP.IsThirdPartyProduct = 1		-- ThirdPartyProduct
			AND PCP.isactive = 1
			AND TPP.isactive = 1
			AND TPP.Code LIKE @SearchText 			
			AND PCSB.IsActive = 1
			AND (PCSB.SupplierName LIKE @SearchText OR PCSB.SupplierShortName LIKE @SearchText)
		GROUP BY
			PCP.PracticeID, 
			PCP.PracticecatalogProductID,
			PCP.ThirdPartyProductID,
			PCP.PracticeCatalogSupplierBrandID,
			PCSB.SupplierShortName,
			PCSB.BrandShortName,
			PCSB.Sequence,
			TPP.ShortName,
			TPP.Code,
			PCP.WholesaleCost,
			PCP.BillingCharge,
			PCP.DMEDeposit,
			PCP.BillingChargeCash,
			PCP.StockingUnits,
			PCP.BuyingUnits,
			PCP.IsPreAuthorization
) AS Sub
ORDER BY
	Sub.IsThirdPartyProduct,
	SUB.Sequence,  -- jb
	Sub.ShortName, -- jb 
	Sub.Code		-- jb

END
