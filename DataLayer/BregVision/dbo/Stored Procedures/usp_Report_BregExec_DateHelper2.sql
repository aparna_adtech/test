﻿CREATE PROC usp_Report_BregExec_DateHelper2
AS
BEGIN

	DECLARE @StartDate	AS DateTime
	DECLARE @EndDate	AS DateTime
	DECLARE @StartYear	AS INT 
	DECLARE @EndYear	AS INT 

	--SET @StartYear = 2006 
	SET @StartDate = '1/6/2006' 
	SET @EndDate = '12/15/2006'
	--SET @EndDate = DATEADD(Day, 1, @EndDate) ;


	SET @StartYear	= Year(@StartDate)
	SET @EndYear	= Year(@EndDate);
	



	WITH                   Years
			  AS ( SELECT   YYYY = @StartYear
				   UNION ALL
				   SELECT   YYYY + 1
				   FROM     Years
				   Where    YYYY < @EndYear
				 ) ,
			Months
			  AS ( SELECT   MM = 1
				   UNION ALL
				   SELECT   MM + 1
				   FROM     Months
				   WHERE    MM < 12
				 ) 


	--SELECT * FROM Years
	--SELECT * FROM Months, Years
	SELECT PracticeName, YYYY, MM FROM PRactice Cross join Years, Months
	ORDER BY PracticeName, YYYY, MM

END