﻿

/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocationBillingAddress_Insert_When_Decentralized_Only
   
   Description:  Inserts a record into table Address
				  , Gets the new AddressID
				  , and Inserts a record into the PracticeLocationBillingAddress table.
   				
Note: THIS SHOULD ONLY RUN/BE CALLED WHEN PRACTICE BILLING IS DECENTRALIZED.		
Note: All address of and the practicelocationbillingaddress are deleted prior to the insert.
   Called From:
		Proc:  dbo.usp_PracticeLocationBillingAddress_UpdateInsert
		
   AUTHOR:       John Bongiorni 8/21/2007 12:35 PM
   
   Modifications:  
   ------------------------------------------------------------ */  
   
CREATE PROCEDURE [dbo].[usp_PracticeLocationBillingAddress_Insert_When_Decentralized_Only]
(
	  @PracticeLocationID			 INT
    , @AddressID                     INT = NULL OUTPUT
	, @AttentionOf					 VARCHAR(50) = NULL
    , @AddressLine1                  VARCHAR(50)
    , @AddressLine2                  VARCHAR(50) = NULL
    , @City                          VARCHAR(50)
    , @State                         CHAR(2)
    , @ZipCode                       CHAR(5)
    , @ZipCodePlus4                  CHAR(4) = NULL
    , @CreatedUserID                 INT
)
AS
BEGIN

	DECLARE @TransactionCountOnEntry    INT,       -- Transaction Count before the transaction begins
			@Err						INT        --  holds the @@Error code returned by SQL Server

	SELECT @Err = @@ERROR
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @Err = 0
	BEGIN    
		SELECT @TransactionCountOnEntry = @@TRANCOUNT
		BEGIN TRANSACTION
	END        

	
--  Delete All PracticeLocationBillingAddresses for the given PracticeLocationID.
	DELETE FROM dbo.PracticeLocationBillingAddress 
	FROM dbo.PracticeLocationBillingAddress AS PLBA
	INNER JOIN dbo.PracticeLocation AS PL
		ON PLBA.PracticeLocationID = PL.PracticeLocationID
	WHERE PL.PracticeLocationID = @PracticeLocationID	
	
	--  Delete All Addresses for the given PracticeLocationID's PracticeLocationBillingAddress.
	DELETE FROM ADDRESS 
	FROM ADDRESS AS A
	INNER JOIN dbo.PracticeLocationBillingAddress AS PLBA
		ON A.AddressID = PLBA.AddressID
	INNER JOIN dbo.PracticeLocation AS PL
		ON PLBA.PracticeLocationID = PL.PracticeLocationID
	WHERE PL.PracticeLocationID = @PracticeLocationID	

	

	IF @Err = 0
	BEGIN

		INSERT
		INTO dbo.Address
		(
			  AddressLine1
			, AddressLine2
			, City
			, State
			, ZipCode
			, ZipCodePlus4
			, CreatedUserID
			, CreatedDate
			, IsActive
		)
		VALUES
		(
			  @AddressLine1
			, @AddressLine2
			, @City
			, @State
			, @ZipCode
			, @ZipCodePlus4
			, @CreatedUserID
			, GETDATE()
			, 1
		)

		SET @Err = @@ERROR
	END

	IF @Err = 0
	BEGIN

		SELECT @AddressID = SCOPE_IDENTITY()

		SELECT @Err = @@ERROR
	END


	IF @Err = 0
	BEGIN

		INSERT
		INTO dbo.PracticeLocationBillingAddress
		(
			  PracticeLocationID
			, AddressID
			, AttentionOf
			, CreatedUserID
			, CreatedDate
			, IsActive
		)
		VALUES
		(
			  @PracticeLocationID
			, @AddressID
			, @AttentionOf
			, @CreatedUserID
			, GETDATE()
			, 1
		)

		SELECT @Err = @@ERROR
	END


	IF @@TranCount > @TransactionCountOnEntry
	BEGIN

		IF @Err = 0
			COMMIT TRANSACTION
		ELSE
			ROLLBACK TRANSACTION
			--  Add any database logging here      
	END
			
		RETURN @Err
END


