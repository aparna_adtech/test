﻿
-- =============================================
-- Author:		Aditya M S K
-- Create date: Dec 5th 2016
-- Description:	This stored procedure will be utilised to check if the given practice location has a billing address or not
-- =============================================
CREATE PROCEDURE [dbo].[usp_PracticeLocationBillingAddressExists] 
	
	@PracticeLocationId INT
		
	AS
BEGIN
	
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT TOP 1 AddressId FROM PracticeLocationBillingAddress WHERE PracticeLocationID = @PracticeLocationId)
	RETURN 1
	ELSE
	RETURN 0
	
END

