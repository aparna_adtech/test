﻿
-- =============================================
-- Author:		Greg Ross
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- Modifications:  20071012 JB  Switch supplierID to PCP.practicecatalogsupplierbrandID.
-- =============================================
CREATE PROCEDURE [dbo].[usp_Get_Dispense_Records_for_Browse_Grid] -- 3
	-- Add the parameters for the stored procedure here
	@PracticeLocationID int
AS
BEGIN


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		
		SUB.ProductInventoryID,			
		SUB.SupplierID,
		SUB.SupplierName,
		SUB.BrandName,
		SUB.ProductName,
		SUB.Code,
		--  SUB.IsThirdPartySupplier
		
		ISNULL(SUB.Side, '') as Side,
		ISNULL(SUB.Size, '') AS Size, 
		ISNULL(SUB.QuantityOnHandPerSystem, 0) AS QuantityOnHand
		
    FROM
    (  
		SELECT
    
		PI.ProductInventoryID, 
		
		
		--  20071012 JB  Swithch out.
		PCP.practicecatalogsupplierbrandID as SupplierID,
		--PCSB.MasterCatalogSupplierID as SupplierID,
		PCSB.SupplierName,
		PCSB.BrandName,
		MCP.Name as ProductName,
		MCP.Code,

		MCP.LeftRightSide as Side,
		MCP.Size, 
		PI.QuantityOnHandPerSystem		
		
		--  PCSB.IsThirdPartySupplier
		 from productInventory PI 
		inner join practicecatalogproduct PCP on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
		inner join practicecatalogsupplierbrand PCSB on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID
		inner join MasterCatalogProduct MCP on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
		where PI.isactive =1 
		and PCP.isactive = 1
		and PCSB.isactive = 1
		and MCP.isactive = 1
		and PI.PracticeLocationID = @PracticeLocationID
		group by PCSB.BrandName,PCSB.SupplierName,MCP.Name,Code, 
						MCP.LeftRightSide,
						MCP.Size, PI.ProductInventoryID
						, PCP.practicecatalogsupplierbrandID --PCSB.MasterCatalogSupplierID
						, PI.QuantityOnHandPerSystem	
	UNION
	
	      select 
				
				PI.ProductInventoryID, 				
				
				--  20071012 JB  Swithch out supplierID.
				PCP.practicecatalogsupplierbrandID as SupplierID,
				--PCSB.ThirdPartySupplierID as SupplierID,
				
				PCSB.SupplierName,
				PCSB.BrandName,
				TPP.Name as ProductName,
				TPP.Code,
				--  PCSB.IsThirdPartySupplier
				TPP.LeftRightSide as Side,
				TPP.Size,
				PI.QuantityOnHandPerSystem

		from productInventory PI 
		inner join practicecatalogproduct PCP on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
		inner join practicecatalogsupplierbrand PCSB on PCP.practicecatalogsupplierbrandID = PCSB.practicecatalogsupplierbrandID
		inner join ThirdPartyProduct TPP on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
		where PI.isactive =1 
		and PCP.isactive = 1
		and PCSB.isactive = 1
		and TPP.isactive = 1
		and PI.PracticeLocationID = @PracticeLocationID
		group by PCSB.BrandName,PCSB.SupplierName,TPP.Name, TPP.Code, TPP.LeftRightSide,
				TPP.Size, PI.ProductInventoryID, PCP.practicecatalogsupplierbrandID, PI.QuantityOnHandPerSystem --PCSB.ThirdPartySupplierID

	) AS Sub
	GROUP BY
		--  SUB.IsThirdPartySupplier,
		SUB.SupplierName,
		SUB.BrandName,
		SUB.ProductName,
		SUB.Code,
		SUB.ProductInventoryID,			
		SUB.SupplierID,
		ISNULL(SUB.Side, ''),
		ISNULL(SUB.Size, ''), 
		ISNULL(SUB.QuantityOnHandPerSystem, 0) 
	ORDER BY
		SUB.SupplierName,
		SUB.BrandName,
		SUB.ProductName,
		SUB.Code


END

