﻿

/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocation_Insert_Email
   
   Description:  Inserts a record into table PracticeLocation.
   
   AUTHOR:       John Bongiorni 7/23/2007 11:05:42 AM
   
   Modifications:  
   ------------------------------------------------------------ */  ------------------ INSERT ------------------
   
CREATE PROCEDURE [dbo].[usp_PracticeLocation_Insert_Email]
(
      @PracticeLocationID				INT			= NULL OUTPUT
    , @PracticeID						INT
    , @Name								VARCHAR(50)
    , @IsPrimaryLocation				BIT			= 0
    , @EmailForOrderApproval			VARCHAR(100)
    , @EmailForLowInventoryAlerts		VARCHAR(100)
    , @IsEmailForLowInventoryAlertsOn	BIT
    , @CreatedUserID					INT
)
AS
DECLARE
    @TransactionCountOnEntry    INT,       -- Transaction Count before the transaction begins
    @Err             			INT        --  holds the @@ERROR code returned by SQL Server

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT @TransactionCountOnEntry = @@TRANCOUNT
	SELECT @Err = @@ERROR
	BEGIN TRANSACTION      

	IF @Err = 0
	BEGIN

		INSERT
		INTO dbo.PracticeLocation
		(
			  PracticeID     
			, Name
			, IsPrimaryLocation
			, EmailForOrderApproval
			, EmailForLowInventoryAlerts
			, IsEmailForLowInventoryAlertsOn
			, CreatedUserID
			, IsActive
		)
		VALUES
		(
			  @PracticeID
			, @Name
			, @IsPrimaryLocation
			, @EmailForOrderApproval
			, @EmailForLowInventoryAlerts
			, @IsEmailForLowInventoryAlertsOn
			, @CreatedUserID
			, 1
		)

		SET @Err = @@ERROR

	END
	IF @Err = 0
	BEGIN

		SELECT @PracticeLocationID = SCOPE_IDENTITY()
		SET @Err = @@ERROR
	
	END
	IF @Err = 0
	BEGIN
	
		EXEC usp_PracticeLocation_Set_Primary_Location @PracticeLocationID = @PracticeLocationID
					
		SET @Err = @@ERROR	
		
	END

	IF @@TranCount > @TransactionCountOnEntry
	BEGIN

		IF @Err = 0
			BEGIN 
				COMMIT TRANSACTION
				RETURN @Err
			END
		ELSE
			BEGIN
				ROLLBACK TRANSACTION
				RETURN @Err
				--  Add any database logging here      
			END
	END




