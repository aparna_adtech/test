﻿



-- =============================================
-- Author:		<Author: Greg Ross>
-- Create date: <Create Date: 07/23/07>
-- Description:	<Description: Used to get all suppliers/Brands in database>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetSuppliers_Backup]
	@PracticeLocationID int
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON ;
     SELECT     DISTINCT MCS.[SupplierName] AS Brand,
                MCS.[MasterCatalogSupplierID] AS SupplierID,
				c.[FirstName] AS Name,
                C.[LastName],
                c.[PhoneWork],
                A.[AddressLine1] AS Address,
                A.City AS City,
                A.[ZipCode] AS Zipcode
				--MCC.[Name] AS Category
        FROM    [MasterCatalogSupplier] MCS
                INNER JOIN address A ON MCS.[AddressID] = A.[AddressID]
                INNER JOIN [Contact] C ON mcs.[ContactID] = c.[ContactID]
				INNER JOIN [MasterCatalogCategory] MCC ON MCS.[MasterCatalogSupplierID] = MCC.[MasterCatalogSupplierID]
				INNER JOIN [MasterCatalogSubCategory] MCSC ON MCC.[MasterCatalogCategoryID] = MCSC.[MasterCatalogCategoryID]
				INNER JOIN [MasterCatalogProduct] MCP ON MCSC.[MasterCatalogSubCategoryID] = MCP.[MasterCatalogSubCategoryID]
				INNER JOIN [PracticeCatalogProduct] PCP ON MCP.[MasterCatalogProductID] = PCP.[MasterCatalogProductID]
				INNER JOIN [ProductInventory] PI ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
				WHERE PI.[PracticeLocationID] = @PracticeLocationID

    END





