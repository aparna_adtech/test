﻿--	  4. Insert a Master Catalog Product Into the
--			Practice's Catalog
--      This called the 3 nested procs:
--		dbo.usp_MasterCatalogSupplier_Get_SupplierID_From_ProductID	
--		dbo.usp_ProductCatalogSupplierBrand_Insert_PracticeID_By_MasterCatalogSupplierID	
--		dbo.usp_ProductCatagoryProuct_Insert_By_ProductID

CREATE PROC [dbo].[usp_Insert_PracticeCatalogProductID_From_MasterCatalogProductID]
(
			  @PracticeID						INT
			, @MasterCatalogProductID			INT
			, @UserID							INT = 0   -- for testing only.
			, @PracticeCatalogProductID			INT OUTPUT
)
AS
BEGIN

DECLARE @MasterCatalogSupplierID   INT	--  MCSupplierID of the MCProductID 
DECLARE @PracticeCatalogSupplierBrandID INT --  ProductCatalogSupplierBrand Identifier for the MCSupplierID


		--  Document
		EXEC dbo.usp_MasterCatalogSupplier_Select_MCSupplierID_By_MCProductID 
					@MasterCatalogProductID
					, @MasterCatalogSupplierID OUT
					
		

		--  Document
		EXEC usp_PracticeCatalogSupplierBrand_Insert_BY_PracticeID_AND_MasterCatalogSupplierID
					  @PracticeID	--  1
					, @MasterCatalogSupplierID					--  1
					, @UserID    		--  0 for testing
					, @PracticeCatalogSupplierBrandID OUT
				
		--  Document		
		EXEC dbo.usp_Insert_PracticeCatalogProductID_From_MasterCatalogProductID			
			   @PracticeID						
			,  @PracticeCatalogSupplierBrandID  
			,  @MasterCatalogProductID			
			,  @UserID			
			,  @PracticeCatalogProductID OUT
						
END			