﻿--  usp_BregVisionOrder_Insert_From_ShoppingCartID  414


CREATE PROCEDURE dbo.usp_BregVisionOrder_Insert_From_ShoppingCartID99
	  @ShoppingCartID INT
	, @UserID INT = -99
	, @CustomPurchaseOrderCode VARCHAR(20) = ''
AS
BEGIN

DECLARE @BVOID INT			--  BregVisionOrderID that is created.
DECLARE @PurchaseOrder INT	--  Purchase Order.  (@BVOID + @PurchaseOrder)

	BEGIN TRAN
	--BEGIN TRY
		INSERT INTO dbo.BregVisionOrder 
			(
				PracticeLocationID,  
				BregVisionOrderStatusID,  
				ShippingTypeID,				 
				--  PurchaseOrder,				-- to be gather later.
				CustomPurchaseOrderCode,	-- from input.
				--  Total,						-- update later.
				CreatedUserID,				-- from parameter.
				--  CreatedDate,				-- default getdate
				ModifiedUserID,				-- from parameter.
				--  ModifiedDate,				-- default getdate()
				IsActive					-- set as 1.
			) 
		SELECT 
			  SC.PracticeLocationID
			, 1 AS BregVisionOrderStatusID  --  Ordered
			, SC.ShippingTypeID
			, @CustomPurchaseOrderCode AS CustomPurchaseOrderCode  --  Not Yet implemented.
			, @UserID AS CreatedUserID
			, @UserID AS ModifiedUserID
			, 1 AS IsActive 		
		FROM ShoppingCart AS SC
		WHERE SC.ShoppingCartID = @ShoppingCartID
			AND SC.ShoppingCartStatusID = 0  -- Pending (as opposed to Denied.)
			AND SC.IsActive = 1
		

		 
			SELECT  @BVOID = SCOPE_IDENTITY()  --  New BregVisionOrderID that was just inserted.
			SELECT @PurchaseOrder = 100000 + @BVOID
			
		--  Leave this here for not yet known reason this works.	
		SELECT 
			  @BVOID AS BVOID
			, @PurchaseOrder AS PurchaseOrder
										
		UPDATE BregVisionOrder
		SET PurchaseOrder = @PurchaseOrder
		WHERE BregVisionOrderID = @BVOID	
		
		

		--  SELECT * FROM dbo.BregVisionOrder WHERE BregVisionOrderID = @BVOID   --= --ORDER BY BregVisionOrderID DESC

		--  INSERT INTO dbo.SupplierOrder, the PracticeCatalogSupplierBrandIDs related to the cart items.
		INSERT INTO dbo.SupplierOrder
		(
			 BregVisionOrderID,
			 PracticeCatalogSupplierBrandID,
			 SupplierOrderStatusID,
			 PurchaseOrder,
			 CustomPurchaseOrderCode,
			 CreatedUserID,
			 ModifiedUserID,
			 IsActive
		)
		SELECT DISTINCT
			  @BVOID AS BregVisionOrderID
			, PCP.PracticeCatalogSupplierBrandID
			, 1	AS SupplierOrderStatusID	--  Ordered
			, @PurchaseOrder	AS PurchaseOrder
			, @CustomPurchaseOrderCode	AS CustomPurchaseOrderCode
			, @UserID AS CreatedUserID
			, @UserID AS ModifiedUserID
			, 1 AS IsActive
 		FROM ShoppingCart AS SC
		INNER JOIN ShoppingCartItem AS SCI
			ON SC.ShoppingCartID = SCI.ShoppingCartID
		INNER JOIN dbo.PracticeCatalogProduct AS PCP
			ON PCP.PracticeCatalogProductID = SCI.PracticeCatalogProductID
		WHERE 
			SC.ShoppingCartID = @ShoppingCartID
			AND SC.IsActive = 1
			AND SCI.IsActive = 1
			AND PCP.IsActive = 1
			AND SC.ShoppingCartStatusID = 0
			AND SCI.ShoppingCartItemStatusID = 0
			
--			
		SELECT * 
		FROM dbo.SupplierOrder 
		WHERE BregVisionOrderID = @BVOID   --ORDER BY SUPPLIERORDERid DESC  --WHERE BregVisionOrderID = @BVOID 
--			


		



	ROLLBACK TRAN
	
	--END TRY
	--BEGIN CATCH
		--SELECT  'Error' AS CATCHMessage
		--ROLLBACK TRAN
	--END CATCH

END

--	SELECT  
--		PCP.PracticeCatalogSupplierBrandID
--	FROM ShoppingCart AS SC
--	INNER JOIN ShoppingCartItem AS SCI
--		ON SC.ShoppingCartID = SCI.ShoppingCartID
--	INNER JOIN dbo.PracticeCatalogProduct AS PCP
--		ON PCP.PracticeCatalogProductID = SCI.PracticeCatalogProductID
----	INNER JOIN PracticeCatalogSupplierBrand AS PCSB
----		ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID	
--	WHERE 
--		SCI.ShoppingCartID = 414  --@ShoppingCartID
		
