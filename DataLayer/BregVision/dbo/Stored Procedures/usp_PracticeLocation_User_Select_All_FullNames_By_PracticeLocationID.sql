﻿/*
	Get the Full Names of all Practice Users that are associated 
	with a give PracticeLocation.  
	
	John Bongiorni  09/24/2007  
*/

CREATE PROCEDURE [dbo].[usp_PracticeLocation_User_Select_All_FullNames_By_PracticeLocationID]  
  @PracticeLocationID INT
AS
BEGIN 
 SELECT
    U.UserID
   , C.ContactID
-- , C.Salutation
-- , C.FirstName
-- , C.MiddleName
-- , C.LastName
-- , C.Suffix

	, RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix) AS UserFullName
 
 FROM dbo.Contact AS C
 INNER JOIN dbo.[User] AS U
		ON C.ContactID = U.ContactID
 INNER JOIN dbo.PracticeLocation_User AS PLU
		ON U.[UserID]= PLU.UserID
 WHERE
	PLU.PracticeLocationID = @PracticeLocationID
	AND PLU.IsActive = 1
	AND U.IsActive = 1
	AND C.IsActive = 1
END