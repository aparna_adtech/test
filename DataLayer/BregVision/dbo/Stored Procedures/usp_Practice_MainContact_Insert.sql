﻿

/* ------------------------------------------------------------
   PROCEDURE:    usp_Practice_MainContact_Insert
   
   Description:  Inserts a record into table Contact
   
   AUTHOR:       John Bongiorni 7/13/2007 5:20 PM
   
   Modifications:  
   ------------------------------------------------------------ */  
   
CREATE PROCEDURE [dbo].[usp_Practice_MainContact_Insert]
(
      @ContactID                     INT = NULL OUTPUT
    , @Title                         VARCHAR(10) = NULL
    , @FirstName                     VARCHAR(50)
    , @MiddleName                    VARCHAR(50) = NULL
    , @LastName                      VARCHAR(50)
    , @Suffix                        VARCHAR(10) = NULL
    , @EmailAddress                  VARCHAR(100) = NULL
    , @PhoneWork                     VARCHAR(25) = NULL
    , @PhoneCell                     VARCHAR(25) = NULL
    , @PhoneHome                     VARCHAR(25) = NULL
    , @Fax                           VARCHAR(25) = NULL
    , @CreatedUserID                 INT
    , @IsActive                      BIT = 1
)
AS
BEGIN
	DECLARE @Err INT

	INSERT
	INTO Contact
	(
          Title
        , FirstName
        , MiddleName
        , LastName
        , Suffix
        , EmailAddress
        , PhoneWork
        , PhoneCell
        , PhoneHome
        , Fax
        , CreatedUserID
        , IsActive
	)
	VALUES
	(
          @Title
        , @FirstName
        , @MiddleName
        , @LastName
        , @Suffix
        , @EmailAddress
        , @PhoneWork
        , @PhoneCell
        , @PhoneHome
        , @Fax
        , @CreatedUserID
        , @IsActive
	)

	SET @Err = @@ERROR
	SELECT @ContactID = SCOPE_IDENTITY()

	RETURN @Err
END


