﻿
-- =============================================
-- Author:		<Author: Tom Thomas>
-- Create date: <Create Date: 03/12/10>
-- Description:	<Description: Update HelpEntity and update HelpEntityTree.
-- =============================================
CREATE PROCEDURE [dbo].[usp_UpdateHelpEntity]  
	@Id int 
	,@EntityId int 
	,@ParentId int
	,@EntityTypeId int
	,@SortOrder int = 0
	,@Title nvarchar(200)
	,@Description nvarchar(Max) = null
	,@Url nvarchar(200)= null
	
AS 
    BEGIN
	
	SET NOCOUNT ON;
	
	IF @EntityTypeId = 1 --Group
	BEGIN
		Update HelpGroups set Title=@Title 
		Where Id=@EntityId
	END
	ELSE IF @EntityTypeId = 2 --Article
	BEGIN
		Update HelpArticles Set Title=@Title, Description=@Description
		Where Id=@EntityId
	END
	ELSE IF @EntityTypeId = 4 --Media
	BEGIN
		Update HelpMedia Set Title=@Title, Description=@Description,Url=@Url
		Where Id=@EntityId
	END
	
	Update HelpEntityTree Set ParentId=@ParentId,SortOrder=@SortOrder
	Where Id=@Id
	
	END



