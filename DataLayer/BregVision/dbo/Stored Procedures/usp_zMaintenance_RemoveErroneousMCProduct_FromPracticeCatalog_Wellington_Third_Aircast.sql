﻿CREATE PROC usp_zMaintenance_RemoveErroneousMCProduct_FromPracticeCatalog_Wellington_Third_Aircast
AS
BEGIN

	--  Get PracticeCatalogSupplierBrandID given the PracticeID, if the supplier is third party, and the brandname.
	SELECT * FROM dbo.PracticeCatalogSupplierBrand AS pcsb   --  PracticeCatalogSupplierBrandID = 107
	WHERE practiceid = 6
	AND IsThirdPartySupplier = 1
	AND brandName = 'AirCast'

	--  Get the PracticeCatalogProducts for the bogus PracticeCatalogSupplierBrandID
	SELECT * FROM dbo.PracticeCatalogProduct    --  PracticeCatalogProduct = 2061
	WHERE practiceCatalogSupplierBrandID = 106

	--  Get the bogus inventory for the bogus PCProduct for the bogus PracticeCatalogSupplierBrandID
	SELECT * FROM dbo.ProductInventory 
	WHERE PracticeCatalogProductID IN ( 

										SELECT PracticeCatalogProductID 
										FROM dbo.PracticeCatalogProduct 
										WHERE practiceCatalogSupplierBrandID = 106
									  )
	-- None are product inventory for this case.
	--  If in productInventory then go through all other tables.
		
--	--  Delete from PracticeCatalogProduct
--		DELETE FROM dbo.PracticeCatalogProduct 
--		WHERE practiceCatalogSupplierBrandID = 106
	
--	--  "Delete the PCSB", by setting the PCSB to isActive = 0 
--	UPDATE dbo.PracticeCatalogSupplierBrand --  PracticeCatalogSupplierBrandID = 107
--	SET IsActive = 0
--	WHERE practiceid = 6
--	AND IsThirdPartySupplier = 1
--	AND BrandName = 'AirCast'
--	AND PracticeCatalogSupplierBrandID = 106

END




