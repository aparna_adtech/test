﻿CREATE PROC usp_zzzPCSupplierBrand_Prep_DisplayOrder
AS
BEGIN
	

	SELECT 
		Sub.PracticeCatalogSupplierBrandID
		, Sub.PracticeName
		, Sub.IsThirdPartySupplier
		, Sub.SupplierShortName
		, Sub.BrandShortName
		, Sub.Sequence
		, Sub.Sort
		, Sub.IsActive
	FROM
	(
	SELECT 
		PCSB.PracticeCatalogSupplierBrandID
		, P.PracticeName
		, PCSB.IsThirdPartySupplier
		, PCSB.SupplierShortName
		, PCSB.BrandShortName
		, PCSB.Sequence
		, 0 AS Sort
		, PCSB.IsActive
	FROM dbo.PracticeCatalogSupplierBrand AS PCSB
	INNER JOIN Practice AS P
		ON PCSB.PracticeID = P.PracticeID
	WHERE PCSB.IsActive = 1
	AND PCSB.SupplierShortName = 'Breg'
	--AND P.PracticeID IN (1,9)	

	UNION

	SELECT 
		PCSB.PracticeCatalogSupplierBrandID
		, P.PracticeName
		, PCSB.IsThirdPartySupplier
		, PCSB.SupplierShortName
		, PCSB.BrandShortName
		, PCSB.Sequence
		, 1 AS Sort
		, PCSB.IsActive
	FROM dbo.PracticeCatalogSupplierBrand AS PCSB
	INNER JOIN Practice AS P
		ON PCSB.PracticeID = P.PracticeID
	WHERE PCSB.IsActive = 1
	AND PCSB.SupplierShortName <> 'Breg'
	--AND P.PracticeID  IN (1,9)
	) AS Sub
	ORDER BY 
		Sub.PracticeName
		, Sub.IsThirdPartySupplier
		, Sub.Sort
		, Sub.SupplierShortName
		, Sub.BrandShortName
		
		--SELECT * FROM practice
		
END		