﻿CREATE PROCEDURE dbo.usp_ProductInventory_Delete79_helper
AS
BEGIN

		DECLARE @ProductInventoryID INT
		DECLARE @PracticeLocationID INT
		DECLARE @PracticeCatalogProductID INT
		DECLARE @Count_ProductInventory_SOLI_OnOrder INT  --  On Order in SupplierOrderLineItem table, none checked in.
			
			
		SET @ProductInventoryID = 2  --6  --4   --5  --4



		SELECT 
			  @PracticeLocationID   = Sub.PracticeLocationID
			, @PracticeCatalogProductID = Sub.PracticeCatalogProductID
		FROM 
			(
				SELECT 
						PracticeLocationID,
						PracticeCatalogProductID
				FROM [ProductInventory] AS PI	WITH (NOLOCK)
				WHERE PI.[ProductInventoryID] = @ProductInventoryID
			 ) AS Sub
			

			
			
				SELECT 
					  PI.ProductInventoryID
					, SOLI.SupplierOrderLineItemID
					, SOLI.QuantityOrdered
					, SUM(PI_OCI.Quantity) AS TotalQuantityCheckedIn
				
				
				FROM dbo.BregVisionOrder AS BVO					WITH (NOLOCK)
				
				INNER JOIN dbo.SupplierOrder AS SO				WITH (NOLOCK)
					ON BVO.BregVisionOrderID = SO.BregVisionOrderID
					AND BVO.PracticeLocationID = @PracticeLocationID
					
				--  Multiple lines below here...
				INNER JOIN dbo.SupplierOrderLineItem AS SOLI	WITH (NOLOCK)
					ON SO.SupplierOrderID = SOLI.SupplierOrderID
					
				LEFT JOIN dbo.ProductInventory_OrderCheckIn AS PI_OCI  WITH (NOLOCK)
					ON SOLI.SupplierOrderLineItemID = PI_OCI.SupplierOrderLineItemID
				
				INNER JOIN dbo.ProductInventory AS PI
					ON PI_OCI.ProductInventoryID = PI.ProductInventoryID
					AND PI.PracticeLocationID = @PracticeLocationID
					AND PI_OCI.ProductInventoryID = @ProductInventoryID
					
				WHERE 
					BVO.PracticeLocationID = @PracticeLocationID
					AND SOLI.QuantityOrdered > ISNULL(PI_OCI.Quantity, 0)  --  Where order qty is more the checked in or none are checked in.
					--AND ISNULL( MAX(  CAST(PI_OCI.IsEntireLineItemCheckedIn AS INT) ), 0 ) = 0    --  EntireLineItemIsNotCheckedIn or none are checked in.

				

				GROUP BY 
					  PI.ProductInventoryID
					, SOLI.SupplierOrderLineItemID
					, SOLI.QuantityOrdered
			
				HAVING ISNULL( MAX(  CAST(PI_OCI.IsEntireLineItemCheckedIn AS INT) ), 0 ) = 0    --  EntireLineItemIsNotCheckedIn or none are checked in.

			
			
			
			
			SELECT 
							@Count_ProductInventory_SOLI_OnOrder 
							  = SUB.Count_ProductInventory_SOLI_OnOrder
						FROM
							(			
								SELECT 
									COUNT(1) AS Count_ProductInventory_SOLI_OnOrder
								
								FROM dbo.BregVisionOrder AS BVO					WITH (NOLOCK)
								
								INNER JOIN dbo.SupplierOrder AS SO				WITH (NOLOCK)
									ON BVO.BregVisionOrderID = SO.BregVisionOrderID
									AND BVO.PracticeLocationID = @PracticeLocationID
									
								INNER JOIN dbo.SupplierOrderLineItem AS SOLI	WITH (NOLOCK)
									ON SO.SupplierOrderID = SOLI.SupplierOrderID
									
								LEFT JOIN dbo.ProductInventory_OrderCheckIn AS PI_OCI  WITH (NOLOCK)
									ON SOLI.SupplierOrderLineItemID = PI_OCI.SupplierOrderLineItemID
								
								INNER JOIN dbo.ProductInventory AS PI
									ON PI_OCI.ProductInventoryID = PI.ProductInventoryID
									AND PI.PracticeLocationID = @PracticeLocationID
									AND PI_OCI.ProductInventoryID = @ProductInventoryID
									
								WHERE 
									BVO.PracticeLocationID = @PracticeLocationID
									AND SOLI.QuantityOrdered > ISNULL(PI_OCI.Quantity, 0)  --  Where order qty is more the checked in or none are checked in.
									AND ISNULL(PI_OCI.IsEntireLineItemCheckedIn, 0) = 0    --  EntireLineItemIsNotCheckedIn or none are checked in.
								
							) AS SUB	
							
							
		SELECT @Count_ProductInventory_SOLI_OnOrder AS Count_ProductInventory_SOLI_OnOrder
		
END		