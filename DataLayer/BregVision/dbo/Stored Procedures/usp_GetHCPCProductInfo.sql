﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[usp_GetHCPCProductInfo]
	@PracticeCatalogProductID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select 
	MCP.ShortName
	,MCP.CODE
	,MCP.Packaging
	,MCP.LeftRightSide
	,MCP.Size
 from PracticeCatalogProduct PCP
	inner join MasterCatalogProduct MCP
		on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
 	 where PCP.PracticeCatalogProductID = @PracticeCatalogProductID
END

