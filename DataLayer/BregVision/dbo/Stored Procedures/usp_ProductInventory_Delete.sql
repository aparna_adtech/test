﻿
/* ------------------------------------------------------------
   PROCEDURE:    [dbo].[usp_ProductInventory_Delete]
   
   Description:  "Deletes" a record from table dbo.ProductInventory
   					This actual flags the IsActive column to false.
					
   Details:      MWS 1/3/2011  Refactored to Set all associated records to inactive and allow a delete
   
				There are three case when the user attempts to 
				 delete a product in the product inventory table.
				Denoted by the @RemovalType variable.
				0. Unable to be removed due to the product have QOH, of Inv. Levels other than 0
				1. Hard Delete, no dependecies in other tables.  Delete.  Should be in the ProductInventory audit table.
				2. Soft Delete, some dependencies, but still okay.  Set Inactive to true.  Product still viewable in reports.		 
				 
   Dependencies: 
   
   Pages used by:  DAL.PracticeLocation.Inventory.cs
   Accessed from web page:  Admin\PracticeLocation_ProductInventory.aspx
   
   AUTHOR:       John Bongiorni 02/05/2008 12:54:42 AM
   
   Modifications:
   MWS 1/3/2011  Removed hard delete and refactored to set all associated records to inactive and allow a delete
   If hard delete, then delete, if not the set to IsActive = False.
   Check for dependencies.

   ------------------------------------------------------------ */ 


CREATE PROCEDURE [dbo].[usp_ProductInventory_Delete]  
    (
        @ProductInventoryID		INT
      , @UserID					INT
      , @RemovalType			INT  OUTPUT
      , @DependencyMessage		VARCHAR(2000) OUTPUT  -- To application / user.
      , @IsAdmin				BIT = 0
    )
AS 
BEGIN

	IF(@IsAdmin = 1)
		EXEC usp_ProductInventory_Admin_Delete @ProductInventoryID, @UserID, @RemovalType OUTPUT, @DependencyMessage OUTPUT
	ELSE
	BEGIN --else
		BEGIN TRY	
    
			DECLARE @PracticeLocationID INT
			DECLARE @PracticeCatalogProductID INT
			
			DECLARE @SumOfAllLevels		INT
			DECLARE @Count_Cart INT 			-- Prerequisite for removal.  No items may be in the shopping cart.
			DECLARE @Count_SOLI_NotFullyCheckedIn int  -- Prerequisite for removal.  No ordered items may outstanding (not fully checked-in).

			DECLARE @Count_SupplierOrderLineItemCount INT 
			DECLARE @Count_ProductInventory_DispenseDetail  INT
			DECLARE @Count_ProductInventory_OrderCheckIn INT 
			DECLARE @Count_ProductInventory_DispenseDetail_PartiallyCheckedIn INT  --  Misnomer fix this!!!  -- Partially Checked in.  Cannot delete is more than 0.
			DECLARE @Count_ProductInventory_SOLI_OnOrder INT  --  On Order in SupplierOrderLineItem table, none checked in.

			DECLARE @Count_ProductInventory_ManualCheckIn INT
			DECLARE @Count_ProductInventory_ReturnedToSupplier int
			DECLARE @Count_ProductInventory_Purge INT
			DECLARE @Count_ProductInventory_QuantityOnHandPhysical INT
			DECLARE @Count_ProductInventory_ResetQuantityOnHandPerSystem Int
			
			DECLARE @Count_Removable INT	--  If more than zero than productInventoryID is not removable.
			DECLARE @DependencyCount INT
			
			SET @DependencyMessage = ''
			
			--  Get the PracticeLocationID and the PracticeCatalogProductID given the @ProductInventoryID. 
			SELECT 
				  @PracticeLocationID   = Sub.PracticeLocationID
				, @PracticeCatalogProductID = Sub.PracticeCatalogProductID
			FROM 
				(
				 SELECT 
					PracticeLocationID,
					PracticeCatalogProductID
				 FROM [ProductInventory] AS PI	WITH (NOLOCK)
				 WHERE PI.[ProductInventoryID] = @ProductInventoryID
				 ) AS Sub
			--  AND PI.IsActive = 1
			
			
			--  TEST PREREQUISITES FOR REMOVAL - BEGIN
			--  Test Prerequisite that QuantityOnHandPerSystem, PI.ParLevel, PI.ReorderLevel, PI.CriticalLevel are all zero.
			SELECT @SumOfAllLevels = SUB.SumOfAllLevels
			FROM
				(
					SELECT 
						PI.QuantityOnHandPerSystem
						+ PI.ParLevel
						+ PI.ReorderLevel
						+ PI.CriticalLevel AS SumOfAllLevels						
					FROM ProductInventory AS PI	WITH (NOLOCK)
					WHERE PI.ProductInventoryID = @ProductInventoryID	
					--  AND PI.IsActive = 1
				) AS SUB
			
			SELECT @SumOfAllLevels AS SumOfAllLevels
			IF (@SumOfAllLevels > 0) 
				BEGIN
					PRINT 'SumofAllLevels: ' + CAST(@SumOfAllLevels AS VARCHAR(10))
					SET @DependencyMessage = @DependencyMessage + ', Levels' 
				END
    
    
    
			--  Check Shopping Cart.  
			--  Tested: the following works and has been tested (unit)
			SELECT 
				@Count_Cart = SUB.Count_Cart
			FROM
				(
					SELECT  			
						COUNT(1) AS Count_Cart  --SC.[PracticeLocationID], SI.[PracticeCatalogProductID] 			
					FROM ShoppingCart AS SC WITH (NOLOCK)
					INNER JOIN ShoppingCartItem AS SI WITH (NOLOCK)
						ON SC.ShoppingCartID = SI.ShoppingCartID
					--ORDER BY SI.practicecatalogproductID
					WHERE 
						      SI.PracticeCatalogProductID = @PracticeCatalogProductID
						  AND SC.PracticeLocationID = @PracticeLocationID
					  --  AND PI.IsActive = 1
				) AS SUB
						
			SELECT @Count_Cart AS Count_Cart
			
			IF (@Count_Cart > 0) 
				BEGIN
					PRINT 'Count in Cart: ' + CAST(@Count_Cart AS VARCHAR(10))
    				SET @DependencyMessage = @DependencyMessage + ', Cart' 
				END

			--  Check that there are no outstanding order items for the product.  ie not fully checked in.	
			--  Get count of SOLineItems that are not fully checked in for the productinventoryID.
				
				SELECT 
					@Count_SOLI_NotFullyCheckedIn = SUB.Count_SOLI_NotFullyCheckedIn								
				FROM
				(
					SELECT 
						COUNT(1) AS Count_SOLI_NotFullyCheckedIn
						--SUM(SOLI.QuantityOrdered) AS SOLI_Ordered_NotFullyCheckedIn
								
					FROM dbo.ProductInventory AS PI WITH(NOLOCK)
				    
					INNER JOIN dbo.PracticeCatalogProduct AS PCP WITH(NOLOCK) 
						ON PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
						AND PI.ProductInventoryID = @ProductInventoryID
						
					INNER JOIN dbo.BregVisionOrder AS BVO WITH(NOLOCK)
						ON PI.PracticeLocationID = BVO.PracticeLocationID
				        
					INNER JOIN dbo.SupplierOrder AS SO WITH(NOLOCK)
						ON BVO.BregVisionOrderID = SO.BregVisionOrderID      
				        
					INNER JOIN dbo.SupplierOrderLineItem AS SOLI WITH(NOLOCK)
						ON SO.SupplierOrderID = SOLI.SupplierOrderID 
						  AND PCP.PracticeCatalogProductID = SOLI.PracticeCatalogProductID      
				   
					INNER JOIN dbo.SupplierOrderLineItemStatus AS SOLIStatus WITH(NOLOCK)
						ON SOLI.SupplierOrderLineItemStatusID = SOLIStatus.SupplierOrderLineItemStatusID   
						
				   WHERE 
						PI.PracticeLocationID = @PracticeLocationID  
						AND PI.ProductInventoryID = @ProductInventoryID 
						AND PCP.IsActive = 1
						AND BVO.IsActive = 1
						AND SO.IsActive = 1
						AND SOLI.IsActive = 1  
						AND SOLIStatus.SupplierOrderLineItemStatusID IN (1, 3)      -- SOLineItemStatus is 'On Order or Partially Checked In'  (has outstanding orders.)
				        --  AND PI.IsActive = 1
					GROUP BY
						PI.ProductInventoryID
				) AS SUB
				
			SELECT 	@Count_SOLI_NotFullyCheckedIn = ISNULL(@Count_SOLI_NotFullyCheckedIn, 0)	-- was sometimes null during testing.
			SELECT @Count_SOLI_NotFullyCheckedIn  AS Count_SOLI_NotFullyCheckedIn
			IF (@Count_SOLI_NotFullyCheckedIn > 0)
				BEGIN
					PRINT 'Count on Order: ' + CAST(@Count_SOLI_NotFullyCheckedIn AS VARCHAR(10))
    				SET @DependencyMessage = @DependencyMessage + ', On Order' 
				END
			
			--  TEST PREREQUISITES FOR REMOVAL - END
			----------------------------
			SET @Count_Removable = @SumOfAllLevels + @Count_Cart + @Count_SOLI_NotFullyCheckedIn 
			
			IF ( @Count_Removable  > 0 )
				BEGIN
					--  Cannot be removed since the prerequisites are not met.
					SET @RemovalType = 0	--  item cannot be removed.
					PRINT 'Cannot be removed! Requisites are NOT meet.'  + CAST(@ProductInventoryID AS VARCHAR(10))
					IF (LEN(@DependencyMessage) >= 2)
						BEGIN
							SET @DependencyMessage = RIGHT(@DependencyMessage, LEN(@DependencyMessage) - 2)  -- Get rid of leading ", "
						END
					PRINT 'DependencyMessages: ' + CAST(@DependencyMessage AS VARCHAR(500))
						
				END
			ELSE	
				BEGIN
					PRINT 'May be removed!  Prerequisites are meet.'  
					--  Check SupplierOrderLineItem to see if product has ever been order for the location.
					--  Following is for testing.  Tested successfully.
					--  TODO:  cHANGE order of the tables.
					SELECT 
						@Count_SupplierOrderLineItemCount = SUB.Count_SupplierOrderLineItemCount
					FROM
					(
				
						SELECT COUNT(1) AS Count_SupplierOrderLineItemCount
						FROM [SupplierOrderLineItem] AS SOLI  WITH (NOLOCK)
						INNER JOIN [SupplierOrder] AS SO  WITH (NOLOCK)
							ON Soli.supplierOrderID = SO.supplierOrderID
						INNER JOIN [BregVisionOrder] AS BVO  WITH (NOLOCK)
							ON SO.BregVisionOrderID = BVO.BregVisionOrderID
						WHERE 
							BVO.[PracticeLocationID] = @PracticeLocationID
							AND SOLI.[PracticeCatalogProductID] = @PracticeCatalogProductID
							AND BVO.[IsActive] = 1
							AND SO.[IsActive] = 1
							AND SOLI.[IsActive] = 1	
					) AS SUB		
				
				
				--  Check if ever dispensed ProductInventory_DispenseDetail
				
					SELECT 
						@Count_ProductInventory_DispenseDetail = SUB.Count_ProductInventory_DispenseDetail
					FROM
						(			
							SELECT COUNT(1) AS Count_ProductInventory_DispenseDetail
							FROM dbo.ProductInventory_DispenseDetail AS PIDD  WITH (NOLOCK)
							WHERE PIDD.ProductInventoryID = @ProductInventoryID
						) AS SUB
				
						
						
					--  Check if product was ever checked in.
					--  Do not need sum here as the record count is being pulled.
					SELECT 
						@Count_ProductInventory_OrderCheckIn = SUB.Count_ProductInventory_OrderCheckIn
					FROM
						(			
							SELECT COUNT(1) AS Count_ProductInventory_OrderCheckIn
							FROM dbo.ProductInventory_OrderCheckIn AS PI_OCI  WITH (NOLOCK)
							WHERE PI_OCI.ProductInventoryID = @ProductInventoryID			
						) AS SUB												
					
					
					--  Check if product was ever manually checked in.	
					SELECT 
						@Count_ProductInventory_ManualCheckIn = SUB.Count_ProductInventory_ManualCheckIn
					FROM
						(			
							SELECT COUNT(1) AS Count_ProductInventory_ManualCheckIn
							FROM dbo.ProductInventory_ManualCheckIn AS PI_OCI  WITH (NOLOCK)
							WHERE PI_OCI.ProductInventoryID = @ProductInventoryID
						) AS SUB
				

					--  Check all for future compatibility although not yet implemented.
					SELECT 
						@Count_ProductInventory_ReturnedToSupplier = SUB.Count_ProductInventory_ReturnedToSupplier
					FROM
						(			
							SELECT COUNT(1) AS Count_ProductInventory_ReturnedToSupplier
							FROM dbo.ProductInventory_ReturnedToSupplier AS PI_OCI  WITH (NOLOCK)
							WHERE PI_OCI.ProductInventoryID = @ProductInventoryID
						) AS SUB
						
					SELECT 
						@Count_ProductInventory_Purge = SUB.Count_ProductInventory_Purge
					FROM
						(			
							SELECT COUNT(1) AS Count_ProductInventory_Purge
							FROM dbo.ProductInventory_Purge AS PI_OCI  WITH (NOLOCK)
							WHERE PI_OCI.ProductInventoryID = @ProductInventoryID
						) AS SUB
							
					SELECT 
						@Count_ProductInventory_QuantityOnHandPhysical = SUB.Count_ProductInventory_QuantityOnHandPhysical
					FROM
						(			
							SELECT COUNT(1) AS Count_ProductInventory_QuantityOnHandPhysical
							FROM dbo.ProductInventory_QuantityOnHandPhysical AS PI_OCI  WITH (NOLOCK)
							WHERE PI_OCI.ProductInventoryID = @ProductInventoryID
						) AS SUB
				
					SELECT 
						@Count_ProductInventory_ResetQuantityOnHandPerSystem = SUB.Count_ProductInventory_ResetQuantityOnHandPerSystem
					FROM
						(			
							SELECT COUNT(1) AS Count_ProductInventory_ResetQuantityOnHandPerSystem
							FROM dbo.ProductInventory_ResetQuantityOnHandPerSystem AS PI_OCI  WITH (NOLOCK)
							WHERE PI_OCI.ProductInventoryID = @ProductInventoryID
						) AS SUB
			

					SET @DependencyCount = 9999  -- To avoid an inadvertent delete.
					SET
						@DependencyCount =	
						  @Count_SupplierOrderLineItemCount 
						+ @Count_ProductInventory_OrderCheckIn 
						+ @Count_ProductInventory_DispenseDetail 
						+ @Count_ProductInventory_ManualCheckIn 
						+ @Count_ProductInventory_ReturnedToSupplier 
						+ @Count_ProductInventory_Purge 
						+ @Count_ProductInventory_QuantityOnHandPhysical 
						+ @Count_ProductInventory_ResetQuantityOnHandPerSystem 
				
					SELECT @DependencyCount AS DependencyCount
					PRINT 'DependencyCount: ' + CAST(@DependencyCount AS VARCHAR(10))
					
					
			
					IF (@DependencyCount = 0)  
						BEGIN
						
							SET @RemovalType = 1 	--  hard delete.
							PRINT 'Delete Hard' + CAST(@ProductInventoryID AS VARCHAR(10))
						
							--  Set the ModifiedUserID prior to deleting, so the user is recorded in the audit table.
							UPDATE dbo.ProductInventory  
							SET 
								  ModifiedUserID = @UserID
								, ModifiedDate = GETDATE()		        
							WHERE ProductInventoryID = @ProductInventoryID

						    DELETE dbo.ProductInventory		            	        
						    WHERE ProductInventoryID = @ProductInventoryID

						END
					ELSE 
						BEGIN
							IF (@DependencyCount > 0)  
							BEGIN
								SET @RemovalType = 2 	--  Execute a soft delete.
								PRINT 'Delete Soft' + CAST(@ProductInventoryID AS VARCHAR(10))

								UPDATE dbo.ProductInventory		            
								SET 
									  ModifiedUserID = @UserID
									, ModifiedDate = GETDATE()
									, IsActive = 0		        
								WHERE ProductInventoryID = @ProductInventoryID

							END
						END		
				
				END  --  End of Else for test of prerequisistes.
			
			
			
	END TRY

	BEGIN CATCH

	  EXECUTE [dbo].[uspLogError]
	  EXECUTE [dbo].[uspPrintError]
	  
	END CATCH	
	END --/else
END   