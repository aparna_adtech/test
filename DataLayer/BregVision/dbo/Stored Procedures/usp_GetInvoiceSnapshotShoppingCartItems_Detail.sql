﻿-- =============================================
-- Author:		Greg Ross
-- Create date: August 28th, 2007
-- Description:	Gets Practice Catalog Supplier Information for Invoice Snapshot (SP 2 of 2) 
-- jb  Add line total
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetInvoiceSnapshotShoppingCartItems_Detail]
	-- Add the parameters for the stored procedure here
	@PracticeLocationID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
	
		SUB.PracticeCatalogProductID,
		SUB.PracticeCatalogSupplierBrandID,
		SUB.BrandShortName,
		SUB.ShortName,
		SUB.Code,
		SUB.Packaging,
		SUB.LeftRightSide,
		SUB.Size,
		SUB.Color,
		SUB.Gender,
		SUB.ActualWholesaleCost,
		SUB.Quantity,
		SUB.ActualWholesaleCost * SUB.Quantity AS TotalCost,
		--  SUB.ActualWholesaleCost * SUB.Quantity AS TotalCost2,  --test this 
		SUB.IsThirdPartySupplier,
		SUB.Sequence

	FROM
		(
   			select	
   					PCP.PracticeCatalogProductID,
					PCSB.PracticeCatalogSupplierBrandID,
					PCSB.BrandShortName,
					MCP.ShortName,
					MCP.Code,
					MCP.Packaging,
					MCP.LeftRightSide,
					MCP.Size,
					MCP.Color,
					MCP.Gender,
					SCI.ActualWholesaleCost,
					SCI.Quantity,
					PCSB.IsThirdPartySupplier,
					ISNULL( PCSB.Sequence, 0 ) AS Sequence
			
			from ShoppingCart SC
			
			inner join ShoppingCartItem SCI
				on SC.ShoppingCartID = SCI.ShoppingCartID
			
			inner join PracticeCatalogProduct PCP 
				on SCI.PracticeCatalogProductID =PCP.PracticeCatalogProductID 
			
			inner join PracticeCatalogSupplierBrand PCSB
				on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
			
			inner join MasterCatalogProduct MCP
				on PCP.MasterCatalogPRoductID = MCP.MasterCatalogPRoductID
			
			where SC.[ShoppingCartStatusID] = 0
					AND SCI.[ShoppingCartItemStatusID] = 0
					AND SC.[IsActive] = 1
					AND SCI.[IsActive] = 1
					AND PCP.IsActive = 1
					AND PCSB.IsActive = 1
					AND PCSB.IsThirdPartySupplier = 0
					AND MCP.IsActive = 1
					and SC.PracticeLocationID = @PracticeLocationID


			UNION
			
			
			   	select	
					PCP.PracticeCatalogProductID,
					PCSB.PracticeCatalogSupplierBrandID,
					PCSB.BrandShortName,
					TPP.ShortName,
					TPP.Code,
					TPP.Packaging,
					TPP.LeftRightSide,
					TPP.Size,
					TPP.Color,
					TPP.Gender,
					SCI.ActualWholesaleCost,
					SCI.Quantity,
					PCSB.IsThirdPartySupplier,
					ISNULL( PCSB.Sequence, 0 ) AS Sequence
				
				from ShoppingCart SC
				
				inner join ShoppingCartItem SCI
					on SC.ShoppingCartID = SCI.ShoppingCartID
				
				inner join PracticeCatalogProduct PCP 
					on SCI.PracticeCatalogProductID =PCP.PracticeCatalogProductID 
				
				inner join PracticeCatalogSupplierBrand PCSB
					on PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
				
				inner join ThirdPartyProduct TPP
					on PCP.ThirdPartyPRoductID = TPP.ThirdPartyPRoductID
				
				where SC.[ShoppingCartStatusID] = 0
						AND SCI.[ShoppingCartItemStatusID] = 0
						AND SC.[IsActive] = 1
						AND SCI.[IsActive] = 1
						AND PCP.IsActive = 1
						AND PCSB.IsActive = 1
						AND PCSB.IsThirdPartySupplier = 1
						AND TPP.IsActive = 1
						and SC.PracticeLocationID = @PracticeLocationID
			) AS SUB

		ORDER BY
			SUB.IsThirdPartySupplier
			, SUB.Sequence
			, SUB.BrandShortName
			, SUB.ShortName
END

