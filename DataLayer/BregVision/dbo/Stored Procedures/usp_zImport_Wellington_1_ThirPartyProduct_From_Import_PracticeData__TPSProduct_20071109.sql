﻿
create proc [dbo].[usp_zImport_Wellington_1_ThirPartyProduct_From_Import_PracticeData__TPSProduct_20071109]
as
begin

INSERT INTO ThirdPartyProduct
	( PracticeCatalogSupplierBrandID
	, Code
	, Name
	, ShortName

	, Packaging
	, LeftRightSide
	, Size
	, Color
	, Gender
	, IsDiscontinued
	, WholesaleListCost
	, Description
	, CreatedUserID
	, CreatedDate
	, IsActive
	)

Select 
	  PracticeCatalogSupplierBrandID
	, ProductCode
	, ProductName
	, ProductShortName

	, Packaging
	, LeftRightSide
	, Size
	, ISNULL(Color, '') AS Color
	, ISNULL(Gender, '') as Gender
	, 0 AS IsDiscontinued
	, WholesaleListCost
	, ISNULL(Description, '') as Description
	, -13
	, GetDate()
	, -13
from Import_PracticeData.dbo.Wellington_TPS_Products_20071109

end