﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- exec usp_GetConsignmentRepsAll
CREATE PROCEDURE [dbo].[usp_GetConsignmentRepsAll]

AS
BEGIN


	SELECT 
		U.[UserId]
		,U.[UserName]
		,M.IsLockedOut
	FROM 
		aspnetdb.dbo.aspnet_Membership M
		Inner Join [aspnetdb].[dbo].[aspnet_Users] U on M.UserId = U.UserId
		left outer join [aspnetdb].[dbo].[aspnet_UsersInRoles] UR on U.UserId = UR.UserId 
		left outer join [aspnetdb].[dbo].[aspnet_Roles] R on UR.RoleId = R.RoleId 
	where R.RoleName = 'BregConsignment'
END
