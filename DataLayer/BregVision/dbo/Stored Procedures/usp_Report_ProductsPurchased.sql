﻿/*

	DECLARE @Output VARCHAR(2000)
	--SET @Output = ''
	--
	--SELECT @Output =
	--	CASE WHEN @Output = '' THEN CAST(PracticeCatalogSupplierBrandID AS VARCHAR(50))
	--		 ELSE @Output + ', ' + CAST(PracticeCatalogSupplierBrandID AS VARCHAR(50))
	--		 END
	--FROM PracticeCatalogSupplierBrand
	--WHERE PracticeID = 6
	--and isactive = 1
	--
	--SELECT @Output AS Output

	Modification:  JB 2007.12.15
			Restructure query to work as redefined by Bobby.


	EXEC [usp_Report_ProductsPurchased] 6, '13, 15, 16, 17, 18, 19, 20, 22, 23, 24', '34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 74, 75, 76, 81, 82, 83, 99, 101'
			, '1/1/2007', '12/31/2007'

*/


CREATE PROC [dbo].[usp_Report_ProductsPurchased]
		  @PracticeID	INT
		, @PracticeLocationID VARCHAR(2000)
		, @SupplierBrandID  VARCHAR(2000)		--  ID String.
		, @StartDate		DATETIME
		, @EndDate			DATETIME
		, @InternalPracticeLocationID VARCHAR(2000)
		, @InternalSupplierBrandID VARCHAR(2000)
AS
BEGIN

SELECT @EndDate = DATEADD(DAY, 1, @EndDate)

-- Parse the Practice Location
DECLARE @locations TABLE ([PracticeLocationID] INT);

-- If the Internal parameter is "ALL", we select all of the Practice Locations
-- for the passed PracticeID.
-- Otherwise, split as we were doing before.
IF(@InternalPracticeLocationID = 'ALL')
BEGIN
	DECLARE @tempLocations TABLE (
		PracticeLocationID INT
		,PracticeLocationName VARCHAR(50)
		,IsPrimaryLocation BIT
		);

	INSERT @tempLocations
	EXEC dbo.usp_PracticeLocation_Select_All_ID_Name_Given_PracticeID @PracticeID = @PracticeID;

	INSERT @locations ([PracticeLocationID])
	SELECT PracticeLocationID
	FROM @tempLocations;
END;
ELSE
BEGIN
	INSERT @locations ([PracticeLocationID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable(@PracticeLocationID, ',');
END;

-- Parse the Supplier Brand
DECLARE @supplierBrands TABLE([SupplierBrandID] INT);

-- If the Internal parameter is "ALL", we select all of the Supplier Brands
-- for the passed PracticeID.
-- Otherwise, split as we were doing before.
IF(@InternalSupplierBrandID = 'ALL')
BEGIN
	DECLARE @tempSupplierBrands TABLE (PracticeCatelogSupplierBrandID INT, SupplierBrand VARCHAR(1000), SupplierName VARCHAR(1000), BrandName VARCHAR(1000), IsThirdPartySupplier BIT, DerivedSequence INT);

	INSERT @tempSupplierBrands
	EXEC dbo.usp_Report_GetPracticeCatalogSupplierBrands_Formatted @PracticeID = @PracticeID;

	INSERT @supplierBrands ([SupplierBrandID])
	SELECT PracticeCatelogSupplierBrandID
	FROM @tempSupplierBrands;
END;
ELSE
BEGIN
	INSERT @supplierBrands ([SupplierBrandID])
	SELECT ID
	FROM dbo.udf_ParseArrayToTable(@SupplierBrandID, ',');
END;


SELECT
		 SUB.PracticeName
		, SUB.PracticeLocation

		, SUB.PracticeCatalogSupplierBrandID

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
			ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
			END					AS SupplierBrand

		, SUB.SupplierShortName	AS Supplier

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
		ELSE SUB.BrandShortName
		END						AS Brand


		, SUB.ProductName			AS Product
		, SUB.Side + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender
		, SUB.Code


		, CAST(SUB.ActualWholesaleCost AS DECIMAL(15,2) )	AS ActualWholesaleCost
		, SUM(SUB.QuantityOrdered)							AS QuantityOrdered
		, CAST( SUM(SUB.LineTotal)  AS DECIMAL(15,2) )		AS TotalCost

		, SUB.Sequence

--		, SUB.OrderDate
FROM
	(SELECT
		  P.PracticeName
		, PL.NAME													AS PracticeLocation

		, PCSB.IsThirdPartySupplier

		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierShortName
		, PCSB.BrandShortName

		, ISNULL(PCSB.Sequence, 999)								AS Sequence
		, PCP.PracticeCatalogProductID

		, MCP.[Name] AS ProductName
		, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) )		AS Side
		, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )				AS Size
		, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
		, ISNULL(MCP.Code, '')										AS Code

		, SOLI.ActualWholesaleCost
		, SOLI.QuantityOrdered
		, SOLI.LineTotal

		, SOLI.CreatedDate AS OrderDate

	FROM dbo.SupplierOrderLineItem AS SOLI WITH (NOLOCK)

	INNER JOIN dbo.SupplierOrder AS SO	WITH (NOLOCK)
		ON SOLI.SupplierOrderID = SO.SupplierOrderID

	INNER JOIN dbo.BregVisionOrder AS BVO	WITH (NOLOCK)
		ON BVO.BregVisionOrderID = SO.BregVisionOrderID

	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON BVO.PracticeLocationID = PL.PracticeLocationID

	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID

	INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
		ON SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID

	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

	INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
		ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

	WHERE
		P.PracticeID = @PracticeID
		--AND P.IsActive = 1
		AND SOLI.IsActive = 1
		AND SO.IsActive = 1
		AND BVO.IsActive = 1
		--AND PL.IsActive = 1
		--AND PCP.IsActive = 1
		--AND PCSB.IsActive = 1
		--AND MCP.IsActive = 1

		AND
		PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			 FROM @locations)

		AND PCSB.PracticeCatalogSupplierBrandID IN
			(SELECT SupplierBrandID
			 FROM @supplierBrands )

		AND SOLI.CreatedDate >= @StartDate
		AND SOLI.CreatedDate < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM

	UNION

		SELECT
		  P.PracticeName
		, PL.NAME													AS PracticeLocation

		, PCSB.IsThirdPartySupplier

		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierShortName
		, PCSB.BrandShortName

		, ISNULL(PCSB.Sequence, 999) + 1000								AS Sequence
		, PCP.PracticeCatalogProductID

		, TPP.[Name] AS ProductName
		, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) )		AS Side
		, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )				AS Size
		, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
		, ISNULL(TPP.Code, '')										AS Code

		, SOLI.ActualWholesaleCost
		, SOLI.QuantityOrdered
		, SOLI.LineTotal

		, SOLI.CreatedDate AS OrderDate


	FROM dbo.SupplierOrderLineItem AS SOLI	WITH (NOLOCK)

	INNER JOIN dbo.SupplierOrder AS SO	WITH (NOLOCK)
		ON SOLI.SupplierOrderID = SO.SupplierOrderID

	INNER JOIN dbo.BregVisionOrder AS BVO	WITH (NOLOCK)
		ON BVO.BregVisionOrderID = SO.BregVisionOrderID

	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON BVO.PracticeLocationID = PL.PracticeLocationID

	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID

	INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
		ON SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID

	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

	INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
		ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

	WHERE
		P.PracticeID = @PracticeID
		--AND P.IsActive = 1
		AND SOLI.IsActive = 1
		AND SO.IsActive = 1
		AND BVO.IsActive = 1
		--AND PL.IsActive = 1
		--AND PCP.IsActive = 1
		--AND PCSB.IsActive = 1
		--AND TPP.IsActive = 1

		AND PL.PracticeLocationID IN
			(SELECT PracticeLocationID
			 FROM @locations)

		AND PCSB.PracticeCatalogSupplierBrandID IN
			(SELECT SupplierBrandID
			 FROM @supplierBrands )

		AND SOLI.CreatedDate >= @StartDate
		AND SOLI.CreatedDate < @EndDate --  Less than since @EndDate has one day added to it since the time is 12AM

	)
		AS SUB

	GROUP BY
		SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.PracticeCatalogSupplierBrandID
		, SUB.IsThirdPartySupplier
		, SUB.Sequence
		, SUB.SupplierShortName
		, SUB.BrandShortName

		, SUB.ProductName
		, SUB.Side
		, SUB.Size
		, SUB.Gender
		, SUB.Code

		, SUB.ActualWholesaleCost

	ORDER BY
		SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.IsThirdPartySupplier
		, SUB.Sequence
		, SUB.SupplierShortName
		, SUB.BrandShortName
		, SUB.ProductName
		, SUB.Code

END
