﻿/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeBillingAddress_Select_AddressID
   
   Description:  Input: PracticeID
				 Get AddressID from table dbo.Address, via
				 the PracticeBillingAddress table
   				     and puts the value into aparameter
   
   AUTHOR:       John Bongiorni 7/16/2007 5:07 PM
   
   Modifications:  
   ------------------------------------------------------------ */ 

CREATE PROCEDURE [dbo].[usp_PracticeBillingAddress_Select_AddressID]
(
	  @PracticeID					 INT
    , @AddressID                     INT			OUTPUT
	, @IsBillingCentralized			 BIT			OUTPUT 
)
AS
BEGIN
	DECLARE @Err INT
	DECLARE @Count INT
	--  SELECT @IsBillingCentralized = 0

	SELECT @IsBillingCentralized = IsBillingCentralized
	FROM dbo.Practice
	WHERE PracticeID = @PracticeID
	
	
	SELECT
			@Count = Count(1)
	FROM	
			dbo.PracticeBillingAddress AS PBA
	WHERE 
		PBA.PracticeID = @PracticeID
		AND PBA.IsActive = 1
		AND PBA.AddressID IS NOT NULL

	IF (@Count > 0)
	BEGIN
		--  SELECT @IsBillingCentralized = 1

		SELECT
				@AddressID = A.AddressID
		FROM	
				dbo.PracticeBillingAddress AS PBA
		INNER JOIN	
				dbo.Address AS A
					ON PBA.AddressID = A.AddressID
		WHERE 
			PBA.PracticeID = @PracticeID
			AND PBA.IsActive = 1
			AND A.IsActive = 1
			AND PBA.AddressID IS NOT NULL
			AND A.AddressID IS NOT NULL
		
		SET @Err = @@ERROR
	 END

		RETURN @Err
END


