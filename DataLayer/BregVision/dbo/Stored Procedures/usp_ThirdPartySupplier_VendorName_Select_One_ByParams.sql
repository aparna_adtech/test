﻿/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_ThirdPartySupplier_Select_One_SupplierNames_ByParams
   
   Description:  Selects the supplier name and supplierShortName
					 from table dbo.ThirdPartySupplier
   				   and puts values into parameters
   
   AUTHOR:       John Bongiorni 10/4/2007 7:09:48 PM
   
   Modifications:  
   ------------------------------------------------------------ */  ---------------------SELECT PARAMS-----------

CREATE PROCEDURE dbo.usp_ThirdPartySupplier_VendorName_Select_One_ByParams
(
      @ThirdPartySupplierID          INT
    , @SupplierName                  VARCHAR(50)    OUTPUT
    , @SupplierShortName             VARCHAR(50)    OUTPUT
)
AS
BEGIN
	DECLARE @Err INT

	SELECT
          @ThirdPartySupplierID = ThirdPartySupplierID      
        , @SupplierName = ISNULL(SupplierName, '')
        , @SupplierShortName = ISNULL(SupplierShortName, '')        

	FROM dbo.ThirdPartySupplier
	WHERE 
		ThirdPartySupplierID = @ThirdPartySupplierID
		AND IsVendor = 1
		AND IsActive = 1

	SET @Err = @@ERROR

	RETURN @Err
End