﻿/*
declare @startdate datetime
declare @enddate datetime
set @startdate = dateadd(m, -1, GetDate())
set @enddate = GetDate()
exec usp_Report_Transfer_ByTransferDates @startdate, @enddate
*/
CREATE PROCEDURE [dbo].[usp_Report_Transfer_ByTransferDates]
      @PracticeID int
    , @PracticeLocationID	VARCHAR(2000)
	, @StartDate Datetime
	, @EndDate DateTime
	, @InternalPracticeLocationID VARCHAR(2000)
AS
BEGIN

	-- Parse the ID strings
	-- Parse the Practice Location
	DECLARE @locations TABLE ([PracticeLocationID] INT);

	-- If the Internal parameter is "ALL", we select all of the Practice Locations
	-- for the passed PracticeID.
	-- Otherwise, split as we were doing before.
	IF(@InternalPracticeLocationID = 'ALL')
	BEGIN
		DECLARE @tempLocations TABLE (
			PracticeLocationID INT
			,PracticeLocationName VARCHAR(50)
			,IsPrimaryLocation BIT
			);

		INSERT @tempLocations
		EXEC dbo.usp_PracticeLocation_Select_All_ID_Name_Given_PracticeID @PracticeID = @PracticeID;

		INSERT @locations ([PracticeLocationID])
		SELECT PracticeLocationID
		FROM @tempLocations;
	END;
	ELSE
	BEGIN
		INSERT @locations ([PracticeLocationID])
		SELECT ID
		FROM dbo.udf_ParseArrayToTable(@PracticeLocationID, ',');
	END;


DECLARE @PracticeLocationIDString VARCHAR(2000)

SET @PracticeLocationIDString = @PracticeLocationID
	/****** Script for SelectTopNRows command from SSMS  ******/
	SELECT 
		   P.PracticeName
		  ,PIT.[TransferDate]
		  ,PL_From.Name as PracticeLocationFromName
		  ,PL_To.Name as PracticeLocationToName
		  ,Coalesce(MCP_From.Name, TPP_From.Name, 'Name not found')  as ProductName
		  ,Coalesce(MCP_From.Size, TPP_From.Size, 'Size not found') as Size
		  ,Coalesce(MCP_From.LeftRightSide, TPP_From.LeftRightSide, 'Side not found') as Side
		  ,Coalesce(MCP_From.Gender, TPP_From.Gender, 'Gender not found') as Gender
		  ,Coalesce(MCP_From.Code, TPP_From.Code, 'Code not found') as Code
		  ,PIT.[Quantity]      
		  ,PIT.[ActualWholesaleCost]
		  ,PIT.[Comments]
		  ,PIT.[CreatedUserID]
		  ,PIT.[CreatedDate]

	  --Product Joins    
	  FROM [dbo].[ProductInventory_Transfer] PIT 
	  INNER JOIN [ProductInventory] AS PI_From  WITH(NOLOCK)
								ON PIT.[ProductInventoryID_TransferFrom] = PI_From.[ProductInventoryID]
	  LEFT OUTER JOIN [PracticeCatalogProduct] AS PCP_From WITH(NOLOCK) 
							ON PI_From.PracticeCatalogProductID = PCP_From.PracticeCatalogProductID	
	  LEFT OUTER JOIN [MasterCatalogProduct] AS MCP_From  WITH(NOLOCK)
								ON PCP_From.[MasterCatalogProductID] = MCP_From.[MasterCatalogProductID] AND PCP_From.IsThirdPartyProduct=0
	  LEFT OUTER JOIN [ThirdPartyProduct] AS TPP_From  WITH(NOLOCK)
								ON PCP_From.[ThirdPartyProductID] = TPP_From.[ThirdPartyProductID] AND PCP_From.IsThirdPartyProduct=1							
	--END Product Joins

	--From Practice Location
	  LEFT OUTER JOIN PracticeLocation PL_From 
		ON PIT.PracticeLocationID_TransferFrom = PL_From.PracticeLocationID
	--END From Practice Location

	--To Practice Location
	  LEFT OUTER JOIN PracticeLocation PL_To
		ON PIT.PracticeLocationID_TransferTo = PL_To.PracticeLocationID
	--END To Practice Location
	
	--Practice
	  LEFT OUTER JOIN Practice p 
		ON p.PracticeID = PL_To.PracticeID
	--END Practice	

	Where
	PIT.IsActive = 1
	AND PIT.practiceID = @PracticeID
	AND PIT.TransferDate >= @StartDate
	-- ditch the time component of PIT.TransferDate when comparing against @EndDate -hchan 11/24/2010
	AND cast(floor(cast(PIT.TransferDate as float)) as datetime) <= @EndDate

	AND 
	PIT.PracticeLocationID_TransferFrom IN 
	(SELECT PracticeLocationID
	FROM @locations)
	
	Order by 
	PIT.TransferDate,
	PL_From.Name,
	PL_To.Name,
	ProductName	

END
