﻿
/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocation_Select_All_ListNames_By_PracticeID
   
   Description:  Selects all PracticeLocation Names and IDs for a given Practice.
				Select all names and ids from the table dbo.PracticeLocation
				given the PracticeID.
   
   AUTHOR:       John Bongiorni 9/21/2007 11:05:42 AM
   
   Modifications:  
   ------------------------------------------------------------ */  

CREATE PROCEDURE dbo.usp_PracticeLocation_Select_All_ListNames_By_PracticeID
			( @PracticeID INT )
AS 
    BEGIN
        DECLARE @Err INT
        SELECT
        
            PL.PracticeLocationID
          , PL.Name AS PracticeLocationName
          , PL.IsPrimaryLocation
        
        FROM
            dbo.PracticeLocation AS PL WITH(NOLOCK)
        
        WHERE
            PL.IsActive = 1
            AND PL.PracticeID = @PracticeID
        
        ORDER BY
            PL.IsPrimaryLocation DESC
	      , PL.Name
        
        SET @Err = @@ERROR

        RETURN @Err
    END
