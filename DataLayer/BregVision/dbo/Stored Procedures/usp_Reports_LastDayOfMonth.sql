﻿/*

EXEC dbo.usp_Reports_LastDayOfMonth

*/


CREATE proc [dbo].[usp_Reports_LastDayOfMonth]
AS 
BEGIN

select CAST( CONVERT(VARCHAR(10), [dbo].[lastDayOfMonth](getdate()), 101) as DateTime) As LastDayOfMonth

END
