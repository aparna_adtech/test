﻿

--  Step 2 for NWIA
--  NWIA's PracticeID is 14

CREATE PROC [dbo].[usp_zImport_NWIA_2_PracticeCatalogProduct_Insert_From_ThirdPartyProduct_NWIA_3rdProducts_20071204]  
	--  @PracticeID INT

as
begin

-- select * from practice  NWIA is practiceID = 14
DECLARE @PracticeID INT
SET @PracticeID = 16     ---   <--  SET PRACTICE ID HERE.

--  SELECT * FROM practicecatalogproduct WHERE thirdpartyproductID IN (1484, 1485)

INSERT INTO PracticeCatalogProduct
	( PracticeID
	, PracticeCatalogSupplierBrandID
	, ThirdPartyProductID
	, IsThirdPartyProduct
	, WholesaleCost
	, CreatedUserID
	, ModifiedUserID
	, ModifiedDate
	, IsActive
	)

--  Note: UNCOMMENT THE FOLLOWING TWO LINES AND RUN SELECT TO VERIFY BEFORE INSERTING!!!
--DECLARE @PracticeID INT
--SET @PracticeID = 16
--  --select * from practice where isactive = 1 
--select top 82 * from thirdpartyproduct order by thirdpartyproductID desc
SELECT
		PCSB.PracticeID
	, PCSB.PracticeCatalogSupplierBrandID
	, TPP.ThirdPartyProductID
	, 1 AS IsThirdPartyProduct
	, TPP.WholesaleListCost AS WholesaleCost
	, -13  as CreatedUserID    --TPS.CreatedUserID
	, -13  as ModifiedUserID 
	, GetDate() AS ModifiedDate
	, TPP.IsActive
FROM ThirdPartyProduct AS TPP
	INNER JOIN PracticeCatalogSupplierBrand AS PCSB
		ON TPP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
	INNER JOIN Practice AS P
		ON P.PracticeID = PCSB.PracticeID
	WHERE P.PracticeID = @PracticeID			--  Enter ProductID of `11 for hanover 
	AND TPP.IsActive = 1	--  Avoid getting bogus third party products that have been backed out
	AND TPP.ThirdPartyProductID NOT IN
		(SELECT ThirdPartyProductID 
		 FROM PracticeCatalogProduct 
		 WHERE IsActive = 1 
			AND IsThirdPartyProduct = 1) 

--  select * from  PracticeCatalogProduct where practiceid = 16
--  select * from practice

end