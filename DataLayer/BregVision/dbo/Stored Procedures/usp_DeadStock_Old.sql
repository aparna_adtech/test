﻿

-- =============================================
-- Author:		<Author:Greg Ross>
-- Create date: <Create Date: 07/23/07>
-- Description:	<Description: Query to get deadstock (Products not dispensed in over 60 days) items for dashboard>
-- Modification:  John Bongiorni  20071008  A productCatalogProduct must be dispensed at least once before showing
--					in dead stock
--				  20080211 John Bongiorni Add Table Hints, specifically WITH (NOLOCK)
-- =============================================
CREATE PROCEDURE [dbo].[usp_DeadStock_Old]
	-- Add the parameters for the stored procedure here
	@PracticeID INT,
	@PracticeLocationID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT  MCS.[SupplierName],
        MCP.name,
        MCP.[Code],
        PI.[QuantityOnHandPerSystem],
        PI.[ParLevel],
        PI.[ReorderLevel],
        PI.[CriticalLevel],
		MCP.masterCatalogProductID
	--,D.[DateDispensed]
	--,[DateDispensed]
FROM    [ProductInventory] PI						WITH (NOLOCK)
        INNER JOIN [PracticeCatalogProduct] PCP		WITH (NOLOCK)
			ON PI.[PracticeCatalogProductID] = PCP.[PracticeCatalogProductID]
        INNER JOIN [MasterCatalogProduct] MCP		WITH (NOLOCK)
			ON PCP.[MasterCatalogProductID] = MCP.[MasterCatalogProductID]
        
		INNER JOIN [MasterCatalogSubCategory] MCSC	WITH (NOLOCK)
			ON MCP.[MasterCatalogSubCategoryID] = MCSC.[MasterCatalogSubCategoryID]
        INNER JOIN [MasterCatalogCategory] MCC		WITH (NOLOCK)
			ON MCSC.[MasterCatalogCategoryID] = MCC.[MasterCatalogCategoryID]
        INNER JOIN [MasterCatalogSupplier] MCS		WITH (NOLOCK)
			ON MCC.[MasterCatalogSupplierID] = MCS.[MasterCatalogSupplierID] 
        --INNER JOIN [Practice] P						WITH (NOLOCK)
		--	ON PCP.[PracticeID] = P.[PracticeID]
		inner join PracticeLocation PL 
			on PI.PracticeLocationID = PL.PracticeLocationID


		--INNER JOIN [PracticeLocation] PL			WITH (NOLOCK)
		--	ON P.[PracticeID] = PL.[PracticeID]

WHERE   PCP.[PracticeCatalogProductID] NOT IN (
        SELECT  [PracticeCatalogProductID]
        FROM    [Dispense]			WITH (NOLOCK)
                INNER JOIN [DispenseDetail] WITH (NOLOCK)
					ON [Dispense].[DispenseID] = [DispenseDetail].[DispenseID]
        WHERE   [PracticeLocationID] = @PracticeLocationID
                AND [DateDispensed] > dateadd(mm, -2, getdate()) )
                
        -- jb 20071008  The product must be dispensed at least one to show up in dead stock.
        --	AND PCP.[PracticeCatalogProductID] IN   -- The product must be Dispensed at least once.
		/*(
			SELECT  [PracticeCatalogProductID]
			FROM    [Dispense]						WITH (NOLOCK)
					INNER JOIN [DispenseDetail]		WITH (NOLOCK)
						ON [Dispense].[DispenseID] = [DispenseDetail].[DispenseID]
			WHERE   [PracticeLocationID] = @PracticeLocationID
		)  */      
        -- jb 20071008 end of modification        
                
        --AND p.[PracticeID] = @PracticeID
        AND pl.[PracticeLocationID] =	@PracticeLocationID
		--AND D.[PracticeLocationID] = @PracticeLocationID
        --Next line added to make sure items with zero quantity aren't included in dead stock - GR - 5/23/08
		AND PI.QuantityOnHandPerSystem <> 0
		AND PI.[IsActive] = 1
        AND PCP.[IsActive] = 1
        AND MCP.IsActive = 1
        AND MCSC.[IsActive] = 1
        AND MCC.[IsActive] = 1
        AND MCS.[IsActive] = 1
        --AND P.[IsActive] = 1
        AND pl.[IsActive] = 1
ORDER BY MCS.[SupplierName],
        MCP.NAME

--Old Code by JB
--SELECT  MCS.[SupplierName],
--        MCP.name,
--        MCP.[Code],
--        PI.[QuantityOnHandPerSystem],
--        PI.[ParLevel],
--        PI.[ReorderLevel],
--        PI.[CriticalLevel]
--	--,D.[DateDispensed]
--	--,[DateDispensed]
--FROM    [ProductInventory] PI						WITH (NOLOCK)
--        INNER JOIN [PracticeCatalogProduct] PCP		WITH (NOLOCK)
--			ON PI.[PracticeCatalogProductID] = PCP.[PracticeCatalogProductID]
--        INNER JOIN [MasterCatalogProduct] MCP		WITH (NOLOCK)
--			ON PCP.[MasterCatalogProductID] = MCP.[MasterCatalogProductID]
--        INNER JOIN [MasterCatalogSubCategory] MCSC	WITH (NOLOCK)
--			ON MCP.[MasterCatalogSubCategoryID] = MCSC.[MasterCatalogSubCategoryID]
--        INNER JOIN [MasterCatalogCategory] MCC		WITH (NOLOCK)
--			ON MCSC.[MasterCatalogCategoryID] = MCC.[MasterCatalogCategoryID]
--        INNER JOIN [MasterCatalogSupplier] MCS		WITH (NOLOCK)
--			ON MCC.[MasterCatalogSupplierID] = MCS.[MasterCatalogSupplierID]
--        INNER JOIN [Practice] P						WITH (NOLOCK)
--			ON PCP.[PracticeID] = P.[PracticeID]
--        INNER JOIN [PracticeLocation] PL			WITH (NOLOCK)
--			ON P.[PracticeID] = PL.[PracticeID]
----INNER JOIN [Dispense] D
----ON PL.[PracticeLocationID] = D.[PracticeLocationID]
----INNER JOIN [DispenseDetail] DD
----ON D.[DispenseID] = DD.[DepenseID]
--WHERE   PCP.[PracticeCatalogProductID] NOT IN (
--        SELECT  [PracticeCatalogProductID]
--        FROM    [Dispense]			WITH (NOLOCK)
--                INNER JOIN [DispenseDetail] WITH (NOLOCK)
--					ON [Dispense].[DispenseID] = [DispenseDetail].[DispenseID]
--        WHERE   [PracticeLocationID] = @PracticeLocationID
--                AND [DateDispensed] > dateadd(mm, -2, getdate()) )
--                
--        -- jb 20071008  The product must be dispensed at least one to show up in dead stock.
--        	AND PCP.[PracticeCatalogProductID] IN   -- The product must be Dispensed at least once.
--		(
--			SELECT  [PracticeCatalogProductID]
--			FROM    [Dispense]						WITH (NOLOCK)
--					INNER JOIN [DispenseDetail]		WITH (NOLOCK)
--						ON [Dispense].[DispenseID] = [DispenseDetail].[DispenseID]
--			WHERE   [PracticeLocationID] = @PracticeLocationID
--		)        
--        -- jb 20071008 end of modification        
--                
--        AND p.[PracticeID] = @PracticeID
--        AND pl.[PracticeLocationID] =	@PracticeLocationID
--		--AND D.[PracticeLocationID] = @PracticeLocationID
--        AND PI.[IsActive] = 1
--        AND PCP.[IsActive] = 1
--        AND MCP.IsActive = 1
--        AND MCSC.[IsActive] = 1
--        AND MCC.[IsActive] = 1
--        AND MCS.[IsActive] = 1
--        AND P.[IsActive] = 1
--        AND pl.[IsActive] = 1
--ORDER BY [SupplierName],
--        NAME
END



