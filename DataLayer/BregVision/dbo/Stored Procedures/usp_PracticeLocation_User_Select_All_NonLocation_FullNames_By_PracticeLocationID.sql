﻿
/*
	Get the Full Names of all Practice Users that are associated 
	with a given Practice, but NOT the PracticeLocation.  
	
	John Bongiorni  09/24/2007  
*/

CREATE PROCEDURE dbo.usp_PracticeLocation_User_Select_All_NonLocation_FullNames_By_PracticeLocationID  
			@PracticeLocationID INT
AS
BEGIN 

		DECLARE @PracticeID INT
        
		SELECT @PracticeID = PracticeID 
		FROM dbo.PracticeLocation WITH (NOLOCK)
		WHERE PracticeLocationID = @PracticeLocationID
				
		 SELECT
			U.UserID
		   , C.ContactID
		
		-- , C.Salutation
		-- , C.FirstName
		-- , C.MiddleName
		-- , C.LastName
		-- , C.Suffix

			, RTRIM( C.FirstName + ' ' + C.LastName + ' ' + C.Suffix) AS UserFullName
		 
		 FROM dbo.Contact AS C
		 INNER JOIN dbo.[User] AS U
				ON C.ContactID = U.ContactID
		 INNER JOIN dbo.Practice AS P  WITH (NOLOCK)
				ON U.PracticeID = P.PracticeID
		 WHERE
			P.PracticeID = @PracticeID
			AND U.IsActive = 1
			AND C.IsActive = 1
            AND U.UserID NOT IN 
				(	
					SELECT UserID 
					FROM PracticeLocation_User WITH (NOLOCK)
					WHERE PracticeLocationID = @PracticeLocationID 
						AND IsActive = 1 
				)
	
END

  