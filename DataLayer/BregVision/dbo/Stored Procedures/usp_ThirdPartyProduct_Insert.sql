﻿

/* 
  Create for duplicates before inserting
*/

CREATE proc [dbo].[usp_ThirdPartyProduct_Insert]
	   @NewThirdPartyProductID INT = -1 OUTPUT
	,  @PracticeCatalogSupplierBrandID INT
	, @ProductCode			VARCHAR(25)
	, @ProductName			VARCHAR(50)
	, @Packaging			VARCHAR(10) = ''
	, @LeftRightSide		VARCHAR(10) = ''
	, @Size					VARCHAR(10) = ''
	, @Color				VARCHAR(10) = ''
	, @Gender				VARCHAR(10) = ''
	, @Mod1					VARCHAR(2) = ''
	, @Mod2					VARCHAR(2) = ''
	, @Mod3					VARCHAR(2) = ''
	, @WholesaleCost		SMALLMONEY  = 0
	, @HCPCSString			VARCHAR(100) = ''
	, @IsHCPCSInserted		BIT = 0
	, @UserID				INT			= 92210
	, @UPCCODE				varchar(75) = ''
	, @UserMessage			VARCHAR(50)	= '' OUTPUT
as
begin


DECLARE @PracticeID				INT
--DECLARE @NewThirdPartyProductID INT
DECLARE @DuplicateRecordCount   INT
DECLARE @NewPracticeCatalogProductID INT

Select 
	@DuplicateRecordCount = COUNT(1)
FROM ThirdPartyProduct AS TPP
INNER JOIN PracticeCatalogProduct AS PCP
	ON TPP.ThirdPartyProductID = PCP.ThirdPartyProductID
WHERE 
	TPP.IsActive = 1
	AND PCP.IsActive = 1
	AND PCP.PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID
	AND Code = @ProductCode
	AND ShortName = @ProductName 
	AND ISNULL(Packaging, '') = ISNULL(@Packaging, '')
	AND ISNULL(LeftRightSide, '') = ISNULL(@LeftRightSide, '')
	AND ISNULL(Size, '') = ISNULL(@Size, '')
	AND ISNULL(Color, '') = ISNULL(@Color, '')
	AND ISNULL(Gender, '') = ISNULL(@Gender, '')

IF (@DuplicateRecordCount = 0)
BEGIN

BEGIN TRAN

INSERT INTO ThirdPartyProduct
	( 
	  PracticeCatalogSupplierBrandID
	, Code
	, Name
	, ShortName

	, Packaging
	, LeftRightSide
	, Size
	, Color
	, Gender
	, Mod1
	, Mod2
	, Mod3
	, IsDiscontinued
	, WholesaleListCost
	, Description
	--, UPCCode

	, CreatedUserID
	, ModifiedUserID
	, ModifiedDate
	, IsActive
	)

Select 
	  @PracticeCatalogSupplierBrandID
	, @ProductCode
	, @ProductName
	, @ProductName
	
	, @Packaging
	, @LeftRightSide
	, @Size
	, ISNULL(@Color, '')	AS Color
	, ISNULL(@Gender, '')	AS Gender
	, ISNULL(@Mod1, '')		AS Mod1
	, ISNULL(@Mod2, '')		AS Mod2
	, ISNULL(@Mod3, '')		AS Mod3
	, 0						AS IsDiscontinued
	, @WholesaleCost		AS WholesaleListCost
	, ''					AS Description
	--, @UPCCODE

	, @UserID				as CreatedUserID
	, @UserID				as ModifiedUserID
	, GetDate()				as ModifiedDate
	, 1						as IsActive


SELECT @NewThirdPartyProductID = SCOPE_IDENTITY()

SELECT @PracticeID = PracticeID 
FROM PracticeCatalogSupplierBrand WITH (NOLOCK)
WHERE PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID


INSERT INTO PracticeCatalogProduct
	( PracticeID
	, PracticeCatalogSupplierBrandID
	, ThirdPartyProductID
	, IsThirdPartyProduct
	, WholesaleCost
	, CreatedUserID
	, ModifiedUserID
	, ModifiedDate
	, IsActive
	)
SELECT 
	  @PracticeID
	, @PracticeCatalogSupplierBrandID		
	, @NewThirdPartyProductID
	, 1
	, @WholesaleCost
	, @UserID
	, @UserID
	, GETDATE()
	, 1


SELECT @NewPracticeCatalogProductID = SCOPE_IDENTITY()

-- if the HCPCSString is not length zero parse the HCPCSString to a tablevariable.
-- and the practicecatalogproductID
-- insert into the product productHCPCs for each.


-- INSERT HCPCs for product.
-- INSERT HCPCs for product.
SELECT @HCPCSstring = RTRIM(LTRIM(REPLACE(@HCPCSstring, ', ', ',')))

IF ((LEN(@HCPCSstring) > 0) AND (@IsHCPCSInserted = 1))
BEGIN

	INSERT INTO productHCPCs
		(PracticeCatalogProductID, HCPCS, CreatedUserID, CreatedDate, ModifiedUserID, ModifiedDate, IsActive)
	SELECT 
		  @NewPracticeCatalogProductID AS PracticeCatalogProductID
		, LTRIM(RTRIM(TextValue)) AS HCPCS
		, @UserID AS CreatedUserID
		, GETDATE() AS CreatedDate
		, @UserID AS ModifiedUserID  
		, GETDATE() AS ModifiedDate
		, 1 AS Active
	FROM dbo.[udf_ParseStringArrayToTable]( @HCPCSstring, ',') 
	WHERE LEN(LTRIM(RTRIM(TextValue))) > 0 
	ORDER BY TextValue
END

--Insert UPCCodes for Product
SELECT @UPCCODE = RTRIM(LTRIM(REPLACE(@UPCCODE, ', ', ',')))

IF ((LEN(@UPCCODE) > 0) /*AND (@IsHCPCSInserted = 1)*/)
BEGIN

	INSERT INTO UPCCode
		(ThirdPartyProductID, Code, CreatedUserID, CreatedDate, ModifiedUserID, ModifiedDate, IsActive)
	SELECT 
		  @NewThirdPartyProductID AS ThirdPartyProductID
		, LTRIM(RTRIM(TextValue)) AS Code
		, @UserID AS CreatedUserID
		, GETDATE() AS CreatedDate
		, @UserID AS ModifiedUserID  
		, GETDATE() AS ModifiedDate
		, 1 AS Active
	FROM dbo.[udf_ParseStringArrayToTable]( @UPCCODE, ',') 
	WHERE LEN(LTRIM(RTRIM(TextValue))) > 0 
	ORDER BY TextValue
END



COMMIT TRAN

END
ELSE
BEGIN
	--SELECT 1 AS one
	SET @UserMessage = 'Duplicate'
	SET @NewThirdPartyProductID = -1;
	--  Duplicate.
END
END
