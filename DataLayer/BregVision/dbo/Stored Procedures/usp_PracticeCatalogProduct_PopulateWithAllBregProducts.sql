﻿



-- =============================================
-- Author:		Mike Sneen
-- Create date: 03/19/2010
-- Description:	Populates the Vision Lite Practice with all Breg Products
-- Modifications: 
--
-- EXEC usp_PracticeCatalogProduct_PopulateWithAllBregProducts 139
-- =============================================
CREATE PROCEDURE [dbo].[usp_PracticeCatalogProduct_PopulateWithAllBregProducts]

@PracticeID int,
@UserID int = 1

AS


BEGIN

	Declare @PracticeCatalogProductID int
	
	Set NoCount on;
	
	DECLARE @BregProductsFromProc TABLE
	(
		SupplierID int,
		MasterCatalogProductID int,
		MasterCatalogSubCategoryID int,
		Code varchar(50),
		ShortName varchar(50),
		Packaging Varchar(50),
		LeftRightSide varchar(10),
		Size varchar(50),
		Color varchar(50),
		Gender varchar(50),
		WholesaleListCost smallmoney
	)

	Declare @BregProducts TABLE
	(
		MasterCatalogProductID int,
		fUsed bit default 0
	)

	--to get breg products
	INSERT INTO @BregProductsFromProc
	EXEC usp_GetMasterCatalogProductInfo 'Brand', 'Breg'

	Insert into @BregProducts(MasterCatalogProductID, fUsed) 
		(Select BPFP.MasterCatalogProductID, 0 
		from @BregProductsFromProc BPFP
		where BPFP.Code IN
		(
			'97063','97062','97064','97061','97065','08402','08404','08403','07334','07335','60044','21704','21703','21713','11233',
			'11234','11214','11213','10155','10154','10240','10220','96510','96500','96510','96520','07043','07042','11180','96534',
			'96533','96532','11174','11172','11173','10601','10604','10602','10606','10607','11183','21732','21735','21734','21733',
			'08494','08493','11093','11094','08504','08503','08502','96543','96544','96542','07250','07714','11286','28452','28453',
			'28454','28443','28444','28442','28423','28424','28422','28403','28404','28402','10271','10281','10272','10283','10273',
			'10282','10271','10281','10372','10373','10363','10362','10393','10383','10392','10382','10394','10384','10392','10393',
			'10342','10352','10442','10432','10443','10433','10442','10432','10433','10443'
		)
		)
		
	Declare @MasterCatalogProductID int

	Select TOP 1 @MasterCatalogProductID = MasterCatalogProductID from @BregProducts where fUsed = 0

	WHILE @@ROWCOUNT <> 0 AND @MasterCatalogProductID IS NOT NULL
	BEGIN

		EXEC usp_PracticeCatalogProduct_Insert_By_MasterCatalogProductID 
													@PracticeID, @MasterCatalogProductID, @Userid, @PracticeCatalogProductID
													
		Update @BregProducts SET fUsed = 1 where MasterCatalogProductID = @MasterCatalogProductID
			
		Select TOP 1 @MasterCatalogProductID = MasterCatalogProductID from @BregProducts where fUsed = 0											
	END



END