﻿


/* ------------------------------------------------------------
   PROCEDURE:    [dbo].[usp_PracticeCatalogSupplierBrand_Delete_Brand]
      
   Description:  "Deletes" a vendor brand record from table dbo.PracticeCatalogSupplierBrand
   					This actual flags the IsActive column to false.

   TODO:	LATER CHECK IF ANY active PRODUCTS ARE ASSOCIATED WITH THIS PracticeCatalogSupplierBrand 
					 before allowing the delete.
					
   AUTHOR:       John Bongiorni 10/5/2007 1:15:01 AM
   
   Modifications:  
   ------------------------------------------------------------ */  ------------------ DELETE ----------------

CREATE PROCEDURE [dbo].[usp_PracticeCatalogSupplierBrand_Delete_Brand]
(
	  @PracticeCatalogSupplierBrandID int
	, @ModifiedUserID INT
	, @BrandProductCount INT OUTPUT
)
AS
BEGIN
	DECLARE @Err INT

	-- Make sure that the Brand does not have any active products associated to it.
	SELECT @BrandProductCount =	
			(SELECT COUNT(1)  
			FROM PracticeCatalogProduct 
			WHERE 
				PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID 
				AND IsActive = 1) 
			
	--  "Delete" the Brand
	UPDATE
		  dbo.PracticeCatalogSupplierBrand

	SET IsActive = 0	  
		, ModifiedUserID = @ModifiedUserID
		, ModifiedDate = GETDATE()
	WHERE 
		PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID
		AND @BrandProductCount = 0

	SET @Err = @@ERROR

	RETURN @Err
End