﻿

-- =============================================
-- Author:		Greg Ross
-- Create date: Nov. 5, 2007
-- Description:	Used for Search in Check In Screen
-- This query searches on both Supplier and date range
-- =============================================
CREATE PROCEDURE [dbo].[usp_SearchForPOsByDateRange_With_ThirdPartyProducts] --3, '01/01/2007', '12/12/2007'
	@PracticeLocationID int,
	@SearchFromDate datetime = null,
	@SearchToDate datetime = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
   select 

		SUB.BregVisionOrderID,
		SUB.CustomPurchaseOrderCode,
		SUB.SupplierOrderID,
		SUB.SupplierOrderLineItemID,
		SUB.ProductInventoryID,
		SUB.PracticeCatalogProductID,
		SUB.BrandShortname,
		SUB.PurchaseOrder,
		SUB.ShortName,
		SUB.Code,
		SUB.ActualWholeSaleCost,
		SUB.SupplierOrderLineItemStatusID,
		
		SUB.InvoiceNumber,  --added by u= 1/1/2010
		SUB.InvoicePaid,    --added by u= 1/1/2010
		
		--  2007.11.15		jb
		SUB.Status AS BVOStatus,
		SUB.Status  AS SOStatus,
				
		SUB.Status,

		SUB.Packaging,
		SUB.LeftRightSide,
		SUB.Size,
		SUB.Color,
		SUB.Gender,
		SUB.CreatedDate,
		
		SUB.QuantityOrdered,
		SUB.QuantityCheckedIn,
		SUB.QuantityToCheckIn,
		
		SUB.IsThirdPartyProduct

--		SUB.BregVisionOrderID,
--		SUB.SupplierOrderID,
--		SUB.SupplierOrderLineItemID,
--		SUB.ProductInventoryID,
--		SUB.PracticeCatalogProductID,
--		SUB.BrandShortname,
--		SUB.PurchaseOrder,
--		SUB.ShortName,
--		SUB.Code,
--		SUB.ActualWholeSaleCost,
--		SUB.SupplierOrderLineItemStatusID,
--		SUB.Status,
--		SUB.Packaging,
--		SUB.LeftRightSide,
--		SUB.Size,
--		SUB.Color,
--		SUB.Gender,
--		SUB.CreatedDate,
--		SUB.QuantityOrdered,
--		SUB.IsThirdPartyProduct
		
from
(

		select 
			BVO.BregVisionOrderID,
			BVO.CustomPurchaseOrderCode,
			SO.SupplierOrderID,
			SOLI.SupplierOrderLineItemID,
			PI.ProductInventoryID,
			SOLI.PracticeCatalogProductID,
			PCSB.BrandShortname,
			BVO.PurchaseOrder,
			MCP.ShortName,
			MCP.Code,
			SOLI.ActualWholeSaleCost,
			SOLI.SupplierOrderLineItemStatusID,
			
			SO.InvoiceNumber, --added by u= 1/1/2010
			SO.InvoicePaid,   --added by u= 1/1/2010
			
			BVOS.Status AS BVOStatus,
			SOS.Status  AS SOStatus,
			
			SOLIS.Status,
			MCP.Packaging,
			MCP.LeftRightSide,
			MCP.Size,
			MCP.Color,
			MCP.Gender,
			BVO.CreatedDate,
			
			SOLI.QuantityOrdered,
			
			--  2007.11.15 JB
			ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityCheckedIn,
			
			CASE 
			WHEN ( SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) ) < 0 THEN 0
			ELSE SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0)
			END 								  AS QuantityToCheckIn,
			
			
			0 AS IsThirdPartyProduct
						

		from BregVisionOrder BVO
		
		inner join SupplierOrder SO 
			on BVO.BregVisionOrderID = SO.BregVisionOrderID
		
		inner join SupplierOrderLineItem SOLI 
			on SO.SupplierOrderID = SOLI.SupplierOrderID
		
		inner join PracticeCatalogSupplierBrand PCSB 
			on SO.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
		
		inner join PracticeCatalogPRoduct PCP 
			on SOLI.PracticeCatalogProductID = 	PCP.PracticeCatalogPRoductID
		
		inner join MasterCatalogProduct MCP
			on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
		
		inner join SupplierOrderLineItemStatus SOLIS
			on SOLI.SupplierOrderLineItemStatusID = SOLIS.SupplierOrderLineItemStatusID
		
		--  2007.11.15  JB
		INNER JOIN dbo.SupplierOrderStatus AS sos
			ON SO.SupplierOrderStatusID = SOS.SupplierOrderStatusID
		
		INNER JOIN dbo.BregVisionOrderStatus AS bvos
			ON BVO.BregVisionOrderStatusID = BVOS.BregVisionOrderStatusID
		
		
		inner join ProductInventory PI 
			on SOLI.PracticeCatalogPRoductID = pi.PracticeCatalogPRoductID

			--2007.11.15 JB And Second part of join condition.		
			AND BVO.PracticeLocationID = PI.PracticeLocationID  --2007.11.15 JB And Second part of join condition.
			--AND PI.IsActive = 1
		
		-- JB 2007.11.15 fix query to put all quantity checked-in for the supplier order line item				
		LEFT JOIN (SELECT SupplierOrderLineItemID, SUM(Quantity) AS QuantityCheckedIn
							FROM dbo.ProductInventory_OrderCheckIn 
							WHERE IsActive = 1
							GROUP BY SupplierOrderLineItemID)AS SubPIOCI
								ON SOLI.SupplierOrderLineItemID = SubPIOCI.SupplierOrderLineItemID	

		where BVO.isactive=1
		and SO.isactive = 1
		and SOLI.isactive = 1


		--and BVO.BregVisionOrderStatusID in (1,3)
		--and SO.SupplierOrderStatusID in (1,3)
		
		--and SOLI.SupplierOrderLineItemStatusID in (1,3)

		
		--  2007.11.15  JB Show all Order regardless of status.
		--  and BVO.BregVisionOrderStatusID in (1,3)
		--  and SO.SupplierOrderStatusID in (1,3)
		

		and BVO.PracticeLocationID = @PracticeLocationID
		and PI.PracticeLocationID = @PracticeLocationID

		and BVO.CreatedDate >= @SearchFromDate
		and BVO.CreatedDate <= @SearchToDate
		--and PCSB.BrandShortName like @SearchText
		and MCP.isactive = 1
		--order by c desc
union

		select 
			BVO.BregVisionOrderID,
			BVO.CustomPurchaseOrderCode,
			SO.SupplierOrderID,
			SOLI.SupplierOrderLineItemID,
			PI.ProductInventoryID,
			SOLI.PracticeCatalogProductID,
			PCSB.BrandShortname,
			BVO.PurchaseOrder,
			TPP.ShortName,
			TPP.Code,
			SOLI.ActualWholeSaleCost,
			SOLI.SupplierOrderLineItemStatusID,

			SO.InvoiceNumber,  --added by u= 1/1/2010
			SO.InvoicePaid,    --added by u= 1/1/2010
			
			BVOS.Status AS BVOStatus,
			SOS.Status  AS SOStatus,
			SOLIS.Status,

			TPP.Packaging,
			TPP.LeftRightSide,
			TPP.Size,
			TPP.Color,
			TPP.Gender,
			BVO.CreatedDate,

			SOLI.QuantityOrdered,

			--  2007.11.15 JB
			ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityCheckedIn,
			
			CASE 
			WHEN ( SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) ) < 0 THEN 0
			ELSE SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0)
			END 								  AS QuantityToCheckIn,


			1 AS IsThirdPartyProduct

		from BregVisionOrder BVO

		inner join SupplierOrder SO 
			on BVO.BregVisionOrderID = SO.BregVisionOrderID

		inner join SupplierOrderLineItem SOLI 
			on SO.SupplierOrderID = SOLI.SupplierOrderID

		inner join PracticeCatalogSupplierBrand PCSB 
			on SO.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 

		inner join PracticeCatalogPRoduct PCP 
			on SOLI.PracticeCatalogProductID = 	PCP.PracticeCatalogPRoductID

		inner join ThirdPartyProduct AS TPP  WITH(NOLOCK) 
			on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID 		

		--inner join MasterCatalogProduct MCP
		--	on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
		inner join SupplierOrderLineItemStatus SOLIS
			on SOLI.SupplierOrderLineItemStatusID = SOLIS.SupplierOrderLineItemStatusID

		--  2007.11.15  JB
		INNER JOIN dbo.SupplierOrderStatus AS sos
			ON SO.SupplierOrderStatusID = SOS.SupplierOrderStatusID
		
		INNER JOIN dbo.BregVisionOrderStatus AS bvos
			ON BVO.BregVisionOrderStatusID = BVOS.BregVisionOrderStatusID
		
		
		inner join ProductInventory PI on SOLI.PracticeCatalogPRoductID = pi.PracticeCatalogPRoductID

			--2007.11.15 JB And Second part of join condition.		
			AND BVO.PracticeLocationID = PI.PracticeLocationID  --2007.11.15 JB And Second part of join condition.
			--AND PI.IsActive = 1
		
		-- JB 2007.11.15 fix query to put all quantity checked-in for the supplier order line item				
		LEFT JOIN (SELECT SupplierOrderLineItemID, SUM(Quantity) AS QuantityCheckedIn
							FROM dbo.ProductInventory_OrderCheckIn 
							WHERE IsActive = 1
							GROUP BY SupplierOrderLineItemID)AS SubPIOCI
								ON SOLI.SupplierOrderLineItemID = SubPIOCI.SupplierOrderLineItemID	



		where BVO.isactive=1
		and SO.isactive = 1
		and SOLI.isactive = 1

		--  2007.11.15  JB Show all Order regardless of status.
		--  and BVO.BregVisionOrderStatusID in (1,3)
		--  and SO.SupplierOrderStatusID in (1,3)
		--and SOLI.SupplierOrderLineItemStatusID in (1,3)
		--and SOLI.SupplierOrderLineItemStatusID in (1,3)
		and BVO.PracticeLocationID = @PracticeLocationID
		and PI.PracticeLocationID = @PracticeLocationID
		and BVO.CreatedDate >= @SearchFromDate
		and BVO.CreatedDate <= @SearchToDate
		--and PCSB.BrandShortName like @SearchText
		and TPP.isactive = 1
		--order by BVO.CreatedDate desc
) AS Sub
ORDER BY
	SUB.CreatedDate desc,
	Sub.IsThirdPartyProduct
	

END


