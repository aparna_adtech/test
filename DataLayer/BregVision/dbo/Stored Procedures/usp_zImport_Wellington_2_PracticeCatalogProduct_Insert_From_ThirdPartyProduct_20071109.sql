﻿
--  Step 2 for Wellington
--  Wellington's PracticeID is 6

CREATE PROC [dbo].[usp_zImport_Wellington_2_PracticeCatalogProduct_Insert_From_ThirdPartyProduct_20071109] 
	@PracticeID INT

as
begin


INSERT INTO PracticeCatalogProduct
	( PracticeID
	, PracticeCatalogSupplierBrandID
	, ThirdPartyProductID
	, IsThirdPartyProduct
	, WholesaleCost
	, CreatedUserID
	, IsActive
	)

--  Note: UNCOMMENT THE FOLLOWING TWO LINES AND RUN SELECT TO VERIFY BEFORE INSERTING!!!
--DECLARE @PracticeID INT
--SET @PracticeID = 6
--  --select * from practice where isactive = 1 

SELECT
		PCSB.PracticeID
	, PCSB.PracticeCatalogSupplierBrandID
	, TPS.ThirdPartyProductID
	, 1 AS IsThirdPartyProduct
	, TPS.WholesaleListCost AS WholesaleCost
	, 13  --TPS.CreatedUserID
	, TPS.IsActive
FROM ThirdPartyProduct AS TPS
	INNER JOIN PracticeCatalogSupplierBrand AS PCSB
		ON TPS.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
	INNER JOIN Practice AS P
		ON P.PracticeID = PCSB.PracticeID
	WHERE P.PracticeID = @PracticeID
	AND TPS.IsActive = 1	--  Avoid getting bogus third party products that have been backed out
	AND TPS.ThirdPartyProductID NOT IN
		(SELECT ThirdPartyProductID 
		 FROM PracticeCatalogProduct 
		 WHERE IsActive = 1 
			AND IsThirdPartyProduct = 1) 

end