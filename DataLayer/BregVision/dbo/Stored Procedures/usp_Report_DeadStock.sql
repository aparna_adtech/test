﻿


-- =============================================
-- Author:		<Author:Greg Ross>
-- Create date: <Create Date: 07/23/07>
-- Description:	<Description: Query to get deadstock (Products not dispensed in over 60 days) items for dashboard>
-- Modification:  John Bongiorni  20071008  A productCatalogProduct must be dispensed at least once before showing
--					in dead stock
-- =============================================
CREATE PROCEDURE [dbo].[usp_Report_DeadStock]
	-- Add the parameters for the stored procedure here
	@PracticeID INT,
	@PracticeLocationID INT,
	@SupplierShortName  VARCHAR(2000),
	@DateRange int --Date range will be negative 60,90, 120 + days respectively
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT 
		 SUB.PracticeName
		, SUB.PracticeLocation

		, SUB.SupplierShortName
		--, SUB.BrandShortName
		
		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN NULL
		ELSE SUB.BrandShortName
		END AS BrandShortName
		
		
		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN 1
		ELSE 0
		END AS IsBrandSuppressed


		, SUB.Name
		, SUB.Code
		, SUB.[QuantityOnHandPerSystem]
        , SUB.[ParLevel]
        , SUB.[ReorderLevel]
        , SUB.[CriticalLevel]

FROM
	(


SELECT  P.PracticeName,
		PL.Name as PracticeLocation,

		PCSB.IsThirdPartySupplier,
		PCSB.SupplierShortName,
		PCSB.BrandShortName,

        MCP.name,
        MCP.[Code],
        PI.[QuantityOnHandPerSystem],
        PI.[ParLevel],
        PI.[ReorderLevel],
        PI.[CriticalLevel]
	--,D.[DateDispensed]
	--,[DateDispensed]
FROM    [ProductInventory] PI
        INNER JOIN [PracticeCatalogProduct] PCP ON PI.[PracticeCatalogProductID] = PCP.[PracticeCatalogProductID]
        INNER JOIN [MasterCatalogProduct] MCP ON PCP.[MasterCatalogProductID] = MCP.[MasterCatalogProductID]
        --INNER JOIN [MasterCatalogSubCategory] MCSC ON MCP.[MasterCatalogSubCategoryID] = MCSC.[MasterCatalogSubCategoryID]
        --INNER JOIN [MasterCatalogCategory] MCC ON MCSC.[MasterCatalogCategoryID] = MCC.[MasterCatalogCategoryID]
        --INNER JOIN [MasterCatalogSupplier] MCS ON MCC.[MasterCatalogSupplierID] = MCS.[MasterCatalogSupplierID]
        INNER JOIN [Practice] P ON PCP.[PracticeID] = P.[PracticeID]
        INNER JOIN [PracticeLocation] PL ON P.[PracticeID] = PL.[PracticeID]
		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
--INNER JOIN [Dispense] D
--ON PL.[PracticeLocationID] = D.[PracticeLocationID]
--INNER JOIN [DispenseDetail] DD
--ON D.[DispenseID] = DD.[DepenseID]
WHERE   PCP.[PracticeCatalogProductID] NOT IN (
        SELECT  [PracticeCatalogProductID]
        FROM    [Dispense]
                INNER JOIN [DispenseDetail] ON [Dispense].[DispenseID] = [DispenseDetail].[DispenseID]
        WHERE   [PracticeLocationID] = @PracticeLocationID
                AND [DateDispensed] > dateadd(dd, @DateRange, getdate()) )
                
        -- jb 20071008  The product must be dispensed at least one to show up in dead stock.
        	AND PCP.[PracticeCatalogProductID] IN   -- The product must be Dispensed at least once.
		(
			SELECT  [PracticeCatalogProductID]
			FROM    [Dispense]
					INNER JOIN [DispenseDetail] ON [Dispense].[DispenseID] = [DispenseDetail].[DispenseID]
			WHERE   [PracticeLocationID] = @PracticeLocationID
		)        
        -- jb 20071008 end of modification        
                
        AND p.[PracticeID] = @PracticeID
        AND pl.[PracticeLocationID] =	@PracticeLocationID
--
		AND PCSB.SupplierShortName IN 			
			(SELECT TextValue 
		 FROM dbo.udf_ParseStringArrayToTable( @SupplierShortName, ',') )


		--AND D.[PracticeLocationID] = @PracticeLocationID
        AND PI.[IsActive] = 1
        AND PCP.[IsActive] = 1
        AND MCP.IsActive = 1
--        AND MCSC.[IsActive] = 1
--        AND MCC.[IsActive] = 1
--        AND MCS.[IsActive] = 1
        AND P.[IsActive] = 1
        AND pl.[IsActive] = 1

UNION

SELECT  P.PracticeName,
		PL.Name as PracticeLocation,

		PCSB.IsThirdPartySupplier,
		PCSB.SupplierShortName,
		PCSB.BrandShortName,

        TPP.name,
        TPP.[Code],
        PI.[QuantityOnHandPerSystem],
        PI.[ParLevel],
        PI.[ReorderLevel],
        PI.[CriticalLevel]
	--,D.[DateDispensed]
	--,[DateDispensed]
FROM    [ProductInventory] PI
        INNER JOIN [PracticeCatalogProduct] PCP ON PI.[PracticeCatalogProductID] = PCP.[PracticeCatalogProductID]
        --INNER JOIN [MasterCatalogProduct] MCP ON PCP.[MasterCatalogProductID] = MCP.[MasterCatalogProductID]
		INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
			ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
        --INNER JOIN [MasterCatalogSubCategory] MCSC ON MCP.[MasterCatalogSubCategoryID] = MCSC.[MasterCatalogSubCategoryID]
        --INNER JOIN [MasterCatalogCategory] MCC ON MCSC.[MasterCatalogCategoryID] = MCC.[MasterCatalogCategoryID]
        --INNER JOIN [MasterCatalogSupplier] MCS ON MCC.[MasterCatalogSupplierID] = MCS.[MasterCatalogSupplierID]
        INNER JOIN [Practice] P ON PCP.[PracticeID] = P.[PracticeID]
        INNER JOIN [PracticeLocation] PL ON P.[PracticeID] = PL.[PracticeID]
		INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
--INNER JOIN [Dispense] D
--ON PL.[PracticeLocationID] = D.[PracticeLocationID]
--INNER JOIN [DispenseDetail] DD
--ON D.[DispenseID] = DD.[DepenseID]
WHERE   PCP.[PracticeCatalogProductID] NOT IN (
        SELECT  [PracticeCatalogProductID]
        FROM    [Dispense]
                INNER JOIN [DispenseDetail] ON [Dispense].[DispenseID] = [DispenseDetail].[DispenseID]
        WHERE   [PracticeLocationID] = @PracticeLocationID
                AND [DateDispensed] > dateadd(dd, @DateRange, getdate()) )
                
        -- jb 20071008  The product must be dispensed at least one to show up in dead stock.
        	AND PCP.[PracticeCatalogProductID] IN   -- The product must be Dispensed at least once.
		(
			SELECT  [PracticeCatalogProductID]
			FROM    [Dispense]
					INNER JOIN [DispenseDetail] ON [Dispense].[DispenseID] = [DispenseDetail].[DispenseID]
			WHERE   [PracticeLocationID] = @PracticeLocationID
		)        
        -- jb 20071008 end of modification        
                
        AND p.[PracticeID] = @PracticeID
        AND pl.[PracticeLocationID] =	@PracticeLocationID
--
		AND PCSB.SupplierShortName IN 			
			(SELECT TextValue 
		 FROM dbo.udf_ParseStringArrayToTable( @SupplierShortName, ',') )


		--AND D.[PracticeLocationID] = @PracticeLocationID
        AND PI.[IsActive] = 1
        AND PCP.[IsActive] = 1
        AND TPP.IsActive = 1
--        AND MCSC.[IsActive] = 1
--        AND MCC.[IsActive] = 1
--        AND MCS.[IsActive] = 1
        AND P.[IsActive] = 1
        AND pl.[IsActive] = 1
	)		
		AS SUB	


ORDER BY SUB.[SupplierShortName],
        SUB.NAME
END




