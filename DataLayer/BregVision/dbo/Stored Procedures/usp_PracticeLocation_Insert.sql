﻿/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocation_Insert

   Description:  Inserts a record into table PracticeLocation.

   AUTHOR:       John Bongiorni 7/23/2007 11:05:42 AM

   --Test Script:
   DECLARE	@return_value int,
		@PracticeLocationID int

	EXEC	@return_value = [dbo].[usp_PracticeLocation_Insert]
			@PracticeLocationID = @PracticeLocationID OUTPUT,
			@PracticeID = 12,
			@Name = N'Clairemont',
			@IsPrimaryLocation = 1,
			@CreatedUserID = 0

	SELECT	@PracticeLocationID as N'@PracticeLocationID'

	SELECT	'Return Value' = @return_value
	GO

   Modifications:  20071017 John Bongiorni
			-- If first practice, then set it as the primary location.
   ------------------------------------------------------------ */


CREATE PROCEDURE [dbo].[usp_PracticeLocation_Insert]
(
      @PracticeLocationID				INT			= NULL OUTPUT
    , @PracticeID						INT
    , @Name								VARCHAR(50)
 	  , @GeneralLedgerNumber				VARCHAR(50)
    , @IsPrimaryLocation				BIT			= 0
--    , @EmailForOrderApproval			VARCHAR(100)
--    , @EmailForLowInventoryAlerts		VARCHAR(100)
--    , @IsEmailForLowInventoryAlertsOn	BIT
    , @CreatedUserID					INT
	, @IsHCPSignatureRequired			BIT
)
AS
BEGIN

DECLARE		@TransactionCountOnEntry    INT       -- Transaction Count before the transaction begins
DECLARE		@Err             			INT        --  holds the @@ERROR code returned by SQL Server
DECLARE		@RecordCount				INT
DECLARE     @PracticeLocationCount		INT

SET @RecordCount = 0




		-- Count active location for the practice
		SELECT @PracticeLocationCount = COUNT(1)
		FROM PracticeLocation AS PL
		INNER JOIN dbo.Practice AS P
			ON P.PracticeID = PL.PracticeID
		WHERE P.PracticeID = @PracticeID
		AND PL.IsActive = 1
		AND PL.IsPrimaryLocation = 1



		SELECT @RecordCount = COUNT(1)
		FROM dbo.PracticeLocation AS pl
		WHERE pl.PracticeID = @PracticeID
			AND pl.[Name] = @Name
			AND pl.IsActive = 1



		IF (@RecordCount = 0)
			BEGIN

				-- If first practice, then set it as the primary location.
				IF (@PracticeLocationCount = 0)
				BEGIN
					SET @IsPrimaryLocation = 1
				END

				INSERT
				INTO dbo.PracticeLocation
				(
					  PracticeID
					, Name
					, GeneralLedgerNumber
					, IsPrimaryLocation
		--			, EmailForOrderApproval
		--			, EmailForLowInventoryAlerts
		--			, IsEmailForLowInventoryAlertsOn
					, CreatedUserID
					, IsActive
					, IsHcpSignatureRequired
				)
				VALUES
				(
					  @PracticeID
					, @Name
					, @GeneralLedgerNumber
					, @IsPrimaryLocation
		--			, @EmailForOrderApproval
		--			, @EmailForLowInventoryAlerts
		--			, @IsEmailForLowInventoryAlertsOn
					, @CreatedUserID
					, 1
					, @IsHCPSignatureRequired
				)

	--			SET @Err = @@ERROR
	--
	--		END
	--		IF @Err = 0
	--		BEGIN

				SELECT @PracticeLocationID = SCOPE_IDENTITY()

	--			SET @Err = @@ERROR

	--		END
	--		IF @Err = 0
	--		BEGIN
			IF ( @IsPrimaryLocation = 1 )
			BEGIN

				EXEC usp_PracticeLocation_Set_Primary_Location @PracticeLocationID = @PracticeLocationID

			END
	--		SET @Err = @@ERROR
	--		END

		END

	ELSE
		BEGIN

			SELECT @PracticeLocationID = pl.PracticeLocationID
			FROM dbo.PracticeLocation AS pl
			WHERE pl.PracticeID = @PracticeID
				AND pl.NAME = @Name
				AND pl.IsActive = 1

		END




--	IF @@TranCount > @TransactionCountOnEntry
--	BEGIN
--
--		IF @Err = 0
--			BEGIN
--				COMMIT TRANSACTION
--				RETURN @Err
--			END
--		ELSE
--			BEGIN
--				ROLLBACK TRANSACTION
--				RETURN @Err
--				--  Add any database logging here
--			END
END
