﻿-- =============================================
-- Author:		Mike Sneen
-- Create date: 06/15/09
-- Description:	Used to get the parent table for the Custom Brace Orders to dispense in the dispense UI. 
-- Exec usp_GetCustomBraceOrdersforDispense_ParentLevel 3, '', ''
--exec usp_GetCustomBraceOrdersforDispense_ParentLevel 3, 'PONumber', '109001'
--exec usp_GetCustomBraceOrdersforDispense_ParentLevel 3, 'PatientID', 'greg'
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetCustomBraceOrdersforDispense_ParentLevel]
	@PracticeLocationID int,
	@SearchCriteria varchar(50), -- we only care if this is 'PONumber' OR 'PatientID' but could also be 'Browse', 'All', '',
	@SearchText  varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	CREATE TABLE #TempBregVisionOrderIdsInDispenseQueue
    (
        BregVisionOrderId   integer NOT NULL,
        
    );
	INSERT INTO #TempBregVisionOrderIdsInDispenseQueue
        (BregVisionOrderId)
    SELECT
        BregVisionOrderId
    FROM DispenseQueue AS ds
    WHERE
        ds.BregVisionOrderId is not null and ds.IsActive = 1

	Declare @BregVisionOrderID int
	Declare @PatientID varchar(50)

	set @bregVisionOrderID = null
	Set @PatientID = null

	if (@SearchCriteria='PONumber' AND ISNUMERIC(@SearchText) = 1)
	begin
	  set @BregVisionOrderID = (Cast( @SearchText as int) - 100000 )  --need to convert this to a number and do any 100000 suberaction necessary
	end
	else if(@SearchCriteria='PatientID') 
	begin
	  set @PatientID = '%' + @SearchText + '%'
	end

	SELECT 
	  BVO.BregVisionOrderID
	, BVO.CustomPurchaseOrderCode
	, BVO.PurchaseOrder
	, BVO.CreatedUserID
	, BVOS.Status
	, ST.ShippingTypeName as ShippingType
	, BVO.Total
	, BVO.CreatedDate 
	, SCCB.PatientName
	, SCCB.PhysicianID
	FROM BregVisionOrder AS BVO
	
	INNER JOIN BregVisionOrderStatus AS BVOS 
		ON BVO.BregVisionOrderStatusID = BVOS.BregVisionOrderStatusID
	INNER JOIN ShoppingCartCustomBrace SCCB
		ON SCCB.BregVisionOrderID = BVO.BregVisionOrderID
	INNER JOIN ShippingType AS ST 
		ON BVO.ShippingTypeID = ST.ShippingTypeID
	

	WHERE BVO.BregVisionOrderStatusID IN (2)  --checked in
		AND BVO.isactive = 1 
		AND BVO.PracticeLocationID = @PracticeLocationID 
		AND SCCB.DispenseID is null
		
		--this, in conjunction with the definition of these variables above, handles custom search
		and SCCB.BregVisionOrderID = Coalesce(@BregVisionOrderID, SCCB.BregVisionOrderID)
		AND ((@PatientID Is Not NULL) AND  (SCCB.PatientName in(Select Distinct PatientName from ShoppingCartCustomBrace where PatientName Like @PatientID)) OR (@PatientID IS NULL))		
		and BVO.BregVisionOrderID not in (select * from #TempBregVisionOrderIdsInDispenseQueue)
	ORDER BY CreatedDate

END
