﻿

--SELECT * FROM dbo.SupplierOrder AS so INNER JOIN dbo.BregVisionOrder  AS bvo --po 100139  --soid 222
--	ON so.BregVisionOrderID = bvo.BregVisionOrderID 
--	WHERE bvo.PracticeLocationID = 3




-- =============================================
-- Author:		Michael Sneen
-- Create date: 08/20/2007
-- Description:	Used in the Oustanding Purchase Order section of the Check-in UI. 
-- This is a query for the Detail table

--  Modifications:  2007.11.13 JB  Comment out correct code for quantities.
--				Add BOGUS code for quantities for compatibility for current version.
--
--				JB 2007.11.15 fix query to put all quantity checked-in for the supplier order line item
--exec usp_GetOustandingCustomBraceOrdersforCheckIn_Detail2Level 3
-- =============================================
Create PROCEDURE [dbo].[usp_GetOustandingCustomBraceOrdersforCheckIn_Detail2Level] --162, 15  -- Prod 222, 3  -- BimsDev 169, 3   -- BimsDev 162, 15

	@PracticeLocationID int

AS
BEGIN

SET NOCOUNT ON;
select  SUB.SupplierOrderID, 
		SUB.ActualWholesaleCost,
		SUB.QuantityOrdered,		
		ISNULL(SUB.QuantityCheckedIn, 0) AS QuantityCheckedIn,   --  Correct for next release
		ISNULL(SUB.QuantityToCheckIn, 0) AS QuantityToCheckIn,  --   Correct for next release
		SUB.LineTotal,
		SUB.Code,
		SUB.ShortName,
		SUB.Packaging,
		SUB.LeftRightSide,
		SUB.Size,
		SUB.Color,
		SUB.Gender,
		SUB.PracticeCatalogProductID,
		SUB.SupplierOrderLineItemID,
		SUB.SupplierOrderLineItemStatusID,
		SUB.ProductInventoryID,
		SUB.Status,
		SUB.IsThirdPartyProduct

from (

	select SOLI.SupplierOrderID, 
		SOLI.ActualWholesaleCost,
		SOLI.QuantityOrdered,
		ISNULL(SubPIOCI.QuantityCheckedIn, 0) AS QuantityCheckedIn,
		CASE
		WHEN ( SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) ) < 0 THEN 0
		ELSE  SOLI.QuantityOrdered - ISNULL(SubPIOCI.QuantityCheckedIn, 0) 
		END									AS QuantityToCheckIn,
		SOLI.LineTotal,
		MCP.Code,
		MCP.ShortName,
		MCP.Packaging,
		MCP.LeftRightSide,
		MCP.Size,
		MCP.Color,
		MCP.Gender,
		PCP.PracticeCatalogProductID,
		SOLI.SupplierOrderLineItemID,
		SOLI.SupplierOrderLineItemStatusID,
		PI.ProductInventoryID,
		SOLIS.Status,
		0 AS IsThirdPartyProduct	
	
from SupplierOrderLineItem SOLI
inner join SupplierOrder SO on SOLI.SupplierOrderID = SO.SupplierOrderID
inner join BregVisionOrder BVO on SO.BregVisionOrderID= BVO.BregVisionOrderID
inner join PracticeCatalogPRoduct PCP 
	on SOLI.PracticeCatalogProductID = 	PCP.PracticeCatalogPRoductID
inner join MasterCatalogProduct MCP
	on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
inner join ProductInventory PI
	on SOLI.PracticeCatalogProductID = PI.PracticeCatalogProductID
inner join SupplierOrderLineItemStatus SOLIS 
	on SOLI.SupplierOrderLineItemStatusID = SOLIS.SupplierOrderLineItemStatusID
LEFT JOIN (SELECT SupplierOrderLineItemID, SUM(Quantity) AS QuantityCheckedIn
			FROM dbo.ProductInventory_OrderCheckIn 
			WHERE IsActive = 1
			GROUP BY SupplierOrderLineItemID)AS SubPIOCI
				ON SOLI.SupplierOrderLineItemID = SubPIOCI.SupplierOrderLineItemID
			where 
				PI.PracticeLocationID = @PracticeLocationID
				AND BVO.BregVisionOrderStatusID IN (1,3) 
				AND BVO.isactive = 1 
				AND BVO.PracticeLocationID = @PracticeLocationID 
				and SO.SupplierOrderStatusID in (1,3)
				and SO.isactive = 1
				AND PCP.IsThirdPartyProduct = 0		-- MasterCatalogProduct
				and PCP.isactive = 1
				and MCP.isactive = 1


			) AS Sub
ORDER BY
	Sub.IsThirdPartyProduct,
	SUB.ShortName,
	SUB.Code,
	SUB.LeftRightSide
	
--and BVO.BregVisionOrderID = @BregVisionOrderID
END







