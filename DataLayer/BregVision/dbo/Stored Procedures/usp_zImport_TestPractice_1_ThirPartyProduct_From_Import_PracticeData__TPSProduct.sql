﻿
create proc [dbo].[usp_zImport_TestPractice_1_ThirPartyProduct_From_Import_PracticeData__TPSProduct]
as
begin

INSERT INTO ThirdPartyProduct
	( PracticeCatalogSupplierBrandID
	, Code
	, Name
	, ShortName

	, Packaging
	, LeftRightSide
	, Size
	, Color
	, Gender
	, IsDiscontinued
	, WholesaleListCost
	, Description
	, CreatedUserID
	, CreatedDate
	, IsActive
	)

Select 
	  PracticeCatalogSupplierBrandID
	, Code
	, Name
	, ShortName

	, Packaging
	, LeftRightSide
	, Size
	, Color
	, Gender
	, 0 AS IsDiscontinued
	, WholesaleListCost
	, Description
	, 0
	, GetDate()
	, 1
from Import_PracticeData.dbo.TESTPractice_TPS_Product_Import$

end