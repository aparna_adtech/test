﻿
/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocation_Select_Name_ByParams
   
   Description:  Selects a the PracticeLocation.Name from table dbo.PracticeLocation
   				   given the Practice Location ID and puts the value into a parameter.
   
   AUTHOR:       John Bongiorni 8/16/2007 11:26:16 AM
   
   Modifications:  
   ------------------------------------------------------------ */  ---------------------SELECT PARAMS-----------

CREATE PROCEDURE [dbo].[usp_PracticeLocation_Select_Name_ByParams]
(
      @PracticeLocationID            INT
    , @Name                          VARCHAR(50)    OUTPUT
)
AS
BEGIN
	DECLARE @Err INT

	SELECT
        @Name = Name
	FROM dbo.PracticeLocation
	WHERE 
		PracticeLocationID = @PracticeLocationID
		AND IsActive = 1

	SET @Err = @@ERROR

	RETURN @Err
End


