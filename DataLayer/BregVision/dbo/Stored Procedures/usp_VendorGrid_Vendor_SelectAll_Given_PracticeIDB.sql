﻿
/* ------------------------------------------------------------
   PROCEDURE:    usp_ThirdPartySupplier_SelectAll_Vendor_And_NonVendorWithPCSBID
				 
   Description:  Selects all records from the table dbo.ThirdPartySupplier
					given a practiceID
   Note:		this is being used by the vendor grid.
   
   AUTHOR:       John Bongiorni 9/26/2007 3:46:45 PM
   
   Modifications:  JB 10/3/2007  3:45PM  Order by sequence if there is one, then by Supplier Short Name.
				   JB 20080319
					Returns the PracticeID, ThirdPartySupplierID, Vendor, IsVendor, and Sequence 
					for all Third Party Vendor and nonVendor Suppliers.
					
   ------------------------------------------------------------ */  -------------------SELECT ALL By PracticeID
--  [usp_ThirdPartySupplier_SelectAll_Vendors_And_NonVendorSuppliers]  31
/*
This replaces [usp_ThirdPartySupplier_SelectAll_Vendors_And_NonVendorSuppliers....]
   Note:  The LinkThirdPartySupplierID is used to link the Vendor Grid's top two hierarchies,
   The NonVendorSuppliers... holds the LinkThirdPartySupplierID in order to match is up to the 
   PCSBs with all PCSBs of the suppliers of LinkThirdPartySupplierID's practice.
   --  [usp_VendorGrid_Vendor_SelectAll_Given_PracticeID2]
   --  Display all suppliers withou the nonvendorsuppliers... logic
*/

CREATE PROCEDURE [dbo].[usp_VendorGrid_Vendor_SelectAll_Given_PracticeIDB]  --31
		@PracticeID	INT
AS
BEGIN
	DECLARE @Err INT
	
	SELECT 
		VendorGroup.Vendor
		, VendorGroup.PracticeID
		, VendorGroup.ThirdPartySupplierID
		, VendorGroup.ThirdPartySupplierID AS LinkThirdPartySupplierID
		, VendorGroup.PracticeCatalogSupplierBrandID -- jb just for testing
		, VendorGroup.IsVendor
		, VendorGroup.Sequence
	
	FROM 
	(
	
	SELECT
		 TPS.SupplierShortName AS Vendor		
		, TPS.PracticeID
		, TPS.ThirdPartySupplierID
		--, TPS.ThirdPartySupplierID AS LinkThirdPartySupplierID
		, 0 AS PracticeCatalogSupplierBrandID
		, TPS.IsVendor		
		--, CASE IsVendor WHEN 1 THEN 'Vendor' WHEN 0 THEN 'Manufacturer' END AS SupplierType
		, ISNULL(TPS.Sequence, 0) AS sequence
	
	FROM dbo.ThirdPartySupplier AS TPS WITH (NOLOCK)
	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB WITH (NOLOCK)
		ON TPS.ThirdPartySupplierID = PCSB.ThirdPartySupplierID
		AND PCSB.IsActive = 1
	WHERE 
		TPS.PracticeID = @PracticeID
		AND TPS.IsActive = 1
		AND IsVendor = 1
		  
	UNION
	
	SELECT --TOP 1  --jb 20080328 
		  TPS.SupplierShortName AS Vendor   --jb 20080328  'NonVendor Suppliers...' AS Vendor   
		,  TPS.PracticeID
		, TPS.ThirdPartySupplierID AS ThirdPartySupplierID
		--, TPS.ThirdPartySupplierID AS LinkThirdPartySupplierID
		, PCSB.PracticeCatalogSupplierBrandID
		, TPS.IsVendor		
		--, CASE IsVendor WHEN 1 THEN 'Vendor' WHEN 0 THEN 'Manufacturer' END AS SupplierType
		, 9999 AS sequence
	
	FROM dbo.ThirdPartySupplier AS TPS WITH (NOLOCK)
	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB WITH (NOLOCK)
		ON TPS.ThirdPartySupplierID = PCSB.ThirdPartySupplierID
		AND PCSB.IsActive = 1
	WHERE 
		TPS.PracticeID = @PracticeID
		AND TPS.IsActive = 1
		AND IsVendor = 0
			  
			  
	) AS VendorGroup
	ORDER BY
	VendorGroup.IsVendor
	, VendorGroup.Sequence
	, VendorGroup.Vendor		  
		  
--	ORDER BY
--		ISNULL(TPS.Sequence, 999)
--		, TPS.SupplierShortName
--	
	--SET @Err = @@ERROR

	--RETURN @Err
END
