﻿
/*

EXEC dbo.usp_Reports_FirstDayOfMonth_Given_NumberOfMonthsBack 2  EXEC dbo.usp_Reports_LastDayOfMonth


*/

CREATE proc dbo.usp_Reports_FirstDayOfMonth_Given_NumberOfMonthsBack
		@NumberOfMonthsBack		INT
AS
BEGIN

select [dbo].[firstDayOfMonth_MonthsAgo](getdate(), @NumberOfMonthsBack) AS FirstOfGivenMonth


END