﻿
/*
-- =============================================
-- Author:		jOHN Bongionri	
-- Create date: 2008.02.29
-- Description:	Get the PCSBs for the PC grid given a practice and the search criteria. 
--  Used in MCToPC.aspx
-- =============================================
--  Modification:  2007.11.27  JB  
--		Proc below does not make any sense and 
--		is being commented out.
--      Add PCSB.IsActive = 1
--		Add SupplierName
--		--  2007.12.12 JB Order by IsThirdPartySupplier, SupplierName, BrandName.

*/
CREATE PROCEDURE [dbo].[usp_PracticeCatalogSupplierBrand_Select_Unique] --1, 'Brand', 'x'
	-- Add the parameters for the stored procedure here
	  @PracticeID INT
	, @SearchCriteria		VARCHAR(50)
	, @SearchText			VARCHAR(50)
AS
BEGIN

			set @SearchText = '%' + @SearchText + '%'	
			if (@SearchCriteria='Browse' or @SearchCriteria='All' or @SearchCriteria='')
			begin
			
--				EXEC	[dbo].[usp_PracticeCatalogSupplierBrand_Select_Unique_All]
--						@PracticeID = @PracticeID

				EXEC	[dbo].[usp_GetPracticeCatalogSupplierBrands] 
						@PracticeID = @PracticeID
			
			end
		else if @SearchCriteria='Name'	-- ProductName
			begin
						
				EXEC  [dbo].[usp_PracticeCatalogSupplierBrand_Select_Unique_By_ProductName]
						  @PracticeID = @PracticeID
						, @SearchText = @SearchText 

			end
		else if @SearchCriteria='Code'
			begin

				EXEC  [dbo].[usp_PracticeCatalogSupplierBrand_Select_Unique_By_Code]
						  @PracticeID = @PracticeID
						, @SearchText = @SearchText 

			end
		else if @SearchCriteria='Brand'
			BEGIN

				EXEC  [dbo].[usp_PracticeCatalogSupplierBrand_Select_Unique_By_Brand]
						  @PracticeID = @PracticeID
						, @SearchText = @SearchText 
			END
			
		else if @SearchCriteria='HCPCs'
			BEGIN

				EXEC  [dbo].[usp_PracticeCatalogSupplierBrand_Select_Unique_By_HCPCs]
						  @PracticeID = @PracticeID
						, @SearchText = @SearchText 
			END
		
									
END


