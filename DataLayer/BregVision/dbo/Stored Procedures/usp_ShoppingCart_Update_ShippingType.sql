﻿CREATE PROC usp_ShoppingCart_Update_ShippingType
	  @ShoppingCartID INT
	, @ShippingTypeID INT
AS
BEGIN

	UPDATE ShoppingCart
	SET ShippingTypeID = @ShippingTypeID
	WHERE ShoppingCartID = @ShoppingCartID

END