﻿

-- =============================================
-- Author:		Greg Ross	
-- Create date: 08/29/07
-- Description:	Creates Purchase Order
-- =============================================
CREATE PROCEDURE [dbo].[usp_CreatePurchaseOrder_Original_20080115]
	@PracticeLocationID int,
	@ShoppingCartID int,
	@UserID int,
	@ShippingTypeID int,
	@PurchaseOrderID int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	DECLARE @RecordCount INT
    DECLARE @Err INT 
    DECLARE @TransactionCountOnEntry INT 

	declare @PONumber int
	declare @BVOID int
	--declare @LineTotal smallmoney

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

        SELECT  @TransactionCountOnEntry = @@TRANCOUNT
        SELECT  @Err = @@ERROR
        BEGIN TRANSACTION      	
		
        IF @Err = 0 
            BEGIN

				--Initial Insert into BregVision Order and gets the identity column
				insert into BregVisionOrder
					(PracticeLocationID,
					 BregVisionOrderStatusID,
					 ShippingTypeID,
					 CustomPurchaseOrderCode,
					 CreatedUserID,
					 CreatedDate,
					 isActive)
					Values(
					@PracticeLocationID,
					1,
					@ShippingTypeID,
					1,
					1,
					getdate(),
					1)

				Select  @BVOID = SCOPE_IDENTITY()
				set @PONumber = @BVOID + 100000
				
				SET @Err = @@ERROR
            END

        IF @Err = 0 
            BEGIN

				--Updates Purchase Order Number
				update BregVisionOrder 
					set PurchaseOrder = @PONumber
					where BregVisionOrderID = @BVOID
				
				SET @Err = @@ERROR
            END

        IF @Err = 0 
            BEGIN

				--Select and insert suppliers into Supplier Order table
				insert into SupplierOrder
					(BregVisionOrderID,
					 PracticeCatalogSupplierBrandID,
					 SupplierOrderStatusID,
					 PurchaseOrder,
					 CustomPurchaseOrderCode,
					 Total,
					 CreatedUserID,
					 CreatedDate,
					 isActive)
					

					( SELECT @BVOID, 
							Derived.PracticeCatalogSupplierBrandID,
							1,
							@PONumber,
							1,
							'1',
							@userid,
							getdate(),
							1
						FROM
						(Select DISTINCT
							PCSB.PracticeCatalogSupplierBrandID
						from
							PracticeCatalogSupplierBrand PCSB
							inner join PracticeCatalogProduct PCP
								on PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
							inner join ShoppingCartItem SCI on PCP.PracticeCatalogProductID = SCI.PracticeCatalogProductID
							where PCSB.isActive=1
								and PCP.isactive=1
								and SCI.isactive=1 
								and SCI.PracticeCatalogProductID 
									in (select PracticeCatalogProductID 
										from ShoppingCartItem 
										where ShoppingCartID = @ShoppingCartID) ) as Derived
						)
				
				SET @Err = @@ERROR
            END

        IF @Err = 0 
            BEGIN

				--Insert data into SupplierOrderLineItem
				insert into SupplierOrderLineItem
					(SupplierOrderID,
					 PracticeCatalogProductID,
					 SupplierOrderLineItemStatusID,
					 ActualWholesaleCost,
					 QuantityOrdered,
					 LineTotal,
					 CreatedUserID,
					 CreatedDate,
					 isActive)
					
					(Select Derived.SupplierOrderID,
							Derived.PracticeCatalogProductID,
							1,
							Derived.ActualWholesaleCost,
							Derived.Quantity,
							Derived.ActualWholesaleCost * Derived.Quantity as LineTotal,
							@UserID,
							getdate(),
							1
						From
   						(select distinct SCI.PracticeCatalogProductID,
						SCI.ActualWholesaleCost,
						SCI.Quantity,
						SO.SupplierORderID
						  from
						PracticeCatalogSupplierBrand PCSB
						inner join PracticeCatalogProduct PCP
							on PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
						inner join ShoppingCartItem SCI on PCP.PracticeCatalogProductID = SCI.PracticeCatalogProductID
						inner join SupplierOrder SO on PCSB.PracticeCatalogSupplierBrandID = SO.PracticeCatalogSupplierBrandID
						where PCSB.isactive=1
							and PCP.isactive=1
							and SCI.isactive=1
							and SO.isactive=1
							and SCI.PracticeCatalogProductID in (select  PracticeCatalogProductID from ShoppingCartItem where ShoppingCartID =@ShoppingCartID
							and SO.BregVisionOrderID = @BVOID) ) as derived
					)
			
				SET @Err = @@ERROR
            END

        IF @Err = 0 
            BEGIN

				--Sum and update totals in SupplierOrder and BregVision Order

				update SupplierOrder
				SET Total = Derived.Total		--  SELECT Derived.Total AS Total
				FROM 
					( SELECT 
							SOLI.SupplierOrderID	AS SupplierOrderID
							, SUM( SOLI.LineTotal ) AS Total
					FROM BregVisionOrder AS BV
					INNER JOIN SupplierOrder AS SO
						ON BV.BregVisionOrderID = SO.BregVisionOrderID
					INNER JOIN SupplierOrderLineItem AS SOLI
						ON SO.SupplierOrderID = SOLI.SupplierOrderID
					WHERE BV.isactive=1
						and SO.isactive=1
						and SOLI.isactive=1
						and BV.BregVisionOrderID = @BVOID
					GROUP BY SOLI.SupplierOrderID  ) AS Derived

				WHERE SupplierOrder.SupplierOrderID = Derived.SupplierOrderID
				
				SET @Err = @@ERROR
            END

        IF @Err = 0 
            BEGIN

				update BregVisionOrder
				SET Total = Derived.Total		--  SELECT Derived.Total AS Total
				FROM 
					( SELECT 
							SO.BregVisionOrderID	AS BregVisionOrderID
							, SUM( SO.Total ) AS Total
					FROM BregVisionOrder AS BV
					INNER JOIN SupplierOrder AS SO
						ON BV.BregVisionOrderID = SO.BregVisionOrderID
					
					WHERE BV.isactive=1
						and SO.isactive=1
						and BV.BregVisionOrderID = @BVOID
					GROUP BY SO.BregVisionOrderID  ) AS Derived

				WHERE BregVisionOrder.BregVisionOrderID = Derived.BregVisionOrderID
				
				SET @Err = @@ERROR
            END

        IF @Err = 0 
            BEGIN

				--If all is good, clear the cart
				delete from ShoppingCartItem where ShoppingCartID = @ShoppingCartID
				delete from ShoppingCart where ShoppingCartID = @ShoppingCartID
                       
			End
			SET @Err = @@ERROR	
        IF @@TranCount > @TransactionCountOnEntry 
            BEGIN

                IF @Err = 0 
                    BEGIN 
                        COMMIT TRANSACTION
                        set @PurchaseOrderID = @PONumber
						RETURN @Err
               -- PRINT @err
                    END
                ELSE 
                    BEGIN
                        ROLLBACK TRANSACTION
                        set @PurchaseOrderID = 0
						RETURN @Err
                -- PRINT @err
	--  Add any database logging here      
                    END
            END				
    end			








