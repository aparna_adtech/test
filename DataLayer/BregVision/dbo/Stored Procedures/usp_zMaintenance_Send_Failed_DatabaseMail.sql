﻿CREATE PROC [dbo].[usp_zMaintenance_Send_Failed_DatabaseMail]
AS
begin
----USE msdb
--SELECT mailitem_id, profile_id, recipients, subject, body, body_format, attachment_encoding
--FROM    msdb.dbo.sysmail_faileditems --AS FI
--WHERE   body_format = 'HTML'
--        AND send_request_date > '03/06/2008'
--	    AND recipients NOT like 'orders@southeastmedical.com%'
--ORDER BY recipients			
				  
DECLARE @profile_name NVARCHAR(128)
DECLARE @recipients   VARCHAR(MAX)
DECLARE @blind_copy_recipients VARCHAR(max)
DECLARE @subject      NVARCHAR(255)
DECLARE @body		 VARCHAR(MAX)
DECLARE @body_format  VARCHAR(20)

SET @profile_name = 'OrderConfirmation'
SET @blind_copy_recipients  = 'jbongiorni@breg.com; rhaywood@breg.com'
DECLARE @mailitem_ID INT
DECLARE @getMailitem_ID CURSOR
    SET @getMailitem_ID  = CURSOR FOR


		SELECT mailitem_ID
		FROM    msdb.dbo.sysmail_faileditems --AS FI
		WHERE   body_format = 'HTML'
				AND send_request_date > '03/06/2008'
				AND recipients NOT like 'orders@southeastmedical.com%'

OPEN @getMailitem_ID 
            FETCH NEXT
    FROM @getMailitem_ID INTO @mailitem_ID
WHILE @@FETCH_STATUS = 0
    BEGIN
                SELECT
					--mailitem_ID 
					--, 'OrderConfirmation' AS profile_name
					 @recipients = recipients 
					, @subject = subject
					, @body = body
					, @body_format = body_format					
				FROM    msdb.dbo.sysmail_faileditems --AS FI
				WHERE   body_format = 'HTML'
						AND send_request_date > '03/06/2008'
						AND recipients NOT like 'orders@southeastmedical.com%'
						AND mailitem_id = @mailitem_id
						
				PRINT @mailitem_ID
				PRINT @recipients 
				PRINT @subject 
				PRINT @body
				PRINT @body_format
				PRINT '---'

				EXEC msdb.dbo.sp_send_dbmail 
						@profile_name = @profile_name
						, @recipients = @recipients
						, @blind_copy_recipients  = @blind_copy_recipients  
						, @subject = @subject
						, @body = @body
						, @body_format = @body_format
							
                FETCH NEXT
        FROM @getMailitem_ID INTO @mailitem_ID
    END
            CLOSE @getMailitem_ID
            DEALLOCATE @getMailitem_ID



--
--                DbCommand dbCommand = db.GetStoredProcCommand("msdb.dbo.sp_send_dbmail");
--
--                //DBMail profile name:  Order Confirmation.
--                db.AddInParameter(dbCommand, "profile_name", DbType.String, "OrderConfirmation");
--                db.AddInParameter(dbCommand, "recipients", DbType.String, MailTo);
--                db.AddInParameter(dbCommand, "subject", DbType.String, MailSubject);
--                db.AddInParameter(dbCommand, "body", DbType.String, MailBody);
--                db.AddInParameter(dbCommand, "body_format", DbType.String, "HTML");
--                
                
end