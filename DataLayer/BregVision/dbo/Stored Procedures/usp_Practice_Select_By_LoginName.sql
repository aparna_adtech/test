﻿/* ------------------------------------------------------------
   PROCEDURE:    [dbo].[usp_Practice_Select_By_LoginName]
   
   Description:  Selects the PracticeID and PracticeName from table Practice
				   from a given UserID
   
   AUTHOR:       John Bongiorni 9/29/2007 1:51:38 PM
   
   Modifications:  
   ------------------------------------------------------------ */   ----------------SELECT ----------------

CREATE PROCEDURE [dbo].[usp_Practice_Select_By_LoginName]
(
	  @UserLoginName	VARCHAR(256)
	, @UserID			INT OUTPUT
    , @PracticeID		INT OUTPUT
    , @PracticeName	    VARCHAR(50) OUTPUT
)
As
BEGIN
	DECLARE @Err INT

	SELECT
		  @UserID = U.UserID
		, @PracticeID = P.PracticeID
		, @PracticeName = P.PracticeName

	FROM 
		aspnetdb.dbo.aspnet_Users AS AU
	INNER JOIN 
		aspnetdb.dbo.aspnet_Applications AS AA
			ON AU.ApplicationId = AA.ApplicationId
	INNER JOIN 
		dbo.[User] AS U
			ON AU.UserID = U.AspNet_UserID
	INNER JOIN 
		dbo.Practice AS P
			ON U.PracticeID = P.PracticeID
		
	WHERE 
		AU.LoweredUserName = LOWER(@UserLoginName)
		AND AA.LoweredApplicationName = LOWER('/bregvision')
		AND U.IsActive = 1
		AND P.IsActive = 1
		
	SET @Err = @@ERROR

	RETURN @Err
End