﻿
/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocationBillingAddress_Select_By_Params
   
   Description:  Input: PracticeLocationID
				 Selects a record from table dbo.Address, and also selects the
					PracticeLocationBillingAddress.AttentionOf column
   				     and puts values into parameters
   
   AUTHOR:       John Bongiorni 8/06/2007 7:15PM
   
   Modifications:  
   ------------------------------------------------------------ */  

CREATE PROCEDURE [dbo].[usp_PracticeLocationBillingAddress_Select_By_Params]
(
	  @PracticeLocationID			 INT
	, @AttentionOf					 VARCHAR(50)    OUTPUT
    , @AddressID                     INT			OUTPUT
    , @AddressLine1                  VARCHAR(50)    OUTPUT
    , @AddressLine2                  VARCHAR(50)    OUTPUT
    , @City                          VARCHAR(50)    OUTPUT
    , @State                         CHAR           OUTPUT
    , @ZipCode                       CHAR(5)        OUTPUT
    , @ZipCodePlus4                  CHAR(4)        OUTPUT
)
AS
BEGIN

	DECLARE @Err INT

	SELECT
		  @AttentionOf = PLBA.AttentionOf 
        , @AddressID = A.AddressID
        , @AddressLine1 = A.AddressLine1
        , @AddressLine2 = A.AddressLine2
        , @City = A.City
        , @State = A.State
        , @ZipCode = A.ZipCode
        , @ZipCodePlus4 = A.ZipCodePlus4

	FROM	
			dbo.PracticeLocationBillingAddress AS PLBA
	INNER JOIN	
			dbo.Address AS A
				ON PLBA.AddressID = A.AddressID

	WHERE 
		PLBA.PracticeLocationID = @PracticeLocationID
		AND PLBA.IsActive = 1
		AND A.IsActive = 1

	SET @Err = @@ERROR

	RETURN @Err
End


