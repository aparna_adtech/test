﻿



-- =============================================
-- Author:		Greg Ross
-- Create date: 09/27/07
-- Description:	Used to get the detail for a Practice. Used in the MCToPC.aspx page
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetPracticeCatalogProductInfoMCToPC_2]

	@PracticeID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select
	PCP.PracticeID, 
	PCP.PracticecatalogProductID,
	PCP.MasterCatalogProductID	AS MasterCatalogProductID,  -- Needs to be ProductID
	PCP.PracticeCatalogSupplierBrandID,
	MCP.ShortName,
	MCP.Code,
	PCP.WholesaleCost,
	PCP.BillingCharge,
	PCP.DMEDeposit,
	PCP.BillingChargeCash,
	0 AS IsThirdPartyProduct					-- Needs to know if this is a Third Party Product.
 from PracticeCatalogProduct PCP
inner join MasterCatalogProduct MCP 
	on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
where PCP.practiceID =				4  --@PracticeID
	and PCP.isactive = 1
	and MCP.isactive = 1
	

UNION

	select
	PCP.PracticeID, 
	PCP.PracticecatalogProductID,
	PCP.ThirdPartyProductID		 AS MasterCatalogProductID,  -- Needs to be ProductID
	PCP.PracticeCatalogSupplierBrandID,
	TPP.ShortName,
	TPP.Code,
	PCP.WholesaleCost,
	PCP.BillingCharge,
	PCP.DMEDeposit,
	PCP.BillingChargeCash,
	1 AS IsThirdPartyProduct					-- Needs to know if this is a Third Party Product.
 from PracticeCatalogProduct PCP
inner join ThirdPartyProduct TPP 
	on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID 
where PCP.practiceID =			4   --@PracticeID
	AND PCP.IsThirdPartyProduct = 1
	and PCP.isactive = 1
	and TPP.isactive = 1


END



