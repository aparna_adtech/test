﻿/*

	Get the MonthName with the Year, Given a DateTime.
	Author: John Bongiorni
	Created:  2007.12.09  9:30AM

	Test:  
		EXEC usp_Reports_MonthNameAndYear '1/1/2007' , '12/31/2007'
		--EXEC usp_Reports_MonthNameAndYear GetDate()

*/


CREATE PROC usp_Reports_MonthNameAndYear
	  @StartDate DATETIME
	, @EndDate DATETIME

AS
BEGIN

	SELECT 
		  dbo.MonthNameAndYear(@StartDate) AS StartMonth
		, dbo.MonthNameAndYear(@EndDate) AS EndMonth


END

--  select * from practice
