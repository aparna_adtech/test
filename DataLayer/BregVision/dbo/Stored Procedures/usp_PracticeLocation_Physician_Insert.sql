﻿

CREATE PROCEDURE [dbo].[usp_PracticeLocation_Physician_Insert]
		  @PracticeLocationID INT
		, @PhysicianID		  INT
		, @CreatedUserID	  INT
AS
BEGIN	

		DECLARE @RecordCount INT
		DECLARE @IsActive	BIT

		SELECT @RecordCount = COUNT(1) 
		FROM dbo.PracticeLocation_Physician
		WHERE  PracticeLocationID = @PracticeLocationID
			AND PhysicianID = @PhysicianID


		SELECT @IsActive	 = IsActive
		FROM dbo.PracticeLocation_Physician
		WHERE  PracticeLocationID = @PracticeLocationID
			AND PhysicianID = @PhysicianID


		IF ( @RecordCount = 0 )
		BEGIN
			INSERT INTO dbo.PracticeLocation_Physician 
				(
					  PracticeLocationID 
					, PhysicianID		
					, CreatedUserID
					, CreatedDate
					, IsActive
				)		
			VALUES
				(
					  @PracticeLocationID 
					, @PhysicianID		
					, @CreatedUserID
					, GetDate()
					, 1
				)
		END


		IF ( @RecordCount = 1 AND @IsActive = 0 )
		BEGIN

			UPDATE dbo.PracticeLocation_Physician 
			SET IsActive = 1
			WHERE  PracticeLocationID = @PracticeLocationID
				AND PhysicianID = @PhysicianID

		END			
				
END



