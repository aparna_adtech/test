﻿-- =============================================
-- Author:		Greg Ross
-- Create date: 08/07/08
-- Description:	Used to update inventory levels based stocking units changed
--Simply multiples QOH by stocking units to reflect change in quantity
-- =============================================
CREATE PROCEDURE [dbo].[usp_UpdateInventoryLevels_PracticeCatalogProduct_20110826]
		@practiceCatalogProductID int,
		@PracticeID int,
		@StockingUnits int,
		@UserID int = 1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

update practicecatalogproduct set stockingunits=@StockingUnits, ModifiedUserID = @UserID, ModifiedDate=GETDATE()
 where practiceCatalogProductID =@practiceCatalogProductID


update productinventory 
	set QuantityOnHandPerSystem = (select QuantityOnHandPerSystem * @StockingUnits)
where practiceCatalogProductID=@practiceCatalogProductID
and practicelocationid 
 in (select practicelocationid from practicelocation where practiceid=@PracticeID) 

 
--select * from Productinventory
--where practiceCatalogProductID=@practiceCatalogProductID
--and practicelocationid 
--in (select practicelocationid from practicelocation where practiceid=@PracticeID)



END
