﻿create PROC dbo.usp_zMaintenance_PCSportsMed_ProductCodes_Update_20071127
AS
BEGIN

	SELECT * FROM practice

	SELECT *
	FROM PracticeCatalogSupplierBrand
	WHERE practiceID = 10  --Primary Care Sports Medicine


		SELECT *
		FROM ThirdPartyProduct
		WHERE PracticeCatalogSupplierBrandID IN (92)
		AND CODE IN 
			(
				  '42-171'
				, '42-172'
				, '42-173'
				, '42-174'
				, '42-175'
				, '44-040'
				, '44-041'
				, '44-042'
				, '44-043'
				, '44-044'
				, '44-045'
				, '44-046'	
			)
			
			
			
		--838	92	42-171
		--839	92	42-172
		--840	92	42-173
		--841	92	42-174
		--842	92	42-175
		--843	92	44-040
		--844	92	44-041
		--845	92	44-042
		--846	92	44-043
		--847	92	44-044
		--848	92	44-045
		--849	92	44-046	


		UPDATE ThirdPartyProduct
		SET Code = '42-176-01'
		WHERE PracticeCatalogSupplierBrandID IN (92)
		AND CODE IN 
			(
				  '42-171'
		--		, '42-172'
		--		, '42-173'
		--		, '42-174'
		--		, '42-175'
		--		, '44-040'
		--		, '44-041'
		--		, '44-042'
		--		, '44-043'
		--		, '44-044'
		--		, '44-045'
		--		, '44-046'	
			)
			
		UPDATE ThirdPartyProduct
		SET Code = '42-176-03'
		WHERE PracticeCatalogSupplierBrandID IN (92)
		AND CODE IN 
			(
		'42-173'
		--		, '42-174'
		--		, '42-175'
		--		, '44-040'
		--		, '44-041'
		--		, '44-042'
		--		, '44-043'
		--		, '44-044'
		--		, '44-045'
		--		, '44-046'	
			)	
			
			UPDATE ThirdPartyProduct
		SET Code = '42-176-04'
		WHERE PracticeCatalogSupplierBrandID IN (92)
		AND CODE IN 
			(
				'42-174'
		--		, '42-175'
		--		, '44-040'
		--		, '44-041'
		--		, '44-042'
		--		, '44-043'
		--		, '44-044'
		--		, '44-045'
		--		, '44-046'	
			)	
			
				UPDATE ThirdPartyProduct
		SET Code = '42-176-05'
		WHERE PracticeCatalogSupplierBrandID IN (92)
		AND CODE IN 
			(
				'42-175'
		--		, '44-040'
		--		, '44-041'
		--		, '44-042'
		--		, '44-043'
		--		, '44-044'
		--		, '44-045'
		--		, '44-046'	
			)
			

				UPDATE ThirdPartyProduct
		SET Code = '44-040-00'
		WHERE PracticeCatalogSupplierBrandID IN (92)
		AND CODE IN 
			(
				'44-040'
		--		, '44-041'
		--		, '44-042'
		--		, '44-043'
		--		, '44-044'
		--		, '44-045'
		--		, '44-046'	
			)	
			
				
		UPDATE ThirdPartyProduct
		SET Code = '44-040-01'
		WHERE PracticeCatalogSupplierBrandID IN (92)
		AND CODE IN 
			(
				'44-041'
		--		, '44-041'
		--		, '44-042'
		--		, '44-043'
		--		, '44-044'
		--		, '44-045'
		--		, '44-046'	
			)	
			
		UPDATE ThirdPartyProduct
		SET Code = '44-040-02'
		WHERE PracticeCatalogSupplierBrandID IN (92)
		AND CODE IN 
			(
			 '44-042'
		--		, '44-043'
		--		, '44-044'
		--		, '44-045'
		--		, '44-046'	
			)		
			
			
		UPDATE ThirdPartyProduct
		SET Code = '44-040-03'
		WHERE PracticeCatalogSupplierBrandID IN (92)
		AND CODE IN 
			(
			 '44-043'
		--		, '44-044'
		--		, '44-045'
		--		, '44-046'	
			)		
			
			

		UPDATE ThirdPartyProduct
		SET Code = '44-040-04'
		WHERE PracticeCatalogSupplierBrandID IN (92)
		AND CODE IN 
			(
				'44-044'
		--		, '44-045'
		--		, '44-046'	
			)		
				
				
		UPDATE ThirdPartyProduct
		SET Code = '44-040-05'
		WHERE PracticeCatalogSupplierBrandID IN (92)
		AND CODE IN 
			(
				'44-045'
		--		, '44-046'	
			)		
				

		UPDATE ThirdPartyProduct
		SET Code = '44-040-06'
		WHERE PracticeCatalogSupplierBrandID IN (92)
		AND CODE IN 
			(
				'44-046'	
			)		
				
				
		SELECT *
		FROM ThirdPartyProduct
		WHERE PracticeCatalogSupplierBrandID IN (92)
		AND ThirdPartyProductID >= 838
		AND ThirdPartyProductID <= 849

	SELECT * FROM PracticeCatalogSupplierBrand 
	WHERE PracticeCatalogSupplierBrandID IN (92)
SELECT * FROM Practice
END		