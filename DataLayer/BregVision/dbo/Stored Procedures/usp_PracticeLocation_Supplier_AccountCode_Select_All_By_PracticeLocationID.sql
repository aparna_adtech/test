﻿

/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocation_Supplier_AccountCode_Select_All_By_PracticeLocationID
   
   Description:  Select All records to display in the Practice Location Supplier Account Code page.
				Selects all records from the table dbo.PracticeLocation_Supplier_AccountCode
						for a given practiceLocationID, plus all the potential records (
						which come from the PracticeCatalogSupplierBrand table, see below in code.)
   
   AUTHOR:       John Bongiorni 9/25/2007 3:00:55 PM
   
   Modifications:  Add column for IsThirdPartySupplier in order to order properly
				  JB 20071014
				--  When supplier is a 3rd party supplier vendor with a brand(s) appended the supplier.
				
   ------------------------------------------------------------ */  -------------------SELECT ALL----------------

CREATE PROCEDURE [dbo].[usp_PracticeLocation_Supplier_AccountCode_Select_All_By_PracticeLocationID] 
			@PracticeLocationID INT
AS
BEGIN

	DECLARE @Err INT
	
--	SELECT
--		  PracticeCatalogSupplierBrandID
--		, AccountCode
--		, CreatedUserID
--		, CreatedDate
--		, ModifiedUserID
--		, ModifiedDate
--		, IsActive
--	
--	FROM dbo.PracticeLocation_Supplier_AccountCode
--	WHERE IsActive = 1
--	--ORDER BY
	
	SELECT 
		  Sub.PracticeLocationID
		, Sub.PracticeCatalogSupplierBrandID
		, Sub.SupplierShortName
		, Sub.IsThirdPartySupplier
		, Sub.Sequence
		, Sub.AccountCode
		, Sub.IsAccountCodeInDBYet	AS IsInDB
	
	FROM

		(
			--  1.  list OF all suppliers from practice catalog, exclude those in the pl's supplier codes.'
			--DECLARE @PracticeLocationID INT
			--SET @PracticeLocationID = 3

			SELECT 
				  @PracticeLocationID	AS PracticeLocationID
				, PCSB.PracticeCatalogSupplierBrandID  AS PracticeCatalogSupplierBrandID
				
				--  When supplier is a 3rd party supplier vendor with a brand(s) appended the brand.
				, CASE WHEN (PCSB.IsThirdPartySupplier = 1 )AND (PCSB.SupplierShortName <> PCSB.BrandShortName) 		
							THEN PCSB.SupplierShortName + ' - ' + PCSB.BrandShortName 
							ELSE PCSB.SupplierShortName
						END AS SupplierShortName
							
				
				, PCSB.IsThirdPartySupplier			AS IsThirdPartySupplier
				, ISNULL(PCSB.Sequence, 0)			AS Sequence
				, ''								AS AccountCode
				, 0									AS IsAccountCodeInDBYet  -- AccountCode is in the db.
				
			FROM dbo.PracticeCatalogSupplierBrand AS PCSB  WITH(NOLOCK)
			
			WHERE 
				( 
				  PCSB.MasterCatalogSupplierID IS NOT NULL
				  OR PCSB.ThirdPartySupplierID IS NOT null	
				)  
				 AND PCSB.practiceID = (SELECT PracticeID FROM PracticeLocation WHERE PracticeLocationID = @PracticeLocationID)
				 AND PCSB.IsActive = 1
				 AND PCSB.PracticeCatalogSupplierBrandID NOT IN 
						(SELECT PracticeCatalogSupplierBrandID 
						 FROM dbo.PracticeLocation_Supplier_AccountCode
						 WHERE PracticeLocationID = @PracticeLocationID
								AND IsActive = 1)
								
		  UNION     
		     
		
			SELECT    --  2.  iN THE Practice Location's Supplier Account Codes.
					  @PracticeLocationID	AS PracticeLocationID
					, PLSAC.PracticeCatalogSupplierBrandID	AS PracticeCatalogSupplierBrandID 

				-- jb 20071014 
				--  When supplier is a 3rd party supplier vendor with a brand(s) appended the supplier.
				, CASE WHEN (PCSB.IsThirdPartySupplier = 1 )AND (PCSB.SupplierShortName <> PCSB.BrandShortName) 		
							THEN PCSB.SupplierShortName + ' - ' + PCSB.BrandShortName 
							ELSE PCSB.SupplierShortName
						END AS SupplierShortName

--					, PCSB.SupplierShortName			AS SupplierShortName
					, PCSB.IsThirdPartySupplier			AS IsThirdPartySupplier
					, ISNULL(PCSB.Sequence, 0)			AS Sequence
					, PLSAC.AccountCode					AS AccountCode
					, 1									AS IsAccountCodeInDBYet  -- AccountCode is in the db.
					
			FROM dbo.PracticeLocation_Supplier_AccountCode AS PLSAC WITH(NOLOCK)
			
			INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB  WITH(NOLOCK)
				ON PLSAC.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
			
			WHERE PLSAC.PracticeLocationID = @PracticeLocationID
								AND PLSAC.IsActive = 1
								AND PCSB.IsActive = 1
								
		) AS Sub
	
	ORDER BY 
		Sub.IsThirdPartySupplier
		, Sub.Sequence
		, Sub.SupplierShortName
					
	SET @Err = @@ERROR

	RETURN @Err
END