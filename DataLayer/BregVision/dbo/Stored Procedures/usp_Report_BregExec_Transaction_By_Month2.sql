﻿CREATE PROC usp_Report_BregExec_Transaction_By_Month2
--	  @PracticeID AS INT
--	, @StartDate  AS DateTime
--	, @EndDate    AS DateTime

AS
BEGIN

	DECLARE @IsEmptyRowsWithinSuppressed AS BIT
	SET @IsEmptyRowsWithinSuppressed = 0

	DECLARE @MinimumTransactions AS INT
	SELECT @MinimumTransactions = 	
				(SELECT CASE WHEN @IsEmptyRowsWithinSuppressed = 0 THEN -1 
				WHEN @IsEmptyRowsWithinSuppressed = 1 THEN 0  
				ELSE -1
				END	AS MinimumTransactions)    


	DECLARE @StartDate	AS DateTime
	DECLARE @EndDate	AS DateTime
	DECLARE @StartYear	AS INT 
	DECLARE @EndYear	AS INT 

 
	SET @StartDate = '11/6/2007' 
	SET @EndDate = '12/31/2007'


	SET @StartYear	= Year(@StartDate)
	SET @EndYear	= Year(@EndDate);
	

	DECLARE @TotalQuantityTable TABLE
		(
			  PracticeName		VARCHAR(50)
			, IsPrimaryLocation BIT
			, PracticeLocation  VARCHAR(50)
			, Year				INT
			, Month				INT
			, YYYYMM			CHAR(6)	
			, QuantityDispensed INT
			, QuantityOrdered   INT
			, TotalTransactions INT	
		);
	


	WITH                   Years
			  AS ( SELECT   YYYY = @StartYear
				   UNION ALL
				   SELECT   YYYY + 1
				   FROM     Years
				   Where    YYYY < @EndYear
				 ) ,
			Months
			  AS ( SELECT   MM = 1
				   UNION ALL
				   SELECT   MM + 1
				   FROM     Months
				   WHERE    MM < 12
				 ) 


	--SELECT * FROM Years
	--SELECT * FROM Months, Years

	INSERT INTO @TotalQuantityTable
		(	  PracticeName		
			, IsPrimaryLocation
			, PracticeLocation  
			, Year				
			, Month				
			, YYYYMM			
			, QuantityDispensed 
			, QuantityOrdered   
			, TotalTransactions 	

		)
	SELECT 
		  P.PracticeName
		, PL.IsPrimaryLocation
		, PL.Name AS PracticeLocation
		, YYYY
		, MM
		, Cast(YYYY as char(4)) + RIGHT('0' + CAST (MM as varchar(2)), 2) AS YYYYMM 
		, 0 AS QuantityDispensed 
		, 0 AS QuantityOrdered   
		, 0 AS TotalTransactions 
--		, Cast(YEAR(@StartDate) as char(4)) + RIGHT('0' + CAST (MONTH(@StartDate) as varchar(2)), 2) AS StartYYYYMM
--		, Cast(YEAR(@EndDate) as char(4)) + RIGHT('0' + CAST (MONTH(@EndDate) as varchar(2)), 2) AS EndYYYYMM
	FROM Years, Months 
	CROSS JOIN Practice AS P
	INNER JOIN PracticeLocation AS PL
		ON P.PracticeID = PL.PracticeID
	where Cast(YYYY as char(4)) + RIGHT('0' + CAST (MM as varchar(2)), 2) >= Cast(YEAR(@StartDate) as char(4)) + RIGHT('0' + CAST (MONTH(@StartDate) as varchar(2)), 2) 
	AND Cast(YYYY as char(4)) + RIGHT('0' + CAST (MM as varchar(2)), 2) <= Cast(YEAR(@EndDate) as char(4)) + RIGHT('0' + CAST (MONTH(@EndDate) as varchar(2)), 2) 
	AND P.PracticeID > 1  -- GET RID OF TEST PRACTICE!!!
	ORDER BY PracticeName, YYYYMM, IsPrimaryLocation, PracticeLocation
	

	

	--  Create Table Variable @TransactionTable
	DECLARE @QuantityOrderedTable TABLE
		(
			  PracticeName		VARCHAR(50)
			, PracticeLocation  VARCHAR(50)
			, Year				INT
			, Month				INT
			, QuantityDispensed INT
			, QuantityOrdered   INT
			, TotalTransactions INT	
		)

	DECLARE @QuantityDispensedTable TABLE
		(
			  PracticeName		VARCHAR(50)
			, PracticeLocation  VARCHAR(50)
			, Year				INT
			, Month				INT
			, QuantityDispensed INT
			, QuantityOrdered   INT
			, TotalTransactions INT
		)


	DECLARE @QuantityOrderQuantityDispensedTable TABLE
		(
			  PracticeName		VARCHAR(50)
			, PracticeLocation  VARCHAR(50)
			, Year				INT
			, Month				INT
			, QuantityDispensed INT
			, QuantityOrdered   INT
			, TotalTransactions INT
		)
	

	INSERT INTO @QuantityOrderedTable
	(
			  PracticeName		
			, PracticeLocation  
			, Year				
			, Month				
			, QuantityDispensed
			, QuantityOrdered 
			, TotalTransactions
	)
	SELECT 
		P.PracticeName
		, PL.Name AS PracticeLocation
		, YEAR( SOLI.CreatedDate ) AS Year
		, MONTH( SOLI.CreatedDate )  AS Month
		, 0
		, SUM(SOLI.QuantityOrdered) AS QuantityOrdered
		, 0
	FROM
		Practice AS P
	INNER JOIN 
		PracticeLocation AS PL
			ON P.PracticeID = PL.PracticeID
	INNER JOIN 
		BregVisionOrder AS BVO
			ON PL.PracticeLocationID = BVO.PracticeLocationID
	INNER JOIN 
		SupplierOrder AS SO
			ON BVO.BregVisionOrderID = SO.BregVisionOrderID
	INNER JOIN
		SupplierOrderLineItem AS SOLI 
			ON SO.SupplierOrderID = SOLI.SupplierOrderID
	Group BY
		P.PracticeName
		, PL.Name
		, YEAR( SOLI.CreatedDate ) 
		, MONTH( SOLI.CreatedDate ) 
	ORDER BY
		P.PracticeName
		, PL.Name	
		, YEAR( SOLI.CreatedDate ) 
		, MONTH( SOLI.CreatedDate ) 


	--
	INSERT INTO @QuantityDispensedTable
	(
			  PracticeName		
			, PracticeLocation  
			, Year				
			, Month				
			, QuantityDispensed
			, QuantityOrdered 
			, TotalTransactions	
	)
	SELECT 
		P.PracticeName
		, PL.Name AS PracticeLocation
		, YEAR( D.DateDispensed ) AS Year
		, MONTH( D.DateDispensed )  AS Month
		, SUM(DD.Quantity) AS QuantityDispensed
		, 0
		, 0
	FROM
		Practice AS P
	INNER JOIN 
		PracticeLocation AS PL
			ON P.PracticeID = PL.PracticeID
	INNER JOIN 
		Dispense AS D
			ON PL.PracticeLocationID = D.PracticeLocationID
	INNER JOIN 
		DispenseDetail AS DD
			ON D.DispenseID = DD.DispenseID
	Group BY
		P.PracticeName
		, PL.Name
		, YEAR( D.DateDispensed ) 
		, MONTH( D.DateDispensed )
	ORDER BY
		P.PracticeName
		, PL.Name	
		, YEAR( D.DateDispensed ) 
		, MONTH( D.DateDispensed )  



	INSERT INTO @QuantityOrderQuantityDispensedTable 
		(
			  PracticeName
			, PracticeLocation  
			, Year				
			, Month			
			, QuantityDispensed
			, QuantityOrdered
			, TotalTransactions
		)
	SELECT 
			SUB.PracticeName
			, SUB.PracticeLocation  
			, SUB.Year				
			, SUB.Month			
			, SUM( ISNULL(SUB.QuantityDispensed	, 0)) AS QuantityDispensed
			, SUM( ISNULL(SUB.QuantityOrdered	, 0)) AS QuantityOrdered
			, SUM( ISNULL(SUB.QuantityDispensed	, 0) + ISNULL(SUB.QuantityOrdered	, 0) ) AS TotalTransactions
	FROM
	(
	SELECT * FROM @QuantityDispensedTable
	UNION
	SELECT * FROM @QuantityOrderedTable
	) AS SUB
	GROUP BY SUB.PracticeName
			, SUB.PracticeLocation  
			, SUB.Year				
			, SUB.Month				
		


	--  INSERT INTO

--	SELECT * 
--	FROM @QuantityOrderedTable AS QOT
--	FULL OUTER JOIN @QuantityDispensedTable AS QDT
--		ON QOT.PracticeName = QDT.PracticeName 
--		AND QOT.PracticeLocation = QDT.PracticeLocation
--		AND QOT.Year = QDT.Year
--		AND QOT.Month = QDT.Month
--		
--	--  usp_Report_BregExec_Transaction_By_Month

--	SELECT * 
--FROM @TotalQuantityTable AS TQT
--inner join @QuantityDispensedTable as QDT
--	ON TQT. = QDT. 
--	AND TQT. = Q
--	(
--			  PracticeName		
--			, PracticeLocation  
--			, Year				
--			, Month				
--			, QuantityDispensed
--			, QuantityOrdered 
--			, TotalTransactions	

--  select * from @QuantityOrderedTable
--  select * from @QuantityDispensedTable



	SELECT 
		  SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.Year
		, SUB.Month
		, SUB.QuantityDispensed
		, SUB.QuantityOrdered
		, SUB.TotalTransactions
	FROM
	(
	SELECT 
		 TQT.PracticeName
		, TQT.PracticeLocation
		, TQT.Year
		, TQT.Month
		, ISNULL( QOQDT.QuantityDispensed, 0) AS QuantityDispensed
		, ISNULL( QOQDT.QuantityOrdered, 0)   AS QuantityOrdered
		, ISNULL( QOQDT.TotalTransactions, 0) AS TotalTransactions
	FROM @TotalQuantityTable				AS TQT
	LEFT JOIN @QuantityOrderQuantityDispensedTable   AS QOQDT
		ON TQT.PracticeName = QOQDT.PracticeName
		AND TQT.PracticeLocation = QOQDT.PracticeLocation 
		AND TQT.Year = QOQDT.Year
		AND TQT.Month = QOQDT.Month
	) AS SUB
	WHERE SUB.TotalTransactions > @MinimumTransactions

--  select * from @QuantityOrderQuantityDispensedTable 

--  select * from @TotalQuantityTable



--       usp_Report_BregExec_Transaction_By_Month2


END