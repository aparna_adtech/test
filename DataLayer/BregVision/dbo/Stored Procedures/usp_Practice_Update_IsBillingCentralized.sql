﻿


/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Practice_Update_IsBillingCentralized
   
   Description:  Updates a record in the table Practice.
			Specifically updates the column IsBilling Centralized
   
   AUTHOR:       John Bongiorni 7/30/2007 8:48:21 PM
   
   Modifications:  
   ------------------------------------------------------------ */  

CREATE PROCEDURE [dbo].[usp_Practice_Update_IsBillingCentralized]
(
	  @PracticeID                    INT
	, @IsBillingCentralized          BIT
	, @ModifiedUserID                INT
)
AS
BEGIN

	DECLARE @Err INT

	UPDATE dbo.Practice
	
	SET
		  IsBillingCentralized = @IsBillingCentralized
		, ModifiedUserID = @ModifiedUserID
		, ModifiedDate = GETDATE()
		
	WHERE 
		PracticeID = @PracticeID


	SET @Err = @@ERROR


		-- JB Modified 20070818  to update all practice location billing addresses when a practice billing address is inserted.	
	--  uPDATE ALL Practice Location if Centralized.
	--DECLARE @IsBillingCentralized BIT	
	--SELECT @IsBillingCentralized = IsBillingCentralized FROM dbo.Practice WHERE PracticeID = @PracticeID
	
	--	SELECT TOP 1 * FROM dbo.Practice 
	
	
	IF ( @IsBillingCentralized = 0 )
		begin
			EXEC dbo.usp_PracticeLocationBillingAddresses_Update_From_Decentralized_PracticeBillingAddress
			  @PracticeID = @PracticeID
			, @UserID	= @ModifiedUserID

		end	
	ELSE
		BEGIN
			UPDATE dbo.Address
				SET 
				   ModifiedUserID = @ModifiedUserID
				 , ModifiedDate = GETDATE()
				 , IsActive = 1

			WHERE AddressID = (SELECT TOP 1 A.AddressID FROM dbo.PracticeBillingAddress A WHERE A.PracticeID = @PracticeID and A.IsActive = 1)
        END

	RETURN @Err
	
End


