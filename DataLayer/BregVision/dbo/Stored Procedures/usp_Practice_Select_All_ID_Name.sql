﻿CREATE PROC [dbo].[usp_Practice_Select_All_ID_Name]
AS
BEGIN

	SELECT 
	    P.PracticeID
	  , P.PracticeName
	FROM dbo.Practice AS P
	WHERE P.IsActive = 1
	--AND P.PracticeName NOT Like '%Test%'
	ORDER BY
		P.PracticeName
	
		
END