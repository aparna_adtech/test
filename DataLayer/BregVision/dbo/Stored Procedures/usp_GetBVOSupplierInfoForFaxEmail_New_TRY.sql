﻿/*
	Get Breg Vision Order Info per Supplier with 3rd Party Vendors consolidated by Brand
	2008.01.22  John Bongiorni
	Consolidate the Vendor Brands
	
	Important:  The SupplierOrderID for 3rd Party Vendors is representative of all
				the supplierOrderIDs for the given vendor for the BregVisionOrder.
				This is being used as a work-around.
				The SupplierOrderID for Suppliers (MC and ThirdParty (NonVendor) is accurate.


	Documentation of the CustomPOs:  _______

	2008.04.27 JB split up by vendor.
*/

CREATE PROCEDURE [dbo].[usp_GetBVOSupplierInfoForFaxEmail_New_TRY] --814 --385  --166
			@BregVisionOrderID INT
AS
BEGIN

--		DECLARE @BregVisionOrderID INT
		DECLARE @PracticeID INT

		--SET @BregVisionOrderID = 166
		SELECT  @PracticeID = 
		(
			SELECT PL.PracticeID 
			FROM BregVisionOrder AS BVO		WITH (NOLOCK)
			INNER JOIN PracticeLocation AS PL	WITH (NOLOCK)
				ON BVO.PracticeLocationID = PL.PracticeLocationID
			WHERE BVO.BregVisionOrderID = @BregVisionOrderID 
		)
		
----  SELECT * FROM practice WHERE practiceID = 11
--
--
----SELECT PracticeID FROM PracticeLocation WHERE PracticeLocationID = 39
--SELECT *
--FROM PracticeCatalogSupplierBrand
--WHERE PracticeID = 11
--ORDER BY IsThirdPartySupplier DESC, SupplierName
--
--WHERE BrandShortName = 'OPS'
--SELECT * 
--FROM ThirdPartySupplier 
--WHERE ThirdPartySupplierID = 27


			DECLARE @UniquePracticeSuppliers AS TABLE
				(     PracticeID					 INT
					, PracticeCatalogSupplierBrandID INT
					, IsThirdPartySupplier			 BIT 
					, SupplierID					 INT
					, IsVendor						 BIT
				)

			INSERT INTO @UniquePracticeSuppliers 
				(     
					  PracticeID					 
					, PracticeCatalogSupplierBrandID 
					, IsThirdPartySupplier			 
					, SupplierID					 
					, IsVendor						 
				)
		--  Get the MCSuppliers
			SELECT
				  PCSB.PracticeID
				, PCSB.PracticeCatalogSupplierBrandID
				, 0								AS IsThirdPartySupplier
				, PCSB.MasterCatalogSupplierID	AS SupplierID
				, 0								AS IsVendor
				
			FROM PracticeCatalogSupplierBrand AS PCSB WITH (NOLOCK)
			INNER JOIN MasterCatalogSupplier AS MCS WITH (NOLOCK)
				ON PCSB.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
				AND PCSB.PracticeID = @PracticeID
				AND MCS.IsActive = 1
				AND PCSB.IsActive = 1
		UNION
		--  Get the 3rd Party Suppliers that are Mfgs NOT Vendors.
			SELECT 
				  PCSB.PracticeID
				, PCSB.PracticeCatalogSupplierBrandID
				, 1								AS IsThirdPartySupplier
				, PCSB.ThirdPartySupplierID		AS SupplierID
				, 0								AS IsVendor
				
			FROM PracticeCatalogSupplierBrand AS PCSB WITH (NOLOCK)
			INNER JOIN ThirdPartySupplier AS TPS WITH (NOLOCK)
				ON PCSB.ThirdPartySupplierID = TPS.ThirdPartySupplierID
				AND PCSB.PracticeID = @PracticeID
				AND TPS.IsVendor = 0
				AND TPS.IsActive = 1
				AND PCSB.IsActive = 1
				


		UNION
			-- Get the first (top 1) pcSupplierBrand for the given vendor	


			SELECT DISTINCT

				  Vendor.PracticeID
				, MIN(PCSB2.PracticeCatalogSupplierBrandID) AS PracticeCatalogSupplierBrandID
				, 1 AS IsThirdPartySupplier
				, Vendor.ThirdPartySupplierID  AS SupplierID
				, 1							   AS IsVendor
				
			FROM 
				(SELECT DISTINCT

				  PCSB.PracticeID
				, PCSB.IsThirdPartySupplier
				, PCSB.ThirdPartySupplierID
				, TPS.IsVendor					AS IsVendor
				
			FROM PracticeCatalogSupplierBrand AS PCSB WITH (NOLOCK)
			INNER JOIN ThirdPartySupplier AS TPS WITH (NOLOCK)
				ON PCSB.ThirdPartySupplierID = TPS.ThirdPartySupplierID
				AND TPS.IsVendor = 1
				AND PCSB.PracticeID = @PracticeID
				AND PCSB.IsThirdPartySupplier = 1
				AND TPS.IsActive = 1
				AND PCSB.IsActive = 1
			) AS Vendor	
			INNER JOIN PracticeCatalogSupplierBrand AS PCSB2
				ON  Vendor.PracticeID = PCSB2.PracticeID
				AND Vendor.ThirdPartySupplierID = PCSB2.ThirdPartySupplierID
				AND PCSB2.IsActive = 1
			GROUP BY
				  Vendor.PracticeID
				, Vendor.ThirdPartySupplierID
				, Vendor.IsThirdPartySupplier
				, Vendor.IsVendor
				
--SELECT * 
--FROM @UniquePracticeSuppliers				
				
				
			DECLARE @VendorPCSBIDs AS TABLE
				(    ThirdPartySupplierID INT
				   , PracticeCatalogSupplierBrandID INT
				)
					
			INSERT INTO @VendorPCSBIDs		
				(
				    ThirdPartySupplierID
				  , PracticeCatalogSupplierBrandID
				)
			SELECT --  ALL PCSBIDs of the Vendor for the given PCSBID
				     PCSB.ThirdPartySupplierID
				   , PCSB.PracticeCatalogSupplierBrandID
			FROM @UniquePracticeSuppliers AS UPS		
			INNER JOIN PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
				ON UPS.PracticeID = PCSB.PracticeID
				AND UPS.IsThirdPartySupplier = PCSB.IsThirdPartySupplier
				AND UPS.SupplierID = PCSB.ThirdPartySupplierID
				AND PCSB.IsActive = 1
			WHERE UPS.IsVendor = 1	
				AND UPS.IsThirdPartySupplier = 1	
				AND PCSB.IsActive = 1	

--			SELECT ThirdPartySupplierID, PracticeCatalogSupplierBrandID 
--			FROM @VendorPCSBIDs
--			ORDER BY ThirdPartySupplierID, PracticeCatalogSupplierBrandID 

			--  Get Supplier (NonVendor) Order Info.	
			select distinct 
				BVO.PurchaseOrder,
				BVO.CustomPurchaseOrderCode,   -- GR custom PO
				-1 AS ThirdPartySupplierID,	   ----JB 2008.04.27 split by vendor.  ONLY FOR USE below.
				PL.PracticeLocationID,			
				PCSB.SupplierName,
				ST.ShippingTypeName,
				SO.Total,
				BVO.CreatedDate,
				SO.SupplierOrderID,
				C.EmailAddress,
				C.Fax,
				ISNULL(PLSAC.AccountCode, '') AS AccountCode,
				0 AS IsVendor
				
			 from BregVisionOrder BVO					WITH (NOLOCK)
				inner join SupplierOrder SO				WITH (NOLOCK)
					on BVO.BregVisionOrderID = SO.BregVisionOrderID
				inner join PracticeCatalogSupplierBrand PCSB		WITH (NOLOCK)
					on SO.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
				INNER JOIN @UniquePracticeSuppliers AS UPS			
					ON 	PCSB.PracticeCatalogSupplierBrandID = UPS.PracticeCatalogSupplierBrandID
					AND PCSB.PracticeID = PCSB.PracticeID
					AND UPS.IsVendor = 0  -- All the MC and 3rd Suppliers except 3rd Vendors.
				inner join ShippingType ST						WITH (NOLOCK)
					on BVO.ShippingTypeID = ST.ShippingTypeID
				inner join PracticeLocation PL			WITH (NOLOCK)
					on 	BVO.PracticeLocationID = PL.PracticeLocationID
				left join Contact C						WITH (NOLOCK)
					on PL.ContactID = C.ContactID
					AND C.IsActive = 1	
				left join PracticeLocation_supplier_AccountCode PLSAC	WITH (NOLOCK)
					on PCSB.PracticeCatalogSupplierBrandID = PLSAC.PracticeCatalogSupplierBrandID
					AND PL.PracticeLocationID = PLSAC.PracticeLocationID
					AND PLSAC.IsActive = 1
			where BVO.isactive=1
				and SO.isactive=1
				and PCSB.isactive=1
				and BVO.BregVisionOrderID = @BregVisionOrderID	
				
		UNION

			--  Get Supplier (Vendor) Order (Brands are consolidated) Info.
			select distinct 
				BVO.PurchaseOrder,
				BVO.CustomPurchaseOrderCode,  --  GR Custom Purchase Order Code
				VPCSBs.ThirdPartySupplierID,		--JB 2008.04.27 split by vendor.
				PL.PracticeLocationID,
				MIN(PCSB.SupplierName) AS SupplierName,
				ST.ShippingTypeName,
				SUM(SO.Total)			AS Total,
				MAX(BVO.CreatedDate)	AS CreateDate,
				MIN(SO.SupplierOrderID) AS SupplierOrderID,
				MAX(C.EmailAddress)		AS EmailAddress,
				MAX(C.Fax)				AS Fax,
				MAX(ISNULL(PLSAC.AccountCode, '')) AS AccountCode,
				1 AS IsVendor
				
			 from BregVisionOrder BVO WITH (NOLOCK)
				inner join SupplierOrder SO WITH (NOLOCK)
					on BVO.BregVisionOrderID = SO.BregVisionOrderID
				inner join PracticeCatalogSupplierBrand PCSB WITH (NOLOCK)
					on SO.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
				INNER JOIN @VendorPCSBIDs AS VPCSBs 
					ON 	PCSB.PracticeCatalogSupplierBrandID = VPCSBs.PracticeCatalogSupplierBrandID
					AND PCSB.ThirdPartySupplierID = VPCSBs.ThirdPartySupplierID					
				inner join ShippingType ST WITH (NOLOCK) 
					on BVO.ShippingTypeID = ST.ShippingTypeID
				inner join PracticeLocation PL WITH (NOLOCK) 
					on 	BVO.PracticeLocationID = PL.PracticeLocationID
				left join Contact C WITH (NOLOCK) 
					on PL.ContactID = C.ContactID
					AND C.IsActive = 1
				left join PracticeLocation_supplier_AccountCode PLSAC WITH (NOLOCK)
					on PCSB.PracticeCatalogSupplierBrandID = PLSAC.PracticeCatalogSupplierBrandID
					AND PL.PracticeLocationID = PLSAC.PracticeLocationID
					AND PLSAC.IsActive = 1
			where BVO.isactive=1
				and SO.isactive=1
				and PCSB.isactive=1
				and BVO.BregVisionOrderID = @BregVisionOrderID	
			GROUP BY
				BVO.PurchaseOrder,
				BVO.CustomPurchaseOrderCode,	--  GR  Custom PurchaseOrder Code.
				PL.PracticeLocationID,
				VPCSBs.ThirdPartySupplierID,     --JB 2008.04.27 split by vendor.
				ST.ShippingTypeName

				

-- Old Query...
--		select distinct 
--			BVO.PurchaseOrder,
--			PL.PracticeLocationID,
--			PCSB.SupplierName,
--			PCSB.BrandShortName,
--			ST.ShippingTypeName,
--			SO.Total,
--			BVO.CreatedDate,
--			SO.SupplierOrderID,
--			C.EmailAddress,
--			C.Fax,
--			ISNULL(PLSAC.AccountCode, '') AS AccountCode
--			--MCS.FaxOrderPlacement as FAXNumber,
--			--MCS.EmailOrderPlacement as Email
--			
--		 from BregVisionOrder BVO WITH (NOLOCK) 
--			inner join SupplierOrder SO WITH (NOLOCK) 
--				on BVO.BregVisionOrderID = SO.BregVisionOrderID
--			inner join PracticeCatalogSupplierBrand PCSB
--				on SO.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
--			inner join ShippingType ST WITH (NOLOCK) 
--				on BVO.ShippingTypeID = ST.ShippingTypeID
--			--inner join MasterCatalogSupplier MCS
--			--	on PCSB.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
--			inner join PracticeLocation PL WITH (NOLOCK) 
--				on 	BVO.PracticeLocationID = PL.PracticeLocationID
--			left join Contact C WITH (NOLOCK) 
--				on PL.ContactID = C.ContactID
--				AND C.IsActive = 1
--			left join PracticeLocation_supplier_AccountCode PLSAC WITH (NOLOCK)
--				--on PL.PracticeLocationID = PLSAC.PracticeLocationID
--			--left join PracticeLocation_supplier_AccountCode PLSAC 
--				on PCSB.PracticeCatalogSupplierBrandID = PLSAC.PracticeCatalogSupplierBrandID
--				AND PL.PracticeLocationID = PLSAC.PracticeLocationID
--				AND PLSAC.IsActive = 1
--		where BVO.isactive=1
--			and SO.isactive=1
--			and PCSB.isactive=1
--			and BVO.BregVisionOrderID=@BregVisionOrderID
			
END			