﻿CREATE PROC dbo.usp_BregVisionOrder_Select_UniqueSuppliers____NotYetImplemented3
AS
BEGIN

DECLARE @BregVisionOrderID INT
DECLARE @PracticeID INT

SET @BregVisionOrderID = 166
SET @PracticeID = 6


select distinct 
	BVO.PurchaseOrder,
	PL.PracticeLocationID,
	
	PCSB.SupplierName,  -- Change to SupplierName
	ST.ShippingTypeName,  
	SO.Total,			  -- Change to SUM(SO.Total)
	BVO.CreatedDate,
	SO.SupplierOrderID,		-- Change to Minimun SupplierOrderID
	C.EmailAddress,
	C.Fax,
	ISNULL(PLSAC.AccountCode, '') AS AccountCode
	--MCS.FaxOrderPlacement as FAXNumber,
	--MCS.EmailOrderPlacement as Email
	
 from BregVisionOrder BVO 
	inner join SupplierOrder SO 
		on BVO.BregVisionOrderID = SO.BregVisionOrderID
	inner join PracticeCatalogSupplierBrand PCSB
		on SO.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
	inner join ShippingType ST 
		on BVO.ShippingTypeID = ST.ShippingTypeID
	--inner join MasterCatalogSupplier MCS
	--	on PCSB.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
	inner join PracticeLocation PL 
		on 	BVO.PracticeLocationID = PL.PracticeLocationID
	left join Contact C 
		on PL.ContactID = C.ContactID
		AND C.IsActive = 1
	left join PracticeLocation_supplier_AccountCode PLSAC
		--on PL.PracticeLocationID = PLSAC.PracticeLocationID
	--left join PracticeLocation_supplier_AccountCode PLSAC 
		on PCSB.PracticeCatalogSupplierBrandID = PLSAC.PracticeCatalogSupplierBrandID
		AND PL.PracticeLocationID = PLSAC.PracticeLocationID
		AND PLSAC.IsActive = 1
where BVO.isactive=1
	and SO.isactive=1
	and PCSB.isactive=1
	and BVO.BregVisionOrderID=@BregVisionOrderID
	
	
	

	--  Get all MC Suppliers, 3rd Party Suppliers, and 3rd Party Vendors.
	--  given the PCSBID.

	--WITH Supplier 
	--SELECT 

	SELECT 

		  PCSB.PracticeID
		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierName
		, PCSB.IsThirdPartySupplier
		, PCSB.MasterCatalogSupplierID
		, PCSB.ThirdPartySupplierID
		, TPS.IsVendor
		
	FROM PracticeCatalogSupplierBrand AS PCSB
	LEFT JOIN MasterCatalogSupplier AS MCS
		ON PCSB.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
	LEFT JOIN ThirdPartySupplier AS TPS
		ON PCSB.ThirdPartySupplierID = TPS.ThirdPartySupplierID
	ORDER BY PCSB.PracticeID



	SELECT 

		  PCSB.PracticeID
		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierName
		, PCSB.IsThirdPartySupplier
		, PCSB.MasterCatalogSupplierID AS SupplierID
	--	, PCSB.ThirdPartySupplierID
	--	, TPS.IsVendor
		, 0							   AS IsVendor
		
	FROM PracticeCatalogSupplierBrand AS PCSB
	INNER JOIN MasterCatalogSupplier AS MCS
		ON PCSB.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
		

	SELECT 

		  PCSB.PracticeID
		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierName
		, PCSB.IsThirdPartySupplier
	--	, PCSB.MasterCatalogSupplierID AS SupplierID
		, PCSB.ThirdPartySupplierID
		, TPS.IsVendor					AS IsVendor
		
	FROM PracticeCatalogSupplierBrand AS PCSB
	INNER JOIN ThirdPartySupplier AS TPS
		ON PCSB.ThirdPartySupplierID = TPS.ThirdPartySupplierID
		AND IsVendor = 0
		
	-- Get the first (top 1) pcSupplierBrand for the given vendor	
	SELECT 

		  PCSB.PracticeID
		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierName
		, PCSB.IsThirdPartySupplier
	--	, PCSB.MasterCatalogSupplierID AS SupplierID
		, PCSB.ThirdPartySupplierID
		, TPS.IsVendor					AS IsVendor
		
	FROM PracticeCatalogSupplierBrand AS PCSB
	INNER JOIN ThirdPartySupplier AS TPS
		ON PCSB.ThirdPartySupplierID = TPS.ThirdPartySupplierID
		AND TPS.IsVendor = 1
		AND PCSB.PracticeID = 6
		

	-- Get the first (top 1) pcSupplierBrand for the given vendor	


	SELECT DISTINCT

		  Vendor.PracticeID
		, MIN(PCSB2.PracticeCatalogSupplierBrandID) AS PracticeCatalogSupplierBrandID
		, 1 AS IsThirdPartySupplier
		, Vendor.ThirdPartySupplierID  AS SupplierID
		, 1							   AS IsVendor
		
	FROM 
		(SELECT DISTINCT

		  PCSB.PracticeID
		, PCSB.IsThirdPartySupplier
		, PCSB.ThirdPartySupplierID
		, TPS.IsVendor					AS IsVendor
		
	FROM PracticeCatalogSupplierBrand AS PCSB
	INNER JOIN ThirdPartySupplier AS TPS
		ON PCSB.ThirdPartySupplierID = TPS.ThirdPartySupplierID
		AND TPS.IsVendor = 1
		AND PCSB.PracticeID = 6
		AND PCSB.IsThirdPartySupplier = 1
		AND TPS.IsActive = 1
		AND PCSB.IsActive = 1
	) AS Vendor	
	INNER JOIN PracticeCatalogSupplierBrand AS PCSB2
		ON  Vendor.PracticeID = PCSB2.PracticeID
		AND Vendor.ThirdPartySupplierID = PCSB2.ThirdPartySupplierID
		AND PCSB2.IsActive = 1
	GROUP BY
		  Vendor.PracticeID
		, Vendor.ThirdPartySupplierID
		, Vendor.IsThirdPartySupplier
		, Vendor.IsVendor
		
		
END