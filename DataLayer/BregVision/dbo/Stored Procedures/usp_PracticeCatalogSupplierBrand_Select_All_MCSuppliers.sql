﻿/* ------------------------------------------------------------
   PROCEDURE:    [dbo].[usp_PracticeCatalogSupplierBrand_Select_All_MCSuppliers]
   
   Description:  Selects all records from the table dbo.PracticeCatalogSupplierBrand
   
   AUTHOR:       John Bongiorni 10/9/2007 6:27:46 PM
   
   Modifications:  
   ------------------------------------------------------------ */  -------------------SELECT ALL----------------

CREATE PROCEDURE [dbo].[usp_PracticeCatalogSupplierBrand_Select_All_MCSuppliers]
			@PracticeID INT

AS
BEGIN
	DECLARE @Err INT
	
	SELECT
--		  PracticeCatalogSupplierBrandID
--		, PracticeID
--		, MasterCatalogSupplierID
--		, ThirdPartySupplierID
		  PCSB.SupplierName
		, PCSB.SupplierShortName
		
		, CASE MCS.IsEmailOrderPlacement 
					WHEN 0 THEN 'Fax' 
					WHEN 1 THEN 'Email' 
					ELSE '' END AS OrderPlacementType
		
		, CASE MCS.IsEmailOrderPlacement 
					WHEN 0 THEN FaxOrderPlacement 
					WHEN 1 THEN EmailOrderPlacement 
					ELSE '' END AS OrderPlacementFaxEmail
		
		, PCSB.Sequence
	
	FROM dbo.PracticeCatalogSupplierBrand AS PCSB
	
	INNER JOIN MasterCatalogSupplier AS MCS
		ON PCSB.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
	
	WHERE 
			PCSB.PracticeID = @PracticeID
		AND PCSB.IsThirdPartySupplier = 0
		AND PCSB.IsActive = 1
		AND MCS.IsActive = 1
		
	ORDER BY
		PCSB.Sequence
	
	SET @Err = @@ERROR

	RETURN @Err
END
