﻿
/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Practice_Select_IsBillingCentralized_By_PracticeLocationID_By_Params
   
   Description: Given a practiceLocationID this checks the location's Practice
				and retrieves the Practice.IsBillingCentralized column
				Selects the bool column IsBillingCentralized from table dbo.Practice
   				   and puts value into a parameter.
   
   AUTHOR:       John Bongiorni 8/21/2007 12:15 PM
   
   Modifications:  
   ------------------------------------------------------------ */  

CREATE PROCEDURE [dbo].[usp_Practice_Select_IsBillingCentralized_By_PracticeLocationID_By_Params]
				
(
      @PracticeLocationID				INT
    , @IsBillingCentralized				BIT            OUTPUT
)
AS
BEGIN

	DECLARE @Err INT

	SELECT
		@IsBillingCentralized = IsBillingCentralized        
	FROM dbo.Practice AS P
	INNER JOIN dbo.PracticeLocation AS PL
		ON P.PracticeID = PL.PracticeID
	WHERE 
		PracticeLocationID = @PracticeLocationID
		

	SET @Err = @@ERROR

	RETURN @Err

End


