﻿/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_ThirdPartySupplier_Update
   
   Description:  Updates a record in the table ThirdPartySupplier
   
   AUTHOR:       John Bongiorni 9/27/2007 5:32:01 PM
   
   Modifications:  20071011 JB    
						When a vendor is a vendor and has active brands and the suppliername or suppliershortname is updated
						then Update all Vendor brands to the new suppliername and suppliershortname.  
   ------------------------------------------------------------ */  ------------------ UPDATE -------------------

CREATE PROCEDURE [dbo].[usp_ThirdPartySupplier_Update]
(
	  @ThirdPartySupplierID          INT
	, @PracticeID                    INT
--	, @ContactID                     INT
--	, @AddressID                     INT
	, @SupplierName                  VARCHAR(50)
	, @SupplierShortName             VARCHAR(50)
	, @FaxOrderPlacement             VARCHAR(50)
	, @EmailOrderPlacement           VARCHAR(100)
	, @IsEmailOrderPlacement         BIT
	, @IsVendor                      BIT
--	, @Sequence                      INT
	, @ModifiedUserID                INT
)
AS
BEGIN
	DECLARE @Err INT
	DECLARE @wasVendor BIT
	DECLARE @Case INT
	SET @Case = 0         -- 0 = No change of supplierType, 1 = change from mfg to vendor, 2 = change from vendor to mfg.
	DECLARE @PracticeCatalogSupplierBrandID INT
	
	--  get vendor/manufacturer type prior to update
	SELECT @wasVendor = IsVendor FROM ThirdPartySupplier
	WHERE ThirdPartySupplierID = @ThirdPartySupplierID
		
	
	--  if has changed from mfg to vendor then pcsb record need to be set to inactive.		
	IF ( (@wasVendor = 0) AND (@IsVendor = 1) )	
	BEGIN
	
		SET @Case = 1	  -- Trying to change supplierType from MFG to Vendor.
	
		UPDATE dbo.PracticeCatalogSupplierBrand 
		SET IsActive = 0
			, ModifiedUserID = @ModifiedUserID
			, ModifiedDate = GETDATE()
		WHERE PracticeID = @PracticeID
		AND ThirdPartySupplierID = @ThirdPartySupplierID	
		
		
	END	
	
	
-- if change from vendor to mfg then check number of vendor brands, if any do not allow the update to
--  the isvendor column.
DECLARE @activeBrandCount INT
SET @activeBrandCount  = 0

		IF ( ( @wasVendor = 1 ) AND ( @IsVendor = 0 ) )	
		BEGIN

			SET @Case = 2  -- Trying to change supplierType from Vendor to MFG.
			
			SELECT @activeBrandCount  = COUNT(1)
			FROM dbo.PracticeCatalogSupplierBrand  
			WHERE ThirdPartySupplierID = @ThirdPartySupplierID
				AND IsActive = 1				

			
		END	


-- if vendor name and short name are changed and their are active brands under the vendor, then update all the brands with the new
--		suppliername and shortsuppliername.
DECLARE @activeVendorBrandCount INT
SET @activeVendorBrandCount  = 0

		IF ( ( @wasVendor = 1 ) AND ( @IsVendor = 1 ) )	
		BEGIN

			SET @Case = 0  -- Trying NOT trying to change supplierType 
			
			SELECT @activeVendorBrandCount = COUNT(1)
			FROM dbo.PracticeCatalogSupplierBrand  
			WHERE ThirdPartySupplierID = @ThirdPartySupplierID
				AND IsActive = 1				

			
		END	
 
DECLARE @activeManufacturerBrandCount INT
SET @activeManufacturerBrandCount = 0
 
		IF ( ( @wasVendor = 0 ) AND ( @IsVendor = 0 ) )	
		BEGIN

			SET @Case = 0  -- Trying NOT trying to change supplierType 
			
			SELECT @activeManufacturerBrandCount = COUNT(1)
			FROM dbo.PracticeCatalogSupplierBrand  
			WHERE ThirdPartySupplierID = @ThirdPartySupplierID
				AND IsActive = 1							
		END	




		IF 	(@Case = 1)    -- Update to Vendor because Manufacturer has been deactived,
						--  dO NOT Insert into pcSB BECAUSE there is no brands for the vendor. 
		  BEGIN
				UPDATE dbo.ThirdPartySupplier
				SET
		--		  PracticeID = @PracticeID
		--		, ContactID = @ContactID
		--		, AddressID = @AddressID
				  SupplierName = @SupplierName
				, SupplierShortName = @SupplierShortName
				, FaxOrderPlacement = @FaxOrderPlacement
				, EmailOrderPlacement = @EmailOrderPlacement
				, IsEmailOrderPlacement = @IsEmailOrderPlacement
				, IsVendor = @IsVendor
		--		, Sequence = @Sequence
				, ModifiedUserID = @ModifiedUserID
				, ModifiedDate = GETDATE()
			WHERE 
				ThirdPartySupplierID = @ThirdPartySupplierID	
			END
		
		IF 	(@Case = 0)    -- Update info except for IsVendor, since vendor has not changed.
						
		  BEGIN
				UPDATE dbo.ThirdPartySupplier
				SET
		--		  PracticeID = @PracticeID
		--		, ContactID = @ContactID
		--		, AddressID = @AddressID
				  SupplierName = @SupplierName
				, SupplierShortName = @SupplierShortName
				, FaxOrderPlacement = @FaxOrderPlacement
				, EmailOrderPlacement = @EmailOrderPlacement
				, IsEmailOrderPlacement = @IsEmailOrderPlacement
		--		, IsVendor = @IsVendor
		--		, Sequence = @Sequence
				, ModifiedUserID = @ModifiedUserID
				, ModifiedDate = GETDATE()
			WHERE 
				ThirdPartySupplierID = @ThirdPartySupplierID
				
			
			-- Update all Vendor brands to the new suppliername and suppliershortname	
			IF ( @activeVendorBrandCount > 0 )
			BEGIN
				
				-- Update all Vendor brands to the new suppliername and suppliershortname
				
				UPDATE dbo.PracticeCatalogSupplierBrand  
				SET 
					  SupplierName = @SupplierName
					, SupplierShortName = @SupplierShortName
					, ModifiedUserID = @ModifiedUserID
					, ModifiedDate = GETDATE()
				WHERE ThirdPartySupplierID = @ThirdPartySupplierID
				AND IsActive = 1
				
			END	
				
			-- Update Manufacturer SupplierName, SupplierShortName, BrandName, and BrandShortName
			--	to the new suppliername and suppliershortname	
			IF ( @activeManufacturerBrandCount > 0 )
			BEGIN
				
				-- Update the Manufacture's SupplierName, SupplierShortName, BrandName, and BrandShortName
				--		to the new suppliername and suppliershortname
				
				UPDATE dbo.PracticeCatalogSupplierBrand  
				SET 
					  SupplierName = @SupplierName
					, SupplierShortName = @SupplierShortName
					, BrandName = @SupplierName
					, BrandShortName = @SupplierShortName
					, ModifiedUserID = @ModifiedUserID
					, ModifiedDate = GETDATE()
				WHERE ThirdPartySupplierID = @ThirdPartySupplierID
				AND IsActive = 1
				
			END	
					
			-- @activeManufacturerBrandCount
		END

-- if no active brands for the vendor then update 3rd and insert to sb.		
	
	IF 	( ( @activeBrandCount  > 0 ) AND (@Case = 2) )   -- Do Not Update the Vendor to a MFG because the Vendor has Brands.
	BEGIN

		UPDATE dbo.ThirdPartySupplier
		SET
	--		  PracticeID = @PracticeID
	--		, ContactID = @ContactID
	--		, AddressID = @AddressID
			 SupplierName = @SupplierName
			, SupplierShortName = @SupplierShortName
			, FaxOrderPlacement = @FaxOrderPlacement
			, EmailOrderPlacement = @EmailOrderPlacement
			, IsEmailOrderPlacement = @IsEmailOrderPlacement
	--		, IsVendor = @IsVendor
	--		, Sequence = @Sequence
			, ModifiedUserID = @ModifiedUserID
			, ModifiedDate = GETDATE()
		WHERE 
			ThirdPartySupplierID = @ThirdPartySupplierID
	END

	  IF 	( ( @activeBrandCount  = 0 ) AND (@Case = 2) )   -- Update to MFG because vendor has no active brands, Insert into pcSB
	  BEGIN
			UPDATE dbo.ThirdPartySupplier
			SET
	--		  PracticeID = @PracticeID
	--		, ContactID = @ContactID
	--		, AddressID = @AddressID
			 SupplierName = @SupplierName
			, SupplierShortName = @SupplierShortName
			, FaxOrderPlacement = @FaxOrderPlacement
			, EmailOrderPlacement = @EmailOrderPlacement
			, IsEmailOrderPlacement = @IsEmailOrderPlacement
			, IsVendor = @IsVendor
	--		, Sequence = @Sequence
			, ModifiedUserID = @ModifiedUserID
			, ModifiedDate = GETDATE()
		WHERE 
			ThirdPartySupplierID = @ThirdPartySupplierID	



		EXEC	[dbo].[usp_PracticeCatalogSupplierBrand_Insert_Mfg]
				@PracticeID = @PracticeID ,
				@ThirdPartySupplierID = @ThirdPartySupplierID,
				@UserID = @ModifiedUserID,
				@PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID OUTPUT


		SELECT @PracticeCatalogSupplierBrandID  = SCOPE_IDENTITY()

		END 
		




	SET @Err = @@ERROR

	RETURN @Err
End