﻿
/* ------------------------------------------------------------
   EXEC usp_PracticeLocation_Select_Permissions 1, 0
   EXEC usp_PracticeLocation_Select_Permissions 1, 23
   PROCEDURE:    dbo.usp_PracticeLocation_Select_Permissions
   
   Description:  Selects all PracticeLocation records for a given Practice.
				Select all records from the table dbo.PracticeLocation
				given the PracticeID.
   
   AUTHOR:       Michael Sneen 4/2/2009  
   
 
   ------------------------------------------------------------ */  

Create PROCEDURE [dbo].[usp_PracticeLocation_Select_Permissions] 
			( @PracticeID INT,
			  @UserID INT )
AS 
    BEGIN
        DECLARE @Err INT
        
SELECT
	        
				PL.PracticeLocationID
			  , PL.PracticeID
			  , PL.Name
			  , PL.IsPrimaryLocation
			  , AllowAccess= ISNULL((Select AllowAccess 
									 from User_PracticeLocation_Permissions UPP  WITH(NOLOCK)
									 where UPP.UserID = @UserID 
										   AND UPP.PracticeLocationID=PL.PracticeLocationID 
										   AND UPP.IsActive = 1 )
									, 1)
	        
			FROM
				dbo.PracticeLocation AS PL WITH(NOLOCK)
	        
			WHERE
				PL.IsActive = 1
				AND PL.PracticeID = @PracticeID
	        
			ORDER BY
				PL.IsPrimaryLocation DESC
			  , PL.Name
	        
		SET @Err = @@ERROR

		RETURN @Err
    END

