﻿CREATE PROC [dbo].[usp_BregVisionOrder_Select_All_With_Everything]
AS
BEGIN


	SELECT
		  --  BVO.PracticeLocationID
		   BVO.CreatedDate		    AS OrderDate 
		,  P.PracticeName
		, PL.NAME					AS PracticeLocation
		, BVO.BregVisionOrderID
		, BVO.PurchaseOrder
		, BVO.Total					AS BVOTotal
		, BVOS.Status				AS BVOStatus
		, SO.SupplierOrderID	
		--, So.PracticeCatalogSupplierBrandID
		, PCSB.SupplierShortName
		, PCSB.BrandShortName
		, SO.Total					AS SOTotal
		, SOS.Status				AS SOStatus
		, SOLI.SupplierOrderLineItemID
		, SOLI.PracticeCatalogProductID
		, SOLIS.Status				AS SOLISStatus
		
		, SOLI.LineTotal			AS SOLILineTotal
		, SOLI.QuantityOrdered
		, SubPIOCI.QuantityCheckedIn
		
		, PI.QuantityOnHandPerSystem
	FROM dbo.BregVisionOrder AS BVO
	INNER JOIN dbo.SupplierOrder AS SO
		ON BVO.BregVisionOrderID = SO.BregVisionOrderID
	INNER JOIN dbo.SupplierOrderLineItem AS SOLI
		ON SO.SupplierOrderID = SOLI.SupplierOrderID
	INNER JOIN dbo.BregVisionOrderStatus AS BVOS
		ON BVO.BregVisionOrderStatusID = BVOS.BregVisionOrderStatusID	
	INNER JOIN dbo.SupplierOrderStatus AS SOS
		ON SO.SupplierOrderStatusID = SOS.SupplierOrderStatusID	
	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
		ON SO.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
	INNER JOIN PracticeLocation AS PL
		ON BVO.PracticeLocationID = PL.PracticeLocationID
	INNER JOIN dbo.Practice AS P
		ON PL.PracticeID = P.PracticeID
	INNER JOIN dbo.SupplierOrderLineItemStatus AS SOLIS
		ON SOLI.SupplierOrderLineItemStatusID = SOLIS.SupplierOrderLineItemStatusID	
	INNER JOIN dbo.ProductInventory AS PI
		ON   PL.PracticeLocationID = PI.PracticeLocationID
			AND SOLI.PracticeCatalogProductID = PI.PracticeCatalogProductID
	LEFT JOIN (SELECT SupplierOrderLineItemID, SUM(Quantity) AS QuantityCheckedIn
				FROM dbo.ProductInventory_OrderCheckIn 
				GROUP BY SupplierOrderLineItemID)AS SubPIOCI
		ON SOLI.SupplierOrderLineItemID = SubPIOCI.SupplierOrderLineItemID
--	WHERE SubPIOCI.QuantityCheckedIn IS NOT NULL
--		AND SubPIOCI.QuantityCheckedIn > SOLI.QuantityOrdered	
--		AND P.PracticeID > 1	
	ORDER BY
		  P.PracticeName
		, PL.NAME				
		, BVO.PurchaseOrder		  DESC
		, PCSB.SupplierShortName
		, PCSB.BrandShortName
END	
