﻿
/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Practice_Select_All_Names
   
   Description:  Selects all records from the table dbo.Practice
   				returns the PracticeID and the PracticeName.
   
   AUTHOR:       John Bongiorni 7/25/2007 9:45:21 PM
   
   Modifications:  Added EMRIntegrationPullEnabled				Mike Sneen		2/15/2013
                   Added FaxDispenseReceiptEnabled				Ysi             09/07/2013
   ------------------------------------------------------------ */  -------------------SELECT ALL----------------

CREATE PROCEDURE [dbo].[usp_Practice_Select_All_Names]
AS
BEGIN
	DECLARE @Err INT
	SELECT     
		P.PracticeID, 
		P.PracticeName, 
		P.IsOrthoSelect, 
		P.IsVisionLite, 
		P.IsActive, 
		PA.PracticeApplicationID,
		P.PaymentTypeID,
		PT.PaymentTypeName,
		P.BillingStartDate,
		BVPC.PracticeCode,
		P.CameraScanEnabled,
		ISNULL(P.IsBregBilling,0) AS IsBregBilling,
		P.EMRIntegrationPullEnabled,
		P.FaxDispenseReceiptEnabled,
		P.IsSecureMessagingInternalEnabled,
		P.IsSecureMessagingExternalEnabled,
		P.IsCustomFitEnabled
	FROM         
		Practice P 
        LEFT OUTER JOIN PracticeApplication PA 
			ON P.PracticeID = PA.PracticeID
		LEFT OUTER JOIN PaymentType PT
			ON P.PaymentTypeID = PT.PaymentTypeID
		LEFT OUTER JOIN BregVisionPracticeCode BVPC
			ON P.PracticeID = BVPC.PracticeID
	
	ORDER BY P.IsActive desc, P.PracticeName
	
	SET @Err = @@ERROR

	RETURN @Err
END
