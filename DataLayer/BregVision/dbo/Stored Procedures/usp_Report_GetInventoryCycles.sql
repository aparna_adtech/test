﻿-- =============================================
-- Author:		Mike Sneen
-- Create date: 1/12/12
-- Description:	Returns a list of active Inventory Cycles for a Practice.  Initially For use in Product Shrinkage Report 
-- =============================================
--usp_Report_GetInventoryCycles 3
CREATE PROCEDURE [dbo].[usp_Report_GetInventoryCycles]
	@PracticeLocationID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    Select 
		IC.EndDate,
		IC.InventoryCycleID,
		IC.StartDate,
		CycleTitle = Convert(Varchar, IC.StartDate) + (CASE WHEN IC.Title IS NULL THEN '' ELSE '-' + IC.Title END)
					  
	From InventoryCycle IC
	where 
		IC.PracticeLocationID = @PracticeLocationID
		AND IC.EndDate IS NOT NULL
		AND IC.IsActive = 1
	Order By InventoryCycleID Desc
    
END
