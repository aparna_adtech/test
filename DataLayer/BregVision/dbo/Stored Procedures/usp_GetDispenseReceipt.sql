﻿-- =============================================
-- Author:		Greg Ross
-- Create date: Sept. 24, 2007
-- Description:	Gets all data necessary to prioduce Dispense Receipt
-- Modifications:  20071002 JB modify for Dispense Receipts without a Physician.
--
--		--20071116 Add Size -- may need to remove Packaging
-- usp_GetDispenseReceipt 670828
--		--20110623 Added SupplierName (by hchan@breg.com)
		--20120104 Added Dispense Signature (by Michael_sneen@yahoo.com)
		--20141218 Altered HCPCSString to use DispenseQueue's HCPCs, if present (bjuan@breg.com)
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetDispenseReceipt]  -- 46
	@DispenseID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		select 
			D.PatientCode,
			ISNULL( D.ReceiptCode, '')	AS ReceiptCode,  -- D.ReceiptCode
			D.Total as Grandtotal,
			D.PatientFirstName AS PatientFirstName,
			D.PatientLastName AS PatientLastName,
			D.Note,
			ISNULL( C.Title, '')	AS Title,
			ISNULL( C.FirstName, '') AS FirstName,
			ISNULL( C.LastName, '')  AS LastName,
			ISNULL( C.Suffix, '')	 AS Suffix,
			SupplierName = (
				   Select mcs1.SupplierShortName from MasterCatalogSupplier mcs1
				   join MasterCatalogCategory mcc on mcs1.MasterCatalogSupplierID = mcc.MasterCatalogSupplierID
				   join MasterCatalogSubCategory mcsc on mcc.MasterCatalogCategoryID = mcsc.MasterCatalogCategoryID
				   where mcsc.MasterCatalogSubCategoryID = MCP.MasterCatalogSubCategoryID
				),
			MCP.ShortName,
			MCP.Code,
			MCP.IsCustomBrace,
			MCP.Packaging,
			
			--MCP.LeftRightSide as Side,  --20091026 MWS Replaced MCP Side with User Chosen Side
			DD.LRModifier as Side,
			MCP.Size,						--20071116 Add Size -- may need to remove Packaging
			
			
			ISNULL( MCP.Color, '') AS Color,
			MCP.Gender,
			DD.ICD9Code,
			DD.ActualChargeBilled as Total,
			DD.DMEDeposit,
			DD.Quantity,
			DD.LineTotal,
			COALESCE(DQ.HCPCs, dbo.ConcatHCPCS(PCP.PracticeCatalogProductID)) AS HCPCSString, -- JB 20071004 Added Column for UDF Concat of HCPCS
																							  -- BJ 20141218 Altered Column to use DispenseQueue HCPCs, if present
			ISNULL( DS.PhysicianSigned , 0 ) as PhysicianSigned,
			CASE WHEN DS.PhysicianSigned=1 THEN DS.PhysicianSignedDate ELSE null END as PhysicianSignedDate,
			DD.IsCustomFit,
			DD.FitterID,
			DD.DispenseDetailID,
			DD.Mod1,
			DD.Mod2,
			DD.Mod3,
			PL.IsHcpSignatureRequired,
			ISNULL( PSR.PatientSignatureRelationship, '') as PatientSignatureRelationship
			
		from DispenseDetail DD
		inner join dispense D on D.DispenseID= DD.DispenseID
		inner join PracticeCatalogProduct PCP on DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID
		inner join MasterCatalogProduct MCP on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
		inner join PracticeLocation PL on D.PracticeLocationID = PL.PracticeLocationID
		left join MasterCatalogSupplier MCS on MCP.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
		left join Physician P on DD.PhysicianID = P.PhysicianID
		left join Contact C on P.ContactID = C.ContactID
		left join DispenseQueue DQ on DD.DispenseQueueID = DQ.DispenseQueueID
		Left Join DispenseSignature DS on DQ.DispenseSignatureID = DS.DispenseSignatureID
		Left Join PatientSignatureRelationship PSR on PSR.Id = DS.PatientSignatureRelationshipId -- getting active patient signature relationships
		where D.DispenseID=@DispenseID 
		and D.isactive=1
		and DD.isactive=1
		--and P.isactive=1
		--and C.isactive = 1
		--and PCP.isactive=1
		--and PSR.IsActive=1
		--and MCP.isactive=1 
		and ((DS.IsActive Is NULL) OR (DS.IsActive Is NOT NULL and DS.IsActive=1))
		
	UNION ALL
	
			select 
			D.PatientCode,
			ISNULL( D.ReceiptCode, '')	AS ReceiptCode,  -- D.ReceiptCode
			D.Total as Grandtotal,
			D.PatientFirstName AS PatientFirstName,
			D.PatientLastName AS PatientLastName,
			D.Note,
			ISNULL( C.Title, '')	AS Title,
			ISNULL( C.FirstName, '') AS FirstName,
			ISNULL( C.LastName, '')  AS LastName,
			ISNULL( C.Suffix, '')	 AS Suffix,
			PCSB.SupplierName,
			TPP.ShortName,
			TPP.Code,
			CAST(0 as bit) AS IsCustomBrace,
			TPP.Packaging,
			
			--TPP.LeftRightSide as Side,  --20091026 MWS Replaced TPP Side with User Chosen Side
			DD.LRModifier as Side,
			TPP.Size,					--20071116 Add Size -- may need to remove Packaging
			
			ISNULL(TPP.Color, '') AS Color,
			TPP.Gender,
			DD.ICD9Code,
			DD.ActualChargeBilled as Total,
			DD.DMEDeposit,
			DD.Quantity,
			DD.LineTotal,
			COALESCE(DQ.HCPCs, dbo.ConcatHCPCS(PCP.PracticeCatalogProductID)) AS HCPCSString, -- JB 20071004 Added Column for UDF Concat of HCPCS
																							  -- BJ 20141218 Added
			ISNULL( DS.PhysicianSigned , 0 ) as PhysicianSigned,
			CASE WHEN DS.PhysicianSigned=1 THEN DS.PhysicianSignedDate ELSE null END as PhysicianSignedDate,
			DD.IsCustomFit,
			DD.FitterID,
			DD.DispenseDetailID,
			DD.Mod1,
			DD.Mod2,
			DD.Mod3,
			PL.IsHcpSignatureRequired,
			ISNULL( PSR.PatientSignatureRelationship, '') as PatientSignatureRelationship
			
		from DispenseDetail DD
		inner join dispense D on D.DispenseID= DD.DispenseID
		inner join PracticeCatalogProduct PCP on DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID
		inner join ThirdPartyProduct TPP on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
		inner join PracticeLocation PL on D.PracticeLocationID = PL.PracticeLocationID
		left join PracticeCatalogSupplierBrand PCSB on TPP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
		left join Physician P on DD.PhysicianID = P.PhysicianID
		left join Contact C on P.ContactID = C.ContactID
		left join DispenseQueue DQ on DD.DispenseQueueID = DQ.DispenseQueueID
		Left Join DispenseSignature DS on DQ.DispenseSignatureID = DS.DispenseSignatureID
		Left Join PatientSignatureRelationship PSR on PSR.Id = DS.PatientSignatureRelationshipId -- getting active patient signature relationships
		where D.DispenseID=@DispenseID 
		and D.isactive=1
		and DD.isactive=1
		--and P.isactive=1
		--and C.isactive = 1
		--and PCP.isactive=1
		--and PSR.IsActive=1
		--and TPP.isactive=1 
		and ((DS.IsActive Is NULL) OR (DS.IsActive Is NOT NULL and DS.IsActive=1))
		
	--) AS Sub
		
END
