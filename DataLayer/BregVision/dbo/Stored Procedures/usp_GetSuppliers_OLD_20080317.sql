﻿
-- =============================================
-- Author:		<Author: Greg Ross>
-- Create date: <Create Date: 07/23/07>
-- Description:	<Description: Used to get all suppliers/Brands in database>
-- modification:  jb 10072007 fix query to be effient and add thirdpartysuppliers.
--  [dbo].[usp_GetSuppliers_2]  10
--
--     SupplierID by itself (using both master catalogsupplierid and thirdpartysupplierID
--		  will not distinguish between MCS Bird and Cronin and TPS DeRoyal
--			leading to B&C products being placed under DeRoyal

--  Modification:
--	2007.11.09 JB  Add to WHERE Clause "AND PCSB.ISACTIVE = 1" ; Add to LEFT JOIN ON CONTACT "AND C.ISACTIVE = 1"
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetSuppliers_OLD_20080317]  --10
	@PracticeLocationID int
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON ;
     
     SELECT 
		Sub.Brand,
		
		Sub.SupplierID,
		
--		Sub.OldSupplierID,
                
		Sub.Name,
        Sub.LastName,
		Sub.PhoneWork,
                
        Sub.Address,
        Sub.City,  
        Sub.Zipcode,
        
        Sub.IsThirdPartySupplier
				--MCC.[Name] AS Category
     
	FROM     
     
     (
     SELECT     --  DISTINCT PCSB.[SupplierName] AS Brand,
                DISTINCT PCSB.BrandShortName AS Brand,  --JB display name for brand
                
                PCSB.PracticeCatalogSupplierBrandID AS supplierID,           
                --  PCSB.[MasterCatalogSupplierID] AS OldSupplierID,
                
                --PCSB.[MasterCatalogSupplierID] AS SupplierID,
                
				ISNULL(c.[FirstName], '') AS Name,
                ISNULL(C.[LastName], '')  AS LastName,
                ISNULL(c.[PhoneWork], '')  AS PhoneWork,
                
                ISNULL(A.[AddressLine1], '')   AS Address,
                ISNULL(A.City, '')  AS City,  
                ISNULL(A.[ZipCode], '')   AS Zipcode,
                0 AS IsThirdPartySupplier,
                PCSB.Sequence
				--MCC.[Name] AS Category


        FROM    [MasterCatalogSupplier] AS MCS
                LEFT JOIN address A 
						ON MCS.[AddressID] = A.[AddressID]
                LEFT JOIN [Contact] C 
						ON mcs.[ContactID] = c.[ContactID]
						AND C.ISACTIVE = 1
				INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
					ON MCS.MasterCatalogSupplierID = pcsb.MasterCatalogSupplierID

				INNER JOIN [PracticeCatalogProduct] PCP 
					ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID				
										
				INNER JOIN [ProductInventory] PI 
					ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]

				WHERE PI.[PracticeLocationID] = @PracticeLocationID
					AND    MCS.ISACTIVE = 1
					AND A.ISACTIVE = 1
					AND PCSB.IsActive = 1
					AND PCP.ISACTIVE = 1
					--  AND PI.ISACTIVE = 1
					AND IsThirdPartySupplier = 0 -- MasterCatalogSuppliers.
					
UNION

     SELECT     --  DISTINCT PCSB.[SupplierName] AS Brand,
				DISTINCT PCSB.BrandShortName AS Brand,  --JB display name for brand
                
                PCSB.PracticeCatalogSupplierBrandID AS supplierID,    --  SupplierBrandID is neccessary to associate
--														--   the child grid without criss-crossing.              
--                TPS.[ThirdPartySupplierID] AS OldSupplierID,
				--TPS.[ThirdPartySupplierID] AS SupplierID,
                
				ISNULL(c.[FirstName], '') AS Name,
                ISNULL(C.[LastName], '')  AS LastName,
                ISNULL(c.[PhoneWork], '')  AS PhoneWork,
                
                ISNULL(A.[AddressLine1], '')   AS Address,
                ISNULL(A.City, '') AS City,  
                ISNULL(A.[ZipCode], '')   AS Zipcode,
                1 AS IsThirdPartySupplier,
                PCSB.Sequence
				--MCC.[Name] AS Category


        FROM    [dbo].[ThirdPartySupplier] AS TPS
                LEFT JOIN address A 
						ON TPS.[AddressID] = A.[AddressID]
                lefT JOIN [Contact] C 
						ON TPS.[ContactID] = c.[ContactID]
						AND C.ISACTIVE = 1
				INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB
					ON TPS.ThirdPartySupplierID = pcsb.ThirdPartySupplierID

				INNER JOIN [PracticeCatalogProduct] PCP 
					ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID				
										
				INNER JOIN [ProductInventory] PI 
					ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]

				WHERE PI.[PracticeLocationID] = @PracticeLocationID
					AND    tps.ISACTIVE = 1
					--AND A.ISACTIVE = 1
					--AND C.ISACTIVE = 1
					AND PCSB.IsActive = 1
					AND PCP.ISACTIVE = 1
					--AND PI.ISACTIVE = 1
					AND IsThirdPartySupplier = 1 -- ThirdPartySuppliers.
					
			) AS Sub
			
		ORDER BY
			SUB.IsThirdPartySupplier
			, SUB.Sequence
			, SUB.Brand

    END





