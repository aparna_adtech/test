﻿
CREATE proc [dbo].[usp_zImport_PCSPortsMed_1_ThirPartyProduct_From_Import_PracticeData__TPSProduct_20071025]
as
begin
--select * from ThirdPartyProduct  should be 712-715

		INSERT INTO ThirdPartyProduct
			( PracticeCatalogSupplierBrandID
			, Code
			, Name
			, ShortName

			, Packaging
			, LeftRightSide
			, Size
			, Color
			, Gender
			, IsDiscontinued
			, WholesaleListCost
			, Description
			, CreatedUserID
			, CreatedDate
			, IsActive
			)

		Select 
			  PracticeCatalogSupplierBrandID
			, ProductCode
			, ProductName
			, ProductShortName

			, Packaging
			, LeftRightSide
			, Size
			, ISNULL(Color, '') as color
			, ISNULL(Gender, '') as Gender
			, 0 AS IsDiscontinued
			, WholesaleListCost
			, ISNULL(Description, '') AS Description
			, 0
			, GetDate()
			, 1
		from Import_PracticeData.dbo.['PCSportsMed 3rd Party Products$']  --SET THIS to IMportdata table.
		where practiceCatalogSupplierBrandID is not null  --  prevent import of extraneous null rows



end