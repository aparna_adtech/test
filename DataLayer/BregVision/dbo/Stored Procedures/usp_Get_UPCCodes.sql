﻿

--exec [usp_Get_UPCCodes] 33,0
--exec [usp_Get_UPCCodes] 0, 121
Create PROCEDURE [dbo].[usp_Get_UPCCodes] 
		@MasterCatalogProductID INT,
		@ThirdPartyProductID INT
		
AS
BEGIN

Select code from UPCCode u
where
	(((@MasterCatalogProductID <> 0) AND (u.MasterCatalogProductID = @MasterCatalogProductID)) OR (@MasterCatalogProductID = 0)) AND
	(((@ThirdPartyProductID <> 0) AND (u.ThirdPartyProductID = @ThirdPartyProductID)) OR (@ThirdPartyProductID = 0))

END