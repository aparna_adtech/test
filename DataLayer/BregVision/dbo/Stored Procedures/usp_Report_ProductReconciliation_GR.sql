﻿




CREATE PROC [dbo].[usp_Report_ProductReconciliation_GR] 
		  @PracticeID	INT
		, @PracticeLocationID VARCHAR(2000)
		, @SupplierShortName  VARCHAR(2000)
		, @StartDate		DATETIME
		, @EndDate			DATETIME
AS
BEGIN

--	DECLARE @PracticeID INT
--	SET @PracticeID = 6

--INSERT INTO zzzReportMessage (Message)
--SELECT @PracticeLocationID

--  SET @EndDate to 11:59:59 PM

SELECT @EndDate = DATEADD(DAY, 1, @EndDate)

DECLARE @PracticeLocationIDString VARCHAR(2000)
SET @PracticeLocationIDString = @PracticeLocationID

INSERT INTO zzzReportMessage (Message)
SELECT @StartDate


INSERT INTO zzzReportMessage (Message)
SELECT @EndDate



SELECT 
		 SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.PurchaseOrder
		, SUB.SupplierShortName
		--, SUB.BrandShortName
		
		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN NULL
		ELSE SUB.BrandShortName
		END AS BrandShortName
		
		
		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN 1
		ELSE 0
		END AS IsBrandSuppressed


		, SUB.ProductName
		--, SUB.Side
		--, SUB.Size
		--, SUB.Gender
		, SUB.Code
		
		--, CAST(SUB.ActualWholesaleCost AS DECIMAL(15,2) )	AS ActualWholesaleCost
		--, SUM(SUB.QuantityOrdered)							AS QuantityOrdered
		--, CAST( SUM(SUB.LineTotal)  AS DECIMAL(15,2) )		AS TotalCost
		, SUB.QuantityOrdered
		, SUB.ActualWholesaleCost
		, SUB.QuantityCheckedIn
		, SUB.QuantityNotCheckedIn

		, SUB.CostOfOutstandingProducts 
		, SUB.OrderDate
FROM
	(SELECT 
		
		  P.PracticeName
		, PL.NAME AS PracticeLocation
		, PCSB.IsThirdPartySupplier
		, SO.PurchaseOrder		
		, PCSB.SupplierShortName
		, PCSB.BrandShortName


		, MCP.[Name] AS ProductName
		--, MCP.LeftRightSide AS Side
		--, MCP.Size AS Size
		--, MCP.Gender AS Gender
		, MCP.Code AS Code
		
		
		, SOLI.QuantityOrdered
		, SOLI.ActualWholesaleCost
		, PIOCI.Quantity as QuantityCheckedIn
		, SOLI.QuantityOrdered - PIOCI.Quantity as QuantityNotCheckedIn
		, SOLI.ActualWholesaleCost * (SOLI.QuantityOrdered - PIOCI.Quantity) as CostOfOutstandingProducts 

		, SOLI.CreatedDate AS OrderDate		
		
	FROM dbo.SupplierOrderLineItem AS SOLI WITH (NOLOCK)
	INNER JOIN dbo.SupplierOrder AS SO	WITH (NOLOCK)
		ON SOLI.SupplierOrderID = SO.SupplierOrderID
	INNER JOIN dbo.BregVisionOrder AS BVO	WITH (NOLOCK)
		ON BVO.BregVisionOrderID = SO.BregVisionOrderID
	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON BVO.PracticeLocationID = PL.PracticeLocationID
	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID
	INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
		ON SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
	INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
		ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
	INNER JOIN ProductInventory_OrderCheckIn as PIOCI WITH (NOLOCK)
		on SOLI.SupplierOrderLineItemID = PIOCI.SupplierOrderLineItemID
	WHERE P.PracticeID = @PracticeID		
		
		AND 
		PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))

		AND PCSB.SupplierShortName IN 			
			(SELECT TextValue 
			 FROM dbo.udf_ParseStringArrayToTable( @SupplierShortName, ',') )
	
		AND SOLI.CreatedDate >= @StartDate
		AND SOLI.CreatedDate < @EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM
		
	UNION
	
		SELECT 
		  P.PracticeName
		, PL.NAME AS PracticeLocation
		, PCSB.IsThirdPartySupplier
		, SO.PurchaseOrder		
		, PCSB.SupplierShortName
		, PCSB.BrandShortName
		
		, TPP.[Name] AS ProductName
		--, TPP.LeftRightSide AS Side
		--, TPP.Size AS Size
		--, TPP.Gender AS Gender
		, TPP.Code AS Code
		
		, SOLI.QuantityOrdered
		, SOLI.ActualWholesaleCost
		, PIOCI.Quantity as QuantityCheckedIn
		, SOLI.QuantityOrdered - PIOCI.Quantity as QuantityNotCheckedIn
		, SOLI.ActualWholesaleCost * (SOLI.QuantityOrdered - PIOCI.Quantity) as CostOfOutstandingProducts 

		, SOLI.CreatedDate AS OrderDate
				
		
	FROM dbo.SupplierOrderLineItem AS SOLI	WITH (NOLOCK)
	INNER JOIN dbo.SupplierOrder AS SO	WITH (NOLOCK)
		ON SOLI.SupplierOrderID = SO.SupplierOrderID
	INNER JOIN dbo.BregVisionOrder AS BVO	WITH (NOLOCK)
		ON BVO.BregVisionOrderID = SO.BregVisionOrderID
	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON BVO.PracticeLocationID = PL.PracticeLocationID
	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID
	INNER JOIN dbo.PracticeCatalogProduct AS pcp	WITH (NOLOCK)
		ON SOLI.PracticeCatalogProductID = PCP.PracticeCatalogProductID
	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 
	INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
		ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID
	INNER JOIN ProductInventory_OrderCheckIn as PIOCI WITH (NOLOCK)
		on SOLI.SupplierOrderLineItemID = PIOCI.SupplierOrderLineItemID
	WHERE 
		P.PracticeID = @PracticeID	

		AND PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))

		AND PCSB.SupplierShortName IN 			
			(SELECT TextValue 
		 FROM dbo.udf_ParseStringArrayToTable( @SupplierShortName, ',') )

		AND SOLI.CreatedDate >= @StartDate
		AND SOLI.CreatedDate < @EndDate --  Less than since @EndDate has one day added to it since the time is 12AM
	
	)		
		AS SUB	
		
	GROUP BY
		SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.IsThirdPartySupplier
		, SUB.PurchaseOrder
		, SUB.SupplierShortName
		, SUB.BrandShortName
		
		, SUB.ProductName
		--, SUB.Side
		--, SUB.Size
		--, SUB.Gender
		, SUB.Code
		, SUB.QuantityOrdered
		, SUB.ActualWholesaleCost
		, SUB.QuantityCheckedIn
		, SUB.QuantityNotCheckedIn
		--, SOLI.LineTotal
		, SUB.CostOfOutstandingProducts 
		, SUB.OrderDate
		
	ORDER BY
		SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.IsThirdPartySupplier
		, SUB.PurchaseOrder
		, SUB.SupplierShortName
		, SUB.BrandShortName
		, SUB.ProductName
		, SUB.Code


		
END
