﻿


-- =============================================
-- Author:		Greg Ross
-- Create date: 08/29/07
-- Description:	Used to get the Supplier Order information for POS prior to faxing or emailing
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetBVOSupplierInfoForFaxEmail]  --166
	-- Add the parameters for the stored procedure here
	@BregVisionOrderID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select distinct 
	BVO.PurchaseOrder,
	PL.PracticeLocationID,
	PCSB.BrandShortName,
	ST.ShippingTypeName,
	SO.Total,
	BVO.CreatedDate,
	SO.SupplierOrderID,
	C.EmailAddress,
	C.Fax,
	ISNULL(PLSAC.AccountCode, '') AS AccountCode
	--MCS.FaxOrderPlacement as FAXNumber,
	--MCS.EmailOrderPlacement as Email
	
 from BregVisionOrder BVO 
	inner join SupplierOrder SO 
		on BVO.BregVisionOrderID = SO.BregVisionOrderID
	inner join PracticeCatalogSupplierBrand PCSB
		on SO.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
	inner join ShippingType ST 
		on BVO.ShippingTypeID = ST.ShippingTypeID
	--inner join MasterCatalogSupplier MCS
	--	on PCSB.MasterCatalogSupplierID = MCS.MasterCatalogSupplierID
	inner join PracticeLocation PL 
		on 	BVO.PracticeLocationID = PL.PracticeLocationID
	left join Contact C 
		on PL.ContactID = C.ContactID
		AND C.IsActive = 1
	left join PracticeLocation_supplier_AccountCode PLSAC
		--on PL.PracticeLocationID = PLSAC.PracticeLocationID
	--left join PracticeLocation_supplier_AccountCode PLSAC 
		on PCSB.PracticeCatalogSupplierBrandID = PLSAC.PracticeCatalogSupplierBrandID
		AND PL.PracticeLocationID = PLSAC.PracticeLocationID
		AND PLSAC.IsActive = 1
where BVO.isactive=1
	and SO.isactive=1
	and PCSB.isactive=1
	and BVO.BregVisionOrderID=@BregVisionOrderID
	 


END



