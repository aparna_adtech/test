﻿CREATE PROC [dbo].[usp_Report_ProductsDispensed_By_HCPCS_With_DateDispensed_PatientCodeV3]
	  @PracticeID					INT
	, @PracticeLocationID			VARCHAR(2000)
	, @StartDate					DATETIME
	, @EndDate						DATETIME
	, @OrderBy						INT
	, @OrderDirection				BIT
	, @HCPCsList					VARCHAR(2000)	= NULL
	, @IsMedicare					BIT				= NULL
	, @MedicareOrderDirection		BIT				= NULL
	, @InternalPracticeLocationID	VARCHAR(2000)
	, @InternalHCPCsList			VARCHAR(2000)	= NULL
AS
BEGIN


-- filter tables
DECLARE @locations TABLE ([PracticeLocationID] INT);
DECLARE @hcpcs TABLE (HCPCS VARCHAR(50));


-- Parse Practice Location List
IF(@InternalPracticeLocationID = 'ALL')
BEGIN
	-- get all locations for a practice
	DECLARE @tempLocations TABLE (
		PracticeLocationID INT
		,PracticeLocationName VARCHAR(50)
		,IsPrimaryLocation BIT
	);
	INSERT @tempLocations
		EXEC dbo.usp_PracticeLocation_Select_All_ID_Name_Given_PracticeID @PracticeID = @PracticeID;

	-- insert just the practice location ID into our locations table var
	INSERT @locations ([PracticeLocationID])
		SELECT PracticeLocationID FROM @tempLocations;
END
ELSE
BEGIN
	-- not including all locations; so, we will use the comma-separated practice locations from parameter @PracticeLocationID
	INSERT @locations ([PracticeLocationID])
		SELECT ID FROM dbo.udf_ParseArrayToTable(@PracticeLocationID, ',');
END


-- Parse HCPCs list
IF(@InternalHCPCsList = 'ALL')
BEGIN
	-- turn table of practice location IDs into a comma-separated list
	DECLARE @locationList NVARCHAR(MAX);
	SELECT @locationList = COALESCE(@locationList + CAST(PracticeLocationID AS NVARCHAR(10)) + ',', '')
		FROM @locations;

	-- drop trailing comma
	IF(LEN(@locationList) > 0)
		SET @locationList = LEFT(@locationList, LEN(@locationList) - 1);

	-- get list of HCPCS based on list of practice locations
	INSERT @hcpcs
		EXEC dbo.usp_GetPracticeLocationHCPCs @PracticeLocationIDString = @locationList;

	-- all dispensements include dispensements without any HCPCS set
	INSERT @hcpcs VALUES ('');
END
ELSE
BEGIN
	INSERT @hcpcs
		SELECT ID from dbo.udf_ParseArrayToTable(@HCPCsList, ',');

	-- handle case where no HCPCS were selected (we want to see dispensements without any HCPCS selected)
	IF NOT EXISTS (SELECT * FROM @hcpcs)
		INSERT @hcpcs VALUES ('');
END


SELECT @EndDate = DATEADD(DAY, 1, @EndDate)


-- look up matching Dispense records with HCPCS
DECLARE @matchingDispenseIDWithHCPCS TABLE ([DispenseID] INT NOT NULL PRIMARY KEY, [HCPCS] XML);
INSERT INTO @matchingDispenseIDWithHCPCS
	SELECT
		D.DispenseID
		,CAST(
			'<a>' +
			REPLACE(
				REPLACE(
					ISNULL(
						ISNULL(
							DQ.HCPCs,
							IIF(
								DQ.IsCustomFit IS NULL or (DQ.IsCustomFit=0 AND PCP.IsSplitCode=1),
								dbo.ConcatHCPCSOTS(pcp.PracticeCatalogProductID),
								dbo.ConcatHCPCS(pcp.PracticeCatalogProductID)
							)
						),
						''
					),
					'&',
					'&amp;'
				),
				',',
				'</a><a>'
			) +
			'</a>'
			AS XML
		) AS HCPCS
	FROM
		Dispense D
		LEFT JOIN DispenseDetail DD on D.DispenseID=DD.DispenseDetailID
		LEFT JOIN DispenseQueue DQ on DD.DispenseQueueID=DQ.DispenseQueueID
		LEFT JOIN ProductInventory [PI] on [PI].ProductInventoryID=DQ.ProductInventoryID
		LEFT JOIN PracticeCatalogProduct PCP on [PI].PracticeCatalogProductID=PCP.PracticeCatalogProductID
		LEFT JOIN PracticeLocation PL on D.PracticeLocationID=PL.PracticeLocationID
		JOIN @locations L on PL.PracticeLocationID=L.PracticeLocationID
	WHERE
		D.IsActive=1
		AND DD.IsActive=1
		AND DD.IsMedicare=COALESCE(@IsMedicare, DD.IsMedicare)
		AND D.DateDispensed>=@StartDate
		AND D.DateDispensed<@EndDate     --  Less than since @EndDate has one day added to it since the time is 12AM
;

DECLARE @filteredDispenseIDs TABLE ([DispenseID] INT NOT NULL PRIMARY KEY);
INSERT INTO @filteredDispenseIDs
	SELECT
		DISTINCT DispenseID
	FROM
		(
			-- expand XML HCPCS into rows and standardize no-HCPCS as an empty string
			SELECT A.DispenseID, LTRIM(RTRIM(B.value('.', 'VARCHAR(MAX)'))) as HCPCS
			FROM @matchingDispenseIDWithHCPCS A
				CROSS APPLY HCPCS.nodes('/a') t(B)
		) T1
		JOIN @hcpcs T2 on T1.HCPCS=T2.HCPCS
;


-- main query
SELECT
	SUB.HCPCS
	, SUB.PracticeName		AS Practice
	, SUB.PracticeLocation	AS Location
	, CASE
		WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
		ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
		END					AS SupplierBrand
	, SUB.SupplierShortName	AS Supplier
	, CASE
		WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
		ELSE SUB.BrandShortName
		END					AS Brand
	, SUB.Sequence
	, SUB.ProductName		AS Product
	, SUB.LeftRightSide + ', ' + SUB.Size  + ', ' + SUB.Gender
							AS SideSizeGender
	, SUB.Code
	, CASE SUB.IsCashCollection
		WHEN 1 THEN Sub.BillingChargeCash
		WHEN 0 THEN 0
		END					AS BillingChargeCash
	, CONVERT(VARCHAR(10), SUB.DateDispensed, 101)
							AS DateDispensed
	, SUB.PatientCode
	, CASE SUB.IsMedicare
		WHEN 1 THEN 'Y'
		WHEN 0 THEN 'N'
		END					AS IsMedicare
	, CASE CAST(SUB.ABNType AS varchar(50))
		WHEN 'Medicare'		THEN 'M' + ',' + CAST(SUB.MedicareOption AS varchar(50))
		WHEN 'Commercial'	THEN 'C' + ',' + CAST(SUB.MedicareOption AS varchar(50))
		END					AS ABNType
	, SUB.ICD9Code
	, SUB.Quantity
	, CAST( SUB.ActualWholesaleCostLineTotal	AS DECIMAL(15,2) )
							AS ActualWholesaleCostLineTotal
	, CAST( SUB.DMEDepositLineTotal				AS DECIMAL(15,2) )
							AS DMEDepositLineTotal
	, SUB.IsCashCollection
	, CASE SUB.IsCashCollection
		WHEN 1 THEN 0
		WHEN 0 THEN CAST( SUB.ActualChargeBilledLineTotal	AS DECIMAL(15,2) )
		END					AS ActualChargeBilledLineTotal
	, ISNULL(SUB.PayerName, 'N/A')
							AS PayerName
	, SUB.DispenseNote
	, SUB.OriginalActualChargeBilled
	, LTRIM(RTRIM(ISNULL(SUB.PatientFirstName,'')))
							AS PatientFirstName
	, LTRIM(RTRIM(ISNULL(SUB.PatientLastName,'')))
							AS PatientLastName
	, SUB.Mod1
	, SUB.Mod2
	, SUB.Mod3
FROM
	(
		SELECT
			ISNULL(NULLIF(
				ISNULL(DQ.HCPCs,
					IIF(DQ.IsCustomFit = NULL or (DQ.IsCustomFit = 0 AND pcp.IsSplitCode=1),
						dbo.ConcatHCPCSOTS(pcp.PracticeCatalogProductID),
						dbo.ConcatHCPCS(pcp.PracticeCatalogProductID)))
				,''), 'No HCPC')
								AS HCPCS
			, P.PracticeName
			, PL.Name			AS PracticeLocation
			, D.PatientCode
			, PCSB.IsThirdPartySupplier
			, PCSB.SupplierShortName
			, PCSB.BrandShortName
			, ISNULL(PCSB.Sequence, 999) + CASE WHEN PCP.IsThirdPartyProduct = 1 THEN 1000 ELSE 0 END
								AS Sequence
			, PCP.PracticeCatalogProductID
			, PCP.BillingChargeCash
			, CASE
				WHEN PCP.IsThirdPartyProduct = 1 THEN TPP.[Name]
				ELSE MCP.[Name]
				END				AS ProductName
			, CAST( ISNULL(DD.LRModifier, 'NA') AS VARCHAR(10) )
								AS LeftRightSide
			, CAST( ISNULL(CASE WHEN PCP.IsThirdPartyProduct = 1 THEN TPP.Size ELSE MCP.Size END, 'NA') AS VARCHAR(50) )
								AS Size
			, CAST( ISNULL(CASE WHEN PCP.IsThirdPartyProduct = 1 THEN TPP.Gender ELSE MCP.Gender END, 'NA')	AS VARCHAR(50) )
								AS Gender
			, ISNULL(CASE WHEN PCP.IsThirdPartyProduct = 1 THEN TPP.Code ELSE MCP.Code END, '')
								AS Code
			, D.DateDispensed
			, DD.IsMedicare
			, DD.ABNType
			, DD.ICD9Code
			, DD.Quantity
			, PI_DD.ActualWholesaleCost
			, DD.ActualChargeBilled
								AS ActualChargeBilled
			, DD.DMEDeposit
			, DD.ActualChargeBilled * DD.Quantity
								AS ActualChargeBilledLineTotal
			, DD.DMEDeposit		AS DMEDepositLineTotal
			, DD.IsCashCollection
			, (PI_DD.ActualWholesaleCost * DD.Quantity) / PCP.StockingUnits
								AS ActualWholesaleCostLineTotal
			, ISNULL(D.Comment, '')
								AS Comment		-- Not Displayed for Version 1.
			, PP.Name			AS PayerName
			, D.Notes			AS DispenseNote
			, DD.ActualChargeBilled
								AS OriginalActualChargeBilled
			, D.PatientFirstName
								AS PatientFirstName
			, D.PatientLastName AS PatientLastName
			, DD.Mod1
			, DD.Mod2
			, DD.Mod3
			, DQ.MedicareOption

		FROM
			@filteredDispenseIDs
		
			INNER JOIN Dispense AS D								WITH (NOLOCK)
				ON [@filteredDispenseIDs].DispenseID = D.DispenseID

			INNER JOIN PracticeLocation AS PL						WITH (NOLOCK)
				ON D.PracticeLocationID = PL.PracticeLocationID

			INNER JOIN Practice AS P								WITH (NOLOCK)
				ON PL.PracticeID = P.PracticeID

			INNER JOin DispenseDetail AS DD							WITH (NOLOCK)
				ON D.DispenseID = DD.DispenseID

			INNER JOIN ProductInventory_DispenseDetail AS PI_DD		WITH (NOLOCK)
				ON DD.DispenseDetailID = PI_DD.DispenseDetailID

			INNER JOIN dbo.PracticeCatalogProduct AS PCP			WITH (NOLOCK)
				ON DD.PracticeCatalogProductID = PCP.PracticeCatalogProductID

			INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB		WITH (NOLOCK)
				ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

			LEFT JOIN dbo.MasterCatalogProduct AS MCP				WITH (NOLOCK)
				ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

			LEFT JOIN dbo.ThirdPartyProduct AS TPP					WITH (NOLOCK)
				ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

			LEFT OUTER JOIN DispenseQueue DQ
				on  DQ.DispenseQueueID = DD.DispenseQueueID

			LEFT JOIN PracticePayer PP
				on PP.PracticePayerID = D.PracticePayerID

		WHERE
			PI_DD.IsActive = 1
	) AS SUB

ORDER BY
	CASE
		WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName DESC))
		WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.ProductName))

		WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName DESC))
		WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.ProductName))

		WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName DESC))
		WHEN (@OrderBy = 1 OR @OrderBy = 3) AND @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.ProductName))

		WHEN @OrderBy = 2 AND @OrderDirection = 0 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed DESC))
		WHEN @OrderBy = 2 AND @OrderDirection = 1 AND @MedicareOrderDirection IS NULL THEN (RANK() OVER (ORDER BY SUB.DateDispensed))

		WHEN @OrderBy = 2 AND @OrderDirection = 0 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed DESC))
		WHEN @OrderBy = 2 AND @OrderDirection = 1 AND @MedicareOrderDirection = 0 THEN (RANK() OVER (ORDER BY SUB.IsMedicare DESC, SUB.DateDispensed))

		WHEN @OrderBy = 2 AND @OrderDirection = 0 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed DESC))
		WHEN @OrderBy = 2 AND @OrderDirection = 1 AND @MedicareOrderDirection = 1 THEN (RANK() OVER (ORDER BY SUB.IsMedicare, SUB.DateDispensed))
	END

END
