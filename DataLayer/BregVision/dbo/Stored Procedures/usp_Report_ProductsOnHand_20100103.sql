﻿
/*

	EXEC [usp_Report_ProductsOnHand] 6, '13, 15, 16, 17, 18, 19, 20, 22, 23, 24', '34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46' 
	EXEC  [usp_Report_ProductsOnHand] 62, '187, 186, 188 ,190 ,189', '857'

*/


CREATE PROC [dbo].[usp_Report_ProductsOnHand_20100103] 
		  @PracticeID	INT
		, @PracticeLocationID VARCHAR(2000)
		, @SupplierBrandID  VARCHAR(2000)   --  ID String.

AS
BEGIN


DECLARE @PracticeLocationIDString VARCHAR(2000)
DECLARE @SupplierBrandIDString VARCHAR(2000)

SET @PracticeLocationIDString = @PracticeLocationID
SET @SupplierBrandIDString = @SupplierBrandID

SELECT 
		  SUB.PracticeName
		, SUB.PracticeLocation

		, SUB.PracticeCatalogSupplierBrandID

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN SUB.SupplierShortName
			ELSE SUB.SupplierShortName + ' (' + SUB.BrandShortName + ')'
			END					AS SupplierBrand

		, SUB.SupplierShortName	AS Supplier

		, CASE WHEN SUB.SupplierShortName = SUB.BrandShortName THEN ''
		ELSE SUB.BrandShortName
		END						AS Brand

		, SUB.Sequence	

		, SUB.ProductName			AS Product
		, SUB.Side + ', ' + SUB.Size  + ', ' + SUB.Gender AS SideSizeGender
		, SUB.Code

		
		, SUM(SUB.QuantityOnHandPerSystem)					AS QuantityOnHandPerSystem
		, SUB.ParLevel										AS ParLevel
		, SUB.ReorderLevel									AS ReorderLevel
		, SUB.CriticalLevel									AS CriticalLevel
		, CAST(SUB.WholesaleCost AS DECIMAL(15,2) )			AS ActualWholesaleCost
		, CAST( SUM(SUB.LineTotal)  AS DECIMAL(15,2) )		AS TotalCost
		, dbo.ConcatHCPCS(Sub.PracticeCatalogProductID) AS HCPCSString

FROM
	(SELECT 
		  P.PracticeName
		, PL.NAME													AS PracticeLocation

		, PCSB.IsThirdPartySupplier

		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierShortName
		, PCSB.BrandShortName

		, ISNULL(PCSB.Sequence, 999)								AS Sequence
		, PCP.PracticeCatalogProductID

		, MCP.[Name] AS ProductName
		, CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) )		AS Side
		, CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) )				AS Size
		, CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
		, ISNULL(MCP.Code, '')										AS Code
		
		, (PCP.WholesaleCost / PCP.StockingUnits) as WholesaleCost
		, PI.QuantityOnHandPerSystem
		, PI.ParLevel
		, PI.ReorderLevel 
		, PI.CriticalLevel 
		, (Convert(money,PCP.WholesaleCost) / PCP.StockingUnits) * PI.QuantityOnHandPerSystem as LineTotal

		
	From dbo.ProductInventory as PI with (NOLOCK)

	INNER JOIN dbo.PracticeCatalogProduct AS PCP	WITH (NOLOCK)
		on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID

	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON PI.PracticeLocationID = PL.PracticeLocationID

	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID

	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 

	INNER JOIN dbo.MasterCatalogProduct AS MCP	WITH (NOLOCK)
		ON PCP.MasterCatalogProductID = MCP.MasterCatalogProductID

	WHERE 
		P.PracticeID = @PracticeID		
		AND P.IsActive = 1
		AND PL.IsActive = 1
		AND PCP.IsActive = 1
		AND PCSB.IsActive = 1
		AND MCP.IsActive = 1
		and PI.IsSetToActive=1
		and PI.IsActive=1
					
		AND PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))

		AND PCSB.PracticeCatalogSupplierBrandID IN 			
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable( @SupplierBrandIDString, ',') )
	
		
	UNION
	
		SELECT 
		  P.PracticeName
		, PL.NAME													AS PracticeLocation

		, PCSB.IsThirdPartySupplier

		, PCSB.PracticeCatalogSupplierBrandID
		, PCSB.SupplierShortName
		, PCSB.BrandShortName
		
		, ISNULL(PCSB.Sequence, 999) + 1000							AS Sequence
		, PCP.PracticeCatalogProductID

		, TPP.[Name]												AS ProductName
		, CAST(ISNULL(TPP.LeftRightSide, 'NA') AS VARCHAR(10) )		AS Side
		, CAST(ISNULL(TPP.Size, 'NA') AS VARCHAR(50) )				AS Size
		, CAST(ISNULL(TPP.Gender, 'NA')	AS VARCHAR(50) )			AS Gender
		, ISNULL(TPP.Code, '')										AS Code
		
		, (PCP.WholesaleCost / PCP.StockingUnits) as WholesaleCost
		, PI.QuantityOnHandPerSystem
		, PI.ParLevel
		, PI.ReorderLevel 
		, PI.CriticalLevel 
		, (Convert(money,PCP.WholesaleCost) / PCP.StockingUnits) * PI.QuantityOnHandPerSystem			as LineTotal
				
		
	From dbo.ProductInventory as PI with (NOLOCK)

	INNER JOIN dbo.PracticeCatalogProduct AS PCP	WITH (NOLOCK)
		on PI.PracticeCatalogProductID = PCP.PracticeCatalogProductID

	INNER JOIN dbo.PracticeLocation AS PL	WITH (NOLOCK)
		ON PI.PracticeLocationID = PL.PracticeLocationID

	INNER JOIN dbo.Practice AS P	WITH (NOLOCK)
		ON PL.PracticeID = P.PracticeID

	INNER JOIN dbo.PracticeCatalogSupplierBrand AS PCSB	WITH (NOLOCK)
		ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID 

	INNER JOIN dbo.ThirdPartyProduct AS TPP	WITH (NOLOCK)
		ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID

	WHERE 
		P.PracticeID = @PracticeID		
		AND P.IsActive = 1
		AND PL.IsActive = 1
		AND PCP.IsActive = 1
		AND PCSB.IsActive = 1
		AND TPP.IsActive = 1
		and PI.IsSetToActive=1
		and PI.IsActive=1

		AND PL.PracticeLocationID IN 
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable(@PracticeLocationIDString, ','))

		AND PCSB.PracticeCatalogSupplierBrandID IN 			
			(SELECT ID 
			 FROM dbo.udf_ParseArrayToTable( @SupplierBrandIDString, ',') )	
	)		
		AS SUB	
		
	GROUP BY
		  SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.PracticeCatalogSupplierBrandID
		, SUB.IsThirdPartySupplier
		, SUB.Sequence
		, SUB.PracticeCatalogProductID
		, SUB.SupplierShortName
		, SUB.BrandShortName
		
		, SUB.ProductName
		, SUB.Side
		, SUB.Size
		, SUB.Gender
		, SUB.Code
		
		, SUB.WholesaleCost
		, SUB.ParLevel
		, SUB.ReorderLevel 
		, SUB.CriticalLevel 
		
	ORDER BY
		  SUB.PracticeName
		, SUB.PracticeLocation
		, SUB.Sequence
		, SUB.SupplierShortName
		, SUB.BrandShortName
		, SUB.ProductName
		, SUB.Code
		, SUB.Side + ', ' + SUB.Size  + ', ' + SUB.Gender
		
END
