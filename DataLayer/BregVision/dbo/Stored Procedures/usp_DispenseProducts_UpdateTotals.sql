﻿

-- =============================================
-- Author:		Greg Ross
-- Create date: Aug. 17, 2007
-- Description:	Used to update the totals during Dispensing.
-- Modification 20071015 John Bongiorni
--			Update Proc to update Total field
--			on Dispense record
--			based on the ID.
-- =============================================
CREATE PROCEDURE [dbo].[usp_DispenseProducts_UpdateTotals]
	@DispenseID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	UPDATE dbo.Dispense
	SET total = SUB.SumLineTotal
	FROM
	(SELECT 
		  Sum(DD.LineTotal)  AS SumLineTotal
	FROM DispenseDetail AS DD WITH (NOLOCK)
	WHERE DD.DispenseID = @DispenseID
	) AS SUB
	WHERE 	Dispense.DispenseID = @DispenseID
	

	
	-- old code jb 20071022 comment out
--   update Dispense set Total=(select Sum(LineTotal) from DispenseDetail 
--	where dispenseid=@DispenseID) 
--	where dispenseid=@DispenseID
		
END

