﻿
--  Step 2 for Wellington
--  TestPractice's PracticeID is 1

CREATE PROC [dbo].[usp_zImport_TestPractice_2_PracticeCatalogProduct_Insert_From_ThirdPartyProduct] 
as
begin

DECLARE @PracticeID INT
SET @PracticeID = 1  --   TestPractice

INSERT INTO PracticeCatalogProduct
	( PracticeID
	, PracticeCatalogSupplierBrandID
	, ThirdPartyProductID
	, IsThirdPartyProduct
	, WholesaleCost
	, CreatedUserID
	, IsActive
	)

SELECT
		PCSB.PracticeID
	, PCSB.PracticeCatalogSupplierBrandID
	, TPS.ThirdPartyProductID
	, 1 AS IsThirdPartyProduct
	, TPS.WholesaleListCost AS WholesaleCost
	, TPS.CreatedUserID
	, TPS.IsActive
FROM ThirdPartyProduct AS TPS
	INNER JOIN PracticeCatalogSupplierBrand AS PCSB
		ON TPS.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID
	INNER JOIN Practice AS P
		ON P.PracticeID = PCSB.PracticeID
	WHERE P.PracticeID = @PracticeID
	AND TPS.ThirdPartyProductID NOT IN
		(SELECT ThirdPartyProductID 
		 FROM PracticeCatalogProduct 
		 WHERE IsActive = 1 
			AND IsThirdPartyProduct = 1) 

end