﻿
/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Practice_Insert
   
   Description:  Inserts a record into table Practice
   
   AUTHOR:       John Bongiorni 7/25/2007 9:45:20 PM
   
   Modifications: 
   Declare @Pid int 
   exec [usp_Practice_Insert] @Pid, 'test Practice Proc', 1, 0, 0
   
   Added FaxDispenseReceiptEnabled				Ysi             09/07/2013
   ------------------------------------------------------------ */  
   
CREATE PROCEDURE [dbo].[usp_Practice_Insert]
(
      @PracticeID                    INT = NULL OUTPUT
    , @PracticeName                  VARCHAR(50)
    , @CreatedUserID                 INT
    , @IsOrthoSelect				 bit
    , @IsVisionLite					 bit = 0
    , @PaymentTypeID				 INT = 1
    , @CameraScanEnabled			 bit = 0
    , @IsBregBilling				 bit = NULL
    , @EMRIntegrationPullEnabled	 bit = 0
    , @FaxDispenseReceiptEnabled	 bit = 0
	, @IsSecureMessagingInternalEnabled bit = 0
	, @IsSecureMessagingExternalEnabled bit = 0
	, @IsCustomFitEnabled BIT = 0
)
AS
BEGIN
	DECLARE @Err INT

	INSERT
	INTO dbo.Practice
	(
		  PracticeName
        , CreatedUserID
        , IsOrthoSelect
        , IsVisionLite
        , BillingStartDate
        , PaymentTypeID
        , IsActive
        , CameraScanEnabled
        , IsBregBilling
        , EMRIntegrationPullEnabled
        , FaxDispenseReceiptEnabled
		, IsSecureMessagingInternalEnabled
		, IsSecureMessagingExternalEnabled
		, IsCustomFitEnabled
	)
	VALUES
	(
          @PracticeName
        , @CreatedUserID
        , @IsOrthoSelect
        , @IsVisionLite
        , Case When @IsVisionLite = 1 then GETDATE() else '12/31/2019 12:00:00 AM' END
        , @PaymentTypeID
        , 1
        , @CameraScanEnabled
        , @IsBregBilling
        , @EMRIntegrationPullEnabled
        , @FaxDispenseReceiptEnabled
		, @IsSecureMessagingInternalEnabled
		, @IsSecureMessagingExternalEnabled
		, @IsCustomFitEnabled
	)

	SET @Err = @@ERROR
	SELECT @PracticeID = SCOPE_IDENTITY()
	

	RETURN @Err
END
