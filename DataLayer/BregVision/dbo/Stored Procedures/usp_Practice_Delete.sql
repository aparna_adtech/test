﻿
/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_Practice_Delete
   
   Description:  "Deletes" a record from table dbo.Practice
   					This actual flags the IsActive column to false.
					
   AUTHOR:       John Bongiorni 7/25/2007 9:45:21 PM
   
   Modifications:  
   ------------------------------------------------------------ */  

CREATE PROCEDURE [dbo].[usp_Practice_Delete]
(
	  @PracticeID int
	, @ModifiedUserID int
)
AS
BEGIN
	DECLARE @Err INT
	DECLARE @PracticeLocationCount INT   --  Number of Active Locations for the Practice.

		--  Check if the Practice has active PracticeLocations

		SELECT @PracticeLocationCount = COUNT(1)
		FROM dbo.Practice AS P
		INNER JOIN PracticeLocation AS PL
			ON P.PracticeID = PL.PracticeID
			AND P.PracticeID =  @PracticeID
			AND PL.IsActive = 1

		IF (@PracticeLocationCount = 0)  -- Only can delete the Practice if no Active PracticeLocations.
		BEGIN

			UPDATE
				  dbo.Practice

			SET IsActive = 0	  
				, ModifiedUserID = @ModifiedUserID 
			WHERE 
				PracticeID = @PracticeID
		
		END
	
	SET @Err = @@ERROR

	RETURN @Err
End

