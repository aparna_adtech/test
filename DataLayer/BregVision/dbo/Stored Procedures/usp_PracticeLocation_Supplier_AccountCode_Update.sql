﻿/* ------------------------------------------------------------
   PROCEDURE:    dbo.usp_PracticeLocation_Supplier_AccountCode_Update
   
   Description:  Updates a record in the table PracticeLocation_Supplier_AccountCode
   
   AUTHOR:       John Bongiorni 9/26/2007 1:45:52 PM
   
   Modifications:  
   ------------------------------------------------------------ */  ------------------ UPDATE -------------------

CREATE PROCEDURE dbo.usp_PracticeLocation_Supplier_AccountCode_Update
(
	  @PracticeLocationID             INT
	, @PracticeCatalogSupplierBrandID INT
	, @AccountCode                    VARCHAR(50)
	, @ModifiedUserID                 INT
)
AS
BEGIN
	DECLARE @Err INT

	UPDATE dbo.PracticeLocation_Supplier_AccountCode
	SET
		  AccountCode = @AccountCode
		, ModifiedUserID = @ModifiedUserID
		, ModifiedDate = GETDATE()
		, IsActive = 1

	WHERE 
		PracticeLocationID = @PracticeLocationID AND
		PracticeCatalogSupplierBrandID = @PracticeCatalogSupplierBrandID

	SET @Err = @@ERROR

	RETURN @Err
End