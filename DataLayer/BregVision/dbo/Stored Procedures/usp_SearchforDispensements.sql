﻿






-- =============================================
-- Author:		<Author: Greg Ross>
-- Create date: <Create Date: 11/28/2008>
-- Description:	<Description: Master stored procedure for search disepensments >
-- =============================================
-- Exec usp_SearchforDispensements  '', '', 3
CREATE PROCEDURE [dbo].[usp_SearchforDispensements]
	-- Add the parameters for the stored procedure here
	@SearchCriteria varchar(50),
	@SearchText varchar(50),
	@PracticeLocationID int


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--All, Code, Brand

--set @SearchText = @SearchText + '%'
if @SearchCriteria = 'ProductCode'
	begin
		EXEC	[dbo].usp_SearchForDispensementsWithProductCode
		@PracticeLocationID = @PracticeLocationID,	
		@SearchText = @SearchText
	end 
else if @SearchCriteria = 'PatientCode'
	begin
	EXEC	[dbo].usp_SearchForDispensementsWithPatientCode
	@PracticeLocationID = @PracticeLocationID,
	@PatientCodeSearchText = @SearchText
	end 
else if @SearchCriteria = 'PatientName'
	begin
	EXEC	[dbo].usp_SearchForDispensementsWithPatientCode
	@PracticeLocationID = @PracticeLocationID,
	@PatientNameSearchText = @SearchText
	end 
else if @SearchCriteria = 'PhysicianName'
	begin
	EXEC	[dbo].usp_SearchForDispensementsWithPhysicianName
	@PracticeLocationID = @PracticeLocationID,
	@SearchText = @SearchText	  
end 
else if @SearchCriteria = 'DispenseDate'
	begin
 
	EXEC	[dbo].usp_SearchForDispensementsWithDispenseDate
	@PracticeLocationID = @PracticeLocationID,
	@SearchText = @SearchText
    end
else if @SearchCriteria = 'BCSClaimID'
	begin

	EXEC	[dbo].usp_SearchForDispensementsWithClaimNumber
	@PracticeLocationID = @PracticeLocationID,
	@SearchText = @SearchText
    end
else if @SearchCriteria = 'All' OR @SearchCriteria = 'Browse' OR @SearchCriteria = ''
	begin
	 
	EXEC	[dbo].usp_SearchForDispensementsAll
	@PracticeLocationID = @PracticeLocationID,
	@SearchText = @SearchText
    end

END