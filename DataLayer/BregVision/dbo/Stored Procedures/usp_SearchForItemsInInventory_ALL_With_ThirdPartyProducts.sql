﻿



/* ------------------------------------------------------------
   PROCEDURE:    [dbo].[usp_GetAllInventoryDataforPCToIventory_ALL_With_ThirdPartyProducts]
   
   Description:  Selects all products (mc and 3rdParty) that are in inventory given a practiceID
					and IsActice of 0 or 1 (false or true)
					This is called from the proc:
						usp_GetAllInventoryDataforPCToIventory						
   
   AUTHOR:       John Bongiorni 10/9/2007 2:36:40 PM
   
   Modifications:  Get the PCSBID and the swicth the alias supplierID to PracticeCatalogSupplierBrandID 
					Added column dbo.ConcatHCPCS(SUB.PracticeCatalogProductID) AS HCPCSString
   ------------------------------------------------------------ */   


CREATE PROCEDURE [dbo].[usp_SearchForItemsInInventory_ALL_With_ThirdPartyProducts]

	@PracticeLocationID int,
	@IsActive bit

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--   NOCOUNT ON


			SELECT 
				  Sub.ProductInventoryID
				, Sub.SupplierShortName
				, Sub.BrandShortName
				
				, SUB.PracticeCatalogProductID			
				, SUB.SupplierID			--.PracticeCatalogSupplierBrandID					
--				, SUB.OldSupplierID				
				, SUB.Category
				, SUB.Product
				, SUB.Code
				
				, ISNULL(SUB.Packaging, '') AS Packaging
				, ISNULL(SUB.LeftRightSide, '') AS Side
				, ISNULL(SUB.Size, '') AS Size
				, ISNULL(SUB.Gender, '') AS Gender
				
				, SUB.QOH
				, SUB.ParLevel
				, SUB.ReorderLevel
				, SUB.CriticalLevel
				, SUB.WholesaleCost
				, SUB.IsActive
				, SUB.IsThirdPartyProduct
				, dbo.ConcatHCPCS(SUB.PracticeCatalogProductID) AS HCPCSString
				, SUB.OldMasterCatalogProductID
				, SUB.OldThirdPartyProductID
				, SUB.NewMasterCatalogProductID
				, SUB.NewThirdPartyProductID
				, SUB.NewPracticeCatalogProductID
				, SUB.Name AS NewProductName
				, SUB.NewProductCode
				, SUB.NewPracticeID
			INTO #WorkingTable
			FROM
			(
					SELECT	-- 249 active and inactive mastercatalogproducts in inventory WORKS!!!
 
							PCSB.SupplierShortName, 
							PCSB.BrandShortName, 
							
							PCSB.PracticeCatalogSupplierBrandID AS SupplierID, 
							
							PCP.PracticeCatalogProductID,		
							
                            PCSB.MasterCatalogSupplierID AS OldSupplierID, 
                            MCC.[NAME] AS Category,
                            --PCSB.BrandShortName AS Category,   --  This column should be unneccessary
																--  Only here to preserve the existing application code.
						    MCP.[Name]			AS Product,
                            
                            MCP.[Code]			AS Code,
                            MCP.[Packaging]		AS Packaging,
                            MCP.[LeftRightSide] AS LeftRightSide,
                            MCP.[Size]			AS Size,
                            MCP.[Gender]		AS Gender,
							
							PI.QuantityOnHandPerSystem AS QOH,
							PI.ParLevel,
							PI.ReorderLevel,
							PI.CriticalLevel,
							PI.ProductInventoryID,
							PCP.WholesaleCost,
							PI.Isactive,
							0 AS IsThirdPartyProduct,

							PR.OldMasterCatalogProductID,
							PR.OldThirdPartyProductID,
							PR.NewMasterCatalogProductID,
							PR.NewThirdPartyProductID,
							MAX(CASE WHEN (NPI.PracticeLocationID IS NOT NULL) THEN NPCP.PracticeCatalogProductID ELSE 0 END) AS NewPracticeCatalogProductID,
							NMCP.Name,
							NMCP.Code AS NewProductCode,
							PR.PracticeID AS NewPracticeID
                     
                    FROM dbo.PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
                    INNER JOIN [PracticeCatalogProduct] AS PCP WITH(NOLOCK)  ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
                    INNER JOIN [ProductInventory] AS PI WITH(NOLOCK) ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
                    INNER JOIN [MasterCatalogProduct] AS MCP  WITH(NOLOCK) ON PCP.[MasterCatalogProductID] = MCP.[MasterCatalogProductID]
					-- the following two inner join can be removed later		
					INNER JOIN dbo.MasterCatalogsubCategory AS MCSC ON MCP.MasterCatalogsubCategoryID = MCSC.MasterCatalogsubCategoryID
					INNER JOIN dbo.MasterCatalogCategory AS MCC ON MCSC.MasterCatalogCategoryID = MCC.MasterCatalogCategoryID
					LEFT OUTER JOIN ProductReplacement AS PR WITH(NOLOCK) ON PR.IsActive = 1 AND
						(PR.OldMasterCatalogProductID = PCP.MasterCatalogProductID) AND
						(PR.PracticeID IS NULL OR PR.PracticeID = PCP.PracticeID)
					LEFT JOIN MasterCatalogProduct NMCP ON NMCP.MasterCatalogProductID = PR.NewMasterCatalogProductID
					LEFT JOIN PracticeCatalogProduct NPCP ON NPCP.MasterCatalogProductID = PR.NewMasterCatalogProductID
					LEFT JOIN ProductInventory NPI ON NPI.PracticeCatalogProductID = NPCP.PracticeCatalogProductID AND NPI.PracticeLocationID = @PracticeLocationID
                                                 
                    WHERE  PI.[PracticeLocationID] = @PracticeLocationID
							AND PCP.IsThirdPartyProduct = 0  -- MasterCatalogProductsOnly
							AND PCSB.IsActive =	1
							AND MCP.IsActive = 1
							AND MCP.IsDiscontinued = 0
							AND PCP.IsActive = 1
							AND PI.IsActive = @IsActive
							AND PCP.IsActive = @IsActive

					GROUP BY
							PCSB.SupplierShortName, 
							PCSB.BrandShortName, 
							PCSB.PracticeCatalogSupplierBrandID,
							PCP.PracticeCatalogProductID,		
                            PCSB.MasterCatalogSupplierID,
                            MCC.[NAME],
						    MCP.[Name],
                            MCP.[Code],
                            MCP.[Packaging],
                            MCP.[LeftRightSide],
                            MCP.[Size],
                            MCP.[Gender],
							PI.QuantityOnHandPerSystem,
							PI.ParLevel,
							PI.ReorderLevel,
							PI.CriticalLevel,
							PI.ProductInventoryID,
							PCP.WholesaleCost,
							PI.Isactive,
							PR.OldMasterCatalogProductID,
							PR.OldThirdPartyProductID,
							PR.NewMasterCatalogProductID,
							PR.NewThirdPartyProductID,
							NMCP.Name,
							NMCP.Code,
							PR.PracticeID

				UNION

					--  Third Party Product Info
					SELECT		-- 249 active and inactive mastercatalogproducts in inventory WORKS!!!
 
							PCSB.SupplierShortName, 
							PCSB.BrandShortName, 
							
							PCSB.PracticeCatalogSupplierBrandID AS SupplierID, 
				
							PCP.PracticeCatalogProductID,		       
                            PCSB.ThirdPartySupplierID AS OldSupplierID,                        
                            'NA' AS Category,
                            --PCSB.BrandShortName AS Category,   --  This column should be unneccessary
																--  Only here to preserve the existing application code.
						    TPP.[Name]			AS Product,                            
                            TPP.[Code]			AS Code,
                            TPP.[Packaging]		AS Packaging,
                            TPP.[LeftRightSide] AS LeftRightSide,
                            TPP.[Size]			AS Size,
                            TPP.[Gender]		AS Gender,
							
							PI.QuantityOnHandPerSystem AS QOH,
							PI.ParLevel,
							PI.ReorderLevel,
							PI.CriticalLevel,
							PI.ProductInventoryID,
							PCP.WholesaleCost,						
							PI.Isactive,							
							1 AS IsThirdPartyProduct,

							PR.OldMasterCatalogProductID,
							PR.OldThirdPartyProductID,
							PR.NewMasterCatalogProductID,
							PR.NewThirdPartyProductID,
							MAX(CASE WHEN (NPI.PracticeLocationID IS NOT NULL) THEN NPCP.PracticeCatalogProductID ELSE 0 END) AS NewPracticeCatalogProductID,
							NTPP.Name,
							NTPP.Code AS NewProductCode,
							PR.PracticeID AS NewPracticeID
                     
                    FROM dbo.PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
                    INNER JOIN [PracticeCatalogProduct] AS PCP WITH(NOLOCK) ON PCSB.PracticeCatalogSupplierBrandID = PCP.PracticeCatalogSupplierBrandID
                    INNER JOIN [ProductInventory] AS PI WITH(NOLOCK) ON PCP.[PracticeCatalogProductID] = PI.[PracticeCatalogProductID]
                    INNER JOIN [ThirdPartyProduct] AS TPP  WITH(NOLOCK) ON PCP.[ThirdPartyProductID] = TPP.[ThirdPartyProductID]
					LEFT OUTER JOIN ProductReplacement AS PR WITH(NOLOCK) ON PR.IsActive = 1 AND
						(PR.OldThirdPartyProductID = TPP.ThirdPartyProductID) AND
						(PR.PracticeID IS NULL OR PR.PracticeID = PCP.PracticeID)
					LEFT JOIN ThirdPartyProduct NTPP ON NTPP.ThirdPartyProductID = PR.NewThirdPartyProductID
					LEFT JOIN PracticeCatalogProduct NPCP ON NPCP.ThirdPartyProductID = PR.NewThirdPartyProductID
					LEFT JOIN ProductInventory NPI ON NPI.PracticeCatalogProductID = NPCP.PracticeCatalogProductID AND NPI.PracticeLocationID = @PracticeLocationID

                    WHERE  PI.[PracticeLocationID] = @PracticeLocationID
							AND PCP.IsThirdPartyProduct = 1		-- ThirdPartyProductsOnly 
							AND PCSB.IsActive =	1
							AND TPP.IsActive = 1
							AND TPP.IsDiscontinued = 0
							AND PCP.IsActive = 1
							AND PI.IsActive = @IsActive
							AND PCP.IsActive = @IsActive

					GROUP BY
							PCSB.SupplierShortName, 
							PCSB.BrandShortName, 
							PCSB.PracticeCatalogSupplierBrandID, 
							PCP.PracticeCatalogProductID,		       
                            PCSB.ThirdPartySupplierID,                        
						    TPP.[Name],                            
                            TPP.[Code],
                            TPP.[Packaging],
                            TPP.[LeftRightSide],
                            TPP.[Size],
                            TPP.[Gender],
							PI.QuantityOnHandPerSystem,
							PI.ParLevel,
							PI.ReorderLevel,
							PI.CriticalLevel,
							PI.ProductInventoryID,
							PCP.WholesaleCost,						
							PI.Isactive,							
							PR.OldMasterCatalogProductID,
							PR.OldThirdPartyProductID,
							PR.NewMasterCatalogProductID,
							PR.NewThirdPartyProductID,
							NTPP.Name,
							NTPP.Code,
							PR.PracticeID
					
				) AS Sub

		SELECT
			OldMasterCatalogProductID,
			OldThirdPartyProductID
		INTO #WorkingTablePracticeSpecific
		FROM #WorkingTable
		WHERE NewPracticeID IS NOT NULL

        SELECT * FROM #WorkingTable WT
		WHERE NOT EXISTS (
			SELECT * FROM #WorkingTablePracticeSpecific WTPS
			WHERE
				COALESCE(WT.OldMasterCatalogProductID, 0) = COALESCE(WTPS.OldMasterCatalogProductID, 0) AND
				COALESCE(WT.OldThirdPartyProductID, 0) = COALESCE(WTPS.OldThirdPartyProductID, 0) AND
                WT.NewPracticeID IS NULL
				)

        ORDER BY
			  IsThirdPartyProduct
			, BrandShortName
			, PracticeCatalogProductID
			, Product
			, Code
END
