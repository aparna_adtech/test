﻿-- =============================================
-- Author:		Greg Ross
-- Create date: 5/5/08
-- Description:	Used to get all information so that user can choose a historical dispensement to view or print
-- =============================================
--usp_ReceiptHistory 3, 'PhysicianName', 'Alleyne'
--usp_ReceiptHistory 3, 'PhysicianName', 'kane'
--usp_ReceiptHistory 3, 'PhysicianName', 'rogers'
--usp_ReceiptHistory 3, 'PatientCode', 'Sneen'
--usp_ReceiptHistory 3, '', ''
--usp_ReceiptHistory 3, 'Batch', '106'
CREATE PROCEDURE [dbo].[usp_ReceiptHistory_20101124]
	-- Add the parameters for the stored procedure here
	@PracticeLocationID int
	, @SearchCriteria varchar(50)
	, @SearchText  varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	/*These variables are used in conjunction with the coalesce statements in the where clause.
	each of these variables will be null except one is @SearchCriteria matches.*/
	Declare @PatientCode varchar(50)			--NULL   These default to null
	Declare @PhysicianName varchar(50)
	Declare @DispenseBatchID int = 0
	
	if @SearchCriteria='PatientCode'
		Set @PatientCode = '%' +  @SearchText + '%'	
	if @SearchCriteria='PhysicianName'
		Set @PhysicianName = '%' +  @SearchText + '%'	
	if @SearchCriteria='Batch'
		Set @DispenseBatchID = CAST(@SearchText as int)

  select d.DispenseID
		, CASE WHEN d.CreatedWithHandheld is Null or d.CreatedWithHandheld=0 THEN 'No'
		ELSE 'Yes'
		End as CreatedWithHandheld		

		,d.DispenseBatchID
		,d.PatientCode
		,d.Total
		,d.DateDispensed
		,HasABNForm = CAST( Coalesce((Select top 1 dd.ABNForm from DispenseDetail dd where dd.DispenseID = d.DispenseID AND dd.ABNForm = 1), 0) as bit)
	from Dispense d 
	where d.practicelocationid=@PracticeLocationID --PLID
	AND COALESCE(d.PatientCode, '') like COALESCE(@PatientCode , d.PatientCode, '')
	AND (
			(
				(@PhysicianName IS NOT NULL) AND (d.DispenseID in (Select distinct dd.DispenseID from DispenseDetail dd 
					join Physician subP on dd.PhysicianID = subP.PhysicianID
					join Contact subC on subP.ContactID = subC.ContactID
					where COALESCE(subC.LastName, '') like COALESCE(@PhysicianName , subC.LastName, '')))
			)
			OR (@PhysicianName IS NULL)
					
		)
	AND (
			((@DispenseBatchID > 0) AND (d.DispenseBatchID = @DispenseBatchID))
			OR
			(@DispenseBatchID = 0)
		)
	order by d.DateDispensed desc
	Print case when @PhysicianName is null then '1' else '2' end
END
