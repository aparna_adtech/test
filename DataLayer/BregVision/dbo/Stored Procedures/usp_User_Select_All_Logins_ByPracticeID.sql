﻿/*
EXEC usp_User_Select_All_Logins_ByPracticeID 1
Get all active User Login Info


Michael Sneen 4/2/2009 *****


*/


CREATE PROCEDURE [dbo].[usp_User_Select_All_Logins_ByPracticeID]
		@PracticeID INT
AS
BEGIN
	SELECT
		U.UserID,
		U.AspNet_UserID,
		AU.UserName,
		AM.Email,
		U.ShowProductInfo,
		U.AllowDispense,
		U.AllowInventory,
		U.AllowCheckIn,
		U.AllowReports,
		U.AllowCart,
		U.allowDispenseModification,
		U.AllowPracticeUserOrderWithoutApproval,
		U.ScrollEntirePage,
		U.DispenseQueueSortDescending,
		U.AllowInventoryClose,
		U.DefaultLocationID,
		U.AllowZeroQtyDispense,
		U.IsActive,
		ISNULL(U.DefaultRecordsPerPage, 10) as DefaultRecordsPerPage,
		U.DefaultDispenseSearchCriteria,
    U.BcsUser,
		Pr.PracticeID,
		UserRole = (Select Top 1 AR.RoleName from [aspnetdb].[dbo].[aspnet_Roles] AR Inner Join [aspnetdb].[dbo].[aspnet_UsersInRoles] UIR ON AR.RoleId = UIR.RoleId Where UIR.UserId = U.AspNet_UserID),
		AM.IsLockedOut
	FROM
		dbo.[User] AS U
		INNER JOIN dbo.Practice AS Pr
			ON U.PracticeID = Pr.PracticeID
		INNER JOIN[aspnetdb].[dbo].[aspnet_Users] as AU
			ON U.AspNet_UserID = AU.UserId
		Left Outer JOIN [aspnetdb].[dbo].[aspnet_Membership] as AM
			ON U.AspNet_UserID = AM.UserId
	WHERE
			Pr.PracticeID = @PracticeID
		--AND U.IsActive = 1
	Order BY
		U.IsActive DESC,
		AU.UserName

END
