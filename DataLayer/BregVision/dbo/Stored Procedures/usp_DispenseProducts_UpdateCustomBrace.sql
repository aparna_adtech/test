﻿

-- =============================================
-- Author:		Mike Sneen
-- Create date: 6/15, 2009
-- Description:	Used to update the Custom Brace during Dispensing.
-- Modification 20071015 John Bongiorni
--			Update Proc to update Total field
--			on Dispense record
--			based on the ID.
-- =============================================
Create PROCEDURE [dbo].[usp_DispenseProducts_UpdateCustomBrace]
	@DispenseID int,
	@BregVisionOrderID int
AS
BEGIN

	SET NOCOUNT ON;


	UPDATE dbo.ShoppingCartCustomBrace
	SET dbo.ShoppingCartCustomBrace.DispenseID = @DispenseID
	where dbo.ShoppingCartCustomBrace.BregVisionOrderID = @BregVisionOrderID

		
END

