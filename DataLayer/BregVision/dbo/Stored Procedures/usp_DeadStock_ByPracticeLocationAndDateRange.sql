﻿




CREATE PROCEDURE [dbo].[usp_DeadStock_ByPracticeLocationAndDateRange]
	-- Add the parameters for the stored procedure here
	@PracticeLocationID NVARCHAR(MAX),
	@SupplierID NVARCHAR(MAX),
	@StartDate DATETIME,
	@EndDate DATETIME,
	@IsConsignment BIT = 0,
	@PracticeID INT,
	@InternalPracticeLocationID NVARCHAR(MAX),
	@InternalSupplierID NVARCHAR(MAX)
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--- begin multi-value @PracticeLocationID parsing
	DECLARE @locations AS TABLE ( [seq] INT, [location] INT );

	-- If the Internal parameter is "ALL", we select all of the Practice Locations
	-- for the passed PracticeID.
	-- Otherwise, split as we were doing before.
	IF(@InternalPracticeLocationID = 'ALL')
	BEGIN

		DECLARE @tempLocations TABLE (
			PracticeLocationID INT
			,PracticeLocationName VARCHAR(50)
			,IsPrimaryLocation BIT
			);

		INSERT @tempLocations
		EXEC dbo.usp_PracticeLocation_Select_All_ID_Name_Given_PracticeID @PracticeID = @PracticeID;

		INSERT @locations (
			[location]
			,[seq]
			)
		SELECT PracticeLocationID
			,ROW_NUMBER() OVER (ORDER BY PracticeLocationID)
		FROM @tempLocations;

	END;
	ELSE
	BEGIN

		WITH pieces([seq], [start], [stop])
		AS
		(
			SELECT 1, 1, CHARINDEX(',', @PracticeLocationID)
			UNION ALL
			SELECT CAST([seq] + 1 AS INT), CAST([stop] + 1 AS INT), CHARINDEX(',', @PracticeLocationID, [stop] + 1)
			FROM pieces
			WHERE [stop] > 0
		)
		INSERT INTO @locations
		SELECT
			[seq],
			CAST(SUBSTRING(@PracticeLocationID, [start], CASE WHEN [stop] > 0 THEN [stop] - start ELSE 512 END) AS INT)
		FROM
			pieces;

	END;
	--- end multi-value @PracticeLocationID parsing
	
	--- begin multi-value @SupplierID parsing
	DECLARE @suppliers as TABLE ( [seq] INT, [supplier] INT );

	-- If the Internal parameter is "ALL", we select all of the Suppliers 
	-- for the passed Practice Locations (taking care to respect the "ALL" possibility from
	-- that as well).
	-- Otherwise, we split as before.
	IF(@InternalSupplierID = 'ALL')
	BEGIN
	 -- if all practice locations, get the list from our table variable and join together
		DECLARE @locationList NVARCHAR(MAX);
		IF(@InternalPracticeLocationID = 'ALL')
		BEGIN

			SELECT @locationList = COALESCE(@locationList + CAST(location AS NVARCHAR(10)) + ',', '')
			FROM @locations;

			IF(LEN(@locationList) > 0)
				SET @locationList = LEFT(@locationList, LEN(@locationList) - 1);

		END;
		ELSE
		BEGIN
			
			SET @locationList = @PracticeLocationID;

		END;

		DECLARE @tempSuppliers TABLE ([Brand] VARCHAR(50), [SupplierID] INT, [FullName] VARCHAR(MAX), [Name] VARCHAR(MAX), [LastName] VARCHAR(MAX), [PhoneWork] VARCHAR(MAX), [Address] VARCHAR(MAX), [City] VARCHAR(MAX), [Zipcode] VARCHAR(20), [IsThirdPartySupplier] BIT);

		INSERT @tempSuppliers
		EXEC dbo.usp_GetSuppliers_MultiLocations @PracticeLocationID = @locationList;

		INSERT @suppliers ([supplier], [seq])
		SELECT SupplierID, ROW_NUMBER() OVER (ORDER BY SupplierID)
		FROM @tempSuppliers;

	END;
	ELSE
	BEGIN
	WITH pieces([seq], [start], [stop])
	AS
	(
		SELECT 1, 1, CHARINDEX(',', @SupplierID)
		UNION ALL
		SELECT CAST([seq] + 1 AS INT), CAST([stop] + 1 AS INT), CHARINDEX(',', @SupplierID, [stop] + 1)
		FROM pieces
		WHERE [stop] > 0
	)
	INSERT INTO @suppliers
	SELECT
		[seq],
		CAST(SUBSTRING(@SupplierID, [start], CASE WHEN [stop] > 0 THEN [stop] - start ELSE 512 END) AS INT)
	FROM
		pieces;
	END;
	--- end multi-value @SupplierID parsing

	SET NOCOUNT OFF
	
	SELECT
		PL.Name AS PracticeLocation,
		COALESCE(MCS.[SupplierName], PCSB.SupplierName) AS SupplierName,
        COALESCE(MCP.name, TPP.Name) AS [Name],
        COALESCE(MCP.[Code], TPP.[Code]) AS [Code],
        PI.[QuantityOnHandPerSystem],
		PI.[ConsignmentQuantity],
        PI.[ParLevel],
        PI.[ReorderLevel],
        PI.[CriticalLevel],
        PI.[ConsignmentQuantity],
        SideSizeGender = (CAST(ISNULL(MCP.LeftRightSide, 'NA') AS VARCHAR(10) ) 
							+ ', ' 
							+ CAST(ISNULL(MCP.Size, 'NA') AS VARCHAR(50) ) 
							+ ', ' 
							+ CAST(ISNULL(MCP.Gender, 'NA')	AS VARCHAR(50) )),
		COALESCE(MCP.masterCatalogProductID, TPP.ThirdPartyProductID) AS masterCatalogProductID,
		PI.IsConsignment
	FROM
		[ProductInventory] PI WITH (NOLOCK)
			INNER JOIN [PracticeCatalogProduct] PCP WITH (NOLOCK)
				ON PI.[PracticeCatalogProductID] = PCP.[PracticeCatalogProductID]
			LEFT OUTER JOIN 
				( [MasterCatalogProduct] MCP WITH (NOLOCK)
					INNER JOIN [MasterCatalogSubCategory] MCSC WITH (NOLOCK)
						ON MCP.[MasterCatalogSubCategoryID] = MCSC.[MasterCatalogSubCategoryID]
					INNER JOIN [MasterCatalogCategory] MCC WITH (NOLOCK)
						ON MCSC.[MasterCatalogCategoryID] = MCC.[MasterCatalogCategoryID]
					INNER JOIN [MasterCatalogSupplier] MCS WITH (NOLOCK)
						ON MCC.[MasterCatalogSupplierID] = MCS.[MasterCatalogSupplierID] 
				)
			ON PCP.[MasterCatalogProductID] = MCP.[MasterCatalogProductID] 
			LEFT OUTER JOIN [ThirdPartyProduct] TPP WITH (NOLOCK)
				ON PCP.ThirdPartyProductID = TPP.ThirdPartyProductID 
			LEFT OUTER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK)
				ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID AND PCP.IsThirdPartyProduct = 1
			INNER JOIN PracticeLocation PL 
				ON PI.PracticeLocationID = PL.PracticeLocationID
	WHERE
		PCP.[PracticeCatalogProductID] NOT IN
		(
			SELECT
				[PracticeCatalogProductID]
			FROM
				[Dispense] WITH (NOLOCK)
					INNER JOIN [DispenseDetail] WITH (NOLOCK)
						ON [Dispense].[DispenseID] = [DispenseDetail].[DispenseID]
			WHERE
				[PracticeLocationID] IN (SELECT location FROM @locations)
				AND [DateDispensed] BETWEEN @StartDate AND @EndDate
        )
        AND PL.[PracticeLocationID] IN (SELECT location FROM @locations)
        AND
        (
			PCP.[PracticeCatalogSupplierBrandID] IN (SELECT supplier FROM @suppliers)
			OR PCSB.[ThirdPartySupplierID] IN (SELECT supplier FROM @suppliers)
		)
        --Next line added to make sure items with zero quantity aren't included in dead stock - GR - 5/23/08
		AND PI.QuantityOnHandPerSystem <> 0
		AND PI.[IsActive] = 1
        AND PCP.[IsActive] = 1
        AND ((
				MCP.IsActive = 1
		        AND MCSC.[IsActive] = 1
		        AND MCC.[IsActive] = 1
				AND MCS.[IsActive] = 1
				AND pl.[IsActive] = 1
			 ) OR TPP.[IsActive] = 1)
        AND ( 
				@IsConsignment = 0
				OR
				(
					@IsConsignment = 1
					AND
					PI.IsConsignment = 1
				)
			)
	ORDER BY
		[SupplierName],
        NAME

END
