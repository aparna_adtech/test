﻿ALTER ROLE [db_owner] ADD MEMBER [VISION\IUSR_BIMS];


GO
ALTER ROLE [db_owner] ADD MEMBER [Vision\BregVisionExecReportViewer];


GO
ALTER ROLE [db_owner] ADD MEMBER [NT AUTHORITY\NETWORK SERVICE];


GO
ALTER ROLE [db_owner] ADD MEMBER [VISION\BregVisionBregAdmins];


GO
ALTER ROLE [db_owner] ADD MEMBER [BregVisionWebApp];


GO
ALTER ROLE [db_owner] ADD MEMBER [bvteam];


GO
ALTER ROLE [db_owner] ADD MEMBER [VISION\gross];


GO
ALTER ROLE [db_owner] ADD MEMBER [VISION\ReplicationUser];


GO
ALTER ROLE [db_ddladmin] ADD MEMBER [BregVisionWebApp];


GO
ALTER ROLE [db_datareader] ADD MEMBER [rhaywood];


GO
ALTER ROLE [db_datareader] ADD MEMBER [BregVisionWebApp];


GO
ALTER ROLE [db_datareader] ADD MEMBER [klord];


GO
ALTER ROLE [db_datareader] ADD MEMBER [BregVisionReader];


GO
ALTER ROLE [db_datareader] ADD MEMBER [ttrivison];


GO
ALTER ROLE [db_datareader] ADD MEMBER [visionapp];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [BregVisionWebApp];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [klord];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [ttrivison];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [visionapp];


GO
ALTER ROLE [db_denydatawriter] ADD MEMBER [BregVisionReader];

