﻿create view AllActiveMasterCatalogUPCCodes
as
	select
		MasterCatalogProductID,
		dbo.ConcatMasterUPCCodes(MasterCatalogProductID) as Code
	from UPCCode
	where IsActive=1 and MasterCatalogProductID is not null
	group by MasterCatalogProductID

