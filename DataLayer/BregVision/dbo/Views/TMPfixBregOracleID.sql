﻿

CREATE view [dbo].[TMPfixBregOracleID]
as
select
--sa.PracticeLocationID
--,sa.AddressID,
p.PracticeName + ' (' + cast(p.PracticeID as varchar) + ')' [Practice]
,pl.Name + ' (' + cast(pl.PracticeLocationID as varchar) + ')' [Location]
,t1.AccountCode
,a.AddressLine1
,a.AddressLine2
--,a.City
--,a.[State]
--,a.ZipCode
,sa.BregOracleID
,sa.BregOracleIDInvalid
from
[PracticeLocationShippingAddress] sa
left join [Address] a on sa.AddressID=a.AddressID
left join PracticeLocation pl on sa.[PracticeLocationID]=pl.[PracticeLocationID]
left join Practice p on p.PracticeID=pl.PracticeID
left join
(
select
c.PracticeLocationID
,c.AccountCode
from
[dbo].[PracticeLocation_Supplier_AccountCode] c
left join [dbo].[PracticeCatalogSupplierBrand] b on c.[PracticeCatalogSupplierBrandID]=b.[PracticeCatalogSupplierBrandID]
where
b.SupplierName like '%breg%'
and b.ThirdPartySupplierID is null
and b.IsActive=1
and c.IsActive=1
) t1 on pl.PracticeLocationID=t1.PracticeLocationID
where
sa.BregOracleID is null
and p.PracticeName not like '%test%'
and p.PracticeID not in (1,3,123,142,144,145,146,147,148,155,163,167,180,189,221,224)
and p.IsActive=1
and pl.IsActive=1
and sa.IsActive=1
and a.IsActive=1

