﻿
CREATE VIEW [dbo].[vwPracticeCatalog]
AS
select
	Sub.PracticeID, 
	Sub.PracticecatalogProductID,
	Sub.MasterCatalogProductID	AS MasterCatalogProductID,  -- Needs to be called ProductID (to handle MC and ThirdParty)
	Sub.PracticeCatalogSupplierBrandID,

--	SUB.Sequence,  -- jb

	Sub.ShortName,
	Sub.Code,
	ISNULL(Sub.LeftRightSide, ' ') + ', ' + ISNULL(Sub.Size, ' ') AS SideSize,
	ISNULL(Sub.LeftRightSide, ' ') AS LeftRightSide,
	ISNULL(Sub.Size, ' ') AS Size,
	ISNULL(Sub.Gender, ' ') AS Gender,
	Sub.WholesaleCost,
	Sub.BillingCharge,
	Sub.DMEDeposit,
	Sub.BillingChargeCash,
	Sub.StockingUnits,
	Sub.BuyingUnits,
                Sub.ProductName,
                Sub.BrandName,
                Sub.BrandShortName,
	Sub.IsThirdPartyProduct,
	dbo.ConcatHCPCS(Sub.PracticeCatalogProductID) AS HCPCSString -- JB 20071004 Added Column for UDF Concat of HCPCS

						-- Need to be put in app code and grid to distinguish 
												--  Master Catalog Product and Third Party Product.
												
	, CASE (SELECT COUNT(1) 
	FROM productinventory AS PI WITH (NOLOCK) 
	WHERE PI.practiceCatalogProductID = sub.practiceCatalogProductID
    ) WHEN 0 THEN 1 ELSE 0 END AS IsDeletable   --  JB  20080225  Added Column for "Deletable" PCPs
												
	
FROM
(
		select
			PCP.PracticeID, 
			PCP.PracticecatalogProductID,
			PCP.MasterCatalogProductID	AS MasterCatalogProductID,  -- Needs to be ProductID
			PCP.PracticeCatalogSupplierBrandID,

			--ISNULL(PCSB.Sequence, 999) AS Sequence,  -- jb

			MCP.ShortName,
			MCP.Code,
			MCP.LeftRightSide,
			MCP.Size,
			MCP.Gender,
			PCP.WholesaleCost,
			PCP.BillingCharge,
			PCP.DMEDeposit,
			PCP.BillingChargeCash,
			PCP.StockingUnits,
			PCP.BuyingUnits,
                                                MCP.Name as ProductName,
                                                PCSB.BrandName,
                                                PCSB.BrandShortName,
			0 AS IsThirdPartyProduct					-- Needs to know if this is a Third Party Product.
		 from PracticeCatalogProduct AS PCP WITH(NOLOCK) 
		inner join MasterCatalogProduct AS MCP WITH(NOLOCK) 
			on PCP.MasterCatalogProductID = MCP.MasterCatalogProductID
		 INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK) 
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

		where  PCP.IsThirdPartyProduct = 0		-- MasterCatalogProduct
			and PCP.isactive = 1
			and MCP.isactive = 1
			

		UNION

			select
			PCP.PracticeID, 
			PCP.PracticecatalogProductID,
			PCP.ThirdPartyProductID		 AS MasterCatalogProductID,  -- Needs to be ProductID
			PCP.PracticeCatalogSupplierBrandID,

			--ISNULL(PCSB.Sequence, 999) AS Sequence,  -- jb

			TPP.ShortName,
			TPP.Code,
			TPP.LeftRightSide,
			TPP.Size,
			TPP.Gender,
			PCP.WholesaleCost,
			PCP.BillingCharge,
			PCP.DMEDeposit,
			PCP.BillingChargeCash,
			PCP.StockingUnits,
			PCP.BuyingUnits,
                                                TPP.Name as ProductName,
                                                PCSB.BrandName,
                                                PCSB.BrandShortName,
			1 AS IsThirdPartyProduct					-- Needs to know if this is a Third Party Product.
		 from PracticeCatalogProduct AS PCP  WITH(NOLOCK) 
		inner join ThirdPartyProduct AS TPP  WITH(NOLOCK) 
			on PCP.ThirdPartyProductID = TPP.ThirdPartyProductID 
		 INNER JOIN PracticeCatalogSupplierBrand AS PCSB WITH(NOLOCK) 
			ON PCP.PracticeCatalogSupplierBrandID = PCSB.PracticeCatalogSupplierBrandID

		where       PCP.IsThirdPartyProduct = 1		-- ThirdPartyProduct
			and PCP.isactive = 1
			and TPP.isactive = 1

) AS Sub
 


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[40] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[66] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2) )"
      End
      ActivePaneConfig = 14
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      PaneHidden = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwPracticeCatalog';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwPracticeCatalog';

