﻿CREATE VIEW xxxBregAccountNumbers
as
SELECT DISTINCT
p.PracticeID
,p.PracticeName
,plsac.AccountCode
FROM
Practice p
,PracticeLocation pl
,PracticeLocation_Supplier_AccountCode plsac
,PracticeCatalogSupplierBrand pcsb
WHERE
p.PracticeID=pl.PracticeID
AND pl.PracticeLocationID=plsac.PracticeLocationID
AND plsac.PracticeCatalogSupplierBrandID=pcsb.PracticeCatalogSupplierBrandID
AND pcsb.IsThirdPartySupplier=0
AND (pcsb.MasterCatalogSupplierID IS NULL OR pcsb.MasterCatalogSupplierID=1)
AND p.IsActive=1
AND pl.IsActive=1
