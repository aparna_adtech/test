﻿
CREATE VIEW [dbo].[Physician]
AS
SELECT        ClinicianID AS PhysicianID, PracticeID, ContactID, CreatedUserID, CreatedDate, ModifiedUserID, ModifiedDate, IsActive, SortOrder, PIN, IsProvider, IsFitter, NPI
FROM            dbo.Clinician
WHERE        (IsProvider = 1)
