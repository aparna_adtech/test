﻿
USE [master]
GO

/****** Object:  Database [BregVisionIntegrations]    Script Date: 4/3/2017 16:48:50 ******/
CREATE DATABASE [BregVisionIntegrations]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BregVisionIntegrations', FILENAME = N'T:\BregVisionIntegrations.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'BregVisionIntegrations_log', FILENAME = N'S:\BregVisionIntegrations_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO

ALTER DATABASE [BregVisionIntegrations] SET COMPATIBILITY_LEVEL = 130
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BregVisionIntegrations].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [BregVisionIntegrations] SET ANSI_NULL_DEFAULT OFF
GO

ALTER DATABASE [BregVisionIntegrations] SET ANSI_NULLS OFF
GO

ALTER DATABASE [BregVisionIntegrations] SET ANSI_PADDING OFF
GO

ALTER DATABASE [BregVisionIntegrations] SET ANSI_WARNINGS OFF
GO

ALTER DATABASE [BregVisionIntegrations] SET ARITHABORT OFF
GO

ALTER DATABASE [BregVisionIntegrations] SET AUTO_CLOSE OFF
GO

ALTER DATABASE [BregVisionIntegrations] SET AUTO_SHRINK OFF
GO

ALTER DATABASE [BregVisionIntegrations] SET AUTO_UPDATE_STATISTICS ON
GO

ALTER DATABASE [BregVisionIntegrations] SET CURSOR_CLOSE_ON_COMMIT OFF
GO

ALTER DATABASE [BregVisionIntegrations] SET CURSOR_DEFAULT  GLOBAL
GO

ALTER DATABASE [BregVisionIntegrations] SET CONCAT_NULL_YIELDS_NULL OFF
GO

ALTER DATABASE [BregVisionIntegrations] SET NUMERIC_ROUNDABORT OFF
GO

ALTER DATABASE [BregVisionIntegrations] SET QUOTED_IDENTIFIER OFF
GO

ALTER DATABASE [BregVisionIntegrations] SET RECURSIVE_TRIGGERS OFF
GO

ALTER DATABASE [BregVisionIntegrations] SET  DISABLE_BROKER
GO

ALTER DATABASE [BregVisionIntegrations] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO

ALTER DATABASE [BregVisionIntegrations] SET DATE_CORRELATION_OPTIMIZATION OFF
GO

ALTER DATABASE [BregVisionIntegrations] SET TRUSTWORTHY OFF
GO

ALTER DATABASE [BregVisionIntegrations] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO

ALTER DATABASE [BregVisionIntegrations] SET PARAMETERIZATION SIMPLE
GO

ALTER DATABASE [BregVisionIntegrations] SET READ_COMMITTED_SNAPSHOT OFF
GO

ALTER DATABASE [BregVisionIntegrations] SET HONOR_BROKER_PRIORITY OFF
GO

ALTER DATABASE [BregVisionIntegrations] SET RECOVERY FULL
GO

ALTER DATABASE [BregVisionIntegrations] SET  MULTI_USER
GO

ALTER DATABASE [BregVisionIntegrations] SET PAGE_VERIFY CHECKSUM
GO

ALTER DATABASE [BregVisionIntegrations] SET DB_CHAINING OFF
GO

ALTER DATABASE [BregVisionIntegrations] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF )
GO

ALTER DATABASE [BregVisionIntegrations] SET TARGET_RECOVERY_TIME = 60 SECONDS
GO

ALTER DATABASE [BregVisionIntegrations] SET DELAYED_DURABILITY = DISABLED
GO

EXEC sys.sp_db_vardecimal_storage_format N'BregVisionIntegrations', N'ON'
GO

ALTER DATABASE [BregVisionIntegrations] SET QUERY_STORE = OFF
GO

USE [BregVisionIntegrations]
GO

ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO

ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO

ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO

ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO

USE [BregVisionIntegrations]
GO

/****** Object:  Table [dbo].[IntegrationEntities]    Script Date: 4/3/2017 16:48:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[IntegrationRequests]    Script Date: 4/3/2017 16:48:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[Integrations]    Script Date: 4/3/2017 16:48:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[IntegrationStatus]    Script Date: 4/3/2017 16:48:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET IDENTITY_INSERT [dbo].[Integrations] ON
GO

INSERT [dbo].[Integrations] ([IntegrationID], [Integration], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1, N'BCS', CAST(N'2017-03-04T00:00:00.000' AS DateTime), 23, NULL, NULL)
GO

SET IDENTITY_INSERT [dbo].[Integrations] OFF
GO

SET IDENTITY_INSERT [dbo].[IntegrationStatus] ON
GO

INSERT [dbo].[IntegrationStatus] ([StatusID], [Status], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (1, N'Queued', CAST(N'2017-04-03T16:28:55.860' AS DateTime), 23, NULL, NULL)
GO

INSERT [dbo].[IntegrationStatus] ([StatusID], [Status], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (2, N'Processing', CAST(N'2017-04-03T16:28:55.873' AS DateTime), 23, NULL, NULL)
GO

INSERT [dbo].[IntegrationStatus] ([StatusID], [Status], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (3, N'Processed', CAST(N'2017-04-03T16:28:55.887' AS DateTime), 23, NULL, NULL)
GO

INSERT [dbo].[IntegrationStatus] ([StatusID], [Status], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (4, N'Success', CAST(N'2017-04-03T16:28:55.903' AS DateTime), 23, NULL, NULL)
GO

INSERT [dbo].[IntegrationStatus] ([StatusID], [Status], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (5, N'Failure', CAST(N'2017-04-03T16:28:55.920' AS DateTime), 23, NULL, NULL)
GO

SET IDENTITY_INSERT [dbo].[IntegrationStatus] OFF
GO

USE [master]
GO

ALTER DATABASE [BregVisionIntegrations] SET  READ_WRITE
GO
