﻿CREATE TABLE [dbo].[DocumentType] (
    [DocumentTypeID]		INT				IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [DocumentTypeName]    VARCHAR (100)	NOT NULL
    CONSTRAINT [PK_DocumentType] PRIMARY KEY CLUSTERED ([DocumentTypeID] ASC)
);

