﻿CREATE TABLE [dbo].[DocumentSource] (
    [DocumentSourceID]		INT				IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [DocumentSourceName]    VARCHAR (100)	NOT NULL
    CONSTRAINT [PK_DocumentSource] PRIMARY KEY CLUSTERED ([DocumentSourceID] ASC)
);

