﻿CREATE TABLE [dbo].[EntityDocumentMapping] (
    [EntityDocumentMappingID]   INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [EntityTypeID]			INT			NOT NULL,
    [EntityID]				INT			NOT NULL,
    [DocumentTypeID]			INT			NOT NULL,
    [DocumentID]            VARCHAR(50)			NOT NULL,
    [DocumentSourceID]		INT			NOT NULL,
    [CreatedUserID]			INT         NOT NULL,
    [CreatedDate]			DATETIME    CONSTRAINT [DF__EntityDocumentMapping__Create__Date] DEFAULT (getdate()) NOT NULL,
    
    CONSTRAINT [PK_EntityDocumentMapping] PRIMARY KEY CLUSTERED ([EntityDocumentMappingID] ASC),
    CONSTRAINT [FK_EntityDocumentMapping_EntityTypeID] FOREIGN KEY ([EntityTypeID]) REFERENCES [dbo].[EntityType] ([EntityTypeID]),
    CONSTRAINT [FK_EntityDocumentMapping_DocumentTypeID] FOREIGN KEY ([DocumentTypeID]) REFERENCES [dbo].[DocumentType] ([DocumentTypeID]),
    CONSTRAINT [FK_EntityDocumentMapping_DocumentSourceID] FOREIGN KEY ([DocumentSourceID]) REFERENCES [dbo].[DocumentSource] ([DocumentSourceID]),
);

GO