﻿CREATE TABLE [dbo].[IntegrationRequests](
	[RequestID] [int] IDENTITY(1,1) NOT NULL,
	[RMQID] UNIQUEIDENTIFIER NOT NULL,
	[IntegrationID] [int] NOT NULL,
	[StatusID] [int] NOT NULL,
	[EntityID] [int] NOT NULL,
	[EntityValue] [varchar](50) NOT NULL,
	[ResultJSON] [varchar](250) NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_IntegrationResults] PRIMARY KEY CLUSTERED 
(
	[RequestID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[IntegrationRequests]  WITH CHECK ADD  CONSTRAINT [FK_IntegrationRequests_IntegrationEntities] FOREIGN KEY([EntityID])
REFERENCES [dbo].[IntegrationEntities] ([EntityID])
GO

ALTER TABLE [dbo].[IntegrationRequests] CHECK CONSTRAINT [FK_IntegrationRequests_IntegrationEntities]
GO
ALTER TABLE [dbo].[IntegrationRequests]  WITH CHECK ADD  CONSTRAINT [FK_IntegrationRequests_Integrations] FOREIGN KEY([IntegrationID])
REFERENCES [dbo].[Integrations] ([IntegrationID])
GO

ALTER TABLE [dbo].[IntegrationRequests] CHECK CONSTRAINT [FK_IntegrationRequests_Integrations]
GO
ALTER TABLE [dbo].[IntegrationRequests]  WITH CHECK ADD  CONSTRAINT [FK_IntegrationRequests_IntegrationStatus] FOREIGN KEY([StatusID])
REFERENCES [dbo].[IntegrationStatus] ([StatusID])
GO

ALTER TABLE [dbo].[IntegrationRequests] CHECK CONSTRAINT [FK_IntegrationRequests_IntegrationStatus]