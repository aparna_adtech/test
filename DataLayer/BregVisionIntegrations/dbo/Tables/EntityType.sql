﻿CREATE TABLE [dbo].[EntityType] (
    [EntityTypeID]		INT				IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [EntityTypeName]    VARCHAR (100)	NOT NULL
    CONSTRAINT [PK_EntityType] PRIMARY KEY CLUSTERED ([EntityTypeID] ASC)
);

