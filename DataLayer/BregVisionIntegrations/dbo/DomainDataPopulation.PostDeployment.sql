﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
USE BregVisionIntegrations
GO

-- Insert into [DocumentSource] table.
IF OBJECT_ID('dbo.DocumentSource') IS NOT NULL
	INSERT INTO [dbo].[DocumentSource] (DocumentSourceName) VALUES ('BCS'), ('Cloud Connect')
ELSE
	RAISERROR ('Could not find table [dbo.DocumentSource]',12, 1) 

-- Insert into [DocumentType] table.
IF OBJECT_ID('dbo.DocumentType') IS NOT NULL
	INSERT INTO [dbo].[DocumentType] (DocumentTypeName) VALUES ('Image'), ('Pdf')
ELSE
	RAISERROR ('Could not find table [dbo.DocumentType]',12, 1) 
	

-- Insert into [EntityType] table.
IF OBJECT_ID('dbo.EntityType') IS NOT NULL
	INSERT INTO [dbo].[EntityType] (EntityTypeName) VALUES ('Dispensement')
ELSE
	RAISERROR ('Could not find table [dbo.EntityType]',12, 1)