﻿CREATE TABLE [dbo].[DispensementBCSClaimMap](
	[DispensementBCSClaimMapID] [int] IDENTITY(1,1) NOT NULL,
	[BCSClaimID] [int] NOT NULL,
	[DispenseID] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
 CONSTRAINT [PK_DispensementBCSClaimMap] PRIMARY KEY CLUSTERED 
(
	[DispensementBCSClaimMapID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]