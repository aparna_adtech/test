﻿using com.breg.vision.Handoff.Models;
using com.breg.vision.Handoff.Models.MongoDB;
using com.breg.vision.HandoffService.External.RabbitMQ;
using com.breg.vision.MongoRepository;
using System;
using System.Web.Http;
using com.breg.vision.Handoff.CryptoLib.AES256;

namespace com.breg.vision.HandoffService.Controllers
{
    [RoutePrefix("job")]
    public class JobController : ApiController
    {
        private MongoRepository<MongoHandoffModel> _handOffModelRepository;
        private RMQSubmitter _rmqSubmitter;

        public JobController()
        {
            _handOffModelRepository = new MongoRepository<MongoHandoffModel>();
            _rmqSubmitter = new RMQSubmitter();
        }

        [Route("rsa")]
        [HttpGet]
        public string CheckRSAStatus()
        {
            try
            {
                Handoff.CryptoLib.AES256.KeyManager.GetCurrentRSAKey();
                return "passed";
            }
            catch
            {
                return "failed";
            }
        }

        [Authorize]
        [Route("submit")]
        [HttpPost]
        public HandoffSubmitResponseModel Submit([FromBody] HandoffRequestModel request)
        {
            var token = Guid.NewGuid().ToString("N");

            if (string.IsNullOrEmpty(request.CorelationId))
            {
                request.CorelationId = token;
            }

            // try encrypting request
            Encrypter.TryEncrypt(request);

            // store in mongodb
            _handOffModelRepository.Add(new MongoHandoffModel { Token = token, Request = request });

            // submit job to rabbitmq
            _rmqSubmitter.PublishRMQRequest(token);

            return new HandoffSubmitResponseModel { ExpectedWait = 5, Token = token };
        }

        [Authorize]
        [Route("status/{token}")]
        [HttpGet]
        public HandoffStatusResponseModel GetStatus(string token)
        {
            // validate that the token is valid
            if (IsTokenFormatValid(token))
            {
                // look for results in mongodb
                var item = _handOffModelRepository.Get(entity => entity.Token == token);
                if (item != null)
                {
                    bool removeItem = false;
                    HandoffStatusResponseModel response = null;

                    // construct response
                    switch (item.Status)
                    {
                        case JobStatus.Completed:
                        case JobStatus.Failed:
                            // flag for removal in mongodb if it was processed successfully
                            // TODO: enable completed jobs removal after testing/validation
                            //if (item.Status == JobStatus.Completed)
                            //    removeItem = true;

                            var copy_of_response =
                                (item.Response != null)
                                    ? new HandoffResponseModel(item.Response)
                                    : null;
                            if (copy_of_response != null)
                                Decrypter.TryDecrypt(copy_of_response);

                            response =
                                new HandoffStatusResponseModel
                                {
                                    Status = item.Status,
                                    ReponseData = copy_of_response
                                };
                            break;
                        case JobStatus.Pending:
                        default:
                            response = HandoffStatusResponseModel.JobStillPendingResponse();
                            break;
                    }

                    // remove items from mongodb that are flagged for removal in above code
                    if (removeItem)
                        _handOffModelRepository.Delete(item);

                    return response;
                }
            }
            
            // default to invalid token response
            return HandoffStatusResponseModel.InvalidTokenResponse();
        }

        private bool IsTokenFormatValid(string token)
        {
            // the token is valid if it can be parsed as a GUID
            return Guid.TryParse(token, out Guid x);
        }
    }
}
