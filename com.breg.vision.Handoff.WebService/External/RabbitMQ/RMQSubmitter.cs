﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;

namespace com.breg.vision.HandoffService.External.RabbitMQ
{
    public class RMQSubmitter
    {
        ConnectionFactory _factory = new ConnectionFactory() { HostName = RabbitMQHostUri };
        private static string RabbitMQHostUri = ConfigurationManager.AppSettings["RabbitMQServiceUrl"];

        public RMQSubmitter()
        {
            _factory.Protocol = Protocols.AMQP_0_9_1;
        }
        public void PublishRMQRequest(string data)
        {
            using (var connection = _factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "HandoffQueue",
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

                var body = Encoding.UTF8.GetBytes(data);
                var properties = channel.CreateBasicProperties();
                properties.Persistent = true;

                channel.BasicPublish(exchange: "",
                                     routingKey: "HandoffQueue",
                                     basicProperties: properties,
                                     body: body);
            }
        }
    }
}