using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace com.breg.vision.Handoff.UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public string CanGetAuthToken()
        {
            var c = Handoff.Client.Client.GetBearerToken("testadmin", "Breg2007!", new Uri("http://stage1-switch.bregvision.com/api/V2/BregWcfService/Handoff/"));
            Console.WriteLine($"token received is {c}");
            Assert.IsFalse(string.IsNullOrEmpty(c));
            return c;
        }

        [TestMethod]
        public void CanEncryptAndDecryptPayload()
        {
            var request_json = File.ReadAllText("HandoffRequestModelSample.json");
            var request = Newtonsoft.Json.JsonConvert.DeserializeObject<com.breg.vision.Handoff.Models.HandoffRequestModel>(request_json);
            Assert.IsTrue(Handoff.CryptoLib.AES256.Encrypter.TryEncrypt(request), "failed encryption");
            Assert.IsTrue(Handoff.CryptoLib.AES256.Decrypter.TryDecrypt(request), "failed decryption");
            var request2 = Newtonsoft.Json.JsonConvert.DeserializeObject<com.breg.vision.Handoff.Models.HandoffRequestModel>(request_json);
            Assert.IsTrue(request.Payload.Equals(request2.Payload), "payload verification failed");
        }

        [TestMethod]
        public void CanGetCurrentRSAKey()
        {
            var k = Handoff.CryptoLib.AES256.KeyManager.GetCurrentRSAKey(true);
            Console.WriteLine(k);
            Assert.IsFalse(string.IsNullOrEmpty(k));
        }

        [TestMethod]
        public void CanGetCurrentRSAKeySize()
        {
            var l = Handoff.CryptoLib.AES256.KeyManager.GetCurrentRSAKeyLengthBytes();
            Console.WriteLine($"length = {l}");
            Assert.IsTrue(l > 0);
        }

        [TestMethod]
        public void CanCreateRSAKeyContainer()
        {
            Handoff.CryptoLib.AES256.KeyManager.CreateRSAContainerInMachineKeyStore();
            CanGetCurrentRSAKeySize();
        }
    }
}