﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using com.breg.vision.Handoff.Worker;
using com.breg.vision.Handoff.ClientModels;
using Newtonsoft.Json;
using com.breg.vision.BCSClaimPushService.ServiceModels;

namespace com.breg.vision.Handoff.UnitTest
{
    [TestClass]
    public class HandsoffWorkerTest
    {
        [TestMethod]
        public void JobType_Document_Document_Are_Saved_By_Doc_Api()
        {
            var request_json = File.ReadAllText("DataSource/DocumentRequestModelSample.json");
            var executor = new DocumentHandoffJobExecutor();

            var response = executor.ProcessRequestPayload(request_json);
            response.Wait();

            var data = JsonConvert.DeserializeObject<DocumentPayloadModel>(response.Result.ResponsePayload);
            Assert.IsFalse(string.IsNullOrEmpty(data.DocumentId));
        }

        [TestMethod]
        public void JobType_VisionToIntegration_DispensementId_Saved_By_Doc_Api()
        {
            var request_json = File.ReadAllText("DataSource/VisionToIntegrationModelSample.json");
            var executor = new VisionToIntegrationHandoffJobExecutor();

            var response = executor.ProcessRequestPayload(request_json);
            response.Wait();

            Assert.IsTrue(response.Result.IsSuccess);
        }

        [TestMethod]
        public void JobType_VisionToDocument_DispensementId_Saved_By_Doc_Api()
        {
            var request_json = File.ReadAllText("DataSource/VisionToDocumentModelSample.json");
            var executor = new VisionToDocumentHandoffJobExecutor();

            var response = executor.ProcessRequestPayload(request_json);
            response.Wait();

            var data = JsonConvert.DeserializeObject<bool>(response.Result.ResponsePayload);
            Assert.IsTrue(data);
        }

        [TestMethod]
        public void JobType_IntegrationToBCS_SubmitClaim()
        {
            var request_json = File.ReadAllText("DataSource/IntegrationToBCSModelSample.json");
            var executor = new IntegrationToBcsHandoffJobExecutor();

            var response = executor.ProcessRequestPayload(request_json);
            response.Wait();

            Assert.IsTrue(response.Result.IsSuccess);
        }

        [TestMethod]
        public void JobType_BcsToIntegration_UpdateClaimId_GetDocContent()
        {
            var request_json = File.ReadAllText("DataSource/BcsToIntegrationModelSample.json");
            var executor = new BcsToIntegrationHandoffJobExecutor();

            var response = executor.ProcessRequestPayload(request_json);
            response.Wait();

            Assert.IsTrue(response.Result.IsSuccess);
        }

        [TestMethod]
        public void JobType_IntegrationToBcsDoc_SendDocumentToBcs()
        {
            var request_json = File.ReadAllText("DataSource/IntegrationToBcsDocModelSample.json");
            var executor = new IntegrationToBcsDocHandoffJobExecutor();

            var response = executor.ProcessRequestPayload(request_json);
            response.Wait();

            Assert.IsTrue(response.Result.IsSuccess);
        }
    }
}