﻿using BregWcfService;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;

namespace BregWcfServiceTests
{
    
    
    /// <summary>
    ///This is a test class for LandingDataTest and is intended
    ///to contain all LandingDataTest Unit Tests
    ///</summary>
    [TestClass()]
    public class LandingDataTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetInventoryOrderStatus2
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        public void GetInventoryOrderStatus2Test()
        {
            int practiceID = 195;// 127;// 76;//5;//1;
            int practiceLocationID = 717;// 479;// 238;//11;//3;
            InventoryOrderStatusResult[] expected;
            InventoryOrderStatusResult[] actual;
            DateTime start = DateTime.Now;
            expected = LandingData.GetInventoryOrderStatus(practiceID, practiceLocationID);
            Console.WriteLine("expected loaded in " + (DateTime.Now - start).TotalSeconds.ToString() + " seconds");
            start = DateTime.Now;
            actual = LandingData.GetInventoryOrderStatus2(practiceID, practiceLocationID);
            Console.WriteLine("actual loaded in " + (DateTime.Now - start).TotalSeconds.ToString() + " seconds");
            Console.WriteLine(string.Format("expected.Length = {0}, actual.Length = {1}", expected.Length, actual.Length));
            Assert.AreEqual(expected.Length, actual.Length, "Number of records");
            for (int i = 0; i < expected.Length; i++)
            {
                Console.WriteLine("Testing item #" + i.ToString());
                Assert.AreEqual(expected[i].BillingCharge, actual[i].BillingCharge, "BillingCharge");
                Assert.AreEqual(expected[i].BillingChargeCash, actual[i].BillingChargeCash, "BillingChargeCash");
                Assert.AreEqual(expected[i].Code, actual[i].Code, "Code");
                Assert.AreEqual(expected[i].Color, actual[i].Color, "Color");
                Assert.AreEqual(expected[i].ConsignmentQuantity, actual[i].ConsignmentQuantity, "ConsignmentQuantity");
                Assert.AreEqual(expected[i].CriticalLevel, actual[i].CriticalLevel, "CriticalLevel");
                Assert.AreEqual(expected[i].DMEDeposit, actual[i].DMEDeposit, "DMEDeposit");
                Assert.AreEqual(expected[i].Gender, actual[i].Gender, "Gender");
                Assert.AreEqual(expected[i].HcpcString, actual[i].HcpcString, "HcpcString");
                Assert.AreEqual(expected[i].IsABN, actual[i].IsABN, "IsABN");
                Assert.AreEqual(expected[i].IsConsignment, actual[i].IsConsignment, "IsConsignment");
                Assert.AreEqual(expected[i].IsThirdParty, actual[i].IsThirdParty, "IsThirdParty");
                Assert.AreEqual(expected[i].IsThirdPartySupplier, actual[i].IsThirdPartySupplier, "IsThirdPartySupplier");
                Assert.AreEqual(expected[i].LeftRightSide, actual[i].LeftRightSide, "LeftRightSide");
                Assert.AreEqual(expected[i].MasterCatalogProductID, actual[i].MasterCatalogProductID, "MasterCatalogProductID");
                Assert.AreEqual(expected[i].MCName, actual[i].MCName, "MCName");
                Assert.AreEqual(expected[i].Packaging, actual[i].Packaging, "Packaging");
                Assert.AreEqual(expected[i].ParLevel, actual[i].ParLevel, "ParLevel");
                Assert.AreEqual(expected[i].PracticeCatalogProductID, actual[i].PracticeCatalogProductID, "PracticeCatalogProductID");
                Assert.AreEqual(expected[i].Priority, actual[i].Priority, "Priority");
                Assert.AreEqual(expected[i].ProductInventoryID, actual[i].ProductInventoryID, "ProductInventoryID");
                Assert.AreEqual(expected[i].QuantityOnHand, actual[i].QuantityOnHand, "QuantityOnHand");
                Assert.AreEqual(expected[i].QuantityOnHandPlusOnOrder, actual[i].QuantityOnHandPlusOnOrder, "QuantityOnHandPlusOnOrder");
                Assert.AreEqual(expected[i].QuantityOnOrder, actual[i].QuantityOnOrder, "QuantityOnOrder");
                Assert.AreEqual(expected[i].ReorderLevel, actual[i].ReorderLevel, "ReorderLevel");
                Assert.AreEqual(expected[i].Size, actual[i].Size, "Size");
                Assert.AreEqual(expected[i].StockingUnits, actual[i].StockingUnits, "StockingUnits");
                Assert.AreEqual(expected[i].SuggestedReorderLevel, actual[i].SuggestedReorderLevel, "SuggestedReorderLevel");
                Assert.AreEqual(expected[i].SupplierName, actual[i].SupplierName, "SupplierName");
                Assert.AreEqual(expected[i].SupplierSequence, actual[i].SupplierSequence, "SupplierSequence");
                Assert.AreEqual(expected[i].ThirdPartyProductID, actual[i].ThirdPartyProductID, "ThirdPartyProductID");
                Assert.AreEqual(expected[i].WholesaleCost, actual[i].WholesaleCost, "WholesaleCost");
                CollectionAssert.AreEqual(expected[i].UPCCode_List, actual[i].UPCCode_List, "UPCCode_List");
            }
        }
    }
}
