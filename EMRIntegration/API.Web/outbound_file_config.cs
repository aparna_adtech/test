//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace API.Web
{
    using System;
    using System.Collections.Generic;
    
    public partial class outbound_file_config
    {
        public int outbound_file_config_id { get; set; }
        public int practice_id { get; set; }
        public string file_dropoff_path { get; set; }
        public bool is_autoqueue_hl7_mdm { get; set; }
        public bool is_guid_filename_override { get; set; }
        public bool is_file_moved_by_bot { get; set; }
    }
}
