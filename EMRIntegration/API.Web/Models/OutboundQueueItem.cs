﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace API.Web.Models
{
    [DataContract]
    public class OutboundQueueItem
    {
        [DataMember]
        public Guid ID { get; set; }
        [DataMember]
        public int PracticeID { get; set; }
        [DataMember]
        public string Filename { get; set; }
        [DataMember]
        public DateTime CreationDate { get; set; }
    }
}