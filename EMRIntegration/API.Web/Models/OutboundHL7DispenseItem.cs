﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace API.Web.Models
{
    [DataContract]
    public class OutboundHL7DispenseItem
    {
        [DataMember]
        public string supplier_name { get; set; }
        [DataMember]
        public string item_code { get; set; }
        [DataMember]
        public string item_name { get; set; }
        [DataMember]
        public int quantity { get; set; }
        [DataMember]
        public decimal unit_price { get; set; }
        [DataMember]
        public decimal dme_deposit { get; set; }
        [DataMember]
        public string hcpcs_code { get; set; }
        [DataMember]
        public string modifier1 { get; set; }
        [DataMember]
        public string modifier2 { get; set; }
        [DataMember]
        public string modifier3 { get; set; }
        [DataMember]
        public string modifier4 { get; set; }
        [DataMember]
        public string Gender { get; set; }
        [DataMember]
        public string Size { get; set; }
        [DataMember]
        public DateTime? patient_signature_date { get; set; }
        [DataMember]
        public DateTime? physician_signature_date { get; set; }
        [DataMember]
        public DateTime dispense_date { get; set; }
        [DataMember]
        public bool is_medicare { get; set; }
        [DataMember]
        public bool is_abn { get; set; }
        [DataMember]
        public string abn_reason { get; set; }
    }
}