//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace API.Web
{
    using System;
    using System.Collections.Generic;
    
    public partial class outbound_hl7_item_queue
    {
        public outbound_hl7_item_queue()
        {
            this.outbound_hl7_diagnosis = new HashSet<outbound_hl7_diagnosis>();
            this.outbound_hl7_dispensement_item = new HashSet<outbound_hl7_dispensement_item>();
        }
    
        public System.Guid outbound_hl7_item_queue_id { get; set; }
        public string message_type { get; set; }
        public int practice_id { get; set; }
        public string remote_schedule_item_id { get; set; }
        public Nullable<System.Guid> associated_outbound_file_item_queue_id { get; set; }
        public bool hcpcs_transform_enabled { get; set; }
        public System.DateTime creation_date { get; set; }
        public Nullable<System.DateTime> send_success_date { get; set; }
        public Nullable<int> source_item_id { get; set; }
        public string emr_clinician_id { get; set; }
    
        public virtual ICollection<outbound_hl7_diagnosis> outbound_hl7_diagnosis { get; set; }
        public virtual ICollection<outbound_hl7_dispensement_item> outbound_hl7_dispensement_item { get; set; }
    }
}
