﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using API.Web.Models;

namespace API.Web
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IEMRIntegration" in both code and config file together.
    [ServiceContract]
    public interface IEMRIntegration
    {
        [OperationContract]
        schedule_item[] GetPatients(int practiceId, int practiceLocationId, DateTime utcStartDate, DateTime? utcEndDate);

        [OperationContract]
        void QueueOutboundFile(int practiceId, string fileName, byte[] fileContents, string vccScheduleId, int sourceItemId);

        [OperationContract]
        Models.OutboundQueueItem[] GetOutboundQueueItems();

        [OperationContract]
        byte[] GetOutboundFileContentsByOutboundQueueID(Guid outboundQueueId);

        [OperationContract]
        void SetOutboundQueueItemProcessed(Guid outboundQueueId);

        [OperationContract]
        string GetQueueProcessorStoragePathForPracticeId(int practiceId);

        [OperationContract]
        void AddQueueProcessorErrorMessageForQueueItem(Guid outboundQueueId, string message);

        [OperationContract]
        void QueueOutboundHL7Request(int practiceID, string scheduleIdentifier, OutboundHL7DispenseItem[] dispenseItems, DiagnosisCode[] diagnosisCodes, int dispenseId, string emrClinicianId);

		[OperationContract]
		void AddScheduledPatient(ScheduledPatient Patient);
    }

	[DataContract]
	public class ScheduledPatient
	{
		public string RemoteScheduleItemID;
		public int PracticeID;
		public int PracticeLocationID;
		public string PatientID;
		public string FirstName;
		public string LastName;
		public string PatientEmail;
		public string PhysicianName;
		public string DiagnosisICD9;
		public string ScheduledVisitDateTime;
		public string ScheduledVisitTimezone;
		public string PatientDOBDateTime;
	}
}
