﻿CREATE TABLE [dbo].[outbound_file_item_queue] (
    [outbound_file_item_queue_id] UNIQUEIDENTIFIER CONSTRAINT [DF__outbound___outbo__32E0915F] DEFAULT (newid()) NOT NULL,
    [practice_id]                 INT              NOT NULL,
    [remote_schedule_item_id]     NVARCHAR (50)    NULL,
    [filestore_path]              NVARCHAR (255)   NOT NULL,
    [filename]                    NVARCHAR (255)   NULL,
    [creation_date]               DATETIME         CONSTRAINT [DF__outbound___creat__33D4B598] DEFAULT (getdate()) NOT NULL,
    [send_success_date]           DATETIME         NULL,
    [channel_send_success_date]   DATETIME         NULL,
    [source_item_id]              INT              NULL,
    [send_suppressed]             BIT              CONSTRAINT [df_send_suppressed] DEFAULT ((0)) NULL,
    CONSTRAINT [pk__outbound_file_item_queue] PRIMARY KEY CLUSTERED ([outbound_file_item_queue_id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_outbound_file_item_queue__practice_id__channel_send_success_date__send_success_date]
    ON [dbo].[outbound_file_item_queue]([practice_id] ASC, [channel_send_success_date] ASC, [send_success_date] ASC);

