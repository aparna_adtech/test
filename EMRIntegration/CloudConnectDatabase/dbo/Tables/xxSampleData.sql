﻿CREATE TABLE [dbo].[xxSampleData] (
    [remote_schedule_item_id]             NVARCHAR (50) NULL,
    [patient_name]                        NVARCHAR (50) NULL,
    [patient_id]                          NVARCHAR (20) NULL,
    [diagnosis_icd9]                      NVARCHAR (50) NULL,
    [scheduled_visit_datetime_timezoneid] NVARCHAR (50) NULL,
    [patient_dob]                         DATETIME      NULL,
    [patient_first_name]                  NVARCHAR (50) NULL,
    [patient_last_name]                   NVARCHAR (25) NULL
);

