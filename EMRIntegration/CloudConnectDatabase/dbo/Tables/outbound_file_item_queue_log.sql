﻿CREATE TABLE [dbo].[outbound_file_item_queue_log] (
    [outbound_file_item_queue_log_id] UNIQUEIDENTIFIER CONSTRAINT [DF_outbound_file_item_queue_log_outbound_file_item_queue_log_id] DEFAULT (newid()) NOT NULL,
    [outbound_file_item_queue_id]     UNIQUEIDENTIFIER NOT NULL,
    [log_datetime]                    DATETIME         CONSTRAINT [DF_outbound_file_item_queue_log_log_datetime] DEFAULT (getdate()) NOT NULL,
    [log_message]                     NTEXT            NOT NULL,
    CONSTRAINT [PK_outbound_file_item_queue_log] PRIMARY KEY NONCLUSTERED ([outbound_file_item_queue_log_id] ASC),
    CONSTRAINT [FK_outbound_file_item_queue_log_outbound_file_item_queue] FOREIGN KEY ([outbound_file_item_queue_id]) REFERENCES [dbo].[outbound_file_item_queue] ([outbound_file_item_queue_id]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_outbound_file_item_queue_log]
    ON [dbo].[outbound_file_item_queue_log]([outbound_file_item_queue_id] ASC, [log_datetime] ASC);

