﻿CREATE TABLE [dbo].[schedule_item_incoming] (
    [patient_id]               NVARCHAR (255) NULL,
    [patient_last_name]        NVARCHAR (255) NULL,
    [patient_first_name]       NVARCHAR (255) NULL,
    [physician_last_name]      NVARCHAR (255) NULL,
    [physician_first_name]     NVARCHAR (255) NULL,
    [scheduled_visit_datetime] DATETIME       NULL
);


GO
-- =============================================
-- Author:		Henry Chan
-- Create date: 12/3/2012
-- Description:	
-- =============================================
CREATE TRIGGER [dbo].[schedule_item_incoming_insert] 
   ON  [dbo].[schedule_item_incoming] 
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @patient_id nvarchar(20);
	declare @patient_name nvarchar(50);
	declare @patient_email nvarchar(50);
	declare @physician_name nvarchar(50);
	declare @diagnosis_icd9 nvarchar(50);
	declare @scheduled_visit_datetime datetime;
	
	select
		@patient_id = patient_id,
		@patient_name = patient_first_name + ' ' + patient_last_name,
		@patient_email = null,
		@physician_name = physician_first_name + ' ' + physician_last_name,
		@scheduled_visit_datetime = scheduled_visit_datetime
	from
		inserted;

    -- Insert statements for trigger here
	insert into schedule_item
	(
	schedule_item_id,
	practice_id,
	practice_location_id,
	patient_id,
	patient_name,
	patient_email,
	physician_name,
	diagnosis_icd9,
	scheduled_visit_datetime,
	creation_date,
	is_active
	)
	values
	(
	NEWID(),
	1,
	3,
	@patient_id,
	@patient_name,
	@patient_email,
	@physician_name,
	@diagnosis_icd9,
	@scheduled_visit_datetime,
	GETDATE(),
	1
	);
END


