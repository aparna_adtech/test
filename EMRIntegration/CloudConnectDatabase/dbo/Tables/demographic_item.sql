﻿CREATE TABLE [dbo].[demographic_item] (
    [demographic_item_id] UNIQUEIDENTIFIER CONSTRAINT [DF_demographic_item_demographic_item_id] DEFAULT (newid()) NOT NULL,
    [practice_id]         INT              NOT NULL,
    [patient_id]          NVARCHAR (255)   NULL,
    [creation_date]       DATETIME         CONSTRAINT [DF_demographic_item_creation_date] DEFAULT (getdate()) NOT NULL,
    [modified_date]       DATETIME         CONSTRAINT [DF_demographic_item_modified_date] DEFAULT (getdate()) NOT NULL,
    [message]             NTEXT            NULL,
    CONSTRAINT [PK_demographic_item] PRIMARY KEY CLUSTERED ([demographic_item_id] ASC)
);

