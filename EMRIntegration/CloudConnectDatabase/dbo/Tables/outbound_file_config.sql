﻿CREATE TABLE [dbo].[outbound_file_config] (
    [outbound_file_config_id]   INT            IDENTITY (1, 1) NOT NULL,
    [practice_id]               INT            NOT NULL,
    [file_dropoff_path]         NVARCHAR (255) NOT NULL,
    [is_autoqueue_hl7_mdm]      BIT            CONSTRAINT [DF_outbound_file_config_is_autoqueue_hl7_mdm] DEFAULT ((0)) NOT NULL,
    [is_guid_filename_override] BIT            CONSTRAINT [DF_outbound_file_config_is_guid_filename_override] DEFAULT ((0)) NOT NULL,
    [is_file_moved_by_bot]      BIT            CONSTRAINT [DF_outbound_file_config_is_file_moved_by_bot] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [pk__outbound_file_config] PRIMARY KEY CLUSTERED ([outbound_file_config_id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [ix__outbound_file_config__practice_id]
    ON [dbo].[outbound_file_config]([practice_id] ASC)
    INCLUDE([file_dropoff_path]);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tells Vision to generate HL7 item queue of message type MDM', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'outbound_file_config', @level2type = N'COLUMN', @level2name = N'is_autoqueue_hl7_mdm';

