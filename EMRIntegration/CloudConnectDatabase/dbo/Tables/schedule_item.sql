﻿CREATE TABLE [dbo].[schedule_item] (
    [schedule_item_id]                    UNIQUEIDENTIFIER CONSTRAINT [DF_schedule_item_schedule_item_id] DEFAULT (newid()) NOT NULL,
    [remote_schedule_item_id]             NVARCHAR (50)    NULL,
    [practice_id]                         INT              NOT NULL,
    [practice_location_id]                INT              NOT NULL,
    [patient_id]                          NVARCHAR (255)   NULL,
    [patient_name]                        NVARCHAR (50)    NULL,
    [patient_email]                       NVARCHAR (50)    NULL,
    [physician_name]                      NVARCHAR (50)    NULL,
    [diagnosis_icd9]                      NVARCHAR (50)    NULL,
    [scheduled_visit_datetime]            DATETIME         NOT NULL,
    [creation_date]                       DATETIME         CONSTRAINT [DF_schedule_item_creation_date] DEFAULT (getdate()) NOT NULL,
	[last_modify_date]                    DATETIME         NULL,
    [is_active]                           BIT              CONSTRAINT [DF_schedule_item_is_active] DEFAULT ((1)) NOT NULL,
    [scheduled_visit_datetime_timezoneid] NVARCHAR (50)    CONSTRAINT [DF_schedule_item_scheduled_visit_datetime_timezoneid] DEFAULT (N'Pacific Standard Time') NOT NULL,
    [patient_dob]                         DATETIME         NULL,
    [is_checked_in]                       BIT              CONSTRAINT [DF_schedule_item_is_checked_in] DEFAULT ((0)) NOT NULL,
    [message]                             NTEXT            NULL,
    [patient_first_name]                  NVARCHAR (50)    NULL,
    [patient_last_name]                   NVARCHAR (25)    NULL,
    CONSTRAINT [PK_schedule_item] PRIMARY KEY CLUSTERED ([schedule_item_id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_schedule_item]
    ON [dbo].[schedule_item]([practice_id] ASC, [practice_location_id] ASC, [scheduled_visit_datetime] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_schedule_item__remote_schedule_item_id__practice_id__practice_location_id]
    ON [dbo].[schedule_item]([remote_schedule_item_id] ASC, [practice_id] ASC, [practice_location_id] ASC);


GO
  CREATE trigger [dbo].[validateEmail]
  on [dbo].[schedule_item]
  after insert, update
  as
	update [dbo].[schedule_item] 
	set patient_email = ''
	from [dbo].[schedule_item] as si
		join inserted as i on i.[schedule_item_id] = si.[schedule_item_id]
		where len(ltrim(rtrim(i.patient_email))) > 0
		and 
		i.patient_email not like '%@%.%'
	return


GO

CREATE TRIGGER [dbo].[updateLastModify]
    ON [dbo].[schedule_item]
    FOR INSERT, UPDATE
    AS
    BEGIN
        set nocount on

		update [dbo].[schedule_item]
			set last_modify_date=getdate()
			from inserted as [i]
			inner join [dbo].[schedule_item] on [dbo].[schedule_item].[schedule_item_id]=[i].[schedule_item_id]
    END
GO