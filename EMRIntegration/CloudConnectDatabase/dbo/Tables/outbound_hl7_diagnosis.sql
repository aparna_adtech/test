﻿CREATE TABLE [dbo].[outbound_hl7_diagnosis] (
    [outbound_hl7_diagnosis_id]  UNIQUEIDENTIFIER CONSTRAINT [DF_outbound_hl7_diagnosis_outbound_hl7_diagnosis_id] DEFAULT (newid()) NOT NULL,
    [outbound_hl7_item_queue_id] UNIQUEIDENTIFIER NOT NULL,
    [diagnosis_code]             NVARCHAR (50)    NOT NULL,
    [coding_method]              NVARCHAR (50)    CONSTRAINT [DF_outbound_hl7_diagnosis_coding_method] DEFAULT ('HCPCS') NOT NULL,
    [creation_date]              DATETIME         CONSTRAINT [DF_outbound_hl7_diagnosis_creation_date] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_outbound_hl7_diagnosis] PRIMARY KEY CLUSTERED ([outbound_hl7_diagnosis_id] ASC),
    CONSTRAINT [FK_outbound_hl7_diagnosis_outbound_hl7_item_queue] FOREIGN KEY ([outbound_hl7_item_queue_id]) REFERENCES [dbo].[outbound_hl7_item_queue] ([outbound_hl7_item_queue_id]) ON DELETE CASCADE ON UPDATE CASCADE
);

