﻿CREATE TABLE [dbo].[inbound_hl7_oru] (
    [inbound_hl7_oru_id] UNIQUEIDENTIFIER CONSTRAINT [DF_inbound_hl7_oru_inbound_hl7_oru_id] DEFAULT (newid()) NOT NULL,
    [practice_id]        INT              NOT NULL,
    [visit_Number]       NVARCHAR (50)    NOT NULL,
    [message_date]       DATETIME         NOT NULL,
    [message]            NTEXT            NOT NULL,
    [creation_date]      DATETIME         CONSTRAINT [DF_inbound_hl7_oru_creation_date] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_inbound_hl7_oru] PRIMARY KEY CLUSTERED ([inbound_hl7_oru_id] ASC)
);

