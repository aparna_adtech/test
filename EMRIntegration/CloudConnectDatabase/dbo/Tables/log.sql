﻿CREATE TABLE [dbo].[log] (
    [log_id]       UNIQUEIDENTIFIER CONSTRAINT [DF_log_log_id] DEFAULT (newid()) NOT NULL,
    [log_datetime] DATETIME         CONSTRAINT [DF_log_log_datetime] DEFAULT (getdate()) NOT NULL,
    [log_message]  NTEXT            NOT NULL,
    CONSTRAINT [PK_log] PRIMARY KEY CLUSTERED ([log_id] ASC)
);

