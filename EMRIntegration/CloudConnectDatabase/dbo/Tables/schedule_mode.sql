﻿CREATE TABLE [dbo].[schedule_mode] (
    [schedule_mode_id] INT            IDENTITY (1, 1) NOT NULL,
    [title]            NVARCHAR (50)  NULL,
    [description]      NVARCHAR (200) NULL,
    CONSTRAINT [PK_schedule_mode] PRIMARY KEY CLUSTERED ([schedule_mode_id] ASC)
);

