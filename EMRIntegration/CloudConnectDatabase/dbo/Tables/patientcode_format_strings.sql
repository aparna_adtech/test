﻿CREATE TABLE [dbo].[patientcode_format_strings] (
    [patientcode_format_strings_id] INT           IDENTITY (1, 1) NOT NULL,
    [practice_id]                   INT           NOT NULL,
    [format_string]                 VARCHAR (255) NULL,
    CONSTRAINT [pk_patientcode_format_strings] PRIMARY KEY CLUSTERED ([patientcode_format_strings_id] ASC)
);

