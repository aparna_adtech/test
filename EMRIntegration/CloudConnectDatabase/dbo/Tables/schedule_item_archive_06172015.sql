﻿CREATE TABLE [dbo].[schedule_item_archive_06172015] (
    [schedule_item_id]                    UNIQUEIDENTIFIER NOT NULL,
    [remote_schedule_item_id]             NVARCHAR (50)    NULL,
    [practice_id]                         INT              NOT NULL,
    [practice_location_id]                INT              NOT NULL,
    [patient_id]                          NVARCHAR (255)   NULL,
    [patient_name]                        NVARCHAR (50)    NULL,
    [patient_email]                       NVARCHAR (50)    NULL,
    [physician_name]                      NVARCHAR (50)    NULL,
    [diagnosis_icd9]                      NVARCHAR (50)    NULL,
    [scheduled_visit_datetime]            DATETIME         NOT NULL,
    [creation_date]                       DATETIME         NOT NULL,
    [is_active]                           BIT              NOT NULL,
    [scheduled_visit_datetime_timezoneid] NVARCHAR (50)    NOT NULL,
    [patient_dob]                         DATETIME         NULL,
    [is_checked_in]                       BIT              NOT NULL,
    [message]                             NTEXT            NULL,
    [patient_first_name]                  NVARCHAR (50)    NULL,
    [patient_last_name]                   NVARCHAR (25)    NULL
);

