﻿CREATE TABLE [dbo].[outbound_hl7_item_queue] (
    [outbound_hl7_item_queue_id]             UNIQUEIDENTIFIER CONSTRAINT [DF_outbound_hl7_item_queue_outbound_hl7_item_queue_id] DEFAULT (newid()) NOT NULL,
    [message_type]                           VARCHAR (10)     CONSTRAINT [DF_outbound_hl7_item_queue_message_type] DEFAULT ('DFT') NOT NULL,
    [practice_id]                            INT              NOT NULL,
    [remote_schedule_item_id]                NVARCHAR (50)    NULL,
    [hcpcs_transform_enabled]                BIT              CONSTRAINT [DF_outbound_hl7_item_queue_hcpcs_transform_enabled] DEFAULT ((0)) NOT NULL,
    [creation_date]                          DATETIME         CONSTRAINT [DF_outbound_hl7_item_queue_creation_date] DEFAULT (getdate()) NOT NULL,
    [send_success_date]                      DATETIME         NULL,
    [associated_outbound_file_item_queue_id] UNIQUEIDENTIFIER CONSTRAINT [DF_outbound_hl7_item_queue_associated_outbound_file_item_queue_id] DEFAULT (newid()) NULL,
    [source_item_id]                         INT              NULL,
    [emr_clinician_id]                       VARCHAR (50)     NULL,
    CONSTRAINT [PK_outbound_hl7_item_queue] PRIMARY KEY CLUSTERED ([outbound_hl7_item_queue_id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_outbound_hl7_item_queue__practice_id]
    ON [dbo].[outbound_hl7_item_queue]([practice_id] ASC)
    INCLUDE([outbound_hl7_item_queue_id], [message_type], [remote_schedule_item_id], [hcpcs_transform_enabled], [source_item_id]);


GO
CREATE NONCLUSTERED INDEX [IX_outbound_hl7_item_queue__practice_id__source_item_id]
    ON [dbo].[outbound_hl7_item_queue]([practice_id] ASC, [source_item_id] ASC)
    INCLUDE([outbound_hl7_item_queue_id], [message_type], [remote_schedule_item_id], [hcpcs_transform_enabled]);

