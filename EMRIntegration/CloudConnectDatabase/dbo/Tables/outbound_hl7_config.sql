﻿CREATE TABLE [dbo].[outbound_hl7_config] (
    [outbound_hl7_config_id] UNIQUEIDENTIFIER CONSTRAINT [DF_outbound_hl7_config_outbound_hl7_config_id] DEFAULT (newid()) NOT NULL,
    [practice_id]            INT              NOT NULL,
    [hl7_message_type]       VARCHAR (10)     CONSTRAINT [DF_outbound_hl7_config_hl7_message_type] DEFAULT ('DFT') NOT NULL,
    CONSTRAINT [PK_outbound_hl7_config] PRIMARY KEY CLUSTERED ([outbound_hl7_config_id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_outbound_hl7_config]
    ON [dbo].[outbound_hl7_config]([practice_id] ASC);

