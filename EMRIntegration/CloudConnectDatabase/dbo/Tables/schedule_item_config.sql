﻿CREATE TABLE [dbo].[schedule_item_config] (
    [schedule_item_config_id] INT IDENTITY (1, 1) NOT NULL,
    [practice_id]             INT NOT NULL,
    [schedule_mode_id]        INT NOT NULL,
    [parameter1]              INT NULL,
    [parameter2]              INT NULL,
    [parameter3]              INT NULL,
    [parameter4]              INT NULL,
    CONSTRAINT [PK_schedule_item_config] PRIMARY KEY CLUSTERED ([schedule_item_config_id] ASC),
    CONSTRAINT [FK_schedule_item_config_schedule_mode] FOREIGN KEY ([schedule_mode_id]) REFERENCES [dbo].[schedule_mode] ([schedule_mode_id]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO

CREATE INDEX [IX_schedule_item_config_practice_id] ON [dbo].[schedule_item_config] ([practice_id])

GO