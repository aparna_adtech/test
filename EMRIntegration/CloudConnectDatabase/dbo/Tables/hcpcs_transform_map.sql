﻿CREATE TABLE [dbo].[hcpcs_transform_map] (
    [hcpcs_transform_map_id] UNIQUEIDENTIFIER CONSTRAINT [DF_hcpcs_transform_map_hcpcs_transform_map_id] DEFAULT (newid()) NOT NULL,
    [item_code]              VARCHAR (50)     NOT NULL,
    [hcpcs_code]             VARCHAR (20)     NOT NULL,
    [transformed_value]      VARCHAR (50)     NULL,
    CONSTRAINT [PK_hcpcs_transform_map] PRIMARY KEY CLUSTERED ([hcpcs_transform_map_id] ASC)
);

