﻿CREATE SEQUENCE [dbo].[outbound_hl7_sequence]
    AS INT
    START WITH 0
    INCREMENT BY 1
    CYCLE
    NO CACHE;

