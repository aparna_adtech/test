﻿CREATE FUNCTION [dbo].[generateRedoxDocumentId]
(
)
RETURNS NVARCHAR(30)
AS
BEGIN
	DECLARE @rand4 INT
	SELECT @rand4 = [Random4] FROM [dbo].[vRandom4Int]
	RETURN N'BREG-' + FORMAT(GETDATE(), 'yyyyMMddHHmmssfff', 'en-US') + N'-' + FORMAT(@rand4, '0000', 'en-US')
END

