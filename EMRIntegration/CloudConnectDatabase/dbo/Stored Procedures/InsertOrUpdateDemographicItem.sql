﻿

CREATE PROCEDURE [dbo].[InsertOrUpdateDemographicItem]
	@practice_id INT,
	@patient_id NVARCHAR(20),
	@message NTEXT=NULL

AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
    BEGIN TRANSACTION;
	UPDATE [dbo].[demographic_item]
	SET
	     [modified_date] = GETDATE()
		,[message]=@message
	WHERE
		[patient_id]=@patient_id;
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [dbo].[demographic_item]
		(
			[practice_id]
			,[patient_id]
			,[creation_date]
			,[modified_date]
			,[message]
		)
		VALUES
		(
			@practice_id
			,@patient_id
			,GETDATE()
			,GETDATE()
			,@message
		);
	END
	COMMIT TRANSACTION;
END
