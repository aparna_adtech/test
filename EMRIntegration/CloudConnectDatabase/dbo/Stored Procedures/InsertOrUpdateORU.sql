﻿




CREATE PROCEDURE [dbo].[InsertOrUpdateORU]
	@practice_id int,
	@visit_number nvarchar(20),
	@message_date nvarchar(20),
	@message nvarchar(max)=null
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
    BEGIN TRANSACTION;
	--UPDATE [dbo].[inbound_adt]  THIS IS ONLY A TEMPLATE FOR UPDATE!!!!!
	--SET
	--	[patient_id]=@patient_id
	--	,[patient_name]=@patient_name
	--	,[patient_email]=@patient_email
	--	,[physician_name]=@physician_name
	--	,[diagnosis_icd9]=@diagnosis_icd9
	--	,[scheduled_visit_datetime]=@scheduled_visit_datetime
	--	,[scheduled_visit_datetime_timezoneid]=@scheduled_visit_datetime_timezoneid
	--	,[patient_dob]=@patient_dob
	--	,[is_checked_in]=@is_checked_in
	--	,[message]=@message
	--WHERE
	--	[remote_schedule_item_id]=@remote_schedule_item_id
	--	AND [practice_id]=@practice_id
	--	AND [practice_location_id]=@practice_location_id;
	--IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [dbo].[inbound_hl7_oru]
		(
			[practice_id]
			,[visit_number]
			,[message_date]
			,[message]
		)
		VALUES
		(
			@practice_id
			,@visit_number
			,convert(datetime, left(@message_date,8)+ ' ' + substring(@message_date,9,2)+ ':' + substring(@message_date,11,2))
			,@message
		);
	END
	COMMIT TRANSACTION;
END




