﻿
-- =============================================
-- Author:		Ron Dashwood
-- Create date: 3/20/2014
-- Description:	
-- =============================================
Create PROCEDURE [dbo].[InsertOrUpdateScheduleItemUsingUpdateSproc]
	@remote_schedule_item_id				nvarchar(50),
	@practice_id							int,
	@practice_location_id					int,
	@patient_id								nvarchar(20),
	@patient_name							nvarchar(50),
	@patient_email							nvarchar(50),
	@physician_name							nvarchar(50),
	@diagnosis_icd9							nvarchar(50),
	@scheduled_visit_datetime				datetime,
	@scheduled_visit_datetime_timezoneid	nvarchar(50)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
    BEGIN TRANSACTION;
	declare @updateRowCount  bit
	EXEC  @updateRowCount = UpdateScheduleItem 
			@remote_schedule_item_id			
			,@remote_schedule_item_id			
			,@practice_id						
			,@practice_location_id				
			,@patient_id							
			,@patient_name						
			,@patient_email						
			,@physician_name						
			,@diagnosis_icd9						
			,@scheduled_visit_datetime			
			,@scheduled_visit_datetime_timezoneid	

	IF @updateRowCount = 0
	BEGIN
		INSERT INTO [dbo].[schedule_item]
		(
			[remote_schedule_item_id]
			,[practice_id]
			,[practice_location_id]
			,[patient_id]
			,[patient_name]
			,[patient_email]
			,[physician_name]
			,[diagnosis_icd9]
			,[scheduled_visit_datetime]
			,[creation_date]
			,[is_active]
			,[scheduled_visit_datetime_timezoneid]
		)
		VALUES
		(
			@remote_schedule_item_id
			,@practice_id
			,@practice_location_id
			,@patient_id
			,@patient_name
			,@patient_email
			,@physician_name
			,@diagnosis_icd9
			,@scheduled_visit_datetime
			,GETDATE()
			,1
			,@scheduled_visit_datetime_timezoneid
		);
	END
	COMMIT TRANSACTION;
END

