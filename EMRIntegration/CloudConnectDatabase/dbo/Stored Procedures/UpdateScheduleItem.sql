﻿
-- =============================================
-- Author:		Ron Dashwood
-- Create date: 3/20/2014
-- Description:	Isolate update allowing for new remote_schedule_item_id value.
-- =============================================
CREATE PROCEDURE [dbo].[UpdateScheduleItem]
	@new_remote_schedule_item_id nvarchar(50),
	@original_remote_schedule_item_id nvarchar(50),
	@practice_id int,
	@practice_location_id int,
	@patient_id nvarchar(20),
	@patient_name nvarchar(50),
	@patient_email nvarchar(50),
	@physician_name nvarchar(50),
	@diagnosis_icd9 nvarchar(50),
	@scheduled_visit_datetime datetime,
	@scheduled_visit_datetime_timezoneid nvarchar(50)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
    BEGIN TRANSACTION;
	UPDATE [dbo].[schedule_item]
	SET
		[remote_schedule_item_id] = @new_remote_schedule_item_id
		,[patient_id]=@patient_id
		,[patient_name]=@patient_name
		,[patient_email]=@patient_email
		,[physician_name]=@physician_name
		,[diagnosis_icd9]=@diagnosis_icd9
		,[scheduled_visit_datetime]=@scheduled_visit_datetime
		,[scheduled_visit_datetime_timezoneid]=@scheduled_visit_datetime_timezoneid
	WHERE
		[remote_schedule_item_id]=@original_remote_schedule_item_id
		AND [practice_id]=@practice_id
		AND [practice_location_id]=@practice_location_id;
	COMMIT TRANSACTION;
	return @@ROWCOUNT
END

