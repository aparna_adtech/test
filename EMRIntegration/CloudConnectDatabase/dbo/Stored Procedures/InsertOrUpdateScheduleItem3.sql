﻿
-- =============================================
-- Author:		Henry Chan
-- Create date: 3/21/2014
-- Description:	Insert or update schedule_item row with optional search_remote_schedule_item_id parameter
-- =============================================
Create PROCEDURE [dbo].[InsertOrUpdateScheduleItem3]
	@remote_schedule_item_id nvarchar(50),
	@practice_id int,
	@practice_location_id int,
	@patient_id nvarchar(20),
	@patient_name nvarchar(50),
	@patient_email nvarchar(50),
	@physician_name nvarchar(50),
	@diagnosis_icd9 nvarchar(50),
	@scheduled_visit_datetime datetime,
	@scheduled_visit_datetime_timezoneid nvarchar(50)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
    
	BEGIN TRANSACTION;
	BEGIN TRY

		UPDATE [dbo].[schedule_item]
		SET
			[patient_id]=@patient_id
			,[patient_name]=@patient_name
			,[patient_email]=@patient_email
			,[physician_name]=@physician_name
			,[diagnosis_icd9]=@diagnosis_icd9
			,[scheduled_visit_datetime]=@scheduled_visit_datetime
			,[scheduled_visit_datetime_timezoneid]=@scheduled_visit_datetime_timezoneid
		WHERE
			[remote_schedule_item_id]=@remote_schedule_item_id
			AND [practice_id]=@practice_id
			AND [practice_location_id]=@practice_location_id;

		IF @@ROWCOUNT = 0
		BEGIN
				INSERT INTO [dbo].[schedule_item]
				(
					[remote_schedule_item_id]
					,[practice_id]
					,[practice_location_id]
					,[patient_id]
					,[patient_name]
					,[patient_email]
					,[physician_name]
					,[diagnosis_icd9]
					,[scheduled_visit_datetime]
					,[creation_date]
					,[is_active]
					,[scheduled_visit_datetime_timezoneid]
				)
				VALUES
				(
					@remote_schedule_item_id
					,@practice_id
					,@practice_location_id
					,@patient_id
					,@patient_name
					,@patient_email
					,@physician_name
					,@diagnosis_icd9
					,@scheduled_visit_datetime
					,GETDATE()
					,1
					,@scheduled_visit_datetime_timezoneid
				);
			END
		END TRY

		BEGIN CATCH
			DECLARE @ErrorMessage NVARCHAR(4000);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE();

			ROLLBACK TRANSACTION

			RAISERROR(
				@ErrorMessage,	-- Message text.
				@ErrorSeverity,	-- Severity.
				@ErrorState		-- State.
			);
		END CATCH

	COMMIT TRANSACTION;
END

