﻿using System.ServiceModel;

namespace BlobService
{
    [ServiceContract]
    public interface IBlobService
    {
        [OperationContract]
        bool UploadFile(byte[] fileBytes, string fileName);

        [OperationContract]
        byte[] GetFile(string fileName);

        [OperationContract]
        bool DeleteFile(string fileName);
    }
}