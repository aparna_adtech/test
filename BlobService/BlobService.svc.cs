﻿using System;
using System.IO;

namespace BlobService
{
    public class BlobService : IBlobService
    {
        public bool UploadFile(byte[] fileBytes, string fileName)
        {
            var isSuccess = false;
            var tempFolderPath = System.Configuration.ConfigurationManager.AppSettings.Get("FileUploadPath");

            try
            {
                if (!string.IsNullOrEmpty(tempFolderPath) && !string.IsNullOrEmpty(fileName))
                {
                    var fullPath = tempFolderPath + "\\" + fileName;
                    Directory.CreateDirectory(Path.GetDirectoryName(fullPath));

                    var fileStream = new FileStream(fullPath, FileMode.OpenOrCreate, FileAccess.ReadWrite);

                    using (var fs = fileStream)
                    {
                        fs.Write(fileBytes, 0, fileBytes.Length);
                        isSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return isSuccess;
        }
  
        public byte[] GetFile(string fileName)
        {
            var tempFolderPath = System.Configuration.ConfigurationManager.AppSettings.Get("FileUploadPath");
            var fullPath = tempFolderPath + "\\" + fileName;
            if (File.Exists(fullPath))
            {
                return File.ReadAllBytes(fullPath);
            }
            throw new FileNotFoundException("Unable to find file.", fullPath);
        }

        public bool DeleteFile(string fileName)
        {
            var tempFolderPath = System.Configuration.ConfigurationManager.AppSettings.Get("FileUploadPath");
            var fullPath = tempFolderPath + "\\" + fileName;
            if (File.Exists(fullPath))
            {
                File.Delete(fullPath);
                return true;
            }
            throw new FileNotFoundException("Unable to find file.", fullPath);
        }
    }
}
