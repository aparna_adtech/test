using ClassLibrary.DAL;

namespace BregRestfulService.Models
{
    public class SupplierJson
    {
        public SupplierJson(int? masterCatalogSupplierId, int? thirdPartySupplierId, bool isActive)
        {
            Id =
                masterCatalogSupplierId != null
                    ? masterCatalogSupplierId + "M"
                    : thirdPartySupplierId + "T";
            IsMasterCatalog = masterCatalogSupplierId != null;
            MasterCatalogSupplierId = masterCatalogSupplierId;
            ThirdPartySupplierId = thirdPartySupplierId;
            IsActive = isActive;
        }

        public string Id { get; set; }
        public bool IsMasterCatalog { get; set; }
        public int? MasterCatalogSupplierId { get; set; }
        public int? ThirdPartySupplierId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }

    }
}