﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Http;
using System.Web.WebPages;
using BregRestfulService.Models;
using ClassLibrary.DAL;

namespace BregRestfulService.Controllers
{
    [RoutePrefix("api/invoiceExport")]
    public class InvoiceExportController : ApiController
    {
        public IHttpActionResult Get(int id)
        {
            if (!UserController.IsUserAuthorized(id, Request))
            {
                return Unauthorized();
            }
            List<Invoice> invoices = GetInvoices(id);
            return Ok(invoices);
        }

        public IHttpActionResult Put([FromBody] Invoice[] selectedInvoices,int id)
        {
            using (var db = VisionDataContext.GetVisionDataContext())
            {
                try
                {
                    var invoicesToExport = GetInvoicesToExport(selectedInvoices, db);
                    var fileName = GetFileName(id);
                    var path = GetFilePath(fileName);

                    using(var exportWriter = new StreamWriter(path,false))
                    {
                        AddHeaderToExportStream(exportWriter);
                        foreach (var invoiceToExport in invoicesToExport)
                        {
                            exportWriter.WriteLine(GetExportData(invoiceToExport));
                            invoiceToExport.IsExported = true;
                        }
                    }
                    db.SubmitChanges();
                    return Ok(fileName);
                }
                catch (Exception e)
                {
                    
                    throw e;
                }
            }
           
        }

        private static string GetFilePath(string fileName)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath(string.Format("~/InvoiceExportFiles/{0}", fileName));
            return path;
        }

        private string GetFileName(int practiceId)
        {
            return string.Format("BregInvoiceExport_{0}_{1}_{2}.csv", practiceId, DateTime.UtcNow.ToString("MMddyyyy"), DateTime.UtcNow.ToString("HHmmss"));
        }

        private static List<ClassLibrary.DAL.Invoice> GetInvoicesToExport(Invoice[] selectedInvoices, VisionDataContext db)
        {
            var invoicesToExport = selectedInvoices.Join(db.Invoices, a => a.Id, b => b.InvoiceID, (a, b) => new {a, b})
                .Select(invoice => invoice.b).ToList();
            return invoicesToExport;
        }

        private List<Invoice> GetInvoices(int practiceId)
        {
            List<Invoice> invoices;

            using (VisionDataContext db = VisionDataContext.GetVisionDataContext())
            {
                try
                {
                    invoices =
                        new List<Invoice>(db.Invoices.Where(inv => inv.PracticeID == practiceId && inv.IsActive)
                            .OrderByDescending(inv => inv.Date)
                            .Select(inv => new Invoice
                            {
                                Id = inv.InvoiceID,
                                Supplier = new SupplierJson(inv.MasterCatalogSupplierID, inv.ThirdPartySupplierID, true)
                                {
                                    Name =
                                        (inv.MasterCatalogSupplier != null
                                            ? inv.MasterCatalogSupplier.SupplierName
                                            : inv.ThirdPartySupplier.SupplierName)
                                },
                                DateAsJSON = inv.Date ?? (DateTime) inv.Date,
                                Number = inv.Number,
                                Tax = inv.SalesTax,
                                ShippingCharges = inv.ShippingCost,
                                Total = inv.Total,
                                IsPaid = inv.IsPaid,
                                IsExported = inv.IsExported
                            }));
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return invoices;
        }

        public static string Escape(string s)
        {
            if (s.Contains(QUOTE))
                s = s.Replace(QUOTE, ESCAPED_QUOTE);

            if (s.IndexOfAny(CHARACTERS_THAT_MUST_BE_QUOTED) > -1)
                s = QUOTE + s + QUOTE;

            return s;
        }

        public static string Unescape(string s)
        {
            if (s.StartsWith(QUOTE) && s.EndsWith(QUOTE))
            {
                s = s.Substring(1, s.Length - 2);

                if (s.Contains(ESCAPED_QUOTE))
                    s = s.Replace(ESCAPED_QUOTE, QUOTE);
            }

            return s;
        }

        private const string QUOTE = "\"";
        private const string ESCAPED_QUOTE = "\"\"";
        private static char[] CHARACTERS_THAT_MUST_BE_QUOTED = { ',', '"', '\n' };

        public class Invoice
        {
            public int Id { get; set; }
            public string Number { get; set; }
            public SupplierJson Supplier { get; set; }
            public decimal? Total { get; set; }
            public DateTime? DateAsJSON { get; set; }
            public DateTime? Date { get; set; }
            public bool IsPaid { get; set; }
            public bool IsExported { get; set; }
            public decimal? Tax { get; set; }
            public decimal? ShippingCharges { get; set; }
        }

        public static MemoryStream  CreateExportStream()
        {
            const string exportHeader = "DOCUMENT TYPE,DESCRIPTION,BATCH ID,DOC. DATE,POSTING DATE,PO NUMBER,VENDOR ID,VENDOR ADDRESS ID,INVOICE NUMBER,INVOICE TOTAL,LOCATION GL#,PAYABLE GL#";
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(exportHeader);
            return stream;
        }
       public static void  AddHeaderToExportStream(StreamWriter exportWriter)
        {
            const string exportHeader = "DOCUMENT TYPE,DESCRIPTION,BATCH ID,DOC. DATE,POSTING DATE,PO NUMBER,VENDOR ID,VENDOR ADDRESS ID,INVOICE NUMBER,INVOICE TOTAL,LOCATION GL#,PAYABLE GL#";
            exportWriter.WriteLine(exportHeader);
        }

        public static string GetExportFileName(DateTime fileDate)
        {
            var fileName = "BregInvoiceExport_" + fileDate.ToString("MMddyyyy_HHmm");

            fileName += ".csv";
            return fileName;
        }

        public static string GetExportData(ClassLibrary.DAL.Invoice invoice)
        {
            var exportData = new List<string>();
            exportData.Add(Escape("INV"));                                  //DOCUMENT TYPE
            exportData.Add(Escape("Import from Vision"));                   //DESCRIPTION

            var batchId = "Vision " + DateTime.UtcNow.ToString("MMddyyyy");
            exportData.Add(Escape(batchId));                                //BATCH ID

            exportData.Add(Escape(invoice.Date != null ? 
                invoice.Date.Value.ToString("d"):""));                      //DOC. DATE
            exportData.Add(Escape(DateTime.UtcNow.ToString("d")));          //POSTING DATE

            var invoiceLineItem = invoice.InvoiceLineItems.FirstOrDefault();

            PopulatePoNumber(invoiceLineItem, exportData);                  //PO NUMBER
            PopulateVendorId(invoice, exportData);                          //VENDOR ID
            exportData.Add(Escape("MAIN"));                                 //VENDOR ADDRESS ID
            exportData.Add(Escape(invoice.Number));                         //INVOICE NUMBER
            exportData.Add(Escape(invoice.Total.ToString()));               //INVOICE TOTAL
            populateGeneralLedgerData(invoiceLineItem, exportData);         //GENERAL LEDGER

            return String.Join(",", exportData);
        }

        private static void PopulateVendorId(ClassLibrary.DAL.Invoice invoice, List<string> exportData)
        {
            var vendorId = invoice.MasterCatalogSupplier != null
                ? invoice.MasterCatalogSupplier.SupplierShortName
                : invoice.ThirdPartySupplier.SupplierShortName;
            exportData.Add(Escape(vendorId));
        }

        private static void PopulatePoNumber(InvoiceLineItem invoiceLineItem, List<string> exportData)
        {
            var poNumber = "";
            if (invoiceLineItem != null)
            {
                var supplierOrder =
                    invoiceLineItem.SupplierOrderLineItem.SupplierOrder;
                poNumber = !supplierOrder.CustomPurchaseOrderCode.IsEmpty()
                    ? supplierOrder.CustomPurchaseOrderCode
                    : supplierOrder.PurchaseOrder.ToString();
            }
            exportData.Add(Escape(poNumber));
        }

        private static void populateGeneralLedgerData(InvoiceLineItem invoiceLineItem, List<string> exportData)
        {
            var locationGl = "N/A";
            var practiceGl = "N/A";
            if (invoiceLineItem != null)
            {
                var practiceLocation =
                    invoiceLineItem.SupplierOrderLineItem.SupplierOrder.
                        BregVisionOrder.PracticeLocation;
                if (!practiceLocation.GeneralLedgerNumber.IsEmpty())
                {
                    locationGl = practiceLocation.GeneralLedgerNumber;
                }

                if (!practiceLocation.Practice.GeneralLedgerNumber.IsEmpty())
                {
                    practiceGl = practiceLocation.Practice.GeneralLedgerNumber;
                }
            }
            exportData.Add(Escape(locationGl)); //LOCATION GL#
            exportData.Add(Escape(practiceGl)); //PAYABLE GL#
        }
    }
}