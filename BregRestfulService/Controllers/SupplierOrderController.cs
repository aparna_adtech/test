﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using BregRestfulService.Models;
using ClassLibrary.DAL;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;

namespace BregRestfulService.Controllers
{
//    [Authorize]
    [RoutePrefix("api/supplierOrder")]
    public class SupplierOrderController : ApiController
    {
       public IHttpActionResult Post([FromBody] Search search, int id)
        {
            if (!UserController.IsUserAuthorized(302, Request))
            {
                return Unauthorized();
            }

            var json = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            json.SerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            var supplierOrders = GetSupplierOrders(id,search.FromDate);
            return Ok(supplierOrders);
        }

        private static List<SupplierOrder> GetSupplierOrders(int practiceId,string fromDateAsJson)
        {
            List<SupplierOrder> supplierOrders;

            DateTime? fromDate = null;

            if (fromDateAsJson != "")
            {
                fromDate = DateTime.Parse(fromDateAsJson);
            }


            using (var db = VisionDataContext.GetVisionDataContext())
            {
                var practiceCatalogSupplierBrands = db.PracticeCatalogSupplierBrands.Where(
                    pcsb => pcsb.PracticeID == practiceId).
                    Join(db.SupplierOrders, pcsb => pcsb.PracticeCatalogSupplierBrandID,
                        so => so.PracticeCatalogSupplierBrandID, (pcsb, so) =>
                            new { SupplerBrand = pcsb, SupplierOrder = so }).OrderByDescending(so => so.SupplierOrder.CreatedDate).AsQueryable();


                supplierOrders = new List<SupplierOrder>(practiceCatalogSupplierBrands.Select(supOrder =>  new SupplierOrder()
                    {
                        SupplierOrderId = supOrder.SupplierOrder.SupplierOrderID,
                        PONumber = supOrder.SupplierOrder.CustomPurchaseOrderCode.IsNullOrWhiteSpace() ? supOrder.SupplierOrder.PurchaseOrder.ToString():supOrder.SupplierOrder.CustomPurchaseOrderCode,
                        Supplier = new Models.SupplierJson(supOrder.SupplerBrand.MasterCatalogSupplierID,supOrder.SupplerBrand.ThirdPartySupplierID,supOrder.SupplerBrand.IsActive)
                        {
                            Name =
                                (supOrder.SupplerBrand.MasterCatalogSupplier != null
                                    ? supOrder.SupplerBrand.MasterCatalogSupplier.SupplierName
                                    : supOrder.SupplerBrand.ThirdPartySupplier.SupplierName),
                        },
                        DateAsJSON = supOrder.SupplierOrder.CreatedDate,
                    }));
            }
            return supplierOrders;
        }

        [HttpGet]
        [Route("lineitems/{id}")]
        public IHttpActionResult GetSupplierOrderLineItems(int id)
        {
            if (!UserController.IsUserAuthorized(302, Request))
            {
                return Unauthorized();
            }

            using (var db = VisionDataContext.GetVisionDataContext())
            {
                var lineItems = new List<LineItem> (db.SupplierOrders.First(so => so.SupplierOrderID == id)

                    .SupplierOrderLineItems.Select(li => new LineItem()
                    {
                        Id = li.SupplierOrderLineItemID,
                        Cost = li.ActualWholesaleCost,
                        ProductCode = li.PracticeCatalogProduct.MasterCatalogProduct != null
                            ? li.PracticeCatalogProduct.MasterCatalogProduct.Code
                            : li.PracticeCatalogProduct.ThirdPartyProduct.Code,
                        Name =
                            li.PracticeCatalogProduct.MasterCatalogProduct != null
                                ? li.PracticeCatalogProduct.MasterCatalogProduct.Name
                                : li.PracticeCatalogProduct.ThirdPartyProduct.Name,
                        QuantityCheckedIn =
                            li.ProductInventory_OrderCheckIns.Any()
                                ? li.ProductInventory_OrderCheckIns.Sum(checkIn => checkIn.Quantity)
                                : 0,
                        QuantityOrdered = li.QuantityOrdered,
                        Invoices = new List<InvoiceLink> (li.InvoiceLineItems.Where(ili => ili.IsActive && ili.Invoice.IsActive).Select(ili => new InvoiceLink()
                        {
                            Date = ili.Invoice.Date != null ? (DateTime) ili.Invoice.Date:(DateTime?) null,
                            InvoiceId = ili.InvoiceID,
                            Number = ili.Invoice.Number,
                            Quantity = ili.Quantity
                        }
                            ))
                    }));


                return Ok(lineItems);
            }
            
        }


        private static SupplierOrder CreateSupplierOrder(int i)
        {
            var supplierOrder = new SupplierOrder()
            {
                SupplierOrderId = 1,
                PONumber = "SO" + i,
//                Supplier = "Breg",
                DateAsJSON = DateTime.Now
            };
            for (int j = 1; j < 3; j++)
            {
                var number = j * i;
                var lineItem = new LineItem()
                {
                    Id = number,
                    ProductCode = "PC" + number,
                    Name = "Brace-" + number,
                    QuantityOrdered = number,
                    QuantityCheckedIn = number,
                    Cost = number
                };

                lineItem.QuantityCheckedIn = j;

                var invoice = new InvoiceLink()
                {
                    InvoiceId = number,
                    Date = DateTime.Now,
                    Quantity = number,
                    Number = "INV#" + number
                };
                
                lineItem.Invoices.Add(invoice);

			    supplierOrder.LineItems.Add(lineItem);

			}
            return supplierOrder;
        }

        public class SupplierOrder
        {
            public int SupplierOrderId { get; set; }
            public string PONumber { get; set; }
            public SupplierJson Supplier { get; set; }
            public DateTime DateAsJSON { get; set; }
            public List<LineItem> LineItems = new List<LineItem>();

        }

//            LineItems: [
//                            {Id:101,ProductCode:'B123', Name: 'Brace', QuantityOrdered: 3, QuantityCheckedIn: 2, Cost: 10.00 ,
//                            CheckIns: [
//                                { Date: '06-15-2015', Quantity: 1 },
//                                { Date: '06-25-2015', Quantity: 1 }
//                            ],
//                            Invoices: [
//                                { Date: '06-15-2015', Quantity: 1 , Number:'ABC123'}
//                            ]}

        public class LineItem
        {
            public int Id { get; set; }
            public string ProductCode { get; set; }
            public string Name { get; set; }
            public float QuantityOrdered { get; set; }
            public float QuantityCheckedIn { get; set; }
            public decimal Cost { get; set; }
 
            public List<InvoiceLink> Invoices = null;
        }


        public class CheckIn
        {
            public DateTime Date { get; set; }
            public int Quantity { get; set; }
        }

        public class InvoiceLink
        {
            public DateTime? Date { get; set; }
            public int Quantity { get; set; }
            public int InvoiceId { get; set; }
            public string Number { get; set; }
        }

        public class Search
        {
            public string FromDate { get; set; }
        }
    }
}
