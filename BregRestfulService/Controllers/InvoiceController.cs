﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web;
using System.Web.Http;
using BregRestfulService.Models;
using ClassLibrary.DAL;
using Microsoft.Ajax.Utilities;

namespace BregRestfulService.Controllers
{
    [RoutePrefix("api/invoice")]
    public class InvoiceController : ApiController
    {

        public IHttpActionResult Get(int id)
        {

            if (!UserController.IsUserAuthorized(id, Request))
            {
                return Unauthorized();
            }
            var invoices = GetInvoices(id);
            return Ok(invoices);
        }

        [HttpGet]
        [Route("{id}/{number}/{supplierIdMasterCatalog}/{supplierIdThirdParty}")]
        public IHttpActionResult Get(int id,string number, int supplierIdMasterCatalog, int supplierIdThirdParty)
        {

            if (!UserController.IsUserAuthorized(id, Request))
            {
                return Unauthorized();
            }
            var invoices = GetInvoiceByNumberAndSupplier(id,number,supplierIdMasterCatalog, supplierIdThirdParty);
            return Ok(invoices);
        }

        private List<Invoice> GetInvoiceByNumberAndSupplier(int practiceId, string number, int supplierIdMasterCatalog, int supplierIdThirdParty)
        {
            using (var db = VisionDataContext.GetVisionDataContext())
            {
                var invoices =
                    new List<Invoice>(
                    db.Invoices.Where(
                        inv =>
                            inv.PracticeID == practiceId && inv.Number == number && inv.IsActive &&
                            (inv.MasterCatalogSupplierID == supplierIdMasterCatalog || inv.ThirdPartySupplierID == supplierIdThirdParty))
                            .Select(inv => new Invoice
                            {
                                Id = inv.InvoiceID,
                                Supplier = new SupplierJson(inv.MasterCatalogSupplierID, inv.ThirdPartySupplierID, true)
                                {
                                    Name =
                                        (inv.MasterCatalogSupplier != null
                                            ? inv.MasterCatalogSupplier.SupplierName
                                            : inv.ThirdPartySupplier.SupplierName)
                                },
                                DateAsJSON = inv.Date ?? (DateTime)inv.Date,
                                Number = inv.Number,
                                ShippingCharges = inv.ShippingCost,
                                Tax = inv.SalesTax,
                                Total = inv.Total,
                                IsPaid = inv.IsPaid
                            }));
                return invoices;
            }
        }

        public IHttpActionResult Post([FromBody] Invoice invoice,int id)
        {
            if (!UserController.IsUserAuthorized(id, Request))
            {
                return Unauthorized();
            }
            var newInvoiceId = CreateNewInvoice(invoice, id);

            return Ok(newInvoiceId);
        }

        public IHttpActionResult Put([FromBody] Invoice invoice)
        {
            if (!UserController.IsUserAuthorized(302, Request))
            {
                return Unauthorized();
            }
            UpdateInvoice(invoice);

            return Ok();
        }

        public IHttpActionResult Delete(int id)
        {
            if (!UserController.IsUserAuthorized(302, Request))
            {
                return Unauthorized();
            }
            DeleteInvoice(id);

            return Ok();
        }

        private void DeleteInvoice(int invoiceId)
        {
            using (var db = VisionDataContext.GetVisionDataContext())
            {
                var invoiceToDelete =
                    db.Invoices.FirstOrDefault(
                        inv => inv.InvoiceID == invoiceId);
                if (invoiceToDelete != null)
                {
                    invoiceToDelete.IsActive = false;
                    db.SubmitChanges();
                }
            }
        }

        [HttpGet]
        [Route("lineitems/{invoiceId}")]
        public IHttpActionResult GetInvoiceLineItems(int invoiceId)
        {
            if (!UserController.IsUserAuthorized(302, Request))
            {
                return Unauthorized();
            }

            using (var db = VisionDataContext.GetVisionDataContext())
            {

                var invoiceLineItems = new List<InvoiceLineItem>(
                        db.Invoices.First(inv => inv.InvoiceID == invoiceId)
                            .InvoiceLineItems.Where(ili => ili.IsActive)
                            .Select(ili =>
                                new InvoiceLineItem()
                                {
                                    InvoiceLineItemId = ili.InvoiceLineItemID,
                                    SupplierOrderLineItemId = ili.SupplierOrderLineItemID,
                                    QuantityInvoiced = ili.Quantity,
                                    WholesaleCost = ili.WholesaleCost,
                                    ProductName = ili.ProductName,
                                    ProductCode = ili.ProductCode,
                                    PurchaseOrderNumber =
                                        ili.SupplierOrderLineItem.SupplierOrder.CustomPurchaseOrderCode.IsNullOrWhiteSpace() ?
                                        ili.SupplierOrderLineItem.SupplierOrder.PurchaseOrder.ToString() : ili.SupplierOrderLineItem.SupplierOrder.CustomPurchaseOrderCode
                                }));
                return Ok(invoiceLineItems);
            }
        }

        [HttpDelete]
        [Route("lineitem/{id}")]
        public IHttpActionResult DeleteInvoiceLineItem(int id)
        {
            if (!UserController.IsUserAuthorized(302, Request))
            {
                return Unauthorized();
            }
            DeleteLineItem(id);
            return Ok("deleted");
        }

        private static void DeleteLineItem(int id)
        {
            using (var db = VisionDataContext.GetVisionDataContext())
            {
                var lineItemToDelete =
                    db.InvoiceLineItems.FirstOrDefault(
                        lineItem => lineItem.InvoiceLineItemID == id);
                if (lineItemToDelete != null)
                {
                    lineItemToDelete.IsActive = false;
//                    lineItemToDelete.ModifiedUserId //Todo: set modified user
                    db.SubmitChanges();
                }
            }
        }

        [HttpPut]
        [Route("product")]
        public IHttpActionResult UpdatePracticeCatalogProduct([FromBody] InvoiceLineItem invoiceLineItem)
        {
            if (!UserController.IsUserAuthorized(302, Request))//TODO Get practice id
            {
                return Unauthorized();
            }

            var bregUserId = UserController.GetBregUserId(302, Request);
            UpdateProduct(invoiceLineItem, bregUserId);
            return Ok("updated");
        }

        private static void UpdateProduct(InvoiceLineItem invoiceLineItem, int bregUserId)
        {
            using (var db = VisionDataContext.GetVisionDataContext())
            {
                var practiceProduct =
                    db.SupplierOrderLineItems.First(
                        soli => soli.SupplierOrderLineItemID == invoiceLineItem.SupplierOrderLineItemId).PracticeCatalogProduct;
                practiceProduct.WholesaleCost = invoiceLineItem.WholesaleCost;
                practiceProduct.ModifiedDate = DateTime.Now;
                practiceProduct.ModifiedUserID =  bregUserId;
                db.SubmitChanges();
            }
        }

        private void UpdateInvoice(Invoice invoice)
        {
            using (var db = VisionDataContext.GetVisionDataContext())
            {
                var existingInvoice = db.Invoices.Single(inv => inv.InvoiceID == invoice.Id);
                existingInvoice.Number = invoice.Number;
                existingInvoice.Total = invoice.Total;
                existingInvoice.SalesTax = invoice.Tax;
                existingInvoice.ShippingCost = invoice.ShippingCharges;
                existingInvoice.Date = invoice.Date > DateTime.MinValue && invoice.Date < DateTime.MaxValue ? invoice.Date : (DateTime?)null;
                existingInvoice.IsPaid = invoice.IsPaid;
                existingInvoice.ModifiedDate = DateTime.Now;
                existingInvoice.ModifiedUserID = UserController.GetBregUserId(302, Request);
                foreach (var invoiceLineItem in invoice.InvoiceLineItems)
                {
                    var persistInvoiceLineItem = existingInvoice.InvoiceLineItems.
                        SingleOrDefault(existingLineItem =>
                            existingLineItem.InvoiceLineItemID == invoiceLineItem.InvoiceLineItemId);
                    if (persistInvoiceLineItem == null)
                    {
                        persistInvoiceLineItem = new ClassLibrary.DAL.InvoiceLineItem();
                        persistInvoiceLineItem.InvoiceID = invoice.Id;
                        persistInvoiceLineItem.SupplierOrderLineItemID = invoiceLineItem.SupplierOrderLineItemId;
                        persistInvoiceLineItem.ProductName = invoiceLineItem.ProductName;
                        persistInvoiceLineItem.ProductCode = invoiceLineItem.ProductCode;
                        persistInvoiceLineItem.IsActive = true;
                        persistInvoiceLineItem.CreatedDate = DateTime.Now;
                        db.InvoiceLineItems.InsertOnSubmit(persistInvoiceLineItem);
                    }

                    persistInvoiceLineItem.Quantity = invoiceLineItem.QuantityInvoiced;
                    persistInvoiceLineItem.WholesaleCost = invoiceLineItem.WholesaleCost;
                }


                db.SubmitChanges();
            }
        }
        private int CreateNewInvoice(Invoice invoice, int practiceId)
        {
            using (var db = VisionDataContext.GetVisionDataContext())
            {
                var newInvoice = new ClassLibrary.DAL.Invoice
                {
                    PracticeID = practiceId, //TODO: Get practice id from...post? Session?  Session.CurrentUser?
                    Number = invoice.Number,
                    MasterCatalogSupplierID = invoice.Supplier.MasterCatalogSupplierId,
                    ThirdPartySupplierID = invoice.Supplier.ThirdPartySupplierId,
                    Total = invoice.Total,
                    SalesTax = invoice.Tax,
                    ShippingCost = invoice.ShippingCharges,
                    Date = invoice.Date > DateTime.MinValue && invoice.Date < DateTime.MaxValue?invoice.Date:(DateTime?) null ,
                    IsActive = true,
                    CreatedDate = DateTime.Now,
                    CreatedUserID = UserController.GetBregUserId(practiceId, Request)
                };
                db.Invoices.InsertOnSubmit(newInvoice);
                db.SubmitChanges();
                return newInvoice.InvoiceID;
            }
        }

        private List<Invoice> GetInvoices(int practiceId)
        {
            List<Invoice> invoices;

            using (var db = VisionDataContext.GetVisionDataContext())
            {
                invoices =
                    new List<Invoice>(db.Invoices.Where(inv => inv.PracticeID == practiceId && inv.IsActive).OrderByDescending(inv => inv.Date)
                        .Select(inv => new Invoice()
                        {
                            Id = inv.InvoiceID,
                            Supplier = new SupplierJson(inv.MasterCatalogSupplierID, inv.ThirdPartySupplierID, true)
                            {
                                Name =
                                    (inv.MasterCatalogSupplier != null
                                        ? inv.MasterCatalogSupplier.SupplierName
                                        : inv.ThirdPartySupplier.SupplierName)
                            },
                            DateAsJSON = inv.Date??(DateTime) inv.Date,
                            Number = inv.Number,
                            ShippingCharges = inv.ShippingCost,
                            Tax = inv.SalesTax,
                            Total = inv.Total,
                            IsPaid = inv.IsPaid,
                            IsExported = inv.IsExported
                        }));
            }
            return invoices;
        }


        public class Invoice
        {
            public int Id { get; set; }
            public string Number { get; set; }
            public SupplierJson Supplier { get; set; }
            public decimal? Total { get; set; }
            public decimal? Tax { get; set; }
            public decimal? ShippingCharges { get; set; }
            public DateTime? DateAsJSON { get; set; }
            public DateTime? Date { get; set; }
            public bool IsPaid { get; set; }
            public List<InvoiceLineItem> InvoiceLineItems { get; set; }
            public bool IsExported { get; set; }
        }

        public class InvoiceLineItem
        {
            public int? InvoiceLineItemId { get; set; }
            public int SupplierOrderLineItemId { get; set; }
            public int QuantityInvoiced { get; set; }
            public decimal WholesaleCost { get; set; }
            public string ProductName { get; set; }
            public string ProductCode { get; set; }
            public string PurchaseOrderNumber { get; set; }
        }
    }

}
