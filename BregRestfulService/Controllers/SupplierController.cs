﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
 using System.Web;
 using System.Web.Http;
 using System.Web.UI;
 using BregRestfulService.Models;
 using ClassLibrary.DAL;

namespace BregRestfulService.Controllers
{
    public class SupplierController : ApiController
    {
        public IHttpActionResult Get(int id)
        {
            if (!UserController.IsUserAuthorized(302, Request))
            {
                return Unauthorized();
            }
            
            List<SupplierJson> suppliers;
            using (var db = VisionDataContext.GetVisionDataContext())
            {
                var suppliersListActive = GetSuppliersList(id, db, true);
                var suppliersListInActive = GetSuppliersList(id, db, false);
                var suppliersListCombinded = suppliersListActive.Union(suppliersListInActive);
                suppliers = new List<SupplierJson>(suppliersListCombinded.OrderBy(sl => sl.Name));
            }
            return Ok(suppliers);
        }

        private static IQueryable<SupplierJson> GetSuppliersList(int practiceId, VisionDataContext db, bool isActive)
        {
            var suppliersList = db.PracticeCatalogSupplierBrands.
                Where(pcsb => pcsb.PracticeID == practiceId && pcsb.IsActive == isActive).
                Select(pcsb => new SupplierJson(pcsb.MasterCatalogSupplierID, pcsb.ThirdPartySupplierID,isActive)
                {
                    Name =
                        (pcsb.MasterCatalogSupplier != null
                            ? pcsb.MasterCatalogSupplier.SupplierName
                            : pcsb.ThirdPartySupplier.SupplierName),
                }).Distinct();
            return suppliersList;
        }
    }

//    public class SupplierJson
//    {
//        public SupplierJson(int? masterCatalogSupplierId, int? thirdPartySupplierId, bool isActive)
//        {
//            Id =
//                masterCatalogSupplierId != null
//                    ? masterCatalogSupplierId + "M"
//                    : thirdPartySupplierId + "T";
//            IsMasterCatalog = masterCatalogSupplierId != null;
//            MasterCatalogSupplierId = masterCatalogSupplierId;
//            ThirdPartySupplierId = thirdPartySupplierId;
//            IsActive = isActive;
//        }
//
//        public string Id { get; set; }
//        public bool IsMasterCatalog { get; set; }
//        public int? MasterCatalogSupplierId { get; set; }
//        public int? ThirdPartySupplierId { get; set; }
//        public string Name { get; set; }
//        public bool IsActive { get; set; }
//    }
}
