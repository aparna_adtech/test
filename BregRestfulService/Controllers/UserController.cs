﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Security;
using ClassLibrary.DAL;
using Microsoft.Ajax.Utilities;

namespace BregRestfulService.Controllers
{
    public class UserController : ApiController
    {
        public IHttpActionResult GetAuthenticateUser(string token)
        {
            string userName = "rdashwood";

            string password = "#1111111";
            if (Membership.ValidateUser(userName, password)) //Validate the username and password
            {
                return Ok();
            }
            return Unauthorized();
        }

        public static User GetUserFromHash(string hash, int practiceId)
        {
            User user = null;

            try
            {
                var userIdLengthIndex = hash.LastIndexOf("|") + 1;

                var userIdLength = int.Parse(hash.Substring(userIdLengthIndex));

                var userId = hash.Substring(0, userIdLength);

                //remove userIdLength and delimiter
                hash = hash.Substring(0, userIdLengthIndex - 1);
                //remove userId and
                hash = hash.Substring(userIdLength);
                //remove "practiceId"
                hash = hash.Substring((practiceId * userIdLength).ToString().Length);

                var password = hash;

                user = new User
                {
                    Username = userId,
                    Password = password
                };

            }
            catch (Exception)
            {
                
                //log failed attempt?;
            } 
            return user;

        }

        public static bool IsUserAuthorized(string hash, int practiceId)
        {
            if(hash.IsNullOrWhiteSpace())
            {
                return false;
            }

            var user = GetUserFromHash(hash, practiceId);

            return user != null && Membership.ValidateUser(user.Username, user.Password);
        }

        public static int GetBregUserId(int practiceId, HttpRequestMessage request)
        {
            int userId = 1;
            
            var hashUser = GetUserFromHash(GetHashFromRequest(request),302);

            if (hashUser == null)
            {
                return userId;
            }

            var membershipUser = Membership.GetUser(hashUser.Username);

            if (membershipUser == null)
            {
                return userId;
            }
            
            using (var db = VisionDataContext.GetVisionDataContext())
            {
                userId = db.Users.First(user => user.AspNet_UserID == (Guid) membershipUser.ProviderUserKey).UserID;
            }

            return userId;
        }

        public static bool IsUserAuthorized(int practiceId, HttpRequestMessage request)
        {
            string token = GetHashFromRequest(request);

            return token != null && UserController.IsUserAuthorized(token, 302);
            //            return token != null && UserController.IsUserAuthorized(token, id);
        }

        private static string GetHashFromRequest(HttpRequestMessage request)
        {
            IEnumerable<string> tokens;
            request.Headers.TryGetValues("token", out tokens);

            if (tokens == null)
            {
                return null;
            }

            return tokens.FirstOrDefault();
        }
    }   
    public class User 
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string PracticeId { get; set; }
    }


    
 }
