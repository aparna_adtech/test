﻿using com.breg.vision.BCSClaimPushService.ServiceModels;
using com.breg.vision.MongoDataModels;
using System;

namespace com.breg.vision.BCSClaimPushService.DataModels
{
    public class BCSClaimPushLog : Entity
    {
        public BCSClaimSubmissionViewModel RequestData { get; set; }
        public string SentXml { get; set; }
        public string ResponseData { get; set; }
        public bool IsSuccess { get; set; }
        public string FailureReason { get; set; }
    }
}
