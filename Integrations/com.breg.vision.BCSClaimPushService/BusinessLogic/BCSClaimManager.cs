﻿using com.breg.vision.BCSClaimPushService.ServiceModels;
using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using com.breg.vision.BCSClaimPushService.BcsProxy;
using com.breg.vision.BCSClaimPushService.DataModels;
using com.breg.vision.MongoRepository;
using System.Threading.Tasks;
using com.breg.vision.Logger;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using com.breg.vision.BCSIntegrationHelper;

namespace com.breg.vision.BCSClaimPushService.BusinessLogic
{
    public class BCSClaimManager
    {
        private string sAuthToken = ConfigurationManager.AppSettings["sAuthToken"];
        private string bcsEndpoint = ConfigurationManager.AppSettings["BCSEndpointCongigurationName"];
        private readonly IBregGatewayService serviceProxy;
        private readonly MongoRepository<BCSClaimPushLog> repository;
        private readonly FileLogger logger;
        static string projectTitle = "com.breg.vision.BCSClaimManager";

        public BCSClaimManager()
        {
            serviceProxy = new BregGatewayServiceClient(bcsEndpoint);
            repository = new MongoRepository<BCSClaimPushLog>();
            logger = new FileLogger();
        }

        public async Task SubmitWithXMLAsync(BCSClaimSubmissionViewModel bcsClaimSubmissionViewModel)
        {
            var claimNumber = string.Empty;
            var claimPushLog = new BCSClaimPushLog() { RequestData = bcsClaimSubmissionViewModel, IsSuccess = true };

            logger.WriteLog($"BCS XML Creation", null, projectTitle, bcsClaimSubmissionViewModel.RMQID);
            var fileText = File.ReadAllText(System.Web.HttpContext.Current.Request.MapPath("~\\bcstemplate.xml"));
            var claimXML = string.Format(fileText, bcsClaimSubmissionViewModel.Patient_FName,
                            bcsClaimSubmissionViewModel.Patient_LName,
                            bcsClaimSubmissionViewModel.Patient_DOB.Value.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture),
                            bcsClaimSubmissionViewModel.DateOfService.Value.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture),
                            bcsClaimSubmissionViewModel.Patient_SelfPay,
                            ((bcsClaimSubmissionViewModel.ProductBarCodes != null) && !string.IsNullOrEmpty(bcsClaimSubmissionViewModel.ProductBarCodes.ElementAtOrDefault(0))) ? bcsClaimSubmissionViewModel.ProductBarCodes[0].ToString() : string.Empty,
                            ((bcsClaimSubmissionViewModel.ProductBarCodes != null) && !string.IsNullOrEmpty(bcsClaimSubmissionViewModel.ProductBarCodes.ElementAtOrDefault(1))) ? bcsClaimSubmissionViewModel.ProductBarCodes[1].ToString() : string.Empty,
                            ((bcsClaimSubmissionViewModel.ProductBarCodes != null) && !string.IsNullOrEmpty(bcsClaimSubmissionViewModel.ProductBarCodes.ElementAtOrDefault(2))) ? bcsClaimSubmissionViewModel.ProductBarCodes[2].ToString() : string.Empty,
                            ((bcsClaimSubmissionViewModel.ProductBarCodes != null) && !string.IsNullOrEmpty(bcsClaimSubmissionViewModel.ProductBarCodes.ElementAtOrDefault(3))) ? bcsClaimSubmissionViewModel.ProductBarCodes[3].ToString() : string.Empty,
                            ((bcsClaimSubmissionViewModel.ProductBarCodes != null) && !string.IsNullOrEmpty(bcsClaimSubmissionViewModel.ProductBarCodes.ElementAtOrDefault(4))) ? bcsClaimSubmissionViewModel.ProductBarCodes[4].ToString() : string.Empty,
                            bcsClaimSubmissionViewModel.PatientSigned.HasValue && bcsClaimSubmissionViewModel.PatientSigned.Value == true ? "Y" : "N",
                            bcsClaimSubmissionViewModel.PatientSignedDate.HasValue ? bcsClaimSubmissionViewModel.PatientSignedDate.Value.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) : string.Empty,
                            bcsClaimSubmissionViewModel.PhysicianSigned.HasValue && bcsClaimSubmissionViewModel.PhysicianSigned.Value == true ? "Y" : "N",
                            bcsClaimSubmissionViewModel.PhysicianSigned.HasValue && bcsClaimSubmissionViewModel.PhysicianSigned.Value == true ? "Y" : "N",
                            bcsClaimSubmissionViewModel.PhysicianSignedDate.HasValue ? bcsClaimSubmissionViewModel.PhysicianSignedDate.Value.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) : string.Empty,
                            bcsClaimSubmissionViewModel.PatientSignedRelationship,
                            bcsClaimSubmissionViewModel.PhysicianFirstName,
                            bcsClaimSubmissionViewModel.PhysicianLastName,
                            ((bcsClaimSubmissionViewModel.ICD9Codes != null) && !string.IsNullOrEmpty(bcsClaimSubmissionViewModel.ICD9Codes.ElementAtOrDefault(0))) ? bcsClaimSubmissionViewModel.ICD9Codes[0].ToString() : string.Empty,
                            ((bcsClaimSubmissionViewModel.ICD9Codes != null) && !string.IsNullOrEmpty(bcsClaimSubmissionViewModel.ICD9Codes.ElementAtOrDefault(1))) ? bcsClaimSubmissionViewModel.ICD9Codes[1].ToString() : string.Empty,
                            ((bcsClaimSubmissionViewModel.ICD9Codes != null) && !string.IsNullOrEmpty(bcsClaimSubmissionViewModel.ICD9Codes.ElementAtOrDefault(2))) ? bcsClaimSubmissionViewModel.ICD9Codes[2].ToString() : string.Empty,
                            ((bcsClaimSubmissionViewModel.ICD9Codes != null) && !string.IsNullOrEmpty(bcsClaimSubmissionViewModel.ICD9Codes.ElementAtOrDefault(3))) ? bcsClaimSubmissionViewModel.ICD9Codes[3].ToString() : string.Empty,
                            bcsClaimSubmissionViewModel.MRNNumber,
                            bcsClaimSubmissionViewModel.BcsSubInventoryId
                            );


            claimPushLog.SentXml = claimXML;

            try
            {
                logger.WriteLog($"Sending Request to BCG Service", null, projectTitle, bcsClaimSubmissionViewModel.RMQID);

                System.Net.ServicePointManager.ServerCertificateValidationCallback += (a, b, c, d) => true;
                claimPushLog.ResponseData = claimNumber = (await serviceProxy.SubmitClaimWithXMLAsync(new SubmitClaimWithXMLRequest(claimXML, sAuthToken, BCGSystemType.BCS, BCGOrderTypes.CO))).SubmitClaimWithXMLResult;
                logger.WriteLog($"Claim Number Recieved as {claimNumber}", null, projectTitle, bcsClaimSubmissionViewModel.RMQID);

                logger.WriteLog($"Sending Document Attachment to BCG Service", null, projectTitle, bcsClaimSubmissionViewModel.RMQID);

                var claimPdfDocument = bcsClaimSubmissionViewModel.CompressedPdf;
                if (claimPdfDocument != null)
                {
                    FileUploadMessage _appendMessage = new FileUploadMessage();
                    _appendMessage.ClaimNumber = claimNumber;
                    _appendMessage.AuthToken = sAuthToken;
                    _appendMessage.FileName = claimNumber.ToString()+".pdf";
                    _appendMessage.SystemType = BCGSystemType.BCS.ToString();
                    _appendMessage.FileBytesStream = new MemoryStream(claimPdfDocument);

                    serviceProxy.AppendDocumentToClaim(_appendMessage);
                }
                logger.WriteLog($"Document {claimNumber}.pdf Submitted to BCG", null, projectTitle, bcsClaimSubmissionViewModel.RMQID);

                //Queue the message back to handoff
                SubmitToHandoff(bcsClaimSubmissionViewModel, claimNumber, claimPushLog);
            }
            catch (Exception ex)
            {

                claimPushLog.IsSuccess = false;
                claimPushLog.FailureReason = ex.ToString();
                AddLogToRepository(claimPushLog, bcsClaimSubmissionViewModel.RMQID);
                logger.WriteLog(ex.Message, ex, "com.breg.vision.BCSClaimPushService", bcsClaimSubmissionViewModel.RMQID);
                throw;
            }

            AddLogToRepository(claimPushLog, bcsClaimSubmissionViewModel.RMQID);
        }

        public bool SubmitDocument(BCSDocumentSubmissionModel claim)
        {         
            try
            {
                logger.WriteLog($"Sending Document Request to BCG Service", null, projectTitle, claim.RMQID);
                System.Net.ServicePointManager.ServerCertificateValidationCallback += (a, b, c, d) => true;
                var response = serviceProxy.AppendDocumentToClaim(new FileUploadMessage() {
                     AuthToken = sAuthToken, 
                     ClaimNumber = claim.ClaimId, 
                     FileName = Guid.NewGuid().ToString("N"), 
                     SystemType = "BCS", 
                     FileBytesStream = string.IsNullOrEmpty(claim.Document) ? null : new MemoryStream(Convert.FromBase64String(claim.Document))
                });

                return true;
            }
            catch (Exception ex)
            {
                logger.WriteLog(ex.Message, ex, "com.breg.vision.BCSClaimPushService", claim.RMQID);
            }

            return false;
        }
        
        private void AddLogToRepository(BCSClaimPushLog claimPushLog, Guid rmqId)
        {
            try
            {
                claimPushLog = repository.Add(claimPushLog);
                logger.WriteLog($"Added BCS claim log to repository with ID - {claimPushLog.Id.ToString()}", null, "com.breg.vision.BCSClaimPushService", rmqId);
            }
            catch (Exception ex)
            {
                logger.WriteLog(ex.Message, ex, projectTitle, rmqId);
            }
        }

        private void SubmitToHandoff(BCSClaimSubmissionViewModel bcsClaimSubmissionViewModel, string claimNumber, BCSClaimPushLog claimPushLog)
        {
            var rmqId = bcsClaimSubmissionViewModel.RMQID;
            try
            {
                logger.WriteLog($"Sending Recieved claim number {claimNumber} to Handoff", null, projectTitle, rmqId);
                var responseModel = new BCSClaimResponseViewModel()
                {
                    ClaimId = claimNumber,
                    SourceEntity = bcsClaimSubmissionViewModel.SourceEntity,
                    SourceEntityID = bcsClaimSubmissionViewModel.SourceEntityID,
                    DocumentSource = bcsClaimSubmissionViewModel.DocumentSource,
                    DocumentType = bcsClaimSubmissionViewModel.DocumentType,
                    RMQID = bcsClaimSubmissionViewModel.RMQID
                };
                string responseJSON = JsonConvert.SerializeObject(responseModel);
                new BcsIntegrationHelper().PostDataToBcsIntegrationQueue(responseJSON, 
                                                                         bcsClaimSubmissionViewModel.RMQID,
                                                                         bcsClaimSubmissionViewModel.UserName,
                                                                         bcsClaimSubmissionViewModel.Password);
                logger.WriteLog($"Response Model posted to Integration Helper Successfully", null, projectTitle, rmqId);
            }
            catch (Exception ex)
            {
                claimPushLog.IsSuccess = false;
                claimPushLog.FailureReason = ex.ToString();
                AddLogToRepository(claimPushLog, rmqId);
                logger.WriteLog(ex.Message, ex, projectTitle, rmqId);
            }
        }
    }
}
