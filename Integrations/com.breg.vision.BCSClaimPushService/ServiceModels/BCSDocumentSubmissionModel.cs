﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace com.breg.vision.BCSClaimPushService.ServiceModels
{
    public class BCSDocumentSubmissionModel
    {
        [Required]
        public string ClaimId { get; set; }

        public Guid RMQID { get; set; }

        [Required]
        public string Document { get; set; }
    }
}