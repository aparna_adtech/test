﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace com.breg.vision.BCSClaimPushService.ServiceModels
{
    public class BCSClaimSubmissionViewModel
    {
        [Required]
        public string Patient_FName { get; set; }

        [Required]
        public string Patient_LName { get; set; }

        [Required]
        public DateTime? Patient_DOB { get; set; } //mm/dd/yyyy format

        [Required]
        public DateTime? DateOfService { get; set; } //mm/dd/yyyy format

        public bool Patient_SelfPay { get; set; }

        public int SourceEntity { get; set; }

        public int SourceEntityID { get; set; }

        public int DocumentType { get; set; }

        public int DocumentSource { get; set; }

        [Required]
        public Guid RMQID { get; set; }

        public List<string> ProductBarCodes { get; set; }

        public bool? PatientSigned { get; set; }
        public DateTime? PatientSignedDate { get; set; }

        public bool? PhysicianSigned { get; set; }
        public DateTime? PhysicianSignedDate { get; set; }

        public string PatientSignedRelationship { get; set; }

        public string PhysicianFirstName { get; set; }
        public string PhysicianLastName { get; set; }

        public List<string> ICD9Codes { get; set; }

        public string PhysicianNPICode { get; set; }
        public string MRNNumber { get; set; }

        public string BcsSubInventoryId { get; set; }
        public byte[] CompressedPdf { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
