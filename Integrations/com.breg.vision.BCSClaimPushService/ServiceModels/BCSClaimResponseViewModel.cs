﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.breg.vision.BCSClaimPushService.ServiceModels
{
    public class BCSClaimResponseViewModel
    {
        public int SourceEntity { get; set; }

        public int SourceEntityID { get; set; }

        public string ClaimId { get; set; }

        public Guid RMQID { get; set; }

        public int DocumentType { get; set; }

        public int DocumentSource { get; set; }
    }
}