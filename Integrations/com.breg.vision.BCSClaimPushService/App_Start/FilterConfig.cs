﻿using System.Web;
using System.Web.Mvc;

namespace com.breg.vision.BCSClaimPushService
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
