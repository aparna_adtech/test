﻿using com.breg.vision.BCSClaimPushService.DataModels;
using com.breg.vision.BCSClaimPushService.ServiceModels;
using Newtonsoft.Json;
using System.Web.Http;
using com.breg.vision.MongoRepository.Abstractions;
using System.Threading.Tasks;
using com.breg.vision.BCSClaimPushService.BusinessLogic;
using System;

namespace com.breg.vision.BCSClaimPushService.Controllers
{
    public class BCSClaimController : ApiController
    {
        private readonly BCSClaimManager bcsClaimManager;
        private readonly IRepository<BCSClaimPushLog> repository;
        private readonly Logger.FileLogger logger;

        public BCSClaimController()
        {
            this.bcsClaimManager = new BCSClaimManager();
            repository = new MongoRepository.MongoRepository<BCSClaimPushLog>();
            logger = new Logger.FileLogger();
        }

        [HttpPost]
        public async Task<IHttpActionResult> Post([FromBody]BCSClaimSubmissionViewModel bcsClaimSubmission)
        {
            try
            {
                if (bcsClaimSubmission == null)
                {
                    ModelState.AddModelError(nameof(BCSClaimSubmissionViewModel), "Input claim json is null or empty!.");
                }
                else
                {
                    Validate(bcsClaimSubmission);
                }

                if (!ModelState.IsValid)
                {
                    logger.WriteLog(JsonConvert.SerializeObject(ModelState), null, "com.breg.vision.BCSClaimPushService", bcsClaimSubmission.RMQID);
                    repository.Add(new BCSClaimPushLog() { IsSuccess = false, RequestData = bcsClaimSubmission, FailureReason = JsonConvert.SerializeObject(ModelState) });
                    return BadRequest(ModelState);
                }

                await bcsClaimManager.SubmitWithXMLAsync(bcsClaimSubmission);
            }
            catch (System.Exception ex)
            {
                var guidValue = bcsClaimSubmission != null && bcsClaimSubmission.RMQID != default(Guid) ? bcsClaimSubmission.RMQID : Guid.NewGuid();
                logger.WriteLog(ex.Message, ex, "com.breg.vision.BCSClaimPushService", guidValue);

                return BadRequest();
            }

            return Ok();
        }

        [HttpPut]
        public async Task<IHttpActionResult> Put([FromBody]BCSDocumentSubmissionModel model)
        {
            try
            {
                if (model == null)
                {
                    ModelState.AddModelError(nameof(BCSDocumentSubmissionModel), "Input claim json is null or empty!.");
                }
                else
                {
                    Validate(model);
                }

                if (!ModelState.IsValid)
                {
                    logger.WriteLog(JsonConvert.SerializeObject(ModelState), null, "com.breg.vision.BCSClaimPushService", model.RMQID);
                    return BadRequest(ModelState);
                }

                bcsClaimManager.SubmitDocument(model);
            }
            catch (System.Exception ex)
            {
                logger.WriteLog(ex.Message, ex, "com.breg.vision.BCSClaimPushService", Guid.NewGuid());

                return BadRequest();
            }

            return Ok();
        }
    }
}