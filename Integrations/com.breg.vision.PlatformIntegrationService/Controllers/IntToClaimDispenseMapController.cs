﻿using com.breg.vision.Handoff.ApiClient;
using com.breg.vision.Handoff.Models;
using com.breg.vision.Integration.VisionBcsModels;
using com.breg.vision.PlatformIntegrationService.BLL;
using com.breg.vision.PlatformIntegrationService.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace com.breg.vision.PlatformIntegrationService.Controllers
{
    [RoutePrefix("IntToClaimDispenseMap")]
    public class IntToClaimDispenseMapController : ApiController
    {
        private Logger.FileLogger logger = new Logger.FileLogger();
        private readonly string HandsOffServiceUri = ConfigurationManager.AppSettings["HandsOffServiceUrl"];
        private readonly string DocumentApiUrl = ConfigurationManager.AppSettings["DocumentApiUrl"];

        [Route("UpdateBcsClaimIntegrationRequest")]
        [ResponseType(typeof(BcsClaimIntRequest))]
        [HttpPost]
        public async Task<IHttpActionResult> UpdateBcsClaimIntegrationRequest([FromBody]BcsClaimReturn returnClaimModel)
        {
            try
            {
                bool result = false;
                BCSClaimIntRequestBLL req = new BCSClaimIntRequestBLL();

                if (returnClaimModel == null)
                {
                    ModelState.AddModelError(nameof(BcsClaimReturn), "Input claim json is null or empty!.");
                }
                else
                {
                    Validate(returnClaimModel);
                }

                string returnJSON = JsonConvert.SerializeObject(returnClaimModel);
                if (!ModelState.IsValid)
                {
                    logger.WriteLog($"Error reading data from Vision To Integration Queue {returnJSON}", null, "com.breg.vision.PlatformIntegrationService", Guid.NewGuid());
                    return BadRequest(ModelState);
                }
                result = req.CreateBCSClaimReturnModel(returnClaimModel, returnJSON);

                //TODO: Remove this code block from here and append it to BCSClaimManager.cs in the claim push service post document submission. This was for image submission which will be covered in later sprint
               
                //var webClient = new ApiClient();
                //var resp = await webClient.Get($"{DocumentApiUrl}?entityType={returnClaimModel.SourceEntity}&entityId={returnClaimModel.SourceEntityID}&docType={returnClaimModel.DocumentType}&documentSource={returnClaimModel.DocumentSource}");
                //try
                //{
                //    if (resp.IsSuccess)
                //    {
                //        IEnumerable<byte[]> documents = JsonConvert.DeserializeObject<IEnumerable<byte[]>>(resp.Content);
                //        if (documents != null && documents.Count() > 0)
                //        {
                //            var request = new BcsDocumentRequestModel() { ClaimId = returnClaimModel.ClaimId, RMQID = returnClaimModel.RMQID, Documents = documents.Select(d => Convert.ToBase64String(d)) };
                //            var data = new HandoffRequestModel() { JobType = JobTypes.IntegrationToBcsDoc, Payload = JsonConvert.SerializeObject(request) };
                //            webClient.Post<HandoffRequestModel>(HandsOffServiceUri, data).Wait();
                //        }
                //    }
                //    else
                //    {
                //        logger.WriteLog($"Unable to get document for Claim number: {returnClaimModel.ClaimId}", null, "com.breg.vision.PlatformIntegrationService", returnClaimModel.RMQID);
                //    }
                //}
                //catch (Exception e)
                //{
                //    logger.WriteLog($"Unable to get document for Claim number: {returnClaimModel.ClaimId}", e, "com.breg.vision.PlatformIntegrationService", returnClaimModel.RMQID);
                //}

                logger.WriteLog($"BCS claim integration request submitted for {returnClaimModel.SourceEntityID}", null, "com.breg.vision.PlatformIntegrationService", returnClaimModel.RMQID);
                return CreatedAtRoute("DefaultApi", result, req);
            }
            catch (System.Exception ex)
            {
                logger.WriteLog(ex.Message, ex, "com.breg.vision.BCSClaimPushService", Guid.NewGuid());
                //log the exception
            }

            return Ok();           
        }
    }
}
