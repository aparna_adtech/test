﻿using com.breg.vision.Integration.VisionBcsModels;
using com.breg.vision.PlatformIntegrationService.BLL;
using com.breg.vision.PlatformIntegrationService.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace com.breg.vision.PlatformIntegrationService.Controllers
{
    [RoutePrefix("VSNToIntRequests")]
    public class VSNToIntRequestsController : ApiController
    {
        private Logger.FileLogger logger = new Logger.FileLogger();
        private readonly EntityDocumentMappingBLL entityDocBA = new EntityDocumentMappingBLL();

        [Route("PostBcsClaimIntegrationRequest")]
        [ResponseType(typeof(BcsClaimIntRequest))]
        [HttpPost]
        public IHttpActionResult PostBcsClaimIntegrationRequest([FromBody]BcsClaimSubmission claim)
        {
            try
            {
                bool result = false;
                BCSClaimIntRequestBLL req = new BCSClaimIntRequestBLL();
                string claimJson = JsonConvert.SerializeObject(claim);

                if (claim == null)
                {
                    ModelState.AddModelError(nameof(BcsClaimSubmission), "Input claim json is null or empty!.");
                }
                else
                {
                    Validate(claim);   
                }

                if (!ModelState.IsValid)
                {
                    logger.WriteLog($"Error reading data from Vision To Integration Queue {claimJson}", null, "com.breg.vision.PlatformIntegrationService", Guid.NewGuid());
                    return BadRequest(ModelState);
                }

                this.SaveDocumentIds(claim);
                result = req.CreateBCSClaimSubmissionModel(claim, claimJson);
                logger.WriteLog($"BCS claim integration request submitted for {claim.SourceEntityID}", null, "com.breg.vision.PlatformIntegrationService", claim.RMQID);
                return CreatedAtRoute("DefaultApi", result, req);
            }
            catch (System.Exception ex)
            {
                logger.WriteLog(ex.Message, ex, "com.breg.vision.PlatformIntegrationService", Guid.NewGuid());
                //log the exception
            }

            return Ok();         
        }

        private void SaveDocumentIds(BcsClaimSubmission claim)
        {
            if (claim.DocumentIDs == null || claim.DocumentIDs.Length == 0)
                return;

            foreach (var doc in claim.DocumentIDs)
            {
                EntityDocumentMapping model = new EntityDocumentMapping()
                {
                    DocumentTypeID = claim.DocumentType,
                    DocumentSourceID = claim.DocumentSource,
                    EntityTypeID = claim.SourceEntity,
                    EntityID = claim.SourceEntityID, 
                    DocumentID = doc
                };

                entityDocBA.Insert(model);
            }
        }
    }
}
