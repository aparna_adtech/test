﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace com.breg.vision.PlatformIntegrationService.Entities
{
    [Table("EntityDocumentMapping")]
    public class EntityDocumentMapping
    {
        [Key]
        public int EntityDocumentMappingID { get; set; }

        [Required]
        public int EntityTypeID { get; set; }

        [ForeignKey("EntityTypeID")]
        public EntityType EntityType { get; set; }

        [Required]
        public int EntityID { get; set; }

        [Required]
        public int DocumentTypeID { get; set; }

        [ForeignKey("DocumentTypeID")]
        public DocumentType DocumentType { get; set; }

        [Required]
        public string DocumentID { get; set; }

        [Required]
        public int DocumentSourceID { get; set; }

        [ForeignKey("DocumentSourceID")]
        public DocumentSource DocumentSource { get; set; }

        [Required]
        public int CreatedUserID { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }
    }
}