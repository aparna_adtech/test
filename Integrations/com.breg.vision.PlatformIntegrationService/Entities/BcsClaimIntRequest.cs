﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace com.breg.vision.PlatformIntegrationService.Entities
{
    [Table("IntegrationRequests")]
    public class BcsClaimIntRequest
    {
        [Key]
        public int RequestID { get; set; }
        [Required]
        public Guid RMQID { get; set; }
        [Required]
        public int IntegrationID { get; set; }
        [Required]
        public int StatusID { get; set; }
        [Required]
        public int EntityID { get; set; }
        [Required]
        public string EntityValue { get; set; }

        public string SentJSON { get; set; }
        public string ResultJSON { get; set; }
        [Required]
        public DateTime CreatedOn { get; set; }
        [Required]
        public int CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
    }
}