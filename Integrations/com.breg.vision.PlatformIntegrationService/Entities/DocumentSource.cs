﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace com.breg.vision.PlatformIntegrationService.Entities
{
    [Table("DocumentSource")]
    public class DocumentSource
    {
        [Key]
        public int DocumentSourceID { get; set; }

        [Required]
        public string DocumentSourceName { get; set; }

        public ICollection<EntityDocumentMapping> EntityDocumentMappings { get; set; }
    }
}