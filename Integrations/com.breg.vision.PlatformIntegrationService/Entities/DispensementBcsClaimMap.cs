﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace com.breg.vision.PlatformIntegrationService.Entities
{
    [Table("DispensementBcsClaimMap")]
    public class DispensementBcsClaimMap
    {
        [Key]
        public int DispensementBCSClaimMapID { get; set; }
        [Required]
        public string BCSClaimID { get; set; }
        [Required]
        public int DispenseID { get; set; }

        [Required]
        [Column(TypeName = "DateTime2")]

        public DateTime CreatedOn { get; set; }
        [Required]
        public int CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
    }
}