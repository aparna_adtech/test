﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.breg.vision.PlatformIntegrationService.Entities
{
    public class BcsDocumentRequestModel
    {
        public string ClaimId { get; set; }
        public Guid RMQID { get; set; }
        public IEnumerable<string> Documents { get; set; }
    }
}