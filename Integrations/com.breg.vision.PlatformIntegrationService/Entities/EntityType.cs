﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace com.breg.vision.PlatformIntegrationService.Entities
{
    [Table("EntityType")]
    public class EntityType
    {
        [Key]
        public int EntityTypeID { get; set; }

        [Required]
        public string EntityTypeName { get; set; }

        public ICollection<EntityDocumentMapping> EntityDocumentMappings { get; set; }
    }
}