﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace com.breg.vision.PlatformIntegrationService.Entities
{
    [Table("DocumentType")]
    public class DocumentType
    {
        [Key]
        public int DocumentTypeID { get; set; }

        [Required]
        public string DocumentTypeName { get; set; }

        public ICollection<EntityDocumentMapping> EntityDocumentMappings { get; set; }
    }
}