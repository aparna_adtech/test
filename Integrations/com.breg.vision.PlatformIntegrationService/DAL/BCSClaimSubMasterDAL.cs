﻿namespace com.breg.vision.PlatformIntegrationService.DAL
{
    using com.breg.vision.Logger;
    using BLL;
    using Entities;

    public class BCSClaimSubMasterDAL
    {
        public static FileLogger _Logger = new FileLogger();
        static string projectTitle = "com.breg.vision.com.breg.vision.PlatformIntegrationService.DAL.BCSClaimSubMasterDAL";

        public bool SendMasterBCSClaimSunIntRequest(BCSClaimIntRequestBLL model)
        {
            BcsClaimIntRequestDAL dal_call = new BcsClaimIntRequestDAL();
            BcsClaimIntRequest newReq = new BcsClaimIntRequest();
            _Logger.WriteLog($"Start of SendMasterBCSClaimSunIntRequest", null, projectTitle, model.RMQID);
            //Create the required model for DAL layer
            newReq.CreatedBy = model.CreatedBy;
            newReq.CreatedOn = model.CreatedOn;
            newReq.EntityID = model.EntityID;
            newReq.EntityValue = model.EntityValue;
            newReq.IntegrationID = model.IntegrationID;
            newReq.ModifiedBy = model.ModifiedBy;
            newReq.ModifiedOn = model.ModifiedOn;
            newReq.SentJSON = model.SentJSON;
            newReq.RMQID = model.RMQID;
            newReq.StatusID = model.StatusID;

            //Call to DAL
            _Logger.WriteLog($"Call to PostBcsClaimIntegrationRequest", null, projectTitle, model.RMQID);
            return dal_call.PostBcsClaimIntegrationRequest(newReq);         
        }

        public bool UpdateMasterBCSClaimSubIntRequest(BCSClaimIntRequestBLL model)
        {
            BcsClaimIntRequestDAL dal_call = new BcsClaimIntRequestDAL();
            BcsClaimIntRequest newReq = new BcsClaimIntRequest();
            _Logger.WriteLog($"Start of UpdateMasterBCSClaimSubIntRequest", null, projectTitle, model.RMQID);
            //Create the required model for DAL layer
            newReq.ModifiedBy = model.ModifiedBy;
            newReq.ModifiedOn = model.ModifiedOn;
            newReq.ResultJSON = model.ResultJSON;
            newReq.StatusID = 4;
            newReq.EntityValue = model.EntityValue;
            newReq.RMQID = model.RMQID;

            //Call to DAL
            _Logger.WriteLog($"Call to UpdateBcsClaimIntegrationRequest", null, projectTitle, model.RMQID);
            return dal_call.UpdateBcsClaimIntegrationRequest(newReq);

        }
    }
}