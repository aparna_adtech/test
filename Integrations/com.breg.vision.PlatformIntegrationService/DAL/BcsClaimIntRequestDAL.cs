﻿using com.breg.vision.BCSIntegrationHelper;
using com.breg.vision.Integration.VisionBcsModels;
using com.breg.vision.Logger;
using com.breg.vision.PlatformIntegrationService.Entities;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Net.Http;
using System.Threading.Tasks;

namespace com.breg.vision.PlatformIntegrationService
{
    public class BcsClaimIntRequestDAL
    {
        VisionBcsIntegrationContext db = new VisionBcsIntegrationContext();
        IntToClaimDispenseMapContext dbContext = new IntToClaimDispenseMapContext();
        public static FileLogger _Logger = new FileLogger();
        static string projectTitle = "com.breg.vision.PlatformIntegrationService.BcsClaimIntRequestDAL";

        public bool PostBcsClaimIntegrationRequest(BcsClaimIntRequest bcsClaimIntegrationRequest)
        {
            _Logger.WriteLog($"Start of PostBcsClaimIntegrationRequest", null, projectTitle, bcsClaimIntegrationRequest.RMQID);

            string exception = string.Empty;
            try
            {
                db._bcsClaimIntegrationRequest.Add(bcsClaimIntegrationRequest);
                db.SaveChanges();

                _Logger.WriteLog($"IntegrationRequest saved to Db", null, projectTitle, bcsClaimIntegrationRequest.RMQID);

                // Now, Make a call to the RabbitMQ Queue with generated Request Id
                string sentJson = Convert.ToString(bcsClaimIntegrationRequest.SentJSON);
                return new BcsIntegrationHelper().PostDataToIntegrationBcsQueue(sentJson, bcsClaimIntegrationRequest.RMQID);
            }
            catch (System.Data.Entity.Core.UpdateException ex)
            {
                _Logger.WriteLog($"UpdateException", ex, projectTitle, bcsClaimIntegrationRequest.RMQID);
            }

            catch (System.Data.Entity.Infrastructure.DbUpdateException ex) //DbContext
            {
                _Logger.WriteLog($"DbUpdateException", ex, projectTitle, bcsClaimIntegrationRequest.RMQID);
            }

            catch (Exception ex)
            {
                _Logger.WriteLog($"OtherException", ex, projectTitle, bcsClaimIntegrationRequest.RMQID);
            }
            return false;
        }
        public bool UpdateBcsClaimIntegrationRequest(BcsClaimIntRequest bcsClaimIntegrationRequest)
        {
            _Logger.WriteLog($"Start of UpdateBcsClaimIntegrationRequest", null, projectTitle, bcsClaimIntegrationRequest.RMQID);
            string exception = string.Empty;
            try
            {
                string connString = ConfigurationManager.ConnectionStrings["VisionBcsIntegrationConnectionString"].ConnectionString.ToString();
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                string sql = $"UPDATE IntegrationRequests SET ResultJSON = @result WHERE RMQID = @rmqId";
                SqlCommand myCommand2 = new SqlCommand(sql, conn);
                myCommand2.Parameters.AddWithValue("@result", bcsClaimIntegrationRequest.ResultJSON);
                myCommand2.Parameters.AddWithValue("@rmqId", bcsClaimIntegrationRequest.RMQID);
                myCommand2.ExecuteNonQuery();
                conn.Close();

                _Logger.WriteLog($"IntegrationRequests updated in Db", null, projectTitle, bcsClaimIntegrationRequest.RMQID);

                UpdateBcsClaimSubMissionMap(bcsClaimIntegrationRequest.ResultJSON);
                return true;
            }
            catch (System.Data.Entity.Core.UpdateException ex)
            {
                _Logger.WriteLog($"UpdateException", ex, projectTitle, bcsClaimIntegrationRequest.RMQID);
            }

            catch (System.Data.Entity.Infrastructure.DbUpdateException ex) //DbContext
            {
                _Logger.WriteLog($"DbUpdateException", ex, projectTitle, bcsClaimIntegrationRequest.RMQID);
            }

            catch (Exception ex)
            {
                _Logger.WriteLog($"OtherException", ex, projectTitle, bcsClaimIntegrationRequest.RMQID);
            }
            return false;
        }
        public bool UpdateBcsClaimSubMissionMap(string returnJSON)
        {
            DispensementBcsClaimMap dispensementClaimMapReq = null;
            string exception = string.Empty;
            BcsClaimReturn returnModel = JsonConvert.DeserializeObject<BcsClaimReturn>(returnJSON);
            dispensementClaimMapReq = new DispensementBcsClaimMap();

            try
            {
                if (!string.IsNullOrEmpty(GetClaimMapData(returnModel.ClaimId)))
                {
                    _Logger.WriteLog($"Inside of UpdateBcsClaimSubMissionMap", null, projectTitle, returnModel.RMQID);

                    dispensementClaimMapReq.DispenseID = returnModel.SourceEntity;
                    dispensementClaimMapReq.BCSClaimID = returnModel.ClaimId;
                    dispensementClaimMapReq.CreatedOn = Convert.ToDateTime(DateTime.Now);
                    dispensementClaimMapReq.CreatedBy = 23;
                    dispensementClaimMapReq.ModifiedBy = 23;
                    dispensementClaimMapReq.ModifiedOn = DateTime.Now;

                    dbContext._dispensementBCSClaimMap.Add(dispensementClaimMapReq);
                    // other changed properties
                    dbContext.SaveChanges();

                    _Logger.WriteLog($"DispensementBcsClaimMap saved successfully", null, projectTitle, returnModel.RMQID);

                    return true;
                }
            }
            catch (System.Data.Entity.Core.UpdateException ex)
            {
                _Logger.WriteLog($"UpdateException", ex, projectTitle, returnModel.RMQID);
            }

            catch (System.Data.Entity.Infrastructure.DbUpdateException ex) //DbContext
            {
                _Logger.WriteLog($"DbUpdateException", ex, projectTitle, returnModel.RMQID);
            }

            catch (Exception ex)
            {
                _Logger.WriteLog($"OtherException", ex, projectTitle, returnModel.RMQID);
            }
            return false;
        }

        private string GetClaimMapData(string claimId)
        {
            var correlationId = Guid.NewGuid();
            _Logger.WriteLog($"Start of GetClaimMapData", null, projectTitle, correlationId);
            string exception = string.Empty;
          
            try
            {
                string connString = ConfigurationManager.ConnectionStrings["IntToClaimDispenseMapConnectionString"].ConnectionString.ToString();
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                string sql = $"Select BCSClaimID from DispensementBcsClaimMap WHERE BCSClaimID = @claimId";
                SqlCommand myCommand2 = new SqlCommand(sql, conn);
                myCommand2.Parameters.AddWithValue("@claimId", claimId);
                int claimID = Convert.ToInt32(myCommand2.ExecuteNonQuery());
                conn.Close();

                _Logger.WriteLog($"End of BcsClaimMapData", null, projectTitle, correlationId);                
                return claimID.ToString();
            }
            catch (System.Data.Entity.Core.UpdateException ex)
            {
                _Logger.WriteLog($"UpdateException", ex, projectTitle, correlationId);
            }

            catch (System.Data.Entity.Infrastructure.DbUpdateException ex) //DbContext
            {
                _Logger.WriteLog($"DbUpdateException", ex, projectTitle, correlationId);
            }

            catch (Exception ex)
            {
                _Logger.WriteLog($"OtherException", ex, projectTitle, correlationId);
            }
            return null;
        }
       
    }
}