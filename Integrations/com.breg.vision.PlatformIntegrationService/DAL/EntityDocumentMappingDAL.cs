﻿namespace com.breg.vision.PlatformIntegrationService.DAL
{
    using com.breg.vision.Logger;
    using Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class EntityDocumentMappingDAL
    {
        VisionBcsIntegrationContext db = new VisionBcsIntegrationContext();
        public static FileLogger _Logger = new FileLogger();
        static string projectTitle = "com.breg.vision.com.breg.vision.PlatformIntegrationService.DAL.EntityDocumentMappingDAL";

        public bool Insert(EntityDocumentMapping model)
        {
            string exception = string.Empty;
            var corelationId = Guid.NewGuid();
            try
            {
                db._entityDocumentMapping.Add(model);
                db.SaveChanges();

                _Logger.WriteLog($"Document ID saved to Db", null, projectTitle, corelationId);

                return true;
            }
            catch (System.Data.Entity.Core.UpdateException ex)
            {
                _Logger.WriteLog($"UpdateException", ex, projectTitle, corelationId);
            }

            catch (System.Data.Entity.Infrastructure.DbUpdateException ex) //DbContext
            {
                _Logger.WriteLog($"DbUpdateException", ex, projectTitle, corelationId);
            }

            catch (Exception ex)
            {
                _Logger.WriteLog($"OtherException", ex, projectTitle, corelationId);
            }
            return false;
        }

        public IList<EntityDocumentMapping> Get(int documentSourceId, int documentTypeId, int entityTypeId, int entityId)
        {
            return db._entityDocumentMapping.Where(ent => ent.DocumentSourceID == documentSourceId &&
                                                   ent.DocumentTypeID == documentTypeId &&
                                                   ent.EntityTypeID == entityTypeId &&
                                                   ent.EntityID == entityId).ToList();
        }
    }
}