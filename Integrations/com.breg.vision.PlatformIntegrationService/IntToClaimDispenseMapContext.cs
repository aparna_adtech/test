﻿using com.breg.vision.PlatformIntegrationService.Entities;
using System.Data.Entity;

namespace com.breg.vision.PlatformIntegrationService
{
    public class IntToClaimDispenseMapContext : DbContext
    {
        public IntToClaimDispenseMapContext()
            : base("name=IntToClaimDispenseMapConnectionString")
        {
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<IntToClaimDispenseMapContext>(null);
            base.OnModelCreating(modelBuilder);
        }
        public DbSet<DispensementBcsClaimMap> _dispensementBCSClaimMap { get; set; }
    }
}