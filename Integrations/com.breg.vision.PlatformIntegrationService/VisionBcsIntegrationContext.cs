﻿namespace com.breg.vision.PlatformIntegrationService
{
    using System.Data.Entity;
    using Entities;

    public class VisionBcsIntegrationContext:DbContext
    {
        public VisionBcsIntegrationContext()
            : base("name=VisionBcsIntegrationConnectionString")
        {
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<VisionBcsIntegrationContext>(null);
            base.OnModelCreating(modelBuilder);
        }
        public DbSet<BcsClaimIntRequest> _bcsClaimIntegrationRequest { get; set; }
        public DbSet<DocumentType> _documentType { get; set; }
        public DbSet<DocumentSource> _documentSource { get; set; }
        public DbSet<EntityType> _entityType { get; set; }
        public DbSet<EntityDocumentMapping> _entityDocumentMapping { get; set; }
    }
}