﻿namespace com.breg.vision.PlatformIntegrationService.BLL
{
    using DAL;
    using Entities;
    using System;
    using System.Collections.Generic;

    public class EntityDocumentMappingBLL
    {
        private readonly EntityDocumentMappingDAL db;
        public EntityDocumentMappingBLL()
        {
            db = new EntityDocumentMappingDAL();
        }

        public bool Insert(EntityDocumentMapping model)
        {
            model.CreatedDate = DateTime.Now;
            return db.Insert(model);
        }

        public IList<EntityDocumentMapping> Get(int documentSourceId, int documentTypeId, int entityTypeId, int entityId)
        {
            return db.Get(documentSourceId, documentTypeId, entityTypeId, entityId);
        }
    }
}