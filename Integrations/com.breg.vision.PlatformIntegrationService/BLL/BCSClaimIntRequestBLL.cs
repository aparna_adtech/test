﻿using com.breg.vision.Integration.VisionBcsModels;
using com.breg.vision.Logger;
using com.breg.vision.PlatformIntegrationService.DAL;
using System;

namespace com.breg.vision.PlatformIntegrationService.BLL
{
    public class BCSClaimIntRequestBLL
    {
        public int RequestID { get; set; }
        public Guid RMQID { get; set; }
        public int IntegrationID { get; set; }
        public int StatusID { get; set; }
        public int EntityID { get; set; }
        public string EntityValue { get; set; }
        public string SentJSON { get; set; }
        public string ResultJSON { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }        
        public string ClaimId { get; set; }
        public static FileLogger _Logger = new FileLogger();
        static string projectTitle = "com.breg.vision.com.breg.vision.PlatformIntegrationService.BLL.BCSClaimIntRequestBLL";

        public bool CreateBCSClaimSubmissionModel(BcsClaimSubmission bcsClaimModel, string claimJson)
        {
            _Logger.WriteLog($"Start of CreateBCSClaimSubmissionModel", null, projectTitle, bcsClaimModel.RMQID);
            BCSClaimIntRequestBLL objModel = new BCSClaimIntRequestBLL();
            objModel.CreatedBy = 23;
            objModel.CreatedOn = DateTime.Now;
            objModel.EntityID = bcsClaimModel.SourceEntityID;
            objModel.EntityValue = bcsClaimModel.SourceEntity.ToString();
            objModel.IntegrationID = 1;
            objModel.StatusID = 1;
            objModel.RMQID = bcsClaimModel.RMQID;
            objModel.SentJSON = claimJson;
            objModel.ModifiedBy = 1;
            objModel.ModifiedOn = DateTime.Now;

            //Make a DAL call
            BCSClaimSubMasterDAL master_call = new BCSClaimSubMasterDAL();
            _Logger.WriteLog($"Call to SendMasterBCSClaimSunIntRequest", null, projectTitle, bcsClaimModel.RMQID);
            return master_call.SendMasterBCSClaimSunIntRequest(objModel);
            
        }

        public bool CreateBCSClaimReturnModel(BcsClaimReturn bcsReturnClaimModel, string returnJson)
        {
            _Logger.WriteLog($"Start of CreateBCSClaimReturnModel", null, projectTitle, bcsReturnClaimModel.RMQID);
            BCSClaimIntRequestBLL objModel = new BCSClaimIntRequestBLL();
           
            objModel.ResultJSON = returnJson;
            objModel.ModifiedBy = 1;
            objModel.ModifiedOn = DateTime.Now;
            objModel.ClaimId = bcsReturnClaimModel.ClaimId;
            objModel.EntityValue = bcsReturnClaimModel.SourceEntity.ToString();
            objModel.RMQID = bcsReturnClaimModel.RMQID;

            //Make a DAL call
            BCSClaimSubMasterDAL master_call = new BCSClaimSubMasterDAL();
            _Logger.WriteLog($"Call to UpdateMasterBCSClaimSubIntRequest", null, projectTitle, bcsReturnClaimModel.RMQID);
            return master_call.UpdateMasterBCSClaimSubIntRequest(objModel);

        }
    }
}
    
