﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.breg.vision.Integration.VisionBcsModels
{
    public class BcsClaimReturn
    {
        public int SourceEntity { get; set; }

        public int SourceEntityID { get; set; }

        public string ClaimId { get; set; }

        public Guid RMQID { get; set; }

        public int DocumentType { get; set; }

        public int DocumentSource { get; set; }
    }
}
