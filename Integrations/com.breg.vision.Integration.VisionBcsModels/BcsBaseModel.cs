﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.breg.vision.Integration.VisionBcsModels
{
    public  abstract class BcsBaseModel 
    {
        public int SourceEntity { get; set; }

        public int SourceEntityID { get; set; }

        public int DocumentSource { get; set; }
        public int DocumentType { get; set; }

        public string[] DocumentIDs { get; set; }

        public BcsBaseModel()
        {

        }
    }
}
