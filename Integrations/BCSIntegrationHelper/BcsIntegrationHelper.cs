﻿using com.breg.vision.Handoff.ApiClient;
using com.breg.vision.Handoff.Models;
using com.breg.vision.Integration.VisionBcsModels;
using com.breg.vision.Logger;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.BCSIntegrationHelper
{
    public class BcsIntegrationHelper
    {
        private string RMQRequestURI = ConfigurationManager.AppSettings["RMQServiceUrl"];
        private string RMQRequestURIParams = ConfigurationManager.AppSettings["RMQueueRequestParameters"];
        private static string HandOffServiceUri = ConfigurationManager.AppSettings["HandOffServiceUrl"];
        private static string HandOffAuthUri = ConfigurationManager.AppSettings["HandOffAuthUrl"];
        public static FileLogger logger = new FileLogger();
        static string projectTitle = "com.breg.vision.BcsIntegrationHelper";
        public void PostDataToVisionIntegrationQueue(BcsClaimSubmission claim, Guid correlationId)
        {
            try
            {
                var claimJSON = JsonConvert.SerializeObject(claim);
               
                //creating the client
                var webClient = new ApiClient();

                var HandoffAAuthToken = GetHandoffAuthToken(HandOffAuthUri, claim.UserName, claim.Password, correlationId);

                if (!string.IsNullOrEmpty(HandoffAAuthToken) && !string.IsNullOrEmpty(claimJSON))
                {
                    var data = new HandoffRequestModel() { JobType = JobTypes.VisionToIntegration, Payload = claimJSON };
                    webClient.Post<HandoffRequestModel>(HandOffServiceUri, data, HandoffAAuthToken, isHandoffCall:true).Wait();
                }
                else
                {
                    logger.WriteLog("PostDataToVisionIntegrationQueue: Handoff auth token/claim json null - could not queue request", null, projectTitle, correlationId);
                    return;
                }

                if (claim!= null && (claim.DocumentIDs != null && claim.DocumentIDs.Length > 0))
                {
                    // Send dispensement id info to Document service for upgrade.
                    var docData = new HandoffRequestModel() { JobType = JobTypes.VisionToDocument, Payload = claimJSON };
                    webClient.Put<HandoffRequestModel>(HandOffServiceUri, docData).Wait();
                }
                else
                {
                    logger.WriteLog("PostDataToVisionIntegrationQueue: could not queue document onto VSNToIntQueue", null, projectTitle, correlationId);
                    return;
                }               

                logger.WriteLog($"Pushed message from Vision to Integration Queue {claimJSON}", null, projectTitle, correlationId);
                return;
            }
            catch (Exception ex)
            {
                logger.WriteLog($"Exception in PostDataToVisionIntegrationQueue", ex, projectTitle, correlationId);
                return;
            }

        }

        public bool PostDataToIntegrationBcsQueue(string resultJson, Guid rmqId)
        {
            try
            {
                var claim = JsonConvert.DeserializeObject<BcsClaimSubmission>(resultJson);            
                var HandoffAAuthToken = GetHandoffAuthToken(HandOffAuthUri, claim.UserName, claim.Password, rmqId);
                var webClient = new ApiClient();

                if (!string.IsNullOrEmpty(HandoffAAuthToken) && !string.IsNullOrEmpty(resultJson))
                {
                    var data = new HandoffRequestModel() { JobType = JobTypes.IntegrationToBCS, Payload = resultJson };
                    webClient.Post<HandoffRequestModel>(HandOffServiceUri, data, HandoffAAuthToken, isHandoffCall:true).Wait();
                }
                else
                {
                    logger.WriteLog("PostDataToIntegrationBcsQueue: Handoff auth token/result json null - could not queue request", null, projectTitle, rmqId);
                    return false;
                }

                logger.WriteLog($"Pushed message from Integration to BCS Queue {resultJson}", null, projectTitle, rmqId);
                return true;
            }
            catch (Exception ex)
            {
                logger.WriteLog($"Exception in PostDataToIntegrationBcsQueue", ex, projectTitle, rmqId);
                return false;
            }
        }

        public void PostDataToBcsIntegrationQueue(string resultJson, Guid rmqId, string UserName, string Password)
        {
            logger.WriteLog($"PostDataToBcsIntegrationQueue Entry", null, projectTitle, rmqId);

            try
            {
                var HandoffAAuthToken = GetHandoffAuthToken(HandOffAuthUri, UserName, Password, rmqId);
                var webClient = new ApiClient();

                if (!string.IsNullOrEmpty(HandoffAAuthToken) && !string.IsNullOrEmpty(resultJson))
                {
                    var data = new HandoffRequestModel() { JobType = JobTypes.BcsToIntegration, Payload = resultJson };
                    webClient.Post<HandoffRequestModel>(HandOffServiceUri, data, HandoffAAuthToken, isHandoffCall:true).Wait();
                }
                else
                {
                    logger.WriteLog("PostDataToBcsIntegrationQueue: Handoff auth token/result json null - could not queue request", null, projectTitle, rmqId);
                    return;
                }

                logger.WriteLog($"Pushed message from BCS to Integration Queue {resultJson}", null, projectTitle, rmqId);
            }
            catch (Exception ex)
            {
                logger.WriteLog($"Exception in PostDataToBcsIntegrationQueue", ex, projectTitle, rmqId);
            }

            logger.WriteLog($"PostDataToBcsIntegrationQueue Exit", null, projectTitle, rmqId);
            return;
        }

        private static string GetHandoffAuthToken(string url, string username, string password, Guid correlationId)
        {
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri(url);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var httpContent = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("grant_type", "password"),
                        new KeyValuePair<string, string>("username", username),
                        new KeyValuePair<string, string>("password", password),
                    });

                    var response = client.PostAsync("token", httpContent);

                    response.Wait();
                    if (response.Result.IsSuccessStatusCode)
                    {
                        var result = response.Result.Content.ReadAsStringAsync();
                        result.Wait();
                        var tokendata = JsonConvert.DeserializeObject<AuthToken>(result.Result);
                        logger.WriteLog($"Retrieved access token from server", null, projectTitle, correlationId);
                        return tokendata.access_token;
                    }
                }
                catch (Exception ex)
                {
                    logger.WriteLog($"Exception in GetHandoffAuthToken()", ex, projectTitle, correlationId);
                }
                return null;
            }
        }
        private class AuthToken
        {
            public string access_token;

            public string token_type;

            public int expires_int;
        }
    }
}

