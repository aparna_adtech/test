﻿using com.breg.vision.MongoDataModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace com.breg.vision.MongoRepository.Abstractions
{
    public interface IRepository<T> where T : Entity
    {
        T GetById(string id);

        T Add(T entity);

        void Add(IEnumerable<T> entities);

        T Update(T entity);

        void Update(IEnumerable<T> entities);

        void Delete(string id);

        void Delete(T entity);

        void Delete(Expression<Func<T, bool>> predicate);

        long Count();

        bool Exists(Expression<Func<T, bool>> predicate);

        T Get(Expression<Func<T, bool>> predicate);

        List<T> GetAll(Expression<Func<T, bool>> predicate);

        List<T> All();
    }
}
