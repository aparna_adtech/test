﻿using com.breg.vision.MongoDataModels;
using com.breg.vision.MongoRepository.Abstractions;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;

namespace com.breg.vision.MongoRepository
{
    public class MongoRepository<T> : IRepository<T>
        where T : Entity
    {
        public MongoRepository()
        {
            this.collection = Util<string>.GetCollectionFromConnectionString<T>(ConfigurationManager.AppSettings["MongoDatabase"]);
        }

        private readonly MongoCollection<T> collection;

        public T GetById(string id)
        {
            return this.collection.AsQueryable<T>().FirstOrDefault(c => c.Id.Equals(id));
        }

        public T Add(T entity)
        {
            SetNewEntityProperties(entity);
            this.collection.Insert<T>(entity);
            return entity;
        }

        private void SetNewEntityProperties(T entity)
        {
            entity.Id = null;
            entity.CreatedOn = DateTime.Now;
            entity.UpdatedOn = DateTime.Now;
        }

        public void Add(IEnumerable<T> entities)
        {
            entities.AsParallel().ForAll((c) =>
            {
                SetNewEntityProperties(c);
            });
            this.collection.InsertBatch<T>(entities);
        }

        public virtual T Update(T entity)
        {
            entity.UpdatedOn = DateTime.Now;
            this.collection.Save<T>(entity);

            return entity;
        }

        public virtual void Update(IEnumerable<T> entities)
        {
            foreach (T entity in entities)
            {
                entity.UpdatedOn = DateTime.Now;
                this.collection.Save<T>(entity);
            }
        }

        public void Delete(string id)
        {
            this.DeleteEntity(id);
        }

        public void Delete(T entity)
        {
            this.DeleteEntity(entity.Id);
        }

        public void Delete(Expression<Func<T, bool>> predicate)
        {
            foreach (T entity in this.collection.AsQueryable<T>().Where(predicate))
            {
                this.DeleteEntity(entity.Id);
            }
        }

        private void DeleteEntity(string id)
        {
            this.collection.Remove(Query<T>.EQ(e => e.Id, id));
        }

        public long Count()
        {
            return this.collection.AsQueryable<T>().Count();
        }

        public bool Exists(Expression<Func<T, bool>> predicate)
        {
            return GetAll(predicate).Count() > 0;
        }

        public T Get(Expression<Func<T, bool>> predicate)
        {
            var entity = this.collection.AsQueryable<T>().FirstOrDefault(predicate);
            if (entity != null)
                return entity;

            return null;
        }

        public List<T> GetAll(Expression<Func<T, bool>> predicate)
        {
            return this.collection.AsQueryable<T>().Where(predicate).ToList();
        }

        public List<T> All()
        {
            return this.collection.AsQueryable<T>().ToList();
        }
    }
}
