﻿using System;

namespace com.breg.vision.MongoRepository
{
    [AttributeUsage(AttributeTargets.Class, Inherited = true)]
    public class CollectionName : Attribute
    {
        public CollectionName(string value)
        {
            if (string.IsNullOrEmpty(value) || value.Trim().Length == 0)
                throw new ArgumentException("Empty collectionname not allowed", "value");

            this.Name = value;
        }

        public virtual string Name { get; private set; }
    }
}
