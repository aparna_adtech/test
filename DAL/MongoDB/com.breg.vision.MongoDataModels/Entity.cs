﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace com.breg.vision.MongoDataModels
{
    public abstract class Entity
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public virtual string Id { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string RecordedBy { get; set; }
        public string UpdatedBy { get; set; }
    }
}
