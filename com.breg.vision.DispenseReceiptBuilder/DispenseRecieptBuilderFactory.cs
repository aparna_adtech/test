﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.breg.vision.DispenseReceiptBuilder.Builders;
using com.breg.vision.DispenseReceiptBuilder.DataModels;

namespace com.breg.vision.DispenseReceiptBuilder
{
    public static class DispenseRecieptBuilderFactory
    {
        public static IDispenseReceiptBuilder GetRecieptBuilder(DispensementDetails dispensement)
        {
			BuilderVersion version = BuilderVersion.V1;
			if (dispensement.DispenseDocumentVersion != null)
				version = (BuilderVersion)dispensement.DispenseDocumentVersion;

			switch (version)
            {
                case BuilderVersion.V1: return new V1DispenseReceiptBuilder(dispensement);

                case BuilderVersion.V2: return new V2DispenseReceiptBuilder(dispensement);

                default: throw new NotImplementedException();
            }
        }
    }
}
