﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.breg.vision.DispenseReceiptBuilder.DataModels
{
    public class DispenseItemDto
    {
        public int DispenseDetailID { get; set; }
        public int? DispenseQueueID { get; set; }
        public bool IsMedicare { get; set; }
        public bool ABNForm { get; set; }
        public string ABNType { get; set; }
        public bool? Cash { get; set; }
        public string Physician { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Code { get; set; }
        public string LRModifier { get; set; }
        public string Size { get; set; }
        public string HCPCs { get; set; }
        public string ICD9Code { get; set; }
        public int MedicareOption { get; set; }
        public string ActualChargeBilled { get; set; }
        public string DMEDeposit { get; set; }
        public int? QuantityToDispense { get; set; }
        public string AbnReasonText { get; set; }
        public string SupplierName { get; set; }
        public string WarrantyInfo { get; set; }
        public string Gender { get; set; }
    }
}
