﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace com.breg.vision.DispenseReceiptBuilder.DataModels
{
    public class DispensementDetails
    {
        public bool? CreatedWithHandheld;
        public bool IsPhysicianSigned;
        public DateTime? PhysicianSignedDate;
        public int DispenseID;
        public DateTime DateDispensed;
        public string PatientCode;
        public string PatientEmail;
        public string PatientFirstName;
        public string PatientLastName;
		public ClassLibrary.DAL.Practice Practice;
        public bool IsSecureMessagingExternalEnabled;
        public string PracticeLocationName;
        public bool EMRIntegrationDispenseReceiptPushEnabled;
        public bool EMRIntegrationHL7PushEnabled;
        public string EmailForPrintDispense;
        public bool EMREnabled;
        public string CloudConnectId;
        public PhysicianName PhysicianName;
        public string PatientRefusalReason;
        public DispenseSignatureMin PatientSignatureInfo;
        public DateTime? PatientSignedDate;
        public IQueryable<DispenseItemDto> DispenseItems;
		public int? DispenseDocumentVersion;
		public XElement DispenseDocumentFeaturesFlags;
        public string PatientSignerRelationship;
        public string FitterName;
    }

    public class DispenseSignatureMin
    {
        public int DispenseSignatureID;
    }  
    public class PhysicianName
    {
        public string FullName;
    }
}
