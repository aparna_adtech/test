﻿using com.breg.vision.DispenseReceiptBuilder.DataModels;
using com.breg.vision.DispenseReceiptBuilder.ViewLoaders;
using System.Web.UI.WebControls;

namespace com.breg.vision.DispenseReceiptBuilder
{
    public interface IDispenseReceiptBuilder
    {
        string BuildDispenseReceipt(int practiceLocationID, bool fromVision, bool? createPDF, bool isMedicareQuery, string patient_code, InertHTMLPage page, Panel ctlPatientReceipt, bool lastItem);
    }
}
