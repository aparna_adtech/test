﻿namespace com.breg.vision.DispenseReceiptBuilder
{
    public enum BuilderVersion
    {
        V1 = 1,
        V2 = 2
    }
}
