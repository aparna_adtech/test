﻿using ClassLibrary.DAL;
using com.breg.vision.DispenseReceiptBuilder.DataModels;
using com.breg.vision.DispenseReceiptBuilder.ViewLoaders;
using com.breg.vision.EmailTemplates;
using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.breg.vision.DispenseReceiptBuilder.Builders
{
    internal abstract class DispenseReceiptBuilderBase : IDispenseReceiptBuilder
	{
		protected string localEmailTemplatePath = ConfigurationManager.AppSettings["EmailTemplatePath"];
		protected DispensementDetails dispensement = null;

		public abstract string BuildDispenseReceipt(int practiceLocationID, bool fromVision, bool? createPDF, bool isMedicareQuery, string patient_code, InertHTMLPage page, Panel ctlPatientReceipt, bool lastItem);

		public DispenseReceiptBuilderBase(DispensementDetails dispensement)
		{
			this.dispensement = dispensement;
		}

        #region private methods
        #endregion
    }
}
