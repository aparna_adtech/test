﻿using ClassLibrary.DAL;
using com.breg.vision.DispenseReceiptBuilder.DataModels;
using com.breg.vision.DispenseReceiptBuilder.ViewLoaders;
using com.breg.vision.EmailTemplates;
using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace com.breg.vision.DispenseReceiptBuilder.Builders
{
    internal class V2DispenseReceiptBuilder : DispenseReceiptBuilderBase
	{
		bool IsSupportsBregBilling { get { return _isSupportsBregBilling; }  }
		bool _isSupportsBregBilling = false;

		public V2DispenseReceiptBuilder(DispensementDetails dispensement) : base(dispensement)
		{
			try
			{
				XDocument xPlatformXml = new XDocument(dispensement.DispenseDocumentFeaturesFlags);
				XElement xel = xPlatformXml.Element("Dispensement").Element("IsBregBilling");
				_isSupportsBregBilling = xel.Value.ToUpper() == bool.TrueString.ToUpper();
			}
			catch (Exception ex)
			{
			}
		}

		//TODO: Need to name the variable better and move this to config
		public override string BuildDispenseReceipt(int practiceLocationID, bool fromVision, bool? createPDF, bool isMedicareQuery, string patient_code, InertHTMLPage page, Panel ctlPatientReceipt, bool lastItem)
        {
            var sb = new StringBuilder();
            var sw = new StringWriter(sb);
            var htmlTW = new HtmlTextWriter(sw);

            if (!string.IsNullOrWhiteSpace(dispensement.PatientEmail) && !dispensement.IsSecureMessagingExternalEnabled)
            {
                AddHIPPAWaiverDetails(fromVision, createPDF, localEmailTemplatePath, dispensement, patient_code, page, ctlPatientReceipt);
            }

            AddDispenseReceiptDetails(practiceLocationID, fromVision, createPDF, localEmailTemplatePath, lastItem, dispensement, page, ctlPatientReceipt, IsSupportsBregBilling);

            AddServiceRefusalSection(page, localEmailTemplatePath, fromVision, ctlPatientReceipt, dispensement.Practice.PracticeName, dispensement.PatientSignatureInfo, dispensement.PatientSignedDate, dispensement.PatientRefusalReason, dispensement.PatientSignerRelationship);

            BuildWarrantyDetails(localEmailTemplatePath, isMedicareQuery, dispensement, page, ctlPatientReceipt);

			foreach (var dispenseItem in dispensement.DispenseItems)
                BuildDispenseItemDetails(practiceLocationID, fromVision, createPDF, localEmailTemplatePath, dispensement, patient_code, page, ctlPatientReceipt, dispenseItem);

            BuildSuplieStandardDetails(localEmailTemplatePath, isMedicareQuery, page, ctlPatientReceipt);

			if (IsSupportsBregBilling)
			{
				PDFRenderer.AddPDFAsControls(page, ctlPatientReceipt.Controls, "Patient Pages from breg.pat.form1.01462 Rev C 0117 1_19_17.pdf", localEmailTemplatePath);
				PDFRenderer.AddPDFAsControls(page, ctlPatientReceipt.Controls, "Licensing Agency Contact List AW-1.02223B 0117 w EC.pdf", localEmailTemplatePath);
			}

			try { ctlPatientReceipt.RenderControl(htmlTW); } catch { }
            return sb.ToString();
        }

        #region private methods
        private void BuildSuplieStandardDetails(string emailTemplatePath, bool isMedicareQuery, InertHTMLPage page, Panel ctlPatientReceipt)
        {
            if (isMedicareQuery)
            {
                SupplierStandard supplierStandard = page.LoadControl(string.Format("{0}SupplierStandard.ascx", emailTemplatePath)) as SupplierStandard;

                supplierStandard.LoadControlInfo();

                ctlPatientReceipt.Controls.Add(supplierStandard);
            }
        }

        private void BuildDispenseItemDetails(int practiceLocationID, bool fromVision, bool? createPDF, string emailTemplatePath, DispensementDetails dispensement, string patient_code, InertHTMLPage page, Panel ctlPatientReceipt, DispenseItemDto dispenseItem)
        {
            //for each dispense line item
            //Added a  missing line in the RenderDispenseReceiptPDF method for setting the ABN type from the SendEmail Method and moved the code to this common method
            // make an abn form for each item that requires it

            dispenseItem.ABNType = dispenseItem.ABNForm == true ? "Medicare" : dispenseItem.ABNType;
            if (dispenseItem.ABNType == "Medicare")
            {
                BuildABNDetailsForMedicare(practiceLocationID, fromVision, createPDF, emailTemplatePath, dispensement, patient_code, page, ctlPatientReceipt, dispenseItem);
            }
            else if (dispenseItem.ABNType == "Commercial")
            {
                BuildABNDetailsForCommercial(practiceLocationID, fromVision, emailTemplatePath, dispensement, patient_code, page, ctlPatientReceipt, dispenseItem);
            }

            // Make a capped rental notification form for each item that requires it
            BuildCappedRentalNotificationDetails(fromVision, createPDF, emailTemplatePath, dispensement, page, ctlPatientReceipt, dispenseItem);
        }

        private void BuildCappedRentalNotificationDetails(bool fromVision, bool? createPDF, string emailTemplatePath, DispensementDetails dispensement, InertHTMLPage page, Panel ctlPatientReceipt, DispenseItemDto dispenseItem)
        {
            if (IsDispenseItemRoutinelyPurchased(dispenseItem))

            {
                Notification notification = page.LoadControl(string.Format("{0}Notification.ascx", emailTemplatePath)) as Notification;
                notification.productName = dispenseItem.Name;
                notification.FromVision = createPDF.HasValue ? (fromVision && !createPDF.Value) : fromVision;
                if (dispensement.PatientSignatureInfo != null)
                {
                    notification.PatientSignatureID = dispensement.PatientSignatureInfo.DispenseSignatureID;
                }
                if (dispensement.PatientSignedDate != null && dispensement.PatientSignedDate.HasValue)
                {
                    notification.PatientSignedDate = dispensement.PatientSignedDate.Value;
                }

                notification.PatientSignerRelationship = dispensement.PatientSignerRelationship;
                notification.LoadControlInfo();
                ctlPatientReceipt.Controls.Add(notification);
            }
        }

        private void BuildABNDetailsForCommercial(int practiceLocationID, bool fromVision, string emailTemplatePath, DispensementDetails dispensement, string patient_code, InertHTMLPage page, Panel ctlPatientReceipt, DispenseItemDto dispenseItem)
        {
            #region get location address information
            var addressLine1 = "";
            var addressLine2 = "";
            var city = "";
            var state = "";
            var zipCode = "";

            var dalPracticeLocationShippingAddress = new DAL.PracticeLocation.ShippingAddress();
            var practiceLocationShippingAddress = dalPracticeLocationShippingAddress.Select(practiceLocationID);
            if (practiceLocationShippingAddress != null)
            {
                addressLine1 = practiceLocationShippingAddress.ShipAddress1;
                addressLine2 = practiceLocationShippingAddress.ShipAddress2;
                city = practiceLocationShippingAddress.ShipCity;
                state = practiceLocationShippingAddress.ShipState;
                zipCode = practiceLocationShippingAddress.ShipZipCode;
            }

            var contact = new DAL.PracticeLocation.Contact().Select(practiceLocationID);
            #endregion

            ABNFormCtlCommercial abnControlCommercial = page.LoadControl(string.Format("{0}ABNFormCtlCommercial.ascx", emailTemplatePath)) as ABNFormCtlCommercial;   //("/App_Resource/

            abnControlCommercial.PracticeName = dispensement.Practice.PracticeName;
            abnControlCommercial.Address1 = addressLine1;
            abnControlCommercial.Address2 = addressLine2;
            abnControlCommercial.City = city;
            abnControlCommercial.State = state;
            abnControlCommercial.Zip = zipCode;
            abnControlCommercial.PatientSignerRelationship = dispensement.PatientSignerRelationship;
            if (contact != null)
            {
                abnControlCommercial.BillingPhone = contact.PhoneWork;
                abnControlCommercial.BillingFax = contact.Fax;
            }

            abnControlCommercial.AbnReason = dispenseItem.AbnReasonText;
            abnControlCommercial.ProductName = dispenseItem.Name.ToString();
            abnControlCommercial.MedicareOption = dispenseItem.MedicareOption;
            if (!string.IsNullOrEmpty(patient_code))
            {
                abnControlCommercial.PatientCode = patient_code.ToString();
            }
            abnControlCommercial.PatientName = (dispensement.PatientFirstName ?? string.Empty) + " " + (dispensement.PatientLastName ?? string.Empty);
            abnControlCommercial.ProductCost = ConvertCurrencyToDecimal(dispenseItem.ActualChargeBilled);
            if (dispensement.PatientSignatureInfo != null)
            {
                abnControlCommercial.PatientSignatureID = dispensement.PatientSignatureInfo.DispenseSignatureID;
            }
            abnControlCommercial.FromVision = fromVision;
            if (dispensement.PatientSignedDate != null && dispensement.PatientSignedDate.HasValue)
            {
                abnControlCommercial.PatientSignedDate = dispensement.PatientSignedDate.Value;
            }
            abnControlCommercial.LoadControlInfo();
            ctlPatientReceipt.Controls.Add(abnControlCommercial);
        }

        private void BuildABNDetailsForMedicare(int practiceLocationID, bool fromVision, bool? createPDF, string emailTemplatePath, DispensementDetails dispensement, string patient_code, InertHTMLPage page, Panel ctlPatientReceipt, DispenseItemDto dispenseItem)
        {
            #region get location address information
            var addressLine1 = "";
            var addressLine2 = "";
            var city = "";
            var state = "";
            var zipCode = "";

            var dalPracticeLocationShippingAddress = new DAL.PracticeLocation.ShippingAddress();
            var practiceLocationShippingAddress = dalPracticeLocationShippingAddress.Select(practiceLocationID);
            if (practiceLocationShippingAddress != null)
            {
                addressLine1 = practiceLocationShippingAddress.ShipAddress1;
                addressLine2 = practiceLocationShippingAddress.ShipAddress2;
                city = practiceLocationShippingAddress.ShipCity;
                state = practiceLocationShippingAddress.ShipState;
                zipCode = practiceLocationShippingAddress.ShipZipCode;
            }

            var contact = new DAL.PracticeLocation.Contact().Select(practiceLocationID);
            #endregion

            ABNFormCtl abnControl = page.LoadControl(string.Format("{0}ABNFormCtl.ascx", emailTemplatePath)) as ABNFormCtl;   //("/App_Resource/BregWcfService.dll/" + "BregWcfService.ABNFormCtl.ascx");
            abnControl.PracticeName = dispensement.Practice.PracticeName;
            abnControl.PatientName = (dispensement.PatientFirstName ?? string.Empty) + " " + (dispensement.PatientLastName ?? string.Empty);

            if (dispensement.PatientSignatureInfo != null)
            {
                abnControl.PatientSignatureID = dispensement.PatientSignatureInfo.DispenseSignatureID;
            }
            abnControl.FromVision = createPDF.HasValue ? (fromVision && !createPDF.Value) : fromVision;
            if (dispensement.PatientSignedDate != null && dispensement.PatientSignedDate.HasValue)
            {
                abnControl.PatientSignedDate = dispensement.PatientSignedDate.Value;
            }
            abnControl.Address1 = addressLine1;
            abnControl.Address2 = addressLine2;
            abnControl.City = city;
            abnControl.State = state;
            abnControl.Zip = zipCode;

            if (contact != null)
            {
                abnControl.BillingPhone = contact.PhoneWork;
                abnControl.BillingFax = contact.Fax;
            }

            if (!string.IsNullOrEmpty(patient_code))
            {
                abnControl.PatientCode = patient_code.ToString();
            }

            abnControl.ProductName = dispenseItem.Name.ToString();
            abnControl.ProductCost = ConvertCurrencyToDecimal(dispenseItem.ActualChargeBilled);
            abnControl.AbnReason = dispenseItem.AbnReasonText;
            abnControl.MedicareOption = dispenseItem.MedicareOption;
            abnControl.ABNType = dispenseItem.ABNType;
            abnControl.PatientSignerRelationship = dispensement.PatientSignerRelationship;
            abnControl.LoadControlInfo();
            ctlPatientReceipt.Controls.Add(abnControl);
        }

        private void BuildWarrantyDetails(string emailTemplatePath, bool isMedicareQuery, DispensementDetails dispensement, InertHTMLPage page, Panel ctlPatientReceipt)
        {
            var distinctSupplierQuery = dispensement.DispenseItems.Select(di => di.SupplierName).Distinct();

            var SupplierQuery = (from distinctSupplier in distinctSupplierQuery
                                 select new WarrantySupplier
                                 {
                                     SupplierName = distinctSupplier,
                                     WarrantyProducts = (
                                         from di1 in dispensement.DispenseItems
                                         where distinctSupplier == di1.SupplierName
                                         select new WarrantyProduct
                                         {
                                             ProductName = di1.Name,
                                             Warranty = di1.WarrantyInfo
                                         }
                                     ).ToList<WarrantyProduct>()
                                 });

            Warranty warranty = page.LoadControl(string.Format("{0}Warranty.ascx", emailTemplatePath)) as Warranty;
            warranty.PracticeName = dispensement.Practice.PracticeName;
            //warranty.PatientSignatureUrl = patientSignatureUrl;
            warranty.WarrantySuppliers = SupplierQuery.ToList<WarrantySupplier>();
            warranty.IsMedicare = isMedicareQuery;
            warranty.LoadControlInfo();

            ctlPatientReceipt.Controls.Add(warranty);
        }

        private static void AddDispenseReceiptDetails(int practiceLocationID, bool fromVision, bool? createPDF, string emailTemplatePath, bool lastItem,
			DispensementDetails dispensement, InertHTMLPage page, Panel ctlPatientReceipt, bool isSupportsBregBilling)
        {
            DispenseReceipt dispenseReceiptControl = page.LoadControl(string.Format("{0}DispenseReceipt.ascx", emailTemplatePath)) as DispenseReceipt;
            dispenseReceiptControl.Practice = dispensement.Practice;
            dispenseReceiptControl.PracticeLocationID = practiceLocationID;

            if (dispensement.PatientSignatureInfo != null)
            {
                dispenseReceiptControl.PatientSignatureID = dispensement.PatientSignatureInfo.DispenseSignatureID;
            }

            if (dispensement.PatientSignerRelationship != null)
            {
                dispenseReceiptControl.PatientSignerRelationship = dispensement.PatientSignerRelationship;
            }

            if (dispensement.PatientSignedDate != null && dispensement.PatientSignedDate.HasValue)
            {
                dispenseReceiptControl.PatientSignedDate = dispensement.PatientSignedDate.Value;
            }
            
            if (dispensement.FitterName != null)
            {
                dispenseReceiptControl.FitterName = dispensement.FitterName;
            }

			dispenseReceiptControl.DispenseID = dispensement.DispenseID;
            dispenseReceiptControl.Practice = dispensement.Practice;
            dispenseReceiptControl.PracticeLocationName = dispensement.PracticeLocationName;
            dispenseReceiptControl.IsBillingChargeDisplayed =
               DAL.Practice.Practice.IsDisplayBillChargeOnReceipt(dispensement.Practice.PracticeID);
            dispenseReceiptControl.LastItem = lastItem;
            dispenseReceiptControl.PatientRefusalReason = dispensement.PatientRefusalReason;
            dispenseReceiptControl.FromVision = createPDF.HasValue ? (fromVision && !createPDF.Value) : fromVision;
			dispenseReceiptControl.IsSupportsBregBilling = isSupportsBregBilling;

			dispenseReceiptControl.LoadControlInfo(dispensement.DispenseID, emailTemplatePath);

            ctlPatientReceipt.Controls.Add(dispenseReceiptControl);
        }

        private static void AddHIPPAWaiverDetails(bool fromVision, bool? createPDF, string emailTemplatePath, DispensementDetails dispensement, string patient_code, InertHTMLPage page, Panel ctlPatientReceipt)
        {
            HippaWaiver hippaWaiver = page.LoadControl(string.Format("{0}HippaWaiver.ascx", emailTemplatePath)) as HippaWaiver;
            hippaWaiver.PracticeName = dispensement.Practice.PracticeName;
            hippaWaiver.PatientEmail = dispensement.PatientEmail;
            hippaWaiver.PatientCode = patient_code;
            hippaWaiver.CreatedDate = dispensement.DateDispensed;
            hippaWaiver.PatientName = (dispensement.PatientFirstName ?? string.Empty) + " " + (dispensement.PatientLastName ?? string.Empty);
            hippaWaiver.PatientSignerRelationship = dispensement.PatientSignerRelationship;

            if (!string.IsNullOrWhiteSpace(dispensement.PatientRefusalReason))
            {
                hippaWaiver.PatientRefusalReason = dispensement.PatientRefusalReason;
            }

            if (dispensement.PatientSignatureInfo != null && dispensement.PatientSignatureInfo.DispenseSignatureID != 0)
            {
                hippaWaiver.PatientSignatureID = dispensement.PatientSignatureInfo.DispenseSignatureID;
            }

            if (dispensement.PatientSignedDate != null && dispensement.PatientSignedDate.HasValue)
            {
                hippaWaiver.PatientSignedDate = dispensement.PatientSignedDate.Value;
            }

            hippaWaiver.FromVision = createPDF.HasValue ? (fromVision && !createPDF.Value) : fromVision;
            hippaWaiver.LoadControlInfo();
            ctlPatientReceipt.Controls.Add(hippaWaiver);
        }

        private bool IsDispenseItemRoutinelyPurchased(DispenseItemDto dispenseItemDto)
        {

            string hcpcsCode = dispenseItemDto.HCPCs;
            if (hcpcsCode == null)
            {
                return false;
            }

            hcpcsCode = hcpcsCode.Trim().ToUpper();

            if (!dispenseItemDto.IsMedicare)
            {
                return false;
            }

            if (!hcpcsCode.StartsWith("E"))
            {
                return false;
            }

            bool isValidHcpcs = IsValidHcpcs(hcpcsCode);

            return isValidHcpcs;
        }
        private bool IsValidHcpcs(string code)
        {
            if (code == null || code.Length < 5)
            {
                return false;
            }

            var trimmedCode = code.Trim().Substring(0, 5);

            using (var visionDb = VisionDataContext.GetVisionDataContext())
            {
                return visionDb.HCPCs.Any(h => h.Code == trimmedCode);
            }
        }

        private static void AddServiceRefusalSection(InertHTMLPage page, string emailTemplatePath, bool fromVision, Panel ctlPatientReceipt, string practiceName, DispenseSignatureMin patientSignatureInfo, DateTime? patientSignedDate, string patientRefusalReason, string patientSignerRelationship)
        {
            // NOTE: Do not add refusal section if there is no refusal reason or if the reason is
            // "ACCEPTED"
            if (string.IsNullOrWhiteSpace(patientRefusalReason)
                || patientRefusalReason.ToUpper() == "ACCEPTED")
            {
                return;
            }

            var serviceRefusal =
                page.LoadControl(
                    string.Format("{0}ServiceRefusal.ascx", emailTemplatePath)
                ) as ServiceRefusal;

            serviceRefusal.PracticeName = practiceName;
            serviceRefusal.PatientRefusalReason = patientRefusalReason;
            serviceRefusal.FromVision = fromVision;

            if (patientSignatureInfo != null)
                serviceRefusal.PatientSignatureID = patientSignatureInfo.DispenseSignatureID;

            if (patientSignedDate != null)
                serviceRefusal.PatientSignedDate = patientSignedDate.Value;

            if (patientSignerRelationship != null)
                serviceRefusal.PatientSignerRelationship = patientSignerRelationship;

            serviceRefusal.LoadControlInfo();

            ctlPatientReceipt.Controls.Add(serviceRefusal);
        }
        //This is copy paste method and needs to be optimised. Copy pasted from Email.Cs in BregWCFService
        private decimal ConvertCurrencyToDecimal(string currency)
        {
            decimal decimalValue;
            if (decimal.TryParse(currency, NumberStyles.Currency, NumberFormatInfo.CurrentInfo, out decimalValue))
            {
                return decimalValue;
            }
            return -1;
        }
        #endregion
    }
}
