﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace com.breg.vision.DispenseReceiptBuilder.ViewLoaders
{
    public class InertHTMLPage : System.Web.UI.Page
    {
        public override void VerifyRenderingInServerForm(Control control)
        {
            // DO NOTHING
        }
        public override bool EnableEventValidation
        {
            get
            {
                return false;
            }
            set
            {
                // DO NOTHING
            }
        }
    }
}
